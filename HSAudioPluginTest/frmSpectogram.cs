﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using HS_Audio;

namespace HSAudioPluginTest
{
    public partial class frmSpectogram : Form
    {
        volatile HSAudioHelper Helper;
        public frmSpectogram(HSAudioHelper Helper)
        {
            CheckForIllegalCrossThreadCalls = false;
            this.Helper = Helper;
            InitializeComponent();
            //comboBox1.Items.AddRange(Enum.GetNames(typeof(HS_Audio_Lib.DSP_FFT_WINDOW)));
            //comboBox1.SelectedItem = "TRIANGLE";
            comboBox1.SelectedIndex = 2;
        }

        private void frmSpectogram_Load(object sender, EventArgs e)
        {
            FFTSize = int.Parse(domainUpDown1.Text);
            try{FFTWindow = (HS_Audio_Lib.DSP_FFT_WINDOW)Enum.Parse(typeof(HS_Audio_Lib.DSP_FFT_WINDOW), comboBox1.SelectedItem.ToString());}catch{}
            hsSpectogramViewer1.DrawColor = hsSpectogramViewer2.DrawColor = colorDialog1.Color;
            hsSpectogramViewer1.MindB = hsSpectogramViewer2.MindB = (float)numericUpDown2.Value;
            hsSpectogramViewer1.Reverse = hsSpectogramViewer2.Reverse = checkBox1.Checked;
            오차 = (float)numericUpDown3.Value;
            timer1.Interval = (int)numericUpDown1.Value;
            //timer1.Start();
            //ThreadPool.QueueUserWorkItem(new WaitCallback(DrawSpec));
            DrawSpecThread = new Thread(new ParameterizedThreadStart(DrawSpec));
            //DrawSpecThread1 = new Thread(new ParameterizedThreadStart(DrawSpec1));
            DrawSpecThread.Start(); //DrawSpecThread1.Start();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
        }
        volatile bool Stop;
        Thread DrawSpecThread, DrawSpecThread1;
        void DrawSpec(object o)
        {
            //InitializeComponent();
            while (!Stop)
            {
                try
                {
                    float[] FFT = new float[FFTSize];
                    Helper.system.getSpectrum(FFT, FFT.Length, 0, FFTWindow);
                    hsSpectogramViewer1.Draw(FFT, 오차); 
                    Helper.system.getSpectrum(FFT, FFT.Length, 1, FFTWindow);
                    hsSpectogramViewer2.Draw(FFT, 오차);
                    Thread.Sleep(Sleep);
                }catch(Exception ex){Thread.Sleep(1000);}
            }
        }
        void DrawSpec1(object o)
        {
            //InitializeComponent();
            while (!Stop)
            {
                try
                {
                    float[] FFT = new float[FFTSize];
                    Helper.system.getSpectrum(FFT, FFT.Length, 1, FFTWindow);
                    hsSpectogramViewer2.Draw(FFT, 오차);
                    Thread.Sleep(Sleep);
                }catch{}
            }
        }
        
        int FFTSize = 1024;
        public HS_Audio_Lib.DSP_FFT_WINDOW FFTWindow{get;set;}


        private void domainUpDown1_TextChanged(object sender, EventArgs e)
        {
            FFTSize = int.Parse(domainUpDown1.Text);
        }

        private void comboBox1_SelectedValueChanged(object sender, EventArgs e)
        {
            FFTWindow = (HS_Audio_Lib.DSP_FFT_WINDOW)Enum.Parse(typeof(HS_Audio_Lib.DSP_FFT_WINDOW), comboBox1.SelectedItem.ToString());
        }

        volatile int Sleep = 100;
        private void numericUpDown1_ValueChanged(object sender, EventArgs e)
        {
            timer1.Interval = (int)numericUpDown1.Value;
            Sleep = (int)numericUpDown1.Value;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (colorDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK) hsSpectogramViewer1.DrawColor = hsSpectogramViewer2.DrawColor = colorDialog1.Color;
        }

        private void numericUpDown2_ValueChanged(object sender, EventArgs e)
        {
            hsSpectogramViewer1.MindB = hsSpectogramViewer2.MindB = (float)numericUpDown2.Value;
        }
        
        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            hsSpectogramViewer1.Reverse = hsSpectogramViewer2.Reverse = checkBox1.Checked;
        }

        float 오차 = 1;
        private void numericUpDown3_ValueChanged(object sender, EventArgs e)
        {
            오차 = (float)numericUpDown3.Value;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Stop = false;
            if (DrawSpecThread!=null)DrawSpecThread.Abort();
            Thread.Sleep(1);
            //hsSpectogramViewer1.DisposeInstance(); hsSpectogramViewer2.DisposeInstance();
            hsSpectogramViewer1.CreateInstance(); hsSpectogramViewer2.CreateInstance();
            DrawSpecThread = new Thread(new ParameterizedThreadStart(DrawSpec));
            DrawSpecThread.Start();
        }

        private void frmSpectogram_KeyPress(object sender, KeyPressEventArgs e)
        {

        }

        private void frmSpectogram_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.KeyData == Keys.F5)button2_Click(null, null);
        }
        bool rev;
        private void checkBox2_CheckedChanged(object sender, EventArgs e)
        {
            hsSpectogramViewer1.DrawWidth = hsSpectogramViewer2.DrawWidth = checkBox2.Checked;
        }
        //HS_Audio_Helper.Forms.frmFFTGraph.FFTSize _Size = HS_Audio_Helper.Forms.frmFFTGraph.FFTSize.Size_512;
        //public HS_Audio_Helper.Forms.frmFFTGraph.FFTSize Size{get{return _Size;}set{_Size = value;}}
    }
}
