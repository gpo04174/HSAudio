﻿namespace HSAudioPluginTest
{
    partial class frmPeakMeter
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.맨위에표시ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.배경투명ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripTextBox2 = new System.Windows.Forms.ToolStripTextBox();
            this.분할자투명ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.테두리표시ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.설정창띄우기좌ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.설정창띄우기우ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.눈금크기ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripTextBox();
            this.스펙트럼할당방법ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.fMODSystemToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.fMODChannelToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.가로배열ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.그래프가로배열ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.분할자세로배열ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.측정속도ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripTextBox1 = new System.Windows.Forms.ToolStripTextBox();
            this.측정크기ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripTextBox3 = new System.Windows.Forms.ToolStripTextBox();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.peak새로고침ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.peakMeterCtrl1 = new Ernzo.WinForms.Controls.PeakMeterCtrl();
            this.peakMeterCtrl2 = new Ernzo.WinForms.Controls.PeakMeterCtrl();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.contextMenuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.BackColor = System.Drawing.SystemColors.Control;
            this.splitContainer1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.splitContainer1.ContextMenuStrip = this.contextMenuStrip1;
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.BackColor = System.Drawing.Color.Transparent;
            this.splitContainer1.Panel1.Controls.Add(this.peakMeterCtrl1);
            this.splitContainer1.Panel1.Resize += new System.EventHandler(this.splitContainer1_Panel2_Resize);
            this.splitContainer1.Panel1MinSize = 1;
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.BackColor = System.Drawing.Color.Transparent;
            this.splitContainer1.Panel2.Controls.Add(this.peakMeterCtrl2);
            this.splitContainer1.Panel2.Resize += new System.EventHandler(this.splitContainer1_Panel2_Resize);
            this.splitContainer1.Panel2MinSize = 1;
            this.splitContainer1.Size = new System.Drawing.Size(632, 223);
            this.splitContainer1.SplitterDistance = 110;
            this.splitContainer1.SplitterWidth = 3;
            this.splitContainer1.TabIndex = 5;
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.맨위에표시ToolStripMenuItem,
            this.배경투명ToolStripMenuItem,
            this.테두리표시ToolStripMenuItem,
            this.toolStripSeparator2,
            this.설정창띄우기좌ToolStripMenuItem,
            this.설정창띄우기우ToolStripMenuItem,
            this.toolStripSeparator1,
            this.눈금크기ToolStripMenuItem,
            this.스펙트럼할당방법ToolStripMenuItem,
            this.가로배열ToolStripMenuItem,
            this.toolStripSeparator3,
            this.측정속도ToolStripMenuItem,
            this.측정크기ToolStripMenuItem,
            this.toolStripSeparator4,
            this.peak새로고침ToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(203, 270);
            // 
            // 맨위에표시ToolStripMenuItem
            // 
            this.맨위에표시ToolStripMenuItem.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.맨위에표시ToolStripMenuItem.CheckOnClick = true;
            this.맨위에표시ToolStripMenuItem.Name = "맨위에표시ToolStripMenuItem";
            this.맨위에표시ToolStripMenuItem.Size = new System.Drawing.Size(202, 22);
            this.맨위에표시ToolStripMenuItem.Text = "맨위에 표시";
            this.맨위에표시ToolStripMenuItem.ToolTipText = "이 창을 맨위로 설정합니다.";
            this.맨위에표시ToolStripMenuItem.CheckedChanged += new System.EventHandler(this.맨위에표시ToolStripMenuItem_CheckedChanged);
            // 
            // 배경투명ToolStripMenuItem
            // 
            this.배경투명ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripTextBox2,
            this.분할자투명ToolStripMenuItem});
            this.배경투명ToolStripMenuItem.Name = "배경투명ToolStripMenuItem";
            this.배경투명ToolStripMenuItem.Size = new System.Drawing.Size(202, 22);
            this.배경투명ToolStripMenuItem.Text = "배경 투명";
            this.배경투명ToolStripMenuItem.ToolTipText = "백그라운드 배경을 투명 처리합니다.\r\n[단축키: F4]";
            this.배경투명ToolStripMenuItem.CheckStateChanged += new System.EventHandler(this.배경투명ToolStripMenuItem_CheckStateChanged);
            this.배경투명ToolStripMenuItem.Click += new System.EventHandler(this.배경투명ToolStripMenuItem_Click);
            // 
            // toolStripTextBox2
            // 
            this.toolStripTextBox2.Enabled = false;
            this.toolStripTextBox2.MaxLength = 9;
            this.toolStripTextBox2.Name = "toolStripTextBox2";
            this.toolStripTextBox2.Size = new System.Drawing.Size(100, 23);
            this.toolStripTextBox2.Text = "#FFFFFFFF";
            this.toolStripTextBox2.ToolTipText = "투명화할 색깔을 설정합니다.";
            this.toolStripTextBox2.Visible = false;
            this.toolStripTextBox2.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.toolStripTextBox2_KeyPress);
            this.toolStripTextBox2.TextChanged += new System.EventHandler(this.toolStripTextBox2_TextChanged);
            // 
            // 분할자투명ToolStripMenuItem
            // 
            this.분할자투명ToolStripMenuItem.CheckOnClick = true;
            this.분할자투명ToolStripMenuItem.Name = "분할자투명ToolStripMenuItem";
            this.분할자투명ToolStripMenuItem.Size = new System.Drawing.Size(160, 22);
            this.분할자투명ToolStripMenuItem.Text = "분할자 투명";
            this.분할자투명ToolStripMenuItem.CheckedChanged += new System.EventHandler(this.분할자투명ToolStripMenuItem_CheckedChanged);
            // 
            // 테두리표시ToolStripMenuItem
            // 
            this.테두리표시ToolStripMenuItem.Checked = true;
            this.테두리표시ToolStripMenuItem.CheckOnClick = true;
            this.테두리표시ToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.테두리표시ToolStripMenuItem.Name = "테두리표시ToolStripMenuItem";
            this.테두리표시ToolStripMenuItem.Size = new System.Drawing.Size(202, 22);
            this.테두리표시ToolStripMenuItem.Text = "테두리 표시";
            this.테두리표시ToolStripMenuItem.CheckedChanged += new System.EventHandler(this.테두리표시ToolStripMenuItem_CheckedChanged);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(199, 6);
            // 
            // 설정창띄우기좌ToolStripMenuItem
            // 
            this.설정창띄우기좌ToolStripMenuItem.Name = "설정창띄우기좌ToolStripMenuItem";
            this.설정창띄우기좌ToolStripMenuItem.Size = new System.Drawing.Size(202, 22);
            this.설정창띄우기좌ToolStripMenuItem.Text = "고급 설정창 띄우기 (좌)";
            this.설정창띄우기좌ToolStripMenuItem.Click += new System.EventHandler(this.설정창띄우기ToolStripMenuItem_Click);
            // 
            // 설정창띄우기우ToolStripMenuItem
            // 
            this.설정창띄우기우ToolStripMenuItem.Name = "설정창띄우기우ToolStripMenuItem";
            this.설정창띄우기우ToolStripMenuItem.Size = new System.Drawing.Size(202, 22);
            this.설정창띄우기우ToolStripMenuItem.Text = "고급 설정창 띄우기 (우)";
            this.설정창띄우기우ToolStripMenuItem.Click += new System.EventHandler(this.설정창띄우기우ToolStripMenuItem_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(199, 6);
            // 
            // 눈금크기ToolStripMenuItem
            // 
            this.눈금크기ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem2});
            this.눈금크기ToolStripMenuItem.Name = "눈금크기ToolStripMenuItem";
            this.눈금크기ToolStripMenuItem.Size = new System.Drawing.Size(202, 22);
            this.눈금크기ToolStripMenuItem.Text = "측정 범위 (눈금 갯수)";
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(152, 23);
            this.toolStripMenuItem2.Text = "30";
            this.toolStripMenuItem2.ToolTipText = "1칸은 -1dB 를 나타냅니다.\r\n(만약 30이면 -30dB 부터 0dB까지 측정 한다는 것입니다.)";
            this.toolStripMenuItem2.KeyDown += new System.Windows.Forms.KeyEventHandler(this.toolStripMenuItem2_KeyDown);
            this.toolStripMenuItem2.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.toolStripMenuItem2_KeyPress);
            this.toolStripMenuItem2.TextChanged += new System.EventHandler(this.toolStripMenuItem2_TextChanged);
            // 
            // 스펙트럼할당방법ToolStripMenuItem
            // 
            this.스펙트럼할당방법ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fMODSystemToolStripMenuItem,
            this.fMODChannelToolStripMenuItem});
            this.스펙트럼할당방법ToolStripMenuItem.Name = "스펙트럼할당방법ToolStripMenuItem";
            this.스펙트럼할당방법ToolStripMenuItem.Size = new System.Drawing.Size(202, 22);
            this.스펙트럼할당방법ToolStripMenuItem.Text = "그래프 할당 방법";
            this.스펙트럼할당방법ToolStripMenuItem.DropDownOpening += new System.EventHandler(this.스펙트럼할당방법ToolStripMenuItem_DropDownOpening);
            // 
            // fMODSystemToolStripMenuItem
            // 
            this.fMODSystemToolStripMenuItem.Checked = true;
            this.fMODSystemToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.fMODSystemToolStripMenuItem.Name = "fMODSystemToolStripMenuItem";
            this.fMODSystemToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F2;
            this.fMODSystemToolStripMenuItem.Size = new System.Drawing.Size(163, 22);
            this.fMODSystemToolStripMenuItem.Text = "Main Output";
            this.fMODSystemToolStripMenuItem.ToolTipText = "마스터 출력 입니다.";
            this.fMODSystemToolStripMenuItem.CheckedChanged += new System.EventHandler(this.fMODChannelToolStripMenuItem_CheckedChanged);
            this.fMODSystemToolStripMenuItem.Click += new System.EventHandler(this.fMODSystemToolStripMenuItem_Click);
            // 
            // fMODChannelToolStripMenuItem
            // 
            this.fMODChannelToolStripMenuItem.Name = "fMODChannelToolStripMenuItem";
            this.fMODChannelToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F3;
            this.fMODChannelToolStripMenuItem.Size = new System.Drawing.Size(163, 22);
            this.fMODChannelToolStripMenuItem.Text = "Channel";
            this.fMODChannelToolStripMenuItem.ToolTipText = "개별 출력 입니다.";
            this.fMODChannelToolStripMenuItem.CheckedChanged += new System.EventHandler(this.fMODChannelToolStripMenuItem_CheckedChanged);
            this.fMODChannelToolStripMenuItem.Click += new System.EventHandler(this.fMODChannelToolStripMenuItem_Click);
            // 
            // 가로배열ToolStripMenuItem
            // 
            this.가로배열ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.그래프가로배열ToolStripMenuItem,
            this.분할자세로배열ToolStripMenuItem});
            this.가로배열ToolStripMenuItem.Name = "가로배열ToolStripMenuItem";
            this.가로배열ToolStripMenuItem.Size = new System.Drawing.Size(202, 22);
            this.가로배열ToolStripMenuItem.Text = "배열 스타일";
            this.가로배열ToolStripMenuItem.CheckedChanged += new System.EventHandler(this.가로배열ToolStripMenuItem_CheckedChanged);
            this.가로배열ToolStripMenuItem.Click += new System.EventHandler(this.세로배열ToolStripMenuItem_Click);
            // 
            // 그래프가로배열ToolStripMenuItem
            // 
            this.그래프가로배열ToolStripMenuItem.Checked = true;
            this.그래프가로배열ToolStripMenuItem.CheckOnClick = true;
            this.그래프가로배열ToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.그래프가로배열ToolStripMenuItem.Name = "그래프가로배열ToolStripMenuItem";
            this.그래프가로배열ToolStripMenuItem.Size = new System.Drawing.Size(166, 22);
            this.그래프가로배열ToolStripMenuItem.Text = "그래프 가로 배열";
            this.그래프가로배열ToolStripMenuItem.CheckedChanged += new System.EventHandler(this.그래프가로배열ToolStripMenuItem_CheckedChanged);
            // 
            // 분할자세로배열ToolStripMenuItem
            // 
            this.분할자세로배열ToolStripMenuItem.Checked = true;
            this.분할자세로배열ToolStripMenuItem.CheckOnClick = true;
            this.분할자세로배열ToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.분할자세로배열ToolStripMenuItem.Name = "분할자세로배열ToolStripMenuItem";
            this.분할자세로배열ToolStripMenuItem.Size = new System.Drawing.Size(166, 22);
            this.분할자세로배열ToolStripMenuItem.Text = "분할자 가로 배열";
            this.분할자세로배열ToolStripMenuItem.CheckedChanged += new System.EventHandler(this.분할자가로배열ToolStripMenuItem_CheckedChanged);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(199, 6);
            // 
            // 측정속도ToolStripMenuItem
            // 
            this.측정속도ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripTextBox1});
            this.측정속도ToolStripMenuItem.Name = "측정속도ToolStripMenuItem";
            this.측정속도ToolStripMenuItem.Size = new System.Drawing.Size(202, 22);
            this.측정속도ToolStripMenuItem.Text = "측정 속도";
            this.측정속도ToolStripMenuItem.ToolTipText = "측정할 속도를 설정합니다. (밀리세컨트 단위 입니다.)";
            // 
            // toolStripTextBox1
            // 
            this.toolStripTextBox1.MaxLength = 5;
            this.toolStripTextBox1.Name = "toolStripTextBox1";
            this.toolStripTextBox1.Size = new System.Drawing.Size(100, 23);
            this.toolStripTextBox1.Text = "25";
            this.toolStripTextBox1.ToolTipText = "측정할 밀리세컨드 (ms) 입니다.";
            this.toolStripTextBox1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.toolStripTextBox1_KeyPress);
            this.toolStripTextBox1.TextChanged += new System.EventHandler(this.toolStripTextBox1_TextChanged);
            // 
            // 측정크기ToolStripMenuItem
            // 
            this.측정크기ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripTextBox3});
            this.측정크기ToolStripMenuItem.Name = "측정크기ToolStripMenuItem";
            this.측정크기ToolStripMenuItem.Size = new System.Drawing.Size(202, 22);
            this.측정크기ToolStripMenuItem.Text = "측정 크기";
            // 
            // toolStripTextBox3
            // 
            this.toolStripTextBox3.Name = "toolStripTextBox3";
            this.toolStripTextBox3.Size = new System.Drawing.Size(100, 23);
            this.toolStripTextBox3.Text = "3000";
            this.toolStripTextBox3.TextChanged += new System.EventHandler(this.toolStripTextBox3_TextChanged);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(199, 6);
            // 
            // peak새로고침ToolStripMenuItem
            // 
            this.peak새로고침ToolStripMenuItem.Name = "peak새로고침ToolStripMenuItem";
            this.peak새로고침ToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F5;
            this.peak새로고침ToolStripMenuItem.Size = new System.Drawing.Size(202, 22);
            this.peak새로고침ToolStripMenuItem.Text = "Peak 새로고침";
            this.peak새로고침ToolStripMenuItem.ToolTipText = "최대 Peak값을 새로고침 합니다.";
            this.peak새로고침ToolStripMenuItem.Click += new System.EventHandler(this.peak새로고침ToolStripMenuItem_Click);
            // 
            // peakMeterCtrl1
            // 
            this.peakMeterCtrl1.BandsCount = 1;
            this.peakMeterCtrl1.ColorHigh = System.Drawing.Color.Red;
            this.peakMeterCtrl1.ColorHighBack = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.peakMeterCtrl1.ColorMedium = System.Drawing.Color.Yellow;
            this.peakMeterCtrl1.ColorMediumBack = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(210)))));
            this.peakMeterCtrl1.ColorNormal = System.Drawing.Color.Green;
            this.peakMeterCtrl1.ColorNormalBack = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(210)))));
            this.peakMeterCtrl1.ContextMenuStrip = this.contextMenuStrip1;
            this.peakMeterCtrl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.peakMeterCtrl1.LED눈금갯수 = 30;
            this.peakMeterCtrl1.Location = new System.Drawing.Point(0, 0);
            this.peakMeterCtrl1.MeterStyle = Ernzo.WinForms.Controls.PeakMeterStyle.PMS_Vertical;
            this.peakMeterCtrl1.Name = "peakMeterCtrl1";
            this.peakMeterCtrl1.Peak굵기 = 2;
            this.peakMeterCtrl1.Peak색상 = System.Drawing.Color.Black;
            this.peakMeterCtrl1.Peak속도 = 15;
            this.peakMeterCtrl1.Size = new System.Drawing.Size(630, 108);
            this.peakMeterCtrl1.TabIndex = 0;
            this.peakMeterCtrl1.Text = "peakMeterCtrl1";
            this.toolTip1.SetToolTip(this.peakMeterCtrl1, "좌 Peak 미터");
            this.peakMeterCtrl1.모눈선색상 = System.Drawing.Color.Gainsboro;
            this.peakMeterCtrl1.SizeChanged += new System.EventHandler(this.peakMeterCtrl1_SizeChanged);
            this.peakMeterCtrl1.DoubleClick += new System.EventHandler(this.peakMeterCtrl1_DoubleClick);
            this.peakMeterCtrl1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.peakMeterCtrl1_KeyDown);
            this.peakMeterCtrl1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.peakMeterCtrl1_MouseDown);
            this.peakMeterCtrl1.Resize += new System.EventHandler(this.peakMeterCtrl1_Resize);
            // 
            // peakMeterCtrl2
            // 
            this.peakMeterCtrl2.BandsCount = 1;
            this.peakMeterCtrl2.ColorHigh = System.Drawing.Color.Red;
            this.peakMeterCtrl2.ColorHighBack = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.peakMeterCtrl2.ColorMedium = System.Drawing.Color.Yellow;
            this.peakMeterCtrl2.ColorMediumBack = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(210)))));
            this.peakMeterCtrl2.ColorNormal = System.Drawing.Color.Green;
            this.peakMeterCtrl2.ColorNormalBack = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(210)))));
            this.peakMeterCtrl2.ContextMenuStrip = this.contextMenuStrip1;
            this.peakMeterCtrl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.peakMeterCtrl2.LED눈금갯수 = 30;
            this.peakMeterCtrl2.Location = new System.Drawing.Point(0, 0);
            this.peakMeterCtrl2.MeterStyle = Ernzo.WinForms.Controls.PeakMeterStyle.PMS_Vertical;
            this.peakMeterCtrl2.Name = "peakMeterCtrl2";
            this.peakMeterCtrl2.Peak굵기 = 2;
            this.peakMeterCtrl2.Peak색상 = System.Drawing.Color.Black;
            this.peakMeterCtrl2.Peak속도 = 15;
            this.peakMeterCtrl2.Size = new System.Drawing.Size(630, 108);
            this.peakMeterCtrl2.TabIndex = 0;
            this.peakMeterCtrl2.Text = "peakMeterCtrl2";
            this.toolTip1.SetToolTip(this.peakMeterCtrl2, "우 Peak 미터");
            this.peakMeterCtrl2.모눈선색상 = System.Drawing.Color.Gainsboro;
            this.peakMeterCtrl2.PropertiesChanged += new Ernzo.WinForms.Controls.PropertiesChangedEventHandler(this.peakMeterCtrl2_PropertiesChanged);
            this.peakMeterCtrl2.SizeChanged += new System.EventHandler(this.peakMeterCtrl2_SizeChanged);
            this.peakMeterCtrl2.DoubleClick += new System.EventHandler(this.peakMeterCtrl1_DoubleClick);
            this.peakMeterCtrl2.KeyDown += new System.Windows.Forms.KeyEventHandler(this.peakMeterCtrl1_KeyDown);
            this.peakMeterCtrl2.MouseDown += new System.Windows.Forms.MouseEventHandler(this.peakMeterCtrl2_MouseDown);
            this.peakMeterCtrl2.Resize += new System.EventHandler(this.peakMeterCtrl2_Resize);
            // 
            // timer1
            // 
            this.timer1.Interval = 25;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // toolTip1
            // 
            this.toolTip1.AutomaticDelay = 100;
            this.toolTip1.BackColor = System.Drawing.Color.Black;
            this.toolTip1.ForeColor = System.Drawing.Color.White;
            this.toolTip1.IsBalloon = true;
            this.toolTip1.OwnerDraw = true;
            this.toolTip1.Draw += new System.Windows.Forms.DrawToolTipEventHandler(this.toolTip1_Draw);
            // 
            // frmPeakMeter
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Gainsboro;
            this.ClientSize = new System.Drawing.Size(632, 223);
            this.Controls.Add(this.splitContainer1);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
            this.Name = "frmPeakMeter";
            this.Text = "RMS 미터 [좌: -Inf. dB / 우: -Inf. dB] (Mono) - RMS (좌: -Inf. dB / 우: -Inf. dB)";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmPeakMeter_FormClosing);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.peakMeterCtrl1_KeyDown);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.ResumeLayout(false);
            this.contextMenuStrip1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.Timer timer1;
        internal Ernzo.WinForms.Controls.PeakMeterCtrl peakMeterCtrl1;
        internal Ernzo.WinForms.Controls.PeakMeterCtrl peakMeterCtrl2;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem 설정창띄우기좌ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 설정창띄우기우ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 가로배열ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 눈금크기ToolStripMenuItem;
        private System.Windows.Forms.ToolStripTextBox toolStripMenuItem2;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem 스펙트럼할당방법ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem fMODSystemToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem fMODChannelToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 맨위에표시ToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripMenuItem 측정속도ToolStripMenuItem;
        private System.Windows.Forms.ToolStripTextBox toolStripTextBox1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripMenuItem peak새로고침ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 그래프가로배열ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 분할자세로배열ToolStripMenuItem;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.ToolStripMenuItem 배경투명ToolStripMenuItem;
        private System.Windows.Forms.ToolStripTextBox toolStripTextBox2;
        private System.Windows.Forms.ToolStripMenuItem 테두리표시ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 분할자투명ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 측정크기ToolStripMenuItem;
        private System.Windows.Forms.ToolStripTextBox toolStripTextBox3;

    }
}