﻿using System;
using System.Windows.Forms;
using HS_Audio;

namespace HSAudioPluginTest
{
    public partial class Form2 : Form
    {
        HSAudioHelper Helper;
        internal Form2()
        {
            InitializeComponent();
        }
        public Form2(HSAudioHelper Helper)
        {
            InitializeComponent();
            this.Helper = Helper;
        }

        private void Form2_Load(object sender, EventArgs e)
        {
            timer1.Start();
        }

        
        private void timer1_Tick(object sender, EventArgs e)
        {
            
        }
    }
    public class FFTInt : HSAudioPluginTest.Control.WPF.ISpectrumPlayer
    {
        HSAudioHelper Helper;
        public FFTInt(HSAudioHelper Helper)
        {
            this.Helper = Helper;
            Helper.MusicChanged+=Helper_MusicChanged;
        }

        private void Helper_MusicChanged(HSAudioHelper Helper, int index, bool Error)
        {
            if (PropertyChanged != null) PropertyChanged(null, null);
        }

        #region ISpectrumPlayer 멤버

        float[] FFT_L = new float[2048];
        public bool GetFFTData(float[] fftDataBuffer)
        {
            if (Helper != null || Helper.system != null)
            {
                Helper.system.getSpectrum(FFT_L, FFT_L.Length, 0, HS_Audio_Lib.DSP_FFT_WINDOW.MAX);
                //Helper.system.getSpectrum(FFT_L, 512, 0, HS_Audio_Lib.DSP_FFT_WINDOW.MAX);
                fftDataBuffer = FFT_L;
                return true;
            }else return false;
        }

        public int GetFFTFrequencyIndex(int frequency)
        {
            return (int)((frequency / 44100) * (FFT_L.Length / 2));
        }

        #endregion

        #region ISoundPlayer 멤버

        public bool IsPlaying
        {
            get
            {
                if (Helper == null) return false; 
                else return Helper.PlayStatus == HSAudioHelper.PlayingStatus.Play;
            }
        }

        #endregion

        #region INotifyPropertyChanged 멤버

        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;

        #endregion
    }
}
