﻿using System;
using System.Drawing;
using System.Windows.Forms;
using HS_Audio;


namespace HSAudioPluginTest
{
    public partial class frmPeakMeter : Form
    {
        HSAudioHelper Data;
        int numchannels = 2;
        int dummy = 0;
        HS_Audio_Lib.SOUND_FORMAT dummyformat = HS_Audio_Lib.SOUND_FORMAT.NONE;
        HS_Audio_Lib.DSP_RESAMPLER _dummyresampler = HS_Audio_Lib.DSP_RESAMPLER.LINEAR;
        PropertyGrid pg, pg1;
        public frmPeakMeter(HSAudioHelper Helper)
        {
            InitializeComponent();
            SetStyle(ControlStyles.SupportsTransparentBackColor, true);
            Data = Helper;
            MaxLdB = float.NegativeInfinity; MaxRdB = float.NegativeInfinity;
            Ltmp = new float[3000];
            Rtmp = new float[3000];
            if (Data.system != null)
            {
                Data.system.getSoftwareFormat(ref dummy, ref dummyformat, ref numchannels, ref dummy, ref _dummyresampler, ref dummy);
            }

            peakMeterCtrl1.SetRange(15, 25, 30);//100, 115, 120);
            peakMeterCtrl2.SetRange(15, 25, 30);//100, 115, 120);
            //peakMeterCtrl1.LED눈금갯수 = 100;
            pg = new PropertyGrid()
            {
                SelectedObject = peakMeterCtrl1,
                Size = new Size(290, 350),
                Dock= DockStyle.Fill,
                Location = new Point(0, 0),
                Anchor = AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right | AnchorStyles.Top
            };
            pg1 = new PropertyGrid()
            {
                SelectedObject = peakMeterCtrl2,
                Size = new Size(290, 350),
                Dock= DockStyle.Fill,
                Location = new Point(0, 0),
                Anchor = AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right | AnchorStyles.Top
            };
            f.Controls.Add(pg); f1.Controls.Add(pg1);
            peakMeterCtrl1.PropertiesChanged += new Ernzo.WinForms.Controls.PropertiesChangedEventHandler(peakMeterCtrl1_PropertiesChanged);
        }

        #region
        public bool GetData = true;
        public int Length = 1;
        public int Interval = 25;
        #endregion
        #region
        public void Start(){ timer1.Start();}
        public void Stop() { timer1.Stop(); }
        #endregion

        public float MaxLdB { get; private set; }
        public float MaxRdB { get; private set; }
        string MaxLdBtxt = "-Inf.", MaxRdBtxt = "-Inf.";
        string Musicpath;
        int[] h = new int[2];
        public float[] Ltmp { get; internal set; } 
        public float[] Rtmp{get;internal set;}
        float LDB=float.NegativeInfinity, RDB=float.NegativeInfinity;
        double d1, d2;
        string Ltxt, Rtxt;
        private void timer1_Tick(object sender, EventArgs e)
        {
            if (Data.system != null)
            {
                if (Ltmp.Length == 0) { Ltmp = new float[31]; } if (Rtmp.Length == 0) { Rtmp = new float[30]; }
                if (numchannels == 1)
                {
                    if (GetData) { Data.system.getWaveData(Ltmp, Ltmp.Length, 0); }
                    else { Data.channel.getWaveData(Ltmp, Ltmp.Length, 0); }
                    for (int i = 0; i < Ltmp.Length; i++) { if (Ltmp[i] > LDB) { LDB = Ltmp[i]; } }
                    RDB = LDB;
                    //LDB = Ltmp[Ltmp.Length / 2];
                    //RDB = Rtmp[Rtmp.Length / 2];
                }
                if (numchannels == 2)
                {
                    if (GetData) { Data.system.getWaveData(Ltmp, Ltmp.Length, 0); Data.system.getWaveData(Rtmp, Rtmp.Length < 2 ? 1 : Rtmp.Length, 1); }
                    else { Data.channel.getWaveData(Ltmp, Ltmp.Length, 0); Data.channel.getWaveData(Rtmp, Rtmp.Length, 1); }
                    for (int i = 0; i < Ltmp.Length; i++) { if (Ltmp[i] > LDB) { LDB = Ltmp[i]; } }
                    for (int i = 0; i < Rtmp.Length; i++) { if (Rtmp[i] > RDB) { RDB = Rtmp[i]; } }
                    //LDB = Ltmp[Ltmp.Length / 2];
                    //RDB = Rtmp[Rtmp.Length / 2];
                }

                if (MaxLdB < LDB) { MaxLdB = LDB; MaxLdBtxt = ConvertdBToString(MaxLdB); }
                if (MaxRdB < RDB) { MaxRdB = RDB; MaxRdBtxt = ConvertdBToString(MaxRdB); }
                Ltxt = ConvertdBToString(LDB, "0.000");Rtxt = ConvertdBToString(RDB, "0.000");
                /*
                if (LDB.ToString().IndexOf("-3.899998E-19")!=-1 || 
                    LDB.ToString().IndexOf("7.39567E-17")!=-1||
                    LDB.ToString().IndexOf("7.395671E-17")!=-1||
                    LDB.ToString().IndexOf("5.277585") != -1 ||
                    LDB == 0) { Ltxt = "-Inf."; } else { Ltxt = Decibels(LDB, 0).ToString("0.000"); }

                if (RDB.ToString().IndexOf("-3.899998E-19") != -1 ||
                    RDB.ToString().IndexOf("7.39567E-17") != -1 ||
                    RDB.ToString().IndexOf("7.395671E-17") != -1 ||
                    RDB.ToString().IndexOf("5.277585") != -1 ||
                    RDB == 0) { Rtxt = "-Inf."; } else { Rtxt = Decibels(RDB, 0).ToString("0.000"); }
                 */
                //peakMeterCtrl1.SetRange(15, 25, 30);//100, 115, 120);
                //peakMeterCtrl2.SetRange(15, 25, 30);//100, 115, 120);
                if (Decibels(RMS(LDB), 0) != 0)
                {
                    d1 = Math.Min(Decibels(RMS(LDB), 0), 0);
                    h[0] = (int)Math.Max((d1 + (peakMeterCtrl1.LED눈금갯수 >= 20 ? peakMeterCtrl1.LED눈금갯수 : 20)), 0);
                    peakMeterCtrl1.SetData(h, 0, 1, false);

                    d2 = Math.Min(Decibels(RMS(RDB), 0), 0);
                    h[0] = (int)Math.Max((d2 + (peakMeterCtrl2.LED눈금갯수 >= 20 ? peakMeterCtrl2.LED눈금갯수 : 20)), 0);
                    peakMeterCtrl2.SetData(h, 0, 1, false);
                }
                else
                { h[0] = 0; peakMeterCtrl1.SetData(h, 0, 1, false); peakMeterCtrl2.SetData(h, 0, 1, false);}

                if (Data.MusicPath != Musicpath) { Musicpath = Data.MusicPath; MaxLdB = MaxRdB = float.NegativeInfinity; MaxLdBtxt = MaxRdBtxt = "-Inf."; }
                this.Text = string.Format("Peak 그래프 [좌: {0} dB / 우: {1} dB] ({2}) - Peak [좌: {3} dB / 우: {4} dB]",
                    Ltxt, Rtxt, LDB == RDB ? "Mono" : "Stereo", MaxLdBtxt, MaxRdBtxt); 
            }
            LDB = RDB = float.NegativeInfinity;
        }

        string _ConvertdBToString;
        public string ConvertdBToString(float dB)
        {
            if (dB == float.PositiveInfinity) { _ConvertdBToString = "Inf."; }
            else if ((Decibels(RMS(LDB), 0) < -360&&Decibels(RMS(RDB), 0) < -360) || dB == float.NegativeInfinity || dB == 0) { _ConvertdBToString = "-Inf."; }
            else { _ConvertdBToString = Decibels(RMS(dB), 0).ToString("0.000"); }
            return _ConvertdBToString;
        }
        public string ConvertdBToString(float dB, string Format)
        {
            if (dB == float.PositiveInfinity) { _ConvertdBToString = "Inf."; }
            else if ((RMS(LDB) < -360&&RMS(RDB) < -360) || dB == float.NegativeInfinity || dB == 0) { _ConvertdBToString = "-Inf."; }
            else { _ConvertdBToString = RMS(dB).ToString(Format); }
            return _ConvertdBToString;
        }

        public static double mag_sqrd(double re, double im) { return (re * re + im * im); }

        public static double Decibels(double re, double im) { return ((re == 0 && im == 0) ? (0) : 10.0 * Math.Log10(((mag_sqrd(re, im))))); }

        public static float RMS(float re){return ((0.045f * 0.045f) / re);}

        private void pictureBox2_Click(object sender, EventArgs e)
        {
             
        }

        private void peakMeterCtrl1_SizeChanged(object sender, EventArgs e)
        {
            //Ltmp = new float[peakMeterCtrl1.Width];
        }

        private void peakMeterCtrl2_SizeChanged(object sender, EventArgs e)
        {
            //Rtmp = new float[peakMeterCtrl2.Width];
        }

        Form f = new Form()
        {
            //MdiParent = this,
            Size = new Size(300, 350),
            Text = "RMS 설정 (좌)...",
            ShowIcon = false,
        };

        Form f1 = new Form()
        {
            //MdiParent = this,
            Size = new Size(300, 350),
            Text = "RMS 설정 (우)...",
            ShowIcon = false,
        };

        private void Form1_Load(object sender, EventArgs e)
        {
            f.FormClosing+=new FormClosingEventHandler(f_FormClosing);
            f1.FormClosing+=new FormClosingEventHandler(f1_FormClosing);
            SetStyle(ControlStyles.SupportsTransparentBackColor, true);
        }

        private void f1_FormClosing(object sender, FormClosingEventArgs e) { e.Cancel = !Closing; f1.Hide(); }
        private void f_FormClosing(object sender, FormClosingEventArgs e) { e.Cancel = !Closing; f.Hide(); }

        public bool Closing;
        private void frmPeakMeter_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = !Closing;
            this.Hide();
        }


        public void peakMeterCtrl1_PropertiesChanged(string Name, Type type, object Value)
        {
            if (type == typeof(int) && Name == "LED눈금갯수")
                if (peakMeterCtrl1.LED눈금갯수 >= 30)
                { peakMeterCtrl1.SetRange(peakMeterCtrl1.LED눈금갯수 - 15, peakMeterCtrl1.LED눈금갯수 - 5, peakMeterCtrl1.LED눈금갯수); }//100, 140, 200);}}
                else { peakMeterCtrl1.SetRange(15, 25, 30); }
        }

        private void 설정창띄우기ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            f.Show();
        }

        private void 설정창띄우기우ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            f1.Show();
        }

        private void 가로배열ToolStripMenuItem_CheckedChanged(object sender, EventArgs e)
        {

        }

        Orientation tmpOrientation;
        private void splitContainer1_Panel2_Resize(object sender, EventArgs e)
        {
            if (splitContainer1.Orientation != tmpOrientation)
            {
                if (splitContainer1.Orientation == Orientation.Vertical)
                {
                    splitContainer1.SplitterDistance = splitContainer1.Width / 2;
                }
                else
                {
                    splitContainer1.SplitterDistance = splitContainer1.Height / 2;
                }
                tmpOrientation = splitContainer1.Orientation;
            }
        }

        private void 세로배열ToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void peakMeterCtrl2_PropertiesChanged(string Name, Type type, object Value)
        {
            if(type == typeof(int)&&Name=="LED눈금갯수")
                if (peakMeterCtrl2.LED눈금갯수 >= 30)
                {peakMeterCtrl2.SetRange(peakMeterCtrl2.LED눈금갯수 - 15, peakMeterCtrl2.LED눈금갯수 - 5, peakMeterCtrl2.LED눈금갯수);}//100, 140, 200);}}
                else { peakMeterCtrl2.SetRange(15, 25, 30); }
        }

        private void toolStripMenuItem2_KeyDown(object sender, KeyEventArgs e)
        {

        }

        private void toolStripMenuItem2_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == Convert.ToChar(Keys.Enter)) { toolStripMenuItem2_TextChanged(null, null); }
            if (!(Char.IsDigit(e.KeyChar)) && e.KeyChar != Convert.ToChar(Keys.Back))
            { e.Handled = true; }
        }

        private void toolStripMenuItem2_TextChanged(object sender, EventArgs e)
        {
            int a;
            if (toolStripMenuItem2.Text == "") a = 30;
            else a = int.Parse(toolStripMenuItem2.Text);
            peakMeterCtrl1.LED눈금갯수 = peakMeterCtrl2.LED눈금갯수 = a > 0 ? a : 0;
            if (a <= 1) { a = 30; }
            //Ltmp = new float[a+1];Rtmp = new float[a];
        }

        private void fMODChannelToolStripMenuItem_CheckedChanged(object sender, EventArgs e)
        {

        }

        FormBorderStyle a;
        Rectangle b;
        Point Loc;
        private void peakMeterCtrl1_DoubleClick(object sender, EventArgs e)
        {
            if (this.FormBorderStyle != System.Windows.Forms.FormBorderStyle.None)
            {
                a = this.FormBorderStyle; b = new Rectangle(this.Top, this.Left, this.Width, this.Height);
                this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                Loc = this.Location;
                Left = Top = 0;
                Width = Screen.PrimaryScreen.WorkingArea.Width;
                Height = Screen.PrimaryScreen.WorkingArea.Height;
            }
            else
            {
                this.FormBorderStyle = a;
                this.Top = b.X; this.Left = b.Y; this.Width = b.Width; this.Height = b.Height;
                this.Location = new Point(Loc.X, Loc.Y);
            }
        }

        internal void peakMeterCtrl1_Resize(object sender, EventArgs e)
        {
            //try { Ltmp = new float[peakMeterCtrl1.Width]; }catch{}
        }

        internal void peakMeterCtrl2_Resize(object sender, EventArgs e)
        {
            //try { Rtmp = new float[peakMeterCtrl2.Width]; }catch{}
        }

        private void 맨위에표시ToolStripMenuItem_CheckedChanged(object sender, EventArgs e)
        {
            this.TopMost = 맨위에표시ToolStripMenuItem.Checked;
        }

        private void 스펙트럼할당방법ToolStripMenuItem_DropDownOpening(object sender, EventArgs e)
        {
            if (Data.system != null) { fMODSystemToolStripMenuItem.Enabled = true; }
            else { fMODSystemToolStripMenuItem.Enabled = false; }
            if (Data.channel != null) { fMODChannelToolStripMenuItem.Enabled = true; }
            else { fMODChannelToolStripMenuItem.Enabled = false; }
        }

        private void fMODSystemToolStripMenuItem_Click(object sender, EventArgs e)
        {
            fMODSystemToolStripMenuItem.Checked = true;
            fMODChannelToolStripMenuItem.Checked = false;
            GetData = true;
        }

        private void fMODChannelToolStripMenuItem_Click(object sender, EventArgs e)
        {
            fMODSystemToolStripMenuItem.Checked = false;
            fMODChannelToolStripMenuItem.Checked = true;
            GetData = false;
            /*
            if (fMODSystemToolStripMenuItem.Checked)
            {

            }
            else
            {

            }*/
        }

        private void toolStripTextBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == Convert.ToChar(Keys.Enter)) { toolStripTextBox1_TextChanged(null, null); }
            if (!(Char.IsDigit(e.KeyChar)) && e.KeyChar != Convert.ToChar(Keys.Back))
            { e.Handled = true; }
        }
        int a1;
        private void toolStripTextBox1_TextChanged(object sender, EventArgs e)
        {
            a1 = int.Parse(toolStripTextBox1.Text);
            this.timer1.Interval=a1==0?1:a1;
        }

        private void peak새로고침ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MaxLdB = MaxRdB = float.NegativeInfinity;
        }

        private void 그래프가로배열ToolStripMenuItem_CheckedChanged(object sender, EventArgs e)
        {
            if (그래프가로배열ToolStripMenuItem.Checked)
                peakMeterCtrl1.MeterStyle = peakMeterCtrl2.MeterStyle = Ernzo.WinForms.Controls.PeakMeterStyle.PMS_Vertical;
            else
                peakMeterCtrl1.MeterStyle = peakMeterCtrl2.MeterStyle = Ernzo.WinForms.Controls.PeakMeterStyle.PMS_Horizontal;
        }

        private void 분할자가로배열ToolStripMenuItem_CheckedChanged(object sender, EventArgs e)
        {
            if (분할자세로배열ToolStripMenuItem.Checked) splitContainer1.Orientation = Orientation.Horizontal;
            else splitContainer1.Orientation = Orientation.Vertical;
        }

        private void peakMeterCtrl1_MouseDown(object sender, MouseEventArgs e)
        {
            설정창띄우기좌ToolStripMenuItem.Visible = true;
            설정창띄우기우ToolStripMenuItem.Visible = false;
        }

        private void peakMeterCtrl2_MouseDown(object sender, MouseEventArgs e)
        {
            설정창띄우기좌ToolStripMenuItem.Visible = false;
            설정창띄우기우ToolStripMenuItem.Visible = true;
        }

        private void toolTip1_Draw(object sender, DrawToolTipEventArgs e)
        {
            e.DrawBackground();
            e.DrawBorder();
            e.DrawText();
        }

        private void 배경투명ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //Color c= Color.FromName("Gainsboro");
            if (!배경투명ToolStripMenuItem.Checked)
            {
                //splitContainer1.BackColor = SystemColors.Control;
                c[0] = peakMeterCtrl1.BackColor;
                c[1] = peakMeterCtrl2.BackColor;
                c[2] = peakMeterCtrl1.모눈선색상;
                c[3] = peakMeterCtrl2.모눈선색상;
                c[4] = splitContainer1.BackColor;
                this.BackColor = Color.Transparent;
                //this.TransparencyKey = peakMeterCtrl1.모눈선색상;
                if (분할자투명ToolStripMenuItem.Checked) splitContainer1.BackColor = Color.Transparent;
                toolStripTextBox2.Enabled = true;
                this.peakMeterCtrl1.모눈선색상 = this.peakMeterCtrl2.모눈선색상 =
                this.peakMeterCtrl1.BackColor = this.peakMeterCtrl2.BackColor = Color.Transparent;
                toolStripTextBox2_TextChanged(null, null);
                toolStripTextBox2.Enabled = 분할자투명ToolStripMenuItem.Enabled = true;
                배경투명ToolStripMenuItem.Checked = true;
            }
            else
            {
                this.TransparencyKey = Color.Empty;
                toolStripTextBox2.Enabled = false;
                peakMeterCtrl1.BackColor = c[0];
                peakMeterCtrl2.BackColor = c[1];
                peakMeterCtrl1.모눈선색상 = c[2];
                peakMeterCtrl2.모눈선색상 = c[3];
                splitContainer1.BackColor = c[4];
                toolStripTextBox2.Enabled = 배경투명ToolStripMenuItem.Checked = false;
            }
            //분할자투명ToolStripMenuItem_CheckedChanged(null, null);
        }

        Color[] c = new Color[5];
        private void 배경투명ToolStripMenuItem_CheckStateChanged(object sender, EventArgs e)
        {
            
        }

        private void toolStripTextBox2_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == Convert.ToChar(Keys.Enter)) { toolStripTextBox2_TextChanged(null, null); }
            if (!(Char.IsDigit(e.KeyChar)) && e.KeyChar != Convert.ToChar(Keys.Back)&&
                e.KeyChar != 'A' && e.KeyChar != 'B' && e.KeyChar != 'C' && e.KeyChar != 'D' && e.KeyChar != 'E' && e.KeyChar!= 'F')
            { e.Handled = true; }
        }

        private void toolStripTextBox2_TextChanged(object sender, EventArgs e)
        {
            if (!toolStripTextBox2.Text.Contains("#")) {toolStripTextBox2.Text="#"+toolStripTextBox2.Text; }
            int a = Convert.ToInt32(toolStripTextBox2.Text.Replace("#", ""), 16);
            this.TransparencyKey=Color.FromArgb(a);
            this.BackColor =splitContainer1.Panel1.BackColor = splitContainer1.Panel2.BackColor = this.TransparencyKey;
            //splitContainer1.BackColor = SystemColors.ControlDark;
        }

        private void 테두리표시ToolStripMenuItem_CheckedChanged(object sender, EventArgs e)
        {
            if (테두리표시ToolStripMenuItem.Checked)
            {splitContainer1.BorderStyle = BorderStyle.FixedSingle; }
            else { splitContainer1.BorderStyle = BorderStyle.None; }
        }

        private void 분할자투명ToolStripMenuItem_CheckedChanged(object sender, EventArgs e)
        {
            if (분할자투명ToolStripMenuItem.Checked)
            {c[4] = splitContainer1.BackColor; splitContainer1.BackColor = Color.Transparent;}
            else{splitContainer1.BackColor = c[4];}
        }

        private void peakMeterCtrl1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyValue == 115) { 배경투명ToolStripMenuItem_Click(null, null); }
        }

        private void toolStripTextBox3_TextChanged(object sender, EventArgs e)
        {
            uint a =uint.Parse(toolStripTextBox3.Text);
            Ltmp = new float[a];
            Rtmp = new float[a];
        }
    }
}
