﻿using HS_Audio.Languge;
using HS_Audio;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Text;
using System.Windows.Forms;
using HS_Audio.Plugin;

namespace HSAudioPluginTest
{
    public partial class Form1 : Form
    {
        public HSAudioPlugin hpi;
        public Form1()
        {
            InitializeComponent();
        }
        public Form1(HSAudioPlugin hpi)
        {
            InitializeComponent();
            this.hpi = hpi;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            timer1.Start();
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = !hpi.Closing;
            if (!hpi.Closing) this.Hide();
        }
        internal void LocaleChange(Locale local) { }

        private void button1_Click(object sender, EventArgs e)
        {
            MessageBox.Show("플러그인 로드 성공!");
        }

        //string Beforestr;
        private void timer1_Tick(object sender, EventArgs e)
        {
            if (hpi.Helper != null) {
                //label2.Text = "파일 경로: "+hpi.Helper.MusicPath;
                try { if (hpi.Helper.MusicPath != textBox1.Text) textBox1.Text = hpi.Helper.MusicPath == null || hpi.Helper.MusicPath == "" ? "(없음)" : hpi.Helper.MusicPath; } catch(Exception ex) { textBox1.Text = ex.Message; }
                label1.Text = string.Format("재생 시간: {0} / {1}", ShowTime(TimeSpan.FromMilliseconds(hpi.Helper.CurrentPosition)), ShowTime(TimeSpan.FromMilliseconds(hpi.Helper.TotalPosition)));
            }
            else{label1.Text = "재생 시간: (없음)";textBox1.Text = "(없음)";}
        }
        public static string ShowTime(TimeSpan Time)
        {
            StringBuilder sb = new StringBuilder();
            /*sb.Append(Time.Hours);sb.Append(":");
            sb.Append(Time.Minutes);sb.Append(":");
            sb.Append(Time.Seconds);sb.Append(".");
            sb.Append(Time.Milliseconds);*/
            sb.Append(Time.Hours.ToString("00")); sb.Append(":");
            sb.Append(Time.Minutes.ToString("00")); sb.Append(":");
            sb.Append(Time.Seconds.ToString("00")); sb.Append(".");
            sb.Append(Time.Milliseconds.ToString("000"));
            return sb.ToString();
        }

        private void numericUpDown1_ValueChanged(object sender, EventArgs e)
        {
            timer1.Interval = (int)numericUpDown1.Value;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            hpi.Host.Command(CommandStatus.Next);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            hpi.Helper.MusicPath = textBox2.Text;
        }

        private void button4_Click(object sender, EventArgs e)
        {

        }

        private void button5_Click(object sender, EventArgs e)
        {
            frmPeakMeter frm = new frmPeakMeter(hpi.Helper);
            frm.Show();
            frm.Start();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            frmSpectogram frm = new frmSpectogram(hpi.Helper);
            frm.Show();
        }

        private void button7_Click(object sender, EventArgs e)
        {
            Form2 frm = new Form2(hpi.Helper);
            frm.Show();
        }
    }
    public class HSAudioPlugin : IHSAudioPlugin
    {
        internal string strName = "간단한 재생 정보";
        internal Image icon;

        private Form1 frm = new Form1();
        internal bool _Disposing;
        internal HS_Audio.HSAudioHelper _Helper;
        //private MemoryStream ms = new MemoryStream();
        private IHSAudioPluginHost m_Host;
        public HS_Audio.Languge.LocaleManager.LocaleChangesEventhandler LocaleChanged { get; set; }
        public HS_Audio.HSAudioHelper Helper { get { return this._Helper; } set { _Helper = value; } }
        public Dictionary<string, string> Setting { get; set; }
        public HSAudioPlugin()
        {
            //strName = "플러그인 테스트";
            NewCreate();
        }
        public string Name
        { get { return strName; } set { strName = value; } }
        public void NewCreate()
        {icon =frm.Icon.ToBitmap();  try { frm.Dispose(); } catch { } frm = new Form1(this);}// if(Helper!=null)MessageBox.Show(Helper.MusicPath);}
        public void Hide() { frm.Hide(); }
        public void Show() { frm.Show(); }
        public bool Closing { get; set; }
        public bool IsDisposed { get { return _Disposing; } }
        public bool IsEnabled { get { return Helper!=null; } }
        public void Dispose() { frm.Close(); frm.Dispose(); _Disposing = true; }
        public Image Icon { get {return icon;/*Icon.Save(ms, System.Drawing.Imaging.ImageFormat.Png);return Image.FromStream(ms);*/ } }
        public IHSAudioPluginHost Host
        {
            get { return m_Host; }
            set
            {
                m_Host = value;
                m_Host.Register(this);
            }
        }
    }
    /*
    public class HSAudioPlugin : IHSAudioPlugin
    {
        private Form1 frm = new Form1();
        internal bool _Disposing;
        internal HS_Audio_Helper.HSAudioHelper _Helper;
        private MemoryStream ms = new MemoryStream(); 
        private string strName;
        private IHSAudioPluginHost m_Host;
        public HS_Audio.Languge.LocaleManager.LocaleChangesEventhandler LocaleChanged { get; set; }
        public HS_Audio_Helper.HSAudioHelper Helper { get { return this._Helper; } set { _Helper = value; } }
        public Dictionary<string, string> Setting { get; set; }
        public HSAudioPlugin()
        {
            strName = "플러그인 테스트";
            NewCreate();
        }
        public string Name
        { get { return strName; } set { strName = value; } }
        public void NewCreate()
        { try { frm.Dispose(); }catch{} frm = new Form1(this); }
        public void Hide() { frm.Hide(); }
        public void Show() {frm.Show();}
        public bool Closing { get; set; }
        public bool IsDisposing { get { return _Disposing; } }
        public void Dispose() { frm.Close(); frm.Dispose(); _Disposing = true; }
        public Image Icon { get { Icon.Save(ms, System.Drawing.Imaging.ImageFormat.Png); return Image.FromStream(ms); } }
        public IHSAudioPluginHost Host
		{
			get{return m_Host;}
			set
			{
				m_Host=value;
				m_Host.Register(this);
			}
		}
    }*/
}
