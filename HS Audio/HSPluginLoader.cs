﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Text;
using HS_Audio;
using HS_Audio.Plugin;
using HS_CSharpUtility.Extension;

namespace HS_Audio
{
    public static class HSPluginLoader1
	{
		public static ICollection<IHSAudioPlugin> LoadPlugins(string path)
		{
            //System.Windows.Forms.MessageBox.Show("LoadPlugins(strnig Path)");
			string[] dllFileNames = null;
            
			if(Directory.Exists(path))
			{
				dllFileNames = Directory.GetFiles(path, "*.dll");

				ICollection<Assembly> assemblies = new List<Assembly>(dllFileNames.Length);
				foreach(string dllFile in dllFileNames)
				{
                    try
                    {
                        AssemblyName an = AssemblyName.GetAssemblyName(dllFile);
                        Assembly assembly = Assembly.Load(an);
                        assemblies.Add(assembly);
                    }
                    catch(Exception ex) { ex.Logging(); }
				}

                Type pluginType = typeof(IHSAudioPlugin);
				ICollection<Type> pluginTypes = new List<Type>();
				foreach(Assembly assembly in assemblies)
				{
                    try
                    {
                        if (assembly != null)
                        {
                            Type[] types = assembly.GetTypes();

                            foreach (Type type in types)
                            {
                                try
                                {
                                    if (type.IsInterface || type.IsAbstract)
                                    {
                                        continue;
                                    }
                                    else
                                    {
                                        if (type.GetInterface(pluginType.FullName) != null)
                                        {
                                            pluginTypes.Add(type);
                                        }
                                    }
                                }
                                catch (Exception ex) { ex.Logging(); }
                            }
                        }
                    }
                    catch (Exception ex) { ex.Logging(); }
                }

                ICollection<IHSAudioPlugin> plugins = new List<IHSAudioPlugin>(pluginTypes.Count);
				foreach(Type type in pluginTypes)
				{
                    try
                    {
                        IHSAudioPlugin plugin = (IHSAudioPlugin)Activator.CreateInstance(type);
                        plugins.Add(plugin);
                    }
                    catch (Exception ex) { ex.Logging(); }
                }

				return plugins;
			}

			return null;
		}
	}
    public static class HSPluginLoader<T>
    {
        public static ICollection<T> LoadPlugins(string path)
        {
            //System.Windows.Forms.MessageBox.Show("LoadPlugins(T Path)");
            string[] dllFileNames = null;
            if(!Directory.Exists(path)){ path = System.Diagnostics.Process.GetCurrentProcess().MainModule.FileName+"\\"+path; }
            if (Directory.Exists(path))
            {
                dllFileNames = Directory.GetFiles(path, "*.dll");

                ICollection<Assembly> assemblies = new List<Assembly>(dllFileNames.Length);
                foreach (string dllFile in dllFileNames)
                {
                    try
                    {
                        AssemblyName an = AssemblyName.GetAssemblyName(dllFile);
                        Assembly assembly = Assembly.Load(an);
                        assemblies.Add(assembly);
                    }
                    catch (Exception ex) { ex.Logging(); }
                }

                Type pluginType = typeof(T);
                ICollection<Type> pluginTypes = new List<Type>();
                foreach (Assembly assembly in assemblies)
                {//System.Runtime.InteropServices.Marshal.GetHINSTANCE(assembly.
                    if (assembly != null)
                    {
                        try
                        {
                            Type[] types = assembly.GetTypes();

                            foreach (Type type in types)
                            {
                                try
                                {
                                    if (type.IsInterface || type.IsAbstract)
                                    {
                                        continue;
                                    }
                                    else
                                    {
                                        if (type.GetInterface(pluginType.FullName) != null)
                                        {
                                            pluginTypes.Add(type);
                                        }
                                    }
                                }
                                catch (Exception ex) { ex.Logging(); }
                            }
                        }
                        catch (Exception ex) { ex.Logging(); }
                    }
                }

                ICollection<T> plugins = new List<T>(pluginTypes.Count);
                //for(int i=0;i<pluginTypes.Count;)
                foreach (Type type in pluginTypes)
                {
                    try
                    {
                        T plugin = (T)Activator.CreateInstance(type);
                        plugins.Add(plugin);
                    }
                    catch (Exception ex) { ex.Logging(); }
                }

                return plugins;
            }

            return null;
        }
    }
}
