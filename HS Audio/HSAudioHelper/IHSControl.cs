﻿using System.Collections.Generic;

namespace HS_Audio
{
    public interface IHSSetting
    {
        Dictionary<string, string> SaveSetting();
        void LoadSetting(Dictionary<string, string> Setting);
    }
}
