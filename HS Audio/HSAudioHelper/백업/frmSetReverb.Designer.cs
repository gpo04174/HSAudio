﻿namespace FMOD_Helper.Forms
{
    partial class frmReverb
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmReverb));
            this.numInstance = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.numEnvironment = new System.Windows.Forms.NumericUpDown();
            this.button3 = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.numEnvDiffusion = new System.Windows.Forms.NumericUpDown();
            this.button4 = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.numRoom = new System.Windows.Forms.NumericUpDown();
            this.button5 = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.numRoomHF = new System.Windows.Forms.NumericUpDown();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.파일FToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.새프리셋ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.열기ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.다른이름으로저장ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.닫기XToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.설정ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.보두기본값으로설정ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.고급설정모드ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.프리셋경로재설정ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.프리셋열기경로재설정ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.프리셋저장경로재설정ToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.프리셋폴더경로재설정ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.도움말FToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.hLLF설명ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.정렬ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripComboBox1 = new System.Windows.Forms.ToolStripComboBox();
            this.폴더선택ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.새로고침ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.제거ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.button6 = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.numRoomLF = new System.Windows.Forms.NumericUpDown();
            this.button7 = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.numHFReference = new System.Windows.Forms.NumericUpDown();
            this.button8 = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.numModulationDepth = new System.Windows.Forms.NumericUpDown();
            this.button9 = new System.Windows.Forms.Button();
            this.label9 = new System.Windows.Forms.Label();
            this.numModulationTime = new System.Windows.Forms.NumericUpDown();
            this.button10 = new System.Windows.Forms.Button();
            this.label10 = new System.Windows.Forms.Label();
            this.numReverbDelay = new System.Windows.Forms.NumericUpDown();
            this.button11 = new System.Windows.Forms.Button();
            this.label11 = new System.Windows.Forms.Label();
            this.numReverb = new System.Windows.Forms.NumericUpDown();
            this.button12 = new System.Windows.Forms.Button();
            this.label12 = new System.Windows.Forms.Label();
            this.numReflectionsDelay = new System.Windows.Forms.NumericUpDown();
            this.button13 = new System.Windows.Forms.Button();
            this.label13 = new System.Windows.Forms.Label();
            this.numReflections = new System.Windows.Forms.NumericUpDown();
            this.button14 = new System.Windows.Forms.Button();
            this.label14 = new System.Windows.Forms.Label();
            this.numDecayLFRatio = new System.Windows.Forms.NumericUpDown();
            this.button15 = new System.Windows.Forms.Button();
            this.label15 = new System.Windows.Forms.Label();
            this.numDecayHFRatio = new System.Windows.Forms.NumericUpDown();
            this.button16 = new System.Windows.Forms.Button();
            this.numDecayTime = new System.Windows.Forms.NumericUpDown();
            this.label16 = new System.Windows.Forms.Label();
            this.button17 = new System.Windows.Forms.Button();
            this.label17 = new System.Windows.Forms.Label();
            this.numDiffusion = new System.Windows.Forms.NumericUpDown();
            this.button18 = new System.Windows.Forms.Button();
            this.label18 = new System.Windows.Forms.Label();
            this.numLFReference = new System.Windows.Forms.NumericUpDown();
            this.button19 = new System.Windows.Forms.Button();
            this.label19 = new System.Windows.Forms.Label();
            this.button20 = new System.Windows.Forms.Button();
            this.label20 = new System.Windows.Forms.Label();
            this.numDensity = new System.Windows.Forms.NumericUpDown();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.numFlag = new System.Windows.Forms.DomainUpDown();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.trbVolume = new System.Windows.Forms.TrackBar();
            this.label21 = new System.Windows.Forms.Label();
            this.numVolume = new System.Windows.Forms.NumericUpDown();
            this.button21 = new System.Windows.Forms.Button();
            this.domainUpDown1 = new System.Windows.Forms.DomainUpDown();
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            ((System.ComponentModel.ISupportInitialize)(this.numInstance)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numEnvironment)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numEnvDiffusion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numRoom)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numRoomHF)).BeginInit();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numRoomLF)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numHFReference)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numModulationDepth)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numModulationTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numReverbDelay)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numReverb)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numReflectionsDelay)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numReflections)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numDecayLFRatio)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numDecayHFRatio)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numDecayTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numDiffusion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numLFReference)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numDensity)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trbVolume)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numVolume)).BeginInit();
            this.SuspendLayout();
            // 
            // numInstance
            // 
            this.numInstance.Enabled = false;
            this.numInstance.Location = new System.Drawing.Point(137, 27);
            this.numInstance.Maximum = new decimal(new int[] {
            3,
            0,
            0,
            0});
            this.numInstance.Name = "numInstance";
            this.numInstance.Size = new System.Drawing.Size(73, 21);
            this.numInstance.TabIndex = 0;
            this.toolTip1.SetToolTip(this.numInstance, "최소값: 0, 최댓값: 3, 기본값: 0\nEAX4 only. Environment Instance. 3 seperate reverbs simult" +
                    "aneously are possible. This specifies which one to set. (win32 only)");
            this.numInstance.ValueChanged += new System.EventHandler(this.num_ValueChanged);
            this.numInstance.KeyDown += new System.Windows.Forms.KeyEventHandler(this.numEnvironment_KeyDown);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Enabled = false;
            this.label1.Location = new System.Drawing.Point(2, 31);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 12);
            this.label1.TabIndex = 1;
            this.label1.Text = "Instance";
            this.toolTip1.SetToolTip(this.label1, "최소값: 0, 최댓값: 3, 기본값: 0\nEAX4 only. Environment Instance. 3 seperate reverbs simult" +
                    "aneously are possible. This specifies which one to set. (win32 only)");
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(216, 28);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(50, 19);
            this.button1.TabIndex = 2;
            this.button1.Text = "기본값";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            this.button1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.numEnvironment_KeyDown);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(216, 51);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(50, 19);
            this.button2.TabIndex = 5;
            this.button2.Text = "기본값";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            this.button2.KeyDown += new System.Windows.Forms.KeyEventHandler(this.numEnvironment_KeyDown);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label2.Location = new System.Drawing.Point(2, 53);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(86, 12);
            this.label2.TabIndex = 4;
            this.label2.Text = "Environment";
            this.toolTip1.SetToolTip(this.label2, "최소값: -1, 최댓값: 25, 기본값: -1\nsets all listener properties (win32/ps2)");
            // 
            // numEnvironment
            // 
            this.numEnvironment.Location = new System.Drawing.Point(137, 50);
            this.numEnvironment.Maximum = new decimal(new int[] {
            25,
            0,
            0,
            0});
            this.numEnvironment.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            -2147483648});
            this.numEnvironment.Name = "numEnvironment";
            this.numEnvironment.Size = new System.Drawing.Size(73, 21);
            this.numEnvironment.TabIndex = 3;
            this.toolTip1.SetToolTip(this.numEnvironment, "최소값: -1, 최댓값: 25, 기본값: -1\nsets all listener properties (win32/ps2)");
            this.numEnvironment.Value = new decimal(new int[] {
            1,
            0,
            0,
            -2147483648});
            this.numEnvironment.ValueChanged += new System.EventHandler(this.num_ValueChanged);
            this.numEnvironment.KeyDown += new System.Windows.Forms.KeyEventHandler(this.numEnvironment_KeyDown);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(216, 74);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(50, 19);
            this.button3.TabIndex = 8;
            this.button3.Text = "기본값";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            this.button3.KeyDown += new System.Windows.Forms.KeyEventHandler(this.numEnvironment_KeyDown);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(2, 75);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(127, 12);
            this.label3.TabIndex = 7;
            this.label3.Text = "Environment Diffusion";
            this.toolTip1.SetToolTip(this.label3, "environment diffusion (win32/xbox)");
            // 
            // numEnvDiffusion
            // 
            this.numEnvDiffusion.DecimalPlaces = 3;
            this.numEnvDiffusion.Increment = new decimal(new int[] {
            1,
            0,
            0,
            196608});
            this.numEnvDiffusion.Location = new System.Drawing.Point(137, 73);
            this.numEnvDiffusion.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            65536});
            this.numEnvDiffusion.Name = "numEnvDiffusion";
            this.numEnvDiffusion.Size = new System.Drawing.Size(73, 21);
            this.numEnvDiffusion.TabIndex = 6;
            this.toolTip1.SetToolTip(this.numEnvDiffusion, "최소값: 0.0, 최댓값: 1.0, 기본값: 1.0\nenvironment diffusion (win32/xbox)");
            this.numEnvDiffusion.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numEnvDiffusion.ValueChanged += new System.EventHandler(this.num_ValueChanged);
            this.numEnvDiffusion.KeyDown += new System.Windows.Forms.KeyEventHandler(this.numEnvironment_KeyDown);
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(216, 96);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(50, 19);
            this.button4.TabIndex = 11;
            this.button4.Text = "기본값";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            this.button4.KeyDown += new System.Windows.Forms.KeyEventHandler(this.numEnvironment_KeyDown);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label4.Location = new System.Drawing.Point(2, 98);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(42, 12);
            this.label4.TabIndex = 10;
            this.label4.Text = "Room";
            this.toolTip1.SetToolTip(this.label4, "최소값: -10000, 최댓값: 0, 기본값: -1000\nRoom Effect Level (at mid frequencies) (win32/xbo" +
                    "x)");
            // 
            // numRoom
            // 
            this.numRoom.Increment = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.numRoom.Location = new System.Drawing.Point(137, 96);
            this.numRoom.Maximum = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.numRoom.Minimum = new decimal(new int[] {
            10000,
            0,
            0,
            -2147483648});
            this.numRoom.Name = "numRoom";
            this.numRoom.Size = new System.Drawing.Size(73, 21);
            this.numRoom.TabIndex = 9;
            this.toolTip1.SetToolTip(this.numRoom, "최소값: -10000, 최댓값: 0, 기본값: -1000\nRoom Effect Level (at mid frequencies) (win32/xbo" +
                    "x)");
            this.numRoom.Value = new decimal(new int[] {
            1000,
            0,
            0,
            -2147483648});
            this.numRoom.ValueChanged += new System.EventHandler(this.num_ValueChanged);
            this.numRoom.KeyDown += new System.Windows.Forms.KeyEventHandler(this.numEnvironment_KeyDown);
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(216, 119);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(50, 19);
            this.button5.TabIndex = 14;
            this.button5.Text = "기본값";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            this.button5.KeyDown += new System.Windows.Forms.KeyEventHandler(this.numEnvironment_KeyDown);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(2, 122);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(57, 12);
            this.label5.TabIndex = 13;
            this.label5.Text = "Room HF";
            this.toolTip1.SetToolTip(this.label5, "Relative room effect level at high frequencies (win32/xbox)");
            // 
            // numRoomHF
            // 
            this.numRoomHF.Increment = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.numRoomHF.Location = new System.Drawing.Point(137, 119);
            this.numRoomHF.Maximum = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.numRoomHF.Minimum = new decimal(new int[] {
            10000,
            0,
            0,
            -2147483648});
            this.numRoomHF.Name = "numRoomHF";
            this.numRoomHF.Size = new System.Drawing.Size(73, 21);
            this.numRoomHF.TabIndex = 12;
            this.toolTip1.SetToolTip(this.numRoomHF, "최소값: -10000, 최댓값: 0, 기본값: -100\nrelative room effect level at high frequencies (wi" +
                    "n32/xbox)");
            this.numRoomHF.Value = new decimal(new int[] {
            100,
            0,
            0,
            -2147483648});
            this.numRoomHF.ValueChanged += new System.EventHandler(this.num_ValueChanged);
            this.numRoomHF.KeyDown += new System.Windows.Forms.KeyEventHandler(this.numEnvironment_KeyDown);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.파일FToolStripMenuItem,
            this.설정ToolStripMenuItem,
            this.도움말FToolStripMenuItem,
            this.정렬ToolStripMenuItem,
            this.toolStripComboBox1,
            this.폴더선택ToolStripMenuItem,
            this.새로고침ToolStripMenuItem,
            this.제거ToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional;
            this.menuStrip1.Size = new System.Drawing.Size(504, 24);
            this.menuStrip1.TabIndex = 15;
            this.menuStrip1.Text = "menuStrip1";
            this.menuStrip1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.numEnvironment_KeyDown);
            // 
            // 파일FToolStripMenuItem
            // 
            this.파일FToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.새프리셋ToolStripMenuItem,
            this.toolStripSeparator3,
            this.열기ToolStripMenuItem,
            this.다른이름으로저장ToolStripMenuItem,
            this.toolStripSeparator2,
            this.닫기XToolStripMenuItem});
            this.파일FToolStripMenuItem.Name = "파일FToolStripMenuItem";
            this.파일FToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.O)));
            this.파일FToolStripMenuItem.Size = new System.Drawing.Size(58, 20);
            this.파일FToolStripMenuItem.Text = "파일(&F)";
            // 
            // 새프리셋ToolStripMenuItem
            // 
            this.새프리셋ToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("새프리셋ToolStripMenuItem.Image")));
            this.새프리셋ToolStripMenuItem.Name = "새프리셋ToolStripMenuItem";
            this.새프리셋ToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.N)));
            this.새프리셋ToolStripMenuItem.Size = new System.Drawing.Size(198, 22);
            this.새프리셋ToolStripMenuItem.Text = "새 프리셋";
            this.새프리셋ToolStripMenuItem.Click += new System.EventHandler(this.새프리셋ToolStripMenuItem_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(195, 6);
            // 
            // 열기ToolStripMenuItem
            // 
            this.열기ToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("열기ToolStripMenuItem.Image")));
            this.열기ToolStripMenuItem.Name = "열기ToolStripMenuItem";
            this.열기ToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.O)));
            this.열기ToolStripMenuItem.Size = new System.Drawing.Size(198, 22);
            this.열기ToolStripMenuItem.Text = "프리셋 열기(&O)";
            this.열기ToolStripMenuItem.Click += new System.EventHandler(this.열기ToolStripMenuItem_Click);
            // 
            // 다른이름으로저장ToolStripMenuItem
            // 
            this.다른이름으로저장ToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("다른이름으로저장ToolStripMenuItem.Image")));
            this.다른이름으로저장ToolStripMenuItem.Name = "다른이름으로저장ToolStripMenuItem";
            this.다른이름으로저장ToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
            this.다른이름으로저장ToolStripMenuItem.Size = new System.Drawing.Size(198, 22);
            this.다른이름으로저장ToolStripMenuItem.Text = "프리셋 저장(&S)";
            this.다른이름으로저장ToolStripMenuItem.Click += new System.EventHandler(this.다른이름으로저장ToolStripMenuItem_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(195, 6);
            // 
            // 닫기XToolStripMenuItem
            // 
            this.닫기XToolStripMenuItem.Name = "닫기XToolStripMenuItem";
            this.닫기XToolStripMenuItem.Size = new System.Drawing.Size(198, 22);
            this.닫기XToolStripMenuItem.Text = "닫기(&X)";
            this.닫기XToolStripMenuItem.Click += new System.EventHandler(this.닫기XToolStripMenuItem_Click);
            // 
            // 설정ToolStripMenuItem
            // 
            this.설정ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.보두기본값으로설정ToolStripMenuItem,
            this.toolStripSeparator1,
            this.고급설정모드ToolStripMenuItem,
            this.toolStripSeparator4,
            this.프리셋경로재설정ToolStripMenuItem});
            this.설정ToolStripMenuItem.Name = "설정ToolStripMenuItem";
            this.설정ToolStripMenuItem.Size = new System.Drawing.Size(59, 20);
            this.설정ToolStripMenuItem.Text = "설정(&T)";
            // 
            // 보두기본값으로설정ToolStripMenuItem
            // 
            this.보두기본값으로설정ToolStripMenuItem.Name = "보두기본값으로설정ToolStripMenuItem";
            this.보두기본값으로설정ToolStripMenuItem.Size = new System.Drawing.Size(192, 22);
            this.보두기본값으로설정ToolStripMenuItem.Text = "모두 기본값으로 설정";
            this.보두기본값으로설정ToolStripMenuItem.Click += new System.EventHandler(this.보두기본값으로설정ToolStripMenuItem_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(189, 6);
            // 
            // 고급설정모드ToolStripMenuItem
            // 
            this.고급설정모드ToolStripMenuItem.CheckOnClick = true;
            this.고급설정모드ToolStripMenuItem.Name = "고급설정모드ToolStripMenuItem";
            this.고급설정모드ToolStripMenuItem.Size = new System.Drawing.Size(192, 22);
            this.고급설정모드ToolStripMenuItem.Text = "고급 변경 모드";
            this.고급설정모드ToolStripMenuItem.ToolTipText = "단위를 5에서 1로 설정합니다";
            this.고급설정모드ToolStripMenuItem.CheckedChanged += new System.EventHandler(this.고급설정모드ToolStripMenuItem_CheckedChanged);
            this.고급설정모드ToolStripMenuItem.Click += new System.EventHandler(this.고급설정모드ToolStripMenuItem_Click);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(189, 6);
            // 
            // 프리셋경로재설정ToolStripMenuItem
            // 
            this.프리셋경로재설정ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.프리셋열기경로재설정ToolStripMenuItem,
            this.프리셋저장경로재설정ToolStripMenuItem1,
            this.toolStripSeparator5,
            this.프리셋폴더경로재설정ToolStripMenuItem});
            this.프리셋경로재설정ToolStripMenuItem.Name = "프리셋경로재설정ToolStripMenuItem";
            this.프리셋경로재설정ToolStripMenuItem.Size = new System.Drawing.Size(192, 22);
            this.프리셋경로재설정ToolStripMenuItem.Text = "프리셋 경로 재설정";
            this.프리셋경로재설정ToolStripMenuItem.Click += new System.EventHandler(this.프리셋경로재설정ToolStripMenuItem_Click);
            // 
            // 프리셋열기경로재설정ToolStripMenuItem
            // 
            this.프리셋열기경로재설정ToolStripMenuItem.Name = "프리셋열기경로재설정ToolStripMenuItem";
            this.프리셋열기경로재설정ToolStripMenuItem.Size = new System.Drawing.Size(208, 22);
            this.프리셋열기경로재설정ToolStripMenuItem.Text = "프리셋 열기 경로 재설정";
            this.프리셋열기경로재설정ToolStripMenuItem.Click += new System.EventHandler(this.프리셋열기경로재설정ToolStripMenuItem_Click);
            // 
            // 프리셋저장경로재설정ToolStripMenuItem1
            // 
            this.프리셋저장경로재설정ToolStripMenuItem1.Name = "프리셋저장경로재설정ToolStripMenuItem1";
            this.프리셋저장경로재설정ToolStripMenuItem1.Size = new System.Drawing.Size(208, 22);
            this.프리셋저장경로재설정ToolStripMenuItem1.Text = "프리셋 저장 경로 재설정";
            this.프리셋저장경로재설정ToolStripMenuItem1.Click += new System.EventHandler(this.프리셋저장경로재설정ToolStripMenuItem1_Click);
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(205, 6);
            // 
            // 프리셋폴더경로재설정ToolStripMenuItem
            // 
            this.프리셋폴더경로재설정ToolStripMenuItem.Name = "프리셋폴더경로재설정ToolStripMenuItem";
            this.프리셋폴더경로재설정ToolStripMenuItem.Size = new System.Drawing.Size(208, 22);
            this.프리셋폴더경로재설정ToolStripMenuItem.Text = "프리셋 폴더 경로 재설정";
            this.프리셋폴더경로재설정ToolStripMenuItem.Click += new System.EventHandler(this.프리셋폴더경로재설정ToolStripMenuItem_Click);
            // 
            // 도움말FToolStripMenuItem
            // 
            this.도움말FToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.hLLF설명ToolStripMenuItem});
            this.도움말FToolStripMenuItem.Name = "도움말FToolStripMenuItem";
            this.도움말FToolStripMenuItem.Size = new System.Drawing.Size(70, 20);
            this.도움말FToolStripMenuItem.Text = "도움말(&F)";
            // 
            // hLLF설명ToolStripMenuItem
            // 
            this.hLLF설명ToolStripMenuItem.Name = "hLLF설명ToolStripMenuItem";
            this.hLLF설명ToolStripMenuItem.Size = new System.Drawing.Size(141, 22);
            this.hLLF설명ToolStripMenuItem.Text = "HF, LF 설명";
            this.hLLF설명ToolStripMenuItem.Click += new System.EventHandler(this.hLLF설명ToolStripMenuItem_Click);
            // 
            // 정렬ToolStripMenuItem
            // 
            this.정렬ToolStripMenuItem.AutoToolTip = true;
            this.정렬ToolStripMenuItem.BackColor = System.Drawing.Color.Transparent;
            this.정렬ToolStripMenuItem.Checked = true;
            this.정렬ToolStripMenuItem.CheckOnClick = true;
            this.정렬ToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.정렬ToolStripMenuItem.Enabled = false;
            this.정렬ToolStripMenuItem.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.정렬ToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("정렬ToolStripMenuItem.Image")));
            this.정렬ToolStripMenuItem.Name = "정렬ToolStripMenuItem";
            this.정렬ToolStripMenuItem.Size = new System.Drawing.Size(28, 20);
            this.정렬ToolStripMenuItem.ToolTipText = "콤보박스의 항목을 언어순에 따라 위로 정렬합니다.";
            this.정렬ToolStripMenuItem.CheckedChanged += new System.EventHandler(this.정렬ToolStripMenuItem_CheckedChanged);
            // 
            // toolStripComboBox1
            // 
            this.toolStripComboBox1.AutoToolTip = true;
            this.toolStripComboBox1.Name = "toolStripComboBox1";
            this.toolStripComboBox1.Size = new System.Drawing.Size(180, 20);
            this.toolStripComboBox1.Text = "(기본값)";
            this.toolStripComboBox1.ToolTipText = "반향효과(Reverb) 프리셋 목록 입니다,";
            this.toolStripComboBox1.SelectedIndexChanged += new System.EventHandler(this.toolStripComboBox1_SelectedIndexChanged);
            // 
            // 폴더선택ToolStripMenuItem
            // 
            this.폴더선택ToolStripMenuItem.AutoToolTip = true;
            this.폴더선택ToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("폴더선택ToolStripMenuItem.Image")));
            this.폴더선택ToolStripMenuItem.Name = "폴더선택ToolStripMenuItem";
            this.폴더선택ToolStripMenuItem.Size = new System.Drawing.Size(28, 20);
            this.폴더선택ToolStripMenuItem.ToolTipText = "현재 선택된 프리셋을 목록에서 제거합니다.";
            this.폴더선택ToolStripMenuItem.Click += new System.EventHandler(this.폴더선택ToolStripMenuItem_Click);
            // 
            // 새로고침ToolStripMenuItem
            // 
            this.새로고침ToolStripMenuItem.AutoToolTip = true;
            this.새로고침ToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("새로고침ToolStripMenuItem.Image")));
            this.새로고침ToolStripMenuItem.Name = "새로고침ToolStripMenuItem";
            this.새로고침ToolStripMenuItem.Size = new System.Drawing.Size(28, 20);
            this.새로고침ToolStripMenuItem.Click += new System.EventHandler(this.새로고침ToolStripMenuItem_Click);
            // 
            // 제거ToolStripMenuItem
            // 
            this.제거ToolStripMenuItem.AutoToolTip = true;
            this.제거ToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("제거ToolStripMenuItem.Image")));
            this.제거ToolStripMenuItem.Name = "제거ToolStripMenuItem";
            this.제거ToolStripMenuItem.Size = new System.Drawing.Size(28, 20);
            this.제거ToolStripMenuItem.ToolTipText = "현재 선택된 프리셋을 목록에서 제거합니다.";
            this.제거ToolStripMenuItem.Click += new System.EventHandler(this.제거ToolStripMenuItem_Click);
            // 
            // button6
            // 
            this.button6.Location = new System.Drawing.Point(216, 142);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(50, 19);
            this.button6.TabIndex = 18;
            this.button6.Text = "기본값";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            this.button6.KeyDown += new System.Windows.Forms.KeyEventHandler(this.numEnvironment_KeyDown);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(2, 145);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(56, 12);
            this.label6.TabIndex = 17;
            this.label6.Text = "Room LF";
            this.toolTip1.SetToolTip(this.label6, "최소값: -10000, 최댓값: 0, 기본값: 0\nrelative room effect level at low frequencies (win32 " +
                    "only)");
            // 
            // numRoomLF
            // 
            this.numRoomLF.Increment = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.numRoomLF.Location = new System.Drawing.Point(137, 142);
            this.numRoomLF.Maximum = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.numRoomLF.Minimum = new decimal(new int[] {
            10000,
            0,
            0,
            -2147483648});
            this.numRoomLF.Name = "numRoomLF";
            this.numRoomLF.Size = new System.Drawing.Size(73, 21);
            this.numRoomLF.TabIndex = 16;
            this.toolTip1.SetToolTip(this.numRoomLF, "최소값: -10000, 최댓값: 0, 기본값: 0\nrelative room effect level at low frequencies (win32 " +
                    "only)");
            this.numRoomLF.ValueChanged += new System.EventHandler(this.num_ValueChanged);
            this.numRoomLF.KeyDown += new System.Windows.Forms.KeyEventHandler(this.numEnvironment_KeyDown);
            // 
            // button7
            // 
            this.button7.Location = new System.Drawing.Point(453, 143);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(50, 19);
            this.button7.TabIndex = 36;
            this.button7.Text = "기본값";
            this.button7.UseVisualStyleBackColor = true;
            this.button7.Click += new System.EventHandler(this.button7_Click);
            this.button7.KeyDown += new System.Windows.Forms.KeyEventHandler(this.numEnvironment_KeyDown);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(272, 144);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(81, 12);
            this.label7.TabIndex = 35;
            this.label7.Text = "HF Reference";
            this.toolTip1.SetToolTip(this.label7, "reference high frequency (hz) (win32/xbox)");
            // 
            // numHFReference
            // 
            this.numHFReference.DecimalPlaces = 1;
            this.numHFReference.Increment = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.numHFReference.Location = new System.Drawing.Point(385, 142);
            this.numHFReference.Maximum = new decimal(new int[] {
            20000,
            0,
            0,
            0});
            this.numHFReference.Minimum = new decimal(new int[] {
            1000,
            0,
            0,
            -2147483648});
            this.numHFReference.Name = "numHFReference";
            this.numHFReference.Size = new System.Drawing.Size(65, 21);
            this.numHFReference.TabIndex = 34;
            this.toolTip1.SetToolTip(this.numHFReference, "최소값: 1000.0, 최댓값: 20000, 기본값: 5000.0\nReference high frequency (hz) (win32/xbox)");
            this.numHFReference.Value = new decimal(new int[] {
            5000,
            0,
            0,
            0});
            this.numHFReference.ValueChanged += new System.EventHandler(this.num_ValueChanged);
            this.numHFReference.KeyDown += new System.Windows.Forms.KeyEventHandler(this.numEnvironment_KeyDown);
            // 
            // button8
            // 
            this.button8.Location = new System.Drawing.Point(453, 120);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(50, 19);
            this.button8.TabIndex = 33;
            this.button8.Text = "기본값";
            this.button8.UseVisualStyleBackColor = true;
            this.button8.Click += new System.EventHandler(this.button8_Click);
            this.button8.KeyDown += new System.Windows.Forms.KeyEventHandler(this.numEnvironment_KeyDown);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(272, 121);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(103, 12);
            this.label8.TabIndex = 32;
            this.label8.Text = "Modulation Depth";
            // 
            // numModulationDepth
            // 
            this.numModulationDepth.DecimalPlaces = 4;
            this.numModulationDepth.Increment = new decimal(new int[] {
            1,
            0,
            0,
            262144});
            this.numModulationDepth.Location = new System.Drawing.Point(385, 119);
            this.numModulationDepth.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            65536});
            this.numModulationDepth.Name = "numModulationDepth";
            this.numModulationDepth.Size = new System.Drawing.Size(65, 21);
            this.numModulationDepth.TabIndex = 31;
            this.toolTip1.SetToolTip(this.numModulationDepth, "최소값: 0.0, 최댓값: 1.0, 기본값: 0.0\r\nModulation depth (win32 only)");
            this.numModulationDepth.ValueChanged += new System.EventHandler(this.num_ValueChanged);
            this.numModulationDepth.KeyDown += new System.Windows.Forms.KeyEventHandler(this.numEnvironment_KeyDown);
            // 
            // button9
            // 
            this.button9.Location = new System.Drawing.Point(453, 97);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(50, 19);
            this.button9.TabIndex = 30;
            this.button9.Text = "기본값";
            this.button9.UseVisualStyleBackColor = true;
            this.button9.Click += new System.EventHandler(this.button9_Click);
            this.button9.KeyDown += new System.Windows.Forms.KeyEventHandler(this.numEnvironment_KeyDown);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(272, 98);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(100, 12);
            this.label9.TabIndex = 29;
            this.label9.Text = "Modulation Time";
            // 
            // numModulationTime
            // 
            this.numModulationTime.DecimalPlaces = 4;
            this.numModulationTime.Increment = new decimal(new int[] {
            5,
            0,
            0,
            196608});
            this.numModulationTime.Location = new System.Drawing.Point(385, 96);
            this.numModulationTime.Maximum = new decimal(new int[] {
            4,
            0,
            0,
            0});
            this.numModulationTime.Minimum = new decimal(new int[] {
            4,
            0,
            0,
            131072});
            this.numModulationTime.Name = "numModulationTime";
            this.numModulationTime.Size = new System.Drawing.Size(65, 21);
            this.numModulationTime.TabIndex = 28;
            this.toolTip1.SetToolTip(this.numModulationTime, "최소값: 0.04, 최댓값: 4.0, 기본값: 0.25\nModulation time (win32 only)");
            this.numModulationTime.Value = new decimal(new int[] {
            25,
            0,
            0,
            131072});
            this.numModulationTime.ValueChanged += new System.EventHandler(this.num_ValueChanged);
            this.numModulationTime.KeyDown += new System.Windows.Forms.KeyEventHandler(this.numEnvironment_KeyDown);
            // 
            // button10
            // 
            this.button10.Location = new System.Drawing.Point(453, 74);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(50, 19);
            this.button10.TabIndex = 27;
            this.button10.Text = "기본값";
            this.button10.UseVisualStyleBackColor = true;
            this.button10.Click += new System.EventHandler(this.button10_Click);
            this.button10.KeyDown += new System.Windows.Forms.KeyEventHandler(this.numEnvironment_KeyDown);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(272, 75);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(80, 12);
            this.label10.TabIndex = 26;
            this.label10.Text = "Reverb Delay";
            // 
            // numReverbDelay
            // 
            this.numReverbDelay.DecimalPlaces = 4;
            this.numReverbDelay.Increment = new decimal(new int[] {
            5,
            0,
            0,
            262144});
            this.numReverbDelay.Location = new System.Drawing.Point(385, 73);
            this.numReverbDelay.Maximum = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.numReverbDelay.Name = "numReverbDelay";
            this.numReverbDelay.Size = new System.Drawing.Size(65, 21);
            this.numReverbDelay.TabIndex = 25;
            this.toolTip1.SetToolTip(this.numReverbDelay, "최소값: 0.0, 최댓값: 0.1, 기본값: 0.011\nLate reverberation delay time relative to initial " +
                    "reflection (win32/xbox)");
            this.numReverbDelay.Value = new decimal(new int[] {
            11,
            0,
            0,
            196608});
            this.numReverbDelay.ValueChanged += new System.EventHandler(this.num_ValueChanged);
            this.numReverbDelay.KeyDown += new System.Windows.Forms.KeyEventHandler(this.numEnvironment_KeyDown);
            // 
            // button11
            // 
            this.button11.Location = new System.Drawing.Point(453, 51);
            this.button11.Name = "button11";
            this.button11.Size = new System.Drawing.Size(50, 19);
            this.button11.TabIndex = 24;
            this.button11.Text = "기본값";
            this.button11.UseVisualStyleBackColor = true;
            this.button11.Click += new System.EventHandler(this.button11_Click);
            this.button11.KeyDown += new System.Windows.Forms.KeyEventHandler(this.numEnvironment_KeyDown);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label11.Location = new System.Drawing.Point(272, 53);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(50, 12);
            this.label11.TabIndex = 23;
            this.label11.Text = "Reverb";
            // 
            // numReverb
            // 
            this.numReverb.Increment = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.numReverb.Location = new System.Drawing.Point(385, 50);
            this.numReverb.Maximum = new decimal(new int[] {
            2000,
            0,
            0,
            0});
            this.numReverb.Minimum = new decimal(new int[] {
            10000,
            0,
            0,
            -2147483648});
            this.numReverb.Name = "numReverb";
            this.numReverb.Size = new System.Drawing.Size(65, 21);
            this.numReverb.TabIndex = 22;
            this.toolTip1.SetToolTip(this.numReverb, "최소값: -10000, 최댓값: 2000, 기본값: 200\nLate reverberation level relative to room effect" +
                    " (win32/xbox)");
            this.numReverb.Value = new decimal(new int[] {
            200,
            0,
            0,
            0});
            this.numReverb.ValueChanged += new System.EventHandler(this.num_ValueChanged);
            this.numReverb.KeyDown += new System.Windows.Forms.KeyEventHandler(this.numEnvironment_KeyDown);
            // 
            // button12
            // 
            this.button12.Location = new System.Drawing.Point(453, 28);
            this.button12.Name = "button12";
            this.button12.Size = new System.Drawing.Size(50, 19);
            this.button12.TabIndex = 21;
            this.button12.Text = "기본값";
            this.button12.UseVisualStyleBackColor = true;
            this.button12.Click += new System.EventHandler(this.button12_Click);
            this.button12.KeyDown += new System.Windows.Forms.KeyEventHandler(this.numEnvironment_KeyDown);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(272, 31);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(103, 12);
            this.label12.TabIndex = 20;
            this.label12.Text = "Reflections Delay";
            // 
            // numReflectionsDelay
            // 
            this.numReflectionsDelay.DecimalPlaces = 4;
            this.numReflectionsDelay.Increment = new decimal(new int[] {
            1,
            0,
            0,
            262144});
            this.numReflectionsDelay.Location = new System.Drawing.Point(385, 27);
            this.numReflectionsDelay.Maximum = new decimal(new int[] {
            3,
            0,
            0,
            65536});
            this.numReflectionsDelay.Name = "numReflectionsDelay";
            this.numReflectionsDelay.Size = new System.Drawing.Size(65, 21);
            this.numReflectionsDelay.TabIndex = 19;
            this.toolTip1.SetToolTip(this.numReflectionsDelay, "최소값: 0.0, 최댓값:0.3, 기본값: 0.007\nInitial reflection delay time (win32/xbox)");
            this.numReflectionsDelay.Value = new decimal(new int[] {
            7,
            0,
            0,
            196608});
            this.numReflectionsDelay.ValueChanged += new System.EventHandler(this.num_ValueChanged);
            this.numReflectionsDelay.KeyDown += new System.Windows.Forms.KeyEventHandler(this.numEnvironment_KeyDown);
            // 
            // button13
            // 
            this.button13.Location = new System.Drawing.Point(216, 235);
            this.button13.Name = "button13";
            this.button13.Size = new System.Drawing.Size(50, 19);
            this.button13.TabIndex = 47;
            this.button13.Text = "기본값";
            this.button13.UseVisualStyleBackColor = true;
            this.button13.Click += new System.EventHandler(this.button13_Click);
            this.button13.KeyDown += new System.Windows.Forms.KeyEventHandler(this.numEnvironment_KeyDown);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(2, 236);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(67, 12);
            this.label13.TabIndex = 46;
            this.label13.Text = "Reflections";
            this.toolTip1.SetToolTip(this.label13, "Early reflections level relative to room effect (win32/xbox)");
            // 
            // numReflections
            // 
            this.numReflections.Increment = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.numReflections.Location = new System.Drawing.Point(137, 234);
            this.numReflections.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.numReflections.Minimum = new decimal(new int[] {
            10000,
            0,
            0,
            -2147483648});
            this.numReflections.Name = "numReflections";
            this.numReflections.Size = new System.Drawing.Size(73, 21);
            this.numReflections.TabIndex = 45;
            this.toolTip1.SetToolTip(this.numReflections, "최소값: -10000, 최댓값:1000, 기본값: -2602\nEarly reflections level relative to room effect" +
                    " (win32/xbox)");
            this.numReflections.Value = new decimal(new int[] {
            2602,
            0,
            0,
            -2147483648});
            this.numReflections.ValueChanged += new System.EventHandler(this.num_ValueChanged);
            this.numReflections.KeyDown += new System.Windows.Forms.KeyEventHandler(this.numEnvironment_KeyDown);
            // 
            // button14
            // 
            this.button14.Location = new System.Drawing.Point(216, 212);
            this.button14.Name = "button14";
            this.button14.Size = new System.Drawing.Size(50, 19);
            this.button14.TabIndex = 44;
            this.button14.Text = "기본값";
            this.button14.UseVisualStyleBackColor = true;
            this.button14.Click += new System.EventHandler(this.button14_Click);
            this.button14.KeyDown += new System.Windows.Forms.KeyEventHandler(this.numEnvironment_KeyDown);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(2, 213);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(91, 12);
            this.label14.TabIndex = 43;
            this.label14.Text = "Decay LF Ratio";
            this.toolTip1.SetToolTip(this.label14, "Low-frequency to Mid-frequency decay time Ratio (win32 only)");
            // 
            // numDecayLFRatio
            // 
            this.numDecayLFRatio.DecimalPlaces = 3;
            this.numDecayLFRatio.Increment = new decimal(new int[] {
            5,
            0,
            0,
            196608});
            this.numDecayLFRatio.Location = new System.Drawing.Point(137, 211);
            this.numDecayLFRatio.Maximum = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.numDecayLFRatio.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            -2147418112});
            this.numDecayLFRatio.Name = "numDecayLFRatio";
            this.numDecayLFRatio.Size = new System.Drawing.Size(73, 21);
            this.numDecayLFRatio.TabIndex = 42;
            this.toolTip1.SetToolTip(this.numDecayLFRatio, "최소값: 0.1, 최댓값:2.0, 기본값: 1.0\nLow-frequency to Mid-frequency decay time Ratio (win3" +
                    "2 only)");
            this.numDecayLFRatio.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numDecayLFRatio.ValueChanged += new System.EventHandler(this.num_ValueChanged);
            this.numDecayLFRatio.KeyDown += new System.Windows.Forms.KeyEventHandler(this.numEnvironment_KeyDown);
            // 
            // button15
            // 
            this.button15.Location = new System.Drawing.Point(216, 189);
            this.button15.Name = "button15";
            this.button15.Size = new System.Drawing.Size(50, 19);
            this.button15.TabIndex = 41;
            this.button15.Text = "기본값";
            this.button15.UseVisualStyleBackColor = true;
            this.button15.Click += new System.EventHandler(this.button15_Click);
            this.button15.KeyDown += new System.Windows.Forms.KeyEventHandler(this.numEnvironment_KeyDown);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(2, 190);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(92, 12);
            this.label15.TabIndex = 40;
            this.label15.Text = "Decay HF Ratio";
            this.toolTip1.SetToolTip(this.label15, "High-frequency to Mid-frequency decay time ratio (win32/xbox)");
            // 
            // numDecayHFRatio
            // 
            this.numDecayHFRatio.DecimalPlaces = 3;
            this.numDecayHFRatio.Increment = new decimal(new int[] {
            5,
            0,
            0,
            196608});
            this.numDecayHFRatio.Location = new System.Drawing.Point(137, 188);
            this.numDecayHFRatio.Maximum = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.numDecayHFRatio.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            -2147418112});
            this.numDecayHFRatio.Name = "numDecayHFRatio";
            this.numDecayHFRatio.Size = new System.Drawing.Size(73, 21);
            this.numDecayHFRatio.TabIndex = 39;
            this.toolTip1.SetToolTip(this.numDecayHFRatio, "최소값: 0.1, 최댓값:2.0, 기본값: 0.83\nHigh-frequency to Mid-frequency decay time ratio (wi" +
                    "n32/xbox)");
            this.numDecayHFRatio.Value = new decimal(new int[] {
            83,
            0,
            0,
            131072});
            this.numDecayHFRatio.ValueChanged += new System.EventHandler(this.num_ValueChanged);
            this.numDecayHFRatio.KeyDown += new System.Windows.Forms.KeyEventHandler(this.numEnvironment_KeyDown);
            // 
            // button16
            // 
            this.button16.Location = new System.Drawing.Point(216, 166);
            this.button16.Name = "button16";
            this.button16.Size = new System.Drawing.Size(50, 19);
            this.button16.TabIndex = 38;
            this.button16.Text = "기본값";
            this.button16.UseVisualStyleBackColor = true;
            this.button16.Click += new System.EventHandler(this.button16_Click);
            this.button16.KeyDown += new System.Windows.Forms.KeyEventHandler(this.numEnvironment_KeyDown);
            // 
            // numDecayTime
            // 
            this.numDecayTime.DecimalPlaces = 4;
            this.numDecayTime.Increment = new decimal(new int[] {
            5,
            0,
            0,
            196608});
            this.numDecayTime.Location = new System.Drawing.Point(137, 165);
            this.numDecayTime.Maximum = new decimal(new int[] {
            20,
            0,
            0,
            0});
            this.numDecayTime.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.numDecayTime.Name = "numDecayTime";
            this.numDecayTime.Size = new System.Drawing.Size(73, 21);
            this.numDecayTime.TabIndex = 37;
            this.toolTip1.SetToolTip(this.numDecayTime, "최소값: 0.1, 최댓값:20.0, 기본값: 1.49\nReverberation decay time at mid frequencies (win32/" +
                    "xbox)");
            this.numDecayTime.Value = new decimal(new int[] {
            49,
            0,
            0,
            131072});
            this.numDecayTime.ValueChanged += new System.EventHandler(this.num_ValueChanged);
            this.numDecayTime.KeyDown += new System.Windows.Forms.KeyEventHandler(this.numEnvironment_KeyDown);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label16.Location = new System.Drawing.Point(2, 167);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(84, 12);
            this.label16.TabIndex = 48;
            this.label16.Text = "Decay Time";
            this.toolTip1.SetToolTip(this.label16, "Reverberation decay time at mid frequencies (win32/xbox)");
            // 
            // button17
            // 
            this.button17.Location = new System.Drawing.Point(453, 189);
            this.button17.Name = "button17";
            this.button17.Size = new System.Drawing.Size(50, 19);
            this.button17.TabIndex = 54;
            this.button17.Text = "기본값";
            this.button17.UseVisualStyleBackColor = true;
            this.button17.Click += new System.EventHandler(this.button17_Click);
            this.button17.KeyDown += new System.Windows.Forms.KeyEventHandler(this.numEnvironment_KeyDown);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Enabled = false;
            this.label17.Location = new System.Drawing.Point(272, 190);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(53, 12);
            this.label17.TabIndex = 53;
            this.label17.Text = "Diffusion";
            // 
            // numDiffusion
            // 
            this.numDiffusion.DecimalPlaces = 1;
            this.numDiffusion.Enabled = false;
            this.numDiffusion.Increment = new decimal(new int[] {
            5,
            0,
            0,
            65536});
            this.numDiffusion.Location = new System.Drawing.Point(385, 188);
            this.numDiffusion.Name = "numDiffusion";
            this.numDiffusion.Size = new System.Drawing.Size(65, 21);
            this.numDiffusion.TabIndex = 52;
            this.toolTip1.SetToolTip(this.numDiffusion, "최소값: 0.0, 최댓값:100.0, 기본값: 100.0\nValue that controls the echo density in the late " +
                    "reverberation decay. (xbox only)");
            this.numDiffusion.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.numDiffusion.ValueChanged += new System.EventHandler(this.num_ValueChanged);
            this.numDiffusion.KeyDown += new System.Windows.Forms.KeyEventHandler(this.numEnvironment_KeyDown);
            // 
            // button18
            // 
            this.button18.Location = new System.Drawing.Point(453, 166);
            this.button18.Name = "button18";
            this.button18.Size = new System.Drawing.Size(50, 19);
            this.button18.TabIndex = 51;
            this.button18.Text = "기본값";
            this.button18.UseVisualStyleBackColor = true;
            this.button18.Click += new System.EventHandler(this.button18_Click);
            this.button18.KeyDown += new System.Windows.Forms.KeyEventHandler(this.numEnvironment_KeyDown);
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(272, 167);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(80, 12);
            this.label18.TabIndex = 50;
            this.label18.Text = "LF Reference";
            // 
            // numLFReference
            // 
            this.numLFReference.DecimalPlaces = 1;
            this.numLFReference.Increment = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.numLFReference.Location = new System.Drawing.Point(385, 165);
            this.numLFReference.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.numLFReference.Minimum = new decimal(new int[] {
            20,
            0,
            0,
            0});
            this.numLFReference.Name = "numLFReference";
            this.numLFReference.Size = new System.Drawing.Size(65, 21);
            this.numLFReference.TabIndex = 49;
            this.toolTip1.SetToolTip(this.numLFReference, "최소값: 20.0, 최댓값:1000.0, 기본값: 250.0\nReference low frequency (hz) (win32 only)");
            this.numLFReference.Value = new decimal(new int[] {
            250,
            0,
            0,
            0});
            this.numLFReference.ValueChanged += new System.EventHandler(this.num_ValueChanged);
            this.numLFReference.KeyDown += new System.Windows.Forms.KeyEventHandler(this.numEnvironment_KeyDown);
            // 
            // button19
            // 
            this.button19.Location = new System.Drawing.Point(453, 235);
            this.button19.Name = "button19";
            this.button19.Size = new System.Drawing.Size(50, 19);
            this.button19.TabIndex = 60;
            this.button19.Text = "기본값";
            this.button19.UseVisualStyleBackColor = true;
            this.button19.Click += new System.EventHandler(this.button19_Click);
            this.button19.KeyDown += new System.Windows.Forms.KeyEventHandler(this.numEnvironment_KeyDown);
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(272, 236);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(36, 12);
            this.label19.TabIndex = 59;
            this.label19.Text = "Flags";
            // 
            // button20
            // 
            this.button20.Location = new System.Drawing.Point(453, 212);
            this.button20.Name = "button20";
            this.button20.Size = new System.Drawing.Size(50, 19);
            this.button20.TabIndex = 57;
            this.button20.Text = "기본값";
            this.button20.UseVisualStyleBackColor = true;
            this.button20.Click += new System.EventHandler(this.button20_Click);
            this.button20.KeyDown += new System.Windows.Forms.KeyEventHandler(this.numEnvironment_KeyDown);
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Enabled = false;
            this.label20.Location = new System.Drawing.Point(272, 213);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(47, 12);
            this.label20.TabIndex = 56;
            this.label20.Text = "Density";
            // 
            // numDensity
            // 
            this.numDensity.DecimalPlaces = 1;
            this.numDensity.Enabled = false;
            this.numDensity.Increment = new decimal(new int[] {
            5,
            0,
            0,
            65536});
            this.numDensity.Location = new System.Drawing.Point(385, 211);
            this.numDensity.Name = "numDensity";
            this.numDensity.Size = new System.Drawing.Size(65, 21);
            this.numDensity.TabIndex = 55;
            this.toolTip1.SetToolTip(this.numDensity, "최소값: 0.0, 최댓값:100.0, 기본값: 100.0\nValue that controls the modal density in the late" +
                    " reverberation decay (xbox only)");
            this.numDensity.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.numDensity.ValueChanged += new System.EventHandler(this.num_ValueChanged);
            this.numDensity.KeyDown += new System.Windows.Forms.KeyEventHandler(this.numEnvironment_KeyDown);
            // 
            // numFlag
            // 
            this.numFlag.BackColor = System.Drawing.SystemColors.Control;
            this.numFlag.Items.Add("0x000");
            this.numFlag.Items.Add("0x1F");
            this.numFlag.Items.Add("0x3F");
            this.numFlag.Items.Add("0x33F");
            this.numFlag.Location = new System.Drawing.Point(385, 233);
            this.numFlag.Name = "numFlag";
            this.numFlag.ReadOnly = true;
            this.numFlag.Size = new System.Drawing.Size(65, 21);
            this.numFlag.TabIndex = 61;
            this.numFlag.Text = "0x003";
            this.toolTip1.SetToolTip(this.numFlag, "Modifies the behavior of above properties (win32/ps2)");
            this.numFlag.SelectedItemChanged += new System.EventHandler(this.num_ValueChanged);
            this.numFlag.KeyDown += new System.Windows.Forms.KeyEventHandler(this.numEnvironment_KeyDown);
            // 
            // saveFileDialog1
            // 
            this.saveFileDialog1.DefaultExt = "*.refctprof";
            this.saveFileDialog1.FileName = "제목 없음";
            this.saveFileDialog1.Filter = "반향효과(Reverb) 프리셋 (*.refctprof)|*.refctprof";
            this.saveFileDialog1.SupportMultiDottedExtensions = true;
            this.saveFileDialog1.Title = "다른 이름으로 반향효과(Reverb) 저장";
            this.saveFileDialog1.FileOk += new System.ComponentModel.CancelEventHandler(this.saveFileDialog1_FileOk);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.DefaultExt = "*.refctprof";
            this.openFileDialog1.FileName = "제목 없음";
            this.openFileDialog1.Filter = "반향효과(Reverb) 프리셋 (*.refctprof)|*.refctprof";
            this.openFileDialog1.SupportMultiDottedExtensions = true;
            this.openFileDialog1.Title = "반향효과(Reverb) 열기";
            this.openFileDialog1.FileOk += new System.ComponentModel.CancelEventHandler(this.openFileDialog1_FileOk);
            // 
            // trbVolume
            // 
            this.trbVolume.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.trbVolume.AutoSize = false;
            this.trbVolume.Location = new System.Drawing.Point(56, 258);
            this.trbVolume.Maximum = 1250;
            this.trbVolume.Name = "trbVolume";
            this.trbVolume.Size = new System.Drawing.Size(323, 26);
            this.trbVolume.TabIndex = 62;
            this.trbVolume.TickFrequency = 20;
            this.trbVolume.Value = 1000;
            this.trbVolume.ValueChanged += new System.EventHandler(this.tbGain_ValueChanged);
            this.trbVolume.KeyDown += new System.Windows.Forms.KeyEventHandler(this.numEnvironment_KeyDown);
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(2, 266);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(48, 12);
            this.label21.TabIndex = 63;
            this.label21.Text = "Volume";
            // 
            // numVolume
            // 
            this.numVolume.DecimalPlaces = 1;
            this.numVolume.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.numVolume.Location = new System.Drawing.Point(385, 264);
            this.numVolume.Maximum = new decimal(new int[] {
            25,
            0,
            0,
            0});
            this.numVolume.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            -2147483648});
            this.numVolume.Name = "numVolume";
            this.numVolume.Size = new System.Drawing.Size(65, 21);
            this.numVolume.TabIndex = 64;
            this.numVolume.ValueChanged += new System.EventHandler(this.numGain_ValueChanged);
            this.numVolume.VisibleChanged += new System.EventHandler(this.numGain_VisibleChanged);
            // 
            // button21
            // 
            this.button21.Location = new System.Drawing.Point(453, 263);
            this.button21.Name = "button21";
            this.button21.Size = new System.Drawing.Size(50, 19);
            this.button21.TabIndex = 65;
            this.button21.Text = "기본값";
            this.button21.UseVisualStyleBackColor = true;
            this.button21.Click += new System.EventHandler(this.button21_Click);
            this.button21.KeyDown += new System.Windows.Forms.KeyEventHandler(this.numEnvironment_KeyDown);
            // 
            // domainUpDown1
            // 
            this.domainUpDown1.BackColor = System.Drawing.Color.White;
            this.domainUpDown1.Items.Add("-99.9");
            this.domainUpDown1.Items.Add("-Inf.");
            this.domainUpDown1.Location = new System.Drawing.Point(385, 264);
            this.domainUpDown1.Name = "domainUpDown1";
            this.domainUpDown1.ReadOnly = true;
            this.domainUpDown1.Size = new System.Drawing.Size(65, 21);
            this.domainUpDown1.TabIndex = 66;
            this.domainUpDown1.Text = "-Inf.";
            this.domainUpDown1.Visible = false;
            this.domainUpDown1.Wrap = true;
            this.domainUpDown1.SelectedItemChanged += new System.EventHandler(this.domainUpDown1_SelectedItemChanged);
            this.domainUpDown1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.numEnvironment_KeyDown);
            // 
            // folderBrowserDialog1
            // 
            this.folderBrowserDialog1.Description = "반향효과(Reverb) 프리셋이 들어있는 폴더 선택";
            // 
            // frmSetCustomReverb
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(504, 290);
            this.Controls.Add(this.domainUpDown1);
            this.Controls.Add(this.button21);
            this.Controls.Add(this.numVolume);
            this.Controls.Add(this.label21);
            this.Controls.Add(this.trbVolume);
            this.Controls.Add(this.numFlag);
            this.Controls.Add(this.button19);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.button20);
            this.Controls.Add(this.label20);
            this.Controls.Add(this.numDensity);
            this.Controls.Add(this.button17);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.numDiffusion);
            this.Controls.Add(this.button18);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.numLFReference);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.button13);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.numReflections);
            this.Controls.Add(this.button14);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.numDecayLFRatio);
            this.Controls.Add(this.button15);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.numDecayHFRatio);
            this.Controls.Add(this.button16);
            this.Controls.Add(this.numDecayTime);
            this.Controls.Add(this.button7);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.numHFReference);
            this.Controls.Add(this.button8);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.numModulationDepth);
            this.Controls.Add(this.button9);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.numModulationTime);
            this.Controls.Add(this.button10);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.numReverbDelay);
            this.Controls.Add(this.button11);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.numReverb);
            this.Controls.Add(this.button12);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.numReflectionsDelay);
            this.Controls.Add(this.button6);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.numRoomLF);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.numRoomHF);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.numRoom);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.numEnvDiffusion);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.numEnvironment);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.numInstance);
            this.Controls.Add(this.menuStrip1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.MaximumSize = new System.Drawing.Size(510, 314);
            this.MinimumSize = new System.Drawing.Size(510, 314);
            this.Name = "frmSetCustomReverb";
            this.Text = "반향효과(Reverb) 설정";
            this.Load += new System.EventHandler(this.frmSetCustomReverb_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.numEnvironment_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.numInstance)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numEnvironment)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numEnvDiffusion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numRoom)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numRoomHF)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numRoomLF)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numHFReference)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numModulationDepth)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numModulationTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numReverbDelay)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numReverb)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numReflectionsDelay)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numReflections)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numDecayLFRatio)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numDecayHFRatio)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numDecayTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numDiffusion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numLFReference)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numDensity)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trbVolume)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numVolume)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.NumericUpDown numInstance;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.NumericUpDown numEnvironment;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.NumericUpDown numEnvDiffusion;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.NumericUpDown numRoom;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.NumericUpDown numRoomHF;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem 파일FToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 다른이름으로저장ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 열기ToolStripMenuItem;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.NumericUpDown numRoomLF;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.NumericUpDown numHFReference;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.NumericUpDown numModulationDepth;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.NumericUpDown numModulationTime;
        private System.Windows.Forms.Button button10;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.NumericUpDown numReverbDelay;
        private System.Windows.Forms.Button button11;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.NumericUpDown numReverb;
        private System.Windows.Forms.Button button12;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.NumericUpDown numReflectionsDelay;
        private System.Windows.Forms.Button button13;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.NumericUpDown numReflections;
        private System.Windows.Forms.Button button14;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.NumericUpDown numDecayLFRatio;
        private System.Windows.Forms.Button button15;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.NumericUpDown numDecayHFRatio;
        private System.Windows.Forms.Button button16;
        private System.Windows.Forms.NumericUpDown numDecayTime;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Button button17;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.NumericUpDown numDiffusion;
        private System.Windows.Forms.Button button18;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.NumericUpDown numLFReference;
        private System.Windows.Forms.Button button19;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Button button20;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.NumericUpDown numDensity;
        private System.Windows.Forms.ToolStripMenuItem 설정ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 보두기본값으로설정ToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem 고급설정모드ToolStripMenuItem;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.ToolStripMenuItem 도움말FToolStripMenuItem;
        private System.Windows.Forms.DomainUpDown numFlag;
        private System.Windows.Forms.ToolStripMenuItem hLLF설명ToolStripMenuItem;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.TrackBar trbVolume;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.NumericUpDown numVolume;
        private System.Windows.Forms.Button button21;
        private System.Windows.Forms.DomainUpDown domainUpDown1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem 닫기XToolStripMenuItem;
        private System.Windows.Forms.ToolStripComboBox toolStripComboBox1;
        private System.Windows.Forms.ToolStripMenuItem 새로고침ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 제거ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 새프리셋ToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripMenuItem 정렬ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 폴더선택ToolStripMenuItem;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripMenuItem 프리셋경로재설정ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 프리셋열기경로재설정ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 프리셋저장경로재설정ToolStripMenuItem1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.ToolStripMenuItem 프리셋폴더경로재설정ToolStripMenuItem;
    }
}