﻿using System;
using System.Windows.Forms;
using FMOD;

namespace FMOD_Helper
{
    public partial class DSPHelper : Form
    {
        public DSPHelper()
        {
            InitializeComponent();
        }
        
        private void DSPHelper_Load(object sender, EventArgs e)
        {
            DSP d = new DSP(); d.showConfigDialog(this.Handle, true);
        }
    }
}
