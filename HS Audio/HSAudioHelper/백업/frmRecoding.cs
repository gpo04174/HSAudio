﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;

using System.Text;
using System.Windows.Forms;
using System.IO;

namespace FMOD_Helper.Forms
{
    public partial class frmRecoding : Form
    {
        FMODHelper helper;

        public frmRecoding(FMODHelper Helper)
        {
            InitializeComponent();
            this.helper = Helper;
            w = new Wave.SaveWave(Helper);
        }
        FMOD.RESULT Result { get; set; }

        FMOD_Helper.Wave.SaveWave w;

        private void button1_Click(object sender, EventArgs e)
        {
            timer1.Start();
            w.StartWrite();
        }
        FileStream fs;
        StreamWriter sw;
        private void button2_Click(object sender, EventArgs e)
        {

        }

        private void saveFileDialog1_FileOk(object sender, CancelEventArgs e)
        {
            fs = new FileStream(saveFileDialog1.FileName, FileMode.Append);
            byte[] a=w.WriteWaveNew(w).GetBuffer();
            fs.Write(a, 0, a.Length);
            sw = new StreamWriter(fs);
            sw.Flush(); sw.Close(); fs.Close();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            w.StopAndDispose();
            button2.Enabled = true;
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            label1.Text = w.Data()[w.Data().Length / 2].ToString();
        }

        private void frmRecoding_Load(object sender, EventArgs e)
        {

        }
    }
}
