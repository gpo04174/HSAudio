﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Ernzo.WinForms.Controls;

namespace FMOD_Helper.Forms
{
    public partial class frmFFTGraph : Form
    {
        PropertyGrid pg;
        public frmFFTGraph(FMODHelper Value)
        {
            InitializeComponent();
            SetStyle(ControlStyles.SupportsTransparentBackColor, true);
            pg = new PropertyGrid()
            {
                SelectedObject = this,
                Size = new Size(290, 350),
                //Dock= DockStyle.Fill,
                Location = new Point(0, 0),
                Anchor = AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right | AnchorStyles.Top
            };
            f.FormClosing+=new FormClosingEventHandler(f_FormClosing);
            Wavedata=Ltmp=Rtmp = new float[this.Width];
            Data = Value;
            Shape = DrawShape.Wave;
            peak = new frmPeakMeter(Value);
            toolStripComboBox1.Text = "TRIANGLE";
        }
        public frmFFTGraph()
        {
            InitializeComponent();
            SetStyle(ControlStyles.SupportsTransparentBackColor, true);
            toolStripComboBox1.Text = "TRIANGLE";
        }
        
        /*
        Graphics g;
        Rectangle ScrRec;
        SolidBrush Backsb = new SolidBrush(Color.YellowGreen);
        
        protected override void OnPaint(PaintEventArgs e)
        {
            Backsb.Color = BackgroundColor;
            e.Graphics.FillRectangle(Backsb, 0, 0, this.Width, this.Height);
            g = e.Graphics;
            ScrRec = e.ClipRectangle;
            if (Data.system != null) { drawSpectrum(e.Graphics); }
            //base.OnPaint(e);
        }
        */

        float[] Spectrum = new float[1024];
        float[] Wavedata = new float[512];


        #region 프로퍼티 메서드
        /// <summary>
        /// 스펙트럼을 그릴 방식 입니다.
        /// </summary>
        public enum DrawSpectrum 
        {
            /// <summary>
            /// FFT스펙트럼으로 그립니다.
            /// </summary>
            FFTSpectrum, 
            /// <summary>
            /// 웨이브 데이터를 가지고 그립니다.
            /// </summary>
            WaveData,
        }
        /// <summary>
        /// 스펙트럼을 그릴 방법 입니다.
        /// </summary>
        public enum DrawShape
        {
            /// <summary>
            /// 이퀼라이저 식으로 그립니다.
            /// </summary>
            Equalizer,
            /// <summary>
            /// 파동형식으로 그립니다.
            /// </summary>
            Wave,
        }
        /// <summary>
        /// 스펙트럼(FFT)의 크기를 지정합니다.
        /// </summary>
        public enum FFTSize {None=0,Size_64=64,Size_128=128,Size_256=256,Size_512=512,Size_1024=1024,Size_2048=2048,Size_4096=4096 }
        /// <summary>
        /// 그리기를 시작합니다.
        /// </summary>
        public void StartDraw() { timer1.Start(); }
        /// <summary>
        /// 그리기를 중단합니다.
        /// </summary>
        public void StopDraw() { timer1.Stop(); }
        #endregion

        #region 프로퍼티
        byte _FFTAverage = 2;
        /// <summary>
        /// FFT 그래프의 평균율을 가져오거나 설정합니다. (이 값이 크면 FFT의 그래프가 크게 나타납니다.)
        /// </summary>
        [Category("스펙트럼"), Description("FFT 그래프의 오차율 입니다."), DefaultValue(2)]
        public byte FFTAverage { get { return _FFTAverage; } set { _FFTAverage = value; } }

        /// <summary>
        /// 화면에 FPS라벨을 그릴여부를 가져오거나 설정합니다.
        /// </summary>
        [Category("스펙트럼"), Description("화면에 FPS라벨을 그릴여부 입니다."), DefaultValue(false)]
        public bool FPSLabel { get { return label1.Visible; } set { label1.Visible = value; } }

        FMODHelper _Data;
        /// <summary>
        /// 스펙트럼을 그릴 데이터를 가져오거나 설정합니다.
        /// </summary>
        public FMODHelper Data { get { return _Data; }
            set { if (value == null)
            { throw new NullReferenceException("Data는 NULL일수 없습니다."); }
            _Data = value;}
        }
        //DrawSpectrum _ToDraw = DrawSpectrum.WAVE;
        /// <summary>
        /// 스펙트럼을 그릴 방법을 가져오거나 설정합니다.
        /// </summary>
        [Category("스펙트럼"), Description("스펙트럼을 그릴 방법 입니다."), DefaultValue(DrawSpectrum.FFTSpectrum)]
        public DrawSpectrum ToDraw { get; set; }

        /// <summary>
        /// 스펙트럼을 그릴 시간(밀리초) 를 가져오거나 설정합니다..
        /// </summary>
        [Category("스펙트럼"), Description("스펙트럼을 그릴 시간(밀리초) 입니다."), DefaultValue(100)]
        public int DrawSpeed
        {
            get { return timer1.Interval; }
            set { timer1.Interval = value; peak.Interval = value; }
        }
        FFTSize _FFTSize = FFTSize.Size_2048;
        /// <summary>
        /// 스팩트럼 FFT의 배열사이즈를 가져오거나 설정합니다.(음수는 올수 없습니다.)
        /// </summary>
        [Category("스펙트럼"), Description("FFT스팩트럼의 사이즈 입니다."), DefaultValue(typeof(FFTSize),"Size_1024")]
        public FFTSize FFTsize { get { return _FFTSize; } set { _FFTSize = value; Spectrum = new float[(int)_FFTSize]; } }

        /// <summary>
        ///  웨이브 데이터의 배열사이즈를 가져오거나 설정합니다.(음수는 올수 없습니다.)
        /// </summary>
        [Category("스펙트럼"), Description("웨이브 데이터의 배열 사이즈 입니다."), DefaultValue(256)]
        public int WavedataSize { get { return Wavedata.Length; } set { /*if (value < 255) { throw new Exception("음수가 아니어야 하며 255보다 커야합니다."); }*/ Wavedata = new float[value]; peak.Ltmp = peak.Rtmp = new float[value]; peak.Length = value; } }

        /// <summary>
        /// 배경색을 가져오거나 설정합니다.
        /// </summary>
        [Category("스펙트럼"), Description("배경색 입니다."), DefaultValue(typeof(Color),"White")]
        public Color BackgroundColor { get { return this.splitContainer1.Panel1.BackColor; } set { this.splitContainer1.Panel1.BackColor = this.splitContainer1.Panel2.BackColor = value; } }

        SolidBrush brush = new SolidBrush(Color.Navy);
        Pen pen = new Pen(Color.Navy) {Width=1 };
        /// <summary>
        /// 그래프를 그릴색을 가져오거나 설정합니다.
        /// </summary>
        [Category("스펙트럼"), Description("그래프를 그릴색 입니다."), DefaultValue(typeof(Color), "Navy")]
        public Color DrawColor { get { return brush.Color; } set { brush.Color=pen.Color = value; } }

        SolidBrush brush1 = new SolidBrush(Color.Navy);
        Pen pen1 = new Pen(Color.Navy) { Width = 1 };
        /// <summary>
        /// 중앙선을 그릴색을 가져오거나 설정합니다.
        /// </summary>
        [Category("스펙트럼"), Description("중앙선을 그릴색 입니다."), DefaultValue(typeof(Color), "Black")]
        public Color DrawLineColor { get { return pen1.Color; } set { brush1.Color = pen1.Color = value; } }

        /// <summary>
        /// 분할자의 색깔을 가져오거나 설정합니다.
        /// </summary>
        [Category("스펙트럼"), Description("분할자의 색깔 입니다."), DefaultValue(typeof(Color), "ControlDark")]
        public Color SplitColor { get { return splitContainer1.BackColor; } set { splitContainer1.BackColor = value; } }

        float _LineThick = 1.0f;
        /// <summary>
        /// 그래프의 굵기를 가져오거나 설정합니다.
        /// </summary>
        [Category("스펙트럼"), Description("그래프의 굵기 입니다."), DefaultValue(1.0f)]
        public float LineThick { get { return _LineThick; } set { pen.Width = _LineThick = value; } }
        
        bool _LineTickLinkFFT = true;
        /// <summary>
        /// 그래프의 굵기에따라 FFT의 Hz도 똑같이 나눌지의 여부를 가져오거나 설정합니다.
        /// </summary>
        [Category("스펙트럼"), Description("그래프의 굵기에따라 FFT의 Hz도 똑같이 나눌지 결정합니다."), DefaultValue(true)]
        public bool LineTickLinkFFT { get { return _LineTickLinkFFT; } set { _LineTickLinkFFT = value; } }

        bool _AutoWaveSize = true;
        /// <summary>
        /// 웨이브 데이터 그래프의 사이즈를 자동으로 조정할지 여부를 가져오거나 설정합니다.(True면 자동조정 False면 사용자 조정)
        /// </summary>
        [Category("스펙트럼"), Description("웨이브 그래프의 크기를 자동으로 조정할지 여부를 설정합니다.(True면 자동조정 False면 사용자 조정)"), DefaultValue(true)]
        public bool AutoWaveSize { get { return _AutoWaveSize; } set { _AutoWaveSize = value; } }

        bool _GetData=true;
        /// <summary>
        /// 그래프를 그릴 데이터를 가져오는 방법을 가져오거나 설정합니다. (True면 FMOD.System에서 가져오고 False면 FMOD.Channel에서 가져옵니다.)
        /// </summary>
        [Category("스펙트럼"), Description("그래프를 그릴 데이터를 가져오는 방법 입니다. (True면 FMOD.System에서 가져오고 False면 FMOD.Channel에서 가져옵니다.)"), DefaultValue(true)]
        public bool GetData { get { return _GetData; } set { if (Data.channel == null && !value) { throw new NullReferenceException("FMOD.Channel이 NULL이므로 변경하지 못했습니다."); } _GetData = value; } }

        /// <summary>
        /// 그래프의 형태을 가져오거나 설정합니다.
        /// </summary>
        [Category("스펙트럼"), Description("그래프의 형태 입니다."), DefaultValue(typeof(DrawShape), "Wave")]
        public DrawShape Shape { get;set; }

        bool _Flip = true;
        /// <summary>
        /// 웨이브 데이터 그래프를 이퀼라이저 식으로 그릴때 대칭할 건지를 가져오거나 설정합니다.
        /// </summary>
        [Category("스펙트럼"), Description("웨이브 데이터 그래프를 이퀼라이저 식으로 그릴때 대칭할건지 설정합니다."), DefaultValue(true)]
        public bool Flip { get { return _Flip; } set { _Flip = value; } }

        bool _DrawLine = true;
        /// <summary>
        /// 웨이브데이터 그래프의 중앙에 선을 그릴여부를 가져오거나 설정합니다.(웨이브데이터 그래프이고 Flip 이나 DrawShape.Wave 로 되있을때만 그려집니다.)
        /// </summary>
        [Category("스펙트럼"), Description("웨이브데이터 그래프의 중앙에 선을 그릴지 결정합니다.(웨이브데이터 그래프이고 Flip 이나 DrawShape.Wave 로 되있을때만 그려집니다.)"), DefaultValue(true)]
        public bool DrawLine { get { return _DrawLine; } set { _DrawLine = value; } }

        FMOD.DSP_FFT_WINDOW _FFTWindow = FMOD.DSP_FFT_WINDOW.TRIANGLE;
        [Category("스펙트럼"), Description("FFT윈도우를 설정합니다.")]
        public FMOD.DSP_FFT_WINDOW FFTWindow { get { return _FFTWindow; } set { _FFTWindow = value; } } 
        #endregion
            
        #region Draw 메서드

        int numchannels = 0;
        int dummy = 0;
        FMOD.SOUND_FORMAT dummyformat = FMOD.SOUND_FORMAT.NONE;
        FMOD.DSP_RESAMPLER _dummyresampler = FMOD.DSP_RESAMPLER.LINEAR;
        int count = 0,count2 = 0;
        float count3=0,_dB;
        float max=0;
        List<PointF> p = new List<PointF>();
        List<RectangleF> rec = new List<RectangleF>();

        private void drawSpectrum(Graphics g, Size size, int Channel)
        {
            if (Data.system != null)
            {
                Data.system.getSoftwareFormat(ref dummy, ref dummyformat, ref numchannels, ref dummy, ref _dummyresampler, ref dummy);
            }
            #region True
            if (Shape == DrawShape.Equalizer)
            {
                if (Data.system != null)
                {
                    //Data.system.getSoftwareFormat(ref dummy, ref dummyformat, ref numchannels, ref dummy, ref dummyresampler, ref dummy);

                    /*
                            DRAW SPECTRUM
                    */
                    //for (count = 0; count < numchannels; count++)
                    //{
                    if (GetData) Data.system.getSpectrum(Spectrum, Spectrum.Length, Channel, FFTWindow);
                    else Data.channel.getSpectrum(Spectrum, Spectrum.Length, Channel, FFTWindow);
                        //float max=0;

                    for (count2 = 0; count2 < (Spectrum.Length - 1); count2 = LineTickLinkFFT?count2 + (int)LineThick:count2+1)
                        {
                            if (max < Spectrum[count2])
                            {
                                max = Spectrum[count2];
                            }
                        }

                        /*
                            The upper band of frequencies at 44khz is pretty boring (ie 11-22khz), so we are only
                            going to display the first 256 frequencies, or (0-11khz) 
                        */

                    for (count2 = 2; count2 < (Spectrum.Length - 1); count2 = LineTickLinkFFT ? count2 + (int)LineThick : count2+1)
                        {
                            //count2 = count2 + LineThick;

                            float height = Spectrum[count2] * FFTAverage / max * size.Height;

                            if (height >= size.Height)
                            {
                                height = size.Height - 1;
                            }

                            if (height < 0)
                            {
                                height = 0;
                            }

                            height = size.Height - height;

                            g.FillRectangle(brush, count3, height, (float)LineThick, size.Height - height);
                            count3 = count3 + LineThick;
                        }
                        count3 = 0;
                    //}
                }
            }
            #endregion
            #region False
            else
            {
                if (Data.channel != null)
                {
                    /*
                            DRAW WAVE
                    */
                    //for (count = 0; count < numchannels; count++)
                   // {
                    if (GetData) Data.system.getSpectrum(Spectrum, Spectrum.Length, Channel, FFTWindow);
                    else Data.channel.getSpectrum(Spectrum, Spectrum.Length, Channel, FFTWindow);

                         for (count2 = 0; count2 < Spectrum.Length - 1; count2++)
                        {
                            if (max < Spectrum[count2])
                            {
                                max = Spectrum[count2];
                            }
                        }

                        /*
                            The upper band of frequencies at 44khz is pretty boring (ie 11-22khz), so we are only
                            going to display the first 256 frequencies, or (0-11khz) 
                        */
                        for (count2 = 0; count2 < Spectrum.Length - 1; count2++)
                        { 
                            float height;

                            height = Spectrum[count2] * FFTAverage / max * size.Height;

                            if (height >= size.Height)
                            {
                                height = size.Height - 1;
                            }

                            if (height < 0)
                            {
                                height = 0;
                            }

                            height = size.Height - height;

                            p.Add(new PointF(count2, height));
                        }

                        try { g.DrawLines(pen, p.ToArray()); }
                        catch { } p.Clear();
                   // }
                }
            #endregion
            }
        }

        private void drawOscilliscope(Graphics g, Size size, int channel,ref float MaxdB)
        {
            /*
            int numchannels = 0;
            int dummy = 0;
            FMOD.SOUND_FORMAT dummyformat = FMOD.SOUND_FORMAT.NONE;
            FMOD.DSP_RESAMPLER dummyresampler = FMOD.DSP_RESAMPLER.LINEAR;
            int count = 0;
            int count2 = 0;*/
            //List<PointF> p = new List<PointF>();
            if (Data.system != null)
            { Data.system.getSoftwareFormat(ref dummy, ref dummyformat, ref numchannels, ref dummy, ref _dummyresampler, ref dummy); }
            #region True
            if (Shape == DrawShape.Equalizer)
            {
                //if (GetData)
                {
                    if (Data.channel != null)
                    {
                        /*
                                DRAW WAVEDATA
                        */
                        
                        //for (count = 0; count < numchannels; count++)
                        //{
                        if (GetData) Data.system.getWaveData(Wavedata, Wavedata.Length, channel);
                        else Data.channel.getWaveData(Wavedata, Wavedata.Length, channel);
                            float max = 0;

                            for (count2 = 0; count2 < Wavedata.Length; count2++)
                            {
                                if (max < Wavedata[count2])
                                {
                                    max = Wavedata[count2];
                                }
                            }

                            /*
                                The upper band of frequencies at 44khz is pretty boring (ie 11-22khz), so we are only
                                going to display the first 256 frequencies, or (0-11khz) 
                            */
                            for (count2 = 0; count2 < Wavedata.Length; count2++)
                            {
                                float height;

                                height = Wavedata[count2] / max * size.Height;

                                if (height >= size.Height)
                                {
                                    height = size.Height - 1;
                                }

                                if (height < 0)
                                {
                                    height = 0;
                                }

                                height = size.Height - height;
                                if (_dB < Wavedata[count2]) { _dB =MaxdB= Wavedata[count2]; }
                                try { g.FillRectangle(brush, count3, height, (float)LineThick, size.Height - height); }
                                catch { }
                                count3 = count3 + LineThick;
                                //g.DrawLine(pen,0, size.Height, /*(float)LineThick*/count2, size.Height - height);
                            }
                            count3=_dB = 0;
                        //}

                    }
                }
            }
            #endregion
            #region False
            else
            {
                if (Data.channel != null)
                {
                    /*
                            DRAW WAVEDATA   
                    */
                    //for (count = 0; count < numchannels; count++)
                    //{

                    if (GetData) Data.system.getWaveData(Wavedata, Wavedata.Length, channel);
                    else Data.channel.getWaveData(Wavedata, Wavedata.Length, channel);

                        for (count2 = 0; count2 < WavedataSize; count2++)
                        {
                            float y;

                            y = (Wavedata[count2] + 1) / 2.0f *size.Height;
                            X = count2 + size.Width - WavedataSize;
                            if (_dB < Wavedata[count2]) { _dB = MaxdB = Wavedata[count2]; }
                            p.Add(new PointF(X,y));//g.DrawLine(pen,X,0,X, y);
                            //count3 = X + LineThick;
                        }
                        try { g.DrawLines(pen, p.ToArray()); }
                        catch { } p.Clear(); count3 = _dB = 0;
                    //}
                    //for (count = 0; count < numchannels; count++)
                    //{

                    //}
                }
            }
            #endregion
        }
        private void drawOscilliscope(Graphics g, Size size, int channel,bool Reverse ,ref float MaxdB)
        {
            /*
            int numchannels = 0;
            int dummy = 0;
            FMOD.SOUND_FORMAT dummyformat = FMOD.SOUND_FORMAT.NONE;
            FMOD.DSP_RESAMPLER dummyresampler = FMOD.DSP_RESAMPLER.LINEAR;
            int count = 0;
            int count2 = 0;*/
            //List<PointF> p = new List<PointF>();
            if (Data.system != null)
            { Data.system.getSoftwareFormat(ref dummy, ref dummyformat, ref numchannels, ref dummy, ref _dummyresampler, ref dummy); }
            
            #region True
            if (Shape == DrawShape.Equalizer)
            {
                #region GetDataTrue
                //if (GetData)
                {
                    
                    if (Data.channel != null)
                    {
                        /*
                                DRAW WAVEDATA
                        */

                        //for (count = 0; count < numchannels; count++)
                        //{
                        if (GetData) Data.system.getWaveData(Wavedata, Wavedata.Length, channel);
                        else Data.channel.getWaveData(Wavedata, Wavedata.Length, channel);
                        //Data.system.getWaveData(Wavedata, Wavedata.Length, channel);
                        float max = 0;

                        for (count2 = 0; count2 < Wavedata.Length; count2++)
                        {
                            if (max < Wavedata[count2])
                            {
                                max = Wavedata[count2];
                            }
                        }

                        /*
                            The upper band of frequencies at 44khz is pretty boring (ie 11-22khz), so we are only
                            going to display the first 256 frequencies, or (0-11khz) 
                        */
                        for (count2 = 0; count2 < Wavedata.Length; count2++)
                        {
                            float height;

                            height = Wavedata[count2] / max * size.Height;

                            if (height >= size.Height)
                            {
                                height = size.Height - 1;
                            }

                            if (height < 0)
                            {
                                height = 0;
                            }
                            /*
                            if (!Reverse)
                            {
                                height = size.Height - height;
                                if (_dB < Wavedata[count2]) { _dB = MaxdB = Wavedata[count2]; }
                                try { g.FillRectangle(brush, count3, height/2, (float)LineThick, size.Height - height); }
                                catch { }
                            }*/
                            else
                            {
                                height = size.Height - height;
                                if (_dB < Wavedata[count2]) { _dB = MaxdB = Wavedata[count2]; }
                                try
                                {
                                    if (Reverse)
                                    {
                                        //g.FillRectangle(brush, count3, height / 2, (float)LineThick, size.Height - height);
                                        //recf.X=count2 + size.Width - WavedataSize;
                                        //recf.Y = (Wavedata[count2] + 1) * size.Height;
                                        recf.X = count3; recf.Y = height / 2;
                                        recf.Width = (float)LineThick; recf.Height = size.Height - height;
                                        rec.Add(recf);

                                    }
                                    else
                                    {
                                        //height = size.Height - height;
                                        //if (_dB < Wavedata[count2]) { _dB = MaxdB = Wavedata[count2]; }
                                        try { g.FillRectangle(brush, count3, height, (float)LineThick, size.Height - height); }
                                        catch { }
                                        //g.FillRectangle(brush, count3, 0, (float)LineThick, size.Height - height); 
                                    }
                                }
                                catch { }
                            }
                            count3 = count3 + LineThick;
                            //g.DrawLine(pen,0, size.Height, /*(float)LineThick*/count2, size.Height - height);
                        }
                        if (Reverse)
                        {
                            try { g.FillRectangles(brush, rec.ToArray()); }
                            catch { }
                            rec.Clear();
                        }
                        count3 = _dB = 0;
                        //}

                    }
                }
                #endregion
                #region GetDataFalse
                #endregion
            }
            #endregion
            #region False
            else
            {
                if (Data.channel != null)
                {
                    /*
                            DRAW WAVEDATA   
                    */
                    //for (count = 0; count < numchannels; count++)
                    //{

                    if (GetData) Data.system.getWaveData(Wavedata, Wavedata.Length, channel);
                    else Data.channel.getWaveData(Wavedata, Wavedata.Length, channel);

                    //p.Add(new PointF(1, size.Height / 2)); p.Add(new PointF(2, size.Height / 2));
                    for (count2 = 0/*2*/; count2 < WavedataSize/*-2*/; count2++)
                    {
                        Y = (Wavedata[count2] + 1) / 2.0f * size.Height;
                        X = count2 + size.Width - WavedataSize;
                        if (_dB < Wavedata[count2]) { _dB = MaxdB = Wavedata[count2]; }
                        p.Add(new PointF(X, Y));//g.DrawLine(pen,X,0,X, y);
                        //count3 = X + LineThick;
                    }
                    //p.Add(new PointF(size.Width - 2, size.Height / 2));p.Add(new PointF(size.Width-1, size.Height / 2));
                    try { g.DrawLines(pen, p.ToArray()); }
                    catch { } p.Clear(); count3 = _dB = 0;
                }
            }
            #endregion
        }
        public Bitmap drawOscilliscope(Size size, int channel, bool Reverse, ref float MaxdB)
        {
            /*
            int numchannels = 0;
            int dummy = 0;
            FMOD.SOUND_FORMAT dummyformat = FMOD.SOUND_FORMAT.NONE;
            FMOD.DSP_RESAMPLER dummyresampler = FMOD.DSP_RESAMPLER.LINEAR;
            int count = 0;
            int count2 = 0;*/
            //List<PointF> p = new List<PointF>();
            Bitmap bmp = new Bitmap(size.Width, size.Height);
            Graphics g = Graphics.FromImage(bmp);
            if (Data.system != null)
            { Data.system.getSoftwareFormat(ref dummy, ref dummyformat, ref numchannels, ref dummy, ref _dummyresampler, ref dummy); }
            #region True
            if (Shape == DrawShape.Equalizer)
            {
                #region GetDataTrue
                //if (GetData)
                {

                    if (Data.channel != null)
                    {
                        /*
                                DRAW WAVEDATA
                        */

                        //for (count = 0; count < numchannels; count++)
                        //{
                        if (GetData) Data.system.getWaveData(Wavedata, Wavedata.Length, channel);
                        else Data.channel.getWaveData(Wavedata, Wavedata.Length, channel);
                        //Data.system.getWaveData(Wavedata, Wavedata.Length, channel);
                        float max = 0;

                        for (count2 = 0; count2 < Wavedata.Length; count2++)
                        {
                            if (max < Wavedata[count2])
                            {
                                max = Wavedata[count2];
                            }
                        }

                        /*
                            The upper band of frequencies at 44khz is pretty boring (ie 11-22khz), so we are only
                            going to display the first 256 frequencies, or (0-11khz) 
                        */
                        for (count2 = 0; count2 < Wavedata.Length; count2++)
                        {
                            float height;

                            height = Wavedata[count2] / max * size.Height;

                            if (height >= size.Height)
                            {
                                height = size.Height - 1;
                            }

                            if (height < 0)
                            {
                                height = 0;
                            }
                            /*
                            if (!Reverse)
                            {
                                height = size.Height - height;
                                if (_dB < Wavedata[count2]) { _dB = MaxdB = Wavedata[count2]; }
                                try { g.FillRectangle(brush, count3, height/2, (float)LineThick, size.Height - height); }
                                catch { }
                            }*/
                            else
                            {
                                height = size.Height - height;
                                if (_dB < Wavedata[count2]) { _dB = MaxdB = Wavedata[count2]; }
                                try
                                {
                                    if (Reverse)
                                    {
                                        //g.FillRectangle(brush, count3, height / 2, (float)LineThick, size.Height - height);
                                        //recf.X=count2 + size.Width - WavedataSize;
                                        //recf.Y = (Wavedata[count2] + 1) * size.Height;
                                        recf.X = count3; recf.Y = height / 2;
                                        recf.Width = (float)LineThick; recf.Height = size.Height - height;
                                        rec.Add(recf);

                                    }
                                    else
                                    {
                                        //height = size.Height - height;
                                        //if (_dB < Wavedata[count2]) { _dB = MaxdB = Wavedata[count2]; }
                                        try { g.FillRectangle(brush, count3, height, (float)LineThick, size.Height - height); }
                                        catch { }
                                        //g.FillRectangle(brush, count3, 0, (float)LineThick, size.Height - height); 
                                    }
                                }
                                catch { }
                            }
                            count3 = count3 + LineThick;
                            //g.DrawLine(pen,0, size.Height, /*(float)LineThick*/count2, size.Height - height);
                        }
                        if (Reverse)
                        {
                            try { g.FillRectangles(brush, rec.ToArray()); }
                            catch { }
                            rec.Clear();
                        }
                        count3 = _dB = 0;
                        //}

                    }
                }
                #endregion
                #region GetDataFalse
                #endregion
            }
            #endregion
            #region False
            else
            {
                if (Data.channel != null)
                {
                    /*
                            DRAW WAVEDATA   
                    */
                    //for (count = 0; count < numchannels; count++)
                    //{

                    if (GetData) Data.system.getWaveData(Wavedata, Wavedata.Length, channel);
                    else Data.channel.getWaveData(Wavedata, Wavedata.Length, channel);

                    //p.Add(new PointF(1, size.Height / 2)); p.Add(new PointF(2, size.Height / 2));
                    for (count2 = 0/*2*/; count2 < WavedataSize/*-2*/; count2++)
                    {
                        Y = (Wavedata[count2] + 1) / 2.0f * size.Height;
                        X = count2 + size.Width - WavedataSize;
                        if (_dB < Wavedata[count2]) { _dB = MaxdB = Wavedata[count2]; }
                        p.Add(new PointF(X, Y));//g.DrawLine(pen,X,0,X, y);
                        //count3 = X + LineThick;
                    }
                    //p.Add(new PointF(size.Width - 2, size.Height / 2));p.Add(new PointF(size.Width-1, size.Height / 2));
                    try { g.DrawLines(pen, p.ToArray()); }
                    catch { } p.Clear(); count3 = _dB = 0;
                }
            }
            #endregion
            return bmp;
        }
        float X, Y;
        RectangleF recf;
        static System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmFFTGraph));
        #endregion

        Form f = new Form()
        {
            //MdiParent = this,
            Size = new Size(300, 350),
            Text = "그래프 설정...",
            ShowIcon=false,
            Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon"))),
        };
        frmPeakMeter peak;
        void f_FormClosing(object sender, CancelEventArgs e) { e.Cancel = true; f.Hide(); }
        private void FFTGraph_Load(object sender, EventArgs e)
        {
            timer2.Start();
            f.Controls.Add(pg);
            f.Show();
            peak.Start();
            peak.Show();
            SetStyle(ControlStyles.SupportsTransparentBackColor, true);
            toolStripComboBox2.SelectedIndex = 1;
        }
        float LdB = 0, RdB = 0;
        Point Center, Center1;
        
        private void timer1_Tick(object sender, EventArgs e)
        {
            //SetStyle(ControlStyles.SupportsTransparentBackColor, true);
            //pictureBox1.Refresh(); pictureBox2.Refresh();
            Bitmap bmp = new Bitmap(pictureBox1.Width, pictureBox1.Height),
                   bmp1 = new Bitmap(pictureBox2.Width, pictureBox2.Height);
            Graphics g = Graphics.FromImage(bmp);
            Graphics g1 = Graphics.FromImage(bmp1);
            if (ToDraw == DrawSpectrum.FFTSpectrum) { drawSpectrum(g, bmp.Size, 0); drawSpectrum(g1, bmp1.Size, 1); }
            else if (ToDraw == DrawSpectrum.WaveData)
            {
                //if(Shape == DrawShape.Equalizer)
                if (DrawLine)
                {
                    Center.X = 0; Center.Y = bmp.Height / 2; Center1.X = bmp.Width; Center1.Y = bmp.Height / 2;
                    drawOscilliscope(g, bmp.Size, 0, Flip, ref LdB); g.DrawLine(pen1, Center, Center1);
                    //pictureBox1.CreateGraphics().DrawImageUnscaled(bmp, 0, 0);

                    Center.X = 0; Center.Y = bmp1.Height / 2; Center1.X = bmp1.Width; Center1.Y = bmp1.Height / 2;
                    drawOscilliscope(g1, bmp1.Size, 1, Flip, ref RdB); g1.DrawLine(pen1, Center, Center1);
                    //pictureBox2.CreateGraphics().DrawImageUnscaled(bmp1, 0, 0);
                }
                else { drawOscilliscope(g, bmp.Size, 0, Flip, ref LdB); drawOscilliscope(g1, bmp1.Size, 1, Flip, ref RdB); }
            }
            else
            {

                //g.DrawRectangle(pen, 0, pictureBox1.Height/2, (int)(pictureBox1.Width - ((Decibels(tmpLMax, 0) - 120))*-1), pictureBox1.Height/2);
            }
            pictureBox1.Image = bmp; pictureBox2.Image = bmp1;
            //bmp.Dispose(); bmp1.Dispose(); g.Dispose(); g1.Dispose();
        }

        int[] h = new int[1];

        float[] Ltmp = new float[50], Rtmp = new float[50];
        float tmpLMax, tmpRMax;
        string LDB_S, RDB_S;
        private void timer2_Tick(object sender, EventArgs e)
        {
            //SetStyle(ControlStyles.SupportsTransparentBackColor, true);
            //toolTip1.BackColor = Color.Black; toolTip1.ForeColor = Color.White;
            Ltmp = new float[pictureBox1.Width != 0 ? pictureBox1.Width : 1]; Rtmp = new float[pictureBox2.Width!=0 ? pictureBox2.Width : 1];
            //Ltmp = new float[this.Width]; Rtmp = new float[this.Width];
            //timer2.Interval = 50;
            int FPS=frmFFTGraph.CalculateFrameRate();
            if (numchannels == 1)
            {
                if (GetData) { Data.channel.getWaveData(Ltmp, Ltmp.Length, 0); Data.channel.getWaveData(Rtmp, Rtmp.Length, 0); }
                else { Data.channel.getWaveData(Ltmp, Ltmp.Length, 0); Data.channel.getWaveData(Rtmp, Rtmp.Length, 0); }
                for (int i = 0; i < Ltmp.Length; i++) { if (tmpLMax < Ltmp[i]) { tmpRMax = tmpLMax = Ltmp[i]; } }
            }
            if (numchannels == 2)
            {
                if (GetData) { Data.system.getWaveData(Ltmp, Ltmp.Length, 0); Data.system.getWaveData(Rtmp, Rtmp.Length, 1); }
                else { Data.channel.getWaveData(Ltmp, Ltmp.Length, 0); Data.channel.getWaveData(Rtmp, Rtmp.Length, 1); }
                tmpLMax = Ltmp[0];//this.Width / 2]; 
                tmpRMax = Rtmp[0];//this.Width / 2];
                //for (int i = 1; i < Rtmp.Length; i++)
                //{
                    //if (tmpLMax > Ltmp[i]) { tmpLMax = Ltmp[i]; }
                    //if (tmpRMax > Rtmp[i]) { tmpRMax = Rtmp[i]; }
                //}
            }

            string a, b;
            label1.Text = (FPS*2).ToString("FPS: 00");
            float LDB = tmpLMax, RDB = tmpRMax;//0.0f,RDB=0.0f;

            if (LDB == 0) { LDB_S = "Inf."; }
            else if (Decibels(LDB, 0) < -365.5) { LDB_S = "-Inf."; }
            else { LDB_S = Decibels(LDB, 0).ToString("0.000"); }

            if (RDB == 0) { RDB_S = "Inf."; }
            else if (Decibels(RDB, 0) < -365.5) { RDB_S = "-Inf."; }
            else { RDB_S = Decibels(RDB, 0).ToString("0.000"); }
            
            /*
            if (Decibels(LDB, 0).ToString().IndexOf("-3.899998E-19") != -1 ||
                Decibels(LDB, 0).ToString().IndexOf("7.39567E-17") != -1 ||
                Decibels(LDB, 0).ToString().IndexOf("7.395671E-17") != -1 ||
                Decibels(LDB, 0).ToString().IndexOf("5.277585") != -1 ||
                Decibels(LDB, 0) == 0) { LDB = 0; }

            if (Decibels(RDB, 0).ToString().IndexOf("-3.899998E-19") != -1 ||
                Decibels(RDB, 0).ToString().IndexOf("7.39567E-17") != -1 ||
                Decibels(RDB, 0).ToString().IndexOf("7.395671E-17") != -1 ||
                Decibels(RDB, 0).ToString().IndexOf("5.277585") != -1 ||
                Decibels(RDB, 0) == 0) { RDB = 0; }
             */
            /*
            if (tmpLMax != 0.0f)
            {
                if (tmpLMax>0) { LDB = tmpLMax; }
                else
                {
                    //if (tmpLMax > 0.1 && tmpLMax < 1) { LDB = tmpLMax; }
                    for(int i=0;i<tmpLMax.ToString().Length;i++){string[] s = CSharpUtility.Utility.StringUtility.ConvertStringToArray(tmpLMax.ToString(),"");}
                    if (tmpLMax < 0.1) { if (false) { LDB = (tmpLMax - 1.0f) * 100; } else { LDB = (tmpLMax - 1.0f) * 10; } }
                    else { LDB = tmpLMax*10; }
                }
            }
            if (tmpRMax != 0.0f)
            {
                if (tmpRMax > 0) { RDB = tmpRMax; }
                else
                {
                    if (tmpRMax < 0.1) { if (tmpRMax < 0.01 && tmpRMax > -0.1) { RDB = (tmpRMax - 1.0f) * 100; } else { RDB = (tmpRMax - 1.0f) * 10; } }
                    else { RDB = tmpRMax * 10; }
                    //if (tmpRMax < 0) { RDB = (tmpRMax - 1.0f)*10; }
                    //else { RDB = (tmpRMax*10); }
                }
            }
             */
            this.Text = string.Format("{0}그래프 [좌: {1} dB / 우: {2} dB] (FPS: {3})",
                ToDraw == DrawSpectrum.FFTSpectrum ? "FFT-EQ " : "웨이브 데이터 ", LDB_S, RDB_S, (FPS * 2).ToString("00"));
        }

        public static double mag_sqrd(double re, double im) { return (re * re + im * im); }

        public static double Decibels(double re, double im) { return ((re == 0 && im == 0) ? (0) : 10.0 * Math.Log10(((mag_sqrd(re, im))))); }

        string AddString(string Text, int num)
        {
            StringBuilder sb = new StringBuilder();
        for (int i = 0; i < num; i++) { sb.Append(Text); } return sb.ToString();}

        private static int lastTick;
        private static int lastFrameRate;
        private static int frameRate;
        public static int CalculateFrameRate()
        {
            if (System.Environment.TickCount - lastTick >= 1000)
            {
                lastFrameRate = frameRate;
                frameRate = 0;
                lastTick = System.Environment.TickCount;
            }
            frameRate++;
            return lastFrameRate;
        }

        private void FFTGraph_SizeChanged(object sender, EventArgs e)
        {
            if (AutoWaveSize)
            {
                WavedataSize = this.Size.Width+5;
            }
            Ltmp = new float[this.Width];Rtmp = new float[this.Width];
            peak.peakMeterCtrl1_Resize(null, null); peak.peakMeterCtrl2_Resize(null, null);
        }

        private void FFTGraph_FormClosed(object sender, FormClosedEventArgs e)
        {
            try { f.Close(); }
            catch { }
        }

        FormBorderStyle a;
        Rectangle b;
        Point Loc;
        private void pictureBox1_DoubleClick(object sender, EventArgs e)
        {
            if (this.FormBorderStyle != System.Windows.Forms.FormBorderStyle.None)
            {
                a = this.FormBorderStyle; b = new Rectangle(this.Top, this.Left, this.Width, this.Height);
                this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                Loc = this.Location;
                Left = Top = 0;
                Width = Screen.PrimaryScreen.WorkingArea.Width;
                Height = Screen.PrimaryScreen.WorkingArea.Height;
            }
            else 
            {
                this.FormBorderStyle = a;
                this.Top = b.X; this.Left = b.Y; this.Width = b.Width; this.Height = b.Height;
                this.Location = new Point(Loc.X, Loc.Y);
            }
        }

        private void pictureBox1_SizeChanged(object sender, EventArgs e)
        {
        }


        private void timer3_Tick(object sender, EventArgs e)
        {

        }

        private void pictureBox2_SizeChanged(object sender, EventArgs e)
        {
        }

        private void 설정창띄우기ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            f.BringToFront();
            f.Show();
        }

        private void peak띄우기ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            peak.Show(); peak.BringToFront();
        }

        private void fFTToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (fFTToolStripMenuItem.Checked)
            {
                fFTToolStripMenuItem.Checked = false;
                웨이브데이터ToolStripMenuItem.Checked = true;
                ToDraw = DrawSpectrum.WaveData;
            }
            else
            {
                fFTToolStripMenuItem.Checked = true;
                웨이브데이터ToolStripMenuItem.Checked = false;
                ToDraw = DrawSpectrum.FFTSpectrum;
            }
        }

        private void 그래프ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (그래프ToolStripMenuItem.Checked)
            {
                그래프ToolStripMenuItem.Checked = false;
                파동ToolStripMenuItem.Checked = true;
                Shape = DrawShape.Wave;
            }
            else
            {
                그래프ToolStripMenuItem.Checked = true;
                파동ToolStripMenuItem.Checked = false;
                Shape = DrawShape.Equalizer;
            }
        }

        private void fMODSystemToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (fMODSystemToolStripMenuItem.Checked)
            {
                fMODSystemToolStripMenuItem.Checked = false;
                fMODChannelToolStripMenuItem.Checked = true;
                GetData = false;
            }
            else
            {
                fMODSystemToolStripMenuItem.Checked = true;
                fMODChannelToolStripMenuItem.Checked = false;
                GetData = true;
            }
        }

        private void toolStripComboBox1_TextChanged(object sender, EventArgs e)
        {
            
        }

        private void toolStripComboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

            toolTip1.Hide(toolStripComboBox1.Owner);
            string a=toolStripComboBox1.Items[toolStripComboBox1.SelectedIndex].ToString();
            this.FFTWindow = (FMOD.DSP_FFT_WINDOW)Enum.Parse(typeof(FMOD.DSP_FFT_WINDOW), a);
            switch (a)
            {
                case "RECT":
                    toolStripComboBox1.ToolTipText = "RECT:\r\nw[n] = 1.0";
                    toolTip1.SetToolTip(toolStripComboBox1.Owner,"RECT:\r\nw[n] = 1.0");break;
                case "TRIANGLE":
                    toolStripComboBox1.ToolTipText = "TRIANGLE:\r\nw[n] = TRI(2n/N)";
                    toolTip1.SetToolTip(toolStripComboBox1.Owner,"TRIANGLE:\r\nw[n] = TRI(2n/N)");break;
                case "HAMMING":
                    toolStripComboBox1.ToolTipText = "HAMMING:\r\nw[n] = 0.54 - (0.46 * COS(n/N) )";
                    toolTip1.SetToolTip(toolStripComboBox1.Owner,"HAMMING:\r\nw[n] = 0.54 - (0.46 * COS(n/N) )");break;
                case "HANNING":
                    toolStripComboBox1.ToolTipText = "HANNING:\r\nw[n] = 0.5 *  (1.0  - COS(n/N) )";
                    toolTip1.SetToolTip(toolStripComboBox1.Owner,"HANNING:\r\nw[n] = 0.5 *  (1.0  - COS(n/N) )"); break;
                case "BLACKMAN":
                    toolStripComboBox1.ToolTipText = "BLACKMAN:\r\nw[n] = 0.42 - (0.5  * COS(n/N) ) + (0.08 * COS(2.0 * n/N) )";
                    toolTip1.SetToolTip(toolStripComboBox1.Owner,"BLACKMAN:\r\nw[n] = 0.42 - (0.5  * COS(n/N) ) + (0.08 * COS(2.0 * n/N) )");break;
                case "BLACKMANHARRIS":
                    toolStripComboBox1.ToolTipText = "BLACKMANHARRIS:\r\nw[n] = 0.35875 - (0.48829 * COS(1.0 * n/N)) + (0.14128 * COS(2.0 * n/N)) - (0.01168 * COS(3.0 * n/N))";
                    toolTip1.SetToolTip(toolStripComboBox1.Owner,"BLACKMANHARRIS:\r\nw[n] = 0.35875 - (0.48829 * COS(1.0 * n/N)) + (0.14128 * COS(2.0 * n/N)) - (0.01168 * COS(3.0 * n/N))"); break;
                case "MAX":
                    toolStripComboBox1.ToolTipText = "MAX";
                    toolTip1.SetToolTip(toolStripComboBox1.Owner,"MAX"); break;
                default:
                    toolStripComboBox1.ToolTipText = "잘못된 값 입니다.";
                    toolTip1.SetToolTip(toolStripComboBox1.Owner,"잘못된 값 입니다."); break;
            }
        }

        Point Mouse;
        private void frmFFTGraph_MouseMove(object sender, MouseEventArgs e)
        {
            Mouse.X = e.X; Mouse.Y = e.Y;
        }

        private void toolStripTextBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == Convert.ToChar(Keys.Enter)) { toolStripTextBox1_TextChanged(null, null); }
            if (!(Char.IsDigit(e.KeyChar)) && e.KeyChar != Convert.ToChar(Keys.Back))
            { e.Handled = true; }
        }

        private void toolStripTextBox1_TextChanged(object sender, EventArgs e)
        {
            int a;
            if (toolStripTextBox1.Text == "") a = 1;
            if (int.Parse(toolStripTextBox1.Text)>0){ a = int.Parse(toolStripTextBox1.Text); }
            else a = 1;
            this.LineThick = a > 0 ? a : 1;
        }

        private void 똑같이나누기ToolStripMenuItem_CheckedChanged(object sender, EventArgs e)
        {
            this.LineTickLinkFFT = 똑같이나누기ToolStripMenuItem.Checked;
        }

        private void toolStripTextBox2_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == Convert.ToChar(Keys.Enter)) { toolStripTextBox1_TextChanged(null, null); }
            if (!(Char.IsDigit(e.KeyChar)) && e.KeyChar != Convert.ToChar(Keys.Back))
            { e.Handled = true; }
        }

        private void 맨위에표시ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.TopMost = 맨위에표시ToolStripMenuItem.Checked;
        }

        private void toolStripTextBox2_TextChanged(object sender, EventArgs e)
        {
            byte a1 = 0;
            if (toolStripTextBox2.Text == "") a1 = 1;
            if (int.Parse(toolStripTextBox2.Text) > 255 || int.Parse(toolStripTextBox2.Text) > 0)
            { try { a1 = byte.Parse(toolStripTextBox2.Text); } catch { a1 = 1; toolStripTextBox2.Text = "1"; } }
            else a1 = 1;
            this.FFTAverage = a1 > 0 ? a1 : (byte)1;
            toolStripTextBox2.Text = a1 > 0 ? a1.ToString() : "1";
        }

        private void toolTip1_Draw(object sender, DrawToolTipEventArgs e)
        {
            e.DrawBackground();
            e.DrawBorder();
            e.DrawText();
        }

        private void toolTip1_Popup(object sender, PopupEventArgs e)
        {
            //toolTip1.BackColor=Color.Black;
            //toolTip1.ForeColor = Color.White;
        }

        private void 배경투명처리ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!배경투명처리ToolStripMenuItem.Checked)
            {
                this.TransparencyKey = BackgroundColor; //분할자투명ToolStripMenuItem.Enabled = true;
                c = splitContainer1.BackColor; if (분할자투명ToolStripMenuItem.Checked) splitContainer1.BackColor = BackgroundColor;
                배경투명처리ToolStripMenuItem.Checked = true;
            }
            else
            {
                this.TransparencyKey = Color.Empty; //분할자투명ToolStripMenuItem.Enabled = false;
                splitContainer1.BackColor = c; 배경투명처리ToolStripMenuItem.Checked = false;
            }
            //분할자투명ToolStripMenuItem_CheckedChanged(null, null);
        }
        
        private void 배경투명처리ToolStripMenuItem_CheckedChanged(object sender, EventArgs e)
        {
            
        }

        private void 테두리표시ToolStripMenuItem_CheckedChanged(object sender, EventArgs e)
        {
            if (테두리표시ToolStripMenuItem.Checked)
            { this.pictureBox1.BorderStyle = this.pictureBox2.BorderStyle = BorderStyle.FixedSingle; }
            else { this.pictureBox1.BorderStyle = this.pictureBox2.BorderStyle = BorderStyle.None; }
        }

        private void toolStripComboBox1_MouseEnter(object sender, EventArgs e)
        {
            //toolTip1.Show("",toolStripComboBox1.Owner);
        }

        Color c;
        private void 분할자투명ToolStripMenuItem_CheckedChanged(object sender, EventArgs e)
        {
            if (분할자투명ToolStripMenuItem.Checked)
            { c = splitContainer1.BackColor; splitContainer1.BackColor = BackgroundColor; }
            else { splitContainer1.BackColor = c; }
        }

        private void splitContainer1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyValue == 115) { 배경투명처리ToolStripMenuItem_Click(null, null); }
        }

        private void toolStripComboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            try { this.FFTsize = (FFTSize)Enum.Parse(typeof(FFTSize), toolStripComboBox2.SelectedItem.ToString() == "None" ? "None" : "Size_" + toolStripComboBox2.SelectedItem.ToString()); }
            catch { MessageBox.Show("치트 같은걸로 값 임의로 변경하지마라!\n원래대로 되돌려놔라", "경고문", MessageBoxButtons.OK, MessageBoxIcon.Stop); }
        }
    }
}
