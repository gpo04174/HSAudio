﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using FMOD;

namespace FMOD_Helper
{
    public partial class frm3D : Form
    {
        internal FMODHelper fh=null;
        public frm3D(FMODHelper Value)
        {
            InitializeComponent();
            fh = Value;
        }
        protected override void WndProc(ref Message m)
        {
            if (m.Msg == 71/* && m.LParam.ToInt32() == 0x452e3c8*/)
            {
                if (this.joysticControl1 != null) 
                { joysticControl1.DrawDotLine(true); }
                if (this.joysticControl2 != null)
                { joysticControl2.DrawDotLine(true); }
            }

            base.WndProc(ref m);
        }
        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (chk3DMode.Checked)
            {
                fh.Play3DSound = true;
                groupBox2.Enabled = groupBox3.Enabled = true;
                lblFMODResult.Text = fh.channel.setMode(MODE._3D).ToString();
                //fdh.fh.channel.set3DMinMaxDistance(-1.0f, 1.0f);
                fh.HelperEx.FMOD3DAttributes = vc;// = fh.channel.set3DAttributes(ref vc, ref vc1).ToString();
                lblFMODResult.Text = fh.HelperEx.FMODResult.ToString();
                joysticControl1.Enabled = true;
                joysticControl1.DrawDotLine(false);
            }
            else
            {
                fh.Play3DSound = false;
                groupBox2.Enabled = groupBox3.Enabled = false;
                lblFMODResult.Text = fh.channel.setMode(MODE._2D).ToString();
                joysticControl1.Enabled = false;
                joysticControl1.DrawDotLine(false);
            }
            fh.system.update();
        }

        bool Change1=true, Change2=true;
        FMOD_Helper.Properties.FMOD3DAttributes vc;
        private void numericUpDown1_ValueChanged(object sender, EventArgs e)
        {
            vc.Pos.x = (float)numericUpDown1.Value;
            vc.Pos.y = (float)numericUpDown2.Value;
            vc.Pos.z = (float)numericUpDown3.Value;
            vc.Vel.x = (float)numericUpDown4.Value;
            vc.Vel.y = (float)numericUpDown5.Value;
            vc.Vel.z = (float)numericUpDown6.Value;

            //Change1 = Change2 = false;
            //joysticControl1.Value = new Point((int)(vc.x * 10), (int)(vc.y * 10));
            //joysticControl2.Value = new Point((int)vc1.x, (int)vc1.y);
            //Change1 = Change2 = true;

            fh.HelperEx.FMOD3DAttributes = vc;
            lblFMODResult.Text = fh.HelperEx.FMODResult.ToString();
            fh.system.update();
        }

        private void frm3D_Load(object sender, EventArgs e)
        {

        }

        public bool Closethis = false;
        private void frm3D_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = !Closethis;
            this.Hide();
        }
        
        private void joysticControl1_JoysticControlValueChanged(Point Location, PointF Value)
        {
            //joysticControl1.Maximum = new Point(100,100);
            label2.Text = "Value: " + XYTostring((int)Value.X, (int)Value.Y);
            label1.Text = "Pixel: " + XYTostring(Location.X, Location.Y);
            if (Change1)
            {
                numericUpDown1.Value = (decimal)Value.X * 0.1m;
                numericUpDown2.Value = (decimal)Value.Y * 0.1m;
            }
        }

        string XYTostring(int X, int Y)
        {
            return "{" + string.Format("X={0},Y={1}", X, Y) + "}";
        }
        string XYTostring(double X, double Y)
        {
            return "{" + string.Format("X={0},Y={1}", X.ToString("0.0"), Y.ToString("0.0")) + "}";
        }

        private void joysticControl1_DoubleClick(object sender, EventArgs e)
        {
            //joysticControl1.ResetDefault();
        }

        private void joysticControl2_JoysticControlValueChanged(Point Location, PointF Value)
        {
            label10.Text = "Pixel: " + XYTostring(Location.X, Location.Y);
            label9.Text = "Value: " + XYTostring((int)Value.X, (int)Value.Y);
            if (Change2)
            {
                numericUpDown4.Value = (decimal)Value.X;
                numericUpDown5.Value = (decimal)Value.Y;
            }
        }

        private void numericUpDown7_ValueChanged(object sender, EventArgs e)
        {
            //joysticControl1.Maximum=new Point((int)numericUpDown7.Value*10, (int)numericUpDown7.Value*10);
        }

        private void numericUpDown8_ValueChanged(object sender, EventArgs e)
        {
            //joysticControl2.Maximum = new Point((int)numericUpDown8.Value, (int)numericUpDown8.Value);
        }
    }
}
