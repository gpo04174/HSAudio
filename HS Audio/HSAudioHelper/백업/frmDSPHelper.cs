﻿using System;
using System.Windows.Forms;
using FMOD;

namespace FMOD_Helper
{
    public partial class frmDSPHelper : Form
    {
        internal FMODDSPHelper fdh;
        public frmDSPHelper(FMODDSPHelper Value)
        {
            InitializeComponent();
            fdh = Value;
            comboBox1.Text = "END_MS";
            fdh.fh.system.createDSPByType(DSP_TYPE.PARAMEQ, ref fdh.dsp);
        }
        
        private void DSPHelper_Load(object sender, EventArgs e)
        {//FMOD.DSP_TYPE.
            //d.showConfigDialog(this.Handle, true);

        }

        private void trackBar1_ValueChanged(object sender, EventArgs e)
        {
            FMOD.RESULT r= fdh.fh.channel.setDelay((DELAYTYPE)Enum.Parse(typeof(DELAYTYPE), comboBox1.Text), (uint)trackBar1.Value, (uint)trackBar2.Value);
        }

        private void comboBox1_TextChanged(object sender, EventArgs e)
        {
            fdh.fh.channel.setDelay((DELAYTYPE)Enum.Parse(typeof(DELAYTYPE), comboBox1.Text), (uint)trackBar1.Value, (uint)trackBar2.Value);
        }

        private void comboBox1_SelectedValueChanged(object sender, EventArgs e)
        {
            
        }
        void init() 
        { 

        }

        private void trackBar3_ValueChanged(object sender, EventArgs e)
        {

        }

        Enum DSPtype;
        private void comboBox3_SelectedValueChanged(object sender, EventArgs e)
        {
            comboBox3.Items.Clear();
            switch (comboBox3.Text)
            {
                case "OSCILLATOR":
                    foreach (string a in Enum.GetNames(typeof(FMOD.DSP_OSCILLATOR))){ comboBox3.Items.Add(a); }
                    lblFMODResult.Text=fdh.fh.system.createDSPByType(DSP_TYPE.MIXER, ref fdh.dsp).ToString();break;
                case "LOWPASS":
                    foreach (string a in Enum.GetNames(typeof(FMOD.DSP_LOWPASS))) { comboBox3.Items.Add(a); }
                    lblFMODResult.Text = fdh.fh.system.createDSPByType(DSP_TYPE.LOWPASS, ref fdh.dsp).ToString(); break;
                case "ITLOWPASS":
                    foreach (string a in Enum.GetNames(typeof(FMOD.DSP_ITLOWPASS))) { comboBox3.Items.Add(a); }
                    lblFMODResult.Text = fdh.fh.system.createDSPByType(DSP_TYPE.ITLOWPASS, ref fdh.dsp).ToString(); break;
                case "HIGHPASS":
                    foreach (string a in Enum.GetNames(typeof(FMOD.DSP_HIGHPASS))) { comboBox3.Items.Add(a); }
                    lblFMODResult.Text = fdh.fh.system.createDSPByType(DSP_TYPE.HIGHPASS, ref fdh.dsp).ToString(); break;
                case "ECHO":
                    foreach (string a in Enum.GetNames(typeof(FMOD.DSP_ECHO))) { comboBox3.Items.Add(a); }
                    lblFMODResult.Text = fdh.fh.system.createDSPByType(DSP_TYPE.ECHO, ref fdh.dsp).ToString(); break;
                case "FLANGE":
                    foreach (string a in Enum.GetNames(typeof(FMOD.DSP_FLANGE))) { comboBox3.Items.Add(a); }
                    lblFMODResult.Text = fdh.fh.system.createDSPByType(DSP_TYPE.FLANGE, ref fdh.dsp).ToString(); break;
                case "DISTORTION":
                    foreach (string a in Enum.GetNames(typeof(FMOD.DSP_DISTORTION))) { comboBox3.Items.Add(a); }
                    lblFMODResult.Text = fdh.fh.system.createDSPByType(DSP_TYPE.DISTORTION, ref fdh.dsp).ToString(); break;
                case "NORMALIZE":
                    foreach (string a in Enum.GetNames(typeof(FMOD.DSP_NORMALIZE))) { comboBox3.Items.Add(a); }
                    lblFMODResult.Text = fdh.fh.system.createDSPByType(DSP_TYPE.NORMALIZE, ref fdh.dsp).ToString(); break;
                case "PARAMEQ":
                    foreach (string a in Enum.GetNames(typeof(FMOD.DSP_PARAMEQ))) { comboBox3.Items.Add(a); }
                    lblFMODResult.Text = fdh.fh.system.createDSPByType(DSP_TYPE.PARAMEQ, ref fdh.dsp).ToString(); break;
                case "PITCHSHIFT":
                    foreach (string a in Enum.GetNames(typeof(FMOD.DSP_PITCHSHIFT))) { comboBox3.Items.Add(a); }
                    lblFMODResult.Text = fdh.fh.system.createDSPByType(DSP_TYPE.PITCHSHIFT, ref fdh.dsp).ToString(); break;
                case "CHORUS":
                    foreach (string a in Enum.GetNames(typeof(FMOD.DSP_CHORUS))) { comboBox3.Items.Add(a); }
                    lblFMODResult.Text = fdh.fh.system.createDSPByType(DSP_TYPE.CHORUS, ref fdh.dsp).ToString(); break;
                case "ITECHO":
                    foreach (string a in Enum.GetNames(typeof(FMOD.DSP_ITECHO))) { comboBox3.Items.Add(a); }
                    lblFMODResult.Text = fdh.fh.system.createDSPByType(DSP_TYPE.ITECHO, ref fdh.dsp).ToString(); break;
                case "COMPRESSOR":
                    foreach (string a in Enum.GetNames(typeof(FMOD.DSP_COMPRESSOR))) { comboBox3.Items.Add(a); }
                    lblFMODResult.Text = fdh.fh.system.createDSPByType(DSP_TYPE.COMPRESSOR, ref fdh.dsp).ToString(); break;
                case "SFXREVERB":
                    foreach (string a in Enum.GetNames(typeof(FMOD.DSP_SFXREVERB))) { comboBox3.Items.Add(a); }
                    lblFMODResult.Text = fdh.fh.system.createDSPByType(DSP_TYPE.SFXREVERB, ref fdh.dsp).ToString(); break;
                case "LOWPASS_SIMPLE":
                    foreach (string a in Enum.GetNames(typeof(FMOD.DSP_LOWPASS_SIMPLE))) { comboBox3.Items.Add(a); }
                    lblFMODResult.Text = fdh.fh.system.createDSPByType(DSP_TYPE.LOWPASS_SIMPLE, ref fdh.dsp).ToString(); break;
                case "DELAY":
                    foreach (string a in Enum.GetNames(typeof(FMOD.DSP_DELAY))) { comboBox3.Items.Add(a); }
                    lblFMODResult.Text = fdh.fh.system.createDSPByType(DSP_TYPE.DELAY, ref fdh.dsp).ToString(); break;
                case "TREMOLO":
                    foreach (string a in Enum.GetNames(typeof(FMOD.DSP_TREMOLO))) { comboBox3.Items.Add(a); }
                    lblFMODResult.Text = fdh.fh.system.createDSPByType(DSP_TYPE.TREMOLO, ref fdh.dsp).ToString(); break;

                case "MIXER":
                    lblFMODResult.Text = fdh.fh.system.createDSPByType(DSP_TYPE.MIXER, ref fdh.dsp).ToString(); break;
                case "LADSPAPLUGIN":
                    lblFMODResult.Text = fdh.fh.system.createDSPByType(DSP_TYPE.LADSPAPLUGIN, ref fdh.dsp).ToString(); break;
                case "VSTPLUGIN":
                    lblFMODResult.Text = fdh.fh.system.createDSPByType(DSP_TYPE.VSTPLUGIN, ref fdh.dsp).ToString(); break;
                case "WINAMPPLUGIN":
                    lblFMODResult.Text = fdh.fh.system.createDSPByType(DSP_TYPE.WINAMPPLUGIN, ref fdh.dsp).ToString(); break;

                default: lblFMODResult.Text = fdh.fh.system.createDSPByType(DSP_TYPE.UNKNOWN, ref fdh.dsp).ToString(); break;
            }
           
        }

        private void trackBar3_Scroll(object sender, EventArgs e)
        {

        }

        private void numericUpDown7_ValueChanged(object sender, EventArgs e)
        {
            //lblFMODResult.Text = fdh.dsp.setParameter((int)Enum.Parse(typeof(FMOD), 10f).ToString();
            //fdh.fh.system.
            //fdh.fh.system.createDSPByType(DSP_TYPE.ECHO, ref fdh.dsp);
        }
    }
}
