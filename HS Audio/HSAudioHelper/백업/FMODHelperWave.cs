﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;
using System.IO;
using System.Threading;

namespace FMOD_Helper.Wave
{
    public class SaveWave:MemoryStream
    {
        public FMOD_Helper.SoftwareFormat Format { get; internal set; }
        internal FMODHelper Helper;
        Thread th;
        ThreadStart thstart;
        uint Length1, datalength;

        uint lastrecordpos, recordpos;

        public FMOD.DSP_READCALLBACK dspreadcallback;
        internal FMOD.DSP dsp_static;
        internal FMOD.DSP_DESCRIPTION dspdesc;

        private DSP.DSPCallBack cb = new DSP.DSPCallBack();
        public unsafe delegate void READCALLBACKOutBufferEventHandler(float* outbuffer, FMOD_Helper.DSP.DSPCallBack Value);
        public event READCALLBACKOutBufferEventHandler READCALLBACKOutBuffer;

        public FMOD.RESULT READCALLBACK(ref FMOD.DSP_STATE dsp_state, IntPtr inbuf, IntPtr outbuf, uint length, int inchannels, int outchannels)
        {
            uint count = 0;
            int count2 = 0;
            IntPtr thisdspraw = dsp_state.instance;
            if (dsp_static != null) { dsp_static.setRaw(thisdspraw); }
            else { dsp_static = new FMOD.DSP(); dsp_static.setRaw(thisdspraw); }
            cb.dsp_state = dsp_state; cb.inbuf = inbuf; cb.inchannels = inchannels; cb.length = length; cb.outbuf = outbuf; cb.outchannels = outchannels;
            unsafe
            {
                float* inbuffer = (float*)inbuf.ToPointer();
                float* outbuffer = (float*)outbuf.ToPointer();

                for (count = 0; count < length; count++)
                {
                    for (count2 = 0; count2 < outchannels; count2++)
                    {
                        outbuffer[(count * outchannels) + count2] = inbuffer[(count * inchannels) + count2];
                    }
                }
                if (READCALLBACKOutBuffer != null) READCALLBACKOutBuffer(outbuffer, cb);
            }

            return FMOD.RESULT.OK;
        }
        public FMOD.RESULT READCALLBACK(ref FMOD.DSP_STATE dsp_state, FMOD_Helper.DSP.DSPCallBack Value)
        {
            uint count = 0;
            int count2 = 0;
            IntPtr thisdspraw = dsp_state.instance;
            if (dsp_static != null) { dsp_static.setRaw(thisdspraw); }
            else { dsp_static = new FMOD.DSP(); dsp_static.setRaw(thisdspraw); }

            unsafe
            {
                float* inbuffer = (float*)Value.inbuf.ToPointer();
                float* outbuffer = (float*)Value.outbuf.ToPointer();

                for (count = 0; count < Value.length; count++)
                {
                    for (count2 = 0; count2 < Value.outchannels; count2++)
                    {
                        outbuffer[(count * Value.outchannels) + count2] = inbuffer[(count * Value.inchannels) + count2];
                    }
                }
            }

            return FMOD.RESULT.OK;
        }

        public SaveWave() { PUBLIC(null); }
        public SaveWave(FMODHelper Helper)
        { PUBLIC(Helper); Format = Helper.CurrentSoftwareFormat; WriteWavHeader(0); }
        public SaveWave(FMODHelper Helper, FmtChunk fmtChunk, DataChunk dataChunk, WavHeader wavHeader, RiffChunk riffChunk)
        { PUBLIC(Helper); Format = Helper.CurrentSoftwareFormat; WriteWavHeader(0, fmtChunk, dataChunk, wavHeader, riffChunk); }
        internal SaveWave(FMODHelper Helper, FMOD_Helper.SoftwareFormat Format)
        {PUBLIC(Helper); this.Format = Format; }
        internal SaveWave(FMODHelper Helper, FMOD_Helper.SoftwareFormat Format, FmtChunk fmtChunk, DataChunk dataChunk, WavHeader wavHeader, RiffChunk riffChunk)
        {PUBLIC(Helper); WriteWavHeader(0, fmtChunk, dataChunk, wavHeader, riffChunk);this.Format = Format;}
        void PUBLIC(FMODHelper Helper)
        {
            if(Helper!=null)this.Helper = Helper;
            thstart = new ThreadStart(WriteWave);
            th = new Thread(thstart) { Name = "웨이브 데이터 쓰기 스레드" };
            this.Format = Helper.CurrentSoftwareFormat;
            if (Helper != null) Length1 = Helper.GetTotalTick(FMOD.TIMEUNIT.PCMBYTES);
            Helper.dsp.dspreadcallback += new FMOD.DSP_READCALLBACK(READCALLBACK);
            unsafe { READCALLBACKOutBuffer = new READCALLBACKOutBufferEventHandler(WriteWave1); }
        }

        public void StartWrite(){try { th.Start(); }catch { } }
        public void PauseWrite(){th.Suspend();}

        public Stream BaseStream { get { return this; } }

        /// <summary>
        /// 이 함수를 호출하게 되면 웨이브가 씌여지고 기존의 웨이브 스트림에는 더이상 쓸 수 없게됩니다. (Dispose와 같음)
        /// </summary>
        public Stream StopAndDispose()
        {
            th.Abort();
            MemoryStream s = new MemoryStream(this.GetBuffer());
            WriteWavHeader(datalength); this.Close(); return s;
        }

        /// <summary>
        /// 현재 웨이브 데이터를 반환합니다.
        /// </summary>
        /// <param name="Close">True면 데이터 헤더가 씌여진 형태이고 False면 데이터 헤더가 씌여지지 않은채로 반환합니다.</param>
        /// <returns></returns>
        public byte[] Data(/*bool Close*/)
        {
            //if(Close)WriteWavHeader(datalength);
            return this.GetBuffer();
        }

        public void WriteHeader() { WriteWavHeader(datalength); }

        private void WriteWave()
        {
            recordpos = Helper.GetCurrentTick(FMOD.TIMEUNIT.RAWBYTES);
            if (recordpos != lastrecordpos)
            {
                IntPtr ptr1 = IntPtr.Zero, ptr2 = IntPtr.Zero;
                uint blocklength;
                uint len1 = 0, len2 = 0;

                blocklength = recordpos - lastrecordpos;
                if (blocklength < 0)
                {blocklength += Length1;}

                /*
                    Lock the sound to get access to the raw data.
                */
                result = Helper.sound.@lock(lastrecordpos * 4, blocklength * 4, ref ptr1, ref ptr2, ref len1, ref len2); /* *4 = stereo 16bit.  1 sample = 4 bytes. */

                /*
                    Write it to disk.
                */
                if (ptr1 != IntPtr.Zero && len1 > 0)
                {
                    byte[] buf = new byte[len1];

                    Marshal.Copy(ptr1, buf, 0, (int)len1);

                    datalength += len1;

                    Write(buf, 0, (int)len1);

                }
                if (ptr2 != IntPtr.Zero && len2 > 0)
                {
                    byte[] buf = new byte[len2];

                    Marshal.Copy(ptr2, buf, 0, (int)len2);

                    datalength += len2;

                    Write(buf, 0, (int)len2);
                }

                /*
                    Unlock the sound to allow FMOD to use it again.
                */
              result = Helper.sound.unlock(ptr1, ptr1, len1, len2);
            }

            lastrecordpos = recordpos;
            if (Helper.system != null){Helper.system.update(); }
        }

        private unsafe void WriteWave1(float* outbuf, FMOD_Helper.DSP.DSPCallBack Value)
        {

        }

        FMOD.RESULT result;
        public MemoryStream WriteWaveNew(SaveWave stream)
        {
            MemoryStream ms = new MemoryStream(this.GetBuffer());
            FMOD.SOUND_TYPE type = FMOD.SOUND_TYPE.UNKNOWN;
            FMOD.SOUND_FORMAT format = FMOD.SOUND_FORMAT.NONE;
            int channels = 0, bits = 0, temp1 = 0;
            float rate = 0.0f;
            float temp = 0.0f;

            if (Helper.sound == null) { return null; }

            Helper.sound.getFormat(ref type, ref format, ref channels, ref bits);
            Helper.sound.getDefaults(ref rate, ref temp, ref temp, ref temp1);

            ms.Seek(0, SeekOrigin.Begin);

            FmtChunk fmtChunk = new FmtChunk();
            DataChunk dataChunk = new DataChunk();
            WavHeader wavHeader = new WavHeader();
            RiffChunk riffChunk = new RiffChunk();

            fmtChunk.chunk = new RiffChunk();
            fmtChunk.chunk.id = new char[4];
            fmtChunk.chunk.id[0] = 'f';
            fmtChunk.chunk.id[1] = 'm';
            fmtChunk.chunk.id[2] = 't';
            fmtChunk.chunk.id[3] = ' ';
            fmtChunk.chunk.size = Marshal.SizeOf(fmtChunk) - Marshal.SizeOf(riffChunk);
            fmtChunk.wFormatTag = 1;
            fmtChunk.nChannels = (ushort)channels;
            fmtChunk.nSamplesPerSec = (uint)rate;
            fmtChunk.nAvgBytesPerSec = (uint)(rate * channels * bits / 8);
            fmtChunk.nBlockAlign = (ushort)(1 * channels * bits / 8);
            fmtChunk.wBitsPerSample = (ushort)bits;

            dataChunk.chunk = new RiffChunk();
            dataChunk.chunk.id = new char[4];
            dataChunk.chunk.id[0] = 'd';
            dataChunk.chunk.id[1] = 'a';
            dataChunk.chunk.id[2] = 't';
            dataChunk.chunk.id[3] = 'a';
            dataChunk.chunk.size = stream.datalength ;

            wavHeader.chunk = new RiffChunk();
            wavHeader.chunk.id = new char[4];
            wavHeader.chunk.id[0] = 'R';
            wavHeader.chunk.id[1] = 'I';
            wavHeader.chunk.id[2] = 'F';
            wavHeader.chunk.id[3] = 'F';
            wavHeader.chunk.size = Marshal.SizeOf(fmtChunk) + Marshal.SizeOf(riffChunk) + datalength;
            wavHeader.rifftype = new char[4];
            wavHeader.rifftype[0] = 'W';
            wavHeader.rifftype[1] = 'A';
            wavHeader.rifftype[2] = 'V';
            wavHeader.rifftype[3] = 'E';

            /*
                Write out the WAV header.
            */
            IntPtr wavHeaderPtr = Marshal.AllocHGlobal(Marshal.SizeOf(wavHeader));
            IntPtr fmtChunkPtr = Marshal.AllocHGlobal(Marshal.SizeOf(fmtChunk));
            IntPtr dataChunkPtr = Marshal.AllocHGlobal(Marshal.SizeOf(dataChunk));
            byte[] wavHeaderBytes = new byte[Marshal.SizeOf(wavHeader)];
            byte[] fmtChunkBytes = new byte[Marshal.SizeOf(fmtChunk)];
            byte[] dataChunkBytes = new byte[Marshal.SizeOf(dataChunk)];

            Marshal.StructureToPtr(wavHeader, wavHeaderPtr, false);
            Marshal.Copy(wavHeaderPtr, wavHeaderBytes, 0, Marshal.SizeOf(wavHeader));

            Marshal.StructureToPtr(fmtChunk, fmtChunkPtr, false);
            Marshal.Copy(fmtChunkPtr, fmtChunkBytes, 0, Marshal.SizeOf(fmtChunk));

            Marshal.StructureToPtr(dataChunk, dataChunkPtr, false);
            Marshal.Copy(dataChunkPtr, dataChunkBytes, 0, Marshal.SizeOf(dataChunk));

            ms.Write(wavHeaderBytes, 0, Marshal.SizeOf(wavHeader));
            ms.Write(fmtChunkBytes, 0, Marshal.SizeOf(fmtChunk));
            ms.Write(dataChunkBytes, 0, Marshal.SizeOf(dataChunk));
            return ms;
        }
        private void WriteWavHeader(long length)
        {
            FMOD.SOUND_TYPE type = FMOD.SOUND_TYPE.UNKNOWN;
            FMOD.SOUND_FORMAT format = FMOD.SOUND_FORMAT.NONE;
            int channels = 0, bits = 0, temp1 = 0;
            float rate = 0.0f;
            float temp = 0.0f;

            if (Helper.sound == null) { return; }

            Helper.sound.getFormat(ref type, ref format, ref channels, ref bits);
            Helper.sound.getDefaults(ref rate, ref temp, ref temp, ref temp1);

            Seek(0, SeekOrigin.Begin);

            FmtChunk fmtChunk = new FmtChunk();
            DataChunk dataChunk = new DataChunk();
            WavHeader wavHeader = new WavHeader();
            RiffChunk riffChunk = new RiffChunk();

            fmtChunk.chunk = new RiffChunk();
            fmtChunk.chunk.id = new char[4];
            fmtChunk.chunk.id[0] = 'f';
            fmtChunk.chunk.id[1] = 'm';
            fmtChunk.chunk.id[2] = 't';
            fmtChunk.chunk.id[3] = ' ';
            fmtChunk.chunk.size = Marshal.SizeOf(fmtChunk) - Marshal.SizeOf(riffChunk);
            fmtChunk.wFormatTag = 1;
            fmtChunk.nChannels = (ushort)channels;
            fmtChunk.nSamplesPerSec = (uint)rate;
            fmtChunk.nAvgBytesPerSec = (uint)(rate * channels * bits / 8);
            fmtChunk.nBlockAlign = (ushort)(1 * channels * bits / 8);
            fmtChunk.wBitsPerSample = (ushort)bits;

            dataChunk.chunk = new RiffChunk();
            dataChunk.chunk.id = new char[4];
            dataChunk.chunk.id[0] = 'd';
            dataChunk.chunk.id[1] = 'a';
            dataChunk.chunk.id[2] = 't';
            dataChunk.chunk.id[3] = 'a';
            dataChunk.chunk.size = length;

            wavHeader.chunk = new RiffChunk();
            wavHeader.chunk.id = new char[4];
            wavHeader.chunk.id[0] = 'R';
            wavHeader.chunk.id[1] = 'I';
            wavHeader.chunk.id[2] = 'F';
            wavHeader.chunk.id[3] = 'F';
            wavHeader.chunk.size = Marshal.SizeOf(fmtChunk) + Marshal.SizeOf(riffChunk) + length;
            wavHeader.rifftype = new char[4];
            wavHeader.rifftype[0] = 'W';
            wavHeader.rifftype[1] = 'A';
            wavHeader.rifftype[2] = 'V';
            wavHeader.rifftype[3] = 'E';

            /*
                Write out the WAV header.
            */
            IntPtr wavHeaderPtr = Marshal.AllocHGlobal(Marshal.SizeOf(wavHeader));
            IntPtr fmtChunkPtr = Marshal.AllocHGlobal(Marshal.SizeOf(fmtChunk));
            IntPtr dataChunkPtr = Marshal.AllocHGlobal(Marshal.SizeOf(dataChunk));
            byte[] wavHeaderBytes = new byte[Marshal.SizeOf(wavHeader)];
            byte[] fmtChunkBytes = new byte[Marshal.SizeOf(fmtChunk)];
            byte[] dataChunkBytes = new byte[Marshal.SizeOf(dataChunk)];

            Marshal.StructureToPtr(wavHeader, wavHeaderPtr, false);
            Marshal.Copy(wavHeaderPtr, wavHeaderBytes, 0, Marshal.SizeOf(wavHeader));

            Marshal.StructureToPtr(fmtChunk, fmtChunkPtr, false);
            Marshal.Copy(fmtChunkPtr, fmtChunkBytes, 0, Marshal.SizeOf(fmtChunk));

            Marshal.StructureToPtr(dataChunk, dataChunkPtr, false);
            Marshal.Copy(dataChunkPtr, dataChunkBytes, 0, Marshal.SizeOf(dataChunk));

            Write(wavHeaderBytes, 0, Marshal.SizeOf(wavHeader));
            Write(fmtChunkBytes, 0, Marshal.SizeOf(fmtChunk));
            Write(dataChunkBytes, 0, Marshal.SizeOf(dataChunk));
        }
        private void WriteWavHeader(long length, FmtChunk fmtChunk, DataChunk dataChunk, WavHeader wavHeader, RiffChunk riffChunk)
        {
            FMOD.SOUND_TYPE type = FMOD.SOUND_TYPE.UNKNOWN;
            FMOD.SOUND_FORMAT format = FMOD.SOUND_FORMAT.NONE;
            int channels = 0, bits = 0, temp1 = 0;
            float rate = 0.0f;
            float temp = 0.0f;

            if (Helper.sound == null) { return; }

            Helper.sound.getFormat(ref type, ref format, ref channels, ref bits);
            Helper.sound.getDefaults(ref rate, ref temp, ref temp, ref temp1);

            Seek(0, SeekOrigin.Begin);

            /*
                Write out the WAV header.
            */
            IntPtr wavHeaderPtr = Marshal.AllocHGlobal(Marshal.SizeOf(wavHeader));
            IntPtr fmtChunkPtr = Marshal.AllocHGlobal(Marshal.SizeOf(fmtChunk));
            IntPtr dataChunkPtr = Marshal.AllocHGlobal(Marshal.SizeOf(dataChunk));
            byte[] wavHeaderBytes = new byte[Marshal.SizeOf(wavHeader)];
            byte[] fmtChunkBytes = new byte[Marshal.SizeOf(fmtChunk)];
            byte[] dataChunkBytes = new byte[Marshal.SizeOf(dataChunk)];

            Marshal.StructureToPtr(wavHeader, wavHeaderPtr, false);
            Marshal.Copy(wavHeaderPtr, wavHeaderBytes, 0, Marshal.SizeOf(wavHeader));

            Marshal.StructureToPtr(fmtChunk, fmtChunkPtr, false);
            Marshal.Copy(fmtChunkPtr, fmtChunkBytes, 0, Marshal.SizeOf(fmtChunk));

            Marshal.StructureToPtr(dataChunk, dataChunkPtr, false);
            Marshal.Copy(dataChunkPtr, dataChunkBytes, 0, Marshal.SizeOf(dataChunk));

            Write(wavHeaderBytes, 0, Marshal.SizeOf(wavHeader));
            Write(fmtChunkBytes, 0, Marshal.SizeOf(fmtChunk));
            Write(dataChunkBytes, 0, Marshal.SizeOf(dataChunk));
        }

        /*
        public static Stream WriteWavHeader(FMODHelper Helper)
        {
            MemoryStream ms = new MemoryStream();
            uint length = Helper.GetCurrentTick(FMOD.TIMEUNIT.RAWBYTES);
            FMOD.SOUND_TYPE type = FMOD.SOUND_TYPE.UNKNOWN;
            FMOD.SOUND_FORMAT format = FMOD.SOUND_FORMAT.NONE;
            int channels = 0, bits = 0, temp1 = 0;
            float rate = 0.0f;
            float temp = 0.0f;

            if (Helper.sound == null) { return null; }

            Helper.sound.getFormat(ref type, ref format, ref channels, ref bits);
            Helper.sound.getDefaults(ref rate, ref temp, ref temp, ref temp1);

            ms.Seek(0, SeekOrigin.Begin);

            FmtChunk fmtChunk = new FmtChunk();
            DataChunk dataChunk = new DataChunk();
            WavHeader wavHeader = new WavHeader();
            RiffChunk riffChunk = new RiffChunk();

            fmtChunk.chunk = new RiffChunk();
            fmtChunk.chunk.id = new char[4];
            fmtChunk.chunk.id[0] = 'f';
            fmtChunk.chunk.id[1] = 'm';
            fmtChunk.chunk.id[2] = 't';
            fmtChunk.chunk.id[3] = ' ';
            fmtChunk.chunk.size = Marshal.SizeOf(fmtChunk) - Marshal.SizeOf(riffChunk);
            fmtChunk.wFormatTag = 1;
            fmtChunk.nChannels = (ushort)channels;
            fmtChunk.nSamplesPerSec = (uint)rate;
            fmtChunk.nAvgBytesPerSec = (uint)(rate * channels * bits / 8);
            fmtChunk.nBlockAlign = (ushort)(1 * channels * bits / 8);
            fmtChunk.wBitsPerSample = (ushort)bits;

            dataChunk.chunk = new RiffChunk();
            dataChunk.chunk.id = new char[4];
            dataChunk.chunk.id[0] = 'd';
            dataChunk.chunk.id[1] = 'a';
            dataChunk.chunk.id[2] = 't';
            dataChunk.chunk.id[3] = 'a';
            dataChunk.chunk.size = (int)length;

            wavHeader.chunk = new RiffChunk();
            wavHeader.chunk.id = new char[4];
            wavHeader.chunk.id[0] = 'R';
            wavHeader.chunk.id[1] = 'I';
            wavHeader.chunk.id[2] = 'F';
            wavHeader.chunk.id[3] = 'F';
            wavHeader.chunk.size = (int)(Marshal.SizeOf(fmtChunk) + Marshal.SizeOf(riffChunk) + length);
            wavHeader.rifftype = new char[4];
            wavHeader.rifftype[0] = 'W';
            wavHeader.rifftype[1] = 'A';
            wavHeader.rifftype[2] = 'V';
            wavHeader.rifftype[3] = 'E';

            //Write out the WAV header.
         
            IntPtr wavHeaderPtr = Marshal.AllocHGlobal(Marshal.SizeOf(wavHeader));
            IntPtr fmtChunkPtr = Marshal.AllocHGlobal(Marshal.SizeOf(fmtChunk));
            IntPtr dataChunkPtr = Marshal.AllocHGlobal(Marshal.SizeOf(dataChunk));
            byte[] wavHeaderBytes = new byte[Marshal.SizeOf(wavHeader)];
            byte[] fmtChunkBytes = new byte[Marshal.SizeOf(fmtChunk)];
            byte[] dataChunkBytes = new byte[Marshal.SizeOf(dataChunk)];

            Marshal.StructureToPtr(wavHeader, wavHeaderPtr, false);
            Marshal.Copy(wavHeaderPtr, wavHeaderBytes, 0, Marshal.SizeOf(wavHeader));

            Marshal.StructureToPtr(fmtChunk, fmtChunkPtr, false);
            Marshal.Copy(fmtChunkPtr, fmtChunkBytes, 0, Marshal.SizeOf(fmtChunk));

            Marshal.StructureToPtr(dataChunk, dataChunkPtr, false);
            Marshal.Copy(dataChunkPtr, dataChunkBytes, 0, Marshal.SizeOf(dataChunk));

            ms.Write(wavHeaderBytes, 0, Marshal.SizeOf(wavHeader));
            ms.Write(fmtChunkBytes, 0, Marshal.SizeOf(fmtChunk));
            ms.Write(dataChunkBytes, 0, Marshal.SizeOf(dataChunk));
            return ms;
        }*/
        
        /*
        public static Stream WriteWavHeader(FMODHelper Helper, int Length)
        {
            MemoryStream ms = new MemoryStream();
            uint length = Helper.GetCurrentTick(FMOD.TIMEUNIT.RAWBYTES);
            FMOD.SOUND_TYPE type = FMOD.SOUND_TYPE.UNKNOWN;
            FMOD.SOUND_FORMAT format = FMOD.SOUND_FORMAT.NONE;
            int channels = 0, bits = 0, temp1 = 0;
            float rate = 0.0f;
            float temp = 0.0f;

            if (Helper.sound == null) { return null; }

            Helper.sound.getFormat(ref type, ref format, ref channels, ref bits);
            Helper.sound.getDefaults(ref rate, ref temp, ref temp, ref temp1);

            ms.Seek(0, SeekOrigin.Begin);

            FmtChunk fmtChunk = new FmtChunk();
            DataChunk dataChunk = new DataChunk();
            WavHeader wavHeader = new WavHeader();
            RiffChunk riffChunk = new RiffChunk();

            fmtChunk.chunk = new RiffChunk();
            fmtChunk.chunk.id = new char[4];
            fmtChunk.chunk.id[0] = 'f';
            fmtChunk.chunk.id[1] = 'm';
            fmtChunk.chunk.id[2] = 't';
            fmtChunk.chunk.id[3] = ' ';
            fmtChunk.chunk.size = Marshal.SizeOf(fmtChunk) - Marshal.SizeOf(riffChunk);
            fmtChunk.wFormatTag = 1;
            fmtChunk.nChannels = (ushort)channels;
            fmtChunk.nSamplesPerSec = (uint)rate;
            fmtChunk.nAvgBytesPerSec = (uint)(rate * channels * bits / 8);
            fmtChunk.nBlockAlign = (ushort)(1 * channels * bits / 8);
            fmtChunk.wBitsPerSample = (ushort)bits;

            dataChunk.chunk = new RiffChunk();
            dataChunk.chunk.id = new char[4];
            dataChunk.chunk.id[0] = 'd';
            dataChunk.chunk.id[1] = 'a';
            dataChunk.chunk.id[2] = 't';
            dataChunk.chunk.id[3] = 'a';
            dataChunk.chunk.size = (int)length;

            wavHeader.chunk = new RiffChunk();
            wavHeader.chunk.id = new char[4];
            wavHeader.chunk.id[0] = 'R';
            wavHeader.chunk.id[1] = 'I';
            wavHeader.chunk.id[2] = 'F';
            wavHeader.chunk.id[3] = 'F';
            wavHeader.chunk.size = (int)(Marshal.SizeOf(fmtChunk) + Marshal.SizeOf(riffChunk) + length);
            wavHeader.rifftype = new char[4];
            wavHeader.rifftype[0] = 'W';
            wavHeader.rifftype[1] = 'A';
            wavHeader.rifftype[2] = 'V';
            wavHeader.rifftype[3] = 'E';

            //Write out the WAV header.
         
            IntPtr wavHeaderPtr = Marshal.AllocHGlobal(Marshal.SizeOf(wavHeader));
            IntPtr fmtChunkPtr = Marshal.AllocHGlobal(Marshal.SizeOf(fmtChunk));
            IntPtr dataChunkPtr = Marshal.AllocHGlobal(Marshal.SizeOf(dataChunk));
            byte[] wavHeaderBytes = new byte[Marshal.SizeOf(wavHeader)];
            byte[] fmtChunkBytes = new byte[Marshal.SizeOf(fmtChunk)];
            byte[] dataChunkBytes = new byte[Marshal.SizeOf(dataChunk)];

            Marshal.StructureToPtr(wavHeader, wavHeaderPtr, false);
            Marshal.Copy(wavHeaderPtr, wavHeaderBytes, 0, Marshal.SizeOf(wavHeader));

            Marshal.StructureToPtr(fmtChunk, fmtChunkPtr, false);
            Marshal.Copy(fmtChunkPtr, fmtChunkBytes, 0, Marshal.SizeOf(fmtChunk));

            Marshal.StructureToPtr(dataChunk, dataChunkPtr, false);
            Marshal.Copy(dataChunkPtr, dataChunkBytes, 0, Marshal.SizeOf(dataChunk));

            ms.Write(wavHeaderBytes, 0, Marshal.SizeOf(wavHeader));
            ms.Write(fmtChunkBytes, 0, Marshal.SizeOf(fmtChunk));
            ms.Write(dataChunkBytes, 0, Marshal.SizeOf(dataChunk));
            return ms;
        }*/
    }

    /*
 WAV Structures 
*/
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct RiffChunk
    {
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 4)]
        public char[] id;
        public long size;
    }

    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct FmtChunk
    {
        public RiffChunk chunk;
        public ushort wFormatTag;    /* format type  */
        public ushort nChannels;    /* number of channels (i.e. mono, stereo...)  */
        public uint nSamplesPerSec;    /* sample rate  */
        public uint nAvgBytesPerSec;    /* for buffer estimation  */
        public ushort nBlockAlign;    /* block size of data  */
        public ushort wBitsPerSample;    /* number of bits per sample of mono data */
    }

    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct DataChunk
    {
        public RiffChunk chunk;
    }

    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct WavHeader
    {
        public RiffChunk chunk;
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 4)]
        public char[] rifftype;
    }
}
