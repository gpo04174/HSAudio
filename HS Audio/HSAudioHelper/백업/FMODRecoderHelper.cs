﻿using System;
using System.Collections.Generic;

using System.Text;
using FMOD_Helper.DSP;

namespace FMOD_Helper
{
    public class FMODRecoderHelper : FMODHelper
    {
        public FMODRecoderHelper(SoftwareFormat SoftwareFormat)
        {
            HelperEx = new FMODHelperEx();
            HelperEx.FMODResult = FMOD.RESULT.OK;
            //FMOD.RESULT result;

            /*
                Create a System object and initialize.
            */
            HelperEx.FMODResult = FMOD.Factory.System_Create(ref system);
            ERRCHECK(HelperEx.FMODResult);

            /*
            HelperEx.FMODResult = system.getVersion(ref version);
            ERRCHECK(HelperEx.FMODResult);
            if (version < FMOD.VERSION.number)
            {
                throw new Exception("Error!  You are using an old version of FMOD " + version.ToString("X") + ".  This program requires " + FMOD.VERSION.number.ToString("X") + ".");
            }*/
            HelperEx.FMODResult =
            system.setSoftwareFormat(SoftwareFormat.Samplerate, SoftwareFormat.SoundFormat,
                                     SoftwareFormat.OutputChannel, SoftwareFormat.MaxInputChannel,
                                     SoftwareFormat.ResamplerMethod);

            HelperEx.FMODResult = system.init(32, FMOD.INITFLAGS.NORMAL, (IntPtr)null);
            if (HelperEx.FMODResult == FMOD.RESULT.OK) PlayStatus = PlayingStatus.Ready; HelperEx = new FMODHelperEx(this); dsp = new FMODDSPHelper(this);
            //timer1.Start();
        }
        public FMODRecoderHelper(SoftwareFormat SoftwareFormat, FMOD.CREATESOUNDEXINFO info)
        {
            SoundInformation=info;
            HelperEx = new FMODHelperEx();
            HelperEx.FMODResult = FMOD.RESULT.OK;
            //FMOD.RESULT result;

            /*
                Create a System object and initialize.
            */
            HelperEx.FMODResult = FMOD.Factory.System_Create(ref system);
            ERRCHECK(HelperEx.FMODResult);

            /*
            HelperEx.FMODResult = system.getVersion(ref version);
            ERRCHECK(HelperEx.FMODResult);
            if (version < FMOD.VERSION.number)
            {
                throw new Exception("Error!  You are using an old version of FMOD " + version.ToString("X") + ".  This program requires " + FMOD.VERSION.number.ToString("X") + ".");
            }*/
            HelperEx.FMODResult =
            system.setSoftwareFormat(SoftwareFormat.Samplerate, SoftwareFormat.SoundFormat,
                                     SoftwareFormat.OutputChannel, SoftwareFormat.MaxInputChannel,
                                     SoftwareFormat.ResamplerMethod);

            HelperEx.FMODResult = system.init(32, FMOD.INITFLAGS.NORMAL, (IntPtr)null);
            if (HelperEx.FMODResult == FMOD.RESULT.OK) PlayStatus = PlayingStatus.Ready; HelperEx = new FMODHelperEx(this); dsp = new FMODDSPHelper(this);
            system.createStream((string)null, FMOD.MODE._2D, ref SoundInformation, ref sound);
            //timer1.Start();
        }

        public FMOD.CREATESOUNDEXINFO SoundInformation = new FMOD.CREATESOUNDEXINFO()
        {
            format = FMOD.SOUND_FORMAT.PCM16,
            numchannels = 2,
            defaultfrequency = 44100,
            length = (uint)(44100 * 2 * 2 * 5)
        };

        public void StartRecording()
        {

        }
        public void StopRecording()
        {

        }

        public new string MusicPath { get; set; }
    }
}
