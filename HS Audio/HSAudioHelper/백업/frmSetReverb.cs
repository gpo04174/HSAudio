﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;

using System.Text;
using System.Windows.Forms;
using CSharpUtility;
using System.IO;

namespace FMOD_Helper.Forms
{
    public partial class frmReverb : Form
    {
        public delegate void CustomReverbValueChangedEventHandler(FMOD.REVERB_PROPERTIES Value, Dictionary<string, string> ReverbPreset);
        public delegate void CustomReverbGainValueChangedEventHandler(float Gain);
        public event CustomReverbValueChangedEventHandler CustomReverbValueChanged;
        public event CustomReverbGainValueChangedEventHandler CustomReverbGainValueChanged;

        FMOD.REVERB_PROPERTIES rp = new FMOD.REVERB_PROPERTIES();
        
        public frmReverb()
        {
            InitializeComponent();
            Common();
        }

        public frmReverb(string ProfileName)
        {
            InitializeComponent();
            Common();
            this.ProfileName = ProfileName;
            toolStripComboBox1.Text = ProfileName;
            if (toolStripComboBox1.Items.IndexOf(ProfileName) != -1)
                toolStripComboBox1.SelectedIndex = toolStripComboBox1.Items.IndexOf(ProfileName);
        }
        public frmReverb(FMOD.REVERB_PROPERTIES Reverbe)
        {
            InitializeComponent();
            Common();
            this.CustomReverb = Reverbe;
        }
        public frmReverb(float Gain)
        {
            InitializeComponent();
            Common();
            this.Gain = Gain;
        }
        public frmReverb(string ProfileName, float Gain)
        {
            InitializeComponent();
            Common();
            this.ProfileName = ProfileName;
            this.Gain = Gain;
        }
        public frmReverb(FMOD.REVERB_PROPERTIES Reverbe, float Gain)
        {
            InitializeComponent();
            Common();
            this.CustomReverb = Reverbe;
            this.Gain = Gain;
        }
        public frmReverb(FMOD.REVERB_PROPERTIES Reverbe, string ProfileName)
        {
            InitializeComponent();
            Common();
            if (toolStripComboBox1.Items.IndexOf(ProfileName) != -1)
                toolStripComboBox1.SelectedIndex = toolStripComboBox1.Items.IndexOf(ProfileName);
            this.CustomReverb = Reverbe;
            this.ProfileName = ProfileName;
            toolStripComboBox1.Text = ProfileName;
        }
        public frmReverb(FMOD.REVERB_PROPERTIES Reverbe, string ProfileName, float Gain)
        {
            InitializeComponent();
            Common();
            if (toolStripComboBox1.Items.IndexOf(ProfileName) != -1)
                toolStripComboBox1.SelectedIndex = toolStripComboBox1.Items.IndexOf(ProfileName);
            this.CustomReverb = Reverbe;
            this.ProfileName = ProfileName;
            this.Gain = Gain;
            toolStripComboBox1.Text = ProfileName;
        }

        void Common()
        {
            새로고침ToolStripMenuItem.ToolTipText = PresetDir + "\n폴더에있는 프리셋의 목록을 새로고침 합니다.";
            toolStripComboBox1.ToolTipText = PresetDir + "\n폴더에있는 프리셋의 목록입니다.";

            if (!System.IO.Directory.Exists(PresetDir)) { System.IO.Directory.CreateDirectory(PresetDir); }
            //openFileDialog1.InitialDirectory = saveFileDialog1.InitialDirectory = a;
            새로고침ToolStripMenuItem_Click(null, null);
            PresetPath.Add("LastFileOpenDirectory", LastFileOpenDirectory == null ? PresetDir : LastFileOpenDirectory);
            PresetPath.Add("LastFileSaveDirectory", LastFileSaveDirectory == null ? PresetDir : LastFileSaveDirectory);
            PresetPath.Add("LastFolderDirectory", LastFolderDirectory == null ? PresetDir : LastFolderDirectory);
        }

        string _ProfileName;
        public string ProfileName 
        {
            get { return _ProfileName; }
            set
            {
                _ProfileName = value; if (value != "" || value != null) { this.Text = "반향효과(Reverb) 설정  - [" + value + "]"; }
                else { this.Text = "반향효과(Reverb) 설정"; } openFileDialog1.FileName = saveFileDialog1.FileName=toolStripComboBox1.Text = value;
            } 
        }

        /// <summary>
        /// Gain 값을 설정합니다. (범위: -99.9 ~ 25.0)
        /// </summary>
        public float Gain {
            get { return (float)(trbVolume.Value - 1000) * 0.1f/*(float)(numGain.Value * 0.01m + 1)*/; }
            set { trbVolume.Value = 1000 + (int)(value * 10); }
        }

        public FMOD.REVERB_PROPERTIES CustomReverb
        {
            get 
            {
                FMOD.REVERB_PROPERTIES rp1 = new FMOD.REVERB_PROPERTIES();
                rp1.DecayHFRatio = (float)this.numDecayHFRatio.Value;
                rp1.DecayLFRatio = (float)this.numDecayLFRatio.Value;
                rp1.DecayTime = (float)this.numDecayTime.Value;
                rp1.Density = (float)this.numDensity.Value;
                rp1.Diffusion = (float)this.numDiffusion.Value;
                rp1.EnvDiffusion = (float)this.numEnvDiffusion.Value; ;
                rp1.Environment = (int)this.numEnvironment.Value; ;
                rp1.Flags = uint.Parse(this.numFlag.Text.Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                rp1.HFReference = (float)this.numHFReference.Value;
                rp1.Instance = (int)this.numInstance.Value;
                rp1.LFReference = (float)this.numLFReference.Value;
                rp1.ModulationDepth = (float)this.numModulationDepth.Value;
                rp1.ModulationTime = (float)this.numModulationTime.Value;
                rp1.Reflections = (int)this.numReflections.Value;
                rp1.ReflectionsDelay = (int)this.numReflectionsDelay.Value;
                rp1.Reverb = (int)this.numReverb.Value;
                rp1.ReverbDelay = (int)this.numReverbDelay.Value;
                rp1.Room = (int)this.numRoom.Value;
                rp1.RoomHF = (int)this.numRoomHF.Value;
                rp1.RoomLF = (int)this.numRoomLF.Value;
                rp = rp1; return rp1; 
            }
            set 
            {
                this.numInstance.Value = value.Instance;
                this.numEnvironment.Value = value.Environment;
                this.numEnvDiffusion.Value = (decimal)value.EnvDiffusion;
                this.numRoom.Value = value.Room;
                this.numRoomHF.Value = value.RoomHF;
                this.numRoomLF.Value = value.RoomLF;
                this.numDecayTime.Value = (decimal)value.DecayTime;
                this.numDecayHFRatio.Value = (decimal)value.DecayHFRatio;
                this.numDecayLFRatio.Value = (decimal)value.DecayLFRatio;
                this.numReflections.Value = value.Reflections;
                this.numReflectionsDelay.Value = (decimal)value.ReflectionsDelay;
                this.numReverb.Value = (decimal)value.Reverb;
                this.numReverbDelay.Value = (decimal)value.ReverbDelay;
                this.numModulationTime.Value = (decimal)value.ModulationTime;
                this.numModulationDepth.Value = (decimal)value.ModulationDepth;
                this.numHFReference.Value = (decimal)value.HFReference;
                this.numLFReference.Value = (decimal)value.LFReference;
                this.numDiffusion.Value = (decimal)value.Diffusion;
                this.numDensity.Value = (decimal)value.Density;
                this.numFlag.Text = "0x" + value.Flags.ToString("X");
                rp = value;
                try { CustomReverbValueChanged(rp, ReverbPreset); }catch { }
            }
        }

        private void num_ValueChanged(object sender, EventArgs e)
        {
            rp.DecayHFRatio = (float)this.numDecayHFRatio.Value;
            rp.DecayLFRatio = (float)this.numDecayLFRatio.Value;
            rp.DecayTime = (float)this.numDecayTime.Value;
            rp.Density = (float)this.numDensity.Value;
            rp.Diffusion = (float)this.numDiffusion.Value;
            rp.EnvDiffusion = (float)this.numEnvDiffusion.Value;
            rp.Environment = (int)this.numEnvironment.Value;
            rp.Flags = uint.Parse(this.numFlag.Text.Replace("0x",""), System.Globalization.NumberStyles.HexNumber);
            rp.HFReference = (float)this.numHFReference.Value;
            rp.Instance = (int)this.numInstance.Value;
            rp.LFReference = (float)this.numLFReference.Value;
            rp.ModulationDepth = (float)this.numModulationDepth.Value;
            rp.ModulationTime = (float)this.numModulationTime.Value;
            rp.Reflections = (int)this.numReflections.Value;
            rp.ReflectionsDelay = (int)this.numReflectionsDelay.Value;
            rp.Reverb = (int)this.numReverb.Value;
            rp.ReverbDelay = (int)this.numReverbDelay.Value;
            rp.Room = (int)this.numRoom.Value;
            rp.RoomHF = (int)this.numRoomHF.Value;
            rp.RoomLF = (int)this.numRoomLF.Value;
            try { CustomReverbValueChanged(rp, ReverbPreset); }catch { }

        }

        private void hLLF설명ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //numericUpDown1.Value = new decimal(float.NaN);
            MessageBox.Show("HF는 High Frequencies 의 줄임말로 우리말로 해석하면 높은 주파수 입니다.\n"+
           "LF는 Low Frequencies 의 줄임말로 우리말로 해석하면 낮은 주파수 입니다.", "HF, LF 설명", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

         private void 보두기본값으로설정ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DialogResult dr = MessageBox.Show("정말 모두 기본값으로 설정하시겠습니까?", "모두 기본값 설정", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
            if (dr == DialogResult.Yes)
            {
                this.numInstance.Value = 0; 
                this.numEnvironment.Value = -1;
                this.numEnvDiffusion.Value = 1;
                this.numRoom.Value = -1000; 
                this.numRoomHF.Value = -100; 
                this.numRoomLF.Value = 0;
                this.numDecayTime.Value = 0.49m; 
                this.numDecayHFRatio.Value = 0.83m;
                this.numDecayLFRatio.Value = 1; 
                this.numReflections.Value = -2602; 
                this.numReflectionsDelay.Value = 0.007m; 
                this.numReverb.Value = 100; 
                this.numReverbDelay.Value = 0.011m;
                this.numModulationTime.Value = 0.25m;
                this.numModulationDepth.Value = 0; 
                this.numHFReference.Value = 5000; 
                this.numLFReference.Value = 250; 
                this.numDiffusion.Value = 100; 
                this.numDensity.Value = 100;
                this.numFlag.Text = "0x003";
                try { CustomReverbValueChanged(rp, ReverbPreset); }catch { }
            }
        }

         #region 개별 기본값 버튼 클릭
         private void button1_Click(object sender, EventArgs e) { this.numInstance.Value = 0; try { CustomReverbValueChanged(rp, ReverbPreset); } catch { } }

         private void button2_Click(object sender, EventArgs e) { this.numEnvironment.Value = -1; try { CustomReverbValueChanged(rp, ReverbPreset); } catch { } }

         private void button3_Click(object sender, EventArgs e) { this.numEnvDiffusion.Value = 1; try { CustomReverbValueChanged(rp, ReverbPreset); } catch { } }

         private void button4_Click(object sender, EventArgs e) { this.numRoom.Value = -1000; try { CustomReverbValueChanged(rp, ReverbPreset); } catch { } }

         private void button5_Click(object sender, EventArgs e) { this.numRoomHF.Value = -100; try { CustomReverbValueChanged(rp, ReverbPreset); } catch { } }

         private void button6_Click(object sender, EventArgs e) { this.numRoomLF.Value = 0; try { CustomReverbValueChanged(rp, ReverbPreset); } catch { } }

         private void button16_Click(object sender, EventArgs e) { this.numDecayTime.Value = 0.49m; try { CustomReverbValueChanged(rp, ReverbPreset); } catch { } }

         private void button15_Click(object sender, EventArgs e) { this.numDecayHFRatio.Value = 0.83m; try { CustomReverbValueChanged(rp, ReverbPreset); } catch { } }

         private void button14_Click(object sender, EventArgs e) { this.numDecayLFRatio.Value = 1; try { CustomReverbValueChanged(rp, ReverbPreset); } catch { } }

         private void button13_Click(object sender, EventArgs e) { this.numReflections.Value = -2602; try { CustomReverbValueChanged(rp, ReverbPreset); } catch { } }

         private void button12_Click(object sender, EventArgs e) { this.numReflectionsDelay.Value = 0.007m; try { CustomReverbValueChanged(rp, ReverbPreset); } catch { } }

         private void button11_Click(object sender, EventArgs e) { this.numReverb.Value = 100; try { CustomReverbValueChanged(rp, ReverbPreset); } catch { } }

         private void button10_Click(object sender, EventArgs e) { this.numReverbDelay.Value = 0.011m; try { CustomReverbValueChanged(rp, ReverbPreset); } catch { } }

         private void button9_Click(object sender, EventArgs e) { this.numModulationTime.Value = 0.25m; try { CustomReverbValueChanged(rp, ReverbPreset); } catch { } }

         private void button8_Click(object sender, EventArgs e) { this.numModulationDepth.Value = 0; try { CustomReverbValueChanged(rp, ReverbPreset); } catch { } }

         private void button7_Click(object sender, EventArgs e) { this.numHFReference.Value = 5000; try { CustomReverbValueChanged(rp, ReverbPreset); } catch { } }

         private void button18_Click(object sender, EventArgs e) { this.numLFReference.Value = 250; try { CustomReverbValueChanged(rp, ReverbPreset); } catch { } }

         private void button17_Click(object sender, EventArgs e) { this.numDiffusion.Value = 100; try { CustomReverbValueChanged(rp, ReverbPreset); } catch { } }

         private void button20_Click(object sender, EventArgs e) { this.numDensity.Value = 100; try { CustomReverbValueChanged(rp, ReverbPreset); } catch { } }

         private void button19_Click(object sender, EventArgs e) { this.numFlag.Text = "0x003"; try { CustomReverbValueChanged(rp, ReverbPreset); } catch { } }

         private void button21_Click(object sender, EventArgs e) { this.numVolume.Value = 0.0m; try { CustomReverbValueChanged(rp, ReverbPreset); } catch { } }
         #endregion

         private void 고급설정모드ToolStripMenuItem_Click(object sender, EventArgs e)
         {
             
         }

         private void 다른이름으로저장ToolStripMenuItem_Click(object sender, EventArgs e)
         {
             if (!System.IO.Directory.Exists(PresetDir)) { System.IO.Directory.CreateDirectory(PresetDir); }
             saveFileDialog1.InitialDirectory = Directory.Exists(LastFileSaveDirectory) ? LastFileSaveDirectory : PresetDir;

             saveFileDialog1.FileName = toolStripComboBox1.Text;
             if (DialogResult.OK == saveFileDialog1.ShowDialog())
             {
                 LastFileSaveDirectory = Path.GetDirectoryName(saveFileDialog1.FileName);
                 System.IO.File.WriteAllLines(saveFileDialog1.FileName, Utility.EtcUtility.SaveSetting(ReverbPreset));
                 ProfileName = System.IO.Path.GetFileNameWithoutExtension(saveFileDialog1.FileName);
             }
         }

         private void saveFileDialog1_FileOk(object sender, CancelEventArgs e)
         {
             
         }

         private void frmSetCustomReverb_Load(object sender, EventArgs e)
         {
             toolStripComboBox1.Text = ProfileName;
         }

         private void tbGain_ValueChanged(object sender, EventArgs e)
         {
             numVolume.Value = (trbVolume.Value - 1000)*0.1m;
             //if (tbGain.Value > 0) { tbGain.Value = 0; domainUpDown1.Text = "-Inf."; domainUpDown1.Visible = true; numGain.Visible = false; }
         }

         private void numGain_ValueChanged(object sender, EventArgs e)
         {
             trbVolume.Value =1000+(int)(numVolume.Value * 10);
             if (numVolume.Value == -100) { domainUpDown1.Text = "-Inf."; domainUpDown1.Visible = true; numVolume.Visible = false; }
             else { domainUpDown1.Visible = false; numVolume.Visible = true; }
             float a = Gain;
             try { CustomReverbGainValueChanged((float)(Gain<=-100?float.NaN:Gain*0.01f+1)); }
             catch { }
             /*
             else if (a > 0) {tbGain.Value= 1000+a; }
             else if (a < 0) { tbGain.Value = 1000 - a; }
             else { tbGain.Value = 1000; }*/
         }

         private void domainUpDown1_SelectedItemChanged(object sender, EventArgs e)
         {
             if (domainUpDown1.SelectedIndex == 0)
             {
                 domainUpDown1.Visible = false;numVolume.Visible = true;
             }
         }

         private void numGain_VisibleChanged(object sender, EventArgs e)
         {
             if (numVolume.Value == -100)
             {
                 if (domainUpDown1.SelectedIndex == 0)
                 { numVolume.Value = -99.9m; domainUpDown1.Visible = false; numVolume.Visible = true; }
             }
         }

         List<Dictionary<string, string>> Presets = new List<Dictionary<string, string>>();
         private void 열기ToolStripMenuItem_Click(object sender, EventArgs e)
         {
             if (!System.IO.Directory.Exists(PresetDir)) { System.IO.Directory.CreateDirectory(PresetDir); }
             //LastFileDirectory
             openFileDialog1.InitialDirectory = Directory.Exists(LastFileOpenDirectory) ? LastFileOpenDirectory : PresetDir;

             if (DialogResult.OK == openFileDialog1.ShowDialog())
             {
                 LastFileOpenDirectory = Path.GetDirectoryName(openFileDialog1.FileName);
                 string[] a1= System.IO.File.ReadAllLines(openFileDialog1.FileName);
                 ReverbPreset = Utility.EtcUtility.LoadSetting(a1);
                 ProfileName= System.IO.Path.GetFileNameWithoutExtension(openFileDialog1.FileName);
                 Presets.Add(ReverbPreset); toolStripComboBox1.Items.Add(ProfileName);
                 toolStripComboBox1.Text = toolStripComboBox1.Items[toolStripComboBox1.Items.Count - 1].ToString();
             }
         }
         
         private void 닫기XToolStripMenuItem_Click(object sender, EventArgs e)
         {
             this.Close();
         }

         private void button22_Click(object sender, EventArgs e)
         {
             this.Close();
         }

         private void numEnvironment_KeyDown(object sender, KeyEventArgs e)
         {
             if (e.KeyValue == 27) { this.Close(); }
         }

         public string PresetDir { get { return Application.StartupPath.Replace("/", "\\") + "\\Preset\\Reverb"; } }
         string LastPreset = "(기본값)";
         private void 새로고침ToolStripMenuItem_Click(object sender, EventArgs e)
         {
             Presets.Clear(); toolStripComboBox1.Items.Clear();
             Presets.Add(InitPreset()); toolStripComboBox1.Items.Add("(기본값)");
             foreach (string a in Directory.GetFiles(LastFolderDirectory, "*.refctprof"))
             {
                 try
                 {
                     Dictionary<string, string> b =
                         CSharpUtility.Utility.EtcUtility.LoadSetting(File.ReadAllLines(a));
                     Presets.Add(b);
                     toolStripComboBox1.Items.Add(Path.GetFileNameWithoutExtension(a));
                 }
                 catch { }
             }
             if (toolStripComboBox1.Items.IndexOf(LastPreset) != -1) toolStripComboBox1.SelectedIndex = toolStripComboBox1.Items.IndexOf(LastPreset);
             else toolStripComboBox1.SelectedIndex = 0;
         }

         internal Dictionary<string, string> ReverbPreset
         {
             get
             {
                 Dictionary<string, string> Setting = new Dictionary<string, string>();
                 Setting.Add("Instance", rp.Instance.ToString());
                 Setting.Add("Environment", rp.Environment.ToString());
                 Setting.Add("EnvDiffusion", rp.EnvDiffusion.ToString());
                 Setting.Add("Room", rp.Room.ToString());
                 Setting.Add("RoomHF", rp.RoomHF.ToString());
                 Setting.Add("RoomLF", rp.RoomLF.ToString());
                 Setting.Add("DecayTime", rp.DecayTime.ToString());
                 Setting.Add("DecayHFRatio", rp.DecayHFRatio.ToString());
                 Setting.Add("DecayLFRatio", rp.DecayLFRatio.ToString());
                 Setting.Add("Reflections", rp.Reflections.ToString());
                 Setting.Add("ReflectionsDelay", rp.ReflectionsDelay.ToString());
                 Setting.Add("Reverb", rp.Reverb.ToString());
                 Setting.Add("ReverbDelay", rp.ReverbDelay.ToString());
                 Setting.Add("ModulationTime", rp.ModulationTime.ToString());
                 Setting.Add("ModulationDepth", rp.ModulationDepth.ToString());
                 Setting.Add("HFReference", rp.HFReference.ToString());
                 Setting.Add("LFReference", rp.LFReference.ToString());
                 Setting.Add("Diffusion", rp.Diffusion.ToString());
                 Setting.Add("Density", rp.Density.ToString());
                 Setting.Add("Flags", "0x" + rp.Flags.ToString("X"));
                 Setting.Add("Reverb_Volume", numVolume.Value.ToString());
                 Setting.Add("AdvanceSetting", 고급설정모드ToolStripMenuItem.Checked.ToString());
                 return Setting;
             }
             set
             {
                 CustomReverb= ConvertPresetToSetting(value);
                 try{numVolume.Value = 
                     decimal.Parse(value["Reverb_Volume"] == null || value["Reverb_Volume"] == "" ? "1000" : value["Reverb_Volume"]);}catch{}
                 try{고급설정모드ToolStripMenuItem.Checked=
                     bool.Parse(value["AdvanceSetting"] == null ||value["AdvanceSetting"] == "" ?"False":value["AdvanceSetting"]);}catch{}
             }
         }
         FMOD.REVERB_PROPERTIES ConvertPresetToSetting(Dictionary<string, string> Value)
         {
             string b;
             FMOD.REVERB_PROPERTIES rp = new FMOD.REVERB_PROPERTIES();
             rp.Instance = int.Parse(Value["Instance"] == null || Value["Instance"] == "" ? "0" : Value["Instance"]);
             b = "Environment";
             rp.Environment = int.Parse(Value[b] == null || Value[b] == "" ? "-1" : Value[b]);
             b = "EnvDiffusion";
             rp.EnvDiffusion = float.Parse(Value[b] == null || Value[b] == "" ? "1" : Value[b]);
             b = "Room";
             rp.Room = int.Parse(Value[b] == null || Value[b] == "" ? "-1000" : Value[b]);
             b = "RoomHF";
             rp.RoomHF = int.Parse(Value[b] == null || Value[b] == "" ? "-100" : Value[b]);
             b = "RoomLF";
             rp.RoomLF = int.Parse(Value[b] == null || Value[b] == "" ? "0" : Value[b]);
             b = "DecayTime";
             rp.DecayTime = float.Parse(Value[b] == null || Value[b] == "" ? "0.49" : Value[b]);
             b = "DecayHFRatio";
             rp.DecayHFRatio = float.Parse(Value[b] == null || Value[b] == "" ? "0.83" : Value[b]);
             b = "DecayLFRatio";
             rp.DecayLFRatio = float.Parse(Value[b] == null || Value[b] == "" ? "1" : Value[b]);
             b = "Reflections";
             rp.Reflections = int.Parse(Value[b] == null || Value[b] == "" ? "-2602" : Value[b]);
             b = "ReflectionsDelay";
             rp.ReflectionsDelay = float.Parse(Value[b] == null || Value[b] == "" ? "0.007" : Value[b]);
             b = "Reverb";
             rp.Reverb = int.Parse(Value[b] == null || Value[b] == "" ? "200" : Value[b]);
             b = "ReverbDelay";
             rp.ReverbDelay = float.Parse(Value[b] == null || Value[b] == "" ? "0.011" : Value[b]);
             b = "ModulationTime";
             rp.ModulationTime = float.Parse(Value[b] == null || Value[b] == "" ? "0.25" : Value[b]);
             b = "ModulationDepth";
             rp.ModulationDepth = float.Parse(Value[b] == null || Value[b] == "" ? "0" : Value[b]);
             b = "HFReference";
             rp.HFReference = float.Parse(Value[b] == null || Value[b] == "" ? "5000" : Value[b]);
             b = "LFReference";
             rp.LFReference = float.Parse(Value[b] == null || Value[b] == "" ? "250" : Value[b]);
             b = "Diffusion";
             rp.Diffusion = float.Parse(Value[b] == null || Value[b] == "" ? "100" : Value[b]);
             b = "Density";
             rp.Density = float.Parse(Value[b] == null || Value[b] == "" ? "100" : Value[b]);
             b = "Flags";
             try { rp.Flags = uint.Parse(Value[b] == null || Value[b] == "" ? "3" : Value[b].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber); }catch{rp.Flags=3;}
             return rp;
         }
         Dictionary<string, string> InitPreset()
         {
             FMOD.REVERB_PROPERTIES rp = DefaultREVERB_PROPERTIES;
             Dictionary<string, string> Setting = new Dictionary<string, string>();
             Setting.Add("Instance", rp.Instance.ToString());
             Setting.Add("Environment", rp.Environment.ToString());
             Setting.Add("EnvDiffusion", rp.EnvDiffusion.ToString());
             Setting.Add("Room", rp.Room.ToString());
             Setting.Add("RoomHF", rp.RoomHF.ToString());
             Setting.Add("RoomLF", rp.RoomLF.ToString());
             Setting.Add("DecayTime", rp.DecayTime.ToString());
             Setting.Add("DecayHFRatio", rp.DecayHFRatio.ToString());
             Setting.Add("DecayLFRatio", rp.DecayLFRatio.ToString());
             Setting.Add("Reflections", rp.Reflections.ToString());
             Setting.Add("ReflectionsDelay", rp.ReflectionsDelay.ToString());
             Setting.Add("Reverb", rp.Reverb.ToString());
             Setting.Add("ReverbDelay", rp.ReverbDelay.ToString());
             Setting.Add("ModulationTime", rp.ModulationTime.ToString());
             Setting.Add("ModulationDepth", rp.ModulationDepth.ToString());
             Setting.Add("HFReference", rp.HFReference.ToString());
             Setting.Add("LFReference", rp.LFReference.ToString());
             Setting.Add("Diffusion", rp.Diffusion.ToString());
             Setting.Add("Density", rp.Density.ToString());
             Setting.Add("Flags", rp.Flags.ToString("0x"+"X"));
             Setting.Add("Reverb_Volume", "1000");
             Setting.Add("AdvanceSetting", "False");
             return Setting;
         }
         FMOD.REVERB_PROPERTIES DefaultREVERB_PROPERTIES
         {
             get
             {
                 FMOD.REVERB_PROPERTIES rp = new FMOD.REVERB_PROPERTIES();
                 rp.Instance = 0;
                 rp.Environment = -1;
                 rp.EnvDiffusion = 1;
                 rp.Room = -1000;
                 rp.RoomHF = -100;
                 rp.RoomLF = 0;
                 rp.DecayTime = 0.49f;
                 rp.DecayHFRatio = 0.83f;
                 rp.DecayLFRatio = 1;
                 rp.Reflections = -2602;
                 rp.ReflectionsDelay = 0.007f;
                 rp.Reverb = 100;
                 rp.ReverbDelay = 0.011f;
                 rp.ModulationTime = 0.25f;
                 rp.ModulationDepth = 0;
                 rp.HFReference = 5000;
                 rp.LFReference = 250;
                 rp.Diffusion = 100;
                 rp.Density = 100;
                 rp.Flags = 0x003;
                 return rp;
             }
         }

         private void 제거ToolStripMenuItem_Click(object sender, EventArgs e)
         {
             if (toolStripComboBox1.Items.Count>1&&toolStripComboBox1.SelectedIndex!=0){
             if (DialogResult.Yes == MessageBox.Show("정말 선택된 프리셋을 목록에서 제거 하시겠습니까?", "반향효과(Reverb) 프리셋 선택된 목록 제거", MessageBoxButtons.YesNo, MessageBoxIcon.Warning))
             { Presets.RemoveAt(toolStripComboBox1.SelectedIndex); toolStripComboBox1.Items.RemoveAt(toolStripComboBox1.SelectedIndex); toolStripComboBox1.SelectedIndex = 0; }}
         }

         private void 새프리셋ToolStripMenuItem_Click(object sender, EventArgs e)
         {
             if (DialogResult.Yes == MessageBox.Show("정말 모두 초기화하고 새 프리셋을 작성하시겠습니까?", "이퀄라이저 프리셋 초기화", MessageBoxButtons.YesNo, MessageBoxIcon.Warning))
             { ReverbPreset = InitPreset(); toolStripComboBox1.SelectedIndex = 0; ProfileName = null; }
         }

         private void 고급설정모드ToolStripMenuItem_CheckedChanged(object sender, EventArgs e)
         {
             if (고급설정모드ToolStripMenuItem.Checked)
             {
                 this.numRoom.Increment = 1;
                 this.numRoomHF.Increment = 1;
                 this.numRoomLF.Increment = 1;
                 this.numDecayTime.Increment = 0.001m;
                 this.numDecayHFRatio.Increment = 0.001m;
                 this.numDecayLFRatio.Increment = 0.001m;
                 this.numReflections.Increment = 1;
                 this.numReverb.Increment = 1;
                 this.numReflectionsDelay.Increment = 0.0001m;
                 this.numModulationTime.Increment = 0.0001m;
                 this.numModulationDepth.Increment = 0.0001m;
                 this.numHFReference.Increment = 1m;
                 this.numLFReference.Increment = 1;
                 this.numDiffusion.Increment = 0.1m;
                 this.numDensity.Increment = 0.1m;
                 this.numFlag.ReadOnly = false;
                 this.numFlag.BackColor = Color.White;
                 label1.Enabled = numInstance.Enabled = button1.Enabled = label17.Enabled =
                                  numDiffusion.Enabled = button17.Enabled = label20.Enabled =
                                  numDensity.Enabled = button20.Enabled = true;
             }
             else
             {
                 this.numRoom.Increment = 100;
                 this.numRoomHF.Increment = 100;
                 this.numRoomLF.Increment = 100;
                 this.numDecayTime.Increment = 0.005m;
                 this.numDecayHFRatio.Increment = 0.005m;
                 this.numDecayLFRatio.Increment = 0.005m;
                 this.numReflections.Increment = 100;
                 this.numReverb.Increment = 100;
                 this.numReflectionsDelay.Increment = 0.0005m;
                 this.numModulationTime.Increment = 0.005m;
                 this.numModulationDepth.Increment = 0.0005m;
                 this.numHFReference.Increment = 100;
                 this.numLFReference.Increment = 5;
                 this.numDiffusion.Increment = 0.5m;
                 this.numDensity.Increment = 0.5m;
                 this.numFlag.ReadOnly = true;
                 this.numFlag.BackColor = SystemColors.Control;
                 label1.Enabled = numInstance.Enabled = button1.Enabled = label17.Enabled =
                                  numDiffusion.Enabled = button17.Enabled = label20.Enabled =
                                  numDensity.Enabled = button20.Enabled = false;
             }
         }

         private void toolStripComboBox1_SelectedIndexChanged(object sender, EventArgs e)
         {
             if (toolStripComboBox1.SelectedIndex != -1)
             {
                 ReverbPreset = Presets[toolStripComboBox1.SelectedIndex];
                 LastPreset = toolStripComboBox1.Items[toolStripComboBox1.SelectedIndex].ToString();
                 ProfileName = LastPreset;
                 if (toolStripComboBox1.SelectedIndex == 0) this.Text = "반향효과(Reverb) 설정";
             }
             else { toolStripComboBox1.SelectedIndex = 0; this.Text = "반향효과(Reverb) 설정"; }
         }

         private void 정렬ToolStripMenuItem_CheckedChanged(object sender, EventArgs e)
         {
             string a = toolStripComboBox1.Text;
             if (정렬ToolStripMenuItem.Checked)
             {
                 if(DialogResult.Yes== MessageBox.Show("정말 언어순으로 위로 정렬하시겠습니까?", "정렬 알림", MessageBoxButtons.YesNo, MessageBoxIcon.Warning)){
                 toolStripComboBox1.Sorted = true;
                 정렬ToolStripMenuItem.BackColor = Color.Lime;}
             }
             else
             {
                 toolStripComboBox1.Sorted = false;
                 정렬ToolStripMenuItem.BackColor = Color.Transparent;
             }
             if (toolStripComboBox1.Items.IndexOf(a) != -1) { toolStripComboBox1.SelectedIndex = toolStripComboBox1.Items.IndexOf(a); }
         }

         private void 폴더선택ToolStripMenuItem_Click(object sender, EventArgs e)
         {
             if (!System.IO.Directory.Exists(PresetDir)) { System.IO.Directory.CreateDirectory(PresetDir); }
             folderBrowserDialog1.SelectedPath = LastFolderDirectory;
             if (DialogResult.OK == folderBrowserDialog1.ShowDialog())
             {LastFolderDirectory = folderBrowserDialog1.SelectedPath;}
         }

         Dictionary<string, string> PresetPath = new Dictionary<string, string>();
         string LastFileOpenDirectory
         {
             get
             {
                 if (File.Exists(PresetDir + "\\LastReverbPath.ini"))
                 {
                     string[] a = File.ReadAllLines(PresetDir + "\\LastReverbPath.ini");
                     string a1 = CSharpUtility.Utility.EtcUtility.LoadSetting(a)["LastFileOpenDirectory"];
                     if (a1 != null || a1 != "") { return Directory.Exists(a1) ? a1 : PresetDir; }
                     else { return PresetDir; }
                 }
                 else { return PresetDir; }
             }
             set
             {
                 if (PresetPath.Count < 3)
                 {
                     PresetPath.Clear(); PresetPath.Add("LastFileOpenDirectory", value);
                     PresetPath.Add("LastFileSaveDirectory", PresetDir);
                     PresetPath.Add("LastFolderDirectory", PresetDir);
                 }
                 else { PresetPath["LastFileOpenDirectory"] = value; }
                 File.WriteAllLines(PresetDir + "\\LastReverbPath.ini", Utility.EtcUtility.SaveSetting(PresetPath));
             }
         }
         string LastFileSaveDirectory
         {
             get
             {
                 if (File.Exists(PresetDir + "\\LastReverbPath.ini"))
                 {
                     string[] a = File.ReadAllLines(PresetDir + "\\LastReverbPath.ini");
                     string a1 = CSharpUtility.Utility.EtcUtility.LoadSetting(a)["LastFileSaveDirectory"];
                     if (a1 != null || a1 != "") { return Directory.Exists(a1) ? a1 : PresetDir; }
                     else { return PresetDir; }
                 }
                 else { return PresetDir; }
             }
             set
             {
                 if (PresetPath.Count < 3)
                 {
                     PresetPath.Clear(); PresetPath.Add("LastFileOpenDirectory", PresetDir);
                     PresetPath.Add("LastFileSaveDirectory", value);
                     PresetPath.Add("LastFolderDirectory", PresetDir);
                 }
                 else { PresetPath["LastFileSaveDirectory"] = value; }
                 File.WriteAllLines(PresetDir + "\\LastReverbPath.ini", Utility.EtcUtility.SaveSetting(PresetPath));
             }
         }
         string LastFolderDirectory
         {
             get
             {
                 if (File.Exists(PresetDir + "\\LastReverbPath.ini"))
                 {
                     string[] a = File.ReadAllLines(PresetDir + "\\LastReverbPath.ini");
                     string a1 = CSharpUtility.Utility.EtcUtility.LoadSetting(a)["LastFolderDirectory"];
                     if (a1 != null || a1 != "") { return Directory.Exists(a1) ? a1 : PresetDir; }
                     else { return PresetDir; }
                 }
                 else { return PresetDir; }
             }
             set
             {
                 if (PresetPath.Count < 2)
                 {
                     PresetPath.Clear(); PresetPath.Add("LastFileOpenDirectory", PresetDir);
                     PresetPath.Add("LastFileSaveDirectory", PresetDir);
                     PresetPath.Add("LastFolderDirectory", value);
                 }
                 else { PresetPath["LastFolderDirectory"] = value; }
                 File.WriteAllLines(PresetDir + "\\LastReverbPath.ini", Utility.EtcUtility.SaveSetting(PresetPath));
             }
         }

         private void openFileDialog1_FileOk(object sender, CancelEventArgs e)
         {

         }

         private void 프리셋경로재설정ToolStripMenuItem_Click(object sender, EventArgs e)
         {
             if (MessageBox.Show("정말 모든 반향효과(Reverb) 프리셋 경로를 재설정 하시겠습니까?", "반향효과(Reverb) 프리셋 경로 모두 재설정",
                 MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
             { LastFolderDirectory = LastFileSaveDirectory = LastFolderDirectory = PresetDir; }
         }

         private void 프리셋열기경로재설정ToolStripMenuItem_Click(object sender, EventArgs e)
         {
             if (MessageBox.Show("정말 반향효과(Reverb) 프리셋 열기 경로를 재설정 하시겠습니까?", "반향효과(Reverb) 프리셋 열기 경로 재설정",
                 MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
             { LastFileOpenDirectory = PresetDir; }
         }

         private void 프리셋저장경로재설정ToolStripMenuItem1_Click(object sender, EventArgs e)
         {
             if (MessageBox.Show("정말 반향효과(Reverb) 프리셋 저장 경로를 재설정 하시겠습니까?", "반향효과(Reverb) 프리셋 열기 저장 재설정",
                 MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
             { LastFileSaveDirectory = PresetDir; }
         }

         private void 프리셋폴더경로재설정ToolStripMenuItem_Click(object sender, EventArgs e)
         {
             if (MessageBox.Show("정말 반향효과(Reverb) 폴더 경로를 재설정 하시겠습니까?", "반향효과(Reverb) 폴더 경로 재설정",
                 MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
             { LastFolderDirectory = PresetDir; }
         }
    }
}
