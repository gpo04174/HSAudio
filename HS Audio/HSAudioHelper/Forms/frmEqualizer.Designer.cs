﻿namespace HS_Audio_Helper.Forms
{
    partial class frmEqualizer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.맨위에표시ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.배경투명처리ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.분할자투명ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.테두리표시ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.설정값ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.열기OToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.저장SToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.설정창띄우기ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.peak띄우기ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.스펙트럼모양ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.fFTToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.웨이브데이터ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.그래프모양ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.그래프ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.파동ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.스펙트럼할당방법ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.fMODSystemToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.fMODChannelToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.fFTWindowToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripComboBox1 = new System.Windows.Forms.ToolStripComboBox();
            this.fFT사이즈ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripComboBox2 = new System.Windows.Forms.ToolStripComboBox();
            this.그래프굵기ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripTextBox1 = new System.Windows.Forms.ToolStripTextBox();
            this.똑같이나누기ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.그래프오차ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripTextBox2 = new System.Windows.Forms.ToolStripTextBox();
            this.timer2 = new System.Windows.Forms.Timer(this.components);
            this.lblFPS = new System.Windows.Forms.Label();
            this.lblLeft = new System.Windows.Forms.Label();
            this.lblRight = new System.Windows.Forms.Label();
            this.peakMeterCtrl1 = new Ernzo.WinForms.Controls.PeakMeterCtrl();
            this.contextMenuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // timer1
            // 
            this.timer1.Interval = 25;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.맨위에표시ToolStripMenuItem,
            this.배경투명처리ToolStripMenuItem,
            this.테두리표시ToolStripMenuItem,
            this.toolStripSeparator4,
            this.설정값ToolStripMenuItem,
            this.toolStripSeparator3,
            this.설정창띄우기ToolStripMenuItem,
            this.peak띄우기ToolStripMenuItem,
            this.toolStripSeparator1,
            this.스펙트럼모양ToolStripMenuItem,
            this.그래프모양ToolStripMenuItem,
            this.스펙트럼할당방법ToolStripMenuItem,
            this.toolStripSeparator2,
            this.fFTWindowToolStripMenuItem,
            this.fFT사이즈ToolStripMenuItem,
            this.그래프굵기ToolStripMenuItem,
            this.그래프오차ToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(191, 314);
            // 
            // 맨위에표시ToolStripMenuItem
            // 
            this.맨위에표시ToolStripMenuItem.CheckOnClick = true;
            this.맨위에표시ToolStripMenuItem.Image = global::HS_Audio.Properties.Resources.PushpinHS;
            this.맨위에표시ToolStripMenuItem.Name = "맨위에표시ToolStripMenuItem";
            this.맨위에표시ToolStripMenuItem.Size = new System.Drawing.Size(190, 22);
            this.맨위에표시ToolStripMenuItem.Text = "맨위에 표시";
            this.맨위에표시ToolStripMenuItem.ToolTipText = "이 창을 맨위로 설정합니다.";
            // 
            // 배경투명처리ToolStripMenuItem
            // 
            this.배경투명처리ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.분할자투명ToolStripMenuItem});
            this.배경투명처리ToolStripMenuItem.Name = "배경투명처리ToolStripMenuItem";
            this.배경투명처리ToolStripMenuItem.Size = new System.Drawing.Size(190, 22);
            this.배경투명처리ToolStripMenuItem.Text = "배경 투명처리";
            this.배경투명처리ToolStripMenuItem.ToolTipText = "백그라운드 배경을 투명 처리합니다.\r\n[단축키: F4]";
            // 
            // 분할자투명ToolStripMenuItem
            // 
            this.분할자투명ToolStripMenuItem.CheckOnClick = true;
            this.분할자투명ToolStripMenuItem.Name = "분할자투명ToolStripMenuItem";
            this.분할자투명ToolStripMenuItem.Size = new System.Drawing.Size(138, 22);
            this.분할자투명ToolStripMenuItem.Text = "분할자 투명";
            // 
            // 테두리표시ToolStripMenuItem
            // 
            this.테두리표시ToolStripMenuItem.Checked = true;
            this.테두리표시ToolStripMenuItem.CheckOnClick = true;
            this.테두리표시ToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.테두리표시ToolStripMenuItem.Name = "테두리표시ToolStripMenuItem";
            this.테두리표시ToolStripMenuItem.Size = new System.Drawing.Size(190, 22);
            this.테두리표시ToolStripMenuItem.Text = "테두리 표시";
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(187, 6);
            // 
            // 설정값ToolStripMenuItem
            // 
            this.설정값ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.열기OToolStripMenuItem,
            this.저장SToolStripMenuItem});
            this.설정값ToolStripMenuItem.Name = "설정값ToolStripMenuItem";
            this.설정값ToolStripMenuItem.Size = new System.Drawing.Size(190, 22);
            this.설정값ToolStripMenuItem.Text = "설정값";
            // 
            // 열기OToolStripMenuItem
            // 
            this.열기OToolStripMenuItem.Name = "열기OToolStripMenuItem";
            this.열기OToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.O)));
            this.열기OToolStripMenuItem.Size = new System.Drawing.Size(158, 22);
            this.열기OToolStripMenuItem.Text = "열기(&O)";
            // 
            // 저장SToolStripMenuItem
            // 
            this.저장SToolStripMenuItem.Name = "저장SToolStripMenuItem";
            this.저장SToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
            this.저장SToolStripMenuItem.Size = new System.Drawing.Size(158, 22);
            this.저장SToolStripMenuItem.Text = "저장(&S)";
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(187, 6);
            // 
            // 설정창띄우기ToolStripMenuItem
            // 
            this.설정창띄우기ToolStripMenuItem.Name = "설정창띄우기ToolStripMenuItem";
            this.설정창띄우기ToolStripMenuItem.Size = new System.Drawing.Size(190, 22);
            this.설정창띄우기ToolStripMenuItem.Text = "고급 설정 창 띄우기";
            this.설정창띄우기ToolStripMenuItem.ToolTipText = "그래프의 고급 설정창을 띄웁니다.\r\n(스펙트럼 설정 이외에는 설정하지 말아주세요) ";
            // 
            // peak띄우기ToolStripMenuItem
            // 
            this.peak띄우기ToolStripMenuItem.Name = "peak띄우기ToolStripMenuItem";
            this.peak띄우기ToolStripMenuItem.Size = new System.Drawing.Size(190, 22);
            this.peak띄우기ToolStripMenuItem.Text = "Peak 띄우기";
            this.peak띄우기ToolStripMenuItem.ToolTipText = "새롭게 Peak미터창을 띄웁니다.";
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(187, 6);
            // 
            // 스펙트럼모양ToolStripMenuItem
            // 
            this.스펙트럼모양ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fFTToolStripMenuItem,
            this.웨이브데이터ToolStripMenuItem});
            this.스펙트럼모양ToolStripMenuItem.Name = "스펙트럼모양ToolStripMenuItem";
            this.스펙트럼모양ToolStripMenuItem.Size = new System.Drawing.Size(190, 22);
            this.스펙트럼모양ToolStripMenuItem.Text = "스펙트럼 그리기 방법";
            // 
            // fFTToolStripMenuItem
            // 
            this.fFTToolStripMenuItem.Checked = true;
            this.fFTToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.fFTToolStripMenuItem.Name = "fFTToolStripMenuItem";
            this.fFTToolStripMenuItem.Size = new System.Drawing.Size(150, 22);
            this.fFTToolStripMenuItem.Text = "FFT-EQ";
            this.fFTToolStripMenuItem.ToolTipText = "FFT그래프를 Hz대역으로 그립니다.";
            // 
            // 웨이브데이터ToolStripMenuItem
            // 
            this.웨이브데이터ToolStripMenuItem.Name = "웨이브데이터ToolStripMenuItem";
            this.웨이브데이터ToolStripMenuItem.Size = new System.Drawing.Size(150, 22);
            this.웨이브데이터ToolStripMenuItem.Text = "웨이브 데이터";
            this.웨이브데이터ToolStripMenuItem.ToolTipText = "출력사운드를 그래프로 그립니다.";
            // 
            // 그래프모양ToolStripMenuItem
            // 
            this.그래프모양ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.그래프ToolStripMenuItem,
            this.파동ToolStripMenuItem});
            this.그래프모양ToolStripMenuItem.Name = "그래프모양ToolStripMenuItem";
            this.그래프모양ToolStripMenuItem.Size = new System.Drawing.Size(190, 22);
            this.그래프모양ToolStripMenuItem.Text = "그래프 모양";
            // 
            // 그래프ToolStripMenuItem
            // 
            this.그래프ToolStripMenuItem.Name = "그래프ToolStripMenuItem";
            this.그래프ToolStripMenuItem.Size = new System.Drawing.Size(110, 22);
            this.그래프ToolStripMenuItem.Text = "그래프";
            this.그래프ToolStripMenuItem.ToolTipText = "막대 형태로 표시됩니다.";
            // 
            // 파동ToolStripMenuItem
            // 
            this.파동ToolStripMenuItem.Checked = true;
            this.파동ToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.파동ToolStripMenuItem.Name = "파동ToolStripMenuItem";
            this.파동ToolStripMenuItem.Size = new System.Drawing.Size(110, 22);
            this.파동ToolStripMenuItem.Text = "파동";
            this.파동ToolStripMenuItem.ToolTipText = "선 형태로 표시됩니다.";
            // 
            // 스펙트럼할당방법ToolStripMenuItem
            // 
            this.스펙트럼할당방법ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fMODSystemToolStripMenuItem,
            this.fMODChannelToolStripMenuItem});
            this.스펙트럼할당방법ToolStripMenuItem.Name = "스펙트럼할당방법ToolStripMenuItem";
            this.스펙트럼할당방법ToolStripMenuItem.Size = new System.Drawing.Size(190, 22);
            this.스펙트럼할당방법ToolStripMenuItem.Text = "스펙트럼 할당 방법";
            // 
            // fMODSystemToolStripMenuItem
            // 
            this.fMODSystemToolStripMenuItem.Checked = true;
            this.fMODSystemToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.fMODSystemToolStripMenuItem.Name = "fMODSystemToolStripMenuItem";
            this.fMODSystemToolStripMenuItem.Size = new System.Drawing.Size(143, 22);
            this.fMODSystemToolStripMenuItem.Text = "Main Output";
            this.fMODSystemToolStripMenuItem.ToolTipText = "마스터 출력 입니다.";
            // 
            // fMODChannelToolStripMenuItem
            // 
            this.fMODChannelToolStripMenuItem.Name = "fMODChannelToolStripMenuItem";
            this.fMODChannelToolStripMenuItem.Size = new System.Drawing.Size(143, 22);
            this.fMODChannelToolStripMenuItem.Text = "Channel";
            this.fMODChannelToolStripMenuItem.ToolTipText = "개별 출력 입니다.";
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(187, 6);
            // 
            // fFTWindowToolStripMenuItem
            // 
            this.fFTWindowToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripComboBox1});
            this.fFTWindowToolStripMenuItem.Name = "fFTWindowToolStripMenuItem";
            this.fFTWindowToolStripMenuItem.Size = new System.Drawing.Size(190, 22);
            this.fFTWindowToolStripMenuItem.Text = "FFT Window";
            this.fFTWindowToolStripMenuItem.ToolTipText = "FFT Window를 계산할 방법입니다.\r\n(해당항목을 선택하고 마우스를 올리면 계산식이 표시됩니다.)";
            // 
            // toolStripComboBox1
            // 
            this.toolStripComboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.toolStripComboBox1.Items.AddRange(new object[] {
            "RECT",
            "TRIANGLE",
            "HAMMING",
            "HANNING",
            "BLACKMAN",
            "BLACKMANHARRIS",
            "MAX"});
            this.toolStripComboBox1.Name = "toolStripComboBox1";
            this.toolStripComboBox1.Size = new System.Drawing.Size(121, 23);
            // 
            // fFT사이즈ToolStripMenuItem
            // 
            this.fFT사이즈ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripComboBox2});
            this.fFT사이즈ToolStripMenuItem.Name = "fFT사이즈ToolStripMenuItem";
            this.fFT사이즈ToolStripMenuItem.Size = new System.Drawing.Size(190, 22);
            this.fFT사이즈ToolStripMenuItem.Text = "FFT 사이즈";
            // 
            // toolStripComboBox2
            // 
            this.toolStripComboBox2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.toolStripComboBox2.Items.AddRange(new object[] {
            "4096",
            "2048",
            "1024",
            "512",
            "256",
            "128",
            "64",
            "None"});
            this.toolStripComboBox2.Name = "toolStripComboBox2";
            this.toolStripComboBox2.Size = new System.Drawing.Size(121, 23);
            // 
            // 그래프굵기ToolStripMenuItem
            // 
            this.그래프굵기ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripTextBox1,
            this.똑같이나누기ToolStripMenuItem});
            this.그래프굵기ToolStripMenuItem.Name = "그래프굵기ToolStripMenuItem";
            this.그래프굵기ToolStripMenuItem.Size = new System.Drawing.Size(190, 22);
            this.그래프굵기ToolStripMenuItem.Text = "그래프 굵기";
            // 
            // toolStripTextBox1
            // 
            this.toolStripTextBox1.Name = "toolStripTextBox1";
            this.toolStripTextBox1.Size = new System.Drawing.Size(100, 23);
            this.toolStripTextBox1.Text = "1";
            // 
            // 똑같이나누기ToolStripMenuItem
            // 
            this.똑같이나누기ToolStripMenuItem.Checked = true;
            this.똑같이나누기ToolStripMenuItem.CheckOnClick = true;
            this.똑같이나누기ToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.똑같이나누기ToolStripMenuItem.Name = "똑같이나누기ToolStripMenuItem";
            this.똑같이나누기ToolStripMenuItem.Size = new System.Drawing.Size(160, 22);
            this.똑같이나누기ToolStripMenuItem.Text = "똑같이 나누기";
            this.똑같이나누기ToolStripMenuItem.ToolTipText = "굵기에 비례해서 Hz대역도 똑같이 나눕니다.";
            // 
            // 그래프오차ToolStripMenuItem
            // 
            this.그래프오차ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripTextBox2});
            this.그래프오차ToolStripMenuItem.Name = "그래프오차ToolStripMenuItem";
            this.그래프오차ToolStripMenuItem.Size = new System.Drawing.Size(190, 22);
            this.그래프오차ToolStripMenuItem.Text = "그래프 오차";
            // 
            // toolStripTextBox2
            // 
            this.toolStripTextBox2.Name = "toolStripTextBox2";
            this.toolStripTextBox2.Size = new System.Drawing.Size(100, 23);
            this.toolStripTextBox2.Text = "2";
            this.toolStripTextBox2.ToolTipText = "이 값을 크게주면 dB 측정범위가 늘어나긴하지만\r\n작은소리도 그래프가 커져서 측정이 정확하지 않을 수 있습니다.\r\n상황에 맞게 적절하게 사용하십시" +
    "오";
            // 
            // timer2
            // 
            this.timer2.Interval = 25;
            this.timer2.Tick += new System.EventHandler(this.timer2_Tick);
            // 
            // lblFPS
            // 
            this.lblFPS.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblFPS.AutoSize = true;
            this.lblFPS.BackColor = System.Drawing.Color.Transparent;
            this.lblFPS.Font = new System.Drawing.Font("굴림", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblFPS.ForeColor = System.Drawing.Color.Blue;
            this.lblFPS.Location = new System.Drawing.Point(1183, 6);
            this.lblFPS.Name = "lblFPS";
            this.lblFPS.Size = new System.Drawing.Size(88, 21);
            this.lblFPS.TabIndex = 2;
            this.lblFPS.Text = "FPS: 00";
            // 
            // lblLeft
            // 
            this.lblLeft.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblLeft.AutoSize = true;
            this.lblLeft.BackColor = System.Drawing.Color.Transparent;
            this.lblLeft.Font = new System.Drawing.Font("굴림", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblLeft.Location = new System.Drawing.Point(1005, 9);
            this.lblLeft.Name = "lblLeft";
            this.lblLeft.Size = new System.Drawing.Size(136, 21);
            this.lblLeft.TabIndex = 4;
            this.lblLeft.Text = "좌: 0.000 dB";
            // 
            // lblRight
            // 
            this.lblRight.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblRight.AutoSize = true;
            this.lblRight.BackColor = System.Drawing.Color.Transparent;
            this.lblRight.Font = new System.Drawing.Font("굴림", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblRight.Location = new System.Drawing.Point(1005, 34);
            this.lblRight.Name = "lblRight";
            this.lblRight.Size = new System.Drawing.Size(136, 21);
            this.lblRight.TabIndex = 5;
            this.lblRight.Text = "우: 0.000 dB";
            // 
            // peakMeterCtrl1
            // 
            this.peakMeterCtrl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.peakMeterCtrl1.BackColor = System.Drawing.Color.Transparent;
            this.peakMeterCtrl1.BandsCount = 256;
            this.peakMeterCtrl1.ColorHigh = System.Drawing.Color.Red;
            this.peakMeterCtrl1.ColorHighBack = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(150)))), ((int)(((byte)(150)))));
            this.peakMeterCtrl1.ColorMedium = System.Drawing.Color.Yellow;
            this.peakMeterCtrl1.ColorMediumBack = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(150)))));
            this.peakMeterCtrl1.ColorNormal = System.Drawing.Color.Green;
            this.peakMeterCtrl1.ColorNormalBack = System.Drawing.Color.FromArgb(((int)(((byte)(150)))), ((int)(((byte)(255)))), ((int)(((byte)(150)))));
            this.peakMeterCtrl1.LED눈금갯수 = 52;
            this.peakMeterCtrl1.Location = new System.Drawing.Point(1, 3);
            this.peakMeterCtrl1.Name = "peakMeterCtrl1";
            this.peakMeterCtrl1.Peak굵기 = 2;
            this.peakMeterCtrl1.Peak색상 = System.Drawing.Color.Black;
            this.peakMeterCtrl1.Peak속도 = 10;
            this.peakMeterCtrl1.Size = new System.Drawing.Size(1284, 312);
            this.peakMeterCtrl1.TabIndex = 0;
            this.peakMeterCtrl1.Text = "peakMeterCtrl1";
            this.peakMeterCtrl1.모눈선색상 = System.Drawing.Color.LightGray;
            this.peakMeterCtrl1.모눈선표시 = false;
            // 
            // frmEqualizer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1284, 318);
            this.Controls.Add(this.lblRight);
            this.Controls.Add(this.lblLeft);
            this.Controls.Add(this.lblFPS);
            this.Controls.Add(this.peakMeterCtrl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
            this.MaximizeBox = false;
            this.Name = "frmEqualizer";
            this.Text = "이퀄라이저 그래프 [좌: 0.000 dB / 우: 0.000 dB] (FPS: 00)";
            this.Load += new System.EventHandler(this.frmEqualizer_Load);
            this.contextMenuStrip1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Ernzo.WinForms.Controls.PeakMeterCtrl peakMeterCtrl1;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem 맨위에표시ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 배경투명처리ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 분할자투명ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 테두리표시ToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripMenuItem 설정값ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 열기OToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 저장SToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripMenuItem 설정창띄우기ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem peak띄우기ToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem 스펙트럼모양ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem fFTToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 웨이브데이터ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 그래프모양ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 그래프ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 파동ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 스펙트럼할당방법ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem fMODSystemToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem fMODChannelToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem fFTWindowToolStripMenuItem;
        private System.Windows.Forms.ToolStripComboBox toolStripComboBox1;
        private System.Windows.Forms.ToolStripMenuItem fFT사이즈ToolStripMenuItem;
        private System.Windows.Forms.ToolStripComboBox toolStripComboBox2;
        private System.Windows.Forms.ToolStripMenuItem 그래프굵기ToolStripMenuItem;
        private System.Windows.Forms.ToolStripTextBox toolStripTextBox1;
        private System.Windows.Forms.ToolStripMenuItem 똑같이나누기ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 그래프오차ToolStripMenuItem;
        private System.Windows.Forms.ToolStripTextBox toolStripTextBox2;
        private System.Windows.Forms.Timer timer2;
        private System.Windows.Forms.Label lblFPS;
        private System.Windows.Forms.Label lblLeft;
        private System.Windows.Forms.Label lblRight;
    }
}