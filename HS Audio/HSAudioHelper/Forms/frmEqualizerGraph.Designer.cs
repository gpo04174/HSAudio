﻿namespace HS_Audio.Forms
{
    partial class frmEqualizerGraph
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.맨위에표시ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.배경투명처리ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.그래프색깔ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.한계치에도달하면색반전ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator7 = new System.Windows.Forms.ToolStripSeparator();
            this.라벨색깔ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.한계치를넘을때색반전ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator8 = new System.Windows.Forms.ToolStripSeparator();
            this.peak색ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.fPS색ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator6 = new System.Windows.Forms.ToolStripSeparator();
            this.낮은색ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.중간색ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.높은색ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.배경밝기ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripComboBox3 = new System.Windows.Forms.ToolStripComboBox();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.라벨ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.fPS라벨표시ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.좌채널라벨표시ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.우채널라벨표시ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.채널ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.좌채널ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.우채널ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.할당방법ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.fMODSystemToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.fMODChannelToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.그래프빈도ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripComboBox4 = new System.Windows.Forms.ToolStripComboBox();
            this.설정값ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.열기OToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.저장SToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.fFTWindowToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripComboBox1 = new System.Windows.Forms.ToolStripComboBox();
            this.fFT사이즈ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripComboBox2 = new System.Windows.Forms.ToolStripComboBox();
            this.그래프오차ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripTextBox2 = new System.Windows.Forms.ToolStripTextBox();
            this.timer2 = new System.Windows.Forms.Timer(this.components);
            this.lblFPS = new System.Windows.Forms.Label();
            this.lblLeft = new System.Windows.Forms.Label();
            this.lblRight = new System.Windows.Forms.Label();
            this.cdHIGHdBColor = new System.Windows.Forms.ColorDialog();
            this.cdMediumdBColor = new System.Windows.Forms.ColorDialog();
            this.cdLOWdBColor = new System.Windows.Forms.ColorDialog();
            this.cdPeekColor = new System.Windows.Forms.ColorDialog();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.cdLabelColor = new System.Windows.Forms.ColorDialog();
            this.cdFPSColor = new System.Windows.Forms.ColorDialog();
            this.평균값ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.측정범위ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripTextBox1 = new System.Windows.Forms.ToolStripTextBox();
            this.logarithmToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator9 = new System.Windows.Forms.ToolStripSeparator();
            this.peakMeterCtrl1 = new Ernzo.WinForms.Controls.PeakMeterCtrl();
            this.dB라벨표시ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.contextMenuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // timer1
            // 
            this.timer1.Interval = 25;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.맨위에표시ToolStripMenuItem,
            this.배경투명처리ToolStripMenuItem,
            this.toolStripSeparator3,
            this.그래프색깔ToolStripMenuItem,
            this.배경밝기ToolStripMenuItem,
            this.toolStripSeparator5,
            this.라벨ToolStripMenuItem,
            this.toolStripSeparator4,
            this.채널ToolStripMenuItem,
            this.할당방법ToolStripMenuItem,
            this.그래프빈도ToolStripMenuItem,
            this.설정값ToolStripMenuItem,
            this.toolStripSeparator1,
            this.측정범위ToolStripMenuItem,
            this.fFTWindowToolStripMenuItem,
            this.fFT사이즈ToolStripMenuItem,
            this.그래프오차ToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(157, 388);
            // 
            // 맨위에표시ToolStripMenuItem
            // 
            this.맨위에표시ToolStripMenuItem.CheckOnClick = true;
            this.맨위에표시ToolStripMenuItem.Image = global::HS_Audio.Properties.Resources.PushpinHS;
            this.맨위에표시ToolStripMenuItem.Name = "맨위에표시ToolStripMenuItem";
            this.맨위에표시ToolStripMenuItem.Size = new System.Drawing.Size(156, 26);
            this.맨위에표시ToolStripMenuItem.Text = "맨 위에 표시";
            this.맨위에표시ToolStripMenuItem.ToolTipText = "이 창을 맨위로 설정합니다.";
            this.맨위에표시ToolStripMenuItem.CheckedChanged += new System.EventHandler(this.맨위에표시ToolStripMenuItem_CheckedChanged);
            // 
            // 배경투명처리ToolStripMenuItem
            // 
            this.배경투명처리ToolStripMenuItem.AutoToolTip = true;
            this.배경투명처리ToolStripMenuItem.Name = "배경투명처리ToolStripMenuItem";
            this.배경투명처리ToolStripMenuItem.Size = new System.Drawing.Size(156, 26);
            this.배경투명처리ToolStripMenuItem.Text = "배경 투명처리";
            this.배경투명처리ToolStripMenuItem.ToolTipText = "백그라운드 배경을 투명 처리합니다.\r\n[단축키: F4]";
            this.배경투명처리ToolStripMenuItem.Click += new System.EventHandler(this.배경투명처리ToolStripMenuItem_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(153, 6);
            // 
            // 그래프색깔ToolStripMenuItem
            // 
            this.그래프색깔ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.한계치에도달하면색반전ToolStripMenuItem,
            this.toolStripSeparator7,
            this.라벨색깔ToolStripMenuItem,
            this.toolStripSeparator8,
            this.peak색ToolStripMenuItem,
            this.fPS색ToolStripMenuItem,
            this.toolStripSeparator6,
            this.높은색ToolStripMenuItem,
            this.중간색ToolStripMenuItem,
            this.낮은색ToolStripMenuItem});
            this.그래프색깔ToolStripMenuItem.Name = "그래프색깔ToolStripMenuItem";
            this.그래프색깔ToolStripMenuItem.Size = new System.Drawing.Size(156, 26);
            this.그래프색깔ToolStripMenuItem.Text = "그래프 색깔";
            // 
            // 한계치에도달하면색반전ToolStripMenuItem
            // 
            this.한계치에도달하면색반전ToolStripMenuItem.CheckOnClick = true;
            this.한계치에도달하면색반전ToolStripMenuItem.Name = "한계치에도달하면색반전ToolStripMenuItem";
            this.한계치에도달하면색반전ToolStripMenuItem.Size = new System.Drawing.Size(218, 22);
            this.한계치에도달하면색반전ToolStripMenuItem.Text = "한계치에 도달하면 색 반전";
            // 
            // toolStripSeparator7
            // 
            this.toolStripSeparator7.Name = "toolStripSeparator7";
            this.toolStripSeparator7.Size = new System.Drawing.Size(215, 6);
            // 
            // 라벨색깔ToolStripMenuItem
            // 
            this.라벨색깔ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.한계치를넘을때색반전ToolStripMenuItem});
            this.라벨색깔ToolStripMenuItem.Name = "라벨색깔ToolStripMenuItem";
            this.라벨색깔ToolStripMenuItem.Size = new System.Drawing.Size(218, 22);
            this.라벨색깔ToolStripMenuItem.Text = "채널 라벨 색 설정";
            this.라벨색깔ToolStripMenuItem.Click += new System.EventHandler(this.라벨색깔ToolStripMenuItem_Click);
            // 
            // 한계치를넘을때색반전ToolStripMenuItem
            // 
            this.한계치를넘을때색반전ToolStripMenuItem.Checked = true;
            this.한계치를넘을때색반전ToolStripMenuItem.CheckOnClick = true;
            this.한계치를넘을때색반전ToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.한계치를넘을때색반전ToolStripMenuItem.Name = "한계치를넘을때색반전ToolStripMenuItem";
            this.한계치를넘을때색반전ToolStripMenuItem.Size = new System.Drawing.Size(218, 22);
            this.한계치를넘을때색반전ToolStripMenuItem.Text = "한계치에 도달하면 색 반전";
            // 
            // toolStripSeparator8
            // 
            this.toolStripSeparator8.Name = "toolStripSeparator8";
            this.toolStripSeparator8.Size = new System.Drawing.Size(215, 6);
            // 
            // peak색ToolStripMenuItem
            // 
            this.peak색ToolStripMenuItem.Name = "peak색ToolStripMenuItem";
            this.peak색ToolStripMenuItem.Size = new System.Drawing.Size(218, 22);
            this.peak색ToolStripMenuItem.Text = "Peek 색 설정";
            this.peak색ToolStripMenuItem.Click += new System.EventHandler(this.peak색ToolStripMenuItem_Click);
            // 
            // fPS색ToolStripMenuItem
            // 
            this.fPS색ToolStripMenuItem.Name = "fPS색ToolStripMenuItem";
            this.fPS색ToolStripMenuItem.Size = new System.Drawing.Size(218, 22);
            this.fPS색ToolStripMenuItem.Text = "FPS 색 설정";
            this.fPS색ToolStripMenuItem.Click += new System.EventHandler(this.fPS색ToolStripMenuItem_Click);
            // 
            // toolStripSeparator6
            // 
            this.toolStripSeparator6.Name = "toolStripSeparator6";
            this.toolStripSeparator6.Size = new System.Drawing.Size(215, 6);
            // 
            // 낮은색ToolStripMenuItem
            // 
            this.낮은색ToolStripMenuItem.Name = "낮은색ToolStripMenuItem";
            this.낮은색ToolStripMenuItem.Size = new System.Drawing.Size(218, 22);
            this.낮은색ToolStripMenuItem.Text = "낮은 색 설정";
            this.낮은색ToolStripMenuItem.Click += new System.EventHandler(this.낮은색ToolStripMenuItem_Click);
            // 
            // 중간색ToolStripMenuItem
            // 
            this.중간색ToolStripMenuItem.Name = "중간색ToolStripMenuItem";
            this.중간색ToolStripMenuItem.Size = new System.Drawing.Size(218, 22);
            this.중간색ToolStripMenuItem.Text = "중간 색 설정";
            this.중간색ToolStripMenuItem.Click += new System.EventHandler(this.중간색ToolStripMenuItem_Click);
            // 
            // 높은색ToolStripMenuItem
            // 
            this.높은색ToolStripMenuItem.Name = "높은색ToolStripMenuItem";
            this.높은색ToolStripMenuItem.Size = new System.Drawing.Size(218, 22);
            this.높은색ToolStripMenuItem.Text = "높은 색 설정";
            this.높은색ToolStripMenuItem.Click += new System.EventHandler(this.높은색ToolStripMenuItem_Click);
            // 
            // 배경밝기ToolStripMenuItem
            // 
            this.배경밝기ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripComboBox3});
            this.배경밝기ToolStripMenuItem.Name = "배경밝기ToolStripMenuItem";
            this.배경밝기ToolStripMenuItem.Size = new System.Drawing.Size(156, 26);
            this.배경밝기ToolStripMenuItem.Text = "배경 밝기";
            // 
            // toolStripComboBox3
            // 
            this.toolStripComboBox3.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.toolStripComboBox3.Items.AddRange(new object[] {
            "00",
            "01",
            "02",
            "03",
            "04",
            "05",
            "06",
            "07",
            "08",
            "09",
            "10",
            "12",
            "13",
            "14",
            "15",
            "16",
            "17",
            "18",
            "19",
            "20",
            "21",
            "23",
            "24",
            "25",
            "26",
            "27",
            "28",
            "29",
            "30",
            "31",
            "32",
            "33",
            "34",
            "35",
            "36",
            "37",
            "38",
            "39",
            "40",
            "41",
            "42",
            "43",
            "44",
            "45",
            "46",
            "47",
            "48",
            "49",
            "50",
            "51",
            "52",
            "53",
            "54",
            "55",
            "56",
            "57",
            "58",
            "59",
            "60",
            "61",
            "62",
            "63",
            "64",
            "65",
            "66",
            "67",
            "68",
            "69",
            "70",
            "71",
            "72",
            "73",
            "74",
            "75",
            "76",
            "77",
            "78",
            "79",
            "80",
            "81",
            "82",
            "83",
            "84",
            "85",
            "86",
            "87",
            "88",
            "89",
            "90",
            "91",
            "92",
            "93",
            "94",
            "95",
            "96",
            "97",
            "98",
            "99",
            "100",
            "101",
            "102",
            "103",
            "104",
            "105",
            "106",
            "107",
            "108",
            "109",
            "110",
            "111",
            "112",
            "113",
            "114",
            "115",
            "116",
            "117",
            "118",
            "119",
            "120",
            "121",
            "122",
            "123",
            "124",
            "125",
            "126",
            "127",
            "128",
            "129",
            "130",
            "131",
            "132",
            "133",
            "134",
            "135",
            "136",
            "137",
            "138",
            "139",
            "140",
            "141",
            "142",
            "143",
            "144",
            "145",
            "146",
            "147",
            "148",
            "149",
            "150",
            "151",
            "152",
            "153",
            "154",
            "155",
            "156",
            "157",
            "158",
            "159",
            "160",
            "160",
            "161",
            "162",
            "163",
            "164",
            "165",
            "166",
            "167",
            "168",
            "169",
            "170",
            "171",
            "172",
            "173",
            "174",
            "175",
            "176",
            "177",
            "178",
            "179",
            "180",
            "181",
            "182",
            "183",
            "184",
            "185",
            "186",
            "187",
            "188",
            "189",
            "190",
            "191",
            "192",
            "193",
            "194",
            "195",
            "196",
            "197",
            "198",
            "199",
            "200",
            "201",
            "202",
            "203",
            "204",
            "205",
            "206",
            "207",
            "208",
            "209",
            "210",
            "211",
            "212",
            "213",
            "214",
            "215",
            "216",
            "217",
            "218",
            "219",
            "220",
            "221",
            "222",
            "223",
            "224",
            "225",
            "226",
            "227",
            "228",
            "229",
            "230",
            "231",
            "232",
            "233",
            "234",
            "235",
            "236",
            "237",
            "238",
            "239",
            "240",
            "241",
            "242",
            "243",
            "244",
            "245",
            "246",
            "247",
            "248",
            "249",
            "250",
            "251",
            "252",
            "253",
            "254",
            "255"});
            this.toolStripComboBox3.Name = "toolStripComboBox3";
            this.toolStripComboBox3.Size = new System.Drawing.Size(121, 23);
            this.toolStripComboBox3.SelectedIndexChanged += new System.EventHandler(this.toolStripComboBox3_SelectedIndexChanged);
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(153, 6);
            // 
            // 라벨ToolStripMenuItem
            // 
            this.라벨ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fPS라벨표시ToolStripMenuItem,
            this.dB라벨표시ToolStripMenuItem,
            this.toolStripSeparator2,
            this.좌채널라벨표시ToolStripMenuItem,
            this.우채널라벨표시ToolStripMenuItem});
            this.라벨ToolStripMenuItem.Name = "라벨ToolStripMenuItem";
            this.라벨ToolStripMenuItem.Size = new System.Drawing.Size(156, 26);
            this.라벨ToolStripMenuItem.Text = "라벨 표시";
            // 
            // fPS라벨표시ToolStripMenuItem
            // 
            this.fPS라벨표시ToolStripMenuItem.Checked = true;
            this.fPS라벨표시ToolStripMenuItem.CheckOnClick = true;
            this.fPS라벨표시ToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.fPS라벨표시ToolStripMenuItem.Name = "fPS라벨표시ToolStripMenuItem";
            this.fPS라벨표시ToolStripMenuItem.Size = new System.Drawing.Size(170, 22);
            this.fPS라벨표시ToolStripMenuItem.Text = "FPS 라벨 표시";
            this.fPS라벨표시ToolStripMenuItem.CheckedChanged += new System.EventHandler(this.fPS라벨표시ToolStripMenuItem_CheckedChanged);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(167, 6);
            // 
            // 좌채널라벨표시ToolStripMenuItem
            // 
            this.좌채널라벨표시ToolStripMenuItem.Checked = true;
            this.좌채널라벨표시ToolStripMenuItem.CheckOnClick = true;
            this.좌채널라벨표시ToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.좌채널라벨표시ToolStripMenuItem.Name = "좌채널라벨표시ToolStripMenuItem";
            this.좌채널라벨표시ToolStripMenuItem.Size = new System.Drawing.Size(170, 22);
            this.좌채널라벨표시ToolStripMenuItem.Text = "좌 채널 라벨 표시";
            this.좌채널라벨표시ToolStripMenuItem.CheckedChanged += new System.EventHandler(this.좌채널라벨표시ToolStripMenuItem_CheckedChanged);
            // 
            // 우채널라벨표시ToolStripMenuItem
            // 
            this.우채널라벨표시ToolStripMenuItem.Checked = true;
            this.우채널라벨표시ToolStripMenuItem.CheckOnClick = true;
            this.우채널라벨표시ToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.우채널라벨표시ToolStripMenuItem.Name = "우채널라벨표시ToolStripMenuItem";
            this.우채널라벨표시ToolStripMenuItem.Size = new System.Drawing.Size(170, 22);
            this.우채널라벨표시ToolStripMenuItem.Text = "우 채널 라벨 표시";
            this.우채널라벨표시ToolStripMenuItem.CheckedChanged += new System.EventHandler(this.우채널라벨표시ToolStripMenuItem_CheckedChanged);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(153, 6);
            // 
            // 채널ToolStripMenuItem
            // 
            this.채널ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.좌채널ToolStripMenuItem,
            this.우채널ToolStripMenuItem,
            this.평균값ToolStripMenuItem});
            this.채널ToolStripMenuItem.Name = "채널ToolStripMenuItem";
            this.채널ToolStripMenuItem.Size = new System.Drawing.Size(156, 26);
            this.채널ToolStripMenuItem.Text = "채널";
            // 
            // 좌채널ToolStripMenuItem
            // 
            this.좌채널ToolStripMenuItem.Checked = true;
            this.좌채널ToolStripMenuItem.CheckOnClick = true;
            this.좌채널ToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.좌채널ToolStripMenuItem.Name = "좌채널ToolStripMenuItem";
            this.좌채널ToolStripMenuItem.Size = new System.Drawing.Size(114, 22);
            this.좌채널ToolStripMenuItem.Text = "좌 채널";
            this.좌채널ToolStripMenuItem.Click += new System.EventHandler(this.좌채널ToolStripMenuItem_Click);
            // 
            // 우채널ToolStripMenuItem
            // 
            this.우채널ToolStripMenuItem.CheckOnClick = true;
            this.우채널ToolStripMenuItem.Name = "우채널ToolStripMenuItem";
            this.우채널ToolStripMenuItem.Size = new System.Drawing.Size(114, 22);
            this.우채널ToolStripMenuItem.Text = "우 채널";
            this.우채널ToolStripMenuItem.Click += new System.EventHandler(this.좌채널ToolStripMenuItem_Click);
            // 
            // 할당방법ToolStripMenuItem
            // 
            this.할당방법ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fMODSystemToolStripMenuItem,
            this.fMODChannelToolStripMenuItem});
            this.할당방법ToolStripMenuItem.Name = "할당방법ToolStripMenuItem";
            this.할당방법ToolStripMenuItem.Size = new System.Drawing.Size(156, 26);
            this.할당방법ToolStripMenuItem.Text = "할당 방법";
            // 
            // fMODSystemToolStripMenuItem
            // 
            this.fMODSystemToolStripMenuItem.Checked = true;
            this.fMODSystemToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.fMODSystemToolStripMenuItem.Name = "fMODSystemToolStripMenuItem";
            this.fMODSystemToolStripMenuItem.Size = new System.Drawing.Size(143, 22);
            this.fMODSystemToolStripMenuItem.Text = "Main Output";
            this.fMODSystemToolStripMenuItem.ToolTipText = "마스터 출력 입니다.";
            this.fMODSystemToolStripMenuItem.Click += new System.EventHandler(this.fMODSystemToolStripMenuItem_Click);
            // 
            // fMODChannelToolStripMenuItem
            // 
            this.fMODChannelToolStripMenuItem.Name = "fMODChannelToolStripMenuItem";
            this.fMODChannelToolStripMenuItem.Size = new System.Drawing.Size(143, 22);
            this.fMODChannelToolStripMenuItem.Text = "Channel";
            this.fMODChannelToolStripMenuItem.ToolTipText = "개별 출력 입니다.";
            this.fMODChannelToolStripMenuItem.Click += new System.EventHandler(this.fMODSystemToolStripMenuItem_Click);
            // 
            // 그래프빈도ToolStripMenuItem
            // 
            this.그래프빈도ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripComboBox4});
            this.그래프빈도ToolStripMenuItem.Name = "그래프빈도ToolStripMenuItem";
            this.그래프빈도ToolStripMenuItem.Size = new System.Drawing.Size(156, 26);
            this.그래프빈도ToolStripMenuItem.Text = "그래프 빈도";
            // 
            // toolStripComboBox4
            // 
            this.toolStripComboBox4.Items.AddRange(new object[] {
            "1",
            "25",
            "50",
            "75",
            "100",
            "125",
            "150",
            "175",
            "200",
            "225",
            "250",
            "275",
            "300",
            "325",
            "350",
            "375",
            "400",
            "425",
            "450",
            "475",
            "500",
            "525",
            "550",
            "575",
            "600",
            "625",
            "650",
            "675",
            "700",
            "725",
            "750",
            "775",
            "800",
            "825",
            "850",
            "875",
            "900",
            "925",
            "950",
            "975",
            "1000",
            "2000",
            "3000"});
            this.toolStripComboBox4.Name = "toolStripComboBox4";
            this.toolStripComboBox4.Size = new System.Drawing.Size(121, 23);
            this.toolStripComboBox4.Text = "25";
            this.toolStripComboBox4.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.toolStripComboBox4_KeyPress);
            this.toolStripComboBox4.TextChanged += new System.EventHandler(this.toolStripComboBox4_TextChanged);
            // 
            // 설정값ToolStripMenuItem
            // 
            this.설정값ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.열기OToolStripMenuItem,
            this.저장SToolStripMenuItem});
            this.설정값ToolStripMenuItem.Name = "설정값ToolStripMenuItem";
            this.설정값ToolStripMenuItem.Size = new System.Drawing.Size(156, 26);
            this.설정값ToolStripMenuItem.Text = "설정값";
            this.설정값ToolStripMenuItem.Visible = false;
            // 
            // 열기OToolStripMenuItem
            // 
            this.열기OToolStripMenuItem.Name = "열기OToolStripMenuItem";
            this.열기OToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.O)));
            this.열기OToolStripMenuItem.Size = new System.Drawing.Size(158, 22);
            this.열기OToolStripMenuItem.Text = "열기(&O)";
            // 
            // 저장SToolStripMenuItem
            // 
            this.저장SToolStripMenuItem.Name = "저장SToolStripMenuItem";
            this.저장SToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
            this.저장SToolStripMenuItem.Size = new System.Drawing.Size(158, 22);
            this.저장SToolStripMenuItem.Text = "저장(&S)";
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(153, 6);
            // 
            // fFTWindowToolStripMenuItem
            // 
            this.fFTWindowToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripComboBox1});
            this.fFTWindowToolStripMenuItem.Name = "fFTWindowToolStripMenuItem";
            this.fFTWindowToolStripMenuItem.Size = new System.Drawing.Size(156, 26);
            this.fFTWindowToolStripMenuItem.Text = "FFT Window";
            this.fFTWindowToolStripMenuItem.ToolTipText = "FFT Window를 계산할 방법입니다.\r\n(해당항목을 선택하고 마우스를 올리면 계산식이 표시됩니다.)";
            // 
            // toolStripComboBox1
            // 
            this.toolStripComboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.toolStripComboBox1.Items.AddRange(new object[] {
            "MAX",
            "RECT",
            "TRIANGLE",
            "HAMMING",
            "HANNING",
            "BLACKMAN",
            "BLACKMANHARRIS"});
            this.toolStripComboBox1.Name = "toolStripComboBox1";
            this.toolStripComboBox1.Size = new System.Drawing.Size(121, 23);
            this.toolStripComboBox1.SelectedIndexChanged += new System.EventHandler(this.toolStripComboBox1_SelectedIndexChanged);
            // 
            // fFT사이즈ToolStripMenuItem
            // 
            this.fFT사이즈ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripComboBox2});
            this.fFT사이즈ToolStripMenuItem.Name = "fFT사이즈ToolStripMenuItem";
            this.fFT사이즈ToolStripMenuItem.Size = new System.Drawing.Size(156, 26);
            this.fFT사이즈ToolStripMenuItem.Text = "FFT 사이즈";
            // 
            // toolStripComboBox2
            // 
            this.toolStripComboBox2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.toolStripComboBox2.Items.AddRange(new object[] {
            "4096",
            "2048",
            "1024",
            "512",
            "256",
            "128",
            "64",
            "None"});
            this.toolStripComboBox2.Name = "toolStripComboBox2";
            this.toolStripComboBox2.Size = new System.Drawing.Size(121, 23);
            this.toolStripComboBox2.SelectedIndexChanged += new System.EventHandler(this.toolStripComboBox2_SelectedIndexChanged);
            // 
            // 그래프오차ToolStripMenuItem
            // 
            this.그래프오차ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripTextBox2,
            this.toolStripSeparator9,
            this.logarithmToolStripMenuItem});
            this.그래프오차ToolStripMenuItem.Name = "그래프오차ToolStripMenuItem";
            this.그래프오차ToolStripMenuItem.Size = new System.Drawing.Size(156, 26);
            this.그래프오차ToolStripMenuItem.Text = "그래프 오차";
            // 
            // toolStripTextBox2
            // 
            this.toolStripTextBox2.Name = "toolStripTextBox2";
            this.toolStripTextBox2.Size = new System.Drawing.Size(100, 23);
            this.toolStripTextBox2.Text = "104";
            this.toolStripTextBox2.ToolTipText = "이 값을 크게주면 dB 측정범위가 늘어나긴하지만\r\n작은소리도 그래프가 커져서 측정이 정확하지 않을 수 있습니다.\r\n상황에 맞게 적절하게 사용하십시" +
    "오";
            this.toolStripTextBox2.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.toolStripTextBox2_KeyPress);
            this.toolStripTextBox2.TextChanged += new System.EventHandler(this.toolStripTextBox2_TextChanged);
            // 
            // timer2
            // 
            this.timer2.Interval = 25;
            this.timer2.Tick += new System.EventHandler(this.timer2_Tick);
            // 
            // lblFPS
            // 
            this.lblFPS.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblFPS.AutoSize = true;
            this.lblFPS.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(30)))), ((int)(((byte)(30)))));
            this.lblFPS.Font = new System.Drawing.Font("굴림", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblFPS.ForeColor = System.Drawing.Color.Lime;
            this.lblFPS.Location = new System.Drawing.Point(923, 6);
            this.lblFPS.Name = "lblFPS";
            this.lblFPS.Size = new System.Drawing.Size(88, 21);
            this.lblFPS.TabIndex = 2;
            this.lblFPS.Text = "FPS: 00";
            // 
            // lblLeft
            // 
            this.lblLeft.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblLeft.AutoSize = true;
            this.lblLeft.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(30)))), ((int)(((byte)(30)))));
            this.lblLeft.Font = new System.Drawing.Font("굴림", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblLeft.ForeColor = System.Drawing.Color.Aqua;
            this.lblLeft.Location = new System.Drawing.Point(705, 10);
            this.lblLeft.Name = "lblLeft";
            this.lblLeft.Size = new System.Drawing.Size(170, 21);
            this.lblLeft.TabIndex = 4;
            this.lblLeft.Text = "좌[?]: 0.000 dB";
            // 
            // lblRight
            // 
            this.lblRight.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblRight.AutoSize = true;
            this.lblRight.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(30)))), ((int)(((byte)(30)))));
            this.lblRight.Font = new System.Drawing.Font("굴림", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblRight.ForeColor = System.Drawing.Color.Aqua;
            this.lblRight.Location = new System.Drawing.Point(705, 35);
            this.lblRight.Name = "lblRight";
            this.lblRight.Size = new System.Drawing.Size(170, 21);
            this.lblRight.TabIndex = 5;
            this.lblRight.Text = "우[?]: 0.000 dB";
            // 
            // cdHIGHdBColor
            // 
            this.cdHIGHdBColor.Color = System.Drawing.Color.Red;
            // 
            // cdMediumdBColor
            // 
            this.cdMediumdBColor.Color = System.Drawing.Color.Yellow;
            // 
            // cdLOWdBColor
            // 
            this.cdLOWdBColor.Color = System.Drawing.Color.Lime;
            // 
            // cdPeekColor
            // 
            this.cdPeekColor.Color = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            // 
            // cdLabelColor
            // 
            this.cdLabelColor.Color = System.Drawing.Color.Aqua;
            // 
            // cdFPSColor
            // 
            this.cdFPSColor.Color = System.Drawing.Color.Lime;
            // 
            // 평균값ToolStripMenuItem
            // 
            this.평균값ToolStripMenuItem.Name = "평균값ToolStripMenuItem";
            this.평균값ToolStripMenuItem.Size = new System.Drawing.Size(114, 22);
            this.평균값ToolStripMenuItem.Text = "평균 값";
            this.평균값ToolStripMenuItem.Visible = false;
            // 
            // 측정범위ToolStripMenuItem
            // 
            this.측정범위ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripTextBox1});
            this.측정범위ToolStripMenuItem.Name = "측정범위ToolStripMenuItem";
            this.측정범위ToolStripMenuItem.Size = new System.Drawing.Size(156, 26);
            this.측정범위ToolStripMenuItem.Text = "측정 범위";
            // 
            // toolStripTextBox1
            // 
            this.toolStripTextBox1.Name = "toolStripTextBox1";
            this.toolStripTextBox1.Size = new System.Drawing.Size(100, 23);
            this.toolStripTextBox1.Text = "20";
            this.toolStripTextBox1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.toolStripTextBox1_KeyPress);
            this.toolStripTextBox1.TextChanged += new System.EventHandler(this.toolStripTextBox1_TextChanged);
            // 
            // logarithmToolStripMenuItem
            // 
            this.logarithmToolStripMenuItem.Checked = true;
            this.logarithmToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.logarithmToolStripMenuItem.Name = "logarithmToolStripMenuItem";
            this.logarithmToolStripMenuItem.Size = new System.Drawing.Size(160, 22);
            this.logarithmToolStripMenuItem.Text = "Logarithm";
            // 
            // toolStripSeparator9
            // 
            this.toolStripSeparator9.Name = "toolStripSeparator9";
            this.toolStripSeparator9.Size = new System.Drawing.Size(157, 6);
            // 
            // peakMeterCtrl1
            // 
            this.peakMeterCtrl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.peakMeterCtrl1.BackColor = System.Drawing.Color.Transparent;
            this.peakMeterCtrl1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.peakMeterCtrl1.BandsCount = 255;
            this.peakMeterCtrl1.ColorHigh = System.Drawing.Color.Red;
            this.peakMeterCtrl1.ColorHighBack = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(150)))), ((int)(((byte)(150)))));
            this.peakMeterCtrl1.ColorMedium = System.Drawing.Color.Yellow;
            this.peakMeterCtrl1.ColorMediumBack = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(150)))));
            this.peakMeterCtrl1.ColorNormal = System.Drawing.Color.Lime;
            this.peakMeterCtrl1.ColorNormalBack = System.Drawing.Color.FromArgb(((int)(((byte)(150)))), ((int)(((byte)(255)))), ((int)(((byte)(150)))));
            this.peakMeterCtrl1.ContextMenuStrip = this.contextMenuStrip1;
            this.peakMeterCtrl1.LED눈금갯수 = 30;
            this.peakMeterCtrl1.Location = new System.Drawing.Point(1, 3);
            this.peakMeterCtrl1.Name = "peakMeterCtrl1";
            this.peakMeterCtrl1.Peak굵기 = 2;
            this.peakMeterCtrl1.Peak색상 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.peakMeterCtrl1.Peak속도 = 10;
            this.peakMeterCtrl1.Size = new System.Drawing.Size(1023, 159);
            this.peakMeterCtrl1.TabIndex = 0;
            this.peakMeterCtrl1.Text = "peakMeterCtrl1";
            this.peakMeterCtrl1.모눈선색상 = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.peakMeterCtrl1.모눈선표시 = false;
            // 
            // dB라벨표시ToolStripMenuItem
            // 
            this.dB라벨표시ToolStripMenuItem.Checked = true;
            this.dB라벨표시ToolStripMenuItem.CheckOnClick = true;
            this.dB라벨표시ToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.dB라벨표시ToolStripMenuItem.Name = "dB라벨표시ToolStripMenuItem";
            this.dB라벨표시ToolStripMenuItem.Size = new System.Drawing.Size(170, 22);
            this.dB라벨표시ToolStripMenuItem.Text = "dB 라벨 표시";
            this.dB라벨표시ToolStripMenuItem.CheckedChanged += new System.EventHandler(this.dB라벨표시ToolStripMenuItem_CheckedChanged);
            // 
            // frmEqualizerGraph
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(30)))), ((int)(((byte)(30)))));
            this.ClientSize = new System.Drawing.Size(1023, 165);
            this.Controls.Add(this.lblRight);
            this.Controls.Add(this.lblLeft);
            this.Controls.Add(this.lblFPS);
            this.Controls.Add(this.peakMeterCtrl1);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
            this.MaximizeBox = false;
            this.Name = "frmEqualizerGraph";
            this.Text = "이퀄라이저 그래프 [좌: 0.000 dB / 우: 0.000 dB] (FPS: 00) - 계산식: [n] = (N==0) ? 0 :10.0 * L" +
    "og10(N^2) //n은 dB";
            this.Load += new System.EventHandler(this.frmEqualizer_Load);
            this.contextMenuStrip1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Ernzo.WinForms.Controls.PeakMeterCtrl peakMeterCtrl1;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem 맨위에표시ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 배경투명처리ToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripMenuItem 설정값ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 열기OToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 저장SToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripMenuItem 할당방법ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem fMODSystemToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem fMODChannelToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem fFTWindowToolStripMenuItem;
        private System.Windows.Forms.ToolStripComboBox toolStripComboBox1;
        private System.Windows.Forms.ToolStripMenuItem fFT사이즈ToolStripMenuItem;
        private System.Windows.Forms.ToolStripComboBox toolStripComboBox2;
        private System.Windows.Forms.ToolStripMenuItem 그래프오차ToolStripMenuItem;
        private System.Windows.Forms.ToolStripTextBox toolStripTextBox2;
        private System.Windows.Forms.Timer timer2;
        private System.Windows.Forms.Label lblFPS;
        private System.Windows.Forms.Label lblLeft;
        private System.Windows.Forms.Label lblRight;
        private System.Windows.Forms.ToolStripMenuItem 채널ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 좌채널ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 우채널ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 배경밝기ToolStripMenuItem;
        private System.Windows.Forms.ToolStripComboBox toolStripComboBox3;
        private System.Windows.Forms.ToolStripMenuItem 그래프색깔ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 높은색ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 중간색ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 낮은색ToolStripMenuItem;
        private System.Windows.Forms.ColorDialog cdHIGHdBColor;
        private System.Windows.Forms.ColorDialog cdMediumdBColor;
        private System.Windows.Forms.ColorDialog cdLOWdBColor;
        private System.Windows.Forms.ColorDialog cdPeekColor;
        private System.Windows.Forms.ToolStripMenuItem 라벨ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem fPS라벨표시ToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem 좌채널라벨표시ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 우채널라벨표시ToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem 그래프빈도ToolStripMenuItem;
        private System.Windows.Forms.ToolStripComboBox toolStripComboBox4;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.ColorDialog cdLabelColor;
        private System.Windows.Forms.ToolStripMenuItem peak색ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem fPS색ToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator6;
        private System.Windows.Forms.ColorDialog cdFPSColor;
        private System.Windows.Forms.ToolStripMenuItem 한계치에도달하면색반전ToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator7;
        private System.Windows.Forms.ToolStripMenuItem 라벨색깔ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 한계치를넘을때색반전ToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator8;
        private System.Windows.Forms.ToolStripMenuItem 평균값ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 측정범위ToolStripMenuItem;
        private System.Windows.Forms.ToolStripTextBox toolStripTextBox1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator9;
        private System.Windows.Forms.ToolStripMenuItem logarithmToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem dB라벨표시ToolStripMenuItem;
    }
}