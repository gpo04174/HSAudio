﻿using System;
using System.Drawing;

using System.Windows.Forms;
using HS_Audio_Lib;

namespace HS_Audio.Forms
{
    public partial class frm3D : Form
    {
        internal HSAudioHelper fh=null;

        //음악이 바뀌는 도중이나 바로 값을 변경하게되면 음악변경이 안됨
        public frm3D(HSAudioHelper Value)
        {
            InitializeComponent();
            fh = Value;
            Value.BeginMusicChanged += (HSAudioHelper Helper, int index, bool Error) => Helper.Play3D = false;
            Value.MusicChanged += (HSAudioHelper Helper, int index, bool Error) => 
            {
                if(chk3DMode.Checked)
                {
                    System.Threading.Thread.Sleep(50);
                    lblFMODResult.Text = fh.channel.setMode(MODE._3D).ToString();
                    fh.Play3D = chk3DMode.Checked;
                }
                    /*
                   (new System.Threading.Thread(new System.Threading.ThreadStart(()=> 
                   {
                   }))).Start();*/
            };
        }

        protected override void WndProc(ref Message m)
        {
            if (m.Msg == 71/* && m.LParam.ToInt32() == 0x452e3c8*/)
            {
                if (this.joysticControl1 != null) 
                { joysticControl1.DrawDotLine(true); }
                if (this.joysticControl2 != null)
                { joysticControl2.DrawDotLine(true); }
            }

            base.WndProc(ref m);
        }
        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (chk3DMode.Checked)
            {
                fh.Play3D = true;
                groupBox2.Enabled = groupBox3.Enabled = true;
                button1.Enabled = 개발자모드ToolStripMenuItem.Checked || FinishRotate1;
                button2.Enabled = 개발자모드ToolStripMenuItem.Checked || FinishRotate2;
                button3.Enabled = true;
                lblFMODResult.Text = fh.channel.setMode(MODE._3D).ToString();
                //fdh.fh.channel.set3DMinMaxDistance(-1.0f, 1.0f);
                fh.HelperEx.FMOD3DAttributes = vc;// = fh.channel.set3DAttributes(ref vc, ref vc1).ToString();
                lblFMODResult.Text = fh.HelperEx.Result.ToString();
                joysticControl1.Enabled = joysticControl2.Enabled = true;
                //joysticControl1.DrawDotLine(false);
            }
            else
            {
                fh.Play3D = false;
                if(!개발자모드ToolStripMenuItem.Checked)
                {
                    timer1.Stop(); timer2.Stop();
                    AddDeg = AddDeg1 = 0;
                    FinishRotate1 = FinishRotate2 = true;
                    chk반전.Enabled = chk반전1.Enabled = true;

                    groupBox2.Enabled = groupBox3.Enabled = button1.Enabled = button2.Enabled = button3.Enabled = false;
                    joysticControl1.Enabled = joysticControl2.Enabled = false;
                }
                lblFMODResult.Text = fh.channel.setMode(MODE._2D).ToString();
                //joysticControl1.DrawDotLine(false);
            }
            fh.system.update();
        }

        bool ValueLock = false;
        bool Change1=true, Change2=true;
        HS_Audio.Properties.HSAudio3DAttributes vc;
        private void numericUpDown1_ValueChanged(object sender, EventArgs e)
        {
            vc.Pos.x = (float)numVectorX.Value;
            vc.Pos.y = (float)numVectorY.Value;
            vc.Pos.z = (float)numVectorZ.Value;
            vc.Vel.x = (float)numPitchX.Value;
            vc.Vel.y = (float)numPitchY.Value;
            vc.Vel.z = (float)numPitchZ.Value;

            //Change1 = Change2 = false;
            //joysticControl1.Value = new Point((int)(vc.x * 10), (int)(vc.y * 10));
            //joysticControl2.Value = new Point((int)vc1.x, (int)vc1.y);
            //Change1 = Change2 = true;


            if (chk3DMode.Checked && fh.Play3D)
            {
                fh.HelperEx.FMOD3DAttributes = vc;
                lblFMODResult.Text = fh.HelperEx.Result.ToString();
                fh.system.update();
            }
        }

        private void frm3D_Load(object sender, EventArgs e)
        {
            joysticControl1.ResetDefault();
            joysticControl2.ResetDefault();
            lblFMODResult.Text = "OK";

            toolStripComboBox1.SelectedIndex = 0;

#if DEBUG
            string[] item = { "직사각형(세로)", "타원 (세로)", "직사각형(가로)", "타원 (가로)" };
            toolStripComboBox1.Items.AddRange(item);
#endif
        }

        public bool Closethis = false;
        private void frm3D_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = !Closethis;
            this.Hide();
        }

        CoordinateBundle LastCoordinateVector;
        private void joysticControl1_JoysticControlValueChanged(Point Location, PointF Value)
        {
            LastCoordinateVector.Location = Location;
            LastCoordinateVector.Value = Value;

            //joysticControl1.Maximum = new Point(100,100);
            label2.Text = "Value: " + XYTostring((int)Value.X, (int)Value.Y);
            label1.Text = "Pixel: " + XYTostring(Location.X, Location.Y);
            if (Change1)
            {
                numVectorX.Value = (decimal)Value.X * numVectorIncrease.Value;
                if (chkVectorHorizon.Checked)numVectorZ.Value = (decimal)Value.Y * numVectorIncrease.Value;
                else  numVectorY.Value = (decimal)Value.Y * numVectorIncrease.Value;
            }
        }

        string XYTostring(int X, int Y)
        {
            return "{" + string.Format("X={0},Y={1}", X, Y) + "}";
        }
        string XYTostring(double X, double Y)
        {
            return "{" + string.Format("X={0},Y={1}", X.ToString("0.0"), Y.ToString("0.0")) + "}";
        }

        private void joysticControl1_DoubleClick(object sender, EventArgs e)
        {
            //joysticControl1.ResetDefault();
        }

        CoordinateBundle LastCoordinatePitch;
        private void joysticControl2_JoysticControlValueChanged(Point Location, PointF Value)
        {
            LastCoordinatePitch.Location = Location;
            LastCoordinatePitch.Value = Value;

            label10.Text = "Pixel: " + XYTostring(Location.X, Location.Y);
            label9.Text = "Value: " + XYTostring((int)Value.X, (int)Value.Y);
            if (Change2)
            {
                numPitchX.Value = (decimal)Value.X * numPitchIncrease.Value;
                if (chkPitchHorizon.Checked) numPitchZ.Value = (decimal)Value.Y * numPitchIncrease.Value;
                else numPitchY.Value = (decimal)Value.Y * numPitchIncrease.Value;
            }
        }

        private void numericUpDown7_ValueChanged(object sender, EventArgs e)
        {
            joysticControl1.Maximum=new Point((int)numaximumVector.Value, (int)numaximumVector.Value);
        }

        private void numericUpDown8_ValueChanged(object sender, EventArgs e)
        {
            joysticControl2.Maximum = new Point((int)numinimumPitch.Value, (int)numinimumPitch.Value);
        }

        private void numIncreaseVector_ValueChanged(object sender, EventArgs e)
        {
            joysticControl1_JoysticControlValueChanged(LastCoordinateVector.Location, LastCoordinateVector.Value);
            numVectorX.Increment = numVectorY.Increment = numVectorZ.Increment = numVectorIncrease.Value;
        }

        private void numIncreasePitch_ValueChanged(object sender, EventArgs e)
        {
            joysticControl2_JoysticControlValueChanged(LastCoordinatePitch.Location, LastCoordinatePitch.Value);
            numPitchX.Increment = numPitchY.Increment = numPitchZ.Increment = numPitchIncrease.Value;
        }

        private void 닫기XToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void 범위제한ToolStripMenuItem_CheckedChanged(object sender, EventArgs e)
        {
            joysticControl1.LimitRange = joysticControl2.LimitRange = 범위제한ToolStripMenuItem.Checked;
        }

        private void 자동원점ㄱㄱToolStripMenuItem_CheckedChanged(object sender, EventArgs e)
        {
            joysticControl1.AutoMoveZero = joysticControl2.AutoMoveZero = 자동원점ㄱㄱToolStripMenuItem.Checked;
        }



        private void toolStripComboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (toolStripComboBox1.SelectedIndex)
            {
                case 0://사각형
                    goto default;
                case 1: //원
                    joysticControl1.ControlShape = joysticControl2.ControlShape = Control.JoysticControl.Shape.Round;
                    break;
                default:
                    joysticControl1.ControlShape = joysticControl2.ControlShape = Control.JoysticControl.Shape.Rectangle;
                    break;
            }
        }

        private void 개발자모드ToolStripMenuItem_CheckedChanged(object sender, EventArgs e)
        {
            if (개발자모드ToolStripMenuItem.Checked)
            {
                groupBox2.Enabled = groupBox3.Enabled = 
                button1.Enabled = button2.Enabled = button3.Enabled =
                joysticControl1.Enabled = joysticControl2.Enabled =
                chk반전.Enabled = chk반전1.Enabled = true;

                num간격.Minimum = num간격1.Minimum = 0;
                num간격.Increment = num간격1.Increment = 0.005m;
                num시간.Minimum = num시간1.Minimum = 0.001m;
            }
            else
            {
                button1.Enabled = 개발자모드ToolStripMenuItem.Checked || FinishRotate1;
                button2.Enabled = 개발자모드ToolStripMenuItem.Checked || FinishRotate2;
                button3.Enabled = chk3DMode.Checked;

                groupBox2.Enabled = groupBox3.Enabled = chk3DMode.Checked; 
                joysticControl1.Enabled = joysticControl2.Enabled = chk3DMode.Checked;

                if (!chk3DMode.Checked)
                {
                    timer1.Stop(); timer2.Stop();
                    FinishRotate1 = FinishRotate2 = true;
                    AddDeg = AddDeg1 = 0;
                }

                chk반전.Enabled = FinishRotate1;
                chk반전1.Enabled = FinishRotate2;

                num간격.Minimum = num간격1.Minimum = 2;
                num시간.Minimum = num시간1.Minimum = 0.025m;

                num간격.Increment = num간격.Increment = 1;
            }
        }

        #region 회전1
        private void button1_Click(object sender, EventArgs e)
        {
            Point StartPt = joysticControl1.ValueLocation;
            if (StartPt.X == 0 && StartPt.Y == 0)
            { 
                MessageBox.Show("원점 (0, 0) 에서는 회전기능을 사용할 수 없습니다.\n가운데 버튼을 이동후 회전기능을 사용해주시기 바랍니다.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            AddDeg = 0;
            //X = r * SIN(2Θ);
            //Y = r * COS(2Θ);
            float X = StartPt.X;
            float Y = StartPt.Y;
            //Math.Sqrt(((0 - X) * (0 - X)) + ((0 - Y) * (0 - Y)));
            r = Math.Sqrt((X * X) + (Y * Y));
            if (X >= 0)
                StartAngle = Math.Atan(Y / X);//처음 각도
            else
                StartAngle = Math.Atan(Y / X) + Math.PI;//처음 각도

            FinishRotate1 = false;
            //개발자모드가 켜있으면 버튼 무조건 활성화
            button1.Enabled = chk반전.Enabled = 개발자모드ToolStripMenuItem.Checked;

            timer1.Start();
        }

        double r;
        double StartAngle;
        double AddDeg = 0;
        //회전이 완료됬는지 나타내는 값
        bool FinishRotate1 = true;
        private void timer1_Tick(object sender, EventArgs e)
        {
            double X_RESULT = r * Math.Cos(StartAngle + (chk반전.Checked ? -AddDeg : AddDeg));
            double Y_RESULT = r * Math.Sin(StartAngle + (chk반전.Checked ? -AddDeg : AddDeg));
            label1.Text = "Value: " + XYTostring((int)X_RESULT, (int)Y_RESULT);
            Point Dot = new Point((int)X_RESULT, (int)Y_RESULT);//new Point((int)(X_RESULT * joysticControl1.Size.Width), (int)(Y_RESULT * joysticControl1.Maximum.Y));
            joysticControl1.ValueLocation = Dot;

            /*
            unsafe
            {
                Point* p = (Point*)System.Runtime.InteropServices.Marshal.AllocCoTaskMem(sizeof(Point));
                p->X = 0;
            }*/

            if (AddDeg > (2 * 3.14159265358979065))
            {
                if (!chk무한.Checked)
                {
                    FinishRotate1 = chk반전.Enabled = true;
                    button1.Enabled = chk3DMode.Checked || 개발자모드ToolStripMenuItem.Checked;
                    timer1.Stop();
                }
                AddDeg = Math.PI / AddAngle1;
            }
            else AddDeg += Math.PI / AddAngle1;
        }

        private void num시간_ValueChanged(object sender, EventArgs e)
        {
            timer1.Interval = (int)(num시간.Value * 1000);
        }

        double AddAngle1 = 30;
        private void num간격_ValueChanged(object sender, EventArgs e)
        {
            AddAngle1 = (float)num간격.Value;
        }
        #endregion

        #region 회전2
        double r1;
        double StartAngle1;
        double AddDeg1 = 0;
        //회전이 완료됬는지 나타내는 값
        bool FinishRotate2 = true;
        private void button2_Click(object sender, EventArgs e)
        {
            Point StartPt = joysticControl2.ValueLocation;
            if (StartPt.X == 0 && StartPt.Y == 0)
            {
                MessageBox.Show("원점 (0, 0) 에서는 회전기능을 사용할 수 없습니다.\n가운데 버튼을 이동후 회전기능을 사용해주시기 바랍니다.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            AddDeg1 = 0;
            //X = r * SIN(2Θ);
            //Y = r * COS(2Θ);
            float X = StartPt.X;
            float Y = StartPt.Y;
            //Math.Sqrt(((0 - X) * (0 - X)) + ((0 - Y) * (0 - Y)));
            r1 = Math.Sqrt((X * X) + (Y * Y));
            if (X >= 0)
                StartAngle1 = Math.Atan(Y / X);//처음 각도
            else
                StartAngle1 = Math.Atan(Y / X) + Math.PI;//처음 각도

            FinishRotate2 = false;
            //개발자모드가 켜있으면 버튼 무조건 활성화
            button2.Enabled = chk반전1.Enabled = 개발자모드ToolStripMenuItem.Checked;

            timer2.Start();
        }

        private void timer2_Tick(object sender, EventArgs e)
        {
            double X_RESULT = r1 * Math.Cos(StartAngle1 + (chk반전1.Checked ? -AddDeg1 : AddDeg1));
            double Y_RESULT = r1 * Math.Sin(StartAngle1 + (chk반전1.Checked ? -AddDeg1 : AddDeg1));
            Point Dot = new Point((int)X_RESULT, (int)Y_RESULT);//new Point((int)(X_RESULT * joysticControl1.Size.Width), (int)(Y_RESULT * joysticControl1.Maximum.Y));
            joysticControl2.ValueLocation = Dot;

            /*
            unsafe
            {
                Point* p = (Point*)System.Runtime.InteropServices.Marshal.AllocCoTaskMem(sizeof(Point));
                p->X = 0;
            }*/

            if (AddDeg1 > (2 * 3.14159265358979065))
            {
                if (!chk무한1.Checked)
                {
                    FinishRotate2 = chk반전1.Enabled = true;
                    button2.Enabled = chk3DMode.Checked || 개발자모드ToolStripMenuItem.Checked;
                    timer2.Stop();
                }
                AddDeg1 = Math.PI / AddAngle2;
            }
            else AddDeg1 += Math.PI / AddAngle2;
        }

        private void num시간1_ValueChanged(object sender, EventArgs e)
        {
            timer2.Interval = (int)(num시간1.Value * 1000);
        }


        double AddAngle2 = 30;
        private void num간격1_ValueChanged(object sender, EventArgs e)
        {
            AddAngle2 = (float)num간격1.Value;
        }
        #endregion

        private void button3_Click(object sender, EventArgs e)
        {
            if (button1.Enabled && button2.Enabled)
            {
                button1_Click(null, null);
                button2_Click(null, null);
            }
            else MessageBox.Show("두 버튼이 모두 활성화가 되있어야 진행할 수 있습니다.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Warning);
        }

        private void 조이스틱컨트롤ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("이 조이스틱 컨트롤은 원의방정식을 응용해서 만든 컨트롤입니다. (원모드 일때)");
        }

        struct CoordinateBundle
        {
            public Point Location;
            public PointF Value;

            //public float Radius { get{ return Math.Sqrt(((0 - X) * (0 - X)) + ((0 - Y) * (0 - Y))); } }
        }
    }
}
