﻿using System;
using System.Windows.Forms;
using HS_Audio;
using System.Drawing;
using System.ComponentModel;

namespace HS_Audio.Forms
{
    public partial class frmEqualizerGraph : Form
    {
        HSAudioHelper Helper;
        public frmEqualizerGraph(HSAudioHelper Helper)
        {
            InitializeComponent();
            toolStripComboBox3.SelectedIndex = 30;
            toolStripComboBox1.SelectedIndex = 4;
            toolStripComboBox2.SelectedIndex = 3; toolStripComboBox2.Text = toolStripComboBox2.Items[toolStripComboBox2.SelectedIndex].ToString();
            this.Helper = Helper;
            Helper.Disposing+=Helper_Disposing;
            timer1.Start();
        }

        private void frmEqualizer_Load(object sender, EventArgs e)
        {
            //timer1.Start();
            //timer2.Start();
            contextMenuStrip1.ImageScalingSize = new Size(16, 16);
        }
        float[] FFTArray = new float[4096];
        int[] PeakFFTArray = new int[255];
        private void timer1_Tick(object sender, EventArgs e)
        {
            
            short i = 0;
            byte j = 255;

            bool LOG = logarithmToolStripMenuItem.Checked;
            int LED = peakMeterCtrl1.LED눈금갯수;

            PeakFFTArray = new int[j];
            if (GetData) if (IsLeft) Helper.system.getSpectrum(FFTArray, (int)FFTsize, 0, FFTWindow); else Helper.system.getSpectrum(FFTArray, (int)FFTsize, 1, FFTWindow);
            else if (Helper.channel != null) if (IsLeft) Helper.channel.getSpectrum(FFTArray, (int)FFTsize, 0, FFTWindow); else Helper.channel.getSpectrum(FFTArray, (int)FFTsize, 1, FFTWindow);
            switch (FFTsize)
            {
                case frmFFTGraph.FFTSize.Size_256: i = 0;break;
                case frmFFTGraph.FFTSize.Size_128: i = 0; j = 255; break;
                case frmFFTGraph.FFTSize.Size_64: i = 0; j = 255; break;
                case frmFFTGraph.FFTSize.None: i = 0; j = 255; break;
                default: i = 1; j = 255; PeakFFTArray[0] = (int)((LOG ? frmFFTGraph.Decibels(FFTArray[1], 0) : FFTArray[1] * LED) + Average); break;

            }

            for (; i < 256; )
            {
                switch (FFTsize)
                {
                    case frmFFTGraph.FFTSize.Size_4096:
                        try { PeakFFTArray[i] = (int)((LOG?frmFFTGraph.Decibels(FFTArray[i * 16], 0):FFTArray[i*16]*LED) + Average); } catch { }i++;break;
                    case frmFFTGraph.FFTSize.Size_2048:
                        try { PeakFFTArray[i] = (int)((LOG?frmFFTGraph.Decibels(FFTArray[i * 8], 0):FFTArray[i*8]*LED) + Average); } catch { }i++;break;
                    case frmFFTGraph.FFTSize.Size_1024:
                        try { PeakFFTArray[i] = (int)((LOG?frmFFTGraph.Decibels(FFTArray[i * 4], 0):FFTArray[i*4]*LED) + Average); } catch { }i++;break;
                    case frmFFTGraph.FFTSize.Size_512:
                        try { PeakFFTArray[i] = (int)((LOG?frmFFTGraph.Decibels(FFTArray[i * 2], 0):FFTArray[i*2]*LED) + Average); } catch { }i++;break;
                    case frmFFTGraph.FFTSize.Size_256:
                        try { PeakFFTArray[i] = (int)((LOG?frmFFTGraph.Decibels(FFTArray[i * 0], 0):FFTArray[i*0]*LED) + Average); } catch { }i++;break;
                    case frmFFTGraph.FFTSize.Size_128:
                        try { if(i<254)PeakFFTArray[i]=PeakFFTArray[i+1] = (int)((LOG?frmFFTGraph.Decibels(FFTArray[i/2], 0):FFTArray[i/2]*LED) + Average);
                              else PeakFFTArray[i]=(int)((LOG?frmFFTGraph.Decibels(FFTArray[i/2], 0):FFTArray[i/2]*LED) + Average);} catch { }i+=2;break;
                    case frmFFTGraph.FFTSize.Size_64:
                        try { PeakFFTArray[i]=PeakFFTArray[i+1]=PeakFFTArray[i+2]=PeakFFTArray[i+3] = (int)((LOG?frmFFTGraph.Decibels(FFTArray[i/4], 0):FFTArray[i/4]*LED) + Average); } catch { }i+=4;break;
                    //case frmFFTGraph.FFTSize.None:
                    default: return;

                } 
            }
            peakMeterCtrl1.SetData(PeakFFTArray, 0, 255, false);
            timer2_Tick(null, null);
        }
        /*
        private void timer1_Tick(object sender, EventArgs e)
        {
            float max = 1; byte a = 1; short b = 1024;
            Helper.system.getSpectrum(FFTArray, b, 0, HS_Audio_Lib.DSP_FFT_WINDOW.MAX);
            //for(byte i=0;i<255;i++)if (max < FFTArray[i]) { max = FFTArray[i];}

            int[] PeakFFTArray = new int[b];
            for (short i = 0; i < b; i += a)
            {
                try { PeakFFTArray[i == 0 ? 0 : i / a] = (int)(FFTArray[i] * 2 / max * 100); }
                catch { break; }
                //PeakFFTArray[i] = (int)frmFFTGraph.Decibels(FFTArray[i], 0)+104;
            }
            peakMeterCtrl1.SetData(PeakFFTArray, 0, 255, false);
        }*/

        int FPS;
        string LDB_S, RDB_S;
        float tmpLMax, tmpRMax, tmpLMax1, tmpRMax1;
        float[] FFTArray_L = new float[1024], FFTArray_R = new float[1024];
        float[] WaveArray_L = new float[1024], WaveArray_R = new float[1024];
        Color tmpL, tmpR;
        private void timer2_Tick(object sender, EventArgs e)
        {
            FPS = CalculateFrameRate();
            tmpLMax = tmpLMax1 = tmpRMax = tmpRMax1 = float.MinValue;

            if(GetData){
            Helper.system.getSpectrum(FFTArray_L, 1024, 0, HS_Audio_Lib.DSP_FFT_WINDOW.BLACKMANHARRIS);
            Helper.system.getSpectrum(FFTArray_R, 1024, 1, HS_Audio_Lib.DSP_FFT_WINDOW.BLACKMANHARRIS);
            Helper.system.getWaveData(WaveArray_L, 1024, 0); 
            Helper.system.getWaveData(WaveArray_R, 1024, 1);}
            else{
            Helper.channel.getSpectrum(FFTArray_L, 1024, 0, HS_Audio_Lib.DSP_FFT_WINDOW.BLACKMANHARRIS);
            Helper.channel.getSpectrum(FFTArray_R, 1024, 1, HS_Audio_Lib.DSP_FFT_WINDOW.BLACKMANHARRIS);
            Helper.channel.getWaveData(WaveArray_L, 1024, 0);
            Helper.channel.getWaveData(WaveArray_R, 1024, 1); }
            Helper.system.update();

            for (int i = 1; i < WaveArray_L.Length; i++)
            {
                if (tmpLMax1 < WaveArray_L[i]) { tmpLMax1 = WaveArray_L[i]; }//this.Width / 2]; 
                if (tmpRMax1 < WaveArray_R[i]) { tmpRMax1 = WaveArray_R[i]; }//this.Width / 2];
            }

            for (int i = 1; i < FFTArray_L.Length; i++)
            {
                if (tmpLMax < FFTArray_L[i]) { tmpLMax = FFTArray_L[i]; }
                if (tmpRMax < FFTArray_R[i]) { tmpRMax = FFTArray_R[i]; }
            }

            lblFPS.Text = FPS.ToString("FPS: 00");
            double LDB = frmFFTGraph.Decibels(tmpLMax, 0), RDB = frmFFTGraph.Decibels(tmpRMax, 0);//0.0f,RDB=0.0f;

            if (tmpLMax == 0) { LDB_S = "0.000"; }
            else if (LDB < -365) { LDB_S = "-Inf."; }
            else { LDB_S = LDB.ToString("0.000"); }

            if (RDB == 0) { RDB_S = "0.000"; }
            else if (RDB < -365) { RDB_S = "-Inf."; }
            else { RDB_S = RDB.ToString("0.000"); }

            this.Text = string.Format("이퀄라이저 그래프 [좌: {0} dB / 우: {1} dB] (FPS: {2}) - 계산식: [n] = (N==0) ? 0 :10.0 * Log10(N^2) //n은 dB", LDB_S, RDB_S, FPS.ToString("00"));
            if (frmFFTGraph.Decibels(tmpLMax1, 0).ToString("0.000") == "0.000") { lblLeft.Text = string.Format("좌[!]:  {0} dB", frmFFTGraph.Decibels(tmpLMax1, 0).ToString("0.000")); }
            else if (frmFFTGraph.Decibels(tmpLMax1, 0) > 0) { //lblLeft.BackColor = CalculateNegativeColor(this.BackColor, false); 
                lblLeft.Text = string.Format("좌[!]: +{0} dB", frmFFTGraph.Decibels(tmpLMax1, 0).ToString("0.000"));
                if (한계치를넘을때색반전ToolStripMenuItem.Checked) lblLeft.ForeColor = CalculateNegativeColor(cdLabelColor.Color);
                else lblLeft.ForeColor = cdLabelColor.Color; 
                if (한계치에도달하면색반전ToolStripMenuItem.Checked&&IsLeft)
                {
                    peakMeterCtrl1.ColorHigh = CalculateNegativeColor(cdHIGHdBColor.Color);
                    peakMeterCtrl1.ColorMedium = CalculateNegativeColor(cdMediumdBColor.Color);
                    peakMeterCtrl1.ColorNormal = CalculateNegativeColor(cdLOWdBColor.Color);
                    PeekColor = CalculateNegativeColor(cdPeekColor.Color);
                }
                else if(IsLeft)
                {
                    peakMeterCtrl1.ColorHigh = cdHIGHdBColor.Color;
                    peakMeterCtrl1.ColorMedium = cdMediumdBColor.Color;
                    peakMeterCtrl1.ColorNormal = cdLOWdBColor.Color;
                    PeekColor = cdPeekColor.Color;
                }
                
            }
            else { //lblLeft.BackColor = this.BackColor; 
                if (frmFFTGraph.Decibels(tmpLMax1, 0)>-365) lblLeft.Text = string.Format("좌[?]: {0} dB", frmFFTGraph.Decibels(tmpLMax1, 0).ToString("0.000")); 
                else lblRight.Text = "좌[?]: -Inf. dB";
                lblLeft.ForeColor = cdLabelColor.Color;
                peakMeterCtrl1.ColorHigh = cdHIGHdBColor.Color;
                peakMeterCtrl1.ColorMedium = cdMediumdBColor.Color;
                peakMeterCtrl1.ColorNormal = cdLOWdBColor.Color;
                PeekColor = cdPeekColor.Color;
            }

            if (frmFFTGraph.Decibels(tmpRMax1, 0).ToString("0.000") == "0.000") { lblRight.Text = string.Format("우[!]:  {0} dB", frmFFTGraph.Decibels(tmpRMax1, 0).ToString("0.000")); }
            else if (frmFFTGraph.Decibels(tmpRMax1, 0) > 0) {// lblRight.BackColor = CalculateNegativeColor(this.BackColor, false); 
                lblRight.Text = string.Format("우[!]: +{0} dB", frmFFTGraph.Decibels(tmpRMax1, 0).ToString("0.000"));
                if(한계치를넘을때색반전ToolStripMenuItem.Checked)lblRight.ForeColor = CalculateNegativeColor(cdLabelColor.Color);
                else lblRight.ForeColor = cdLabelColor.Color; 
                if (한계치에도달하면색반전ToolStripMenuItem.Checked && !IsLeft)
                {
                    peakMeterCtrl1.ColorHigh = CalculateNegativeColor(cdHIGHdBColor.Color);
                    peakMeterCtrl1.ColorMedium = CalculateNegativeColor(cdMediumdBColor.Color);
                    peakMeterCtrl1.ColorNormal = CalculateNegativeColor(cdLOWdBColor.Color);
                    PeekColor = CalculateNegativeColor(cdPeekColor.Color);
                }
                else if (!IsLeft)
                {
                    peakMeterCtrl1.ColorHigh = cdHIGHdBColor.Color;
                    peakMeterCtrl1.ColorMedium = cdMediumdBColor.Color;
                    peakMeterCtrl1.ColorNormal = cdLOWdBColor.Color;
                    PeekColor = cdPeekColor.Color;
                }
            }
            else {// lblRight.BackColor = this.BackColor; 
                if (frmFFTGraph.Decibels(tmpRMax1, 0) > -365) lblRight.Text = string.Format("우[?]: {0} dB", frmFFTGraph.Decibels(tmpRMax1, 0).ToString("0.000"));
                else lblRight.Text = "우[?]: -Inf. dB";
                lblRight.ForeColor = cdLabelColor.Color; 
                peakMeterCtrl1.ColorHigh = cdHIGHdBColor.Color;
                peakMeterCtrl1.ColorMedium = cdMediumdBColor.Color;
                peakMeterCtrl1.ColorNormal = cdLOWdBColor.Color;
                PeekColor = cdPeekColor.Color;
            }
        }
        /*
        int FPS;
        string LDB_S, RDB_S;
        float LDB, RDB;
        //float tmpLMax, tmpRMax;
        float[] WaveArray_L = new float[1024], WaveArray_R = new float[1024];
        private void timer2_Tick(object sender, EventArgs e)
        {
            FPS = CalculateFrameRate();

            Helper.system.getWaveData(WaveArray_L, WaveArray_L.Length, 0);
            Helper.system.getWaveData(WaveArray_R, WaveArray_R.Length,1);

            lblFPS.Text = FPS.ToString("FPS: 00");

            LDB = WaveArray_L[WaveArray_L.Length / 2];
            RDB = WaveArray_R[WaveArray_R.Length / 2];

            if (LDB == 0) { LDB_S = "Inf."; }
            else if (frmFFTGraph.Decibels(LDB, 0) < -365.5) { LDB_S = "-Inf."; }
            else { LDB_S = frmFFTGraph.Decibels(LDB, 0).ToString("0.000"); }

            if (RDB == 0) { RDB_S = "Inf."; }
            else if (frmFFTGraph.Decibels(RDB, 0) < -365.5) { RDB_S = "-Inf."; }
            else { RDB_S = frmFFTGraph.Decibels(RDB, 0).ToString("0.000"); }

            this.Text = string.Format("이퀄라이저 그래프 [좌: {0} dB / 우: {1} dB] (FPS: {2})",LDB_S, RDB_S, FPS.ToString("00"));
        }
         */
        public static Color CalculateNegativeColor(Color OriginColor, bool IncludeAlpha=false)
        {
            if (IncludeAlpha) return Color.FromArgb(255 - OriginColor.A,255 - OriginColor.R, 255 - OriginColor.G, 255 - OriginColor.B);
           else return Color.FromArgb(255 - OriginColor.R, 255 - OriginColor.G, 255 - OriginColor.B);
        }
        #region 속성
        public byte Brightness { get { return this.BackColor.R; } 
            set {BackColor=lblLeft.BackColor=lblRight.BackColor=lblFPS.BackColor=Color.FromArgb(value, value, value); } }

        /// <summary>
        /// 스펙트럼을 그릴 시간(밀리초) 를 가져오거나 설정합니다..
        /// </summary>
        //[Category("스펙트럼"), Description("스펙트럼을 그릴 시간(밀리초) 입니다."), DefaultValue(100)]
        public int DrawSpeed
        {
            get { return timer1.Interval; }
            set { timer1.Interval = value;}
        }

        public Color PeekColor { get { return peakMeterCtrl1.Peak색상; } set { peakMeterCtrl1.Peak색상 = value; } }

        public Color HIGHdBColor { get { return peakMeterCtrl1.ColorHigh; } set { peakMeterCtrl1.ColorHigh = value; } }

        public Color MediumdBColor { get { return peakMeterCtrl1.ColorMedium; } set { peakMeterCtrl1.ColorMedium = value; } }

        public Color LOWdBColor { get { return peakMeterCtrl1.ColorNormal; } set { peakMeterCtrl1.ColorNormal = value; } }

        bool _GetData = true;
        /// <summary>
        /// 그래프를 그릴 데이터를 가져오는 방법을 가져오거나 설정합니다. (True면 FMOD.System에서 가져오고 False면 FMOD.Channel에서 가져옵니다.)
        /// </summary>
        [Category(""), Description("그래프를 그릴 데이터를 가져오는 방법 입니다. (True면 Main Output에서 가져오고 False면 Channel에서 가져옵니다.)"), DefaultValue(true)]
        public bool GetData { get { return _GetData; } set { if (Helper.channel == null && !value) { throw new NullReferenceException("Channel이 NULL이므로 변경하지 못했습니다."); } _GetData = value; } }


        bool _IsLeft = true;
        /// <summary>
        /// 
        /// </summary>
        public bool IsLeft { get { return _IsLeft; } set { _IsLeft = value; } }

        /// <summary>
        /// 
        /// </summary>
        HS_Audio_Lib.DSP_FFT_WINDOW _FFTWindow = HS_Audio_Lib.DSP_FFT_WINDOW.HANNING;
        [Category(""), Description("FFT윈도우를 설정합니다.")]
        public HS_Audio_Lib.DSP_FFT_WINDOW FFTWindow { get { return _FFTWindow; } set { _FFTWindow = value; } }


        frmFFTGraph.FFTSize _FFTSize = frmFFTGraph.FFTSize.Size_512;
        /// <summary>
        /// FFT의 배열사이즈를 가져오거나 설정합니다.(음수는 올수 없습니다.)
        /// </summary>
        [Category(""), Description("FFT의 배열사이즈 입니다."), DefaultValue(typeof(frmFFTGraph.FFTSize), "Size_2048")]
        public frmFFTGraph.FFTSize FFTsize { get { return _FFTSize; } set { _FFTSize = value; } }
        int FFTsizeIndex(frmFFTGraph.FFTSize FFTSIZE)
        {
            switch (FFTSIZE)
            {
                case frmFFTGraph.FFTSize.Size_4096: return 0;
                case frmFFTGraph.FFTSize.Size_2048: return 1;
                case frmFFTGraph.FFTSize.Size_1024: return 2;
                case frmFFTGraph.FFTSize.Size_512: return 3;
                case frmFFTGraph.FFTSize.Size_128: return 4;
                case frmFFTGraph.FFTSize.Size_64: return 5;
                case frmFFTGraph.FFTSize.None: return 6;
                default: return 0;
            }
        }

        float _Average=104;
        /// <summary>
        /// 
        /// </summary>
        public float Average { get { return _Average; } set { _Average = value; } }
        #endregion


        private int lastTick;
        private int lastFrameRate;
        private int frameRate;
        public int CalculateFrameRate()
        {
            if (System.Environment.TickCount - lastTick >= 1000)
            {
                lastFrameRate = frameRate;
                frameRate = 0;
                lastTick = System.Environment.TickCount;
            }
            frameRate++;
            return lastFrameRate;
        }

        private void toolStripComboBox3_SelectedIndexChanged(object sender, EventArgs e)
        {
            Brightness = Convert.ToByte(toolStripComboBox3.Items[toolStripComboBox3.SelectedIndex]);
        }

        private void 높은색ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (cdHIGHdBColor.ShowDialog() == System.Windows.Forms.DialogResult.OK) HIGHdBColor = cdHIGHdBColor.Color;
        }

        private void 중간색ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (cdMediumdBColor.ShowDialog() == System.Windows.Forms.DialogResult.OK) MediumdBColor = cdMediumdBColor.Color;
        }

        private void 낮은색ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (cdLOWdBColor.ShowDialog() == System.Windows.Forms.DialogResult.OK) LOWdBColor = cdLOWdBColor.Color;
        }

        private void fPS라벨표시ToolStripMenuItem_CheckedChanged(object sender, EventArgs e)
        {
            lblFPS.Visible = fPS라벨표시ToolStripMenuItem.Checked;
        }

        private void 좌채널라벨표시ToolStripMenuItem_CheckedChanged(object sender, EventArgs e)
        {
            lblLeft.Visible = 좌채널라벨표시ToolStripMenuItem.Checked;
        }

        private void 우채널라벨표시ToolStripMenuItem_CheckedChanged(object sender, EventArgs e)
        {
            lblRight.Visible = 우채널라벨표시ToolStripMenuItem.Checked;
        }

        private void toolStripComboBox4_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (HS_CSharpUtility.Utility.ControlUtility.OnlyInputNumber(e))
            { toolStripComboBox4_TextChanged(null, null); }
        }

        private void toolStripComboBox4_TextChanged(object sender, EventArgs e)
        {
            int a;
            if (toolStripComboBox4.Text == "") a = 1;
            if (int.Parse(toolStripComboBox4.Text) > 0) { a = int.Parse(toolStripComboBox4.Text); }
            else a = 1;
            this.DrawSpeed = a > 0 ? a : 1;
        }

        private void 좌채널ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (좌채널ToolStripMenuItem.Checked)
            {
                좌채널ToolStripMenuItem.Checked = false;
                우채널ToolStripMenuItem.Checked = true;
            }
            else
            {
                좌채널ToolStripMenuItem.Checked = true;
                우채널ToolStripMenuItem.Checked = false;
            }
            IsLeft = 좌채널ToolStripMenuItem.Checked;
        }

        private void toolStripTextBox2_TextChanged(object sender, EventArgs e)
        {
            float a;
            if (toolStripTextBox2.Text == "") a = 1;
            if (float.Parse(toolStripTextBox2.Text) > 0) { a = float.Parse(toolStripTextBox2.Text); }
            else a = 1;
            this.Average = a;
        }

        private void toolStripTextBox2_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (HS_CSharpUtility.Utility.ControlUtility.OnlyInputNumber(e))
            { toolStripTextBox2_TextChanged(null, null); }
        }

        private void 맨위에표시ToolStripMenuItem_CheckedChanged(object sender, EventArgs e)
        {
            this.TopMost =맨위에표시ToolStripMenuItem.Checked;
        }

        private void 배경투명처리ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!배경투명처리ToolStripMenuItem.Checked)
            {
                this.TransparencyKey = this.BackColor;
                배경투명처리ToolStripMenuItem.Checked = true;
            }
            else { this.TransparencyKey = Color.Empty; 배경투명처리ToolStripMenuItem.Checked = false; }
        }

        private void fMODSystemToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (fMODSystemToolStripMenuItem.Checked)
            {
                fMODSystemToolStripMenuItem.Checked = false;
                fMODChannelToolStripMenuItem.Checked = true;
                GetData = false;
            }
            else
            {
                fMODSystemToolStripMenuItem.Checked = true;
                fMODChannelToolStripMenuItem.Checked = false;
                GetData = true;
            }
        }

        private void toolStripComboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            toolTip1.Hide(toolStripComboBox1.Owner);
            string a = toolStripComboBox1.Items[toolStripComboBox1.SelectedIndex].ToString();
            this.FFTWindow = (HS_Audio_Lib.DSP_FFT_WINDOW)Enum.Parse(typeof(HS_Audio_Lib.DSP_FFT_WINDOW), a);
            switch (a)
            {
                case "RECT":
                    toolStripComboBox1.ToolTipText = "RECT:\r\nw[n] = 1.0";
                    toolTip1.SetToolTip(toolStripComboBox1.Owner, "RECT:\r\nw[n] = 1.0"); break;
                case "TRIANGLE":
                    toolStripComboBox1.ToolTipText = "TRIANGLE:\r\nw[n] = TRI(2n/N)";
                    toolTip1.SetToolTip(toolStripComboBox1.Owner, "TRIANGLE:\r\nw[n] = TRI(2n/N)"); break;
                case "HAMMING":
                    toolStripComboBox1.ToolTipText = "HAMMING:\r\nw[n] = 0.54 - (0.46 * COS(n/N) )";
                    toolTip1.SetToolTip(toolStripComboBox1.Owner, "HAMMING:\r\nw[n] = 0.54 - (0.46 * COS(n/N) )"); break;
                case "HANNING":
                    toolStripComboBox1.ToolTipText = "HANNING:\r\nw[n] = 0.5 *  (1.0  - COS(n/N) )";
                    toolTip1.SetToolTip(toolStripComboBox1.Owner, "HANNING:\r\nw[n] = 0.5 *  (1.0  - COS(n/N) )"); break;
                case "BLACKMAN":
                    toolStripComboBox1.ToolTipText = "BLACKMAN:\r\nw[n] = 0.42 - (0.5  * COS(n/N) ) + (0.08 * COS(2.0 * n/N) )";
                    toolTip1.SetToolTip(toolStripComboBox1.Owner, "BLACKMAN:\r\nw[n] = 0.42 - (0.5  * COS(n/N) ) + (0.08 * COS(2.0 * n/N) )"); break;
                case "BLACKMANHARRIS":
                    toolStripComboBox1.ToolTipText = "BLACKMANHARRIS:\r\nw[n] = 0.35875 - (0.48829 * COS(1.0 * n/N)) + (0.14128 * COS(2.0 * n/N)) - (0.01168 * COS(3.0 * n/N))";
                    toolTip1.SetToolTip(toolStripComboBox1.Owner, "BLACKMANHARRIS:\r\nw[n] = 0.35875 - (0.48829 * COS(1.0 * n/N)) + (0.14128 * COS(2.0 * n/N)) - (0.01168 * COS(3.0 * n/N))"); break;
                case "MAX":
                    toolStripComboBox1.ToolTipText = "MAX";
                    toolTip1.SetToolTip(toolStripComboBox1.Owner, "MAX"); break;
                default:
                    toolStripComboBox1.ToolTipText = "잘못된 값 입니다.";
                    toolTip1.SetToolTip(toolStripComboBox1.Owner, "잘못된 값 입니다."); break;
            }
        }

        private void Helper_Disposing(object sender, bool All) { if (All)this.Close(); }

        private void 라벨색깔ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (cdLabelColor.ShowDialog() == System.Windows.Forms.DialogResult.OK) lblLeft.ForeColor = lblRight.ForeColor = cdLabelColor.Color;
        }

        private void peak색ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (cdPeekColor.ShowDialog() == System.Windows.Forms.DialogResult.OK) PeekColor = cdPeekColor.Color;
        }

        private void fPS색ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (cdFPSColor.ShowDialog() == System.Windows.Forms.DialogResult.OK) lblFPS.ForeColor = cdFPSColor.Color;
        }

        private void toolStripComboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            try { this.FFTsize = (frmFFTGraph.FFTSize)Enum.Parse(typeof(frmFFTGraph.FFTSize), toolStripComboBox2.SelectedItem.ToString() == "None" ? "None" : "Size_" + toolStripComboBox2.SelectedItem.ToString()); }catch { }
        }

        private void toolStripTextBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (HS_CSharpUtility.Utility.ControlUtility.OnlyInputNumber(e))
                toolStripTextBox1_TextChanged(null, null);
        }

        private void toolStripTextBox1_TextChanged(object sender, EventArgs e)
        {
            int a;
            if (toolStripTextBox1.Text == "") a = 1;
            if (int.Parse(toolStripTextBox1.Text) > 0) a = int.Parse(toolStripTextBox1.Text);
            else a = 1;
            peakMeterCtrl1.LED눈금갯수 = a;
        }

        private void dB라벨표시ToolStripMenuItem_CheckedChanged(object sender, EventArgs e)
        {
            lblLeft.Visible = dB라벨표시ToolStripMenuItem.Checked;
            lblRight.Visible = dB라벨표시ToolStripMenuItem.Checked;
        }
    }
}
