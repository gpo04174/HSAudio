﻿using System;
using System.Windows.Forms;
using HS_Audio_Helper;
using System.Drawing;

namespace HS_Audio_Helper.Forms
{
    public partial class frmEqualizer : Form
    {
        HSAudioHelper Helper;
        public frmEqualizer(HSAudioHelper Helper)
        {
            InitializeComponent();
            this.Helper = Helper;
        }

        private void frmEqualizer_Load(object sender, EventArgs e)
        {
            timer1.Start();
            timer2.Start();
        }
        float[] FFTArray = new float[4096];
        int[] PeakFFTArray = new int[255];
        private void timer1_Tick(object sender, EventArgs e)
        {
            Helper.system.getSpectrum(FFTArray, 4096, 0, HS_Audio_Lib.DSP_FFT_WINDOW.BLACKMANHARRIS);
            for(byte i=0;i<255;i++)
            {
                try { PeakFFTArray[i] = (int)frmFFTGraph.Decibels(FFTArray[i * 16], 0) + 120; }catch{}
            }
            peakMeterCtrl1.SetData(PeakFFTArray, 0, 255, false);
        }
        /*
        private void timer1_Tick(object sender, EventArgs e)
        {
            float max = 1; byte a = 1; short b = 1024;
            Helper.system.getSpectrum(FFTArray, b, 0, HS_Audio_Lib.DSP_FFT_WINDOW.MAX);
            //for(byte i=0;i<255;i++)if (max < FFTArray[i]) { max = FFTArray[i];}

            int[] PeakFFTArray = new int[b];
            for (short i = 0; i < b; i += a)
            {
                try { PeakFFTArray[i == 0 ? 0 : i / a] = (int)(FFTArray[i] * 2 / max * 100); }
                catch { break; }
                //PeakFFTArray[i] = (int)frmFFTGraph.Decibels(FFTArray[i], 0)+104;
            }
            peakMeterCtrl1.SetData(PeakFFTArray, 0, 255, false);
        }*/

        int FPS;
        string LDB_S, RDB_S;
        float tmpLMax, tmpRMax, tmpLMax1, tmpRMax1;
        float[] FFTArray_L = new float[1024], FFTArray_R = new float[1024];
        float[] WaveArray_L = new float[1024], WaveArray_R = new float[1024];
        private void timer2_Tick(object sender, EventArgs e)
        {
            FPS = CalculateFrameRate();
            tmpLMax = tmpRMax = float.NegativeInfinity;


            Helper.system.getSpectrum(FFTArray_L, 1024, 0, HS_Audio_Lib.DSP_FFT_WINDOW.BLACKMANHARRIS);
            Helper.system.getSpectrum(FFTArray_R, 1024, 1, HS_Audio_Lib.DSP_FFT_WINDOW.BLACKMANHARRIS);
            Helper.system.getWaveData(WaveArray_L, 1024, 0); 
            Helper.system.getWaveData(WaveArray_R, 1024, 1);
            Helper.system.update();

            tmpLMax1 = WaveArray_L[512];//this.Width / 2]; 
            tmpRMax1 = WaveArray_R[512];//this.Width / 2];

            for (int i = 1; i < FFTArray_L.Length; i++)
            {
                if (tmpLMax < FFTArray_L[i]) { tmpLMax = FFTArray_L[i]; }
                if (tmpRMax < FFTArray_R[i]) { tmpRMax = FFTArray_R[i]; }
            }

            lblFPS.Text = FPS.ToString("FPS: 00");
            double LDB = frmFFTGraph.Decibels(tmpLMax, 0), RDB = frmFFTGraph.Decibels(tmpRMax, 0);//0.0f,RDB=0.0f;

            if (tmpLMax == 0) { LDB_S = "-Inf."; }
            else if (LDB < -365.5) { LDB_S = "-Inf."; }
            else { LDB_S = LDB.ToString("0.000"); }

            if (RDB == 0) { RDB_S = "-Inf."; }
            else if (RDB < -365.5) { RDB_S = "-Inf."; }
            else { RDB_S = RDB.ToString("0.000"); }

            this.Text = string.Format("이퀄라이저 그래프 [좌: {0} dB / 우: {1} dB] (FPS: {2})",LDB_S, RDB_S, FPS.ToString("00"));
            if (LDB >= 0) { lblLeft.ForeColor = Color.Red; } else { lblLeft.ForeColor = Color.Black; }
            if (RDB >= 0) { lblRight.ForeColor = Color.Red; } else { lblRight.ForeColor = Color.Black; }
            lblLeft.Text = string.Format("좌: {0} dB",  frmFFTGraph.Decibels(tmpLMax1,0).ToString("0.000"));
            lblRight.Text = string.Format("우: {0} dB", frmFFTGraph.Decibels(tmpRMax1,0).ToString("0.000"));
        }
        /*
        int FPS;
        string LDB_S, RDB_S;
        float LDB, RDB;
        //float tmpLMax, tmpRMax;
        float[] WaveArray_L = new float[1024], WaveArray_R = new float[1024];
        private void timer2_Tick(object sender, EventArgs e)
        {
            FPS = CalculateFrameRate();

            Helper.system.getWaveData(WaveArray_L, WaveArray_L.Length, 0);
            Helper.system.getWaveData(WaveArray_R, WaveArray_R.Length,1);

            lblFPS.Text = FPS.ToString("FPS: 00");

            LDB = WaveArray_L[WaveArray_L.Length / 2];
            RDB = WaveArray_R[WaveArray_R.Length / 2];

            if (LDB == 0) { LDB_S = "Inf."; }
            else if (frmFFTGraph.Decibels(LDB, 0) < -365.5) { LDB_S = "-Inf."; }
            else { LDB_S = frmFFTGraph.Decibels(LDB, 0).ToString("0.000"); }

            if (RDB == 0) { RDB_S = "Inf."; }
            else if (frmFFTGraph.Decibels(RDB, 0) < -365.5) { RDB_S = "-Inf."; }
            else { RDB_S = frmFFTGraph.Decibels(RDB, 0).ToString("0.000"); }

            this.Text = string.Format("이퀄라이저 그래프 [좌: {0} dB / 우: {1} dB] (FPS: {2})",LDB_S, RDB_S, FPS.ToString("00"));
        }
         */
        private int lastTick;
        private int lastFrameRate;
        private int frameRate;
        public int CalculateFrameRate()
        {
            if (System.Environment.TickCount - lastTick >= 1000)
            {
                lastFrameRate = frameRate;
                frameRate = 0;
                lastTick = System.Environment.TickCount;
            }
            frameRate++;
            return lastFrameRate;
        }
    }
}
