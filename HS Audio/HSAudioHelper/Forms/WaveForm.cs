﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Runtime.InteropServices;
using System.Threading;
using System.Windows.Forms;
using HS_Audio;
using NAudio.Wave;

namespace HS_Audio.Forms
{
    public partial class WaveForm : Form
    {
        internal HSAudioHelper Helper;
        public WaveForm(HSAudioHelper Helper)
        {
            this.Helper = Helper; 
            /*
            try{Helper.system.getWaveData(Sam, Sam.Length, 0);}catch{}
            ws.WaveFormat = new WaveFormat((int)Helper.DefaultFrequency, 1);
            ws.WaveFormat.Encoding = WaveFormatEncoding.Pcm; ws.WaveFormat.BitsPerSample = 16;
            ws.Write(Sam);*/
            InitializeComponent();
            hsWaveDraw1.MouseWheel+=hsWaveDraw_MouseWheel;
            hsWaveDraw2.MouseWheel+=hsWaveDraw_MouseWheel;
            TEXT = this.Text;
            frm = new Setting.WaveFormSetting(hsWaveDraw1, hsWaveDraw2, hsWaveDraw1.Row, hsWaveDraw2.Row);
            ColorUpdate();
        }

        void ColorUpdate()
        {
            hsWaveDraw1.pen = pen;hsWaveDraw2.pen = pen1;
            hsWaveDraw1.BackColor = c;hsWaveDraw2.BackColor = c1;
        }

        Setting.WaveFormSetting frm;
        private void WaveForm_Load(object sender, EventArgs e)
        {
            checkBox1.Checked = true;
            //pen = new Pen(Color.Yellow);
            //pen1 = new Pen(Color.Lime);
            //hsWaveDraw1.Row.DrawHeightLine = hsWaveDraw2.Row.DrawHeightLine = false;
            hsWaveDraw1.DrawRow = hsWaveDraw2.DrawRow = false;

            waveformPainter1.Sample = (int)numericUpDown1.Value;
            //waveViewer1.WaveStream = ws;
            
            domainUpDown1.SelectedIndex = 5;
            ThreadPool.QueueUserWorkItem(new WaitCallback(LOL));
            ThreadPool.QueueUserWorkItem(new WaitCallback(LOLOL));
            timer1.Start();
        }
        int DrawingTime=18;
        private void numericUpDown3_ValueChanged(object sender, EventArgs e)
        {
            //timer1.Interval = (int)numericUpDown3.Value;
            DrawingTime = (int)numericUpDown3.Value;
        }

        private void numericUpDown1_ValueChanged(object sender, EventArgs e)
        {
            //waveformPainter1.Sample = (int)numericUpDown1.Value;
            BORDER_WIDTH= (int)numericUpDown1.Value;
            //waveViewer1.SamplesPerPixel = (int)numericUpDown1.Value;
        }

        float[] Sam = new float[1024];
        float[] Sam1 = new float[1024];
        private void numericUpDown2_ValueChanged(object sender, EventArgs e)
        {
            Sam = new float[(int)numericUpDown2.Value];
            Sam1 = new float[(int)numericUpDown2.Value];
            //MaxLength = (int)numericUpDown2.Value;
        }

        Color c = Color.FromArgb(0, 0, 32);
        Color c1 = Color.FromArgb(0, 0, 32);
        Pen pen1 = new Pen(Color.Yellow);//Color.FromArgb(180, 180, 0));
        Pen pen = new Pen(Color.Lime);//Color.FromArgb(0, 180, 0));
        int BORDER_WIDTH=1;

        HSWaveStream ws = new HSWaveStream();
        int FPS;
        string TEXT = "웨이브 스펙트럼";
        private void timer1_Tick(object sender, EventArgs e)
        {
            this.Text = string.Format("{0} [FPS: {1}]", TEXT, FPS);
            //waveViewer1.Draw();
            //foreach(float a in Sam)waveformPainter1.AddMax(a);
            //DrawNormalizedAudio(ref Sam, pictureBox1, pen,c, BORDER_WIDTH);

        }

        private int lastTick;
        private int lastFrameRate;
        private int frameRate;
        public int CalculateFrameRate()
        {
            if (System.Environment.TickCount - lastTick >= 1000)
            {
                lastFrameRate = frameRate;
                frameRate = 0;
                lastTick = System.Environment.TickCount;
            }
            frameRate++;
            return lastFrameRate;
        }
        float[] ls = new float[50000];
        float[] ls1 = new float[50000];
        int DrawSpeed = 25;
        unsafe void LOLOL(object o)
        {
            th1 = Thread.CurrentThread;
            while (true)
            {
                DrawWave();
                Thread.Sleep(DrawSpeed);//10
            }
        }

        Thread th, th1;
        
        unsafe void LOL(object o)
        {
            th = Thread.CurrentThread;
            while (true)
            {
                try
                {
                    Helper.system.getWaveData(Sam, Sam.Length, 0);
                    Helper.system.getWaveData(Sam1, Sam1.Length, 1);
                    //ws.Write(Sam);
                    lst.AddRange(Sam);lst.RemoveRange(0, Sam.Length);
                    lst1.AddRange(Sam1); lst1.RemoveRange(0, Sam1.Length);
                    /*
                    fixed (float* a = ls)
                    {
                        IntPtr b = new IntPtr(a);
                        Marshal.ReAllocCoTaskMem(b, sizeof(float) * MaxLength);
                        Marshal.Copy(ls.);
                        Queue<float> hjh = new Queue<float>();hjh.
                    }*/
                }catch{}
                Thread.Sleep(DrawingTime);
            }
        }

        Point[] pt = new Point[2];
        int MaxLength = 50000;
        volatile List<float> lst = new List<float>(new float[50000]);
        volatile List<float> lst1 = new List<float>(new float[50000]);
        public void DrawNormalizedAudio(/*ref */float[] data, PictureBox pb, Pen pen, Color BGColor, int BORDER_WIDTH=0)
        {
            Bitmap bmp;
            if (pb.Image == null) bmp = new Bitmap(pb.Width, pb.Height);
            else bmp = (Bitmap)pb.Image;

            //int BORDER_WIDTH = 5;
            int width = bmp.Width;// - (2 * BORDER_WIDTH);
            int height = bmp.Height;// - (2 * BORDER_WIDTH);

            using (Graphics g = Graphics.FromImage(bmp))
            {
                g.Clear(BGColor);
                int size = data.Length;
                for (int iPixel = 0; iPixel < width; iPixel++)
                {
                    // determine start and end points within WAV
                    int start = (int)((float)iPixel * ((float)size / (float)width));
                    int end = (int)((float)(iPixel + 1) * ((float)size / (float)width));
                    float min = float.MaxValue;
                    float max = float.MinValue;
                    for (int i = start; i < end; i++)
                    {
                        float val = data[i];
                        min = val < min ? val : min;
                        max = val > max ? val : max;
                    }
                    int yMax = BORDER_WIDTH + height - (int)((max + 1) * .5 * height);
                    int yMin = BORDER_WIDTH + height - (int)((min + 1) * .5 * height);
                    pt[0].X=pt[1].X=iPixel + BORDER_WIDTH;
                    pt[0].Y = yMax;
                    pt[1].Y=yMin;
                    g.DrawLines(pen, pt);
                }
            }
            pb.Image = bmp;
        }

        private void numericUpDown4_ValueChanged(object sender, EventArgs e)
        {

            MaxLength = (int)numericUpDown4.Value;

            //ls = new float[MaxLength];
            //ls1 = new float[MaxLength];
            if (checkBox1.Checked)
            {
                if (lst.Count>MaxLength) lst.RemoveRange(0, lst.Count - MaxLength);
                else if (lst.Count<MaxLength ) lst.InsertRange(0, new float[MaxLength - lst.Count]);
                if (lst1.Count>MaxLength ) lst1.RemoveRange(0, lst1.Count - MaxLength);
                else if (lst1.Count<MaxLength) lst1.InsertRange(0, new float[MaxLength - lst1.Count]);
            }
            //if (th1 != null && th1.ThreadState != ThreadState.Running)
                DrawWave();
            
            //lst = new List<float>((int)numericUpDown4.Value);
        }

        private void pictureBox1_Paint(object sender, PaintEventArgs e)
        {

        }

        int MaxBuffer = 50000;
        private void domainUpDown1_SelectedItemChanged(object sender, EventArgs e)
        {
            numericUpDown2.Value = 1024;
            numericUpDown3.Value = 18;
            switch (domainUpDown1.SelectedIndex)
            {
                case 0:numericUpDown4.Value=1560;break;//1:1
                case 1:numericUpDown4.Value=3125;break;//1:2
                case 2:numericUpDown4.Value=6250;break;//1:4
                case 3:numericUpDown4.Value=12500;break;//1:8
                case 4:numericUpDown4.Value=25000;break;//1:16
                case 5:numericUpDown4.Value=50000;break;//1:32
                case 6:numericUpDown4.Value=100000;break;//1:64
                case 7:numericUpDown4.Value=200000;break;//1:128
                case 8:numericUpDown4.Value=400000;break;//1:256
                case 9:numericUpDown4.Value=800000;break;//1:512
                case 10:numericUpDown4.Value=1000000;break;//1:1024
                case 11:numericUpDown4.Value=2000000;break;//1:2048
                case 12:numericUpDown4.Value=4000000;break;//1:4096
                case 13:numericUpDown4.Value=8000000;break;//1:8192
                case 14:numericUpDown4.Value=10000000;break;//1:16384
            }
        }

        private void numericUpDown5_ValueChanged(object sender, EventArgs e)
        {
            timer1.Interval = (int)numericUpDown5.Value;
            DrawSpeed = (int)numericUpDown5.Value;
        }

        bool DynamicAlloc;
        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox1.Checked) numericUpDown4_ValueChanged(null, null);
            else
            {
                if (lst.Count < 10000000) lst.InsertRange(0, new float[10000000 - lst.Count]);
                if (lst1.Count < 10000000) lst1.InsertRange(0, new float[10000000 - lst1.Count]);
            }
            DynamicAlloc = checkBox1.Checked;
        }

        private void WaveForm_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.KeyCode == Keys.F5)GC.Collect();
        }

        private void WaveForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            frm.IsClosing = true;
            frm.Close();
        }

        private void 설정ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frm.Show();
            frm.UpdateGrid();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            설정ToolStripMenuItem_Click(null, null);
            splitContainer1.ContextMenuStrip =this.ContextMenuStrip= null;
        }

        private void hsWaveDraw_MouseWheel(object sender, MouseEventArgs e)
        {
            // 휠을 아래로 내리면
            // 축소(Reduce)가 아닌 확대(Expand) 이므로 false 대입
            if (e.Delta > 0&&domainUpDown1.SelectedIndex>0)domainUpDown1.SelectedIndex--;
            else if (e.Delta < 0 && domainUpDown1.SelectedIndex < domainUpDown1.Items.Count-1) domainUpDown1.SelectedIndex++;
        }

        private void hsWaveDraw1_MouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button  == System.Windows.Forms.MouseButtons.Right && domainUpDown1.SelectedIndex > 0) domainUpDown1.SelectedIndex--;
            else if (e.Button == System.Windows.Forms.MouseButtons.Left && domainUpDown1.SelectedIndex < domainUpDown1.Items.Count - 1) domainUpDown1.SelectedIndex++;
            else if (e.Button == System.Windows.Forms.MouseButtons.XButton2&& domainUpDown1.SelectedIndex > 0)
                if (domainUpDown1.SelectedIndex > 1) domainUpDown1.SelectedIndex -= 2; else domainUpDown1.SelectedIndex--;
            else if (e.Button == System.Windows.Forms.MouseButtons.XButton1 && domainUpDown1.SelectedIndex < domainUpDown1.Items.Count - 1)
                if (domainUpDown1.SelectedIndex < domainUpDown1.Items.Count-2) domainUpDown1.SelectedIndex+=2; else domainUpDown1.SelectedIndex ++;
        }

        bool checkBox1_Checked;
        private void button2_Click(object sender, EventArgs e)
        {
            if (th1 != null) th1.Abort(); if (th != null) th.Abort();
            if (button2.Text == " ")
            {
                checkBox1.Checked = checkBox1_Checked;
                checkBox1.Enabled = true;
                button2.Text = "";
                button2.BackgroundImage = Properties.Resources.PauseHS;
                ThreadPool.QueueUserWorkItem(new WaitCallback(LOL));
                ThreadPool.QueueUserWorkItem(new WaitCallback(LOLOL));
            }
            else
            {
                checkBox1_Checked = checkBox1.Checked; 
                checkBox1.Enabled =  checkBox1.Checked = false;
                button2.Text = " "; 
                button2.BackgroundImage = Properties.Resources.PlayHS;
            }
        }

        private void WaveForm_SizeChanged(object sender, EventArgs e)
        {
            DrawWave();
        }
        object o = new object();
        void DrawWave()
        {
            lock (o)
            {
                try
                {//try {for (int i =0; i < MaxLength; i++) {ls[i] = lst[lst.Count-MaxLength+i]; ls1[i] = lst1[lst1.Count-MaxLength+i];}}catch{}

                    if (DynamicAlloc || (lst.Count <= MaxLength || lst1.Count <= MaxLength)) {ls = lst.ToArray(); ls1 = lst1.ToArray();}
                    else
                    {
                        List<float> tmp = lst.GetRange(lst.Count - MaxLength - 1, MaxLength);
                        List<float> tmp1 = lst1.GetRange(lst1.Count - MaxLength - 1, MaxLength);
                        ls = tmp.ToArray(); tmp.Clear();
                        ls1 = tmp1.ToArray(); tmp1.Clear();
                    }
                    hsWaveDraw1.DrawNormalizedAudio(ls);
                    hsWaveDraw2.DrawNormalizedAudio(ls1);
                    Array.Clear(ls, 0, ls.Length); Array.Clear(ls1, 0, ls1.Length);
                    FPS = CalculateFrameRate();
                }
                catch{}
            }
        }
    }
}
