﻿namespace HS_Audio.Forms
{
    partial class frmFFTGraph
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            Stop = true;
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmFFTGraph));
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.timer2 = new System.Windows.Forms.Timer(this.components);
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.label1 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.timer3 = new System.Windows.Forms.Timer(this.components);
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.맨위에표시ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.배경투명처리ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.분할자투명ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.테두리표시ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.그래프늘이기ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.설정값ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.열기OToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.저장SToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.설정창띄우기ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.스펙트럼모양ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.fFTToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.웨이브데이터ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.그래프모양ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.그래프ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.파동ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.스펙트럼할당방법ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.fMODSystemToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.fMODChannelToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.fFTWindowToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripComboBox1 = new System.Windows.Forms.ToolStripComboBox();
            this.fFT사이즈ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripComboBox2 = new System.Windows.Forms.ToolStripComboBox();
            this.그래프굵기ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripTextBox1 = new System.Windows.Forms.ToolStripTextBox();
            this.똑같이나누기ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.그래프오차ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripTextBox2 = new System.Windows.Forms.ToolStripTextBox();
            this.그래프간격ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripTextBox4 = new System.Windows.Forms.ToolStripTextBox();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.그리기다른알고리즘ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.웨이브그래프ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripComboBox3 = new System.Windows.Forms.ToolStripComboBox();
            this.toolStripTextBox3 = new System.Windows.Forms.ToolStripTextBox();
            this.fFT그래프ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.최소DBToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.그리기영역자동조절ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.contextMenuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // timer1
            // 
            this.timer1.Interval = 25;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // timer2
            // 
            this.timer2.Interval = 25;
            this.timer2.Tick += new System.EventHandler(this.timer2_Tick);
            // 
            // splitContainer1
            // 
            this.splitContainer1.BackColor = System.Drawing.SystemColors.ControlDark;
            this.splitContainer1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.BackColor = System.Drawing.Color.White;
            this.splitContainer1.Panel1.Controls.Add(this.label1);
            this.splitContainer1.Panel1.Controls.Add(this.pictureBox1);
            this.splitContainer1.Panel1.SizeChanged += new System.EventHandler(this.splitContainer1_Panel1_SizeChanged);
            this.splitContainer1.Panel1MinSize = 1;
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.BackColor = System.Drawing.Color.White;
            this.splitContainer1.Panel2.Controls.Add(this.pictureBox2);
            this.splitContainer1.Panel2.SizeChanged += new System.EventHandler(this.splitContainer1_Panel2_SizeChanged);
            this.splitContainer1.Panel2MinSize = 1;
            this.splitContainer1.Size = new System.Drawing.Size(801, 291);
            this.splitContainer1.SplitterDistance = 142;
            this.splitContainer1.SplitterWidth = 3;
            this.splitContainer1.TabIndex = 4;
            this.splitContainer1.SizeChanged += new System.EventHandler(this.splitContainer1_SizeChanged);
            this.splitContainer1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.splitContainer1_KeyDown);
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(713, 3);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(48, 12);
            this.label1.TabIndex = 6;
            this.label1.Text = "FPS: 00";
            this.label1.Visible = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox1.Location = new System.Drawing.Point(0, 0);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(799, 141);
            this.pictureBox1.TabIndex = 4;
            this.pictureBox1.TabStop = false;
            this.toolTip1.SetToolTip(this.pictureBox1, "좌");
            this.pictureBox1.SizeChanged += new System.EventHandler(this.pictureBox2_SizeChanged);
            this.pictureBox1.DoubleClick += new System.EventHandler(this.pictureBox1_DoubleClick);
            // 
            // pictureBox2
            // 
            this.pictureBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.pictureBox2.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox2.Location = new System.Drawing.Point(0, 0);
            this.pictureBox2.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(799, 145);
            this.pictureBox2.TabIndex = 6;
            this.pictureBox2.TabStop = false;
            this.toolTip1.SetToolTip(this.pictureBox2, "우");
            this.pictureBox2.SizeChanged += new System.EventHandler(this.pictureBox2_SizeChanged);
            this.pictureBox2.Click += new System.EventHandler(this.pictureBox2_Click);
            this.pictureBox2.DoubleClick += new System.EventHandler(this.pictureBox1_DoubleClick);
            // 
            // timer3
            // 
            this.timer3.Interval = 50;
            this.timer3.Tick += new System.EventHandler(this.timer3_Tick);
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.맨위에표시ToolStripMenuItem,
            this.배경투명처리ToolStripMenuItem,
            this.테두리표시ToolStripMenuItem,
            this.그래프늘이기ToolStripMenuItem,
            this.toolStripSeparator4,
            this.설정값ToolStripMenuItem,
            this.toolStripSeparator3,
            this.설정창띄우기ToolStripMenuItem,
            this.toolStripSeparator1,
            this.스펙트럼모양ToolStripMenuItem,
            this.그래프모양ToolStripMenuItem,
            this.스펙트럼할당방법ToolStripMenuItem,
            this.toolStripSeparator2,
            this.fFTWindowToolStripMenuItem,
            this.fFT사이즈ToolStripMenuItem,
            this.그래프굵기ToolStripMenuItem,
            this.그래프오차ToolStripMenuItem,
            this.그래프간격ToolStripMenuItem,
            this.toolStripSeparator5,
            this.그리기다른알고리즘ToolStripMenuItem,
            this.그리기영역자동조절ToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(195, 408);
            this.contextMenuStrip1.Opening += new System.ComponentModel.CancelEventHandler(this.contextMenuStrip1_Opening);
            // 
            // 맨위에표시ToolStripMenuItem
            // 
            this.맨위에표시ToolStripMenuItem.CheckOnClick = true;
            this.맨위에표시ToolStripMenuItem.Image = global::HS_Audio.Properties.Resources.PushpinHS;
            this.맨위에표시ToolStripMenuItem.Name = "맨위에표시ToolStripMenuItem";
            this.맨위에표시ToolStripMenuItem.Size = new System.Drawing.Size(194, 22);
            this.맨위에표시ToolStripMenuItem.Text = "맨위에 표시";
            this.맨위에표시ToolStripMenuItem.ToolTipText = "이 창을 맨위로 설정합니다.";
            this.맨위에표시ToolStripMenuItem.Click += new System.EventHandler(this.맨위에표시ToolStripMenuItem_Click);
            // 
            // 배경투명처리ToolStripMenuItem
            // 
            this.배경투명처리ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.분할자투명ToolStripMenuItem});
            this.배경투명처리ToolStripMenuItem.Name = "배경투명처리ToolStripMenuItem";
            this.배경투명처리ToolStripMenuItem.Size = new System.Drawing.Size(194, 22);
            this.배경투명처리ToolStripMenuItem.Text = "배경 투명처리";
            this.배경투명처리ToolStripMenuItem.ToolTipText = "백그라운드 배경을 투명 처리합니다.\r\n[단축키: F4]";
            this.배경투명처리ToolStripMenuItem.CheckedChanged += new System.EventHandler(this.배경투명처리ToolStripMenuItem_CheckedChanged);
            this.배경투명처리ToolStripMenuItem.Click += new System.EventHandler(this.배경투명처리ToolStripMenuItem_Click);
            // 
            // 분할자투명ToolStripMenuItem
            // 
            this.분할자투명ToolStripMenuItem.CheckOnClick = true;
            this.분할자투명ToolStripMenuItem.Name = "분할자투명ToolStripMenuItem";
            this.분할자투명ToolStripMenuItem.Size = new System.Drawing.Size(138, 22);
            this.분할자투명ToolStripMenuItem.Text = "분할자 투명";
            this.분할자투명ToolStripMenuItem.CheckedChanged += new System.EventHandler(this.분할자투명ToolStripMenuItem_CheckedChanged);
            // 
            // 테두리표시ToolStripMenuItem
            // 
            this.테두리표시ToolStripMenuItem.Checked = true;
            this.테두리표시ToolStripMenuItem.CheckOnClick = true;
            this.테두리표시ToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.테두리표시ToolStripMenuItem.Name = "테두리표시ToolStripMenuItem";
            this.테두리표시ToolStripMenuItem.Size = new System.Drawing.Size(194, 22);
            this.테두리표시ToolStripMenuItem.Text = "테두리 표시";
            this.테두리표시ToolStripMenuItem.CheckedChanged += new System.EventHandler(this.테두리표시ToolStripMenuItem_CheckedChanged);
            // 
            // 그래프늘이기ToolStripMenuItem
            // 
            this.그래프늘이기ToolStripMenuItem.CheckOnClick = true;
            this.그래프늘이기ToolStripMenuItem.Name = "그래프늘이기ToolStripMenuItem";
            this.그래프늘이기ToolStripMenuItem.Size = new System.Drawing.Size(194, 22);
            this.그래프늘이기ToolStripMenuItem.Text = "그래프 늘이기";
            this.그래프늘이기ToolStripMenuItem.CheckedChanged += new System.EventHandler(this.그래프늘이기ToolStripMenuItem_CheckedChanged);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(191, 6);
            // 
            // 설정값ToolStripMenuItem
            // 
            this.설정값ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.열기OToolStripMenuItem,
            this.저장SToolStripMenuItem});
            this.설정값ToolStripMenuItem.Name = "설정값ToolStripMenuItem";
            this.설정값ToolStripMenuItem.Size = new System.Drawing.Size(194, 22);
            this.설정값ToolStripMenuItem.Text = "설정값";
            // 
            // 열기OToolStripMenuItem
            // 
            this.열기OToolStripMenuItem.Name = "열기OToolStripMenuItem";
            this.열기OToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.O)));
            this.열기OToolStripMenuItem.Size = new System.Drawing.Size(158, 22);
            this.열기OToolStripMenuItem.Text = "열기(&O)";
            this.열기OToolStripMenuItem.Click += new System.EventHandler(this.열기OToolStripMenuItem_Click);
            // 
            // 저장SToolStripMenuItem
            // 
            this.저장SToolStripMenuItem.Name = "저장SToolStripMenuItem";
            this.저장SToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
            this.저장SToolStripMenuItem.Size = new System.Drawing.Size(158, 22);
            this.저장SToolStripMenuItem.Text = "저장(&S)";
            this.저장SToolStripMenuItem.Click += new System.EventHandler(this.저장SToolStripMenuItem_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(191, 6);
            // 
            // 설정창띄우기ToolStripMenuItem
            // 
            this.설정창띄우기ToolStripMenuItem.Name = "설정창띄우기ToolStripMenuItem";
            this.설정창띄우기ToolStripMenuItem.Size = new System.Drawing.Size(194, 22);
            this.설정창띄우기ToolStripMenuItem.Text = "고급 설정 창 띄우기";
            this.설정창띄우기ToolStripMenuItem.ToolTipText = "그래프의 고급 설정창을 띄웁니다.\r\n(스펙트럼 설정 이외에는 설정하지 말아주세요) ";
            this.설정창띄우기ToolStripMenuItem.Click += new System.EventHandler(this.설정창띄우기ToolStripMenuItem_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(191, 6);
            // 
            // 스펙트럼모양ToolStripMenuItem
            // 
            this.스펙트럼모양ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fFTToolStripMenuItem,
            this.웨이브데이터ToolStripMenuItem});
            this.스펙트럼모양ToolStripMenuItem.Name = "스펙트럼모양ToolStripMenuItem";
            this.스펙트럼모양ToolStripMenuItem.Size = new System.Drawing.Size(194, 22);
            this.스펙트럼모양ToolStripMenuItem.Text = "스펙트럼 그리기 방법";
            // 
            // fFTToolStripMenuItem
            // 
            this.fFTToolStripMenuItem.Checked = true;
            this.fFTToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.fFTToolStripMenuItem.Name = "fFTToolStripMenuItem";
            this.fFTToolStripMenuItem.Size = new System.Drawing.Size(150, 22);
            this.fFTToolStripMenuItem.Text = "FFT-EQ";
            this.fFTToolStripMenuItem.ToolTipText = "FFT그래프를 Hz대역으로 그립니다.";
            this.fFTToolStripMenuItem.Click += new System.EventHandler(this.fFTToolStripMenuItem_Click);
            // 
            // 웨이브데이터ToolStripMenuItem
            // 
            this.웨이브데이터ToolStripMenuItem.Name = "웨이브데이터ToolStripMenuItem";
            this.웨이브데이터ToolStripMenuItem.Size = new System.Drawing.Size(150, 22);
            this.웨이브데이터ToolStripMenuItem.Text = "웨이브 데이터";
            this.웨이브데이터ToolStripMenuItem.ToolTipText = "출력사운드를 그래프로 그립니다.";
            this.웨이브데이터ToolStripMenuItem.Click += new System.EventHandler(this.fFTToolStripMenuItem_Click);
            // 
            // 그래프모양ToolStripMenuItem
            // 
            this.그래프모양ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.그래프ToolStripMenuItem,
            this.파동ToolStripMenuItem});
            this.그래프모양ToolStripMenuItem.Name = "그래프모양ToolStripMenuItem";
            this.그래프모양ToolStripMenuItem.Size = new System.Drawing.Size(194, 22);
            this.그래프모양ToolStripMenuItem.Text = "그래프 모양";
            // 
            // 그래프ToolStripMenuItem
            // 
            this.그래프ToolStripMenuItem.Name = "그래프ToolStripMenuItem";
            this.그래프ToolStripMenuItem.Size = new System.Drawing.Size(110, 22);
            this.그래프ToolStripMenuItem.Text = "그래프";
            this.그래프ToolStripMenuItem.ToolTipText = "막대 형태로 표시됩니다.";
            this.그래프ToolStripMenuItem.Click += new System.EventHandler(this.그래프ToolStripMenuItem_Click);
            // 
            // 파동ToolStripMenuItem
            // 
            this.파동ToolStripMenuItem.Checked = true;
            this.파동ToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.파동ToolStripMenuItem.Name = "파동ToolStripMenuItem";
            this.파동ToolStripMenuItem.Size = new System.Drawing.Size(110, 22);
            this.파동ToolStripMenuItem.Text = "파동";
            this.파동ToolStripMenuItem.ToolTipText = "선 형태로 표시됩니다.";
            this.파동ToolStripMenuItem.Click += new System.EventHandler(this.그래프ToolStripMenuItem_Click);
            // 
            // 스펙트럼할당방법ToolStripMenuItem
            // 
            this.스펙트럼할당방법ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fMODSystemToolStripMenuItem,
            this.fMODChannelToolStripMenuItem});
            this.스펙트럼할당방법ToolStripMenuItem.Name = "스펙트럼할당방법ToolStripMenuItem";
            this.스펙트럼할당방법ToolStripMenuItem.Size = new System.Drawing.Size(194, 22);
            this.스펙트럼할당방법ToolStripMenuItem.Text = "스펙트럼 할당 방법";
            // 
            // fMODSystemToolStripMenuItem
            // 
            this.fMODSystemToolStripMenuItem.Checked = true;
            this.fMODSystemToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.fMODSystemToolStripMenuItem.Name = "fMODSystemToolStripMenuItem";
            this.fMODSystemToolStripMenuItem.Size = new System.Drawing.Size(143, 22);
            this.fMODSystemToolStripMenuItem.Text = "Main Output";
            this.fMODSystemToolStripMenuItem.ToolTipText = "마스터 출력 입니다.";
            this.fMODSystemToolStripMenuItem.Click += new System.EventHandler(this.fMODSystemToolStripMenuItem_Click);
            // 
            // fMODChannelToolStripMenuItem
            // 
            this.fMODChannelToolStripMenuItem.Name = "fMODChannelToolStripMenuItem";
            this.fMODChannelToolStripMenuItem.Size = new System.Drawing.Size(143, 22);
            this.fMODChannelToolStripMenuItem.Text = "Channel";
            this.fMODChannelToolStripMenuItem.ToolTipText = "개별 출력 입니다.";
            this.fMODChannelToolStripMenuItem.Click += new System.EventHandler(this.fMODSystemToolStripMenuItem_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(191, 6);
            // 
            // fFTWindowToolStripMenuItem
            // 
            this.fFTWindowToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripComboBox1});
            this.fFTWindowToolStripMenuItem.Name = "fFTWindowToolStripMenuItem";
            this.fFTWindowToolStripMenuItem.Size = new System.Drawing.Size(194, 22);
            this.fFTWindowToolStripMenuItem.Text = "FFT Window";
            this.fFTWindowToolStripMenuItem.ToolTipText = "FFT Window를 계산할 방법입니다.\r\n(해당항목을 선택하고 마우스를 올리면 계산식이 표시됩니다.)";
            this.fFTWindowToolStripMenuItem.DropDownOpening += new System.EventHandler(this.fFTWindowToolStripMenuItem_DropDownOpening);
            // 
            // toolStripComboBox1
            // 
            this.toolStripComboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.toolStripComboBox1.Items.AddRange(new object[] {
            "RECT",
            "TRIANGLE",
            "HAMMING",
            "HANNING",
            "BLACKMAN",
            "BLACKMANHARRIS",
            "MAX"});
            this.toolStripComboBox1.Name = "toolStripComboBox1";
            this.toolStripComboBox1.Size = new System.Drawing.Size(121, 23);
            this.toolStripComboBox1.SelectedIndexChanged += new System.EventHandler(this.toolStripComboBox1_SelectedIndexChanged);
            this.toolStripComboBox1.MouseEnter += new System.EventHandler(this.toolStripComboBox1_MouseEnter);
            this.toolStripComboBox1.TextChanged += new System.EventHandler(this.toolStripComboBox1_TextChanged);
            // 
            // fFT사이즈ToolStripMenuItem
            // 
            this.fFT사이즈ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripComboBox2});
            this.fFT사이즈ToolStripMenuItem.Name = "fFT사이즈ToolStripMenuItem";
            this.fFT사이즈ToolStripMenuItem.Size = new System.Drawing.Size(194, 22);
            this.fFT사이즈ToolStripMenuItem.Text = "FFT 사이즈";
            this.fFT사이즈ToolStripMenuItem.DropDownOpening += new System.EventHandler(this.fFT사이즈ToolStripMenuItem_DropDownOpening);
            // 
            // toolStripComboBox2
            // 
            this.toolStripComboBox2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.toolStripComboBox2.Items.AddRange(new object[] {
            "4096",
            "2048",
            "1024",
            "512",
            "256",
            "128",
            "64",
            "None"});
            this.toolStripComboBox2.Name = "toolStripComboBox2";
            this.toolStripComboBox2.Size = new System.Drawing.Size(121, 23);
            this.toolStripComboBox2.SelectedIndexChanged += new System.EventHandler(this.toolStripComboBox2_SelectedIndexChanged);
            // 
            // 그래프굵기ToolStripMenuItem
            // 
            this.그래프굵기ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripTextBox1,
            this.똑같이나누기ToolStripMenuItem});
            this.그래프굵기ToolStripMenuItem.Name = "그래프굵기ToolStripMenuItem";
            this.그래프굵기ToolStripMenuItem.Size = new System.Drawing.Size(194, 22);
            this.그래프굵기ToolStripMenuItem.Text = "그래프 굵기";
            this.그래프굵기ToolStripMenuItem.DropDownOpening += new System.EventHandler(this.그래프굵기ToolStripMenuItem_DropDownOpening);
            // 
            // toolStripTextBox1
            // 
            this.toolStripTextBox1.Name = "toolStripTextBox1";
            this.toolStripTextBox1.Size = new System.Drawing.Size(100, 23);
            this.toolStripTextBox1.Text = "1";
            this.toolStripTextBox1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.toolStripTextBox1_KeyPress);
            this.toolStripTextBox1.TextChanged += new System.EventHandler(this.toolStripTextBox1_TextChanged);
            // 
            // 똑같이나누기ToolStripMenuItem
            // 
            this.똑같이나누기ToolStripMenuItem.Checked = true;
            this.똑같이나누기ToolStripMenuItem.CheckOnClick = true;
            this.똑같이나누기ToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.똑같이나누기ToolStripMenuItem.Name = "똑같이나누기ToolStripMenuItem";
            this.똑같이나누기ToolStripMenuItem.Size = new System.Drawing.Size(160, 22);
            this.똑같이나누기ToolStripMenuItem.Text = "똑같이 나누기";
            this.똑같이나누기ToolStripMenuItem.ToolTipText = "굵기에 비례해서 Hz대역도 똑같이 나눕니다.";
            this.똑같이나누기ToolStripMenuItem.CheckedChanged += new System.EventHandler(this.똑같이나누기ToolStripMenuItem_CheckedChanged);
            // 
            // 그래프오차ToolStripMenuItem
            // 
            this.그래프오차ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripTextBox2});
            this.그래프오차ToolStripMenuItem.Name = "그래프오차ToolStripMenuItem";
            this.그래프오차ToolStripMenuItem.Size = new System.Drawing.Size(194, 22);
            this.그래프오차ToolStripMenuItem.Text = "그래프 오차";
            this.그래프오차ToolStripMenuItem.DropDownOpening += new System.EventHandler(this.그래프오차ToolStripMenuItem_DropDownOpening);
            // 
            // toolStripTextBox2
            // 
            this.toolStripTextBox2.Name = "toolStripTextBox2";
            this.toolStripTextBox2.Size = new System.Drawing.Size(100, 23);
            this.toolStripTextBox2.Text = "2";
            this.toolStripTextBox2.ToolTipText = "이 값을 크게주면 dB 측정범위가 늘어나긴하지만\r\n작은소리도 그래프가 커져서 측정이 정확하지 않을 수 있습니다.\r\n상황에 맞게 적절하게 사용하십시" +
    "오";
            this.toolStripTextBox2.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.toolStripTextBox2_KeyPress);
            this.toolStripTextBox2.TextChanged += new System.EventHandler(this.toolStripTextBox2_TextChanged);
            // 
            // 그래프간격ToolStripMenuItem
            // 
            this.그래프간격ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripTextBox4});
            this.그래프간격ToolStripMenuItem.Name = "그래프간격ToolStripMenuItem";
            this.그래프간격ToolStripMenuItem.Size = new System.Drawing.Size(194, 22);
            this.그래프간격ToolStripMenuItem.Text = "막대 그래프 간격";
            // 
            // toolStripTextBox4
            // 
            this.toolStripTextBox4.Name = "toolStripTextBox4";
            this.toolStripTextBox4.Size = new System.Drawing.Size(100, 23);
            this.toolStripTextBox4.Text = "0";
            this.toolStripTextBox4.ToolTipText = "이 값을 크게주면 dB 측정범위가 늘어나긴하지만\r\n작은소리도 그래프가 커져서 측정이 정확하지 않을 수 있습니다.\r\n상황에 맞게 적절하게 사용하십시" +
    "오";
            this.toolStripTextBox4.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.toolStripTextBox4_KeyPress);
            this.toolStripTextBox4.TextChanged += new System.EventHandler(this.toolStripTextBox4_TextChanged);
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(191, 6);
            // 
            // 그리기다른알고리즘ToolStripMenuItem
            // 
            this.그리기다른알고리즘ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.웨이브그래프ToolStripMenuItem,
            this.fFT그래프ToolStripMenuItem});
            this.그리기다른알고리즘ToolStripMenuItem.Name = "그리기다른알고리즘ToolStripMenuItem";
            this.그리기다른알고리즘ToolStripMenuItem.Size = new System.Drawing.Size(194, 22);
            this.그리기다른알고리즘ToolStripMenuItem.Text = "그리기 다른 알고리즘";
            this.그리기다른알고리즘ToolStripMenuItem.CheckedChanged += new System.EventHandler(this.그리기다른알고리즘ToolStripMenuItem_CheckedChanged);
            // 
            // 웨이브그래프ToolStripMenuItem
            // 
            this.웨이브그래프ToolStripMenuItem.CheckOnClick = true;
            this.웨이브그래프ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripComboBox3,
            this.toolStripTextBox3});
            this.웨이브그래프ToolStripMenuItem.Name = "웨이브그래프ToolStripMenuItem";
            this.웨이브그래프ToolStripMenuItem.Size = new System.Drawing.Size(150, 22);
            this.웨이브그래프ToolStripMenuItem.Text = "웨이브 그래프";
            this.웨이브그래프ToolStripMenuItem.CheckedChanged += new System.EventHandler(this.그리기다른알고리즘ToolStripMenuItem_CheckedChanged);
            // 
            // toolStripComboBox3
            // 
            this.toolStripComboBox3.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.toolStripComboBox3.Items.AddRange(new object[] {
            "1:1",
            "1:2",
            "1:4",
            "1:8",
            "1:16",
            "1:32",
            "1:64",
            "1:128",
            "1:256",
            "1:512",
            "1:1024",
            "1:2048",
            "1:4096",
            "1:8192"});
            this.toolStripComboBox3.Name = "toolStripComboBox3";
            this.toolStripComboBox3.Size = new System.Drawing.Size(121, 23);
            this.toolStripComboBox3.SelectedIndexChanged += new System.EventHandler(this.toolStripComboBox3_SelectedIndexChanged);
            // 
            // toolStripTextBox3
            // 
            this.toolStripTextBox3.Name = "toolStripTextBox3";
            this.toolStripTextBox3.Size = new System.Drawing.Size(100, 23);
            this.toolStripTextBox3.Text = "1024";
            this.toolStripTextBox3.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.toolStripTextBox3_KeyPress);
            this.toolStripTextBox3.TextChanged += new System.EventHandler(this.toolStripTextBox3_TextChanged);
            // 
            // fFT그래프ToolStripMenuItem
            // 
            this.fFT그래프ToolStripMenuItem.CheckOnClick = true;
            this.fFT그래프ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.최소DBToolStripMenuItem});
            this.fFT그래프ToolStripMenuItem.Name = "fFT그래프ToolStripMenuItem";
            this.fFT그래프ToolStripMenuItem.Size = new System.Drawing.Size(150, 22);
            this.fFT그래프ToolStripMenuItem.Text = "FFT 그래프";
            // 
            // 최소DBToolStripMenuItem
            // 
            this.최소DBToolStripMenuItem.Name = "최소DBToolStripMenuItem";
            this.최소DBToolStripMenuItem.Size = new System.Drawing.Size(116, 22);
            this.최소DBToolStripMenuItem.Text = "최소 dB";
            // 
            // 그리기영역자동조절ToolStripMenuItem
            // 
            this.그리기영역자동조절ToolStripMenuItem.CheckOnClick = true;
            this.그리기영역자동조절ToolStripMenuItem.Name = "그리기영역자동조절ToolStripMenuItem";
            this.그리기영역자동조절ToolStripMenuItem.Size = new System.Drawing.Size(194, 22);
            this.그리기영역자동조절ToolStripMenuItem.Text = "그리기 영역 자동 조절";
            this.그리기영역자동조절ToolStripMenuItem.CheckedChanged += new System.EventHandler(this.그리기영역자동조절ToolStripMenuItem_CheckedChanged);
            // 
            // toolTip1
            // 
            this.toolTip1.AutomaticDelay = 200;
            this.toolTip1.BackColor = System.Drawing.Color.Black;
            this.toolTip1.ForeColor = System.Drawing.Color.White;
            this.toolTip1.IsBalloon = true;
            this.toolTip1.OwnerDraw = true;
            this.toolTip1.Draw += new System.Windows.Forms.DrawToolTipEventHandler(this.toolTip1_Draw);
            this.toolTip1.Popup += new System.Windows.Forms.PopupEventHandler(this.toolTip1_Popup);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.DefaultExt = "*.fftini";
            this.openFileDialog1.Filter = "FFT 그래프 설정 파일 (*.fftini)|*.fftini|기본 설정 파일 (*.ini)|*.ini";
            this.openFileDialog1.Title = "구성 설정 파일 열기";
            // 
            // saveFileDialog1
            // 
            this.saveFileDialog1.DefaultExt = "*.fftini";
            this.saveFileDialog1.Filter = "FFT 그래프 설정 파일 (*.fftini)|*.fftini|기본 설정 파일 (*.ini)|*.ini";
            this.saveFileDialog1.Title = "구성 설정 파일 저장";
            // 
            // frmFFTGraph
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.Color.Silver;
            this.ClientSize = new System.Drawing.Size(801, 291);
            this.ContextMenuStrip = this.contextMenuStrip1;
            this.Controls.Add(this.splitContainer1);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.Name = "frmFFTGraph";
            this.Text = "FFT그래프 [좌: Inf. dB / 우: Inf. dB] (FPS: 00)";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FFTGraph_FormClosed);
            this.Load += new System.EventHandler(this.FFTGraph_Load);
            this.SizeChanged += new System.EventHandler(this.FFTGraph_SizeChanged);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.splitContainer1_KeyDown);
            this.MouseMove += new System.Windows.Forms.MouseEventHandler(this.frmFFTGraph_MouseMove);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.PerformLayout();
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.contextMenuStrip1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Timer timer2;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Timer timer3;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem 설정창띄우기ToolStripMenuItem;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem 스펙트럼모양ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem fFTToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 웨이브데이터ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 그래프모양ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 그래프ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 파동ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 스펙트럼할당방법ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem fMODSystemToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem fMODChannelToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem fFTWindowToolStripMenuItem;
        private System.Windows.Forms.ToolStripComboBox toolStripComboBox1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem 그래프굵기ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 똑같이나누기ToolStripMenuItem;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.ToolStripTextBox toolStripTextBox1;
        private System.Windows.Forms.ToolStripMenuItem 그래프오차ToolStripMenuItem;
        private System.Windows.Forms.ToolStripTextBox toolStripTextBox2;
        private System.Windows.Forms.ToolStripMenuItem 맨위에표시ToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripMenuItem 배경투명처리ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 테두리표시ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 분할자투명ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem fFT사이즈ToolStripMenuItem;
        private System.Windows.Forms.ToolStripComboBox toolStripComboBox2;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripMenuItem 설정값ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 열기OToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 저장SToolStripMenuItem;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.ToolStripMenuItem 그리기다른알고리즘ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 그래프늘이기ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 그리기영역자동조절ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 그래프간격ToolStripMenuItem;
        private System.Windows.Forms.ToolStripTextBox toolStripTextBox4;
        private System.Windows.Forms.ToolStripMenuItem 웨이브그래프ToolStripMenuItem;
        private System.Windows.Forms.ToolStripComboBox toolStripComboBox3;
        private System.Windows.Forms.ToolStripMenuItem fFT그래프ToolStripMenuItem;
        private System.Windows.Forms.ToolStripTextBox toolStripTextBox3;
        private System.Windows.Forms.ToolStripMenuItem 최소DBToolStripMenuItem;
    }
}