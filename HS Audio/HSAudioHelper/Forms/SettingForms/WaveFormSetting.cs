﻿using System;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using HS_Audio.Control;

namespace HS_Audio.Forms.Setting
{
    public partial class WaveFormSetting : Form
    {
        public WaveFormSetting()
        {
            InitializeComponent();
        }
        public WaveFormSetting(IHSWaveDraw GraphL, IHSWaveDraw GraphR, Row RowL, Row RowR)
        {
            InitializeComponent();
            prg좌그래프.SelectedObject = GraphR;
            prg우그래프.SelectedObject = GraphL;
            prg좌모눈선.SelectedObject = RowR;
            prg우모눈선.SelectedObject = RowL;
        }
        public bool IsClosing = false;
        private void WaveFormSetting_Load(object sender, EventArgs e)
        {

        }

        public void UpdateGrid()
        {
            prg좌그래프.Refresh();
            prg우그래프.Refresh();
            prg좌모눈선.Refresh();
            prg우모눈선.Refresh();
        }
        private void WaveFormSetting_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = !IsClosing;
            this.Hide();
        }
    }
}
