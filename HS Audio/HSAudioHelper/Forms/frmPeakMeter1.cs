﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using HS_Audio.Control;
using HS_CSharpUtility;
using Polenter.Serialization.Core;

namespace HS_Audio.Forms
{
    public partial class frmPeakMeter1 : HS_Audio.Control.HSCustomForm, IHSSetting
    {
        #region 사운드 유틸리티
        string _ConvertdBToString;
        public string ConvertdBToString(float Sample, bool PlusWhen0Over = false)
        {
            if (Sample == float.PositiveInfinity) { _ConvertdBToString = "Inf."; }
            else if (DecibelsforSound(Sample) < -360 || Sample == float.NegativeInfinity || Sample == 0) _ConvertdBToString = "-Inf.";
            else
            {
                double db = DecibelsforSound(Sample);
                if (PlusWhen0Over && (db>-0.0004&&db<0))_ConvertdBToString = db.ToString(" 0.000");
                else if (PlusWhen0Over && db == 0) _ConvertdBToString = " 0.000";
                //else if (PlusWhen0Over && (db<0.0005&&db > 0)) _ConvertdBToString = db.ToString("+0.000");
                else if (PlusWhen0Over && db > 0) _ConvertdBToString = db.ToString("+0.000");
                else _ConvertdBToString = db.ToString("0.000");
            }
            return _ConvertdBToString;
        }
        public string ConvertdBToString(float Sample, string Format)
        {
            if (Sample == float.PositiveInfinity) { _ConvertdBToString = "Inf."; }
            else if (DecibelsforSound(Sample) < -360 || Sample == float.NegativeInfinity || Sample == 0) _ConvertdBToString = "-Inf.";
            else if(Sample==0)_ConvertdBToString = Sample.ToString(Format);
            return _ConvertdBToString;
        }

        public static double mag_sqrd(double re, double im) { return (re * re + im * im); }

        public static double Decibels(double re, double im) {if(re==double.NegativeInfinity)return re; return ((re == 0 && im == 0) ? (double.NaN) : 10.0 * Math.Log10(((mag_sqrd(re, im))))); }
        public static double DecibelsforSound(double Sample, double Peak = 0) {if(Sample==double.NegativeInfinity)return Sample; return ((Sample == 0 && Peak == 0) ? (double.NegativeInfinity) : 10.0 * Math.Log10(((mag_sqrd(Sample, Peak)))));}
        #endregion

        HSAudioHelper Data;
        public frmPeakMeter1(HSAudioHelper Helper)
        {
            InitializeComponent();
            SetStyle(ControlStyles.SupportsTransparentBackColor, true);
            LED눈금갯수 = 50;
            this.Data = Helper;
            Ltmp = new float[2048];
            Rtmp = new float[2048];
            ProgressAnimate_Start = new ThreadStart(updateProgress);
            toolStripComboBox1.SelectedIndex = 2;
            toolStripComboBox1.Text = toolStripComboBox1.Items[2].ToString();
        }

        #region 프로퍼티
        bool _GetData = true;
        public bool GetData{get{return _GetData;}set{_GetData = value;}}

        int _LED눈금갯수 = 100000;
        public int LED눈금갯수{get{return progressBarGradient1.MaximumValue/_LED눈금갯수;}set{progressBarGradient1.MaximumValue=progressBarGradient2.MaximumValue=value*_LED눈금갯수;progressBarGradient1.MindB=progressBarGradient2.MindB=-value;}}
        
        private static int _sampleDelay = 60;
        private int sampleDelay = _sampleDelay;
        public int SampleDelay
		{
			get{return sampleDelay;}

			set
			{
				sampleDelay = Math.Max(0, value);
				if(frameDelay > sampleDelay)
                    frameDelay = sampleDelay;
			}
		}

        private int frameDelay = 10;
		public int FrameDelay
		{
			get{return frameDelay;}
			set{frameDelay = Math.Max(0, Math.Min(sampleDelay, value));}
		}
        #endregion

        #region 함수
        public void Start()
        {
            if(ProgressAnimate!=null)ProgressAnimate.Abort();
            ProgressAnimate = new Thread(ProgressAnimate_Start);
            ProgressAnimate.Start();
            timer1.Start();
        }
        
        public float[] InitData(int Length, float Value)
        {
            float[] f = new float[Length];
            for(int i=0;i<Length;i++)f[i] = Value;
            return f;
        }

        public static float CaculatorLengthforPercent(int n, float Percent)
        {
            return (n/100f)*Percent;
        }
        #endregion

        #region 그래프 그리기
        int numchannels = 2;
        
        float _MaxLdB = float.NegativeInfinity, 
              _MaxRdB = float.NegativeInfinity;
        public float MaxLdB { get{return _MaxLdB;} private set{_MaxLdB = value;} }
        public float MaxRdB { get{return _MaxRdB;} private set{_MaxRdB = value;} }
        string MaxLdBtxt = "-Inf.", MaxRdBtxt = "-Inf.";
        string Musicpath;
        int[] h = new int[2];
        public float[] Ltmp { get; internal set; } 
        public float[] Rtmp{get;internal set;}
        float LDB=float.NegativeInfinity, RDB=float.NegativeInfinity;
        double d1, d2;
        string Ltxt, Rtxt;
        private Color White_1 = Color.FromArgb(254, 255, 255);
        private void timer1_Tick(object sender, EventArgs e)
        {
            if (Data.system != null)
            {
                /*
                if (LDB.ToString().IndexOf("-3.899998E-19")!=-1 || 
                    LDB.ToString().IndexOf("7.39567E-17")!=-1||
                    LDB.ToString().IndexOf("7.395671E-17")!=-1||
                    LDB.ToString().IndexOf("5.277585") != -1 ||
                    LDB == 0) { Ltxt = "-Inf."; } else { Ltxt = Decibels(LDB, 0).ToString("0.000"); }

                if (RDB.ToString().IndexOf("-3.899998E-19") != -1 ||
                    RDB.ToString().IndexOf("7.39567E-17") != -1 ||
                    RDB.ToString().IndexOf("7.395671E-17") != -1 ||
                    RDB.ToString().IndexOf("5.277585") != -1 ||
                    RDB == 0) { Rtxt = "-Inf."; } else { Rtxt = Decibels(RDB, 0).ToString("0.000"); }
                 */
                //peakMeterCtrl1.SetRange(15, 25, 30);//100, 115, 120);
                //peakMeterCtrl2.SetRange(15, 25, 30);//100, 115, 120);

                if (Data.MusicPath != Musicpath)
                {
                    Musicpath = Data.MusicPath;
                    MaxLdB = MaxRdB = float.NegativeInfinity; MaxLdBtxt = MaxRdBtxt = "-Inf.";
                }
                this.Text = string.Format("Peak 미터1 [좌: {0} dB / 우: {1} dB] ({2}) - Peak [좌: {3} dB / 우: {4} dB]",
                    Ltxt, Rtxt, LDB == RDB ? "Mono" : "Stereo", MaxLdBtxt, MaxRdBtxt);

                if (!Smooth)
                {
                    Work();
                    
                    int MaxValue = progressBarGradient1.MaximumValue;
                    int MaxValue1 = progressBarGradient2.MaximumValue;

                    double aa = _LED눈금갯수 * d1;
                    aa = Math.Max(aa, -MaxValue);
                    int b = (int)(MaxValue + aa);
                    progressBarGradient1.Value = (int)Math.Min(b, MaxValue);

                    aa = _LED눈금갯수 * d2;
                    aa = Math.Max(aa, -MaxValue1);
                    b = (int)(MaxValue1 + aa);
                    progressBarGradient2.Value = (int)Math.Min(b, MaxValue1);
                }
                lbl_LeftPeakValue.Text = ConvertdBToString(LDB, true);
                lbl_RightPeakValue.Text = ConvertdBToString(RDB, true);


                if (d1 >= 0)
                {
                    lbl_LeftPeakValue.BackColor = Color.Red; lbl_LeftPeakValue.ForeColor = White_1;
                }
                else if (d1 >= -6 && d1 < 0)
                {
                    lbl_LeftPeakValue.BackColor = Color.Black; lbl_LeftPeakValue.ForeColor = White_1;
                }
                else
                {
                    lbl_LeftPeakValue.BackColor = Color.DarkGreen; lbl_LeftPeakValue.ForeColor = White_1;
                }

                if (d1 != 0)
                {

                    if (색깔적용ToolStripMenuItem.Checked)
                    {
                        if (d1 >= 0) lbl_LeftPeakValue.BackColor = Color.Red;
                        else if (d1 >= -6 && d1 < 0) lbl_LeftPeakValue.BackColor = Color.Black;
                        else lbl_LeftPeakValue.BackColor = Color.DarkGreen;
                        lbl_LeftPeakValue.ForeColor = White_1;
                    }
                    else
                    {
                        lbl_LeftPeakValue.BackColor = Color.Transparent;
                        lbl_LeftPeakValue.ForeColor = White_1;
                    }
                    lbl_LeftPeakValue.ForeColor = d1 > 0 ? White_1 : Color.Black;
                    lbl_LeftPeakValue.ForeColor = White_1;//Color.FromArgb(0,0,255);

                }
                else
                {
                    lbl_LeftPeakValue.BackColor = Color.Red; lbl_LeftPeakValue.ForeColor = White_1;
                }

                if (d2 != 0)//Decibels(RDB, 0) != 0)
                {

                    if (색깔적용ToolStripMenuItem.Checked)
                    {
                        if (d2 >= 0) lbl_RightPeakValue.BackColor = Color.Red;
                        else if (d2 >= -6 && d2 < 0) lbl_RightPeakValue.BackColor = Color.Black;
                        else lbl_RightPeakValue.BackColor = Color.DarkGreen;
                        lbl_RightPeakValue.ForeColor = White_1;
                    }
                    else
                    {
                        lbl_RightPeakValue.BackColor = Color.Transparent;
                        lbl_RightPeakValue.ForeColor = White_1;
                    }

                }
                else
                {
                    lbl_RightPeakValue.BackColor = Color.Red; lbl_RightPeakValue.ForeColor = White_1;
                }


                lbl_LeftPeak.Text = ConvertdBToString(MaxLdB, true);
                lbl_RightPeak.Text = ConvertdBToString(MaxRdB, true);
                double _d1 = DecibelsforSound(MaxLdB);
                if (_d1 != 0)
                {
                    if (색깔적용ToolStripMenuItem.Checked)
                    {
                        if (_d1 >= 0) lbl_LeftPeak.BackColor = Color.Red;
                        else if (_d1 >= -6 && d1 < 0) lbl_LeftPeak.BackColor = Color.Black;
                        else lbl_LeftPeak.BackColor = Color.DarkGreen;
                        lbl_LeftPeak.ForeColor = White_1;
                    }
                    else
                    {
                        lbl_LeftPeak.BackColor = Color.Transparent;
                        lbl_LeftPeak.ForeColor = White_1;
                    }
                    //lbl_LeftPeak.BackColor = d1 >= 0 ? Color.Red : White_1;
                    //lbl_LeftPeak.ForeColor = d1 >= 0 ? White_1 : Color.Black;
                }
                else
                {
                    lbl_LeftPeak.BackColor = Color.Red; lbl_LeftPeak.ForeColor = White_1;
                }

                double _d2 = DecibelsforSound(MaxRdB);
                if (_d2 != 0)
                {
                    if (색깔적용ToolStripMenuItem.Checked)
                    {
                        if (_d2 >= 0) lbl_RightPeak.BackColor = Color.Red;
                        else if (_d2 >= -6 && _d2 < 0) lbl_RightPeak.BackColor = Color.Black;
                        else lbl_RightPeak.BackColor = Color.DarkGreen;
                        lbl_RightPeak.ForeColor = White_1;
                    }
                    else
                    {
                        lbl_RightPeak.BackColor = Color.Transparent;
                        lbl_RightPeak.ForeColor = White_1;
                    }
                    //lbl_RightPeak.BackColor = d1 >= 0 ? Color.Red : White_1;
                    //lbl_RightPeak.ForeColor = d1 >= 0 ? White_1 : Color.Black;
                }
                else
                {
                    lbl_RightPeak.BackColor = Color.Red; lbl_RightPeak.ForeColor = White_1;
                }
            }
            if(!Smooth)LDB = RDB = float.NegativeInfinity;
        }

        private void Work()
        {
            #region
            if (Data.system != null)
            {
                //if (Ltmp.Length == 0) { Ltmp = new float[DefaultLED+1]; } if (Rtmp.Length == 0) { Rtmp = new float[DefaultLED]; }
                if (numchannels == 1)
                {
                    if (GetData) Data.system.getWaveData(Ltmp, Ltmp.Length, 0);
                    else Data.channel.getWaveData(Ltmp, Ltmp.Length, 0);

                    for (int i = 0; i < Ltmp.Length; i++)
                        if (Ltmp[i] > LDB)
                            LDB = Ltmp[i];

                    RDB = LDB;
                    //LDB = Ltmp[Ltmp.Length / 2];
                    //RDB = Rtmp[Rtmp.Length / 2];
                }
                if (numchannels == 2)
                {
                    if (GetData)
                    {
                        Data.system.getWaveData(Ltmp, Ltmp.Length, 0);
                        Data.system.getWaveData(Rtmp, Rtmp.Length < 2 ? 1 : Rtmp.Length, 1);
                    }
                    else
                    {
                        Data.channel.getWaveData(Ltmp, Ltmp.Length, 0);
                        Data.channel.getWaveData(Rtmp, Rtmp.Length, 1);
                    }

                    for (int i = 0; i < Ltmp.Length; i++)
                        if (Ltmp[i] > LDB)
                            LDB = Ltmp[i];

                    for (int i = 0; i < Rtmp.Length; i++)
                        if (Rtmp[i] > RDB)
                            RDB = Rtmp[i];
                    //LDB = Ltmp[Ltmp.Length / 2];
                    //RDB = Rtmp[Rtmp.Length / 2];
                }

                if (MaxLdB < LDB)
                {
                    MaxLdB = LDB;
                    MaxLdBtxt = ConvertdBToString(MaxLdB, true);
                }
                if (MaxRdB < RDB)
                {
                    MaxRdB = RDB;
                    MaxRdBtxt = ConvertdBToString(MaxRdB, true);
                }
                //Ltxt = ConvertdBToString(LDB, "0.000");Rtxt = ConvertdBToString(RDB, "0.000");
                Ltxt = ConvertdBToString(LDB, true); Rtxt = ConvertdBToString(RDB, true);

                d1 = DecibelsforSound(LDB);
                d2 = DecibelsforSound(RDB);
            }
            #endregion
        }

        bool Smooth = true;
        Thread ProgressAnimate;
        ThreadStart ProgressAnimate_Start;
        private void updateProgress()
		{
            while (Smooth)
            {
                try
                {
                    ProgressBarGradientInternal progressBar1 = progressBarGradient1;
                    ProgressBarGradientInternal progressBar2 = progressBarGradient2;

                    int tempFrameDelay = frameDelay;
                    int tempSampleDelay = sampleDelay;

                    int MaxValue = progressBarGradient1.MaximumValue;
                    int MaxValue1 = progressBarGradient2.MaximumValue;

                    // for each channel, determine the step size necessary for each iteration
                    int leftGoal = 0;
                    int rightGoal = 0;

                    #region
                    {
                        Work();
                        double aa = _LED눈금갯수 * d1;
                        aa = Math.Max(aa, -MaxValue);
                        int b = (int)(MaxValue + aa);
                        leftGoal = (int)Math.Min(b, MaxValue);

                        aa = _LED눈금갯수 * d2;
                        aa = Math.Max(aa, -MaxValue1);
                        b = (int)(MaxValue1 + aa);
                        rightGoal = (int)Math.Min(b, MaxValue1);
                    }
                    #endregion
                    /*
				// average across all samples to get the goals
				for(int i = 0; i < SAMPLES; i++)
				{
					leftGoal += (Int16)samples.GetValue(i, 0, 0);
					rightGoal += (Int16)samples.GetValue(i, 1, 0);
				}

				leftGoal = (int)Math.Abs(leftGoal / SAMPLES);
				rightGoal = (int)Math.Abs(rightGoal / SAMPLES);*/

                    double range1 = leftGoal - progressBarGradient1.Value;
                    double range2 = rightGoal - progressBarGradient2.Value;

                    double exactValue1 = progressBarGradient1.Value;
                    double exactValue2 = progressBarGradient2.Value;

                    double stepSize1 = range1 / tempSampleDelay * tempFrameDelay;
                    if (Math.Abs(stepSize1) < .01)
                    {
                        stepSize1 = Math.Sign(range1) * .01;
                    }
                    double absStepSize1 = Math.Abs(stepSize1);

                    double stepSize2 = range2 / tempSampleDelay * tempFrameDelay;
                    if (Math.Abs(stepSize2) < .01)
                    {
                        stepSize2 = Math.Sign(range2) * .01;
                    }
                    double absStepSize2 = Math.Abs(stepSize2);

                    // increment/decrement the bars' values until both equal their desired goals,
                    // sleeping between iterations
                    if ((progressBar1.Value == leftGoal) && (progressBar2.Value == rightGoal))
                    {
                        Thread.Sleep(tempSampleDelay);
                    }
                    else
                    {
                        do
                        {
                            if (progressBar1.Value != leftGoal)
                            {
                                if (absStepSize1 < Math.Abs(leftGoal - progressBar1.Value))
                                {
                                    exactValue1 += stepSize1;
                                    progressBar1.Value = (int)Math.Round(exactValue1);
                                }
                                else
                                {
                                    progressBar1.Value = leftGoal;
                                }
                            }

                            if (progressBar2.Value != rightGoal)
                            {
                                if (absStepSize2 < Math.Abs(rightGoal - progressBar2.Value))
                                {
                                    exactValue2 += stepSize2;
                                    progressBar2.Value = (int)Math.Round(exactValue2);
                                }
                                else
                                {
                                    progressBar2.Value = rightGoal;
                                }
                            }

                            Thread.Sleep(tempFrameDelay);
                        } while ((progressBar1.Value != leftGoal) || (progressBar2.Value != rightGoal));
                    }
                }
                catch{Thread.Sleep(100);}
                LDB = RDB = float.NegativeInfinity;
            }
		}
        #endregion

        #region 설정
        Dictionary<string, string> _Setting = new Dictionary<string,string>();
        internal Dictionary<string, string> Setting
        {
            get {return GetSetting(); }
            set
            {
                Dictionary<string, string> a = value;
                try{if(a.ContainsKey("FormLocation"))this.Location =HS_CSharpUtility.Utility.ConvertUtility.StringToPoint(a["FormLocation"]);}catch{}
                try{if(a.ContainsKey("FormOpacity"))this.Opacity =double.Parse(a["FormOpacity"]);}catch{}
                try{if(a.ContainsKey("TopMost"))맨위에표시ToolStripMenuItem.Checked = bool.Parse(a["TopMost"]);}catch{}
                try{if(a.ContainsKey("ShowEdge"))테두리표시ToolStripMenuItem.Checked=bool.Parse(a["ShowEdge"]);}catch{}
                try{if(a.ContainsKey("IsBackgrountTransparent"))배경투명ToolStripMenuItem.Checked=bool.Parse(a["IsBackgrountTransparent"]);}catch{}
                try{if(a.ContainsKey("IsSplitterTransparent"))분할자투명ToolStripMenuItem.Checked=bool.Parse(a["IsSplitterTransparent"]);}catch{}
                try{if(a.ContainsKey("IsShowdBLable"))레벨레이블보이기ToolStripMenuItem.Checked = bool.Parse(a["IsShowdBLable"]);}catch{}
                try{if(a.ContainsKey("IsdBLableBold"))굵게ToolStripMenuItem.Checked = bool.Parse(a["IsdBLableBold"]);}catch{}
                try{if(a.ContainsKey("IsdBLableEdge"))테두리ToolStripMenuItem.Checked = bool.Parse(a["IsdBLableEdge"]);}catch{}
                try{if(a.ContainsKey("IsdBLableApplyColor"))색깔적용ToolStripMenuItem.Checked = bool.Parse(a["IsdBLableApplyColor"]);}catch{}
                try{if(a.ContainsKey("dBLableColor"))lbl_LeftPeak.ForeColor=lbl_LeftPeakValue.ForeColor=lbl_RightPeak.ForeColor=lbl_RightPeakValue.ForeColor = HS_CSharpUtility.Utility.ConvertUtility.StringToColor(a["dBLableColor"]);}catch{}
                try{if(a.ContainsKey("ShowTitle"))타이틀표시ToolStripMenuItem.Checked=bool.Parse(a["ShowTitle"]);}catch{}
                try{if(a.ContainsKey("SoundAlloc"))fMODSystemToolStripMenuItem.Checked=bool.Parse(a["SoundAlloc"]);}catch{}
                try{if(a.ContainsKey("WidthGraph"))그래프가로배열ToolStripMenuItem.Checked=bool.Parse(a["WidthGraph"]);}catch{}
                try{if(a.ContainsKey("WidthSplit"))분할자세로배열ToolStripMenuItem.Checked=bool.Parse(a["WidthSplit"]);}catch{}
                try{if(a.ContainsKey("FormSize"))this.Size =HS_CSharpUtility.Utility.ConvertUtility.StringToSize(a["FormSize"]);}catch{}
                try{if(a.ContainsKey("SplitterDistance"))splitContainer1.SplitterDistance=Convert.ToInt32(a["SplitterDistance"]);}catch{}
                //Color c = progressBarGradient2.BackColor;
                try{if(a.ContainsKey("SplitterColor"))splitContainer1.BackColor = colorDialog1.Color = HS_CSharpUtility.Utility.ConvertUtility.StringToColor(a["SplitterColor"]); }catch{}
                try{if(a.ContainsKey("SplitterSplitterWidth"))splitContainer1.SplitterWidth = Convert.ToInt32(a["SplitterSplitterWidth"]);}catch{}
                //progressBarGradient2.BackColor = c;
                //try{if(a.ContainsKey("RightMeterBandsCount"))timer1.Interval=Convert.ToInt32(a["DrawSpeed"]);}catch{}
                try{if(a.ContainsKey("DrawSpeed"))toolStripTextBox1.Text=a["DrawSpeed"];}catch{}
                //try{if(a.ContainsKey("RightMeterBandsCount"))Ltmp =Rtmp= new float[Convert.ToInt32(a["SoundBuffer"])];}catch{}
                try{if(a.ContainsKey("SoundBuffer"))toolStripTextBox3.Text= a["SoundBuffer"];}catch{}
                try{if(a.ContainsKey("LEDCount"))toolStripMenuItem2.Text = a["LEDCount"];}catch{}
                try{if(a.ContainsKey("AttackTime"))toolStripTextBox5.Text= a["AttackTime"];}catch{}
                try{if(a.ContainsKey("ReleaseTime"))toolStripTextBox4.Text = a["ReleaseTime"];}catch{}
                
                try{if(a.ContainsKey("dBLeveleShow"))dB레벨레이블ToolStripMenuItem.Checked=bool.Parse(a["dBLeveleShow"]);}catch{}
                try{if(a.ContainsKey("dBLevelQuality"))품질우선ToolStripMenuItem.Checked=bool.Parse(a["dBLevelQuality"]);}catch{}
                try{if(a.ContainsKey("dBLevelShowMinus"))음수부호표시ToolStripMenuItem.Checked=bool.Parse(a["dBLevelShowMinus"]);}catch{}
                try{if(a.ContainsKey("dBLevelShowdBLable"))dB문자표시ToolStripMenuItem.Checked=bool.Parse(a["dBLevelShowdBLable"]);}catch{}
                try{if(a.ContainsKey("dBLeveldBLableSpace"))dB문자공백ToolStripMenuItem.Checked=bool.Parse(a["dBLeveldBLableSpace"]);}catch{}
                try{if(a.ContainsKey("dBLevelSpace"))toolStripComboBox1.SelectedIndex=Convert.ToByte(a["dBLevelSpace"])-1;}catch{}

                FontFamily ff = null;
                try
                {
                    if (a.ContainsKey("dBLevelFontFamily"))
                    {
                        byte[] tmpff = Utility.EtcUtility.ConvertStringToByteArray(a["dBLevelFontFamily"], false);
                        
                        System.IO.MemoryStream ms = new System.IO.MemoryStream(tmpff);
                        var dump = new Polenter.Serialization.SharpSerializer();
                        //ff = dump.Deserialize(ms) as FontFamily;
                        ms.Dispose();
                        //ff = Utility.EtcUtility.SerializableOpen(tmpff) as FontFamily;
                    }
                }
                catch{}
                try
                {
                    if (a.ContainsKey("dBLevelFont"))
                    {
                        byte[] b = Utility.EtcUtility.ConvertStringToByteArray(a["dBLevelFont"], false);
                        Font f = Utility.EtcUtility.SerializableOpen(b) as Font;
                        if (f != null && ff != null) fontDialog1.Font = progressBarGradient1.Font = progressBarGradient2.Font = new Font(ff, f.Size, f.Style);
                        if (f != null) fontDialog1.Font=progressBarGradient1.Font=progressBarGradient2.Font = f;
                        else if (ff != null) fontDialog1.Font = progressBarGradient1.Font = progressBarGradient2.Font = new Font(ff, fontDialog1.Font.Size, fontDialog1.Font.Style);
                        else throw new Exception();
                    }
                }catch{}

                //[LeftMeter]
                //try{if(a.ContainsKey("LeftMeterBandsCount"))peakMeterCtrl1.BandsCount=Convert.ToInt32(a["LeftMeterBandsCount"]);}catch{}
                //try{if(a.ContainsKey("LeftMeterGridShow"))peakMeterCtrl1.모눈선표시=bool.Parse(a["LeftMeterGridShow"]);}catch{}
                //try{if(a.ContainsKey("LeftMeterGridColor"))peakMeterCtrl1.모눈선색상=HS_CSharpUtility.Utility.ConvertUtility.StringToColor(a["LeftMeterGridColor"]);}catch{}
                //try{if(a.ContainsKey("LeftMeterColorBackShow"))peakMeterCtrl1.색상표시=bool.Parse(a["LeftMeterColorBackShow"]);}catch{}
                try{if(a.ContainsKey("LeftMeterColorHigh"))progressBarGradient1.MaximumColor = HS_CSharpUtility.Utility.ConvertUtility.StringToColor(a["LeftMeterColorHigh"]);}catch{}
                //try{if(a.ContainsKey("LeftMeterColorHighBack"))peakMeterCtrl1.ColorHighBack=HS_CSharpUtility.Utility.ConvertUtility.StringToColor(a["LeftMeterColorHighBack"]);}catch{}
                try{if(a.ContainsKey("LeftMeterColorMedium"))progressBarGradient1.MidColor=HS_CSharpUtility.Utility.ConvertUtility.StringToColor(a["LeftMeterColorMedium"]);}catch{}
                try{if(a.ContainsKey("LeftMeterBackgroundColor"))progressBarGradient1.BackColor=HS_CSharpUtility.Utility.ConvertUtility.StringToColor(a["LeftMeterBackgroundColor"]);}catch{}
                try{if(a.ContainsKey("LeftMeterColorNormal"))progressBarGradient1.MinimumColor=HS_CSharpUtility.Utility.ConvertUtility.StringToColor(a["LeftMeterColorNormal"]);}catch{}
                try{if(a.ContainsKey("LeftMeterColorShow"))progressBarGradient1.ShowMidColor=bool.Parse(a["LeftMeterColorShow"]);}catch{}
                try{if(a.ContainsKey("LeftMeterColorPosition"))progressBarGradient1.MidColorPosition=float.Parse(a["LeftMeterColorPosition"]);}catch{}
                //try{if(a.ContainsKey("LeftMeterColorMediumBack"))peakMeterCtrl1.ColorMediumBack=HS_CSharpUtility.Utility.ConvertUtility.StringToColor(a["LeftMeterColorMediumBack"]);}catch{}
                //try{if(a.ContainsKey("LeftMeterColorNormalBack"))peakMeterCtrl1.ColorNormalBack=HS_CSharpUtility.Utility.ConvertUtility.StringToColor(a["LeftMeterColorNormalBack"]);}catch{}
                //try{if(a.ContainsKey("LeftMeterPeakShow"))peakMeterCtrl1.Peak표시=bool.Parse(a["LeftMeterPeakShow"]);}catch{}
                //try{if(a.ContainsKey("LeftMeterPeakBolder"))peakMeterCtrl1.Peak굵기=Convert.ToInt32(a["LeftMeterPeakBolder"]);}catch{}
                //try{if(a.ContainsKey("LeftMeterPeakSpeed"))peakMeterCtrl1.Peak속도=Convert.ToInt32(a["LeftMeterPeakSpeed"]);}catch{}
                //try{if(a.ContainsKey("LeftMeterPeakColor"))peakMeterCtrl1.Peak색상=HS_CSharpUtility.Utility.ConvertUtility.StringToColor(a["LeftMeterPeakColor"]);}catch{}
                //[RightMeter]
                //try{if(a.ContainsKey("RightMeterBandsCount"))peakMeterCtrl2.BandsCount=Convert.ToInt32(a["RightMeterBandsCount"]);}catch{}
                //try{if(a.ContainsKey("RightMeterGridShow"))peakMeterCtrl2.모눈선표시=bool.Parse(a["RightMeterGridShow"]);}catch{}
                //try{if(a.ContainsKey("RightMeterGridColor"))peakMeterCtrl2.모눈선색상=HS_CSharpUtility.Utility.ConvertUtility.StringToColor(a["RightMeterGridColor"]);}catch{}
                //try{if(a.ContainsKey("RightMeterColorBackShow"))peakMeterCtrl2.색상표시=bool.Parse(a["RightMeterColorBackShow"]);}catch{}
                try{if(a.ContainsKey("RightMeterColorHigh"))progressBarGradient2.MaximumColor=HS_CSharpUtility.Utility.ConvertUtility.StringToColor(a["RightMeterColorHigh"]);}catch{}
                //try{if(a.ContainsKey("RightMeterColorHighBack"))peakMeterCtrl2.ColorHighBack=HS_CSharpUtility.Utility.ConvertUtility.StringToColor(a["RightMeterColorHighBack"]);}catch{}
                try{if(a.ContainsKey("RightMeterColorMedium"))progressBarGradient2.MidColor=HS_CSharpUtility.Utility.ConvertUtility.StringToColor(a["RightMeterColorMedium"]);}catch{}
                try{if(a.ContainsKey("RightMeterColorNormal"))progressBarGradient2.MinimumColor=HS_CSharpUtility.Utility.ConvertUtility.StringToColor(a["RightMeterColorNormal"]);}catch{}
                try{if(a.ContainsKey("RightMeterBackgroundColor"))progressBarGradient2.BackColor=HS_CSharpUtility.Utility.ConvertUtility.StringToColor(a["RightMeterBackgroundColor"]);}catch{}
                try{if(a.ContainsKey("RightMeterColorShow"))progressBarGradient2.ShowMidColor=bool.Parse(a["RightMeterColorShow"]);}catch{}
                try{if(a.ContainsKey("RightMeterColorPosition"))progressBarGradient2.MidColorPosition=float.Parse(a["RightMeterColorPosition"]);}catch{}
                //try{if(a.ContainsKey("RightMeterColorMediumBack"))peakMeterCtrl2.ColorMediumBack=HS_CSharpUtility.Utility.ConvertUtility.StringToColor(a["RightMeterColorMediumBack"]);}catch{}
                //try{if(a.ContainsKey("RightMeterColorNormalBack"))peakMeterCtrl2.ColorNormalBack=HS_CSharpUtility.Utility.ConvertUtility.StringToColor(a["RightMeterColorNormalBack"]);}catch{}
                //try{if(a.ContainsKey("RightMeterPeakShow"))peakMeterCtrl2.Peak표시=bool.Parse(a["RightMeterPeakShow"]);}catch{}
                //try{if(a.ContainsKey("RightMeterPeakBolder"))peakMeterCtrl2.Peak굵기=Convert.ToInt32(a["RightMeterPeakBolder"]);}catch{}
                //try{if(a.ContainsKey("RightMeterPeakSpeed"))peakMeterCtrl2.Peak속도=Convert.ToInt32(a["RightMeterPeakSpeed"]);}catch{}
                //try{if(a.ContainsKey("RightMeterPeakColor"))peakMeterCtrl2.Peak색상=HS_CSharpUtility.Utility.ConvertUtility.StringToColor(a["RightMeterPeakColor"]);}catch{}

                try{if(a.ContainsKey("IsBackgrountTransparent"))배경투명ToolStripMenuItem_Click(null, null);}catch{}

                try
                {
                    d1 = DecibelsforSound(LDB);
                    d2 = DecibelsforSound(RDB);
                    lbl_RightPeak.BackColor = d1 >= 0 ? Color.Red : White_1;
                    lbl_RightPeak.ForeColor = d1 >= 0 ? White_1 : Color.Black;
                    lbl_LeftPeak.BackColor = d2 >= 0 ? Color.Red : White_1;
                    lbl_LeftPeak.ForeColor = d2 >= 0 ? White_1 : Color.Black;

                    d1 = DecibelsforSound(MaxLdB);
                    d2 = DecibelsforSound(MaxRdB);
                    lbl_RightPeakValue.BackColor = d1 >= 0 ? Color.Red : Color.Yellow;
                    lbl_RightPeakValue.ForeColor = d1 >= 0 ? White_1 : Color.Black;
                    lbl_LeftPeakValue.BackColor = d2 >= 0 ? Color.Red : Color.Yellow;
                    lbl_LeftPeakValue.ForeColor = d2 >= 0 ? White_1 : Color.Black;

                    d1 = d2 = float.NegativeInfinity;
                }catch{}
            }
        }
        Dictionary<string, string> GetSetting()
        {
            Dictionary<string, string> a = new Dictionary<string, string>();
            a.Add("FormSize", HS_CSharpUtility.Utility.ConvertUtility.SizeToString(this.Size));
            a.Add("FormLocation", HS_CSharpUtility.Utility.ConvertUtility.PointToString(this.Location));
            try  { a.Add("FormOpacity", this.Opacity.ToString("0.00")); }catch{}
            a.Add("TopMost", this.TopMost.ToString());
            a.Add("ShowEdge", 테두리표시ToolStripMenuItem.Checked.ToString());
            a.Add("IsBackgroundTransparent", 배경투명ToolStripMenuItem.Checked.ToString());
            a.Add("IsShowdBLable", 레벨레이블보이기ToolStripMenuItem.Checked.ToString());
            a.Add("IsdBLableBold", 굵게ToolStripMenuItem.Checked.ToString());
            a.Add("IsdBLableEdge", 테두리ToolStripMenuItem.Checked.ToString());
            a.Add("IsdBLableApplyColor", 색깔적용ToolStripMenuItem.Checked.ToString());
            a.Add("dBLableColor", HS_CSharpUtility.Utility.ConvertUtility.ColorToString(lbl_LeftPeak.ForeColor, true));
            a.Add("IsSplitterTransparent", 분할자투명ToolStripMenuItem.Checked.ToString());
            a.Add("SplitterDistance", splitContainer1.SplitterDistance.ToString());
            a.Add("SplitterColor", HS_CSharpUtility.Utility.ConvertUtility.ColorToString(splitContainer1.BackColor, true));
            a.Add("SplitterSplitterWidth", splitContainer1.SplitterWidth.ToString());
            a.Add("ShowTitle", 타이틀표시ToolStripMenuItem.Checked.ToString());
            a.Add("SoundAlloc", fMODSystemToolStripMenuItem.Checked.ToString());
            a.Add("WidthGraph", 그래프가로배열ToolStripMenuItem.Checked.ToString());
            a.Add("WidthSplit", 분할자세로배열ToolStripMenuItem.Checked.ToString());

            a.Add("LEDCount", LED눈금갯수.ToString());
            a.Add("DrawSpeed", timer1.Interval.ToString());
            a.Add("SoundBuffer", Ltmp.Length.ToString());
            a.Add("AttackTime", FrameDelay.ToString());
            a.Add("ReleaseTime", SampleDelay.ToString());

            a.Add("\r\n[dBLevel]", null);
            a.Add("dBLeveleShow", dB레벨레이블ToolStripMenuItem.Checked.ToString());
            a.Add("dBLevelQuality", 품질우선ToolStripMenuItem.Checked.ToString());
            a.Add("dBLevelShowMinus", 음수부호표시ToolStripMenuItem.Checked.ToString());
            a.Add("dBLevelShowdBLable", dB문자표시ToolStripMenuItem.Checked.ToString());
            a.Add("dBLeveldBLableSpace", dB문자공백ToolStripMenuItem.Checked.ToString());
            a.Add("dBLevelSpace", (toolStripComboBox1.SelectedIndex + 1).ToString());

            a.Add("\r\n//아래의 값 2개는 절때 건드리지 말것!!", null);
            try
            {
                byte[] tmp = Utility.EtcUtility.SerializableSaveToByteArray(fontDialog1.Font);
                a.Add("\r\n//"+fontDialog1.Font.ToString(), null);
                a.Add("dBLevelFont", Utility.EtcUtility.ConvertByteArrayTostring(tmp));
                /*
                var dump = new Polenter.Serialization.SharpSerializer();
                System.IO.MemoryStream ms = new System.IO.MemoryStream();
                dump.Serialize(fontDialog1.Font.FontFamily, ms);

                System.Drawing.Text.PrivateFontCollection pp = new System.Drawing.Text.PrivateFontCollection();
                
                Font
                //byte[] tmp = Utility.EtcUtility.SerializableSaveToByteArray(fontDialog1.Font.FontFamily);
                tmp = ms.ToArray(); ms.Dispose();
                ms = new System.IO.MemoryStream(tmp);
                FontFamily ff = dump.Deserialize(ms) as FontFamily; ms.Dispose();


                a.Add("dBLevelFontFamily", Utility.EtcUtility.ConvertByteArrayTostring(tmp));
                ms.Dispose();*/
            }catch/*(DeserializingException ex)*/{}

            a.Add("\r\n[LeftMeter]", null);
            //a.Add("LeftMeterBandsCount", BandsCount.ToString());
            //a.Add("LeftMeterGridShow", peakMeterCtrl1.모눈선표시.ToString());
            //a.Add("LeftMeterGridColor", HS_CSharpUtility.Utility.ConvertUtility.ColorToString(peakMeterCtrl1.모눈선색상));
            //a.Add("LeftMeterColorBackShow", peakMeterCtrl1.색상표시.ToString());
            a.Add("LeftMeterBackgroundColor", HS_CSharpUtility.Utility.ConvertUtility.ColorRGBToString(progressBarGradient1.BackColor));
            a.Add("LeftMeterColorHigh", HS_CSharpUtility.Utility.ConvertUtility.ColorToString(progressBarGradient1.MaximumColor));
            //a.Add("LeftMeterColorHighBack", HS_CSharpUtility.Utility.ConvertUtility.ColorToString(peakMeterCtrl1.ColorHighBack));                
            a.Add("LeftMeterColorMedium", HS_CSharpUtility.Utility.ConvertUtility.ColorToString(progressBarGradient1.MidColor));
            a.Add("LeftMeterColorNormal", HS_CSharpUtility.Utility.ConvertUtility.ColorToString(progressBarGradient1.MinimumColor));
            a.Add("LeftMeterColorShow", progressBarGradient1.ShowMidColor.ToString());
            a.Add("LeftMeterColorPosition", progressBarGradient1.MidColorPosition.ToString());
            //a.Add("LeftMeterColorMediumBack", HS_CSharpUtility.Utility.ConvertUtility.ColorToString(peakMeterCtrl1.ColorMediumBack));
            //a.Add("LeftMeterColorNormalBack", HS_CSharpUtility.Utility.ConvertUtility.ColorToString(peakMeterCtrl1.ColorNormalBack));
            //a.Add("LeftMeterPeakShow", peakMeterCtrl1.Peak표시.ToString());
            //a.Add("LeftMeterPeakBolder", peakMeterCtrl1.Peak굵기.ToString());
            //a.Add("LeftMeterPeakSpeed", peakMeterCtrl1.Peak속도.ToString());
            //a.Add("LeftMeterPeakColor", HS_CSharpUtility.Utility.ConvertUtility.ColorToString(peakMeterCtrl1.Peak색상));
            a.Add("LeftMeterColordBLevel", HS_CSharpUtility.Utility.ConvertUtility.ColorToString(progressBarGradient1.ForeColor));

            a.Add("\r\n[RightMeter]", null);
            //a.Add("RightMeterBandsCount", peakMeterCtrl2.BandsCount.ToString());
            //a.Add("RightMeterGridShow", peakMeterCtrl2.모눈선표시.ToString());
            //a.Add("RightMeterGridColor", HS_CSharpUtility.Utility.ConvertUtility.ColorToString(peakMeterCtrl2.모눈선색상));
            //a.Add("RightMeterColorBackShow", peakMeterCtrl2.색상표시.ToString());
            a.Add("RightMeterBackgroundColor", HS_CSharpUtility.Utility.ConvertUtility.ColorRGBToString(progressBarGradient2.BackColor));
            a.Add("RightMeterColorHigh", HS_CSharpUtility.Utility.ConvertUtility.ColorToString(progressBarGradient2.MaximumColor));
            //a.Add("RightMeterColorHighBack", HS_CSharpUtility.Utility.ConvertUtility.ColorToString(peakMeterCtrl2.ColorHighBack));               
            a.Add("RightMeterColorMedium", HS_CSharpUtility.Utility.ConvertUtility.ColorToString(progressBarGradient2.MidColor));
            a.Add("RightMeterColorNormal", HS_CSharpUtility.Utility.ConvertUtility.ColorToString(progressBarGradient2.MinimumColor));
            a.Add("RightMeterColorShow", progressBarGradient2.ShowMidColor.ToString());
            a.Add("RightMeterColorPosition", progressBarGradient2.MidColorPosition.ToString());
            //a.Add("RightMeterColorMediumBack", HS_CSharpUtility.Utility.ConvertUtility.ColorToString(peakMeterCtrl2.ColorMediumBack));
            //a.Add("RightMeterColorNormalBack", HS_CSharpUtility.Utility.ConvertUtility.ColorToString(peakMeterCtrl2.ColorNormalBack));
            //a.Add("RightMeterPeakShow", peakMeterCtrl2.Peak표시.ToString());
            //a.Add("RightMeterPeakBolder", peakMeterCtrl2.Peak굵기.ToString());
            //a.Add("RightMeterPeakSpeed", peakMeterCtrl2.Peak속도.ToString());
            //a.Add("RightMeterPeakColor", HS_CSharpUtility.Utility.ConvertUtility.ColorToString(peakMeterCtrl2.Peak색상));
            a.Add("RightMeterColordBLevel", HS_CSharpUtility.Utility.ConvertUtility.ColorToString(progressBarGradient2.ForeColor));
            return a;
        }

        public void LoadSetting(Dictionary<string, string> Setting)
        {
            this.Setting = Setting;
        }       
        public Dictionary<string, string> SaveSetting()
        {
            return Setting;
        }
 
        string SettingDirectory
        { 
            get{ string a = Application.StartupPath + "\\Settings\\PeakMeter";
            try { if (!System.IO.Directory.Exists(a)) { System.IO.Directory.CreateDirectory(a); } }
            catch { return null; } return a; }
        }
        #endregion

        private void 맨위에표시ToolStripMenuItem_CheckedChanged(object sender, EventArgs e)
        {
            this.TopMost = 맨위에표시ToolStripMenuItem.Checked;
        }

        private void 닫기ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void toolStripTextBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            //if (e.KeyChar == Convert.ToChar(Keys.Enter)) { toolStripMenuItem2_TextChanged(null, null); }
            if (!(Char.IsDigit(e.KeyChar)) && e.KeyChar != Convert.ToChar(Keys.Back))
            { e.Handled = true; }
        }

        bool timer1_Start;
        private void frmPeakMeter_WindowShowChanged(object sender, EventArgs e)
        {
            if (WindowState == FormWindowState.Minimized || this.WindowShowStatus == HS_Audio.Control.WindowShowState.Hide)
            {timer1_Start = timer1.Enabled; timer1.Stop();}
            else  {if (timer1_Start) timer1.Start();}
        }

        Point splitContainer1MouseDownLocation;
        MouseButtons splitContainer1MouseButtons = System.Windows.Forms.MouseButtons.None;
        private void progressBarGradient1_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Left)
            {
                this.Left += e.X - splitContainer1MouseDownLocation.X;
                this.Top += e.Y - splitContainer1MouseDownLocation.Y;
            }
        }

        private void progressBarGradient1_MouseUp(object sender, MouseEventArgs e)
        {
            splitContainer1MouseButtons = System.Windows.Forms.MouseButtons.None;
            this.Cursor = Cursors.Default;
        }

        private void progressBarGradient1_MouseDown(object sender, MouseEventArgs e)
        {
            splitContainer1MouseButtons = System.Windows.Forms.MouseButtons.Left;
            splitContainer1MouseDownLocation = e.Location;
            this.Cursor = Cursors.SizeAll;
        }

        private void progressBarGradient1_MouseLeave(object sender, EventArgs e)
        {
            progressBarGradient1_MouseUp(null,null);
        }

        private void frmPeakMeter1_Load(object sender, EventArgs e)
        {
            splitContainer1.SplitterDistance = splitContainer1.Height / 2;
            contextMenuStrip1.ImageScalingSize = new Size(16, 16);
            progressBarGradient1.MidColorPosition = progressBarGradient2.MidColorPosition = 0.65f;
        }

        private void peak새로고침ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MaxLdB = MaxRdB =float.NegativeInfinity;
        }

        private void 테두리표시ToolStripMenuItem_CheckedChanged(object sender, EventArgs e)
        {
            if (테두리표시ToolStripMenuItem.Checked)
            {splitContainer1.BorderStyle = BorderStyle.FixedSingle; }
            else { splitContainer1.BorderStyle = BorderStyle.None; }
        }

        Color TransparentColor = Color.FromArgb(1,1,1);
        Color[] c = new Color[3];
        private void 배경투명ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!배경투명ToolStripMenuItem.Checked)
            {
                //splitContainer1.BackColor = SystemColors.Control;
                c[0] = progressBarGradient1.BackColor;
                c[1] = progressBarGradient2.BackColor;
                c[2] = splitContainer1.BackColor;
                this.BackColor = TransparentColor;//Color.Transparent;
                //this.TransparencyKey = peakMeterCtrl1.모눈선색상;
                if (분할자투명ToolStripMenuItem.Checked) splitContainer1.BackColor = TransparentColor;
                toolStripTextBox2.Enabled = true;
                this.progressBarGradient1.BackColor = this.progressBarGradient2.BackColor = TransparentColor;
                //toolStripTextBox2_TextChanged(null, null);
                this.TransparencyKey=TransparentColor;
                toolStripTextBox2.Enabled = 분할자투명ToolStripMenuItem.Enabled = true;
                배경투명ToolStripMenuItem.Checked = true;
            }
            else
            {
                this.TransparencyKey = Color.Empty;
                toolStripTextBox2.Enabled = false;
                progressBarGradient1.BackColor = c[0];
                progressBarGradient2.BackColor = c[1];
                splitContainer1.BackColor = c[2];
                toolStripTextBox2.Enabled = 배경투명ToolStripMenuItem.Checked = false;
            }
        }

        private void 분할자색상ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (colorDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                splitContainer1.BackColor = colorDialog1.Color;
        }

        private void 분할자투명ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (분할자투명ToolStripMenuItem.Checked)
            {c[2] = splitContainer1.BackColor; splitContainer1.BackColor = Color.Transparent;}
            else{splitContainer1.BackColor = c[2];}
        }

        private void toolStripMenuItem2_TextChanged(object sender, EventArgs e)
        {
            int a = 50;
            try{if (toolStripMenuItem2.Text != "") a = int.Parse(toolStripMenuItem2.Text);}catch{}
            LED눈금갯수 = Math.Min(a < 1?50:a, 360);//Math.Min(Math.Max(a,50), 1000000);
            Start();
        }

        private void toolStripTextBox3_TextChanged(object sender, EventArgs e)
        {
            int a = 2048;
            try{if (toolStripTextBox3.Text != "") a = int.Parse(toolStripTextBox3.Text);}catch{}
            Ltmp = InitData(a < 1?2048:a, float.NegativeInfinity);
            Rtmp = InitData(a < 1?2048:a, float.NegativeInfinity);
        }

        int a1;
        private void 측정속도ToolStripMenuItem_TextChanged(object sender, EventArgs e)
        {
            try{a1 = int.Parse(toolStripTextBox1.Text);}catch{a1 = 25;toolStripTextBox1.Text = "25";}
            if(a1<1)timer1.Stop();else timer1.Start();
            this.timer1.Interval=a1<1?1:a1;
        }

        private void toolStripTextBox2_TextChanged(object sender, EventArgs e)
        {
            
            if (!toolStripTextBox2.Text.Contains("#")) {toolStripTextBox2.Text="#"+toolStripTextBox2.Text; }
            int a = Convert.ToInt32(toolStripTextBox2.Text.Replace("#", ""), 16);
            this.TransparencyKey=Color.FromArgb(a);
             
            this.BackColor =splitContainer1.Panel1.BackColor = splitContainer1.Panel2.BackColor = this.TransparencyKey;
            //splitContainer1.BackColor = SystemColors.ControlDark;
        }

        private void fMODSystemToolStripMenuItem_Click(object sender, EventArgs e)
        {
            fMODSystemToolStripMenuItem.Checked = true;
            fMODChannelToolStripMenuItem.Checked = false;
            GetData = true;
        }

        private void fMODChannelToolStripMenuItem_Click(object sender, EventArgs e)
        {
            fMODSystemToolStripMenuItem.Checked = false;
            fMODChannelToolStripMenuItem.Checked = true;
            GetData = false;
        }

        private void toolStripTextBox4_KeyPress(object sender, KeyPressEventArgs e)
        {            
        }

        private void toolStripTextBox5_KeyPress(object sender, KeyPressEventArgs e)
        {
        }

        bool Lock = false;
        private void 가로미터ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                Lock = true;
                float X = CaculatorLengthforPercent(SystemInformation.PrimaryMonitorSize.Width, 5);
                float Y = CaculatorLengthforPercent(SystemInformation.PrimaryMonitorSize.Height, 10);

                this.Location = new Point((int)X, (int)Y);
                그래프가로배열ToolStripMenuItem.Checked = 분할자세로배열ToolStripMenuItem.Checked = true;
                this.Size = new Size(this.Height, this.Width);
                splitContainer1.SplitterDistance = splitContainer1.Height / 2;
            }finally{Lock = false;}
            사이즈맞추기ToolStripMenuItem_Click(null, null);
        }

        private void 세로미터ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                Lock = true;
                float X = CaculatorLengthforPercent(SystemInformation.PrimaryMonitorSize.Width, 5);
                float Y = CaculatorLengthforPercent(SystemInformation.PrimaryMonitorSize.Height, 10);

                this.Location = new Point((int)X, (int)Y);
                그래프가로배열ToolStripMenuItem.Checked = 분할자세로배열ToolStripMenuItem.Checked = false;
                this.Size = new Size(this.Height, this.Width);//99, 800
                splitContainer1.SplitterDistance = splitContainer1.Width / 2;
            }finally{Lock = false;}
            사이즈맞추기ToolStripMenuItem_Click(null, null);
        }

        private void 그래프가로배열ToolStripMenuItem_CheckedChanged(object sender, EventArgs e)
        {
            if (그래프가로배열ToolStripMenuItem.Checked)progressBarGradient1.Orientation = progressBarGradient2.Orientation = System.Windows.Forms.Orientation.Horizontal;
            else progressBarGradient1.Orientation = progressBarGradient2.Orientation = System.Windows.Forms.Orientation.Vertical;
            if(!Lock)사이즈맞추기ToolStripMenuItem_Click(null, null);
        }

        private void 분할자세로배열ToolStripMenuItem_CheckedChanged(object sender, EventArgs e)
        {
            if (분할자세로배열ToolStripMenuItem.Checked) splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            else splitContainer1.Orientation = System.Windows.Forms.Orientation.Vertical;
            if(!Lock)사이즈맞추기ToolStripMenuItem_Click(null, null);
        }

        private void progressBarGradient2_MouseClick(object sender, MouseEventArgs e)
        {
            //Start();
        }

        private void 레벨레이블보이기ToolStripMenuItem_CheckedChanged(object sender, EventArgs e)
        {
            //peakMeterCtrl1.Size = peakMeterCtrl1.Size-label1.Height
            /*peakMeterCtrl1.Anchor = peakMeterCtrl2.Anchor = 레벨레이블보이기ToolStripMenuItem.Checked ?
                AnchorStyles.Bottom | AnchorStyles.Right | AnchorStyles.Left :
                AnchorStyles.Bottom | AnchorStyles.Right | AnchorStyles.Left | AnchorStyles.Top;*/
            lbl_LeftPeak.Visible = lbl_RightPeak.Visible =
            lbl_LeftPeakValue.Visible = lbl_RightPeakValue.Visible = 레벨레이블보이기ToolStripMenuItem.Checked;
            lbl_LeftPeak.BringToFront();lbl_RightPeak.BringToFront();lbl_LeftPeakValue.BringToFront();lbl_RightPeakValue.BringToFront();
            if (그래프가로배열ToolStripMenuItem.Checked && 분할자세로배열ToolStripMenuItem.Checked)
            {
                lbl_LeftPeak.RotateAngle = 90;
            }
            사이즈맞추기ToolStripMenuItem_Click(null, null);
        }

        private void 굵게ToolStripMenuItem_CheckedChanged(object sender, EventArgs e)
        {
            lbl_LeftPeak.Font = new System.Drawing.Font(lbl_LeftPeak.Font, 굵게ToolStripMenuItem.Checked ? FontStyle.Bold : FontStyle.Regular);
            lbl_RightPeak.Font = new System.Drawing.Font(lbl_RightPeak.Font, 굵게ToolStripMenuItem.Checked ? FontStyle.Bold : FontStyle.Regular);
            lbl_LeftPeakValue.Font = new System.Drawing.Font(lbl_LeftPeakValue.Font, 굵게ToolStripMenuItem.Checked ? FontStyle.Bold : FontStyle.Regular);
            lbl_RightPeakValue.Font = new System.Drawing.Font(lbl_RightPeakValue.Font, 굵게ToolStripMenuItem.Checked ? FontStyle.Bold : FontStyle.Regular);
        }

        private void 테두리ToolStripMenuItem_CheckedChanged(object sender, EventArgs e)
        {
            lbl_LeftPeak.BorderStyle = lbl_LeftPeakValue.BorderStyle = lbl_RightPeak.BorderStyle = lbl_RightPeakValue.BorderStyle =
                테두리ToolStripMenuItem.Checked ? BorderStyle.FixedSingle : BorderStyle.None;
        }

        private void 글자색변경ToolStripMenuItem_CheckedChanged(object sender, EventArgs e)
        {
            colorDialog2.Color = White_1;
            colorDialog2.CustomColors[0] = White_1.ToArgb();
            colorDialog2.CustomColors[1] = Color.FromArgb(1, 1, 1).ToArgb();
            colorDialog2.CustomColors[2] = Color.Transparent.ToArgb();
            colorDialog2.CustomColors[3] = Color.Gray.ToArgb();
            colorDialog2.CustomColors[4] = SystemColors.Control.ToArgb();
            if (colorDialog2.ShowDialog() == System.Windows.Forms.DialogResult.OK) White_1 = colorDialog2.Color;
        }

        private void 색깔적용ToolStripMenuItem_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void 사이즈맞추기ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            splitContainer1.Size = new Size(this.Width, this.Height);
            if (!그래프가로배열ToolStripMenuItem.Checked && !분할자세로배열ToolStripMenuItem.Checked&&레벨레이블보이기ToolStripMenuItem.Checked)
            {
                if (true)
                {
                    progressBarGradient1.Size = new Size(splitContainer1.Panel1.Width, splitContainer1.Panel1.Height - lbl_LeftPeak.Height - lbl_LeftPeakValue.Height);
                    progressBarGradient2.Size = new Size(splitContainer1.Panel2.Width, splitContainer1.Panel2.Height - lbl_RightPeak.Height - lbl_RightPeakValue.Height);
                    progressBarGradient1.Location = new Point(0, lbl_RightPeak.Height + lbl_RightPeakValue.Height);
                    progressBarGradient2.Location = new Point(0, lbl_RightPeak.Height + lbl_RightPeakValue.Height);
                }
            }
            else
            {
                progressBarGradient1.Size = new Size(splitContainer1.Panel1.Width, splitContainer1.Panel1.Height);
                progressBarGradient2.Size = new Size(splitContainer1.Panel2.Width, splitContainer1.Panel2.Height);
                progressBarGradient1.Location = new Point(0, 0);
                progressBarGradient2.Location = new Point(0, 0);
            }
            splitContainer1.Size = new Size(this.Width, this.Height);
        }

        private void 타이틀표시ToolStripMenuItem_CheckedChanged(object sender, EventArgs e)
        {
            this.FormBorderStyle = 타이틀표시ToolStripMenuItem.Checked?
                System.Windows.Forms.FormBorderStyle.SizableToolWindow:
                System.Windows.Forms.FormBorderStyle.None;
        }

        private void 투명하게ToolStripMenuItem_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void lbl_LeftPeak_Click(object sender, EventArgs e)
        {
            MaxLdB = float.NegativeInfinity;
        }

        private void lbl_RightPeak_Click(object sender, EventArgs e)
        {
            MaxRdB =float.NegativeInfinity;
        }

        private void 저장SToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try {saveFileDialog1.InitialDirectory = SettingDirectory; }catch{}
            DateTime dt = DateTime.Now;
            saveFileDialog1.FileName = string.Format("PeakMeter1_{0}-{1}-{2} {3} {4}시 {5}분 {6}초.pkmtini",
                dt.Year.ToString("0000"), dt.Month.ToString("00"), dt.Day.ToString("00"),
                dt.Hour > 11 ? "오후" : "오전", dt.Hour > 12 ? (dt.Hour - 12).ToString("00") : dt.Hour.ToString("00"), dt.Minute.ToString("00"), dt.Second.ToString("00"));
            saveFileDialog1.ShowDialog();
        }

        private void 열기OToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try{openFileDialog1.InitialDirectory = SettingDirectory;}catch{}
            openFileDialog1.ShowDialog();
        }

        private void saveFileDialog1_FileOk(object sender, CancelEventArgs e)
        {
            try
            {
                Dictionary<string, string> tmp = GetSetting();
                string[] a = HS_CSharpUtility.Utility.EtcUtility.SaveSettingWithoutFilter(tmp);
                System.IO.File.WriteAllLines(saveFileDialog1.FileName, a);
            }
            catch (Exception ex) { MessageBox.Show("설정 저장 실패!!\n\n예외: " + ex.Message, "Peak 미터", MessageBoxButtons.OK, MessageBoxIcon.Error);}
        }

        private void openFileDialog1_FileOk(object sender, CancelEventArgs e)
        {
            try
            {
                string[] a = System.IO.File.ReadAllLines(openFileDialog1.FileName);
                Dictionary<string, string> b = HS_CSharpUtility.Utility.EtcUtility.LoadSetting(a);
                LoadSetting(b);
            }
            catch(Exception ex) { MessageBox.Show("설정 열기 실패!!\n\n예외: "+ex.Message, "Peak 미터", MessageBoxButtons.OK, MessageBoxIcon.Error);}
        }

        private void dB레벨레이블ToolStripMenuItem_CheckedChanged(object sender, EventArgs e)
        {
            progressBarGradient1.VisibledB=progressBarGradient2.VisibledB = dB레벨레이블ToolStripMenuItem.Checked;
        }

        System.Drawing.Text.PrivateFontCollection font;
        private void 글꼴설정ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            fontDialog1.Font = progressBarGradient1.Font;
            if (fontDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                progressBarGradient1.Font = progressBarGradient2.Font = fontDialog1.Font;
                /*
                string FontDirectory = Environment.GetFolderPath(Environment.SpecialFolder.System);
                string[] FontFiles = Directory.GetFiles(FontDirectory, ".ttf");
                for (int i = 0; i < FontFiles.Length; i++)
                {
                    TagLib.NonContainer.
                }*/
            }
            //dB굵게ToolStripMenuItem.Checked = progressBarGradient1.Font.Bold;
        }

        private void dB글자색변경ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //colorDialog3.Color = progressBarGradient1.ForeColor;
            if(colorDialog3.ShowDialog() == System.Windows.Forms.DialogResult.OK){progressBarGradient1.ForeColor=progressBarGradient2.ForeColor = colorDialog3.Color;}
        }

        private void dB굵게ToolStripMenuItem_CheckedChanged(object sender, EventArgs e)
        {
            if (dB굵게ToolStripMenuItem.Checked)
            {
                Font ft = new System.Drawing.Font(progressBarGradient1.Font, FontStyle.Bold);
                progressBarGradient1.Font = progressBarGradient2.Font = ft;
            }
            else
            {
                Font ft = new System.Drawing.Font(progressBarGradient1.Font, FontStyle.Regular);
                progressBarGradient1.Font = progressBarGradient2.Font = ft;
            }
        }

        private void 음수부호표시ToolStripMenuItem_CheckedChanged(object sender, EventArgs e)
        {
            progressBarGradient1.ShowMinus = progressBarGradient2.ShowMinus=음수부호표시ToolStripMenuItem.Checked;
        }

        private void dB문자표시ToolStripMenuItem_CheckedChanged(object sender, EventArgs e)
        {
            progressBarGradient1.ShowdB = progressBarGradient2.ShowdB= dB문자표시ToolStripMenuItem.Checked;
        }

        private void dB문자공백ToolStripMenuItem_CheckedChanged(object sender, EventArgs e)
        {
            progressBarGradient1.SpacedB = progressBarGradient2.SpacedB=dB문자공백ToolStripMenuItem.Checked;
        }

        private void toolStripComboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            progressBarGradient1.IntervaldB = progressBarGradient2.IntervaldB=toolStripComboBox1.SelectedIndex+1;
        }

        private void 품질우선ToolStripMenuItem_CheckedChanged(object sender, EventArgs e)
        {
            progressBarGradient1.FlickerReduce = progressBarGradient2.FlickerReduce=품질우선ToolStripMenuItem.Checked;
        }

        private void 그래프부드럽게ToolStripMenuItem_CheckedChanged(object sender, EventArgs e)
        {
            Smooth = 그래프부드럽게ToolStripMenuItem.Checked;
            if(ProgressAnimate!=null)ProgressAnimate.Abort();
            if (Smooth)
            {
                ProgressAnimate = new Thread(ProgressAnimate_Start);
                ProgressAnimate.Start();
            }
        }

        private void toolStripTextBox1_TextChanged(object sender, EventArgs e)
        {
            int a1;
            try{a1 = int.Parse(toolStripTextBox1.Text);}catch{a1 = 25;toolStripTextBox1.Text = "25";}
            if(a1<1)timer1.Stop();else timer1.Start();
            this.timer1.Interval=a1<1?1:a1;
        }

        private void toolStripTextBox4_TextChanged(object sender, EventArgs e)
        {
            int a = _sampleDelay;
            try{if (toolStripTextBox4.Text != "") a = int.Parse(toolStripTextBox4.Text);}catch{}
            SampleDelay = a < 1 ? _sampleDelay : a;
            Start();
        }

        private void toolStripTextBox5_TextChanged(object sender, EventArgs e)
        {
            int a = 10;
            try{if (toolStripTextBox5.Text != "") a = int.Parse(toolStripTextBox5.Text);}catch{}
            FrameDelay = a < 1?10:a;
            Start();
        }
    }
}
