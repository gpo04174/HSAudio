﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using Ernzo.WinForms.Controls;

namespace HS_Audio.Forms
{
    public partial class frmFFTGraph : Form, IHSSetting
    {
        PropertyGrid pg;
        string SettingDirectory
        { 
            get{ string a = Application.StartupPath + "\\Settings\\FFTGraph";
            try { if (!System.IO.Directory.Exists(a)) { System.IO.Directory.CreateDirectory(a); } }
            catch { return null; } return a; }
        }

        public frmFFTGraph(HSAudioHelper Value)
        {
            InitializeComponent();
            SetStyle(ControlStyles.SupportsTransparentBackColor, true);
            pg = new PropertyGrid()
            {
                SelectedObject = this,
                Size = new Size(290, 350),
                //Dock= DockStyle.Fill,
                Location = new Point(0, 0),
                Anchor = AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right | AnchorStyles.Top
            };
            f.FormClosing+=new FormClosingEventHandler(f_FormClosing);
            Wavedata=Ltmp=Rtmp = new float[this.Width];
            Data = Value;
            Shape = DrawShape.Wave;
            toolStripComboBox1.Text = "TRIANGLE";
            this.openFileDialog1.InitialDirectory = saveFileDialog1.InitialDirectory = SettingDirectory;
        }
        public frmFFTGraph()
        {
            InitializeComponent();
            SetStyle(ControlStyles.SupportsTransparentBackColor, true);
            toolStripComboBox1.Text = "TRIANGLE";
            this.openFileDialog1.InitialDirectory = saveFileDialog1.InitialDirectory = SettingDirectory;
        }
        
        /*
        Graphics g;
        Rectangle ScrRec;
        SolidBrush Backsb = new SolidBrush(Color.YellowGreen);
        
        protected override void OnPaint(PaintEventArgs e)
        {
            Backsb.Color = BackgroundColor;
            e.Graphics.FillRectangle(Backsb, 0, 0, this.Width, this.Height);
            g = e.Graphics;
            ScrRec = e.ClipRectangle;
            if (Data.system != null) { drawSpectrum(e.Graphics); }
            //base.OnPaint(e);
        }
        */

        float[] Spectrum = new float[1024];
        float[] Wavedata = new float[512];

        #region 설정값
        [Obsolete]
        public void LoadSetting(Dictionary<string, string> a){Setting = new Dictionary<string,string>(a);}
        public Dictionary<string, string> SaveSetting(){return Setting;}

        internal Dictionary<string, string> Setting
        {
            get
            {
                Dictionary<string, string> a = new Dictionary<string, string>();
                a.Add("TopMost", 맨위에표시ToolStripMenuItem.Checked.ToString());
                a.Add("IsBackgroundTransparent", 배경투명처리ToolStripMenuItem.Checked.ToString());
                a.Add("IsSplitterTransparent", 분할자투명ToolStripMenuItem.Checked.ToString());
                a.Add("ShowEdge", 테두리표시ToolStripMenuItem.Checked.ToString());
                a.Add("DrawSpectrum", fFTToolStripMenuItem.Checked.ToString());
                a.Add("GraphShape", 그래프ToolStripMenuItem.Checked.ToString());
                a.Add("SoundAlloc", fMODSystemToolStripMenuItem.Checked.ToString());
                a.Add("FFTWindow", toolStripComboBox1.SelectedIndex.ToString());
                a.Add("FFTSize", toolStripComboBox2.SelectedIndex.ToString());
                a.Add("GraphBolder", toolStripTextBox1.Text);
                a.Add("GraphSameDive", 똑같이나누기ToolStripMenuItem.Checked.ToString());
                a.Add("GraphError", toolStripTextBox2.Text);
                a.Add("DrawColor", string.Format("{0},{1},{2},{3}", DrawColor.A, DrawColor.R, DrawColor.G, DrawColor.B));
                a.Add("BackgroundColor", string.Format("{0},{1},{2},{3}",BackgroundColor.A, BackgroundColor.R, BackgroundColor.G, BackgroundColor.B));
                a.Add("DrawLineColor", string.Format("{0},{1},{2},{3}", DrawLineColor.A, DrawLineColor.R, DrawLineColor.G, DrawLineColor.B));
                a.Add("DrawLine", DrawLine.ToString());
                a.Add("FormSize", string.Format("{0},{1}", this.Width, this.Height));
                a.Add("FormLocation", string.Format("{0},{1}", this.Location.X, this.Location.Y));
                a.Add("LinearSpace", toolStripTextBox4.Text.ToString());
                return a;
            }
            set
            {
                try {맨위에표시ToolStripMenuItem.Checked=this.TopMost = bool.Parse(value["TopMost"]); }catch{}
                try {배경투명처리ToolStripMenuItem.Checked=bool.Parse(value["IsBackgroundTransparent"]); }catch { }
                try {분할자투명ToolStripMenuItem.Checked=bool.Parse(value["IsSplitterTransparent"]); }catch { }
                try {테두리표시ToolStripMenuItem.Checked=bool.Parse(value["ShowEdge"]); }catch { }
                try {this.FFTWindow=(HS_Audio_Lib.DSP_FFT_WINDOW)int.Parse(value["FFTWindow"]); }catch { }
                try {toolStripComboBox2.SelectedIndex=int.Parse(value["FFTSize"]); }catch { }
                try {toolStripTextBox1.Text= int.Parse(value["GraphBolder"]).ToString();}catch { }
                try {똑같이나누기ToolStripMenuItem.Checked=bool.Parse(value["GraphSameDive"]); }catch { }
                try {toolStripTextBox2.Text = int.Parse(value["GraphError"]).ToString(); }catch { }
                try{toolStripTextBox4.Text = float.Parse(value["LinearSpace"]).ToString();}catch { }

                try
                {
                    if (bool.Parse(value["DrawSpectrum"]))
                    {
                        fFTToolStripMenuItem.Checked = true;
                        웨이브데이터ToolStripMenuItem.Checked = false;
                        ToDraw = DrawSpectrum.FFTSpectrum;
                    }
                    else
                    {
                        fFTToolStripMenuItem.Checked = false;
                        웨이브데이터ToolStripMenuItem.Checked = true;
                        ToDraw = DrawSpectrum.WaveData;
                    }
                }
                catch { }
                try
                {
                    if (bool.Parse(value["SoundAlloc"]))
                    {
                        fMODSystemToolStripMenuItem.Checked = true;
                        fMODChannelToolStripMenuItem.Checked = false;
                        GetData = true;
                    }
                    else
                    {
                        fMODSystemToolStripMenuItem.Checked = false;
                        fMODChannelToolStripMenuItem.Checked = true;
                        GetData = false;
                    }
                } catch { }
                try
                {
                    if (bool.Parse(value["GraphShape"]))
                    {
                        그래프ToolStripMenuItem.Checked = true;
                        파동ToolStripMenuItem.Checked = false;
                        Shape = DrawShape.Equalizer;
                    }
                    else
                    {
                        그래프ToolStripMenuItem.Checked = false;
                        파동ToolStripMenuItem.Checked = true;
                        Shape = DrawShape.Wave;
                    }
                }catch { }
                try {DrawLine=bool.Parse(value["DrawLine"]); }catch { }
                try 
                {
                    string[] a =HS_CSharpUtility.Utility.StringUtility.ConvertStringToArray(value["DrawColor"], ",");
                    if (a.Length == 4) { DrawColor =Color.FromArgb(Convert.ToInt32(a[0]), Convert.ToInt32(a[1]),Convert.ToInt32(a[2]),Convert.ToInt32(a[3])); }
                    else if (a.Length == 3) { DrawColor = Color.FromArgb(Convert.ToInt32(a[0]), Convert.ToInt32(a[1]), Convert.ToInt32(a[2])); } }catch { }
                try
                {
                    string[] a = HS_CSharpUtility.Utility.StringUtility.ConvertStringToArray(value["BackgroundColor"], ",");
                    if (a.Length == 4) { BackgroundColor = Color.FromArgb(Convert.ToInt32(a[0]), Convert.ToInt32(a[1]), Convert.ToInt32(a[2]), Convert.ToInt32(a[3])); }
                    else if (a.Length == 3) { BackgroundColor = Color.FromArgb(Convert.ToInt32(a[0]), Convert.ToInt32(a[1]), Convert.ToInt32(a[2])); }}catch { }
                try
                {
                    string[] a = HS_CSharpUtility.Utility.StringUtility.ConvertStringToArray(value["DrawLineColor"], ",");
                    if (a.Length == 4) { DrawLineColor = Color.FromArgb(Convert.ToInt32(a[0]), Convert.ToInt32(a[1]), Convert.ToInt32(a[2]), Convert.ToInt32(a[3])); }
                    else if (a.Length == 3) { DrawLineColor = Color.FromArgb(Convert.ToInt32(a[0]), Convert.ToInt32(a[1]), Convert.ToInt32(a[2])); }} catch { }
                try
                {
                    string[] a = HS_CSharpUtility.Utility.StringUtility.ConvertStringToArray(value["FormSize"], ",");
                    this.Size = new Size(Convert.ToInt32(a[0]), Convert.ToInt32(a[1])); }catch { }
                try
                {
                    string[] a = HS_CSharpUtility.Utility.StringUtility.ConvertStringToArray(value["FormLocation"], ",");
                    this.Location = new Point(Convert.ToInt32(a[0]), Convert.ToInt32(a[1])); }catch { }
            }
        }
        #endregion

        #region 프로퍼티 메서드
        /// <summary>
        /// 스펙트럼을 그릴 방식 입니다.
        /// </summary>
        public enum DrawSpectrum 
        {
            /// <summary>
            /// FFT스펙트럼으로 그립니다.
            /// </summary>
            FFTSpectrum, 
            /// <summary>
            /// 웨이브 데이터를 가지고 그립니다.
            /// </summary>
            WaveData,
        }
        /// <summary>
        /// 스펙트럼을 그릴 방법 입니다.
        /// </summary>
        public enum DrawShape
        {
            /// <summary>
            /// 이퀼라이저 식으로 그립니다.
            /// </summary>
            Equalizer,
            /// <summary>
            /// 파동형식으로 그립니다.
            /// </summary>
            Wave,
        }
        /// <summary>
        /// 스펙트럼(FFT)의 크기를 지정합니다.
        /// </summary>
        public enum FFTSize {None=0,Size_64=64,Size_128=128,Size_256=256,Size_512=512,Size_1024=1024,Size_2048=2048,Size_4096=4096 }
        /// <summary>
        /// 그리기를 시작합니다.
        /// </summary>
        public void StartDraw() { timer1.Start(); }
        /// <summary>
        /// 그리기를 중단합니다.
        /// </summary>
        public void StopDraw() { timer1.Stop(); }
        #endregion

        #region 프로퍼티
        float _FFTAverage = 2;
        /// <summary>
        /// FFT 그래프의 평균율을 가져오거나 설정합니다. (이 값이 크면 FFT의 그래프가 크게 나타납니다.)
        /// </summary>
        [Category("스펙트럼"), Description("FFT 그래프의 오차율 입니다."), DefaultValue(2)]
        public float FFTAverage { get { return _FFTAverage; } set { _FFTAverage = value; } }

        float _LinearSpace = 0;
        /// <summary>
        /// FFT 그래프의 평균율을 가져오거나 설정합니다. (이 값이 크면 FFT의 그래프가 크게 나타납니다.)
        /// </summary>
        [Category("스펙트럼"), Description("막대그래프 일때의 그래프간 간격 입니다."), DefaultValue(0)]
        public float LinearSpace { get { return _LinearSpace; } set { _LinearSpace = value; } }

        /// <summary>
        /// 화면에 FPS라벨을 그릴여부를 가져오거나 설정합니다.
        /// </summary>
        [Category("스펙트럼"), Description("화면에 FPS라벨을 그릴여부 입니다."), DefaultValue(false)]
        public bool FPSLabel { get { return label1.Visible; } set { label1.Visible = value; } }

        HSAudioHelper _Data;
        /// <summary>
        /// 스펙트럼을 그릴 데이터를 가져오거나 설정합니다.
        /// </summary>
        public HSAudioHelper Data { get { return _Data; }
            set { if (value == null)throw new NullReferenceException("Data는 NULL일수 없습니다.");
            _Data = value;value.Disposing+=Helper_Disposing;}
        }

        DrawSpectrum _ToDraw = DrawSpectrum.FFTSpectrum;
        //DrawSpectrum _ToDraw = DrawSpectrum.WAVE;
        /// <summary>
        /// 스펙트럼을 그릴 방법을 가져오거나 설정합니다.
        /// </summary>
        [Category("스펙트럼"), Description("스펙트럼을 그릴 방법 입니다."), DefaultValue(DrawSpectrum.FFTSpectrum)]
        public DrawSpectrum ToDraw { get{return _ToDraw;} set{_ToDraw = value; panel1사이즈조정(); panel2사이즈조정();} }

        /// <summary>
        /// 스펙트럼을 그릴 시간(밀리초) 를 가져오거나 설정합니다..
        /// </summary>
        [Category("스펙트럼"), Description("스펙트럼을 그릴 시간(밀리초) 입니다."), DefaultValue(100)]
        public int DrawSpeed
        {
            get { return timer1.Interval; }
            set { timer1.Interval = value; }
        }
        FFTSize _FFTSize = FFTSize.Size_2048;
        /// <summary>
        /// 스팩트럼 FFT의 배열사이즈를 가져오거나 설정합니다.(음수는 올수 없습니다.)
        /// </summary>
        [Category("스펙트럼"), Description("FFT스팩트럼의 사이즈 입니다."), DefaultValue(typeof(FFTSize), "Size_2048")]
        public FFTSize FFTsize
        {
            get{return _FFTSize; }
            set
            {
                _FFTSize = value; Spectrum = new float[(int)_FFTSize];
                /*
                if (!AutoSizing)
                {
                    this.pictureBox1.Size = new Size(splitContainer1.Width, pictureBox1.Height);
                    this.pictureBox2.Size = new Size(splitContainer1.Width, pictureBox2.Height);
                }
                else
                {
                    this.pictureBox1.Size = new Size((int)value, pictureBox1.Height);
                    this.pictureBox2.Size = new Size((int)value, pictureBox2.Height);
                }*/
                panel1사이즈조정(); panel2사이즈조정();
            }
        }

        /// <summary>
        ///  웨이브 데이터의 배열사이즈를 가져오거나 설정합니다.(음수가 아니어야 하며 16385보다 작아야 합니다.)
        /// </summary>
        [Category("스펙트럼"), Description("웨이브 데이터의 배열 사이즈 입니다."), DefaultValue(256)]
        public int WavedataSize { get { return Wavedata.Length; } set { if (value > 16384||value<1) { throw new Exception("0보다 커야 하며 16385보다 작아야 합니다."); }
            Wavedata =LeftData=RightData= new float[value]; VRSize = new Size(value, pictureBox1.Size.Height); /*peak.Ltmp = peak.Rtmp = new float[value]; peak.Length = value;*/ } }

        /// <summary>
        /// 배경색을 가져오거나 설정합니다.
        /// </summary>
        [Category("스펙트럼"), Description("배경색 입니다."), DefaultValue(typeof(Color),"White")]
        public Color BackgroundColor { get { return this.splitContainer1.Panel1.BackColor; } set { this.splitContainer1.Panel1.BackColor = this.splitContainer1.Panel2.BackColor = value; } }

        SolidBrush brush = new SolidBrush(Color.Navy);
        Pen pen = new Pen(Color.Navy) {Width=1 };
        /// <summary>
        /// 그래프를 그릴색을 가져오거나 설정합니다.
        /// </summary>
        [Category("스펙트럼"), Description("그래프를 그릴색 입니다."), DefaultValue(typeof(Color), "Navy")]
        public Color DrawColor { get { return brush.Color; } set { brush.Color=pen.Color = value; } }

        SolidBrush brush1 = new SolidBrush(Color.Navy);
        Pen pen1 = new Pen(Color.Navy) { Width = 1 };
        /// <summary>
        /// 중앙선을 그릴색을 가져오거나 설정합니다.
        /// </summary>
        [Category("스펙트럼"), Description("중앙선을 그릴색 입니다."), DefaultValue(typeof(Color), "Black")]
        public Color DrawLineColor { get { return pen1.Color; } set { brush1.Color = pen1.Color = value; } }

        /// <summary>
        /// 분할자의 색깔을 가져오거나 설정합니다.
        /// </summary>
        [Category("스펙트럼"), Description("분할자의 색깔 입니다."), DefaultValue(typeof(Color), "ControlDark")]
        public Color SplitColor { get { return splitContainer1.BackColor; } set { splitContainer1.BackColor = value; } }

        float _LineThick = 1.0f;
        /// <summary>
        /// 그래프의 굵기를 가져오거나 설정합니다.
        /// </summary>
        [Category("스펙트럼"), Description("그래프의 굵기 입니다."), DefaultValue(1.0f)]
        public float LineThick
        {
            get {return _LineThick; }
            set
            {
                pen.Width = _LineThick = value;
                /*
                if (ToDraw == DrawSpectrum.FFTSpectrum)
                {
                    pictureBox1.Size = new Size((int)((int)FFTsize * ((!LineTickLinkFFT && Shape == DrawShape.Equalizer) ? LineThick : 1)), pictureBox1.Height);
                    pictureBox2.Size = new Size((int)((int)FFTsize * ((!LineTickLinkFFT && Shape == DrawShape.Equalizer) ? LineThick : 1)), pictureBox2.Height);
                }*/
                panel1사이즈조정(); panel2사이즈조정();
            }
        }
        
        bool _LineTickLinkFFT = true;
        /// <summary>
        /// 그래프의 굵기에따라 FFT의 Hz도 똑같이 나눌지의 여부를 가져오거나 설정합니다.
        /// </summary>
        [Category("스펙트럼"), Description("그래프의 굵기에따라 FFT의 Hz도 똑같이 나눌지 결정합니다."), DefaultValue(true)]
        public bool LineTickLinkFFT { get { return _LineTickLinkFFT; } set { _LineTickLinkFFT = value; } }

        bool _AutoWaveSize = true;
        /// <summary>
        /// 웨이브 데이터 크기를 자동으로 조정할지 여부를 가져오거나 설정합니다.(True면 자동조정 False면 사용자 조정)
        /// </summary>
        [Category("스펙트럼"), Description("웨이브 데이터의 크기를 자동으로 조정할지 여부를 설정합니다.(True면 자동조정 False면 사용자 조정)"), DefaultValue(true)]
        public bool AutoWaveSize { get { return _AutoWaveSize; } set { _AutoWaveSize = value; panel1사이즈조정();panel2사이즈조정();} }

        /// <summary>
        /// 그려지는 영역이 보이는 화면보다 크면 스크롤을 표시할지 여부를 가져오거나 설정합니다.
        /// </summary>
        [Category("스펙트럼"), Description("그려지는 영역이 보이는 화면보다 크면 스크롤을 표시할지 여부를 설정합니다."), DefaultValue(false)]
        public bool AutoSizing
        {
            get{return splitContainer1.Panel1.AutoScroll/*&&(splitContainer1.Panel1.AutoScroll == splitContainer1.Panel2.AutoScroll)*/;}
            set
            {
                splitContainer1.Panel1.AutoScroll = splitContainer1.Panel2.AutoScroll = value;
                /*
                if (value)
                {

                    this.pictureBox1.Size = new Size((int)FFTsize, pictureBox1.Height);
                    this.pictureBox2.Size = new Size((int)FFTsize, pictureBox2.Height);
                }
                else
                {
                    this.pictureBox1.Size = new Size(splitContainer1.Width, pictureBox1.Height);
                    this.pictureBox2.Size = new Size(splitContainer1.Width - 5, pictureBox2.Height);
                }*/
                panel1사이즈조정(); panel2사이즈조정();
            }
        }

        bool _GetData=true;
        /// <summary>
        /// 그래프를 그릴 데이터를 가져오는 방법을 가져오거나 설정합니다. (True면 FMOD.System에서 가져오고 False면 FMOD.Channel에서 가져옵니다.)
        /// </summary>
        [Category("스펙트럼"), Description("그래프를 그릴 데이터를 가져오는 방법 입니다. (True면 Main Output에서 가져오고 False면 Channel에서 가져옵니다.)"), DefaultValue(true)]
        public bool GetData { get { return _GetData; } set { if (Data.channel == null && !value) { throw new NullReferenceException("Channel이 NULL이므로 변경하지 못했습니다."); } _GetData = value; } }

        /// <summary>
        /// 그래프의 형태을 가져오거나 설정합니다.
        /// </summary>
        [Category("스펙트럼"), Description("그래프의 형태 입니다."), DefaultValue(typeof(DrawShape), "Wave")]
        public DrawShape Shape { get;set; }

        bool _Flip = true;
        /// <summary>
        /// 웨이브 데이터 그래프를 이퀼라이저 식으로 그릴때 대칭할 건지를 가져오거나 설정합니다.
        /// </summary>
        [Category("스펙트럼"), Description("웨이브 데이터 그래프를 이퀼라이저 식으로 그릴때 대칭할건지 설정합니다."), DefaultValue(true)]
        public bool Flip { get { return _Flip; } set { _Flip = value; } }

        bool _DrawLine = true;
        /// <summary>
        /// 웨이브데이터 그래프의 중앙에 선을 그릴여부를 가져오거나 설정합니다.(웨이브데이터 그래프이고 Flip 이나 DrawShape.Wave 로 되있을때만 그려집니다.)
        /// </summary>
        [Category("스펙트럼"), Description("웨이브데이터 그래프의 중앙에 선을 그릴지 결정합니다.(웨이브데이터 그래프이고 Flip 이나 DrawShape.Wave 로 되있을때만 그려집니다.)"), DefaultValue(true)]
        public bool DrawLine { get { return _DrawLine; } set { _DrawLine = value; } }

        HS_Audio_Lib.DSP_FFT_WINDOW _FFTWindow = HS_Audio_Lib.DSP_FFT_WINDOW.TRIANGLE;
        [Category("스펙트럼"), Description("FFT윈도우를 설정합니다.")]
        public HS_Audio_Lib.DSP_FFT_WINDOW FFTWindow { get { return _FFTWindow; } set { _FFTWindow = value; } } 
        #endregion
            
        #region Draw 메서드

        int numchannels = 0;
        int dummy = 0;
        HS_Audio_Lib.SOUND_FORMAT dummyformat = HS_Audio_Lib.SOUND_FORMAT.NONE;
        HS_Audio_Lib.DSP_RESAMPLER _dummyresampler = HS_Audio_Lib.DSP_RESAMPLER.LINEAR;
        int count = 0,count2 = 0;
        float count3=0,_dB;
        float max=0;
        List<PointF> p = new List<PointF>();
        List<RectangleF> rec = new List<RectangleF>();

        private float drawSpectrum(Graphics g, Size size, int Channel, bool Log = false, float Peak = -50)
        {
            int index,index1 = 0; 
            float min = 0;//,//1f;
            //max = 0;
            #region True
            if (Shape == DrawShape.Equalizer)
            {
                if (Data!=null&&Data.system != null)
                {
                    Data.system.getSoftwareFormat(ref dummy, ref dummyformat, ref numchannels, ref dummy, ref _dummyresampler, ref dummy);

                    /*
                            DRAW SPECTRUM
                    */
                    //for (count = 0; count < numchannels; count++)
                    //{
                    if (GetData) Data.system.getSpectrum(Spectrum, Spectrum.Length, Channel, FFTWindow);
                    else Data.channel.getSpectrum(Spectrum, Spectrum.Length, Channel, FFTWindow);
                        //float max=0;
                    if (!Log)
                    for (count2 = 0; count2 < (Spectrum.Length - 1); count2 = LineTickLinkFFT ? count2 + (int)LineThick : count2 + 1)
                    {
                        if (Spectrum[count2] > max){max = Spectrum[count2]; index = count2;}
                        else if (Spectrum[count2] < min){ min = Spectrum[count2]; index1 = count2;}
                        /*
                    else{
                        break;
                        float dec = (float)Decibels(Spectrum[count2],0);
                        if (dec>max) { max = dec; index = count2; }
                        else if (dec<min) { min = dec; index1 = count2; }}*/
                        /*  
                        if (max < Spectrum[count2]) { max = Spectrum[count2]; index = count2; }
                        else if (min > Spectrum[count2]) { min = Spectrum[count2]; index1 = count2; }
                         */
                    }

                        /*
                            The upper band of frequencies at 44khz is pretty boring (ie 11-22khz), so we are only
                            going to display the first 256 frequencies, or (0-11khz) 
                        */

                    for (count2 = 2; count2 < (Spectrum.Length - 1); count2 = LineTickLinkFFT ? count2 + (int)LineThick : count2+1)
                        {
                            //count2 = count2 + LineThick;
                            float height = 0;
                            if (Log) height = (float)(1 - (Math.Max(Decibels(Spectrum[count2] * FFTAverage, 0), Peak)) / Peak) * size.Height;
                            else
                            {
                                height = Spectrum[count2] * FFTAverage / max * size.Height;
                                if (height >= size.Height) height = size.Height - 1;
                                if (height < 0) height = 0;
                            }
                            height = size.Height - height;
                            g.FillRectangle(brush, count3==0?count3:count3+LinearSpace, height, (float)LineThick, size.Height - height);
                            count3 = (count3 == 0 ? 0 : LinearSpace) + count3 + LineThick;
                        }
                        count3 = 0;
                    //}
                }
            }
            #endregion
            #region False
            else
            {
                if (Data!=null&&Data.channel != null)
                {
                    Data.system.getSoftwareFormat(ref dummy, ref dummyformat, ref numchannels, ref dummy, ref _dummyresampler, ref dummy);
                    /*
                            DRAW WAVE
                    */
                    //for (count = 0; count < numchannels; count++)
                   // {
                    if (GetData) Data.system.getSpectrum(Spectrum, Spectrum.Length, Channel, FFTWindow);
                    else Data.channel.getSpectrum(Spectrum, Spectrum.Length, Channel, FFTWindow);

                    if (!Log)
                         for (count2 = 0; count2 < Spectrum.Length - 1; count2++)
                        {
                            if (max < Spectrum[count2]) {max = Spectrum[count2];index = count2;}
                            else if (min > Spectrum[count2]) { min = Spectrum[count2]; index1 = count2; }
                        }
                        /*
                            The upper band of frequencies at 44khz is pretty boring (ie 11-22khz), so we are only
                            going to display the first 256 frequencies, or (0-11khz) 
                        */
                        for (count2 = 0; count2 < Spectrum.Length - 1; count2++)
                        { 
                            float height = 0;;
                            if (Log) height = (float)(1 - (Math.Max(Decibels(Spectrum[count2] * FFTAverage, 0), Peak)) / Peak) * size.Height;
                            else
                            {
                                height = Spectrum[count2] * FFTAverage / max * size.Height;
                                //height = (Spectrum[count2] / max) * size.Height;
                                //height = (Spectrum[count2]/max)/max * size.Height;
                                if (height >= size.Height) height = size.Height - 1;
                                if (height < 0) height = 0;
                            }
                            height = size.Height - height;
                            p.Add(new PointF(count2, height));
                        }

                        try { g.DrawLines(pen, p.ToArray()); }
                        catch { } p.Clear();
                   // }
                }
            }
            #endregion
            return Spectrum.Length > 0 ? Spectrum[Spectrum.Length / 2] : float.NegativeInfinity;
        }

        private void drawOscilliscope(Graphics g, Size size, int channel,ref float MaxdB)
        {
            /*
            int numchannels = 0;
            int dummy = 0;
            FMOD.SOUND_FORMAT dummyformat = FMOD.SOUND_FORMAT.NONE;
            FMOD.DSP_RESAMPLER dummyresampler = FMOD.DSP_RESAMPLER.LINEAR;
            int count = 0;
            int count2 = 0;*/
            //List<PointF> p = new List<PointF>();
            if (Data.system != null)
            { Data.system.getSoftwareFormat(ref dummy, ref dummyformat, ref numchannels, ref dummy, ref _dummyresampler, ref dummy); }
            #region True
            if (Shape == DrawShape.Equalizer)
            {
                //if (GetData)
                {
                    if (Data.channel != null)
                    {
                        /*
                                DRAW WAVEDATA
                        */
                        
                        //for (count = 0; count < numchannels; count++)
                        //{
                        if (GetData) Data.system.getWaveData(Wavedata, Wavedata.Length, channel);
                        else Data.channel.getWaveData(Wavedata, Wavedata.Length, channel);
                            float max = 0;

                            for (count2 = 0; count2 < Wavedata.Length; count2++)
                            {
                                if (max < Wavedata[count2])
                                {
                                    max = Wavedata[count2];
                                }
                            }

                            /*
                                The upper band of frequencies at 44khz is pretty boring (ie 11-22khz), so we are only
                                going to display the first 256 frequencies, or (0-11khz) 
                            */
                            for (count2 = 0; count2 < Wavedata.Length; count2++)
                            {
                                float height;

                                height = Wavedata[count2] / max * size.Height;

                                if (height >= size.Height)
                                {
                                    height = size.Height - 1;
                                }

                                if (height < 0)
                                {
                                    height = 0;
                                }

                                height = size.Height - height;
                                if (_dB < Wavedata[count2]) { _dB =MaxdB= Wavedata[count2]; }
                                try { g.FillRectangle(brush, count3, height, (float)LineThick, size.Height - height); }
                                catch { }
                                count3 = count3 + LineThick;
                                //g.DrawLine(pen,0, size.Height, /*(float)LineThick*/count2, size.Height - height);
                            }
                            count3=_dB = 0;
                        //}

                    }
                }
            }
            #endregion
            #region False
            else
            {
                if (Data.channel != null)
                {
                    PointF pf = new PointF();
                    /*
                            DRAW WAVEDATA   
                    */
                    //for (count = 0; count < numchannels; count++)
                    //{

                    if (GetData) Data.system.getWaveData(Wavedata, Wavedata.Length, channel);
                    else Data.channel.getWaveData(Wavedata, Wavedata.Length, channel);

                        for (count2 = 0; count2 < WavedataSize; count2++)
                        {
                            //float y;

                            Y = (Wavedata[count2] + 1) / 2.0f *size.Height;
                            X = count2 + size.Width - WavedataSize;
                            if (_dB < Wavedata[count2]) { _dB = MaxdB = Wavedata[count2]; }
                            pf.X = X; pf.Y = Y;
                            p.Add(pf);//g.DrawLine(pen,X,0,X, y);
                            //count3 = X + LineThick;
                        }
                        try { g.DrawLines(pen, p.ToArray()); }
                        catch { } p.Clear(); count3 = _dB = 0;
                    //}
                    //for (count = 0; count < numchannels; count++)
                    //{

                    //}
                }
            }
            #endregion
        }
        public bool IsResize=false;
        private Bitmap drawOscilliscope(Bitmap bmp, Size size, List<float> Wavedata, ref float MaxdB, int channel = 2, bool Reverse = false)
        {
            /*
            int numchannels = 0;
            int dummy = 0;
            FMOD.SOUND_FORMAT dummyformat = FMOD.SOUND_FORMAT.NONE;
            FMOD.DSP_RESAMPLER dummyresampler = FMOD.DSP_RESAMPLER.LINEAR;
            int count = 0;
            int count2 = 0;*/
            //List<PointF> p = new List<PointF>();
            if (Data.system != null)
            { Data.system.getSoftwareFormat(ref dummy, ref dummyformat, ref numchannels, ref dummy, ref _dummyresampler, ref dummy); }
            
            #region True
            if (Shape == DrawShape.Equalizer)
            {
                Graphics g = Graphics.FromImage(bmp); g.Clear(BackColor);
                #region GetDataTrue
                //if (GetData)
                {
                    
                    if (Data.channel != null)
                    {
                        /*
                                DRAW WAVEDATA
                        */

                        //for (count = 0; count < numchannels; count++)
                        //{
                        //if (GetData) Data.system.getWaveData(Wavedata, Wavedata.Length, channel);
                        //else Data.channel.getWaveData(Wavedata, Wavedata.Length, channel);
                        float max = 0;

                        for (count2 = 0; count2 < Wavedata.Count; count2++)
                        {
                            if (max < Wavedata[count2])
                            {max = Wavedata[count2]; }
                        }

                        /*
                            The upper band of frequencies at 44khz is pretty boring (ie 11-22khz), so we are only
                            going to display the first 256 frequencies, or (0-11khz) 
                        */
                        for (count2 = 0; count2 < Wavedata.Count; count2++)
                        {
                            float height;

                            height = Wavedata[count2] / max * size.Height;

                            if (height >= size.Height)
                            {height = size.Height - 1; }
                            if (height < 0) {height = 0; }
                            else
                            {
                                height = size.Height - height;
                                if (_dB < Wavedata[count2]) { _dB = MaxdB = Wavedata[count2]; }
                                try
                                {
                                    if (Reverse)
                                    {
                                        maxSamples = size.Width;
                                        for (int x = 0; x < size.Width; x++)
                                        {
                                            float lineHeight = size.Height * GetSample(x - size.Width + insertPos, Wavedata);
                                            float y1 = (size.Height - lineHeight) / 2;
                                            g.DrawLine(pen, x, y1, x, y1 + lineHeight);
                                        }/*
                                        recf.X = count3; recf.Y = height / 2;
                                        recf.Width = (float)LineThick; recf.Height = size.Height - height;
                                        rec.Add(recf);*/

                                    }
                                    else
                                    {
                                        //height = size.Height - height;
                                        //if (_dB < Wavedata[count2]) { _dB = MaxdB = Wavedata[count2]; }
                                        try { g.FillRectangle(brush, count3, height, (float)LineThick, size.Height - height); }
                                        catch { }
                                        //g.FillRectangle(brush, count3, 0, (float)LineThick, size.Height - height); 
                                    }
                                }
                                catch { }
                            }
                            count3 = count3 + LineThick;
                            //g.DrawLine(pen,0, size.Height, /*(float)LineThick*/count2, size.Height - height);
                        }
                        if (Reverse)
                        {
                            try { g.FillRectangles(brush, rec.ToArray()); } catch { }
                            rec.Clear();
                        }
                        count3 = _dB = 0;
                        //}

                    }
                }
                #endregion
                #region GetDataFalse
                #endregion
                return bmp;
            }
            #endregion
            #region False
            else
            {
                if (true)
                {
                    Graphics g = Graphics.FromImage(bmp);

                    #region 그리기다른알고리즘 False
                    if (!웨이브그래프ToolStripMenuItem.Checked)
                    {
                        PointF pf = new PointF();
                        /*
                                DRAW WAVEDATA   
                        */
                        //for (count = 0; count < numchannels; count++)
                        //{
                        int WavedataSize = this.WavedataSize;
                        //p.Add(new PointF(1, size.Height / 2)); p.Add(new PointF(2, size.Height / 2));
                        //int Maxsize = WavedataSize / bmp.Size.Width;

                        for (count2 = 0/*2*/; count2 < WavedataSize/*-2*/; count2++)
                        {
                            try{Y = (Wavedata[count2] + 1) / 2.0f * bmp.Height;}catch{}//size.Height;
                            X = count2 + bmp.Width - WavedataSize;
                            if (_dB < Wavedata[count2])
                                _dB = MaxdB = Wavedata[count2];
                            pf.X = count2; pf.Y = Y;
                            p.Add(pf);//g.DrawLine(pen,X,0,X, y);
                            //count3 = X + LineThick;
                        }
                        //p.Add(new PointF(size.Width - 2, size.Height / 2));p.Add(new PointF(size.Width-1, size.Height / 2));
                        try{g.DrawLines(pen, p.ToArray());g.Dispose();}catch{}
                        p.Clear();
                        //g.ScaleTransform(size.Width, size.Height);
                        return bmp;
                    }
                    #endregion
                    else
                    {
                        Point[] pt = new Point[size.Width];
                        Point pf = new Point();
                        Graphics g1;
                        int 한번에더할숫자 = Math.Max(WavedataSize / bmp.Size.Width,1), i=0, j=0;
                        for (int x = 0; x <size.Width; x++)
                        {
                            //pictureBox1.SizeMode = pictureBox2.SizeMode = PictureBoxSizeMode.StretchImage;
                            pf.Y = (int)((Wavedata[x] + 1) / 2.0f * bmp.Height); pf.X =j;try{pt[x] = new Point(pf.X, pf.Y);}catch{}
                            if(i>=한번에더할숫자||bmp.Size.Width>WavedataSize){i=0;j++;}else i++; //this.Size = new Size(1000, this.Size.Height);
                            //X = count2 + bmp.Width - WavedataSize;
                            //float lineHeight = size.Height * GetSample(x - OriginSize.Width + insertPos, Wavedata);
                            //float y1 = (OriginSize.Height - lineHeight) / 2;

                        }
                        Bitmap tmp = new Bitmap(j<1?1:j,size.Height);
                        using (g1 = Graphics.FromImage(tmp)) {g1.DrawLines(pen, pt); g1.Dispose(); }
                        bmp = tmp;
                        //bmp.Save("D:\\D데이터\\tmp.png");
                        return tmp;
                        //using (g1 = Graphics.FromImage(bmp)) { g1.DrawImage(tmp, 0, 0, bmp.Width, bmp.Height); }
                        //bmp = new Bitmap(bmp, size.Width, size.Height);
                        
                        
                        //for (count2 = 0/*2*/; count2 < WavedataSize/*-2*/; count2++)
                          //bmp.SetPixel((int)((bmp.Size.Width * count2) / WavedataSize), (int)(bmp.Size.Height * (Wavedata[count2] + 1f) / 2), DrawColor);
                     }
                }
                return new Bitmap(0, 0);
            }
            #endregion
        }
        Point[] BeforePoint = new Point[2];
        Point[] pt = new Point[2];
        public bool AddLine = true;
        int BORDER_WIDTH = 0;
        private Bitmap drawOscilliscope(Bitmap bmp, Size DrawSize, float[] Wavedata, ref float MaxdB, int channel = 2, bool DrawLine = true, bool Reverse = false)
        {
            /*
            int numchannels = 0;
            int dummy = 0;
            FMOD.SOUND_FORMAT dummyformat = FMOD.SOUND_FORMAT.NONE;
            FMOD.DSP_RESAMPLER dummyresampler = FMOD.DSP_RESAMPLER.LINEAR;
            int count = 0;
            int count2 = 0;*/
            //List<PointF> p = new List<PointF>();
            //if (Data.system != null){ Data.system.getSoftwareFormat(ref dummy, ref dummyformat, ref numchannels, ref dummy, ref _dummyresampler, ref dummy); }
            
            #region True
            if (Shape == DrawShape.Equalizer)
            {
                Graphics g = Graphics.FromImage(bmp);
                #region GetDataTrue
                //if (GetData)
                {
                    
                    if (Data.channel != null)
                    {
                        /*
                                DRAW WAVEDATA
                        */

                        //for (count = 0; count < numchannels; count++)
                        //{
                        //if (GetData) Data.system.getWaveData(Wavedata, Wavedata.Length, channel);
                        //else Data.channel.getWaveData(Wavedata, Wavedata.Length, channel);
                        float max = 0;

                        for (count2 = 0; count2 < Wavedata.Length; count2++)
                        {
                            if (max < Wavedata[count2])
                            {max = Wavedata[count2]; }
                        }

                        /*
                            The upper band of frequencies at 44khz is pretty boring (ie 11-22khz), so we are only
                            going to display the first 256 frequencies, or (0-11khz) 
                        */
                        for (count2 = 0; count2 < Wavedata.Length; count2++)
                        {
                            float height;

                            height = Wavedata[count2] / max * DrawSize.Height;

                            if (height >= DrawSize.Height)height = DrawSize.Height - 1;
                            if (height < 0)height = 0;
                            else
                            {
                                height = DrawSize.Height - height;
                                if (_dB < Wavedata[count2]) { _dB = MaxdB = Wavedata[count2]; }
                                try
                                {
                                    if (Reverse)
                                    {
                                        maxSamples = DrawSize.Width;
                                        for (int x = 0; x < DrawSize.Width; x++)
                                        {
                                            float lineHeight = DrawSize.Height * GetSample(x - DrawSize.Width + insertPos, Wavedata);
                                            float y1 = (DrawSize.Height - lineHeight) / 2;
                                            g.DrawLine(pen, x, y1, x, y1 + lineHeight);
                                        }/*
                                        recf.X = count3; recf.Y = height / 2;
                                        recf.Width = (float)LineThick; recf.Height = size.Height - height;
                                        rec.Add(recf);*/

                                    }
                                    else
                                    {
                                        //height = size.Height - height;
                                        //if (_dB < Wavedata[count2]) { _dB = MaxdB = Wavedata[count2]; }
                                        try { g.FillRectangle(brush, count3, height, (float)LineThick, DrawSize.Height - height); }
                                        catch { }
                                        //g.FillRectangle(brush, count3, 0, (float)LineThick, size.Height - height); 
                                    }
                                }
                                catch { }
                            }
                            count3 = count3 + LineThick;
                            //g.DrawLine(pen,0, size.Height, /*(float)LineThick*/count2, size.Height - height);
                        }
                        if (Reverse)
                        {
                            try { g.FillRectangles(brush, rec.ToArray()); } catch { }
                            rec.Clear();
                        }
                        count3 = _dB = 0;
                        //}
                        return bmp;
                    }
                }
                #endregion
                #region GetDataFalse
                #endregion
            }
            #endregion
            #region False
            else
            {
                if (true)
                {
                    Graphics g = Graphics.FromImage(bmp);

                    #region 그리기다른알고리즘 False
                    if (!웨이브그래프ToolStripMenuItem.Checked)
                    {
                        PointF pf = new PointF();
                        /*
                                DRAW WAVEDATA   
                        */
                        //for (count = 0; count < numchannels; count++)
                        //{
                        int WavedataSize = this.WavedataSize;
                        //p.Add(new PointF(1, size.Height / 2)); p.Add(new PointF(2, size.Height / 2));
                        //int Maxsize = WavedataSize / bmp.Size.Width;

                        for (count2 = 0/*2*/; count2 < WavedataSize/*-2*/; count2++)
                        {
                            try{Y = (Wavedata[count2] + 1) / 2.0f * bmp.Height;}catch{}//size.Height;
                            X = count2 + bmp.Width - WavedataSize;
                            if (_dB < Wavedata[count2])
                                _dB = MaxdB = Wavedata[count2];
                            pf.X = count2; pf.Y = Y;
                            p.Add(pf);//g.DrawLine(pen,X,0,X, y);
                            //count3 = X + LineThick;
                        }
                        //p.Add(new PointF(size.Width - 2, size.Height / 2));p.Add(new PointF(size.Width-1, size.Height / 2));
                        try{ g.DrawLines(pen, p.ToArray()); }
                        finally
                        {
                            if (DrawLine)
                            {
                                Center.X = 0; Center.Y = bmp.Height / 2; Center1.X = bmp.Width; Center1.Y = bmp.Height / 2;
                                g.DrawLine(pen1, Center, Center1);
                            }
                            g.Dispose();
                        }
                        p.Clear();
                        return bmp;
                        //g.ScaleTransform(size.Width, size.Height);
                    }
                    #endregion
                        
                    else
                    {
                        int height = DrawSize.Height;
                        int width = DrawSize.Width;
                        bool OverBuffer = !AddLine;// (width*3)<Data.Length;
                        if (!OverBuffer)
                        {
                            BeforePoint[0].X = 0; BeforePoint[0].Y = height / 2; BeforePoint[1].X = 0; BeforePoint[1].Y = height / 2;
                        }
                        //Buf.Clear();
                        int size = Wavedata.Length;
                        for (int iPixel = 0; iPixel < width; iPixel++)
                        {
                            // determine start and end points within WAV
                            int start = (int)((float)iPixel * ((float)size / (float)width));
                            int end = (int)((float)(iPixel + 1) * ((float)size / (float)width));
                            float min = float.MaxValue;
                            float max = float.MinValue;
                            for (int i = start; i < end; i++)
                            {
                                float val = Wavedata[i];
                                min = val < min ? val : min;
                                max = val > max ? val : max;
                            }
                            int yMax = BORDER_WIDTH + height - (int)((max + 1) * .5 * height);
                            int yMin = BORDER_WIDTH + height - (int)((min + 1) * .5 * height);
                            pt[0].X = pt[1].X = iPixel + BORDER_WIDTH;
                            pt[0].Y = yMax;
                            pt[1].Y = yMin;

                            if (!OverBuffer && pt[1].Y > -1)
                            {
                                //Point[] p = new Point[2]{pt[0], pt[1]};
                                //Buf.Add(p);
                                g.DrawLine(pen, BeforePoint[0], pt[1]);
                                BeforePoint[0].X = BeforePoint[1].X = iPixel + BORDER_WIDTH;
                                BeforePoint[0].Y = yMax;
                                BeforePoint[1].Y = yMin;
                            }
                            /*
                            if (pt[0].Y > 0&&iPixel>0)
                            {
                                if ((pt[0].X - BeforePoint[0].X) > 1)
                                {
                                    //AfterPoint.X = 
                                }
                            }*/
                            g.DrawLines(pen, pt);
                            //return;
                        }
                     }
                }
            }
            #endregion
            return bmp;
        }
        private PointF[] drawOscilliscopePoint(Size size, float[] Wavedata,int RealWidth ,ref float MaxdB, bool Reverse = false)
        {
            /*
            int numchannels = 0;
            int dummy = 0;
            FMOD.SOUND_FORMAT dummyformat = FMOD.SOUND_FORMAT.NONE;
            FMOD.DSP_RESAMPLER dummyresampler = FMOD.DSP_RESAMPLER.LINEAR;
            int count = 0;
            int count2 = 0;*/
            //List<PointF> p = new List<PointF>();
            if (Data.system != null)
            { Data.system.getSoftwareFormat(ref dummy, ref dummyformat, ref numchannels, ref dummy, ref _dummyresampler, ref dummy); }
            #region False
            if(Shape == DrawShape.Wave)
            {
                if (true)
                {
                    #region 그리기다른알고리즘 False
                    if (!웨이브그래프ToolStripMenuItem.Checked)
                    {

                        /*
                                DRAW WAVEDATA   
                        */
                        //for (count = 0; count < numchannels; count++)
                        //{
                        int WavedataSize = this.WavedataSize;
                        PointF[] p = new PointF[WavedataSize];
                        //p.Add(new PointF(1, size.Height / 2)); p.Add(new PointF(2, size.Height / 2));
                        //int Maxsize = WavedataSize / bmp.Size.Width;

                        for (count2 = 0/*2*/; count2 < WavedataSize/*-2*/; count2++)
                        {
                            try{Y = (Wavedata[count2] + 1) / 2.0f * size.Height;}catch{}//size.Height;
                            X = count2 + size.Width - WavedataSize;
                            if (_dB < Wavedata[count2])
                                _dB = MaxdB = Wavedata[count2];
                            p[count2].X= count2;  p[count2].Y = Y;
                            //count3 = X + LineThick;
                        }
                        //p.Add(new PointF(size.Width - 2, size.Height / 2));p.Add(new PointF(size.Width-1, size.Height / 2));
                        return p;
                        //g.ScaleTransform(size.Width, size.Height);
                    }
                    #endregion
                    else
                    {
                        PointF[] pt = new PointF[Wavedata.Length];
                        int 한번에더할숫자 = Math.Max(Wavedata.Length / size.Width,1), i=0, j=0;
                        int k = 0;
                        for (int x = 0; x <Wavedata.Length; x++)if(i>=한번에더할숫자||size.Width>Wavedata.Length){i=0;j++;}else i++;
                        k = j+1;
                        j = i = 0;
                        for (int x = 0; x <Wavedata.Length; x++)
                        {
                            //pictureBox1.SizeMode = pictureBox2.SizeMode = PictureBoxSizeMode.StretchImage;
                            try{pt[x].X=Math.Max(RealWidth-k+j, 0); pt[x].Y=(Wavedata[x] + 1) / 2.0f * size.Height;}catch{}
                            if(i>=한번에더할숫자||size.Width>Wavedata.Length){i=0;j++;}else i++; //this.Size = new Size(1s000, this.Size.Height);
                            //X = count2 + bmp.Width - WavedataSize;
                            //float lineHeight = size.Height * GetSample(x - OriginSize.Width + insertPos, Wavedata);
                            //float y1 = (OriginSize.Height - lineHeight) / 2;

                        }
                        //bmp.Save("D:\\D데이터\\tmp.png");
                        return pt ;
                        //using (g1 = Graphics.FromImage(bmp)) { g1.DrawImage(tmp, 0, 0, bmp.Width, bmp.Height); }  `
                        //bmp = new Bitmap(bmp, size.Width, size.Height);
                        
                        
                        //for (count2 = 0/*2*/; count2 < WavedataSize/*-2*/; count2++)
                          //bmp.SetPixel((int)((bmp.Size.Width * count2) / WavedataSize), (int)(bmp.Size.Height * (Wavedata[count2] + 1f) / 2), DrawColor);
                     }
                }
            }
           return new PointF[1];
            #endregion
        }
        private Bitmap drawOscilliscopeWithZoom(Bitmap bmp, float[] Wavedata,int RealWidth, bool DrawLine = true)
        {
            /*
            int numchannels = 0;
            int dummy = 0;
            FMOD.SOUND_FORMAT dummyformat = FMOD.SOUND_FORMAT.NONE;
            FMOD.DSP_RESAMPLER dummyresampler = FMOD.DSP_RESAMPLER.LINEAR;
            int count = 0;
            int count2 = 0;*/
            //List<PointF> p = new List<PointF>();
            //if (Data.system != null) { Data.system.getSoftwareFormat(ref dummy, ref dummyformat, ref numchannels, ref dummy, ref _dummyresampler, ref dummy); }
            #region False
            if(Shape == DrawShape.Wave)
            {
                if (true)
                {
                        PointF[] pt = new PointF[Wavedata.Length];
                        float 한번에더할숫자 = Math.Max((float)Wavedata.Length / (float)RealWidth, 1);
                        float i=0, j = 0;
                        float k = 0;
                        float x점증가 = (Wavedata.Length / RealWidth) / 한번에더할숫자;
                        //for (int x = 0; x <Wavedata.Length; x++)if(i>=한번에더할숫자||RealWidth>Wavedata.Length){i=0;k++;}else i++;
                        for (int x = 0; x <Wavedata.Length; x++)if(i>한번에더할숫자||bmp.Width>Wavedata.Length){i=0;k+=x점증가;}else i+=x점증가;
                        float 미세조정 = Math.Max((RealWidth-k)/(Wavedata.Length/한번에더할숫자),0);
                        //k = j;
                        j = i = 0;
                        for (int x = 0; x <= Wavedata.Length; x++)
                        {
                            //for (float z = 0; z <= 한번에더할숫자; z += x점증가)
                            try{pt[x].X=j; pt[x].Y=(Wavedata[x] + 1) / 2.0f * bmp.Height;}catch{}
                            if(i>=(한번에더할숫자)||bmp.Width>Wavedata.Length){i=0;j+=(1+미세조정);}else i+=(x점증가);
                        }
                        /*
                        for (int x = 0; x <Wavedata.Length; x++)
                        {

                            try{pt[x].X=Math.Min(RealWidth-k+j, j); pt[x].Y=(Wavedata[x] + 1) / 2.0f * bmp.Height;}catch{}
                            if(i>=한번에더할숫자||bmp.Width>Wavedata.Length){i=0;j++;}else i++; //this.Size = new Size(1s000, this.Size.Height);
                            //X = count2 + bmp.Width - WavedataSize;
                            //float lineHeight = size.Height * GetSample(x - OriginSize.Width + insertPos, Wavedata);
                            //float y1 = (OriginSize.Height - lineHeight) / 2;

                        }*/
                        //bmp.Save("D:\\D데이터\\tmp.png");
                        using (Graphics g1 = Graphics.FromImage(bmp))
                        {
                            g1.DrawLines(pen, pt);
                            if (DrawLine)
                            {
                                Center.X = 0; Center.Y = bmp.Height / 2; Center1.X = bmp.Width; Center1.Y = bmp.Height / 2;
                                g1.DrawLine(pen1, Center, Center1);
                            }
                        }
                        //bmp = new Bitmap(bmp, size.Width, size.Height);
                        
                        
                        //for (count2 = 0/*2*/; count2 < WavedataSize/*-2*/; count2++)
                          //bmp.SetPixel((int)((bmp.Size.Width * count2) / WavedataSize), (int)(bmp.Size.Height * (Wavedata[count2] + 1f) / 2), DrawColor);
                }
            }
           return bmp;
            #endregion
        }
        private Image drawOscilliscope(Image bmp, Size size, int channel, bool Reverse, ref float MaxdB)
        {
            /*
            int numchannels = 0;
            int dummy = 0;
            FMOD.SOUND_FORMAT dummyformat = FMOD.SOUND_FORMAT.NONE;
            FMOD.DSP_RESAMPLER dummyresampler = FMOD.DSP_RESAMPLER.LINEAR;
            int count = 0;
            int count2 = 0;*/
            //List<PointF> p = new List<PointF>();
            if (Data.system != null)
            {
                Data.system.getSoftwareFormat(ref dummy, ref dummyformat, ref numchannels, ref dummy, ref _dummyresampler, ref dummy);
            }

            #region True
            if (Shape == DrawShape.Equalizer)
            {
                Graphics g = Graphics.FromImage(bmp);
                #region GetDataTrue
                //if (GetData)
                {

                    if (Data.channel != null)
                    {
                        /*
                                DRAW WAVEDATA
                        */

                        //for (count = 0; count < numchannels; count++)
                        //{
                        if (GetData) Data.system.getWaveData(Wavedata, Wavedata.Length, channel);
                        else Data.channel.getWaveData(Wavedata, Wavedata.Length, channel);
                        //Data.system.getWaveData(Wavedata, Wavedata.Length, channel);
                        float max = 0;

                        for (count2 = 0; count2 < Wavedata.Length; count2++)
                        {
                            if (max < Wavedata[count2])
                            {
                                max = Wavedata[count2];
                            }
                        }

                        /*
                            The upper band of frequencies at 44khz is pretty boring (ie 11-22khz), so we are only
                            going to display the first 256 frequencies, or (0-11khz) 
                        */
                        for (count2 = 0; count2 < Wavedata.Length; count2++)
                        {
                            float height;

                            height = Wavedata[count2] / max * size.Height;

                            if (height >= size.Height)
                            {
                                height = size.Height - 1;
                            }
                            if (height < 0)
                            {
                                height = 0;
                            }
                            else
                            {
                                height = size.Height - height;
                                if (_dB < Wavedata[count2])
                                {
                                    _dB = MaxdB = Wavedata[count2];
                                }
                                try
                                {
                                    if (Reverse)
                                    {
                                        maxSamples = size.Width;
                                        for (int x = 0; x < size.Width; x++)
                                        {
                                            float lineHeight = size.Height * GetSample(x - size.Width + insertPos, Wavedata);
                                            float y1 = (size.Height - lineHeight) / 2;
                                            g.DrawLine(pen, x, y1, x, y1 + lineHeight);
                                        }/*
                                        recf.X = count3; recf.Y = height / 2;
                                        recf.Width = (float)LineThick; recf.Height = size.Height - height;
                                        rec.Add(recf);*/

                                    }
                                    else
                                    {
                                        //height = size.Height - height;
                                        //if (_dB < Wavedata[count2]) { _dB = MaxdB = Wavedata[count2]; }
                                        try
                                        {
                                            g.FillRectangle(brush, count3, height, (float)LineThick, size.Height - height);
                                        }
                                        catch
                                        {
                                        }
                                        //g.FillRectangle(brush, count3, 0, (float)LineThick, size.Height - height); 
                                    }
                                }
                                catch
                                {
                                }
                            }
                            count3 = count3 + LineThick;
                            //g.DrawLine(pen,0, size.Height, /*(float)LineThick*/count2, size.Height - height);
                        }
                        if (Reverse)
                        {
                            try
                            {
                                g.FillRectangles(brush, rec.ToArray());
                            }
                            catch
                            {
                            }
                            rec.Clear();
                        }
                        count3 = _dB = 0;
                        //}

                    }
                    return bmp;
                }
                #endregion
                #region GetDataFalse
                #endregion
            }
            #endregion
            #region False
            else
            {
                if (Data.channel != null)
                {
                    Graphics g = Graphics.FromImage(bmp);
                    if (GetData) Data.system.getWaveData(Wavedata, Wavedata.Length, channel);
                    else Data.channel.getWaveData(Wavedata, Wavedata.Length, channel);

                    if (!웨이브그래프ToolStripMenuItem.Checked)
                    {
                        PointF pf = new PointF();
                        /*
                                DRAW WAVEDATA   
                        */
                        //for (count = 0; count < numchannels; count++)
                        //{
                        int WavedataSize = this.WavedataSize;
                        //p.Add(new PointF(1, size.Height / 2)); p.Add(new PointF(2, size.Height / 2));
                        //int Maxsize = WavedataSize / bmp.Size.Width;

                        for (count2 = 0/*2*/; count2 < WavedataSize/*-2*/; count2++)
                        {
                            try
                            {
                                Y = (Wavedata[count2] + 1) / 2.0f * bmp.Height;
                            }
                            catch
                            {
                            }//size.Height;
                            X = count2 + bmp.Width - WavedataSize;
                            if (_dB < Wavedata[count2])
                                _dB = MaxdB = Wavedata[count2];
                            pf.X = count2; pf.Y = Y;
                            p.Add(pf);//g.DrawLine(pen,X,0,X, y);
                            //count3 = X + LineThick;
                        }
                        //p.Add(new PointF(size.Width - 2, size.Height / 2));p.Add(new PointF(size.Width-1, size.Height / 2));
                        try
                        {
                            g.DrawLines(pen, p.ToArray()); g.Dispose();
                        }
                        catch
                        {
                        }

                        //g.ScaleTransform(size.Width, size.Height);

                        p.Clear();
                        return bmp;
                    }
                    else
                    {
                        Point[] pt = new Point[size.Width];
                        Point pf = new Point();
                        Graphics g1;
                        int 한번에더할숫자 = Math.Max(WavedataSize / bmp.Size.Width,1), i=0, j=0;
                        for (int x = 0; x <size.Width; x++)
                        {
                            //pictureBox1.SizeMode = pictureBox2.SizeMode = PictureBoxSizeMode.StretchImage;
                            pf.Y = (int)((Wavedata[x] + 1) / 2.0f * bmp.Height); pf.X =j;try{pt[x] = new Point(pf.X, pf.Y);}catch{}
                            if(i>한번에더할숫자||bmp.Size.Width>WavedataSize){i=0;j++;}else i++; //this.Size = new Size(1000, this.Size.Height);
                            //X = count2 + bmp.Width - WavedataSize;
                            //float lineHeight = size.Height * GetSample(x - OriginSize.Width + insertPos, Wavedata);
                            //float y1 = (OriginSize.Height - lineHeight) / 2;

                        }
                        Bitmap tmp = new Bitmap(j,size.Height);
                        using (g1 = Graphics.FromImage(tmp)) {g1.DrawLines(pen, pt); g1.Dispose(); }
                        bmp = tmp;
                        return tmp;
                        //using (g1 = Graphics.FromImage(bmp)) { g1.DrawImage(tmp, 0, 0, bmp.Width, bmp.Height); }
                        //bmp = new Bitmap(bmp, size.Width, size.Height);


                        //for (count2 = 0/*2*/; count2 < WavedataSize/*-2*/; count2++)
                        //bmp.SetPixel((int)((bmp.Size.Width * count2) / WavedataSize), (int)(bmp.Size.Height * (Wavedata[count2] + 1f) / 2), DrawColor);
                    }
                }
            }
            return new Bitmap(0, 0);
            #endregion
        }

        int maxSamples;
        int insertPos;
        float GetSample(int index, float[] samples)
        {
            if (index < 0)
                index += maxSamples;
            if (index >= 0 & index < samples.Length)
                return samples[index];
            return 0;
        }
        float GetSample(int index, List<float> samples)
        {
            if (index < 0)
                index += maxSamples;
            if (index >= 0 & index < samples.Count)
                return samples[index];
            return 0;
        }

        public Bitmap drawOscilliscope(Size size, int channel, bool Reverse, ref float MaxdB)
        {
            /*
            int numchannels = 0;
            int dummy = 0;
            FMOD.SOUND_FORMAT dummyformat = FMOD.SOUND_FORMAT.NONE;
            FMOD.DSP_RESAMPLER dummyresampler = FMOD.DSP_RESAMPLER.LINEAR;
            int count = 0;
            int count2 = 0;*/
            //List<PointF> p = new List<PointF>();
            Bitmap bmp = new Bitmap(size.Width, size.Height);
            Graphics g = Graphics.FromImage(bmp);
            if (Data.system != null)
            { Data.system.getSoftwareFormat(ref dummy, ref dummyformat, ref numchannels, ref dummy, ref _dummyresampler, ref dummy); }
            #region True
            if (Shape == DrawShape.Equalizer)
            {
                #region GetDataTrue
                //if (GetData)
                {

                    if (Data.channel != null)
                    {
                        /*
                                DRAW WAVEDATA
                        */

                        //for (count = 0; count < numchannels; count++)
                        //{
                        if (GetData) Data.system.getWaveData(Wavedata, Wavedata.Length, channel);
                        else Data.channel.getWaveData(Wavedata, Wavedata.Length, channel);
                        //Data.system.getWaveData(Wavedata, Wavedata.Length, channel);
                        float max = 0;

                        for (count2 = 0; count2 < Wavedata.Length; count2++)
                        {
                            if (max < Wavedata[count2])
                            {
                                max = Wavedata[count2];
                            }
                        }

                        /*
                            The upper band of frequencies at 44khz is pretty boring (ie 11-22khz), so we are only
                            going to display the first 256 frequencies, or (0-11khz) 
                        */
                        for (count2 = 0; count2 < Wavedata.Length; count2++)
                        {
                            float height;

                            height = Wavedata[count2] / max * size.Height;

                            if (height >= size.Height)
                            {
                                height = size.Height - 1;
                            }

                            if (height < 0)
                            {
                                height = 0;
                            }
                            /*
                            if (!Reverse)
                            {
                                height = size.Height - height;
                                if (_dB < Wavedata[count2]) { _dB = MaxdB = Wavedata[count2]; }
                                try { g.FillRectangle(brush, count3, height/2, (float)LineThick, size.Height - height); }
                                catch { }
                            }*/
                            else
                            {
                                height = size.Height - height;
                                if (_dB < Wavedata[count2]) { _dB = MaxdB = Wavedata[count2]; }
                                try
                                {
                                    if (Reverse)
                                    {
                                        //g.FillRectangle(brush, count3, height / 2, (float)LineThick, size.Height - height);
                                        //recf.X=count2 + size.Width - WavedataSize;
                                        //recf.Y = (Wavedata[count2] + 1) * size.Height;
                                        recf.X = count3; recf.Y = height / 2;
                                        recf.Width = (float)LineThick; recf.Height = size.Height - height;
                                        rec.Add(recf);

                                    }
                                    else
                                    {
                                        //height = size.Height - height;
                                        //if (_dB < Wavedata[count2]) { _dB = MaxdB = Wavedata[count2]; }
                                        try { g.FillRectangle(brush, count3, height, (float)LineThick, size.Height - height); }
                                        catch { }
                                        //g.FillRectangle(brush, count3, 0, (float)LineThick, size.Height - height); 
                                    }
                                }
                                catch { }
                            }
                            count3 = count3 + LineThick;
                            //g.DrawLine(pen,0, size.Height, /*(float)LineThick*/count2, size.Height - height);
                        }
                        if (Reverse)
                        {
                            try { g.FillRectangles(brush, rec.ToArray()); }
                            catch { }
                            rec.Clear();
                        }
                        count3 = _dB = 0;
                        //}

                    }
                }
                #endregion
                #region GetDataFalse
                #endregion
            }
            #endregion
            #region False
            else
            {
                if (Data.channel != null)
                {
                    PointF pf = new PointF();
                    /*
                            DRAW WAVEDATA   
                    */
                    //for (count = 0; count < numchannels; count++)
                    //{

                    if (GetData) Data.system.getWaveData(Wavedata, Wavedata.Length, channel);
                    else Data.channel.getWaveData(Wavedata, Wavedata.Length, channel);

                    //p.Add(new PointF(1, size.Height / 2)); p.Add(new PointF(2, size.Height / 2));
                    for (count2 = 0/*2*/; count2 < WavedataSize/*-2*/; count2++)
                    {
                        Y = (Wavedata[count2] + 1) / 2.0f * size.Height;
                        X = count2 + size.Width - WavedataSize;
                        if (_dB < Wavedata[count2]) { _dB = MaxdB = Wavedata[count2]; }
                        pf.X = X; pf.Y = Y;
                        p.Add(pf);//g.DrawLine(pen,X,0,X, y);
                        //count3 = X + LineThick;
                    }
                    //p.Add(new PointF(size.Width - 2, size.Height / 2));p.Add(new PointF(size.Width-1, size.Height / 2));
                    try { g.DrawLines(pen, p.ToArray()); }
                    catch { } p.Clear(); count3 = _dB = 0;
                }
            }
            #endregion
            return bmp;
        }
        float X, Y;
        RectangleF recf;
        static System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmFFTGraph));
        #endregion

        Form f = new Form()
        {
            //MdiParent = this,
            Size = new Size(300, 350),
            Text = "그래프 설정...",
            ShowIcon=false,
            Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon"))),
        };
        void f_FormClosing(object sender, CancelEventArgs e) { e.Cancel = true; f.Hide(); }
        private void FFTGraph_Load(object sender, EventArgs e)
        {
            this.pictureBox1.Location =this.pictureBox2.Location= new Point(0, 0);
            this.pictureBox1.Size = this.splitContainer1.Panel1.Size;
            this.pictureBox2.Size = this.splitContainer1.Panel2.Size;
            timer2.Start();
            f.Controls.Add(pg);
            f.Show();
            SetStyle(ControlStyles.SupportsTransparentBackColor, true);
            toolStripComboBox2.SelectedIndex = 1;
            BeforeSize = 1024;BeforeSpeed = 25;
            toolStripComboBox3.SelectedIndex = 5;
            //ThreadPool.QueueUserWorkItem(new WaitCallback(DrawOSC_Thread));
        }
        float LdB = 0, RdB = 0;
        Point Center, Center1;
        //Bitmap bmp,bmp1;
        Bitmap tmp = null, tmp1 = null;
        int CheckInt;
        private void timer1_Tick(object sender, EventArgs e)
        {
            if (tmp != null) tmp.Dispose(); if (tmp1 != null) tmp1.Dispose();
            if (그리기크기<1||!웨이브그래프ToolStripMenuItem.Checked)
            {
                if (ToDraw == DrawSpectrum.FFTSpectrum)
                {
                    if ((int)this.FFTsize > 1)
                    {
                        tmp = new Bitmap(pictureBox1.Width, pictureBox1.Height);
                        tmp1 = new Bitmap(pictureBox2.Width, pictureBox2.Height);
                        /*
                        if (AutoSizing)
                        {
                            tmp = new Bitmap((int)this.FFTsize, pictureBox1.Height);
                            tmp1 = new Bitmap((int)this.FFTsize, pictureBox2.Height);
                        }
                        else
                        {
                            tmp = new Bitmap(pictureBox1.Width, pictureBox1.Height);
                            tmp1 = new Bitmap(pictureBox2.Width, pictureBox2.Height);
                        }*/
                        try
                        {
                            Graphics g = Graphics.FromImage(tmp);
                            Graphics g1 = Graphics.FromImage(tmp1);
                            if (fFT그래프ToolStripMenuItem.Checked) {drawSpectrum(g, tmp.Size, 0, true, -50); drawSpectrum(g1, tmp1.Size, 1, true, -50);}
                            else{tmpLMax = drawSpectrum(g, tmp.Size, 0); tmpRMax = drawSpectrum(g1, tmp1.Size, 1);}
                            pictureBox1.Image = tmp; pictureBox2.Image = tmp1;
                        }catch{}
                    }
                }
                else if (ToDraw == DrawSpectrum.WaveData)
                {
                    tmp = new Bitmap(pictureBox1.Width, pictureBox1.Height);
                    tmp1 = new Bitmap(pictureBox2.Width, pictureBox2.Height);
                    if (GetData)
                    {
                        Data.system.getWaveData(Wavedata, Wavedata.Length, 0); LeftData = Wavedata;
                        Data.system.getWaveData(Wavedata, Wavedata.Length, 1);RightData= Wavedata;
                    }
                    else
                    {
                        Data.channel.getWaveData(Wavedata, Wavedata.Length, 0);LeftData = Wavedata;
                        Data.channel.getWaveData(Wavedata, Wavedata.Length, 1);RightData= Wavedata;
                    }
                    tmp = drawOscilliscope(tmp, VRSize, LeftData, ref LdB, 0, DrawLine, Flip);
                    tmp1 = drawOscilliscope(tmp1, VRSize, RightData, ref RdB, 1, DrawLine, Flip);
                    /*
                    if (DrawLine)
                    {
                        Center.X = 0; Center.Y = tmp.Height / 2; Center1.X = tmp.Width; Center1.Y = tmp.Height / 2;
                        tmp = drawOscilliscope(tmp, VRSize, LeftData, ref LdB, 0, DrawLine, Flip);
                        if (tmp != null) using (Graphics g = Graphics.FromImage(tmp)) g.DrawLine(pen1, Center, Center1);
                        //pictureBox1.CreateGraphics().DrawImageUnscaled(bmp, 0, 0);

                        Center.X = 0; Center.Y = tmp1.Height / 2; Center1.X = tmp1.Width; Center1.Y = tmp1.Height / 2;
                        tmp1 = drawOscilliscope(tmp1, VRSize, RightData, ref RdB ,1, DrawLine, Flip);
                        if (tmp1 != null) using (Graphics g = Graphics.FromImage(tmp1)) g.DrawLine(pen1, Center, Center1);
                        //tmp1.Save("D:\\D데이터\\tmp.png");
                        //pictureBox2.CreateGraphics().DrawImageUnscaled(bmp1, 0, 0);
                    }
                    else {drawOscilliscope(tmp, VRSize, 0, Flip, ref LdB); drawOscilliscope(tmp1, VRSize, 1, Flip, ref RdB);}*/
                    pictureBox1.Image = tmp; pictureBox2.Image = tmp1;
                }
            }
            else
            {
                tmp = new Bitmap(splitContainer1.Panel1.Width, pictureBox1.Height);
                tmp1 = new Bitmap(splitContainer1.Panel2.Width, pictureBox2.Height);
                tmp = drawOscilliscopeWithZoom(tmp, _LeftData.ToArray(), tmp.Width, DrawLine);
                tmp1 = drawOscilliscopeWithZoom(tmp1, _RightData.ToArray(), tmp1.Width, DrawLine);
                pictureBox1.Image = tmp; pictureBox2.Image = tmp1;
            }
            //bmp.Dispose(); bmp1.Dispose(); g.Dispose(); g1.Dispose();
        }
        //int MaxLength=50000;
        Bitmap DrawOSC, DrawOSC1;
        Size pictureBox1_Size, pictureBox2_Size;
        float[] LeftData, RightData;
        bool Stop= false;
        /*internal void DrawOSC_Thread(object o)
        {
            while(!Stop){
            //SetStyle(ControlStyles.SupportsTransparentBackColor, true);
                //pictureBox1.Refresh(); pictureBox2.Refresh();
                try{
                //bmp=bmp==null?bmp = new Bitmap(WavedataSize, pictureBox1_Size.Height):bmp;
                //bmp1=bmp1==null?bmp1 = new Bitmap(WavedataSize, pictureBox2_Size.Height):bmp1;
                if (CheckInt > 그리기크기)
                {
                    //pictureBox1.Image = pictureBox2.Image = null; 
                    //_LeftData = new List<PointF>(new PointF[pictureBox1.Width]); _RightData = new List<PointF>(new PointF[pictureBox2.Width]);
                    //_LeftData.Clear(); _RightData.Clear();
                    //LeftData = new List<PointF>(new PointF[(pictureBox1_Size.Width*WavedataSize)]); _RightData = new List<PointF>(new PointF[(pictureBox2_Size.Width*WavedataSize)]);
                    //_LeftData.Clear(); _RightData.Clear();
                    CheckInt = 0; 
                }

                if (ToDraw == DrawSpectrum.FFTSpectrum)
                {
                    Graphics g = Graphics.FromImage(bmp);
                    Graphics g1 = Graphics.FromImage(bmp1);
                    drawSpectrum(g, bmp.Size, 0); drawSpectrum(g1, bmp1.Size, 1);
                }
                else if (ToDraw == DrawSpectrum.WaveData)
                {

                   //if(bmp!=null)bmp.Disp\ose(); if(bmp1!=null)bmp1.Dispose();
                   bmp = new Bitmap(pictureBox1_Size.Width, pictureBox1_Size.Height);
                   bmp1 = new Bitmap(pictureBox2_Size.Width, pictureBox2_Size.Height);
                   //DrawOSC = bmp;DrawOSC1 = bmp1;
                    //if(Shape == DrawShape.Equalizer)
                    if (DrawLine)
                    {
                        //_LeftData.Clear(); _RightData.Clear();
                        //Data.Pause();
                        //_LeftData = new List<PointF>(new PointF[pictureBox1.Width]); _RightData = new List<PointF>(new PointF[pictureBox2.Width]);
                        CheckInt++;

                        //Size VRSize = new System.Drawing.Size(이어서그리기, pictureBox1_Size.Height);
                        PointF[] a1 = drawOscilliscopePoint(pictureBox1_Size, _LeftData.ToArray(), pictureBox1_Size.Width, ref LdB, Flip);
                        PointF[] a2 = drawOscilliscopePoint(pictureBox2_Size, _RightData.ToArray(), pictureBox2_Size.Width, ref RdB, Flip);
                        //_RightData.InsertRange(_RightData.Count-WavedataSize, a2);

                        Center.X = 0; Center.Y = bmp.Height / 2; Center1.X = bmp.Width; Center1.Y = bmp.Height / 2;
                        //if (tmp != null) using (Graphics g = Graphics.FromImage(tmp)) g.DrawLine(pen1, Center, Center1);
                        //pictureBox1.CreateGraphics().DrawImageUnscaled(bmp, 0, 0);

                        Center.X = 0; Center.Y = bmp1.Height / 2; Center1.X = bmp1.Width; Center1.Y = bmp1.Height / 2;
                        //if (tmp1 != null) using (Graphics g = Graphics.FromImage(tmp1)) g.DrawLine(pen1, Center, Center1);
                        //tmp.Save("D:\\D데이터\\tmp.png");
                        //pictureBox2.CreateGraphics().DrawImageUnscaled(bmp1, 0, 0);
                        //_LeftData.Clear(); _RightData.Clear();

                        try
                        {
                            using (Graphics g = Graphics.FromImage(bmp))
                            {
                                using (Graphics g1 = Graphics.FromImage(bmp1))
                                {
                                    //g.PixelOffsetMode = System.Drawing.Drawing2D.PixelOffsetMode.HighQuality;
                                    //g.CompositingMode = System.Drawing.Drawing2D.CompositingMode.SourceCopy;
                                    for(int i=0;i<a1.Length;i++)try
                                    {
                                            //_LeftData.Clear(); _RightData.Clear();
                                        g.DrawLine(pen,a1[i], a1[i+1]);
                                        g1.DrawLine(pen, a2[i], a2[i + 1]);
                                        //Thread.Sleep(1);
                                    }catch{}
                                    g.DrawLine(pen1, Center, Center1);
                                }
                            }
                        }
                        catch{}

                        DrawOSC = bmp; DrawOSC1 = bmp1;
                        //bmp.Save("D:\\D데이터\\tmp.png");
                        //CheckInt = CheckInt+이어서그리기;
                    }}
                }
                catch{}
               finally{ Thread.Sleep(DrawSpeed);}
            }
            //Stop = true;
        }*/

        internal void GetWave_Thread(object o)
        {
            while (!Stop)
            {
                try{
                    if (GetData)
                    {
                        Data.system.getWaveData(LeftData, LeftData.Length, 0);
                        Data.system.getWaveData(RightData, RightData.Length, 1);
                    }
                    else
                    {
                        Data.channel.getWaveData(LeftData, LeftData.Length, 0);
                        Data.channel.getWaveData(RightData, RightData.Length, 1);
                    }
                    _LeftData.AddRange(LeftData); _LeftData.RemoveRange(0, LeftData.Length);
                    _RightData.AddRange(RightData); _RightData.RemoveRange(0, RightData.Length);
                }catch{}
                finally{Thread.Sleep(18);}
                    //Thread.Sleep(LeftData.Length/50);
                //_LeftData.Clear(); _RightData.Clear();
            }
        }
        [Obsolete]
        internal void GetWave_Thread1(object o)
        {
            int i, 갯수, Offest, j, 나머지, g, k; 
            while (!Stop)
            {
                float[] a = new float[그리기크기], b = new float[그리기크기];

                int c = 0;
                try{
                    if (GetData)
                    {
                        Data.system.getWaveData(a, a.Length, 0);
                        Data.system.getWaveData(b, b.Length, 1);
                        갯수 = LeftData.Length / a.Length;
                        for (i =0; i < 갯수-1; i++){
                            int Offset = i * a.Length;
                            for(j = Offset; j<a.Length+Offset; j++){c++;
                                try{LeftData[/*(i* a.Length)+(j-Offset)*/j] = LeftData[ a.Length+j];}catch{}
                                try{RightData[j] = RightData[ a.Length+j];}catch{} }}

                        나머지 = LeftData.Length % a.Length;
                        g = 나머지 == 0 ? LeftData.Length - a.Length : LeftData.Length - a.Length + 나머지;
                        for(k = 0;k<a.Length;k++){
                            if(k+g<LeftData.Length){
                               LeftData[LeftData.Length - a.Length+k] = a[k];
                               RightData[LeftData.Length - a.Length+k] = b[k];}
                           else break;}
                    }
                    else
                    {
                        Data.channel.getWaveData(a, a.Length, 0);
                        Data.channel.getWaveData(b, b.Length, 1);
                        갯수 = LeftData.Length / a.Length;
                        for (i =0; i < 갯수-1; i++){
                            int Offset = i * a.Length;
                            for(j = Offset; j<a.Length+Offset; j++){c++;
                                try{LeftData[/*(i* a.Length)+(j-Offset)*/j] = LeftData[ a.Length+j];}catch{}
                                try{RightData[j] = RightData[ a.Length+j];}catch{} }}

                        나머지 = LeftData.Length % a.Length;
                        g = 나머지 == 0 ? LeftData.Length - a.Length : LeftData.Length - a.Length + 나머지;
                        for(k = 0;k<a.Length;k++){
                            if(k+g<LeftData.Length){
                               LeftData[LeftData.Length - a.Length+k] = a[k];
                               RightData[LeftData.Length - a.Length+k] = b[k];}
                           else break;}
                    }
                }catch{}
                    //Thread.Sleep(LeftData.Length/50);
                //_LeftData.Clear(); _RightData.Clear();
            }
        }

        int[] h = new int[1];

        float[] Ltmp = new float[1000], Rtmp = new float[1000];
        float tmpLMax, tmpRMax;
        string LDB_S, RDB_S;
        int FPS;// { get {return /*frmFFTGraph.*/CalculateFrameRate(); } }
        private void timer2_Tick(object sender, EventArgs e)
        {
            FPS = CalculateFrameRate();
            //SetStyle(ControlStyles.SupportsTransparentBackColor, true);
            //toolTip1.BackColor = Color.Black; toolTip1.ForeColor = Color.White;
            
            //Ltmp = new float[this.Width]; Rtmp = new float[this.Width];
            //timer2.Interval = 50;
            //int FPS=frmFFTGraph.CalculateFrameRate();
            /*
            if (numchannels == 1)
            {
                if (GetData) { Data.channel.getWaveData(Ltmp, Ltmp.Length, 0); Data.channel.getWaveData(Rtmp, Rtmp.Length, 0); }
                else { Data.channel.getWaveData(Ltmp, Ltmp.Length, 0); Data.channel.getWaveData(Rtmp, Rtmp.Length, 0); }
                for (int i = 0; i < Ltmp.Length; i++) { if (tmpLMax < Ltmp[i]) { tmpRMax = tmpLMax = Ltmp[i]; } }
            }
            if (numchannels == 2)
            {
                if (GetData) { Data.system.getWaveData(Ltmp, Ltmp.Length, 0); Data.system.getWaveData(Rtmp, Rtmp.Length, 1); }
                else { Data.channel.getWaveData(Ltmp, Ltmp.Length, 0); Data.channel.getWaveData(Rtmp, Rtmp.Length, 1); }
                tmpLMax = Ltmp[Ltmp.Length/2];//this.Width / 2]; 
                tmpRMax = Rtmp[Rtmp.Length/2];//this.Width / 2];

                
                if (MaxLength <= _LeftData.Count) _LeftData.RemoveRange(0, Ltmp.Length);
                _LeftData.AddRange(Ltmp);
                if (MaxLength <= _RightData.Count) _RightData.RemoveRange(0, Rtmp.Length);
                _RightData.AddRange(Rtmp);

                //for (int i = 1; i < Rtmp.Length; i++)
                //{
                    //if (tmpLMax > Ltmp[i]) { tmpLMax = Ltmp[i]; }
                    //if (tmpRMax > Rtmp[i]) { tmpRMax = Rtmp[i]; }
                //}
            }*/
            if (Data != null)
            {
                if (GetData && Data.system != null)
                {
                    float[] a1 = new float[128], a2 = new float[128];
                    Data.system.getWaveData(a1, 128, 0);
                    Data.system.getWaveData(a2, 128, 1);
                    tmpLMax = a1[127]; tmpRMax = a2[127];
                }
                else if (!GetData && Data.channel != null)
                {
                    float[] a1 = new float[128], a2 = new float[128];
                    Data.channel.getWaveData(a1, 128, 0);
                    Data.channel.getWaveData(a2, 128, 1);
                    tmpLMax = a1[127]; tmpRMax = a2[127];
                }
                else tmpLMax=tmpRMax = float.NaN;
                string a, b;
                label1.Text = FPS.ToString("FPS: 00");
                float LDB = tmpLMax, RDB = tmpRMax;//0.0f,RDB=0.0f;

                if (LDB == 0) LDB_S = "Inf.";
                else if (Decibels(LDB, 0) < -365.5) LDB_S = "-Inf.";
                else LDB_S = Decibels(LDB, 0).ToString("0.000");

                if (RDB == 0)RDB_S = "Inf.";
                else if (Decibels(RDB, 0) < -365.5) RDB_S = "-Inf.";
                else RDB_S = Decibels(RDB, 0).ToString("0.000");
            }
            
            /*
            if (Decibels(LDB, 0).ToString().IndexOf("-3.899998E-19") != -1 ||
                Decibels(LDB, 0).ToString().IndexOf("7.39567E-17") != -1 ||
                Decibels(LDB, 0).ToString().IndexOf("7.395671E-17") != -1 ||
                Decibels(LDB, 0).ToString().IndexOf("5.277585") != -1 ||
                Decibels(LDB, 0) == 0) { LDB = 0; }

            if (Decibels(RDB, 0).ToString().IndexOf("-3.899998E-19") != -1 ||
                Decibels(RDB, 0).ToString().IndexOf("7.39567E-17") != -1 ||
                Decibels(RDB, 0).ToString().IndexOf("7.395671E-17") != -1 ||
                Decibels(RDB, 0).ToString().IndexOf("5.277585") != -1 ||
                Decibels(RDB, 0) == 0) { RDB = 0; }
             */
            /*
            if (tmpLMax != 0.0f)
            {
                if (tmpLMax>0) { LDB = tmpLMax; }
                else
                {
                    //if (tmpLMax > 0.1 && tmpLMax < 1) { LDB = tmpLMax; }
                    for(int i=0;i<tmpLMax.ToString().Length;i++){string[] s = CSharpUtility.Utility.StringUtility.ConvertStringToArray(tmpLMax.ToString(),"");}
                    if (tmpLMax < 0.1) { if (false) { LDB = (tmpLMax - 1.0f) * 100; } else { LDB = (tmpLMax - 1.0f) * 10; } }
                    else { LDB = tmpLMax*10; }
                }
            }
            if (tmpRMax != 0.0f)
            {
                if (tmpRMax > 0) { RDB = tmpRMax; }
                else
                {
                    if (tmpRMax < 0.1) { if (tmpRMax < 0.01 && tmpRMax > -0.1) { RDB = (tmpRMax - 1.0f) * 100; } else { RDB = (tmpRMax - 1.0f) * 10; } }
                    else { RDB = tmpRMax * 10; }
                    //if (tmpRMax < 0) { RDB = (tmpRMax - 1.0f)*10; }
                    //else { RDB = (tmpRMax*10); }
                }
            }
             */
            this.Text = string.Format("{0}그래프 [좌: {1} dB / 우: {2} dB] (FPS: {3})",
                ToDraw == DrawSpectrum.FFTSpectrum ? "FFT-EQ " : "웨이브 데이터 ", LDB_S, RDB_S, FPS.ToString("00"));
        }

        public static double mag_sqrd(double re, double im) { return (re * re + im * im); }

        public static double Decibels(double re, double im) { return ((re == 0 && im == 0) ? (0) : 10.0 * Math.Log10(((mag_sqrd(re, im))))); }

        string AddString(string Text, int num)
        {
            StringBuilder sb = new StringBuilder();
        for (int i = 0; i < num; i++) { sb.Append(Text); } return sb.ToString();}

        private int lastTick;
        private int lastFrameRate;
        private int frameRate;
        public int CalculateFrameRate()
        {
            if (System.Environment.TickCount - lastTick >= 1000)
            {
                lastFrameRate = frameRate;
                frameRate = 0;
                lastTick = System.Environment.TickCount;
            }
            frameRate++;
            return lastFrameRate;
        }

        /*
        private static int lastTick;
        private static int lastFrameRate;
        private static int frameRate;
        public static int CalculateFrameRate()
        {
            if (System.Environment.TickCount - lastTick >= 1000)
            {
                lastFrameRate = frameRate;
                frameRate = 0;
                lastTick = System.Environment.TickCount;
            }
            frameRate++;
            return lastFrameRate;
        }*/

        private void FFTGraph_SizeChanged(object sender, EventArgs e)
        {
            if (AutoWaveSize)
            {
                try { WavedataSize = splitContainer1.Width; }catch{}
            }
            Ltmp = new float[pictureBox1.Width]; Rtmp = new float[pictureBox2.Width];
            //if(이어서그리기>0&&그리기다른알고리즘ToolStripMenuItem.Checked){_LeftData.Clear();_RightData.Clear(); _LeftData = new List<PointF>(new PointF[pictureBox1.Width*WavedataSize]); _RightData = new List<PointF>(new PointF[pictureBox2.Width*WavedataSize]);}
            pictureBox1_Size = pictureBox1.Size; pictureBox2_Size = pictureBox2.Size;
            //try { peak.peakMeterCtrl1_Resize(null, null); }catch{}
            //try { peak.peakMeterCtrl2_Resize(null, null);}catch{}
        }

        private void FFTGraph_FormClosed(object sender, FormClosedEventArgs e)
        {
            try { f.Close(); }
            catch { }
        }

        FormBorderStyle a;
        Rectangle b;
        Point Loc;
        private void pictureBox1_DoubleClick(object sender, EventArgs e)
        {
            if (this.FormBorderStyle != System.Windows.Forms.FormBorderStyle.None)
            {
                a = this.FormBorderStyle; b = new Rectangle(this.Top, this.Left, this.Width, this.Height);
                this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                Loc = this.Location;
                Left = Top = 0;
                Width = Screen.PrimaryScreen.WorkingArea.Width;
                Height = Screen.PrimaryScreen.WorkingArea.Height;
            }
            else 
            {
                this.FormBorderStyle = a;
                this.Top = b.X; this.Left = b.Y; this.Width = b.Width; this.Height = b.Height;
                this.Location = new Point(Loc.X, Loc.Y);
            }
        }


        private void timer3_Tick(object sender, EventArgs e)
        {

        }

        private void 설정창띄우기ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            f.BringToFront();
            f.Show();
        }

        private void fFTToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (fFTToolStripMenuItem.Checked)
            {
                fFTToolStripMenuItem.Checked = false;
                웨이브데이터ToolStripMenuItem.Checked = true;
                ToDraw = DrawSpectrum.WaveData;
            }
            else
            {
                fFTToolStripMenuItem.Checked = true;
                웨이브데이터ToolStripMenuItem.Checked = false;
                ToDraw = DrawSpectrum.FFTSpectrum;
            }
        }

        private void fMODSystemToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (fMODSystemToolStripMenuItem.Checked)
            {
                fMODSystemToolStripMenuItem.Checked = false;
                fMODChannelToolStripMenuItem.Checked = true;
                GetData = false;
            }
            else
            {
                fMODSystemToolStripMenuItem.Checked = true;
                fMODChannelToolStripMenuItem.Checked = false;
                GetData = true;
            }
        }

        private void toolStripComboBox1_TextChanged(object sender, EventArgs e)
        {
            
        }

        private void toolStripComboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

            toolTip1.Hide(toolStripComboBox1.Owner);
            string a=toolStripComboBox1.Items[toolStripComboBox1.SelectedIndex].ToString();
            this.FFTWindow = (HS_Audio_Lib.DSP_FFT_WINDOW)Enum.Parse(typeof(HS_Audio_Lib.DSP_FFT_WINDOW), a);
            switch (a)
            {
                case "RECT":
                    toolStripComboBox1.ToolTipText = "RECT:\r\nw[n] = 1.0";
                    toolTip1.SetToolTip(toolStripComboBox1.Owner,"RECT:\r\nw[n] = 1.0");break;
                case "TRIANGLE":
                    toolStripComboBox1.ToolTipText = "TRIANGLE:\r\nw[n] = TRI(2n/N)";
                    toolTip1.SetToolTip(toolStripComboBox1.Owner,"TRIANGLE:\r\nw[n] = TRI(2n/N)");break;
                case "HAMMING":
                    toolStripComboBox1.ToolTipText = "HAMMING:\r\nw[n] = 0.54 - (0.46 * COS(n/N) )";
                    toolTip1.SetToolTip(toolStripComboBox1.Owner,"HAMMING:\r\nw[n] = 0.54 - (0.46 * COS(n/N) )");break;
                case "HANNING":
                    toolStripComboBox1.ToolTipText = "HANNING:\r\nw[n] = 0.5 *  (1.0  - COS(n/N) )";
                    toolTip1.SetToolTip(toolStripComboBox1.Owner,"HANNING:\r\nw[n] = 0.5 *  (1.0  - COS(n/N) )"); break;
                case "BLACKMAN":
                    toolStripComboBox1.ToolTipText = "BLACKMAN:\r\nw[n] = 0.42 - (0.5  * COS(n/N) ) + (0.08 * COS(2.0 * n/N) )";
                    toolTip1.SetToolTip(toolStripComboBox1.Owner,"BLACKMAN:\r\nw[n] = 0.42 - (0.5  * COS(n/N) ) + (0.08 * COS(2.0 * n/N) )");break;
                case "BLACKMANHARRIS":
                    toolStripComboBox1.ToolTipText = "BLACKMANHARRIS:\r\nw[n] = 0.35875 - (0.48829 * COS(1.0 * n/N)) + (0.14128 * COS(2.0 * n/N)) - (0.01168 * COS(3.0 * n/N))";
                    toolTip1.SetToolTip(toolStripComboBox1.Owner,"BLACKMANHARRIS:\r\nw[n] = 0.35875 - (0.48829 * COS(1.0 * n/N)) + (0.14128 * COS(2.0 * n/N)) - (0.01168 * COS(3.0 * n/N))"); break;
                case "MAX":
                    toolStripComboBox1.ToolTipText = "MAX";
                    toolTip1.SetToolTip(toolStripComboBox1.Owner,"MAX"); break;
                default:
                    toolStripComboBox1.ToolTipText = "잘못된 값 입니다.";
                    toolTip1.SetToolTip(toolStripComboBox1.Owner,"잘못된 값 입니다."); break;
            }
        }

        Point Mouse;
        private void frmFFTGraph_MouseMove(object sender, MouseEventArgs e)
        {
            Mouse.X = e.X; Mouse.Y = e.Y;
        }

        private void toolStripTextBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == Convert.ToChar(Keys.Enter)) { toolStripTextBox1_TextChanged(null, null); }
            if (!(Char.IsDigit(e.KeyChar)) && e.KeyChar != Convert.ToChar(Keys.Back))
            { e.Handled = true; }
        }

        private void toolStripTextBox1_TextChanged(object sender, EventArgs e)
        {
            int a;
            if (toolStripTextBox1.Text == "") a = 1;
            if (int.Parse(toolStripTextBox1.Text)>0){ a = int.Parse(toolStripTextBox1.Text); }
            else a = 1;
            this.LineThick = a > 0 ? a : 1;
        }

        private void 똑같이나누기ToolStripMenuItem_CheckedChanged(object sender, EventArgs e)
        {
            this.LineTickLinkFFT = 똑같이나누기ToolStripMenuItem.Checked;
        }

        private void toolStripTextBox2_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == Convert.ToChar(Keys.Enter)) { toolStripTextBox1_TextChanged(null, null); }
            if (!(Char.IsDigit(e.KeyChar)) && e.KeyChar != Convert.ToChar(Keys.Back))
            { e.Handled = true; }
        }

        private void 맨위에표시ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.TopMost = 맨위에표시ToolStripMenuItem.Checked;
        }

        private void toolStripTextBox2_TextChanged(object sender, EventArgs e)
        {
            float a1 = 0;
            try { if (toolStripTextBox2.Text == "" || Convert.ToInt32(toolStripTextBox2.Text) == 0){ a1 = 1; toolStripTextBox2.Text = "1";} }
            catch { a1 = 1; toolStripTextBox2.Text = "1"; }
            if (float.Parse(toolStripTextBox2.Text) > 255 || float.Parse(toolStripTextBox2.Text) > 0)
            { try { a1 = float.Parse(toolStripTextBox2.Text); } catch { a1 = 1; toolStripTextBox2.Text = "1"; } }
            else a1 = 1;
            this.FFTAverage = a1 > 0 ? a1 : 1;
            toolStripTextBox2.Text = a1 > 0 ? a1.ToString() : "1";
        }

        private void toolTip1_Draw(object sender, DrawToolTipEventArgs e)
        {
            e.DrawBackground();
            e.DrawBorder();
            e.DrawText();
        }

        private void toolTip1_Popup(object sender, PopupEventArgs e)
        {
            //toolTip1.BackColor=Color.Black;
            //toolTip1.ForeColor = Color.White;
        }

        private void 배경투명처리ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!배경투명처리ToolStripMenuItem.Checked)
            {
                this.TransparencyKey = BackgroundColor; //분할자투명ToolStripMenuItem.Enabled = true;
                c = splitContainer1.BackColor; if (분할자투명ToolStripMenuItem.Checked) splitContainer1.BackColor = BackgroundColor;
                배경투명처리ToolStripMenuItem.Checked = true;
            }
            else
            {
                this.TransparencyKey = Color.Empty; //분할자투명ToolStripMenuItem.Enabled = false;
                splitContainer1.BackColor = c; 배경투명처리ToolStripMenuItem.Checked = false;
            }
            //분할자투명ToolStripMenuItem_CheckedChanged(null, null);
        }
        
        private void 배경투명처리ToolStripMenuItem_CheckedChanged(object sender, EventArgs e)
        {
            
        }

        private void 테두리표시ToolStripMenuItem_CheckedChanged(object sender, EventArgs e)
        {
            if (테두리표시ToolStripMenuItem.Checked)
            { this.splitContainer1.BorderStyle = this.pictureBox2.BorderStyle = BorderStyle.FixedSingle; }
            else { this.splitContainer1.BorderStyle = this.pictureBox2.BorderStyle = BorderStyle.None; }
        }

        private void toolStripComboBox1_MouseEnter(object sender, EventArgs e)
        {
            //toolTip1.Show("",toolStripComboBox1.Owner);
        }

        Color c;
        private void 분할자투명ToolStripMenuItem_CheckedChanged(object sender, EventArgs e)
        {
            if (분할자투명ToolStripMenuItem.Checked)
            { c = splitContainer1.BackColor; splitContainer1.BackColor = BackgroundColor; }
            else { splitContainer1.BackColor = c; }
        }

        private void splitContainer1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyValue == 115) { 배경투명처리ToolStripMenuItem_Click(null, null); }
        }

        private void toolStripComboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            try { this.FFTsize = (FFTSize)Enum.Parse(typeof(FFTSize), toolStripComboBox2.SelectedItem.ToString() == "None" ? "None" : "Size_" + toolStripComboBox2.SelectedItem.ToString()); }
            catch { MessageBox.Show("치트 같은걸로 값 임의로 변경하지마라!\n원래대로 되돌려놔라", "경고문", MessageBoxButtons.OK, MessageBoxIcon.Stop); }
        }

        private void fFTToolStripMenuItem_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void 저장SToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (saveFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            { System.IO.File.WriteAllLines(saveFileDialog1.FileName, HS_CSharpUtility.Utility.EtcUtility.SaveSetting(Setting)); }
        }
        private void 열기OToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            { Setting = HS_CSharpUtility.Utility.EtcUtility.LoadSetting(System.IO.File.ReadAllLines(openFileDialog1.FileName)); }
        }

        private void 그래프ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (그래프ToolStripMenuItem.Checked)
            {
                그래프ToolStripMenuItem.Checked = false;
                파동ToolStripMenuItem.Checked = true;
                Shape = DrawShape.Wave;
            }
            else
            {
                그래프ToolStripMenuItem.Checked = true;
                파동ToolStripMenuItem.Checked = false;
                Shape = DrawShape.Equalizer;
            }
        }

        private void fFTWindowToolStripMenuItem_DropDownOpening(object sender, EventArgs e)
        {
            toolStripComboBox1.SelectedIndex = (int)this.FFTWindow;
            toolStripComboBox1.Text = toolStripComboBox1.Items[toolStripComboBox1.SelectedIndex].ToString();
        }

        int FFTsizeIndex(FFTSize FFTSIZE)
        {
            switch (FFTSIZE)
            {
                case FFTSize.Size_4096: return 0;
                case FFTSize.Size_2048:return 1;
                case FFTSize.Size_1024:return 2;
                case FFTSize.Size_512:return 3;
                case FFTSize.Size_128: return 4;
                case FFTSize.Size_64: return 5;
                case FFTSize.None: return 6;
                default:return 0;
            }
        }
        private void fFT사이즈ToolStripMenuItem_DropDownOpening(object sender, EventArgs e)
        {
            toolStripComboBox2.SelectedIndex = FFTsizeIndex(this.FFTsize);
            toolStripComboBox2.Text = toolStripComboBox2.Items[toolStripComboBox2.SelectedIndex].ToString();
        }

        private void 그래프오차ToolStripMenuItem_DropDownOpening(object sender, EventArgs e)
        {
            toolStripTextBox2.Text = this.FFTAverage.ToString();
        }

        private void 그래프굵기ToolStripMenuItem_DropDownOpening(object sender, EventArgs e)
        {
            toolStripTextBox1.Text = ((int)this.LineThick).ToString();
            this.똑같이나누기ToolStripMenuItem.Checked = this.LineTickLinkFFT;
        }

        Size VRSize;
        private void pictureBox2_SizeChanged(object sender, EventArgs e)
        {
            timer1_Tick(null, null);
            timer2_Tick(null, null);
            Ltmp = new float[pictureBox1.Width != 0 ? pictureBox1.Width : 1]; Rtmp = new float[pictureBox2.Width != 0 ? pictureBox2.Width : 1];
            if (웨이브그래프ToolStripMenuItem.Checked) VRSize =new Size(WavedataSize, this.pictureBox1.Size.Height);
            else VRSize = this.pictureBox1.Size;
            
        }
        private void Helper_Disposing(object sender, bool All) { if (All)this.Close(); }

        int BeforeSize, BeforeSpeed;
        bool BeforeAutoWaveSize;

        bool 그리기다른알고리즘;
        private void 그리기다른알고리즘ToolStripMenuItem_CheckedChanged(object sender, EventArgs e)
        {
            그리기다른알고리즘 = 웨이브그래프ToolStripMenuItem.Checked;
            if (웨이브그래프ToolStripMenuItem.Checked && 그리기크기 > 0&&ToDraw == DrawSpectrum.WaveData)
            {
                BeforeSize = WavedataSize;
                BeforeSpeed = DrawSpeed;
                BeforeAutoWaveSize = AutoWaveSize;
                AutoWaveSize = false;
                DrawSpeed = 18;
                //timer1.Stop();
                Stop = false;
                ThreadPool.QueueUserWorkItem(new WaitCallback(GetWave_Thread));// ThreadPool.QueueUserWorkItem(new WaitCallback(DrawOSC_Thread));
            }
            else{ Stop = true;DrawSpeed = BeforeSpeed; WavedataSize = BeforeSize;AutoWaveSize = BeforeAutoWaveSize;}//timer1.Start();}   
            /*
            if(그리기다른알고리즘ToolStripMenuItem.Checked&&그리기크기>0){Stop=false;
            this.WavedataSize = 1024; AutoWaveSize = false; LeftData = new float[WavedataSize * 그리기크기]; RightData = new float[WavedataSize * 그리기크기];
                ThreadPool.QueueUserWorkItem(new WaitCallback(GetWave_Thread));ThreadPool.QueueUserWorkItem(new WaitCallback(DrawOSC_Thread));return;}
            else Stop=true;
            if (그리기다른알고리즘ToolStripMenuItem.Checked) {this.WavedataSize = 50000;AutoWaveSize = false;}// {pictureBox1.SizeMode = pictureBox2.SizeMode = PictureBoxSizeMode.StretchImage;}
            else{ this.WavedataSize = pictureBox1.Width;AutoWaveSize = true; } //pictureBox1.SizeMode = pictureBox2.SizeMode = PictureBoxSizeMode.Normal;*/
        }

        private void 그래프늘이기ToolStripMenuItem_CheckedChanged(object sender, EventArgs e)
        {
            if (그래프늘이기ToolStripMenuItem.Checked) pictureBox1.SizeMode = pictureBox2.SizeMode = PictureBoxSizeMode.StretchImage;
            else pictureBox1.SizeMode = pictureBox2.SizeMode = PictureBoxSizeMode.Normal;
        }

        private void toolStripTextBox3_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == Convert.ToChar(Keys.Enter)) { toolStripTextBox3_TextChanged(null, null); }
            if (!(Char.IsDigit(e.KeyChar)) && e.KeyChar != Convert.ToChar(Keys.Back))
            { e.Handled = true; }
        }


        public int 그리기크기;//{get;set{LeftData = }}
        private void toolStripTextBox3_TextChanged(object sender, EventArgs e)
        {
            int a=0;
            if (toolStripTextBox3.Text == ""){그리기크기=0;Stop=true;return;}
            else if (int.Parse(toolStripTextBox3.Text)>0)
            { 
                a = int.Parse(toolStripTextBox3.Text);
                WavedataSize = 1024; LeftData = new float[20000]; RightData = new float[20000];
                //if(웨이브그래프ToolStripMenuItem.Checked)WavedataSize = 3000;
                //_LeftData = new List<PointF>(new PointF[(pictureBox1_Size.Width*WavedataSize)]); _RightData = new List<PointF>(new PointF[(pictureBox2_Size.Width*WavedataSize)]);
                if(Stop){Stop = false; //ThreadPool.QueueUserWorkItem(new WaitCallback(DrawOSC_Thread)); 
                    ThreadPool.QueueUserWorkItem(new WaitCallback(GetWave_Thread));}
            }
                //bool Start = ThreadPool.QueueUserWorkItem(new WaitCallback(DrawOSC_Thread));
            else {Stop=true;a=0;}
            this.그리기크기 = a;
            //_LeftData = new List<float>(이어서그리기); _RightData = new List<float>(이어서그리기);
        }

        private void contextMenuStrip1_Opening(object sender, CancelEventArgs e)
        {

        }
        private void panel1사이즈조정()
        {
            //if (!AutoSizing)
            if (ToDraw == DrawSpectrum.FFTSpectrum)
                if (!AutoSizing) this.pictureBox1.Size = new Size(splitContainer1.Width, splitContainer1.Panel1.Height);
                else this.pictureBox1.Size = new Size((int)((int)FFTsize * ((!LineTickLinkFFT && Shape == DrawShape.Equalizer) ? LineThick : 1)), pictureBox1.Height);
            else
                if (!AutoSizing||WavedataSize <= splitContainer1.Width) this.pictureBox1.Size = new Size(splitContainer1.Width, splitContainer1.Panel1.Height);
                else this.pictureBox1.Size = new Size((int)(WavedataSize * (LineTickLinkFFT?1:LineThick)), pictureBox1.Height);

            //if (splitContainer1.Panel1.VerticalScroll.Visible) pictureBox1.Size = new Size(pictureBox1.Width, pictureBox1.Height - splitContainer1.Panel1.AutoScrollMargin.Height);
        }
        private void panel2사이즈조정()
        {
            if (ToDraw == DrawSpectrum.FFTSpectrum)
                if (!AutoSizing) this.pictureBox2.Size = new Size(splitContainer1.Width, splitContainer1.Panel2.Height);
                else this.pictureBox2.Size = new Size((int)((int)FFTsize * ((!LineTickLinkFFT && Shape == DrawShape.Equalizer) ? LineThick : 1)), /*(int)FFTsize>splitContainer1.Panel1.Width?:*/pictureBox2.Height);
            else
                if (!AutoSizing || WavedataSize <= splitContainer1.Width) this.pictureBox2.Size = new Size(splitContainer1.Width, splitContainer1.Panel2.Height);
                else this.pictureBox2.Size = new Size((int)(WavedataSize * (LineTickLinkFFT?1:LineThick)), pictureBox2.Height);
            //if (splitContainer1.Panel2.VerticalScroll.Visible) pictureBox2.Size = new Size(pictureBox2.Width, pictureBox2.Height - splitContainer1.Panel2.AutoScrollMargin.Height);
        }
        private void splitContainer1_Panel1_SizeChanged(object sender, EventArgs e)
        {
            //if(ToDraw == DrawSpectrum.FFTSpectrum&&LineTickLinkFFT)
            
        }

        private void splitContainer1_Panel2_SizeChanged(object sender, EventArgs e)
        {
        }

        private void 그리기영역자동조절ToolStripMenuItem_CheckedChanged(object sender, EventArgs e)
        {
            AutoSizing = 그리기영역자동조절ToolStripMenuItem.Checked;
        }

        private void splitContainer1_SizeChanged(object sender, EventArgs e)
        {
            panel1사이즈조정();panel2사이즈조정();
        }


        List<float> _LeftData = new List<float>(new float[50000]), _RightData = new List<float>(new float[50000]);
        private void toolStripComboBox3_SelectedIndexChanged(object sender, EventArgs e)
        {
            WavedataSize = 1024;
            //numericUpDown3.Value = 18;
            switch (toolStripComboBox3.SelectedIndex)
            {
                case 0: 그리기크기 = 1560; break;//1:1
                case 1: 그리기크기 = 3125; break;//1:2
                case 2: 그리기크기 = 6250; break;//1:4
                case 3: 그리기크기 = 12500; break;//1:8
                case 4: 그리기크기 = 25000; break;//1:16
                case 5: 그리기크기 = 50000; break;//1:32
                case 6: 그리기크기 = 100000; break;//1:64
                case 7: 그리기크기 = 200000; break;//1:128
                case 8: 그리기크기 = 400000; break;//1:256
                case 9: 그리기크기 = 800000; break;//1:512
                case 10: 그리기크기 = 1000000; break;//1:1024
                case 11: 그리기크기 = 2000000; break;//1:2048
                case 12: 그리기크기 = 4000000; break;//1:4096
                case 13: 그리기크기 = 8000000; break;//1:8192
                case 14: 그리기크기 = 10000000; break;//1:16384
            }
            if (그리기크기 < _LeftData.Count) _LeftData.RemoveRange(0, _LeftData.Count - 그리기크기);
            else if (그리기크기 > _LeftData.Count) _LeftData.InsertRange(0, new float[그리기크기 - _LeftData.Count]);
            if (그리기크기 < _RightData.Count) _RightData.RemoveRange(0, _RightData.Count - 그리기크기);
            else if (그리기크기 > _RightData.Count) _RightData.InsertRange(0, new float[그리기크기 - _RightData.Count]);
        }

        private void toolStripTextBox4_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == Convert.ToChar(Keys.Enter))toolStripTextBox4_TextChanged(null, null);
            if (!(Char.IsDigit(e.KeyChar)) && e.KeyChar != Convert.ToChar(Keys.Back)) e.Handled = true;
        }

        private void toolStripTextBox4_TextChanged(object sender, EventArgs e)
        {
            float a1 = 0;
            if (float.Parse(toolStripTextBox4.Text) > 255 || float.Parse(toolStripTextBox4.Text)>-1)
            { try { a1 = float.Parse(toolStripTextBox4.Text); } catch { a1 = 0; toolStripTextBox4.Text = "0"; } }
            else a1 = 0;
            this.LinearSpace = a1 > -1 ? a1 : 0;
            toolStripTextBox4.Text = a1 > -1 ? a1.ToString() : "0";
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {

        }

        private void waveformPainter1_Click(object sender, EventArgs e)
        {

        }
    }
}
