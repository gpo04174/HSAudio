﻿namespace HS_Audio.Forms
{
    partial class frmPeakMeter1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
            if(ProgressAnimate!=null)ProgressAnimate.Abort();
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.설정값ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.열기OToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.저장SToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.peak새로고침ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator6 = new System.Windows.Forms.ToolStripSeparator();
            this.맨위에표시ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.타이틀표시ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.테두리표시ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator8 = new System.Windows.Forms.ToolStripSeparator();
            this.배경투명ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.분할자투명ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripTextBox2 = new System.Windows.Forms.ToolStripTextBox();
            this.toolStripSeparator7 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.dB레벨레이블ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.품질우선ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.음수부호표시ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dB문자표시ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dB문자공백ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dB굵게ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dB문자공간ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripComboBox1 = new System.Windows.Forms.ToolStripComboBox();
            this.dB테두리ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.글꼴설정ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator11 = new System.Windows.Forms.ToolStripSeparator();
            this.dB투명하게ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dB글자색변경ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.레벨레이블ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.레벨레이블보이기ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.굵게ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.테두리ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator12 = new System.Windows.Forms.ToolStripSeparator();
            this.색깔적용ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.글자색변경ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator9 = new System.Windows.Forms.ToolStripSeparator();
            this.사이즈맞추기ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.설정창띄우기좌ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.설정창띄우기우ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.분할자색상ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.스펙트럼할당방법ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.fMODSystemToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.fMODChannelToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.가로배열ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.그래프가로배열ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.분할자세로배열ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.가로미터ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.세로미터ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.눈금크기ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripTextBox();
            this.측정속도ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripTextBox1 = new System.Windows.Forms.ToolStripTextBox();
            this.측정크기ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripTextBox3 = new System.Windows.Forms.ToolStripTextBox();
            this.toolStripSeparator10 = new System.Windows.Forms.ToolStripSeparator();
            this.그래프부드럽게ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.그래프어택타임ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripTextBox5 = new System.Windows.Forms.ToolStripTextBox();
            this.그래프릴리즈시간ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripTextBox4 = new System.Windows.Forms.ToolStripTextBox();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.닫기ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.lbl_LeftPeakValue = new HS_Audio.Control.HSCustomLable();
            this.lbl_LeftPeak = new HS_Audio.Control.HSCustomLable();
            this.progressBarGradient1 = new HS_Audio.Control.ProgressBarGradientInternal();
            this.verticalLabel1 = new HS_Audio.Control.VerticalLabel();
            this.lbl_RightPeakValue = new HS_Audio.Control.HSCustomLable();
            this.lbl_RightPeak = new HS_Audio.Control.HSCustomLable();
            this.progressBarGradient2 = new HS_Audio.Control.ProgressBarGradientInternal();
            this.colorDialog1 = new System.Windows.Forms.ColorDialog();
            this.colorDialog2 = new System.Windows.Forms.ColorDialog();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.fontDialog1 = new System.Windows.Forms.FontDialog();
            this.colorDialog3 = new System.Windows.Forms.ColorDialog();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.contextMenuStrip1.SuspendLayout();
            this.progressBarGradient1.SuspendLayout();
            this.SuspendLayout();
            // 
            // timer1
            // 
            this.timer1.Interval = 25;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // splitContainer1
            // 
            this.splitContainer1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.splitContainer1.ContextMenuStrip = this.contextMenuStrip1;
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.lbl_LeftPeakValue);
            this.splitContainer1.Panel1.Controls.Add(this.lbl_LeftPeak);
            this.splitContainer1.Panel1.Controls.Add(this.progressBarGradient1);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.lbl_RightPeakValue);
            this.splitContainer1.Panel2.Controls.Add(this.lbl_RightPeak);
            this.splitContainer1.Panel2.Controls.Add(this.progressBarGradient2);
            this.splitContainer1.Size = new System.Drawing.Size(671, 84);
            this.splitContainer1.SplitterDistance = 40;
            this.splitContainer1.SplitterWidth = 3;
            this.splitContainer1.TabIndex = 1;
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.설정값ToolStripMenuItem,
            this.peak새로고침ToolStripMenuItem,
            this.toolStripSeparator6,
            this.맨위에표시ToolStripMenuItem,
            this.타이틀표시ToolStripMenuItem,
            this.테두리표시ToolStripMenuItem,
            this.toolStripSeparator8,
            this.배경투명ToolStripMenuItem,
            this.분할자투명ToolStripMenuItem,
            this.toolStripSeparator7,
            this.toolStripMenuItem1,
            this.레벨레이블ToolStripMenuItem,
            this.toolStripSeparator2,
            this.설정창띄우기좌ToolStripMenuItem,
            this.설정창띄우기우ToolStripMenuItem,
            this.분할자색상ToolStripMenuItem,
            this.toolStripSeparator1,
            this.스펙트럼할당방법ToolStripMenuItem,
            this.가로배열ToolStripMenuItem,
            this.toolStripSeparator3,
            this.눈금크기ToolStripMenuItem,
            this.측정속도ToolStripMenuItem,
            this.측정크기ToolStripMenuItem,
            this.toolStripSeparator10,
            this.그래프부드럽게ToolStripMenuItem,
            this.그래프어택타임ToolStripMenuItem,
            this.그래프릴리즈시간ToolStripMenuItem,
            this.toolStripSeparator4,
            this.닫기ToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(203, 514);
            // 
            // 설정값ToolStripMenuItem
            // 
            this.설정값ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.열기OToolStripMenuItem,
            this.저장SToolStripMenuItem});
            this.설정값ToolStripMenuItem.Name = "설정값ToolStripMenuItem";
            this.설정값ToolStripMenuItem.Size = new System.Drawing.Size(202, 22);
            this.설정값ToolStripMenuItem.Text = "설정 값";
            // 
            // 열기OToolStripMenuItem
            // 
            this.열기OToolStripMenuItem.Name = "열기OToolStripMenuItem";
            this.열기OToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.O)));
            this.열기OToolStripMenuItem.Size = new System.Drawing.Size(158, 22);
            this.열기OToolStripMenuItem.Text = "열기(&O)";
            this.열기OToolStripMenuItem.Click += new System.EventHandler(this.열기OToolStripMenuItem_Click);
            // 
            // 저장SToolStripMenuItem
            // 
            this.저장SToolStripMenuItem.Name = "저장SToolStripMenuItem";
            this.저장SToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
            this.저장SToolStripMenuItem.Size = new System.Drawing.Size(158, 22);
            this.저장SToolStripMenuItem.Text = "저장(&S)";
            this.저장SToolStripMenuItem.Click += new System.EventHandler(this.저장SToolStripMenuItem_Click);
            // 
            // peak새로고침ToolStripMenuItem
            // 
            this.peak새로고침ToolStripMenuItem.Name = "peak새로고침ToolStripMenuItem";
            this.peak새로고침ToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F5;
            this.peak새로고침ToolStripMenuItem.Size = new System.Drawing.Size(202, 22);
            this.peak새로고침ToolStripMenuItem.Text = "Peak 새로고침";
            this.peak새로고침ToolStripMenuItem.ToolTipText = "최대 Peak값을 새로고침 합니다.";
            this.peak새로고침ToolStripMenuItem.Click += new System.EventHandler(this.peak새로고침ToolStripMenuItem_Click);
            // 
            // toolStripSeparator6
            // 
            this.toolStripSeparator6.Name = "toolStripSeparator6";
            this.toolStripSeparator6.Size = new System.Drawing.Size(199, 6);
            // 
            // 맨위에표시ToolStripMenuItem
            // 
            this.맨위에표시ToolStripMenuItem.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.맨위에표시ToolStripMenuItem.CheckOnClick = true;
            this.맨위에표시ToolStripMenuItem.Name = "맨위에표시ToolStripMenuItem";
            this.맨위에표시ToolStripMenuItem.Size = new System.Drawing.Size(202, 22);
            this.맨위에표시ToolStripMenuItem.Text = "맨 위에 표시";
            this.맨위에표시ToolStripMenuItem.ToolTipText = "이 창을 맨위로 설정합니다.";
            this.맨위에표시ToolStripMenuItem.CheckedChanged += new System.EventHandler(this.맨위에표시ToolStripMenuItem_CheckedChanged);
            // 
            // 타이틀표시ToolStripMenuItem
            // 
            this.타이틀표시ToolStripMenuItem.Checked = true;
            this.타이틀표시ToolStripMenuItem.CheckOnClick = true;
            this.타이틀표시ToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.타이틀표시ToolStripMenuItem.Name = "타이틀표시ToolStripMenuItem";
            this.타이틀표시ToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F3;
            this.타이틀표시ToolStripMenuItem.Size = new System.Drawing.Size(202, 22);
            this.타이틀표시ToolStripMenuItem.Text = "타이틀 표시";
            this.타이틀표시ToolStripMenuItem.CheckedChanged += new System.EventHandler(this.타이틀표시ToolStripMenuItem_CheckedChanged);
            // 
            // 테두리표시ToolStripMenuItem
            // 
            this.테두리표시ToolStripMenuItem.Checked = true;
            this.테두리표시ToolStripMenuItem.CheckOnClick = true;
            this.테두리표시ToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.테두리표시ToolStripMenuItem.Name = "테두리표시ToolStripMenuItem";
            this.테두리표시ToolStripMenuItem.Size = new System.Drawing.Size(202, 22);
            this.테두리표시ToolStripMenuItem.Text = "테두리 표시";
            this.테두리표시ToolStripMenuItem.CheckedChanged += new System.EventHandler(this.테두리표시ToolStripMenuItem_CheckedChanged);
            // 
            // toolStripSeparator8
            // 
            this.toolStripSeparator8.Name = "toolStripSeparator8";
            this.toolStripSeparator8.Size = new System.Drawing.Size(199, 6);
            // 
            // 배경투명ToolStripMenuItem
            // 
            this.배경투명ToolStripMenuItem.Name = "배경투명ToolStripMenuItem";
            this.배경투명ToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F4;
            this.배경투명ToolStripMenuItem.Size = new System.Drawing.Size(202, 22);
            this.배경투명ToolStripMenuItem.Text = "배경 투명";
            this.배경투명ToolStripMenuItem.ToolTipText = "백그라운드 배경을 투명 처리합니다.\r\n[단축키: F4]";
            this.배경투명ToolStripMenuItem.Click += new System.EventHandler(this.배경투명ToolStripMenuItem_Click);
            // 
            // 분할자투명ToolStripMenuItem
            // 
            this.분할자투명ToolStripMenuItem.CheckOnClick = true;
            this.분할자투명ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripTextBox2});
            this.분할자투명ToolStripMenuItem.Name = "분할자투명ToolStripMenuItem";
            this.분할자투명ToolStripMenuItem.Size = new System.Drawing.Size(202, 22);
            this.분할자투명ToolStripMenuItem.Text = "└ 분할자 투명";
            this.분할자투명ToolStripMenuItem.Click += new System.EventHandler(this.분할자투명ToolStripMenuItem_Click);
            // 
            // toolStripTextBox2
            // 
            this.toolStripTextBox2.Enabled = false;
            this.toolStripTextBox2.MaxLength = 9;
            this.toolStripTextBox2.Name = "toolStripTextBox2";
            this.toolStripTextBox2.Size = new System.Drawing.Size(100, 23);
            this.toolStripTextBox2.Text = "#FF010101";
            this.toolStripTextBox2.ToolTipText = "투명화할 색깔을 설정합니다.";
            this.toolStripTextBox2.Visible = false;
            this.toolStripTextBox2.TextChanged += new System.EventHandler(this.toolStripTextBox2_TextChanged);
            // 
            // toolStripSeparator7
            // 
            this.toolStripSeparator7.Name = "toolStripSeparator7";
            this.toolStripSeparator7.Size = new System.Drawing.Size(199, 6);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.dB레벨레이블ToolStripMenuItem,
            this.품질우선ToolStripMenuItem,
            this.음수부호표시ToolStripMenuItem,
            this.dB문자표시ToolStripMenuItem,
            this.dB문자공백ToolStripMenuItem,
            this.dB굵게ToolStripMenuItem,
            this.dB문자공간ToolStripMenuItem,
            this.dB테두리ToolStripMenuItem,
            this.글꼴설정ToolStripMenuItem,
            this.toolStripSeparator11,
            this.dB투명하게ToolStripMenuItem,
            this.dB글자색변경ToolStripMenuItem});
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(202, 22);
            this.toolStripMenuItem1.Text = "dB 레벨 레이블";
            // 
            // dB레벨레이블ToolStripMenuItem
            // 
            this.dB레벨레이블ToolStripMenuItem.Checked = true;
            this.dB레벨레이블ToolStripMenuItem.CheckOnClick = true;
            this.dB레벨레이블ToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.dB레벨레이블ToolStripMenuItem.Name = "dB레벨레이블ToolStripMenuItem";
            this.dB레벨레이블ToolStripMenuItem.Size = new System.Drawing.Size(214, 22);
            this.dB레벨레이블ToolStripMenuItem.Text = "dB 레벨 레이블 보이기";
            this.dB레벨레이블ToolStripMenuItem.CheckedChanged += new System.EventHandler(this.dB레벨레이블ToolStripMenuItem_CheckedChanged);
            // 
            // 품질우선ToolStripMenuItem
            // 
            this.품질우선ToolStripMenuItem.Checked = true;
            this.품질우선ToolStripMenuItem.CheckOnClick = true;
            this.품질우선ToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.품질우선ToolStripMenuItem.Name = "품질우선ToolStripMenuItem";
            this.품질우선ToolStripMenuItem.Size = new System.Drawing.Size(214, 22);
            this.품질우선ToolStripMenuItem.Text = "└ 품질 우선";
            this.품질우선ToolStripMenuItem.ToolTipText = "체크하면 깜빡임이 줄어드는대신 성능 감소\r\n테크 해제하면 깜빡임이 증가하는대신 성능 향상";
            this.품질우선ToolStripMenuItem.CheckedChanged += new System.EventHandler(this.품질우선ToolStripMenuItem_CheckedChanged);
            // 
            // 음수부호표시ToolStripMenuItem
            // 
            this.음수부호표시ToolStripMenuItem.Checked = true;
            this.음수부호표시ToolStripMenuItem.CheckOnClick = true;
            this.음수부호표시ToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.음수부호표시ToolStripMenuItem.Name = "음수부호표시ToolStripMenuItem";
            this.음수부호표시ToolStripMenuItem.Size = new System.Drawing.Size(214, 22);
            this.음수부호표시ToolStripMenuItem.Text = "└ 음수 부호(-) 표시";
            this.음수부호표시ToolStripMenuItem.CheckedChanged += new System.EventHandler(this.음수부호표시ToolStripMenuItem_CheckedChanged);
            // 
            // dB문자표시ToolStripMenuItem
            // 
            this.dB문자표시ToolStripMenuItem.CheckOnClick = true;
            this.dB문자표시ToolStripMenuItem.Name = "dB문자표시ToolStripMenuItem";
            this.dB문자표시ToolStripMenuItem.Size = new System.Drawing.Size(214, 22);
            this.dB문자표시ToolStripMenuItem.Text = "└ dB 문자 표시";
            this.dB문자표시ToolStripMenuItem.CheckedChanged += new System.EventHandler(this.dB문자표시ToolStripMenuItem_CheckedChanged);
            // 
            // dB문자공백ToolStripMenuItem
            // 
            this.dB문자공백ToolStripMenuItem.CheckOnClick = true;
            this.dB문자공백ToolStripMenuItem.Name = "dB문자공백ToolStripMenuItem";
            this.dB문자공백ToolStripMenuItem.Size = new System.Drawing.Size(214, 22);
            this.dB문자공백ToolStripMenuItem.Text = "   └ dB 문자 공백";
            this.dB문자공백ToolStripMenuItem.CheckedChanged += new System.EventHandler(this.dB문자공백ToolStripMenuItem_CheckedChanged);
            // 
            // dB굵게ToolStripMenuItem
            // 
            this.dB굵게ToolStripMenuItem.CheckOnClick = true;
            this.dB굵게ToolStripMenuItem.Name = "dB굵게ToolStripMenuItem";
            this.dB굵게ToolStripMenuItem.Size = new System.Drawing.Size(214, 22);
            this.dB굵게ToolStripMenuItem.Text = "└ 굵게";
            this.dB굵게ToolStripMenuItem.Visible = false;
            this.dB굵게ToolStripMenuItem.CheckedChanged += new System.EventHandler(this.dB굵게ToolStripMenuItem_CheckedChanged);
            // 
            // dB문자공간ToolStripMenuItem
            // 
            this.dB문자공간ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripComboBox1});
            this.dB문자공간ToolStripMenuItem.Name = "dB문자공간ToolStripMenuItem";
            this.dB문자공간ToolStripMenuItem.Size = new System.Drawing.Size(214, 22);
            this.dB문자공간ToolStripMenuItem.Text = "└ dB 공간";
            // 
            // toolStripComboBox1
            // 
            this.toolStripComboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.toolStripComboBox1.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "10"});
            this.toolStripComboBox1.Name = "toolStripComboBox1";
            this.toolStripComboBox1.Size = new System.Drawing.Size(121, 23);
            this.toolStripComboBox1.SelectedIndexChanged += new System.EventHandler(this.toolStripComboBox1_SelectedIndexChanged);
            // 
            // dB테두리ToolStripMenuItem
            // 
            this.dB테두리ToolStripMenuItem.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.dB테두리ToolStripMenuItem.CheckOnClick = true;
            this.dB테두리ToolStripMenuItem.Name = "dB테두리ToolStripMenuItem";
            this.dB테두리ToolStripMenuItem.Size = new System.Drawing.Size(214, 22);
            this.dB테두리ToolStripMenuItem.Text = "└ 테두리";
            this.dB테두리ToolStripMenuItem.Visible = false;
            // 
            // 글꼴설정ToolStripMenuItem
            // 
            this.글꼴설정ToolStripMenuItem.Name = "글꼴설정ToolStripMenuItem";
            this.글꼴설정ToolStripMenuItem.Size = new System.Drawing.Size(214, 22);
            this.글꼴설정ToolStripMenuItem.Text = "└ 글꼴 설정";
            this.글꼴설정ToolStripMenuItem.Click += new System.EventHandler(this.글꼴설정ToolStripMenuItem_Click);
            // 
            // toolStripSeparator11
            // 
            this.toolStripSeparator11.Name = "toolStripSeparator11";
            this.toolStripSeparator11.Size = new System.Drawing.Size(211, 6);
            // 
            // dB투명하게ToolStripMenuItem
            // 
            this.dB투명하게ToolStripMenuItem.Checked = true;
            this.dB투명하게ToolStripMenuItem.CheckOnClick = true;
            this.dB투명하게ToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.dB투명하게ToolStripMenuItem.Name = "dB투명하게ToolStripMenuItem";
            this.dB투명하게ToolStripMenuItem.Size = new System.Drawing.Size(214, 22);
            this.dB투명하게ToolStripMenuItem.Text = "└ 투명하게 (글자색 무시)";
            this.dB투명하게ToolStripMenuItem.Visible = false;
            // 
            // dB글자색변경ToolStripMenuItem
            // 
            this.dB글자색변경ToolStripMenuItem.Name = "dB글자색변경ToolStripMenuItem";
            this.dB글자색변경ToolStripMenuItem.Size = new System.Drawing.Size(214, 22);
            this.dB글자색변경ToolStripMenuItem.Text = "   └ 글자색 변경";
            this.dB글자색변경ToolStripMenuItem.Click += new System.EventHandler(this.dB글자색변경ToolStripMenuItem_Click);
            // 
            // 레벨레이블ToolStripMenuItem
            // 
            this.레벨레이블ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.레벨레이블보이기ToolStripMenuItem,
            this.굵게ToolStripMenuItem,
            this.테두리ToolStripMenuItem,
            this.toolStripSeparator12,
            this.색깔적용ToolStripMenuItem,
            this.글자색변경ToolStripMenuItem,
            this.toolStripSeparator9,
            this.사이즈맞추기ToolStripMenuItem});
            this.레벨레이블ToolStripMenuItem.Name = "레벨레이블ToolStripMenuItem";
            this.레벨레이블ToolStripMenuItem.Size = new System.Drawing.Size(202, 22);
            this.레벨레이블ToolStripMenuItem.Text = "Peak 레벨 레이블";
            // 
            // 레벨레이블보이기ToolStripMenuItem
            // 
            this.레벨레이블보이기ToolStripMenuItem.CheckOnClick = true;
            this.레벨레이블보이기ToolStripMenuItem.Name = "레벨레이블보이기ToolStripMenuItem";
            this.레벨레이블보이기ToolStripMenuItem.Size = new System.Drawing.Size(207, 22);
            this.레벨레이블보이기ToolStripMenuItem.Text = "Peak 레벨 레이블 보이기";
            this.레벨레이블보이기ToolStripMenuItem.CheckedChanged += new System.EventHandler(this.레벨레이블보이기ToolStripMenuItem_CheckedChanged);
            // 
            // 굵게ToolStripMenuItem
            // 
            this.굵게ToolStripMenuItem.CheckOnClick = true;
            this.굵게ToolStripMenuItem.Name = "굵게ToolStripMenuItem";
            this.굵게ToolStripMenuItem.Size = new System.Drawing.Size(207, 22);
            this.굵게ToolStripMenuItem.Text = "└ 굵게";
            this.굵게ToolStripMenuItem.CheckedChanged += new System.EventHandler(this.굵게ToolStripMenuItem_CheckedChanged);
            // 
            // 테두리ToolStripMenuItem
            // 
            this.테두리ToolStripMenuItem.Checked = true;
            this.테두리ToolStripMenuItem.CheckOnClick = true;
            this.테두리ToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.테두리ToolStripMenuItem.Name = "테두리ToolStripMenuItem";
            this.테두리ToolStripMenuItem.Size = new System.Drawing.Size(207, 22);
            this.테두리ToolStripMenuItem.Text = "└ 테두리";
            this.테두리ToolStripMenuItem.CheckedChanged += new System.EventHandler(this.테두리ToolStripMenuItem_CheckedChanged);
            // 
            // toolStripSeparator12
            // 
            this.toolStripSeparator12.Name = "toolStripSeparator12";
            this.toolStripSeparator12.Size = new System.Drawing.Size(204, 6);
            // 
            // 색깔적용ToolStripMenuItem
            // 
            this.색깔적용ToolStripMenuItem.Checked = true;
            this.색깔적용ToolStripMenuItem.CheckOnClick = true;
            this.색깔적용ToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.색깔적용ToolStripMenuItem.Name = "색깔적용ToolStripMenuItem";
            this.색깔적용ToolStripMenuItem.Size = new System.Drawing.Size(207, 22);
            this.색깔적용ToolStripMenuItem.Text = "└ 색깔 적용";
            // 
            // 글자색변경ToolStripMenuItem
            // 
            this.글자색변경ToolStripMenuItem.Name = "글자색변경ToolStripMenuItem";
            this.글자색변경ToolStripMenuItem.Size = new System.Drawing.Size(207, 22);
            this.글자색변경ToolStripMenuItem.Text = "   └ 글자색 변경";
            this.글자색변경ToolStripMenuItem.CheckedChanged += new System.EventHandler(this.글자색변경ToolStripMenuItem_CheckedChanged);
            // 
            // toolStripSeparator9
            // 
            this.toolStripSeparator9.Name = "toolStripSeparator9";
            this.toolStripSeparator9.Size = new System.Drawing.Size(204, 6);
            // 
            // 사이즈맞추기ToolStripMenuItem
            // 
            this.사이즈맞추기ToolStripMenuItem.Name = "사이즈맞추기ToolStripMenuItem";
            this.사이즈맞추기ToolStripMenuItem.Size = new System.Drawing.Size(207, 22);
            this.사이즈맞추기ToolStripMenuItem.Text = "사이즈 맞추기";
            this.사이즈맞추기ToolStripMenuItem.Click += new System.EventHandler(this.사이즈맞추기ToolStripMenuItem_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(199, 6);
            // 
            // 설정창띄우기좌ToolStripMenuItem
            // 
            this.설정창띄우기좌ToolStripMenuItem.Name = "설정창띄우기좌ToolStripMenuItem";
            this.설정창띄우기좌ToolStripMenuItem.Size = new System.Drawing.Size(202, 22);
            this.설정창띄우기좌ToolStripMenuItem.Text = "고급 설정창 띄우기 (좌)";
            this.설정창띄우기좌ToolStripMenuItem.Visible = false;
            // 
            // 설정창띄우기우ToolStripMenuItem
            // 
            this.설정창띄우기우ToolStripMenuItem.Name = "설정창띄우기우ToolStripMenuItem";
            this.설정창띄우기우ToolStripMenuItem.Size = new System.Drawing.Size(202, 22);
            this.설정창띄우기우ToolStripMenuItem.Text = "고급 설정창 띄우기 (우)";
            this.설정창띄우기우ToolStripMenuItem.Visible = false;
            // 
            // 분할자색상ToolStripMenuItem
            // 
            this.분할자색상ToolStripMenuItem.Name = "분할자색상ToolStripMenuItem";
            this.분할자색상ToolStripMenuItem.Size = new System.Drawing.Size(202, 22);
            this.분할자색상ToolStripMenuItem.Text = "분할자 색상";
            this.분할자색상ToolStripMenuItem.Click += new System.EventHandler(this.분할자색상ToolStripMenuItem_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(199, 6);
            // 
            // 스펙트럼할당방법ToolStripMenuItem
            // 
            this.스펙트럼할당방법ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fMODSystemToolStripMenuItem,
            this.fMODChannelToolStripMenuItem});
            this.스펙트럼할당방법ToolStripMenuItem.Name = "스펙트럼할당방법ToolStripMenuItem";
            this.스펙트럼할당방법ToolStripMenuItem.Size = new System.Drawing.Size(202, 22);
            this.스펙트럼할당방법ToolStripMenuItem.Text = "그래프 할당 방법";
            // 
            // fMODSystemToolStripMenuItem
            // 
            this.fMODSystemToolStripMenuItem.Checked = true;
            this.fMODSystemToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.fMODSystemToolStripMenuItem.Name = "fMODSystemToolStripMenuItem";
            this.fMODSystemToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F2;
            this.fMODSystemToolStripMenuItem.Size = new System.Drawing.Size(163, 22);
            this.fMODSystemToolStripMenuItem.Text = "Main Output";
            this.fMODSystemToolStripMenuItem.ToolTipText = "마스터 출력 입니다.";
            this.fMODSystemToolStripMenuItem.Click += new System.EventHandler(this.fMODSystemToolStripMenuItem_Click);
            // 
            // fMODChannelToolStripMenuItem
            // 
            this.fMODChannelToolStripMenuItem.Name = "fMODChannelToolStripMenuItem";
            this.fMODChannelToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F3;
            this.fMODChannelToolStripMenuItem.Size = new System.Drawing.Size(163, 22);
            this.fMODChannelToolStripMenuItem.Text = "Channel";
            this.fMODChannelToolStripMenuItem.ToolTipText = "개별 출력 입니다.";
            this.fMODChannelToolStripMenuItem.Click += new System.EventHandler(this.fMODChannelToolStripMenuItem_Click);
            // 
            // 가로배열ToolStripMenuItem
            // 
            this.가로배열ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.그래프가로배열ToolStripMenuItem,
            this.분할자세로배열ToolStripMenuItem,
            this.toolStripSeparator5,
            this.가로미터ToolStripMenuItem,
            this.세로미터ToolStripMenuItem});
            this.가로배열ToolStripMenuItem.Name = "가로배열ToolStripMenuItem";
            this.가로배열ToolStripMenuItem.Size = new System.Drawing.Size(202, 22);
            this.가로배열ToolStripMenuItem.Text = "배열 스타일";
            // 
            // 그래프가로배열ToolStripMenuItem
            // 
            this.그래프가로배열ToolStripMenuItem.Checked = true;
            this.그래프가로배열ToolStripMenuItem.CheckOnClick = true;
            this.그래프가로배열ToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.그래프가로배열ToolStripMenuItem.Name = "그래프가로배열ToolStripMenuItem";
            this.그래프가로배열ToolStripMenuItem.Size = new System.Drawing.Size(166, 22);
            this.그래프가로배열ToolStripMenuItem.Text = "그래프 가로 배열";
            this.그래프가로배열ToolStripMenuItem.CheckedChanged += new System.EventHandler(this.그래프가로배열ToolStripMenuItem_CheckedChanged);
            // 
            // 분할자세로배열ToolStripMenuItem
            // 
            this.분할자세로배열ToolStripMenuItem.Checked = true;
            this.분할자세로배열ToolStripMenuItem.CheckOnClick = true;
            this.분할자세로배열ToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.분할자세로배열ToolStripMenuItem.Name = "분할자세로배열ToolStripMenuItem";
            this.분할자세로배열ToolStripMenuItem.Size = new System.Drawing.Size(166, 22);
            this.분할자세로배열ToolStripMenuItem.Text = "분할자 가로 배열";
            this.분할자세로배열ToolStripMenuItem.CheckedChanged += new System.EventHandler(this.분할자세로배열ToolStripMenuItem_CheckedChanged);
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(163, 6);
            // 
            // 가로미터ToolStripMenuItem
            // 
            this.가로미터ToolStripMenuItem.Name = "가로미터ToolStripMenuItem";
            this.가로미터ToolStripMenuItem.Size = new System.Drawing.Size(166, 22);
            this.가로미터ToolStripMenuItem.Text = "가로 미터";
            this.가로미터ToolStripMenuItem.Click += new System.EventHandler(this.가로미터ToolStripMenuItem_Click);
            // 
            // 세로미터ToolStripMenuItem
            // 
            this.세로미터ToolStripMenuItem.Name = "세로미터ToolStripMenuItem";
            this.세로미터ToolStripMenuItem.Size = new System.Drawing.Size(166, 22);
            this.세로미터ToolStripMenuItem.Text = "세로 미터";
            this.세로미터ToolStripMenuItem.Click += new System.EventHandler(this.세로미터ToolStripMenuItem_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(199, 6);
            // 
            // 눈금크기ToolStripMenuItem
            // 
            this.눈금크기ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem2});
            this.눈금크기ToolStripMenuItem.Name = "눈금크기ToolStripMenuItem";
            this.눈금크기ToolStripMenuItem.Size = new System.Drawing.Size(202, 22);
            this.눈금크기ToolStripMenuItem.Text = "측정 범위 (눈금 갯수)";
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(152, 23);
            this.toolStripMenuItem2.Text = "50";
            this.toolStripMenuItem2.ToolTipText = "1칸은 -1dB 를 나타냅니다.\r\n(만약 30이면 -30dB 부터 0dB까지 측정 한다는 것입니다.)\r\n(값의 범위는 1 ~ 360 입니다.)";
            this.toolStripMenuItem2.TextChanged += new System.EventHandler(this.toolStripMenuItem2_TextChanged);
            // 
            // 측정속도ToolStripMenuItem
            // 
            this.측정속도ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripTextBox1});
            this.측정속도ToolStripMenuItem.Name = "측정속도ToolStripMenuItem";
            this.측정속도ToolStripMenuItem.Size = new System.Drawing.Size(202, 22);
            this.측정속도ToolStripMenuItem.Text = "측정 속도";
            this.측정속도ToolStripMenuItem.ToolTipText = "측정할 속도를 설정합니다. (밀리세컨트 단위 입니다.)";
            this.측정속도ToolStripMenuItem.TextChanged += new System.EventHandler(this.측정속도ToolStripMenuItem_TextChanged);
            // 
            // toolStripTextBox1
            // 
            this.toolStripTextBox1.MaxLength = 5;
            this.toolStripTextBox1.Name = "toolStripTextBox1";
            this.toolStripTextBox1.Size = new System.Drawing.Size(100, 23);
            this.toolStripTextBox1.Text = "25";
            this.toolStripTextBox1.ToolTipText = "측정할 밀리세컨드 (ms) 입니다.";
            this.toolStripTextBox1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.toolStripTextBox1_KeyPress);
            this.toolStripTextBox1.TextChanged += new System.EventHandler(this.toolStripTextBox1_TextChanged);
            // 
            // 측정크기ToolStripMenuItem
            // 
            this.측정크기ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripTextBox3});
            this.측정크기ToolStripMenuItem.Name = "측정크기ToolStripMenuItem";
            this.측정크기ToolStripMenuItem.Size = new System.Drawing.Size(202, 22);
            this.측정크기ToolStripMenuItem.Text = "측정 버퍼";
            this.측정크기ToolStripMenuItem.ToolTipText = "Peak를 측정할 버퍼(샘플수) 입니다. \r\n이 값을 올릴수록 측정이 정확해 지지만 그래프의 변동은 작습니다.\r\n1~16384 사이의 값을 넣어 " +
    "주세요 (기본 2048)";
            // 
            // toolStripTextBox3
            // 
            this.toolStripTextBox3.Name = "toolStripTextBox3";
            this.toolStripTextBox3.Size = new System.Drawing.Size(100, 23);
            this.toolStripTextBox3.Text = "2048";
            this.toolStripTextBox3.ToolTipText = "Peak를 측정할 버퍼(샘플수) 입니다. \r\n이 값을 올릴수록 측정이 정확해 지지만 그래프의 변동은 작습니다.\r\n1~16384 사이의 값을 넣어 " +
    "주세요 (기본 2048)";
            this.toolStripTextBox3.TextChanged += new System.EventHandler(this.toolStripTextBox3_TextChanged);
            // 
            // toolStripSeparator10
            // 
            this.toolStripSeparator10.Name = "toolStripSeparator10";
            this.toolStripSeparator10.Size = new System.Drawing.Size(199, 6);
            // 
            // 그래프부드럽게ToolStripMenuItem
            // 
            this.그래프부드럽게ToolStripMenuItem.Checked = true;
            this.그래프부드럽게ToolStripMenuItem.CheckOnClick = true;
            this.그래프부드럽게ToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.그래프부드럽게ToolStripMenuItem.Name = "그래프부드럽게ToolStripMenuItem";
            this.그래프부드럽게ToolStripMenuItem.Size = new System.Drawing.Size(202, 22);
            this.그래프부드럽게ToolStripMenuItem.Text = "그래프 부드럽게";
            this.그래프부드럽게ToolStripMenuItem.CheckedChanged += new System.EventHandler(this.그래프부드럽게ToolStripMenuItem_CheckedChanged);
            // 
            // 그래프어택타임ToolStripMenuItem
            // 
            this.그래프어택타임ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripTextBox5});
            this.그래프어택타임ToolStripMenuItem.Name = "그래프어택타임ToolStripMenuItem";
            this.그래프어택타임ToolStripMenuItem.Size = new System.Drawing.Size(202, 22);
            this.그래프어택타임ToolStripMenuItem.Text = "└ 그래프 어택 타임";
            // 
            // toolStripTextBox5
            // 
            this.toolStripTextBox5.Name = "toolStripTextBox5";
            this.toolStripTextBox5.Size = new System.Drawing.Size(100, 23);
            this.toolStripTextBox5.Text = "10";
            this.toolStripTextBox5.ToolTipText = "그래프가 릴리즈후 올라가기까지의 시간 입니다.\r\n측정 시간이랑 똑같습니다.\r\n(밀리세컨트(ms) 1000/1 초 / 기본값: 10ms)";
            this.toolStripTextBox5.TextChanged += new System.EventHandler(this.toolStripTextBox5_TextChanged);
            // 
            // 그래프릴리즈시간ToolStripMenuItem
            // 
            this.그래프릴리즈시간ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripTextBox4});
            this.그래프릴리즈시간ToolStripMenuItem.Name = "그래프릴리즈시간ToolStripMenuItem";
            this.그래프릴리즈시간ToolStripMenuItem.Size = new System.Drawing.Size(202, 22);
            this.그래프릴리즈시간ToolStripMenuItem.Text = "└ 그래프 릴리즈 시간";
            // 
            // toolStripTextBox4
            // 
            this.toolStripTextBox4.Name = "toolStripTextBox4";
            this.toolStripTextBox4.Size = new System.Drawing.Size(100, 23);
            this.toolStripTextBox4.Text = "60";
            this.toolStripTextBox4.ToolTipText = "그래프가 떨어지기까지의 시간 입니다.\r\n(밀리세컨트(ms) 1000/1 초 / 기본값: 50ms)";
            this.toolStripTextBox4.TextChanged += new System.EventHandler(this.toolStripTextBox4_TextChanged);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(199, 6);
            // 
            // 닫기ToolStripMenuItem
            // 
            this.닫기ToolStripMenuItem.Name = "닫기ToolStripMenuItem";
            this.닫기ToolStripMenuItem.Size = new System.Drawing.Size(202, 22);
            this.닫기ToolStripMenuItem.Text = "닫기";
            this.닫기ToolStripMenuItem.Click += new System.EventHandler(this.닫기ToolStripMenuItem_Click);
            // 
            // lbl_LeftPeakValue
            // 
            this.lbl_LeftPeakValue.AutoSize = true;
            this.lbl_LeftPeakValue.BackColor = System.Drawing.Color.Yellow;
            this.lbl_LeftPeakValue.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl_LeftPeakValue.ContextMenuStrip = this.contextMenuStrip1;
            this.lbl_LeftPeakValue.Dock = System.Windows.Forms.DockStyle.Top;
            this.lbl_LeftPeakValue.Location = new System.Drawing.Point(0, 14);
            this.lbl_LeftPeakValue.Name = "lbl_LeftPeakValue";
            this.lbl_LeftPeakValue.NewText = null;
            this.lbl_LeftPeakValue.RotateAngle = 0;
            this.lbl_LeftPeakValue.Size = new System.Drawing.Size(30, 14);
            this.lbl_LeftPeakValue.TabIndex = 4;
            this.lbl_LeftPeakValue.Text = "-Inf.";
            this.lbl_LeftPeakValue.Visible = false;
            // 
            // lbl_LeftPeak
            // 
            this.lbl_LeftPeak.AutoSize = true;
            this.lbl_LeftPeak.BackColor = System.Drawing.Color.Yellow;
            this.lbl_LeftPeak.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl_LeftPeak.ContextMenuStrip = this.contextMenuStrip1;
            this.lbl_LeftPeak.Dock = System.Windows.Forms.DockStyle.Top;
            this.lbl_LeftPeak.Location = new System.Drawing.Point(0, 0);
            this.lbl_LeftPeak.Name = "lbl_LeftPeak";
            this.lbl_LeftPeak.NewText = "-Inf.";
            this.lbl_LeftPeak.RotateAngle = 0;
            this.lbl_LeftPeak.Size = new System.Drawing.Size(30, 14);
            this.lbl_LeftPeak.TabIndex = 3;
            this.lbl_LeftPeak.Text = "-Inf.";
            this.lbl_LeftPeak.Visible = false;
            this.lbl_LeftPeak.Click += new System.EventHandler(this.lbl_LeftPeak_Click);
            // 
            // progressBarGradient1
            // 
            this.progressBarGradient1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.progressBarGradient1.BackColor = System.Drawing.SystemColors.Control;
            this.progressBarGradient1.ContextMenuStrip = this.contextMenuStrip1;
            this.progressBarGradient1.Controls.Add(this.verticalLabel1);
            this.progressBarGradient1.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.progressBarGradient1.IntervaldB = 3;
            this.progressBarGradient1.Location = new System.Drawing.Point(0, 0);
            this.progressBarGradient1.MaximumColor = System.Drawing.Color.Crimson;
            this.progressBarGradient1.MaximumValue = 100000;
            this.progressBarGradient1.MidColor = System.Drawing.Color.Goldenrod;
            this.progressBarGradient1.MidColorPosition = 0.6F;
            this.progressBarGradient1.MinimumColor = System.Drawing.Color.Lime;
            this.progressBarGradient1.MinimumValue = 0;
            this.progressBarGradient1.MouseInteractive = false;
            this.progressBarGradient1.Name = "progressBarGradient1";
            this.progressBarGradient1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            this.progressBarGradient1.ShowMidColor = true;
            this.progressBarGradient1.ShowMinus = true;
            this.progressBarGradient1.Size = new System.Drawing.Size(669, 38);
            this.progressBarGradient1.TabIndex = 1;
            this.progressBarGradient1.Value = 50;
            this.progressBarGradient1.VisibledB = true;
            this.progressBarGradient1.MouseClick += new System.Windows.Forms.MouseEventHandler(this.progressBarGradient2_MouseClick);
            this.progressBarGradient1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.progressBarGradient1_MouseDown);
            this.progressBarGradient1.MouseLeave += new System.EventHandler(this.progressBarGradient1_MouseLeave);
            this.progressBarGradient1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.progressBarGradient1_MouseMove);
            this.progressBarGradient1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.progressBarGradient1_MouseUp);
            // 
            // verticalLabel1
            // 
            this.verticalLabel1.BackColor = System.Drawing.Color.Yellow;
            this.verticalLabel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.verticalLabel1.Location = new System.Drawing.Point(654, 0);
            this.verticalLabel1.Name = "verticalLabel1";
            this.verticalLabel1.RenderingMode = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.verticalLabel1.Size = new System.Drawing.Size(15, 39);
            this.verticalLabel1.TabIndex = 1;
            this.verticalLabel1.Text = "-Inf.";
            this.verticalLabel1.TextDrawMode = HS_Audio.Control.DrawMode.TopBottom;
            this.verticalLabel1.TransparentBackground = false;
            this.verticalLabel1.Visible = false;
            // 
            // lbl_RightPeakValue
            // 
            this.lbl_RightPeakValue.AutoSize = true;
            this.lbl_RightPeakValue.BackColor = System.Drawing.Color.Yellow;
            this.lbl_RightPeakValue.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl_RightPeakValue.ContextMenuStrip = this.contextMenuStrip1;
            this.lbl_RightPeakValue.Location = new System.Drawing.Point(0, 14);
            this.lbl_RightPeakValue.Name = "lbl_RightPeakValue";
            this.lbl_RightPeakValue.NewText = null;
            this.lbl_RightPeakValue.RotateAngle = 0;
            this.lbl_RightPeakValue.Size = new System.Drawing.Size(30, 14);
            this.lbl_RightPeakValue.TabIndex = 7;
            this.lbl_RightPeakValue.Text = "-Inf.";
            this.lbl_RightPeakValue.Visible = false;
            // 
            // lbl_RightPeak
            // 
            this.lbl_RightPeak.AutoSize = true;
            this.lbl_RightPeak.BackColor = System.Drawing.Color.Yellow;
            this.lbl_RightPeak.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl_RightPeak.ContextMenuStrip = this.contextMenuStrip1;
            this.lbl_RightPeak.Location = new System.Drawing.Point(0, 0);
            this.lbl_RightPeak.Name = "lbl_RightPeak";
            this.lbl_RightPeak.NewText = null;
            this.lbl_RightPeak.RotateAngle = 0;
            this.lbl_RightPeak.Size = new System.Drawing.Size(30, 14);
            this.lbl_RightPeak.TabIndex = 7;
            this.lbl_RightPeak.Text = "-Inf.";
            this.lbl_RightPeak.Visible = false;
            this.lbl_RightPeak.Click += new System.EventHandler(this.lbl_RightPeak_Click);
            // 
            // progressBarGradient2
            // 
            this.progressBarGradient2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.progressBarGradient2.ContextMenuStrip = this.contextMenuStrip1;
            this.progressBarGradient2.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.progressBarGradient2.IntervaldB = 3;
            this.progressBarGradient2.Location = new System.Drawing.Point(0, 0);
            this.progressBarGradient2.MaximumColor = System.Drawing.Color.Crimson;
            this.progressBarGradient2.MaximumValue = 100000;
            this.progressBarGradient2.MidColor = System.Drawing.Color.Goldenrod;
            this.progressBarGradient2.MidColorPosition = 0.6F;
            this.progressBarGradient2.MinimumColor = System.Drawing.Color.Lime;
            this.progressBarGradient2.MinimumValue = 0;
            this.progressBarGradient2.MouseInteractive = false;
            this.progressBarGradient2.Name = "progressBarGradient2";
            this.progressBarGradient2.Orientation = System.Windows.Forms.Orientation.Horizontal;
            this.progressBarGradient2.ShowMidColor = true;
            this.progressBarGradient2.ShowMinus = true;
            this.progressBarGradient2.Size = new System.Drawing.Size(669, 38);
            this.progressBarGradient2.TabIndex = 2;
            this.progressBarGradient2.Value = 0;
            this.progressBarGradient2.VisibledB = true;
            this.progressBarGradient2.MouseClick += new System.Windows.Forms.MouseEventHandler(this.progressBarGradient2_MouseClick);
            this.progressBarGradient2.MouseDown += new System.Windows.Forms.MouseEventHandler(this.progressBarGradient1_MouseDown);
            this.progressBarGradient2.MouseLeave += new System.EventHandler(this.progressBarGradient1_MouseLeave);
            this.progressBarGradient2.MouseMove += new System.Windows.Forms.MouseEventHandler(this.progressBarGradient1_MouseMove);
            this.progressBarGradient2.MouseUp += new System.Windows.Forms.MouseEventHandler(this.progressBarGradient1_MouseUp);
            // 
            // colorDialog1
            // 
            this.colorDialog1.AnyColor = true;
            this.colorDialog1.FullOpen = true;
            // 
            // colorDialog2
            // 
            this.colorDialog2.AnyColor = true;
            this.colorDialog2.FullOpen = true;
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.DefaultExt = "*.pkmtini";
            this.openFileDialog1.Filter = "Peak 미터 그래프 설정 파일 (*.pkmtini)|*.pkmtini|기본 설정 파일 (*.ini)|*.ini";
            this.openFileDialog1.FileOk += new System.ComponentModel.CancelEventHandler(this.openFileDialog1_FileOk);
            // 
            // saveFileDialog1
            // 
            this.saveFileDialog1.DefaultExt = "*.pkmtini";
            this.saveFileDialog1.Filter = "Peak 미터 그래프 설정 파일 (*.pkmtini)|*.pkmtini|기본 설정 파일 (*.ini)|*.ini";
            this.saveFileDialog1.FileOk += new System.ComponentModel.CancelEventHandler(this.saveFileDialog1_FileOk);
            // 
            // colorDialog3
            // 
            this.colorDialog3.AnyColor = true;
            this.colorDialog3.FullOpen = true;
            // 
            // frmPeakMeter1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(671, 84);
            this.Controls.Add(this.splitContainer1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
            this.Name = "frmPeakMeter1";
            this.Text = "frmPeakMeter1";
            this.WindowStateChanged += new HS_Audio.Control.HSCustomForm.WindowStateChangedEventHandler(this.frmPeakMeter_WindowShowChanged);
            this.WindowShowChanged += new HS_Audio.Control.WindowShowChangedEventHandler(this.frmPeakMeter_WindowShowChanged);
            this.Load += new System.EventHandler(this.frmPeakMeter1_Load);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.PerformLayout();
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.Panel2.PerformLayout();
            this.splitContainer1.ResumeLayout(false);
            this.contextMenuStrip1.ResumeLayout(false);
            this.progressBarGradient1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private Control.ProgressBarGradientInternal progressBarGradient1;
        private Control.ProgressBarGradientInternal progressBarGradient2;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem 설정값ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 열기OToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 저장SToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem peak새로고침ToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator6;
        private System.Windows.Forms.ToolStripMenuItem 맨위에표시ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 타이틀표시ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 테두리표시ToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator8;
        private System.Windows.Forms.ToolStripMenuItem 배경투명ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 분할자투명ToolStripMenuItem;
        private System.Windows.Forms.ToolStripTextBox toolStripTextBox2;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator7;
        private System.Windows.Forms.ToolStripMenuItem 레벨레이블ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 굵게ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 테두리ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 글자색변경ToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem 설정창띄우기좌ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 설정창띄우기우ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 분할자색상ToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem 눈금크기ToolStripMenuItem;
        private System.Windows.Forms.ToolStripTextBox toolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem 스펙트럼할당방법ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem fMODSystemToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem fMODChannelToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 가로배열ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 그래프가로배열ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 분할자세로배열ToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.ToolStripMenuItem 가로미터ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 세로미터ToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripMenuItem 측정속도ToolStripMenuItem;
        private System.Windows.Forms.ToolStripTextBox toolStripTextBox1;
        private System.Windows.Forms.ToolStripMenuItem 측정크기ToolStripMenuItem;
        private System.Windows.Forms.ToolStripTextBox toolStripTextBox3;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripMenuItem 닫기ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 그래프릴리즈시간ToolStripMenuItem;
        private System.Windows.Forms.ToolStripTextBox toolStripTextBox4;
        private System.Windows.Forms.ToolStripMenuItem 그래프어택타임ToolStripMenuItem;
        private System.Windows.Forms.ToolStripTextBox toolStripTextBox5;
        private HS_Audio.Control.HSCustomLable lbl_RightPeakValue;
        private HS_Audio.Control.HSCustomLable lbl_RightPeak;
        private HS_Audio.Control.HSCustomLable lbl_LeftPeakValue;
        private HS_Audio.Control.HSCustomLable lbl_LeftPeak;
        private Control.VerticalLabel verticalLabel1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator10;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem dB레벨레이블ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem dB굵게ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem dB테두리ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem dB글자색변경ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem dB투명하게ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 레벨레이블보이기ToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator11;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator12;
        private System.Windows.Forms.ToolStripMenuItem 색깔적용ToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator9;
        private System.Windows.Forms.ToolStripMenuItem 사이즈맞추기ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 글꼴설정ToolStripMenuItem;
        private System.Windows.Forms.FontDialog fontDialog1;
        private System.Windows.Forms.ColorDialog colorDialog1;
        private System.Windows.Forms.ColorDialog colorDialog2;
        private System.Windows.Forms.ColorDialog colorDialog3;
        private System.Windows.Forms.ToolStripMenuItem 음수부호표시ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem dB문자표시ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem dB문자공간ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem dB문자공백ToolStripMenuItem;
        private System.Windows.Forms.ToolStripComboBox toolStripComboBox1;
        private System.Windows.Forms.ToolStripMenuItem 품질우선ToolStripMenuItem;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.ToolStripMenuItem 그래프부드럽게ToolStripMenuItem;
    }
}