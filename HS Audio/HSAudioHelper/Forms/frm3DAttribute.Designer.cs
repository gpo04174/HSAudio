﻿namespace HS_Audio.Forms
{
    partial class frm3D
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm3D));
            this.chk3DMode = new System.Windows.Forms.CheckBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.numPitchY = new System.Windows.Forms.NumericUpDown();
            this.numPitchZ = new System.Windows.Forms.NumericUpDown();
            this.numPitchX = new System.Windows.Forms.NumericUpDown();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.numVectorY = new System.Windows.Forms.NumericUpDown();
            this.numVectorZ = new System.Windows.Forms.NumericUpDown();
            this.numVectorX = new System.Windows.Forms.NumericUpDown();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel2 = new System.Windows.Forms.ToolStripStatusLabel();
            this.lblFMODResult = new System.Windows.Forms.ToolStripStatusLabel();
            this.comboBox2 = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.numaximumVector = new System.Windows.Forms.NumericUpDown();
            this.numinimumPitch = new System.Windows.Forms.NumericUpDown();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.numVectorIncrease = new System.Windows.Forms.NumericUpDown();
            this.numPitchIncrease = new System.Windows.Forms.NumericUpDown();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.chkVectorHorizon = new System.Windows.Forms.CheckBox();
            this.chkPitchHorizon = new System.Windows.Forms.CheckBox();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.button1 = new System.Windows.Forms.Button();
            this.chk무한 = new System.Windows.Forms.CheckBox();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.파일FToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.닫기XToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.설정TToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.개발자모드ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.모양ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripComboBox1 = new System.Windows.Forms.ToolStripComboBox();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.범위제한ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.자동원점ㄱㄱToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.도움말HToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.num시간 = new System.Windows.Forms.NumericUpDown();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.chk반전 = new System.Windows.Forms.CheckBox();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.chk반전1 = new System.Windows.Forms.CheckBox();
            this.joysticControl2 = new HS_Audio.Control.JoysticControl();
            this.joysticControl1 = new HS_Audio.Control.JoysticControl();
            this.label15 = new System.Windows.Forms.Label();
            this.num간격 = new System.Windows.Forms.NumericUpDown();
            this.label16 = new System.Windows.Forms.Label();
            this.num간격1 = new System.Windows.Forms.NumericUpDown();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.num시간1 = new System.Windows.Forms.NumericUpDown();
            this.chk무한1 = new System.Windows.Forms.CheckBox();
            this.button2 = new System.Windows.Forms.Button();
            this.timer2 = new System.Windows.Forms.Timer(this.components);
            this.button3 = new System.Windows.Forms.Button();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.조이스틱컨트롤ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numPitchY)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numPitchZ)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numPitchX)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numVectorY)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numVectorZ)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numVectorX)).BeginInit();
            this.statusStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numaximumVector)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numinimumPitch)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numVectorIncrease)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numPitchIncrease)).BeginInit();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.num시간)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.num간격)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.num간격1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.num시간1)).BeginInit();
            this.SuspendLayout();
            // 
            // chk3DMode
            // 
            this.chk3DMode.AutoSize = true;
            this.chk3DMode.Location = new System.Drawing.Point(5, 27);
            this.chk3DMode.Name = "chk3DMode";
            this.chk3DMode.Size = new System.Drawing.Size(66, 16);
            this.chk3DMode.TabIndex = 0;
            this.chk3DMode.Text = "3D 사용";
            this.chk3DMode.UseVisualStyleBackColor = true;
            this.chk3DMode.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.label6);
            this.groupBox3.Controls.Add(this.label7);
            this.groupBox3.Controls.Add(this.label8);
            this.groupBox3.Controls.Add(this.numPitchY);
            this.groupBox3.Controls.Add(this.numPitchZ);
            this.groupBox3.Controls.Add(this.numPitchX);
            this.groupBox3.Enabled = false;
            this.groupBox3.Location = new System.Drawing.Point(112, 49);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(100, 109);
            this.groupBox3.TabIndex = 8;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "3D Pitch";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(5, 51);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(17, 12);
            this.label6.TabIndex = 5;
            this.label6.Text = "Y:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(5, 78);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(17, 12);
            this.label7.TabIndex = 4;
            this.label7.Text = "Z:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(5, 23);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(17, 12);
            this.label8.TabIndex = 3;
            this.label8.Text = "X:";
            // 
            // numPitchY
            // 
            this.numPitchY.DecimalPlaces = 4;
            this.numPitchY.Increment = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.numPitchY.Location = new System.Drawing.Point(24, 49);
            this.numPitchY.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.numPitchY.Minimum = new decimal(new int[] {
            1000000,
            0,
            0,
            -2147483648});
            this.numPitchY.Name = "numPitchY";
            this.numPitchY.Size = new System.Drawing.Size(70, 21);
            this.numPitchY.TabIndex = 2;
            this.numPitchY.ValueChanged += new System.EventHandler(this.numericUpDown1_ValueChanged);
            // 
            // numPitchZ
            // 
            this.numPitchZ.DecimalPlaces = 4;
            this.numPitchZ.Increment = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.numPitchZ.Location = new System.Drawing.Point(24, 76);
            this.numPitchZ.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.numPitchZ.Minimum = new decimal(new int[] {
            1000000,
            0,
            0,
            -2147483648});
            this.numPitchZ.Name = "numPitchZ";
            this.numPitchZ.Size = new System.Drawing.Size(70, 21);
            this.numPitchZ.TabIndex = 1;
            this.numPitchZ.ValueChanged += new System.EventHandler(this.numericUpDown1_ValueChanged);
            // 
            // numPitchX
            // 
            this.numPitchX.DecimalPlaces = 4;
            this.numPitchX.Increment = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.numPitchX.Location = new System.Drawing.Point(24, 21);
            this.numPitchX.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.numPitchX.Minimum = new decimal(new int[] {
            1000000,
            0,
            0,
            -2147483648});
            this.numPitchX.Name = "numPitchX";
            this.numPitchX.Size = new System.Drawing.Size(70, 21);
            this.numPitchX.TabIndex = 0;
            this.numPitchX.ValueChanged += new System.EventHandler(this.numericUpDown1_ValueChanged);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.numVectorY);
            this.groupBox2.Controls.Add(this.numVectorZ);
            this.groupBox2.Controls.Add(this.numVectorX);
            this.groupBox2.Enabled = false;
            this.groupBox2.Location = new System.Drawing.Point(3, 49);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(106, 109);
            this.groupBox2.TabIndex = 7;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "3D Vector";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 51);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(17, 12);
            this.label5.TabIndex = 5;
            this.label5.Text = "Y:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 78);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(17, 12);
            this.label4.TabIndex = 4;
            this.label4.Text = "Z:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 23);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(17, 12);
            this.label3.TabIndex = 3;
            this.label3.Text = "X:";
            // 
            // numVectorY
            // 
            this.numVectorY.DecimalPlaces = 4;
            this.numVectorY.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.numVectorY.Location = new System.Drawing.Point(25, 49);
            this.numVectorY.Maximum = new decimal(new int[] {
            5000,
            0,
            0,
            0});
            this.numVectorY.Minimum = new decimal(new int[] {
            5000,
            0,
            0,
            -2147483648});
            this.numVectorY.Name = "numVectorY";
            this.numVectorY.Size = new System.Drawing.Size(70, 21);
            this.numVectorY.TabIndex = 2;
            this.numVectorY.ValueChanged += new System.EventHandler(this.numericUpDown1_ValueChanged);
            // 
            // numVectorZ
            // 
            this.numVectorZ.DecimalPlaces = 4;
            this.numVectorZ.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.numVectorZ.Location = new System.Drawing.Point(25, 76);
            this.numVectorZ.Maximum = new decimal(new int[] {
            5000,
            0,
            0,
            0});
            this.numVectorZ.Minimum = new decimal(new int[] {
            5000,
            0,
            0,
            -2147483648});
            this.numVectorZ.Name = "numVectorZ";
            this.numVectorZ.Size = new System.Drawing.Size(70, 21);
            this.numVectorZ.TabIndex = 1;
            this.numVectorZ.ValueChanged += new System.EventHandler(this.numericUpDown1_ValueChanged);
            // 
            // numVectorX
            // 
            this.numVectorX.DecimalPlaces = 4;
            this.numVectorX.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.numVectorX.Location = new System.Drawing.Point(25, 21);
            this.numVectorX.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.numVectorX.Minimum = new decimal(new int[] {
            10000,
            0,
            0,
            -2147483648});
            this.numVectorX.Name = "numVectorX";
            this.numVectorX.Size = new System.Drawing.Size(70, 21);
            this.numVectorX.TabIndex = 0;
            this.numVectorX.ValueChanged += new System.EventHandler(this.numericUpDown1_ValueChanged);
            // 
            // statusStrip1
            // 
            this.statusStrip1.AutoSize = false;
            this.statusStrip1.BackColor = System.Drawing.Color.Transparent;
            this.statusStrip1.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1,
            this.toolStripStatusLabel2,
            this.lblFMODResult});
            this.statusStrip1.Location = new System.Drawing.Point(0, 257);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(511, 17);
            this.statusStrip1.TabIndex = 65;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Font = new System.Drawing.Font("Gulim", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(67, 12);
            this.toolStripStatusLabel1.Text = "내부 상태:";
            this.toolStripStatusLabel1.ToolTipText = "내부 상태 (OK가 아니면 설정실패!)";
            // 
            // toolStripStatusLabel2
            // 
            this.toolStripStatusLabel2.Font = new System.Drawing.Font("Gulim", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.toolStripStatusLabel2.ForeColor = System.Drawing.SystemColors.Control;
            this.toolStripStatusLabel2.IsLink = true;
            this.toolStripStatusLabel2.Name = "toolStripStatusLabel2";
            this.toolStripStatusLabel2.Size = new System.Drawing.Size(0, 12);
            // 
            // lblFMODResult
            // 
            this.lblFMODResult.Font = new System.Drawing.Font("Gulim", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblFMODResult.ForeColor = System.Drawing.Color.Blue;
            this.lblFMODResult.Name = "lblFMODResult";
            this.lblFMODResult.Size = new System.Drawing.Size(24, 12);
            this.lblFMODResult.Text = "OK";
            // 
            // comboBox2
            // 
            this.comboBox2.FormattingEnabled = true;
            this.comboBox2.Location = new System.Drawing.Point(69, 25);
            this.comboBox2.Name = "comboBox2";
            this.comboBox2.Size = new System.Drawing.Size(121, 20);
            this.comboBox2.TabIndex = 66;
            this.comboBox2.Visible = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(228, 175);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(101, 12);
            this.label1.TabIndex = 68;
            this.label1.Text = "Pixel: {X=0, Y=0}";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(228, 190);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(105, 12);
            this.label2.TabIndex = 69;
            this.label2.Text = "Value: {X=0, Y=0}";
            // 
            // numaximumVector
            // 
            this.numaximumVector.DecimalPlaces = 1;
            this.numaximumVector.Increment = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.numaximumVector.Location = new System.Drawing.Point(284, 205);
            this.numaximumVector.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.numaximumVector.Name = "numaximumVector";
            this.numaximumVector.Size = new System.Drawing.Size(60, 21);
            this.numaximumVector.TabIndex = 71;
            this.numaximumVector.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.numaximumVector.Visible = false;
            this.numaximumVector.ValueChanged += new System.EventHandler(this.numericUpDown7_ValueChanged);
            // 
            // numinimumPitch
            // 
            this.numinimumPitch.Location = new System.Drawing.Point(433, 205);
            this.numinimumPitch.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.numinimumPitch.Name = "numinimumPitch";
            this.numinimumPitch.Size = new System.Drawing.Size(60, 21);
            this.numinimumPitch.TabIndex = 72;
            this.numinimumPitch.Value = new decimal(new int[] {
            200,
            0,
            0,
            0});
            this.numinimumPitch.Visible = false;
            this.numinimumPitch.ValueChanged += new System.EventHandler(this.numericUpDown8_ValueChanged);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(376, 175);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(101, 12);
            this.label10.TabIndex = 74;
            this.label10.Text = "Pixel: {X=0, Y=0}";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(376, 190);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(105, 12);
            this.label9.TabIndex = 73;
            this.label9.Text = "Value: {X=0, Y=0}";
            // 
            // numVectorIncrease
            // 
            this.numVectorIncrease.DecimalPlaces = 4;
            this.numVectorIncrease.Increment = new decimal(new int[] {
            5,
            0,
            0,
            262144});
            this.numVectorIncrease.Location = new System.Drawing.Point(290, 205);
            this.numVectorIncrease.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.numVectorIncrease.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            262144});
            this.numVectorIncrease.Name = "numVectorIncrease";
            this.numVectorIncrease.Size = new System.Drawing.Size(60, 21);
            this.numVectorIncrease.TabIndex = 77;
            this.numVectorIncrease.Value = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.numVectorIncrease.ValueChanged += new System.EventHandler(this.numIncreaseVector_ValueChanged);
            // 
            // numPitchIncrease
            // 
            this.numPitchIncrease.DecimalPlaces = 4;
            this.numPitchIncrease.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.numPitchIncrease.Location = new System.Drawing.Point(437, 205);
            this.numPitchIncrease.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.numPitchIncrease.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            262144});
            this.numPitchIncrease.Name = "numPitchIncrease";
            this.numPitchIncrease.Size = new System.Drawing.Size(60, 21);
            this.numPitchIncrease.TabIndex = 78;
            this.numPitchIncrease.Value = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.numPitchIncrease.ValueChanged += new System.EventHandler(this.numIncreasePitch_ValueChanged);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(228, 209);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(49, 12);
            this.label11.TabIndex = 79;
            this.label11.Text = "증가 폭:";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(376, 209);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(49, 12);
            this.label12.TabIndex = 80;
            this.label12.Text = "증가 폭:";
            // 
            // chkVectorHorizon
            // 
            this.chkVectorHorizon.AutoSize = true;
            this.chkVectorHorizon.Location = new System.Drawing.Point(230, 230);
            this.chkVectorHorizon.Name = "chkVectorHorizon";
            this.chkVectorHorizon.Size = new System.Drawing.Size(105, 16);
            this.chkVectorHorizon.TabIndex = 81;
            this.chkVectorHorizon.Text = "수평 (Horizon)";
            this.chkVectorHorizon.UseVisualStyleBackColor = true;
            // 
            // chkPitchHorizon
            // 
            this.chkPitchHorizon.AutoSize = true;
            this.chkPitchHorizon.Location = new System.Drawing.Point(378, 230);
            this.chkPitchHorizon.Name = "chkPitchHorizon";
            this.chkPitchHorizon.Size = new System.Drawing.Size(105, 16);
            this.chkPitchHorizon.TabIndex = 82;
            this.chkPitchHorizon.Text = "수평 (Horizon)";
            this.chkPitchHorizon.UseVisualStyleBackColor = true;
            // 
            // timer1
            // 
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // button1
            // 
            this.button1.Enabled = false;
            this.button1.Location = new System.Drawing.Point(2, 160);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(68, 23);
            this.button1.TabIndex = 83;
            this.button1.Text = "회전";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // chk무한
            // 
            this.chk무한.AutoSize = true;
            this.chk무한.Location = new System.Drawing.Point(3, 235);
            this.chk무한.Margin = new System.Windows.Forms.Padding(2);
            this.chk무한.Name = "chk무한";
            this.chk무한.Size = new System.Drawing.Size(48, 16);
            this.chk무한.TabIndex = 84;
            this.chk무한.Text = "무한";
            this.chk무한.UseVisualStyleBackColor = true;
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.파일FToolStripMenuItem,
            this.설정TToolStripMenuItem,
            this.도움말HToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(511, 24);
            this.menuStrip1.TabIndex = 85;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // 파일FToolStripMenuItem
            // 
            this.파일FToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.닫기XToolStripMenuItem});
            this.파일FToolStripMenuItem.Name = "파일FToolStripMenuItem";
            this.파일FToolStripMenuItem.Size = new System.Drawing.Size(57, 20);
            this.파일FToolStripMenuItem.Text = "파일(&F)";
            // 
            // 닫기XToolStripMenuItem
            // 
            this.닫기XToolStripMenuItem.Name = "닫기XToolStripMenuItem";
            this.닫기XToolStripMenuItem.Size = new System.Drawing.Size(116, 22);
            this.닫기XToolStripMenuItem.Text = "닫기 (&X)";
            this.닫기XToolStripMenuItem.Click += new System.EventHandler(this.닫기XToolStripMenuItem_Click);
            // 
            // 설정TToolStripMenuItem
            // 
            this.설정TToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.모양ToolStripMenuItem,
            this.toolStripSeparator2,
            this.범위제한ToolStripMenuItem,
            this.자동원점ㄱㄱToolStripMenuItem,
            this.toolStripSeparator3,
            this.개발자모드ToolStripMenuItem});
            this.설정TToolStripMenuItem.Name = "설정TToolStripMenuItem";
            this.설정TToolStripMenuItem.Size = new System.Drawing.Size(58, 20);
            this.설정TToolStripMenuItem.Text = "설정(&T)";
            // 
            // 개발자모드ToolStripMenuItem
            // 
            this.개발자모드ToolStripMenuItem.CheckOnClick = true;
            this.개발자모드ToolStripMenuItem.Name = "개발자모드ToolStripMenuItem";
            this.개발자모드ToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.개발자모드ToolStripMenuItem.Text = "개발자 모드";
            this.개발자모드ToolStripMenuItem.CheckedChanged += new System.EventHandler(this.개발자모드ToolStripMenuItem_CheckedChanged);
            // 
            // 모양ToolStripMenuItem
            // 
            this.모양ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripComboBox1});
            this.모양ToolStripMenuItem.Name = "모양ToolStripMenuItem";
            this.모양ToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.모양ToolStripMenuItem.Text = "모양";
            // 
            // toolStripComboBox1
            // 
            this.toolStripComboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.toolStripComboBox1.Items.AddRange(new object[] {
            "사각형",
            "원"});
            this.toolStripComboBox1.Name = "toolStripComboBox1";
            this.toolStripComboBox1.Size = new System.Drawing.Size(121, 23);
            this.toolStripComboBox1.SelectedIndexChanged += new System.EventHandler(this.toolStripComboBox1_SelectedIndexChanged);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(149, 6);
            // 
            // 범위제한ToolStripMenuItem
            // 
            this.범위제한ToolStripMenuItem.Checked = true;
            this.범위제한ToolStripMenuItem.CheckOnClick = true;
            this.범위제한ToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.범위제한ToolStripMenuItem.Name = "범위제한ToolStripMenuItem";
            this.범위제한ToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.범위제한ToolStripMenuItem.Text = "범위 제한";
            this.범위제한ToolStripMenuItem.CheckedChanged += new System.EventHandler(this.범위제한ToolStripMenuItem_CheckedChanged);
            // 
            // 자동원점ㄱㄱToolStripMenuItem
            // 
            this.자동원점ㄱㄱToolStripMenuItem.CheckOnClick = true;
            this.자동원점ㄱㄱToolStripMenuItem.Name = "자동원점ㄱㄱToolStripMenuItem";
            this.자동원점ㄱㄱToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.자동원점ㄱㄱToolStripMenuItem.Text = "자동 원점 ㄱㄱ";
            this.자동원점ㄱㄱToolStripMenuItem.CheckedChanged += new System.EventHandler(this.자동원점ㄱㄱToolStripMenuItem_CheckedChanged);
            // 
            // 도움말HToolStripMenuItem
            // 
            this.도움말HToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.조이스틱컨트롤ToolStripMenuItem});
            this.도움말HToolStripMenuItem.Name = "도움말HToolStripMenuItem";
            this.도움말HToolStripMenuItem.Size = new System.Drawing.Size(72, 20);
            this.도움말HToolStripMenuItem.Text = "도움말(&H)";
            // 
            // num시간
            // 
            this.num시간.DecimalPlaces = 3;
            this.num시간.Increment = new decimal(new int[] {
            5,
            0,
            0,
            196608});
            this.num시간.Location = new System.Drawing.Point(39, 186);
            this.num시간.Maximum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.num시간.Minimum = new decimal(new int[] {
            25,
            0,
            0,
            196608});
            this.num시간.Name = "num시간";
            this.num시간.Size = new System.Drawing.Size(50, 21);
            this.num시간.TabIndex = 86;
            this.num시간.Value = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.num시간.ValueChanged += new System.EventHandler(this.num시간_ValueChanged);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(92, 188);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(17, 12);
            this.label13.TabIndex = 87;
            this.label13.Text = "초";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(3, 188);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(29, 12);
            this.label14.TabIndex = 88;
            this.label14.Text = "시간";
            // 
            // chk반전
            // 
            this.chk반전.AutoSize = true;
            this.chk반전.Location = new System.Drawing.Point(58, 235);
            this.chk반전.Name = "chk반전";
            this.chk반전.Size = new System.Drawing.Size(48, 16);
            this.chk반전.TabIndex = 89;
            this.chk반전.Text = "반전";
            this.toolTip1.SetToolTip(this.chk반전, "다음번 회전부터 적용됩니다.");
            this.chk반전.UseVisualStyleBackColor = true;
            // 
            // chk반전1
            // 
            this.chk반전1.AutoSize = true;
            this.chk반전1.Location = new System.Drawing.Point(168, 235);
            this.chk반전1.Name = "chk반전1";
            this.chk반전1.Size = new System.Drawing.Size(48, 16);
            this.chk반전1.TabIndex = 98;
            this.chk반전1.Text = "반전";
            this.toolTip1.SetToolTip(this.chk반전1, "다음번 회전부터 적용됩니다.");
            this.chk반전1.UseVisualStyleBackColor = true;
            // 
            // joysticControl2
            // 
            this.joysticControl2.BackColor = System.Drawing.Color.Cyan;
            this.joysticControl2.ButtonSize = ((byte)(20));
            this.joysticControl2.DotLineColor = System.Drawing.Color.Blue;
            this.joysticControl2.Enabled = false;
            this.joysticControl2.Location = new System.Drawing.Point(365, 28);
            this.joysticControl2.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.joysticControl2.Maximum = ((System.Drawing.PointF)(resources.GetObject("joysticControl2.Maximum")));
            this.joysticControl2.Name = "joysticControl2";
            this.joysticControl2.Size = new System.Drawing.Size(140, 140);
            this.joysticControl2.TabIndex = 76;
            this.toolTip1.SetToolTip(this.joysticControl2, "버튼을 원점(0, 0)으로 되돌려 두시려면\r\n빈 공간을 더블 클릭 해주시면 됩니다.");
            this.joysticControl2.Value = ((System.Drawing.PointF)(resources.GetObject("joysticControl2.Value")));
            this.joysticControl2.JoysticControlValueChanged += new HS_Audio.Control.JoysticControl.JoysticControlValueChangedEventHandler(this.joysticControl2_JoysticControlValueChanged);
            // 
            // joysticControl1
            // 
            this.joysticControl1.BackColor = System.Drawing.Color.Cyan;
            this.joysticControl1.ButtonSize = ((byte)(20));
            this.joysticControl1.DotLineColor = System.Drawing.Color.Blue;
            this.joysticControl1.Enabled = false;
            this.joysticControl1.Location = new System.Drawing.Point(218, 28);
            this.joysticControl1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.joysticControl1.Maximum = ((System.Drawing.PointF)(resources.GetObject("joysticControl1.Maximum")));
            this.joysticControl1.Name = "joysticControl1";
            this.joysticControl1.Size = new System.Drawing.Size(140, 140);
            this.joysticControl1.TabIndex = 75;
            this.toolTip1.SetToolTip(this.joysticControl1, "버튼을 원점(0, 0)으로 되돌려 두시려면\r\n빈 공간을 더블 클릭 해주시면 됩니다.");
            this.joysticControl1.Value = ((System.Drawing.PointF)(resources.GetObject("joysticControl1.Value")));
            this.joysticControl1.JoysticControlValueChanged += new HS_Audio.Control.JoysticControl.JoysticControlValueChangedEventHandler(this.joysticControl1_JoysticControlValueChanged);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(3, 212);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(29, 12);
            this.label15.TabIndex = 92;
            this.label15.Text = "간격";
            // 
            // num간격
            // 
            this.num간격.DecimalPlaces = 3;
            this.num간격.Location = new System.Drawing.Point(39, 210);
            this.num간격.Maximum = new decimal(new int[] {
            200,
            0,
            0,
            0});
            this.num간격.Minimum = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.num간격.Name = "num간격";
            this.num간격.Size = new System.Drawing.Size(67, 21);
            this.num간격.TabIndex = 90;
            this.num간격.Value = new decimal(new int[] {
            30,
            0,
            0,
            0});
            this.num간격.ValueChanged += new System.EventHandler(this.num간격_ValueChanged);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(113, 212);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(29, 12);
            this.label16.TabIndex = 100;
            this.label16.Text = "간격";
            // 
            // num간격1
            // 
            this.num간격1.DecimalPlaces = 3;
            this.num간격1.Location = new System.Drawing.Point(149, 210);
            this.num간격1.Maximum = new decimal(new int[] {
            200,
            0,
            0,
            0});
            this.num간격1.Minimum = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.num간격1.Name = "num간격1";
            this.num간격1.Size = new System.Drawing.Size(67, 21);
            this.num간격1.TabIndex = 99;
            this.num간격1.Value = new decimal(new int[] {
            30,
            0,
            0,
            0});
            this.num간격1.ValueChanged += new System.EventHandler(this.num간격1_ValueChanged);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(113, 188);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(29, 12);
            this.label17.TabIndex = 97;
            this.label17.Text = "시간";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(202, 188);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(17, 12);
            this.label18.TabIndex = 96;
            this.label18.Text = "초";
            // 
            // num시간1
            // 
            this.num시간1.DecimalPlaces = 3;
            this.num시간1.Increment = new decimal(new int[] {
            5,
            0,
            0,
            196608});
            this.num시간1.Location = new System.Drawing.Point(149, 186);
            this.num시간1.Maximum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.num시간1.Minimum = new decimal(new int[] {
            25,
            0,
            0,
            196608});
            this.num시간1.Name = "num시간1";
            this.num시간1.Size = new System.Drawing.Size(50, 21);
            this.num시간1.TabIndex = 95;
            this.num시간1.Value = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.num시간1.ValueChanged += new System.EventHandler(this.num시간1_ValueChanged);
            // 
            // chk무한1
            // 
            this.chk무한1.AutoSize = true;
            this.chk무한1.Location = new System.Drawing.Point(113, 235);
            this.chk무한1.Margin = new System.Windows.Forms.Padding(2);
            this.chk무한1.Name = "chk무한1";
            this.chk무한1.Size = new System.Drawing.Size(48, 16);
            this.chk무한1.TabIndex = 94;
            this.chk무한1.Text = "무한";
            this.chk무한1.UseVisualStyleBackColor = true;
            // 
            // button2
            // 
            this.button2.Enabled = false;
            this.button2.Location = new System.Drawing.Point(145, 160);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(68, 23);
            this.button2.TabIndex = 93;
            this.button2.Text = "회전";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // timer2
            // 
            this.timer2.Tick += new System.EventHandler(this.timer2_Tick);
            // 
            // button3
            // 
            this.button3.Enabled = false;
            this.button3.Location = new System.Drawing.Point(71, 160);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(73, 23);
            this.button3.TabIndex = 101;
            this.button3.Text = "동시 회전";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(149, 6);
            // 
            // 조이스틱컨트롤ToolStripMenuItem
            // 
            this.조이스틱컨트롤ToolStripMenuItem.Name = "조이스틱컨트롤ToolStripMenuItem";
            this.조이스틱컨트롤ToolStripMenuItem.Size = new System.Drawing.Size(170, 22);
            this.조이스틱컨트롤ToolStripMenuItem.Text = "조이스틱 컨트롤...";
            this.조이스틱컨트롤ToolStripMenuItem.Click += new System.EventHandler(this.조이스틱컨트롤ToolStripMenuItem_Click);
            // 
            // frm3D
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(511, 274);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.num간격1);
            this.Controls.Add(this.chk반전1);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.num시간1);
            this.Controls.Add(this.chk무한1);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.num간격);
            this.Controls.Add(this.chk반전);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.num시간);
            this.Controls.Add(this.chk무한);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.chkPitchHorizon);
            this.Controls.Add(this.chkVectorHorizon);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.numPitchIncrease);
            this.Controls.Add(this.numVectorIncrease);
            this.Controls.Add(this.joysticControl2);
            this.Controls.Add(this.joysticControl1);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.numinimumPitch);
            this.Controls.Add(this.numaximumVector);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.comboBox2);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.chk3DMode);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "frm3D";
            this.Text = "3D리스너 설정";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frm3D_FormClosing);
            this.Load += new System.EventHandler(this.frm3D_Load);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numPitchY)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numPitchZ)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numPitchX)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numVectorY)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numVectorZ)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numVectorX)).EndInit();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numaximumVector)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numinimumPitch)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numVectorIncrease)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numPitchIncrease)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.num시간)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.num간격)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.num간격1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.num시간1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.CheckBox chk3DMode;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.NumericUpDown numPitchY;
        private System.Windows.Forms.NumericUpDown numPitchZ;
        private System.Windows.Forms.NumericUpDown numPitchX;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.NumericUpDown numVectorY;
        private System.Windows.Forms.NumericUpDown numVectorZ;
        private System.Windows.Forms.NumericUpDown numVectorX;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel2;
        private System.Windows.Forms.ToolStripStatusLabel lblFMODResult;
        private System.Windows.Forms.ComboBox comboBox2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.NumericUpDown numaximumVector;
        private System.Windows.Forms.NumericUpDown numinimumPitch;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private Control.JoysticControl joysticControl1;
        private Control.JoysticControl joysticControl2;
        private System.Windows.Forms.NumericUpDown numVectorIncrease;
        private System.Windows.Forms.NumericUpDown numPitchIncrease;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.CheckBox chkVectorHorizon;
        private System.Windows.Forms.CheckBox chkPitchHorizon;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.CheckBox chk무한;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem 파일FToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 닫기XToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 설정TToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 개발자모드ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 도움말HToolStripMenuItem;
        private System.Windows.Forms.NumericUpDown num시간;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.CheckBox chk반전;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.NumericUpDown num간격;
        private System.Windows.Forms.ToolStripMenuItem 모양ToolStripMenuItem;
        private System.Windows.Forms.ToolStripComboBox toolStripComboBox1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem 범위제한ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 자동원점ㄱㄱToolStripMenuItem;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.NumericUpDown num간격1;
        private System.Windows.Forms.CheckBox chk반전1;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.NumericUpDown num시간1;
        private System.Windows.Forms.CheckBox chk무한1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Timer timer2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripMenuItem 조이스틱컨트롤ToolStripMenuItem;
    }
}