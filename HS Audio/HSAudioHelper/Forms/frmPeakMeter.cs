﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;


namespace HS_Audio.Forms
{
    public partial class frmPeakMeter : HS_Audio.Control.HSCustomForm, IHSSetting
    {
        string SettingDirectory
        { 
            get{ string a = Application.StartupPath + "\\Settings\\PeakMeter";
            try { if (!System.IO.Directory.Exists(a)) { System.IO.Directory.CreateDirectory(a); } }
            catch { return null; } return a; }
        }

        public float[] InitData(int Length, float Value)
        {
            float[] f = new float[Length];
            for(int i=0;i<Length;i++)f[i] = Value;
            return f;
        }

        const int DefaultLED = 40;
        HSAudioHelper Data;
        int numchannels = 2;
        int dummy = 0;
        HS_Audio_Lib.SOUND_FORMAT dummyformat = HS_Audio_Lib.SOUND_FORMAT.NONE;
        HS_Audio_Lib.DSP_RESAMPLER _dummyresampler = HS_Audio_Lib.DSP_RESAMPLER.LINEAR;
        PropertyGrid pg, pg1;
        public frmPeakMeter(HSAudioHelper Helper)
        {
            InitializeComponent();
            SetStyle(ControlStyles.SupportsTransparentBackColor, true);
            Data = Helper;
            MaxLdB = float.NegativeInfinity; MaxRdB = float.NegativeInfinity;
            Ltmp = InitData(2048, float.NegativeInfinity);
            Rtmp = InitData(2048, float.NegativeInfinity);
            if (Data.system != null)
                Data.system.getSoftwareFormat(ref dummy, ref dummyformat, ref numchannels, ref dummy, ref _dummyresampler, ref dummy);

            peakMeterCtrl1.SetRange(21, 21+13, DefaultLED);//100, 115, 120);
            peakMeterCtrl2.SetRange(21, 21+13, DefaultLED);//100, 115, 120);
            //peakMeterCtrl1.LED눈금갯수 = 100;
            pg = new PropertyGrid()
            {
                SelectedObject = peakMeterCtrl1,
                Size = new Size(290, 350),
                Dock= DockStyle.Fill,
                Location = new Point(0, 0),
                Anchor = AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right | AnchorStyles.Top
            };
            pg1 = new PropertyGrid()
            {
                SelectedObject = peakMeterCtrl2,
                Size = new Size(290, 350),
                Dock= DockStyle.Fill,
                Location = new Point(0, 0),
                Anchor = AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right | AnchorStyles.Top
            };
            f.Controls.Add(pg); f1.Controls.Add(pg1);
            //peakMeterCtrl1.PropertiesChanged += new Ernzo.WinForms.Controls.PropertiesChangedEventHandler(peakMeterCtrl1_PropertiesChanged);
        }

        #region 설정
        Dictionary<string, string> _Setting = new Dictionary<string,string>();
        internal Dictionary<string, string> Setting
        {
            get
            {
                Dictionary<string, string> a = new Dictionary<string, string>();
                a.Add("FormSize", HS_CSharpUtility.Utility.ConvertUtility.SizeToString(this.Size));
                a.Add("FormLocation", HS_CSharpUtility.Utility.ConvertUtility.PointToString(this.Location));
                try{a.Add("FormOpacity", this.Opacity.ToString("0.00"));}catch{}
                a.Add("TopMost", this.TopMost.ToString());
                a.Add("ShowEdge", 테두리표시ToolStripMenuItem.Checked.ToString());
                a.Add("IsBackgroundTransparent", 배경투명ToolStripMenuItem.Checked.ToString());
                a.Add("IsShowdBLable", 레벨레이블보이기ToolStripMenuItem.Checked.ToString());
                a.Add("IsdBLableBold", 굵게ToolStripMenuItem.Checked.ToString());
                a.Add("IsdBLableEdge", 테두리ToolStripMenuItem.Checked.ToString());
                a.Add("IsdBLableApplyColor", 색깔적용ToolStripMenuItem.Checked.ToString());
                a.Add("dBLableColor", HS_CSharpUtility.Utility.ConvertUtility.ColorToString(lbl_LeftPeak.ForeColor,true));
                a.Add("IsSplitterTransparent", 분할자투명ToolStripMenuItem.Checked.ToString());
                a.Add("SplitterDistance", splitContainer1.SplitterDistance.ToString());
                a.Add("SplitterColor", HS_CSharpUtility.Utility.ConvertUtility.ColorToString(splitContainer1.BackColor,true));
                a.Add("ShowTitle", 타이틀표시ToolStripMenuItem.Checked.ToString());
                a.Add("SoundAlloc", fMODSystemToolStripMenuItem.Checked.ToString());
                a.Add("WidthGraph", 그래프가로배열ToolStripMenuItem.Checked.ToString());
                a.Add("WidthSplit", 분할자세로배열ToolStripMenuItem.Checked.ToString());
                a.Add("DrawSpeed", timer1.Interval.ToString());
                a.Add("SoundBuffer",Ltmp.Length.ToString());
                a.Add("LEDCount", toolStripMenuItem2.ToString());

                a.Add("\r\n[LeftMeter]", null);
                a.Add("LeftMeterBandsCount", peakMeterCtrl1.BandsCount.ToString());
                a.Add("LeftMeterGridShow", peakMeterCtrl1.모눈선표시.ToString());
                a.Add("LeftMeterGridColor", HS_CSharpUtility.Utility.ConvertUtility.ColorToString(peakMeterCtrl1.모눈선색상));
                a.Add("LeftMeterColorBackShow", peakMeterCtrl1.색상표시.ToString());
                a.Add("LeftMeterBackgroundColor", HS_CSharpUtility.Utility.ConvertUtility.ColorToString(peakMeterCtrl1.BackColor));
                a.Add("LeftMeterColorHigh", HS_CSharpUtility.Utility.ConvertUtility.ColorToString(peakMeterCtrl1.ColorHigh));
                a.Add("LeftMeterColorHighBack", HS_CSharpUtility.Utility.ConvertUtility.ColorToString(peakMeterCtrl1.ColorHighBack));                
                a.Add("LeftMeterColorMedium", HS_CSharpUtility.Utility.ConvertUtility.ColorToString(peakMeterCtrl1.ColorMedium));
                a.Add("LeftMeterColorMediumBack", HS_CSharpUtility.Utility.ConvertUtility.ColorToString(peakMeterCtrl1.ColorMediumBack));
                a.Add("LeftMeterColorNormal", HS_CSharpUtility.Utility.ConvertUtility.ColorToString(peakMeterCtrl1.ColorNormal));
                a.Add("LeftMeterColorNormalBack", HS_CSharpUtility.Utility.ConvertUtility.ColorToString(peakMeterCtrl1.ColorNormalBack));
                a.Add("LeftMeterPeakShow", peakMeterCtrl1.Peak표시.ToString());
                a.Add("LeftMeterPeakBolder", peakMeterCtrl1.Peak굵기.ToString());
                a.Add("LeftMeterPeakSpeed", peakMeterCtrl1.Peak속도.ToString());
                a.Add("LeftMeterPeakColor", HS_CSharpUtility.Utility.ConvertUtility.ColorToString(peakMeterCtrl1.Peak색상));

                a.Add("\r\n[RightMeter]", null);
                a.Add("RightMeterBandsCount", peakMeterCtrl2.BandsCount.ToString());
                a.Add("RightMeterGridShow", peakMeterCtrl2.모눈선표시.ToString());
                a.Add("RightMeterGridColor", HS_CSharpUtility.Utility.ConvertUtility.ColorToString(peakMeterCtrl2.모눈선색상));
                a.Add("RightMeterColorBackShow", peakMeterCtrl2.색상표시.ToString());
                a.Add("RightMeterBackgroundColor", HS_CSharpUtility.Utility.ConvertUtility.ColorToString(peakMeterCtrl2.BackColor));
                a.Add("RightMeterColorHigh", HS_CSharpUtility.Utility.ConvertUtility.ColorToString(peakMeterCtrl2.ColorHigh));
                a.Add("RightMeterColorHighBack", HS_CSharpUtility.Utility.ConvertUtility.ColorToString(peakMeterCtrl2.ColorHighBack));
                a.Add("RightMeterColorMedium", HS_CSharpUtility.Utility.ConvertUtility.ColorToString(peakMeterCtrl2.ColorMedium));
                a.Add("RightMeterColorMediumBack", HS_CSharpUtility.Utility.ConvertUtility.ColorToString(peakMeterCtrl2.ColorMediumBack));
                a.Add("RightMeterColorNormal", HS_CSharpUtility.Utility.ConvertUtility.ColorToString(peakMeterCtrl2.ColorNormal));
                a.Add("RightMeterColorNormalBack", HS_CSharpUtility.Utility.ConvertUtility.ColorToString(peakMeterCtrl2.ColorNormalBack));
                a.Add("RightMeterPeakShow", peakMeterCtrl2.Peak표시.ToString());
                a.Add("RightMeterPeakBolder", peakMeterCtrl2.Peak굵기.ToString());
                a.Add("RightMeterPeakSpeed", peakMeterCtrl2.Peak속도.ToString());
                a.Add("RightMeterPeakColor", HS_CSharpUtility.Utility.ConvertUtility.ColorToString(peakMeterCtrl2.Peak색상));
                return a;
            }
            set
            {
                Dictionary<string, string> a = value;
                try{if(a.ContainsKey("FormLocation"))this.Location =HS_CSharpUtility.Utility.ConvertUtility.StringToPoint(a["FormLocation"]);}catch{}
                try{if(a.ContainsKey("FormOpacity"))this.Opacity =double.Parse(a["FormOpacity"]);}catch{}
                try{if(a.ContainsKey("TopMost"))맨위에표시ToolStripMenuItem.Checked = bool.Parse(a["TopMost"]);}catch{}
                try{if(a.ContainsKey("ShowEdge"))테두리표시ToolStripMenuItem.Checked=bool.Parse(a["ShowEdge"]);}catch{}
                try{if(a.ContainsKey("IsBackgrountTransparent"))배경투명ToolStripMenuItem.Checked=bool.Parse(a["IsBackgrountTransparent"]);}catch{}
                try{if(a.ContainsKey("IsSplitterTransparent"))분할자투명ToolStripMenuItem.Checked=bool.Parse(a["IsSplitterTransparent"]);}catch{}
                try{if(a.ContainsKey("IsShowdBLable"))레벨레이블보이기ToolStripMenuItem.Checked = bool.Parse(a["IsShowdBLable"]);}catch{}
                try{if(a.ContainsKey("IsdBLableBold"))굵게ToolStripMenuItem.Checked = bool.Parse(a["IsdBLableBold"]);}catch{}
                try{if(a.ContainsKey("IsdBLableEdge"))테두리ToolStripMenuItem.Checked = bool.Parse(a["IsdBLableEdge"]);}catch{}
                try{if(a.ContainsKey("IsdBLableApplyColor"))색깔적용ToolStripMenuItem.Checked = bool.Parse(a["IsdBLableApplyColor"]);}catch{}
                try{if(a.ContainsKey("dBLableColor"))lbl_LeftPeak.ForeColor=lbl_LeftPeakValue.ForeColor=lbl_RightPeak.ForeColor=lbl_RightPeakValue.ForeColor = HS_CSharpUtility.Utility.ConvertUtility.StringToColor(a["dBLableColor"]);}catch{}
                try{if(a.ContainsKey("ShowTitle"))타이틀표시ToolStripMenuItem.Checked=bool.Parse(a["ShowTitle"]);}catch{}
                try{if(a.ContainsKey("SoundAlloc"))fMODSystemToolStripMenuItem.Checked=bool.Parse(a["SoundAlloc"]);}catch{}
                try{if(a.ContainsKey("WidthGraph"))그래프가로배열ToolStripMenuItem.Checked=bool.Parse(a["WidthGraph"]);}catch{}
                try{if(a.ContainsKey("WidthSplit"))분할자세로배열ToolStripMenuItem.Checked=bool.Parse(a["WidthSplit"]);}catch{}
                try{if(a.ContainsKey("FormSize"))this.Size =HS_CSharpUtility.Utility.ConvertUtility.StringToSize(a["FormSize"]);}catch{}
                try{if(a.ContainsKey("SplitterDistance"))splitContainer1.SplitterDistance=Convert.ToInt32(a["SplitterDistance"]);}catch{}
                //Color c = peakMeterCtrl2.BackColor;
                try{if(a.ContainsKey("SplitterColor"))splitContainer1.BackColor = colorDialog1.Color = HS_CSharpUtility.Utility.ConvertUtility.StringToColor(a["SplitterColor"]); }catch{}
                //peakMeterCtrl2.BackColor = c;
                //try{if(a.ContainsKey("RightMeterBandsCount"))timer1.Interval=Convert.ToInt32(a["DrawSpeed"]);}catch{}
                try{if(a.ContainsKey("DrawSpeed"))toolStripTextBox1.Text=a["DrawSpeed"];}catch{}
                //try{if(a.ContainsKey("RightMeterBandsCount"))Ltmp =Rtmp= new float[Convert.ToInt32(a["SoundBuffer"])];}catch{}
                try{if(a.ContainsKey("SoundBuffer"))toolStripTextBox3.Text= a["SoundBuffer"];}catch{}
                try{if(a.ContainsKey("LEDCount"))toolStripMenuItem2.Text = a["LEDCount"];}catch{}
                //[LeftMeter]
                try{if(a.ContainsKey("LeftMeterBandsCount"))peakMeterCtrl1.BandsCount=Convert.ToInt32(a["LeftMeterBandsCount"]);}catch{}
                try{if(a.ContainsKey("LeftMeterGridShow"))peakMeterCtrl1.모눈선표시=bool.Parse(a["LeftMeterGridShow"]);}catch{}
                try{if(a.ContainsKey("LeftMeterGridColor"))peakMeterCtrl1.모눈선색상=HS_CSharpUtility.Utility.ConvertUtility.StringToColor(a["LeftMeterGridColor"]);}catch{}
                try{if(a.ContainsKey("LeftMeterColorBackShow"))peakMeterCtrl1.색상표시=bool.Parse(a["LeftMeterColorBackShow"]);}catch{}
                try{if(a.ContainsKey("LeftMeterBackgroundColor"))peakMeterCtrl1.BackColor=HS_CSharpUtility.Utility.ConvertUtility.StringToColor(a["LeftMeterBackgroundColor"]);}catch{}
                try{if(a.ContainsKey("LeftMeterColorHigh"))peakMeterCtrl1.ColorHigh = HS_CSharpUtility.Utility.ConvertUtility.StringToColor(a["LeftMeterColorHigh"]);}catch{}
                try{if(a.ContainsKey("LeftMeterColorHighBack"))peakMeterCtrl1.ColorHighBack=HS_CSharpUtility.Utility.ConvertUtility.StringToColor(a["LeftMeterColorHighBack"]);}catch{}
                try{if(a.ContainsKey("LeftMeterColorMedium"))peakMeterCtrl1.ColorMedium=HS_CSharpUtility.Utility.ConvertUtility.StringToColor(a["LeftMeterColorMedium"]);}catch{}
                try{if(a.ContainsKey("LeftMeterColorMediumBack"))peakMeterCtrl1.ColorMediumBack=HS_CSharpUtility.Utility.ConvertUtility.StringToColor(a["LeftMeterColorMediumBack"]);}catch{}
                try{if(a.ContainsKey("LeftMeterColorNormal"))peakMeterCtrl1.ColorNormal=HS_CSharpUtility.Utility.ConvertUtility.StringToColor(a["LeftMeterColorNormal"]);}catch{}
                try{if(a.ContainsKey("LeftMeterColorNormalBack"))peakMeterCtrl1.ColorNormalBack=HS_CSharpUtility.Utility.ConvertUtility.StringToColor(a["LeftMeterColorNormalBack"]);}catch{}
                try{if(a.ContainsKey("LeftMeterPeakShow"))peakMeterCtrl1.Peak표시=bool.Parse(a["LeftMeterPeakShow"]);}catch{}
                try{if(a.ContainsKey("LeftMeterPeakBolder"))peakMeterCtrl1.Peak굵기=Convert.ToInt32(a["LeftMeterPeakBolder"]);}catch{}
                try{if(a.ContainsKey("LeftMeterPeakSpeed"))peakMeterCtrl1.Peak속도=Convert.ToInt32(a["LeftMeterPeakSpeed"]);}catch{}
                try{if(a.ContainsKey("LeftMeterPeakColor"))peakMeterCtrl1.Peak색상=HS_CSharpUtility.Utility.ConvertUtility.StringToColor(a["LeftMeterPeakColor"]);}catch{}
                //[RightMeter]
                try{if(a.ContainsKey("RightMeterBandsCount"))peakMeterCtrl2.BandsCount=Convert.ToInt32(a["RightMeterBandsCount"]);}catch{}
                try{if(a.ContainsKey("RightMeterGridShow"))peakMeterCtrl2.모눈선표시=bool.Parse(a["RightMeterGridShow"]);}catch{}
                try{if(a.ContainsKey("RightMeterGridColor"))peakMeterCtrl2.모눈선색상=HS_CSharpUtility.Utility.ConvertUtility.StringToColor(a["RightMeterGridColor"]);}catch{}
                try{if(a.ContainsKey("RightMeterColorBackShow"))peakMeterCtrl2.색상표시=bool.Parse(a["RightMeterColorBackShow"]);}catch{}
                try{if(a.ContainsKey("RightMeterBackgroundColor"))peakMeterCtrl2.BackColor=HS_CSharpUtility.Utility.ConvertUtility.StringToColor(a["RightMeterBackgroundColor"]);}catch{}
                try{if(a.ContainsKey("RightMeterColorHigh"))peakMeterCtrl2.ColorHigh=HS_CSharpUtility.Utility.ConvertUtility.StringToColor(a["RightMeterColorHigh"]);}catch{}
                try{if(a.ContainsKey("RightMeterColorHighBack"))peakMeterCtrl2.ColorHighBack=HS_CSharpUtility.Utility.ConvertUtility.StringToColor(a["RightMeterColorHighBack"]);}catch{}
                try{if(a.ContainsKey("RightMeterColorMedium"))peakMeterCtrl2.ColorMedium=HS_CSharpUtility.Utility.ConvertUtility.StringToColor(a["RightMeterColorMedium"]);}catch{}
                try{if(a.ContainsKey("RightMeterColorMediumBack"))peakMeterCtrl2.ColorMediumBack=HS_CSharpUtility.Utility.ConvertUtility.StringToColor(a["RightMeterColorMediumBack"]);}catch{}
                try{if(a.ContainsKey("RightMeterColorNormal"))peakMeterCtrl2.ColorNormal=HS_CSharpUtility.Utility.ConvertUtility.StringToColor(a["RightMeterColorNormal"]);}catch{}
                try{if(a.ContainsKey("RightMeterColorNormalBack"))peakMeterCtrl2.ColorNormalBack=HS_CSharpUtility.Utility.ConvertUtility.StringToColor(a["RightMeterColorNormalBack"]);}catch{}
                try{if(a.ContainsKey("RightMeterPeakShow"))peakMeterCtrl2.Peak표시=bool.Parse(a["RightMeterPeakShow"]);}catch{}
                try{if(a.ContainsKey("RightMeterPeakBolder"))peakMeterCtrl2.Peak굵기=Convert.ToInt32(a["RightMeterPeakBolder"]);}catch{}
                try{if(a.ContainsKey("RightMeterPeakSpeed"))peakMeterCtrl2.Peak속도=Convert.ToInt32(a["RightMeterPeakSpeed"]);}catch{}
                try{if(a.ContainsKey("RightMeterPeakColor"))peakMeterCtrl2.Peak색상=HS_CSharpUtility.Utility.ConvertUtility.StringToColor(a["RightMeterPeakColor"]);}catch{}

                try{if(a.ContainsKey("IsBackgrountTransparent"))배경투명ToolStripMenuItem_Click(null, null);}catch{}

                try
                {
                    d1 = DecibelsforSound(LDB);
                    d2 = DecibelsforSound(RDB);
                    lbl_RightPeak.BackColor = d1 >= 0 ? Color.Red : White_1;
                    lbl_RightPeak.ForeColor = d1 >= 0 ? White_1 : Color.Black;
                    lbl_LeftPeak.BackColor = d2 >= 0 ? Color.Red : White_1;
                    lbl_LeftPeak.ForeColor = d2 >= 0 ? White_1 : Color.Black;

                    d1 = DecibelsforSound(MaxLdB);
                    d2 = DecibelsforSound(MaxRdB);
                    lbl_RightPeakValue.BackColor = d1 >= 0 ? Color.Red : Color.Yellow;
                    lbl_RightPeakValue.ForeColor = d1 >= 0 ? White_1 : Color.Black;
                    lbl_LeftPeakValue.BackColor = d2 >= 0 ? Color.Red : Color.Yellow;
                    lbl_LeftPeakValue.ForeColor = d2 >= 0 ? White_1 : Color.Black;

                    d1 = d2 = float.NegativeInfinity;
                }catch{}
            }
        }
        
        public void LoadSetting(Dictionary<string, string> Setting)
        {
            this.Setting = Setting;
        }       
        public Dictionary<string, string> SaveSetting()
        {
            return Setting;
        }
        #endregion

        #region
        public bool GetData = true;
        public int Length = 1;
        public int Interval = 25;
        #endregion
        #region
        public void Start(){ timer1.Start();}
        public void Stop() { timer1.Stop(); }
        #endregion

        float _MaxLdB = float.NegativeInfinity, 
              _MaxRdB = float.NegativeInfinity;
        public float MaxLdB { get{return _MaxLdB;} private set{_MaxLdB = value;} }
        public float MaxRdB { get{return _MaxRdB;} private set{_MaxRdB = value;} }
        string MaxLdBtxt = "-Inf.", MaxRdBtxt = "-Inf.";
        string Musicpath;
        int[] h = new int[2];
        public float[] Ltmp { get; internal set; } 
        public float[] Rtmp{get;internal set;}
        float LDB=float.NegativeInfinity, RDB=float.NegativeInfinity;
        double d1, d2;
        string Ltxt, Rtxt;
        private Color White_1 = Color.FromArgb(254, 255, 255);
        private void timer1_Tick(object sender, EventArgs e)
        {
            if (Data.system != null)
            {
                if (Ltmp.Length == 0) { Ltmp = new float[DefaultLED+1]; } if (Rtmp.Length == 0) { Rtmp = new float[DefaultLED]; }
                if (numchannels == 1)
                {
                    if (GetData) { Data.system.getWaveData(Ltmp, Ltmp.Length, 0); }
                    else { Data.channel.getWaveData(Ltmp, Ltmp.Length, 0); }
                    for (int i = 0; i < Ltmp.Length; i++) { if (Ltmp[i] > LDB) { LDB = Ltmp[i]; } }
                    RDB = LDB;
                    //LDB = Ltmp[Ltmp.Length / 2];
                    //RDB = Rtmp[Rtmp.Length / 2];
                }
                if (numchannels == 2)
                {
                    if (GetData) { Data.system.getWaveData(Ltmp, Ltmp.Length, 0); Data.system.getWaveData(Rtmp, Rtmp.Length < 2 ? 1 : Rtmp.Length, 1); }
                    else { Data.channel.getWaveData(Ltmp, Ltmp.Length, 0); Data.channel.getWaveData(Rtmp, Rtmp.Length, 1); }
                    for (int i = 0; i < Ltmp.Length; i++) { if (Ltmp[i] > LDB) { LDB = Ltmp[i]; } }
                    for (int i = 0; i < Rtmp.Length; i++) { if (Rtmp[i] > RDB) { RDB = Rtmp[i]; } }
                    //LDB = Ltmp[Ltmp.Length / 2];
                    //RDB = Rtmp[Rtmp.Length / 2];
                }

                if (MaxLdB < LDB) { MaxLdB = LDB; MaxLdBtxt = ConvertdBToString(MaxLdB, true); }
                if (MaxRdB < RDB) { MaxRdB = RDB; MaxRdBtxt = ConvertdBToString(MaxRdB, true); }
                //Ltxt = ConvertdBToString(LDB, "0.000");Rtxt = ConvertdBToString(RDB, "0.000");
                Ltxt = ConvertdBToString(LDB, true);Rtxt = ConvertdBToString(RDB, true);
                /*
                if (LDB.ToString().IndexOf("-3.899998E-19")!=-1 || 
                    LDB.ToString().IndexOf("7.39567E-17")!=-1||
                    LDB.ToString().IndexOf("7.395671E-17")!=-1||
                    LDB.ToString().IndexOf("5.277585") != -1 ||
                    LDB == 0) { Ltxt = "-Inf."; } else { Ltxt = Decibels(LDB, 0).ToString("0.000"); }

                if (RDB.ToString().IndexOf("-3.899998E-19") != -1 ||
                    RDB.ToString().IndexOf("7.39567E-17") != -1 ||
                    RDB.ToString().IndexOf("7.395671E-17") != -1 ||
                    RDB.ToString().IndexOf("5.277585") != -1 ||
                    RDB == 0) { Rtxt = "-Inf."; } else { Rtxt = Decibels(RDB, 0).ToString("0.000"); }
                 */
                //peakMeterCtrl1.SetRange(15, 25, 30);//100, 115, 120);
                //peakMeterCtrl2.SetRange(15, 25, 30);//100, 115, 120);

                if (Data.MusicPath != Musicpath) { Musicpath = Data.MusicPath; MaxLdB = MaxRdB = float.NegativeInfinity; MaxLdBtxt = MaxRdBtxt = "-Inf."; }
                this.Text = string.Format("Peak 미터 [좌: {0} dB / 우: {1} dB] ({2}) - Peak [좌: {3} dB / 우: {4} dB]",
                    Ltxt, Rtxt, LDB == RDB ? "Mono" : "Stereo", MaxLdBtxt, MaxRdBtxt);
                lbl_LeftPeakValue.Text = ConvertdBToString(LDB, true);
                lbl_RightPeakValue.Text = ConvertdBToString(RDB, true);
                

                /*                    
                    if(d1>=0){lbl_LeftPeakValue.BackColor = Color.Red;lbl_LeftPeakValue.ForeColor = White_1;}
                    else if (d1 >= -6 && d1 < 0){ lbl_LeftPeakValue.BackColor = Color.Black;lbl_LeftPeakValue.ForeColor = White_1;}
                    else{ lbl_LeftPeakValue.BackColor = Color.DarkGreen;lbl_LeftPeakValue.ForeColor = White_1;}}*/

                d1 = DecibelsforSound(LDB);
                if (d1 != 0)
                {
                    if (색깔적용ToolStripMenuItem.Checked)
                    {
                        if (d1 >= 0) lbl_LeftPeakValue.BackColor = Color.Red;
                        else if (d1 >= -6 && d1 < 0) lbl_LeftPeakValue.BackColor = Color.Black;
                        else lbl_LeftPeakValue.BackColor = Color.DarkGreen;
                        lbl_LeftPeakValue.ForeColor = White_1;
                    }
                    else
                    {
                        lbl_LeftPeakValue.BackColor = Color.Transparent;
                        lbl_LeftPeakValue.ForeColor = White_1;
                    }
                    //lbl_LeftPeakValue.ForeColor = d1 > 0 ? White_1 : Color.Black;
                    //lbl_LeftPeakValue.ForeColor = Color.FromArgb(0,0,255);//White_1;

                    d1 = Math.Min(d1, 0);
                    int b = peakMeterCtrl1.LED눈금갯수<1?10:peakMeterCtrl1.LED눈금갯수;
                    h[0] = (int)Math.Max(b+d1, 0);//(int)Math.Max((d1 + (peakMeterCtrl1.LED눈금갯수 >= 20 ? peakMeterCtrl1.LED눈금갯수 : 20)), 0);
                    peakMeterCtrl1.SetData(h, 0, 1, false);
                }
                else {h[0] = peakMeterCtrl1.LED눈금갯수-1; peakMeterCtrl1.SetData(h, 0, 1, false);lbl_LeftPeakValue.BackColor = Color.Red;lbl_LeftPeakValue.ForeColor = White_1; }
                
                d2 = DecibelsforSound(RDB);
                if (d2 != 0)//Decibels(RDB, 0) != 0)
                {
                    if (색깔적용ToolStripMenuItem.Checked)
                    {
                        if (d2 >= 0) lbl_RightPeakValue.BackColor = Color.Red;
                        else if (d2 >= -6 && d2 < 0) lbl_RightPeakValue.BackColor = Color.Black;
                        else lbl_RightPeakValue.BackColor = Color.DarkGreen;
                        lbl_RightPeakValue.ForeColor = White_1;
                    }
                    else
                    {
                        lbl_RightPeakValue.BackColor = Color.Transparent;
                        lbl_RightPeakValue.ForeColor = White_1;
                    }
                    //lbl_RightPeakValue.ForeColor = d2 > 0 ? White_1 : Color.Black;

                    d2 = Math.Min(d2, 0);
                    int b = peakMeterCtrl2.LED눈금갯수<1?10:peakMeterCtrl2.LED눈금갯수;
                    h[0] = (int)Math.Max(b+d2, 0);//(int)Math.Max((d2 + (peakMeterCtrl2.LED눈금갯수 >= 20 ? peakMeterCtrl2.LED눈금갯수 : 20)), 0);
                    peakMeterCtrl2.SetData(h, 0, 1, false);
                }else{h[0] = peakMeterCtrl2.LED눈금갯수-1; peakMeterCtrl2.SetData(h, 0, 1, false);lbl_RightPeakValue.BackColor = Color.Red; lbl_RightPeakValue.ForeColor = White_1;}
                

                lbl_LeftPeak.Text = ConvertdBToString(MaxLdB, true);
                lbl_RightPeak.Text = ConvertdBToString(MaxRdB, true);
                d1 = DecibelsforSound(MaxLdB);
                if (d1 != 0)
                {
                    if (색깔적용ToolStripMenuItem.Checked)
                    {
                        if (d1 >= 0) lbl_LeftPeak.BackColor = Color.Red;
                        else if (d1 >= -6 && d1 < 0) lbl_LeftPeak.BackColor = Color.Black;
                        else lbl_LeftPeak.BackColor = Color.DarkGreen;
                        lbl_LeftPeak.ForeColor = White_1;
                    }
                    else
                    {
                        lbl_LeftPeak.BackColor = Color.Transparent;
                        lbl_LeftPeak.ForeColor = White_1;
                    }
                    //lbl_LeftPeak.BackColor = d1 >= 0 ? Color.Red : White_1;
                    //lbl_LeftPeak.ForeColor = d1 >= 0 ? White_1 : Color.Black;
                }else {lbl_LeftPeak.BackColor = Color.Red;lbl_LeftPeak.ForeColor = White_1;}
                
                d2 = DecibelsforSound(MaxRdB);
                if (d2 != 0)
                {                    
                    if (색깔적용ToolStripMenuItem.Checked)
                    {
                        if (d2 >= 0) lbl_RightPeak.BackColor = Color.Red;
                        else if (d2 >= -6 && d2 < 0) lbl_RightPeak.BackColor = Color.Black;
                        else lbl_RightPeak.BackColor = Color.DarkGreen;
                        lbl_RightPeak.ForeColor = White_1;
                    }
                    else
                    {
                        lbl_RightPeak.BackColor = Color.Transparent;
                        lbl_RightPeak.ForeColor = White_1;
                    }
                    //lbl_RightPeak.BackColor = d1 >= 0 ? Color.Red : White_1;
                    //lbl_RightPeak.ForeColor = d1 >= 0 ? White_1 : Color.Black;
                }else {lbl_RightPeak.BackColor = Color.Red;lbl_RightPeak.ForeColor = White_1;}
            }
            LDB = RDB = float.NegativeInfinity;
        }

        string _ConvertdBToString;
        public string ConvertdBToString(float Sample, bool PlusWhen0Over = false)
        {
            if (Sample == float.PositiveInfinity) { _ConvertdBToString = "Inf."; }
            else if (DecibelsforSound(Sample) < -360 || Sample == float.NegativeInfinity || Sample == 0) _ConvertdBToString = "-Inf.";
            else
            {
                double db = DecibelsforSound(Sample);
                if (PlusWhen0Over && (db>-0.0004&&db<0))_ConvertdBToString = db.ToString(" 0.000");
                else if (PlusWhen0Over && db == 0) _ConvertdBToString = " 0.000";
                //else if (PlusWhen0Over && (db<0.0005&&db > 0)) _ConvertdBToString = db.ToString("+0.000");
                else if (PlusWhen0Over && db > 0) _ConvertdBToString = db.ToString("+0.000");
                else _ConvertdBToString = db.ToString("0.000");
            }
            return _ConvertdBToString;
        }
        public string ConvertdBToString(float Sample, string Format)
        {
            if (Sample == float.PositiveInfinity) { _ConvertdBToString = "Inf."; }
            else if (DecibelsforSound(Sample) < -360 || Sample == float.NegativeInfinity || Sample == 0) _ConvertdBToString = "-Inf.";
            else if(Sample==0)_ConvertdBToString = Sample.ToString(Format);
            return _ConvertdBToString;
        }

        public static double mag_sqrd(double re, double im) { return (re * re + im * im); }

        public static double Decibels(double re, double im) {if(re==double.NegativeInfinity)return re; return ((re == 0 && im == 0) ? (double.NaN) : 10.0 * Math.Log10(((mag_sqrd(re, im))))); }
        public static double DecibelsforSound(double Sample, double Peak = 0) {if(Sample==double.NegativeInfinity)return Sample; return ((Sample == 0 && Peak == 0) ? (double.NegativeInfinity) : 10.0 * Math.Log10(((mag_sqrd(Sample, Peak))))); }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
             
        }

        private void peakMeterCtrl1_SizeChanged(object sender, EventArgs e)
        {
            //Ltmp = new float[peakMeterCtrl1.Width];
        }

        private void peakMeterCtrl2_SizeChanged(object sender, EventArgs e)
        {
            //Rtmp = new float[peakMeterCtrl2.Width];
        }

        Form f = new Form()
        {
            //MdiParent = this,
            Size = new Size(300, 350),
            Text = "Peak 설정 (좌)...",
            ShowIcon = false,
        };

        Form f1 = new Form()
        {
            //MdiParent = this,
            Size = new Size(300, 350),
            Text = "Peak 설정 (우)...",
            ShowIcon = false,
        };

        private void Form1_Load(object sender, EventArgs e)
        {
            f.FormClosing+=new FormClosingEventHandler(f_FormClosing);
            f1.FormClosing+=new FormClosingEventHandler(f1_FormClosing);
            SetStyle(ControlStyles.SupportsTransparentBackColor, true);
            peakMeterCtrl1.LED눈금갯수 = peakMeterCtrl2.LED눈금갯수 = 40;
            peakMeterCtrl1.Start(peakMeterCtrl1.Peak속도);
            peakMeterCtrl2.Start(peakMeterCtrl2.Peak속도);
            contextMenuStrip1.ImageScalingSize = new Size(16, 16);
        }

        private void f1_FormClosing(object sender, FormClosingEventArgs e) { e.Cancel = !CloseForm; f1.Hide(); }
        private void f_FormClosing(object sender, FormClosingEventArgs e) { e.Cancel = !CloseForm; f.Hide(); }

        //public bool Closing = true;
        private void frmPeakMeter_FormClosing(object sender, FormClosingEventArgs e)
        {
            //e.Cancel = !Closing;
            this.Hide();
        }


        public void peakMeterCtrl1_PropertiesChanged(string Name, Type type, object Value)
        {
            if (type == typeof(int) && Name == "LED눈금갯수")
            {
                /*
                if (peakMeterCtrl1.LED눈금갯수 >= 30)
                { peakMeterCtrl1.SetRange(peakMeterCtrl1.LED눈금갯수 - 15, peakMeterCtrl1.LED눈금갯수 - 5, peakMeterCtrl1.LED눈금갯수); }//100, 140, 200);}}
                else { peakMeterCtrl1.SetRange(15, 25, 30); }*/
                float Avg = peakMeterCtrl1.LED눈금갯수/6f;
                if (peakMeterCtrl1.LED눈금갯수 < 12)
                {
                    float Bottom = Math.Max(Avg * 3.4f, (int)(Avg * 3.4f));
                    float Mid = Math.Max(Bottom + (Avg * 2), (int)Avg * 5.4f);
                    float Top = Math.Min(Mid + Avg, peakMeterCtrl1.LED눈금갯수);
                    peakMeterCtrl1.SetRange(((int)Bottom) - 1, ((int)Mid) - 1, peakMeterCtrl1.LED눈금갯수);
                }
                else
                {
                    float Bottom = Math.Max(Avg * 3.2f, (int)(Avg * 3.2f));
                    float Mid = Math.Max(Bottom + (Avg * 2), (int)Avg * 5.2f);
                    float Top = Math.Min(Mid + Avg, peakMeterCtrl1.LED눈금갯수);
                    peakMeterCtrl1.SetRange(((int)Bottom) - 1, ((int)Mid) - 1, peakMeterCtrl1.LED눈금갯수);
                    if (peakMeterCtrl1.LED눈금갯수 > 32) peakMeterCtrl1.SetRange(peakMeterCtrl1.LED눈금갯수 - 18, peakMeterCtrl1.LED눈금갯수-6, peakMeterCtrl1.LED눈금갯수);
                }
            }
        }
        private void peakMeterCtrl2_PropertiesChanged(string Name, Type type, object Value)
        {
            if (type == typeof(int) && Name == "LED눈금갯수")
            {
                //1:2:3
                /*
                if (peakMeterCtrl2.LED눈금갯수 >= 30)
                {peakMeterCtrl2.SetRange(peakMeterCtrl2.LED눈금갯수 - 15, peakMeterCtrl2.LED눈금갯수 - 5, peakMeterCtrl2.LED눈금갯수);}//100, 140, 200);}}
                else { peakMeterCtrl2.SetRange(15, 25, 30); }*/
                float Avg = peakMeterCtrl2.LED눈금갯수 / 6f;
                if (peakMeterCtrl2.LED눈금갯수 < 12)
                {
                    float Bottom = Math.Max(Avg * 3.4f, (int)(Avg * 3.4f));
                    float Mid = Math.Max(Bottom + (Avg * 2), (int)Avg * 5.4f);
                    float Top = Math.Min(Mid + Avg, peakMeterCtrl2.LED눈금갯수);
                    peakMeterCtrl2.SetRange(((int)Bottom) - 1, ((int)Mid) - 1, peakMeterCtrl2.LED눈금갯수);
                }
                else
                {
                    float Bottom = Math.Max(Avg * 3.2f, (int)(Avg * 3.2f));
                    float Mid = Math.Max(Bottom + (Avg * 2), (int)Avg * 5.2f);
                    float Top = Math.Min(Mid + Avg, peakMeterCtrl2.LED눈금갯수);
                    peakMeterCtrl2.SetRange(((int)Bottom) - 1, ((int)Mid) - 1, peakMeterCtrl2.LED눈금갯수);
                    if (peakMeterCtrl2.LED눈금갯수 > 32) peakMeterCtrl2.SetRange(peakMeterCtrl2.LED눈금갯수 - 18, peakMeterCtrl2.LED눈금갯수 - 6, peakMeterCtrl2.LED눈금갯수);
                }
            }
        }

        private void 설정창띄우기ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            f.Show();
        }

        private void 설정창띄우기우ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            f1.Show();
        }

        private void 가로배열ToolStripMenuItem_CheckedChanged(object sender, EventArgs e)
        {

        }

        Orientation tmpOrientation;
        private void splitContainer1_Panel2_Resize(object sender, EventArgs e)
        {
            if (splitContainer1.Orientation != tmpOrientation)
            {
                if (splitContainer1.Orientation == Orientation.Vertical)
                {
                    splitContainer1.SplitterDistance = splitContainer1.Width / 2;
                }
                else
                {
                    splitContainer1.SplitterDistance = splitContainer1.Height / 2;
                }
                tmpOrientation = splitContainer1.Orientation;
            }
        }

        private void 세로배열ToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void toolStripMenuItem2_TextChanged(object sender, EventArgs e)
        {
            int a;
            try
            {
                if (toolStripMenuItem2.Text == "") a = DefaultLED;
                else a = int.Parse(toolStripMenuItem2.Text);
                //if (a <= 1) a = DefaultLED;
                peakMeterCtrl1.LED눈금갯수 = peakMeterCtrl2.LED눈금갯수 = a > 0 ? a : 0;
            }
            catch
            {
                toolStripMenuItem2.Text = "40";
                a = 40;
                peakMeterCtrl1.LED눈금갯수 = peakMeterCtrl2.LED눈금갯수 = a > 0 ? a : 0;

            }
            //Ltmp = new float[a+1];Rtmp = new float[a];
        }

        private void fMODChannelToolStripMenuItem_CheckedChanged(object sender, EventArgs e)
        {

        }

        FormBorderStyle a;
        Rectangle b;
        Point Loc;
        bool FullScreen;
        private void peakMeterCtrl1_DoubleClick(object sender, EventArgs e)
        {
            if (!FullScreen)//this.FormBorderStyle != System.Windows.Forms.FormBorderStyle.None)
            {
                a = this.FormBorderStyle; b = new Rectangle(this.Top, this.Left, this.Width, this.Height);
                this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                FullScreen = true;
                Loc = this.Location;
                Left = Top = 0;
                Width = Screen.PrimaryScreen.WorkingArea.Width;
                Height = Screen.PrimaryScreen.WorkingArea.Height;
            }
            else
            {
                this.FormBorderStyle = 타이틀표시ToolStripMenuItem.Checked?
                    System.Windows.Forms.FormBorderStyle.SizableToolWindow:
                    System.Windows.Forms.FormBorderStyle.None;
                this.Location = new Point(Loc.X, Loc.Y);
                this.Top = b.X; this.Left = b.Y; this.Width = b.Width; this.Height = b.Height;
                FullScreen = false;
            }
        }

        internal void peakMeterCtrl1_Resize(object sender, EventArgs e)
        {
            //try { Ltmp = new float[peakMeterCtrl1.Width]; }catch{}
        }

        internal void peakMeterCtrl2_Resize(object sender, EventArgs e)
        {
            //try { Rtmp = new float[peakMeterCtrl2.Width]; }catch{}
        }

        private void 맨위에표시ToolStripMenuItem_CheckedChanged(object sender, EventArgs e)
        {
            this.TopMost = 맨위에표시ToolStripMenuItem.Checked;
        }

        private void 스펙트럼할당방법ToolStripMenuItem_DropDownOpening(object sender, EventArgs e)
        {
            if (Data.system != null) { fMODSystemToolStripMenuItem.Enabled = true; }
            else { fMODSystemToolStripMenuItem.Enabled = false; }
            if (Data.channel != null) { fMODChannelToolStripMenuItem.Enabled = true; }
            else { fMODChannelToolStripMenuItem.Enabled = false; }
        }

        private void fMODSystemToolStripMenuItem_Click(object sender, EventArgs e)
        {
            fMODSystemToolStripMenuItem.Checked = true;
            fMODChannelToolStripMenuItem.Checked = false;
            GetData = true;
        }

        private void fMODChannelToolStripMenuItem_Click(object sender, EventArgs e)
        {
            fMODSystemToolStripMenuItem.Checked = false;
            fMODChannelToolStripMenuItem.Checked = true;
            GetData = false;
            /*
            if (fMODSystemToolStripMenuItem.Checked)
            {

            }
            else
            {

            }*/
        }

        int a1;
        private void toolStripTextBox1_TextChanged(object sender, EventArgs e)
        {
            try{a1 = int.Parse(toolStripTextBox1.Text);}catch{a1 = 25;toolStripTextBox1.Text = "25";}
            if(a1<1)timer1.Stop();else timer1.Start();
            this.timer1.Interval=a1<1?1:a1;
        }

        private void peak새로고침ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MaxLdB = MaxRdB = float.NegativeInfinity;
        }

        private void 그래프가로배열ToolStripMenuItem_CheckedChanged(object sender, EventArgs e)
        {
            if (그래프가로배열ToolStripMenuItem.Checked)
                peakMeterCtrl1.MeterStyle = peakMeterCtrl2.MeterStyle = Ernzo.WinForms.Controls.PeakMeterStyle.PMS_Vertical;
            else
                peakMeterCtrl1.MeterStyle = peakMeterCtrl2.MeterStyle = Ernzo.WinForms.Controls.PeakMeterStyle.PMS_Horizontal;
        }

        private void 분할자가로배열ToolStripMenuItem_CheckedChanged(object sender, EventArgs e)
        {
            if (분할자세로배열ToolStripMenuItem.Checked) splitContainer1.Orientation = Orientation.Horizontal;
            else splitContainer1.Orientation = Orientation.Vertical;
        }

        private void peakMeterCtrl1_MouseDown(object sender, MouseEventArgs e)
        {
            설정창띄우기좌ToolStripMenuItem.Visible = true;
            설정창띄우기우ToolStripMenuItem.Visible = false;
        }

        private void peakMeterCtrl2_MouseDown(object sender, MouseEventArgs e)
        {
            설정창띄우기좌ToolStripMenuItem.Visible = false;
            설정창띄우기우ToolStripMenuItem.Visible = true;
        }

        private void toolTip1_Draw(object sender, DrawToolTipEventArgs e)
        {
            e.DrawBackground();
            e.DrawBorder();
            e.DrawText();
        }

        private void 배경투명ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //Color c= Color.FromName("Gainsboro");
            if (!배경투명ToolStripMenuItem.Checked)
            {
                //splitContainer1.BackColor = SystemColors.Control;
                c[0] = peakMeterCtrl1.BackColor;
                c[1] = peakMeterCtrl2.BackColor;
                c[2] = peakMeterCtrl1.모눈선색상;
                c[3] = peakMeterCtrl2.모눈선색상;
                c[4] = splitContainer1.BackColor;
                this.BackColor = Color.Transparent;
                //this.TransparencyKey = peakMeterCtrl1.모눈선색상;
                if (분할자투명ToolStripMenuItem.Checked) splitContainer1.BackColor = Color.Transparent;
                toolStripTextBox2.Enabled = true;
                this.peakMeterCtrl1.모눈선색상 = this.peakMeterCtrl2.모눈선색상 =
                this.peakMeterCtrl1.BackColor = this.peakMeterCtrl2.BackColor = Color.Transparent;
                toolStripTextBox2_TextChanged(null, null);
                toolStripTextBox2.Enabled = 분할자투명ToolStripMenuItem.Enabled = true;
                배경투명ToolStripMenuItem.Checked = true;
            }
            else
            {
                this.TransparencyKey = Color.Empty;
                toolStripTextBox2.Enabled = false;
                peakMeterCtrl1.BackColor = c[0];
                peakMeterCtrl2.BackColor = c[1];
                peakMeterCtrl1.모눈선색상 = c[2];
                peakMeterCtrl2.모눈선색상 = c[3];
                splitContainer1.BackColor = c[4];
                toolStripTextBox2.Enabled = 배경투명ToolStripMenuItem.Checked = false;
            }
            //분할자투명ToolStripMenuItem_CheckedChanged(null, null);
        }

        Color[] c = new Color[5];

        private void toolStripTextBox2_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == Convert.ToChar(Keys.Enter)) { toolStripTextBox2_TextChanged(null, null); }
            if (!(Char.IsDigit(e.KeyChar)) && e.KeyChar != Convert.ToChar(Keys.Back)&&
                e.KeyChar != 'A' && e.KeyChar != 'B' && e.KeyChar != 'C' && e.KeyChar != 'D' && e.KeyChar != 'E' && e.KeyChar!= 'F')
            { e.Handled = true; }
        }

        private void toolStripTextBox2_TextChanged(object sender, EventArgs e)
        {
            if (!toolStripTextBox2.Text.Contains("#")) {toolStripTextBox2.Text="#"+toolStripTextBox2.Text; }
            int a = Convert.ToInt32(toolStripTextBox2.Text.Replace("#", ""), 16);
            this.TransparencyKey=Color.FromArgb(a);
            this.BackColor =splitContainer1.Panel1.BackColor = splitContainer1.Panel2.BackColor = this.TransparencyKey;
            //splitContainer1.BackColor = SystemColors.ControlDark;
        }

        private void 테두리표시ToolStripMenuItem_CheckedChanged(object sender, EventArgs e)
        {
            if (테두리표시ToolStripMenuItem.Checked)
            {splitContainer1.BorderStyle = BorderStyle.FixedSingle; }
            else { splitContainer1.BorderStyle = BorderStyle.None; }
        }

        private void 분할자투명ToolStripMenuItem_CheckedChanged(object sender, EventArgs e)
        {
            if (분할자투명ToolStripMenuItem.Checked)
            {c[4] = splitContainer1.BackColor; splitContainer1.BackColor = Color.Transparent;}
            else{splitContainer1.BackColor = c[4];}
        }

        private void peakMeterCtrl1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyValue == 115) { 배경투명ToolStripMenuItem_Click(null, null); }
        }

        private void toolStripTextBox3_TextChanged(object sender, EventArgs e)
        {
            int a;
            if (toolStripTextBox3.Text == "") a = 2048;
            else a = int.Parse(toolStripTextBox3.Text);
            Ltmp = InitData(a < 1?2048:a, float.NegativeInfinity);
            Rtmp = InitData(a < 1?2048:a, float.NegativeInfinity);
        }

        private void 가로미터ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            float X = CaculatorLengthforPercent(SystemInformation.PrimaryMonitorSize.Width, 5);
            float Y = CaculatorLengthforPercent(SystemInformation.PrimaryMonitorSize.Height, 10);

            this.Location = new Point((int)X, (int)Y);
            그래프가로배열ToolStripMenuItem.Checked = 분할자세로배열ToolStripMenuItem.Checked = true;
            this.Size = new Size(this.Height, this.Width);
        }

        private void 세로미터ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            float X = CaculatorLengthforPercent(SystemInformation.PrimaryMonitorSize.Width, 5);
            float Y = CaculatorLengthforPercent(SystemInformation.PrimaryMonitorSize.Height, 10);

            this.Location = new Point((int)X, (int)Y);
            그래프가로배열ToolStripMenuItem.Checked=분할자세로배열ToolStripMenuItem.Checked = false;
            this.Size = new Size(this.Height, this.Width);//99, 800
        }
        /*
        public enum FormLocation{Top, Left, Bottom, Right, Center, Hide};
        public FormLocation GetFormLocation
        {
            
        }*/

        private void 타이틀표시ToolStripMenuItem_CheckedChanged(object sender, EventArgs e)
        {
            this.FormBorderStyle = 타이틀표시ToolStripMenuItem.Checked?
                System.Windows.Forms.FormBorderStyle.SizableToolWindow:
                System.Windows.Forms.FormBorderStyle.None;
            /*
            if (타이틀표시ToolStripMenuItem.Checked)
            {
                this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                타이틀표시ToolStripMenuItem.Checked = false;
            }
            else
            {
                this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
                타이틀표시ToolStripMenuItem.Checked = true;
            }*/
        }

        private void contextMenuStrip1_Opening(object sender, System.ComponentModel.CancelEventArgs e)
        {
            타이틀표시ToolStripMenuItem.Enabled = 가로미터ToolStripMenuItem.Enabled=세로미터ToolStripMenuItem.Enabled=!FullScreen;
        }


        
        public static float CaculatorLengthforPercent(int n, float Percent)
        {
            return (n/100f)*Percent;
        }
        public static double CaculatorPercent(long Current, long longOriginal){return ((float)Current / (float)longOriginal) * 100f; }
        public static float CaculatorPercent(int Current, int longOriginal){return ((float)Current / (float)longOriginal) * 100f; }
        public static float CaculatorPercent(float Current, float longOriginal){return (Current / longOriginal) * 100f; }
        public static double CaculatorPercent(double Current, double longOriginal){return (Current / longOriginal) * 100f; }
        
        private void 저장SToolStripMenuItem_Click(object sender, EventArgs e)
        {
           try{saveFileDialog1.InitialDirectory = SettingDirectory;}catch{}
           DateTime dt = DateTime.Now;
           saveFileDialog1.FileName = string.Format("PeakMeter_{0}-{1}-{2} {3} {4}시 {5}분 {6}초.pkmtini", 
               dt.Year.ToString("0000"), dt.Month.ToString("00"), dt.Day.ToString("00"),
               dt.Hour>11?"오후":"오전", dt.Hour>12?(dt.Hour-12).ToString("00"):dt.Hour.ToString("00"), dt.Minute.ToString("00"), dt.Second.ToString("00"));
            saveFileDialog1.ShowDialog();
        }
        private void 열기OToolStripMenuItem_Click(object sender, EventArgs e)
        {
           try{openFileDialog1.InitialDirectory = SettingDirectory;}catch{}
           openFileDialog1.ShowDialog();
        }

        private void saveFileDialog1_FileOk(object sender, System.ComponentModel.CancelEventArgs e)
        {
            try
            {
                string[] a = HS_CSharpUtility.Utility.EtcUtility.SaveSettingWithoutFilter(Setting);
                System.IO.File.WriteAllLines(saveFileDialog1.FileName, a);
            }
            catch(Exception ex) { MessageBox.Show("설정 저장 실패!!\n\n예외: "+ex.Message, "Peak 미터", MessageBoxButtons.OK, MessageBoxIcon.Error);}
        }

        private void openFileDialog1_FileOk(object sender, System.ComponentModel.CancelEventArgs e)
        {
            try
            {
                string[] a = System.IO.File.ReadAllLines(openFileDialog1.FileName);
                Dictionary<string, string> b = HS_CSharpUtility.Utility.EtcUtility.LoadSetting(a);
                LoadSetting(b);
            }
            catch(Exception ex) { MessageBox.Show("설정 열기 실패!!\n\n예외: "+ex.Message, "Peak 미터", MessageBoxButtons.OK, MessageBoxIcon.Error);}
        }
        
        private void toolStripMenuItem_KeyPress(object sender, KeyPressEventArgs e)
        {
            //if (e.KeyChar == Convert.ToChar(Keys.Enter)) { toolStripMenuItem2_TextChanged(null, null); }
            if (!(Char.IsDigit(e.KeyChar)) && e.KeyChar != Convert.ToChar(Keys.Back))
            { e.Handled = true; }
        }
        
        Point splitContainer1MouseDownLocation;
        MouseButtons splitContainer1MouseButtons = System.Windows.Forms.MouseButtons.None;
        private void splitContainer1_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Left)
            {
                this.Left += e.X - splitContainer1MouseDownLocation.X;
                this.Top += e.Y -splitContainer1MouseDownLocation.Y;
            }
        }

        private void splitContainer1_MouseLeave(object sender, EventArgs e)
        {
            splitContainer1_MouseUp(null,null);
        }

        private void splitContainer1_MouseUp(object sender, MouseEventArgs e)
        {
            splitContainer1MouseButtons = System.Windows.Forms.MouseButtons.None;
            this.Cursor = Cursors.Default;
        }

        private void splitContainer1_MouseDown(object sender, MouseEventArgs e)
        {
            splitContainer1MouseButtons = System.Windows.Forms.MouseButtons.Left;
            splitContainer1MouseDownLocation = e.Location;
            this.Cursor = Cursors.SizeAll;
        }

        private void 레벨레이블보이기ToolStripMenuItem_CheckedChanged(object sender, EventArgs e)
        {
            //peakMeterCtrl1.Size = peakMeterCtrl1.Size-label1.Height
            /*peakMeterCtrl1.Anchor = peakMeterCtrl2.Anchor = 레벨레이블보이기ToolStripMenuItem.Checked ?
                AnchorStyles.Bottom | AnchorStyles.Right | AnchorStyles.Left :
                AnchorStyles.Bottom | AnchorStyles.Right | AnchorStyles.Left | AnchorStyles.Top;*/
            lbl_LeftPeak.Visible = lbl_RightPeak.Visible =
            lbl_LeftPeakValue.Visible = lbl_RightPeakValue.Visible = 레벨레이블보이기ToolStripMenuItem.Checked;
        }

        private void 굵게ToolStripMenuItem_CheckStateChanged(object sender, EventArgs e)
        {
            lbl_LeftPeak.Font = new System.Drawing.Font(lbl_LeftPeak.Font, 굵게ToolStripMenuItem.Checked ? FontStyle.Bold : FontStyle.Regular);
            lbl_RightPeak.Font = new System.Drawing.Font(lbl_RightPeak.Font, 굵게ToolStripMenuItem.Checked ? FontStyle.Bold : FontStyle.Regular);
            lbl_LeftPeakValue.Font = new System.Drawing.Font(lbl_LeftPeakValue.Font, 굵게ToolStripMenuItem.Checked ? FontStyle.Bold : FontStyle.Regular);
            lbl_RightPeakValue.Font = new System.Drawing.Font(lbl_RightPeakValue.Font, 굵게ToolStripMenuItem.Checked ? FontStyle.Bold : FontStyle.Regular);
        }

        private void 테두리ToolStripMenuItem_CheckedChanged(object sender, EventArgs e)
        {
            lbl_LeftPeak.BorderStyle=lbl_LeftPeakValue.BorderStyle=lbl_RightPeak.BorderStyle=lbl_RightPeakValue.BorderStyle =
                테두리ToolStripMenuItem.Checked ? BorderStyle.FixedSingle : BorderStyle.None;
        }

        private void 분할자색상ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (colorDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                splitContainer1.BackColor = colorDialog1.Color;
        }

        private void 글자색변경ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            colorDialog2.Color = White_1;
            colorDialog2.CustomColors[0] = White_1.ToArgb();
            colorDialog2.CustomColors[1] = Color.FromArgb(1,1,1).ToArgb();
            colorDialog2.CustomColors[2] = Color.Transparent.ToArgb();
            colorDialog2.CustomColors[3] = Color.Gray.ToArgb();
            colorDialog2.CustomColors[4] = SystemColors.Control.ToArgb();
            if (colorDialog2.ShowDialog() == System.Windows.Forms.DialogResult.OK)White_1 = colorDialog2.Color;
        }

        private void 닫기ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void lbl_LeftPeak_Click(object sender, EventArgs e)
        {
            MaxLdB = float.NegativeInfinity;
        }

        private void lbl_RightPeak_Click(object sender, EventArgs e)
        {
            MaxRdB = float.NegativeInfinity;
        }

        bool timer1_Start;
        private void frmPeakMeter_WindowShowChanged(object sender, EventArgs e)
        {
            if (WindowState == FormWindowState.Minimized || this.WindowShowStatus == HS_Audio.Control.WindowShowState.Hide)
            {timer1_Start = timer1.Enabled; timer1.Stop();}
            else  {if (timer1_Start) timer1.Start();}
        }
    }
}
