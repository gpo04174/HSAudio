﻿using HS_Audio_Lib;
using System.Text;

namespace HS_Audio.Bundles
{
    public class SoundDeviceFormat
    {
        public SoundDeviceFormat() { }
        public SoundDeviceFormat(int Samplerate, SOUND_FORMAT SoundFormat, int OutputChannel,
                              int MaxInputChannel, DSP_RESAMPLER ResamplerMethod, int Bits)
        {
            this.Samplerate = Samplerate; this.SoundFormat = SoundFormat; this.OutputChannel = OutputChannel;
            this.MaxInputChannel = MaxInputChannel; this.ResamplerMethod = ResamplerMethod; this._Bits = Bits;
        }

        public int Samplerate;
        public SOUND_FORMAT SoundFormat;
        public int OutputChannel;
        public int MaxInputChannel;
        public DSP_RESAMPLER ResamplerMethod;
        public OUTPUTTYPE OutputType;
        public int Bits { get { return _Bits; } }
        public bool IsDefault { get { return _IsDefault; } internal set { _IsDefault = value; } }
        public bool IsDefaultOutputType { get { return _IsDefaultOutputType; } internal set { _IsDefaultOutputType = value; } }

        public bool SamplerateAuto = false;


        internal int _Bits;
        internal bool _IsDefault = true;
        internal bool _IsDefaultOutputType = true;
    }
}
