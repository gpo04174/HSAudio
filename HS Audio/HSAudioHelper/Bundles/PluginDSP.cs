﻿using HS_Audio.DSP;
using HS_Audio_Lib;
using System;
using System.Collections.Generic;
using System.Text;

namespace HS_Audio.Bundles
{
    public abstract class PluginDSP : IDisposable
    {
        [System.Runtime.InteropServices.DllImport("Kernel32")]
        public extern static Boolean CloseHandle(IntPtr handle);

        internal PluginDSP() { }
        internal PluginDSP(int Index) { this._Index = Index; }
        internal PluginDSP(string Path, string Name = null)
        {
            this._Path = Path;
            _Name = (Name == null && (Path != null || Path != "")) ? System.IO.Path.GetFileName(Path) : Name;
        }
        internal PluginDSP(HS_Audio_Lib.PLUGINTYPE PluginType) { this._PluginType = PluginType; }
        internal PluginDSP(int Index, HS_Audio_Lib.PLUGINTYPE PluginType) { this._Index = Index; this._PluginType = PluginType; }

        public void Dispose()
        {
            if (dsp != null) dsp.Dispose();
            if (Dialog != null)
            {
                Dialog.IsClose = true;
                Dialog.Dispose();
            }
            PluginKind = PluginKind.UNKNOWN;
            _Name = null;
            _Handle = _Version = 0;
            _Index = -1;
            _DSPHandle = IntPtr.Zero;
            IsLoad = false;
            try { ByPass = false; } catch { }
            dsp = null;
            Dialog = null;
            //CloseHandle(Handle);
            GC.Collect();
        }

        protected internal PLUGINTYPE _PluginType;
        protected internal uint _Priority;
        [Obsolete("제 플레이어에서는 사용을 하지 않습니다. 대신 Priority 를 씁니다.")]
        protected internal int _Index;
        protected internal uint _Handle;
        protected internal string _Name;
        protected internal uint _Version;
        protected internal string _Path;
        protected internal IntPtr _DSPHandle;
        protected internal bool _IsSystemPlugin;

        public delegate void EffectChangeEventHandler(PluginDSP p, bool ByPass);

        public HS_Audio_Lib.PLUGINTYPE PluginType { get { return _PluginType; } }
        public uint Priority { get { return _Priority; } }
        [Obsolete("제 플레이어에서는 사용을 하지 않습니다. 대신 Priority 를 씁니다.")]
        public int Index { get { return _Index; } }
        public uint Handle { get { return _Handle; } }
        public string FullName { get { return _Name; } }
        public string Name
        {
            get
            {
                if (FullName == null) return null;
                if (FullName.Remove(3).ToUpper() == "VST") return FullName.Substring(3 + 1);
                else if (FullName.Remove(6).ToUpper() == "WINAMP") return FullName.Substring(6 + 1);
                else return FullName;
            }
        }
        public uint Version { get { return _Version; } }
        public string Path { get { return _Path; } }
        public bool IsSystemPlugin { get; internal set; }
        // public IntPtr DSPHandle{get{return _DSPHandle;}}
        public HSAudioDSPHelper dsp { get; internal set; }
        public bool IsLoad { get; internal set; }
        public HS_Audio.Plugin.frmVSTPluginHost Dialog { get; internal set; }
        public bool ByPass
        {
            get { return dsp != null ? dsp.Bypass : false; }
            internal set { if (dsp != null) dsp.Bypass = value; try { EffectChange(this, value); } catch { } }
        }
        public PluginKind PluginKind { get; internal set; }

        public event EffectChangeEventHandler EffectChange
        #region 숨겨진 기능
            = new EffectChangeEventHandler(EffectChanged_Internal);
        private static void EffectChanged_Internal(PluginDSP p, bool ByPass) { }
        #endregion

        public override string ToString()
        {
            return FullName;
        }
    }

    internal class PluginDSPInstance : PluginDSP
    {
        public PluginDSPInstance() : base() { }
        public PluginDSPInstance(int Index) : base(Index) { }
        public PluginDSPInstance(string Path, string Name = null) : base(Path, Name) { }
        public PluginDSPInstance(HS_Audio_Lib.PLUGINTYPE PluginType) : base(PluginType) { }
        public PluginDSPInstance(int Index, HS_Audio_Lib.PLUGINTYPE PluginType) : base(Index, PluginType) { }
    }
}
