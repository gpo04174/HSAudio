﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HS_Audio.Bundles
{
    public class SoundDevice
    {
        internal protected SoundDevice(int ID, string Name, HS_Audio_Lib.GUID guid) { this.ID = ID; this.Name = Name; this.GUID = guid; }
        protected SoundDevice() { }
        public int ID { get; protected internal set; }
        public string Name { get; protected internal set; }
        public HS_Audio_Lib.GUID GUID { get; protected internal set; }

        public override string ToString() { try { return (Name == null || Name == "") ? GUID.ToString() : Name; } catch { return ""; } }

        /// <summary>
        /// Compare another device is same
        /// </summary>
        /// <param name="obj">Compare another device</param>
        /// <returns></returns>
        public override bool Equals(object obj)
        {
            SoundDevice d = obj as SoundDevice;
            if (d == null) return false;
            if (d.GUID.Data4.Length != GUID.Data4.Length) return false;

            else return d.GUID.Equals(this.GUID);// && d.ID == this.ID;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public static bool MatchDeviceArray(SoundDevice[] Device1, SoundDevice[] Device2)
        {
            if (Device1 == null) throw new NullReferenceException("Device1 can't be null");
            if (Device2 == null) throw new NullReferenceException("Device2 can't be null");

            if (Device1.Length != Device2.Length) return false;

            for (int i = 0; i < Device1.Length; i++)
                if (!Device1[i].Equals(Device2[i])) return false;
            return true;
        }
    }
}
