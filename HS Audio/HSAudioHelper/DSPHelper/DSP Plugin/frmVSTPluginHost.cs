﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using HS_Audio_Lib;
using HS_Audio.Bundles;

namespace HS_Audio.Plugin
{
    public partial class frmVSTPluginHost : Form
    {
        #region Windows Form Designer generated code

        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.파일FToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.닫기XToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator11 = new System.Windows.Forms.ToolStripSeparator();
            this.개발자모드ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.설정TToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.맨위로설정ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.타이틀스타일ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripComboBox2 = new System.Windows.Forms.ToolStripComboBox();
            this.타이틀숨기기ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.메뉴숨기기ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.투명화ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator14 = new System.Windows.Forms.ToolStripSeparator();
            this.폰트ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.글자색바꾸기ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.배경색바꾸기ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator7 = new System.Windows.Forms.ToolStripSeparator();
            this.플러그인우회ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.보기VToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.컨트롤에크기맞추기ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator9 = new System.Windows.Forms.ToolStripSeparator();
            this.창크기고정ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.스크롤바보이기ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.스케일모드ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripComboBox1 = new System.Windows.Forms.ToolStripComboBox();
            this.colorDialog1 = new System.Windows.Forms.ColorDialog();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.toolStripMenuItem10 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem11 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator12 = new System.Windows.Forms.ToolStripSeparator();
            this.개발자모드ToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.맨위로설정ToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.타이틀스타일ToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripComboBox3 = new System.Windows.Forms.ToolStripComboBox();
            this.타이틀숨기기ToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.메뉴숨기기ToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.투명화ToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator13 = new System.Windows.Forms.ToolStripSeparator();
            this.폰트ToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.글자색바꾸기ToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.배경색바꾸기ToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator8 = new System.Windows.Forms.ToolStripSeparator();
            this.플러그인우회ToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem7 = new System.Windows.Forms.ToolStripMenuItem();
            this.컨트롤에크기맞추기ToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator10 = new System.Windows.Forms.ToolStripSeparator();
            this.창크기고정ToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.스크롤바보이기ToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator6 = new System.Windows.Forms.ToolStripSeparator();
            this.스케일모드ToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripComboBox4 = new System.Windows.Forms.ToolStripComboBox();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.tabControl1 = new Dotnetrix.Controls.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.vstPluginControl1 = new HS_Audio.Plugin.VSTPluginControl();
            this.fontDialog1 = new System.Windows.Forms.FontDialog();
            this.colorDialog2 = new System.Windows.Forms.ColorDialog();
            this.statusStrip1.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.contextMenuStrip1.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.SuspendLayout();
            // 
            // statusStrip1
            // 
            this.statusStrip1.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1});
            this.statusStrip1.Location = new System.Drawing.Point(0, 496);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(649, 22);
            this.statusStrip1.TabIndex = 1;
            this.statusStrip1.Text = "statusStrip1";
            this.statusStrip1.Visible = false;
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(48, 17);
            this.toolStripStatusLabel1.Text = "준비";
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.파일FToolStripMenuItem,
            this.설정TToolStripMenuItem,
            this.보기VToolStripMenuItem});
            this.menuStrip1.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.HorizontalStackWithOverflow;
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(649, 33);
            this.menuStrip1.TabIndex = 2;
            this.menuStrip1.Text = "menuStrip1";
            this.menuStrip1.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.menuStrip1_ItemClicked);
            this.menuStrip1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.menuStrip1_MouseDown);
            this.menuStrip1.MouseLeave += new System.EventHandler(this.menuStrip1_MouseLeave);
            this.menuStrip1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.menuStrip1_MouseMove);
            this.menuStrip1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.menuStrip1_MouseUp);
            // 
            // 파일FToolStripMenuItem
            // 
            this.파일FToolStripMenuItem.BackColor = System.Drawing.Color.WhiteSmoke;
            this.파일FToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.닫기XToolStripMenuItem,
            this.toolStripSeparator11,
            this.개발자모드ToolStripMenuItem});
            this.파일FToolStripMenuItem.Name = "파일FToolStripMenuItem";
            this.파일FToolStripMenuItem.Size = new System.Drawing.Size(79, 29);
            this.파일FToolStripMenuItem.Text = "파일(&F)";
            // 
            // 닫기XToolStripMenuItem
            // 
            this.닫기XToolStripMenuItem.BackColor = System.Drawing.SystemColors.Control;
            this.닫기XToolStripMenuItem.Name = "닫기XToolStripMenuItem";
            this.닫기XToolStripMenuItem.Size = new System.Drawing.Size(198, 30);
            this.닫기XToolStripMenuItem.Text = "닫기(&X)";
            this.닫기XToolStripMenuItem.Click += new System.EventHandler(this.닫기XToolStripMenuItem_Click);
            // 
            // toolStripSeparator11
            // 
            this.toolStripSeparator11.Name = "toolStripSeparator11";
            this.toolStripSeparator11.Size = new System.Drawing.Size(195, 6);
            // 
            // 개발자모드ToolStripMenuItem
            // 
            this.개발자모드ToolStripMenuItem.CheckOnClick = true;
            this.개발자모드ToolStripMenuItem.Name = "개발자모드ToolStripMenuItem";
            this.개발자모드ToolStripMenuItem.Size = new System.Drawing.Size(198, 30);
            this.개발자모드ToolStripMenuItem.Text = "개발자 모드";
            this.개발자모드ToolStripMenuItem.CheckedChanged += new System.EventHandler(this.개발자모드ToolStripMenuItem_CheckedChanged);
            this.개발자모드ToolStripMenuItem.Click += new System.EventHandler(this.개발자모드ToolStripMenuItem_Click);
            // 
            // 설정TToolStripMenuItem
            // 
            this.설정TToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.맨위로설정ToolStripMenuItem,
            this.toolStripSeparator3,
            this.타이틀스타일ToolStripMenuItem,
            this.타이틀숨기기ToolStripMenuItem,
            this.투명화ToolStripMenuItem,
            this.toolStripSeparator14,
            this.폰트ToolStripMenuItem,
            this.글자색바꾸기ToolStripMenuItem,
            this.toolStripSeparator1,
            this.배경색바꾸기ToolStripMenuItem,
            this.toolStripSeparator7,
            this.플러그인우회ToolStripMenuItem});
            this.설정TToolStripMenuItem.Name = "설정TToolStripMenuItem";
            this.설정TToolStripMenuItem.Size = new System.Drawing.Size(80, 29);
            this.설정TToolStripMenuItem.Text = "설정(&T)";
            this.설정TToolStripMenuItem.DropDownOpening += new System.EventHandler(this.설정TToolStripMenuItem_DropDownOpening);
            // 
            // 맨위로설정ToolStripMenuItem
            // 
            this.맨위로설정ToolStripMenuItem.CheckOnClick = true;
            this.맨위로설정ToolStripMenuItem.Image = global::HS_Audio.Properties.Resources.PushpinHS;
            this.맨위로설정ToolStripMenuItem.Name = "맨위로설정ToolStripMenuItem";
            this.맨위로설정ToolStripMenuItem.Size = new System.Drawing.Size(206, 30);
            this.맨위로설정ToolStripMenuItem.Text = "맨 위로 설정";
            this.맨위로설정ToolStripMenuItem.CheckedChanged += new System.EventHandler(this.맨위로설정ToolStripMenuItem_CheckedChanged);
            this.맨위로설정ToolStripMenuItem.Click += new System.EventHandler(this.맨위로설정ToolStripMenuItem_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(203, 6);
            // 
            // 타이틀스타일ToolStripMenuItem
            // 
            this.타이틀스타일ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripComboBox2});
            this.타이틀스타일ToolStripMenuItem.Enabled = false;
            this.타이틀스타일ToolStripMenuItem.Name = "타이틀스타일ToolStripMenuItem";
            this.타이틀스타일ToolStripMenuItem.Size = new System.Drawing.Size(206, 30);
            this.타이틀스타일ToolStripMenuItem.Text = "타이틀 스타일";
            // 
            // toolStripComboBox2
            // 
            this.toolStripComboBox2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.toolStripComboBox2.Items.AddRange(new object[] {
            "모든 버튼 있음",
            "닫기(X) 버튼만 있음",
            "모든 버튼 있음 - 고정",
            "닫기(X) 버튼만 있음 - 고정"});
            this.toolStripComboBox2.Name = "toolStripComboBox2";
            this.toolStripComboBox2.Size = new System.Drawing.Size(140, 33);
            this.toolStripComboBox2.SelectedIndexChanged += new System.EventHandler(this.toolStripComboBox2_SelectedIndexChanged);
            // 
            // 타이틀숨기기ToolStripMenuItem
            // 
            this.타이틀숨기기ToolStripMenuItem.CheckOnClick = true;
            this.타이틀숨기기ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.메뉴숨기기ToolStripMenuItem});
            this.타이틀숨기기ToolStripMenuItem.Name = "타이틀숨기기ToolStripMenuItem";
            this.타이틀숨기기ToolStripMenuItem.Size = new System.Drawing.Size(206, 30);
            this.타이틀숨기기ToolStripMenuItem.Text = "타이틀 숨기기";
            this.타이틀숨기기ToolStripMenuItem.CheckedChanged += new System.EventHandler(this.타이틀숨기기ToolStripMenuItem_CheckedChanged);
            this.타이틀숨기기ToolStripMenuItem.DropDownOpening += new System.EventHandler(this.타이틀숨기기ToolStripMenuItem_DropDownOpening);
            this.타이틀숨기기ToolStripMenuItem.Click += new System.EventHandler(this.타이틀숨기기ToolStripMenuItem_Click);
            // 
            // 메뉴숨기기ToolStripMenuItem
            // 
            this.메뉴숨기기ToolStripMenuItem.CheckOnClick = true;
            this.메뉴숨기기ToolStripMenuItem.Name = "메뉴숨기기ToolStripMenuItem";
            this.메뉴숨기기ToolStripMenuItem.Size = new System.Drawing.Size(198, 30);
            this.메뉴숨기기ToolStripMenuItem.Text = "메뉴 숨기기";
            this.메뉴숨기기ToolStripMenuItem.CheckedChanged += new System.EventHandler(this.메뉴숨기기ToolStripMenuItem_CheckedChanged);
            this.메뉴숨기기ToolStripMenuItem.Click += new System.EventHandler(this.메뉴숨기기ToolStripMenuItem_Click);
            // 
            // 투명화ToolStripMenuItem
            // 
            this.투명화ToolStripMenuItem.CheckOnClick = true;
            this.투명화ToolStripMenuItem.Name = "투명화ToolStripMenuItem";
            this.투명화ToolStripMenuItem.Size = new System.Drawing.Size(206, 30);
            this.투명화ToolStripMenuItem.Text = "투명화";
            this.투명화ToolStripMenuItem.Visible = false;
            this.투명화ToolStripMenuItem.CheckedChanged += new System.EventHandler(this.투명화ToolStripMenuItem_CheckedChanged);
            this.투명화ToolStripMenuItem.Click += new System.EventHandler(this.투명화ToolStripMenuItem_Click);
            // 
            // toolStripSeparator14
            // 
            this.toolStripSeparator14.Name = "toolStripSeparator14";
            this.toolStripSeparator14.Size = new System.Drawing.Size(203, 6);
            // 
            // 폰트ToolStripMenuItem
            // 
            this.폰트ToolStripMenuItem.Image = global::HS_Audio.Properties.Resources.폰트설정;
            this.폰트ToolStripMenuItem.Name = "폰트ToolStripMenuItem";
            this.폰트ToolStripMenuItem.Size = new System.Drawing.Size(206, 30);
            this.폰트ToolStripMenuItem.Text = "폰트";
            this.폰트ToolStripMenuItem.Click += new System.EventHandler(this.폰트ToolStripMenuItem_Click);
            // 
            // 글자색바꾸기ToolStripMenuItem
            // 
            this.글자색바꾸기ToolStripMenuItem.Enabled = false;
            this.글자색바꾸기ToolStripMenuItem.Image = global::HS_Audio.Properties.Resources.글자색지정;
            this.글자색바꾸기ToolStripMenuItem.Name = "글자색바꾸기ToolStripMenuItem";
            this.글자색바꾸기ToolStripMenuItem.Size = new System.Drawing.Size(206, 30);
            this.글자색바꾸기ToolStripMenuItem.Text = "글자색 바꾸기";
            this.글자색바꾸기ToolStripMenuItem.Click += new System.EventHandler(this.글자색바꾸기ToolStripMenuItem_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(203, 6);
            // 
            // 배경색바꾸기ToolStripMenuItem
            // 
            this.배경색바꾸기ToolStripMenuItem.Name = "배경색바꾸기ToolStripMenuItem";
            this.배경색바꾸기ToolStripMenuItem.Size = new System.Drawing.Size(206, 30);
            this.배경색바꾸기ToolStripMenuItem.Text = "배경색 바꾸기";
            this.배경색바꾸기ToolStripMenuItem.Click += new System.EventHandler(this.배경색바꾸기ToolStripMenuItem_Click);
            // 
            // toolStripSeparator7
            // 
            this.toolStripSeparator7.Name = "toolStripSeparator7";
            this.toolStripSeparator7.Size = new System.Drawing.Size(203, 6);
            // 
            // 플러그인우회ToolStripMenuItem
            // 
            this.플러그인우회ToolStripMenuItem.Name = "플러그인우회ToolStripMenuItem";
            this.플러그인우회ToolStripMenuItem.Size = new System.Drawing.Size(206, 30);
            this.플러그인우회ToolStripMenuItem.Text = "플러그인 우회";
            this.플러그인우회ToolStripMenuItem.Click += new System.EventHandler(this.플러그인우회ToolStripMenuItem_Click);
            // 
            // 보기VToolStripMenuItem
            // 
            this.보기VToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.컨트롤에크기맞추기ToolStripMenuItem,
            this.toolStripSeparator9,
            this.창크기고정ToolStripMenuItem,
            this.스크롤바보이기ToolStripMenuItem,
            this.toolStripSeparator2,
            this.스케일모드ToolStripMenuItem});
            this.보기VToolStripMenuItem.Name = "보기VToolStripMenuItem";
            this.보기VToolStripMenuItem.Size = new System.Drawing.Size(81, 29);
            this.보기VToolStripMenuItem.Text = "보기(&V)";
            // 
            // 컨트롤에크기맞추기ToolStripMenuItem
            // 
            this.컨트롤에크기맞추기ToolStripMenuItem.Name = "컨트롤에크기맞추기ToolStripMenuItem";
            this.컨트롤에크기맞추기ToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F5;
            this.컨트롤에크기맞추기ToolStripMenuItem.Size = new System.Drawing.Size(289, 30);
            this.컨트롤에크기맞추기ToolStripMenuItem.Text = "컨트롤에 크기 맞추기";
            this.컨트롤에크기맞추기ToolStripMenuItem.Click += new System.EventHandler(this.컨트롤에크기맞추기ToolStripMenuItem_Click);
            // 
            // toolStripSeparator9
            // 
            this.toolStripSeparator9.Name = "toolStripSeparator9";
            this.toolStripSeparator9.Size = new System.Drawing.Size(286, 6);
            // 
            // 창크기고정ToolStripMenuItem
            // 
            this.창크기고정ToolStripMenuItem.Checked = true;
            this.창크기고정ToolStripMenuItem.CheckOnClick = true;
            this.창크기고정ToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.창크기고정ToolStripMenuItem.Name = "창크기고정ToolStripMenuItem";
            this.창크기고정ToolStripMenuItem.Size = new System.Drawing.Size(289, 30);
            this.창크기고정ToolStripMenuItem.Text = "창 크기 고정";
            this.창크기고정ToolStripMenuItem.CheckedChanged += new System.EventHandler(this.창크기고정ToolStripMenuItem_CheckedChanged);
            this.창크기고정ToolStripMenuItem.Click += new System.EventHandler(this.컨트롤에크기맞추기ToolStripMenuItem_Click);
            // 
            // 스크롤바보이기ToolStripMenuItem
            // 
            this.스크롤바보이기ToolStripMenuItem.CheckOnClick = true;
            this.스크롤바보이기ToolStripMenuItem.Name = "스크롤바보이기ToolStripMenuItem";
            this.스크롤바보이기ToolStripMenuItem.Size = new System.Drawing.Size(289, 30);
            this.스크롤바보이기ToolStripMenuItem.Text = "스크롤바 보이기";
            this.스크롤바보이기ToolStripMenuItem.CheckedChanged += new System.EventHandler(this.스크롤바보이기ToolStripMenuItem_CheckedChanged);
            this.스크롤바보이기ToolStripMenuItem.Click += new System.EventHandler(this.스크롤바보이기ToolStripMenuItem_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(286, 6);
            // 
            // 스케일모드ToolStripMenuItem
            // 
            this.스케일모드ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripComboBox1});
            this.스케일모드ToolStripMenuItem.Enabled = false;
            this.스케일모드ToolStripMenuItem.Name = "스케일모드ToolStripMenuItem";
            this.스케일모드ToolStripMenuItem.Size = new System.Drawing.Size(289, 30);
            this.스케일모드ToolStripMenuItem.Text = "스케일 모드";
            // 
            // toolStripComboBox1
            // 
            this.toolStripComboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.toolStripComboBox1.Items.AddRange(new object[] {
            "없음",
            "폰트 기준",
            "DPI 기준",
            "부모창 기준"});
            this.toolStripComboBox1.Name = "toolStripComboBox1";
            this.toolStripComboBox1.Size = new System.Drawing.Size(121, 33);
            this.toolStripComboBox1.SelectedIndexChanged += new System.EventHandler(this.toolStripComboBox1_SelectedIndexChanged);
            // 
            // colorDialog1
            // 
            this.colorDialog1.FullOpen = true;
            // 
            // checkBox1
            // 
            this.checkBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.checkBox1.AutoSize = true;
            this.checkBox1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.checkBox1.Location = new System.Drawing.Point(575, 4);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(70, 22);
            this.checkBox1.TabIndex = 3;
            this.checkBox1.Text = "우회";
            this.checkBox1.UseVisualStyleBackColor = false;
            this.checkBox1.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
            this.checkBox1.Click += new System.EventHandler(this.checkBox1_Click);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Magenta;
            this.panel1.ContextMenuStrip = this.contextMenuStrip1;
            this.panel1.Cursor = System.Windows.Forms.Cursors.SizeAll;
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 33);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(649, 4);
            this.panel1.TabIndex = 4;
            this.toolTip1.SetToolTip(this.panel1, "잡고 이동합니다");
            this.panel1.Visible = false;
            this.panel1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.menuStrip1_MouseDown);
            this.panel1.MouseLeave += new System.EventHandler(this.menuStrip1_MouseLeave);
            this.panel1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.menuStrip1_MouseMove);
            this.panel1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.menuStrip1_MouseUp);
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem10,
            this.toolStripMenuItem1,
            this.toolStripMenuItem7});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(199, 127);
            this.contextMenuStrip1.Opening += new System.ComponentModel.CancelEventHandler(this.contextMenuStrip1_Opening);
            // 
            // toolStripMenuItem10
            // 
            this.toolStripMenuItem10.BackColor = System.Drawing.SystemColors.Control;
            this.toolStripMenuItem10.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem11,
            this.toolStripSeparator12,
            this.개발자모드ToolStripMenuItem1});
            this.toolStripMenuItem10.Name = "toolStripMenuItem10";
            this.toolStripMenuItem10.Size = new System.Drawing.Size(198, 30);
            this.toolStripMenuItem10.Text = "파일(&F)";
            // 
            // toolStripMenuItem11
            // 
            this.toolStripMenuItem11.BackColor = System.Drawing.SystemColors.Control;
            this.toolStripMenuItem11.Name = "toolStripMenuItem11";
            this.toolStripMenuItem11.Size = new System.Drawing.Size(180, 30);
            this.toolStripMenuItem11.Text = "닫기(&X)";
            this.toolStripMenuItem11.Click += new System.EventHandler(this.닫기XToolStripMenuItem_Click);
            // 
            // toolStripSeparator12
            // 
            this.toolStripSeparator12.Name = "toolStripSeparator12";
            this.toolStripSeparator12.Size = new System.Drawing.Size(177, 6);
            // 
            // 개발자모드ToolStripMenuItem1
            // 
            this.개발자모드ToolStripMenuItem1.Name = "개발자모드ToolStripMenuItem1";
            this.개발자모드ToolStripMenuItem1.Size = new System.Drawing.Size(180, 30);
            this.개발자모드ToolStripMenuItem1.Text = "개발자 모드";
            this.개발자모드ToolStripMenuItem1.CheckedChanged += new System.EventHandler(this.개발자모드ToolStripMenuItem1_CheckedChanged);
            this.개발자모드ToolStripMenuItem1.Click += new System.EventHandler(this.개발자모드ToolStripMenuItem_Click);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.맨위로설정ToolStripMenuItem1,
            this.toolStripSeparator4,
            this.타이틀스타일ToolStripMenuItem1,
            this.타이틀숨기기ToolStripMenuItem1,
            this.투명화ToolStripMenuItem1,
            this.toolStripSeparator13,
            this.폰트ToolStripMenuItem1,
            this.글자색바꾸기ToolStripMenuItem1,
            this.toolStripSeparator5,
            this.배경색바꾸기ToolStripMenuItem1,
            this.toolStripSeparator8,
            this.플러그인우회ToolStripMenuItem1});
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(198, 30);
            this.toolStripMenuItem1.Text = "설정(&T)";
            // 
            // 맨위로설정ToolStripMenuItem1
            // 
            this.맨위로설정ToolStripMenuItem1.CheckOnClick = true;
            this.맨위로설정ToolStripMenuItem1.Image = global::HS_Audio.Properties.Resources.PushpinHS;
            this.맨위로설정ToolStripMenuItem1.Name = "맨위로설정ToolStripMenuItem1";
            this.맨위로설정ToolStripMenuItem1.Size = new System.Drawing.Size(206, 30);
            this.맨위로설정ToolStripMenuItem1.Text = "맨 위로 설정";
            this.맨위로설정ToolStripMenuItem1.CheckedChanged += new System.EventHandler(this.맨위로설정ToolStripMenuItem1_CheckedChanged);
            this.맨위로설정ToolStripMenuItem1.Click += new System.EventHandler(this.맨위로설정ToolStripMenuItem_Click);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(203, 6);
            // 
            // 타이틀스타일ToolStripMenuItem1
            // 
            this.타이틀스타일ToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripComboBox3});
            this.타이틀스타일ToolStripMenuItem1.Enabled = false;
            this.타이틀스타일ToolStripMenuItem1.Name = "타이틀스타일ToolStripMenuItem1";
            this.타이틀스타일ToolStripMenuItem1.Size = new System.Drawing.Size(206, 30);
            this.타이틀스타일ToolStripMenuItem1.Text = "타이틀 스타일";
            // 
            // toolStripComboBox3
            // 
            this.toolStripComboBox3.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.toolStripComboBox3.Items.AddRange(new object[] {
            "모든 버튼 있음",
            "닫기(X) 버튼만 있음",
            "모든 버튼 있음 - 고정",
            "닫기(X) 버튼만 있음 - 고정"});
            this.toolStripComboBox3.Name = "toolStripComboBox3";
            this.toolStripComboBox3.Size = new System.Drawing.Size(140, 33);
            // 
            // 타이틀숨기기ToolStripMenuItem1
            // 
            this.타이틀숨기기ToolStripMenuItem1.CheckOnClick = true;
            this.타이틀숨기기ToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.메뉴숨기기ToolStripMenuItem1});
            this.타이틀숨기기ToolStripMenuItem1.Name = "타이틀숨기기ToolStripMenuItem1";
            this.타이틀숨기기ToolStripMenuItem1.Size = new System.Drawing.Size(206, 30);
            this.타이틀숨기기ToolStripMenuItem1.Text = "타이틀 숨기기";
            this.타이틀숨기기ToolStripMenuItem1.CheckedChanged += new System.EventHandler(this.타이틀숨기기ToolStripMenuItem1_CheckedChanged);
            this.타이틀숨기기ToolStripMenuItem1.DropDownOpening += new System.EventHandler(this.타이틀숨기기ToolStripMenuItem_DropDownOpening);
            this.타이틀숨기기ToolStripMenuItem1.Click += new System.EventHandler(this.타이틀숨기기ToolStripMenuItem_Click);
            // 
            // 메뉴숨기기ToolStripMenuItem1
            // 
            this.메뉴숨기기ToolStripMenuItem1.CheckOnClick = true;
            this.메뉴숨기기ToolStripMenuItem1.Name = "메뉴숨기기ToolStripMenuItem1";
            this.메뉴숨기기ToolStripMenuItem1.Size = new System.Drawing.Size(180, 30);
            this.메뉴숨기기ToolStripMenuItem1.Text = "메뉴 숨기기";
            this.메뉴숨기기ToolStripMenuItem1.CheckedChanged += new System.EventHandler(this.메뉴숨기기ToolStripMenuItem1_CheckedChanged);
            this.메뉴숨기기ToolStripMenuItem1.Click += new System.EventHandler(this.메뉴숨기기ToolStripMenuItem_Click);
            // 
            // 투명화ToolStripMenuItem1
            // 
            this.투명화ToolStripMenuItem1.Name = "투명화ToolStripMenuItem1";
            this.투명화ToolStripMenuItem1.Size = new System.Drawing.Size(206, 30);
            this.투명화ToolStripMenuItem1.Text = "투명화";
            this.투명화ToolStripMenuItem1.CheckedChanged += new System.EventHandler(this.투명화ToolStripMenuItem1_CheckedChanged);
            // 
            // toolStripSeparator13
            // 
            this.toolStripSeparator13.Name = "toolStripSeparator13";
            this.toolStripSeparator13.Size = new System.Drawing.Size(203, 6);
            // 
            // 폰트ToolStripMenuItem1
            // 
            this.폰트ToolStripMenuItem1.Image = global::HS_Audio.Properties.Resources.폰트설정;
            this.폰트ToolStripMenuItem1.Name = "폰트ToolStripMenuItem1";
            this.폰트ToolStripMenuItem1.Size = new System.Drawing.Size(206, 30);
            this.폰트ToolStripMenuItem1.Text = "폰트";
            this.폰트ToolStripMenuItem1.Click += new System.EventHandler(this.폰트ToolStripMenuItem_Click);
            // 
            // 글자색바꾸기ToolStripMenuItem1
            // 
            this.글자색바꾸기ToolStripMenuItem1.Enabled = false;
            this.글자색바꾸기ToolStripMenuItem1.Image = global::HS_Audio.Properties.Resources.글자색지정;
            this.글자색바꾸기ToolStripMenuItem1.Name = "글자색바꾸기ToolStripMenuItem1";
            this.글자색바꾸기ToolStripMenuItem1.Size = new System.Drawing.Size(206, 30);
            this.글자색바꾸기ToolStripMenuItem1.Text = "글자색 바꾸기";
            this.글자색바꾸기ToolStripMenuItem1.Click += new System.EventHandler(this.글자색바꾸기ToolStripMenuItem_Click);
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(203, 6);
            this.toolStripSeparator5.Visible = false;
            // 
            // 배경색바꾸기ToolStripMenuItem1
            // 
            this.배경색바꾸기ToolStripMenuItem1.Name = "배경색바꾸기ToolStripMenuItem1";
            this.배경색바꾸기ToolStripMenuItem1.Size = new System.Drawing.Size(206, 30);
            this.배경색바꾸기ToolStripMenuItem1.Text = "배경색 바꾸기";
            this.배경색바꾸기ToolStripMenuItem1.Visible = false;
            // 
            // toolStripSeparator8
            // 
            this.toolStripSeparator8.Name = "toolStripSeparator8";
            this.toolStripSeparator8.Size = new System.Drawing.Size(203, 6);
            // 
            // 플러그인우회ToolStripMenuItem1
            // 
            this.플러그인우회ToolStripMenuItem1.CheckOnClick = true;
            this.플러그인우회ToolStripMenuItem1.Name = "플러그인우회ToolStripMenuItem1";
            this.플러그인우회ToolStripMenuItem1.Size = new System.Drawing.Size(206, 30);
            this.플러그인우회ToolStripMenuItem1.Text = "플러그인 우회";
            this.플러그인우회ToolStripMenuItem1.Click += new System.EventHandler(this.플러그인우회ToolStripMenuItem_Click);
            // 
            // toolStripMenuItem7
            // 
            this.toolStripMenuItem7.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.컨트롤에크기맞추기ToolStripMenuItem1,
            this.toolStripSeparator10,
            this.창크기고정ToolStripMenuItem1,
            this.스크롤바보이기ToolStripMenuItem1,
            this.toolStripSeparator6,
            this.스케일모드ToolStripMenuItem1});
            this.toolStripMenuItem7.Name = "toolStripMenuItem7";
            this.toolStripMenuItem7.Size = new System.Drawing.Size(198, 30);
            this.toolStripMenuItem7.Text = "보기(&V)";
            // 
            // 컨트롤에크기맞추기ToolStripMenuItem1
            // 
            this.컨트롤에크기맞추기ToolStripMenuItem1.Name = "컨트롤에크기맞추기ToolStripMenuItem1";
            this.컨트롤에크기맞추기ToolStripMenuItem1.Size = new System.Drawing.Size(258, 30);
            this.컨트롤에크기맞추기ToolStripMenuItem1.Text = "컨트롤에 크기 맞추기";
            // 
            // toolStripSeparator10
            // 
            this.toolStripSeparator10.Name = "toolStripSeparator10";
            this.toolStripSeparator10.Size = new System.Drawing.Size(255, 6);
            // 
            // 창크기고정ToolStripMenuItem1
            // 
            this.창크기고정ToolStripMenuItem1.Checked = true;
            this.창크기고정ToolStripMenuItem1.CheckOnClick = true;
            this.창크기고정ToolStripMenuItem1.CheckState = System.Windows.Forms.CheckState.Checked;
            this.창크기고정ToolStripMenuItem1.Name = "창크기고정ToolStripMenuItem1";
            this.창크기고정ToolStripMenuItem1.Size = new System.Drawing.Size(258, 30);
            this.창크기고정ToolStripMenuItem1.Text = "창 크기 고정";
            this.창크기고정ToolStripMenuItem1.CheckedChanged += new System.EventHandler(this.창크기고정ToolStripMenuItem1_CheckedChanged);
            this.창크기고정ToolStripMenuItem1.Click += new System.EventHandler(this.컨트롤에크기맞추기ToolStripMenuItem_Click);
            // 
            // 스크롤바보이기ToolStripMenuItem1
            // 
            this.스크롤바보이기ToolStripMenuItem1.CheckOnClick = true;
            this.스크롤바보이기ToolStripMenuItem1.Name = "스크롤바보이기ToolStripMenuItem1";
            this.스크롤바보이기ToolStripMenuItem1.Size = new System.Drawing.Size(258, 30);
            this.스크롤바보이기ToolStripMenuItem1.Text = "스크롤바 보이기";
            this.스크롤바보이기ToolStripMenuItem1.CheckedChanged += new System.EventHandler(this.스크롤바보이기ToolStripMenuItem1_CheckedChanged);
            this.스크롤바보이기ToolStripMenuItem1.Click += new System.EventHandler(this.스크롤바보이기ToolStripMenuItem_Click);
            // 
            // toolStripSeparator6
            // 
            this.toolStripSeparator6.Name = "toolStripSeparator6";
            this.toolStripSeparator6.Size = new System.Drawing.Size(255, 6);
            // 
            // 스케일모드ToolStripMenuItem1
            // 
            this.스케일모드ToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripComboBox4});
            this.스케일모드ToolStripMenuItem1.Enabled = false;
            this.스케일모드ToolStripMenuItem1.Name = "스케일모드ToolStripMenuItem1";
            this.스케일모드ToolStripMenuItem1.Size = new System.Drawing.Size(258, 30);
            this.스케일모드ToolStripMenuItem1.Text = "스케일 모드";
            // 
            // toolStripComboBox4
            // 
            this.toolStripComboBox4.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.toolStripComboBox4.Items.AddRange(new object[] {
            "없음",
            "폰트 기준",
            "DPI 기준",
            "부모창 기준"});
            this.toolStripComboBox4.Name = "toolStripComboBox4";
            this.toolStripComboBox4.Size = new System.Drawing.Size(121, 33);
            // 
            // tabControl1
            // 
            this.tabControl1.ContextMenuStrip = this.contextMenuStrip1;
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 33);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(649, 485);
            this.tabControl1.TabIndex = 5;
            this.toolTip1.SetToolTip(this.tabControl1, "잡고 이동합니다");
            this.tabControl1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.menuStrip1_MouseDown);
            this.tabControl1.MouseLeave += new System.EventHandler(this.menuStrip1_MouseLeave);
            this.tabControl1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.menuStrip1_MouseMove);
            this.tabControl1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.menuStrip1_MouseUp);
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.vstPluginControl1);
            this.tabPage1.Location = new System.Drawing.Point(4, 29);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(641, 452);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "tabPage1";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // vstPluginControl1
            // 
            this.vstPluginControl1.BackColor = System.Drawing.Color.Gray;
            this.vstPluginControl1.Location = new System.Drawing.Point(1, 0);
            this.vstPluginControl1.Name = "vstPluginControl1";
            this.vstPluginControl1.Size = new System.Drawing.Size(640, 472);
            this.vstPluginControl1.TabIndex = 0;
            this.vstPluginControl1.Text = "vstPluginControl1";
            this.vstPluginControl1.ControlAdded += new System.Windows.Forms.ControlEventHandler(this.vstPluginControl1_ControlAdded);
            this.vstPluginControl1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.vstPluginControl1_MouseMove);
            // 
            // frmVSTPluginHost
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(649, 518);
            this.ContextMenuStrip = this.contextMenuStrip1;
            this.Controls.Add(this.checkBox1);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.menuStrip1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "frmVSTPluginHost";
            this.ShowIcon = false;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmVSTPluginHost_FormClosing);
            this.Load += new System.EventHandler(this.frmVSTPluginHost_Load);
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.contextMenuStrip1.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        private ToolStripMenuItem 타이틀숨기기ToolStripMenuItem;
        private ToolStripMenuItem 메뉴숨기기ToolStripMenuItem;
        private ToolStripSeparator toolStripSeparator3;
        private ToolStripMenuItem 타이틀스타일ToolStripMenuItem;
        private ToolStripComboBox toolStripComboBox2;
        private StatusStrip statusStrip1;
        private ToolStripStatusLabel toolStripStatusLabel1;
        private MenuStrip menuStrip1;
        private ToolStripMenuItem 파일FToolStripMenuItem;
        private ToolStripMenuItem 닫기XToolStripMenuItem;
        private ToolStripMenuItem 설정TToolStripMenuItem;
        private ToolStripMenuItem 맨위로설정ToolStripMenuItem;
        private ToolStripSeparator toolStripSeparator1;
        private ToolStripMenuItem 배경색바꾸기ToolStripMenuItem;
        private ColorDialog colorDialog1;
        private CheckBox checkBox1;
        private ToolStripMenuItem 보기VToolStripMenuItem;
        private ToolStripMenuItem 컨트롤에크기맞추기ToolStripMenuItem;
        private ToolStripMenuItem 스케일모드ToolStripMenuItem;
        private ToolStripComboBox toolStripComboBox1;
        private ToolStripSeparator toolStripSeparator2;

        internal HS_Audio.Plugin.VSTPluginControl vstPluginControl1;
        private Panel panel1;
        private ToolTip toolTip1;
        private ContextMenuStrip contextMenuStrip1;
        private ToolStripMenuItem toolStripMenuItem10;
        private ToolStripMenuItem toolStripMenuItem11;
        private ToolStripMenuItem toolStripMenuItem7;
        private ToolStripMenuItem 컨트롤에크기맞추기ToolStripMenuItem1;
        private ToolStripSeparator toolStripSeparator6;
        private ToolStripMenuItem 스케일모드ToolStripMenuItem1;
        private ToolStripComboBox toolStripComboBox4;
        private ToolStripMenuItem toolStripMenuItem1;
        private ToolStripMenuItem 맨위로설정ToolStripMenuItem1;
        private ToolStripSeparator toolStripSeparator4;
        private ToolStripMenuItem 타이틀스타일ToolStripMenuItem1;
        private ToolStripComboBox toolStripComboBox3;
        private ToolStripMenuItem 타이틀숨기기ToolStripMenuItem1;
        private ToolStripMenuItem 메뉴숨기기ToolStripMenuItem1;
        private ToolStripSeparator toolStripSeparator5;
        private Dotnetrix.Controls.TabControl tabControl1;
        //System.Windows.Forms.TabControl tabControl1;
        private TabPage tabPage1;
        private ToolStripMenuItem 창크기고정ToolStripMenuItem;
        private ToolStripMenuItem 창크기고정ToolStripMenuItem1;
        private ToolStripMenuItem 스크롤바보이기ToolStripMenuItem;
        private ToolStripMenuItem 스크롤바보이기ToolStripMenuItem1;
        private ToolStripSeparator toolStripSeparator7;
        private ToolStripMenuItem 플러그인우회ToolStripMenuItem;
        private ToolStripSeparator toolStripSeparator8;
        private ToolStripMenuItem 플러그인우회ToolStripMenuItem1;
        private ToolStripSeparator toolStripSeparator9;
        private ToolStripSeparator toolStripSeparator10;
        private ToolStripMenuItem 개발자모드ToolStripMenuItem;
        private ToolStripSeparator toolStripSeparator11;
        private ToolStripMenuItem 개발자모드ToolStripMenuItem1;
        private ToolStripSeparator toolStripSeparator12;
        private ToolStripMenuItem 투명화ToolStripMenuItem;
        private ToolStripMenuItem 투명화ToolStripMenuItem1;
        private ToolStripMenuItem 폰트ToolStripMenuItem1;
        private FontDialog fontDialog1;
        private ToolStripSeparator toolStripSeparator14;
        private ToolStripSeparator toolStripSeparator13;
        private ToolStripMenuItem 폰트ToolStripMenuItem;
        private ToolStripMenuItem 글자색바꾸기ToolStripMenuItem;
        private ToolStripMenuItem 글자색바꾸기ToolStripMenuItem1;
        private ColorDialog colorDialog2;
        private ToolStripMenuItem 배경색바꾸기ToolStripMenuItem1;
        #endregion

        [DllImport("kernel32.dll")]
        static extern void RtlMoveMemory(IntPtr dest, IntPtr src, uint len);

        public PluginDSP Plugin{get; internal set;}

        protected frmVSTPluginHost()
        {
            InitializeComponent();
            ReCalcOnce();
        }
        public frmVSTPluginHost(PluginDSP Plugin)
        {
            InitializeComponent();
            this.Plugin = Plugin;
            if (Plugin != null) Plugin.EffectChange += Plugin_EffectChange;
            ReCalcOnce();
        }
        private void frmVSTPluginHost_Load(object sender, System.EventArgs e)
        {
            contextMenuStrip1.ImageScalingSize = menuStrip1.ImageScalingSize = new Size(16, 16);
            this.Scale(1f, 1f);
            toolStripComboBox1.SelectedIndex = 0;
            toolStripComboBox2.SelectedIndex = 3;

            tabPage1.Text = "HS 플레이어 VST 플러그인 창 - " + Plugin.Name;
            this.Text = "HS 플레이어 VST 플러그인 창 - " + Plugin.Name;
            //Plugin.dsp.ReRegisterDSP();
        }


        int count, count2;
        private RESULT PK(ref DSP_STATE state, IntPtr In, IntPtr Out, uint length, int inchannels, int outchannels)
        {
            unsafe
            {
                float* inbuffer = (float*)In.ToPointer();
                float* outbuffer = (float*)Out.ToPointer();

                for (count = 0; count < length; count++)
                {
                    for (count2 = 0; count2 < outchannels; count2++)
                    {
                        float a = inbuffer[(count * inchannels) + count2];
                        //if (count2 == 0) {if (Leftmax < a) Leftmax = a;}
                        //else if (count2 == 1) if (RightMax < a) RightMax = a;
                        outbuffer[(count * outchannels) + count2] = a;
                    }
                }
            }
            //RtlMoveMemory(In, Out, length * (uint)outchannels);
            return RESULT.OK;
        }

        public bool IsClose;
        private void frmVSTPluginHost_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = !IsClose;
            this.Hide();
        }

        private void 배경색바꾸기ToolStripMenuItem_Click(object sender, System.EventArgs e)
        {
            colorDialog1.Color = vstPluginControl1.BackColor;
            if (colorDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK) panel1.BackColor =tabPage1.BackColor=vstPluginControl1.BackColor =colorDialog1.Color;
        }

        private void 닫기XToolStripMenuItem_Click(object sender, System.EventArgs e)
        {
            this.Close();
        }

        bool CheckedLock;
        private void checkBox1_Click(object sender, System.EventArgs e)
        {
            //CheckedLock = true;
            if (Plugin != null) Plugin.ByPass = checkBox1.Checked;
            else checkBox1.Checked = !checkBox1.Checked;
            //CheckedLock = false;
        }

        private void Plugin_EffectChange(PluginDSP p, bool ByPass)
        {
            /*if (!CheckedLock)*/ checkBox1.Checked =플러그인우회ToolStripMenuItem.Checked=플러그인우회ToolStripMenuItem1.Checked= ByPass;

            if (ByPass) tabPage1.Text = tabPage1.Text + " [우회됨(Bypass)]";
            else  tabPage1.Text = tabPage1.Text.Replace(" [우회됨(Bypass)]", "");//try{int a = tabPage1.Text.LastIndexOf("[");tabPage1.Text.Remove(a+1); }catch{}
        }

        private void vstPluginControl1_ControlAdded(object sender, ControlEventArgs e)
        {

        }

        internal void 컨트롤에크기맞추기ToolStripMenuItem_Click(object sender, System.EventArgs e)
        {
            ReCaculatorSize();
            ReSize();
            ReSize();
        }

        Size ControlSize;
        public void ReSize()
        {
            //this.DefaultPadding
            List<IntPtr> aa = GetChildWindows(vstPluginControl1.Handle);
            RECT rect = new RECT();
            if (aa.Count > 0)
            {
                bool a1 = GetWindowRect(aa[0], out rect);
                System.Drawing.Size size = rect.Size;//new System.Drawing.Size(rect.Right - rect.Left, rect.Bottom - rect.Top);
                vstPluginControl1.Size = size;

                System.Drawing.Size menusz = new Size(0, menuStrip1.Height);
                System.Drawing.Size newsz = size + addsz + (메뉴숨기기ToolStripMenuItem.Checked ? new Size() : menusz);
                //vst.Top = rect.Top;
                this.Size =  ControlSize = newsz;//new System.Drawing.Size(size.Width+addsz.Width, size.Height+addsz.Height);

                tabPage1.AutoScroll = 스크롤바보이기ToolStripMenuItem.Checked;
                this.MaximumSize = 창크기고정ToolStripMenuItem.Checked ? ControlSize : new Size(0, 0);
                if(this.FormBorderStyle!= System.Windows.Forms.FormBorderStyle.None)this.FormBorderStyle = 창크기고정ToolStripMenuItem.Checked?System.Windows.Forms.FormBorderStyle.FixedToolWindow:System.Windows.Forms.FormBorderStyle.SizableToolWindow;
            }
        }

        System.Drawing.Size Size_NotAutoScroll;
        System.Drawing.Size Size_AutoScroll;
        System.Drawing.Size addsz;
        public void ReCaculatorSize()
        {
            bool tmp = menuStrip1.Visible;
            if (!tmp){메뉴숨기기ToolStripMenuItem.Checked = false;메뉴숨기기ToolStripMenuItem_Click(null, null);}

            ReCalcOnce();
            
            if (!tmp){메뉴숨기기ToolStripMenuItem.Checked = true;메뉴숨기기ToolStripMenuItem_Click(null, null);}
        }
        private void ReCalcOnce()
        {
            RECT sz = new RECT();
            GetWindowRect(this.Handle, out sz);
            if (this.FormBorderStyle == System.Windows.Forms.FormBorderStyle.None) ;
            System.Drawing.Size mainsz = sz.Size;//new Size(rect.Right - rect.Left, rect.Bottom - rect.Top);
            System.Drawing.Size subsz = new Size(tabPage1.Size.Width, tabControl1.Size.Height);//vstPluginControl1.Size;
            addsz = new System.Drawing.Size((mainsz.Width - subsz.Width) + 3, mainsz.Height - subsz.Height + 3);
        }

        #region Win32 창 관련
        public delegate bool EnumWindowProc(IntPtr hWnd, IntPtr parameter);

        [DllImport("user32")]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool EnumChildWindows(IntPtr window, EnumWindowProc callback, IntPtr i);

        public static List<IntPtr> GetChildWindows(IntPtr parent)
        {
            List<IntPtr> result = new List<IntPtr>();
            GCHandle listHandle = GCHandle.Alloc(result);
            try
            {
                EnumWindowProc childProc = new EnumWindowProc(EnumWindow);
                EnumChildWindows(parent, childProc, GCHandle.ToIntPtr(listHandle));
            }
            finally
            {
                if (listHandle.IsAllocated)
                    listHandle.Free();
            }
            return result;
        }

        private static bool EnumWindow(IntPtr handle, IntPtr pointer)
        {
            GCHandle gch = GCHandle.FromIntPtr(pointer);
            List<IntPtr> list = gch.Target as List<IntPtr>;
            if (list == null)
                throw new InvalidCastException("GCHandle Target could not be cast as List<IntPtr>");

            list.Add(handle);
            return true;
        }

        [DllImport("user32.dll", SetLastError = true)]
        static extern bool GetWindowRect(IntPtr hWnd, out RECT lpRect);

        [StructLayout(LayoutKind.Sequential)]
        public struct RECT
        {
            public int Left;
            public int Top;
            public int Right;
            public int Bottom;


            private Size _Size;
            private System.Drawing.Point _Location;
            public int Width{get{return Right - Left;}}
            public int Height{get{return Bottom - Top;}}
            public Size Size{get{_Size.Width = Width;_Size.Height = Height; return _Size;}}
            public Point Location{get{_Location.X = Left;_Location.Y = Top; return _Location;}}
            public System.Drawing.Rectangle Rectangle{get{return new Rectangle(Left, Top, Width, Height);}}
        }
        #endregion

        private void toolStripComboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (toolStripComboBox1.SelectedIndex)
            {
                case 0: this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None; break;
                case 1: this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None; break;
                case 2: this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi; break;
                default: this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit; break;
            }
        }

        private void 메뉴숨기기ToolStripMenuItem_CheckStateChanged(object sender, EventArgs e)
        {

        }

        private void 타이틀숨기기ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (타이틀숨기기ToolStripMenuItem.Checked)
            {
                this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;

                //this.vstPluginControl1.Location = menuStrip1.Visible ? new Point(0, menuStrip1.Height + 10) : new Point(0, 10);
                //panel1.Height+=10;
                //this.Height += 10;
            }
            else if (toolStripComboBox2.Enabled)
            {
                switch (toolStripComboBox2.SelectedIndex)
                {
                    case 0: this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Sizable; break;
                    case 1: this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow; break;
                    case 2: this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog; break;
                    default: this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow; break;
                }

                //this.vstPluginControl1.Location = menuStrip1.Visible ? new Point(0, menuStrip1.Height - 10) : new Point(0, 10);
                //panel1.Height += 10;
                //this.Height += 10;
            }
            else this.FormBorderStyle = 창크기고정ToolStripMenuItem.Checked?System.Windows.Forms.FormBorderStyle.FixedToolWindow:System.Windows.Forms.FormBorderStyle.SizableToolWindow;
            //ReCaculatorSize();
            //ReSize();
        }

        private void 타이틀숨기기ToolStripMenuItem_DropDownOpening(object sender, EventArgs e)
        {
            //메뉴숨기기ToolStripMenuItem.Enabled = this.FormBorderStyle == System.Windows.Forms.FormBorderStyle.None;
        }

        private void toolStripComboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (toolStripComboBox2.SelectedIndex)
            {
                case 0: if (this.FormBorderStyle!= System.Windows.Forms.FormBorderStyle.None) this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Sizable; break;
                case 1: if (this.FormBorderStyle != System.Windows.Forms.FormBorderStyle.None) this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow; break;
                case 2: if (this.FormBorderStyle != System.Windows.Forms.FormBorderStyle.None) this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog; break;
                default: if (this.FormBorderStyle != System.Windows.Forms.FormBorderStyle.None) this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow; break;
            }
        }
        Point menuStrip1MouseDownLocation;
        MouseButtons menuStrip1MouseButtons = System.Windows.Forms.MouseButtons.None;
        private void vstPluginControl1_MouseMove(object sender, MouseEventArgs e)
        {

        }

        Point label1MouseDownLocation;
        MouseButtons label1MouseButtons = System.Windows.Forms.MouseButtons.None;
        private void menuStrip1_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Left)
            {
                this.Left += e.X - menuStrip1MouseDownLocation.X;
                this.Top += e.Y - menuStrip1MouseDownLocation.Y;
            }
        }

        private void menuStrip1_MouseLeave(object sender, EventArgs e)
        {
            menuStrip1_MouseUp(null, null);
        }

        private void menuStrip1_MouseUp(object sender, MouseEventArgs e)
        {
            menuStrip1MouseButtons = System.Windows.Forms.MouseButtons.None;
            this.Cursor = Cursors.Default;
        }

        private void menuStrip1_MouseDown(object sender, MouseEventArgs e)
        {
            menuStrip1MouseButtons = System.Windows.Forms.MouseButtons.Left;
            menuStrip1MouseDownLocation = e.Location;
            this.Cursor = Cursors.SizeAll;
        }

        private void 메뉴숨기기ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //DockStyle ds = this.tabControl1.Dock;
            menuStrip1.Visible = !메뉴숨기기ToolStripMenuItem.Checked;
            checkBox1.Visible = !메뉴숨기기ToolStripMenuItem.Checked;

            //this.tabControl1.Dock = DockStyle.None;
            if (menuStrip1.Visible)
            {
                int a = menuStrip1.Height;

                this.tabControl1.Location = new Point(0, a); //panel1.Height>0?new Point(0, a+panel1.Height):new Point(0, a);
                this.Height += a;
                //this.vstPluginControl1.Height += a;
                //this.tabControl1.Height += menuStrip1.Height;
            }
            else
            {
                this.tabControl1.Location = new Point(0, 0);
                this.Height -= menuStrip1.Height;
                //this.tabControl1.Height -=menuStrip1.Height; //new Size(tabControl1.Width, tabControl1.Height
                //this.vstPluginControl1.Height -= menuStrip1.Height;
                //this.Size = new Size(this.Width, this.Height-menuStrip1.Height);
            }
            //this.tabControl1.Dock = ds;
            //ReCaculatorSize();
            //ReSize();
        }

        private void 설정TToolStripMenuItem_DropDownOpening(object sender, EventArgs e)
        {
            맨위로설정ToolStripMenuItem.Checked = 맨위로설정ToolStripMenuItem1.Checked = this.TopMost;
        }

        private void 맨위로설정ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.TopMost = 맨위로설정ToolStripMenuItem.Checked;
        }

        private void checkBox2_Click(object sender, EventArgs e)
        {

        }

        bool OpeningLock;
        private void contextMenuStrip1_Opening(object sender, System.ComponentModel.CancelEventArgs e)
        {
            OpeningLock = true;
        }
        private void menuStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

        private void 스크롤바보이기ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //vstPluginControl1.Dock = 스크롤바보이기ToolStripMenuItem.Checked ? DockStyle.None : DockStyle.Fill;
            tabPage1.AutoScroll = 스크롤바보이기ToolStripMenuItem.Checked;
        }

        private void 개발자모드ToolStripMenuItem_Click(object sender, EventArgs e){ 타이틀스타일ToolStripMenuItem.Enabled = toolStripComboBox2.Enabled = toolStripComboBox3.Enabled = false; 스케일모드ToolStripMenuItem.Enabled = 스케일모드ToolStripMenuItem1.Enabled = 타이틀스타일ToolStripMenuItem1.Enabled = 개발자모드ToolStripMenuItem.Checked; }

        private void 투명화ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //tabControl1.
        }


        #region 체크상태 동기화
        private void 플러그인우회ToolStripMenuItem_Click(object sender, EventArgs e){ checkBox1.Checked = !플러그인우회ToolStripMenuItem.Checked;checkBox1_Click(null, null); }
        private void 맨위로설정ToolStripMenuItem_CheckedChanged(object sender, System.EventArgs e){맨위로설정ToolStripMenuItem1.Checked = 맨위로설정ToolStripMenuItem.Checked;}
        private void 맨위로설정ToolStripMenuItem1_CheckedChanged(object sender, EventArgs e){맨위로설정ToolStripMenuItem.Checked = 맨위로설정ToolStripMenuItem1.Checked;}

        private void 메뉴숨기기ToolStripMenuItem_CheckedChanged(object sender, EventArgs e){메뉴숨기기ToolStripMenuItem1.Checked = 메뉴숨기기ToolStripMenuItem.Checked;}
        private void 타이틀숨기기ToolStripMenuItem_CheckedChanged(object sender, EventArgs e){타이틀숨기기ToolStripMenuItem1.Checked = 타이틀숨기기ToolStripMenuItem.Checked; }
        private void 타이틀숨기기ToolStripMenuItem1_CheckedChanged(object sender, EventArgs e){타이틀숨기기ToolStripMenuItem.Checked = 타이틀숨기기ToolStripMenuItem1.Checked; }
        private void 메뉴숨기기ToolStripMenuItem1_CheckedChanged(object sender, EventArgs e){메뉴숨기기ToolStripMenuItem.Checked = 메뉴숨기기ToolStripMenuItem1.Checked;}

        private void 창크기고정ToolStripMenuItem_CheckedChanged(object sender, EventArgs e){창크기고정ToolStripMenuItem1.Checked = 창크기고정ToolStripMenuItem.Checked;}
        private void 스크롤바보이기ToolStripMenuItem_CheckedChanged(object sender, EventArgs e) {스크롤바보이기ToolStripMenuItem1.Checked = 스크롤바보이기ToolStripMenuItem.Checked; }
        private void 스크롤바보이기ToolStripMenuItem1_CheckedChanged(object sender, EventArgs e){스크롤바보이기ToolStripMenuItem.Checked = 스크롤바보이기ToolStripMenuItem1.Checked;}
        private void 창크기고정ToolStripMenuItem1_CheckedChanged(object sender, EventArgs e){창크기고정ToolStripMenuItem.Checked = 창크기고정ToolStripMenuItem1.Checked;}
        private void 개발자모드ToolStripMenuItem_CheckedChanged(object sender, EventArgs e){개발자모드ToolStripMenuItem1.Checked = 개발자모드ToolStripMenuItem.Checked;}
        private void 개발자모드ToolStripMenuItem1_CheckedChanged(object sender, EventArgs e){개발자모드ToolStripMenuItem.Checked = 개발자모드ToolStripMenuItem1.Checked;}
        private void checkBox1_CheckedChanged(object sender, EventArgs e){플러그인우회ToolStripMenuItem.Checked = checkBox1.Checked;}
        private void 투명화ToolStripMenuItem1_CheckedChanged(object sender, EventArgs e){투명화ToolStripMenuItem.Checked = 투명화ToolStripMenuItem1.Checked;}
        private void 투명화ToolStripMenuItem_CheckedChanged(object sender, EventArgs e){투명화ToolStripMenuItem1.Checked = 투명화ToolStripMenuItem.Checked;}
        #endregion

        private void 폰트ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            fontDialog1.Font = tabControl1.Font;
            if (fontDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK) tabControl1.Font=tabPage1.Font=fontDialog1.Font;
        }

        private void 글자색바꾸기ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            colorDialog2.Color = tabPage1.ForeColor;
            if (colorDialog2.ShowDialog() == System.Windows.Forms.DialogResult.OK) tabPage1.ForeColor = colorDialog2.Color;
        }

    }   
    partial class frmVSTPluginHost
    {
        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
            //if (CalculatePeakThread != null) CalculatePeakThread.Abort();
        }
    }
}
