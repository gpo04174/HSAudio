﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.InteropServices;
using System.Threading;
using System.Windows.Forms;
using HS_Audio.DSP;
using HS_Audio_Lib;
using HS_CSharpUtility.Extension;
using HS_Audio.Bundles;

namespace HS_Audio.Forms
{
    public partial class frmDSPPlugin : Form
    {
        HSAudioHelper Helper;
        HS_Audio_Lib.DSPConnection dpc;

        protected frmDSPPlugin()
        {
            InitializeComponent();
            CalculatePeakThreadStart = new ThreadStart(CalculatePeakThreadStart_Loop);
        }

        public frmDSPPlugin(HSAudioHelper Helper)
        {
            this.Helper = Helper;
            InitializeComponent();
            CalculatePeakThreadStart = new ThreadStart(CalculatePeakThreadStart_Loop);
            //this.Helper.MusicChanging+=Helper_MusicPathChanged;
            
        }
        /*
        private void Helper_MusicPathChanged(string MusicPath, int index, bool Error)
        {
            PluginDSP[] p = Helper.Plugins;
            HS_Audio_Lib.RESULT res = RESULT.OK;
            for (int i = p.Length - 1; i >= 0; i--)
            {
                if (Helper.Plugins[i] != null && !Helper.Plugins[i].IsSystemPlugin)
                {
                    res = Helper.Plugins[i].dsp.remove();
                    res = Helper.channel.addDSP(Helper.Plugins[i].dsp, ref dpc);
                }
            }
            RegisterItem(Helper.Plugins);
        }*/

        string SettingPath = Application.StartupPath + "\\Settings\\DSP\\frmDSPPlugin.ini";
        Dictionary<string, string> Setting = new Dictionary<string, string>();
        public bool UpdateSetting()
        {
            try
            {
                if (!Directory.Exists(Application.StartupPath + "\\Settings\\DSP")) Directory.CreateDirectory(Application.StartupPath + "\\Settings\\DSP");
                string[] a = HS_CSharpUtility.Utility.EtcUtility.SaveSetting(Setting);
                File.WriteAllLines(SettingPath, a);
                return true;
            }
            catch{return false;}
        }

        private void frmDSPPlugin_Load(object sender, EventArgs e)
        {
            try
            {
                if (File.Exists(SettingPath))
                {
                    string[] a = File.ReadAllLines(SettingPath);
                    Setting = HS_CSharpUtility.Utility.EtcUtility.LoadSetting(a);
                }
            }catch{}
            comboBox1.SelectedIndex = 1;
            comboBox1.Text = comboBox1.Items[comboBox1.SelectedIndex].ToString();
        }
        private void 윈앰프플러그인경로설정ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (folderBrowserDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                try{Helper.WinAmpPluginPath=folderBrowserDialog1.SelectedPath;}
                catch(SoundSystemException ex){MessageBox.Show(ex.Message, "재생 설정",MessageBoxButtons.OK ,MessageBoxIcon.Error);}
            }
        }

        List<ListViewItem> ExtPlugin = new List<ListViewItem>();
        int lastIndex = 1;
        int[] ColWidth = new int[4];
        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            listView1_ItemChecked_Lock = true;
            try
            {
                if (comboBox1.SelectedIndex == 0)
                {
                    if (lastIndex == comboBox1.SelectedIndex) return;
                    lastIndex = comboBox1.SelectedIndex;
                    ColWidth[0] = listView1.Columns[0].Width;
                    ColWidth[1] = listView1.Columns[3].Width;
                    ColWidth[2] = listView1.Columns[5].Width;
                    ColWidth[3] = listView1.Columns[7].Width;
                    listView1.Columns[0].Width = listView1.Columns[3].Width = listView1.Columns[5].Width = listView1.Columns[7].Width = 0;
                    contextMenuStrip1.Enabled = listView1.CheckBoxes = false;
                    if (ExtPlugin != null) ExtPlugin.Clear();
                    foreach (ListViewItem li in listView1.Items) ExtPlugin.Add(li);
                    listView1.Items.Clear();

                    for (int i = 0; i < Helper.DefaultPlugins.Length; i++)
                    {
                        try
                        {
                            PluginDSP p = Helper.DefaultPlugins[i];
                            ListViewItem li = new ListViewItem(p.Priority.ToString());
                            if (p.FullName == null || p.FullName == "") p._Name = System.IO.Path.GetFileName(openFileDialog1.FileName);
                            li.SubItems.Add(p.FullName.Replace("FMOD", ""));
                            li.SubItems.Add("Main");
                            li.SubItems.Add("");
                            li.SubItems.Add(p.PluginType.ToString());
                            li.SubItems.Add("0x" + p.Version.ToHexString(8));
                            li.SubItems.Add("0x" + p.Handle.ToHexString(8));
                            li.SubItems.Add(p.Path);
                            listView1.Items.Add(li);
                        }
                        catch{ }
                    }
                }
                else
                {
                    if (lastIndex == comboBox1.SelectedIndex) return;
                    lastIndex = comboBox1.SelectedIndex;
                    listView1.Columns[0].Width = ColWidth[0];
                    listView1.Columns[3].Width = ColWidth[1];
                    listView1.Columns[5].Width = ColWidth[2];
                    listView1.Columns[7].Width = ColWidth[3];

                    listView1.Items.Clear();
                    listView1.CheckBoxes = true;
                    for (int i = 0; i < ExtPlugin.Count; i++) listView1.Items.Add(ExtPlugin[i]);
                    contextMenuStrip1.Enabled = true;
                    if (ExtPlugin != null) ExtPlugin.Clear();
                }
            }finally{listView1_ItemChecked_Lock = false;}
        }

        private void openFileDialog1_FileOk(object sender, System.ComponentModel.CancelEventArgs e)
        {
            Setting["LastOpenPath"] = Path.GetDirectoryName(openFileDialog1.FileName);
            UpdateSetting();
            for (int i = 0; i < openFileDialog1.FileNames.Length; i++)
            {
                PluginDSP p =null;
                try { p = Helper.CreatePlugins(openFileDialog1.FileNames[i], (uint)listView1.Items.Count, true); }
                catch(SoundSystemException sex){MessageBox.Show("플러그인 파일을 열 수 없습니다. ("+sex.Result+")\n\n원인: "+sex.PureMessage, "HS 플레이어 DSP 플러그인", MessageBoxButtons.OK, MessageBoxIcon.Error);}
                catch(Exception ex){MessageBox.Show("플러그인 파일을 열 수 없습니다.\n\n원인: "+ex.Message, "HS 플레이어 DSP 플러그인", MessageBoxButtons.OK, MessageBoxIcon.Error);return;}
                //p.dsp.showConfigDialog(this.Handle, false);
                if (p != null)
                {
                    ListViewItem li = new ListViewItem(p.Priority.ToString());
                    if (p.FullName == null || p.FullName == "") p._Name = System.IO.Path.GetFileName(openFileDialog1.FileName);
                    li.SubItems.Add(p.Name);

                    if (p.PluginType == HS_Audio_Lib.PLUGINTYPE.CODEC)li.SubItems.Add("(CODEC)");
                    else if (p.PluginType == HS_Audio_Lib.PLUGINTYPE.OUTPUT) li.SubItems.Add("(OUTPUT)");
                    else li.SubItems.Add(p.IsSystemPlugin ? "Main" : "Channel");

                    li.SubItems.Add(p.ByPass ? "●" : "");
                    li.SubItems.Add(p.PluginType.ToString() + (p.PluginKind == PluginKind.UNKNOWN ? "" : "(" + p.PluginKind + ")"));
                    li.SubItems.Add("0x"+p.Version.ToHexString(8));
                    li.SubItems.Add("0x" + p.Handle.ToHexString(8));
                    li.SubItems.Add(p.Path);

                    if(comboBox1.SelectedIndex == 0)ExtPlugin.Add(li);
                    else listView1.Items.Add(li);
                    p.EffectChange += Plugin_EffectChange;
                }
            }
        }

        private void Plugin_EffectChange(PluginDSP p, bool ByPass)
        {
            ListViewItem li = FindItem(p);
            if(li!=null)li.SubItems[3].Text = ByPass?"●":"";
        }


        bool IsClosing;
        private void frmDSPPlugin_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = !IsClosing;
            this.Hide();
        }

        private void 플러그인열기ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try{openFileDialog1.InitialDirectory = Setting["LastOpenPath"];}catch(KeyNotFoundException){}
            openFileDialog1.ShowDialog();
        }

        //List<HS_Audio_Lib.DSP> dsp = new List<HS_Audio_Lib.DSP>();
        HS_Audio_Lib.DSP Lastdsp;
        private void 적용ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //if (Lastdsp != null) Lastdsp.remove();
            for (int i = 0; i< listView1.SelectedItems.Count; i++)
            {
                PluginDSP p = FindPlugin(listView1.SelectedItems[i]);
                RESULT d = RESULT.OK;
                if (p != null)
                {
                    if (!p.IsLoad) d = Helper.system.createDSPByPlugin(p.Handle, ref p._DSPHandle);
                    if (d != RESULT.OK) {MessageBox.Show("플러그인 활성화 실패!");return;}
                    p.IsLoad = true; listView1.SelectedItems[i].Text = "●";
                }
            }
            //Lastdsp = Helper.Plugins[int.Parse(listView1.SelectedItems[0].Text)].dsp;
            //Helper.system.addDSP(d, 
        }

        private void 삭제ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("정말 선택한 플러그인을 삭제하시겠습니까?", "HS 플레이어", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == System.Windows.Forms.DialogResult.Yes)
            {
                for (int i = listView1.SelectedItems.Count - 1; i >= 0; i--)
                {
                    PluginDSP p = FindPlugin(listView1.SelectedItems[i]);
                    if(p.dsp!=null) p.dsp.ShowConfigDialog(IntPtr.Zero, false);
                    listView1.Items.Remove(listView1.SelectedItems[i]);
                    Helper.RemovePlugin(p);
                    p.Dispose(); p = null;
                }
            }
        }

        internal PluginDSP FindPlugin(ListViewItem li)
        {
            foreach (PluginDSP p in this.Helper.Plugins) if (p.Handle == Convert.ToUInt32(li.SubItems[6].Text, 16)) return p;
            return null;
        }
        internal ListViewItem FindItem(PluginDSP p)
        {
            if (comboBox1.SelectedIndex == 1)
            {
                foreach (ListViewItem li in this.listView1.Items)
                    if (p.Handle == Convert.ToUInt32(li.SubItems[6].Text, 16)) return li;
            }
            else
            {
                foreach (ListViewItem li in this.ExtPlugin)
                    if (p.Handle == Convert.ToUInt32(li.SubItems[6].Text, 16)) return li;
            }
            return null;
        }

        private void 해제ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < listView1.SelectedItems.Count; i++)
            {
                PluginDSP p = FindPlugin(listView1.SelectedItems[i]);
                RESULT d;
                if (p != null)
                {
                    //Helper.ReleasePlugin(p);
                    p.dsp.DisConnect();
                    p.IsLoad = false; listView1.SelectedItems[i].Text = "";
                }
            }
        }

        HS_Audio_Lib.DSP currentdsp;
        private void 창열기ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            List<IntPtr> ip = new List<IntPtr>();
            RESULT res = RESULT.OK;
            for (int i = 0; i < listView1.SelectedItems.Count; i++)
            {
                PluginDSP p = FindPlugin(listView1.SelectedItems[i]);
                try
                {
                    ip.Add(IntPtr.Zero);
                    //p.Dialog = new Form(); p.Dialog.Show();
                    HS_Audio.Plugin.frmVSTPluginHost vst = p.Dialog as HS_Audio.Plugin.frmVSTPluginHost;
                    HS_Audio.Plugin.VSTPluginControl vstc = vst.vstPluginControl1;
                    //res = p.dsp.ShowConfigDialog(vst.vstPluginControl1.Handle/*p.Dialog.Handle*/, true);

                    System.Drawing.Size sz = vstc.Size;
                    p.Dialog.Show();
                    p.Dialog.BringToFront();
                }
                catch{res = p.dsp.ShowConfigDialog(IntPtr.Zero, true);}
            }
        }

        #region Win32 창 관련
        public delegate bool EnumWindowProc(IntPtr hWnd, IntPtr parameter);

        [DllImport("user32")]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool EnumChildWindows(IntPtr window, EnumWindowProc callback, IntPtr i);

        public static List<IntPtr> GetChildWindows(IntPtr parent)
        {
            List<IntPtr> result = new List<IntPtr>();
            GCHandle listHandle = GCHandle.Alloc(result);
            try
            {
                EnumWindowProc childProc = new EnumWindowProc(EnumWindow);
                EnumChildWindows(parent, childProc, GCHandle.ToIntPtr(listHandle));
            }
            finally
            {
                if (listHandle.IsAllocated)
                    listHandle.Free();
            }
            return result;
        }

        private static bool EnumWindow(IntPtr handle, IntPtr pointer)
        {
            GCHandle gch = GCHandle.FromIntPtr(pointer);
            List<IntPtr> list = gch.Target as List<IntPtr>;
            if (list == null)
                throw new InvalidCastException("GCHandle Target could not be cast as List<IntPtr>");

            list.Add(handle);
            return true;
        }

        [DllImport("user32.dll")]
        static extern IntPtr GetForegroundWindow();

        [DllImport("user32.dll", SetLastError = true)]
        static extern bool GetWindowRect(IntPtr hWnd, out RECT lpRect);

        [StructLayout(LayoutKind.Sequential)]
        public struct RECT
        {
            public int Left;
            public int Top;
            public int Right;
            public int Bottom;
        }
        #endregion

        ListViewItem li;
        bool listView1_ItemChecked_Lock;
        bool IsMainOutput;
        private void listView1_ItemChecked(object sender, ItemCheckedEventArgs e)
        {
            if (!listView1_ItemChecked_Lock)
            {
                PluginDSP p = FindPlugin(e.Item);
                RESULT d = RESULT.OK;
                if (p != null)
                {
                    if (p.PluginType != PLUGINTYPE.DSP)
                    {
                        listView1_ItemChecked_Lock = true;
                        e.Item.Checked = false;
                        listView1_ItemChecked_Lock = false;
                        MessageBox.Show("DSP 플러그인이 아니면 체크 할 수 없습니다.\n\nCODEC 플러그인은 플레이어에 없는 형식도 열 수 있게 해주는\n플러그인 입니다. \n\nOUTPUT 플러그인은 출력하는 장치를 설정할 수 있습니다.", 
                            this.Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                    if (e.Item.Checked)
                    {
                        //HS_Audio_Lib.RESULT red = p.dsp.setBypass(true);
                        //for (int i = listView1.Items.Count - 1; i >= 0; i--) if (listView1.Items[i]!= e.Item) listView1.Items[i].Checked = false;
                        //p.dsp = null;
                        if (!p.IsLoad)
                            if (mainOutputToolStripMenuItem.Checked) {p.dsp.Connect(true);p.IsSystemPlugin = true;}//d = Helper.CreateDSPByPlugin(p);
                            else  {p.dsp.Connect(false);p.IsSystemPlugin = false;}
                        if (d != RESULT.OK)
                        {
                            e.Item.Checked = false;
                            MessageBox.Show("플러그인 활성화 실패! (" + d.ToString() + ")"); return;
                        }
                        else
                        {
                            p.IsLoad = true;
                            Helper.PriorityPlugin(p);
                            RegisterItem(Helper.Plugins);
                        }
                        p.IsLoad = true; //listView1.SelectedItems[i].Text = "●";
                    }
                    else if (p.IsLoad)
                    {/*p.dsp.showConfigDialog(this.Handle, false);*/
                        p.IsLoad = !(p.dsp.DisConnect() == RESULT.OK);
                    }

                    if (!p.IsSystemPlugin) Helper.UpdateMix();
                }
            }
        }
        internal void RegisterItem(PluginDSP[] plugin)
        {
            listView1_ItemChecked_Lock = true;
            listView1.Items.Clear();
            //PluginDSP[] p1 = new PluginDSP[plugin.Length];
            for (int i = 0; i < plugin.Length; i++)
            {
                try
                {
                    if (plugin[i] != null)
                    {
                        PluginDSP p = plugin[i];
                        ListViewItem li = new ListViewItem(p.Priority.ToString());
                        li.Checked = p.IsLoad;
                        if (p.FullName == null || p.FullName == "") p._Name = System.IO.Path.GetFileName(openFileDialog1.FileName);
                        li.SubItems.Add(p.Name);
                        li.SubItems.Add(p.IsSystemPlugin ? "Main" : "Channel");
                        li.SubItems.Add(p.ByPass ? "●" : "");
                        li.SubItems.Add(p.PluginType.ToString() + (p.PluginKind == PluginKind.UNKNOWN ? "" : "(" + p.PluginKind + ")"));
                        li.SubItems.Add("0x" + p.Version.ToHexString(8));
                        li.SubItems.Add("0x" + p.Handle.ToHexString(8));
                        li.SubItems.Add(p.Path);
                        if (comboBox1.SelectedIndex == 0) ExtPlugin.Add(li);
                        else listView1.Items.Add(li);
                    }
                }
                catch{}
            }
            listView1_ItemChecked_Lock = false;
        }

        ListViewItem[] LastSelectPlugin;
        private void contextMenuStrip1_Opening(object sender, System.ComponentModel.CancelEventArgs e)
        {
            삭제ToolStripMenuItem.Enabled = 플러그인우회ToolStripMenuItem.Enabled = 플러그인우회ToolStripMenuItem.Visible=contextMenuStrip1.Enabled=플러그인할당ToolStripMenuItem.Enabled= true;
            플러그인우회ToolStripMenuItem.Text = "플러그인 우회";
            모든플러그인우회ToolStripMenuItem.Visible = false;
            플러그인할당ToolStripMenuItem.Enabled = 창열기ToolStripMenuItem.Enabled = false;

            if (comboBox1.SelectedIndex == 0||listView1.SelectedItems.Count == 0) contextMenuStrip1.Enabled = false;//창열기ToolStripMenuItem.Enabled = 삭제ToolStripMenuItem.Enabled = 플러그인우회ToolStripMenuItem.Enabled = 플러그인할당ToolStripMenuItem .Enabled= false;
            else if (listView1.SelectedItems.Count == 1)
            {
                플러그인우회ToolStripMenuItem.Visible =해당폴더열기ToolStripMenuItem.Enabled= true;
                모든플러그인우회ToolStripMenuItem.Visible = 모든플러그인우회취소ToolStripMenuItem.Visible = false;
                플러그인우회ToolStripMenuItem.Enabled = false;
                PluginDSP p=FindPlugin(listView1.SelectedItems[0]);
                HSAudioDSPHelper dsp = p == null ? null : p.dsp;

                if (p == null) 플러그인우회ToolStripMenuItem.Text = "(선택된 플러그인을 찾을 수 없어 우회가 비활성화 되었습니다.)";
                else if (dsp == null)플러그인우회ToolStripMenuItem.Text = "(선택된 플러그인의 내부 DSP속성이 존재하지 않아 우회가 비활성화 되었습니다.)";
                else
                {
                    bool ByPass = p.dsp.Bypass;
                    RESULT res = p.dsp.Result;
                    if (res == RESULT.OK) {플러그인우회ToolStripMenuItem.Checked = ByPass;플러그인우회ToolStripMenuItem.Enabled = true;플러그인할당ToolStripMenuItem.Enabled = 창열기ToolStripMenuItem.Enabled = true;}
                    else {플러그인우회ToolStripMenuItem.Text = "(선택된 플러그인의 내부 DSP오류가 발생하여 우회가 비활성화 되었습니다.)";}
                }

                if (comboBox1.SelectedIndex == 0) 플러그인우회ToolStripMenuItem.Text = "(내부 플러그인은 우회가 불가능 합니다.)";
                else LastSelectPlugin = new ListViewItem[1] { listView1.SelectedItems[0] };

                if (p.PluginType == PLUGINTYPE.CODEC) 플러그인우회ToolStripMenuItem.Text = "(코덱 플러그인은 우회가 불가능 합니다.)";
                else if (p.PluginType == PLUGINTYPE.OUTPUT) 플러그인우회ToolStripMenuItem.Text = "(출력 플러그인은 우회가 불가능 합니다.)";
            }
            else
            {
                플러그인우회ToolStripMenuItem.Visible = false;
                모든플러그인우회ToolStripMenuItem.Visible = 모든플러그인우회취소ToolStripMenuItem.Visible=true;

                if (comboBox1.SelectedIndex == 0)
                {
                    모든플러그인우회ToolStripMenuItem.Text = "(내부 플러그인은 우회가 불가능 합니다.)";
                    모든플러그인우회취소ToolStripMenuItem.Text = "(내부 플러그인은 우회취소가 불가능 합니다.)";
                }
                else if (comboBox1.SelectedIndex > 0)
                {
                    모든플러그인우회ToolStripMenuItem.Text = "선택된 플러그인 우회";
                    모든플러그인우회취소ToolStripMenuItem.Text = "선택된 플러그인 우회 취소";
                }

                LastSelectPlugin = new ListViewItem[listView1.SelectedItems.Count];
                for (int i = 0; i < listView1.SelectedItems.Count; i++)
                    LastSelectPlugin[i] = listView1.SelectedItems[i];
            }
        }

        private void 플러그인우회ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            PluginDSP p = FindPlugin(LastSelectPlugin[0]);
            RESULT res = RESULT.OK;
            if (플러그인우회ToolStripMenuItem.Checked)
            {
                //System.Threading.ThreadStart ths = new System.Threading.ThreadStart(()=>{p.dsp.setBypass(플러그인우회ToolStripMenuItem.Checked);Thread.CurrentThread.Abort();});
                //System.Threading.Thread th = new System.Threading.Thread(ths);th.Start();
                //System.Runtime.InteropServices.Marshal
                //ThreadPool.RegisterWaitForSingleObject(

                p.dsp.Bypass = p.ByPass = false;
                res = p.dsp.Result;
                LastSelectPlugin[0].SubItems[3].Text = "";
                //플러그인우회ToolStripMenuItem.Checked = false;
            }
            else
            {
                p.dsp.Bypass = p.ByPass = true;
                res = p.dsp.Result;
                LastSelectPlugin[0].SubItems[3].Text = "●";
                //플러그인우회ToolStripMenuItem.Checked = true;
            }
        }

        private void 플러그인우회ToolStripMenuItem_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void saveFileDialog1_FileOk(object sender, System.ComponentModel.CancelEventArgs e)
        {

        }

        private void 해당폴더열기ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                string a = Environment.GetEnvironmentVariable("windir") + "\\explorer.exe";
                for (int i = 0; i < listView1.SelectedItems.Count; i++)
                {System.Diagnostics.Process.Start(a, "/select," + listView1.SelectedItems[i].SubItems[7].Text);}
            }
            catch { }
        }

        private void mainOutputToolStripMenuItem_Click(object sender, EventArgs e)
        {
            PluginDSP p = FindPlugin(LastSelectPlugin[0]);
            if (mainOutputToolStripMenuItem.Checked)
            {
                if (p.IsLoad)
                {
                    p.IsSystemPlugin = false;
                    p.dsp.Connect(false);
                    //p.dsp.UpdateMainDSP();
                    //p.dsp.ReRegisterDSP(false);
                    Helper.PriorityPlugin(p);
                    RegisterItem(Helper.Plugins);
                }
                //else  LastSelectPlugin[0].SubItems[2].Text = "Main";

                mainOutputToolStripMenuItem.Checked = false;
                channelToolStripMenuItem.Checked = true;
            }
            else
            {
                if (p.IsLoad)
                {
                    p.IsSystemPlugin = true;
                    //p.dsp.ReRegisterDSP(true);
                    p.dsp.Connect(true);
                    Helper.PriorityPlugin(p);
                    RegisterItem(Helper.Plugins);
                }
                //else LastSelectPlugin[0].SubItems[2].Text = "Channel";
                mainOutputToolStripMenuItem.Checked = true;
                channelToolStripMenuItem.Checked = false;
            }
            Helper.UpdateMix();
        }

        private void 플러그인할당ToolStripMenuItem_DropDownOpening(object sender, EventArgs e)
        {
            PluginDSP p = FindPlugin(LastSelectPlugin[0]);
            mainOutputToolStripMenuItem.Checked = p.IsSystemPlugin;
            channelToolStripMenuItem.Checked = !p.IsSystemPlugin;
        }

        private void 모든플러그인우회ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ListViewItem[] a = LastSelectPlugin;
            for (int i = 0; i < a.Length; i++)
            {
                try
                {
                    PluginDSP p = FindPlugin(LastSelectPlugin[i]);
                    RESULT res = RESULT.OK;
                    if (p.dsp != null)
                    {
                        p.dsp.Bypass = p.ByPass = true;
                        res = p.dsp.Result;
                        LastSelectPlugin[i].SubItems[3].Text = "●";
                    }
                }
                catch{}
            }
        }

        private void 모든플러그인우회취소ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ListViewItem[] a = LastSelectPlugin;
            for (int i = 0; i < a.Length; i++)
            {
                try
                {
                    PluginDSP p = FindPlugin(LastSelectPlugin[i]);
                    RESULT res = RESULT.OK;

                    //플러그인우회ToolStripMenuItem.Checked = true;
                    //System.Threading.ThreadStart ths = new System.Threading.ThreadStart(()=>{p.dsp.setBypass(플러그인우회ToolStripMenuItem.Checked);Thread.CurrentThread.Abort();});
                    //System.Threading.Thread th = new System.Threading.Thread(ths);th.Start();
                    //System.Runtime.InteropServices.Marshal
                    //ThreadPool.RegisterWaitForSingleObject(
                    if (p.dsp != null)
                    {
                        p.dsp.Bypass = p.ByPass = false;
                        res = p.dsp.Result;
                        LastSelectPlugin[i].SubItems[3].Text = "";
                    }
                }catch{}
            }
        }


        Thread CalculatePeakThread;
        ThreadStart CalculatePeakThreadStart;
        private void checkBox2_CheckedChanged(object sender, EventArgs e)
        {
            if (CalculatePeakThread != null) CalculatePeakThread.Abort();
            if (checkBox2.Checked)
            {
                CalculatePeakThread = new Thread(CalculatePeakThreadStart);
                //Plugin.dsp.dspreadcallback += PK;
                //Plugin.dsp.ReRegisterDSP();
                CalculatePeakThread.Start();
            }
            else vuMeter1.Level = vuMeter2.Level = 0; //Plugin.dsp.dspreadcallback -= PK;
        }


        float Leftmax, RightMax;
        float[] tmpLeft = new float[2048], tmpRight = new float[2048];
        int count;
        private void CalculatePeakThreadStart_Loop()
        {
            try
            {
                //Plugin.dsp.RegisterCALLBACK(PK);
                while (true)
                {
                    Leftmax = float.NegativeInfinity;
                    RightMax = float.NegativeInfinity;

                    Helper.system.getWaveData(tmpLeft, tmpLeft.Length, 0);
                    Helper.system.getWaveData(tmpRight, tmpRight.Length, 1);

                    for (count = 0; count < tmpLeft.Length; count++) if (Leftmax < tmpLeft[count]) Leftmax = tmpLeft[count];
                    for (count = 0; count < tmpRight.Length; count++) if (RightMax < tmpRight[count]) RightMax = tmpRight[count];
                    /*
                    vuMeter1.InvokeIfNeeded(() =>
                    {
                        
                    });*/
                    vuMeter1.Level = Leftmax > 1 || Leftmax < -1 ? 65536 : (int)(Math.Abs(Leftmax) * 65536);
                    vuMeter2.Level = RightMax > 1 || RightMax < -1 ? 65536 : (int)(Math.Abs(RightMax) * 65536);
                    Thread.Sleep(25);
                }
            }
            finally
            {
                //Plugin.dsp.UnRegisterCALLBACK();
            }
        }
    }
}
