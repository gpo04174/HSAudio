﻿namespace HS_Audio.Forms
{
    partial class frmDSPPlugin
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing); 
            if (CalculatePeakThread != null) CalculatePeakThread.Abort();
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.파일FToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.플러그인열기ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.플러그인목록저장ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.플러그인목록열기ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.플러그인관리ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.윈앰프플러그인경로설정ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.보기VToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.새로고침ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.listView1 = new System.Windows.Forms.ListView();
            this.columnHeader0 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader7 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader6 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader5 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.창열기ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.플러그인우회ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.모든플러그인우회ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.모든플러그인우회취소ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.플러그인할당ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mainOutputToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.channelToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.해당폴더열기ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator7 = new System.Windows.Forms.ToolStripSeparator();
            this.삭제ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.checkBox2 = new System.Windows.Forms.CheckBox();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.vuMeter1 = new HS_Audio.Control.VuMeter();
            this.vuMeter2 = new HS_Audio.Control.VuMeter();
            this.statusStrip1.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.contextMenuStrip1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // statusStrip1
            // 
            this.statusStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1});
            this.statusStrip1.Location = new System.Drawing.Point(0, 345);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(953, 22);
            this.statusStrip1.TabIndex = 0;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(31, 17);
            this.toolStripStatusLabel1.Text = "준비";
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.파일FToolStripMenuItem,
            this.보기VToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(953, 24);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // 파일FToolStripMenuItem
            // 
            this.파일FToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.플러그인열기ToolStripMenuItem,
            this.toolStripSeparator2,
            this.플러그인목록저장ToolStripMenuItem,
            this.플러그인목록열기ToolStripMenuItem,
            this.toolStripSeparator1,
            this.플러그인관리ToolStripMenuItem});
            this.파일FToolStripMenuItem.Name = "파일FToolStripMenuItem";
            this.파일FToolStripMenuItem.Size = new System.Drawing.Size(57, 20);
            this.파일FToolStripMenuItem.Text = "파일(&F)";
            // 
            // 플러그인열기ToolStripMenuItem
            // 
            this.플러그인열기ToolStripMenuItem.Name = "플러그인열기ToolStripMenuItem";
            this.플러그인열기ToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.O)));
            this.플러그인열기ToolStripMenuItem.Size = new System.Drawing.Size(193, 22);
            this.플러그인열기ToolStripMenuItem.Text = "플러그인 열기";
            this.플러그인열기ToolStripMenuItem.Click += new System.EventHandler(this.플러그인열기ToolStripMenuItem_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(190, 6);
            // 
            // 플러그인목록저장ToolStripMenuItem
            // 
            this.플러그인목록저장ToolStripMenuItem.Enabled = false;
            this.플러그인목록저장ToolStripMenuItem.Name = "플러그인목록저장ToolStripMenuItem";
            this.플러그인목록저장ToolStripMenuItem.Size = new System.Drawing.Size(193, 22);
            this.플러그인목록저장ToolStripMenuItem.Text = "플러그인 목록 저장";
            // 
            // 플러그인목록열기ToolStripMenuItem
            // 
            this.플러그인목록열기ToolStripMenuItem.Enabled = false;
            this.플러그인목록열기ToolStripMenuItem.Name = "플러그인목록열기ToolStripMenuItem";
            this.플러그인목록열기ToolStripMenuItem.Size = new System.Drawing.Size(193, 22);
            this.플러그인목록열기ToolStripMenuItem.Text = "플러그인 목록 열기";
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(190, 6);
            // 
            // 플러그인관리ToolStripMenuItem
            // 
            this.플러그인관리ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.윈앰프플러그인경로설정ToolStripMenuItem});
            this.플러그인관리ToolStripMenuItem.Enabled = false;
            this.플러그인관리ToolStripMenuItem.Name = "플러그인관리ToolStripMenuItem";
            this.플러그인관리ToolStripMenuItem.Size = new System.Drawing.Size(193, 22);
            this.플러그인관리ToolStripMenuItem.Text = "플러그 인 관리";
            // 
            // 윈앰프플러그인경로설정ToolStripMenuItem
            // 
            this.윈앰프플러그인경로설정ToolStripMenuItem.Name = "윈앰프플러그인경로설정ToolStripMenuItem";
            this.윈앰프플러그인경로설정ToolStripMenuItem.Size = new System.Drawing.Size(218, 22);
            this.윈앰프플러그인경로설정ToolStripMenuItem.Text = "윈앰프 플러그인 경로 설정";
            // 
            // 보기VToolStripMenuItem
            // 
            this.보기VToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.새로고침ToolStripMenuItem});
            this.보기VToolStripMenuItem.Name = "보기VToolStripMenuItem";
            this.보기VToolStripMenuItem.Size = new System.Drawing.Size(59, 20);
            this.보기VToolStripMenuItem.Text = "보기(&V)";
            // 
            // 새로고침ToolStripMenuItem
            // 
            this.새로고침ToolStripMenuItem.Enabled = false;
            this.새로고침ToolStripMenuItem.Name = "새로고침ToolStripMenuItem";
            this.새로고침ToolStripMenuItem.Size = new System.Drawing.Size(122, 22);
            this.새로고침ToolStripMenuItem.Text = "새로고침";
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 24);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(953, 321);
            this.tabControl1.TabIndex = 2;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.listView1);
            this.tabPage1.Controls.Add(this.comboBox1);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(945, 295);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "플러그인 목록";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // listView1
            // 
            this.listView1.CheckBoxes = true;
            this.listView1.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader0,
            this.columnHeader1,
            this.columnHeader7,
            this.columnHeader6,
            this.columnHeader4,
            this.columnHeader2,
            this.columnHeader5,
            this.columnHeader3});
            this.listView1.ContextMenuStrip = this.contextMenuStrip1;
            this.listView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listView1.FullRowSelect = true;
            this.listView1.LabelWrap = false;
            this.listView1.Location = new System.Drawing.Point(3, 23);
            this.listView1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.listView1.Name = "listView1";
            this.listView1.Size = new System.Drawing.Size(939, 269);
            this.listView1.TabIndex = 1;
            this.listView1.UseCompatibleStateImageBehavior = false;
            this.listView1.View = System.Windows.Forms.View.Details;
            this.listView1.ItemChecked += new System.Windows.Forms.ItemCheckedEventHandler(this.listView1_ItemChecked);
            // 
            // columnHeader0
            // 
            this.columnHeader0.Text = "순위";
            this.columnHeader0.Width = 38;
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "이름";
            this.columnHeader1.Width = 236;
            // 
            // columnHeader7
            // 
            this.columnHeader7.Text = "할당";
            this.columnHeader7.Width = 80;
            // 
            // columnHeader6
            // 
            this.columnHeader6.Text = "우회";
            this.columnHeader6.Width = 40;
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "타입";
            this.columnHeader4.Width = 104;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "버전";
            this.columnHeader2.Width = 78;
            // 
            // columnHeader5
            // 
            this.columnHeader5.Text = "핸들";
            this.columnHeader5.Width = 90;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "경로";
            this.columnHeader3.Width = 300;
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.창열기ToolStripMenuItem,
            this.toolStripSeparator4,
            this.플러그인우회ToolStripMenuItem,
            this.모든플러그인우회ToolStripMenuItem,
            this.모든플러그인우회취소ToolStripMenuItem,
            this.플러그인할당ToolStripMenuItem,
            this.toolStripSeparator5,
            this.해당폴더열기ToolStripMenuItem,
            this.toolStripSeparator7,
            this.삭제ToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(219, 176);
            this.contextMenuStrip1.Opening += new System.ComponentModel.CancelEventHandler(this.contextMenuStrip1_Opening);
            // 
            // 창열기ToolStripMenuItem
            // 
            this.창열기ToolStripMenuItem.Name = "창열기ToolStripMenuItem";
            this.창열기ToolStripMenuItem.Size = new System.Drawing.Size(218, 22);
            this.창열기ToolStripMenuItem.Text = "창 열기";
            this.창열기ToolStripMenuItem.Click += new System.EventHandler(this.창열기ToolStripMenuItem_Click);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(215, 6);
            // 
            // 플러그인우회ToolStripMenuItem
            // 
            this.플러그인우회ToolStripMenuItem.Name = "플러그인우회ToolStripMenuItem";
            this.플러그인우회ToolStripMenuItem.Size = new System.Drawing.Size(218, 22);
            this.플러그인우회ToolStripMenuItem.Text = "플러그인 우회";
            this.플러그인우회ToolStripMenuItem.Click += new System.EventHandler(this.플러그인우회ToolStripMenuItem_Click);
            // 
            // 모든플러그인우회ToolStripMenuItem
            // 
            this.모든플러그인우회ToolStripMenuItem.Name = "모든플러그인우회ToolStripMenuItem";
            this.모든플러그인우회ToolStripMenuItem.Size = new System.Drawing.Size(218, 22);
            this.모든플러그인우회ToolStripMenuItem.Text = "선택된 플러그인 우회";
            this.모든플러그인우회ToolStripMenuItem.Visible = false;
            this.모든플러그인우회ToolStripMenuItem.Click += new System.EventHandler(this.모든플러그인우회ToolStripMenuItem_Click);
            // 
            // 모든플러그인우회취소ToolStripMenuItem
            // 
            this.모든플러그인우회취소ToolStripMenuItem.Name = "모든플러그인우회취소ToolStripMenuItem";
            this.모든플러그인우회취소ToolStripMenuItem.Size = new System.Drawing.Size(218, 22);
            this.모든플러그인우회취소ToolStripMenuItem.Text = "선택된 플러그인 우회 취소";
            this.모든플러그인우회취소ToolStripMenuItem.Visible = false;
            this.모든플러그인우회취소ToolStripMenuItem.Click += new System.EventHandler(this.모든플러그인우회취소ToolStripMenuItem_Click);
            // 
            // 플러그인할당ToolStripMenuItem
            // 
            this.플러그인할당ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mainOutputToolStripMenuItem,
            this.channelToolStripMenuItem});
            this.플러그인할당ToolStripMenuItem.Name = "플러그인할당ToolStripMenuItem";
            this.플러그인할당ToolStripMenuItem.Size = new System.Drawing.Size(218, 22);
            this.플러그인할당ToolStripMenuItem.Text = "플러그인 할당";
            this.플러그인할당ToolStripMenuItem.DropDownOpening += new System.EventHandler(this.플러그인할당ToolStripMenuItem_DropDownOpening);
            // 
            // mainOutputToolStripMenuItem
            // 
            this.mainOutputToolStripMenuItem.Checked = true;
            this.mainOutputToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.mainOutputToolStripMenuItem.Name = "mainOutputToolStripMenuItem";
            this.mainOutputToolStripMenuItem.Size = new System.Drawing.Size(143, 22);
            this.mainOutputToolStripMenuItem.Text = "Main Output";
            this.mainOutputToolStripMenuItem.Click += new System.EventHandler(this.mainOutputToolStripMenuItem_Click);
            // 
            // channelToolStripMenuItem
            // 
            this.channelToolStripMenuItem.Name = "channelToolStripMenuItem";
            this.channelToolStripMenuItem.Size = new System.Drawing.Size(143, 22);
            this.channelToolStripMenuItem.Text = "Channel";
            this.channelToolStripMenuItem.Click += new System.EventHandler(this.mainOutputToolStripMenuItem_Click);
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(215, 6);
            // 
            // 해당폴더열기ToolStripMenuItem
            // 
            this.해당폴더열기ToolStripMenuItem.Name = "해당폴더열기ToolStripMenuItem";
            this.해당폴더열기ToolStripMenuItem.Size = new System.Drawing.Size(218, 22);
            this.해당폴더열기ToolStripMenuItem.Text = "해당 폴더 열기";
            this.해당폴더열기ToolStripMenuItem.Click += new System.EventHandler(this.해당폴더열기ToolStripMenuItem_Click);
            // 
            // toolStripSeparator7
            // 
            this.toolStripSeparator7.Name = "toolStripSeparator7";
            this.toolStripSeparator7.Size = new System.Drawing.Size(215, 6);
            // 
            // 삭제ToolStripMenuItem
            // 
            this.삭제ToolStripMenuItem.Name = "삭제ToolStripMenuItem";
            this.삭제ToolStripMenuItem.Size = new System.Drawing.Size(218, 22);
            this.삭제ToolStripMenuItem.Text = "삭제";
            this.삭제ToolStripMenuItem.Click += new System.EventHandler(this.삭제ToolStripMenuItem_Click);
            // 
            // comboBox1
            // 
            this.comboBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.comboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Items.AddRange(new object[] {
            "내장 플러그인",
            "사용자 플러그인"});
            this.comboBox1.Location = new System.Drawing.Point(3, 3);
            this.comboBox1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(939, 20);
            this.comboBox1.TabIndex = 0;
            this.comboBox1.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // tabPage2
            // 
            this.tabPage2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.tabPage2.Controls.Add(this.groupBox1);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(945, 295);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "설정";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.checkBox2);
            this.groupBox1.Controls.Add(this.vuMeter1);
            this.groupBox1.Controls.Add(this.vuMeter2);
            this.groupBox1.Location = new System.Drawing.Point(3, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(415, 130);
            this.groupBox1.TabIndex = 5;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "                         ";
            // 
            // checkBox2
            // 
            this.checkBox2.AutoSize = true;
            this.checkBox2.ForeColor = System.Drawing.Color.Lime;
            this.checkBox2.Location = new System.Drawing.Point(11, 0);
            this.checkBox2.Name = "checkBox2";
            this.checkBox2.Size = new System.Drawing.Size(96, 16);
            this.checkBox2.TabIndex = 4;
            this.checkBox2.Text = "VU 미터 사용";
            this.checkBox2.UseVisualStyleBackColor = true;
            this.checkBox2.CheckedChanged += new System.EventHandler(this.checkBox2_CheckedChanged);
            // 
            // saveFileDialog1
            // 
            this.saveFileDialog1.FileOk += new System.ComponentModel.CancelEventHandler(this.saveFileDialog1_FileOk);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.DefaultExt = "*.dll";
            this.openFileDialog1.Filter = "플러그인 파일 (*.dll)|*.dll|코덱 (*.ax)|*.ax";
            this.openFileDialog1.Multiselect = true;
            this.openFileDialog1.Title = "DSP 플러그인 파일 열기";
            this.openFileDialog1.FileOk += new System.ComponentModel.CancelEventHandler(this.openFileDialog1_FileOk);
            // 
            // folderBrowserDialog1
            // 
            this.folderBrowserDialog1.Description = "윈앰프 플러그인이 있는 폴더 선택";
            // 
            // vuMeter1
            // 
            this.vuMeter1.AnalogMeter = true;
            this.vuMeter1.BackColor = System.Drawing.Color.Transparent;
            this.vuMeter1.DialBackground = System.Drawing.Color.White;
            this.vuMeter1.DialTextNegative = System.Drawing.Color.Black;
            this.vuMeter1.DialTextPositive = System.Drawing.Color.Red;
            this.vuMeter1.DialTextZero = System.Drawing.Color.DarkGreen;
            this.vuMeter1.Font = new System.Drawing.Font("Georgia", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.vuMeter1.Led1ColorOff = System.Drawing.Color.DarkGreen;
            this.vuMeter1.Led1ColorOn = System.Drawing.Color.LimeGreen;
            this.vuMeter1.Led1Count = 32;
            this.vuMeter1.Led2ColorOff = System.Drawing.Color.Olive;
            this.vuMeter1.Led2ColorOn = System.Drawing.Color.Yellow;
            this.vuMeter1.Led2Count = 10;
            this.vuMeter1.Led3ColorOff = System.Drawing.Color.Maroon;
            this.vuMeter1.Led3ColorOn = System.Drawing.Color.Red;
            this.vuMeter1.Led3Count = 4;
            this.vuMeter1.LedSize = new System.Drawing.Size(10, 15);
            this.vuMeter1.LedSpace = 1;
            this.vuMeter1.Level = 0;
            this.vuMeter1.LevelMax = 65535;
            this.vuMeter1.Location = new System.Drawing.Point(3, 19);
            this.vuMeter1.MeterScale = HS_Audio.Control.MeterScale.Log10;
            this.vuMeter1.Name = "vuMeter1";
            this.vuMeter1.NeedleColor = System.Drawing.Color.Gray;
            this.vuMeter1.PeakHold = false;
            this.vuMeter1.Peakms = 50;
            this.vuMeter1.PeakNeedleColor = System.Drawing.Color.Silver;
            this.vuMeter1.ShowDialOnly = false;
            this.vuMeter1.ShowLedPeak = false;
            this.vuMeter1.ShowTextInDial = true;
            this.vuMeter1.Size = new System.Drawing.Size(203, 162);
            this.vuMeter1.TabIndex = 2;
            this.vuMeter1.TextInDial = new string[] {
        "-40",
        "-20",
        "-10",
        "-5",
        "0",
        "+6"};
            this.vuMeter1.UseLedLight = false;
            this.vuMeter1.VerticalBar = true;
            this.vuMeter1.VuText = "VU - L";
            // 
            // vuMeter2
            // 
            this.vuMeter2.AnalogMeter = true;
            this.vuMeter2.BackColor = System.Drawing.Color.Transparent;
            this.vuMeter2.DialBackground = System.Drawing.Color.White;
            this.vuMeter2.DialTextNegative = System.Drawing.Color.Black;
            this.vuMeter2.DialTextPositive = System.Drawing.Color.Red;
            this.vuMeter2.DialTextZero = System.Drawing.Color.DarkGreen;
            this.vuMeter2.Font = new System.Drawing.Font("Georgia", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.vuMeter2.Led1ColorOff = System.Drawing.Color.DarkGreen;
            this.vuMeter2.Led1ColorOn = System.Drawing.Color.LimeGreen;
            this.vuMeter2.Led1Count = 32;
            this.vuMeter2.Led2ColorOff = System.Drawing.Color.Olive;
            this.vuMeter2.Led2ColorOn = System.Drawing.Color.Yellow;
            this.vuMeter2.Led2Count = 10;
            this.vuMeter2.Led3ColorOff = System.Drawing.Color.Maroon;
            this.vuMeter2.Led3ColorOn = System.Drawing.Color.Red;
            this.vuMeter2.Led3Count = 4;
            this.vuMeter2.LedSize = new System.Drawing.Size(10, 15);
            this.vuMeter2.LedSpace = 1;
            this.vuMeter2.Level = 0;
            this.vuMeter2.LevelMax = 65535;
            this.vuMeter2.Location = new System.Drawing.Point(208, 19);
            this.vuMeter2.MeterScale = HS_Audio.Control.MeterScale.Log10;
            this.vuMeter2.Name = "vuMeter2";
            this.vuMeter2.NeedleColor = System.Drawing.Color.Gray;
            this.vuMeter2.PeakHold = false;
            this.vuMeter2.Peakms = 50;
            this.vuMeter2.PeakNeedleColor = System.Drawing.Color.Silver;
            this.vuMeter2.ShowDialOnly = false;
            this.vuMeter2.ShowLedPeak = false;
            this.vuMeter2.ShowTextInDial = true;
            this.vuMeter2.Size = new System.Drawing.Size(203, 162);
            this.vuMeter2.TabIndex = 3;
            this.vuMeter2.TextInDial = new string[] {
        "-40",
        "-20",
        "-10",
        "-5",
        "0",
        "+6"};
            this.vuMeter2.UseLedLight = false;
            this.vuMeter2.VerticalBar = true;
            this.vuMeter2.VuText = "VU - R";
            // 
            // frmDSPPlugin
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(953, 367);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "frmDSPPlugin";
            this.Text = "DSP 플러그인 관리자";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmDSPPlugin_FormClosing);
            this.Load += new System.EventHandler(this.frmDSPPlugin_Load);
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.contextMenuStrip1.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem 파일FToolStripMenuItem;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.ToolStripMenuItem 플러그인관리ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 윈앰프플러그인경로설정ToolStripMenuItem;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.ToolStripMenuItem 플러그인목록저장ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 플러그인목록열기ToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ListView listView1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
        private System.Windows.Forms.ColumnHeader columnHeader0;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private System.Windows.Forms.ColumnHeader columnHeader5;
        private System.Windows.Forms.ToolStripMenuItem 플러그인열기ToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem 삭제ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 창열기ToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripMenuItem 플러그인우회ToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.ToolStripMenuItem 보기VToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 새로고침ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 해당폴더열기ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 플러그인할당ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem mainOutputToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem channelToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator7;
        private System.Windows.Forms.ToolStripMenuItem 모든플러그인우회ToolStripMenuItem;
        private System.Windows.Forms.ColumnHeader columnHeader6;
        private System.Windows.Forms.ColumnHeader columnHeader7;
        private System.Windows.Forms.ToolStripMenuItem 모든플러그인우회취소ToolStripMenuItem;
        private HS_Audio.Control.VuMeter vuMeter2;
        private HS_Audio.Control.VuMeter vuMeter1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.CheckBox checkBox2;
    }
}