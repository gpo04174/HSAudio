﻿using System;
using System.Threading;
using System.Windows.Forms;
using HS_Audio_Helper;
using HS_Audio_Helper.DSP;

namespace HS_Audio_Helper.Forms
{
    public partial class frmAGC : Form
    {
        

        double[] Peak = new double[1];
        float[] PeakValue = new float[1];
        float[] AutoRatio = new float[1];
        float MaxdB = 0;
        bool Channel_Seperate;
        public HS_Audio_Lib.RESULT READCALLBACK_AGC(ref HS_Audio_Lib.DSP_STATE dsp_state, IntPtr InBuf, IntPtr OutBuf, uint length, int inchannels, int outchannels)
        {
            uint count = 0;
            int count2 = 0;
            float bb = 0;
            IsMax = new bool[outchannels];
            unsafe
            {
                float* b;
                float* inbuffer = (float*)InBuf.ToPointer();
                float* outbuffer = (float*)OutBuf.ToPointer();

                if (Peak.Length != outchannels) {Peak = new double[outchannels];}
                if (PeakValue.Length != outchannels) PeakValue = new float[outchannels];
                if (AutoRatio.Length != outchannels) AutoRatio = new float[outchannels];
                for (count = 0; count < length; count++)
                {
                    for (count2 = 0; count2 < outchannels; count2++)
                    {
                        b = &inbuffer[(count * outchannels) + count2];
                        double pk = Decibels(b[0], 0);
                        if (pk > MaxdB)
                        {
                            if (Channel_Seperate)
                            {
                                IsMax[count2] = true;
                                if (pk > Peak[count2])
                                {
                                    //HS_Audio_Helper.DSP.HSDSP.AGC(inbuffer, outbuffer, AGC, length, outchannels); goto Exit;
                                    PeakValue[count2] = b[0];
                                    Peak[count2] = pk;
                                }
                            }
                            else
                            {
                                IsMax[0] = true;
                                if (pk > Peak[0])
                                {
                                    //HS_Audio_Helper.DSP.HSDSP.AGC(inbuffer, outbuffer, AGC, length, outchannels); goto Exit;
                                    PeakValue[0] = b[0];
                                    Peak[0] = pk;
                                }
                            }
                        }
                    }
                }
                /*
                for (count = 0; count < length; count++)
                    for (count2 = 0; count2 < outchannels; count2++)
                        outbuffer[(count * outchannels) + count2] = inbuffer[(count * outchannels) + count2];
                goto Exit;*/
                //float* MaxHigh_L, MaxHigh_R, MaxLow_L, MaxLow_R;
                //float[] a = new float[outchannels * 2];
                                /*
                for (count = 0; count < length; count++)
                {
                    for (count2 = 0; count2 < outchannels; count2++)
                    {
                        b = &inbuffer[(count * outchannels) + count2];
                        if (b[0] > 1.0f && Peak[count2] < (b[0] - 1.0f)) Peak[count2] = b[0] - 1.0f;
                        if (b[0] < -1.0f && Peak[count2 + outchannels] > (b[0] + 1.0f)) Peak[count2 + outchannels] = b[0] + 1.0f;
                    }
                }*/
                if (MaxdB == 0) MaxdB = -0.9999999f;
                for (count = 0; count < length; count++)
                {
                    for (count2 = 0; count2 < outchannels; count2++)
                    {
                        bb = Channel_Seperate ? (float)Peak[count2] : (float)Peak[0];
                        b = &inbuffer[(count * outchannels) + count2];
                        outbuffer[(count * outchannels) + count2] = Channel_Seperate?
                            b[0].BassBoosta(200, IsMax[count2] ? bb : MaxdB, IsMax[count2]?AutoRatio[count2]:0):
                            b[0].BassBoosta(200, IsMax[0] ? bb : MaxdB, IsMax[0] ? AutoRatio[0] : 0);
                    }
                }
                /*
                float[] a1 = new float[outchannels];
                for(int i = 0; i < a1.Length; i++)a1[i] = float.MinValue;
                for (count2 = 0; count2 < outchannels; count2++)
                    if (Decibels(a[count2],0) > Decibels(a[count2+outchannels], 0)) a1[count2] = a[count2];
                    else a1[count2] = a[count2+outchannels];
                double a2 = double.MinValue;
                for (count = 0; count < length; count++)
                {
                    for (count2 = 0; count2 < outchannels; count2++)
                    {
                        b = &inbuffer[(count * inchannels) + count2];
                        if(Minus)
                        {
                            outbuffer[(count * outchannels) + count2] = b[0]>0?b[0] - a1[count2]:b[0] + a1[count2];
                            a2 = Decibels(outbuffer[(count * outchannels) + count2],0);
                            //if (b[0]>0f) outbuffer[(count * outchannels) + count2] = b[0] - a1[count2];
                            //else outbuffer[(count * outchannels) + count2] = b[0] - a[count2 + outchannels];
                        }
                        else
                        {
                            if (b[0] > 1.0f) outbuffer[(count * outchannels) + count2] = b[0] - a[count2];
                            else if (b[0] < -1.0f) outbuffer[(count * outchannels) + count2] = b[0] - a[count2 + outchannels];
                            else outbuffer[(count * outchannels) + count2] = b[0];
                        }
                    }
                }*/
            }

        Exit:
            return HS_Audio_Lib.RESULT.OK;
        }

        Thread AutoGainThread;
        bool[] IsMax = new bool[1];
        float bb, aa;
        void AutoGainThread_Loop(object o)
        {
            AutoGainThread = Thread.CurrentThread;
            aa = float.NaN;
            bb = float.NaN;
            while (true)
            {
                try
                {
                    if (Peak != null)
                    {
                        for (int i = 0; i < (Channel_Seperate?PeakValue.Length:1); i++)
                        {
                            if (IsMax.Length != PeakValue.Length)
                            {
                                IsMax = new bool[PeakValue.Length]; break;
                            }
                            bb = (float)Peak[i];
                            if (IsMax[i]||bb>MaxdB)
                            {
                                for (int j = -200; j <= 0; j++)
                                {
                                    aa = PeakValue[i].BassBoosta(200, MaxdB, j); if (Decibels(aa, 0) >= MaxdB)
                                    {
                                        AutoRatio[i] = j - 1;
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }
                catch
                {
                }
                Thread.Sleep(1);
            }
        }

        public HS_Audio_Lib.RESULT READCALLBACK_BassBooster(ref HS_Audio_Lib.DSP_STATE dsp_state, IntPtr InBuf, IntPtr OutBuf, uint length, int inchannels, int outchannels)
        {
            uint count = 0;
            int count2 = 0;

            unsafe
            {
                float* b;
                float* inbuffer = (float*)InBuf.ToPointer();
                float* outbuffer = (float*)OutBuf.ToPointer();
                //if(A == b&c == d && e == f)

                for (count = 0; count < length; count++)
                {
                    for (count2 = 0; count2 < outchannels; count2++)
                    {
                        b = &inbuffer[(count * outchannels) + count2];
                        outbuffer[(count * outchannels) + count2] = HS_Audio_Helper.DSP.HSDSP.BassBoosta(b[0], Hz, Gain, Ratio);
                    }
                }
            }
            return HS_Audio_Lib.RESULT.OK;
        }
        public frmAGC(HSAudioHelper Helper)
        {
            InitializeComponent();
            DSP[0] = new HSAudioDSPHelper(Helper,READCALLBACK_AGC,true);
            DSP[1] = new HSAudioDSPHelper(Helper, READCALLBACK_BassBooster,true);
            DSP[0].Bypass = DSP[1].Bypass = true;
        }

        private void frmAGC_Load(object sender, EventArgs e)
        {
        }
        HSAudioDSPHelper[] DSP = new HSAudioDSPHelper[2];


        bool On, Minus = true;
        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            DSP[0].Bypass = !checkBox1.Checked;
            if (checkBox1.Checked) ThreadPool.QueueUserWorkItem(new WaitCallback(AutoGainThread_Loop));
            else if(AutoGainThread!=null)AutoGainThread.Abort();
        }

        private void checkBox2_CheckedChanged(object sender, EventArgs e)
        {
            //Minus =checkBox2.Checked;
            Channel_Seperate = checkBox2.Checked;
        }
        public static float mag_sqrd(float re, float im) { return (re * re + im * im); }
        public static double Decibels(float re, float im) { return ((re == 0 && im == 0) ? (0) : 10.0 * Math.Log10(((mag_sqrd(re, im))))); }

        double AGC; float AGC1;
        private void numericUpDown1_ValueChanged(object sender, EventArgs e)
        {
            AGC = (double)numericUpDown1.Value;
            AGC1 = (float)numericUpDown1.Value;
            MaxdB = (float)numericUpDown1.Value;
        }

        private void checkBox3_CheckedChanged(object sender, EventArgs e)
        {
            DSP[1].Bypass = !checkBox3.Checked;
        }

        float Hz = 70, Gain = 2, Ratio = 1;

        private void numericUpDown2_ValueChanged(object sender, EventArgs e)
        {
            Hz = (float)numericUpDown2.Value;
            numericUpDown4.Minimum = -numericUpDown2.Value;
            numericUpDown4.Maximum = numericUpDown2.Value;
            Gain = (float)numericUpDown3.Value;
            Ratio = (float)numericUpDown4.Value;
        }
    }
}
