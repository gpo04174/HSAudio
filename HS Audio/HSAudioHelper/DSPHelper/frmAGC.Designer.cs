﻿namespace HS_Audio.Forms
{
    partial class frmAGC
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
            foreach(HS_Audio.DSP.HSAudioDSPHelper dsp in DSP)if(dsp!=null)dsp.DisConnect();
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmAGC));
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.checkBox2 = new System.Windows.Forms.CheckBox();
            this.numericUpDown1 = new System.Windows.Forms.NumericUpDown();
            this.chkHSBooster = new System.Windows.Forms.CheckBox();
            this.numHSBoosterFrequency = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.numHSBoosterRatio = new System.Windows.Forms.NumericUpDown();
            this.label3 = new System.Windows.Forms.Label();
            this.numHSBoosterWetMix = new System.Windows.Forms.NumericUpDown();
            this.numGainControlLevel = new System.Windows.Forms.NumericUpDown();
            this.chkGainControlEnable = new System.Windows.Forms.CheckBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label30 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.cbGainControlChannel = new System.Windows.Forms.ComboBox();
            this.chkGainControlMainOutput = new System.Windows.Forms.CheckBox();
            this.chkGainControlBypass = new System.Windows.Forms.CheckBox();
            this.chkStereoExpand = new System.Windows.Forms.CheckBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.pnl_dBBar_StereoExpand = new System.Windows.Forms.Panel();
            this.dBBar_StereoExpand_R = new HS_Audio.Control.ProgressBarGradientDecibel();
            this.label27 = new System.Windows.Forms.Label();
            this.dBBar_StereoExpand_L = new HS_Audio.Control.ProgressBarGradientDecibel();
            this.chkStereoExpandAdvance = new System.Windows.Forms.CheckBox();
            this.label16 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.numStereoExpandDryMix = new System.Windows.Forms.NumericUpDown();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.numStereoExpandWetMix = new System.Windows.Forms.NumericUpDown();
            this.label12 = new System.Windows.Forms.Label();
            this.cbStereoExpandMethod = new System.Windows.Forms.ComboBox();
            this.label10 = new System.Windows.Forms.Label();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.chkStereoExpandGainAuto = new System.Windows.Forms.CheckBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.numStereoExpandGainValue = new System.Windows.Forms.NumericUpDown();
            this.chkStereoExpandSideSame = new System.Windows.Forms.CheckBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.numStereoExpandMidValue = new System.Windows.Forms.NumericUpDown();
            this.label4 = new System.Windows.Forms.Label();
            this.chkStereoExpandOutput = new System.Windows.Forms.CheckBox();
            this.chkStereoExpandBypass = new System.Windows.Forms.CheckBox();
            this.numStereoExpandSideValue = new System.Windows.Forms.NumericUpDown();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.pnl_dBBar_HSBooster = new System.Windows.Forms.Panel();
            this.dBBar_HSBooster_R = new HS_Audio.Control.ProgressBarGradientDecibel();
            this.label31 = new System.Windows.Forms.Label();
            this.dBBar_HSBooster_L = new HS_Audio.Control.ProgressBarGradientDecibel();
            this.chkHSBoosterMono = new System.Windows.Forms.CheckBox();
            this.chkHSBoosterAdvance = new System.Windows.Forms.CheckBox();
            this.label22 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.numHSBoosterDryMix = new System.Windows.Forms.NumericUpDown();
            this.label20 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.cbHSBoosterPreset = new System.Windows.Forms.ComboBox();
            this.chkHSBoosterMainOutput = new System.Windows.Forms.CheckBox();
            this.chkHSBoosterBypass = new System.Windows.Forms.CheckBox();
            this.btnBassBoosterReset = new System.Windows.Forms.Button();
            this.label19 = new System.Windows.Forms.Label();
            this.chkHSBoosterLow = new System.Windows.Forms.CheckBox();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.파일FToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.새프리셋ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.프리셋열기ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.프리셋저장ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.설정TToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.보기VToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dB바보이기ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator6 = new System.Windows.Forms.ToolStripSeparator();
            this.맨위로설정ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.창닫을때모든효과비활성화ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.개발자모드ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.프리셋경로재설정ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.프리셋열기경로재설정ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.프리셋저장경로재설정ToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.프리셋폴더경로재설정ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.비트변환최우선ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.도움말HToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.커스텀DSP에대해서ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripComboBox1 = new System.Windows.Forms.ToolStripComboBox();
            this.폴더선택ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.새로고침ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.제거ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.chkMonoDownMix = new System.Windows.Forms.CheckBox();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.label11 = new System.Windows.Forms.Label();
            this.cbMonoDownMix = new System.Windows.Forms.ComboBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.chkMonoDownMixMainOutput = new System.Windows.Forms.CheckBox();
            this.chkMonoDownMixBypass = new System.Windows.Forms.CheckBox();
            this.numMonoDownMixMIX = new System.Windows.Forms.NumericUpDown();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.label26 = new System.Windows.Forms.Label();
            this.cbSoundReversChannels = new System.Windows.Forms.ComboBox();
            this.chkSoundReverseMainOutput = new System.Windows.Forms.CheckBox();
            this.chkSoundReverseBypass = new System.Windows.Forms.CheckBox();
            this.chkSoundReverse = new System.Windows.Forms.CheckBox();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.chkConvertBitEx = new System.Windows.Forms.CheckBox();
            this.chkConvertBitDistoration = new System.Windows.Forms.CheckBox();
            this.label24 = new System.Windows.Forms.Label();
            this.cbConvertBitType = new System.Windows.Forms.ComboBox();
            this.chkConvertBitMainOutput = new System.Windows.Forms.CheckBox();
            this.chkConvertBitBypass = new System.Windows.Forms.CheckBox();
            this.label23 = new System.Windows.Forms.Label();
            this.chkConvertBit = new System.Windows.Forms.CheckBox();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.button1 = new System.Windows.Forms.Button();
            this.label25 = new System.Windows.Forms.Label();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numHSBoosterFrequency)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numHSBoosterRatio)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numHSBoosterWetMix)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numGainControlLevel)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.pnl_dBBar_StereoExpand.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numStereoExpandDryMix)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numStereoExpandWetMix)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numStereoExpandGainValue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numStereoExpandMidValue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numStereoExpandSideValue)).BeginInit();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.pnl_dBBar_HSBooster.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numHSBoosterDryMix)).BeginInit();
            this.menuStrip1.SuspendLayout();
            this.groupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numMonoDownMixMIX)).BeginInit();
            this.groupBox6.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this.SuspendLayout();
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(210)))), ((int)(((byte)(0)))));
            this.checkBox1.Location = new System.Drawing.Point(6, 0);
            this.checkBox1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(78, 16);
            this.checkBox1.TabIndex = 0;
            this.checkBox1.Text = "AGC 사용";
            this.checkBox1.UseVisualStyleBackColor = true;
            this.checkBox1.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
            // 
            // checkBox2
            // 
            this.checkBox2.AutoSize = true;
            this.checkBox2.Checked = true;
            this.checkBox2.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBox2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.checkBox2.Location = new System.Drawing.Point(94, 20);
            this.checkBox2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.checkBox2.Name = "checkBox2";
            this.checkBox2.Size = new System.Drawing.Size(48, 16);
            this.checkBox2.TabIndex = 1;
            this.checkBox2.Text = "빼기";
            this.checkBox2.UseVisualStyleBackColor = true;
            this.checkBox2.Visible = false;
            this.checkBox2.CheckedChanged += new System.EventHandler(this.checkBox2_CheckedChanged);
            // 
            // numericUpDown1
            // 
            this.numericUpDown1.DecimalPlaces = 2;
            this.numericUpDown1.Location = new System.Drawing.Point(6, 19);
            this.numericUpDown1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.numericUpDown1.Maximum = new decimal(new int[] {
            50,
            0,
            0,
            0});
            this.numericUpDown1.Minimum = new decimal(new int[] {
            50,
            0,
            0,
            -2147483648});
            this.numericUpDown1.Name = "numericUpDown1";
            this.numericUpDown1.Size = new System.Drawing.Size(80, 21);
            this.numericUpDown1.TabIndex = 2;
            this.numericUpDown1.ValueChanged += new System.EventHandler(this.numericUpDown1_ValueChanged);
            // 
            // chkHSBooster
            // 
            this.chkHSBooster.AutoSize = true;
            this.chkHSBooster.Font = new System.Drawing.Font("Gulim", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.chkHSBooster.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(210)))), ((int)(((byte)(0)))));
            this.chkHSBooster.Location = new System.Drawing.Point(7, 0);
            this.chkHSBooster.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.chkHSBooster.Name = "chkHSBooster";
            this.chkHSBooster.Size = new System.Drawing.Size(99, 16);
            this.chkHSBooster.TabIndex = 3;
            this.chkHSBooster.Text = "HS™ 부스터";
            this.chkHSBooster.UseVisualStyleBackColor = true;
            this.chkHSBooster.CheckedChanged += new System.EventHandler(this.chkBassBooster_CheckedChanged);
            // 
            // numHSBoosterFrequency
            // 
            this.numHSBoosterFrequency.DecimalPlaces = 2;
            this.numHSBoosterFrequency.Increment = new decimal(new int[] {
            5,
            0,
            0,
            65536});
            this.numHSBoosterFrequency.Location = new System.Drawing.Point(40, 68);
            this.numHSBoosterFrequency.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.numHSBoosterFrequency.Maximum = new decimal(new int[] {
            22000,
            0,
            0,
            0});
            this.numHSBoosterFrequency.Name = "numHSBoosterFrequency";
            this.numHSBoosterFrequency.Size = new System.Drawing.Size(66, 21);
            this.numHSBoosterFrequency.TabIndex = 4;
            this.numHSBoosterFrequency.Value = new decimal(new int[] {
            95,
            0,
            0,
            0});
            this.numHSBoosterFrequency.ValueChanged += new System.EventHandler(this.BassBooster_ValueChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(2, 70);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(20, 12);
            this.label1.TabIndex = 5;
            this.label1.Text = "Hz";
            // 
            // numHSBoosterRatio
            // 
            this.numHSBoosterRatio.DecimalPlaces = 4;
            this.numHSBoosterRatio.Increment = new decimal(new int[] {
            2,
            0,
            0,
            196608});
            this.numHSBoosterRatio.Location = new System.Drawing.Point(40, 90);
            this.numHSBoosterRatio.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.numHSBoosterRatio.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.numHSBoosterRatio.Name = "numHSBoosterRatio";
            this.numHSBoosterRatio.Size = new System.Drawing.Size(66, 21);
            this.numHSBoosterRatio.TabIndex = 6;
            this.numHSBoosterRatio.Value = new decimal(new int[] {
            4,
            0,
            0,
            65536});
            this.numHSBoosterRatio.ValueChanged += new System.EventHandler(this.BassBooster_ValueChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(2, 93);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(33, 12);
            this.label3.TabIndex = 9;
            this.label3.Text = "Ratio";
            // 
            // numHSBoosterWetMix
            // 
            this.numHSBoosterWetMix.DecimalPlaces = 3;
            this.numHSBoosterWetMix.Increment = new decimal(new int[] {
            5,
            0,
            0,
            65536});
            this.numHSBoosterWetMix.Location = new System.Drawing.Point(40, 112);
            this.numHSBoosterWetMix.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.numHSBoosterWetMix.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            -2147483648});
            this.numHSBoosterWetMix.Name = "numHSBoosterWetMix";
            this.numHSBoosterWetMix.Size = new System.Drawing.Size(66, 21);
            this.numHSBoosterWetMix.TabIndex = 8;
            this.numHSBoosterWetMix.Value = new decimal(new int[] {
            40,
            0,
            0,
            0});
            this.numHSBoosterWetMix.ValueChanged += new System.EventHandler(this.BassBooster_ValueChanged);
            // 
            // numGainControlLevel
            // 
            this.numGainControlLevel.DecimalPlaces = 4;
            this.numGainControlLevel.Increment = new decimal(new int[] {
            5,
            0,
            0,
            131072});
            this.numGainControlLevel.Location = new System.Drawing.Point(39, 22);
            this.numGainControlLevel.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.numGainControlLevel.Maximum = new decimal(new int[] {
            128,
            0,
            0,
            0});
            this.numGainControlLevel.Minimum = new decimal(new int[] {
            128,
            0,
            0,
            -2147483648});
            this.numGainControlLevel.Name = "numGainControlLevel";
            this.numGainControlLevel.Size = new System.Drawing.Size(80, 21);
            this.numGainControlLevel.TabIndex = 10;
            this.numGainControlLevel.ValueChanged += new System.EventHandler(this.numericUpDown5_ValueChanged);
            // 
            // chkGainControlEnable
            // 
            this.chkGainControlEnable.AutoSize = true;
            this.chkGainControlEnable.Font = new System.Drawing.Font("Gulim", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.chkGainControlEnable.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(210)))), ((int)(((byte)(0)))));
            this.chkGainControlEnable.Location = new System.Drawing.Point(10, 0);
            this.chkGainControlEnable.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.chkGainControlEnable.Name = "chkGainControlEnable";
            this.chkGainControlEnable.Size = new System.Drawing.Size(81, 16);
            this.chkGainControlEnable.TabIndex = 11;
            this.chkGainControlEnable.Text = "게인 조절";
            this.chkGainControlEnable.UseVisualStyleBackColor = true;
            this.chkGainControlEnable.CheckedChanged += new System.EventHandler(this.checkBox4_CheckedChanged);
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.Transparent;
            this.groupBox1.Controls.Add(this.label30);
            this.groupBox1.Controls.Add(this.label29);
            this.groupBox1.Controls.Add(this.label28);
            this.groupBox1.Controls.Add(this.cbGainControlChannel);
            this.groupBox1.Controls.Add(this.chkGainControlMainOutput);
            this.groupBox1.Controls.Add(this.chkGainControlBypass);
            this.groupBox1.Controls.Add(this.chkGainControlEnable);
            this.groupBox1.Controls.Add(this.numGainControlLevel);
            this.groupBox1.Location = new System.Drawing.Point(4, 34);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(162, 100);
            this.groupBox1.TabIndex = 12;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "                    ";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.ForeColor = System.Drawing.Color.White;
            this.label30.Location = new System.Drawing.Point(125, 26);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(20, 12);
            this.label30.TabIndex = 43;
            this.label30.Text = "dB";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.ForeColor = System.Drawing.Color.White;
            this.label29.Location = new System.Drawing.Point(8, 27);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(29, 12);
            this.label29.TabIndex = 42;
            this.label29.Text = "게인";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.ForeColor = System.Drawing.Color.White;
            this.label28.Location = new System.Drawing.Point(7, 53);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(29, 12);
            this.label28.TabIndex = 41;
            this.label28.Text = "채널";
            // 
            // cbGainControlChannel
            // 
            this.cbGainControlChannel.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbGainControlChannel.FormattingEnabled = true;
            this.cbGainControlChannel.Items.AddRange(new object[] {
            "모두",
            "좌",
            "우"});
            this.cbGainControlChannel.Location = new System.Drawing.Point(39, 49);
            this.cbGainControlChannel.Name = "cbGainControlChannel";
            this.cbGainControlChannel.Size = new System.Drawing.Size(91, 20);
            this.cbGainControlChannel.TabIndex = 40;
            this.cbGainControlChannel.SelectedIndexChanged += new System.EventHandler(this.cbGainControlChannel_SelectedIndexChanged);
            // 
            // chkGainControlMainOutput
            // 
            this.chkGainControlMainOutput.AutoSize = true;
            this.chkGainControlMainOutput.Checked = true;
            this.chkGainControlMainOutput.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkGainControlMainOutput.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.chkGainControlMainOutput.Location = new System.Drawing.Point(5, 75);
            this.chkGainControlMainOutput.Name = "chkGainControlMainOutput";
            this.chkGainControlMainOutput.Size = new System.Drawing.Size(92, 16);
            this.chkGainControlMainOutput.TabIndex = 13;
            this.chkGainControlMainOutput.Text = "Main Output";
            this.chkGainControlMainOutput.UseVisualStyleBackColor = true;
            this.chkGainControlMainOutput.CheckedChanged += new System.EventHandler(this.checkBox6_CheckedChanged);
            // 
            // chkGainControlBypass
            // 
            this.chkGainControlBypass.AutoSize = true;
            this.chkGainControlBypass.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.chkGainControlBypass.Location = new System.Drawing.Point(103, 74);
            this.chkGainControlBypass.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.chkGainControlBypass.Name = "chkGainControlBypass";
            this.chkGainControlBypass.Size = new System.Drawing.Size(48, 16);
            this.chkGainControlBypass.TabIndex = 12;
            this.chkGainControlBypass.Text = "우회";
            this.chkGainControlBypass.UseVisualStyleBackColor = true;
            this.chkGainControlBypass.CheckedChanged += new System.EventHandler(this.checkBox5_CheckedChanged);
            // 
            // chkStereoExpand
            // 
            this.chkStereoExpand.AutoSize = true;
            this.chkStereoExpand.Font = new System.Drawing.Font("Gulim", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.chkStereoExpand.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(210)))), ((int)(((byte)(0)))));
            this.chkStereoExpand.Location = new System.Drawing.Point(7, 1);
            this.chkStereoExpand.Margin = new System.Windows.Forms.Padding(2);
            this.chkStereoExpand.Name = "chkStereoExpand";
            this.chkStereoExpand.Size = new System.Drawing.Size(107, 16);
            this.chkStereoExpand.TabIndex = 13;
            this.chkStereoExpand.Text = "스테레오 확장";
            this.chkStereoExpand.UseVisualStyleBackColor = true;
            this.chkStereoExpand.CheckedChanged += new System.EventHandler(this.chkStereoExpand_CheckedChanged);
            // 
            // groupBox2
            // 
            this.groupBox2.BackColor = System.Drawing.Color.Transparent;
            this.groupBox2.Controls.Add(this.pnl_dBBar_StereoExpand);
            this.groupBox2.Controls.Add(this.chkStereoExpandAdvance);
            this.groupBox2.Controls.Add(this.label16);
            this.groupBox2.Controls.Add(this.label18);
            this.groupBox2.Controls.Add(this.numStereoExpandDryMix);
            this.groupBox2.Controls.Add(this.label13);
            this.groupBox2.Controls.Add(this.label14);
            this.groupBox2.Controls.Add(this.numStereoExpandWetMix);
            this.groupBox2.Controls.Add(this.label12);
            this.groupBox2.Controls.Add(this.cbStereoExpandMethod);
            this.groupBox2.Controls.Add(this.label10);
            this.groupBox2.Controls.Add(this.comboBox1);
            this.groupBox2.Controls.Add(this.chkStereoExpandGainAuto);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Controls.Add(this.numStereoExpandGainValue);
            this.groupBox2.Controls.Add(this.chkStereoExpandSideSame);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.numStereoExpandMidValue);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.chkStereoExpandOutput);
            this.groupBox2.Controls.Add(this.chkStereoExpandBypass);
            this.groupBox2.Controls.Add(this.numStereoExpandSideValue);
            this.groupBox2.Controls.Add(this.chkStereoExpand);
            this.groupBox2.Location = new System.Drawing.Point(170, 34);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox2.Size = new System.Drawing.Size(234, 210);
            this.groupBox2.TabIndex = 14;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "                           ";
            // 
            // pnl_dBBar_StereoExpand
            // 
            this.pnl_dBBar_StereoExpand.Controls.Add(this.dBBar_StereoExpand_R);
            this.pnl_dBBar_StereoExpand.Controls.Add(this.label27);
            this.pnl_dBBar_StereoExpand.Controls.Add(this.dBBar_StereoExpand_L);
            this.pnl_dBBar_StereoExpand.Location = new System.Drawing.Point(187, 18);
            this.pnl_dBBar_StereoExpand.Name = "pnl_dBBar_StereoExpand";
            this.pnl_dBBar_StereoExpand.Size = new System.Drawing.Size(43, 187);
            this.pnl_dBBar_StereoExpand.TabIndex = 37;
            // 
            // dBBar_StereoExpand_R
            // 
            this.dBBar_StereoExpand_R.AttackTime = 10;
            this.dBBar_StereoExpand_R.Block = ((uint)(1024u));
            this.dBBar_StereoExpand_R.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.dBBar_StereoExpand_R.Location = new System.Drawing.Point(22, 22);
            this.dBBar_StereoExpand_R.MaxdB = 0F;
            this.dBBar_StereoExpand_R.MaximumColor = System.Drawing.Color.Red;
            this.dBBar_StereoExpand_R.MidColor = System.Drawing.Color.Yellow;
            this.dBBar_StereoExpand_R.MidColorPosition = 0.2999999F;
            this.dBBar_StereoExpand_R.MindB = -60F;
            this.dBBar_StereoExpand_R.MinimumColor = System.Drawing.Color.Lime;
            this.dBBar_StereoExpand_R.MouseInteractive = false;
            this.dBBar_StereoExpand_R.Name = "dBBar_StereoExpand_R";
            this.dBBar_StereoExpand_R.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.dBBar_StereoExpand_R.ReleaseTime = 30;
            this.dBBar_StereoExpand_R.ShowMidColor = true;
            this.dBBar_StereoExpand_R.Size = new System.Drawing.Size(20, 165);
            this.dBBar_StereoExpand_R.Smooth = false;
            this.dBBar_StereoExpand_R.TabIndex = 2;
            this.dBBar_StereoExpand_R.Value = 0F;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.ForeColor = System.Drawing.Color.White;
            this.label27.Location = new System.Drawing.Point(1, 4);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(40, 12);
            this.label27.TabIndex = 1;
            this.label27.Text = " L    R";
            // 
            // dBBar_StereoExpand_L
            // 
            this.dBBar_StereoExpand_L.AttackTime = 10;
            this.dBBar_StereoExpand_L.Block = ((uint)(1024u));
            this.dBBar_StereoExpand_L.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.dBBar_StereoExpand_L.Location = new System.Drawing.Point(1, 22);
            this.dBBar_StereoExpand_L.MaxdB = 0F;
            this.dBBar_StereoExpand_L.MaximumColor = System.Drawing.Color.Red;
            this.dBBar_StereoExpand_L.MidColor = System.Drawing.Color.Yellow;
            this.dBBar_StereoExpand_L.MidColorPosition = 0.2999999F;
            this.dBBar_StereoExpand_L.MindB = -60F;
            this.dBBar_StereoExpand_L.MinimumColor = System.Drawing.Color.Lime;
            this.dBBar_StereoExpand_L.MouseInteractive = false;
            this.dBBar_StereoExpand_L.Name = "dBBar_StereoExpand_L";
            this.dBBar_StereoExpand_L.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.dBBar_StereoExpand_L.ReleaseTime = 30;
            this.dBBar_StereoExpand_L.ShowMidColor = true;
            this.dBBar_StereoExpand_L.Size = new System.Drawing.Size(20, 165);
            this.dBBar_StereoExpand_L.Smooth = false;
            this.dBBar_StereoExpand_L.TabIndex = 0;
            this.dBBar_StereoExpand_L.Value = 0F;
            // 
            // chkStereoExpandAdvance
            // 
            this.chkStereoExpandAdvance.AutoSize = true;
            this.chkStereoExpandAdvance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.chkStereoExpandAdvance.Location = new System.Drawing.Point(129, 108);
            this.chkStereoExpandAdvance.Margin = new System.Windows.Forms.Padding(2);
            this.chkStereoExpandAdvance.Name = "chkStereoExpandAdvance";
            this.chkStereoExpandAdvance.Size = new System.Drawing.Size(48, 16);
            this.chkStereoExpandAdvance.TabIndex = 36;
            this.chkStereoExpandAdvance.Text = "고급";
            this.chkStereoExpandAdvance.UseVisualStyleBackColor = true;
            this.chkStereoExpandAdvance.CheckedChanged += new System.EventHandler(this.chkStereoExpandAdvance_CheckedChanged);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Enabled = false;
            this.label16.ForeColor = System.Drawing.Color.White;
            this.label16.Location = new System.Drawing.Point(5, 120);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(24, 12);
            this.label16.TabIndex = 35;
            this.label16.Text = "Dry";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Enabled = false;
            this.label18.ForeColor = System.Drawing.Color.White;
            this.label18.Location = new System.Drawing.Point(107, 120);
            this.label18.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(15, 12);
            this.label18.TabIndex = 34;
            this.label18.Text = "%";
            // 
            // numStereoExpandDryMix
            // 
            this.numStereoExpandDryMix.DecimalPlaces = 3;
            this.numStereoExpandDryMix.Enabled = false;
            this.numStereoExpandDryMix.Location = new System.Drawing.Point(37, 117);
            this.numStereoExpandDryMix.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.numStereoExpandDryMix.Name = "numStereoExpandDryMix";
            this.numStereoExpandDryMix.Size = new System.Drawing.Size(64, 21);
            this.numStereoExpandDryMix.TabIndex = 33;
            this.numStereoExpandDryMix.ValueChanged += new System.EventHandler(this.numStereoExpandDryMix_ValueChanged);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.ForeColor = System.Drawing.Color.White;
            this.label13.Location = new System.Drawing.Point(5, 97);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(26, 12);
            this.label13.TabIndex = 32;
            this.label13.Text = "Mix";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.ForeColor = System.Drawing.Color.White;
            this.label14.Location = new System.Drawing.Point(107, 98);
            this.label14.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(15, 12);
            this.label14.TabIndex = 31;
            this.label14.Text = "%";
            // 
            // numStereoExpandWetMix
            // 
            this.numStereoExpandWetMix.DecimalPlaces = 3;
            this.numStereoExpandWetMix.Location = new System.Drawing.Point(37, 94);
            this.numStereoExpandWetMix.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.numStereoExpandWetMix.Name = "numStereoExpandWetMix";
            this.numStereoExpandWetMix.Size = new System.Drawing.Size(64, 21);
            this.numStereoExpandWetMix.TabIndex = 30;
            this.numStereoExpandWetMix.Value = new decimal(new int[] {
            10000,
            0,
            0,
            131072});
            this.numStereoExpandWetMix.ValueChanged += new System.EventHandler(this.numStereoExpandWetMix_ValueChanged);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.ForeColor = System.Drawing.Color.White;
            this.label12.Location = new System.Drawing.Point(4, 142);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(29, 12);
            this.label12.TabIndex = 29;
            this.label12.Text = "방법";
            // 
            // cbStereoExpandMethod
            // 
            this.cbStereoExpandMethod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbStereoExpandMethod.FormattingEnabled = true;
            this.cbStereoExpandMethod.Items.AddRange(new object[] {
            "√2 (1.4142135... )",
            "N (채널수)"});
            this.cbStereoExpandMethod.Location = new System.Drawing.Point(37, 140);
            this.cbStereoExpandMethod.Name = "cbStereoExpandMethod";
            this.cbStereoExpandMethod.Size = new System.Drawing.Size(117, 20);
            this.cbStereoExpandMethod.TabIndex = 28;
            this.cbStereoExpandMethod.SelectedIndexChanged += new System.EventHandler(this.cbStereoExpandMethod_SelectedIndexChanged);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.ForeColor = System.Drawing.Color.White;
            this.label10.Location = new System.Drawing.Point(7, 28);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(41, 12);
            this.label10.TabIndex = 27;
            this.label10.Text = "프리셋";
            // 
            // comboBox1
            // 
            this.comboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Items.AddRange(new object[] {
            "스테레오 1",
            "스테레오 2",
            "스테레오 3",
            "스테레오 4",
            "스테레오 5",
            "HS™ 부스터 전용",
            "스테레오 끄기",
            "(사용자 설정)"});
            this.comboBox1.Location = new System.Drawing.Point(51, 24);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(134, 20);
            this.comboBox1.TabIndex = 26;
            this.comboBox1.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // chkStereoExpandGainAuto
            // 
            this.chkStereoExpandGainAuto.AutoSize = true;
            this.chkStereoExpandGainAuto.Checked = true;
            this.chkStereoExpandGainAuto.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkStereoExpandGainAuto.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.chkStereoExpandGainAuto.Location = new System.Drawing.Point(130, 165);
            this.chkStereoExpandGainAuto.Name = "chkStereoExpandGainAuto";
            this.chkStereoExpandGainAuto.Size = new System.Drawing.Size(48, 16);
            this.chkStereoExpandGainAuto.TabIndex = 25;
            this.chkStereoExpandGainAuto.Text = "자동";
            this.chkStereoExpandGainAuto.UseVisualStyleBackColor = true;
            this.chkStereoExpandGainAuto.CheckedChanged += new System.EventHandler(this.checkBox8_CheckedChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.ForeColor = System.Drawing.Color.White;
            this.label8.Location = new System.Drawing.Point(3, 167);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(31, 12);
            this.label8.TabIndex = 24;
            this.label8.Text = "Gain";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.ForeColor = System.Drawing.Color.White;
            this.label9.Location = new System.Drawing.Point(107, 166);
            this.label9.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(20, 12);
            this.label9.TabIndex = 23;
            this.label9.Text = "dB";
            // 
            // numStereoExpandGainValue
            // 
            this.numStereoExpandGainValue.DecimalPlaces = 3;
            this.numStereoExpandGainValue.Enabled = false;
            this.numStereoExpandGainValue.Increment = new decimal(new int[] {
            2,
            0,
            0,
            131072});
            this.numStereoExpandGainValue.Location = new System.Drawing.Point(37, 163);
            this.numStereoExpandGainValue.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.numStereoExpandGainValue.Maximum = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.numStereoExpandGainValue.Minimum = new decimal(new int[] {
            7,
            0,
            0,
            -2147483648});
            this.numStereoExpandGainValue.Name = "numStereoExpandGainValue";
            this.numStereoExpandGainValue.Size = new System.Drawing.Size(64, 21);
            this.numStereoExpandGainValue.TabIndex = 22;
            this.numStereoExpandGainValue.Value = new decimal(new int[] {
            1,
            0,
            0,
            -2147483648});
            this.numStereoExpandGainValue.ValueChanged += new System.EventHandler(this.numStereoExpandGainValue_ValueChanged);
            // 
            // chkStereoExpandSideSame
            // 
            this.chkStereoExpandSideSame.AutoSize = true;
            this.chkStereoExpandSideSame.Checked = true;
            this.chkStereoExpandSideSame.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkStereoExpandSideSame.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.chkStereoExpandSideSame.Location = new System.Drawing.Point(129, 48);
            this.chkStereoExpandSideSame.Name = "chkStereoExpandSideSame";
            this.chkStereoExpandSideSame.Size = new System.Drawing.Size(52, 40);
            this.chkStereoExpandSideSame.TabIndex = 21;
            this.chkStereoExpandSideSame.Text = "Side\r\n값과 \r\n같게";
            this.chkStereoExpandSideSame.UseVisualStyleBackColor = true;
            this.chkStereoExpandSideSame.CheckedChanged += new System.EventHandler(this.checkBox7_CheckedChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.ForeColor = System.Drawing.Color.White;
            this.label7.Location = new System.Drawing.Point(5, 74);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(26, 12);
            this.label7.TabIndex = 20;
            this.label7.Text = "Mid";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.ForeColor = System.Drawing.Color.White;
            this.label6.Location = new System.Drawing.Point(5, 52);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(30, 12);
            this.label6.TabIndex = 19;
            this.label6.Text = "Side";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.ForeColor = System.Drawing.Color.White;
            this.label5.Location = new System.Drawing.Point(107, 74);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(15, 12);
            this.label5.TabIndex = 18;
            this.label5.Text = "%";
            // 
            // numStereoExpandMidValue
            // 
            this.numStereoExpandMidValue.DecimalPlaces = 3;
            this.numStereoExpandMidValue.Enabled = false;
            this.numStereoExpandMidValue.Increment = new decimal(new int[] {
            25,
            0,
            0,
            131072});
            this.numStereoExpandMidValue.Location = new System.Drawing.Point(37, 71);
            this.numStereoExpandMidValue.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.numStereoExpandMidValue.Name = "numStereoExpandMidValue";
            this.numStereoExpandMidValue.Size = new System.Drawing.Size(64, 21);
            this.numStereoExpandMidValue.TabIndex = 17;
            this.numStereoExpandMidValue.Value = new decimal(new int[] {
            3000,
            0,
            0,
            131072});
            this.numStereoExpandMidValue.ValueChanged += new System.EventHandler(this.numStereoExpandMidValue_ValueChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.ForeColor = System.Drawing.Color.White;
            this.label4.Location = new System.Drawing.Point(106, 51);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(15, 12);
            this.label4.TabIndex = 16;
            this.label4.Text = "%";
            // 
            // chkStereoExpandOutput
            // 
            this.chkStereoExpandOutput.AutoSize = true;
            this.chkStereoExpandOutput.Checked = true;
            this.chkStereoExpandOutput.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkStereoExpandOutput.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.chkStereoExpandOutput.Location = new System.Drawing.Point(7, 189);
            this.chkStereoExpandOutput.Name = "chkStereoExpandOutput";
            this.chkStereoExpandOutput.Size = new System.Drawing.Size(92, 16);
            this.chkStereoExpandOutput.TabIndex = 14;
            this.chkStereoExpandOutput.Text = "Main Output";
            this.chkStereoExpandOutput.UseVisualStyleBackColor = true;
            this.chkStereoExpandOutput.CheckedChanged += new System.EventHandler(this.chkStereoExpandOutput_CheckedChanged);
            // 
            // chkStereoExpandBypass
            // 
            this.chkStereoExpandBypass.AutoSize = true;
            this.chkStereoExpandBypass.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.chkStereoExpandBypass.Location = new System.Drawing.Point(105, 189);
            this.chkStereoExpandBypass.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.chkStereoExpandBypass.Name = "chkStereoExpandBypass";
            this.chkStereoExpandBypass.Size = new System.Drawing.Size(48, 16);
            this.chkStereoExpandBypass.TabIndex = 15;
            this.chkStereoExpandBypass.Text = "우회";
            this.chkStereoExpandBypass.UseVisualStyleBackColor = true;
            this.chkStereoExpandBypass.CheckedChanged += new System.EventHandler(this.chkStereoExpandBypass_CheckedChanged);
            // 
            // numStereoExpandSideValue
            // 
            this.numStereoExpandSideValue.DecimalPlaces = 3;
            this.numStereoExpandSideValue.Increment = new decimal(new int[] {
            5,
            0,
            0,
            65536});
            this.numStereoExpandSideValue.Location = new System.Drawing.Point(37, 48);
            this.numStereoExpandSideValue.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.numStereoExpandSideValue.Name = "numStereoExpandSideValue";
            this.numStereoExpandSideValue.Size = new System.Drawing.Size(64, 21);
            this.numStereoExpandSideValue.TabIndex = 14;
            this.numStereoExpandSideValue.Value = new decimal(new int[] {
            3000,
            0,
            0,
            131072});
            this.numStereoExpandSideValue.ValueChanged += new System.EventHandler(this.chkStereoExpandSideValue_ValueChanged);
            // 
            // groupBox3
            // 
            this.groupBox3.BackColor = System.Drawing.Color.Transparent;
            this.groupBox3.Controls.Add(this.checkBox1);
            this.groupBox3.Controls.Add(this.numericUpDown1);
            this.groupBox3.Controls.Add(this.checkBox2);
            this.groupBox3.Enabled = false;
            this.groupBox3.Location = new System.Drawing.Point(4, 315);
            this.groupBox3.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox3.Size = new System.Drawing.Size(156, 53);
            this.groupBox3.TabIndex = 15;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "                   ";
            // 
            // groupBox4
            // 
            this.groupBox4.BackColor = System.Drawing.Color.Transparent;
            this.groupBox4.Controls.Add(this.pnl_dBBar_HSBooster);
            this.groupBox4.Controls.Add(this.chkHSBoosterMono);
            this.groupBox4.Controls.Add(this.chkHSBoosterAdvance);
            this.groupBox4.Controls.Add(this.label22);
            this.groupBox4.Controls.Add(this.label21);
            this.groupBox4.Controls.Add(this.numHSBoosterDryMix);
            this.groupBox4.Controls.Add(this.label20);
            this.groupBox4.Controls.Add(this.label2);
            this.groupBox4.Controls.Add(this.cbHSBoosterPreset);
            this.groupBox4.Controls.Add(this.chkHSBoosterMainOutput);
            this.groupBox4.Controls.Add(this.chkHSBoosterBypass);
            this.groupBox4.Controls.Add(this.btnBassBoosterReset);
            this.groupBox4.Controls.Add(this.label19);
            this.groupBox4.Controls.Add(this.chkHSBoosterLow);
            this.groupBox4.Controls.Add(this.chkHSBooster);
            this.groupBox4.Controls.Add(this.numHSBoosterFrequency);
            this.groupBox4.Controls.Add(this.label1);
            this.groupBox4.Controls.Add(this.numHSBoosterRatio);
            this.groupBox4.Controls.Add(this.label3);
            this.groupBox4.Controls.Add(this.numHSBoosterWetMix);
            this.groupBox4.Location = new System.Drawing.Point(408, 35);
            this.groupBox4.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox4.Size = new System.Drawing.Size(232, 225);
            this.groupBox4.TabIndex = 16;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "                        ";
            // 
            // pnl_dBBar_HSBooster
            // 
            this.pnl_dBBar_HSBooster.Controls.Add(this.dBBar_HSBooster_R);
            this.pnl_dBBar_HSBooster.Controls.Add(this.label31);
            this.pnl_dBBar_HSBooster.Controls.Add(this.dBBar_HSBooster_L);
            this.pnl_dBBar_HSBooster.Location = new System.Drawing.Point(186, 17);
            this.pnl_dBBar_HSBooster.Name = "pnl_dBBar_HSBooster";
            this.pnl_dBBar_HSBooster.Size = new System.Drawing.Size(43, 202);
            this.pnl_dBBar_HSBooster.TabIndex = 38;
            // 
            // dBBar_HSBooster_R
            // 
            this.dBBar_HSBooster_R.AttackTime = 10;
            this.dBBar_HSBooster_R.Block = ((uint)(1024u));
            this.dBBar_HSBooster_R.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.dBBar_HSBooster_R.Location = new System.Drawing.Point(22, 22);
            this.dBBar_HSBooster_R.MaxdB = 0F;
            this.dBBar_HSBooster_R.MaximumColor = System.Drawing.Color.Red;
            this.dBBar_HSBooster_R.MidColor = System.Drawing.Color.Yellow;
            this.dBBar_HSBooster_R.MidColorPosition = 0.3F;
            this.dBBar_HSBooster_R.MindB = -60F;
            this.dBBar_HSBooster_R.MinimumColor = System.Drawing.Color.Lime;
            this.dBBar_HSBooster_R.MouseInteractive = false;
            this.dBBar_HSBooster_R.Name = "dBBar_HSBooster_R";
            this.dBBar_HSBooster_R.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.dBBar_HSBooster_R.ReleaseTime = 30;
            this.dBBar_HSBooster_R.ShowMidColor = true;
            this.dBBar_HSBooster_R.Size = new System.Drawing.Size(20, 180);
            this.dBBar_HSBooster_R.Smooth = false;
            this.dBBar_HSBooster_R.TabIndex = 2;
            this.dBBar_HSBooster_R.Value = 0F;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.ForeColor = System.Drawing.Color.White;
            this.label31.Location = new System.Drawing.Point(1, 6);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(40, 12);
            this.label31.TabIndex = 1;
            this.label31.Text = " L    R";
            // 
            // dBBar_HSBooster_L
            // 
            this.dBBar_HSBooster_L.AttackTime = 10;
            this.dBBar_HSBooster_L.Block = ((uint)(1024u));
            this.dBBar_HSBooster_L.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.dBBar_HSBooster_L.Location = new System.Drawing.Point(1, 22);
            this.dBBar_HSBooster_L.MaxdB = 0F;
            this.dBBar_HSBooster_L.MaximumColor = System.Drawing.Color.Red;
            this.dBBar_HSBooster_L.MidColor = System.Drawing.Color.Yellow;
            this.dBBar_HSBooster_L.MidColorPosition = 0.3F;
            this.dBBar_HSBooster_L.MindB = -60F;
            this.dBBar_HSBooster_L.MinimumColor = System.Drawing.Color.Lime;
            this.dBBar_HSBooster_L.MouseInteractive = false;
            this.dBBar_HSBooster_L.Name = "dBBar_HSBooster_L";
            this.dBBar_HSBooster_L.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.dBBar_HSBooster_L.ReleaseTime = 30;
            this.dBBar_HSBooster_L.ShowMidColor = true;
            this.dBBar_HSBooster_L.Size = new System.Drawing.Size(20, 180);
            this.dBBar_HSBooster_L.Smooth = false;
            this.dBBar_HSBooster_L.TabIndex = 0;
            this.dBBar_HSBooster_L.Value = 0F;
            // 
            // chkHSBoosterMono
            // 
            this.chkHSBoosterMono.AutoSize = true;
            this.chkHSBoosterMono.Checked = true;
            this.chkHSBoosterMono.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkHSBoosterMono.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.chkHSBoosterMono.Location = new System.Drawing.Point(134, 77);
            this.chkHSBoosterMono.Name = "chkHSBoosterMono";
            this.chkHSBoosterMono.Size = new System.Drawing.Size(48, 16);
            this.chkHSBoosterMono.TabIndex = 42;
            this.chkHSBoosterMono.Text = "모노";
            this.chkHSBoosterMono.UseVisualStyleBackColor = true;
            this.chkHSBoosterMono.CheckedChanged += new System.EventHandler(this.chkBassBoosterMono_CheckedChanged);
            // 
            // chkHSBoosterAdvance
            // 
            this.chkHSBoosterAdvance.AutoSize = true;
            this.chkHSBoosterAdvance.Checked = true;
            this.chkHSBoosterAdvance.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkHSBoosterAdvance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.chkHSBoosterAdvance.Location = new System.Drawing.Point(134, 125);
            this.chkHSBoosterAdvance.Margin = new System.Windows.Forms.Padding(2);
            this.chkHSBoosterAdvance.Name = "chkHSBoosterAdvance";
            this.chkHSBoosterAdvance.Size = new System.Drawing.Size(48, 16);
            this.chkHSBoosterAdvance.TabIndex = 41;
            this.chkHSBoosterAdvance.Text = "고급";
            this.chkHSBoosterAdvance.UseVisualStyleBackColor = true;
            this.chkHSBoosterAdvance.CheckedChanged += new System.EventHandler(this.chkBassBoosterAdvance_CheckedChanged);
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.ForeColor = System.Drawing.Color.White;
            this.label22.Location = new System.Drawing.Point(111, 138);
            this.label22.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(15, 12);
            this.label22.TabIndex = 40;
            this.label22.Text = "%";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.ForeColor = System.Drawing.Color.White;
            this.label21.Location = new System.Drawing.Point(3, 137);
            this.label21.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(24, 12);
            this.label21.TabIndex = 39;
            this.label21.Text = "Dry";
            // 
            // numHSBoosterDryMix
            // 
            this.numHSBoosterDryMix.DecimalPlaces = 3;
            this.numHSBoosterDryMix.Increment = new decimal(new int[] {
            5,
            0,
            0,
            65536});
            this.numHSBoosterDryMix.Location = new System.Drawing.Point(40, 134);
            this.numHSBoosterDryMix.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.numHSBoosterDryMix.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            -2147483648});
            this.numHSBoosterDryMix.Name = "numHSBoosterDryMix";
            this.numHSBoosterDryMix.Size = new System.Drawing.Size(66, 21);
            this.numHSBoosterDryMix.TabIndex = 38;
            this.numHSBoosterDryMix.Value = new decimal(new int[] {
            35,
            0,
            0,
            0});
            this.numHSBoosterDryMix.ValueChanged += new System.EventHandler(this.BassBooster_ValueChanged);
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.ForeColor = System.Drawing.Color.White;
            this.label20.Location = new System.Drawing.Point(5, 25);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(41, 12);
            this.label20.TabIndex = 37;
            this.label20.Text = "프리셋";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(111, 116);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(15, 12);
            this.label2.TabIndex = 35;
            this.label2.Text = "%";
            // 
            // cbHSBoosterPreset
            // 
            this.cbHSBoosterPreset.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbHSBoosterPreset.FormattingEnabled = true;
            this.cbHSBoosterPreset.Items.AddRange(new object[] {
            "베이스 1",
            "베이스 2 (전용)",
            "베이스 3",
            "베이스 (75Hz)",
            "ClearAudio×Stereo",
            "ClearAudio+ 1",
            "값 테스트",
            "모두 끄기",
            "(사용자 설정)"});
            this.cbHSBoosterPreset.Location = new System.Drawing.Point(49, 21);
            this.cbHSBoosterPreset.Name = "cbHSBoosterPreset";
            this.cbHSBoosterPreset.Size = new System.Drawing.Size(135, 20);
            this.cbHSBoosterPreset.TabIndex = 36;
            this.cbHSBoosterPreset.SelectedIndexChanged += new System.EventHandler(this.cbBassBooster_SelectedIndexChanged);
            // 
            // chkHSBoosterMainOutput
            // 
            this.chkHSBoosterMainOutput.AutoSize = true;
            this.chkHSBoosterMainOutput.Checked = true;
            this.chkHSBoosterMainOutput.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkHSBoosterMainOutput.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.chkHSBoosterMainOutput.Location = new System.Drawing.Point(4, 181);
            this.chkHSBoosterMainOutput.Name = "chkHSBoosterMainOutput";
            this.chkHSBoosterMainOutput.Size = new System.Drawing.Size(92, 16);
            this.chkHSBoosterMainOutput.TabIndex = 16;
            this.chkHSBoosterMainOutput.Text = "Main Output";
            this.chkHSBoosterMainOutput.UseVisualStyleBackColor = true;
            this.chkHSBoosterMainOutput.CheckedChanged += new System.EventHandler(this.chkBassBoosterMainOutput_CheckedChanged);
            // 
            // chkHSBoosterBypass
            // 
            this.chkHSBoosterBypass.AutoSize = true;
            this.chkHSBoosterBypass.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.chkHSBoosterBypass.Location = new System.Drawing.Point(4, 203);
            this.chkHSBoosterBypass.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.chkHSBoosterBypass.Name = "chkHSBoosterBypass";
            this.chkHSBoosterBypass.Size = new System.Drawing.Size(48, 16);
            this.chkHSBoosterBypass.TabIndex = 17;
            this.chkHSBoosterBypass.Text = "우회";
            this.chkHSBoosterBypass.UseVisualStyleBackColor = true;
            this.chkHSBoosterBypass.CheckedChanged += new System.EventHandler(this.chkBassBoosterBypass_CheckedChanged);
            // 
            // btnBassBoosterReset
            // 
            this.btnBassBoosterReset.Location = new System.Drawing.Point(4, 156);
            this.btnBassBoosterReset.Margin = new System.Windows.Forms.Padding(2);
            this.btnBassBoosterReset.Name = "btnBassBoosterReset";
            this.btnBassBoosterReset.Size = new System.Drawing.Size(149, 21);
            this.btnBassBoosterReset.TabIndex = 12;
            this.btnBassBoosterReset.Text = "버퍼 리셋";
            this.btnBassBoosterReset.UseVisualStyleBackColor = true;
            this.btnBassBoosterReset.Click += new System.EventHandler(this.btnBassBoosterReset_Click);
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.ForeColor = System.Drawing.Color.White;
            this.label19.Location = new System.Drawing.Point(3, 116);
            this.label19.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(25, 12);
            this.label19.TabIndex = 11;
            this.label19.Text = "Wet";
            // 
            // chkHSBoosterLow
            // 
            this.chkHSBoosterLow.AutoSize = true;
            this.chkHSBoosterLow.Checked = true;
            this.chkHSBoosterLow.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkHSBoosterLow.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.chkHSBoosterLow.Location = new System.Drawing.Point(4, 47);
            this.chkHSBoosterLow.Margin = new System.Windows.Forms.Padding(2);
            this.chkHSBoosterLow.Name = "chkHSBoosterLow";
            this.chkHSBoosterLow.Size = new System.Drawing.Size(76, 16);
            this.chkHSBoosterLow.TabIndex = 10;
            this.chkHSBoosterLow.Text = "저음 보강";
            this.chkHSBoosterLow.UseVisualStyleBackColor = true;
            this.chkHSBoosterLow.CheckedChanged += new System.EventHandler(this.chkBassBoosterLow_CheckedChanged);
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.Color.Transparent;
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.파일FToolStripMenuItem,
            this.설정TToolStripMenuItem,
            this.도움말HToolStripMenuItem,
            this.toolStripComboBox1,
            this.폴더선택ToolStripMenuItem,
            this.새로고침ToolStripMenuItem,
            this.제거ToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Padding = new System.Windows.Forms.Padding(4, 1, 0, 1);
            this.menuStrip1.Size = new System.Drawing.Size(643, 25);
            this.menuStrip1.TabIndex = 17;
            this.menuStrip1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.menuStrip1_MouseDown);
            this.menuStrip1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.menuStrip1_MouseMove);
            this.menuStrip1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.menuStrip1_MouseUp);
            // 
            // 파일FToolStripMenuItem
            // 
            this.파일FToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.새프리셋ToolStripMenuItem,
            this.toolStripSeparator5,
            this.프리셋열기ToolStripMenuItem,
            this.프리셋저장ToolStripMenuItem});
            this.파일FToolStripMenuItem.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))));
            this.파일FToolStripMenuItem.Name = "파일FToolStripMenuItem";
            this.파일FToolStripMenuItem.Size = new System.Drawing.Size(57, 23);
            this.파일FToolStripMenuItem.Text = "파일(&F)";
            // 
            // 새프리셋ToolStripMenuItem
            // 
            this.새프리셋ToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("새프리셋ToolStripMenuItem.Image")));
            this.새프리셋ToolStripMenuItem.Name = "새프리셋ToolStripMenuItem";
            this.새프리셋ToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.N)));
            this.새프리셋ToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.새프리셋ToolStripMenuItem.Text = "새 프리셋";
            this.새프리셋ToolStripMenuItem.Click += new System.EventHandler(this.새프리셋ToolStripMenuItem_Click);
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(177, 6);
            // 
            // 프리셋열기ToolStripMenuItem
            // 
            this.프리셋열기ToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("프리셋열기ToolStripMenuItem.Image")));
            this.프리셋열기ToolStripMenuItem.Name = "프리셋열기ToolStripMenuItem";
            this.프리셋열기ToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.O)));
            this.프리셋열기ToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.프리셋열기ToolStripMenuItem.Text = "프리셋 열기";
            this.프리셋열기ToolStripMenuItem.Click += new System.EventHandler(this.프리셋열기ToolStripMenuItem_Click);
            // 
            // 프리셋저장ToolStripMenuItem
            // 
            this.프리셋저장ToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("프리셋저장ToolStripMenuItem.Image")));
            this.프리셋저장ToolStripMenuItem.Name = "프리셋저장ToolStripMenuItem";
            this.프리셋저장ToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
            this.프리셋저장ToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.프리셋저장ToolStripMenuItem.Text = "프리셋 저장";
            this.프리셋저장ToolStripMenuItem.Click += new System.EventHandler(this.프리셋저장ToolStripMenuItem_Click);
            // 
            // 설정TToolStripMenuItem
            // 
            this.설정TToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.보기VToolStripMenuItem,
            this.toolStripSeparator6,
            this.맨위로설정ToolStripMenuItem,
            this.toolStripSeparator2,
            this.창닫을때모든효과비활성화ToolStripMenuItem,
            this.개발자모드ToolStripMenuItem,
            this.toolStripSeparator4,
            this.프리셋경로재설정ToolStripMenuItem,
            this.toolStripSeparator1,
            this.비트변환최우선ToolStripMenuItem});
            this.설정TToolStripMenuItem.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))));
            this.설정TToolStripMenuItem.Name = "설정TToolStripMenuItem";
            this.설정TToolStripMenuItem.Size = new System.Drawing.Size(58, 23);
            this.설정TToolStripMenuItem.Text = "설정(&T)";
            // 
            // 보기VToolStripMenuItem
            // 
            this.보기VToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.dB바보이기ToolStripMenuItem});
            this.보기VToolStripMenuItem.ForeColor = System.Drawing.Color.Black;
            this.보기VToolStripMenuItem.Name = "보기VToolStripMenuItem";
            this.보기VToolStripMenuItem.Size = new System.Drawing.Size(230, 22);
            this.보기VToolStripMenuItem.Text = "보기(&V)";
            // 
            // dB바보이기ToolStripMenuItem
            // 
            this.dB바보이기ToolStripMenuItem.Checked = true;
            this.dB바보이기ToolStripMenuItem.CheckOnClick = true;
            this.dB바보이기ToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.dB바보이기ToolStripMenuItem.Enabled = false;
            this.dB바보이기ToolStripMenuItem.Name = "dB바보이기ToolStripMenuItem";
            this.dB바보이기ToolStripMenuItem.Size = new System.Drawing.Size(139, 22);
            this.dB바보이기ToolStripMenuItem.Text = "dB바 보이기";
            this.dB바보이기ToolStripMenuItem.CheckedChanged += new System.EventHandler(this.dB바보이기ToolStripMenuItem_CheckedChanged);
            // 
            // toolStripSeparator6
            // 
            this.toolStripSeparator6.Name = "toolStripSeparator6";
            this.toolStripSeparator6.Size = new System.Drawing.Size(227, 6);
            // 
            // 맨위로설정ToolStripMenuItem
            // 
            this.맨위로설정ToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("맨위로설정ToolStripMenuItem.Image")));
            this.맨위로설정ToolStripMenuItem.Name = "맨위로설정ToolStripMenuItem";
            this.맨위로설정ToolStripMenuItem.Size = new System.Drawing.Size(230, 22);
            this.맨위로설정ToolStripMenuItem.Text = "맨 위로 설정";
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(227, 6);
            // 
            // 창닫을때모든효과비활성화ToolStripMenuItem
            // 
            this.창닫을때모든효과비활성화ToolStripMenuItem.CheckOnClick = true;
            this.창닫을때모든효과비활성화ToolStripMenuItem.Enabled = false;
            this.창닫을때모든효과비활성화ToolStripMenuItem.Name = "창닫을때모든효과비활성화ToolStripMenuItem";
            this.창닫을때모든효과비활성화ToolStripMenuItem.Size = new System.Drawing.Size(230, 22);
            this.창닫을때모든효과비활성화ToolStripMenuItem.Text = "창 닫을때 모든 효과 비활성화";
            // 
            // 개발자모드ToolStripMenuItem
            // 
            this.개발자모드ToolStripMenuItem.Name = "개발자모드ToolStripMenuItem";
            this.개발자모드ToolStripMenuItem.Size = new System.Drawing.Size(230, 22);
            this.개발자모드ToolStripMenuItem.Text = "개발자 모드(&D)";
            this.개발자모드ToolStripMenuItem.Click += new System.EventHandler(this.개발자모드ToolStripMenuItem_Click);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(227, 6);
            // 
            // 프리셋경로재설정ToolStripMenuItem
            // 
            this.프리셋경로재설정ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.프리셋열기경로재설정ToolStripMenuItem,
            this.프리셋저장경로재설정ToolStripMenuItem1,
            this.toolStripSeparator3,
            this.프리셋폴더경로재설정ToolStripMenuItem});
            this.프리셋경로재설정ToolStripMenuItem.Name = "프리셋경로재설정ToolStripMenuItem";
            this.프리셋경로재설정ToolStripMenuItem.Size = new System.Drawing.Size(230, 22);
            this.프리셋경로재설정ToolStripMenuItem.Text = "프리셋 경로 재설정";
            this.프리셋경로재설정ToolStripMenuItem.Click += new System.EventHandler(this.프리셋경로재설정ToolStripMenuItem_Click);
            // 
            // 프리셋열기경로재설정ToolStripMenuItem
            // 
            this.프리셋열기경로재설정ToolStripMenuItem.Name = "프리셋열기경로재설정ToolStripMenuItem";
            this.프리셋열기경로재설정ToolStripMenuItem.Size = new System.Drawing.Size(203, 22);
            this.프리셋열기경로재설정ToolStripMenuItem.Text = "프리셋 열기 경로 재설정";
            this.프리셋열기경로재설정ToolStripMenuItem.Click += new System.EventHandler(this.프리셋열기경로재설정ToolStripMenuItem_Click);
            // 
            // 프리셋저장경로재설정ToolStripMenuItem1
            // 
            this.프리셋저장경로재설정ToolStripMenuItem1.Name = "프리셋저장경로재설정ToolStripMenuItem1";
            this.프리셋저장경로재설정ToolStripMenuItem1.Size = new System.Drawing.Size(203, 22);
            this.프리셋저장경로재설정ToolStripMenuItem1.Text = "프리셋 저장 경로 재설정";
            this.프리셋저장경로재설정ToolStripMenuItem1.Click += new System.EventHandler(this.프리셋저장경로재설정ToolStripMenuItem1_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(200, 6);
            // 
            // 프리셋폴더경로재설정ToolStripMenuItem
            // 
            this.프리셋폴더경로재설정ToolStripMenuItem.Name = "프리셋폴더경로재설정ToolStripMenuItem";
            this.프리셋폴더경로재설정ToolStripMenuItem.Size = new System.Drawing.Size(203, 22);
            this.프리셋폴더경로재설정ToolStripMenuItem.Text = "프리셋 폴더 경로 재설정";
            this.프리셋폴더경로재설정ToolStripMenuItem.Click += new System.EventHandler(this.프리셋폴더경로재설정ToolStripMenuItem_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(227, 6);
            // 
            // 비트변환최우선ToolStripMenuItem
            // 
            this.비트변환최우선ToolStripMenuItem.Checked = true;
            this.비트변환최우선ToolStripMenuItem.CheckOnClick = true;
            this.비트변환최우선ToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.비트변환최우선ToolStripMenuItem.Name = "비트변환최우선ToolStripMenuItem";
            this.비트변환최우선ToolStripMenuItem.Size = new System.Drawing.Size(230, 22);
            this.비트변환최우선ToolStripMenuItem.Text = "비트 변환 최우선";
            // 
            // 도움말HToolStripMenuItem
            // 
            this.도움말HToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.커스텀DSP에대해서ToolStripMenuItem});
            this.도움말HToolStripMenuItem.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))));
            this.도움말HToolStripMenuItem.Name = "도움말HToolStripMenuItem";
            this.도움말HToolStripMenuItem.Size = new System.Drawing.Size(72, 23);
            this.도움말HToolStripMenuItem.Text = "도움말(&H)";
            // 
            // 커스텀DSP에대해서ToolStripMenuItem
            // 
            this.커스텀DSP에대해서ToolStripMenuItem.Name = "커스텀DSP에대해서ToolStripMenuItem";
            this.커스텀DSP에대해서ToolStripMenuItem.Size = new System.Drawing.Size(173, 22);
            this.커스텀DSP에대해서ToolStripMenuItem.Text = "HS™ DSP에 대해서";
            this.커스텀DSP에대해서ToolStripMenuItem.Click += new System.EventHandler(this.커스텀DSP에대해서ToolStripMenuItem_Click);
            // 
            // toolStripComboBox1
            // 
            this.toolStripComboBox1.DropDownWidth = 300;
            this.toolStripComboBox1.Items.AddRange(new object[] {
            "(기본값)"});
            this.toolStripComboBox1.Name = "toolStripComboBox1";
            this.toolStripComboBox1.Size = new System.Drawing.Size(100, 23);
            this.toolStripComboBox1.Text = "(기본값)";
            this.toolStripComboBox1.SelectedIndexChanged += new System.EventHandler(this.toolStripComboBox1_SelectedIndexChanged);
            // 
            // 폴더선택ToolStripMenuItem
            // 
            this.폴더선택ToolStripMenuItem.AutoToolTip = true;
            this.폴더선택ToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("폴더선택ToolStripMenuItem.Image")));
            this.폴더선택ToolStripMenuItem.Name = "폴더선택ToolStripMenuItem";
            this.폴더선택ToolStripMenuItem.Size = new System.Drawing.Size(28, 23);
            this.폴더선택ToolStripMenuItem.ToolTipText = "현재 선택된 프리셋을 목록에서 제거합니다.";
            this.폴더선택ToolStripMenuItem.Click += new System.EventHandler(this.폴더선택ToolStripMenuItem_Click);
            // 
            // 새로고침ToolStripMenuItem
            // 
            this.새로고침ToolStripMenuItem.AutoToolTip = true;
            this.새로고침ToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("새로고침ToolStripMenuItem.Image")));
            this.새로고침ToolStripMenuItem.Name = "새로고침ToolStripMenuItem";
            this.새로고침ToolStripMenuItem.Size = new System.Drawing.Size(28, 23);
            this.새로고침ToolStripMenuItem.ToolTipText = "프리셋의 목록을 새로고침 합니다. ";
            this.새로고침ToolStripMenuItem.Click += new System.EventHandler(this.새로고침ToolStripMenuItem_Click);
            // 
            // 제거ToolStripMenuItem
            // 
            this.제거ToolStripMenuItem.AutoToolTip = true;
            this.제거ToolStripMenuItem.Enabled = false;
            this.제거ToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("제거ToolStripMenuItem.Image")));
            this.제거ToolStripMenuItem.Name = "제거ToolStripMenuItem";
            this.제거ToolStripMenuItem.Size = new System.Drawing.Size(28, 23);
            this.제거ToolStripMenuItem.ToolTipText = "현재 선택된 프리셋을 목록에서 제거합니다.\r\n(기본값은 제거되지 않습니다.)";
            this.제거ToolStripMenuItem.Click += new System.EventHandler(this.제거ToolStripMenuItem_Click);
            // 
            // chkMonoDownMix
            // 
            this.chkMonoDownMix.AutoSize = true;
            this.chkMonoDownMix.Font = new System.Drawing.Font("Gulim", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.chkMonoDownMix.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(210)))), ((int)(((byte)(0)))));
            this.chkMonoDownMix.Location = new System.Drawing.Point(7, 1);
            this.chkMonoDownMix.Margin = new System.Windows.Forms.Padding(2);
            this.chkMonoDownMix.Name = "chkMonoDownMix";
            this.chkMonoDownMix.Size = new System.Drawing.Size(107, 16);
            this.chkMonoDownMix.TabIndex = 13;
            this.chkMonoDownMix.Text = "모노 다운믹스";
            this.chkMonoDownMix.UseVisualStyleBackColor = true;
            this.chkMonoDownMix.CheckedChanged += new System.EventHandler(this.chkMonoDownMix_CheckedChanged);
            // 
            // groupBox5
            // 
            this.groupBox5.BackColor = System.Drawing.Color.Transparent;
            this.groupBox5.Controls.Add(this.label11);
            this.groupBox5.Controls.Add(this.cbMonoDownMix);
            this.groupBox5.Controls.Add(this.label15);
            this.groupBox5.Controls.Add(this.label17);
            this.groupBox5.Controls.Add(this.chkMonoDownMixMainOutput);
            this.groupBox5.Controls.Add(this.chkMonoDownMixBypass);
            this.groupBox5.Controls.Add(this.numMonoDownMixMIX);
            this.groupBox5.Controls.Add(this.chkMonoDownMix);
            this.groupBox5.Location = new System.Drawing.Point(5, 139);
            this.groupBox5.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox5.Size = new System.Drawing.Size(162, 99);
            this.groupBox5.TabIndex = 18;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "                          ";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.ForeColor = System.Drawing.Color.White;
            this.label11.Location = new System.Drawing.Point(7, 28);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(41, 12);
            this.label11.TabIndex = 27;
            this.label11.Text = "프리셋";
            // 
            // cbMonoDownMix
            // 
            this.cbMonoDownMix.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbMonoDownMix.FormattingEnabled = true;
            this.cbMonoDownMix.Items.AddRange(new object[] {
            "모노 100%",
            "모노 90%",
            "모노 80%",
            "모노 70%",
            "모노 60%",
            "모노 50%",
            "모노 40%",
            "모노 30%",
            "모노 20%",
            "모노 10%",
            "모노 끄기"});
            this.cbMonoDownMix.Location = new System.Drawing.Point(51, 24);
            this.cbMonoDownMix.Name = "cbMonoDownMix";
            this.cbMonoDownMix.Size = new System.Drawing.Size(102, 20);
            this.cbMonoDownMix.TabIndex = 26;
            this.cbMonoDownMix.SelectedIndexChanged += new System.EventHandler(this.cbMonoDownMix_SelectedIndexChanged);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.ForeColor = System.Drawing.Color.White;
            this.label15.Location = new System.Drawing.Point(7, 53);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(29, 12);
            this.label15.TabIndex = 19;
            this.label15.Text = "레벨";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.ForeColor = System.Drawing.Color.White;
            this.label17.Location = new System.Drawing.Point(131, 52);
            this.label17.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(15, 12);
            this.label17.TabIndex = 16;
            this.label17.Text = "%";
            // 
            // chkMonoDownMixMainOutput
            // 
            this.chkMonoDownMixMainOutput.AutoSize = true;
            this.chkMonoDownMixMainOutput.Checked = true;
            this.chkMonoDownMixMainOutput.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkMonoDownMixMainOutput.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.chkMonoDownMixMainOutput.Location = new System.Drawing.Point(4, 76);
            this.chkMonoDownMixMainOutput.Name = "chkMonoDownMixMainOutput";
            this.chkMonoDownMixMainOutput.Size = new System.Drawing.Size(92, 16);
            this.chkMonoDownMixMainOutput.TabIndex = 14;
            this.chkMonoDownMixMainOutput.Text = "Main Output";
            this.chkMonoDownMixMainOutput.UseVisualStyleBackColor = true;
            this.chkMonoDownMixMainOutput.CheckedChanged += new System.EventHandler(this.chkMonoDownMixMainOutput_CheckedChanged);
            // 
            // chkMonoDownMixBypass
            // 
            this.chkMonoDownMixBypass.AutoSize = true;
            this.chkMonoDownMixBypass.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.chkMonoDownMixBypass.Location = new System.Drawing.Point(103, 76);
            this.chkMonoDownMixBypass.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.chkMonoDownMixBypass.Name = "chkMonoDownMixBypass";
            this.chkMonoDownMixBypass.Size = new System.Drawing.Size(48, 16);
            this.chkMonoDownMixBypass.TabIndex = 15;
            this.chkMonoDownMixBypass.Text = "우회";
            this.chkMonoDownMixBypass.UseVisualStyleBackColor = true;
            this.chkMonoDownMixBypass.CheckedChanged += new System.EventHandler(this.chkMonoDownMixBypass_CheckedChanged);
            // 
            // numMonoDownMixMIX
            // 
            this.numMonoDownMixMIX.DecimalPlaces = 3;
            this.numMonoDownMixMIX.Increment = new decimal(new int[] {
            5,
            0,
            0,
            65536});
            this.numMonoDownMixMIX.Location = new System.Drawing.Point(51, 48);
            this.numMonoDownMixMIX.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.numMonoDownMixMIX.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            -2147483648});
            this.numMonoDownMixMIX.Name = "numMonoDownMixMIX";
            this.numMonoDownMixMIX.Size = new System.Drawing.Size(77, 21);
            this.numMonoDownMixMIX.TabIndex = 14;
            this.numMonoDownMixMIX.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.numMonoDownMixMIX.ValueChanged += new System.EventHandler(this.numMonoDownMixMIX_ValueChanged);
            // 
            // groupBox6
            // 
            this.groupBox6.BackColor = System.Drawing.Color.Transparent;
            this.groupBox6.Controls.Add(this.label26);
            this.groupBox6.Controls.Add(this.cbSoundReversChannels);
            this.groupBox6.Controls.Add(this.chkSoundReverseMainOutput);
            this.groupBox6.Controls.Add(this.chkSoundReverseBypass);
            this.groupBox6.Controls.Add(this.chkSoundReverse);
            this.groupBox6.Location = new System.Drawing.Point(5, 243);
            this.groupBox6.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox6.Size = new System.Drawing.Size(162, 67);
            this.groupBox6.TabIndex = 16;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "                       ";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.ForeColor = System.Drawing.Color.White;
            this.label26.Location = new System.Drawing.Point(7, 24);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(29, 12);
            this.label26.TabIndex = 39;
            this.label26.Text = "채널";
            // 
            // cbSoundReversChannels
            // 
            this.cbSoundReversChannels.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbSoundReversChannels.FormattingEnabled = true;
            this.cbSoundReversChannels.Items.AddRange(new object[] {
            "모두",
            "좌",
            "우"});
            this.cbSoundReversChannels.Location = new System.Drawing.Point(38, 20);
            this.cbSoundReversChannels.Name = "cbSoundReversChannels";
            this.cbSoundReversChannels.Size = new System.Drawing.Size(91, 20);
            this.cbSoundReversChannels.TabIndex = 38;
            this.cbSoundReversChannels.SelectedIndexChanged += new System.EventHandler(this.cbSoundReversChannels_SelectedIndexChanged);
            // 
            // chkSoundReverseMainOutput
            // 
            this.chkSoundReverseMainOutput.AutoSize = true;
            this.chkSoundReverseMainOutput.Checked = true;
            this.chkSoundReverseMainOutput.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkSoundReverseMainOutput.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.chkSoundReverseMainOutput.Location = new System.Drawing.Point(6, 45);
            this.chkSoundReverseMainOutput.Name = "chkSoundReverseMainOutput";
            this.chkSoundReverseMainOutput.Size = new System.Drawing.Size(92, 16);
            this.chkSoundReverseMainOutput.TabIndex = 37;
            this.chkSoundReverseMainOutput.Text = "Main Output";
            this.chkSoundReverseMainOutput.UseVisualStyleBackColor = true;
            this.chkSoundReverseMainOutput.CheckedChanged += new System.EventHandler(this.chkSoundReverseMainOutput_CheckedChanged);
            // 
            // chkSoundReverseBypass
            // 
            this.chkSoundReverseBypass.AutoSize = true;
            this.chkSoundReverseBypass.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.chkSoundReverseBypass.Location = new System.Drawing.Point(106, 45);
            this.chkSoundReverseBypass.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.chkSoundReverseBypass.Name = "chkSoundReverseBypass";
            this.chkSoundReverseBypass.Size = new System.Drawing.Size(48, 16);
            this.chkSoundReverseBypass.TabIndex = 36;
            this.chkSoundReverseBypass.Text = "우회";
            this.chkSoundReverseBypass.UseVisualStyleBackColor = true;
            this.chkSoundReverseBypass.CheckedChanged += new System.EventHandler(this.chkSoundReverseBypass_CheckedChanged);
            // 
            // chkSoundReverse
            // 
            this.chkSoundReverse.AutoSize = true;
            this.chkSoundReverse.Font = new System.Drawing.Font("Gulim", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.chkSoundReverse.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(210)))), ((int)(((byte)(0)))));
            this.chkSoundReverse.Location = new System.Drawing.Point(6, 0);
            this.chkSoundReverse.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.chkSoundReverse.Name = "chkSoundReverse";
            this.chkSoundReverse.Size = new System.Drawing.Size(94, 16);
            this.chkSoundReverse.TabIndex = 0;
            this.chkSoundReverse.Text = "사운드 반전";
            this.chkSoundReverse.UseVisualStyleBackColor = true;
            this.chkSoundReverse.CheckedChanged += new System.EventHandler(this.chkSoundReverse_CheckedChanged);
            // 
            // groupBox7
            // 
            this.groupBox7.BackColor = System.Drawing.Color.Transparent;
            this.groupBox7.Controls.Add(this.chkConvertBitEx);
            this.groupBox7.Controls.Add(this.chkConvertBitDistoration);
            this.groupBox7.Controls.Add(this.label24);
            this.groupBox7.Controls.Add(this.cbConvertBitType);
            this.groupBox7.Controls.Add(this.chkConvertBitMainOutput);
            this.groupBox7.Controls.Add(this.chkConvertBitBypass);
            this.groupBox7.Controls.Add(this.label23);
            this.groupBox7.Controls.Add(this.chkConvertBit);
            this.groupBox7.Location = new System.Drawing.Point(170, 248);
            this.groupBox7.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox7.Size = new System.Drawing.Size(189, 120);
            this.groupBox7.TabIndex = 16;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "                    ";
            // 
            // chkConvertBitEx
            // 
            this.chkConvertBitEx.AutoSize = true;
            this.chkConvertBitEx.ForeColor = System.Drawing.Color.White;
            this.chkConvertBitEx.Location = new System.Drawing.Point(126, 45);
            this.chkConvertBitEx.Name = "chkConvertBitEx";
            this.chkConvertBitEx.Size = new System.Drawing.Size(39, 16);
            this.chkConvertBitEx.TabIndex = 43;
            this.chkConvertBitEx.Text = "Ex";
            this.toolTip1.SetToolTip(this.chkConvertBitEx, "Ex 모드로 변환합니다.");
            this.chkConvertBitEx.UseVisualStyleBackColor = true;
            this.chkConvertBitEx.CheckedChanged += new System.EventHandler(this.chkToIntEx_CheckedChanged);
            // 
            // chkConvertBitDistoration
            // 
            this.chkConvertBitDistoration.AutoSize = true;
            this.chkConvertBitDistoration.ForeColor = System.Drawing.Color.White;
            this.chkConvertBitDistoration.Location = new System.Drawing.Point(32, 45);
            this.chkConvertBitDistoration.Name = "chkConvertBitDistoration";
            this.chkConvertBitDistoration.Size = new System.Drawing.Size(88, 16);
            this.chkConvertBitDistoration.TabIndex = 42;
            this.chkConvertBitDistoration.Text = "왜곡 시키기";
            this.toolTip1.SetToolTip(this.chkConvertBitDistoration, "음악이 한계(Peak)를 초과하면 지직거리는 소리를 냅니다");
            this.chkConvertBitDistoration.UseVisualStyleBackColor = true;
            this.chkConvertBitDistoration.CheckedChanged += new System.EventHandler(this.chkToIntDistoration_CheckedChanged);
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.ForeColor = System.Drawing.Color.Fuchsia;
            this.label24.Location = new System.Drawing.Point(5, 67);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(173, 24);
            this.label24.TabIndex = 41;
            this.label24.Text = "이 프로그램은 내부적으로 \r\n32비트 소숫점으로 처리합니다.";
            // 
            // cbConvertBitType
            // 
            this.cbConvertBitType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbConvertBitType.FormattingEnabled = true;
            this.cbConvertBitType.Items.AddRange(new object[] {
            "32비트 (정수 처리)",
            "24비트 [스튜디오]",
            "16비트 [MP3, FLAC...]",
            "12비트 [옛날 오디오]",
            "8비트 [옛날 라디오]",
            "7비트 [18세기 음질]",
            "6비트 [실험용]",
            "5비트 [실험용]",
            "4비트 [실험용]",
            "3비트 [재미용]",
            "8비트 Ex [옛날 오디오]"});
            this.cbConvertBitType.Location = new System.Drawing.Point(32, 19);
            this.cbConvertBitType.Name = "cbConvertBitType";
            this.cbConvertBitType.Size = new System.Drawing.Size(152, 20);
            this.cbConvertBitType.TabIndex = 40;
            this.cbConvertBitType.SelectedIndexChanged += new System.EventHandler(this.cbToInt_SelectedIndexChanged);
            // 
            // chkConvertBitMainOutput
            // 
            this.chkConvertBitMainOutput.AutoSize = true;
            this.chkConvertBitMainOutput.Checked = true;
            this.chkConvertBitMainOutput.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkConvertBitMainOutput.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.chkConvertBitMainOutput.Location = new System.Drawing.Point(5, 98);
            this.chkConvertBitMainOutput.Name = "chkConvertBitMainOutput";
            this.chkConvertBitMainOutput.Size = new System.Drawing.Size(92, 16);
            this.chkConvertBitMainOutput.TabIndex = 39;
            this.chkConvertBitMainOutput.Text = "Main Output";
            this.chkConvertBitMainOutput.UseVisualStyleBackColor = true;
            this.chkConvertBitMainOutput.CheckedChanged += new System.EventHandler(this.chkToIntMainOutput_CheckedChanged);
            // 
            // chkConvertBitBypass
            // 
            this.chkConvertBitBypass.AutoSize = true;
            this.chkConvertBitBypass.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.chkConvertBitBypass.Location = new System.Drawing.Point(105, 98);
            this.chkConvertBitBypass.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.chkConvertBitBypass.Name = "chkConvertBitBypass";
            this.chkConvertBitBypass.Size = new System.Drawing.Size(48, 16);
            this.chkConvertBitBypass.TabIndex = 38;
            this.chkConvertBitBypass.Text = "우회";
            this.chkConvertBitBypass.UseVisualStyleBackColor = true;
            this.chkConvertBitBypass.CheckedChanged += new System.EventHandler(this.chkToIntBypass_CheckedChanged);
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.ForeColor = System.Drawing.Color.White;
            this.label23.Location = new System.Drawing.Point(3, 23);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(29, 12);
            this.label23.TabIndex = 2;
            this.label23.Text = "타입";
            // 
            // chkConvertBit
            // 
            this.chkConvertBit.AutoSize = true;
            this.chkConvertBit.Font = new System.Drawing.Font("Gulim", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.chkConvertBit.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(210)))), ((int)(((byte)(0)))));
            this.chkConvertBit.Location = new System.Drawing.Point(6, 0);
            this.chkConvertBit.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.chkConvertBit.Name = "chkConvertBit";
            this.chkConvertBit.Size = new System.Drawing.Size(81, 16);
            this.chkConvertBit.TabIndex = 0;
            this.chkConvertBit.Text = "비트 변환";
            this.toolTip1.SetToolTip(this.chkConvertBit, "Windows Vista 이상에서 WASAPI의 \r\nAGC에 싫증이 나신분들이 사용하시면 좋습니다.");
            this.chkConvertBit.UseVisualStyleBackColor = true;
            this.chkConvertBit.CheckedChanged += new System.EventHandler(this.chkToInt_CheckedChanged);
            // 
            // button1
            // 
            this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button1.BackColor = System.Drawing.Color.Transparent;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("Gulim", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.button1.ForeColor = System.Drawing.Color.Silver;
            this.button1.Location = new System.Drawing.Point(614, 2);
            this.button1.Margin = new System.Windows.Forms.Padding(2);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(26, 22);
            this.button1.TabIndex = 19;
            this.button1.Text = "_";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label25
            // 
            this.label25.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label25.AutoSize = true;
            this.label25.BackColor = System.Drawing.Color.Transparent;
            this.label25.Font = new System.Drawing.Font("Gulim", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label25.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.label25.Location = new System.Drawing.Point(460, 6);
            this.label25.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(150, 15);
            this.label25.TabIndex = 20;
            this.label25.Text = "HS™ DSP (커스텀)";
            // 
            // saveFileDialog1
            // 
            this.saveFileDialog1.DefaultExt = "hsdsp";
            this.saveFileDialog1.Filter = "HS™DSP 프리셋 (*.hsdsp)|*.hsdsp";
            this.saveFileDialog1.Title = "다른 이름으로 HS™DSP 프로파일 저장";
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.DefaultExt = "hsdsp";
            this.openFileDialog1.Filter = "HS™DSP프리셋 (*.hsdsp)|*.hsdsp";
            this.openFileDialog1.Title = "HS™DSP 프리셋 열기";
            // 
            // timer1
            // 
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // frmAGC
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.AutoScroll = true;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(643, 375);
            this.Controls.Add(this.label25);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.groupBox7);
            this.Controls.Add(this.groupBox6);
            this.Controls.Add(this.groupBox5);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.menuStrip1);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MainMenuStrip = this.menuStrip1;
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "frmAGC";
            this.Text = "HS™ DSP (커스텀)";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmAGC_FormClosing);
            this.Load += new System.EventHandler(this.frmAGC_Load);
            this.SizeChanged += new System.EventHandler(this.frmAGC_SizeChanged);
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.frmAGC_MouseDown);
            this.MouseMove += new System.Windows.Forms.MouseEventHandler(this.frmAGC_MouseMove);
            this.MouseUp += new System.Windows.Forms.MouseEventHandler(this.frmAGC_MouseUp);
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numHSBoosterFrequency)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numHSBoosterRatio)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numHSBoosterWetMix)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numGainControlLevel)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.pnl_dBBar_StereoExpand.ResumeLayout(false);
            this.pnl_dBBar_StereoExpand.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numStereoExpandDryMix)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numStereoExpandWetMix)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numStereoExpandGainValue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numStereoExpandMidValue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numStereoExpandSideValue)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.pnl_dBBar_HSBooster.ResumeLayout(false);
            this.pnl_dBBar_HSBooster.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numHSBoosterDryMix)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numMonoDownMixMIX)).EndInit();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.CheckBox checkBox2;
        private System.Windows.Forms.NumericUpDown numericUpDown1;
        private System.Windows.Forms.CheckBox chkHSBooster;
        private System.Windows.Forms.NumericUpDown numHSBoosterFrequency;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown numHSBoosterRatio;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.NumericUpDown numHSBoosterWetMix;
        private System.Windows.Forms.NumericUpDown numGainControlLevel;
        private System.Windows.Forms.CheckBox chkGainControlEnable;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.CheckBox chkGainControlBypass;
        private System.Windows.Forms.CheckBox chkGainControlMainOutput;
        private System.Windows.Forms.CheckBox chkStereoExpand;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.CheckBox chkStereoExpandOutput;
        private System.Windows.Forms.CheckBox chkStereoExpandBypass;
        private System.Windows.Forms.NumericUpDown numStereoExpandSideValue;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem 파일FToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 설정TToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 개발자모드ToolStripMenuItem;
        private System.Windows.Forms.CheckBox chkStereoExpandSideSame;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.NumericUpDown numStereoExpandMidValue;
        private System.Windows.Forms.CheckBox chkStereoExpandGainAuto;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.NumericUpDown numStereoExpandGainValue;
        private System.Windows.Forms.ToolStripMenuItem 창닫을때모든효과비활성화ToolStripMenuItem;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.CheckBox chkMonoDownMix;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.ComboBox cbMonoDownMix;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.CheckBox chkMonoDownMixMainOutput;
        private System.Windows.Forms.CheckBox chkMonoDownMixBypass;
        private System.Windows.Forms.NumericUpDown numMonoDownMixMIX;
        private System.Windows.Forms.ToolStripMenuItem 도움말HToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 커스텀DSP에대해서ToolStripMenuItem;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.ComboBox cbStereoExpandMethod;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.NumericUpDown numStereoExpandWetMix;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.NumericUpDown numStereoExpandDryMix;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.CheckBox chkSoundReverseBypass;
        private System.Windows.Forms.CheckBox chkSoundReverse;
        private System.Windows.Forms.CheckBox chkSoundReverseMainOutput;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.CheckBox chkHSBoosterLow;
        private System.Windows.Forms.CheckBox chkHSBoosterMainOutput;
        private System.Windows.Forms.CheckBox chkHSBoosterBypass;
        private System.Windows.Forms.Button btnBassBoosterReset;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cbHSBoosterPreset;
        private System.Windows.Forms.NumericUpDown numHSBoosterDryMix;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.CheckBox chkStereoExpandAdvance;
        private System.Windows.Forms.CheckBox chkHSBoosterAdvance;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.ComboBox cbConvertBitType;
        private System.Windows.Forms.CheckBox chkConvertBitMainOutput;
        private System.Windows.Forms.CheckBox chkConvertBitBypass;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.CheckBox chkConvertBit;
        private System.Windows.Forms.CheckBox chkHSBoosterMono;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem 비트변환최우선ToolStripMenuItem;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.ToolStripMenuItem 맨위로설정ToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.CheckBox chkConvertBitDistoration;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.ComboBox cbSoundReversChannels;
        private System.Windows.Forms.ToolStripMenuItem 프리셋열기ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 프리셋저장ToolStripMenuItem;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.ToolStripComboBox toolStripComboBox1;
        private System.Windows.Forms.ToolStripMenuItem 폴더선택ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 새로고침ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 제거ToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripMenuItem 프리셋경로재설정ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 프리셋열기경로재설정ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 프리셋저장경로재설정ToolStripMenuItem1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripMenuItem 프리셋폴더경로재설정ToolStripMenuItem;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
        private System.Windows.Forms.ToolStripMenuItem 새프리셋ToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.ComboBox cbGainControlChannel;
        private System.Windows.Forms.Panel pnl_dBBar_StereoExpand;
        private Control.ProgressBarGradientDecibel dBBar_StereoExpand_R;
        private System.Windows.Forms.Label label27;
        private Control.ProgressBarGradientDecibel dBBar_StereoExpand_L;
        private System.Windows.Forms.Panel pnl_dBBar_HSBooster;
        private Control.ProgressBarGradientDecibel dBBar_HSBooster_R;
        private System.Windows.Forms.Label label31;
        private Control.ProgressBarGradientDecibel dBBar_HSBooster_L;
        private System.Windows.Forms.ToolStripMenuItem 보기VToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator6;
        private System.Windows.Forms.ToolStripMenuItem dB바보이기ToolStripMenuItem;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.CheckBox chkConvertBitEx;
    }
}