﻿using System;
using System.Collections.Generic;
using System.Drawing;

using System.Windows.Forms;
using HS_Audio.DSP;
using System.IO;


namespace HS_Audio.Forms
{
    public partial class frmEqualizer : Form
    {
        const int FirstHz=40;
        //static int FirstHz { get { return 40; } }
        HSAudioHelper help;
        HSAudioDSPHelper helpdsp;
        //public DSPParameter[] _dspp = new DSPParameter[39];
        List<Dictionary<string, string>> Presets = new List<Dictionary<string, string>>();
        List<HSAudioDSPHelperEqualizer> feq = new List<HSAudioDSPHelperEqualizer>(39);
        public bool AutoUpdate = true;
        public frmEqualizer(HSAudioHelper Helper)
        {
            InitializeComponent();
            string a1 = Application.StartupPath.Replace("/", "\\") + "\\Preset\\EQ";
            if (!System.IO.Directory.Exists(a1)) { System.IO.Directory.CreateDirectory(a1); }
            openFileDialog1.InitialDirectory = saveFileDialog1.InitialDirectory = a1;

            새로고침ToolStripMenuItem.ToolTipText = PresetDir + "\n폴더에있는 프리셋의 목록을 새로고침 합니다.";
            toolStripComboBox1.ToolTipText = PresetDir + "\n폴더에있는 프리셋의 목록입니다.";
            this.help = Helper;

            Helper.MusicChanging += new HSAudioHelper.MusicChangeEventHandler(MusicPath_Change);
            try
            {
                for (int i = 0; i < 39; i++)  feq.Add(new HSAudioDSPHelperEqualizer(Helper));
            }catch{}

            Bypass(true); Bypass(0, false);

            //InitPreset();
            새로고침ToolStripMenuItem_Click(null, null);

            string a = Application.StartupPath.Replace("/", "\\") + "\\Preset\\EQ";
            if (!System.IO.Directory.Exists(a)) { System.IO.Directory.CreateDirectory(a); }
            openFileDialog1.InitialDirectory = saveFileDialog1.InitialDirectory = a;
            //feq.
        }

        public DSPParameter[] EQ
        {
            get{return _dspp;}
            set 
            {
                for (int i = 0; i < value.Length;i++ )
                    feq[i].Equalizer = value[i]; 
            }
        }

        public void ReRegister()
        {
            if (rbChannel.Checked)
            {
                for (int i = 0; i < feq.Count; i++)
                {
                    feq[i].ReRegisterDSP(HS_Audio_Lib.DSP_TYPE.PARAMEQ, false);
                    feq[i].Equalizer = _dspp[i];
                }
                feq[2].Gain = trbGain.Value * 0.002f;
            }
            else
            {
                for (int i = 0; i < feq.Count; i++)
                {
                    feq[i].DisConnect(); feq[i].ReRegisterDSP(HS_Audio_Lib.DSP_TYPE.PARAMEQ, true);
                    feq[i].Equalizer = _dspp[i];
                }
                feq[2].Gain = trbGain.Value * 0.002f;
            }
            help.UpdateMix();
        }
        public void ReRegister(bool IsSystem)
        {
            if (!IsSystem)
            {
                for (int i = 0; i < feq.Count; i++)
                {
                    feq[i].ReRegisterDSP(HS_Audio_Lib.DSP_TYPE.PARAMEQ, false);
                    feq[i].Equalizer = _dspp[i];
                }
                feq[2].Gain = trbGain.Value * 0.002f;
            }
            else
            {
                for (int i = 0; i < feq.Count; i++)
                {
                    feq[i].DisConnect(); feq[i].ReRegisterDSP(HS_Audio_Lib.DSP_TYPE.PARAMEQ, true);
                    feq[i].Equalizer = _dspp[i];
                }
                feq[2].Gain = trbGain.Value * 0.002f;
            }
            help.UpdateMix();
        }

        public void Bypass(bool Bypass = true){ for (int i = 0; i < feq.Count; i++)try{ feq[i].Bypass = Bypass;}catch{} }
        protected internal void Bypass(int index, bool Bypass) { try { feq[index].Bypass = Bypass; } catch { } }

        #region EQ변경
        DSPParameter dp;

        private void trackBarEQ_ValueChanged(object sender, EventArgs e)
        {
            TrackBar tb = sender as TrackBar;
            try
            {
                int a = (int)tb.Tag;

#if DEBUG
                if (a == 0) ;
#endif

                _dspp[a].Index = GetDefault[a].Index;
                _dspp[a].Value = tb.Value * 0.01f;

                if (!chkByPass.Checked)
                {
                    feq[a].Equalizer = _dspp[a];
                    Bypass(a, _dspp[a].Value == 1);
                }
            }
            catch { }
        }
        
        /*
        private void trackBar1_ValueChanged(object sender, EventArgs e)
        {
            //FMOD.RESULT a = feq.SetParameter(_dspp[0]);
            _dspp[0].Index = FirstHz;
            _dspp[0].Value=trb40Hz.Value*0.01f;
            feq[0].Equalizer = _dspp[0];

            Bypass(_dspp[0].Value == 1);
            
            //feq.dsp.setParameter((int)FMOD.DSP_PARAMEQ.CENTER, _dspp[0].Index);
            //feq.dsp.setParameter((int)FMOD.DSP_PARAMEQ.BANDWIDTH, 1);
            //feq.dsp.setParameter((int)FMOD.DSP_PARAMEQ.GAIN, _dspp[0].Value);
        }

        private void trackBar2_ValueChanged(object sender, EventArgs e)
        {
            int a=1;
            _dspp[a].Index = GetDefault[a].Index; 
            _dspp[a].Value = trb64Hz.Value * 0.01f;
            feq[a].Equalizer = _dspp[a];

            Bypass(_dspp[a].Value == 1);
        }

        private void trackBar3_ValueChanged(object sender, EventArgs e)
        {
            int a = 2;
            _dspp[a].Index = GetDefault[a].Index;
            _dspp[a].Value = trb78Hz.Value * 0.01f;
            feq[a].Equalizer = _dspp[a];

            Bypass(_dspp[a].Value == 1);
        }

        private void trackBar4_ValueChanged(object sender, EventArgs e)
        {
            int a = 3;
            _dspp[a].Index = GetDefault[a].Index;
            _dspp[a].Value = trb90Hz.Value * 0.01f;
            feq[a].Equalizer = _dspp[a];

            Bypass(_dspp[a].Value == 1);
        }

        private void trackBar5_ValueChanged(object sender, EventArgs e)
        {
            int a = 4;
            _dspp[a].Index = GetDefault[a].Index;
            _dspp[a].Value = trb110Hz.Value * 0.01f;
            feq[a].Equalizer = _dspp[a];

            Bypass(_dspp[a].Value == 1);
        }

        private void trackBar6_ValueChanged(object sender, EventArgs e)
        {
            int a = 5;
            _dspp[a].Index = GetDefault[a].Index;
            _dspp[a].Value = trb125Hz.Value * 0.01f;
            feq[a].Equalizer = _dspp[a];

            Bypass(_dspp[a].Value == 1);
        }

        private void trackBar7_ValueChanged(object sender, EventArgs e)
        {
            int a = 6;
            _dspp[a].Index = GetDefault[a].Index;
            _dspp[a].Value = trb200Hz.Value * 0.01f;
            feq[a].Equalizer = _dspp[a];

            Bypass(_dspp[a].Value == 1);
        }

        private void trackBar8_ValueChanged(object sender, EventArgs e)
        {
            int a = 7;
            _dspp[a].Index = GetDefault[a].Index;
            _dspp[a].Value = trb300Hz.Value * 0.01f;
            feq[a].Equalizer = _dspp[a];

            Bypass(_dspp[a].Value == 1);
        }

        private void trackBar9_ValueChanged(object sender, EventArgs e)
        {
            int a = 8;
            _dspp[a].Index = GetDefault[a].Index;
            _dspp[a].Value = trb400Hz.Value * 0.01f;
            feq[a].Equalizer = _dspp[a];

            Bypass(_dspp[a].Value == 1);
        }

        private void trackBar10_ValueChanged(object sender, EventArgs e)
        {
            int a = 9;
            _dspp[a].Index = GetDefault[a].Index;
            _dspp[a].Value = trb500Hz.Value * 0.01f;
            feq[a].Equalizer = _dspp[a];

            Bypass(_dspp[a].Value == 1);
        }

        private void trackBar11_ValueChanged(object sender, EventArgs e)
        {
            int a = 10;
            _dspp[a].Index = GetDefault[a].Index;
            _dspp[a].Value = trb550Hz.Value * 0.01f;
            feq[a].Equalizer = _dspp[a];

            Bypass(_dspp[a].Value == 1);
        }

        private void trackBar12_ValueChanged(object sender, EventArgs e)
        {
            int a = 11;
            _dspp[a].Index = GetDefault[a].Index;
            _dspp[a].Value = trb600Hz.Value * 0.01f;
            feq[a].Equalizer = _dspp[a];

            Bypass(_dspp[a].Value == 1);
        }

        private void trackBar13_ValueChanged(object sender, EventArgs e)
        {
            int a = 12;
            _dspp[a].Index = GetDefault[a].Index;
            _dspp[a].Value = trb650Hz.Value * 0.01f;
            feq[a].Equalizer = _dspp[a];
        }

        private void trackBar14_ValueChanged(object sender, EventArgs e)
        {
            int a = 13;
            _dspp[a].Index = GetDefault[a].Index;
            _dspp[a].Value = trb700Hz.Value * 0.01f;
            feq[a].Equalizer = _dspp[a];
        }

        private void trackBar15_ValueChanged(object sender, EventArgs e)
        {
            int a = 14;
            _dspp[a].Index = GetDefault[a].Index;
            _dspp[a].Value = trb750Hz.Value * 0.01f;
            feq[a].Equalizer = _dspp[a];
        }

        private void trackBar16_ValueChanged(object sender, EventArgs e)
        {
            int a = 15;
            _dspp[a].Index = GetDefault[a].Index;
            _dspp[a].Value = trb800Hz.Value * 0.01f;
            feq[a].Equalizer = _dspp[a];
        }

        private void trackBar17_ValueChanged(object sender, EventArgs e)
        {
            int a = 16;
            _dspp[a].Index = GetDefault[a].Index;
            _dspp[a].Value = trb850Hz.Value * 0.01f;
            feq[a].Equalizer = _dspp[a];
        }

        private void trackBar18_ValueChanged(object sender, EventArgs e)
        {
            int a = 17;
            _dspp[a].Index = GetDefault[a].Index;
            _dspp[a].Value = trb900Hz.Value * 0.01f;
            feq[a].Equalizer = _dspp[a];
        }

        private void trackBar19_ValueChanged(object sender, EventArgs e)
        {
            int a = 18;
            _dspp[a].Index = GetDefault[a].Index;
            _dspp[a].Value = trb950Hz.Value * 0.01f;
            feq[a].Equalizer = _dspp[a];
        }

        private void trackBar20_ValueChanged(object sender, EventArgs e)
        {
            int a = 19;
            _dspp[a].Index = GetDefault[a].Index;
            _dspp[a].Value = trb1KHz.Value * 0.01f;
            feq[a].Equalizer = _dspp[a];
        }

        private void trackBar21_ValueChanged(object sender, EventArgs e)
        {
            int a = 20;
            _dspp[a].Index = GetDefault[a].Index;
            _dspp[a].Value = trb2KHz.Value * 0.01f;
            feq[a].Equalizer = _dspp[a];
        }

        private void trackBar22_ValueChanged(object sender, EventArgs e)
        {
            int a = 21;
            _dspp[a].Index = GetDefault[a].Index;
            _dspp[a].Value = trb3KHz.Value * 0.01f;
            feq[a].Equalizer = _dspp[a];
        }

        private void trackBar23_ValueChanged(object sender, EventArgs e)
        {
            int a = 22;
            _dspp[a].Index = GetDefault[a].Index;
            _dspp[a].Value = trb4KHz.Value * 0.01f;
            feq[a].Equalizer = _dspp[a];
        }

        private void trackBar24_ValueChanged(object sender, EventArgs e)
        {
            int a = 23;
            _dspp[a].Index = GetDefault[a].Index;
            _dspp[a].Value = trb5KHz.Value * 0.01f;
            feq[a].Equalizer = _dspp[a];
        }

        private void trackBar25_ValueChanged(object sender, EventArgs e)
        {
            int a = 24;
            _dspp[a].Index = GetDefault[a].Index;
            _dspp[a].Value = trb6KHz.Value * 0.01f;
            feq[a].Equalizer = _dspp[a];
        }

        private void trackBar26_ValueChanged(object sender, EventArgs e)
        {
            int a = 25;
            _dspp[a].Index = GetDefault[a].Index;
            _dspp[a].Value = trb7KHz.Value * 0.01f;
            feq[a].Equalizer = _dspp[a];
        }

        private void trackBar27_ValueChanged(object sender, EventArgs e)
        {
            int a = 26;
            _dspp[a].Index = GetDefault[a].Index;
            _dspp[a].Value = trb8KHz.Value * 0.01f;
            feq[a].Equalizer = _dspp[a];
        }

        private void trackBar28_ValueChanged(object sender, EventArgs e)
        {
            int a = 27;
            _dspp[a].Index = GetDefault[a].Index;
            _dspp[a].Value = trb9KHz.Value * 0.01f;
            feq[a].Equalizer = _dspp[a];
        }

        private void trackBar29_ValueChanged(object sender, EventArgs e)
        {
            int a = 28;
            _dspp[a].Index = GetDefault[a].Index;
            _dspp[a].Value = trb10KHz.Value * 0.01f;
            feq[a].Equalizer = _dspp[a];
        }

        private void trackBar30_ValueChanged(object sender, EventArgs e)
        {
            int a = 29;
            _dspp[a].Index = GetDefault[a].Index;
            _dspp[a].Value = trb11KHz.Value * 0.01f;
            feq[a].Equalizer = _dspp[a];
        }

        private void trackBar31_ValueChanged(object sender, EventArgs e)
        {
            int a = 30;
            _dspp[a].Index = GetDefault[a].Index;
            _dspp[a].Value = trb12KHz.Value * 0.01f;
            feq[a].Equalizer = _dspp[a];
        }

        private void trackBar32_ValueChanged(object sender, EventArgs e)
        {
            int a = 31;
            _dspp[a].Index = GetDefault[a].Index;
            _dspp[a].Value = trb13KHz.Value * 0.01f;
            feq[a].Equalizer = _dspp[a];
        }

        private void trackBar33_ValueChanged(object sender, EventArgs e)
        {
            int a = 32;
            _dspp[a].Index = GetDefault[a].Index;
            _dspp[a].Value = trb14KHz.Value * 0.01f;
            feq[a].Equalizer = _dspp[a];
        }

        private void trackBar34_ValueChanged(object sender, EventArgs e)
        {
            int a = 33;
            _dspp[a].Index = GetDefault[a].Index;
            _dspp[a].Value = trb15KHz.Value * 0.01f;
            feq[a].Equalizer = _dspp[a];
        }

        private void trackBar35_ValueChanged(object sender, EventArgs e)
        {
            int a = 34;
            _dspp[a].Index = GetDefault[a].Index;
            _dspp[a].Value = trb16KHz.Value * 0.01f;
            feq[a].Equalizer = _dspp[a];
        }

        private void trackBar36_ValueChanged(object sender, EventArgs e)
        {
            int a = 35;
            _dspp[a].Index = GetDefault[a].Index;
            _dspp[a].Value = trb17KHz.Value * 0.01f;
            feq[a].Equalizer = _dspp[a];
        }

        private void trackBar37_ValueChanged(object sender, EventArgs e)
        {
            int a = 36;
            _dspp[a].Index = GetDefault[a].Index;
            _dspp[a].Value = trb18KHz.Value * 0.01f;
            feq[a].Equalizer = _dspp[a];
        }

        private void trackBar38_ValueChanged(object sender, EventArgs e)
        {
            int a = 37;
            _dspp[a].Index = GetDefault[a].Index;
            _dspp[a].Value = trb19KHz.Value * 0.01f;
            feq[a].Equalizer = _dspp[a];
        }

        private void trackBar39_ValueChanged(object sender, EventArgs e)
        {
            int a = 38;
            _dspp[a].Index = GetDefault[a].Index;
            _dspp[a].Value = trb20KHz.Value * 0.01f;
            feq[a].Equalizer = _dspp[a];
        }*/
        #endregion

        #region 기본값버튼 클릭
        int DefaultValue = 100;

        private void button1_Click(object sender, EventArgs e)
        {
            trb40Hz.Value = DefaultValue;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            trb64Hz.Value = DefaultValue;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            trb78Hz.Value = DefaultValue;
        }

        private void button4_Click(object sender, EventArgs e)
        {
            trb90Hz.Value = DefaultValue;
        }

        private void button5_Click(object sender, EventArgs e)
        {
            trb110Hz.Value = DefaultValue;
        }

        private void button6_Click(object sender, EventArgs e)
        {
            trb125Hz.Value = DefaultValue;
        }

        private void button7_Click(object sender, EventArgs e)
        {
            trb200Hz.Value = DefaultValue;
        }

        private void button8_Click(object sender, EventArgs e)
        {
            trb300Hz.Value = DefaultValue;
        }

        private void button9_Click(object sender, EventArgs e)
        {
            trb400Hz.Value = DefaultValue;
        }

        private void button10_Click(object sender, EventArgs e)
        {
            trb500Hz.Value = DefaultValue;
        }

        private void button11_Click(object sender, EventArgs e)
        {
            trb550Hz.Value = DefaultValue;
        }

        private void button12_Click(object sender, EventArgs e)
        {
            trb600Hz.Value = DefaultValue;
        }

        private void button13_Click(object sender, EventArgs e)
        {
            trb650Hz.Value = DefaultValue;
        }

        private void button14_Click(object sender, EventArgs e)
        {
            trb700Hz.Value = DefaultValue;
        }

        private void button15_Click(object sender, EventArgs e)
        {
            trb750Hz.Value = DefaultValue;
        }

        private void button16_Click(object sender, EventArgs e)
        {
            trb800Hz.Value = DefaultValue;
        }

        private void button17_Click(object sender, EventArgs e)
        {
            trb850Hz.Value = DefaultValue;
        }

        private void button18_Click(object sender, EventArgs e)
        {
            trb900Hz.Value = DefaultValue;
        }

        private void button19_Click(object sender, EventArgs e)
        {
            trb950Hz.Value = DefaultValue;
        }

        private void button20_Click(object sender, EventArgs e)
        {
            trb1KHz.Value = DefaultValue;
        }

        private void button21_Click(object sender, EventArgs e)
        {
            trb2KHz.Value = DefaultValue;
        }

        private void button22_Click(object sender, EventArgs e)
        {
            trb3KHz.Value = DefaultValue;
        }

        private void button23_Click(object sender, EventArgs e)
        {
            trb4KHz.Value = DefaultValue;
        }

        private void button24_Click(object sender, EventArgs e)
        {
            trb5KHz.Value = DefaultValue;
        }

        private void button25_Click(object sender, EventArgs e)
        {
            trb6KHz.Value = DefaultValue;
        }

        private void button26_Click(object sender, EventArgs e)
        {
            trb7KHz.Value = DefaultValue;
        }

        private void button27_Click(object sender, EventArgs e)
        {
            trb8KHz.Value = DefaultValue;
        }

        private void button28_Click(object sender, EventArgs e)
        {
            trb9KHz.Value = DefaultValue;
        }

        private void button29_Click(object sender, EventArgs e)
        {
            trb10KHz.Value = DefaultValue;
        }

        private void button30_Click(object sender, EventArgs e)
        {
            trb11KHz.Value = DefaultValue;
        }

        private void button31_Click(object sender, EventArgs e)
        {
            trb12KHz.Value = DefaultValue;
        }

        private void button32_Click(object sender, EventArgs e)
        {
            trb13KHz.Value = DefaultValue;
        }

        private void button33_Click(object sender, EventArgs e)
        {
            trb14KHz.Value = DefaultValue;
        }

        private void button34_Click(object sender, EventArgs e)
        {
            trb15KHz.Value = DefaultValue;
        }

        private void button35_Click(object sender, EventArgs e)
        {
            trb16KHz.Value = DefaultValue;
        }

        private void button36_Click(object sender, EventArgs e)
        {
            trb17KHz.Value = DefaultValue;
        }

        private void button37_Click(object sender, EventArgs e)
        {
            trb18KHz.Value = DefaultValue;
        }

        private void button38_Click(object sender, EventArgs e)
        {
            trb19KHz.Value = DefaultValue;
        }

        private void button39_Click(object sender, EventArgs e)
        {
            trb20KHz.Value = DefaultValue;
        }
        #endregion

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (EQUse.Checked)
            {
                for (int i = 0; i < feq.Count; i++)
                {
                    feq[i].Connect();
                    //feq[i].Equalizer = _dspp[i];
                }
                if (chkByPass.Checked) Bypass();
                else System.Threading.Thread.Sleep(50);
                feq[0].Gain = trbGain.Value * 0.002f;
                panel1.Enabled = panel2.Enabled = true;
            }
            else 
            {
                for (int i = 0; i < feq.Count; i++)
                {
                    feq[i].DisConnect();
                    //try{feq[i].Equalizer = GetDefault[i];}catch{}
                }
                //try{feq[0].Gain = 1f;}catch{}
                panel1.Enabled = panel2.Enabled = false;
            }

        }

        private void EQ_ValueChanged(object sender, EventArgs e)
        {
            //_dspp[0].Index = 15; _dspp[0].Value = trackBar1.Value * 0.01f;
            
        }

        public DSPParameter[] _dspp = GetDefault;

        public static DSPParameter[] GetDefault
        {
            get
            {
                List<DSPParameter> a = new List<DSPParameter>();
                a.Add(new DSPParameter(FirstHz, 1));
                a.Add(new DSPParameter(64, 1));
                a.Add(new DSPParameter(78, 1));
                a.Add(new DSPParameter(90, 1));
                a.Add(new DSPParameter(110, 1));
                a.Add(new DSPParameter(125, 1));
                a.Add(new DSPParameter(200, 1));
                a.Add(new DSPParameter(300, 1));
                a.Add(new DSPParameter(400, 1));
                a.Add(new DSPParameter(500, 1));
                a.Add(new DSPParameter(550, 1));
                a.Add(new DSPParameter(600, 1));
                a.Add(new DSPParameter(650, 1));
                a.Add(new DSPParameter(700, 1));
                a.Add(new DSPParameter(750, 1));
                a.Add(new DSPParameter(800, 1));
                a.Add(new DSPParameter(850, 1));
                a.Add(new DSPParameter(900, 1));
                a.Add(new DSPParameter(950, 1));
                a.Add(new DSPParameter(1000, 1));
                a.Add(new DSPParameter(2000, 1));
                a.Add(new DSPParameter(3000, 1));
                a.Add(new DSPParameter(4000, 1));
                a.Add(new DSPParameter(5000, 1));
                a.Add(new DSPParameter(6000, 1));
                a.Add(new DSPParameter(7000, 1));
                a.Add(new DSPParameter(8000, 1));
                a.Add(new DSPParameter(9000, 1));
                a.Add(new DSPParameter(10000, 1));
                a.Add(new DSPParameter(11000, 1));
                a.Add(new DSPParameter(12000, 1));
                a.Add(new DSPParameter(13000, 1));
                a.Add(new DSPParameter(14000, 1));
                a.Add(new DSPParameter(15000, 1));
                a.Add(new DSPParameter(16000, 1));
                a.Add(new DSPParameter(17000, 1));
                a.Add(new DSPParameter(18000, 1));
                a.Add(new DSPParameter(19000, 1));
                a.Add(new DSPParameter(20000, 1));
                return a.ToArray();
            }
        }

        private void trbGain_ValueChanged(object sender, EventArgs e)
        {
            //for (int i = 0; i < feq.Count;i++ )
            //feq[i].Mix = trbGain.Value * 0.003f;
            if (!chkByPass.Checked)
                feq[0].Gain = chkByPass.Checked ? 1 : trbGain.Value * 0.002f;
            label4.Text = trbGain.Value.ToString();
        }

        

        private void frmEqulazer_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = true;
            this.Hide();
        }

        private void checkBox2_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {
            if (rbChannel.Checked)
            {
                for (int i = 0; i < feq.Count; i++){
                    feq[i].ReRegisterDSP(HS_Audio_Lib.DSP_TYPE.PARAMEQ, false);
                    feq[i].Equalizer = _dspp[i];}
                feq[2].Gain = chkByPass.Checked?1:trbGain.Value * 0.002f;
            }
            else
            {
                for (int i = 0; i < feq.Count; i++){
                    feq[i].DisConnect(); feq[i].ReRegisterDSP(HS_Audio_Lib.DSP_TYPE.PARAMEQ, true);
                    feq[i].Equalizer = _dspp[i];}
                feq[2].Gain = chkByPass.Checked?1:trbGain.Value * 0.002f;
            }
            help.UpdateMix();
        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void 프리셋열기ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!System.IO.Directory.Exists(PresetDir)) { System.IO.Directory.CreateDirectory(PresetDir); }
            //LastFileDirectory
            openFileDialog1.InitialDirectory = Directory.Exists(LastFileOpenDirectory) ? LastFileOpenDirectory : PresetDir;

            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                string[] a1 = System.IO.File.ReadAllLines(openFileDialog1.FileName);
                EQPreset = HS_CSharpUtility.Utility.EtcUtility.LoadSetting(a1);
                Presets.Add(EQPreset);
                toolStripComboBox1.Text = toolStripComboBox1.Items[toolStripComboBox1.Items.Count - 1].ToString();
            }
        }

        private void 프리셋저장ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!System.IO.Directory.Exists(PresetDir)) { System.IO.Directory.CreateDirectory(PresetDir); }
            saveFileDialog1.InitialDirectory = Directory.Exists(LastFileSaveDirectory) ? LastFileSaveDirectory : PresetDir;

            saveFileDialog1.FileName = toolStripComboBox1.Text;
            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                LastFileSaveDirectory = Path.GetDirectoryName(saveFileDialog1.FileName);
                string[] a1 = HS_CSharpUtility.Utility.EtcUtility.SaveSetting(EQPreset);
                System.IO.File.WriteAllLines(saveFileDialog1.FileName, a1);
                if(자동으로새로고침ToolStripMenuItem.Checked) 새로고침ToolStripMenuItem_Click(null, null);
            }
        }
        Dictionary<string, string> InitPreset()
        {

            Dictionary<string, string> eqpreset = new Dictionary<string, string>();
            eqpreset.Add(trb40Hz.Name, "100"); eqpreset.Add(trb64Hz.Name, "100");
            eqpreset.Add(trb78Hz.Name, "100"); eqpreset.Add(trb90Hz.Name, "100");
            eqpreset.Add(trb110Hz.Name, "100");
            eqpreset.Add(trb125Hz.Name, "100"); eqpreset.Add(trb200Hz.Name, "100");
            eqpreset.Add(trb300Hz.Name, "100"); eqpreset.Add(trb400Hz.Name, "100");
            eqpreset.Add(trb500Hz.Name, "100"); eqpreset.Add(trb550Hz.Name, "100");
            eqpreset.Add(trb600Hz.Name, "100"); eqpreset.Add(trb650Hz.Name, "100");
            eqpreset.Add(trb700Hz.Name, "100"); eqpreset.Add(trb750Hz.Name, "100");
            eqpreset.Add(trb800Hz.Name, "100"); eqpreset.Add(trb850Hz.Name, "100");
            eqpreset.Add(trb900Hz.Name, "100"); eqpreset.Add(trb950Hz.Name, "100");
            eqpreset.Add(trb1KHz.Name, "100"); eqpreset.Add(trb2KHz.Name, "100");
            eqpreset.Add(trb3KHz.Name, "100"); eqpreset.Add(trb4KHz.Name, "100");
            eqpreset.Add(trb5KHz.Name, "100"); eqpreset.Add(trb6KHz.Name, "100");
            eqpreset.Add(trb7KHz.Name, "100"); eqpreset.Add(trb8KHz.Name, "100");
            eqpreset.Add(trb9KHz.Name, "100"); eqpreset.Add(trb10KHz.Name, "100");
            eqpreset.Add(trb11KHz.Name, "100"); eqpreset.Add(trb12KHz.Name, "100");
            eqpreset.Add(trb13KHz.Name, "100"); eqpreset.Add(trb14KHz.Name, "100");
            eqpreset.Add(trb15KHz.Name, "100"); eqpreset.Add(trb16KHz.Name, "100");
            eqpreset.Add(trb17KHz.Name, "100"); eqpreset.Add(trb18KHz.Name, "100");
            eqpreset.Add(trb19KHz.Name, "100"); eqpreset.Add(trb20KHz.Name, "100");
            eqpreset.Add(trbGain.Name, "200"); eqpreset.Add(rbMainOuput.Name, "True");
            return eqpreset;
        }
        internal Dictionary<string, string> EQPreset
        {
            get
            {
                Dictionary<string, string> eqpreset = new Dictionary<string, string>();
                eqpreset.Add(trb40Hz.Name, trb40Hz.Value.ToString());
                eqpreset.Add(trb64Hz.Name, trb64Hz.Value.ToString());
                eqpreset.Add(trb78Hz.Name, trb78Hz.Value.ToString());
                eqpreset.Add(trb90Hz.Name, trb90Hz.Value.ToString());
                eqpreset.Add(trb110Hz.Name, trb110Hz.Value.ToString()); 
                eqpreset.Add(trb125Hz.Name, trb125Hz.Value.ToString());
                eqpreset.Add(trb200Hz.Name, trb200Hz.Value.ToString());
                eqpreset.Add(trb300Hz.Name, trb300Hz.Value.ToString());
                eqpreset.Add(trb400Hz.Name, trb400Hz.Value.ToString());
                eqpreset.Add(trb500Hz.Name, trb500Hz.Value.ToString());
                eqpreset.Add(trb550Hz.Name, trb550Hz.Value.ToString());
                eqpreset.Add(trb600Hz.Name, trb600Hz.Value.ToString());
                eqpreset.Add(trb650Hz.Name, trb650Hz.Value.ToString());
                eqpreset.Add(trb700Hz.Name, trb700Hz.Value.ToString());
                eqpreset.Add(trb750Hz.Name, trb750Hz.Value.ToString());
                eqpreset.Add(trb800Hz.Name, trb800Hz.Value.ToString());
                eqpreset.Add(trb850Hz.Name, trb850Hz.Value.ToString());
                eqpreset.Add(trb900Hz.Name, trb900Hz.Value.ToString());
                eqpreset.Add(trb950Hz.Name, trb950Hz.Value.ToString());
                eqpreset.Add(trb1KHz.Name, trb1KHz.Value.ToString());
                eqpreset.Add(trb2KHz.Name, trb2KHz.Value.ToString());
                eqpreset.Add(trb3KHz.Name, trb3KHz.Value.ToString());
                eqpreset.Add(trb4KHz.Name, trb4KHz.Value.ToString());
                eqpreset.Add(trb5KHz.Name, trb5KHz.Value.ToString());
                eqpreset.Add(trb6KHz.Name, trb6KHz.Value.ToString());
                eqpreset.Add(trb7KHz.Name, trb7KHz.Value.ToString());
                eqpreset.Add(trb8KHz.Name, trb8KHz.Value.ToString());
                eqpreset.Add(trb9KHz.Name, trb9KHz.Value.ToString());
                eqpreset.Add(trb10KHz.Name, trb10KHz.Value.ToString());
                eqpreset.Add(trb11KHz.Name, trb11KHz.Value.ToString());
                eqpreset.Add(trb12KHz.Name, trb12KHz.Value.ToString());
                eqpreset.Add(trb13KHz.Name, trb13KHz.Value.ToString());
                eqpreset.Add(trb14KHz.Name, trb14KHz.Value.ToString());
                eqpreset.Add(trb15KHz.Name, trb15KHz.Value.ToString());
                eqpreset.Add(trb16KHz.Name, trb16KHz.Value.ToString());
                eqpreset.Add(trb17KHz.Name, trb17KHz.Value.ToString());
                eqpreset.Add(trb18KHz.Name, trb18KHz.Value.ToString());
                eqpreset.Add(trb19KHz.Name, trb19KHz.Value.ToString());
                eqpreset.Add(trb20KHz.Name, trb20KHz.Value.ToString());
                eqpreset.Add(trbGain.Name, trbGain.Value.ToString());
                eqpreset.Add(rbMainOuput.Name, rbMainOuput.Checked.ToString());
                return eqpreset;
            }
            set
            {
                try { trb40Hz.Value = int.Parse(value[trb40Hz.Name]); }catch{}
                try { trb64Hz.Value = int.Parse(value[trb64Hz.Name]); }catch { }
                try { trb78Hz.Value = int.Parse(value[trb78Hz.Name]); }catch { }
                try { trb90Hz.Value = int.Parse(value[trb90Hz.Name]); }catch{}
                try { trb110Hz.Value = int.Parse(value[trb110Hz.Name]); }catch{}
                try { trb125Hz.Value = int.Parse(value[trb125Hz.Name]); }catch{}
                try { trb200Hz.Value = int.Parse(value[trb200Hz.Name]); }catch{}
                try { trb400Hz.Value = int.Parse(value[trb400Hz.Name]); }catch{}
                try { trb300Hz.Value = int.Parse(value[trb300Hz.Name]); }catch{}
                try { trb500Hz.Value = int.Parse(value[trb500Hz.Name]); }catch{}
                try { trb550Hz.Value = int.Parse(value[trb550Hz.Name]); }catch{}
                try { trb600Hz.Value = int.Parse(value[trb600Hz.Name]); }catch{}
                try { trb650Hz.Value = int.Parse(value[trb650Hz.Name]); }catch{}
                try { trb700Hz.Value = int.Parse(value[trb700Hz.Name]); }catch{}
                try { trb750Hz.Value = int.Parse(value[trb750Hz.Name]); }catch { }
                try { trb800Hz.Value = int.Parse(value[trb800Hz.Name]); } catch { }
                try { trb850Hz.Value = int.Parse(value[trb850Hz.Name]); } catch { }
                try { trb900Hz.Value = int.Parse(value[trb900Hz.Name]); }catch { }
                try { trb950Hz.Value = int.Parse(value[trb950Hz.Name]); } catch { }
                try { trb1KHz.Value = int.Parse(value[trb1KHz.Name]); } catch { }
                try { trb2KHz.Value = int.Parse(value[trb2KHz.Name]); }catch{}
                try { trb3KHz.Value = int.Parse(value[trb3KHz.Name]); }catch{}
                try { trb4KHz.Value = int.Parse(value[trb4KHz.Name]); }catch{}
                try { trb5KHz.Value = int.Parse(value[trb5KHz.Name]); }catch{}
                try { trb6KHz.Value = int.Parse(value[trb6KHz.Name]); }catch{}
                try { trb7KHz.Value = int.Parse(value[trb7KHz.Name]); }catch{}
                try { trb8KHz.Value = int.Parse(value[trb8KHz.Name]); }catch{}
                try { trb9KHz.Value = int.Parse(value[trb9KHz.Name]); }catch{}
                try { trb10KHz.Value = int.Parse(value[trb10KHz.Name]); }catch{}
                try { trb11KHz.Value = int.Parse(value[trb11KHz.Name]); }catch{}
                try { trb12KHz.Value = int.Parse(value[trb12KHz.Name]); }catch{}
                try { trb13KHz.Value = int.Parse(value[trb13KHz.Name]); }catch{}
                try { trb14KHz.Value = int.Parse(value[trb14KHz.Name]); }catch{}
                try { trb15KHz.Value = int.Parse(value[trb15KHz.Name]); }catch{}
                try { trb16KHz.Value = int.Parse(value[trb16KHz.Name]); }catch{}
                try { trb17KHz.Value = int.Parse(value[trb17KHz.Name]); }catch{}
                try { trb18KHz.Value = int.Parse(value[trb18KHz.Name]); }catch{}
                try { trb19KHz.Value = int.Parse(value[trb19KHz.Name]); }catch{}
                try { trb20KHz.Value = int.Parse(value[trb20KHz.Name]); }catch{}
                try { trbGain.Value = int.Parse(value[trbGain.Name]); }catch{}
                try { bool a = bool.Parse(value[rbMainOuput.Name]); if(a != rbMainOuput.Checked) rbMainOuput.Checked = a; }catch{}
                if (EQUse.Checked)rbChannel.Checked=!rbMainOuput.Checked;
                if (chkByPass.Checked) Bypass();
            }
        }

        private void 새프리셋ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if(DialogResult.Yes == MessageBox.Show("정말 모두 초기화하고 새 프리셋을 작성하시겠습니까?", "이퀄라이저 프리셋 초기화", MessageBoxButtons.YesNo, MessageBoxIcon.Warning))
            { EQPreset = InitPreset(); toolStripComboBox1.SelectedIndex = 0; PresetText = null; }
        }

        public string PresetDir { get {return Application.StartupPath.Replace("/", "\\") + "\\Preset\\EQ"; } }
        string LastPreset = "(기본값)";
        private void 새로고침ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Presets.Clear();toolStripComboBox1.Items.Clear();
            Presets.Add(InitPreset()); toolStripComboBox1.Items.Add("(기본값)");
            foreach(string a in Directory.GetFiles(LastFolderDirectory, "*.eqprof"))
            {
                try
                {
                    Dictionary<string, string> b =
                        HS_CSharpUtility.Utility.EtcUtility.LoadSetting(File.ReadAllLines(a));
                    Presets.Add(b);
                    toolStripComboBox1.Items.Add(Path.GetFileNameWithoutExtension(a));
                }
                catch { }
            }
            if (toolStripComboBox1.Items.IndexOf(LastPreset) != -1) toolStripComboBox1.SelectedIndex = toolStripComboBox1.Items.IndexOf(LastPreset);
            else toolStripComboBox1.SelectedIndex = 0;
        }

        string _PresetText;
        public string PresetText { get { return _PresetText; } set { 
            this.Text = value!=null&&value!=""?"이퀄라이저(EQ) 설정 - [" + value + "]":"이퀄라이저(EQ) 설정"; } }
        private void toolStripComboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (toolStripComboBox1.SelectedIndex != -1)
            {
                EQPreset = Presets[toolStripComboBox1.SelectedIndex];
                LastPreset = toolStripComboBox1.Items[toolStripComboBox1.SelectedIndex].ToString();
                PresetText = LastPreset;
                if (toolStripComboBox1.SelectedIndex == 0) { this.Text = "이퀄라이저(EQ) 설정"; 제거ToolStripMenuItem.Enabled = false; }
                else 제거ToolStripMenuItem.Enabled = true;
                //checkBox1_CheckedChanged(null, null);
            }
        }

        private void toolStripSeparator2_Click(object sender, EventArgs e)
        {

        }

        private void 제거ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (toolStripComboBox1.Items.Count > 1 && toolStripComboBox1.SelectedIndex != 0){
            if (DialogResult.Yes == MessageBox.Show("정말 선택된 프리셋을 목록에서 제거 하시겠습니까?", "이퀄라이저(EQ) 선택된 프리셋 목록에서 제거", MessageBoxButtons.YesNo, MessageBoxIcon.Warning))
            { Presets.RemoveAt(toolStripComboBox1.SelectedIndex); toolStripComboBox1.Items.RemoveAt(toolStripComboBox1.SelectedIndex); toolStripComboBox1.SelectedIndex = 0; }}
        }

        private void 정렬ToolStripMenuItem_CheckedChanged(object sender, EventArgs e)
        {
            string a = toolStripComboBox1.Text;
            if (정렬ToolStripMenuItem.Checked)
            {
                toolStripComboBox1.Sorted = true;
                정렬ToolStripMenuItem.BackColor = Color.Lime;
            }
            else
            {
                toolStripComboBox1.Sorted =false;
                정렬ToolStripMenuItem.BackColor = Color.Transparent;
            }
            if (toolStripComboBox1.Items.IndexOf(a) != -1) { toolStripComboBox1.SelectedIndex = toolStripComboBox1.Items.IndexOf(a); }
        }

        private void 폴더선택ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!System.IO.Directory.Exists(PresetDir)) { System.IO.Directory.CreateDirectory(PresetDir); }
            folderBrowserDialog1.SelectedPath = Directory.Exists(LastFolderDirectory) ? LastFolderDirectory : PresetDir; ;
            if (DialogResult.OK == folderBrowserDialog1.ShowDialog())
            { LastFolderDirectory = folderBrowserDialog1.SelectedPath; 새로고침ToolStripMenuItem_Click(null, null); }
        }
        Dictionary<string, string> PresetPath = new Dictionary<string, string>();
        string LastFileOpenDirectory
        {
            get
            {
                if (File.Exists(PresetDir + "\\LastEQPath.ini"))
                {
                    string[] a = File.ReadAllLines(PresetDir + "\\LastEQPath.ini");
                    string a1 = HS_CSharpUtility.Utility.EtcUtility.LoadSetting(a)["LastFileOpenDirectory"];
                    if (a1 != null || a1 != "") { return Directory.Exists(a1) ? a1 : PresetDir; }
                    else { return PresetDir; }
                }
                else { return PresetDir; }
            }
            set
            {
                if (PresetPath.Count < 3)
                {
                    PresetPath.Clear(); PresetPath.Add("LastFileOpenDirectory", value);
                    PresetPath.Add("LastFileSaveDirectory", PresetDir);
                    PresetPath.Add("LastFolderDirectory", PresetDir);
                }
                else { PresetPath["LastFileOpenDirectory"] = value; }
                File.WriteAllLines(PresetDir + "\\LastEQPath.ini", HS_CSharpUtility.Utility.EtcUtility.SaveSetting(PresetPath));
            }
        }
        string LastFileSaveDirectory
        {
            get
            {
                if (File.Exists(PresetDir + "\\LastEQPath.ini"))
                {
                    string[] a = File.ReadAllLines(PresetDir + "\\LastEQPath.ini");
                    string a1 = HS_CSharpUtility.Utility.EtcUtility.LoadSetting(a)["LastFileSaveDirectory"];
                    if (a1 != null || a1 != "") { return Directory.Exists(a1) ? a1 : PresetDir;}
                    else { return PresetDir; }
                }
                else { return PresetDir; }
            }
            set
            {
                if (PresetPath.Count < 3)
                {
                    PresetPath.Clear(); PresetPath.Add("LastFileOpenDirectory", PresetDir);
                    PresetPath.Add("LastFileSaveDirectory", value);
                    PresetPath.Add("LastFolderDirectory", PresetDir);
                }
                else { PresetPath["LastFileSaveDirectory"] = value; }
                File.WriteAllLines(PresetDir + "\\LastEQPath.ini", HS_CSharpUtility.Utility.EtcUtility.SaveSetting(PresetPath));
            }
        }
        string LastFolderDirectory
        {
            get
            {
                if (File.Exists(PresetDir + "\\LastEQPath.ini"))
                {
                    string[] a = File.ReadAllLines(PresetDir + "\\LastEQPath.ini");
                    string a1 = HS_CSharpUtility.Utility.EtcUtility.LoadSetting(a)["LastFolderDirectory"];
                    if (a1 != null || a1 != "") { return Directory.Exists(a1) ? a1 : PresetDir; }
                    else { return PresetDir; }
                }
                else { return PresetDir; }
            }
            set
            {
                if (PresetPath.Count < 2)
                {
                    PresetPath.Clear(); PresetPath.Add("LastFileOpenDirectory", PresetDir);
                    PresetPath.Add("LastFileSaveDirectory", PresetDir);
                    PresetPath.Add("LastFolderDirectory", value);
                }
                else { PresetPath["LastFolderDirectory"] = value; }
                File.WriteAllLines(PresetDir + "\\LastEQPath.ini", HS_CSharpUtility.Utility.EtcUtility.SaveSetting(PresetPath));
            }
        }

        private void 프리셋경로재설정ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("정말 모든 이퀄라이저(EQ) 프리셋 경로를 재설정 하시겠습니까?", "이퀄라이저(EQ) 경로 모두 재설정",
                MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
            { LastFolderDirectory = LastFileSaveDirectory = LastFolderDirectory = PresetDir; }
        }

        private void 프리셋열기경로재설정ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("정말 이퀄라이저(EQ) 프리셋 열기 경로를 재설정 하시겠습니까?", "이퀄라이저(EQ) 열기 경로 재설정",
                MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
            { LastFileOpenDirectory = PresetDir; }
        }

        private void 프리셋저장경로재설정ToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("정말 이퀄라이저(EQ) 프리셋 저장 경로를 재설정 하시겠습니까?", "이퀄라이저(EQ) 열기 저장 재설정",
                MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
            { LastFileSaveDirectory = PresetDir; }
        }

        private void 프리셋폴더경로재설정ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("정말 이퀄라이저(EQ) 프리셋 폴더 경로를 재설정 하시겠습니까?", "이퀄라이저(EQ) 폴더 경로 재설정",
                MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
            { LastFolderDirectory = PresetDir; }
        }

        internal void MusicPath_Change(HSAudioHelper Helper, int index, bool Error)
        {
            if (AutoUpdate&&!Error)
            {
                if (EQUse.Checked) { ReRegister(); }
            }
        }

        private void 자동DSP등록ToolStripMenuItem_CheckedChanged(object sender, EventArgs e) { AutoUpdate = 자동DSP등록ToolStripMenuItem.Checked; }

        private void 설정TToolStripMenuItem_DropDownOpening(object sender, EventArgs e) { 자동DSP등록ToolStripMenuItem.Checked = AutoUpdate; }

        private void frmEqulazer_Load(object sender, EventArgs e)
        {

        }

        private void chkByPass_CheckedChanged(object sender, EventArgs e)
        {
            if(chkByPass.Checked)Bypass(true);
            else
            {
                for (int i = 1; i < feq.Count; i++)
                    try { Bypass(i, _dspp[i].Value == 1); } catch { }
            }
            feq[0].Gain = chkByPass.Checked ? 1f :trbGain.Value * 0.002f;
        }

        private void panel2_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}
