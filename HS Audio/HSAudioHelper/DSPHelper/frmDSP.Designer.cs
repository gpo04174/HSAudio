﻿namespace HS_Audio.Forms
{
    partial class frmDSP
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmDSP));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.button38 = new System.Windows.Forms.Button();
            this.button37 = new System.Windows.Forms.Button();
            this.button36 = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.lblMAXAMP = new System.Windows.Forms.Label();
            this.trbMAXAMP = new System.Windows.Forms.TrackBar();
            this.label3 = new System.Windows.Forms.Label();
            this.lblTHRESHHOLD = new System.Windows.Forms.Label();
            this.trbTHRESHHOLD = new System.Windows.Forms.TrackBar();
            this.label2 = new System.Windows.Forms.Label();
            this.lblFADETIME = new System.Windows.Forms.Label();
            this.trbFADETIME = new System.Windows.Forms.TrackBar();
            this.chkNOMALIZER = new System.Windows.Forms.CheckBox();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel2 = new System.Windows.Forms.ToolStripStatusLabel();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.button3 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.lblWETMIX_ECHO = new System.Windows.Forms.Label();
            this.trbWETMIX_ECHO = new System.Windows.Forms.TrackBar();
            this.label9 = new System.Windows.Forms.Label();
            this.lblDECAYRATIO = new System.Windows.Forms.Label();
            this.trbDECAYRATIO = new System.Windows.Forms.TrackBar();
            this.label11 = new System.Windows.Forms.Label();
            this.lblDELAY = new System.Windows.Forms.Label();
            this.trbDELAY_ECHO = new System.Windows.Forms.TrackBar();
            this.chkECHO = new System.Windows.Forms.CheckBox();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.comboBox2 = new System.Windows.Forms.ComboBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.button7 = new System.Windows.Forms.Button();
            this.label18 = new System.Windows.Forms.Label();
            this.cbFFTSIZE = new System.Windows.Forms.ComboBox();
            this.button4 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.trbOVERLAP = new System.Windows.Forms.TrackBar();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.trbPITCH = new System.Windows.Forms.TrackBar();
            this.chkPITCHSHIFT = new System.Windows.Forms.CheckBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.button14 = new System.Windows.Forms.Button();
            this.label31 = new System.Windows.Forms.Label();
            this.lblDRYMIX_FLANGE = new System.Windows.Forms.Label();
            this.trbDRYMIX_FLANGE = new System.Windows.Forms.TrackBar();
            this.button5 = new System.Windows.Forms.Button();
            this.button8 = new System.Windows.Forms.Button();
            this.button9 = new System.Windows.Forms.Button();
            this.label14 = new System.Windows.Forms.Label();
            this.lblWETMIX_FLANGE = new System.Windows.Forms.Label();
            this.trbWETMIX_FLANGE = new System.Windows.Forms.TrackBar();
            this.label19 = new System.Windows.Forms.Label();
            this.lblRATE_FLANGE = new System.Windows.Forms.Label();
            this.trbRATE_FLANGE = new System.Windows.Forms.TrackBar();
            this.label21 = new System.Windows.Forms.Label();
            this.lblDEPTH_FLANGE = new System.Windows.Forms.Label();
            this.trbDEPTH_FLANGE = new System.Windows.Forms.TrackBar();
            this.chkFLANGE = new System.Windows.Forms.CheckBox();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.button13 = new System.Windows.Forms.Button();
            this.button10 = new System.Windows.Forms.Button();
            this.button11 = new System.Windows.Forms.Button();
            this.label29 = new System.Windows.Forms.Label();
            this.lblGAINMAKEUP = new System.Windows.Forms.Label();
            this.button12 = new System.Windows.Forms.Button();
            this.label23 = new System.Windows.Forms.Label();
            this.trbGAINMAKEUP = new System.Windows.Forms.TrackBar();
            this.lblRELEASE = new System.Windows.Forms.Label();
            this.trbRELEASE = new System.Windows.Forms.TrackBar();
            this.label25 = new System.Windows.Forms.Label();
            this.lblATTACK = new System.Windows.Forms.Label();
            this.trbATTACK = new System.Windows.Forms.TrackBar();
            this.label27 = new System.Windows.Forms.Label();
            this.lblTHRESHOLD = new System.Windows.Forms.Label();
            this.trbTHRESHOLD = new System.Windows.Forms.TrackBar();
            this.chkCOMPRESSOR = new System.Windows.Forms.CheckBox();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.button17 = new System.Windows.Forms.Button();
            this.button18 = new System.Windows.Forms.Button();
            this.label37 = new System.Windows.Forms.Label();
            this.lblRESONANCE = new System.Windows.Forms.Label();
            this.trbRESONANCE = new System.Windows.Forms.TrackBar();
            this.label39 = new System.Windows.Forms.Label();
            this.lblCUTOFF = new System.Windows.Forms.Label();
            this.trbCUTOFF = new System.Windows.Forms.TrackBar();
            this.chkLOWPASS = new System.Windows.Forms.CheckBox();
            this.trbGain = new System.Windows.Forms.TrackBar();
            this.label1 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.button16 = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.lblDISTORTION = new System.Windows.Forms.Label();
            this.trbDISTORTION = new System.Windows.Forms.TrackBar();
            this.chkDISTORTION = new System.Windows.Forms.CheckBox();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.button25 = new System.Windows.Forms.Button();
            this.label22 = new System.Windows.Forms.Label();
            this.lblFEEDBACK_CHORUS = new System.Windows.Forms.Label();
            this.trbFEEDBACK_CHORUS = new System.Windows.Forms.TrackBar();
            this.button22 = new System.Windows.Forms.Button();
            this.button23 = new System.Windows.Forms.Button();
            this.button24 = new System.Windows.Forms.Button();
            this.button15 = new System.Windows.Forms.Button();
            this.button19 = new System.Windows.Forms.Button();
            this.button20 = new System.Windows.Forms.Button();
            this.button21 = new System.Windows.Forms.Button();
            this.lblWETMIX3_CHORUS = new System.Windows.Forms.Label();
            this.lblWETMIX2_CHORUS = new System.Windows.Forms.Label();
            this.label43 = new System.Windows.Forms.Label();
            this.trbWETMIX3_CHORUS = new System.Windows.Forms.TrackBar();
            this.label44 = new System.Windows.Forms.Label();
            this.trbWETMIX2_CHORUS = new System.Windows.Forms.TrackBar();
            this.label38 = new System.Windows.Forms.Label();
            this.lblWETMIX1_CHORUS = new System.Windows.Forms.Label();
            this.trbWETMIX1_CHORUS = new System.Windows.Forms.TrackBar();
            this.label41 = new System.Windows.Forms.Label();
            this.lblDRYMIX_CHORUS = new System.Windows.Forms.Label();
            this.trbDRYMIX_CHORUS = new System.Windows.Forms.TrackBar();
            this.label24 = new System.Windows.Forms.Label();
            this.lblDEPTH_CHORUS = new System.Windows.Forms.Label();
            this.trbDEPTH_CHORUS = new System.Windows.Forms.TrackBar();
            this.label30 = new System.Windows.Forms.Label();
            this.lblRATE_CHORUS = new System.Windows.Forms.Label();
            this.trbRATE_CHORUS = new System.Windows.Forms.TrackBar();
            this.label34 = new System.Windows.Forms.Label();
            this.lblDELAY_CHORUS = new System.Windows.Forms.Label();
            this.trbDELAY_CHORUS = new System.Windows.Forms.TrackBar();
            this.chkCHORUS = new System.Windows.Forms.CheckBox();
            this.btnWhaWha = new System.Windows.Forms.Button();
            this.trbWhaWhaSpeed = new System.Windows.Forms.TrackBar();
            this.label15 = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.label20 = new System.Windows.Forms.Label();
            this.trbWhaWhaDelay = new System.Windows.Forms.TrackBar();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnDSPRemove = new System.Windows.Forms.Button();
            this.btnDSPRegister = new System.Windows.Forms.Button();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this.button26 = new System.Windows.Forms.Button();
            this.label26 = new System.Windows.Forms.Label();
            this.lblSHAPE = new System.Windows.Forms.Label();
            this.trbSHAPE = new System.Windows.Forms.TrackBar();
            this.button27 = new System.Windows.Forms.Button();
            this.button28 = new System.Windows.Forms.Button();
            this.button29 = new System.Windows.Forms.Button();
            this.button30 = new System.Windows.Forms.Button();
            this.button31 = new System.Windows.Forms.Button();
            this.button32 = new System.Windows.Forms.Button();
            this.button33 = new System.Windows.Forms.Button();
            this.lblSPREAD = new System.Windows.Forms.Label();
            this.lblPHASE = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.trbSPREAD = new System.Windows.Forms.TrackBar();
            this.label40 = new System.Windows.Forms.Label();
            this.trbPHASE = new System.Windows.Forms.TrackBar();
            this.label42 = new System.Windows.Forms.Label();
            this.lblSQUARE = new System.Windows.Forms.Label();
            this.trbSQUARE = new System.Windows.Forms.TrackBar();
            this.label46 = new System.Windows.Forms.Label();
            this.lblDUTY = new System.Windows.Forms.Label();
            this.trbDUTY = new System.Windows.Forms.TrackBar();
            this.label48 = new System.Windows.Forms.Label();
            this.lblSKEW = new System.Windows.Forms.Label();
            this.trbSKEW = new System.Windows.Forms.TrackBar();
            this.label50 = new System.Windows.Forms.Label();
            this.lblDEPTH_TREMOLO = new System.Windows.Forms.Label();
            this.trbDEPTH_TREMOLO = new System.Windows.Forms.TrackBar();
            this.label52 = new System.Windows.Forms.Label();
            this.lblFREQUENCY = new System.Windows.Forms.Label();
            this.trbFREQUENCY = new System.Windows.Forms.TrackBar();
            this.chkTREMOLO = new System.Windows.Forms.CheckBox();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.cbTYPE_OSCILLATOR = new System.Windows.Forms.ComboBox();
            this.tbRATE_OSCILLATOR = new System.Windows.Forms.TrackBar();
            this.numRATE_OSCILLATOR = new System.Windows.Forms.NumericUpDown();
            this.panel2 = new System.Windows.Forms.Panel();
            this.chkHIGHPASS = new System.Windows.Forms.CheckBox();
            this.groupBox11 = new System.Windows.Forms.GroupBox();
            this.button39 = new System.Windows.Forms.Button();
            this.label49 = new System.Windows.Forms.Label();
            this.button40 = new System.Windows.Forms.Button();
            this.trbCUTOFF_HIGHPASS = new System.Windows.Forms.TrackBar();
            this.label45 = new System.Windows.Forms.Label();
            this.lblCUTOFF_HIGHPASS = new System.Windows.Forms.Label();
            this.lblRESONANCE_HIGHPASS = new System.Windows.Forms.Label();
            this.trbRESONANCE_HIGHPASS = new System.Windows.Forms.TrackBar();
            this.chkOSCILLATOR = new System.Windows.Forms.CheckBox();
            this.groupBox10 = new System.Windows.Forms.GroupBox();
            this.button35 = new System.Windows.Forms.Button();
            this.button34 = new System.Windows.Forms.Button();
            this.label33 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.rbFMOD_System = new System.Windows.Forms.RadioButton();
            this.rbFMOD_Channel = new System.Windows.Forms.RadioButton();
            this.label35 = new System.Windows.Forms.Label();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.파일FToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.새프리셋ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.프로파일열기OToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.프로파일저장SToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.닫기XToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.설정TToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.프리셋경로재설정ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.프리셋열기경로재설정ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.프리셋저장경로재설정ToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.프리셋폴더경로재설정ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.자동DSP등록ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.정렬ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripComboBox1 = new System.Windows.Forms.ToolStripComboBox();
            this.폴더선택ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.새로고침ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.제거ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trbMAXAMP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trbTHRESHHOLD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trbFADETIME)).BeginInit();
            this.statusStrip1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trbWETMIX_ECHO)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trbDECAYRATIO)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trbDELAY_ECHO)).BeginInit();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trbOVERLAP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trbPITCH)).BeginInit();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trbDRYMIX_FLANGE)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trbWETMIX_FLANGE)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trbRATE_FLANGE)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trbDEPTH_FLANGE)).BeginInit();
            this.groupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trbGAINMAKEUP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trbRELEASE)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trbATTACK)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trbTHRESHOLD)).BeginInit();
            this.groupBox6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trbRESONANCE)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trbCUTOFF)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trbGain)).BeginInit();
            this.groupBox7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trbDISTORTION)).BeginInit();
            this.groupBox8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trbFEEDBACK_CHORUS)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trbWETMIX3_CHORUS)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trbWETMIX2_CHORUS)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trbWETMIX1_CHORUS)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trbDRYMIX_CHORUS)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trbDEPTH_CHORUS)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trbRATE_CHORUS)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trbDELAY_CHORUS)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trbWhaWhaSpeed)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trbWhaWhaDelay)).BeginInit();
            this.panel1.SuspendLayout();
            this.groupBox9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trbSHAPE)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trbSPREAD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trbPHASE)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trbSQUARE)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trbDUTY)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trbSKEW)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trbDEPTH_TREMOLO)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trbFREQUENCY)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbRATE_OSCILLATOR)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numRATE_OSCILLATOR)).BeginInit();
            this.panel2.SuspendLayout();
            this.groupBox11.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trbCUTOFF_HIGHPASS)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trbRESONANCE_HIGHPASS)).BeginInit();
            this.groupBox10.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.button38);
            this.groupBox1.Controls.Add(this.button37);
            this.groupBox1.Controls.Add(this.button36);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.lblMAXAMP);
            this.groupBox1.Controls.Add(this.trbMAXAMP);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.lblTHRESHHOLD);
            this.groupBox1.Controls.Add(this.trbTHRESHHOLD);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.lblFADETIME);
            this.groupBox1.Controls.Add(this.trbFADETIME);
            this.groupBox1.Enabled = false;
            this.groupBox1.ForeColor = System.Drawing.Color.Blue;
            this.groupBox1.Location = new System.Drawing.Point(465, 617);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupBox1.Size = new System.Drawing.Size(458, 114);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "노멀라이즈";
            // 
            // button38
            // 
            this.button38.ForeColor = System.Drawing.Color.Black;
            this.button38.Location = new System.Drawing.Point(391, 83);
            this.button38.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.button38.Name = "button38";
            this.button38.Size = new System.Drawing.Size(56, 22);
            this.button38.TabIndex = 35;
            this.button38.Text = "기본값";
            this.button38.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.button38.UseVisualStyleBackColor = true;
            this.button38.Click += new System.EventHandler(this.button38_Click);
            // 
            // button37
            // 
            this.button37.ForeColor = System.Drawing.Color.Black;
            this.button37.Location = new System.Drawing.Point(391, 46);
            this.button37.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.button37.Name = "button37";
            this.button37.Size = new System.Drawing.Size(56, 22);
            this.button37.TabIndex = 34;
            this.button37.Text = "기본값";
            this.button37.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.button37.UseVisualStyleBackColor = true;
            this.button37.Click += new System.EventHandler(this.button37_Click);
            // 
            // button36
            // 
            this.button36.ForeColor = System.Drawing.Color.Black;
            this.button36.Location = new System.Drawing.Point(391, 12);
            this.button36.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.button36.Name = "button36";
            this.button36.Size = new System.Drawing.Size(56, 22);
            this.button36.TabIndex = 33;
            this.button36.Text = "기본값";
            this.button36.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.button36.UseVisualStyleBackColor = true;
            this.button36.Click += new System.EventHandler(this.button36_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(9, 88);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(59, 12);
            this.label5.TabIndex = 9;
            this.label5.Text = "MAXAMP";
            // 
            // lblMAXAMP
            // 
            this.lblMAXAMP.AutoSize = true;
            this.lblMAXAMP.ForeColor = System.Drawing.Color.Black;
            this.lblMAXAMP.Location = new System.Drawing.Point(348, 88);
            this.lblMAXAMP.Name = "lblMAXAMP";
            this.lblMAXAMP.Size = new System.Drawing.Size(17, 12);
            this.lblMAXAMP.TabIndex = 8;
            this.lblMAXAMP.Text = "20";
            // 
            // trbMAXAMP
            // 
            this.trbMAXAMP.AutoSize = false;
            this.trbMAXAMP.Location = new System.Drawing.Point(99, 78);
            this.trbMAXAMP.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.trbMAXAMP.Maximum = 100000;
            this.trbMAXAMP.Name = "trbMAXAMP";
            this.trbMAXAMP.Size = new System.Drawing.Size(245, 31);
            this.trbMAXAMP.TabIndex = 7;
            this.trbMAXAMP.Value = 20;
            this.trbMAXAMP.ValueChanged += new System.EventHandler(this.trackBar3_ValueChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(8, 53);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(85, 12);
            this.label3.TabIndex = 6;
            this.label3.Text = "THRESHHOLD";
            // 
            // lblTHRESHHOLD
            // 
            this.lblTHRESHHOLD.AutoSize = true;
            this.lblTHRESHHOLD.ForeColor = System.Drawing.Color.Black;
            this.lblTHRESHHOLD.Location = new System.Drawing.Point(348, 53);
            this.lblTHRESHHOLD.Name = "lblTHRESHHOLD";
            this.lblTHRESHHOLD.Size = new System.Drawing.Size(23, 12);
            this.lblTHRESHHOLD.TabIndex = 5;
            this.lblTHRESHHOLD.Text = "100";
            // 
            // trbTHRESHHOLD
            // 
            this.trbTHRESHHOLD.AutoSize = false;
            this.trbTHRESHHOLD.Location = new System.Drawing.Point(97, 46);
            this.trbTHRESHHOLD.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.trbTHRESHHOLD.Maximum = 10000;
            this.trbTHRESHHOLD.Name = "trbTHRESHHOLD";
            this.trbTHRESHHOLD.Size = new System.Drawing.Size(247, 26);
            this.trbTHRESHHOLD.TabIndex = 4;
            this.trbTHRESHHOLD.Value = 1000;
            this.trbTHRESHHOLD.ValueChanged += new System.EventHandler(this.trackBar2_ValueChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(8, 17);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(66, 12);
            this.label2.TabIndex = 3;
            this.label2.Text = "FADETIME";
            // 
            // lblFADETIME
            // 
            this.lblFADETIME.AutoSize = true;
            this.lblFADETIME.ForeColor = System.Drawing.Color.Black;
            this.lblFADETIME.Location = new System.Drawing.Point(348, 18);
            this.lblFADETIME.Name = "lblFADETIME";
            this.lblFADETIME.Size = new System.Drawing.Size(29, 12);
            this.lblFADETIME.TabIndex = 2;
            this.lblFADETIME.Text = "5000";
            // 
            // trbFADETIME
            // 
            this.trbFADETIME.AutoSize = false;
            this.trbFADETIME.Location = new System.Drawing.Point(97, 10);
            this.trbFADETIME.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.trbFADETIME.Maximum = 20000;
            this.trbFADETIME.Name = "trbFADETIME";
            this.trbFADETIME.Size = new System.Drawing.Size(245, 28);
            this.trbFADETIME.TabIndex = 1;
            this.trbFADETIME.Value = 5000;
            this.trbFADETIME.ValueChanged += new System.EventHandler(this.trackBar1_ValueChanged);
            // 
            // chkNOMALIZER
            // 
            this.chkNOMALIZER.AutoSize = true;
            this.chkNOMALIZER.Location = new System.Drawing.Point(465, 592);
            this.chkNOMALIZER.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.chkNOMALIZER.Name = "chkNOMALIZER";
            this.chkNOMALIZER.Size = new System.Drawing.Size(112, 16);
            this.chkNOMALIZER.TabIndex = 0;
            this.chkNOMALIZER.Text = "노멀라이즈 사용";
            this.chkNOMALIZER.UseVisualStyleBackColor = true;
            this.chkNOMALIZER.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
            // 
            // statusStrip1
            // 
            this.statusStrip1.AutoSize = false;
            this.statusStrip1.BackColor = System.Drawing.SystemColors.Control;
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1,
            this.toolStripStatusLabel2});
            this.statusStrip1.Location = new System.Drawing.Point(0, 726);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional;
            this.statusStrip1.Size = new System.Drawing.Size(966, 20);
            this.statusStrip1.TabIndex = 1;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(67, 15);
            this.toolStripStatusLabel1.Text = "내부 상태:";
            // 
            // toolStripStatusLabel2
            // 
            this.toolStripStatusLabel2.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.toolStripStatusLabel2.ForeColor = System.Drawing.Color.Blue;
            this.toolStripStatusLabel2.Name = "toolStripStatusLabel2";
            this.toolStripStatusLabel2.Size = new System.Drawing.Size(24, 15);
            this.toolStripStatusLabel2.Text = "OK";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.button3);
            this.groupBox2.Controls.Add(this.button2);
            this.groupBox2.Controls.Add(this.button1);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.lblWETMIX_ECHO);
            this.groupBox2.Controls.Add(this.trbWETMIX_ECHO);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Controls.Add(this.lblDECAYRATIO);
            this.groupBox2.Controls.Add(this.trbDECAYRATIO);
            this.groupBox2.Controls.Add(this.label11);
            this.groupBox2.Controls.Add(this.lblDELAY);
            this.groupBox2.Controls.Add(this.trbDELAY_ECHO);
            this.groupBox2.Enabled = false;
            this.groupBox2.ForeColor = System.Drawing.Color.Blue;
            this.groupBox2.Location = new System.Drawing.Point(3, 24);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupBox2.Size = new System.Drawing.Size(455, 108);
            this.groupBox2.TabIndex = 10;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "에코";
            // 
            // button3
            // 
            this.button3.ForeColor = System.Drawing.Color.Black;
            this.button3.Location = new System.Drawing.Point(391, 78);
            this.button3.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(57, 22);
            this.button3.TabIndex = 12;
            this.button3.Text = "기본값";
            this.button3.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button2
            // 
            this.button2.ForeColor = System.Drawing.Color.Black;
            this.button2.Location = new System.Drawing.Point(391, 43);
            this.button2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(57, 22);
            this.button2.TabIndex = 11;
            this.button2.Text = "기본값";
            this.button2.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button1
            // 
            this.button1.ForeColor = System.Drawing.Color.Black;
            this.button1.Location = new System.Drawing.Point(391, 12);
            this.button1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(57, 22);
            this.button1.TabIndex = 10;
            this.button1.Text = "기본값";
            this.button1.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.ForeColor = System.Drawing.Color.Black;
            this.label7.Location = new System.Drawing.Point(8, 80);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(53, 12);
            this.label7.TabIndex = 9;
            this.label7.Text = "WETMIX";
            // 
            // lblWETMIX_ECHO
            // 
            this.lblWETMIX_ECHO.AutoSize = true;
            this.lblWETMIX_ECHO.ForeColor = System.Drawing.Color.Black;
            this.lblWETMIX_ECHO.Location = new System.Drawing.Point(348, 80);
            this.lblWETMIX_ECHO.Name = "lblWETMIX_ECHO";
            this.lblWETMIX_ECHO.Size = new System.Drawing.Size(29, 12);
            this.lblWETMIX_ECHO.TabIndex = 8;
            this.lblWETMIX_ECHO.Text = "1000";
            // 
            // trbWETMIX_ECHO
            // 
            this.trbWETMIX_ECHO.AutoSize = false;
            this.trbWETMIX_ECHO.Location = new System.Drawing.Point(97, 76);
            this.trbWETMIX_ECHO.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.trbWETMIX_ECHO.Maximum = 1000;
            this.trbWETMIX_ECHO.Name = "trbWETMIX_ECHO";
            this.trbWETMIX_ECHO.Size = new System.Drawing.Size(245, 29);
            this.trbWETMIX_ECHO.TabIndex = 7;
            this.trbWETMIX_ECHO.Value = 1000;
            this.trbWETMIX_ECHO.ValueChanged += new System.EventHandler(this.trbWETMIX_ECHO_ValueChanged);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.ForeColor = System.Drawing.Color.Black;
            this.label9.Location = new System.Drawing.Point(8, 48);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(82, 12);
            this.label9.TabIndex = 6;
            this.label9.Text = "DECAYRATIO";
            // 
            // lblDECAYRATIO
            // 
            this.lblDECAYRATIO.AutoSize = true;
            this.lblDECAYRATIO.ForeColor = System.Drawing.Color.Black;
            this.lblDECAYRATIO.Location = new System.Drawing.Point(348, 49);
            this.lblDECAYRATIO.Name = "lblDECAYRATIO";
            this.lblDECAYRATIO.Size = new System.Drawing.Size(21, 12);
            this.lblDECAYRATIO.TabIndex = 5;
            this.lblDECAYRATIO.Text = "0.5";
            // 
            // trbDECAYRATIO
            // 
            this.trbDECAYRATIO.AutoSize = false;
            this.trbDECAYRATIO.Location = new System.Drawing.Point(97, 42);
            this.trbDECAYRATIO.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.trbDECAYRATIO.Maximum = 1000;
            this.trbDECAYRATIO.Name = "trbDECAYRATIO";
            this.trbDECAYRATIO.Size = new System.Drawing.Size(245, 28);
            this.trbDECAYRATIO.TabIndex = 4;
            this.trbDECAYRATIO.Value = 500;
            this.trbDECAYRATIO.ValueChanged += new System.EventHandler(this.trbDECAYRATIO_ValueChanged);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.ForeColor = System.Drawing.Color.Black;
            this.label11.Location = new System.Drawing.Point(10, 18);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(44, 12);
            this.label11.TabIndex = 3;
            this.label11.Text = "DELAY";
            // 
            // lblDELAY
            // 
            this.lblDELAY.AutoSize = true;
            this.lblDELAY.ForeColor = System.Drawing.Color.Black;
            this.lblDELAY.Location = new System.Drawing.Point(348, 17);
            this.lblDELAY.Name = "lblDELAY";
            this.lblDELAY.Size = new System.Drawing.Size(23, 12);
            this.lblDELAY.TabIndex = 2;
            this.lblDELAY.Text = "500";
            // 
            // trbDELAY_ECHO
            // 
            this.trbDELAY_ECHO.AutoSize = false;
            this.trbDELAY_ECHO.Location = new System.Drawing.Point(97, 10);
            this.trbDELAY_ECHO.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.trbDELAY_ECHO.Maximum = 5000;
            this.trbDELAY_ECHO.Name = "trbDELAY_ECHO";
            this.trbDELAY_ECHO.Size = new System.Drawing.Size(245, 26);
            this.trbDELAY_ECHO.TabIndex = 1;
            this.trbDELAY_ECHO.Value = 500;
            this.trbDELAY_ECHO.ValueChanged += new System.EventHandler(this.trbDELAY_ValueChanged);
            // 
            // chkECHO
            // 
            this.chkECHO.AutoSize = true;
            this.chkECHO.Location = new System.Drawing.Point(6, 2);
            this.chkECHO.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.chkECHO.Name = "chkECHO";
            this.chkECHO.Size = new System.Drawing.Size(76, 16);
            this.chkECHO.TabIndex = 0;
            this.chkECHO.Text = "에코 사용";
            this.chkECHO.UseVisualStyleBackColor = true;
            this.chkECHO.CheckedChanged += new System.EventHandler(this.checkBox2_CheckedChanged);
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(156, 433);
            this.comboBox1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(121, 20);
            this.comboBox1.TabIndex = 11;
            this.comboBox1.Visible = false;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(108, 438);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(41, 12);
            this.label8.TabIndex = 12;
            this.label8.Text = "프리셋";
            this.label8.Visible = false;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(86, 6);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(41, 12);
            this.label10.TabIndex = 15;
            this.label10.Text = "프리셋";
            this.label10.Visible = false;
            // 
            // comboBox2
            // 
            this.comboBox2.FormattingEnabled = true;
            this.comboBox2.Location = new System.Drawing.Point(127, 2);
            this.comboBox2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.comboBox2.Name = "comboBox2";
            this.comboBox2.Size = new System.Drawing.Size(121, 20);
            this.comboBox2.TabIndex = 14;
            this.comboBox2.Visible = false;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.button7);
            this.groupBox3.Controls.Add(this.label18);
            this.groupBox3.Controls.Add(this.cbFFTSIZE);
            this.groupBox3.Controls.Add(this.button4);
            this.groupBox3.Controls.Add(this.button6);
            this.groupBox3.Controls.Add(this.label12);
            this.groupBox3.Controls.Add(this.label13);
            this.groupBox3.Controls.Add(this.trbOVERLAP);
            this.groupBox3.Controls.Add(this.label16);
            this.groupBox3.Controls.Add(this.label17);
            this.groupBox3.Controls.Add(this.trbPITCH);
            this.groupBox3.Enabled = false;
            this.groupBox3.ForeColor = System.Drawing.Color.Blue;
            this.groupBox3.Location = new System.Drawing.Point(3, 158);
            this.groupBox3.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupBox3.Size = new System.Drawing.Size(455, 108);
            this.groupBox3.TabIndex = 13;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "피치 시프트";
            // 
            // button7
            // 
            this.button7.ForeColor = System.Drawing.Color.Black;
            this.button7.Location = new System.Drawing.Point(391, 44);
            this.button7.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(57, 22);
            this.button7.TabIndex = 15;
            this.button7.Text = "기본값";
            this.button7.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.button7.UseVisualStyleBackColor = true;
            this.button7.Click += new System.EventHandler(this.button7_Click);
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.ForeColor = System.Drawing.Color.Black;
            this.label18.Location = new System.Drawing.Point(5, 49);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(54, 12);
            this.label18.TabIndex = 14;
            this.label18.Text = "FFTSIZE";
            // 
            // cbFFTSIZE
            // 
            this.cbFFTSIZE.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbFFTSIZE.FormattingEnabled = true;
            this.cbFFTSIZE.Items.AddRange(new object[] {
            "256",
            "512",
            "1024",
            "2048",
            "4096"});
            this.cbFFTSIZE.Location = new System.Drawing.Point(73, 44);
            this.cbFFTSIZE.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.cbFFTSIZE.Name = "cbFFTSIZE";
            this.cbFFTSIZE.Size = new System.Drawing.Size(266, 20);
            this.cbFFTSIZE.TabIndex = 13;
            this.cbFFTSIZE.SelectedIndexChanged += new System.EventHandler(this.cbFFTSIZE_SelectedIndexChanged);
            // 
            // button4
            // 
            this.button4.ForeColor = System.Drawing.Color.Black;
            this.button4.Location = new System.Drawing.Point(391, 77);
            this.button4.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(57, 22);
            this.button4.TabIndex = 12;
            this.button4.Text = "기본값";
            this.button4.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // button6
            // 
            this.button6.ForeColor = System.Drawing.Color.Black;
            this.button6.Location = new System.Drawing.Point(391, 16);
            this.button6.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(57, 22);
            this.button6.TabIndex = 10;
            this.button6.Text = "기본값";
            this.button6.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.ForeColor = System.Drawing.Color.Black;
            this.label12.Location = new System.Drawing.Point(5, 79);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(61, 12);
            this.label12.TabIndex = 9;
            this.label12.Text = "OVERLAP";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.ForeColor = System.Drawing.Color.Black;
            this.label13.Location = new System.Drawing.Point(348, 79);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(11, 12);
            this.label13.TabIndex = 8;
            this.label13.Text = "4";
            // 
            // trbOVERLAP
            // 
            this.trbOVERLAP.AutoSize = false;
            this.trbOVERLAP.Location = new System.Drawing.Point(73, 68);
            this.trbOVERLAP.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.trbOVERLAP.Maximum = 32;
            this.trbOVERLAP.Minimum = 1;
            this.trbOVERLAP.Name = "trbOVERLAP";
            this.trbOVERLAP.Size = new System.Drawing.Size(267, 31);
            this.trbOVERLAP.TabIndex = 7;
            this.trbOVERLAP.Value = 4;
            this.trbOVERLAP.ValueChanged += new System.EventHandler(this.trackBar4_ValueChanged);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.ForeColor = System.Drawing.Color.Black;
            this.label16.Location = new System.Drawing.Point(6, 18);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(41, 12);
            this.label16.TabIndex = 3;
            this.label16.Text = "PITCH";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.ForeColor = System.Drawing.Color.Black;
            this.label17.Location = new System.Drawing.Point(346, 19);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(29, 12);
            this.label17.TabIndex = 2;
            this.label17.Text = "1000";
            // 
            // trbPITCH
            // 
            this.trbPITCH.AutoSize = false;
            this.trbPITCH.Location = new System.Drawing.Point(75, 10);
            this.trbPITCH.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.trbPITCH.Maximum = 2000;
            this.trbPITCH.Minimum = 500;
            this.trbPITCH.Name = "trbPITCH";
            this.trbPITCH.Size = new System.Drawing.Size(267, 29);
            this.trbPITCH.TabIndex = 1;
            this.trbPITCH.Value = 1000;
            this.trbPITCH.ValueChanged += new System.EventHandler(this.trbPITCH_ValueChanged);
            // 
            // chkPITCHSHIFT
            // 
            this.chkPITCHSHIFT.AutoSize = true;
            this.chkPITCHSHIFT.Location = new System.Drawing.Point(3, 138);
            this.chkPITCHSHIFT.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.chkPITCHSHIFT.Name = "chkPITCHSHIFT";
            this.chkPITCHSHIFT.Size = new System.Drawing.Size(116, 16);
            this.chkPITCHSHIFT.TabIndex = 16;
            this.chkPITCHSHIFT.Text = "피치 시프트 사용";
            this.chkPITCHSHIFT.UseVisualStyleBackColor = true;
            this.chkPITCHSHIFT.CheckedChanged += new System.EventHandler(this.checkBox3_CheckedChanged);
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.button14);
            this.groupBox4.Controls.Add(this.label31);
            this.groupBox4.Controls.Add(this.lblDRYMIX_FLANGE);
            this.groupBox4.Controls.Add(this.trbDRYMIX_FLANGE);
            this.groupBox4.Controls.Add(this.button5);
            this.groupBox4.Controls.Add(this.button8);
            this.groupBox4.Controls.Add(this.button9);
            this.groupBox4.Controls.Add(this.label14);
            this.groupBox4.Controls.Add(this.lblWETMIX_FLANGE);
            this.groupBox4.Controls.Add(this.trbWETMIX_FLANGE);
            this.groupBox4.Controls.Add(this.label19);
            this.groupBox4.Controls.Add(this.lblRATE_FLANGE);
            this.groupBox4.Controls.Add(this.trbRATE_FLANGE);
            this.groupBox4.Controls.Add(this.label21);
            this.groupBox4.Controls.Add(this.lblDEPTH_FLANGE);
            this.groupBox4.Controls.Add(this.trbDEPTH_FLANGE);
            this.groupBox4.Enabled = false;
            this.groupBox4.Location = new System.Drawing.Point(3, 289);
            this.groupBox4.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupBox4.Size = new System.Drawing.Size(455, 140);
            this.groupBox4.TabIndex = 18;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "플레인지";
            // 
            // button14
            // 
            this.button14.Location = new System.Drawing.Point(392, 112);
            this.button14.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.button14.Name = "button14";
            this.button14.Size = new System.Drawing.Size(57, 22);
            this.button14.TabIndex = 16;
            this.button14.Text = "기본값";
            this.button14.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.button14.UseVisualStyleBackColor = true;
            this.button14.Click += new System.EventHandler(this.button14_Click);
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(8, 114);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(51, 12);
            this.label31.TabIndex = 15;
            this.label31.Text = "DRYMIX";
            // 
            // lblDRYMIX_FLANGE
            // 
            this.lblDRYMIX_FLANGE.AutoSize = true;
            this.lblDRYMIX_FLANGE.Location = new System.Drawing.Point(348, 114);
            this.lblDRYMIX_FLANGE.Name = "lblDRYMIX_FLANGE";
            this.lblDRYMIX_FLANGE.Size = new System.Drawing.Size(23, 12);
            this.lblDRYMIX_FLANGE.TabIndex = 14;
            this.lblDRYMIX_FLANGE.Text = "550";
            // 
            // trbDRYMIX_FLANGE
            // 
            this.trbDRYMIX_FLANGE.AutoSize = false;
            this.trbDRYMIX_FLANGE.Location = new System.Drawing.Point(65, 109);
            this.trbDRYMIX_FLANGE.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.trbDRYMIX_FLANGE.Maximum = 1000;
            this.trbDRYMIX_FLANGE.Name = "trbDRYMIX_FLANGE";
            this.trbDRYMIX_FLANGE.Size = new System.Drawing.Size(276, 28);
            this.trbDRYMIX_FLANGE.TabIndex = 13;
            this.trbDRYMIX_FLANGE.Value = 550;
            this.trbDRYMIX_FLANGE.ValueChanged += new System.EventHandler(this.trbDRYMIX_FLANGE_ValueChanged);
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(392, 78);
            this.button5.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(57, 22);
            this.button5.TabIndex = 12;
            this.button5.Text = "기본값";
            this.button5.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // button8
            // 
            this.button8.Location = new System.Drawing.Point(392, 43);
            this.button8.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(57, 22);
            this.button8.TabIndex = 11;
            this.button8.Text = "기본값";
            this.button8.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.button8.UseVisualStyleBackColor = true;
            this.button8.Click += new System.EventHandler(this.button8_Click);
            // 
            // button9
            // 
            this.button9.Location = new System.Drawing.Point(392, 12);
            this.button9.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(57, 22);
            this.button9.TabIndex = 10;
            this.button9.Text = "기본값";
            this.button9.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.button9.UseVisualStyleBackColor = true;
            this.button9.Click += new System.EventHandler(this.button9_Click);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(8, 80);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(53, 12);
            this.label14.TabIndex = 9;
            this.label14.Text = "WETMIX";
            // 
            // lblWETMIX_FLANGE
            // 
            this.lblWETMIX_FLANGE.AutoSize = true;
            this.lblWETMIX_FLANGE.Location = new System.Drawing.Point(348, 80);
            this.lblWETMIX_FLANGE.Name = "lblWETMIX_FLANGE";
            this.lblWETMIX_FLANGE.Size = new System.Drawing.Size(23, 12);
            this.lblWETMIX_FLANGE.TabIndex = 8;
            this.lblWETMIX_FLANGE.Text = "450";
            // 
            // trbWETMIX_FLANGE
            // 
            this.trbWETMIX_FLANGE.AutoSize = false;
            this.trbWETMIX_FLANGE.Location = new System.Drawing.Point(65, 76);
            this.trbWETMIX_FLANGE.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.trbWETMIX_FLANGE.Maximum = 1000;
            this.trbWETMIX_FLANGE.Name = "trbWETMIX_FLANGE";
            this.trbWETMIX_FLANGE.Size = new System.Drawing.Size(276, 28);
            this.trbWETMIX_FLANGE.TabIndex = 7;
            this.trbWETMIX_FLANGE.Value = 450;
            this.trbWETMIX_FLANGE.ValueChanged += new System.EventHandler(this.trbWETMIX_FLANGE_ValueChanged);
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(8, 48);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(37, 12);
            this.label19.TabIndex = 6;
            this.label19.Text = "RATE";
            // 
            // lblRATE_FLANGE
            // 
            this.lblRATE_FLANGE.AutoSize = true;
            this.lblRATE_FLANGE.Location = new System.Drawing.Point(348, 49);
            this.lblRATE_FLANGE.Name = "lblRATE_FLANGE";
            this.lblRATE_FLANGE.Size = new System.Drawing.Size(23, 12);
            this.lblRATE_FLANGE.TabIndex = 5;
            this.lblRATE_FLANGE.Text = "100";
            // 
            // trbRATE_FLANGE
            // 
            this.trbRATE_FLANGE.AutoSize = false;
            this.trbRATE_FLANGE.Location = new System.Drawing.Point(65, 42);
            this.trbRATE_FLANGE.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.trbRATE_FLANGE.Maximum = 2000;
            this.trbRATE_FLANGE.Name = "trbRATE_FLANGE";
            this.trbRATE_FLANGE.Size = new System.Drawing.Size(276, 28);
            this.trbRATE_FLANGE.TabIndex = 4;
            this.trbRATE_FLANGE.Value = 100;
            this.trbRATE_FLANGE.ValueChanged += new System.EventHandler(this.trbRATE_ValueChanged);
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(8, 18);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(45, 12);
            this.label21.TabIndex = 3;
            this.label21.Text = "DEPTH";
            // 
            // lblDEPTH_FLANGE
            // 
            this.lblDEPTH_FLANGE.AutoSize = true;
            this.lblDEPTH_FLANGE.Location = new System.Drawing.Point(348, 17);
            this.lblDEPTH_FLANGE.Name = "lblDEPTH_FLANGE";
            this.lblDEPTH_FLANGE.Size = new System.Drawing.Size(29, 12);
            this.lblDEPTH_FLANGE.TabIndex = 2;
            this.lblDEPTH_FLANGE.Text = "1000";
            // 
            // trbDEPTH_FLANGE
            // 
            this.trbDEPTH_FLANGE.AutoSize = false;
            this.trbDEPTH_FLANGE.Location = new System.Drawing.Point(65, 10);
            this.trbDEPTH_FLANGE.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.trbDEPTH_FLANGE.Maximum = 1000;
            this.trbDEPTH_FLANGE.Name = "trbDEPTH_FLANGE";
            this.trbDEPTH_FLANGE.Size = new System.Drawing.Size(276, 28);
            this.trbDEPTH_FLANGE.TabIndex = 1;
            this.trbDEPTH_FLANGE.Value = 1000;
            this.trbDEPTH_FLANGE.ValueChanged += new System.EventHandler(this.trbDEPTH_ValueChanged);
            // 
            // chkFLANGE
            // 
            this.chkFLANGE.AutoSize = true;
            this.chkFLANGE.Location = new System.Drawing.Point(3, 270);
            this.chkFLANGE.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.chkFLANGE.Name = "chkFLANGE";
            this.chkFLANGE.Size = new System.Drawing.Size(100, 16);
            this.chkFLANGE.TabIndex = 17;
            this.chkFLANGE.Text = "플레인지 사용";
            this.chkFLANGE.UseVisualStyleBackColor = true;
            this.chkFLANGE.CheckedChanged += new System.EventHandler(this.checkBox4_CheckedChanged);
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.button13);
            this.groupBox5.Controls.Add(this.button10);
            this.groupBox5.Controls.Add(this.button11);
            this.groupBox5.Controls.Add(this.label29);
            this.groupBox5.Controls.Add(this.lblGAINMAKEUP);
            this.groupBox5.Controls.Add(this.button12);
            this.groupBox5.Controls.Add(this.label23);
            this.groupBox5.Controls.Add(this.trbGAINMAKEUP);
            this.groupBox5.Controls.Add(this.lblRELEASE);
            this.groupBox5.Controls.Add(this.trbRELEASE);
            this.groupBox5.Controls.Add(this.label25);
            this.groupBox5.Controls.Add(this.lblATTACK);
            this.groupBox5.Controls.Add(this.trbATTACK);
            this.groupBox5.Controls.Add(this.label27);
            this.groupBox5.Controls.Add(this.lblTHRESHOLD);
            this.groupBox5.Controls.Add(this.trbTHRESHOLD);
            this.groupBox5.Enabled = false;
            this.groupBox5.ForeColor = System.Drawing.Color.Blue;
            this.groupBox5.Location = new System.Drawing.Point(3, 635);
            this.groupBox5.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupBox5.Size = new System.Drawing.Size(455, 146);
            this.groupBox5.TabIndex = 20;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "컴프레셔";
            // 
            // button13
            // 
            this.button13.ForeColor = System.Drawing.Color.Black;
            this.button13.Location = new System.Drawing.Point(391, 17);
            this.button13.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.button13.Name = "button13";
            this.button13.Size = new System.Drawing.Size(57, 22);
            this.button13.TabIndex = 16;
            this.button13.Text = "기본값";
            this.button13.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.button13.UseVisualStyleBackColor = true;
            this.button13.Click += new System.EventHandler(this.button13_Click);
            // 
            // button10
            // 
            this.button10.ForeColor = System.Drawing.Color.Black;
            this.button10.Location = new System.Drawing.Point(391, 115);
            this.button10.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(57, 22);
            this.button10.TabIndex = 12;
            this.button10.Text = "기본값";
            this.button10.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.button10.UseVisualStyleBackColor = true;
            this.button10.Click += new System.EventHandler(this.button10_Click);
            // 
            // button11
            // 
            this.button11.ForeColor = System.Drawing.Color.Black;
            this.button11.Location = new System.Drawing.Point(391, 83);
            this.button11.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.button11.Name = "button11";
            this.button11.Size = new System.Drawing.Size(57, 22);
            this.button11.TabIndex = 11;
            this.button11.Text = "기본값";
            this.button11.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.button11.UseVisualStyleBackColor = true;
            this.button11.Click += new System.EventHandler(this.button11_Click);
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("굴림", 9.209303F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label29.ForeColor = System.Drawing.Color.Black;
            this.label29.Location = new System.Drawing.Point(8, 19);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(90, 13);
            this.label29.TabIndex = 15;
            this.label29.Text = "GAINMAKEUP";
            // 
            // lblGAINMAKEUP
            // 
            this.lblGAINMAKEUP.AutoSize = true;
            this.lblGAINMAKEUP.ForeColor = System.Drawing.Color.Black;
            this.lblGAINMAKEUP.Location = new System.Drawing.Point(352, 19);
            this.lblGAINMAKEUP.Name = "lblGAINMAKEUP";
            this.lblGAINMAKEUP.Size = new System.Drawing.Size(11, 12);
            this.lblGAINMAKEUP.TabIndex = 14;
            this.lblGAINMAKEUP.Text = "0";
            // 
            // button12
            // 
            this.button12.ForeColor = System.Drawing.Color.Black;
            this.button12.Location = new System.Drawing.Point(391, 50);
            this.button12.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.button12.Name = "button12";
            this.button12.Size = new System.Drawing.Size(57, 22);
            this.button12.TabIndex = 10;
            this.button12.Text = "기본값";
            this.button12.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.button12.UseVisualStyleBackColor = true;
            this.button12.Click += new System.EventHandler(this.button12_Click);
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.ForeColor = System.Drawing.Color.Black;
            this.label23.Location = new System.Drawing.Point(8, 119);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(60, 12);
            this.label23.TabIndex = 9;
            this.label23.Text = "RELEASE";
            // 
            // trbGAINMAKEUP
            // 
            this.trbGAINMAKEUP.AutoSize = false;
            this.trbGAINMAKEUP.Location = new System.Drawing.Point(100, 11);
            this.trbGAINMAKEUP.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.trbGAINMAKEUP.Maximum = 100;
            this.trbGAINMAKEUP.Name = "trbGAINMAKEUP";
            this.trbGAINMAKEUP.Size = new System.Drawing.Size(245, 29);
            this.trbGAINMAKEUP.TabIndex = 13;
            this.trbGAINMAKEUP.ValueChanged += new System.EventHandler(this.trackBar10_ValueChanged);
            // 
            // lblRELEASE
            // 
            this.lblRELEASE.AutoSize = true;
            this.lblRELEASE.ForeColor = System.Drawing.Color.Black;
            this.lblRELEASE.Location = new System.Drawing.Point(351, 122);
            this.lblRELEASE.Name = "lblRELEASE";
            this.lblRELEASE.Size = new System.Drawing.Size(17, 12);
            this.lblRELEASE.TabIndex = 8;
            this.lblRELEASE.Text = "50";
            // 
            // trbRELEASE
            // 
            this.trbRELEASE.AutoSize = false;
            this.trbRELEASE.Location = new System.Drawing.Point(100, 114);
            this.trbRELEASE.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.trbRELEASE.Maximum = 1000;
            this.trbRELEASE.Minimum = 1;
            this.trbRELEASE.Name = "trbRELEASE";
            this.trbRELEASE.Size = new System.Drawing.Size(245, 29);
            this.trbRELEASE.TabIndex = 7;
            this.trbRELEASE.Value = 50;
            this.trbRELEASE.ValueChanged += new System.EventHandler(this.trackBar7_ValueChanged);
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.ForeColor = System.Drawing.Color.Black;
            this.label25.Location = new System.Drawing.Point(8, 88);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(54, 12);
            this.label25.TabIndex = 6;
            this.label25.Text = "ATTACK";
            // 
            // lblATTACK
            // 
            this.lblATTACK.AutoSize = true;
            this.lblATTACK.ForeColor = System.Drawing.Color.Black;
            this.lblATTACK.Location = new System.Drawing.Point(351, 89);
            this.lblATTACK.Name = "lblATTACK";
            this.lblATTACK.Size = new System.Drawing.Size(17, 12);
            this.lblATTACK.TabIndex = 5;
            this.lblATTACK.Text = "50";
            // 
            // trbATTACK
            // 
            this.trbATTACK.AutoSize = false;
            this.trbATTACK.Location = new System.Drawing.Point(100, 80);
            this.trbATTACK.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.trbATTACK.Maximum = 200;
            this.trbATTACK.Minimum = 1;
            this.trbATTACK.Name = "trbATTACK";
            this.trbATTACK.Size = new System.Drawing.Size(245, 28);
            this.trbATTACK.TabIndex = 4;
            this.trbATTACK.Value = 50;
            this.trbATTACK.ValueChanged += new System.EventHandler(this.trackBar8_ValueChanged);
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("굴림", 9.209303F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label27.ForeColor = System.Drawing.Color.Black;
            this.label27.Location = new System.Drawing.Point(8, 54);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(85, 13);
            this.label27.TabIndex = 3;
            this.label27.Text = "THRESHOLD";
            // 
            // lblTHRESHOLD
            // 
            this.lblTHRESHOLD.AutoSize = true;
            this.lblTHRESHOLD.ForeColor = System.Drawing.Color.Black;
            this.lblTHRESHOLD.Location = new System.Drawing.Point(352, 54);
            this.lblTHRESHOLD.Name = "lblTHRESHOLD";
            this.lblTHRESHOLD.Size = new System.Drawing.Size(11, 12);
            this.lblTHRESHOLD.TabIndex = 2;
            this.lblTHRESHOLD.Text = "0";
            // 
            // trbTHRESHOLD
            // 
            this.trbTHRESHOLD.AutoSize = false;
            this.trbTHRESHOLD.Location = new System.Drawing.Point(100, 48);
            this.trbTHRESHOLD.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.trbTHRESHOLD.Maximum = 600;
            this.trbTHRESHOLD.Name = "trbTHRESHOLD";
            this.trbTHRESHOLD.Size = new System.Drawing.Size(245, 26);
            this.trbTHRESHOLD.TabIndex = 1;
            this.trbTHRESHOLD.ValueChanged += new System.EventHandler(this.trackBar9_ValueChanged);
            // 
            // chkCOMPRESSOR
            // 
            this.chkCOMPRESSOR.AutoSize = true;
            this.chkCOMPRESSOR.Location = new System.Drawing.Point(3, 617);
            this.chkCOMPRESSOR.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.chkCOMPRESSOR.Name = "chkCOMPRESSOR";
            this.chkCOMPRESSOR.Size = new System.Drawing.Size(100, 16);
            this.chkCOMPRESSOR.TabIndex = 19;
            this.chkCOMPRESSOR.Text = "컴프레셔 사용";
            this.chkCOMPRESSOR.UseVisualStyleBackColor = true;
            this.chkCOMPRESSOR.CheckedChanged += new System.EventHandler(this.checkBox5_CheckedChanged);
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.button17);
            this.groupBox6.Controls.Add(this.button18);
            this.groupBox6.Controls.Add(this.label37);
            this.groupBox6.Controls.Add(this.lblRESONANCE);
            this.groupBox6.Controls.Add(this.trbRESONANCE);
            this.groupBox6.Controls.Add(this.label39);
            this.groupBox6.Controls.Add(this.lblCUTOFF);
            this.groupBox6.Controls.Add(this.trbCUTOFF);
            this.groupBox6.Enabled = false;
            this.groupBox6.ForeColor = System.Drawing.Color.Blue;
            this.groupBox6.Location = new System.Drawing.Point(3, 454);
            this.groupBox6.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupBox6.Size = new System.Drawing.Size(455, 79);
            this.groupBox6.TabIndex = 20;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "로우패스";
            // 
            // button17
            // 
            this.button17.ForeColor = System.Drawing.Color.Black;
            this.button17.Location = new System.Drawing.Point(392, 48);
            this.button17.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.button17.Name = "button17";
            this.button17.Size = new System.Drawing.Size(57, 22);
            this.button17.TabIndex = 11;
            this.button17.Text = "기본값";
            this.button17.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.button17.UseVisualStyleBackColor = true;
            this.button17.Click += new System.EventHandler(this.button17_Click);
            // 
            // button18
            // 
            this.button18.ForeColor = System.Drawing.Color.Black;
            this.button18.Location = new System.Drawing.Point(392, 16);
            this.button18.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.button18.Name = "button18";
            this.button18.Size = new System.Drawing.Size(57, 22);
            this.button18.TabIndex = 10;
            this.button18.Text = "기본값";
            this.button18.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.button18.UseVisualStyleBackColor = true;
            this.button18.Click += new System.EventHandler(this.button18_Click);
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.ForeColor = System.Drawing.Color.Black;
            this.label37.Location = new System.Drawing.Point(8, 53);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(81, 12);
            this.label37.TabIndex = 6;
            this.label37.Text = "RESONANCE";
            // 
            // lblRESONANCE
            // 
            this.lblRESONANCE.AutoSize = true;
            this.lblRESONANCE.ForeColor = System.Drawing.Color.Black;
            this.lblRESONANCE.Location = new System.Drawing.Point(346, 54);
            this.lblRESONANCE.Name = "lblRESONANCE";
            this.lblRESONANCE.Size = new System.Drawing.Size(23, 12);
            this.lblRESONANCE.TabIndex = 5;
            this.lblRESONANCE.Text = "500";
            // 
            // trbRESONANCE
            // 
            this.trbRESONANCE.AutoSize = false;
            this.trbRESONANCE.Location = new System.Drawing.Point(94, 46);
            this.trbRESONANCE.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.trbRESONANCE.Maximum = 1270;
            this.trbRESONANCE.Name = "trbRESONANCE";
            this.trbRESONANCE.Size = new System.Drawing.Size(247, 28);
            this.trbRESONANCE.TabIndex = 4;
            this.trbRESONANCE.Value = 100;
            this.trbRESONANCE.ValueChanged += new System.EventHandler(this.trbRESONANCE_ValueChanged);
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.ForeColor = System.Drawing.Color.Black;
            this.label39.Location = new System.Drawing.Point(8, 19);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(53, 12);
            this.label39.TabIndex = 3;
            this.label39.Text = "CUTOFF";
            // 
            // lblCUTOFF
            // 
            this.lblCUTOFF.AutoSize = true;
            this.lblCUTOFF.ForeColor = System.Drawing.Color.Black;
            this.lblCUTOFF.Location = new System.Drawing.Point(346, 18);
            this.lblCUTOFF.Name = "lblCUTOFF";
            this.lblCUTOFF.Size = new System.Drawing.Size(29, 12);
            this.lblCUTOFF.TabIndex = 2;
            this.lblCUTOFF.Text = "5000";
            // 
            // trbCUTOFF
            // 
            this.trbCUTOFF.AutoSize = false;
            this.trbCUTOFF.Location = new System.Drawing.Point(94, 13);
            this.trbCUTOFF.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.trbCUTOFF.Maximum = 22000;
            this.trbCUTOFF.Minimum = 56;
            this.trbCUTOFF.Name = "trbCUTOFF";
            this.trbCUTOFF.Size = new System.Drawing.Size(247, 28);
            this.trbCUTOFF.TabIndex = 1;
            this.trbCUTOFF.Value = 5000;
            this.trbCUTOFF.ValueChanged += new System.EventHandler(this.trbCUTOFF_ValueChanged);
            // 
            // chkLOWPASS
            // 
            this.chkLOWPASS.AutoSize = true;
            this.chkLOWPASS.Location = new System.Drawing.Point(3, 437);
            this.chkLOWPASS.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.chkLOWPASS.Name = "chkLOWPASS";
            this.chkLOWPASS.Size = new System.Drawing.Size(100, 16);
            this.chkLOWPASS.TabIndex = 19;
            this.chkLOWPASS.Text = "로우패스 사용";
            this.chkLOWPASS.UseVisualStyleBackColor = true;
            this.chkLOWPASS.CheckedChanged += new System.EventHandler(this.checkBox6_CheckedChanged);
            // 
            // trbGain
            // 
            this.trbGain.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.trbGain.AutoSize = false;
            this.trbGain.Enabled = false;
            this.trbGain.Location = new System.Drawing.Point(97, 30);
            this.trbGain.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.trbGain.Maximum = 1000;
            this.trbGain.Minimum = 300;
            this.trbGain.Name = "trbGain";
            this.trbGain.Size = new System.Drawing.Size(681, 28);
            this.trbGain.TabIndex = 21;
            this.trbGain.TickFrequency = 5;
            this.trbGain.Value = 900;
            this.trbGain.Scroll += new System.EventHandler(this.trbGain_Scroll);
            this.trbGain.ValueChanged += new System.EventHandler(this.trbGain_ValueChanged);
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.Enabled = false;
            this.label1.Location = new System.Drawing.Point(782, 40);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(23, 12);
            this.label1.TabIndex = 22;
            this.label1.Text = "600";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("굴림", 9.209303F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label4.Location = new System.Drawing.Point(3, 40);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(75, 13);
            this.label4.TabIndex = 23;
            this.label4.Text = "DSP Gain";
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.button16);
            this.groupBox7.Controls.Add(this.label6);
            this.groupBox7.Controls.Add(this.label28);
            this.groupBox7.Controls.Add(this.lblDISTORTION);
            this.groupBox7.Controls.Add(this.trbDISTORTION);
            this.groupBox7.Enabled = false;
            this.groupBox7.ForeColor = System.Drawing.Color.Blue;
            this.groupBox7.Location = new System.Drawing.Point(3, 559);
            this.groupBox7.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupBox7.Size = new System.Drawing.Size(455, 50);
            this.groupBox7.TabIndex = 22;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "왜곡 (DISTORTION)";
            // 
            // button16
            // 
            this.button16.ForeColor = System.Drawing.Color.Black;
            this.button16.Location = new System.Drawing.Point(392, 18);
            this.button16.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.button16.Name = "button16";
            this.button16.Size = new System.Drawing.Size(57, 22);
            this.button16.TabIndex = 10;
            this.button16.Text = "기본값";
            this.button16.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.button16.UseVisualStyleBackColor = true;
            this.button16.Click += new System.EventHandler(this.button16_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(348, 80);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(23, 12);
            this.label6.TabIndex = 8;
            this.label6.Text = "450";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.ForeColor = System.Drawing.Color.Black;
            this.label28.Location = new System.Drawing.Point(8, 22);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(43, 12);
            this.label28.TabIndex = 3;
            this.label28.Text = "LEVEL";
            // 
            // lblDISTORTION
            // 
            this.lblDISTORTION.AutoSize = true;
            this.lblDISTORTION.ForeColor = System.Drawing.Color.Black;
            this.lblDISTORTION.Location = new System.Drawing.Point(346, 23);
            this.lblDISTORTION.Name = "lblDISTORTION";
            this.lblDISTORTION.Size = new System.Drawing.Size(23, 12);
            this.lblDISTORTION.TabIndex = 2;
            this.lblDISTORTION.Text = "300";
            // 
            // trbDISTORTION
            // 
            this.trbDISTORTION.AutoSize = false;
            this.trbDISTORTION.Location = new System.Drawing.Point(57, 16);
            this.trbDISTORTION.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.trbDISTORTION.Maximum = 1000;
            this.trbDISTORTION.Name = "trbDISTORTION";
            this.trbDISTORTION.Size = new System.Drawing.Size(285, 28);
            this.trbDISTORTION.TabIndex = 1;
            this.trbDISTORTION.Value = 300;
            this.trbDISTORTION.ValueChanged += new System.EventHandler(this.trbDISTORTION_ValueChanged);
            // 
            // chkDISTORTION
            // 
            this.chkDISTORTION.AutoSize = true;
            this.chkDISTORTION.Location = new System.Drawing.Point(3, 540);
            this.chkDISTORTION.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.chkDISTORTION.Name = "chkDISTORTION";
            this.chkDISTORTION.Size = new System.Drawing.Size(163, 16);
            this.chkDISTORTION.TabIndex = 21;
            this.chkDISTORTION.Text = "왜곡 (DISTORTION) 사용";
            this.chkDISTORTION.UseVisualStyleBackColor = true;
            this.chkDISTORTION.CheckedChanged += new System.EventHandler(this.checkBox7_CheckedChanged);
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this.button25);
            this.groupBox8.Controls.Add(this.label22);
            this.groupBox8.Controls.Add(this.lblFEEDBACK_CHORUS);
            this.groupBox8.Controls.Add(this.trbFEEDBACK_CHORUS);
            this.groupBox8.Controls.Add(this.button22);
            this.groupBox8.Controls.Add(this.button23);
            this.groupBox8.Controls.Add(this.button24);
            this.groupBox8.Controls.Add(this.button15);
            this.groupBox8.Controls.Add(this.button19);
            this.groupBox8.Controls.Add(this.button20);
            this.groupBox8.Controls.Add(this.button21);
            this.groupBox8.Controls.Add(this.lblWETMIX3_CHORUS);
            this.groupBox8.Controls.Add(this.lblWETMIX2_CHORUS);
            this.groupBox8.Controls.Add(this.label43);
            this.groupBox8.Controls.Add(this.trbWETMIX3_CHORUS);
            this.groupBox8.Controls.Add(this.label44);
            this.groupBox8.Controls.Add(this.trbWETMIX2_CHORUS);
            this.groupBox8.Controls.Add(this.label38);
            this.groupBox8.Controls.Add(this.lblWETMIX1_CHORUS);
            this.groupBox8.Controls.Add(this.trbWETMIX1_CHORUS);
            this.groupBox8.Controls.Add(this.label41);
            this.groupBox8.Controls.Add(this.lblDRYMIX_CHORUS);
            this.groupBox8.Controls.Add(this.trbDRYMIX_CHORUS);
            this.groupBox8.Controls.Add(this.label24);
            this.groupBox8.Controls.Add(this.lblDEPTH_CHORUS);
            this.groupBox8.Controls.Add(this.trbDEPTH_CHORUS);
            this.groupBox8.Controls.Add(this.label30);
            this.groupBox8.Controls.Add(this.lblRATE_CHORUS);
            this.groupBox8.Controls.Add(this.trbRATE_CHORUS);
            this.groupBox8.Controls.Add(this.label34);
            this.groupBox8.Controls.Add(this.lblDELAY_CHORUS);
            this.groupBox8.Controls.Add(this.trbDELAY_CHORUS);
            this.groupBox8.Enabled = false;
            this.groupBox8.ForeColor = System.Drawing.Color.Blue;
            this.groupBox8.Location = new System.Drawing.Point(465, 23);
            this.groupBox8.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupBox8.Size = new System.Drawing.Size(458, 270);
            this.groupBox8.TabIndex = 11;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "코러스";
            // 
            // button25
            // 
            this.button25.ForeColor = System.Drawing.Color.Black;
            this.button25.Location = new System.Drawing.Point(388, 78);
            this.button25.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.button25.Name = "button25";
            this.button25.Size = new System.Drawing.Size(57, 22);
            this.button25.TabIndex = 32;
            this.button25.Text = "기본값";
            this.button25.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.button25.UseVisualStyleBackColor = true;
            this.button25.Click += new System.EventHandler(this.button25_Click);
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.ForeColor = System.Drawing.Color.Black;
            this.label22.Location = new System.Drawing.Point(7, 85);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(69, 12);
            this.label22.TabIndex = 31;
            this.label22.Text = "FEEDBACK";
            // 
            // lblFEEDBACK_CHORUS
            // 
            this.lblFEEDBACK_CHORUS.AutoSize = true;
            this.lblFEEDBACK_CHORUS.ForeColor = System.Drawing.Color.Black;
            this.lblFEEDBACK_CHORUS.Location = new System.Drawing.Point(343, 85);
            this.lblFEEDBACK_CHORUS.Name = "lblFEEDBACK_CHORUS";
            this.lblFEEDBACK_CHORUS.Size = new System.Drawing.Size(29, 12);
            this.lblFEEDBACK_CHORUS.TabIndex = 30;
            this.lblFEEDBACK_CHORUS.Text = "1000";
            // 
            // trbFEEDBACK_CHORUS
            // 
            this.trbFEEDBACK_CHORUS.AutoSize = false;
            this.trbFEEDBACK_CHORUS.Location = new System.Drawing.Point(77, 78);
            this.trbFEEDBACK_CHORUS.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.trbFEEDBACK_CHORUS.Maximum = 1000;
            this.trbFEEDBACK_CHORUS.Name = "trbFEEDBACK_CHORUS";
            this.trbFEEDBACK_CHORUS.Size = new System.Drawing.Size(260, 28);
            this.trbFEEDBACK_CHORUS.TabIndex = 29;
            this.trbFEEDBACK_CHORUS.Value = 1000;
            this.trbFEEDBACK_CHORUS.ValueChanged += new System.EventHandler(this.trbFEEDBACK_CHORUS_ValueChanged);
            // 
            // button22
            // 
            this.button22.ForeColor = System.Drawing.Color.Black;
            this.button22.Location = new System.Drawing.Point(388, 242);
            this.button22.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.button22.Name = "button22";
            this.button22.Size = new System.Drawing.Size(57, 22);
            this.button22.TabIndex = 28;
            this.button22.Text = "기본값";
            this.button22.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.button22.UseVisualStyleBackColor = true;
            this.button22.Click += new System.EventHandler(this.button22_Click);
            // 
            // button23
            // 
            this.button23.ForeColor = System.Drawing.Color.Black;
            this.button23.Location = new System.Drawing.Point(388, 210);
            this.button23.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.button23.Name = "button23";
            this.button23.Size = new System.Drawing.Size(57, 22);
            this.button23.TabIndex = 27;
            this.button23.Text = "기본값";
            this.button23.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.button23.UseVisualStyleBackColor = true;
            this.button23.Click += new System.EventHandler(this.button23_Click);
            // 
            // button24
            // 
            this.button24.ForeColor = System.Drawing.Color.Black;
            this.button24.Location = new System.Drawing.Point(388, 178);
            this.button24.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.button24.Name = "button24";
            this.button24.Size = new System.Drawing.Size(57, 22);
            this.button24.TabIndex = 26;
            this.button24.Text = "기본값";
            this.button24.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.button24.UseVisualStyleBackColor = true;
            this.button24.Click += new System.EventHandler(this.button24_Click);
            // 
            // button15
            // 
            this.button15.ForeColor = System.Drawing.Color.Black;
            this.button15.Location = new System.Drawing.Point(388, 16);
            this.button15.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.button15.Name = "button15";
            this.button15.Size = new System.Drawing.Size(57, 22);
            this.button15.TabIndex = 25;
            this.button15.Text = "기본값";
            this.button15.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.button15.UseVisualStyleBackColor = true;
            this.button15.Click += new System.EventHandler(this.button15_Click);
            // 
            // button19
            // 
            this.button19.ForeColor = System.Drawing.Color.Black;
            this.button19.Location = new System.Drawing.Point(388, 144);
            this.button19.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.button19.Name = "button19";
            this.button19.Size = new System.Drawing.Size(57, 22);
            this.button19.TabIndex = 24;
            this.button19.Text = "기본값";
            this.button19.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.button19.UseVisualStyleBackColor = true;
            this.button19.Click += new System.EventHandler(this.button19_Click);
            // 
            // button20
            // 
            this.button20.ForeColor = System.Drawing.Color.Black;
            this.button20.Location = new System.Drawing.Point(388, 110);
            this.button20.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.button20.Name = "button20";
            this.button20.Size = new System.Drawing.Size(57, 22);
            this.button20.TabIndex = 23;
            this.button20.Text = "기본값";
            this.button20.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.button20.UseVisualStyleBackColor = true;
            this.button20.Click += new System.EventHandler(this.button20_Click);
            // 
            // button21
            // 
            this.button21.ForeColor = System.Drawing.Color.Black;
            this.button21.Location = new System.Drawing.Point(388, 46);
            this.button21.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.button21.Name = "button21";
            this.button21.Size = new System.Drawing.Size(57, 22);
            this.button21.TabIndex = 22;
            this.button21.Text = "기본값";
            this.button21.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.button21.UseVisualStyleBackColor = true;
            this.button21.Click += new System.EventHandler(this.button21_Click);
            // 
            // lblWETMIX3_CHORUS
            // 
            this.lblWETMIX3_CHORUS.AutoSize = true;
            this.lblWETMIX3_CHORUS.ForeColor = System.Drawing.Color.Black;
            this.lblWETMIX3_CHORUS.Location = new System.Drawing.Point(343, 247);
            this.lblWETMIX3_CHORUS.Name = "lblWETMIX3_CHORUS";
            this.lblWETMIX3_CHORUS.Size = new System.Drawing.Size(23, 12);
            this.lblWETMIX3_CHORUS.TabIndex = 21;
            this.lblWETMIX3_CHORUS.Text = "500";
            // 
            // lblWETMIX2_CHORUS
            // 
            this.lblWETMIX2_CHORUS.AutoSize = true;
            this.lblWETMIX2_CHORUS.ForeColor = System.Drawing.Color.Black;
            this.lblWETMIX2_CHORUS.Location = new System.Drawing.Point(343, 215);
            this.lblWETMIX2_CHORUS.Name = "lblWETMIX2_CHORUS";
            this.lblWETMIX2_CHORUS.Size = new System.Drawing.Size(23, 12);
            this.lblWETMIX2_CHORUS.TabIndex = 20;
            this.lblWETMIX2_CHORUS.Text = "500";
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.ForeColor = System.Drawing.Color.Black;
            this.label43.Location = new System.Drawing.Point(7, 246);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(59, 12);
            this.label43.TabIndex = 19;
            this.label43.Text = "WETMIX3";
            // 
            // trbWETMIX3_CHORUS
            // 
            this.trbWETMIX3_CHORUS.AutoSize = false;
            this.trbWETMIX3_CHORUS.Location = new System.Drawing.Point(77, 239);
            this.trbWETMIX3_CHORUS.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.trbWETMIX3_CHORUS.Maximum = 1000;
            this.trbWETMIX3_CHORUS.Name = "trbWETMIX3_CHORUS";
            this.trbWETMIX3_CHORUS.Size = new System.Drawing.Size(260, 28);
            this.trbWETMIX3_CHORUS.TabIndex = 18;
            this.trbWETMIX3_CHORUS.Value = 500;
            this.trbWETMIX3_CHORUS.ValueChanged += new System.EventHandler(this.trbWETMIX3_CHORUS_ValueChanged);
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.ForeColor = System.Drawing.Color.Black;
            this.label44.Location = new System.Drawing.Point(7, 215);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(59, 12);
            this.label44.TabIndex = 17;
            this.label44.Text = "WETMIX2";
            // 
            // trbWETMIX2_CHORUS
            // 
            this.trbWETMIX2_CHORUS.AutoSize = false;
            this.trbWETMIX2_CHORUS.Location = new System.Drawing.Point(77, 206);
            this.trbWETMIX2_CHORUS.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.trbWETMIX2_CHORUS.Maximum = 1000;
            this.trbWETMIX2_CHORUS.Name = "trbWETMIX2_CHORUS";
            this.trbWETMIX2_CHORUS.Size = new System.Drawing.Size(260, 28);
            this.trbWETMIX2_CHORUS.TabIndex = 16;
            this.trbWETMIX2_CHORUS.Value = 500;
            this.trbWETMIX2_CHORUS.ValueChanged += new System.EventHandler(this.trbWETMIX2_CHORUS_ValueChanged);
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.ForeColor = System.Drawing.Color.Black;
            this.label38.Location = new System.Drawing.Point(7, 181);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(59, 12);
            this.label38.TabIndex = 15;
            this.label38.Text = "WETMIX1";
            // 
            // lblWETMIX1_CHORUS
            // 
            this.lblWETMIX1_CHORUS.AutoSize = true;
            this.lblWETMIX1_CHORUS.ForeColor = System.Drawing.Color.Black;
            this.lblWETMIX1_CHORUS.Location = new System.Drawing.Point(343, 182);
            this.lblWETMIX1_CHORUS.Name = "lblWETMIX1_CHORUS";
            this.lblWETMIX1_CHORUS.Size = new System.Drawing.Size(23, 12);
            this.lblWETMIX1_CHORUS.TabIndex = 14;
            this.lblWETMIX1_CHORUS.Text = "500";
            // 
            // trbWETMIX1_CHORUS
            // 
            this.trbWETMIX1_CHORUS.AutoSize = false;
            this.trbWETMIX1_CHORUS.Location = new System.Drawing.Point(77, 174);
            this.trbWETMIX1_CHORUS.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.trbWETMIX1_CHORUS.Maximum = 1000;
            this.trbWETMIX1_CHORUS.Name = "trbWETMIX1_CHORUS";
            this.trbWETMIX1_CHORUS.Size = new System.Drawing.Size(260, 28);
            this.trbWETMIX1_CHORUS.TabIndex = 13;
            this.trbWETMIX1_CHORUS.Value = 500;
            this.trbWETMIX1_CHORUS.ValueChanged += new System.EventHandler(this.trbWETMIX1_CHORUS_ValueChanged);
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.ForeColor = System.Drawing.Color.Black;
            this.label41.Location = new System.Drawing.Point(7, 149);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(51, 12);
            this.label41.TabIndex = 12;
            this.label41.Text = "DRYMIX";
            // 
            // lblDRYMIX_CHORUS
            // 
            this.lblDRYMIX_CHORUS.AutoSize = true;
            this.lblDRYMIX_CHORUS.ForeColor = System.Drawing.Color.Black;
            this.lblDRYMIX_CHORUS.Location = new System.Drawing.Point(343, 149);
            this.lblDRYMIX_CHORUS.Name = "lblDRYMIX_CHORUS";
            this.lblDRYMIX_CHORUS.Size = new System.Drawing.Size(23, 12);
            this.lblDRYMIX_CHORUS.TabIndex = 11;
            this.lblDRYMIX_CHORUS.Text = "500";
            // 
            // trbDRYMIX_CHORUS
            // 
            this.trbDRYMIX_CHORUS.AutoSize = false;
            this.trbDRYMIX_CHORUS.Location = new System.Drawing.Point(77, 139);
            this.trbDRYMIX_CHORUS.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.trbDRYMIX_CHORUS.Maximum = 1000;
            this.trbDRYMIX_CHORUS.Name = "trbDRYMIX_CHORUS";
            this.trbDRYMIX_CHORUS.Size = new System.Drawing.Size(260, 28);
            this.trbDRYMIX_CHORUS.TabIndex = 10;
            this.trbDRYMIX_CHORUS.Value = 500;
            this.trbDRYMIX_CHORUS.ValueChanged += new System.EventHandler(this.trbDRYMIX_CHORUS_ValueChanged);
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.ForeColor = System.Drawing.Color.Black;
            this.label24.Location = new System.Drawing.Point(7, 115);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(45, 12);
            this.label24.TabIndex = 9;
            this.label24.Text = "DEPTH";
            // 
            // lblDEPTH_CHORUS
            // 
            this.lblDEPTH_CHORUS.AutoSize = true;
            this.lblDEPTH_CHORUS.ForeColor = System.Drawing.Color.Black;
            this.lblDEPTH_CHORUS.Location = new System.Drawing.Point(343, 115);
            this.lblDEPTH_CHORUS.Name = "lblDEPTH_CHORUS";
            this.lblDEPTH_CHORUS.Size = new System.Drawing.Size(17, 12);
            this.lblDEPTH_CHORUS.TabIndex = 8;
            this.lblDEPTH_CHORUS.Text = "20";
            // 
            // trbDEPTH_CHORUS
            // 
            this.trbDEPTH_CHORUS.AutoSize = false;
            this.trbDEPTH_CHORUS.Location = new System.Drawing.Point(77, 107);
            this.trbDEPTH_CHORUS.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.trbDEPTH_CHORUS.Maximum = 1000;
            this.trbDEPTH_CHORUS.Name = "trbDEPTH_CHORUS";
            this.trbDEPTH_CHORUS.Size = new System.Drawing.Size(260, 28);
            this.trbDEPTH_CHORUS.TabIndex = 7;
            this.trbDEPTH_CHORUS.Value = 30;
            this.trbDEPTH_CHORUS.ValueChanged += new System.EventHandler(this.trbDEPTH_CHORUS_ValueChanged);
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.ForeColor = System.Drawing.Color.Black;
            this.label30.Location = new System.Drawing.Point(6, 53);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(37, 12);
            this.label30.TabIndex = 6;
            this.label30.Text = "RATE";
            // 
            // lblRATE_CHORUS
            // 
            this.lblRATE_CHORUS.AutoSize = true;
            this.lblRATE_CHORUS.ForeColor = System.Drawing.Color.Black;
            this.lblRATE_CHORUS.Location = new System.Drawing.Point(343, 53);
            this.lblRATE_CHORUS.Name = "lblRATE_CHORUS";
            this.lblRATE_CHORUS.Size = new System.Drawing.Size(23, 12);
            this.lblRATE_CHORUS.TabIndex = 5;
            this.lblRATE_CHORUS.Text = "800";
            // 
            // trbRATE_CHORUS
            // 
            this.trbRATE_CHORUS.AutoSize = false;
            this.trbRATE_CHORUS.Location = new System.Drawing.Point(77, 46);
            this.trbRATE_CHORUS.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.trbRATE_CHORUS.Maximum = 2000;
            this.trbRATE_CHORUS.Name = "trbRATE_CHORUS";
            this.trbRATE_CHORUS.Size = new System.Drawing.Size(260, 28);
            this.trbRATE_CHORUS.TabIndex = 4;
            this.trbRATE_CHORUS.Value = 800;
            this.trbRATE_CHORUS.ValueChanged += new System.EventHandler(this.trbRATE_CHORUS_ValueChanged);
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.ForeColor = System.Drawing.Color.Black;
            this.label34.Location = new System.Drawing.Point(6, 18);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(44, 12);
            this.label34.TabIndex = 3;
            this.label34.Text = "DELAY";
            // 
            // lblDELAY_CHORUS
            // 
            this.lblDELAY_CHORUS.AutoSize = true;
            this.lblDELAY_CHORUS.ForeColor = System.Drawing.Color.Black;
            this.lblDELAY_CHORUS.Location = new System.Drawing.Point(343, 18);
            this.lblDELAY_CHORUS.Name = "lblDELAY_CHORUS";
            this.lblDELAY_CHORUS.Size = new System.Drawing.Size(29, 12);
            this.lblDELAY_CHORUS.TabIndex = 2;
            this.lblDELAY_CHORUS.Text = "4000";
            // 
            // trbDELAY_CHORUS
            // 
            this.trbDELAY_CHORUS.AutoSize = false;
            this.trbDELAY_CHORUS.Location = new System.Drawing.Point(77, 13);
            this.trbDELAY_CHORUS.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.trbDELAY_CHORUS.Maximum = 10000;
            this.trbDELAY_CHORUS.Name = "trbDELAY_CHORUS";
            this.trbDELAY_CHORUS.Size = new System.Drawing.Size(260, 28);
            this.trbDELAY_CHORUS.TabIndex = 1;
            this.trbDELAY_CHORUS.Value = 4000;
            this.trbDELAY_CHORUS.ValueChanged += new System.EventHandler(this.trbDELAY_CHORUS_ValueChanged);
            // 
            // chkCHORUS
            // 
            this.chkCHORUS.AutoSize = true;
            this.chkCHORUS.Location = new System.Drawing.Point(465, 4);
            this.chkCHORUS.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.chkCHORUS.Name = "chkCHORUS";
            this.chkCHORUS.Size = new System.Drawing.Size(88, 16);
            this.chkCHORUS.TabIndex = 10;
            this.chkCHORUS.Text = "코러스 사용";
            this.chkCHORUS.UseVisualStyleBackColor = true;
            this.chkCHORUS.CheckedChanged += new System.EventHandler(this.checkBox8_CheckedChanged);
            // 
            // btnWhaWha
            // 
            this.btnWhaWha.ForeColor = System.Drawing.Color.Blue;
            this.btnWhaWha.Location = new System.Drawing.Point(3, 1);
            this.btnWhaWha.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnWhaWha.Name = "btnWhaWha";
            this.btnWhaWha.Size = new System.Drawing.Size(75, 20);
            this.btnWhaWha.TabIndex = 26;
            this.btnWhaWha.Text = "Wha-Wha";
            this.btnWhaWha.UseVisualStyleBackColor = true;
            this.btnWhaWha.Click += new System.EventHandler(this.btnWhaWha_Click);
            // 
            // trbWhaWhaSpeed
            // 
            this.trbWhaWhaSpeed.AutoSize = false;
            this.trbWhaWhaSpeed.Location = new System.Drawing.Point(125, 1);
            this.trbWhaWhaSpeed.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.trbWhaWhaSpeed.Maximum = 100;
            this.trbWhaWhaSpeed.Minimum = 1;
            this.trbWhaWhaSpeed.Name = "trbWhaWhaSpeed";
            this.trbWhaWhaSpeed.Size = new System.Drawing.Size(75, 18);
            this.trbWhaWhaSpeed.TabIndex = 27;
            this.trbWhaWhaSpeed.Value = 30;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.ForeColor = System.Drawing.Color.Blue;
            this.label15.Location = new System.Drawing.Point(83, 6);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(40, 12);
            this.label15.TabIndex = 28;
            this.label15.Text = "speed";
            // 
            // timer1
            // 
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.ForeColor = System.Drawing.Color.Blue;
            this.label20.Location = new System.Drawing.Point(206, 6);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(36, 12);
            this.label20.TabIndex = 30;
            this.label20.Text = "delay";
            // 
            // trbWhaWhaDelay
            // 
            this.trbWhaWhaDelay.AutoSize = false;
            this.trbWhaWhaDelay.Location = new System.Drawing.Point(248, 2);
            this.trbWhaWhaDelay.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.trbWhaWhaDelay.Maximum = 20;
            this.trbWhaWhaDelay.Minimum = 1;
            this.trbWhaWhaDelay.Name = "trbWhaWhaDelay";
            this.trbWhaWhaDelay.Size = new System.Drawing.Size(80, 18);
            this.trbWhaWhaDelay.TabIndex = 29;
            this.trbWhaWhaDelay.Value = 1;
            this.trbWhaWhaDelay.ValueChanged += new System.EventHandler(this.WhaWha_Delay_ValueChanged);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btnWhaWha);
            this.panel1.Controls.Add(this.label20);
            this.panel1.Controls.Add(this.trbWhaWhaSpeed);
            this.panel1.Controls.Add(this.trbWhaWhaDelay);
            this.panel1.Controls.Add(this.label15);
            this.panel1.Location = new System.Drawing.Point(112, 433);
            this.panel1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(332, 22);
            this.panel1.TabIndex = 31;
            // 
            // btnDSPRemove
            // 
            this.btnDSPRemove.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDSPRemove.Enabled = false;
            this.btnDSPRemove.Location = new System.Drawing.Point(889, 34);
            this.btnDSPRemove.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnDSPRemove.Name = "btnDSPRemove";
            this.btnDSPRemove.Size = new System.Drawing.Size(75, 23);
            this.btnDSPRemove.TabIndex = 24;
            this.btnDSPRemove.Text = "DSP끄기";
            this.btnDSPRemove.UseVisualStyleBackColor = true;
            this.btnDSPRemove.Click += new System.EventHandler(this.btnDSPRemove_Click);
            // 
            // btnDSPRegister
            // 
            this.btnDSPRegister.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDSPRegister.Location = new System.Drawing.Point(814, 34);
            this.btnDSPRegister.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnDSPRegister.Name = "btnDSPRegister";
            this.btnDSPRegister.Size = new System.Drawing.Size(75, 23);
            this.btnDSPRegister.TabIndex = 25;
            this.btnDSPRegister.Text = "DSP켜기";
            this.btnDSPRegister.UseVisualStyleBackColor = true;
            this.btnDSPRegister.Click += new System.EventHandler(this.btnDSPRegister_Click);
            // 
            // groupBox9
            // 
            this.groupBox9.Controls.Add(this.button26);
            this.groupBox9.Controls.Add(this.label26);
            this.groupBox9.Controls.Add(this.lblSHAPE);
            this.groupBox9.Controls.Add(this.trbSHAPE);
            this.groupBox9.Controls.Add(this.button27);
            this.groupBox9.Controls.Add(this.button28);
            this.groupBox9.Controls.Add(this.button29);
            this.groupBox9.Controls.Add(this.button30);
            this.groupBox9.Controls.Add(this.button31);
            this.groupBox9.Controls.Add(this.button32);
            this.groupBox9.Controls.Add(this.button33);
            this.groupBox9.Controls.Add(this.lblSPREAD);
            this.groupBox9.Controls.Add(this.lblPHASE);
            this.groupBox9.Controls.Add(this.label36);
            this.groupBox9.Controls.Add(this.trbSPREAD);
            this.groupBox9.Controls.Add(this.label40);
            this.groupBox9.Controls.Add(this.trbPHASE);
            this.groupBox9.Controls.Add(this.label42);
            this.groupBox9.Controls.Add(this.lblSQUARE);
            this.groupBox9.Controls.Add(this.trbSQUARE);
            this.groupBox9.Controls.Add(this.label46);
            this.groupBox9.Controls.Add(this.lblDUTY);
            this.groupBox9.Controls.Add(this.trbDUTY);
            this.groupBox9.Controls.Add(this.label48);
            this.groupBox9.Controls.Add(this.lblSKEW);
            this.groupBox9.Controls.Add(this.trbSKEW);
            this.groupBox9.Controls.Add(this.label50);
            this.groupBox9.Controls.Add(this.lblDEPTH_TREMOLO);
            this.groupBox9.Controls.Add(this.trbDEPTH_TREMOLO);
            this.groupBox9.Controls.Add(this.label52);
            this.groupBox9.Controls.Add(this.lblFREQUENCY);
            this.groupBox9.Controls.Add(this.trbFREQUENCY);
            this.groupBox9.Enabled = false;
            this.groupBox9.ForeColor = System.Drawing.Color.Blue;
            this.groupBox9.Location = new System.Drawing.Point(463, 318);
            this.groupBox9.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupBox9.Size = new System.Drawing.Size(461, 270);
            this.groupBox9.TabIndex = 33;
            this.groupBox9.TabStop = false;
            this.groupBox9.Text = "트레몰로";
            // 
            // button26
            // 
            this.button26.ForeColor = System.Drawing.Color.Black;
            this.button26.Location = new System.Drawing.Point(392, 82);
            this.button26.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.button26.Name = "button26";
            this.button26.Size = new System.Drawing.Size(56, 22);
            this.button26.TabIndex = 32;
            this.button26.Text = "기본값";
            this.button26.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.button26.UseVisualStyleBackColor = true;
            this.button26.Click += new System.EventHandler(this.button26_Click);
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.ForeColor = System.Drawing.Color.Black;
            this.label26.Location = new System.Drawing.Point(7, 85);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(45, 12);
            this.label26.TabIndex = 31;
            this.label26.Text = "SHAPE";
            // 
            // lblSHAPE
            // 
            this.lblSHAPE.AutoSize = true;
            this.lblSHAPE.ForeColor = System.Drawing.Color.Black;
            this.lblSHAPE.Location = new System.Drawing.Point(343, 85);
            this.lblSHAPE.Name = "lblSHAPE";
            this.lblSHAPE.Size = new System.Drawing.Size(29, 12);
            this.lblSHAPE.TabIndex = 30;
            this.lblSHAPE.Text = "1000";
            // 
            // trbSHAPE
            // 
            this.trbSHAPE.AutoSize = false;
            this.trbSHAPE.Location = new System.Drawing.Point(91, 78);
            this.trbSHAPE.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.trbSHAPE.Maximum = 1000;
            this.trbSHAPE.Name = "trbSHAPE";
            this.trbSHAPE.Size = new System.Drawing.Size(246, 28);
            this.trbSHAPE.TabIndex = 29;
            this.toolTip1.SetToolTip(this.trbSHAPE, "LFO shape morph between triangle and sine.");
            this.trbSHAPE.Value = 1000;
            this.trbSHAPE.ValueChanged += new System.EventHandler(this.trbSHAPE_ValueChanged);
            // 
            // button27
            // 
            this.button27.ForeColor = System.Drawing.Color.Black;
            this.button27.Location = new System.Drawing.Point(392, 245);
            this.button27.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.button27.Name = "button27";
            this.button27.Size = new System.Drawing.Size(56, 22);
            this.button27.TabIndex = 28;
            this.button27.Text = "기본값";
            this.button27.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.button27.UseVisualStyleBackColor = true;
            this.button27.Click += new System.EventHandler(this.button27_Click);
            // 
            // button28
            // 
            this.button28.ForeColor = System.Drawing.Color.Black;
            this.button28.Location = new System.Drawing.Point(392, 210);
            this.button28.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.button28.Name = "button28";
            this.button28.Size = new System.Drawing.Size(56, 22);
            this.button28.TabIndex = 27;
            this.button28.Text = "기본값";
            this.button28.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.button28.UseVisualStyleBackColor = true;
            this.button28.Click += new System.EventHandler(this.button28_Click);
            // 
            // button29
            // 
            this.button29.ForeColor = System.Drawing.Color.Black;
            this.button29.Location = new System.Drawing.Point(392, 178);
            this.button29.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.button29.Name = "button29";
            this.button29.Size = new System.Drawing.Size(56, 22);
            this.button29.TabIndex = 26;
            this.button29.Text = "기본값";
            this.button29.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.button29.UseVisualStyleBackColor = true;
            this.button29.Click += new System.EventHandler(this.button29_Click);
            // 
            // button30
            // 
            this.button30.ForeColor = System.Drawing.Color.Black;
            this.button30.Location = new System.Drawing.Point(392, 16);
            this.button30.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.button30.Name = "button30";
            this.button30.Size = new System.Drawing.Size(56, 22);
            this.button30.TabIndex = 25;
            this.button30.Text = "기본값";
            this.button30.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.button30.UseVisualStyleBackColor = true;
            this.button30.Click += new System.EventHandler(this.button30_Click);
            // 
            // button31
            // 
            this.button31.ForeColor = System.Drawing.Color.Black;
            this.button31.Location = new System.Drawing.Point(392, 144);
            this.button31.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.button31.Name = "button31";
            this.button31.Size = new System.Drawing.Size(56, 22);
            this.button31.TabIndex = 24;
            this.button31.Text = "기본값";
            this.button31.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.button31.UseVisualStyleBackColor = true;
            this.button31.Click += new System.EventHandler(this.button31_Click);
            // 
            // button32
            // 
            this.button32.ForeColor = System.Drawing.Color.Black;
            this.button32.Location = new System.Drawing.Point(392, 113);
            this.button32.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.button32.Name = "button32";
            this.button32.Size = new System.Drawing.Size(56, 22);
            this.button32.TabIndex = 23;
            this.button32.Text = "기본값";
            this.button32.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.button32.UseVisualStyleBackColor = true;
            this.button32.Click += new System.EventHandler(this.button32_Click);
            // 
            // button33
            // 
            this.button33.ForeColor = System.Drawing.Color.Black;
            this.button33.Location = new System.Drawing.Point(392, 49);
            this.button33.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.button33.Name = "button33";
            this.button33.Size = new System.Drawing.Size(56, 22);
            this.button33.TabIndex = 22;
            this.button33.Text = "기본값";
            this.button33.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.button33.UseVisualStyleBackColor = true;
            this.button33.Click += new System.EventHandler(this.button33_Click);
            // 
            // lblSPREAD
            // 
            this.lblSPREAD.AutoSize = true;
            this.lblSPREAD.ForeColor = System.Drawing.Color.Black;
            this.lblSPREAD.Location = new System.Drawing.Point(343, 247);
            this.lblSPREAD.Name = "lblSPREAD";
            this.lblSPREAD.Size = new System.Drawing.Size(23, 12);
            this.lblSPREAD.TabIndex = 21;
            this.lblSPREAD.Text = "500";
            // 
            // lblPHASE
            // 
            this.lblPHASE.AutoSize = true;
            this.lblPHASE.ForeColor = System.Drawing.Color.Black;
            this.lblPHASE.Location = new System.Drawing.Point(343, 215);
            this.lblPHASE.Name = "lblPHASE";
            this.lblPHASE.Size = new System.Drawing.Size(11, 12);
            this.lblPHASE.TabIndex = 20;
            this.lblPHASE.Text = "0";
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.ForeColor = System.Drawing.Color.Black;
            this.label36.Location = new System.Drawing.Point(7, 246);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(53, 12);
            this.label36.TabIndex = 19;
            this.label36.Text = "SPREAD";
            // 
            // trbSPREAD
            // 
            this.trbSPREAD.AutoSize = false;
            this.trbSPREAD.Location = new System.Drawing.Point(91, 239);
            this.trbSPREAD.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.trbSPREAD.Maximum = 1000;
            this.trbSPREAD.Minimum = -1000;
            this.trbSPREAD.Name = "trbSPREAD";
            this.trbSPREAD.Size = new System.Drawing.Size(246, 28);
            this.trbSPREAD.TabIndex = 18;
            this.toolTip1.SetToolTip(this.trbSPREAD, "Rotation / auto-pan effect.");
            this.trbSPREAD.Value = 500;
            this.trbSPREAD.ValueChanged += new System.EventHandler(this.trbSPREAD_ValueChanged);
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.ForeColor = System.Drawing.Color.Black;
            this.label40.Location = new System.Drawing.Point(7, 215);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(45, 12);
            this.label40.TabIndex = 17;
            this.label40.Text = "PHASE";
            // 
            // trbPHASE
            // 
            this.trbPHASE.AutoSize = false;
            this.trbPHASE.Location = new System.Drawing.Point(91, 206);
            this.trbPHASE.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.trbPHASE.Maximum = 1000;
            this.trbPHASE.Name = "trbPHASE";
            this.trbPHASE.Size = new System.Drawing.Size(246, 28);
            this.trbPHASE.TabIndex = 16;
            this.toolTip1.SetToolTip(this.trbPHASE, "Instantaneous LFO phase.");
            this.trbPHASE.ValueChanged += new System.EventHandler(this.trbPHASE_ValueChanged);
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.ForeColor = System.Drawing.Color.Black;
            this.label42.Location = new System.Drawing.Point(7, 181);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(54, 12);
            this.label42.TabIndex = 15;
            this.label42.Text = "SQUARE";
            // 
            // lblSQUARE
            // 
            this.lblSQUARE.AutoSize = true;
            this.lblSQUARE.ForeColor = System.Drawing.Color.Black;
            this.lblSQUARE.Location = new System.Drawing.Point(343, 182);
            this.lblSQUARE.Name = "lblSQUARE";
            this.lblSQUARE.Size = new System.Drawing.Size(11, 12);
            this.lblSQUARE.TabIndex = 14;
            this.lblSQUARE.Text = "0";
            // 
            // trbSQUARE
            // 
            this.trbSQUARE.AutoSize = false;
            this.trbSQUARE.Location = new System.Drawing.Point(91, 174);
            this.trbSQUARE.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.trbSQUARE.Maximum = 1000;
            this.trbSQUARE.Name = "trbSQUARE";
            this.trbSQUARE.Size = new System.Drawing.Size(246, 28);
            this.trbSQUARE.TabIndex = 13;
            this.toolTip1.SetToolTip(this.trbSQUARE, "Flatness of the LFO shape.");
            this.trbSQUARE.ValueChanged += new System.EventHandler(this.trbSQUARE_ValueChanged);
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.ForeColor = System.Drawing.Color.Black;
            this.label46.Location = new System.Drawing.Point(7, 149);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(37, 12);
            this.label46.TabIndex = 12;
            this.label46.Text = "DUTY";
            // 
            // lblDUTY
            // 
            this.lblDUTY.AutoSize = true;
            this.lblDUTY.ForeColor = System.Drawing.Color.Black;
            this.lblDUTY.Location = new System.Drawing.Point(343, 149);
            this.lblDUTY.Name = "lblDUTY";
            this.lblDUTY.Size = new System.Drawing.Size(23, 12);
            this.lblDUTY.TabIndex = 11;
            this.lblDUTY.Text = "500";
            // 
            // trbDUTY
            // 
            this.trbDUTY.AutoSize = false;
            this.trbDUTY.Location = new System.Drawing.Point(91, 139);
            this.trbDUTY.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.trbDUTY.Maximum = 1000;
            this.trbDUTY.Name = "trbDUTY";
            this.trbDUTY.Size = new System.Drawing.Size(246, 28);
            this.trbDUTY.TabIndex = 10;
            this.toolTip1.SetToolTip(this.trbDUTY, "LFO on-time.");
            this.trbDUTY.Value = 500;
            this.trbDUTY.ValueChanged += new System.EventHandler(this.trbDUTY_ValueChanged);
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.ForeColor = System.Drawing.Color.Black;
            this.label48.Location = new System.Drawing.Point(7, 115);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(39, 12);
            this.label48.TabIndex = 9;
            this.label48.Text = "SKEW";
            // 
            // lblSKEW
            // 
            this.lblSKEW.AutoSize = true;
            this.lblSKEW.ForeColor = System.Drawing.Color.Black;
            this.lblSKEW.Location = new System.Drawing.Point(343, 115);
            this.lblSKEW.Name = "lblSKEW";
            this.lblSKEW.Size = new System.Drawing.Size(11, 12);
            this.lblSKEW.TabIndex = 8;
            this.lblSKEW.Text = "0";
            // 
            // trbSKEW
            // 
            this.trbSKEW.AutoSize = false;
            this.trbSKEW.Location = new System.Drawing.Point(91, 107);
            this.trbSKEW.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.trbSKEW.Maximum = 1000;
            this.trbSKEW.Minimum = -1000;
            this.trbSKEW.Name = "trbSKEW";
            this.trbSKEW.Size = new System.Drawing.Size(246, 28);
            this.trbSKEW.TabIndex = 7;
            this.toolTip1.SetToolTip(this.trbSKEW, "Time-skewing of LFO cycle.");
            this.trbSKEW.ValueChanged += new System.EventHandler(this.trbSKEW_ValueChanged);
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.ForeColor = System.Drawing.Color.Black;
            this.label50.Location = new System.Drawing.Point(6, 53);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(45, 12);
            this.label50.TabIndex = 6;
            this.label50.Text = "DEPTH";
            // 
            // lblDEPTH_TREMOLO
            // 
            this.lblDEPTH_TREMOLO.AutoSize = true;
            this.lblDEPTH_TREMOLO.ForeColor = System.Drawing.Color.Black;
            this.lblDEPTH_TREMOLO.Location = new System.Drawing.Point(343, 53);
            this.lblDEPTH_TREMOLO.Name = "lblDEPTH_TREMOLO";
            this.lblDEPTH_TREMOLO.Size = new System.Drawing.Size(29, 12);
            this.lblDEPTH_TREMOLO.TabIndex = 5;
            this.lblDEPTH_TREMOLO.Text = "1000";
            // 
            // trbDEPTH_TREMOLO
            // 
            this.trbDEPTH_TREMOLO.AutoSize = false;
            this.trbDEPTH_TREMOLO.Location = new System.Drawing.Point(91, 46);
            this.trbDEPTH_TREMOLO.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.trbDEPTH_TREMOLO.Maximum = 1000;
            this.trbDEPTH_TREMOLO.Name = "trbDEPTH_TREMOLO";
            this.trbDEPTH_TREMOLO.Size = new System.Drawing.Size(246, 28);
            this.trbDEPTH_TREMOLO.TabIndex = 4;
            this.toolTip1.SetToolTip(this.trbDEPTH_TREMOLO, "Tremolo depth.");
            this.trbDEPTH_TREMOLO.Value = 1000;
            this.trbDEPTH_TREMOLO.ValueChanged += new System.EventHandler(this.trbDEPTH_TREMOLO_ValueChanged);
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.ForeColor = System.Drawing.Color.Black;
            this.label52.Location = new System.Drawing.Point(6, 18);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(79, 12);
            this.label52.TabIndex = 3;
            this.label52.Text = "FREQUENCY";
            // 
            // lblFREQUENCY
            // 
            this.lblFREQUENCY.AutoSize = true;
            this.lblFREQUENCY.ForeColor = System.Drawing.Color.Black;
            this.lblFREQUENCY.Location = new System.Drawing.Point(343, 18);
            this.lblFREQUENCY.Name = "lblFREQUENCY";
            this.lblFREQUENCY.Size = new System.Drawing.Size(23, 12);
            this.lblFREQUENCY.TabIndex = 2;
            this.lblFREQUENCY.Text = "400";
            // 
            // trbFREQUENCY
            // 
            this.trbFREQUENCY.AutoSize = false;
            this.trbFREQUENCY.Location = new System.Drawing.Point(91, 13);
            this.trbFREQUENCY.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.trbFREQUENCY.Maximum = 2000;
            this.trbFREQUENCY.Name = "trbFREQUENCY";
            this.trbFREQUENCY.Size = new System.Drawing.Size(246, 28);
            this.trbFREQUENCY.TabIndex = 1;
            this.toolTip1.SetToolTip(this.trbFREQUENCY, "LFO frequency in Hz.");
            this.trbFREQUENCY.Value = 400;
            this.trbFREQUENCY.ValueChanged += new System.EventHandler(this.trbFREQUENCY_ValueChanged);
            // 
            // chkTREMOLO
            // 
            this.chkTREMOLO.AutoSize = true;
            this.chkTREMOLO.Location = new System.Drawing.Point(465, 297);
            this.chkTREMOLO.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.chkTREMOLO.Name = "chkTREMOLO";
            this.chkTREMOLO.Size = new System.Drawing.Size(100, 16);
            this.chkTREMOLO.TabIndex = 32;
            this.chkTREMOLO.Text = "트레몰로 사용";
            this.chkTREMOLO.UseVisualStyleBackColor = true;
            this.chkTREMOLO.CheckedChanged += new System.EventHandler(this.checkBox9_CheckedChanged);
            // 
            // cbTYPE_OSCILLATOR
            // 
            this.cbTYPE_OSCILLATOR.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbTYPE_OSCILLATOR.FormattingEnabled = true;
            this.cbTYPE_OSCILLATOR.Items.AddRange(new object[] {
            "SINE",
            "SQUARE",
            "SAWUP",
            "SAWDOWN",
            "TRIANGLE",
            "NOISE"});
            this.cbTYPE_OSCILLATOR.Location = new System.Drawing.Point(48, 19);
            this.cbTYPE_OSCILLATOR.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.cbTYPE_OSCILLATOR.Name = "cbTYPE_OSCILLATOR";
            this.cbTYPE_OSCILLATOR.Size = new System.Drawing.Size(326, 20);
            this.cbTYPE_OSCILLATOR.TabIndex = 0;
            this.toolTip1.SetToolTip(this.cbTYPE_OSCILLATOR, "Waveform type");
            this.cbTYPE_OSCILLATOR.SelectedValueChanged += new System.EventHandler(this.cbTYPE_OSCILLATOR_SelectedValueChanged);
            // 
            // tbRATE_OSCILLATOR
            // 
            this.tbRATE_OSCILLATOR.AutoSize = false;
            this.tbRATE_OSCILLATOR.Location = new System.Drawing.Point(48, 46);
            this.tbRATE_OSCILLATOR.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tbRATE_OSCILLATOR.Maximum = 22000;
            this.tbRATE_OSCILLATOR.Minimum = 1;
            this.tbRATE_OSCILLATOR.Name = "tbRATE_OSCILLATOR";
            this.tbRATE_OSCILLATOR.Size = new System.Drawing.Size(260, 29);
            this.tbRATE_OSCILLATOR.TabIndex = 17;
            this.toolTip1.SetToolTip(this.tbRATE_OSCILLATOR, "Frequency of the sinewave in hz");
            this.tbRATE_OSCILLATOR.Value = 220;
            this.tbRATE_OSCILLATOR.ValueChanged += new System.EventHandler(this.tbRATE_OSCILLATOR_ValueChanged);
            // 
            // numRATE_OSCILLATOR
            // 
            this.numRATE_OSCILLATOR.DecimalPlaces = 1;
            this.numRATE_OSCILLATOR.Location = new System.Drawing.Point(314, 52);
            this.numRATE_OSCILLATOR.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.numRATE_OSCILLATOR.Maximum = new decimal(new int[] {
            22000,
            0,
            0,
            0});
            this.numRATE_OSCILLATOR.Name = "numRATE_OSCILLATOR";
            this.numRATE_OSCILLATOR.Size = new System.Drawing.Size(63, 21);
            this.numRATE_OSCILLATOR.TabIndex = 19;
            this.toolTip1.SetToolTip(this.numRATE_OSCILLATOR, "Frequency of the sinewave in hz");
            this.numRATE_OSCILLATOR.Value = new decimal(new int[] {
            220,
            0,
            0,
            0});
            this.numRATE_OSCILLATOR.ValueChanged += new System.EventHandler(this.numRATE_OSCILLATOR_ValueChanged);
            // 
            // panel2
            // 
            this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel2.AutoScroll = true;
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.chkHIGHPASS);
            this.panel2.Controls.Add(this.groupBox11);
            this.panel2.Controls.Add(this.chkOSCILLATOR);
            this.panel2.Controls.Add(this.groupBox10);
            this.panel2.Controls.Add(this.groupBox2);
            this.panel2.Controls.Add(this.groupBox9);
            this.panel2.Controls.Add(this.groupBox1);
            this.panel2.Controls.Add(this.chkTREMOLO);
            this.panel2.Controls.Add(this.chkNOMALIZER);
            this.panel2.Controls.Add(this.panel1);
            this.panel2.Controls.Add(this.chkECHO);
            this.panel2.Controls.Add(this.comboBox1);
            this.panel2.Controls.Add(this.groupBox8);
            this.panel2.Controls.Add(this.label8);
            this.panel2.Controls.Add(this.comboBox2);
            this.panel2.Controls.Add(this.chkCHORUS);
            this.panel2.Controls.Add(this.label10);
            this.panel2.Controls.Add(this.groupBox7);
            this.panel2.Controls.Add(this.groupBox3);
            this.panel2.Controls.Add(this.chkDISTORTION);
            this.panel2.Controls.Add(this.chkPITCHSHIFT);
            this.panel2.Controls.Add(this.chkCOMPRESSOR);
            this.panel2.Controls.Add(this.chkFLANGE);
            this.panel2.Controls.Add(this.groupBox4);
            this.panel2.Controls.Add(this.groupBox6);
            this.panel2.Controls.Add(this.groupBox5);
            this.panel2.Controls.Add(this.chkLOWPASS);
            this.panel2.Enabled = false;
            this.panel2.Location = new System.Drawing.Point(0, 88);
            this.panel2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(967, 638);
            this.panel2.TabIndex = 34;
            // 
            // chkHIGHPASS
            // 
            this.chkHIGHPASS.AutoSize = true;
            this.chkHIGHPASS.Location = new System.Drawing.Point(465, 738);
            this.chkHIGHPASS.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.chkHIGHPASS.Name = "chkHIGHPASS";
            this.chkHIGHPASS.Size = new System.Drawing.Size(100, 16);
            this.chkHIGHPASS.TabIndex = 37;
            this.chkHIGHPASS.Text = "하이패스 사용";
            this.chkHIGHPASS.UseVisualStyleBackColor = true;
            this.chkHIGHPASS.CheckedChanged += new System.EventHandler(this.checkBox11_CheckedChanged);
            // 
            // groupBox11
            // 
            this.groupBox11.Controls.Add(this.button39);
            this.groupBox11.Controls.Add(this.label49);
            this.groupBox11.Controls.Add(this.button40);
            this.groupBox11.Controls.Add(this.trbCUTOFF_HIGHPASS);
            this.groupBox11.Controls.Add(this.label45);
            this.groupBox11.Controls.Add(this.lblCUTOFF_HIGHPASS);
            this.groupBox11.Controls.Add(this.lblRESONANCE_HIGHPASS);
            this.groupBox11.Controls.Add(this.trbRESONANCE_HIGHPASS);
            this.groupBox11.Enabled = false;
            this.groupBox11.ForeColor = System.Drawing.Color.Blue;
            this.groupBox11.Location = new System.Drawing.Point(468, 757);
            this.groupBox11.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupBox11.Name = "groupBox11";
            this.groupBox11.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupBox11.Size = new System.Drawing.Size(457, 90);
            this.groupBox11.TabIndex = 36;
            this.groupBox11.TabStop = false;
            this.groupBox11.Text = "하이패스";
            // 
            // button39
            // 
            this.button39.ForeColor = System.Drawing.Color.Black;
            this.button39.Location = new System.Drawing.Point(388, 17);
            this.button39.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.button39.Name = "button39";
            this.button39.Size = new System.Drawing.Size(56, 22);
            this.button39.TabIndex = 19;
            this.button39.Text = "기본값";
            this.button39.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.button39.UseVisualStyleBackColor = true;
            this.button39.Click += new System.EventHandler(this.button39_Click);
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.ForeColor = System.Drawing.Color.Black;
            this.label49.Location = new System.Drawing.Point(6, 19);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(53, 12);
            this.label49.TabIndex = 14;
            this.label49.Text = "CUTOFF";
            // 
            // button40
            // 
            this.button40.ForeColor = System.Drawing.Color.Black;
            this.button40.Location = new System.Drawing.Point(388, 50);
            this.button40.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.button40.Name = "button40";
            this.button40.Size = new System.Drawing.Size(56, 22);
            this.button40.TabIndex = 18;
            this.button40.Text = "기본값";
            this.button40.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.button40.UseVisualStyleBackColor = true;
            this.button40.Click += new System.EventHandler(this.button40_Click);
            // 
            // trbCUTOFF_HIGHPASS
            // 
            this.trbCUTOFF_HIGHPASS.AutoSize = false;
            this.trbCUTOFF_HIGHPASS.Location = new System.Drawing.Point(93, 13);
            this.trbCUTOFF_HIGHPASS.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.trbCUTOFF_HIGHPASS.Maximum = 22000;
            this.trbCUTOFF_HIGHPASS.Name = "trbCUTOFF_HIGHPASS";
            this.trbCUTOFF_HIGHPASS.Size = new System.Drawing.Size(247, 28);
            this.trbCUTOFF_HIGHPASS.TabIndex = 12;
            this.trbCUTOFF_HIGHPASS.Value = 5000;
            this.trbCUTOFF_HIGHPASS.ValueChanged += new System.EventHandler(this.trbCUTOFF_HIGHPASS_ValueChanged);
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.ForeColor = System.Drawing.Color.Black;
            this.label45.Location = new System.Drawing.Point(6, 54);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(81, 12);
            this.label45.TabIndex = 17;
            this.label45.Text = "RESONANCE";
            // 
            // lblCUTOFF_HIGHPASS
            // 
            this.lblCUTOFF_HIGHPASS.AutoSize = true;
            this.lblCUTOFF_HIGHPASS.ForeColor = System.Drawing.Color.Black;
            this.lblCUTOFF_HIGHPASS.Location = new System.Drawing.Point(345, 18);
            this.lblCUTOFF_HIGHPASS.Name = "lblCUTOFF_HIGHPASS";
            this.lblCUTOFF_HIGHPASS.Size = new System.Drawing.Size(29, 12);
            this.lblCUTOFF_HIGHPASS.TabIndex = 13;
            this.lblCUTOFF_HIGHPASS.Text = "5000";
            // 
            // lblRESONANCE_HIGHPASS
            // 
            this.lblRESONANCE_HIGHPASS.AutoSize = true;
            this.lblRESONANCE_HIGHPASS.ForeColor = System.Drawing.Color.Black;
            this.lblRESONANCE_HIGHPASS.Location = new System.Drawing.Point(345, 54);
            this.lblRESONANCE_HIGHPASS.Name = "lblRESONANCE_HIGHPASS";
            this.lblRESONANCE_HIGHPASS.Size = new System.Drawing.Size(23, 12);
            this.lblRESONANCE_HIGHPASS.TabIndex = 16;
            this.lblRESONANCE_HIGHPASS.Text = "100";
            // 
            // trbRESONANCE_HIGHPASS
            // 
            this.trbRESONANCE_HIGHPASS.AutoSize = false;
            this.trbRESONANCE_HIGHPASS.Location = new System.Drawing.Point(93, 46);
            this.trbRESONANCE_HIGHPASS.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.trbRESONANCE_HIGHPASS.Maximum = 1000;
            this.trbRESONANCE_HIGHPASS.Name = "trbRESONANCE_HIGHPASS";
            this.trbRESONANCE_HIGHPASS.Size = new System.Drawing.Size(247, 28);
            this.trbRESONANCE_HIGHPASS.TabIndex = 15;
            this.trbRESONANCE_HIGHPASS.Value = 100;
            this.trbRESONANCE_HIGHPASS.ValueChanged += new System.EventHandler(this.trbRESONANCE_HIGHPASS_ValueChanged);
            // 
            // chkOSCILLATOR
            // 
            this.chkOSCILLATOR.AutoSize = true;
            this.chkOSCILLATOR.Location = new System.Drawing.Point(3, 786);
            this.chkOSCILLATOR.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.chkOSCILLATOR.Name = "chkOSCILLATOR";
            this.chkOSCILLATOR.Size = new System.Drawing.Size(202, 16);
            this.chkOSCILLATOR.TabIndex = 35;
            this.chkOSCILLATOR.Text = "오실레이터 (OSCILLATOR) 사용";
            this.chkOSCILLATOR.UseVisualStyleBackColor = true;
            this.chkOSCILLATOR.CheckedChanged += new System.EventHandler(this.checkBox10_CheckedChanged);
            // 
            // groupBox10
            // 
            this.groupBox10.Controls.Add(this.numRATE_OSCILLATOR);
            this.groupBox10.Controls.Add(this.button35);
            this.groupBox10.Controls.Add(this.button34);
            this.groupBox10.Controls.Add(this.tbRATE_OSCILLATOR);
            this.groupBox10.Controls.Add(this.label33);
            this.groupBox10.Controls.Add(this.label32);
            this.groupBox10.Controls.Add(this.cbTYPE_OSCILLATOR);
            this.groupBox10.ForeColor = System.Drawing.Color.Blue;
            this.groupBox10.Location = new System.Drawing.Point(3, 808);
            this.groupBox10.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupBox10.Name = "groupBox10";
            this.groupBox10.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupBox10.Size = new System.Drawing.Size(455, 90);
            this.groupBox10.TabIndex = 34;
            this.groupBox10.TabStop = false;
            this.groupBox10.Text = "오실레이터 (OSCILLATOR)";
            // 
            // button35
            // 
            this.button35.ForeColor = System.Drawing.Color.Black;
            this.button35.Location = new System.Drawing.Point(391, 52);
            this.button35.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.button35.Name = "button35";
            this.button35.Size = new System.Drawing.Size(57, 22);
            this.button35.TabIndex = 18;
            this.button35.Text = "기본값";
            this.button35.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.button35.UseVisualStyleBackColor = true;
            this.button35.Click += new System.EventHandler(this.button35_Click);
            // 
            // button34
            // 
            this.button34.ForeColor = System.Drawing.Color.Black;
            this.button34.Location = new System.Drawing.Point(391, 20);
            this.button34.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.button34.Name = "button34";
            this.button34.Size = new System.Drawing.Size(57, 22);
            this.button34.TabIndex = 17;
            this.button34.Text = "기본값";
            this.button34.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.button34.UseVisualStyleBackColor = true;
            this.button34.Click += new System.EventHandler(this.button34_Click);
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.ForeColor = System.Drawing.Color.Black;
            this.label33.Location = new System.Drawing.Point(5, 58);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(37, 12);
            this.label33.TabIndex = 2;
            this.label33.Text = "RATE";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.ForeColor = System.Drawing.Color.Black;
            this.label32.Location = new System.Drawing.Point(5, 23);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(37, 12);
            this.label32.TabIndex = 1;
            this.label32.Text = "TYPE";
            // 
            // rbFMOD_System
            // 
            this.rbFMOD_System.AutoSize = true;
            this.rbFMOD_System.Enabled = false;
            this.rbFMOD_System.Location = new System.Drawing.Point(87, 65);
            this.rbFMOD_System.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.rbFMOD_System.Name = "rbFMOD_System";
            this.rbFMOD_System.Size = new System.Drawing.Size(91, 16);
            this.rbFMOD_System.TabIndex = 35;
            this.rbFMOD_System.Text = "Main Output";
            this.rbFMOD_System.UseVisualStyleBackColor = true;
            this.rbFMOD_System.CheckedChanged += new System.EventHandler(this.radioButton1_CheckedChanged);
            // 
            // rbFMOD_Channel
            // 
            this.rbFMOD_Channel.AutoSize = true;
            this.rbFMOD_Channel.Checked = true;
            this.rbFMOD_Channel.Enabled = false;
            this.rbFMOD_Channel.Location = new System.Drawing.Point(198, 65);
            this.rbFMOD_Channel.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.rbFMOD_Channel.Name = "rbFMOD_Channel";
            this.rbFMOD_Channel.Size = new System.Drawing.Size(70, 16);
            this.rbFMOD_Channel.TabIndex = 36;
            this.rbFMOD_Channel.TabStop = true;
            this.rbFMOD_Channel.Text = "Channel";
            this.rbFMOD_Channel.UseVisualStyleBackColor = true;
            this.rbFMOD_Channel.CheckedChanged += new System.EventHandler(this.radioButton2_CheckedChanged);
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("굴림", 9.209303F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label35.Location = new System.Drawing.Point(3, 66);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(68, 13);
            this.label35.TabIndex = 37;
            this.label35.Text = "할당 방법";
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.파일FToolStripMenuItem,
            this.설정TToolStripMenuItem,
            this.정렬ToolStripMenuItem,
            this.toolStripComboBox1,
            this.폴더선택ToolStripMenuItem,
            this.새로고침ToolStripMenuItem,
            this.제거ToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional;
            this.menuStrip1.Size = new System.Drawing.Size(966, 27);
            this.menuStrip1.TabIndex = 38;
            this.menuStrip1.Text = "menuStrip1";
            this.menuStrip1.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.menuStrip1_ItemClicked);
            // 
            // 파일FToolStripMenuItem
            // 
            this.파일FToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.새프리셋ToolStripMenuItem,
            this.toolStripSeparator2,
            this.프로파일열기OToolStripMenuItem,
            this.프로파일저장SToolStripMenuItem,
            this.toolStripSeparator1,
            this.닫기XToolStripMenuItem});
            this.파일FToolStripMenuItem.Name = "파일FToolStripMenuItem";
            this.파일FToolStripMenuItem.Size = new System.Drawing.Size(57, 23);
            this.파일FToolStripMenuItem.Text = "파일(&F)";
            this.파일FToolStripMenuItem.ToolTipText = "ㅁㄴㅇㅁㅇㅁㄴ";
            // 
            // 새프리셋ToolStripMenuItem
            // 
            this.새프리셋ToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("새프리셋ToolStripMenuItem.Image")));
            this.새프리셋ToolStripMenuItem.Name = "새프리셋ToolStripMenuItem";
            this.새프리셋ToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.N)));
            this.새프리셋ToolStripMenuItem.Size = new System.Drawing.Size(198, 22);
            this.새프리셋ToolStripMenuItem.Text = "새 프리셋";
            this.새프리셋ToolStripMenuItem.Click += new System.EventHandler(this.새프리셋ToolStripMenuItem_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(195, 6);
            // 
            // 프로파일열기OToolStripMenuItem
            // 
            this.프로파일열기OToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("프로파일열기OToolStripMenuItem.Image")));
            this.프로파일열기OToolStripMenuItem.Name = "프로파일열기OToolStripMenuItem";
            this.프로파일열기OToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.O)));
            this.프로파일열기OToolStripMenuItem.Size = new System.Drawing.Size(198, 22);
            this.프로파일열기OToolStripMenuItem.Text = "프리셋 열기(&O)";
            this.프로파일열기OToolStripMenuItem.Click += new System.EventHandler(this.프로파일열기OToolStripMenuItem_Click);
            // 
            // 프로파일저장SToolStripMenuItem
            // 
            this.프로파일저장SToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("프로파일저장SToolStripMenuItem.Image")));
            this.프로파일저장SToolStripMenuItem.Name = "프로파일저장SToolStripMenuItem";
            this.프로파일저장SToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
            this.프로파일저장SToolStripMenuItem.Size = new System.Drawing.Size(198, 22);
            this.프로파일저장SToolStripMenuItem.Text = "프리셋 저장(&S)";
            this.프로파일저장SToolStripMenuItem.Click += new System.EventHandler(this.프로파일저장SToolStripMenuItem_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(195, 6);
            // 
            // 닫기XToolStripMenuItem
            // 
            this.닫기XToolStripMenuItem.Image = global::HS_Audio.Properties.Resources.FillLeftHS;
            this.닫기XToolStripMenuItem.Name = "닫기XToolStripMenuItem";
            this.닫기XToolStripMenuItem.Size = new System.Drawing.Size(198, 22);
            this.닫기XToolStripMenuItem.Text = "닫기(&X)";
            this.닫기XToolStripMenuItem.Click += new System.EventHandler(this.닫기XToolStripMenuItem_Click);
            // 
            // 설정TToolStripMenuItem
            // 
            this.설정TToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.프리셋경로재설정ToolStripMenuItem,
            this.toolStripSeparator4,
            this.자동DSP등록ToolStripMenuItem});
            this.설정TToolStripMenuItem.Name = "설정TToolStripMenuItem";
            this.설정TToolStripMenuItem.Size = new System.Drawing.Size(57, 23);
            this.설정TToolStripMenuItem.Text = "설정(&T)";
            this.설정TToolStripMenuItem.DropDownOpening += new System.EventHandler(this.설정TToolStripMenuItem_DropDownOpening);
            // 
            // 프리셋경로재설정ToolStripMenuItem
            // 
            this.프리셋경로재설정ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.프리셋열기경로재설정ToolStripMenuItem,
            this.프리셋저장경로재설정ToolStripMenuItem1,
            this.toolStripSeparator3,
            this.프리셋폴더경로재설정ToolStripMenuItem});
            this.프리셋경로재설정ToolStripMenuItem.Name = "프리셋경로재설정ToolStripMenuItem";
            this.프리셋경로재설정ToolStripMenuItem.Size = new System.Drawing.Size(178, 22);
            this.프리셋경로재설정ToolStripMenuItem.Text = "프리셋 경로 재설정";
            this.프리셋경로재설정ToolStripMenuItem.Click += new System.EventHandler(this.프리셋경로재설정ToolStripMenuItem_Click);
            // 
            // 프리셋열기경로재설정ToolStripMenuItem
            // 
            this.프리셋열기경로재설정ToolStripMenuItem.Name = "프리셋열기경로재설정ToolStripMenuItem";
            this.프리셋열기경로재설정ToolStripMenuItem.Size = new System.Drawing.Size(206, 22);
            this.프리셋열기경로재설정ToolStripMenuItem.Text = "프리셋 열기 경로 재설정";
            this.프리셋열기경로재설정ToolStripMenuItem.Click += new System.EventHandler(this.프리셋열기경로재설정ToolStripMenuItem_Click);
            // 
            // 프리셋저장경로재설정ToolStripMenuItem1
            // 
            this.프리셋저장경로재설정ToolStripMenuItem1.Name = "프리셋저장경로재설정ToolStripMenuItem1";
            this.프리셋저장경로재설정ToolStripMenuItem1.Size = new System.Drawing.Size(206, 22);
            this.프리셋저장경로재설정ToolStripMenuItem1.Text = "프리셋 저장 경로 재설정";
            this.프리셋저장경로재설정ToolStripMenuItem1.Click += new System.EventHandler(this.프리셋저장경로재설정ToolStripMenuItem1_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(203, 6);
            // 
            // 프리셋폴더경로재설정ToolStripMenuItem
            // 
            this.프리셋폴더경로재설정ToolStripMenuItem.Name = "프리셋폴더경로재설정ToolStripMenuItem";
            this.프리셋폴더경로재설정ToolStripMenuItem.Size = new System.Drawing.Size(206, 22);
            this.프리셋폴더경로재설정ToolStripMenuItem.Text = "프리셋 폴더 경로 재설정";
            this.프리셋폴더경로재설정ToolStripMenuItem.Click += new System.EventHandler(this.프리셋폴더경로재설정ToolStripMenuItem_Click);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(175, 6);
            // 
            // 자동DSP등록ToolStripMenuItem
            // 
            this.자동DSP등록ToolStripMenuItem.Checked = true;
            this.자동DSP등록ToolStripMenuItem.CheckOnClick = true;
            this.자동DSP등록ToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.자동DSP등록ToolStripMenuItem.Name = "자동DSP등록ToolStripMenuItem";
            this.자동DSP등록ToolStripMenuItem.Size = new System.Drawing.Size(178, 22);
            this.자동DSP등록ToolStripMenuItem.Text = "자동 DSP 등록";
            this.자동DSP등록ToolStripMenuItem.CheckedChanged += new System.EventHandler(this.자동DSP등록ToolStripMenuItem_CheckedChanged);
            // 
            // 정렬ToolStripMenuItem
            // 
            this.정렬ToolStripMenuItem.AutoToolTip = true;
            this.정렬ToolStripMenuItem.BackColor = System.Drawing.Color.Transparent;
            this.정렬ToolStripMenuItem.Checked = true;
            this.정렬ToolStripMenuItem.CheckOnClick = true;
            this.정렬ToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.정렬ToolStripMenuItem.Enabled = false;
            this.정렬ToolStripMenuItem.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.정렬ToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("정렬ToolStripMenuItem.Image")));
            this.정렬ToolStripMenuItem.Name = "정렬ToolStripMenuItem";
            this.정렬ToolStripMenuItem.Size = new System.Drawing.Size(28, 23);
            this.정렬ToolStripMenuItem.ToolTipText = "콤보박스의 항목을 언어순에 따라 위로 정렬합니다.";
            this.정렬ToolStripMenuItem.CheckedChanged += new System.EventHandler(this.정렬ToolStripMenuItem_CheckedChanged);
            this.정렬ToolStripMenuItem.Click += new System.EventHandler(this.정렬ToolStripMenuItem_Click);
            // 
            // toolStripComboBox1
            // 
            this.toolStripComboBox1.AutoToolTip = true;
            this.toolStripComboBox1.Name = "toolStripComboBox1";
            this.toolStripComboBox1.Size = new System.Drawing.Size(180, 23);
            this.toolStripComboBox1.Text = "(기본값)";
            this.toolStripComboBox1.ToolTipText = "DSP 프리셋 목록 입니다,";
            this.toolStripComboBox1.SelectedIndexChanged += new System.EventHandler(this.toolStripComboBox1_SelectedIndexChanged);
            // 
            // 폴더선택ToolStripMenuItem
            // 
            this.폴더선택ToolStripMenuItem.AutoToolTip = true;
            this.폴더선택ToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("폴더선택ToolStripMenuItem.Image")));
            this.폴더선택ToolStripMenuItem.Name = "폴더선택ToolStripMenuItem";
            this.폴더선택ToolStripMenuItem.Size = new System.Drawing.Size(28, 23);
            this.폴더선택ToolStripMenuItem.ToolTipText = "현재 선택된 프리셋을 목록에서 제거합니다.";
            this.폴더선택ToolStripMenuItem.Click += new System.EventHandler(this.폴더선택ToolStripMenuItem_Click);
            // 
            // 새로고침ToolStripMenuItem
            // 
            this.새로고침ToolStripMenuItem.AutoToolTip = true;
            this.새로고침ToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("새로고침ToolStripMenuItem.Image")));
            this.새로고침ToolStripMenuItem.Name = "새로고침ToolStripMenuItem";
            this.새로고침ToolStripMenuItem.Size = new System.Drawing.Size(28, 23);
            this.새로고침ToolStripMenuItem.ToolTipText = "프리셋의 목록을 새로고침 합니다. ";
            this.새로고침ToolStripMenuItem.Click += new System.EventHandler(this.새로고침ToolStripMenuItem_Click);
            // 
            // 제거ToolStripMenuItem
            // 
            this.제거ToolStripMenuItem.AutoToolTip = true;
            this.제거ToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("제거ToolStripMenuItem.Image")));
            this.제거ToolStripMenuItem.Name = "제거ToolStripMenuItem";
            this.제거ToolStripMenuItem.Size = new System.Drawing.Size(28, 23);
            this.제거ToolStripMenuItem.ToolTipText = "현재 선택된 프리셋을 목록에서 제거합니다.\r\n(기본값은 제거되지 않습니다.)";
            this.제거ToolStripMenuItem.Click += new System.EventHandler(this.제거ToolStripMenuItem_Click);
            // 
            // saveFileDialog1
            // 
            this.saveFileDialog1.DefaultExt = "*.dspprof";
            this.saveFileDialog1.FileName = "제목 없음";
            this.saveFileDialog1.Filter = "DSP프리셋 (*.dspprof)|*.dspprof";
            this.saveFileDialog1.Title = "다른 이름으로 DSP 프로파일 저장";
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.DefaultExt = "*.dspprof";
            this.openFileDialog1.FileName = "제목 없음";
            this.openFileDialog1.Filter = "DSP프리셋 (*.dspprof)|*.dspprof";
            this.openFileDialog1.Title = "DSP 프로파일 열기";
            // 
            // folderBrowserDialog1
            // 
            this.folderBrowserDialog1.Description = "DSP관리자 프리셋이 들어있는 폴더 선택";
            // 
            // frmDSP
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(966, 746);
            this.Controls.Add(this.label35);
            this.Controls.Add(this.rbFMOD_Channel);
            this.Controls.Add(this.rbFMOD_System);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.btnDSPRegister);
            this.Controls.Add(this.btnDSPRemove);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.trbGain);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "frmDSP";
            this.ShowIcon = false;
            this.Text = "DSP 관리자";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmDSP_FormClosing);
            this.Load += new System.EventHandler(this.frmDSP_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trbMAXAMP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trbTHRESHHOLD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trbFADETIME)).EndInit();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trbWETMIX_ECHO)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trbDECAYRATIO)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trbDELAY_ECHO)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trbOVERLAP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trbPITCH)).EndInit();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trbDRYMIX_FLANGE)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trbWETMIX_FLANGE)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trbRATE_FLANGE)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trbDEPTH_FLANGE)).EndInit();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trbGAINMAKEUP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trbRELEASE)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trbATTACK)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trbTHRESHOLD)).EndInit();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trbRESONANCE)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trbCUTOFF)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trbGain)).EndInit();
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trbDISTORTION)).EndInit();
            this.groupBox8.ResumeLayout(false);
            this.groupBox8.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trbFEEDBACK_CHORUS)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trbWETMIX3_CHORUS)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trbWETMIX2_CHORUS)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trbWETMIX1_CHORUS)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trbDRYMIX_CHORUS)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trbDEPTH_CHORUS)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trbRATE_CHORUS)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trbDELAY_CHORUS)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trbWhaWhaSpeed)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trbWhaWhaDelay)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.groupBox9.ResumeLayout(false);
            this.groupBox9.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trbSHAPE)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trbSPREAD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trbPHASE)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trbSQUARE)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trbDUTY)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trbSKEW)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trbDEPTH_TREMOLO)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trbFREQUENCY)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbRATE_OSCILLATOR)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numRATE_OSCILLATOR)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.groupBox11.ResumeLayout(false);
            this.groupBox11.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trbCUTOFF_HIGHPASS)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trbRESONANCE_HIGHPASS)).EndInit();
            this.groupBox10.ResumeLayout(false);
            this.groupBox10.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.CheckBox chkNOMALIZER;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.TrackBar trbFADETIME;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblFADETIME;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lblTHRESHHOLD;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label lblMAXAMP;
        private System.Windows.Forms.TrackBar trbMAXAMP;
        private System.Windows.Forms.TrackBar trbTHRESHHOLD;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label lblWETMIX_ECHO;
        private System.Windows.Forms.TrackBar trbWETMIX_ECHO;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label lblDECAYRATIO;
        private System.Windows.Forms.TrackBar trbDECAYRATIO;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label lblDELAY;
        private System.Windows.Forms.TrackBar trbDELAY_ECHO;
        private System.Windows.Forms.CheckBox chkECHO;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.ComboBox comboBox2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TrackBar trbOVERLAP;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TrackBar trbPITCH;
        private System.Windows.Forms.ComboBox cbFFTSIZE;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.CheckBox chkPITCHSHIFT;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label lblWETMIX_FLANGE;
        private System.Windows.Forms.TrackBar trbWETMIX_FLANGE;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label lblRATE_FLANGE;
        private System.Windows.Forms.TrackBar trbRATE_FLANGE;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label lblDEPTH_FLANGE;
        private System.Windows.Forms.TrackBar trbDEPTH_FLANGE;
        private System.Windows.Forms.CheckBox chkFLANGE;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Button button10;
        private System.Windows.Forms.Button button11;
        private System.Windows.Forms.Button button12;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label lblRELEASE;
        private System.Windows.Forms.TrackBar trbRELEASE;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label lblATTACK;
        private System.Windows.Forms.TrackBar trbATTACK;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label lblTHRESHOLD;
        private System.Windows.Forms.TrackBar trbTHRESHOLD;
        private System.Windows.Forms.CheckBox chkCOMPRESSOR;
        private System.Windows.Forms.Button button13;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label lblGAINMAKEUP;
        private System.Windows.Forms.TrackBar trbGAINMAKEUP;
        private System.Windows.Forms.Button button14;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label lblDRYMIX_FLANGE;
        private System.Windows.Forms.TrackBar trbDRYMIX_FLANGE;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.Button button17;
        private System.Windows.Forms.Button button18;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label lblRESONANCE;
        private System.Windows.Forms.TrackBar trbRESONANCE;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Label lblCUTOFF;
        private System.Windows.Forms.TrackBar trbCUTOFF;
        private System.Windows.Forms.CheckBox chkLOWPASS;
        private System.Windows.Forms.TrackBar trbGain;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.Button button16;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label lblDISTORTION;
        private System.Windows.Forms.TrackBar trbDISTORTION;
        private System.Windows.Forms.CheckBox chkDISTORTION;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label lblDEPTH_CHORUS;
        private System.Windows.Forms.TrackBar trbDEPTH_CHORUS;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label lblRATE_CHORUS;
        private System.Windows.Forms.TrackBar trbRATE_CHORUS;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label lblDELAY_CHORUS;
        private System.Windows.Forms.TrackBar trbDELAY_CHORUS;
        private System.Windows.Forms.CheckBox chkCHORUS;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label lblWETMIX1_CHORUS;
        private System.Windows.Forms.TrackBar trbWETMIX1_CHORUS;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.Label lblDRYMIX_CHORUS;
        private System.Windows.Forms.TrackBar trbDRYMIX_CHORUS;
        private System.Windows.Forms.Label lblWETMIX3_CHORUS;
        private System.Windows.Forms.Label lblWETMIX2_CHORUS;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.TrackBar trbWETMIX3_CHORUS;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.TrackBar trbWETMIX2_CHORUS;
        private System.Windows.Forms.Button button15;
        private System.Windows.Forms.Button button19;
        private System.Windows.Forms.Button button20;
        private System.Windows.Forms.Button button21;
        private System.Windows.Forms.Button button22;
        private System.Windows.Forms.Button button23;
        private System.Windows.Forms.Button button24;
        private System.Windows.Forms.Button btnWhaWha;
        private System.Windows.Forms.TrackBar trbWhaWhaSpeed;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TrackBar trbWhaWhaDelay;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button button25;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label lblFEEDBACK_CHORUS;
        private System.Windows.Forms.TrackBar trbFEEDBACK_CHORUS;
        private System.Windows.Forms.Button btnDSPRemove;
        private System.Windows.Forms.Button btnDSPRegister;
        private System.Windows.Forms.GroupBox groupBox9;
        private System.Windows.Forms.Button button26;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label lblSHAPE;
        private System.Windows.Forms.TrackBar trbSHAPE;
        private System.Windows.Forms.Button button27;
        private System.Windows.Forms.Button button28;
        private System.Windows.Forms.Button button29;
        private System.Windows.Forms.Button button30;
        private System.Windows.Forms.Button button31;
        private System.Windows.Forms.Button button32;
        private System.Windows.Forms.Button button33;
        private System.Windows.Forms.Label lblSPREAD;
        private System.Windows.Forms.Label lblPHASE;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.TrackBar trbSPREAD;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.TrackBar trbPHASE;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.Label lblSQUARE;
        private System.Windows.Forms.TrackBar trbSQUARE;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.Label lblDUTY;
        private System.Windows.Forms.TrackBar trbDUTY;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.Label lblSKEW;
        private System.Windows.Forms.TrackBar trbSKEW;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.Label lblDEPTH_TREMOLO;
        private System.Windows.Forms.TrackBar trbDEPTH_TREMOLO;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.Label lblFREQUENCY;
        private System.Windows.Forms.TrackBar trbFREQUENCY;
        private System.Windows.Forms.CheckBox chkTREMOLO;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.CheckBox chkOSCILLATOR;
        private System.Windows.Forms.GroupBox groupBox10;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.ComboBox cbTYPE_OSCILLATOR;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.NumericUpDown numRATE_OSCILLATOR;
        private System.Windows.Forms.Button button35;
        private System.Windows.Forms.Button button34;
        private System.Windows.Forms.TrackBar tbRATE_OSCILLATOR;
        private System.Windows.Forms.RadioButton rbFMOD_System;
        private System.Windows.Forms.RadioButton rbFMOD_Channel;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem 파일FToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 프로파일열기OToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 프로파일저장SToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem 닫기XToolStripMenuItem;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.ToolStripComboBox toolStripComboBox1;
        private System.Windows.Forms.ToolStripMenuItem 새로고침ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 새프리셋ToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem 제거ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 정렬ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 폴더선택ToolStripMenuItem;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
        private System.Windows.Forms.ToolStripMenuItem 설정TToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 프리셋경로재설정ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 프리셋열기경로재설정ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 프리셋저장경로재설정ToolStripMenuItem1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripMenuItem 프리셋폴더경로재설정ToolStripMenuItem;
        private System.Windows.Forms.Button button38;
        private System.Windows.Forms.Button button37;
        private System.Windows.Forms.Button button36;
        private System.Windows.Forms.CheckBox chkHIGHPASS;
        private System.Windows.Forms.GroupBox groupBox11;
        private System.Windows.Forms.Button button39;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.Button button40;
        private System.Windows.Forms.TrackBar trbCUTOFF_HIGHPASS;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.Label lblCUTOFF_HIGHPASS;
        private System.Windows.Forms.Label lblRESONANCE_HIGHPASS;
        private System.Windows.Forms.TrackBar trbRESONANCE_HIGHPASS;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripMenuItem 자동DSP등록ToolStripMenuItem;
    }
}