﻿using HS_Audio.DSP;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Reflection;
using System.Text;
using System.Windows.Forms;

namespace HS_Audio.Forms//HS_Audio.DSPHelper.Forms
{
    public partial class frmSoundScript : Form
    {
        string LastText;
        string Title = "제목 없음";
        HSAudioHelper Helper;
        HSAudioDSPHelper dsp;

        #region Processing
        public unsafe HS_Audio_Lib.RESULT READCALLBACK(ref HS_Audio_Lib.DSP_STATE dsp_state, IntPtr InBuf, IntPtr OutBuf, uint length, int inchannels, int outchannels)
        {
            if (Method != null)
                try
                {
                    HS_Audio.Utils.DynaInvoke.InvokeMethodSlow(Method, "SoundScript", "ProcessMain", new object[] { InBuf, OutBuf, length, inchannels, outchannels });
                    //dynamic d = Method; d.ProcessMain(InBuf, OutBuf, length, inchannels, outchannels);
                } catch { }
            else
            {
                float* inbuffer = (float*)InBuf.ToPointer();
                float* outbuffer = (float*)OutBuf.ToPointer();

                int index = 0;

                for (int count = 0; count < length; count++)
                {
                    for (int ChannelCnt = 0; ChannelCnt < outchannels; ChannelCnt++)
                    {
                        index = (count * outchannels) + ChannelCnt;
                        outbuffer[index] = inbuffer[index];
                    }
                }
            }
            return HS_Audio_Lib.RESULT.OK;
        }
        #endregion

        #region 내부 함수
        void SetEditor(string Text)
        {
            fastColoredTextBox1.Dispose();
            /*
            for(int i = 0; i < this.Controls.Count; i++)
                if(Controls[i].Name == "fastColoredTextBox1")*/
            fastColoredTextBox1 = new FastColoredTextBoxNS.FastColoredTextBox();

            this.fastColoredTextBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
            | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            this.fastColoredTextBox1.Location = new System.Drawing.Point(0, 2);
            this.fastColoredTextBox1.Language = FastColoredTextBoxNS.Language.CSharp;
            this.fastColoredTextBox1.Size = new Size(tabPage1.Width, tabPage1.Height - 2);
            this.fastColoredTextBox1.Text = Text;
            this.fastColoredTextBox1.PreferredLineWidth = 10000;

            this.splitContainer1.Panel1.Controls.Add(fastColoredTextBox1);

            GC.Collect();
        }

        /// <summary>
        /// 리소스에서 갔다쓰기 싫어서 걍 이렇게 만듦 ㅎㅎ
        /// </summary>
        /// <returns></returns>
        static string GetNewScript()
        {
            StringBuilder sb = new StringBuilder();

            sb.AppendLine("//이 using 문은 지우지 마세요. (Please, don't remove this using statement.)");
            sb.AppendLine("using System;");
            sb.AppendLine("//여기서부터 using 문을 추가 / 삭제 하시면 됩니다. (From here, you can add / delete using statement.)");
            sb.AppendLine("using System.Windows.Forms;");
            sb.AppendLine("using System.Drawing;");
            sb.AppendLine("using System.IO;");
            sb.AppendLine("using System.Text;");
            sb.AppendLine();
            sb.AppendLine("//이 네임스페이스를 변경하지 마세요. (Please, don't modify this namespace)");
            sb.AppendLine("namespace HSScript");
            sb.AppendLine("{");
            sb.AppendLine("    //이 클래스 이름을 변경하지 마세요. (Please, don't modify this class)");
            sb.AppendLine("    class SoundScript");
            sb.AppendLine("    {");
            sb.AppendLine("        /* 실시간 사운드 프로세싱 메서드 입니다. (Realtime Sound Processing Method)");
            sb.AppendLine("        (InBuf=입력사운드_배열 포인터, OutBuf=출력사운드_배열_포인터, length=논리적_배열길이, inchannels=입력채널갯수, outchannels=출력채널갯수) */");
            sb.AppendLine("        //이 함수와 매개변수를 변경하지 마세요. (Please, don't modify this method, parameter)");
            sb.AppendLine("        public unsafe void ProcessMain(IntPtr InBuf, IntPtr OutBuf, uint length, int inchannels, int outchannels)");
            sb.AppendLine("        {");
            sb.AppendLine("            float* inbuffer = (float*)InBuf.ToPointer();");
            sb.AppendLine("            float* outbuffer = (float*)OutBuf.ToPointer();");
            sb.AppendLine("            ");
            sb.AppendLine("            int index = 0;");
            sb.AppendLine("            ");
            sb.AppendLine("            for (int count = 0; count < length; count++)");
            sb.AppendLine("            {");
            sb.AppendLine("                for (int ChannelCnt = 0; ChannelCnt < outchannels; ChannelCnt++)");
            sb.AppendLine("                {");
            sb.AppendLine("                    index = (count * outchannels) + ChannelCnt;");
            sb.AppendLine("                    ");
            sb.AppendLine("                    outbuffer[index] = inbuffer[index];");
            sb.AppendLine("                }");
            sb.AppendLine("            }");
            sb.AppendLine("        }");
            sb.AppendLine("        ");
            sb.AppendLine("        /*빌드(컴파일)후 1번만 실행되는 함수 입니다.*/");
            sb.AppendLine("        //(매개변수를 변경하지 마세요. (Please, don't modify this parameter))");
            sb.AppendLine("        public void OnOnce(bool On, bool IsBypass, bool IsSystemAlloc)");
            sb.AppendLine("        {");
            sb.AppendLine("             //컴파일 성공시 현재 정보 메세지박스 출력");
            sb.AppendLine("             MessageBox.Show(\"빌드(컴파일) 성공!!\\n\\n현재 음향효과 적용여부: [\"+ (On ? \"켜짐\" : \"꺼짐\") + \"]\", \"HS 사운드 스크립트\");");
            sb.AppendLine("        }");
            sb.AppendLine("        ");
            sb.AppendLine("        /*실행상태가 변경되면 실행되는 함수 입니다.*/");
            sb.AppendLine("        //(매개변수를 변경하지 마세요. (Please, don't modify this parameter))");
            sb.AppendLine("        public void OnRun(bool IsRun)");
            sb.AppendLine("        {");
            sb.AppendLine("             //스크립트 실행여부 메세지박스 출력");
            sb.AppendLine("             MessageBox.Show(\"스크립트가 \"+(IsRun?\"실행됨\":\"실행멈춤\"), \"HS 사운드 스크립트\");");
            sb.AppendLine("        }");
            sb.AppendLine("        ");
            sb.AppendLine("        /*현재 스크립트가 언로드되기전에 실행됩니다. (각종 핸들등 해제할 개체가있으면 이 문에 서술해주세요)*/");
            sb.AppendLine("        public void OnDispose()");
            sb.AppendLine("        {");
            sb.AppendLine("             //스크립트가 언로드될때 메세지박스 출력");
            sb.AppendLine("             MessageBox.Show(\"이 스크립트 인스턴스는 언로드 됩니다.\", \"HS 사운드 스크립트\");");
            sb.AppendLine("        }");
            sb.AppendLine("        ");
            sb.AppendLine("        /*사운드 켜짐상태가 변경되면 발생되는 이벤트메서드(함수) 입니다.*/");
            sb.AppendLine("        //(매개변수를 변경하지 마세요. (Please, don't modify this parameter))");
            sb.AppendLine("        public void OnCheck(bool On)");
            sb.AppendLine("        {");
            sb.AppendLine("            "); 
            sb.AppendLine("        }");
            sb.AppendLine("        ");
            sb.AppendLine("        /*사운드가 우회되면 발생되는 이벤트메서드(함수) 입니다.*/");
            sb.AppendLine("        //(매개변수를 변경하지 마세요. (Please, don't modify this parameter))");
            sb.AppendLine("        public void OnBypass(bool Bypassed)");
            sb.AppendLine("        {");
            sb.AppendLine("            ");
            sb.AppendLine("        }");
            sb.AppendLine("        ");
            sb.AppendLine("        /*사운드할당이 변경되면 발생되는 이벤트메서드(함수) 입니다.*/");
            sb.AppendLine("        //(매개변수를 변경하지 마세요. (Please, don't modify this parameter))");
            sb.AppendLine("        public void OnAlloc(bool IsSystem)");
            sb.AppendLine("        {");
            sb.AppendLine("            ");
            sb.AppendLine("        }");
            sb.AppendLine("    }");
            sb.AppendLine("}");

            return sb.ToString();
        }
        #endregion

        #region 공개 메서드
        public static string ToKorean(DateTime time, bool 오전오후 = true, bool MilliSecond = false)
        {
            StringBuilder sb = new StringBuilder(time.Year.ToString()); sb.Append("년 ");
            sb.Append(time.Month.ToString("00")); sb.Append("월 ");
            sb.Append(time.Day.ToString("00")); sb.AppendFormat("일 ");
            if (오전오후)
            {
                if (time.Hour == 0) sb.Append("오전 12시 ");
                else if (time.Hour < 12) { sb.Append("오전 "); sb.Append(time.Hour.ToString("00")); sb.Append("시 "); }
                else if (time.Hour == 12) sb.Append("오후 12시 ");
                else { sb.Append("오후 "); sb.Append((time.Hour - 12).ToString("00")); sb.Append("시 "); }
            }
            else { sb.Append(time.Hour.ToString("00")); sb.AppendFormat("시 "); }
            sb.Append(time.Minute.ToString("00")); sb.AppendFormat("분 ");
            if (MilliSecond) { sb.Append(time.Second.ToString("00")); sb.Append("."); sb.Append(time.Millisecond.ToString("000")); sb.Append("초"); }
            else sb.Append(time.Second.ToString("00")); sb.AppendFormat("초");
            return sb.ToString();
        }
        public static string ToKorean(TimeSpan time, bool 오전오후 = true, bool MilliSecond = false)
        {
            StringBuilder sb = new StringBuilder();
            if (오전오후)
            {
                if (time.Hours == 0) sb.Append("오전 12시 ");
                else if (time.Hours < 12) { sb.Append("오전 "); sb.Append(time.Hours.ToString("00")); sb.Append("시 "); }
                else if (time.Hours == 12) sb.Append("오후 12시 ");
                else { sb.Append("오후 "); sb.Append((time.Hours - 12).ToString("00")); sb.Append("시 "); }
            }
            else { sb.Append(time.Hours.ToString("00")); sb.AppendFormat("시 "); }
            sb.Append(time.Minutes.ToString("00")); sb.AppendFormat("분 ");
            if (MilliSecond) { sb.Append(time.Seconds.ToString("00")); sb.Append("."); sb.Append(time.Milliseconds.ToString("000")); sb.Append("초"); }
            else sb.Append(time.Seconds.ToString("00")); sb.AppendFormat("초");
            return sb.ToString();
        }
        #endregion

        public frmSoundScript(HSAudioHelper Helper)
        {
            InitializeComponent();
            this.Helper = Helper;

            dsp = new HSAudioDSPHelper(Helper, READCALLBACK, true);
            //this.intellisenseTextBox1.PopulateAutoCompleteList += IntellisenseTextBox1_PopulateAutoCompleteList;
            //this.intellisenseTextBox1.PopulateToolTipList += IntellisenseTextBox1_PopulateAutoCompleteList;

        }

        private void IntellisenseTextBox1_PopulateAutoCompleteList(object sender, Control.AutoCompletionEventArgs e)
        {
            //e.AutoCompleteValues = this.ruleParser.GetExpressionCompletions(e.Prefix);
        }

        private void frmSoundScript_Load(object sender, EventArgs e)
        {
            this.Icon = Properties.Resources.CSharp_Script_Icon;
            
            tabControl1.TabPages.Remove(tabControl1.TabPages[1]);//설정페이지 구현되면 이 문장 지우기
            tabControl2.TabPages.Remove(tabControl2.TabPages[2]);//콘솔창 구현되면 이 문장 지우기

            toolStripComboBox1.SelectedIndex = 0;

            string a = GetNewScript();
            SetText(a);
            LastText = a;

            Text = Title + " - HS 사운드 스크립트";
            Changed = false;

            //최초 1번만 클릭 실행
            btnOn_Click(null, null);

            //btnRun1.Click += 실행및적용ToolStripMenuItem_Click;
        }

        private void 새스크립트문ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (Prompt())
            {
                //fastColoredTextBox1.Text = sb.ToString();
                string a = GetNewScript();
                SetText(a);
                LastText = a;

                Title = "제목 없음";
                Text = Title + " - HS 사운드 스크립트";
                Changed = false;
            }
        }

        Assembly Method;
        Assembly MethodCompile;
        private void btnCompile_Click(object sender, EventArgs e)
        {
            txtOutput.Clear();
            listView1.Items.Clear();
            tabControl2.SelectedIndex = 1;

            bool IsChange = this.Changed;
            //색깔을 초기화한다음 원래 커서위치에다 갔다 놓기
            ClearCursor();

            if (!IsChange) { this.Changed = IsChange; Text = Title + " - HS 사운드 스크립트"; }

            //txtOutput.AppendText("------ 빌드 시작(Build started): 이름(Name): " + Title+", 시간(Time): " + ToKorean(DateTime.Now, true, false)+ " ------\r\n");
            txtOutput.AppendText("------ 빌드 시작: 이름: " + Title + ", 시간: " + ToKorean(new TimeSpan(DateTime.Now.Ticks), true, false) + " ------\r\n");
            try 
            {
                //Method = CSScriptLibrary.CSScript.LoadCode(fastColoredTextBox1.Text);
                Assembly LastMethod = Method;
                Assembly LastMethodCompile = MethodCompile;

                MethodCompile = CSScriptLibrary.CSScript.LoadCode(일반편집기사용ToolStripMenuItem.Checked?txtCode.Text:fastColoredTextBox1.Text); //CSScriptLibrary.CSScript.LoadCode(fastColoredTextBox1.Text, ref App_domain);
                if (MethodCompile == null) throw new Exception("스크립트에 오류가 있습니다.");
                else lblStatus.Text = "성공";

                if(!MethodCompile.Equals(LastMethodCompile)) ClearMethod(LastMethodCompile);
                
                //txtOutput.AppendText("\r\n========== 빌드(Build): 1 성공(succeeded), 0 오류(error), 0 경고(warning) ==========");
                txtOutput.AppendText("\r\n========== 빌드: 1 성공, 0 오류, 0 경고 ==========");

                try
                {
                    HS_Audio.Utils.DynaInvoke.InvokeMethodSlow(MethodCompile, "SoundScript", "OnOnce", new object[] { btnOn_Check, btnBypass_Check, toolStripComboBox1.SelectedIndex == 0 }); 
                    //dynamic d = MethodCompile; d.OnRun(btnOn_Check, btnBypass_Check, toolStripComboBox1.SelectedIndex == 0);
                } catch { }

                if (IsRun)
                { 
                    try
                    {
                        HS_Audio.Utils.DynaInvoke.InvokeMethodSlow(MethodCompile, "SoundScript", "OnRun", new object[] { true });
                        //dynamic d = MethodCompile; d.OnRun(true);
                    } catch { }

                    Method = MethodCompile;
                    if(LastMethod!=null && !MethodCompile.Equals(LastMethod) && !LastMethod.Equals(LastMethodCompile)) ClearMethod(LastMethod);
                }
                if(MethodCompile != null)btnRun.Enabled = true;
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.ToString());
                tabControl2.SelectedIndex = 0;
                string[] a = ex.Message.Split('\r', '\n');

                int error = 0, warn = 0;

                for (int i = 0; i < a.Length; i++)
                {
                    //수준, 오류코드, 위치, 오류설명, 소스파일
                    string[] b = a[i].Split(' ');

                    if (b.Length >= 4)
                    {
                        if (b[1].ToUpper() == "WARNING") warn++;
                        else error++;

                        if (b[1].ToUpper() == "WARNING" && 경고숨기기ToolStripMenuItem.Checked) continue;

                        //오류수준 파싱
                        ListViewItem li = new ListViewItem(b[1].ToUpper()== "WARNING"?"경고(Warning)":"오류(Error)");

                        //오류코드 파싱
                        li.SubItems.Add(b[2].Replace(":", ""));

                        //위치 파싱
                        string c = b[0].Substring(b[0].LastIndexOf("(") + 1);
                        string[] c1 = c.Split(',');
                        li.SubItems.Add(string.Format("줄: {0}, 열: {1}", GetInt(c1[0]), GetInt(c1[1])));
                        li.Tag = GetInt(c1[0])+","+GetInt(c1[1]);

                        //오류설명 파싱
                        StringBuilder sb = new StringBuilder();
                        for (int j = 3; j < b.Length; j++) sb.Append(b[j] + " ");
                        li.SubItems.Add(sb.ToString());

                        //소스파일 파싱
                        li.SubItems.Add(b[0].Remove(b[0].LastIndexOf("(")));

                        listView1.Items.Add(li);
                    }
                }
                listView1.Sorting = SortOrder.Ascending;
                listView1.Sort();

                txtOutput.AppendText(ex.Message);
                lblStatus.Text = "빌드(컴파일) 실패!!";

                // 0 성공(succeeded),
                txtOutput.AppendText(string.Format("\r\n========== 빌드(Build): 0 성공(succeeded), {0} 오류(error), {1} 경고(warning) ==========", error, warn));

                //sender 가 true면 예외 던지기.
                bool ThrowException = false;
                try { ThrowException = (bool)sender; } catch { }
                if (ThrowException) throw ex;
            }
        }
        int GetInt(string Text)
        {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < Text.Length; i++)
                if (Text[i] > 47 && Text[i] < 58) sb.Append(Text[i]);
            return Convert.ToInt32(sb.ToString());
        }

        bool btnBypass_Check;
        private void btnBypass_Click(object sender, EventArgs e)
        {
            btnBypass_Check = !btnBypass_Check;
            dsp.Bypass = btnBypass_Check;
            btnBypass.Image = btnBypass_Check ? Properties.Resources.Checkbox_Check : Properties.Resources.Checkbox_UnCheck;

            if (Method != null)
                try
                {
                    HS_Audio.Utils.DynaInvoke.InvokeMethodSlow(Method, "SoundScript", "OnBypass", new object[] { btnBypass_Check });
                    //dynamic d = Method; d.OnBypass(btnBypass_Check);
                }
                catch { }
        }
        bool btnOn_Check;
        private void btnOn_Click(object sender, EventArgs e)
        {
            btnOn_Check = !btnOn_Check;
            if (btnOn_Check) dsp.ReRegisterDSP(toolStripComboBox1.SelectedIndex == 0);//a = dsp.Connect(toolStripComboBox1.SelectedIndex == 0);
            else dsp.DisConnect();
            btnOn.Image = btnOn_Check ? Properties.Resources.Checkbox_Check : Properties.Resources.Checkbox_UnCheck;

            if (Method != null)
                try
                {
                    HS_Audio.Utils.DynaInvoke.InvokeMethodSlow(Method, "SoundScript", "OnCheck", new object[] { btnOn_Check });
                    //dynamic d = Method; d.OnCheck(btnOn_Check);
                }
                catch { }
        }

        string PathScript
        {
            get
            {
                string a = Application.StartupPath.Replace("/", "\\") + "\\Script";
                if (!Directory.Exists(a))try  { Directory.CreateDirectory(a); }
                    catch { }
                return a;
            }
        }

        #region 파일 입출력
        private void 열기ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (Prompt())
            {
                openFileDialog1.InitialDirectory = PathScript;
                if (openFileDialog1.ShowDialog() == DialogResult.OK)
                {
                    LastText = File.ReadAllText(openFileDialog1.FileName);
                    SetText(LastText);
                    Title = Path.GetFileNameWithoutExtension(openFileDialog1.FileName);
                    Text = Title + " - HS 사운드 스크립트";
                    Changed = false;
                }
            }
        }

        private void 저장ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Changed = !Save();
        }
        private void 다른이름으로스크립트저장ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            saveFileDialog1.InitialDirectory = PathScript;
            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    File.WriteAllText(saveFileDialog1.FileName, GetText());
                    Changed = false;
                    openFileDialog1.FileName = saveFileDialog1.FileName;
                    Title = Path.GetFileNameWithoutExtension(saveFileDialog1.FileName);
                    Text = Title + " - HS 사운드 스크립트";
                }
                catch { }
            }
        }

        bool Prompt()
        {
            if (Changed)//LastText != GetText())
            {
                DialogResult dr = MessageBox.Show(Title + "의 내용이 변경되었습니다. 저장하시겠습니까?", "HS 사운드 스크립트", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning);
                if (dr == DialogResult.Yes) { Changed = !Save(); return !Changed; }
                else if (dr == DialogResult.No) return true;
                else return false;
            }
            return true;
        }

        bool Save()
        {
            if (Changed)// || LastText != GetText())
            {
                //openFileDialog1.FileName != "" && openFileDialog1.FileName != null && 
                if (openFileDialog1.FileName != "" && openFileDialog1.FileName != null)
                    try { File.WriteAllText(openFileDialog1.FileName, GetText()); LastText = GetText(); Text = Title + " - HS 사운드 스크립트"; Changed = false; } catch { return false; }
                else
                {
                    saveFileDialog1.InitialDirectory = PathScript;
                    DialogResult dr = saveFileDialog1.ShowDialog();
                    if (dr == DialogResult.OK)
                    {
                        File.WriteAllText(saveFileDialog1.FileName, GetText());
                        Changed = false;
                        openFileDialog1.FileName = saveFileDialog1.FileName;
                        Title = Path.GetFileNameWithoutExtension(saveFileDialog1.FileName);
                        Text = Title + " - HS 사운드 스크립트";
                    }
                    return dr == DialogResult.OK;
                }
            }
            return true;
        }
        void MoveCursor(int Line, int Colume, Color color, bool ClearColor)
        {
            if(txtCode.Visible)
            {
                //ClearCursor();
                int index = txtCode.GetFirstCharIndexFromLine(Line);
                txtCode.Select(index, txtCode.Lines[Line].Length);//Colume
                txtCode.SelectionColor = color;
                txtCode.ScrollToCaret();
            }
            else
            {
                fastColoredTextBox1.Selection = new FastColoredTextBoxNS.Range(fastColoredTextBox1, new FastColoredTextBoxNS.Place(Convert.ToInt32(Colume), Convert.ToInt32(Line)), 
                    new FastColoredTextBoxNS.Place(Convert.ToInt32(Colume), Convert.ToInt32(Line)));
            }
        }
        void ClearCursor()
        {
            int Position = txtCode.SelectionStart;
            txtCode.Select(0, txtCode.TextLength);
            txtCode.SelectionColor = Color.Black;
            txtCode.Select(Position, 0);
            //txtCode.SelectionStart = Position;
        }
        #endregion

        private void 오류목록모눈선표시ToolStripMenuItem_CheckedChanged(object sender, EventArgs e)
        {
            listView1.GridLines = 오류목록모눈선표시ToolStripMenuItem.Checked;
        }

        private void 일반편집기사용ToolStripMenuItem_CheckedChanged(object sender, EventArgs e)
        {
            if (일반편집기사용ToolStripMenuItem.Checked) txtCode.Text = fastColoredTextBox1.Text;
            else SetEditor(txtCode.Text);

            fastColoredTextBox1.Visible = !일반편집기사용ToolStripMenuItem.Checked;
            txtCode.Visible = 일반편집기사용ToolStripMenuItem.Checked;
        }
        public void SetText(string Text)
        {
            fastColoredTextBox1.Text = Text;
            txtCode.Text = Text;
            //txtCode.Text = new ColorCode.CodeColorizer().Colorize(Text, ColorCode.Languages.CSharp);

        }
        public string GetText() { return fastColoredTextBox1.Visible?fastColoredTextBox1.Text:txtCode.Text; }

        private void 줄번호표시ToolStripMenuItem_CheckedChanged(object sender, EventArgs e)
        {
            fastColoredTextBox1.ShowLineNumbers = 줄번호표시ToolStripMenuItem.Checked;
        }

        private void listView1_SelectedIndexChanged(object sender, EventArgs e)
        {
            bool IsChange = this.Changed;

            ClearCursor();
            for (int i = 0; i < listView1.SelectedItems.Count; i++)
            {
                string[] a = listView1.SelectedItems[i].Tag.ToString().Split(',');
                MoveCursor(Convert.ToInt32(a[0]) - 1, Convert.ToInt32(a[1]) - 1, Color.Red, true);
            }
            //else if (listView1.SelectedItems.Count < 1) ClearCursor();

            if (!IsChange) { this.Changed = IsChange; Text = Title + " - HS 사운드 스크립트"; }
        }

        private void toolStripComboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (btnOn_Check) dsp.ReRegisterDSP(toolStripComboBox1.SelectedIndex == 0);
            if (Method != null)
                try
                {
                    HS_Audio.Utils.DynaInvoke.InvokeMethodSlow(Method, "SoundScript", "OnAlloc", new object[] { toolStripComboBox1.SelectedIndex == 0 });
                    //dynamic d = Method; d.OnAlloc(toolStripComboBox1.SelectedIndex == 0);
                }
                catch { }
        }

        bool IsRun;
        private void 실행및적용ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if(!IsRun)
            {
                try { if(MethodCompile == null) btnCompile_Click(true, null); }
                catch (Exception ex)
                {
                    if (MethodCompile!=null && MessageBox.Show("컴파일(빌드)에 실패하였습니다. \n\n마지막으로 성공한 빌드를 실행하시겠습니까?", "HS 플레이어 사운드 스크립트", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                        IsRun = true;
                    else { IsRun = false; return; }
                }

                Method = MethodCompile;
                //if (Method != null) 
                try
                {
                    Utils.DynaInvoke.InvokeMethodSlow(Method, "SoundScript", "OnRun", new object[] { true });
                    //dynamic d = Method; d.OnRun(true);
                }
                catch { }

                실행및적용ToolStripMenuItem.Image = btnRun.Image = Properties.Resources.StopRun;
                실행및적용ToolStripMenuItem.Text = btnRun.Text = btnRun.ToolTipText = "스크립트 실행 멈춤";
                
            }
            else
            {
                실행및적용ToolStripMenuItem.Image = btnRun.Image = Properties.Resources.Run;
                실행및적용ToolStripMenuItem.Text = btnRun.Text = btnRun.ToolTipText = "스크립트 실행";
                Assembly tmp = Method;
                Method = null;
                try
                {
                    HS_Audio.Utils.DynaInvoke.InvokeMethodSlow(tmp, "SoundScript", "OnRun", new object[] { false });
                    //dynamic d = Method; d.OnRun(false);
                }
                catch { }
            }
            IsRun = !IsRun;
        }
        private void btnRunWithoutDebugging_Click(object sender, EventArgs e)
        {

        }

        private void toolStripMenuItem2_Click(object sender, EventArgs e)
        {

        }

        bool Changed;
        private void txtCode_TextChanged(object sender, EventArgs e)
        {
            Changed = true;
            this.Text = Title + " [*] - HS 사운드 스크립트";
        }

        private void txtCode_TextChanged(object sender, FastColoredTextBoxNS.TextChangingEventArgs e)
        {
            Changed = true;
            this.Text = Title + " [*] - HS 사운드 스크립트";
        }

        private void 효과초기화ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if(MessageBox.Show("정말 빌드(컴파일)된 음향효과를 초기화 하시겠습니까?", "HS 플레이어 사운드 스크립트", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
            {
                ClearMethod();
                ClearMethodCompile();
                IsRun = false;
                btnRun.Image = Properties.Resources.Run;
                btnRun.Enabled = false;
            }
        }

        #region 어셈블리 초기화
        void ClearMethod(Assembly Method)
        {
            if (Method != null) try { HS_Audio.Utils.DynaInvoke.InvokeMethodSlow(Method, "SoundScript", "OnDispose", null); } catch { }
        }
        void ClearMethod() { ClearMethod(Method); Method = null; }
        void ClearMethodCompile() { ClearMethod(MethodCompile); MethodCompile = null; }
        #endregion

        private void frmSoundScript_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = true;
            DialogResult dr = MessageBox.Show("이 창을 닫게되면 스크립트 실행이 멈춥니다.\n\n정말 이 창을 닫으시겠습니까?", "HS 플레이어 스크립트",MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
            if (dr == DialogResult.Yes)
            {
                if (Prompt())
                {
                    e.Cancel = false;
                    ClearMethod();
                    ClearMethodCompile();
                    IsRun = false;
                    btnRun.Image = Properties.Resources.Run;
                    btnRun.Enabled = false;

                    dsp.Dispose();
                }
            }
        }
    }
}
