﻿using System;
using System.Windows.Forms;
using HS_Audio.DSP;
using System.Collections.Generic;
using System.IO;

namespace HS_Audio.Forms
{
    public partial class frmDSP : Form
    {
        System.Collections.Generic.List<HSAudioDSPHelper> DSPHelper = new System.Collections.Generic.List<HSAudioDSPHelper>(12);
        internal
            System.Collections.Generic.Dictionary<string, string> Settings = new Dictionary<string, string>();
        System.Collections.Generic.List<Dictionary<string, string>> Presets = new List<Dictionary<string, string>>();
        HSAudioHelper Helper;
        public bool AutoUpdate = true;

        string _ProfileName;
        public string ProfileName
        { get { return _ProfileName; }
            set { _ProfileName = value; this.Text = "DSP 관리자 - [" + value + "]"; }}

        private frmDSP()
        {
            InitializeComponent();
            string a = Application.StartupPath.Replace("/", "\\") + "\\Preset\\DSP";
            if (!System.IO.Directory.Exists(a)) { System.IO.Directory.CreateDirectory(a); }
            openFileDialog1.InitialDirectory = saveFileDialog1.InitialDirectory = a;
            새로고침ToolStripMenuItem.ToolTipText = PresetDir + "\n폴더에있는 프리셋의 목록을 새로고침 합니다.";
            toolStripComboBox1.ToolTipText = PresetDir + "\n폴더에있는 프리셋의 목록입니다.";
            Settings = InitPreset();
            새로고침ToolStripMenuItem_Click(null, null);

        }
        public frmDSP(HSAudioHelper Helper)
        {
            InitializeComponent();
            string a = Application.StartupPath.Replace("/", "\\") + "\\Preset\\DSP";
            if (!System.IO.Directory.Exists(a)) { System.IO.Directory.CreateDirectory(a); }
            openFileDialog1.InitialDirectory = saveFileDialog1.InitialDirectory = a;

            새로고침ToolStripMenuItem.ToolTipText = PresetDir + "\n폴더에있는 프리셋의 목록을 새로고침 합니다.";
            toolStripComboBox1.ToolTipText = PresetDir + "\n폴더에있는 프리셋의 목록입니다.";
            this.Helper = Helper;
            Helper.MusicChanging += new HSAudioHelper.MusicChangeEventHandler(MusicPath_Change);
            Settings = InitPreset();
            새로고침ToolStripMenuItem_Click(null, null);
            /*
            DSPHelper.Add(new FMODDSPHelper(Helper, FMOD.DSP_TYPE.NORMALIZE));
            DSPHelper.Add(new FMODDSPHelper(Helper, FMOD.DSP_TYPE.ECHO));
            DSPHelper.Add(new FMODDSPHelper(Helper, FMOD.DSP_TYPE.PITCHSHIFT));
            DSPHelper.Add(new FMODDSPHelper(Helper, FMOD.DSP_TYPE.FLANGE));
            DSPHelper.Add(new FMODDSPHelper(Helper, FMOD.DSP_TYPE.COMPRESSOR));
            DSPHelper.Add(new FMODDSPHelper(Helper, FMOD.DSP_TYPE.ITLOWPASS));
            DSPHelper.Add(new FMODDSPHelper(Helper, FMOD.DSP_TYPE.DISTORTION));
            DSPHelper.Add(new FMODDSPHelper(Helper, FMOD.DSP_TYPE.CHORUS));
            DSPHelper.Add(new FMODDSPHelper(Helper, FMOD.DSP_TYPE.TREMOLO));
            checkBox1_CheckedChanged(null, null);
            checkBox2_CheckedChanged(null, null);
            checkBox3_CheckedChanged(null, null);
            checkBox4_CheckedChanged(null, null);
            checkBox5_CheckedChanged(null, null);
            checkBox6_CheckedChanged(null, null);
            checkBox7_CheckedChanged(null, null);
            checkBox8_CheckedChanged(null, null);
            checkBox9_CheckedChanged(null, null);
            trbGain_ValueChanged(null, null);*/
            cbFFTSIZE.Text = "1024"; cbTYPE_OSCILLATOR.Text = "SINE";
        }
        public frmDSP(HSAudioHelper Helper, bool IsSystem)
        {
            InitializeComponent();
            string a = Application.StartupPath.Replace("/", "\\") + "\\Preset\\DSP";
            if (!System.IO.Directory.Exists(a)) { System.IO.Directory.CreateDirectory(a); }
            openFileDialog1.InitialDirectory = saveFileDialog1.InitialDirectory = a;
            새로고침ToolStripMenuItem.ToolTipText = PresetDir + "\n폴더에있는 프리셋의 목록을 새로고침 합니다.";
            toolStripComboBox1.ToolTipText = PresetDir + "\n폴더에있는 프리셋의 목록입니다.";
            this.Helper = Helper;
            Helper.MusicChanging += new HSAudioHelper.MusicChangeEventHandler(MusicPath_Change);
            Settings = InitPreset();
            새로고침ToolStripMenuItem_Click(null, null);

            /*DSPHelper.Add(new FMODDSPHelper(Helper, FMOD.DSP_TYPE.NORMALIZE, System));
            DSPHelper.Add(new FMODDSPHelper(Helper, FMOD.DSP_TYPE.ECHO, System));
            DSPHelper.Add(new FMODDSPHelper(Helper, FMOD.DSP_TYPE.PITCHSHIFT, System));
            DSPHelper.Add(new FMODDSPHelper(Helper, FMOD.DSP_TYPE.FLANGE, System));
            DSPHelper.Add(new FMODDSPHelper(Helper, FMOD.DSP_TYPE.COMPRESSOR, System));
            DSPHelper.Add(new FMODDSPHelper(Helper, FMOD.DSP_TYPE.ITLOWPASS, System));
            DSPHelper.Add(new FMODDSPHelper(Helper, FMOD.DSP_TYPE.DISTORTION, System));
            DSPHelper.Add(new FMODDSPHelper(Helper, FMOD.DSP_TYPE.CHORUS, System));
            DSPHelper.Add(new FMODDSPHelper(Helper, FMOD.DSP_TYPE.TREMOLO, System));
            checkBox1_CheckedChanged(null, null);
            checkBox2_CheckedChanged(null, null);
            checkBox3_CheckedChanged(null, null);
            checkBox4_CheckedChanged(null, null);
            checkBox5_CheckedChanged(null, null);
            checkBox6_CheckedChanged(null, null);
            checkBox7_CheckedChanged(null, null);
            checkBox8_CheckedChanged(null, null);
            checkBox9_CheckedChanged(null, null);
            trbGain_ValueChanged(null, null);*/
            cbFFTSIZE.Text = "1024"; cbTYPE_OSCILLATOR.Text = "SINE";
        }

        //[Obsolete("왠만하면 이 메서드는 쓰지 마십시오. 버그가 발견된것 같습니다.")]
        public void Remove()
        {
            for (int a = 0; a < DSPHelper.Count; a++) { try { DSPHelper[a].DisConnect(); }catch{} }
        }
        public void Remove(int index)
        {try { DSPHelper[index].DisConnect(); }catch { }}

        public void Reset()
        {
            for (int a = 0; a < DSPHelper.Count; a++) { DSPHelper[a].Reset(); }
        }
        public void Reset(int index) { DSPHelper[index].Reset();}

        bool IsSystem = false;
        public void ReRegister()
        {
            if (btnDSPRemove.Enabled)
            {
                Stop = true;
                Remove(); //Reset();

                DSPHelper.Add(new HSAudioDSPHelper(Helper, HS_Audio_Lib.DSP_TYPE.NORMALIZE, IsSystem));
                DSPHelper.Add(new HSAudioDSPHelper(Helper, HS_Audio_Lib.DSP_TYPE.ECHO, IsSystem));
                DSPHelper.Add(new HSAudioDSPHelper(Helper, HS_Audio_Lib.DSP_TYPE.PITCHSHIFT, IsSystem));
                DSPHelper.Add(new HSAudioDSPHelper(Helper, HS_Audio_Lib.DSP_TYPE.FLANGE, IsSystem));
                DSPHelper.Add(new HSAudioDSPHelper(Helper, HS_Audio_Lib.DSP_TYPE.COMPRESSOR, IsSystem));
                DSPHelper.Add(new HSAudioDSPHelper(Helper, HS_Audio_Lib.DSP_TYPE.ITLOWPASS, IsSystem));
                DSPHelper.Add(new HSAudioDSPHelper(Helper, HS_Audio_Lib.DSP_TYPE.DISTORTION, IsSystem));
                DSPHelper.Add(new HSAudioDSPHelper(Helper, HS_Audio_Lib.DSP_TYPE.CHORUS, IsSystem));
                DSPHelper.Add(new HSAudioDSPHelper(Helper, HS_Audio_Lib.DSP_TYPE.TREMOLO, IsSystem));
                DSPHelper.Add(null);
                DSPHelper.Add(new HSAudioDSPHelper(Helper, HS_Audio_Lib.DSP_TYPE.HIGHPASS, IsSystem));
                checkBox1_CheckedChanged(null, null);
                checkBox2_CheckedChanged(null, null);
                checkBox3_CheckedChanged(null, null);
                checkBox4_CheckedChanged(null, null);
                checkBox5_CheckedChanged(null, null);
                checkBox6_CheckedChanged(null, null);
                checkBox7_CheckedChanged(null, null);
                checkBox8_CheckedChanged(null, null);
                checkBox9_CheckedChanged(null, null);
                checkBox10_CheckedChanged(null, null);
                checkBox11_CheckedChanged(null, null);
                trbGain_ValueChanged(null, null);
                Stop = false;
            }
        }
        public void ReRegister(bool IsSystem)
        {
            if (btnDSPRemove.Enabled)
            {
                Stop = true;
                if (DSPHelper.Count == 0)
                {
                    Remove();
                    this.IsSystem = IsSystem;
                    DSPHelper.Clear();
                    DSPHelper.Add(new HSAudioDSPHelper(Helper, HS_Audio_Lib.DSP_TYPE.NORMALIZE, IsSystem));
                    DSPHelper.Add(new HSAudioDSPHelper(Helper, HS_Audio_Lib.DSP_TYPE.ECHO, IsSystem));
                    DSPHelper.Add(new HSAudioDSPHelper(Helper, HS_Audio_Lib.DSP_TYPE.PITCHSHIFT, IsSystem));
                    DSPHelper.Add(new HSAudioDSPHelper(Helper, HS_Audio_Lib.DSP_TYPE.FLANGE, IsSystem));
                    DSPHelper.Add(new HSAudioDSPHelper(Helper, HS_Audio_Lib.DSP_TYPE.COMPRESSOR, IsSystem));
                    DSPHelper.Add(new HSAudioDSPHelper(Helper, HS_Audio_Lib.DSP_TYPE.ITLOWPASS, IsSystem));
                    DSPHelper.Add(new HSAudioDSPHelper(Helper, HS_Audio_Lib.DSP_TYPE.DISTORTION, IsSystem));
                    DSPHelper.Add(new HSAudioDSPHelper(Helper, HS_Audio_Lib.DSP_TYPE.CHORUS, IsSystem));
                    DSPHelper.Add(new HSAudioDSPHelper(Helper, HS_Audio_Lib.DSP_TYPE.TREMOLO, IsSystem));
                    DSPHelper.Add(null);
                    DSPHelper.Add(new HSAudioDSPHelper(Helper, HS_Audio_Lib.DSP_TYPE.HIGHPASS, IsSystem));
                    for (int i = 0; i < DSPHelper.Count; i++) if (DSPHelper[i] != null){ DSPHelper[i].Bypass = true;DSPHelper[i].Connect();}
                }
                else for (int i = 0; i < DSPHelper.Count; i++) DSPHelper[i].ReRegisterDSP(IsSystem);

                System.Threading.Thread.Sleep(50);
                trbGain_ValueChanged(null, null);
                /*
                checkBox1_CheckedChanged(null, null);
                checkBox2_CheckedChanged(null, null);
                checkBox3_CheckedChanged(null, null);
                checkBox4_CheckedChanged(null, null);
                checkBox5_CheckedChanged(null, null);
                checkBox6_CheckedChanged(null, null);
                checkBox7_CheckedChanged(null, null);
                checkBox8_CheckedChanged(null, null);
                checkBox9_CheckedChanged(null, null);
                checkBox10_CheckedChanged(null, null);
                checkBox11_CheckedChanged(null, null);*/
                //trbGain_ValueChanged(null, null);
                Stop = false;
            }
        }

        internal void MusicPath_Change(HSAudioHelper Helper, int index, bool Error)
        {
            if (AutoUpdate && !Error)
            {
                if (!btnDSPRegister.Enabled)
                {
                    if (rbFMOD_Channel.Checked) { ReRegister(false); }
                    else { ReRegister(true); }
                    Helper.UpdateMix();
                }
            }
        }

        #region 기본값 버튼 클릭
        private void button1_Click(object sender, EventArgs e) { trbDELAY_ECHO.Value = 500; }
        private void button2_Click(object sender, EventArgs e) { trbDECAYRATIO.Value = 500; }
        private void button3_Click(object sender, EventArgs e) { trbWETMIX_ECHO.Value = 1000; }
        private void button4_Click(object sender, EventArgs e) { trbOVERLAP.Value = 4; }
        private void button5_Click(object sender, EventArgs e) { trbWETMIX_FLANGE.Value = 450; }
        private void button6_Click(object sender, EventArgs e) { trbPITCH.Value = 1000; }
        private void button7_Click(object sender, EventArgs e) { cbFFTSIZE.Text = "1024"; }
        private void button8_Click(object sender, EventArgs e) { trbRATE_FLANGE.Value = 100; }
        private void button9_Click(object sender, EventArgs e) { trbDEPTH_FLANGE.Value = 1000; }
        private void button10_Click(object sender, EventArgs e) { trbRELEASE.Value = 50; }
        private void button11_Click(object sender, EventArgs e) { trbATTACK.Value = 50; }
        private void button12_Click(object sender, EventArgs e) { trbTHRESHOLD.Value = 0; }
        private void button13_Click(object sender, EventArgs e) { trbGAINMAKEUP.Value = 0; }
        private void button14_Click(object sender, EventArgs e) { trbDRYMIX_FLANGE.Value = 550; }
        private void button15_Click(object sender, EventArgs e) { trbDELAY_CHORUS.Value = 4000; }
        private void button16_Click(object sender, EventArgs e) { trbDISTORTION.Value = 500;/*toolStripStatusLabel2.Text = DSPHelper[6].SetParameter((int)FMOD.DSP_DISTORTION.LEVEL, 300).ToString();*/}
        private void button17_Click(object sender, EventArgs e) { trbRESONANCE.Value = 100; }
        private void button18_Click(object sender, EventArgs e) { trbCUTOFF.Value = 5000; }
        private void button19_Click(object sender, EventArgs e) { trbDRYMIX_CHORUS.Value = 500; }
        private void button20_Click(object sender, EventArgs e) { trbDEPTH_CHORUS.Value = 20; }
        private void button21_Click(object sender, EventArgs e) { trbRATE_CHORUS.Value = 800; }
        private void button22_Click(object sender, EventArgs e) { trbWETMIX3_CHORUS.Value = 500; }
        private void button23_Click(object sender, EventArgs e) { trbWETMIX2_CHORUS.Value = 500; }
        private void button24_Click(object sender, EventArgs e) { trbWETMIX1_CHORUS.Value = 500; }
        private void button25_Click(object sender, EventArgs e) { trbFEEDBACK_CHORUS.Value = 1000; }
        private void button26_Click(object sender, EventArgs e) { trbSHAPE.Value = 1000; }
        private void button27_Click(object sender, EventArgs e) { trbSPREAD.Value = 0; }
        private void button28_Click(object sender, EventArgs e) { trbPHASE.Value = 0; }
        private void button29_Click(object sender, EventArgs e) { trbSQUARE.Value = 0; }
        private void button30_Click(object sender, EventArgs e) { trbFREQUENCY.Value = 400; }
        private void button31_Click(object sender, EventArgs e) { trbDUTY.Value = 500; }
        private void button32_Click(object sender, EventArgs e) { trbSKEW.Value = 0; }
        private void button33_Click(object sender, EventArgs e) { trbDEPTH_TREMOLO.Value = 1000; }
        private void button34_Click(object sender, EventArgs e) { numRATE_OSCILLATOR.Value = 220; }
        private void button35_Click(object sender, EventArgs e) { cbTYPE_OSCILLATOR.SelectedIndex = 0; }
        private void button36_Click(object sender, EventArgs e) { trbFADETIME.Value = 5000; }
        private void button37_Click(object sender, EventArgs e) { trbTHRESHHOLD.Value = 100; }
        private void button38_Click(object sender, EventArgs e) { trbMAXAMP.Value = 20; }
        private void button39_Click(object sender, EventArgs e) { trbCUTOFF_HIGHPASS.Value = 5000; }
        private void button40_Click(object sender, EventArgs e) { trbRESONANCE_HIGHPASS.Value = 100; }
        #endregion

        #region 체크 값 변경

        float Gain = 0.8f;
        public bool DSPUse
        {
            get { return !btnDSPRegister.Enabled; }
            set
            {
                if (value) { if (btnDSPRegister.Enabled)btnDSPRegister_Click(null, null); }
                else { if (btnDSPRemove.Enabled)btnDSPRemove_Click(null, null); }
            }
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                DSPHelper[0].Bypass = !(chkNOMALIZER.Checked && panel2.Enabled);
                DSPHelper[0].Gain = Gain;
                groupBox1.Enabled=chkNOMALIZER.Checked && panel2.Enabled;
                toolStripStatusLabel2.Text =  DSPHelper[0].Result.ToString();
                trackBar1_ValueChanged(null, null);trackBar2_ValueChanged(null, null);trackBar3_ValueChanged(null, null);
            }
            catch (ArgumentOutOfRangeException) { }
            Settings[chkNOMALIZER.Name] = chkNOMALIZER.Checked.ToString();
        }

        private void checkBox2_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                DSPHelper[1].Gain = Gain;
                DSPHelper[1].Bypass = !(chkECHO.Checked && panel2.Enabled);
                groupBox2.Enabled = chkECHO.Checked && panel2.Enabled; 
                toolStripStatusLabel2.Text = DSPHelper[1].Result.ToString();
                trbDELAY_ValueChanged(null, null); trbWETMIX_ECHO_ValueChanged(null, null); trbDECAYRATIO_ValueChanged(null, null);
            }
            catch (ArgumentOutOfRangeException) { }
            Settings[chkECHO.Name] = chkECHO.Checked.ToString();
        }

        private void checkBox3_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                DSPHelper[2].Gain = Gain;
                DSPHelper[2].Bypass = !(chkPITCHSHIFT.Checked && panel2.Enabled);
                groupBox3.Enabled = chkPITCHSHIFT.Checked && panel2.Enabled;
                toolStripStatusLabel2.Text = DSPHelper[2].Result.ToString();
                trbPITCH_ValueChanged(null, null); cbFFTSIZE_SelectedIndexChanged(null, null); trackBar4_ValueChanged(null, null);
            }
            catch (ArgumentOutOfRangeException) { }
            Settings[chkPITCHSHIFT.Name] = chkPITCHSHIFT.Checked.ToString();
        }
        private void checkBox4_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                DSPHelper[3].Gain = Gain;
                DSPHelper[3].Bypass = !(chkFLANGE.Checked && panel2.Enabled);
                groupBox4.Enabled = chkFLANGE.Checked && panel2.Enabled;
                toolStripStatusLabel2.Text = DSPHelper[3].Result.ToString();
                trbDEPTH_ValueChanged(null, null); trbRATE_ValueChanged(null, null); trbWETMIX_FLANGE_ValueChanged(null, null); trbDRYMIX_FLANGE_ValueChanged(null, null);
            }
            catch (ArgumentOutOfRangeException) { }
            Settings[chkFLANGE.Name] = chkFLANGE.Checked.ToString();
        }

        private void checkBox5_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                DSPHelper[4].Gain = Gain;
                DSPHelper[4].Bypass = !(chkCOMPRESSOR.Checked && panel2.Enabled);
                groupBox5.Enabled = chkCOMPRESSOR.Checked && panel2.Enabled;
                toolStripStatusLabel2.Text = DSPHelper[4].Result.ToString();
                trackBar7_ValueChanged(null, null); trackBar8_ValueChanged(null, null); trackBar9_ValueChanged(null, null); trackBar10_ValueChanged(null, null);
            }
            catch (ArgumentOutOfRangeException) { }
            Settings[chkCOMPRESSOR.Name] = chkCOMPRESSOR.Checked.ToString();
        }

        private void checkBox6_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                DSPHelper[5].Gain = Gain;
                DSPHelper[5].Bypass = !(chkLOWPASS.Checked && panel2.Enabled);
                groupBox6.Enabled = chkLOWPASS.Checked && panel2.Enabled;
                toolStripStatusLabel2.Text = DSPHelper[5].Result.ToString();
                trbCUTOFF_ValueChanged(null, null);trbRESONANCE_ValueChanged(null, null);
            }
            catch (ArgumentOutOfRangeException) { }
            Settings[chkLOWPASS.Name] = chkLOWPASS.Checked.ToString();
        }

        private void checkBox7_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                DSPHelper[6].Gain = Gain;
                DSPHelper[6].Bypass = !(chkDISTORTION.Checked && panel2.Enabled);
                groupBox7.Enabled = chkDISTORTION.Checked && panel2.Enabled;
                toolStripStatusLabel2.Text = DSPHelper[6].Result.ToString();
                trbDISTORTION_ValueChanged(null, null);
            }
            catch (ArgumentOutOfRangeException) { }
            Settings[chkDISTORTION.Name] = chkDISTORTION.Checked.ToString();
        }

        private void checkBox8_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                DSPHelper[7].Gain = Gain;
                DSPHelper[7].Bypass = !(chkCHORUS.Checked && panel2.Enabled);
                groupBox8.Enabled=chkCHORUS.Checked && panel2.Enabled;
                toolStripStatusLabel2.Text = DSPHelper[7].Result.ToString();
                trbDELAY_CHORUS_ValueChanged(null, null); trbRATE_CHORUS_ValueChanged(null, null); trbFEEDBACK_CHORUS_ValueChanged(null, null); trbDEPTH_CHORUS_ValueChanged(null, null);
                trbDRYMIX_CHORUS_ValueChanged(null, null); trbWETMIX1_CHORUS_ValueChanged(null, null); trbWETMIX2_CHORUS_ValueChanged(null, null); trbWETMIX3_CHORUS_ValueChanged(null, null);
            }
            catch (ArgumentOutOfRangeException) { }
            Settings[chkCHORUS.Name] = chkCHORUS.Checked.ToString();
        }

        private void checkBox9_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                DSPHelper[8].Gain = Gain;
                DSPHelper[8].Bypass = !(chkTREMOLO.Checked && panel2.Enabled);
                groupBox9.Enabled = chkTREMOLO.Checked && panel2.Enabled;
                toolStripStatusLabel2.Text = DSPHelper[8].Result.ToString();
                trbFREQUENCY_ValueChanged(null, null); trbDEPTH_TREMOLO_ValueChanged(null, null); trbSHAPE_ValueChanged(null, null); trbSKEW_ValueChanged (null, null);
                trbDUTY_ValueChanged(null, null); trbSQUARE_ValueChanged(null, null); trbPHASE_ValueChanged(null, null); trbSPREAD_ValueChanged(null, null);
            }
            catch (ArgumentOutOfRangeException) { }
            Settings[chkTREMOLO.Name] = chkTREMOLO.Checked.ToString();
        }

        private void checkBox10_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                if (DSPHelper[9] == null) DSPHelper[9] = new HSAudioDSPHelper(Helper, HS_Audio_Lib.DSP_TYPE.OSCILLATOR, IsSystem);
                DSPHelper[9].Gain = Gain;
                DSPHelper[9].Bypass = !(chkOSCILLATOR.Checked && panel2.Enabled);
                groupBox10.Enabled =chkOSCILLATOR.Checked && panel2.Enabled;
                toolStripStatusLabel2.Text = DSPHelper[9].Result.ToString();
                cbFFTSIZE_SelectedIndexChanged(null, null); tbRATE_OSCILLATOR_ValueChanged(null, null);
            }
            catch (ArgumentOutOfRangeException) { }
            Settings[chkOSCILLATOR.Name] = chkOSCILLATOR.Checked.ToString();
        }
        private void checkBox11_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                DSPHelper[10].Gain = Gain;
                DSPHelper[10].Bypass = !(chkHIGHPASS.Checked && panel2.Enabled);
                groupBox11.Enabled = chkHIGHPASS.Checked && panel2.Enabled;
                toolStripStatusLabel2.Text = DSPHelper[10].Result.ToString();
                trbCUTOFF_HIGHPASS_ValueChanged(null, null);
                trbRESONANCE_HIGHPASS_ValueChanged(null, null);
            }
            catch (ArgumentOutOfRangeException) { }
            Settings[chkHIGHPASS.Name] = chkLOWPASS.Checked.ToString();
        }
        #endregion

        #region 값 변경

        private void trackBar1_ValueChanged(object sender, EventArgs e)
        {
            try {toolStripStatusLabel2.Text=
                DSPHelper[0].SetParameter((int)HS_Audio_Lib.DSP_NORMALIZE.FADETIME, trbFADETIME.Value * 0.1f).ToString();
            lblFADETIME.Text = trbFADETIME.Value.ToString();}
            catch (ArgumentOutOfRangeException) { }
            Settings[trbFADETIME.Name] = trbFADETIME.Value.ToString();
        }
        bool IsClosing;
        private void frmDSP_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = !IsClosing;
            this.Hide();
        }

        private void trackBar2_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                toolStripStatusLabel2.Text =
                DSPHelper[0].SetParameter((int)HS_Audio_Lib.DSP_NORMALIZE.THRESHHOLD, trbTHRESHHOLD.Value * 0.0001f).ToString();
                lblTHRESHHOLD.Text = (trbTHRESHHOLD.Value/* *0.00001*/).ToString();
            }
            catch (ArgumentOutOfRangeException) { }
            Settings[trbTHRESHHOLD.Name] = trbTHRESHHOLD.Value.ToString();
        }

        private void trackBar3_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                toolStripStatusLabel2.Text =
                DSPHelper[0].SetParameter((int)HS_Audio_Lib.DSP_NORMALIZE.MAXAMP, trbMAXAMP.Value).ToString();
                lblMAXAMP.Text = (trbMAXAMP.Value/*  * 10*/).ToString();
            }
            catch (ArgumentOutOfRangeException) { }
            Settings[trbMAXAMP.Name] = trbMAXAMP.Value.ToString();
        }

        private void trbDECAYRATIO_ValueChanged(object sender, EventArgs e)
        {
            try{if (chkECHO.Checked)
                 {toolStripStatusLabel2.Text =
                        DSPHelper[1].SetParameter((int)HS_Audio_Lib.DSP_ECHO.DECAYRATIO, trbDECAYRATIO.Value * 0.001f).ToString();
                    lblDECAYRATIO.Text = (trbDECAYRATIO.Value/* * 0.001d*/).ToString(); }}
            catch (ArgumentOutOfRangeException) { }
            Settings[trbDECAYRATIO.Name] = trbDECAYRATIO.Value.ToString();
        }

        private void trbWETMIX_ECHO_ValueChanged(object sender, EventArgs e)
        {
            try{if (chkECHO.Checked)
                {toolStripStatusLabel2.Text =
                       DSPHelper[1].SetParameter((int)HS_Audio_Lib.DSP_ECHO.WETMIX, trbWETMIX_ECHO.Value*0.001f).ToString();
                    lblWETMIX_ECHO.Text = (trbWETMIX_ECHO.Value/*  * 0.001d*/).ToString();}}
            catch (ArgumentOutOfRangeException) { }
            Settings[trbWETMIX_ECHO.Name] = trbWETMIX_ECHO.Value.ToString();
        }

        private void trbDELAY_ValueChanged(object sender, EventArgs e)
        {
            try{if (chkECHO.Checked)
                {toolStripStatusLabel2.Text =
                DSPHelper[1].SetParameter((int)HS_Audio_Lib.DSP_ECHO.DELAY, trbDELAY_ECHO.Value).ToString();
                lblDELAY.Text = trbDELAY_ECHO.Value.ToString();}}
            catch(ArgumentOutOfRangeException){}
            Settings[trbDELAY_ECHO.Name] = trbDELAY_ECHO.Value.ToString();
        }

        private void trbPITCH_ValueChanged(object sender, EventArgs e)
        {
            if (chkPITCHSHIFT.Checked){
                toolStripStatusLabel2.Text=DSPHelper[2].SetParameter((int)HS_Audio_Lib.DSP_PITCHSHIFT.PITCH, trbPITCH.Value * 0.001f).ToString();}
            label17.Text = trbPITCH.Value.ToString();
            Settings[trbPITCH.Name] = trbPITCH.Value.ToString();
        }

        private void trackBar4_ValueChanged(object sender, EventArgs e)
        {
            try{if (chkPITCHSHIFT.Checked)
            { toolStripStatusLabel2.Text = DSPHelper[2].SetParameter((int)HS_Audio_Lib.DSP_PITCHSHIFT.OVERLAP, trbOVERLAP.Value).ToString(); }
            label13.Text = trbOVERLAP.Value.ToString();}
            catch (ArgumentOutOfRangeException) { }
            Settings["trbOVERLAP"] = trbOVERLAP.Value.ToString();
        }

        private void trackBar10_ValueChanged(object sender, EventArgs e)
        {
            try{toolStripStatusLabel2.Text = DSPHelper[4].SetParameter((int)HS_Audio_Lib.DSP_COMPRESSOR.GAINMAKEUP, trbGAINMAKEUP.Value * 0.1f).ToString();
            lblGAINMAKEUP.Text = trbGAINMAKEUP.Value.ToString();}
            catch (ArgumentOutOfRangeException) { }
            Settings[trbGAINMAKEUP.Name] = trbGAINMAKEUP.Value.ToString();
        }

        private void trackBar8_ValueChanged(object sender, EventArgs e)
        {
            try{toolStripStatusLabel2.Text = DSPHelper[4].SetParameter((int)HS_Audio_Lib.DSP_COMPRESSOR.ATTACK, trbATTACK.Value).ToString();
            lblATTACK.Text = trbATTACK.Value.ToString();}
            catch (ArgumentOutOfRangeException) { }
            Settings[trbATTACK.Name] = trbATTACK.Value.ToString();
        }

        private void trackBar7_ValueChanged(object sender, EventArgs e)
        {
            try{toolStripStatusLabel2.Text = DSPHelper[4].SetParameter((int)HS_Audio_Lib.DSP_COMPRESSOR.RELEASE, trbRELEASE.Value).ToString();
            lblRELEASE.Text = trbRELEASE.Value.ToString();}
            catch (ArgumentOutOfRangeException) { }
            Settings[trbRELEASE.Name] = trbRELEASE.Value.ToString();
        }

        private void trackBar9_ValueChanged(object sender, EventArgs e)
        {
            try{toolStripStatusLabel2.Text = DSPHelper[4].SetParameter((int)HS_Audio_Lib.DSP_COMPRESSOR.THRESHOLD, trbTHRESHOLD.Value * 0.1f).ToString();
            lblTHRESHOLD.Text = trbTHRESHOLD.Value.ToString();}
            catch (ArgumentOutOfRangeException) { }
            Settings[trbTHRESHOLD.Name] = trbTHRESHOLD.Value.ToString();
        }

        private void trbDEPTH_ValueChanged(object sender, EventArgs e)
        {
            toolStripStatusLabel2.Text = DSPHelper[3].SetParameter((int)HS_Audio_Lib.DSP_FLANGE.DEPTH, trbDEPTH_FLANGE.Value * 0.001f).ToString();
            lblDEPTH_FLANGE.Text = trbDEPTH_FLANGE.Value.ToString();
            Settings[trbDEPTH_FLANGE.Name] = trbDEPTH_FLANGE.Value.ToString();
        }

        private void trbRATE_ValueChanged(object sender, EventArgs e)
        {
            toolStripStatusLabel2.Text = DSPHelper[3].SetParameter((int)HS_Audio_Lib.DSP_FLANGE.RATE, trbRATE_FLANGE.Value * 0.01f).ToString();
            lblRATE_FLANGE.Text = trbRATE_FLANGE.Value.ToString();
            Settings[trbRATE_FLANGE.Name] = trbRATE_FLANGE.Value.ToString();
        }

        private void trbWETMIX_FLANGE_ValueChanged(object sender, EventArgs e)
        {
            toolStripStatusLabel2.Text = DSPHelper[3].SetParameter((int)HS_Audio_Lib.DSP_FLANGE.WETMIX, trbWETMIX_FLANGE.Value * 0.001f).ToString();
            lblWETMIX_FLANGE.Text = trbWETMIX_FLANGE.Value.ToString();
            Settings[trbWETMIX_FLANGE.Name] = trbWETMIX_FLANGE.Value.ToString();
        }

        private void trbDRYMIX_FLANGE_ValueChanged(object sender, EventArgs e)
        {
            toolStripStatusLabel2.Text = DSPHelper[3].SetParameter((int)HS_Audio_Lib.DSP_FLANGE.DRYMIX, trbDRYMIX_FLANGE.Value * 0.001f).ToString();
            lblDRYMIX_FLANGE.Text = trbDRYMIX_FLANGE.Value.ToString();
            Settings[trbDRYMIX_FLANGE.Name] = trbDRYMIX_FLANGE.Value.ToString();
        }

        private void trbCUTOFF_ValueChanged(object sender, EventArgs e)
        {
            toolStripStatusLabel2.Text = DSPHelper[5].SetParameter((int)HS_Audio_Lib.DSP_ITLOWPASS.CUTOFF, trbCUTOFF.Value).ToString();
            lblCUTOFF.Text = trbCUTOFF.Value.ToString();
            Settings[trbCUTOFF.Name] = trbCUTOFF.Value.ToString();
        }

        private void trbRESONANCE_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                toolStripStatusLabel2.Text = DSPHelper[5].SetParameter((int)HS_Audio_Lib.DSP_ITLOWPASS.RESONANCE, trbRESONANCE.Value * 0.1f).ToString();
                lblRESONANCE.Text = trbRESONANCE.Value.ToString();
                Settings[trbRESONANCE.Name] = trbRESONANCE.Value.ToString();
            }
            catch (ArgumentOutOfRangeException) { }
        }

        private void trbGain_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                Gain = trbGain.Value * 0.001f;
                label1.Text = (trbGain.Value - 300).ToString();
                checkBox1_CheckedChanged(null, null);
                checkBox2_CheckedChanged(null, null);
                checkBox3_CheckedChanged(null, null);
                checkBox4_CheckedChanged(null, null);
                checkBox5_CheckedChanged(null, null);
                checkBox6_CheckedChanged(null, null);
                checkBox7_CheckedChanged(null, null);
                checkBox8_CheckedChanged(null, null);
                checkBox9_CheckedChanged(null, null);
                checkBox10_CheckedChanged(null, null);
                checkBox11_CheckedChanged(null, null);
                //if (DSPHelper[9] != null) DSPHelper[9].Gain = Gain - 0.3f;
            }
            catch (ArgumentOutOfRangeException) { }
            Settings["trbGain"] = trbGain.Value.ToString();
        }

        private void trbDISTORTION_ValueChanged(object sender, EventArgs e)
        {
            toolStripStatusLabel2.Text = DSPHelper[6].SetParameter((int)HS_Audio_Lib.DSP_DISTORTION.LEVEL, trbDISTORTION.Value * 0.001f).ToString();
            lblDISTORTION.Text = (trbDISTORTION.Value).ToString();
            Settings[trbDISTORTION.Name] = trbDISTORTION.Value.ToString();
        }

        private void trbDELAY_CHORUS_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                toolStripStatusLabel2.Text = DSPHelper[7].SetParameter((int)HS_Audio_Lib.DSP_CHORUS.DELAY, trbDELAY_CHORUS.Value * 0.01f).ToString();
                lblDELAY_CHORUS.Text = trbDELAY_CHORUS.Value.ToString();
            }
            catch (ArgumentOutOfRangeException) { }
            Settings[trbDELAY_CHORUS.Name] = trbDELAY_CHORUS.Value.ToString();
        }

        private void trbRATE_CHORUS_ValueChanged(object sender, EventArgs e)
        {
            toolStripStatusLabel2.Text = DSPHelper[7].SetParameter((int)HS_Audio_Lib.DSP_CHORUS.RATE, trbRATE_CHORUS.Value*0.01f).ToString();
            lblRATE_CHORUS.Text = trbRATE_CHORUS.Value.ToString();
            Settings[trbRATE_CHORUS.Name] = trbRATE_CHORUS.Value.ToString();
        }

        private void trbDEPTH_CHORUS_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                toolStripStatusLabel2.Text = DSPHelper[7].SetParameter((int)HS_Audio_Lib.DSP_CHORUS.DEPTH, trbDEPTH_CHORUS.Value * 0.001f).ToString();
                lblDEPTH_CHORUS.Text = trbDEPTH_CHORUS.Value.ToString();
                Settings[trbDEPTH_CHORUS.Name] = trbDEPTH_CHORUS.Value.ToString();
            }
            catch (ArgumentOutOfRangeException) { }
        }

        private void trbDRYMIX_CHORUS_ValueChanged(object sender, EventArgs e)
        {
            toolStripStatusLabel2.Text = DSPHelper[7].SetParameter((int)HS_Audio_Lib.DSP_CHORUS.DRYMIX, trbDRYMIX_CHORUS.Value * 0.001f).ToString();
            lblDRYMIX_CHORUS.Text = trbDRYMIX_CHORUS.Value.ToString();
            Settings[lblDRYMIX_CHORUS.Name] = trbDRYMIX_CHORUS.Value.ToString();
        }

        private void trbWETMIX1_CHORUS_ValueChanged(object sender, EventArgs e)
        {
            toolStripStatusLabel2.Text = DSPHelper[7].SetParameter((int)HS_Audio_Lib.DSP_CHORUS.WETMIX1, trbWETMIX1_CHORUS.Value*0.001f).ToString();
            lblWETMIX1_CHORUS.Text = trbWETMIX1_CHORUS.Value.ToString();
            Settings[trbWETMIX1_CHORUS.Name] = trbWETMIX1_CHORUS.Value.ToString();
        }

        private void trbWETMIX2_CHORUS_ValueChanged(object sender, EventArgs e)
        {
            toolStripStatusLabel2.Text = DSPHelper[7].SetParameter((int)HS_Audio_Lib.DSP_CHORUS.WETMIX2, trbWETMIX2_CHORUS.Value * 0.001f).ToString();
            lblWETMIX2_CHORUS.Text = trbWETMIX2_CHORUS.Value.ToString();
            Settings[trbWETMIX2_CHORUS.Name] = trbWETMIX2_CHORUS.Value.ToString();
        }

        private void trbWETMIX3_CHORUS_ValueChanged(object sender, EventArgs e)
        {
            toolStripStatusLabel2.Text = DSPHelper[7].SetParameter((int)HS_Audio_Lib.DSP_CHORUS.WETMIX3, trbWETMIX3_CHORUS.Value * 0.001f).ToString();
            lblWETMIX3_CHORUS.Text = trbWETMIX3_CHORUS.Value.ToString();
            Settings[trbWETMIX3_CHORUS.Name] = trbWETMIX3_CHORUS.Value.ToString();
        }

        private void trbFEEDBACK_CHORUS_ValueChanged(object sender, EventArgs e)
        {
            toolStripStatusLabel2.Text = DSPHelper[7].SetParameter((int)HS_Audio_Lib.DSP_CHORUS.FEEDBACK, trbFEEDBACK_CHORUS.Value * 0.001f).ToString();
            lblFEEDBACK_CHORUS.Text = trbFEEDBACK_CHORUS.Value.ToString();
            Settings[trbFEEDBACK_CHORUS.Name] = trbFEEDBACK_CHORUS.Value.ToString();
        }

        bool Lock;
        private void btnDSPRemove_Click(object sender, EventArgs e)
        {
            Remove();
            btnDSPRegister.Enabled = true;
            btnDSPRemove.Enabled=trbGain.Enabled=label1.Enabled = panel2.Enabled =
                rbFMOD_System.Enabled = rbFMOD_Channel.Enabled=false;
            Helper.UpdateMix();
            Lock = true;
        }

        private void btnDSPRegister_Click(object sender, EventArgs e)
        {
            panel2.Enabled = trbGain.Enabled = label1.Enabled=btnDSPRemove.Enabled =
                rbFMOD_System.Enabled = rbFMOD_Channel.Enabled = true;
            btnDSPRegister.Enabled = false;
            radioButton2_CheckedChanged(null, null);
            Helper.UpdateMix();
        }

        private void btnWhaWha_Click(object sender, EventArgs e)
        {
            toolStripStatusLabel2.Text = DSPHelper[5].SetParameter((int)HS_Audio_Lib.DSP_ITLOWPASS.RESONANCE, 1270 * 0.1f).ToString();
            btnWhaWha.Enabled = false;
            int max = 1000;
            WhaWha=i= trbCUTOFF.Value;

            for (i = WhaWha; i >= max; i = i - trbWhaWhaSpeed.Value)
            {
                if (Stop) { break; }
                if (i < 500) { trbCUTOFF.Value = max; break; }
                else { trbCUTOFF.Value = i; }
                System.Threading.Thread.Sleep(trbWhaWhaDelay.Value); Application.DoEvents(); 
            }

            for (i = max; i <= WhaWha; i = i + trbWhaWhaSpeed.Value)
            {
                if (Stop) { break; }
                if (i > WhaWha) { break; }
                else { trbCUTOFF.Value = i; }
                System.Threading.Thread.Sleep(trbWhaWhaDelay.Value); Application.DoEvents(); 
            }
            //timer1.Start();
            btnWhaWha.Enabled = true;
            toolStripStatusLabel2.Text = DSPHelper[5].SetParameter((int)HS_Audio_Lib.DSP_ITLOWPASS.RESONANCE, (float)trbRESONANCE.Value).ToString();
        }

        int i;
        int WhaWha;
        bool Stop;
        private void timer1_Tick(object sender, EventArgs e)
        {
            /*
            if (i <= WhaWha)
            {
                trbCUTOFF.Value=i = i - WhaWhaDelay.Value;
                if (i <= 56)
                {
                    trbCUTOFF.Value = i = i + WhaWhaDelay.Value;
                }
            }
            else { timer1.Stop(); }*/
            timer1.Stop();
        }

        private void WhaWha_Delay_ValueChanged(object sender, EventArgs e)
        {
            timer1.Interval = trbWhaWhaDelay.Value;
        }

        private void trbFREQUENCY_ValueChanged(object sender, EventArgs e)
        {toolStripStatusLabel2.Text = 
            DSPHelper[8].SetParameter((int)HS_Audio_Lib.DSP_TREMOLO.FREQUENCY, trbFREQUENCY.Value * 0.001f).ToString();
            lblFREQUENCY.Text = trbFREQUENCY.Value.ToString();
            Settings[trbFREQUENCY.Name] = trbFREQUENCY.Value.ToString();
        }

        private void trbDEPTH_TREMOLO_ValueChanged(object sender, EventArgs e)
        {toolStripStatusLabel2.Text = 
            DSPHelper[8].SetParameter((int)HS_Audio_Lib.DSP_TREMOLO.DEPTH, trbDEPTH_TREMOLO.Value * 0.001f).ToString();
        lblDEPTH_TREMOLO.Text = trbDEPTH_TREMOLO.Value.ToString();
        Settings[trbDEPTH_TREMOLO.Name] = trbDEPTH_TREMOLO.Value.ToString();
        }

        private void trbSHAPE_ValueChanged(object sender, EventArgs e)
        {toolStripStatusLabel2.Text =  
            DSPHelper[8].SetParameter((int)HS_Audio_Lib.DSP_TREMOLO.SHAPE, trbSHAPE.Value * 0.001f).ToString();
        lblSHAPE.Text = trbSHAPE.Value.ToString();
        Settings[trbSHAPE.Name] = trbSHAPE.Value.ToString();
        }

        private void trbSKEW_ValueChanged(object sender, EventArgs e)
        {toolStripStatusLabel2.Text = 
            DSPHelper[8].SetParameter((int)HS_Audio_Lib.DSP_TREMOLO.SKEW, trbSKEW.Value * 0.001f).ToString();
            lblSKEW.Text = trbSKEW.Value.ToString();
            Settings[trbSKEW.Name] = trbSKEW.Value.ToString();
        }

        private void trbDUTY_ValueChanged(object sender, EventArgs e)
        {toolStripStatusLabel2.Text = 
            DSPHelper[8].SetParameter((int)HS_Audio_Lib.DSP_TREMOLO.DUTY, trbDUTY.Value * 0.001f).ToString();
            lblDUTY.Text = trbDUTY.Value.ToString();
            Settings[trbDUTY.Name] = trbDUTY.Value.ToString();
        }

        private void trbSQUARE_ValueChanged(object sender, EventArgs e)
        {toolStripStatusLabel2.Text = 
            DSPHelper[8].SetParameter((int)HS_Audio_Lib.DSP_TREMOLO.SQUARE, trbSQUARE.Value * 0.001f).ToString();
        lblSQUARE.Text = trbSQUARE.Value.ToString();
        Settings[trbSQUARE.Name] = trbSQUARE.Value.ToString();
        }

        private void trbPHASE_ValueChanged(object sender, EventArgs e)
        {toolStripStatusLabel2.Text = 
            DSPHelper[8].SetParameter((int)HS_Audio_Lib.DSP_TREMOLO.PHASE, trbPHASE.Value * 0.001f).ToString();
            lblPHASE.Text = trbPHASE.Value.ToString();
            Settings[trbPHASE.Name] = trbPHASE.Value.ToString();
        }

        private void trbSPREAD_ValueChanged(object sender, EventArgs e)
        {toolStripStatusLabel2.Text = 
            DSPHelper[8].SetParameter((int)HS_Audio_Lib.DSP_TREMOLO.SPREAD, trbSPREAD.Value * 0.001f).ToString();
            lblSPREAD.Text = trbSPREAD.Value.ToString();
            Settings[trbSPREAD.Name] = trbSPREAD.Value.ToString();
        }

        private void tbRATE_OSCILLATOR_ValueChanged(object sender, EventArgs e)
        {
            numRATE_OSCILLATOR.Value = tbRATE_OSCILLATOR.Value;
        }

        private void numRATE_OSCILLATOR_ValueChanged(object sender, EventArgs e)
        {
            toolStripStatusLabel2.Text =
            DSPHelper[9].SetParameter((int)HS_Audio_Lib.DSP_OSCILLATOR.RATE, (float)numRATE_OSCILLATOR.Value).ToString();
            Settings[numRATE_OSCILLATOR.Name] = numRATE_OSCILLATOR.Value.ToString();
        }

        private void cbTYPE_OSCILLATOR_SelectedValueChanged(object sender, EventArgs e)
        {
            if (DSPHelper.Count>9&&DSPHelper[9] != null)
            toolStripStatusLabel2.Text =
                DSPHelper[9].SetParameter((int)HS_Audio_Lib.DSP_OSCILLATOR.TYPE, cbTYPE_OSCILLATOR.SelectedIndex).ToString();
            Settings[cbTYPE_OSCILLATOR.Name] = cbTYPE_OSCILLATOR.SelectedIndex.ToString();
            //if(cbTYPE_OSCILLATOR.SelectedIndex==4)
        }
        #endregion

        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {
            if (!btnDSPRegister.Enabled)
            {
                if (rbFMOD_Channel.Checked) { ReRegister(false); }
                else { ReRegister(true); }
                Helper.UpdateMix();
            }
            Settings[rbFMOD_Channel.Name] = rbFMOD_Channel.Checked.ToString();
        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void trbGain_Scroll(object sender, EventArgs e)
        {

        }

        private void 프로파일열기OToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!System.IO.Directory.Exists(PresetDir)) { System.IO.Directory.CreateDirectory(PresetDir); }
            //LastFileDirectory
            openFileDialog1.InitialDirectory = Directory.Exists(LastFileOpenDirectory) ? LastFileOpenDirectory : PresetDir;

            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
               string[] a1 = System.IO.File.ReadAllLines(openFileDialog1.FileName);
               DSPPreset=HS_CSharpUtility.Utility.EtcUtility.LoadSetting(a1);
               ProfileName = System.IO.Path.GetFileNameWithoutExtension(openFileDialog1.FileName);
               Presets.Add(DSPPreset);
               toolStripComboBox1.Text = toolStripComboBox1.Items[toolStripComboBox1.Items.Count - 1].ToString();
            }
        }


        public Dictionary<string, string> DSPPreset
        {
            get{return Settings;}
            set
            {
            /*
            List<System.Windows.Forms.Control> c = new List<System.Windows.Forms.Control>();
            for (int i = 0; i < panel2.Controls.Count; i++)
            {
                c.Add(panel2.Controls[i]);
            }
            */
            //foreach (KeyvaluePair<string, string> s in value) { Settings[s.Key] = s.Value; }

            try{trbGain.Value = Convert.ToInt32(value[trbGain.Name]);}catch{}
            try{rbFMOD_Channel.Checked = Convert.ToBoolean(value[rbFMOD_Channel.Name]);}catch{}
            try{rbFMOD_System.Checked = !rbFMOD_Channel.Checked;}catch{}

            try{chkECHO.Checked = Convert.ToBoolean(value[chkECHO.Name]);}catch{}
            try{trbDELAY_ECHO.Value = Convert.ToInt32(value[trbDELAY_ECHO.Name]);}catch{}
            try{trbDECAYRATIO.Value = Convert.ToInt32(value[trbDECAYRATIO.Name]);}catch{}
            try{trbWETMIX_ECHO.Value = Convert.ToInt32(value[trbWETMIX_ECHO.Name]);}catch{}

            try{chkCHORUS.Checked = Convert.ToBoolean(value[chkCHORUS.Name]);}catch{}
            try{trbDELAY_CHORUS.Value = Convert.ToInt32(value[trbDELAY_CHORUS.Name]);}catch{}
            try{trbFEEDBACK_CHORUS.Value = Convert.ToInt32(value[trbFEEDBACK_CHORUS.Name]);}catch{}
            try{trbDEPTH_CHORUS.Value = Convert.ToInt32(value[trbDEPTH_CHORUS.Name]);}catch{}
            try{trbDRYMIX_CHORUS.Value = Convert.ToInt32(value[trbDRYMIX_CHORUS.Name]);}catch{}
            try{trbWETMIX1_CHORUS.Value = Convert.ToInt32(value[trbWETMIX1_CHORUS.Name]);}catch{}
            try{trbWETMIX2_CHORUS.Value = Convert.ToInt32(value[trbWETMIX2_CHORUS.Name]);}catch{}
            try{trbWETMIX3_CHORUS.Value = Convert.ToInt32(value[trbWETMIX3_CHORUS.Name]);}catch{}

            try{chkPITCHSHIFT.Checked = Convert.ToBoolean(value[chkPITCHSHIFT.Name]);}catch{}
            try{trbPITCH.Value = Convert.ToInt32(value[trbPITCH.Name]);}catch{}
            try{cbFFTSIZE.Text = value[cbFFTSIZE.Name];}catch{}
            try{trbOVERLAP.Value = Convert.ToInt32(value[trbOVERLAP.Name]);}catch{}

            try{chkTREMOLO.Checked = Convert.ToBoolean(value[chkTREMOLO.Name]);}catch{}
            try{trbFREQUENCY.Value = Convert.ToInt32(value[trbFREQUENCY.Name]);}catch{}
            try{trbDEPTH_TREMOLO.Value = Convert.ToInt32(value[trbDEPTH_TREMOLO.Name]);}catch{}
            try{trbSHAPE.Value = Convert.ToInt32(value[trbSHAPE.Name]);}catch{}
            try{trbSKEW.Value = Convert.ToInt32(value[trbSKEW.Name]);}catch{}
            try{trbDUTY.Value = Convert.ToInt32(value[trbDUTY.Name]);}catch{}
            try{trbSQUARE.Value = Convert.ToInt32(value[trbSQUARE.Name]);}catch{}
            try{trbPHASE.Value = Convert.ToInt32(value[trbPHASE.Name]);}catch{}
            try{trbSPREAD.Value = Convert.ToInt32(value[trbSPREAD.Name]);}catch{}

            try{chkFLANGE.Checked = Convert.ToBoolean(value[chkFLANGE.Name]);}catch{}
            try{trbDEPTH_FLANGE.Value = Convert.ToInt32(value[trbDEPTH_FLANGE.Name]);}catch{}
            try{trbRATE_FLANGE.Value = Convert.ToInt32(value[trbRATE_FLANGE.Name]);}catch{}
            try{trbWETMIX_FLANGE.Value = Convert.ToInt32(value[trbWETMIX_FLANGE.Name]);}catch{}
            try{trbDRYMIX_FLANGE.Value = Convert.ToInt32(value[trbDRYMIX_FLANGE.Name]);}catch{}

            try{chkLOWPASS.Checked = Convert.ToBoolean(value[chkLOWPASS.Name]);}catch{}
            try{trbCUTOFF.Value = Convert.ToInt32(value[trbCUTOFF.Name]);}catch{}
            try{trbRESONANCE.Value = Convert.ToInt32(value[trbRESONANCE.Name]);}catch{}

            try{chkDISTORTION.Checked = Convert.ToBoolean(value[chkDISTORTION.Name]);}catch{}
            try{trbDISTORTION.Value = Convert.ToInt32(value[trbDISTORTION.Name]);}catch{}

            try{chkNOMALIZER.Checked = Convert.ToBoolean(value[chkNOMALIZER.Name]);}catch{}
            try{trbFADETIME.Value = Convert.ToInt32(value[trbFADETIME.Name]);}catch{}
            try{trbTHRESHHOLD.Value = Convert.ToInt32(value[trbTHRESHHOLD.Name]);}catch{}
            try{trbMAXAMP.Value = Convert.ToInt32(value[trbMAXAMP.Name]);}catch{}

            try{chkCOMPRESSOR.Checked = Convert.ToBoolean(value[chkCOMPRESSOR.Name]);}catch{}
            try{trbGAINMAKEUP.Value = Convert.ToInt32(value[trbGAINMAKEUP.Name]);}catch{}
            try{trbTHRESHOLD.Value = Convert.ToInt32(value[trbTHRESHOLD.Name]);}catch{}
            try{trbATTACK.Value = Convert.ToInt32(value[trbATTACK.Name]);}catch{}
            try{trbRELEASE.Value = Convert.ToInt32(value[trbRELEASE.Name]);}catch{}

            try{chkOSCILLATOR.Checked = Convert.ToBoolean(value[chkOSCILLATOR.Name]);}catch{}
            try{cbTYPE_OSCILLATOR.SelectedIndex = Convert.ToInt32(value[cbTYPE_OSCILLATOR.Name]);}catch{}
            try{numRATE_OSCILLATOR.Value = Convert.ToDecimal(value[numRATE_OSCILLATOR.Name]);}catch{}

            try{chkHIGHPASS.Checked = Convert.ToBoolean(value[chkHIGHPASS.Name]);}catch{}
            try{trbCUTOFF_HIGHPASS.Value = Convert.ToInt32(value[trbCUTOFF_HIGHPASS.Name]);}catch{}
            try{trbRESONANCE_HIGHPASS.Value = Convert.ToInt32(value[trbRESONANCE_HIGHPASS.Name]);}catch{}
            //Helper.UpdateMix();
            }
        }
        public void SetDefaultAll()
        {
            //Settings.Add("", "");
            button1_Click(null, null); button2_Click(null, null);
            button3_Click(null, null); button4_Click(null, null);
            button5_Click(null, null); button6_Click(null, null);
            button7_Click(null, null); button8_Click(null, null);
            button9_Click(null, null); button10_Click(null, null);
            button11_Click(null, null); button12_Click(null, null);
            button13_Click(null, null); button14_Click(null, null);
            button15_Click(null, null); button16_Click(null, null);
            button17_Click(null, null); button18_Click(null, null);
            button19_Click(null, null); button20_Click(null, null);
            button21_Click(null, null); button22_Click(null, null);
            button23_Click(null, null); button24_Click(null, null);
            button25_Click(null, null); button26_Click(null, null);
            button27_Click(null, null); button28_Click(null, null);
            button29_Click(null, null); button30_Click(null, null);
            button31_Click(null, null); button32_Click(null, null);
            button33_Click(null, null); button34_Click(null, null);
            button35_Click(null, null); button36_Click(null, null);
            button37_Click(null, null); button38_Click(null, null);
            button39_Click(null, null); button40_Click(null, null);
        }

        public Dictionary<string, string> InitPreset()
        {
            Dictionary<string, string> Settings = new Dictionary<string, string>(); //Settings.Clear();
            Settings.Add(trbGain.Name, "900");
            Settings.Add(rbFMOD_Channel.Name, "True");

            Settings.Add(chkECHO.Name, "False");
            Settings.Add(trbDELAY_ECHO.Name, "500"); Settings.Add(trbDECAYRATIO.Name, "500"); Settings.Add(trbWETMIX_ECHO.Name, "1000");

            Settings.Add(chkCHORUS.Name, "False");
            Settings.Add(trbDELAY_CHORUS.Name, "4000"); Settings.Add(trbRATE_CHORUS.Name, "800"); Settings.Add(trbFEEDBACK_CHORUS.Name, "1000");
            Settings.Add(trbDEPTH_CHORUS.Name, "20"); Settings.Add(trbDRYMIX_CHORUS.Name, "500"); Settings.Add(trbWETMIX1_CHORUS.Name, "500");
            Settings.Add(trbWETMIX2_CHORUS.Name, "500"); Settings.Add(trbWETMIX3_CHORUS.Name, "500");

            Settings.Add(chkPITCHSHIFT.Name, "False");
            Settings.Add(trbPITCH.Name, "1000"); Settings.Add(cbFFTSIZE.Name, "1024"); Settings.Add(trbOVERLAP.Name, "4");

            Settings.Add(chkTREMOLO.Name, "False");
            Settings.Add(trbFREQUENCY.Name, "400"); Settings.Add(trbDEPTH_TREMOLO.Name, "1000");
            Settings.Add(trbSHAPE.Name, "1000"); Settings.Add(trbSKEW.Name, "0"); Settings.Add(trbDUTY.Name, "500");
            Settings.Add(trbSQUARE.Name, "0"); Settings.Add(trbPHASE.Name, "0"); Settings.Add(trbSPREAD.Name, "500");

            Settings.Add(chkFLANGE.Name, "False");
            Settings.Add(trbDEPTH_FLANGE.Name, "1000"); Settings.Add(trbRATE_FLANGE.Name, "100");
            Settings.Add(trbWETMIX_FLANGE.Name, "450"); Settings.Add(trbDRYMIX_FLANGE.Name, "550");

            Settings.Add(chkLOWPASS.Name, "False");
            Settings.Add(trbCUTOFF.Name, "5000"); Settings.Add(trbRESONANCE.Name, "500");

            Settings.Add(chkDISTORTION.Name, "False");
            Settings.Add(trbDISTORTION.Name, "300");

            Settings.Add(chkNOMALIZER.Name, "False");
            Settings.Add(trbFADETIME.Name, "5000"); Settings.Add(trbTHRESHHOLD.Name, "100"); Settings.Add(trbMAXAMP.Name, "20");

            Settings.Add(chkCOMPRESSOR.Name, "False");
            Settings.Add(trbGAINMAKEUP.Name, "0"); Settings.Add(trbTHRESHOLD.Name, "0");
            Settings.Add(trbATTACK.Name, "50"); Settings.Add(trbRELEASE.Name, "50");

            Settings.Add(chkOSCILLATOR.Name, "False");
            Settings.Add(cbTYPE_OSCILLATOR.Name, "0"); Settings.Add(numRATE_OSCILLATOR.Name, "220");

            Settings.Add(chkHIGHPASS.Name, "False");
            Settings.Add(trbCUTOFF_HIGHPASS.Name, "5000"); Settings.Add(trbRESONANCE_HIGHPASS.Name, "100");
            return Settings;
        }

        private void ResetSetting()
        {

        }

        private void 프로파일저장SToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!System.IO.Directory.Exists(PresetDir)) { System.IO.Directory.CreateDirectory(PresetDir); }
            saveFileDialog1.InitialDirectory = Directory.Exists(LastFileSaveDirectory) ? LastFileSaveDirectory : PresetDir;

            saveFileDialog1.FileName = toolStripComboBox1.Text;
            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                LastFileSaveDirectory = Path.GetDirectoryName(saveFileDialog1.FileName);
                string[] a1 = HS_CSharpUtility.Utility.EtcUtility.SaveSetting(Settings);
                //string b =CSharpUtility.Utility.StringUtility.ConvertArrayToString(a, true);
                System.IO.File.WriteAllLines(saveFileDialog1.FileName, a1);
                ProfileName = System.IO.Path.GetFileNameWithoutExtension(saveFileDialog1.FileName);
            }
        }

        private void 닫기XToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void 새프리셋ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (DialogResult.Yes == MessageBox.Show("정말 모두 초기화하고 새 프리셋을 작성하시겠습니까?", "DSP 프리셋 초기화", MessageBoxButtons.YesNo, MessageBoxIcon.Warning))
            { DSPPreset = InitPreset(); toolStripComboBox1.SelectedIndex=0; this.Text = "DSP 관리자"; }
        }

        public string PresetDir { get { return Application.StartupPath.Replace("/", "\\") + "\\Preset\\DSP"; } }
        string LastPreset = "(기본값)";
        private void 새로고침ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Presets.Clear(); toolStripComboBox1.Items.Clear();
            Presets.Add(InitPreset()); toolStripComboBox1.Items.Add("(기본값)");
            foreach (string a in Directory.GetFiles(LastFolderDirectory, "*.dspprof"))
            {
                try
                {
                    Dictionary<string, string> b =
                        HS_CSharpUtility.Utility.EtcUtility.LoadSetting(File.ReadAllLines(a));
                    Presets.Add(b);
                    toolStripComboBox1.Items.Add(Path.GetFileNameWithoutExtension(a));
                }
                catch { }
            }
            if (toolStripComboBox1.Items.IndexOf(LastPreset) != -1) toolStripComboBox1.SelectedIndex = toolStripComboBox1.Items.IndexOf(LastPreset);
            else toolStripComboBox1.SelectedIndex = 0;
        }

        string _PresetText;
        public string PresetText { get { return _PresetText; } 
            set { this.Text = value!=null&&value!=""?"DSP 관리자 - [" + value + "]":"DSP 관리자"; } }
        private void toolStripComboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (toolStripComboBox1.SelectedIndex != -1)
            {
                DSPPreset = InitPreset();
                DSPPreset = Presets[toolStripComboBox1.SelectedIndex];
                PresetText = toolStripComboBox1.Text;
                LastPreset = toolStripComboBox1.Items[toolStripComboBox1.SelectedIndex].ToString();
                if (toolStripComboBox1.SelectedIndex == 0) { this.Text = "DSP 관리자"; 제거ToolStripMenuItem.Enabled = false; }
                else 제거ToolStripMenuItem.Enabled = true;
            }
        }

        private void 제거ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (toolStripComboBox1.Items.Count>1&&toolStripComboBox1.SelectedIndex!=0){
            if (DialogResult.Yes == MessageBox.Show("정말 선택된 프리셋을 목록에서 제거 하시겠습니까?", "DSP 프리셋 선택된 목록 제거", MessageBoxButtons.YesNo, MessageBoxIcon.Warning))
            { Presets.RemoveAt(toolStripComboBox1.SelectedIndex); toolStripComboBox1.Items.RemoveAt(toolStripComboBox1.SelectedIndex); toolStripComboBox1.SelectedIndex = 0; }}
        }

        private void 정렬ToolStripMenuItem_CheckedChanged(object sender, EventArgs e)
        {
            string a = toolStripComboBox1.Text;
            if (정렬ToolStripMenuItem.Checked)
            {
                toolStripComboBox1.Sorted = true;
                정렬ToolStripMenuItem.BackColor = System.Drawing.Color.Lime;
            }
            else
            {
                toolStripComboBox1.Sorted = false;
                정렬ToolStripMenuItem.BackColor = System.Drawing.Color.Transparent;
            }
            if (toolStripComboBox1.Items.IndexOf(a) != -1) { toolStripComboBox1.SelectedIndex = toolStripComboBox1.Items.IndexOf(a); }
        }

        private void 정렬ToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void 폴더선택ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!System.IO.Directory.Exists(PresetDir)) { System.IO.Directory.CreateDirectory(PresetDir); }
            folderBrowserDialog1.SelectedPath = Directory.Exists(LastFolderDirectory) ? LastFolderDirectory : PresetDir; ;
            if (DialogResult.OK == folderBrowserDialog1.ShowDialog())
            { LastFolderDirectory = folderBrowserDialog1.SelectedPath; }
        }

        Dictionary<string, string> PresetPath = new Dictionary<string, string>();
        string LastFileOpenDirectory
        {
            get
            {
                if (File.Exists(PresetDir + "\\LastDSPPath.ini"))
                {
                    string[] a = File.ReadAllLines(PresetDir + "\\LastDSPPath.ini");
                    string a1 = HS_CSharpUtility.Utility.EtcUtility.LoadSetting(a)["LastFileOpenDirectory"];
                    if (a1 != null || a1 != "") { return Directory.Exists(a1) ? a1 : PresetDir; }
                    else { return PresetDir; }
                }
                else { return PresetDir; }
            }
            set
            {
                if (PresetPath.Count < 3)
                {
                    PresetPath.Clear(); PresetPath.Add("LastFileOpenDirectory", value);
                    PresetPath.Add("LastFileSaveDirectory", PresetDir);
                    PresetPath.Add("LastFolderDirectory", PresetDir);
                }
                else { PresetPath["LastFileOpenDirectory"] = value; }
                File.WriteAllLines(PresetDir + "\\LastDSPPath.ini", HS_CSharpUtility.Utility.EtcUtility.SaveSetting(PresetPath));
            }
        }
        string LastFileSaveDirectory
        {
            get
            {
                if (File.Exists(PresetDir + "\\LastDSPPath.ini"))
                {
                    string[] a = File.ReadAllLines(PresetDir + "\\LastDSPPath.ini");
                    string a1 = HS_CSharpUtility.Utility.EtcUtility.LoadSetting(a)["LastFileSaveDirectory"];
                    if (a1 != null || a1 != "") { return Directory.Exists(a1) ? a1 : PresetDir; }
                    else { return PresetDir; }
                }
                else { return PresetDir; }
            }
            set
            {
                if (PresetPath.Count < 3)
                {
                    PresetPath.Clear(); PresetPath.Add("LastFileOpenDirectory", PresetDir);
                    PresetPath.Add("LastFileSaveDirectory", value);
                    PresetPath.Add("LastFolderDirectory", PresetDir);
                }
                else { PresetPath["LastFileSaveDirectory"] = value; }
                File.WriteAllLines(PresetDir + "\\LastDSPPath.ini", HS_CSharpUtility.Utility.EtcUtility.SaveSetting(PresetPath));
            }
        }
        string LastFolderDirectory
        {
            get
            {
                if (File.Exists(PresetDir + "\\LastDSPPath.ini"))
                {
                    string[] a = File.ReadAllLines(PresetDir + "\\LastDSPPath.ini");
                    string a1 = HS_CSharpUtility.Utility.EtcUtility.LoadSetting(a)["LastFolderDirectory"];
                    if (a1 != null || a1 != "") { return Directory.Exists(a1) ? a1 : PresetDir; }
                    else { return PresetDir; }
                }
                else { return PresetDir; }
            }
            set
            {
                if (PresetPath.Count < 2)
                {
                    PresetPath.Clear(); PresetPath.Add("LastFileOpenDirectory", PresetDir);
                    PresetPath.Add("LastFileSaveDirectory", PresetDir);
                    PresetPath.Add("LastFolderDirectory", value);
                }
                else { PresetPath["LastFolderDirectory"] = value; }
                File.WriteAllLines(PresetDir + "\\LastDSPPath.ini", HS_CSharpUtility.Utility.EtcUtility.SaveSetting(PresetPath));
            }
        }

        private void 프리셋경로재설정ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("정말 모든 DSP관리자 프리셋 경로를 재설정 하시겠습니까?", "DSP관리자 프리셋 경로 모두 재설정",
                MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
            { LastFolderDirectory = LastFileSaveDirectory = LastFolderDirectory = PresetDir; }
        }

        private void 프리셋열기경로재설정ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("정말 DSP프리셋 열기 경로를 재설정 하시겠습니까?", "DSP관리자 프리셋 열기 경로 재설정",
                MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
            { LastFileOpenDirectory = PresetDir; }
        }

        private void 프리셋저장경로재설정ToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("정말 DSP관리자 프리셋 저장 경로를 재설정 하시겠습니까?", "DSP관리자 프리셋 열기 저장 재설정",
                MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
            { LastFileSaveDirectory = PresetDir; }
        }

        private void 프리셋폴더경로재설정ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("정말 DSP관리자 프리셋 폴더 경로를 재설정 하시겠습니까?", "DSP관리자 프리셋 폴더 경로 재설정",
                MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
            { LastFolderDirectory = PresetDir; }
        }

        private void menuStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

        private void trbCUTOFF_HIGHPASS_ValueChanged(object sender, EventArgs e)
        {
            DSPHelper[10].SetParameter((int)HS_Audio_Lib.DSP_HIGHPASS.CUTOFF, trbCUTOFF_HIGHPASS.Value);
            lblCUTOFF_HIGHPASS.Text = trbCUTOFF_HIGHPASS.Value.ToString();
            Settings[trbCUTOFF_HIGHPASS.Name] = trbCUTOFF_HIGHPASS.Value.ToString();
        }

        private void trbRESONANCE_HIGHPASS_ValueChanged(object sender, EventArgs e)
        {
            DSPHelper[10].SetParameter((int)HS_Audio_Lib.DSP_HIGHPASS.RESONANCE, trbRESONANCE_HIGHPASS.Value*0.01f);
            lblRESONANCE_HIGHPASS.Text = trbRESONANCE_HIGHPASS.Value.ToString();
            Settings[trbRESONANCE_HIGHPASS.Name] = trbRESONANCE_HIGHPASS.Value.ToString();
        }

        private void cbFFTSIZE_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (chkPITCHSHIFT.Checked)
            { toolStripStatusLabel2.Text = DSPHelper[2].SetParameter((int)HS_Audio_Lib.DSP_PITCHSHIFT.FFTSIZE, float.Parse(cbFFTSIZE.SelectedItem.ToString())).ToString(); }
            Settings[cbFFTSIZE.Name] = cbFFTSIZE.Text;
        }

        private void 설정TToolStripMenuItem_DropDownOpening(object sender, EventArgs e)
        {자동DSP등록ToolStripMenuItem.Checked = AutoUpdate; }

        private void 자동DSP등록ToolStripMenuItem_CheckedChanged(object sender, EventArgs e)
        {AutoUpdate = 자동DSP등록ToolStripMenuItem.Checked; }

        private void frmDSP_Load(object sender, EventArgs e)
        {

        }
    }
}
