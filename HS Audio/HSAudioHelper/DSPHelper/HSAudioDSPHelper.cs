﻿using System;
using System.Collections.Generic;

using System.Text;
using HS_Audio_Lib;
using System.Runtime.InteropServices;

namespace HS_Audio.DSP
{
    /*
    public interface IFMODDSPHelper
    {
        FMODDSPHelper DSPHelper { get; private set; }
    }*/
    [Serializable]
    public class HSAudioDSPHelper : IDisposable//: IFMODDSPHelper
    {
        /// <summary>
        /// 빈 DSP를 만듭니다. (만들고 반드시 Helper개체를 할당 시켜 주시기 바랍니다.)
        /// </summary>
        /// <returns></returns>
        public static HSAudioDSPHelper Empty() { return new HSAudioDSPHelper(); }

        internal static HS_Audio_Lib.DSP dsp_static;
        public DSP_READCALLBACK dspreadcallback;// = new HS_Audio_Lib.DSP_READCALLBACK(READCALLBACK);
        public delegate RESULT DSP_READCALLBACK1(ref DSP_STATE dsp_state, IntPtr inbuffer, IntPtr outbuffer, uint length, int inchannels, int outchannels);
        
        /*
        private HS_Audio_Lib.RESULT READCALLBACK(ref HS_Audio_Lib.DSP_STATE dsp_state, IntPtr inbuf, IntPtr outbuf, uint length, int inchannels, int outchannels)
        {
            uint count = 0;
            int count2 = 0;
            //IntPtr thisdspraw = dsp_state.instance;
            //if (dsp_static != null) { dsp_static.setRaw(thisdspraw); }
            //else { dsp_static = new HS_Audio_Lib.DSP(); dsp_static.setRaw(thisdspraw); }

            unsafe
            {
                float* inbuffer = (float*)inbuf.ToPointer();
                float* outbuffer = (float*)outbuf.ToPointer();

                for (count = 0; count < length; count++)
                {
                    for (count2 = 0; count2 < outchannels; count2++)
                    {
                        outbuffer[(count * outchannels) + count2] = inbuffer[(count * inchannels) + count2];
                    }
                }
            }

            return HS_Audio_Lib.RESULT.OK;
        }*/

        protected HS_Audio_Lib.DSP DSP;
        /// <summary>
        /// 전문가 외에는 손대지 말것.
        /// </summary>
        public HS_Audio_Lib.DSP GetDSP { get { return DSP; }}
        protected HS_Audio_Lib.DSP_DESCRIPTION DSPDescription;
        /// <summary>
        /// 전문가 외에는 손대지 말것.
        /// </summary>
        public HS_Audio_Lib.DSP_DESCRIPTION GetDSPDescription { get { return DSPDescription; } }
        protected HS_Audio_Lib.DSPConnection DSPConnection;
        /// <summary>
        /// 전문가 외에는 손대지 말것.
        /// </summary>
        public HS_Audio_Lib.DSPConnection GetDSPConnection { get { return DSPConnection; } }
        public string DSPConnectionName { get { return new string(DSPDescription.name);} private set { DSPDescription.name = value.ToCharArray(); } }

        public HSAudioHelper Helper{get; protected set;}

        private HS_Audio_Lib.RESULT _result;
        public HS_Audio_Lib.RESULT Result { get { return _result; } internal set { _result = value; /*HSAudioHelper.ERRCHECK(value);*/ } }


        protected HSAudioDSPHelper() { }
        public HSAudioDSPHelper(HSAudioHelper Value)
        {
            Helper = Value;
            DSP = new HS_Audio_Lib.DSP();
            Helper.BeginMusicChanged += Helper_BeginMusicChanged;
            Helper.MusicChanged += Helper_MusicChanged;
        }
        public HSAudioDSPHelper(HSAudioHelper Value,bool System=false)
        {
            Helper = Value;
            INIT(System);
            /*
            result = fh.system.createDSP(ref DSPDescription, ref dsp);
            result = fh.system.addDSP(dsp, ref dspcon);
            this.DSPDescription.channels = 0;*/

            //this.DSPDescription.read = this.dspreadcallback;
            HS_Audio_Lib.DSP dsp1 = dsp_static;
            Helper.BeginMusicChanged += Helper_BeginMusicChanged;
            Helper.MusicChanged+=Helper_MusicChanged;

        }
        public HSAudioDSPHelper(HSAudioHelper Value, HS_Audio_Lib.DSP_TYPE DSPType, bool System=false)
        {
            Helper = Value;
            INIT(System, DSPType);/*
            result = fh.system.createDSPByType(DSPType, ref dsp);
            if (System) { result = fh.system.addDSP(dsp, ref dspcon); }
            else { result = fh.channel.addDSP(dsp, ref dspcon);}
            this.DSPDescription.channels = 0;*/

            //this.DSPDescription.read = this.dspreadcallback;
            HS_Audio_Lib.DSP dsp1 = dsp_static;
            Helper.BeginMusicChanged += Helper_BeginMusicChanged;
            Helper.MusicChanged += Helper_MusicChanged;
        }
        public HSAudioDSPHelper(HSAudioHelper Value,DSP_READCALLBACK CallBack,bool System=false)
        {
            Helper = Value;
            INIT(System, DSPType);
            dspreadcallback = CallBack;
            DSPDescription.read = dspreadcallback;
            /*
            result = fh.system.createDSP(ref DSPDescription, ref dsp);
            result = fh.system.addDSP(dsp, ref dspcon);
            this.DSPDescription.channels = 0;*/

            //this.DSPDescription.read = this.dspreadcallback;
            HS_Audio_Lib.DSP dsp1 = dsp_static;
            Helper.BeginMusicChanged += Helper_BeginMusicChanged;
            Helper.MusicChanged += Helper_MusicChanged;

        }
        public HSAudioDSPHelper(HSAudioHelper Value, DSP_READCALLBACK CallBack, HS_Audio_Lib.DSP_TYPE DSPType, bool System=false)
        {
            Helper = Value;
            INIT(System, DSPType);
            dspreadcallback = CallBack;
            DSPDescription.read = dspreadcallback;
            /*
            result = fh.system.createDSPByType(DSPType, ref dsp);
            if (System) { result = fh.system.addDSP(dsp, ref dspcon); }
            else { result = fh.channel.addDSP(dsp, ref dspcon);}
            this.DSPDescription.channels = 0;*/

            //this.DSPDescription.read = this.dspreadcallback;
            HS_Audio_Lib.DSP dsp1 = dsp_static;
            Helper.BeginMusicChanged += Helper_BeginMusicChanged;
            Helper.MusicChanged+=Helper_MusicChanged;
        }

        /// <summary>
        /// DSP를 재등록 시킵니다. 기본은 HS_Audio_Helper.DSP.FMODDSPHelper.IsSystem 의 값으로 할당됩니다.
        /// </summary>
        public void ReRegisterDSP() {ReRegisterDSP(IsSystem); }
        /// <summary>
        /// DSP를 재등록 시킵니다.
        /// </summary>
        /// <param name="System">True면 FMOD.System 에 할당되고 False면 FMOD.Channel 에 할당됩니다.</param>
        public void ReRegisterDSP(bool System)
        {
            Disconnect();
            INIT(System, DSPType);
            dspreadcallback = DSPDescription.read;
            //IntPtr ipt = IntPtr.Zero; if (DSP != null) DSP.getUserData(ref ipt);
            //if (DSP != null && ipt != IntPtr.Zero) DSP.setUserData(ipt);
            Connect();
        }
        /// <summary>
        /// DSP를 재등록 시킵니다.
        /// </summary>
        /// <param name="System">True면 FMOD.System 에 할당되고 False면 FMOD.Channel 에 할당됩니다.</param>
        /// <param name="ByPass">처음부터 우회하고 등록할껀지의 여부 입니다.</param>
        public void ReRegisterDSP(bool System, bool ByPass)
        {
            this.Bypass = Bypass;
            Disconnect();
            INIT(System, DSPType);
            dspreadcallback = DSPDescription.read;
            Connect();
        }
        /// <summary>
        /// DSP를 재등록 시킵니다.
        /// </summary>
        /// <param name="DSPType">DSP타입 입니다.</param>
        /// <param name="System">True면 FMOD.System 에 할당되고 False면 FMOD.Channel 에 할당됩니다.</param>
        public void ReRegisterDSP(HS_Audio_Lib.DSP_TYPE DSPType, bool System)
        {
            //IntPtr ipt = IntPtr.Zero; if (DSP != null) DSP.getUserData(ref ipt);
            //if (DSP != null && ipt != IntPtr.Zero) DSP.setUserData(ipt);
            Disconnect();
            INIT(System, DSPType);
            dspreadcallback = DSPDescription.read;
            Connect();
        }
        /// <summary>
        /// DSP를 재등록 시킵니다.
        /// </summary>
        /// <param name="DSPType">DSP타입 입니다.</param>
        /// <param name="System">True면 FMOD.System 에 할당되고 False면 FMOD.Channel 에 할당됩니다.</param>
        /// <param name="ByPass">처음부터 우회하고 등록할껀지의 여부 입니다.</param>
        public void ReRegisterDSP(HS_Audio_Lib.DSP_TYPE DSPType, bool System, bool ByPass)
        {
            this.Bypass = Bypass;
            Disconnect();
            INIT(System, DSPType);
            dspreadcallback = DSPDescription.read;
            Connect();
        }

        protected void INIT(bool System=false, DSP_TYPE Type = DSP_TYPE.UNKNOWN)
        {
            if (Helper != null || Helper.system != null)
            {
                this.IsSystem = System;
                DSPType = Type;

                CurrentDSPType = DSPType;
                DSPDescription.read = this.dspreadcallback;
                if (DSPType == DSP_TYPE.UNKNOWN || DSPType == DSP_TYPE.WINAMPPLUGIN || DSPType == DSP_TYPE.VSTPLUGIN) Result = Helper.system.createDSP(ref DSPDescription, ref DSP);
                else Result = Helper.system.createDSPByType(DSPType, ref DSP);
                //this.DSPDescription.channels = 0;
                //this.DSPDescription.read = this.dspreadcallback;
            }
        }
        protected void INIT(HSAudioHelper Helper,bool System = false, DSP_TYPE Type = DSP_TYPE.UNKNOWN)
        {
            this.Helper = Helper;
            if (Helper != null || Helper.system != null)
            {
                this.IsSystem = System;
                DSPType = Type;

                CurrentDSPType = DSPType;
                if (DSPType == DSP_TYPE.UNKNOWN) Result = Helper.system.createDSP(ref DSPDescription, ref DSP);
                else Result = Helper.system.createDSPByType(DSPType, ref DSP);
                //this.DSPDescription.channels = 0;
                //this.DSPDescription.read = this.dspreadcallback;
            }
        }

        public HS_Audio_Lib.RESULT DisConnect() { IsConnect = false;return Disconnect();  }
        HS_Audio_Lib.RESULT Disconnect(){try { if (DSP != null && !Helper.IsRestart) { Helper.IsRestart = false; return DSP.remove(); } else { Helper.IsRestart = false; } } catch { } return HS_Audio_Lib.RESULT.OK;}

        public bool IsConnect { get; private set; }
        public bool Connect()
        {
            try
            {
                Disconnect();
                Result = DSP.setBypass(Bypass);
                if (IsSystem) Result = Helper.system.addDSP(DSP, ref DSPConnection);
                else Result = Helper.channel.addDSP(DSP, ref DSPConnection);//Result = Helper.dsp.DSP.addInput(DSP, ref DSPConnection);
                IsConnect = (Result == RESULT.OK);
                Gain = CurrentMix;
                Bypass = CurrentBypass;
                return true;
            }
            catch {return false;}
        }
        public bool Connect(bool System)
        {
            try
            {
                Disconnect();
                IsSystem = System;
                Result = DSP.setBypass(Bypass);
                if (System) Result = Helper.system.addDSP(DSP, ref DSPConnection);
                else Result = Helper.channel.addDSP(DSP, ref DSPConnection);//Result = Helper.dsp.DSP.addInput(DSP, ref DSPConnection);
                
                IsConnect = (Result == RESULT.OK);
                Gain = CurrentMix;
                Bypass = CurrentBypass;
                return true;
            }
            catch {return false;}
        }

        public bool UpdateMainDSP()
        {
            try
            {
                float a = Helper.dsp.Gain;
                Helper.dsp.Gain = a;
                return true;
            }
            catch{return false;}
        }

        public void Dispose()
        {
            DisConnect();
            try
            {
                Helper.BeginMusicChanged -= Helper_BeginMusicChanged;
                Helper.MusicChanged -= Helper_MusicChanged;
                //IntPtr pt = DSP.getRaw();
                //Marshal.FreeCoTaskMem(pt);
                DSP = null;
                Helper = null;
                GC.Collect();
            }catch{}
        }
        
        public HS_Audio_Lib.RESULT Reset() {return DSP.reset(); }

        public HS_Audio_Lib.RESULT SetParameter(DSPParameter Parameter)
        {
            return DSP.setParameter(Parameter.Index, Parameter.Value);
        }
        /// <summary>
        /// DSP를 설정합니다.
        /// </summary>
        /// <param name="index">인덱스 입니다.</param>
        /// <param name="Value"></param>
        /// <returns></returns>
        public HS_Audio_Lib.RESULT SetParameter(int index, float Value)
        {
            return DSP.setParameter(index, Value);
        }
        
        DSPParameter _GetParameter;
        /// <summary>
        /// DSP를 가져옵니다.
        /// </summary>
        /// <param name="index">FMOD.DSP_TYPE에 대한 각 값의 인덱스 입니다. FMOD.DSP_TYPE에 대한 각 값의 인덱스 입니다. (만약 FMOD.DSP_TYPE 를 PARAMEQ 로 줬다면 60HZ의 설정은 (FMOD.DSP_PARAMEQ.BANDWIDTH, 60) 형태로 입력하면 됩니다.)</param>
        /// <param name="Name">가져올 이름 입니다.</param>
        /// <returns></returns>
        public DSPParameter GetParameter(int index, string Name)
        {
            StringBuilder sb = new StringBuilder(Name);
            _GetParameter.Index = index;
            DSP.getParameter(index, ref _GetParameter.Value, sb, sb.Length);
            return _GetParameter;
        }

        public HS_Audio_Lib.RESULT ShowConfigDialog(IntPtr Handle, bool Show)
        {
            return DSP.showConfigDialog(Handle, Show);
        }


        //public bool SetCallBack(DSP_READCALLBACK CallBack) { try { DSPDescription.read = CallBack; return true; } catch { return false; } }

        bool _Bypass;
        //float LastGain = 1.0f;
        public bool CurrentBypass;
        public bool Bypass 
        { 
            get { Result = DSP.getBypass(ref _Bypass); return _Bypass; }
            set { CurrentBypass = value; Result = DSP.setBypass(value); /*if (value) { LastGain = Gain; Gain = 1.0f; } else { Gain = LastGain; }*/ } 
        }

        public bool IsSystem;

        public HS_Audio_Lib.DSP_TYPE CurrentDSPType { get; protected set; }

        private HS_Audio_Lib.DSP_TYPE _DSPType = DSP_TYPE.UNKNOWN;
        public HS_Audio_Lib.DSP_TYPE DSPType
        {
            get { return _DSPType; }
            set{_DSPType = value;}
        }

        public float CurrentMix=1;
        public float Gain 
        {
            get { float Mix = 0; Result = DSPConnection.getMix(ref Mix); return Mix; }
            set { CurrentMix= value; try{Result = DSPConnection.setMix(value); }catch(NullReferenceException){Result = RESULT.ERR_UNINITIALIZED;}}
        }
        /*
        public FMOD.RESULT SetMix(bool System, float Mix)
        {

        }
        public float GetMIx(bool System)
        {
            float _SetMix;
            if(System)
                dspcon.
        }*/

        DSPSpeakerActive _Speaker = new DSPSpeakerActive(SPEAKER.FRONT_LEFT | SPEAKER.FRONT_RIGHT,true);
        public DSPSpeakerActive Speaker
        {
            get {Result=DSP.getSpeakerActive(_Speaker.Speaker, ref _Speaker.Active); return _Speaker; }
            set { Result = DSP.setSpeakerActive(value.Speaker, value.Active); }
        }

        public IntPtr Instance
        {
            get {try{return DSP!=null?DSP.getRaw():IntPtr.Zero;}catch{return IntPtr.Zero;}}
            set{DSP.setRaw(value);}
        }

        ~HSAudioDSPHelper() { DisConnect();
        }

        #region 이벤트 처리기

        private void Helper_BeginMusicChanged(HSAudioHelper Helper, int index, bool Error)
        {
            try{if(!this.IsSystem)this.Disconnect();}catch{}
        }

        private void Helper_MusicChanged(HSAudioHelper Helper, int index, bool Error)
        {
            try
            {
                if (!this.IsSystem&&this.IsConnect) this.Connect();//this.ReRegisterDSP();
            }catch{}
        }
        #endregion
    }
    /*
    public interface IFMODDSPHelper
    {
        float MIx { get; set; }
        FMOD.RESULT result{get;set;}
        FMOD.RESULT SetParameter(DSPParameter Parameter);
        HS_Audio_Helper.FMODHelper Helper { get; set; }
        FMOD.DSPConnection dspcon { get; set; }
    }*/

    /// <summary>
    /// FMOD.DSP의 스피커에대한 구조체 입니다.
    /// </summary>
    [Serializable]
    public struct DSPSpeakerActive
    {
        public DSPSpeakerActive(HS_Audio_Lib.SPEAKER Speaker, bool Active)
        { this.Active = Active; this.Speaker = Speaker; }
        public bool Active;
        public HS_Audio_Lib.SPEAKER Speaker;
    }
    /// <summary>
    /// FMOD.DSP의 파라미터 값에대한 구조체 입니다.
    /// </summary>
    [Serializable]
    public struct DSPParameter
    {
        /// <summary>
        /// DSP구조체의 인스턴스를 초기화합니다.
        /// </summary>
        /// <param name="Index">FMOD.DSP_TYPE에 대한 각 값의 인덱스 입니다. (만약 FMOD.DSP_TYPE 를 PARAMEQ 로 줬다면 60HZ의 설정은 (FMOD.DSP_PARAMEQ.BANDWIDTH, 60) 형태로 입력하면 됩니다.)</param>
        /// <param name="Value">인덱스에 대한 값 입니다.</param>
        public DSPParameter(int Index, float Value) { this.Index = Index; this.Value = Value; }
        /// <summary>
        /// DSP_TYPE에 대한 각 값의 인덱스 입니다.
        /// </summary>
        public int Index;
        /// <summary>
        /// 인덱스에 대한 값 입니다.
        /// </summary>
        public float Value;
    }
    /// <summary>
    /// 
    /// </summary>
    [Serializable]
    public struct DSPCallBack
    {
        public DSPCallBack(DSP_STATE dsp_state,IntPtr inbuf, IntPtr outbuf, uint length, int inchannels, int outchannels/*, FMOD.RESULT Result*/)
        { this.dsp_state = dsp_state; this.inbuf = inbuf; this.outbuf = outbuf; this.length = length; this.inchannels = inchannels; this.outchannels = outchannels; /*this.Result = Result;*/ }
        public HS_Audio_Lib.DSP_STATE dsp_state;
        public IntPtr inbuf;
        public IntPtr outbuf;
        public uint length;
        public int inchannels;
        public int outchannels;
        //public FMOD.RESULT Result;
    }
}
