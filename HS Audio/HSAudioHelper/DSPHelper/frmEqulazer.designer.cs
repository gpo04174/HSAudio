﻿namespace HS_Audio_Helper.Forms
{
    partial class frmEqulazer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            for (int i = 0; i < feq.Count; i++) { try { feq[i].Dispose(); } catch { } }
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmEqulazer));
            this.label2 = new System.Windows.Forms.Label();
            this.trb550Hz = new System.Windows.Forms.TrackBar();
            this.button20 = new System.Windows.Forms.Button();
            this.button21 = new System.Windows.Forms.Button();
            this.trb40Hz = new System.Windows.Forms.TrackBar();
            this.button1 = new System.Windows.Forms.Button();
            this.trb64Hz = new System.Windows.Forms.TrackBar();
            this.button2 = new System.Windows.Forms.Button();
            this.trb78Hz = new System.Windows.Forms.TrackBar();
            this.button3 = new System.Windows.Forms.Button();
            this.button11 = new System.Windows.Forms.Button();
            this.trb90Hz = new System.Windows.Forms.TrackBar();
            this.trb650Hz = new System.Windows.Forms.TrackBar();
            this.button4 = new System.Windows.Forms.Button();
            this.button8 = new System.Windows.Forms.Button();
            this.trb110Hz = new System.Windows.Forms.TrackBar();
            this.trb300Hz = new System.Windows.Forms.TrackBar();
            this.button5 = new System.Windows.Forms.Button();
            this.button9 = new System.Windows.Forms.Button();
            this.trb125Hz = new System.Windows.Forms.TrackBar();
            this.trb400Hz = new System.Windows.Forms.TrackBar();
            this.button6 = new System.Windows.Forms.Button();
            this.button10 = new System.Windows.Forms.Button();
            this.trb200Hz = new System.Windows.Forms.TrackBar();
            this.trb500Hz = new System.Windows.Forms.TrackBar();
            this.button7 = new System.Windows.Forms.Button();
            this.button22 = new System.Windows.Forms.Button();
            this.button23 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.trb800Hz = new System.Windows.Forms.TrackBar();
            this.button26 = new System.Windows.Forms.Button();
            this.button27 = new System.Windows.Forms.Button();
            this.trb950Hz = new System.Windows.Forms.TrackBar();
            this.button30 = new System.Windows.Forms.Button();
            this.button25 = new System.Windows.Forms.Button();
            this.button28 = new System.Windows.Forms.Button();
            this.trb9KHz = new System.Windows.Forms.TrackBar();
            this.button29 = new System.Windows.Forms.Button();
            this.button24 = new System.Windows.Forms.Button();
            this.button32 = new System.Windows.Forms.Button();
            this.button31 = new System.Windows.Forms.Button();
            this.button36 = new System.Windows.Forms.Button();
            this.button35 = new System.Windows.Forms.Button();
            this.trb7KHz = new System.Windows.Forms.TrackBar();
            this.button34 = new System.Windows.Forms.Button();
            this.trb8KHz = new System.Windows.Forms.TrackBar();
            this.button33 = new System.Windows.Forms.Button();
            this.button19 = new System.Windows.Forms.Button();
            this.trb10KHz = new System.Windows.Forms.TrackBar();
            this.trb11KHz = new System.Windows.Forms.TrackBar();
            this.button12 = new System.Windows.Forms.Button();
            this.button18 = new System.Windows.Forms.Button();
            this.trb12KHz = new System.Windows.Forms.TrackBar();
            this.trb13KHz = new System.Windows.Forms.TrackBar();
            this.button13 = new System.Windows.Forms.Button();
            this.button17 = new System.Windows.Forms.Button();
            this.trb14KHz = new System.Windows.Forms.TrackBar();
            this.trb15KHz = new System.Windows.Forms.TrackBar();
            this.button14 = new System.Windows.Forms.Button();
            this.button16 = new System.Windows.Forms.Button();
            this.trb16KHz = new System.Windows.Forms.TrackBar();
            this.trb17KHz = new System.Windows.Forms.TrackBar();
            this.button15 = new System.Windows.Forms.Button();
            this.trb18KHz = new System.Windows.Forms.TrackBar();
            this.button39 = new System.Windows.Forms.Button();
            this.trb19KHz = new System.Windows.Forms.TrackBar();
            this.button38 = new System.Windows.Forms.Button();
            this.trb20KHz = new System.Windows.Forms.TrackBar();
            this.button37 = new System.Windows.Forms.Button();
            this.trb6KHz = new System.Windows.Forms.TrackBar();
            this.trb5KHz = new System.Windows.Forms.TrackBar();
            this.trb900Hz = new System.Windows.Forms.TrackBar();
            this.trb4KHz = new System.Windows.Forms.TrackBar();
            this.trb750Hz = new System.Windows.Forms.TrackBar();
            this.trb3KHz = new System.Windows.Forms.TrackBar();
            this.trb600Hz = new System.Windows.Forms.TrackBar();
            this.trb2KHz = new System.Windows.Forms.TrackBar();
            this.trb1KHz = new System.Windows.Forms.TrackBar();
            this.trb850Hz = new System.Windows.Forms.TrackBar();
            this.trb700Hz = new System.Windows.Forms.TrackBar();
            this.panel1 = new System.Windows.Forms.Panel();
            this.rbChannel = new System.Windows.Forms.RadioButton();
            this.rbMainOuput = new System.Windows.Forms.RadioButton();
            this.label4 = new System.Windows.Forms.Label();
            this.trbGain = new System.Windows.Forms.TrackBar();
            this.EQUse = new System.Windows.Forms.CheckBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.파일FToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.새프리셋ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.프리셋열기ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.프리셋저장ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.끝내기XToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.설정TToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.프리셋경로재설정ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.프리셋열기경로재설정ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.프리셋저장경로재설정ToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.프리셋폴더경로재설정ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.자동DSP등록ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.정렬ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripComboBox1 = new System.Windows.Forms.ToolStripComboBox();
            this.폴더선택ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.새로고침ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.자동으로새로고침ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.제거ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            ((System.ComponentModel.ISupportInitialize)(this.trb550Hz)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trb40Hz)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trb64Hz)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trb78Hz)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trb90Hz)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trb650Hz)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trb110Hz)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trb300Hz)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trb125Hz)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trb400Hz)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trb200Hz)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trb500Hz)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trb800Hz)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trb950Hz)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trb9KHz)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trb7KHz)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trb8KHz)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trb10KHz)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trb11KHz)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trb12KHz)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trb13KHz)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trb14KHz)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trb15KHz)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trb16KHz)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trb17KHz)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trb18KHz)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trb19KHz)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trb20KHz)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trb6KHz)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trb5KHz)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trb900Hz)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trb4KHz)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trb750Hz)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trb3KHz)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trb600Hz)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trb2KHz)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trb1KHz)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trb850Hz)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trb700Hz)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trbGain)).BeginInit();
            this.panel2.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(5, 16);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(25, 13);
            this.label2.TabIndex = 33;
            this.label2.Text = "+10";
            // 
            // trb550Hz
            // 
            this.trb550Hz.AutoSize = false;
            this.trb550Hz.Location = new System.Drawing.Point(382, 11);
            this.trb550Hz.Maximum = 300;
            this.trb550Hz.Minimum = 5;
            this.trb550Hz.Name = "trb550Hz";
            this.trb550Hz.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.trb550Hz.Size = new System.Drawing.Size(17, 113);
            this.trb550Hz.TabIndex = 10;
            this.trb550Hz.TickStyle = System.Windows.Forms.TickStyle.None;
            this.trb550Hz.Value = 100;
            this.trb550Hz.ValueChanged += new System.EventHandler(this.trackBar11_ValueChanged);
            // 
            // button20
            // 
            this.button20.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.button20.Location = new System.Drawing.Point(25, 276);
            this.button20.Name = "button20";
            this.button20.Size = new System.Drawing.Size(32, 21);
            this.button20.TabIndex = 56;
            this.button20.Text = "1KHz";
            this.button20.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.button20.UseVisualStyleBackColor = true;
            this.button20.Click += new System.EventHandler(this.button20_Click);
            // 
            // button21
            // 
            this.button21.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.button21.Location = new System.Drawing.Point(57, 276);
            this.button21.Name = "button21";
            this.button21.Size = new System.Drawing.Size(32, 21);
            this.button21.TabIndex = 58;
            this.button21.Text = "2KHz";
            this.button21.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.button21.UseVisualStyleBackColor = true;
            this.button21.Click += new System.EventHandler(this.button21_Click);
            // 
            // trb40Hz
            // 
            this.trb40Hz.AutoSize = false;
            this.trb40Hz.Location = new System.Drawing.Point(36, 11);
            this.trb40Hz.Maximum = 300;
            this.trb40Hz.Minimum = 5;
            this.trb40Hz.Name = "trb40Hz";
            this.trb40Hz.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.trb40Hz.Size = new System.Drawing.Size(17, 113);
            this.trb40Hz.TabIndex = 0;
            this.trb40Hz.TickStyle = System.Windows.Forms.TickStyle.None;
            this.trb40Hz.Value = 100;
            this.trb40Hz.Scroll += new System.EventHandler(this.trackBar1_Scroll);
            this.trb40Hz.ValueChanged += new System.EventHandler(this.trackBar1_ValueChanged);
            // 
            // button1
            // 
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.button1.Location = new System.Drawing.Point(28, 130);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(32, 21);
            this.button1.TabIndex = 34;
            this.button1.Text = "40Hz";
            this.button1.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // trb64Hz
            // 
            this.trb64Hz.AutoSize = false;
            this.trb64Hz.Location = new System.Drawing.Point(67, 11);
            this.trb64Hz.Maximum = 300;
            this.trb64Hz.Minimum = 5;
            this.trb64Hz.Name = "trb64Hz";
            this.trb64Hz.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.trb64Hz.Size = new System.Drawing.Size(17, 113);
            this.trb64Hz.TabIndex = 1;
            this.trb64Hz.TickStyle = System.Windows.Forms.TickStyle.None;
            this.trb64Hz.Value = 100;
            this.trb64Hz.ValueChanged += new System.EventHandler(this.trackBar2_ValueChanged);
            // 
            // button2
            // 
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.button2.Location = new System.Drawing.Point(60, 130);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(32, 21);
            this.button2.TabIndex = 36;
            this.button2.Text = "64Hz";
            this.button2.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // trb78Hz
            // 
            this.trb78Hz.AutoSize = false;
            this.trb78Hz.Location = new System.Drawing.Point(99, 11);
            this.trb78Hz.Maximum = 300;
            this.trb78Hz.Minimum = 5;
            this.trb78Hz.Name = "trb78Hz";
            this.trb78Hz.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.trb78Hz.Size = new System.Drawing.Size(17, 113);
            this.trb78Hz.TabIndex = 2;
            this.trb78Hz.Tag = "90Hz";
            this.trb78Hz.TickStyle = System.Windows.Forms.TickStyle.None;
            this.trb78Hz.Value = 100;
            this.trb78Hz.ValueChanged += new System.EventHandler(this.trackBar3_ValueChanged);
            // 
            // button3
            // 
            this.button3.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.button3.Location = new System.Drawing.Point(91, 130);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(32, 21);
            this.button3.TabIndex = 38;
            this.button3.Text = "78Hz";
            this.button3.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button11
            // 
            this.button11.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.button11.Location = new System.Drawing.Point(375, 130);
            this.button11.Name = "button11";
            this.button11.Size = new System.Drawing.Size(37, 21);
            this.button11.TabIndex = 54;
            this.button11.Text = "550Hz";
            this.button11.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.button11.UseVisualStyleBackColor = true;
            this.button11.Click += new System.EventHandler(this.button11_Click);
            // 
            // trb90Hz
            // 
            this.trb90Hz.AutoSize = false;
            this.trb90Hz.Location = new System.Drawing.Point(132, 11);
            this.trb90Hz.Maximum = 300;
            this.trb90Hz.Minimum = 5;
            this.trb90Hz.Name = "trb90Hz";
            this.trb90Hz.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.trb90Hz.Size = new System.Drawing.Size(17, 113);
            this.trb90Hz.TabIndex = 3;
            this.trb90Hz.Tag = "110Hz";
            this.trb90Hz.TickStyle = System.Windows.Forms.TickStyle.None;
            this.trb90Hz.Value = 100;
            this.trb90Hz.ValueChanged += new System.EventHandler(this.trackBar4_ValueChanged);
            // 
            // trb650Hz
            // 
            this.trb650Hz.AutoSize = false;
            this.trb650Hz.Location = new System.Drawing.Point(455, 11);
            this.trb650Hz.Maximum = 300;
            this.trb650Hz.Minimum = 5;
            this.trb650Hz.Name = "trb650Hz";
            this.trb650Hz.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.trb650Hz.Size = new System.Drawing.Size(17, 113);
            this.trb650Hz.TabIndex = 53;
            this.trb650Hz.TickStyle = System.Windows.Forms.TickStyle.None;
            this.trb650Hz.Value = 100;
            this.trb650Hz.ValueChanged += new System.EventHandler(this.trackBar13_ValueChanged);
            // 
            // button4
            // 
            this.button4.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.button4.Location = new System.Drawing.Point(124, 130);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(32, 21);
            this.button4.TabIndex = 40;
            this.button4.Text = "90Hz";
            this.button4.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // button8
            // 
            this.button8.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.button8.Location = new System.Drawing.Point(266, 130);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(37, 21);
            this.button8.TabIndex = 52;
            this.button8.Text = "300Hz";
            this.button8.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.button8.UseVisualStyleBackColor = true;
            this.button8.Click += new System.EventHandler(this.button8_Click);
            // 
            // trb110Hz
            // 
            this.trb110Hz.AutoSize = false;
            this.trb110Hz.Location = new System.Drawing.Point(168, 11);
            this.trb110Hz.Maximum = 300;
            this.trb110Hz.Minimum = 5;
            this.trb110Hz.Name = "trb110Hz";
            this.trb110Hz.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.trb110Hz.Size = new System.Drawing.Size(17, 113);
            this.trb110Hz.TabIndex = 4;
            this.trb110Hz.Tag = "125Hz";
            this.trb110Hz.TickStyle = System.Windows.Forms.TickStyle.None;
            this.trb110Hz.Value = 100;
            this.trb110Hz.ValueChanged += new System.EventHandler(this.trackBar5_ValueChanged);
            // 
            // trb300Hz
            // 
            this.trb300Hz.AutoSize = false;
            this.trb300Hz.Location = new System.Drawing.Point(276, 11);
            this.trb300Hz.Maximum = 300;
            this.trb300Hz.Minimum = 5;
            this.trb300Hz.Name = "trb300Hz";
            this.trb300Hz.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.trb300Hz.Size = new System.Drawing.Size(17, 113);
            this.trb300Hz.TabIndex = 7;
            this.trb300Hz.Tag = "400Hz";
            this.trb300Hz.TickStyle = System.Windows.Forms.TickStyle.None;
            this.trb300Hz.Value = 100;
            this.trb300Hz.ValueChanged += new System.EventHandler(this.trackBar8_ValueChanged);
            // 
            // button5
            // 
            this.button5.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.button5.Location = new System.Drawing.Point(157, 130);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(37, 21);
            this.button5.TabIndex = 42;
            this.button5.Text = "110Hz";
            this.button5.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // button9
            // 
            this.button9.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.button9.Location = new System.Drawing.Point(303, 130);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(37, 21);
            this.button9.TabIndex = 50;
            this.button9.Text = "400Hz";
            this.button9.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.button9.UseVisualStyleBackColor = true;
            this.button9.Click += new System.EventHandler(this.button9_Click);
            // 
            // trb125Hz
            // 
            this.trb125Hz.AutoSize = false;
            this.trb125Hz.Location = new System.Drawing.Point(203, 11);
            this.trb125Hz.Maximum = 300;
            this.trb125Hz.Minimum = 5;
            this.trb125Hz.Name = "trb125Hz";
            this.trb125Hz.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.trb125Hz.Size = new System.Drawing.Size(17, 113);
            this.trb125Hz.TabIndex = 5;
            this.trb125Hz.Tag = "200Hz";
            this.trb125Hz.TickStyle = System.Windows.Forms.TickStyle.None;
            this.trb125Hz.Value = 100;
            this.trb125Hz.ValueChanged += new System.EventHandler(this.trackBar6_ValueChanged);
            // 
            // trb400Hz
            // 
            this.trb400Hz.AutoSize = false;
            this.trb400Hz.Location = new System.Drawing.Point(313, 11);
            this.trb400Hz.Maximum = 300;
            this.trb400Hz.Minimum = 5;
            this.trb400Hz.Name = "trb400Hz";
            this.trb400Hz.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.trb400Hz.Size = new System.Drawing.Size(17, 113);
            this.trb400Hz.TabIndex = 8;
            this.trb400Hz.Tag = "450Hz";
            this.trb400Hz.TickStyle = System.Windows.Forms.TickStyle.None;
            this.trb400Hz.Value = 100;
            this.trb400Hz.ValueChanged += new System.EventHandler(this.trackBar9_ValueChanged);
            // 
            // button6
            // 
            this.button6.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.button6.Location = new System.Drawing.Point(194, 130);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(37, 21);
            this.button6.TabIndex = 44;
            this.button6.Text = "125Hz";
            this.button6.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // button10
            // 
            this.button10.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.button10.Location = new System.Drawing.Point(339, 130);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(37, 21);
            this.button10.TabIndex = 48;
            this.button10.Text = "500Hz";
            this.button10.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.button10.UseVisualStyleBackColor = true;
            this.button10.Click += new System.EventHandler(this.button10_Click);
            // 
            // trb200Hz
            // 
            this.trb200Hz.AutoSize = false;
            this.trb200Hz.Location = new System.Drawing.Point(240, 11);
            this.trb200Hz.Maximum = 300;
            this.trb200Hz.Minimum = 5;
            this.trb200Hz.Name = "trb200Hz";
            this.trb200Hz.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.trb200Hz.Size = new System.Drawing.Size(17, 113);
            this.trb200Hz.TabIndex = 6;
            this.trb200Hz.Tag = "300Hz";
            this.trb200Hz.TickStyle = System.Windows.Forms.TickStyle.None;
            this.trb200Hz.Value = 100;
            this.trb200Hz.ValueChanged += new System.EventHandler(this.trackBar7_ValueChanged);
            // 
            // trb500Hz
            // 
            this.trb500Hz.AutoSize = false;
            this.trb500Hz.Location = new System.Drawing.Point(348, 11);
            this.trb500Hz.Maximum = 300;
            this.trb500Hz.Minimum = 5;
            this.trb500Hz.Name = "trb500Hz";
            this.trb500Hz.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.trb500Hz.Size = new System.Drawing.Size(17, 113);
            this.trb500Hz.TabIndex = 9;
            this.trb500Hz.TickStyle = System.Windows.Forms.TickStyle.None;
            this.trb500Hz.Value = 100;
            this.trb500Hz.ValueChanged += new System.EventHandler(this.trackBar10_ValueChanged);
            // 
            // button7
            // 
            this.button7.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.button7.Location = new System.Drawing.Point(230, 130);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(37, 21);
            this.button7.TabIndex = 46;
            this.button7.Text = "200Hz";
            this.button7.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.button7.UseVisualStyleBackColor = true;
            this.button7.Click += new System.EventHandler(this.button7_Click);
            // 
            // button22
            // 
            this.button22.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.button22.Location = new System.Drawing.Point(88, 276);
            this.button22.Name = "button22";
            this.button22.Size = new System.Drawing.Size(32, 21);
            this.button22.TabIndex = 60;
            this.button22.Text = "3KHz";
            this.button22.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.button22.UseVisualStyleBackColor = true;
            this.button22.Click += new System.EventHandler(this.button22_Click);
            // 
            // button23
            // 
            this.button23.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.button23.Location = new System.Drawing.Point(119, 276);
            this.button23.Name = "button23";
            this.button23.Size = new System.Drawing.Size(32, 21);
            this.button23.TabIndex = 62;
            this.button23.Text = "4KHz";
            this.button23.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.button23.UseVisualStyleBackColor = true;
            this.button23.Click += new System.EventHandler(this.button23_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(4, 163);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(25, 13);
            this.label1.TabIndex = 63;
            this.label1.Text = "+10";
            // 
            // trb800Hz
            // 
            this.trb800Hz.AutoSize = false;
            this.trb800Hz.Location = new System.Drawing.Point(564, 11);
            this.trb800Hz.Maximum = 300;
            this.trb800Hz.Minimum = 5;
            this.trb800Hz.Name = "trb800Hz";
            this.trb800Hz.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.trb800Hz.Size = new System.Drawing.Size(17, 113);
            this.trb800Hz.TabIndex = 68;
            this.trb800Hz.TickStyle = System.Windows.Forms.TickStyle.None;
            this.trb800Hz.Value = 100;
            this.trb800Hz.ValueChanged += new System.EventHandler(this.trackBar16_ValueChanged);
            // 
            // button26
            // 
            this.button26.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.button26.Location = new System.Drawing.Point(212, 276);
            this.button26.Name = "button26";
            this.button26.Size = new System.Drawing.Size(32, 21);
            this.button26.TabIndex = 69;
            this.button26.Text = "7KHz";
            this.button26.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.button26.UseVisualStyleBackColor = true;
            this.button26.Click += new System.EventHandler(this.button26_Click);
            // 
            // button27
            // 
            this.button27.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.button27.Location = new System.Drawing.Point(243, 276);
            this.button27.Name = "button27";
            this.button27.Size = new System.Drawing.Size(32, 21);
            this.button27.TabIndex = 71;
            this.button27.Text = "8KHz";
            this.button27.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.button27.UseVisualStyleBackColor = true;
            this.button27.Click += new System.EventHandler(this.button27_Click);
            // 
            // trb950Hz
            // 
            this.trb950Hz.AutoSize = false;
            this.trb950Hz.Location = new System.Drawing.Point(671, 11);
            this.trb950Hz.Maximum = 300;
            this.trb950Hz.Minimum = 5;
            this.trb950Hz.Name = "trb950Hz";
            this.trb950Hz.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.trb950Hz.Size = new System.Drawing.Size(17, 113);
            this.trb950Hz.TabIndex = 66;
            this.trb950Hz.TickStyle = System.Windows.Forms.TickStyle.None;
            this.trb950Hz.Value = 100;
            this.trb950Hz.ValueChanged += new System.EventHandler(this.trackBar19_ValueChanged);
            // 
            // button30
            // 
            this.button30.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.button30.Location = new System.Drawing.Point(342, 276);
            this.button30.Name = "button30";
            this.button30.Size = new System.Drawing.Size(38, 21);
            this.button30.TabIndex = 65;
            this.button30.Text = "11KHz";
            this.button30.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.button30.UseVisualStyleBackColor = true;
            this.button30.Click += new System.EventHandler(this.button30_Click);
            // 
            // button25
            // 
            this.button25.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.button25.Location = new System.Drawing.Point(181, 276);
            this.button25.Name = "button25";
            this.button25.Size = new System.Drawing.Size(32, 21);
            this.button25.TabIndex = 67;
            this.button25.Text = "6KHz";
            this.button25.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.button25.UseVisualStyleBackColor = true;
            this.button25.Click += new System.EventHandler(this.button25_Click);
            // 
            // button28
            // 
            this.button28.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.button28.Location = new System.Drawing.Point(274, 276);
            this.button28.Name = "button28";
            this.button28.Size = new System.Drawing.Size(32, 21);
            this.button28.TabIndex = 73;
            this.button28.Text = "9KHz";
            this.button28.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.button28.UseVisualStyleBackColor = true;
            this.button28.Click += new System.EventHandler(this.button28_Click);
            // 
            // trb9KHz
            // 
            this.trb9KHz.AutoSize = false;
            this.trb9KHz.Location = new System.Drawing.Point(282, 157);
            this.trb9KHz.Maximum = 300;
            this.trb9KHz.Minimum = 5;
            this.trb9KHz.Name = "trb9KHz";
            this.trb9KHz.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.trb9KHz.Size = new System.Drawing.Size(17, 113);
            this.trb9KHz.TabIndex = 74;
            this.trb9KHz.TickStyle = System.Windows.Forms.TickStyle.None;
            this.trb9KHz.Value = 100;
            this.trb9KHz.ValueChanged += new System.EventHandler(this.trackBar28_ValueChanged);
            // 
            // button29
            // 
            this.button29.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.button29.Location = new System.Drawing.Point(305, 276);
            this.button29.Name = "button29";
            this.button29.Size = new System.Drawing.Size(38, 21);
            this.button29.TabIndex = 75;
            this.button29.Text = "10KHz";
            this.button29.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.button29.UseVisualStyleBackColor = true;
            this.button29.Click += new System.EventHandler(this.button29_Click);
            // 
            // button24
            // 
            this.button24.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.button24.Location = new System.Drawing.Point(150, 276);
            this.button24.Name = "button24";
            this.button24.Size = new System.Drawing.Size(32, 21);
            this.button24.TabIndex = 77;
            this.button24.Text = "5KHz";
            this.button24.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.button24.UseVisualStyleBackColor = true;
            this.button24.Click += new System.EventHandler(this.button24_Click);
            // 
            // button32
            // 
            this.button32.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.button32.Location = new System.Drawing.Point(416, 276);
            this.button32.Name = "button32";
            this.button32.Size = new System.Drawing.Size(38, 21);
            this.button32.TabIndex = 81;
            this.button32.Text = "13KHz";
            this.button32.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.button32.UseVisualStyleBackColor = true;
            this.button32.Click += new System.EventHandler(this.button32_Click);
            // 
            // button31
            // 
            this.button31.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.button31.Location = new System.Drawing.Point(379, 276);
            this.button31.Name = "button31";
            this.button31.Size = new System.Drawing.Size(38, 21);
            this.button31.TabIndex = 79;
            this.button31.Text = "12KHz";
            this.button31.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.button31.UseVisualStyleBackColor = true;
            this.button31.Click += new System.EventHandler(this.button31_Click);
            // 
            // button36
            // 
            this.button36.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.button36.Location = new System.Drawing.Point(564, 276);
            this.button36.Name = "button36";
            this.button36.Size = new System.Drawing.Size(38, 21);
            this.button36.TabIndex = 89;
            this.button36.Text = "17KHz";
            this.button36.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.button36.UseVisualStyleBackColor = true;
            this.button36.Click += new System.EventHandler(this.button36_Click);
            // 
            // button35
            // 
            this.button35.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.button35.Location = new System.Drawing.Point(527, 276);
            this.button35.Name = "button35";
            this.button35.Size = new System.Drawing.Size(38, 21);
            this.button35.TabIndex = 87;
            this.button35.Text = "16KHz";
            this.button35.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.button35.UseVisualStyleBackColor = true;
            this.button35.Click += new System.EventHandler(this.button35_Click);
            // 
            // trb7KHz
            // 
            this.trb7KHz.AutoSize = false;
            this.trb7KHz.Location = new System.Drawing.Point(219, 157);
            this.trb7KHz.Maximum = 300;
            this.trb7KHz.Minimum = 5;
            this.trb7KHz.Name = "trb7KHz";
            this.trb7KHz.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.trb7KHz.Size = new System.Drawing.Size(17, 113);
            this.trb7KHz.TabIndex = 84;
            this.trb7KHz.TickStyle = System.Windows.Forms.TickStyle.None;
            this.trb7KHz.Value = 100;
            this.trb7KHz.ValueChanged += new System.EventHandler(this.trackBar26_ValueChanged);
            // 
            // button34
            // 
            this.button34.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.button34.Location = new System.Drawing.Point(490, 276);
            this.button34.Name = "button34";
            this.button34.Size = new System.Drawing.Size(38, 21);
            this.button34.TabIndex = 85;
            this.button34.Text = "15KHz";
            this.button34.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.button34.UseVisualStyleBackColor = true;
            this.button34.Click += new System.EventHandler(this.button34_Click);
            // 
            // trb8KHz
            // 
            this.trb8KHz.AutoSize = false;
            this.trb8KHz.Location = new System.Drawing.Point(251, 157);
            this.trb8KHz.Maximum = 300;
            this.trb8KHz.Minimum = 5;
            this.trb8KHz.Name = "trb8KHz";
            this.trb8KHz.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.trb8KHz.Size = new System.Drawing.Size(17, 113);
            this.trb8KHz.TabIndex = 82;
            this.trb8KHz.TickStyle = System.Windows.Forms.TickStyle.None;
            this.trb8KHz.Value = 100;
            this.trb8KHz.ValueChanged += new System.EventHandler(this.trackBar27_ValueChanged);
            // 
            // button33
            // 
            this.button33.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.button33.Location = new System.Drawing.Point(453, 276);
            this.button33.Name = "button33";
            this.button33.Size = new System.Drawing.Size(38, 21);
            this.button33.TabIndex = 83;
            this.button33.Text = "14KHz";
            this.button33.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.button33.UseVisualStyleBackColor = true;
            this.button33.Click += new System.EventHandler(this.button33_Click);
            // 
            // button19
            // 
            this.button19.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.button19.Location = new System.Drawing.Point(663, 130);
            this.button19.Name = "button19";
            this.button19.Size = new System.Drawing.Size(37, 21);
            this.button19.TabIndex = 105;
            this.button19.Text = "950Hz";
            this.button19.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.button19.UseVisualStyleBackColor = true;
            this.button19.Click += new System.EventHandler(this.button19_Click);
            // 
            // trb10KHz
            // 
            this.trb10KHz.AutoSize = false;
            this.trb10KHz.Location = new System.Drawing.Point(314, 157);
            this.trb10KHz.Maximum = 300;
            this.trb10KHz.Minimum = 5;
            this.trb10KHz.Name = "trb10KHz";
            this.trb10KHz.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.trb10KHz.Size = new System.Drawing.Size(17, 113);
            this.trb10KHz.TabIndex = 90;
            this.trb10KHz.TickStyle = System.Windows.Forms.TickStyle.None;
            this.trb10KHz.Value = 100;
            this.trb10KHz.ValueChanged += new System.EventHandler(this.trackBar29_ValueChanged);
            // 
            // trb11KHz
            // 
            this.trb11KHz.AutoSize = false;
            this.trb11KHz.Location = new System.Drawing.Point(353, 157);
            this.trb11KHz.Maximum = 300;
            this.trb11KHz.Minimum = 5;
            this.trb11KHz.Name = "trb11KHz";
            this.trb11KHz.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.trb11KHz.Size = new System.Drawing.Size(17, 113);
            this.trb11KHz.TabIndex = 104;
            this.trb11KHz.TickStyle = System.Windows.Forms.TickStyle.None;
            this.trb11KHz.Value = 100;
            this.trb11KHz.ValueChanged += new System.EventHandler(this.trackBar30_ValueChanged);
            // 
            // button12
            // 
            this.button12.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.button12.Location = new System.Drawing.Point(411, 130);
            this.button12.Name = "button12";
            this.button12.Size = new System.Drawing.Size(37, 21);
            this.button12.TabIndex = 91;
            this.button12.Text = "600Hz";
            this.button12.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.button12.UseVisualStyleBackColor = true;
            this.button12.Click += new System.EventHandler(this.button12_Click);
            // 
            // button18
            // 
            this.button18.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.button18.Location = new System.Drawing.Point(627, 130);
            this.button18.Name = "button18";
            this.button18.Size = new System.Drawing.Size(37, 21);
            this.button18.TabIndex = 103;
            this.button18.Text = "900Hz";
            this.button18.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.button18.UseVisualStyleBackColor = true;
            this.button18.Click += new System.EventHandler(this.button18_Click);
            // 
            // trb12KHz
            // 
            this.trb12KHz.AutoSize = false;
            this.trb12KHz.Location = new System.Drawing.Point(390, 157);
            this.trb12KHz.Maximum = 300;
            this.trb12KHz.Minimum = 5;
            this.trb12KHz.Name = "trb12KHz";
            this.trb12KHz.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.trb12KHz.Size = new System.Drawing.Size(17, 113);
            this.trb12KHz.TabIndex = 92;
            this.trb12KHz.TickStyle = System.Windows.Forms.TickStyle.None;
            this.trb12KHz.Value = 100;
            this.trb12KHz.ValueChanged += new System.EventHandler(this.trackBar31_ValueChanged);
            // 
            // trb13KHz
            // 
            this.trb13KHz.AutoSize = false;
            this.trb13KHz.Location = new System.Drawing.Point(426, 156);
            this.trb13KHz.Maximum = 300;
            this.trb13KHz.Minimum = 5;
            this.trb13KHz.Name = "trb13KHz";
            this.trb13KHz.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.trb13KHz.Size = new System.Drawing.Size(17, 114);
            this.trb13KHz.TabIndex = 102;
            this.trb13KHz.TickStyle = System.Windows.Forms.TickStyle.None;
            this.trb13KHz.Value = 100;
            this.trb13KHz.ValueChanged += new System.EventHandler(this.trackBar32_ValueChanged);
            // 
            // button13
            // 
            this.button13.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.button13.Location = new System.Drawing.Point(447, 130);
            this.button13.Name = "button13";
            this.button13.Size = new System.Drawing.Size(37, 21);
            this.button13.TabIndex = 93;
            this.button13.Text = "650Hz";
            this.button13.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.button13.UseVisualStyleBackColor = true;
            this.button13.Click += new System.EventHandler(this.button13_Click);
            // 
            // button17
            // 
            this.button17.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.button17.Location = new System.Drawing.Point(591, 130);
            this.button17.Name = "button17";
            this.button17.Size = new System.Drawing.Size(37, 21);
            this.button17.TabIndex = 101;
            this.button17.Text = "850Hz";
            this.button17.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.button17.UseVisualStyleBackColor = true;
            this.button17.Click += new System.EventHandler(this.button17_Click);
            // 
            // trb14KHz
            // 
            this.trb14KHz.AutoSize = false;
            this.trb14KHz.Location = new System.Drawing.Point(463, 157);
            this.trb14KHz.Maximum = 300;
            this.trb14KHz.Minimum = 5;
            this.trb14KHz.Name = "trb14KHz";
            this.trb14KHz.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.trb14KHz.Size = new System.Drawing.Size(17, 113);
            this.trb14KHz.TabIndex = 94;
            this.trb14KHz.TickStyle = System.Windows.Forms.TickStyle.None;
            this.trb14KHz.Value = 100;
            this.trb14KHz.ValueChanged += new System.EventHandler(this.trackBar33_ValueChanged);
            // 
            // trb15KHz
            // 
            this.trb15KHz.AutoSize = false;
            this.trb15KHz.Location = new System.Drawing.Point(501, 157);
            this.trb15KHz.Maximum = 300;
            this.trb15KHz.Minimum = 5;
            this.trb15KHz.Name = "trb15KHz";
            this.trb15KHz.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.trb15KHz.Size = new System.Drawing.Size(17, 113);
            this.trb15KHz.TabIndex = 100;
            this.trb15KHz.TickStyle = System.Windows.Forms.TickStyle.None;
            this.trb15KHz.Value = 100;
            this.trb15KHz.ValueChanged += new System.EventHandler(this.trackBar34_ValueChanged);
            // 
            // button14
            // 
            this.button14.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.button14.Location = new System.Drawing.Point(483, 130);
            this.button14.Name = "button14";
            this.button14.Size = new System.Drawing.Size(37, 21);
            this.button14.TabIndex = 95;
            this.button14.Text = "700Hz";
            this.button14.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.button14.UseVisualStyleBackColor = true;
            this.button14.Click += new System.EventHandler(this.button14_Click);
            // 
            // button16
            // 
            this.button16.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.button16.Location = new System.Drawing.Point(555, 130);
            this.button16.Name = "button16";
            this.button16.Size = new System.Drawing.Size(37, 21);
            this.button16.TabIndex = 99;
            this.button16.Text = "800Hz";
            this.button16.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.button16.UseVisualStyleBackColor = true;
            this.button16.Click += new System.EventHandler(this.button16_Click);
            // 
            // trb16KHz
            // 
            this.trb16KHz.AutoSize = false;
            this.trb16KHz.Location = new System.Drawing.Point(536, 157);
            this.trb16KHz.Maximum = 300;
            this.trb16KHz.Minimum = 5;
            this.trb16KHz.Name = "trb16KHz";
            this.trb16KHz.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.trb16KHz.Size = new System.Drawing.Size(17, 113);
            this.trb16KHz.TabIndex = 96;
            this.trb16KHz.TickStyle = System.Windows.Forms.TickStyle.None;
            this.trb16KHz.Value = 100;
            this.trb16KHz.ValueChanged += new System.EventHandler(this.trackBar35_ValueChanged);
            // 
            // trb17KHz
            // 
            this.trb17KHz.AutoSize = false;
            this.trb17KHz.Location = new System.Drawing.Point(575, 157);
            this.trb17KHz.Maximum = 300;
            this.trb17KHz.Minimum = 5;
            this.trb17KHz.Name = "trb17KHz";
            this.trb17KHz.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.trb17KHz.Size = new System.Drawing.Size(17, 113);
            this.trb17KHz.TabIndex = 98;
            this.trb17KHz.TickStyle = System.Windows.Forms.TickStyle.None;
            this.trb17KHz.Value = 100;
            this.trb17KHz.ValueChanged += new System.EventHandler(this.trackBar36_ValueChanged);
            // 
            // button15
            // 
            this.button15.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.button15.Location = new System.Drawing.Point(519, 130);
            this.button15.Name = "button15";
            this.button15.Size = new System.Drawing.Size(37, 21);
            this.button15.TabIndex = 97;
            this.button15.Text = "750Hz";
            this.button15.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.button15.UseVisualStyleBackColor = true;
            this.button15.Click += new System.EventHandler(this.button15_Click);
            // 
            // trb18KHz
            // 
            this.trb18KHz.AutoSize = false;
            this.trb18KHz.Location = new System.Drawing.Point(611, 157);
            this.trb18KHz.Maximum = 300;
            this.trb18KHz.Minimum = 5;
            this.trb18KHz.Name = "trb18KHz";
            this.trb18KHz.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.trb18KHz.Size = new System.Drawing.Size(17, 113);
            this.trb18KHz.TabIndex = 110;
            this.trb18KHz.TickStyle = System.Windows.Forms.TickStyle.None;
            this.trb18KHz.Value = 100;
            this.trb18KHz.ValueChanged += new System.EventHandler(this.trackBar37_ValueChanged);
            // 
            // button39
            // 
            this.button39.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.button39.Location = new System.Drawing.Point(675, 276);
            this.button39.Name = "button39";
            this.button39.Size = new System.Drawing.Size(38, 21);
            this.button39.TabIndex = 111;
            this.button39.Text = "20KHz";
            this.button39.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.button39.UseVisualStyleBackColor = true;
            this.button39.Click += new System.EventHandler(this.button39_Click);
            // 
            // trb19KHz
            // 
            this.trb19KHz.AutoSize = false;
            this.trb19KHz.Location = new System.Drawing.Point(647, 157);
            this.trb19KHz.Maximum = 300;
            this.trb19KHz.Minimum = 5;
            this.trb19KHz.Name = "trb19KHz";
            this.trb19KHz.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.trb19KHz.Size = new System.Drawing.Size(17, 112);
            this.trb19KHz.TabIndex = 108;
            this.trb19KHz.TickStyle = System.Windows.Forms.TickStyle.None;
            this.trb19KHz.Value = 100;
            this.trb19KHz.ValueChanged += new System.EventHandler(this.trackBar38_ValueChanged);
            // 
            // button38
            // 
            this.button38.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.button38.Location = new System.Drawing.Point(638, 276);
            this.button38.Name = "button38";
            this.button38.Size = new System.Drawing.Size(38, 21);
            this.button38.TabIndex = 109;
            this.button38.Text = "19KHz";
            this.button38.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.button38.UseVisualStyleBackColor = true;
            this.button38.Click += new System.EventHandler(this.button38_Click);
            // 
            // trb20KHz
            // 
            this.trb20KHz.AutoSize = false;
            this.trb20KHz.Location = new System.Drawing.Point(683, 156);
            this.trb20KHz.Maximum = 300;
            this.trb20KHz.Minimum = 5;
            this.trb20KHz.Name = "trb20KHz";
            this.trb20KHz.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.trb20KHz.Size = new System.Drawing.Size(17, 113);
            this.trb20KHz.TabIndex = 106;
            this.trb20KHz.TickStyle = System.Windows.Forms.TickStyle.None;
            this.trb20KHz.Value = 100;
            this.trb20KHz.ValueChanged += new System.EventHandler(this.trackBar39_ValueChanged);
            // 
            // button37
            // 
            this.button37.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.button37.Location = new System.Drawing.Point(601, 276);
            this.button37.Name = "button37";
            this.button37.Size = new System.Drawing.Size(38, 21);
            this.button37.TabIndex = 107;
            this.button37.Text = "18KHz";
            this.button37.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.button37.UseVisualStyleBackColor = true;
            this.button37.Click += new System.EventHandler(this.button37_Click);
            // 
            // trb6KHz
            // 
            this.trb6KHz.AutoSize = false;
            this.trb6KHz.Location = new System.Drawing.Point(188, 157);
            this.trb6KHz.Maximum = 300;
            this.trb6KHz.Minimum = 5;
            this.trb6KHz.Name = "trb6KHz";
            this.trb6KHz.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.trb6KHz.Size = new System.Drawing.Size(17, 113);
            this.trb6KHz.TabIndex = 112;
            this.trb6KHz.TickStyle = System.Windows.Forms.TickStyle.None;
            this.trb6KHz.Value = 100;
            this.trb6KHz.ValueChanged += new System.EventHandler(this.trackBar25_ValueChanged);
            // 
            // trb5KHz
            // 
            this.trb5KHz.AutoSize = false;
            this.trb5KHz.Location = new System.Drawing.Point(156, 157);
            this.trb5KHz.Maximum = 300;
            this.trb5KHz.Minimum = 5;
            this.trb5KHz.Name = "trb5KHz";
            this.trb5KHz.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.trb5KHz.Size = new System.Drawing.Size(17, 113);
            this.trb5KHz.TabIndex = 113;
            this.trb5KHz.TickStyle = System.Windows.Forms.TickStyle.None;
            this.trb5KHz.Value = 100;
            this.trb5KHz.ValueChanged += new System.EventHandler(this.trackBar24_ValueChanged);
            // 
            // trb900Hz
            // 
            this.trb900Hz.AutoSize = false;
            this.trb900Hz.Location = new System.Drawing.Point(636, 11);
            this.trb900Hz.Maximum = 300;
            this.trb900Hz.Minimum = 5;
            this.trb900Hz.Name = "trb900Hz";
            this.trb900Hz.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.trb900Hz.Size = new System.Drawing.Size(17, 113);
            this.trb900Hz.TabIndex = 114;
            this.trb900Hz.TickStyle = System.Windows.Forms.TickStyle.None;
            this.trb900Hz.Value = 100;
            this.trb900Hz.ValueChanged += new System.EventHandler(this.trackBar18_ValueChanged);
            // 
            // trb4KHz
            // 
            this.trb4KHz.AutoSize = false;
            this.trb4KHz.Location = new System.Drawing.Point(125, 157);
            this.trb4KHz.Maximum = 300;
            this.trb4KHz.Minimum = 5;
            this.trb4KHz.Name = "trb4KHz";
            this.trb4KHz.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.trb4KHz.Size = new System.Drawing.Size(17, 113);
            this.trb4KHz.TabIndex = 115;
            this.trb4KHz.TickStyle = System.Windows.Forms.TickStyle.None;
            this.trb4KHz.Value = 100;
            this.trb4KHz.ValueChanged += new System.EventHandler(this.trackBar23_ValueChanged);
            // 
            // trb750Hz
            // 
            this.trb750Hz.AutoSize = false;
            this.trb750Hz.Location = new System.Drawing.Point(527, 11);
            this.trb750Hz.Maximum = 300;
            this.trb750Hz.Minimum = 5;
            this.trb750Hz.Name = "trb750Hz";
            this.trb750Hz.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.trb750Hz.Size = new System.Drawing.Size(17, 113);
            this.trb750Hz.TabIndex = 116;
            this.trb750Hz.TickStyle = System.Windows.Forms.TickStyle.None;
            this.trb750Hz.Value = 100;
            this.trb750Hz.ValueChanged += new System.EventHandler(this.trackBar15_ValueChanged);
            // 
            // trb3KHz
            // 
            this.trb3KHz.AutoSize = false;
            this.trb3KHz.Location = new System.Drawing.Point(94, 157);
            this.trb3KHz.Maximum = 300;
            this.trb3KHz.Minimum = 5;
            this.trb3KHz.Name = "trb3KHz";
            this.trb3KHz.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.trb3KHz.Size = new System.Drawing.Size(17, 113);
            this.trb3KHz.TabIndex = 117;
            this.trb3KHz.TickStyle = System.Windows.Forms.TickStyle.None;
            this.trb3KHz.Value = 100;
            this.trb3KHz.ValueChanged += new System.EventHandler(this.trackBar22_ValueChanged);
            // 
            // trb600Hz
            // 
            this.trb600Hz.AutoSize = false;
            this.trb600Hz.Location = new System.Drawing.Point(420, 11);
            this.trb600Hz.Maximum = 300;
            this.trb600Hz.Minimum = 5;
            this.trb600Hz.Name = "trb600Hz";
            this.trb600Hz.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.trb600Hz.Size = new System.Drawing.Size(17, 113);
            this.trb600Hz.TabIndex = 11;
            this.trb600Hz.TickStyle = System.Windows.Forms.TickStyle.None;
            this.trb600Hz.Value = 100;
            this.trb600Hz.ValueChanged += new System.EventHandler(this.trackBar12_ValueChanged);
            // 
            // trb2KHz
            // 
            this.trb2KHz.AutoSize = false;
            this.trb2KHz.Location = new System.Drawing.Point(64, 157);
            this.trb2KHz.Maximum = 300;
            this.trb2KHz.Minimum = 5;
            this.trb2KHz.Name = "trb2KHz";
            this.trb2KHz.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.trb2KHz.Size = new System.Drawing.Size(17, 113);
            this.trb2KHz.TabIndex = 120;
            this.trb2KHz.TickStyle = System.Windows.Forms.TickStyle.None;
            this.trb2KHz.Value = 100;
            this.trb2KHz.ValueChanged += new System.EventHandler(this.trackBar21_ValueChanged);
            // 
            // trb1KHz
            // 
            this.trb1KHz.AutoSize = false;
            this.trb1KHz.Location = new System.Drawing.Point(33, 157);
            this.trb1KHz.Maximum = 300;
            this.trb1KHz.Minimum = 5;
            this.trb1KHz.Name = "trb1KHz";
            this.trb1KHz.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.trb1KHz.Size = new System.Drawing.Size(17, 113);
            this.trb1KHz.TabIndex = 121;
            this.trb1KHz.TickStyle = System.Windows.Forms.TickStyle.None;
            this.trb1KHz.Value = 100;
            this.trb1KHz.ValueChanged += new System.EventHandler(this.trackBar20_ValueChanged);
            // 
            // trb850Hz
            // 
            this.trb850Hz.AutoSize = false;
            this.trb850Hz.Location = new System.Drawing.Point(600, 11);
            this.trb850Hz.Maximum = 300;
            this.trb850Hz.Minimum = 5;
            this.trb850Hz.Name = "trb850Hz";
            this.trb850Hz.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.trb850Hz.Size = new System.Drawing.Size(17, 113);
            this.trb850Hz.TabIndex = 122;
            this.trb850Hz.TickStyle = System.Windows.Forms.TickStyle.None;
            this.trb850Hz.Value = 100;
            this.trb850Hz.ValueChanged += new System.EventHandler(this.trackBar17_ValueChanged);
            // 
            // trb700Hz
            // 
            this.trb700Hz.AutoSize = false;
            this.trb700Hz.Location = new System.Drawing.Point(491, 11);
            this.trb700Hz.Maximum = 300;
            this.trb700Hz.Minimum = 5;
            this.trb700Hz.Name = "trb700Hz";
            this.trb700Hz.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.trb700Hz.Size = new System.Drawing.Size(17, 113);
            this.trb700Hz.TabIndex = 123;
            this.trb700Hz.TickStyle = System.Windows.Forms.TickStyle.None;
            this.trb700Hz.Value = 100;
            this.trb700Hz.ValueChanged += new System.EventHandler(this.trackBar14_ValueChanged);
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.rbChannel);
            this.panel1.Controls.Add(this.rbMainOuput);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.trbGain);
            this.panel1.Enabled = false;
            this.panel1.Location = new System.Drawing.Point(1, 70);
            this.panel1.MaximumSize = new System.Drawing.Size(73, 280);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(73, 280);
            this.panel1.TabIndex = 126;
            // 
            // rbChannel
            // 
            this.rbChannel.AutoSize = true;
            this.rbChannel.Location = new System.Drawing.Point(3, 4);
            this.rbChannel.Name = "rbChannel";
            this.rbChannel.Size = new System.Drawing.Size(64, 17);
            this.rbChannel.TabIndex = 132;
            this.rbChannel.Text = "Channel";
            this.rbChannel.UseVisualStyleBackColor = true;
            this.rbChannel.CheckedChanged += new System.EventHandler(this.radioButton2_CheckedChanged);
            // 
            // rbMainOuput
            // 
            this.rbMainOuput.AutoSize = true;
            this.rbMainOuput.Checked = true;
            this.rbMainOuput.Location = new System.Drawing.Point(3, 31);
            this.rbMainOuput.Name = "rbMainOuput";
            this.rbMainOuput.Size = new System.Drawing.Size(57, 30);
            this.rbMainOuput.TabIndex = 131;
            this.rbMainOuput.TabStop = true;
            this.rbMainOuput.Text = "Main\r\nOutput";
            this.rbMainOuput.UseVisualStyleBackColor = true;
            this.rbMainOuput.CheckedChanged += new System.EventHandler(this.radioButton1_CheckedChanged);
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(21, 259);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(25, 13);
            this.label4.TabIndex = 129;
            this.label4.Text = "200";
            // 
            // trbGain
            // 
            this.trbGain.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.trbGain.AutoSize = false;
            this.trbGain.Location = new System.Drawing.Point(14, 73);
            this.trbGain.Maximum = 500;
            this.trbGain.Name = "trbGain";
            this.trbGain.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.trbGain.Size = new System.Drawing.Size(39, 180);
            this.trbGain.TabIndex = 126;
            this.trbGain.TickFrequency = 25;
            this.trbGain.TickStyle = System.Windows.Forms.TickStyle.Both;
            this.trbGain.Value = 200;
            this.trbGain.ValueChanged += new System.EventHandler(this.trbGain_ValueChanged);
            // 
            // EQUse
            // 
            this.EQUse.AutoSize = true;
            this.EQUse.Location = new System.Drawing.Point(1, 28);
            this.EQUse.Name = "EQUse";
            this.EQUse.Size = new System.Drawing.Size(63, 17);
            this.EQUse.TabIndex = 127;
            this.EQUse.Text = "EQ사용";
            this.EQUse.UseVisualStyleBackColor = true;
            this.EQUse.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
            // 
            // panel2
            // 
            this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel2.AutoScroll = true;
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.label8);
            this.panel2.Controls.Add(this.label7);
            this.panel2.Controls.Add(this.label6);
            this.panel2.Controls.Add(this.label5);
            this.panel2.Controls.Add(this.trb1KHz);
            this.panel2.Controls.Add(this.trb2KHz);
            this.panel2.Controls.Add(this.trb600Hz);
            this.panel2.Controls.Add(this.trb700Hz);
            this.panel2.Controls.Add(this.trb3KHz);
            this.panel2.Controls.Add(this.trb850Hz);
            this.panel2.Controls.Add(this.trb4KHz);
            this.panel2.Controls.Add(this.trb750Hz);
            this.panel2.Controls.Add(this.trb5KHz);
            this.panel2.Controls.Add(this.trb900Hz);
            this.panel2.Controls.Add(this.trb6KHz);
            this.panel2.Controls.Add(this.trb18KHz);
            this.panel2.Controls.Add(this.trb10KHz);
            this.panel2.Controls.Add(this.button39);
            this.panel2.Controls.Add(this.trb11KHz);
            this.panel2.Controls.Add(this.trb19KHz);
            this.panel2.Controls.Add(this.button12);
            this.panel2.Controls.Add(this.button38);
            this.panel2.Controls.Add(this.trb12KHz);
            this.panel2.Controls.Add(this.trb20KHz);
            this.panel2.Controls.Add(this.trb13KHz);
            this.panel2.Controls.Add(this.button37);
            this.panel2.Controls.Add(this.trb7KHz);
            this.panel2.Controls.Add(this.button19);
            this.panel2.Controls.Add(this.trb8KHz);
            this.panel2.Controls.Add(this.button18);
            this.panel2.Controls.Add(this.button32);
            this.panel2.Controls.Add(this.button13);
            this.panel2.Controls.Add(this.button31);
            this.panel2.Controls.Add(this.button17);
            this.panel2.Controls.Add(this.button24);
            this.panel2.Controls.Add(this.trb14KHz);
            this.panel2.Controls.Add(this.trb9KHz);
            this.panel2.Controls.Add(this.trb15KHz);
            this.panel2.Controls.Add(this.button29);
            this.panel2.Controls.Add(this.button14);
            this.panel2.Controls.Add(this.button28);
            this.panel2.Controls.Add(this.button16);
            this.panel2.Controls.Add(this.button26);
            this.panel2.Controls.Add(this.trb16KHz);
            this.panel2.Controls.Add(this.button27);
            this.panel2.Controls.Add(this.trb17KHz);
            this.panel2.Controls.Add(this.button30);
            this.panel2.Controls.Add(this.button15);
            this.panel2.Controls.Add(this.button25);
            this.panel2.Controls.Add(this.button36);
            this.panel2.Controls.Add(this.button22);
            this.panel2.Controls.Add(this.button35);
            this.panel2.Controls.Add(this.button23);
            this.panel2.Controls.Add(this.button34);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.button33);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.trb800Hz);
            this.panel2.Controls.Add(this.trb550Hz);
            this.panel2.Controls.Add(this.trb950Hz);
            this.panel2.Controls.Add(this.button20);
            this.panel2.Controls.Add(this.trb650Hz);
            this.panel2.Controls.Add(this.button21);
            this.panel2.Controls.Add(this.trb40Hz);
            this.panel2.Controls.Add(this.button1);
            this.panel2.Controls.Add(this.trb64Hz);
            this.panel2.Controls.Add(this.button2);
            this.panel2.Controls.Add(this.trb78Hz);
            this.panel2.Controls.Add(this.button3);
            this.panel2.Controls.Add(this.button11);
            this.panel2.Controls.Add(this.trb90Hz);
            this.panel2.Controls.Add(this.button4);
            this.panel2.Controls.Add(this.button8);
            this.panel2.Controls.Add(this.trb110Hz);
            this.panel2.Controls.Add(this.trb300Hz);
            this.panel2.Controls.Add(this.button5);
            this.panel2.Controls.Add(this.button9);
            this.panel2.Controls.Add(this.trb125Hz);
            this.panel2.Controls.Add(this.trb400Hz);
            this.panel2.Controls.Add(this.button6);
            this.panel2.Controls.Add(this.button10);
            this.panel2.Controls.Add(this.trb200Hz);
            this.panel2.Controls.Add(this.trb500Hz);
            this.panel2.Controls.Add(this.button7);
            this.panel2.Enabled = false;
            this.panel2.Location = new System.Drawing.Point(77, 28);
            this.panel2.MaximumSize = new System.Drawing.Size(750, 323);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(726, 323);
            this.panel2.TabIndex = 0;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(13, 219);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(13, 13);
            this.label8.TabIndex = 127;
            this.label8.Text = "0";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(14, 73);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(13, 13);
            this.label7.TabIndex = 126;
            this.label7.Text = "0";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(9, 110);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(16, 13);
            this.label6.TabIndex = 125;
            this.label6.Text = "-5";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(9, 258);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(16, 13);
            this.label5.TabIndex = 124;
            this.label5.Text = "-5";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(20, 50);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(29, 13);
            this.label3.TabIndex = 129;
            this.label3.Text = "Gain";
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.파일FToolStripMenuItem,
            this.설정TToolStripMenuItem,
            this.정렬ToolStripMenuItem,
            this.toolStripComboBox1,
            this.폴더선택ToolStripMenuItem,
            this.새로고침ToolStripMenuItem,
            this.제거ToolStripMenuItem});
            this.menuStrip1.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.HorizontalStackWithOverflow;
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Padding = new System.Windows.Forms.Padding(5, 2, 0, 2);
            this.menuStrip1.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional;
            this.menuStrip1.Size = new System.Drawing.Size(805, 27);
            this.menuStrip1.TabIndex = 130;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // 파일FToolStripMenuItem
            // 
            this.파일FToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.새프리셋ToolStripMenuItem,
            this.toolStripSeparator2,
            this.프리셋열기ToolStripMenuItem,
            this.프리셋저장ToolStripMenuItem,
            this.toolStripSeparator1,
            this.끝내기XToolStripMenuItem});
            this.파일FToolStripMenuItem.Name = "파일FToolStripMenuItem";
            this.파일FToolStripMenuItem.Size = new System.Drawing.Size(57, 23);
            this.파일FToolStripMenuItem.Text = "파일(&F)";
            // 
            // 새프리셋ToolStripMenuItem
            // 
            this.새프리셋ToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("새프리셋ToolStripMenuItem.Image")));
            this.새프리셋ToolStripMenuItem.Name = "새프리셋ToolStripMenuItem";
            this.새프리셋ToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.N)));
            this.새프리셋ToolStripMenuItem.Size = new System.Drawing.Size(197, 22);
            this.새프리셋ToolStripMenuItem.Text = "새 프리셋";
            this.새프리셋ToolStripMenuItem.Click += new System.EventHandler(this.새프리셋ToolStripMenuItem_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(194, 6);
            this.toolStripSeparator2.Click += new System.EventHandler(this.toolStripSeparator2_Click);
            // 
            // 프리셋열기ToolStripMenuItem
            // 
            this.프리셋열기ToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("프리셋열기ToolStripMenuItem.Image")));
            this.프리셋열기ToolStripMenuItem.Name = "프리셋열기ToolStripMenuItem";
            this.프리셋열기ToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.O)));
            this.프리셋열기ToolStripMenuItem.Size = new System.Drawing.Size(197, 22);
            this.프리셋열기ToolStripMenuItem.Text = "프리셋 열기(&O)";
            this.프리셋열기ToolStripMenuItem.Click += new System.EventHandler(this.프리셋열기ToolStripMenuItem_Click);
            // 
            // 프리셋저장ToolStripMenuItem
            // 
            this.프리셋저장ToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("프리셋저장ToolStripMenuItem.Image")));
            this.프리셋저장ToolStripMenuItem.Name = "프리셋저장ToolStripMenuItem";
            this.프리셋저장ToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
            this.프리셋저장ToolStripMenuItem.Size = new System.Drawing.Size(197, 22);
            this.프리셋저장ToolStripMenuItem.Text = "프리셋 저장(&S)";
            this.프리셋저장ToolStripMenuItem.Click += new System.EventHandler(this.프리셋저장ToolStripMenuItem_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(194, 6);
            // 
            // 끝내기XToolStripMenuItem
            // 
            this.끝내기XToolStripMenuItem.Image = global::HS_Audio.Properties.Resources.FillLeftHS;
            this.끝내기XToolStripMenuItem.Name = "끝내기XToolStripMenuItem";
            this.끝내기XToolStripMenuItem.Size = new System.Drawing.Size(197, 22);
            this.끝내기XToolStripMenuItem.Text = "닫기(&X)";
            // 
            // 설정TToolStripMenuItem
            // 
            this.설정TToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.프리셋경로재설정ToolStripMenuItem,
            this.toolStripSeparator4,
            this.자동DSP등록ToolStripMenuItem});
            this.설정TToolStripMenuItem.Name = "설정TToolStripMenuItem";
            this.설정TToolStripMenuItem.Size = new System.Drawing.Size(58, 23);
            this.설정TToolStripMenuItem.Text = "설정(&T)";
            this.설정TToolStripMenuItem.DropDownOpening += new System.EventHandler(this.설정TToolStripMenuItem_DropDownOpening);
            // 
            // 프리셋경로재설정ToolStripMenuItem
            // 
            this.프리셋경로재설정ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.프리셋열기경로재설정ToolStripMenuItem,
            this.프리셋저장경로재설정ToolStripMenuItem1,
            this.toolStripSeparator5,
            this.프리셋폴더경로재설정ToolStripMenuItem});
            this.프리셋경로재설정ToolStripMenuItem.Name = "프리셋경로재설정ToolStripMenuItem";
            this.프리셋경로재설정ToolStripMenuItem.Size = new System.Drawing.Size(176, 22);
            this.프리셋경로재설정ToolStripMenuItem.Text = "프리셋 경로 재설정";
            this.프리셋경로재설정ToolStripMenuItem.Click += new System.EventHandler(this.프리셋경로재설정ToolStripMenuItem_Click);
            // 
            // 프리셋열기경로재설정ToolStripMenuItem
            // 
            this.프리셋열기경로재설정ToolStripMenuItem.Name = "프리셋열기경로재설정ToolStripMenuItem";
            this.프리셋열기경로재설정ToolStripMenuItem.Size = new System.Drawing.Size(203, 22);
            this.프리셋열기경로재설정ToolStripMenuItem.Text = "프리셋 열기 경로 재설정";
            this.프리셋열기경로재설정ToolStripMenuItem.Click += new System.EventHandler(this.프리셋열기경로재설정ToolStripMenuItem_Click);
            // 
            // 프리셋저장경로재설정ToolStripMenuItem1
            // 
            this.프리셋저장경로재설정ToolStripMenuItem1.Name = "프리셋저장경로재설정ToolStripMenuItem1";
            this.프리셋저장경로재설정ToolStripMenuItem1.Size = new System.Drawing.Size(203, 22);
            this.프리셋저장경로재설정ToolStripMenuItem1.Text = "프리셋 저장 경로 재설정";
            this.프리셋저장경로재설정ToolStripMenuItem1.Click += new System.EventHandler(this.프리셋저장경로재설정ToolStripMenuItem1_Click);
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(200, 6);
            // 
            // 프리셋폴더경로재설정ToolStripMenuItem
            // 
            this.프리셋폴더경로재설정ToolStripMenuItem.Name = "프리셋폴더경로재설정ToolStripMenuItem";
            this.프리셋폴더경로재설정ToolStripMenuItem.Size = new System.Drawing.Size(203, 22);
            this.프리셋폴더경로재설정ToolStripMenuItem.Text = "프리셋 폴더 경로 재설정";
            this.프리셋폴더경로재설정ToolStripMenuItem.Click += new System.EventHandler(this.프리셋폴더경로재설정ToolStripMenuItem_Click);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(173, 6);
            // 
            // 자동DSP등록ToolStripMenuItem
            // 
            this.자동DSP등록ToolStripMenuItem.Checked = true;
            this.자동DSP등록ToolStripMenuItem.CheckOnClick = true;
            this.자동DSP등록ToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.자동DSP등록ToolStripMenuItem.Name = "자동DSP등록ToolStripMenuItem";
            this.자동DSP등록ToolStripMenuItem.Size = new System.Drawing.Size(176, 22);
            this.자동DSP등록ToolStripMenuItem.Text = "자동 DSP 등록";
            this.자동DSP등록ToolStripMenuItem.CheckedChanged += new System.EventHandler(this.자동DSP등록ToolStripMenuItem_CheckedChanged);
            // 
            // 정렬ToolStripMenuItem
            // 
            this.정렬ToolStripMenuItem.AutoToolTip = true;
            this.정렬ToolStripMenuItem.BackColor = System.Drawing.Color.Transparent;
            this.정렬ToolStripMenuItem.Checked = true;
            this.정렬ToolStripMenuItem.CheckOnClick = true;
            this.정렬ToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.정렬ToolStripMenuItem.Enabled = false;
            this.정렬ToolStripMenuItem.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.정렬ToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("정렬ToolStripMenuItem.Image")));
            this.정렬ToolStripMenuItem.Name = "정렬ToolStripMenuItem";
            this.정렬ToolStripMenuItem.Size = new System.Drawing.Size(28, 23);
            this.정렬ToolStripMenuItem.ToolTipText = "콤보박스의 항목을 언어순에 따라 위로 정렬합니다.";
            this.정렬ToolStripMenuItem.CheckedChanged += new System.EventHandler(this.정렬ToolStripMenuItem_CheckedChanged);
            // 
            // toolStripComboBox1
            // 
            this.toolStripComboBox1.AutoToolTip = true;
            this.toolStripComboBox1.Name = "toolStripComboBox1";
            this.toolStripComboBox1.Size = new System.Drawing.Size(155, 23);
            this.toolStripComboBox1.Text = "(기본값)";
            this.toolStripComboBox1.ToolTipText = "이뭘라이저(EQ)프리셋 목록 입니다,";
            this.toolStripComboBox1.SelectedIndexChanged += new System.EventHandler(this.toolStripComboBox1_SelectedIndexChanged);
            // 
            // 폴더선택ToolStripMenuItem
            // 
            this.폴더선택ToolStripMenuItem.AutoToolTip = true;
            this.폴더선택ToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("폴더선택ToolStripMenuItem.Image")));
            this.폴더선택ToolStripMenuItem.Name = "폴더선택ToolStripMenuItem";
            this.폴더선택ToolStripMenuItem.Size = new System.Drawing.Size(28, 23);
            this.폴더선택ToolStripMenuItem.ToolTipText = "현재 선택된 프리셋을 목록에서 제거합니다.";
            this.폴더선택ToolStripMenuItem.Click += new System.EventHandler(this.폴더선택ToolStripMenuItem_Click);
            // 
            // 새로고침ToolStripMenuItem
            // 
            this.새로고침ToolStripMenuItem.AutoToolTip = true;
            this.새로고침ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.자동으로새로고침ToolStripMenuItem});
            this.새로고침ToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("새로고침ToolStripMenuItem.Image")));
            this.새로고침ToolStripMenuItem.Name = "새로고침ToolStripMenuItem";
            this.새로고침ToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F5;
            this.새로고침ToolStripMenuItem.Size = new System.Drawing.Size(28, 23);
            this.새로고침ToolStripMenuItem.Click += new System.EventHandler(this.새로고침ToolStripMenuItem_Click);
            // 
            // 자동으로새로고침ToolStripMenuItem
            // 
            this.자동으로새로고침ToolStripMenuItem.CheckOnClick = true;
            this.자동으로새로고침ToolStripMenuItem.Name = "자동으로새로고침ToolStripMenuItem";
            this.자동으로새로고침ToolStripMenuItem.Size = new System.Drawing.Size(173, 22);
            this.자동으로새로고침ToolStripMenuItem.Text = "자동으로 새로고침";
            // 
            // 제거ToolStripMenuItem
            // 
            this.제거ToolStripMenuItem.AutoToolTip = true;
            this.제거ToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("제거ToolStripMenuItem.Image")));
            this.제거ToolStripMenuItem.Name = "제거ToolStripMenuItem";
            this.제거ToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.Delete;
            this.제거ToolStripMenuItem.Size = new System.Drawing.Size(28, 23);
            this.제거ToolStripMenuItem.ToolTipText = "현재 선택된 프리셋을 목록에서 제거합니다.";
            this.제거ToolStripMenuItem.Click += new System.EventHandler(this.제거ToolStripMenuItem_Click);
            // 
            // saveFileDialog1
            // 
            this.saveFileDialog1.DefaultExt = "*.eqprof";
            this.saveFileDialog1.FileName = "제목 없음";
            this.saveFileDialog1.Filter = "이퀄라이저 프리셋 (*.eqprof)|*.eqprof";
            this.saveFileDialog1.Title = "다른 이름으로 이퀄라이저(EQ) 프로파일 저장";
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.DefaultExt = "*.eqprof";
            this.openFileDialog1.FileName = "제목 없음";
            this.openFileDialog1.Filter = "이퀄라이저 프리셋 (*.eqprof)|*.eqprof";
            this.openFileDialog1.Title = "이퀄라이저(EQ) 프로파일 열기";
            // 
            // folderBrowserDialog1
            // 
            this.folderBrowserDialog1.Description = "이퀄라이저(EQ) 프리셋이 들어있는 폴더 선택";
            // 
            // frmEqulazer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(805, 352);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.EQUse);
            this.Controls.Add(this.menuStrip1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.MinimumSize = new System.Drawing.Size(88, 104);
            this.Name = "frmEqulazer";
            this.Text = "이퀄라이저(EQ) 설정";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmEqulazer_FormClosing);
            this.Load += new System.EventHandler(this.frmEqulazer_Load);
            ((System.ComponentModel.ISupportInitialize)(this.trb550Hz)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trb40Hz)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trb64Hz)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trb78Hz)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trb90Hz)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trb650Hz)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trb110Hz)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trb300Hz)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trb125Hz)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trb400Hz)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trb200Hz)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trb500Hz)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trb800Hz)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trb950Hz)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trb9KHz)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trb7KHz)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trb8KHz)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trb10KHz)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trb11KHz)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trb12KHz)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trb13KHz)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trb14KHz)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trb15KHz)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trb16KHz)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trb17KHz)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trb18KHz)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trb19KHz)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trb20KHz)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trb6KHz)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trb5KHz)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trb900Hz)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trb4KHz)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trb750Hz)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trb3KHz)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trb600Hz)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trb2KHz)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trb1KHz)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trb850Hz)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trb700Hz)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trbGain)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TrackBar trb550Hz;
        private System.Windows.Forms.Button button20;
        private System.Windows.Forms.Button button21;
        private System.Windows.Forms.TrackBar trb40Hz;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TrackBar trb64Hz;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.TrackBar trb78Hz;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button11;
        private System.Windows.Forms.TrackBar trb90Hz;
        private System.Windows.Forms.TrackBar trb650Hz;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.TrackBar trb110Hz;
        private System.Windows.Forms.TrackBar trb300Hz;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.TrackBar trb125Hz;
        private System.Windows.Forms.TrackBar trb400Hz;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button10;
        private System.Windows.Forms.TrackBar trb200Hz;
        private System.Windows.Forms.TrackBar trb500Hz;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button button22;
        private System.Windows.Forms.Button button23;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TrackBar trb800Hz;
        private System.Windows.Forms.Button button26;
        private System.Windows.Forms.Button button27;
        private System.Windows.Forms.TrackBar trb950Hz;
        private System.Windows.Forms.Button button30;
        private System.Windows.Forms.Button button25;
        private System.Windows.Forms.Button button28;
        private System.Windows.Forms.TrackBar trb9KHz;
        private System.Windows.Forms.Button button29;
        private System.Windows.Forms.Button button24;
        private System.Windows.Forms.Button button32;
        private System.Windows.Forms.Button button31;
        private System.Windows.Forms.Button button36;
        private System.Windows.Forms.Button button35;
        private System.Windows.Forms.TrackBar trb7KHz;
        private System.Windows.Forms.Button button34;
        private System.Windows.Forms.TrackBar trb8KHz;
        private System.Windows.Forms.Button button33;
        private System.Windows.Forms.Button button19;
        private System.Windows.Forms.TrackBar trb10KHz;
        private System.Windows.Forms.TrackBar trb11KHz;
        private System.Windows.Forms.Button button12;
        private System.Windows.Forms.Button button18;
        private System.Windows.Forms.TrackBar trb12KHz;
        private System.Windows.Forms.TrackBar trb13KHz;
        private System.Windows.Forms.Button button13;
        private System.Windows.Forms.Button button17;
        private System.Windows.Forms.TrackBar trb14KHz;
        private System.Windows.Forms.TrackBar trb15KHz;
        private System.Windows.Forms.Button button14;
        private System.Windows.Forms.Button button16;
        private System.Windows.Forms.TrackBar trb16KHz;
        private System.Windows.Forms.TrackBar trb17KHz;
        private System.Windows.Forms.Button button15;
        private System.Windows.Forms.TrackBar trb18KHz;
        private System.Windows.Forms.Button button39;
        private System.Windows.Forms.TrackBar trb19KHz;
        private System.Windows.Forms.Button button38;
        private System.Windows.Forms.TrackBar trb20KHz;
        private System.Windows.Forms.Button button37;
        private System.Windows.Forms.TrackBar trb6KHz;
        private System.Windows.Forms.TrackBar trb5KHz;
        private System.Windows.Forms.TrackBar trb900Hz;
        private System.Windows.Forms.TrackBar trb4KHz;
        private System.Windows.Forms.TrackBar trb750Hz;
        private System.Windows.Forms.TrackBar trb3KHz;
        private System.Windows.Forms.TrackBar trb600Hz;
        private System.Windows.Forms.TrackBar trb2KHz;
        private System.Windows.Forms.TrackBar trb1KHz;
        private System.Windows.Forms.TrackBar trb850Hz;
        private System.Windows.Forms.TrackBar trb700Hz;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TrackBar trbGain;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.RadioButton rbChannel;
        private System.Windows.Forms.RadioButton rbMainOuput;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem 파일FToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 프리셋열기ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 프리셋저장ToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem 끝내기XToolStripMenuItem;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.ToolStripMenuItem 새프리셋ToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripComboBox toolStripComboBox1;
        private System.Windows.Forms.ToolStripMenuItem 새로고침ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 제거ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 정렬ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 폴더선택ToolStripMenuItem;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
        private System.Windows.Forms.ToolStripMenuItem 설정TToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 프리셋경로재설정ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 프리셋열기경로재설정ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 프리셋저장경로재설정ToolStripMenuItem1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.ToolStripMenuItem 프리셋폴더경로재설정ToolStripMenuItem;
        public System.Windows.Forms.CheckBox EQUse;
        private System.Windows.Forms.ToolStripMenuItem 자동으로새로고침ToolStripMenuItem;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripMenuItem 자동DSP등록ToolStripMenuItem;
    }
}