﻿using System;
using System.Drawing;
using System.Threading;
using System.Windows.Forms;
using HS_Audio.DSP;
using System.IO;
using System.Collections.Generic;

namespace HS_Audio.Forms
{
    public partial class frmAGC : Form
    {
        public enum Channels{Both=0, L=1, R=2}

        //public void INIT<T>(T Obj){Obj = new T();}
        #region 실제 처리 함수

        #region AGC
        float[] InBuffer;
        float[] OutBuffer;
        uint length;
        int outchannels;
        int inchannels;
        float[] GainAGC = { 0.9999999f, 0.9999999f };
        float[] AutoRatio = new float[2];
        float[] AutoRatio1 = new float[2];
        bool ChannelSeperate;
        public HS_Audio_Lib.RESULT READCALLBACK_AGC(ref HS_Audio_Lib.DSP_STATE dsp_state, IntPtr InBuf, IntPtr OutBuf, uint length, int inchannels, int outchannels)
        {
            uint count = 0;
            int count2 = 0;
            
            unsafe
            {
                float* b;
                float* inbuffer = (float*)InBuf.ToPointer();
                float* outbuffer = (float*)OutBuf.ToPointer();

                if(AutoRatio.Length!=outchannels) AutoRatio = new float[outchannels];

                if (InBuffer == null||InBuffer.Length != length * inchannels) InBuffer = new float[length * inchannels];
                if (OutBuffer == null || OutBuffer.Length != length * outchannels) OutBuffer = new float[length * outchannels];
                this.outchannels = inchannels; this.inchannels = inchannels; this.length = length;
                for (count = 0; count < length; count++)
                 for (count2 = 0; count2 < inchannels; count2++)
                     InBuffer[(count * inchannels) + count2] = inbuffer[(count * inchannels) + count2];
                if(GainAGC.Length!=inchannels)GainAGC = new float[inchannels].ValueInit(1);

                /*
                for (count = 0; count < length; count++)
                    for (count2 = 0; count2 < outchannels; count2++)
                        outbuffer[(count * outchannels) + count2] = inbuffer[(count * outchannels) + count2];
                goto Exit;*/
                //float* MaxHigh_L, MaxHigh_R, MaxLow_L, MaxLow_R;
                //float[] a = new float[outchannels * 2];
                                /*
                for (count = 0; count < length; count++)
                {
                    for (count2 = 0; count2 < outchannels; count2++)
                    {
                        b = &inbuffer[(count * outchannels) + count2];
                        if (b[0] > 1.0f && Peak[count2] < (b[0] - 1.0f)) Peak[count2] = b[0] - 1.0f;
                        if (b[0] < -1.0f && Peak[count2 + outchannels] > (b[0] + 1.0f)) Peak[count2 + outchannels] = b[0] + 1.0f;
                    }
                }*/
                
                for (count = 0; count < length; count++)
                {
                    for (count2 = 0; count2 < outchannels; count2++)
                    {
                        b = &inbuffer[(count * outchannels) + count2];
                        outbuffer[(count * outchannels) + count2] =(float)HSDSP.Volume(b[0],ChannelSeperate?AutoRatio[count2]:AutoRatio[0]); // b[0].BassBoosta(200,  ChannelSeperate ?GainAGC[count2]:GainAGC[0], ChannelSeperate ? AutoRatio[count2] : AutoRatio[0]);
                    }
                }
                for (count = 0; count < length; count++)
                    for (count2 = 0; count2 < outchannels; count2++)
                        OutBuffer[(count * outchannels) + count2] = outbuffer[(count * outchannels) + count2];
                /*
                float[] a1 = new float[outchannels];
                for(int i = 0; i < a1.Length; i++)a1[i] = float.MinValue;
                for (count2 = 0; count2 < outchannels; count2++)
                    if (Decibels(a[count2],0) > Decibels(a[count2+outchannels], 0)) a1[count2] = a[count2];
                    else a1[count2] = a[count2+outchannels];
                double a2 = double.MinValue;
                for (count = 0; count < length; count++)
                {
                    for (count2 = 0; count2 < outchannels; count2++)
                    {
                        b = &inbuffer[(count * inchannels) + count2];
                        if(Minus)
                        {
                            outbuffer[(count * outchannels) + count2] = b[0]>0?b[0] - a1[count2]:b[0] + a1[count2];
                            a2 = Decibels(outbuffer[(count * outchannels) + count2],0);
                            //if (b[0]>0f) outbuffer[(count * outchannels) + count2] = b[0] - a1[count2];
                            //else outbuffer[(count * outchannels) + count2] = b[0] - a[count2 + outchannels];
                        }
                        else
                        {
                            if (b[0] > 1.0f) outbuffer[(count * outchannels) + count2] = b[0] - a[count2];
                            else if (b[0] < -1.0f) outbuffer[(count * outchannels) + count2] = b[0] - a[count2 + outchannels];
                            else outbuffer[(count * outchannels) + count2] = b[0];
                        }
                    }
                }*/
            }

        Exit:
            return HS_Audio_Lib.RESULT.OK;
        }

        double MaxdB = 0;
        Thread AutoGainThread;
        bool IsMax;
        void AutoGainThread_Loop(object o)
        {
            AutoGainThread = Thread.CurrentThread;
            double aa = double.NaN;
            float b = float.NaN;
            double[] LastPeak = new double[inchannels];
            float[] PeakValue = new float[inchannels].ValueInit(1);
            double[] Peak = new double[0];
            float tmp = 0;
            while (true)
            {
                //bool ChannelSeperate = this.ChannelSeperate;
                uint count = 0;
                int count2 = 0;
                IsMax = false;
                try
                {
                    if (Peak.Length!= outchannels)Peak = new double[inchannels];
                    for (int i = 0; i < Peak.Length; i++) Peak[i] = LastPeak[i];// + (LastPeak[i] < MaxdB ? 0.01f : 0); //Peak[i] = LastPeak[i] < MaxdB ? LastPeak[i] + (LastPeak[i] < MaxdB ? 0.01f : 0) : LastPeak[i] - 0.05f;
                    if (PeakValue.Length != outchannels) PeakValue = new float[outchannels];
                    //if (AutoRatio.Length != outchannels) AutoRatio = new float[outchannels];
                    //if (NowdB.Length != outchannels) NowdB = new double[outchannels];
                    #region
                    if (ChannelSeperate)
                    {
                        for (count = 0; count < InBuffer.Length / inchannels; count++)
                        {
                            for (count2 = 0; count2 < inchannels; count2++)
                            {
                                b = InBuffer[(count * inchannels) + count2];
                                double pk = Decibels(b, 0);
                                if (pk > MaxdB)
                                {
                                    //IsMax = true;
                                    if (pk > Peak[count2])
                                    {
                                        //HS_Audio_Helper.DSP.HSDSP.AGC(inbuffer, outbuffer, AGC, length, outchannels); goto Exit;
                                        PeakValue[count2] = b;
                                        Peak[count2] = pk;
                                    }
                                }
                            }
                        }
                        for (int i = 0; i < PeakValue.Length; i++)
                        {
                            float bb = (Peak[i] == 0||Peak[i] == float.NaN) ?1 : (float)Peak[i];
                            for (int j = -200; j <= 0; j++)
                            {
                                aa = HSDSP.BassBoosta_GainControl(PeakValue[i], 200, bb, j);
                                if (Decibels(aa, 0) >= MaxdB)
                                {
                                    AutoRatio[i] = j - 1; break;
                                }
                                //else if (Decibels(PeakValue[i], 0) > MaxdB) PeakValue[i] = PeakValue[i] > 0 ? PeakValue[i] - 0.05f : PeakValue[i] + 0.05f;
                            }
                            GainAGC[i] = bb;
                        }
                        LastPeak = Peak;
                    }
                    #endregion
                    #region
                    else
                    {
                        for (count = 0; count < InBuffer.Length; count++)
                        {
                            b = InBuffer[count];
                            double pk = Decibels(b, 0);

                            if (pk > MaxdB)
                            {
                                IsMax = pk > MaxdB;
                                if (Peak[0] < pk)
                                {
                                    PeakValue[0] = b;
                                    Peak[0] = pk;
                                }
                            }

                            //else if (Peak[0] > Decibels(PeakValue[0], 0)) PeakValue[0] = b;
                            //Peak[0] = Peak[0]+(Peak[0] < MaxdB?0.05f:-0.05f);
                        }
                        //float bb = (Peak[0] == 0 || Peak[0] == float.NaN) ? 1 : (float)Peak[0];
                        if (IsMax)
                        {
                            tmp = 0;
                            for (float j = -200; j <= 0; j += 0.2f)
                            {
                                aa = HSDSP.Volume(PeakValue[0], j);//aa = HSDSP.BassBoosta(PeakValue[0], 200, bb, j);
                                if (Decibels(aa, 0) >= MaxdB)
                                {
                                    AutoRatio[0] = j - 1; break;
                                    StopPeak = false;
                                }
                                //else AutoRatio[0] = j;
                            }
                        }
                        else
                        {
                            if (!StopPeak)
                            {
                                ///output스레드를 하나 더 만든다
                                if (Peak[0] > MaxdB)
                                {
                                    tmp -= 0.02f;
                                    AutoRatio[0] = AutoRatio[0] += 0.02f;
                                    Peak[0] = HSDSP.Volume(Peak[0], tmp);
                                }
                                else if (Peak[0] < MaxdB)
                                {
                                    tmp += 0.02f;
                                    AutoRatio[0] -= 0.02f;
                                    Peak[0] = HSDSP.Volume(Peak[0], tmp);
                                }
                                else tmp = AutoRatio[0] = 0;
                            }
                        }
                        //if (!IsMax) if (Peak[0] + 0.05 < MaxdB) Peak[0] = Peak[0] + 0.05f;
                        //GainAGC[0] = bb;
                        LastPeak = Peak;
                    }
                    #endregion
                    /*
                    if (NowdB[i]< Peak[i])
                    {
                        //if (MaxdB > Peak[i]) Peak[i] =Peak[i] +0.05f;
                        //else if(MaxdB <Peak[i])Peak[i] = Peak[i] + 0.05f;
                        //else Peak[i]  = MaxdB;
                        //Peak[i] = MaxdB;
                        Peak[i] = Peak[i]+0.05;
                        if (AutoRatio[i] < 0) AutoRatio[i] = AutoRatio[i] + 1;
                        if (Decibels(PeakValue[i], 0) > MaxdB) PeakValue[i] = PeakValue[i] > 0 ? PeakValue[i] - 0.05f : PeakValue[i] + 0.05f;
                    }

                    if (Peak != null)
                        for (int i = 0; i < Peak.Length; i++)
                            if (Peak[i] > (MaxdB == 0 ? 0.9999999f : MaxdB)) Peak[i] = Peak[i] - 0.005f;
                            else if (Peak[i] < (MaxdB == 0 ? 0.9999999f : MaxdB)) Peak[i] = Peak[i] + 0.005f;*/
                }
                catch(Exception ex)
                {
                    HS_CSharpUtility.LoggingUtility.Logging(ex);
                }
                Thread.Sleep(25);
            }
        }

        bool StopPeak;
        Thread AutoPeakThread;
        void AutoPeakThread_Loop(object o)
        {
            AutoPeakThread = Thread.CurrentThread;
            double aa = double.NaN;
            float b = float.NaN;
            double[] LastPeak = new double[inchannels];
            float[] PeakValue = new float[inchannels].ValueInit(1);
            double[] Peak = new double[0];
            bool IsMax;
            //float tmp = 0;
            while (true)
            {
                uint count = 0;
                int count2 = 0;

                IsMax = false;
                try
                {
                    if (Peak.Length != outchannels) Peak = new double[inchannels];
                    for (int i = 0; i < Peak.Length; i++) Peak[i] = LastPeak[i];// + (LastPeak[i] < MaxdB ? 0.01f : 0); //Peak[i] = LastPeak[i] < MaxdB ? LastPeak[i] + (LastPeak[i] < MaxdB ? 0.01f : 0) : LastPeak[i] - 0.05f;
                    if (PeakValue.Length != outchannels) PeakValue = new float[outchannels];

                    //if (!IsMax)
                    {
                        if (ChannelSeperate) ;
                        else
                        {

                            /*for(float i = AutoRatio[0];i<200;i+=0.5f)
                                aa = (float)HSDSP.Volume(PeakValue[0], j);
                                if (pk > MaxdB)
                                {
                                    IsMax = true;
                                    if (Peak[0] < pk)
                                    {
                                        PeakValue[0] = b;
                                        Peak[0] = pk;
                                    }
                                }*/
                            for (count = 0; count < OutBuffer.Length; count++)
                            {
                                b = OutBuffer[count];
                                double pk = Decibels(b, 0);

                                if (pk > MaxdB)
                                {
                                    IsMax =true;
                                    if (Peak[0] < pk)
                                    {
                                        PeakValue[0] = b;
                                        Peak[0] = pk;
                                    }
                                }

                                //else if (Peak[0] > Decibels(PeakValue[0], 0)) PeakValue[0] = b;
                                //Peak[0] = Peak[0]+(Peak[0] < MaxdB?0.05f:-0.05f);
                            }
                            //float bb = (Peak[0] == 0 || Peak[0] == float.NaN) ? 1 : (float)Peak[0];
                            if (IsMax)
                            {
                                tmp = 0;
                                for (float j = -200; j <= 0; j += 0.2f)
                                {
                                    aa = HSDSP.Volume(PeakValue[0], j);//aa = HSDSP.BassBoosta(PeakValue[0], 200, bb, j);
                                    double dB = Decibels(aa, 0);
                                    if (dB >= MaxdB)
                                    {
                                        StopPeak = true;
                                        //AutoRatio[0] = j - 1; break;
                                    }
                                    //else AutoRatio[0] = j;
                                }
                            }
                            if (IsMax) StopPeak = false;
                        }
                    }
                }catch{}
                Thread.Sleep(30);
            }
        }
        #endregion

        #region HS™ 부스터
        bool dBBarHSBoosterEnable = false;

        float READCALLBACK_BassBoosterFrequency = 55.0f;
        float READCALLBACK_BassBoosterRatio=0.2f;

        float READCALLBACK_BassBoosterWetMix = 0.3f;
        /*
        float READCALLBACK_BassBoosterWetMix
        {
            get{return _READCALLBACK_BassBoosterWetMix;}
            set{READCALLBACK_BassBoosterWetSub = value<0;_READCALLBACK_BassBoosterWetMix = -value;}
        }*/

        float READCALLBACK_BassBoosterDryMix = 0.4f;
        /*
        float READCALLBACK_BassBoosterDryMix
        {
            get{return _READCALLBACK_BassBoosterDryMix;}
            set{READCALLBACK_BassBoosterDrySub = value<0;_READCALLBACK_BassBoosterDryMix = -value;}
        }*/

        float READCALLBACK_BassBoosterMix = 0.5f;
        /*
        float READCALLBACK_BassBoosterMix
        {
            get{return _READCALLBACK_BassBoosterMix;}
            set{READCALLBACK_BassBoosterMixSub = value<0;_READCALLBACK_BassBoosterMix = -value;}
        }*/

        bool READCALLBACK_BassBoosterLow = true;
        bool READCALLBACK_BassBoosterAdvanced = true;
        bool READCALLBACK_BassBoosterMono = true;
        int READCALLBACK_BassBooster_BeforeChannel = 2;

        bool READCALLBACK_BassBoosterWetSub = false;
        bool READCALLBACK_BassBoosterDrySub = false;
        bool READCALLBACK_BassBoosterMixSub = false;

        //HSDSP.BandPassFilter lpc = new HSDSP.BandPassFilter();
        HSDSP.BandPassFilter[] bb = new HSDSP.BandPassFilter[2];
        float[] READCALLBACK_BassBooster_Peak = new float[2];

        public HS_Audio_Lib.RESULT READCALLBACK_BassBooster(ref HS_Audio_Lib.DSP_STATE dsp_state, IntPtr InBuf, IntPtr OutBuf, uint length, int inchannels, int outchannels)
        {
            uint count = 0;
            int count2 = 0;
            if (!READCALLBACK_BassBoosterMono&&READCALLBACK_BassBooster_BeforeChannel != outchannels)
            {
                READCALLBACK_BassBooster_BeforeChannel = outchannels;
                bb = new HSDSP.BandPassFilter[outchannels];
                for(int i=0;i<bb.Length;i++)bb[i] = new HSDSP.BandPassFilter();
            }

            
            unsafe
            {
                float b;
                float* inbuffer = (float*)InBuf.ToPointer();
                float* outbuffer = (float*)OutBuf.ToPointer();
                //if(A == b&c == d && e == f)
                //HSDSP.LCLP(inbuffer, outbuffer, 1024, 2, 44100f, READCALLBACK_BassBoosterFrequency, READCALLBACK_BassBoosterRatio);
                for (count = 0; count < length; count++)
                {
                    for (count2 = 0; count2 < outchannels; count2++)
                    {
                        b = inbuffer[(count * outchannels) + count2];
                        //x = bb[0].Update(b, READCALLBACK_BassBoosterFrequency, READCALLBACK_BassBoosterRatio, 48000f, READCALLBACK_BassBoosterLow);
                        
                        float x = //outbuffer[(count * outchannels) + count2];
                            READCALLBACK_BassBoosterMono?
                        bb[0].Update(b, READCALLBACK_BassBoosterFrequency, READCALLBACK_BassBoosterRatio, 48000f, READCALLBACK_BassBoosterLow):
                        bb[count2].Update(b, READCALLBACK_BassBoosterFrequency, READCALLBACK_BassBoosterRatio, 24000.0f, READCALLBACK_BassBoosterLow);

                            //lpc.Update_16(b, READCALLBACK_BassBoosterFrequency, READCALLBACK_BassBoosterRatio, 48000.0f, READCALLBACK_BassBoosterLow);
                            //bb.Process(b, 70, 1f, 0.1f);

                        //float Wet = HSDSP.SetMix(x, x + y, READCALLBACK_BassBoosterDryMix);


                        outbuffer[(count * outchannels) + count2] = READCALLBACK_BassBoosterAdvanced ?
                            HSDSP.SetMix(b, x, READCALLBACK_BassBoosterWetMix, READCALLBACK_BassBoosterDryMix, 1, 0):
                            HSDSP.SetMix(b, x, READCALLBACK_BassBoosterMix);
                        /*
                        if (READCALLBACK_BassBoosterAdvanced)
                        {
                            float a1 = HSDSP.SetMix(0, b, READCALLBACK_BassBoosterDryMix);
                            float a2 = HSDSP.SetMix(0, x, READCALLBACK_BassBoosterWetMix);

                        }
                        else outbuffer[(count * outchannels) + count2] = HSDSP.SetMix(b, x, READCALLBACK_BassBoosterMix > 0 ? READCALLBACK_BassBoosterMix : READCALLBACK_BassBoosterMix / 2f);
                        */
                        /*
                        float c = READCALLBACK_BassBoosterAdvanced?
                            HSDSP.SetMix(b, x, READCALLBACK_BassBoosterWetMix, READCALLBACK_BassBoosterDryMix, 1, 0, false):
                        outbuffer[(count * outchannels) + count2] = c;*/
                        //outbuffer[(count * outchannels) + count2] = HS_Audio_Helper.DSP.HSDSP.BassBoosta(b[0], Hz, Gain_Bass, Ratio);

                    }
                }
                if (dBBarHSBoosterEnable)
                {
                    float[] a = HSDSPUtils.GetMax(outbuffer, length * 2, inchannels);
                    for (int i = 0; i < a.Length; i++) a[i] = a[i].Decibels();
                    READCALLBACK_BassBooster_Peak = a;
                }
            }
            return HS_Audio_Lib.RESULT.OK;
        }
        #endregion

        #region 게인 조절
        float Gain;
        Channels GainControlChannel = Channels.Both;
        int GainControlIndex;

        float tmp;
        public HS_Audio_Lib.RESULT READCALLBACK_Gain(ref HS_Audio_Lib.DSP_STATE dsp_state, IntPtr InBuf, IntPtr OutBuf, uint length, int inchannels, int outchannels)
        {
            int count = 0;
            int count2 = 0;

            unsafe
            {
                float* inbuffer = (float*)InBuf.ToPointer();
                float* outbuffer = (float*)OutBuf.ToPointer();
                //if(A == b&c == d && e == f)

                for (count = 0; count < length; count++)
                {
                    for (count2 = 0; count2 < outchannels; count2++)
                    {
                        //b = &inbuffer[(count * outchannels) + count2];
                        GainControlIndex = (count * outchannels) + count2;
                        tmp = inbuffer[GainControlIndex];
                        switch (GainControlChannel)
                        {
                            case Channels.L: outbuffer[GainControlIndex] = count2 == 0 ? HSDSP.Volume(tmp/*b[0]*/, Gain) : tmp; break;
                            case Channels.R: outbuffer[GainControlIndex] = count2 == 1 ? HSDSP.Volume(tmp/*b[0]*/, Gain) : tmp; break;
                            default: outbuffer[GainControlIndex] = HSDSP.Volume(tmp/*b[0]*/, Gain); break;
                        }
                    }
                }
            }
            return HS_Audio_Lib.RESULT.OK;
        }
        #endregion

        #region 스테레오 확장
        bool dBBarStereoExpandEnable = false;

        float READCALLBACK_StereoExpandSideValue = 0.5f;
        float READCALLBACK_StereoExpandMidValue = 0.5f;
        float READCALLBACK_StereoExpandGainValue = 0;
        float READCALLBACK_StereoExpandMethod = 1.414213562373095f;
        float READCALLBACK_StereoExpandWetMix = 1f;
        float READCALLBACK_StereoExpandDryMix = 0f;
        float READCALLBACK_StereoExpandMix = 1f;
        decimal StereoExpandAutoGain = -0.055m;
        float StereoExpandAutoGainValue = 5.5f;
        bool READCALLBACK_StereoExpandAdvancd = false;
        public HS_Audio_Lib.RESULT READCALLBACK_StereoExpand(ref HS_Audio_Lib.DSP_STATE dsp_state, IntPtr InBuf, IntPtr OutBuf, uint length, int inchannels, int outchannels)
        {
            unsafe
            {
                float* inbuffer = (float*)InBuf.ToPointer();
                float* outbuffer = (float*)OutBuf.ToPointer();
                //if(A == b&c == d && e == f)'
                if (READCALLBACK_StereoExpandAdvancd) HSDSP.StereoExpand(inbuffer, outbuffer, length * 2, READCALLBACK_StereoExpandSideValue, READCALLBACK_StereoExpandMidValue, READCALLBACK_StereoExpandGainValue, READCALLBACK_StereoExpandWetMix, READCALLBACK_StereoExpandDryMix, READCALLBACK_StereoExpandMethod);
                else HSDSP.StereoExpand(inbuffer, outbuffer, length * 2, READCALLBACK_StereoExpandSideValue, READCALLBACK_StereoExpandMidValue, READCALLBACK_StereoExpandGainValue, READCALLBACK_StereoExpandMix, READCALLBACK_StereoExpandMethod);
                //this.Invoke(new Action(()=>{ }));
                if (dBBarStereoExpandEnable)
                {
                    float[] a = HSDSPUtils.GetMax(outbuffer, length * 2, inchannels);
                    for (int i = 0; i < a.Length; i++) a[i] = a[i].Decibels();
                    this.Invoke(new Action(()=> { dBBarStereoExpandCheck(a); }));
                }
            }
            return HS_Audio_Lib.RESULT.OK;
        }
        #endregion

        #region 모노 다운믹스
        float Mix_Mono = 1;
        public HS_Audio_Lib.RESULT READCALLBACK_MonoDownMix(ref HS_Audio_Lib.DSP_STATE dsp_state, IntPtr InBuf, IntPtr OutBuf, uint length, int inchannels, int outchannels)
        {
            unsafe
            {
                float* inbuffer = (float*)InBuf.ToPointer();
                float* outbuffer = (float*)OutBuf.ToPointer();
                HSDSP.SetMono(inbuffer, outbuffer, length * (uint)inchannels, outchannels, Mix_Mono);
            }
            return HS_Audio_Lib.RESULT.OK;
        }
        #endregion

        #region 소리 반전
        Channels SoundReverseChannel = Channels.Both;
        public HS_Audio_Lib.RESULT READCALLBACK_SoundReverse(ref HS_Audio_Lib.DSP_STATE dsp_state, IntPtr InBuf, IntPtr OutBuf, uint length, int inchannels, int outchannels)
        {
            int i, j;
            unsafe
            {
                //float N = 2f;
                float* inbuffer = (float*)InBuf.ToPointer();
                float* outbuffer = (float*)OutBuf.ToPointer();
                //if(A == b&c == d && e == f)'
                for (i = 0; i < length * outchannels; i += outchannels)
                    for (j = 0; j < outchannels; j++)
                    {
                        float x = inbuffer[i + j];
                        //outbuffer[i + j] = (float)Math.Sign(x) * (float)Math.Pow(Math.Atan(Math.Pow(Math.Abs(x), N)), (1 / N));
                        //outbuffer[i + j] = (float)(x*(Math.Abs(x) + N)/(Math.Pow(x, 2) + (N-1)*Math.Abs(x) + 1));
                        //HSDSP.LCLP(inbuffer, outbuffer, length*2, outchannels, 64, 1, 1);
                        switch (SoundReverseChannel)
                        {
                            case Channels.L: outbuffer[i + j] = j == 0 ? -x : x; break;
                            case Channels.R: outbuffer[i + j] = j == 1 ? -x : x; break;
                            default: outbuffer[i + j] = -x; break;
                        }
                    }
            }
            return HS_Audio_Lib.RESULT.OK;
        }
        #endregion

        #region 정수 변환
        enum Float2Int{Int=32,Int_24=24, Short=16,Bit12=12, Byte=8,Byte_Ex=-8,Bit7=7,Bit6=6,Bit5=5,Bit4=4,Bit3=3, Bypass=0}
        Float2Int HowFloat2Int = Float2Int.Int;
        bool ToIntDistoration;
        bool ToIntEx = false;
        public HS_Audio_Lib.RESULT READCALLBACK_ToInt(ref HS_Audio_Lib.DSP_STATE dsp_state, IntPtr InBuf, IntPtr OutBuf, uint length, int inchannels, int outchannels)
        {
            
            int i, j;
            unsafe
            {
                float* inbuffer = (float*)InBuf.ToPointer();
                float* outbuffer = (float*)OutBuf.ToPointer();
                //if(A == b&c == d && e == f)'
                for (i = 0; i < length * outchannels; i += outchannels)
                    for (j = 0; j < outchannels; j++)
                    {
                        //if()
                        switch (HowFloat2Int)
                        {
                            case Float2Int.Int: outbuffer[i + j] = ToIntEx ?
                                    HSDSPUtils.ToUInt(inbuffer[i + j], ToIntDistoration) / 4294967295f:
                                    HSDSPUtils.ToInt(inbuffer[i + j], ToIntDistoration) / 2147483647f;
                                break;
                            case Float2Int.Int_24: outbuffer[i + j] = ToIntEx ?
                                    HSDSPUtils.ToUInt24(inbuffer[i + j], ToIntDistoration) / 16777216f:
                                    HSDSPUtils.ToInt24(inbuffer[i + j], ToIntDistoration) / 8388608f;
                                break;
                            case Float2Int.Short:outbuffer[i + j] = ToIntEx ?
                                    HSDSPUtils.ToUShort(inbuffer[i + j], ToIntDistoration) / 65536f :
                                    HSDSPUtils.ToShort(inbuffer[i + j], ToIntDistoration) / 32768f;
                                break;
                            case Float2Int.Bit12: outbuffer[i + j] = HSDSPUtils.To11Bit(inbuffer[i + j], ToIntDistoration, ToIntEx) / (ToIntEx ? 4096f : 2048f); break;
                            case Float2Int.Byte: outbuffer[i + j] = ToIntEx ?
                                    HSDSPUtils.ToByte(inbuffer[i + j], ToIntDistoration) / 255f :
                                    HSDSPUtils.ToSByte(inbuffer[i + j], ToIntDistoration) / 128f;
                                break;
                            case Float2Int.Bit7: outbuffer[i + j] = HSDSPUtils.To7Bit(inbuffer[i + j], ToIntDistoration, ToIntEx) / (ToIntEx ? 128f : 64f); break;
                            case Float2Int.Bit6: outbuffer[i + j] = HSDSPUtils.To6Bit(inbuffer[i + j], ToIntDistoration, ToIntEx) / (ToIntEx ? 64f : 32f); break;
                            case Float2Int.Bit5: outbuffer[i + j] = HSDSPUtils.To5Bit(inbuffer[i + j], ToIntDistoration, ToIntEx) / (ToIntEx ? 32f : 16f); break;
                            case Float2Int.Bit4: outbuffer[i + j] = HSDSPUtils.To4Bit(inbuffer[i + j], ToIntDistoration, ToIntEx) / (ToIntEx ? 16f : 8f); break;
                            case Float2Int.Bit3: outbuffer[i + j] = HSDSPUtils.To3Bit(inbuffer[i + j], ToIntDistoration, ToIntEx) / (ToIntEx ? 8f : 4f); break;
                            default: outbuffer[i + j] = inbuffer[i + j]; break;
                        }
                        
                    }
            }
            return HS_Audio_Lib.RESULT.OK;
        }
        #endregion

        #region (예약 됨)
        uint READCALLBACK_예약_BeforeLength = 2;
        System.Collections.Generic.List<float[]> 예약_Buffer = new System.Collections.Generic.List<float[]>();
        public HS_Audio_Lib.RESULT READCALLBACK_예약(ref HS_Audio_Lib.DSP_STATE dsp_state, IntPtr InBuf, IntPtr OutBuf, uint length, int inchannels, int outchannels)
        {
            uint count = 0;
            int count2 = 0;

            if (예약_Buffer.Count != outchannels || READCALLBACK_예약_BeforeLength != length)
            {
                READCALLBACK_예약_BeforeLength = length;
                예약_Buffer.Clear();

                예약_Buffer = new System.Collections.Generic.List<float[]>();
                for (int i = 0; i < outchannels; i++) 예약_Buffer.Add(new float[length]);

            }

            unsafe
            {
                float b;
                float* inbuffer = (float*)InBuf.ToPointer();
                float* outbuffer = (float*)OutBuf.ToPointer();
                //if(A == b&c == d && e == f)

                for (count = 0; count < length; count++)
                {

                    for (count2 = 0; count2 < outchannels; count2++)
                    {
                        b = inbuffer[(count * outchannels) + count2];
                        fixed (float* aa = 예약_Buffer[(int)count2]) aa[count] = b;
                        //float x = bb[inchannels].Update(b, READCALLBACK_BassBoosterFrequency, READCALLBACK_BassBoosterRatio, 48000.0f, READCALLBACK_BassBoosterLow);
                    }
                }

                for (count2 = 0; count2 < outchannels; count2++)
                    fixed (float* aa = 예약_Buffer[count2])
                        HSDSP.BandPassFilter.UU(aa, aa, length, READCALLBACK_BassBoosterFrequency, READCALLBACK_BassBoosterRatio);

                for (count = 0; count < length; count++)
                {
                    for (count2 = 0; count2 < outchannels; count2++)
                    {
                        b = inbuffer[(count * outchannels) + count2];
                        float x = 0;
                        fixed (float* aa = 예약_Buffer[count2]) x = aa[count];

                        outbuffer[(count * outchannels) + count2] = x;
                    }
                }
            }
            return HS_Audio_Lib.RESULT.OK;
        }
        #endregion
        #endregion

        public frmAGC(HSAudioHelper Helper)
        {
            InitializeComponent();
            DSP = new HSAudioDSPHelper[10]; 
            DSP[0] = new HSAudioDSPHelper(Helper,READCALLBACK_AGC,true);
            DSP[1] = new HSAudioDSPHelper(Helper, READCALLBACK_BassBooster, true);
            DSP[2] = new HSAudioDSPHelper(Helper, READCALLBACK_Gain, true);
            DSP[3] = new HSAudioDSPHelper(Helper, READCALLBACK_StereoExpand, true);
            DSP[4] = new HSAudioDSPHelper(Helper, READCALLBACK_MonoDownMix, true);
            DSP[5] = new HSAudioDSPHelper(Helper, READCALLBACK_SoundReverse, true);
            DSP[6] = new HSAudioDSPHelper(Helper, READCALLBACK_ToInt, true);
            for (int i = 0; i < DSP.Length-1; i++) if(DSP[i]!=null)DSP[i].Bypass = true;
            for (int i = 0; i < DSP.Length - 1; i++) if (DSP[i] != null) DSP[i].DisConnect();
        }

        private void frmAGC_Load(object sender, EventArgs e)
        {
            menuStrip1.ImageScalingSize = new System.Drawing.Size(16, 16);
            HS_CSharpUtility.Utility.Win32Utility.HideXButton(this.Handle);
            READCALLBACK_StereoExpandSideValue = READCALLBACK_StereoExpandMidValue = 0.65f;
            READCALLBACK_StereoExpandGainValue = 30 * (float)StereoExpandAutoGain;
            numStereoExpandGainValue.Enabled = false;

            cbSoundReversChannels.SelectedIndex = 0;

            cbMonoDownMix.SelectedIndex = cbGainControlChannel.SelectedIndex = 0;
            comboBox1.SelectedIndex = 
            cbStereoExpandMethod.SelectedIndex=
            cbConvertBitType.SelectedIndex = 1;
            cbHSBoosterPreset.SelectedIndex = 4;

            for (int i = 0; i < bb.Length; i++) bb[i] = new HSDSP.BandPassFilter();

            string a = Application.StartupPath.Replace("/", "\\") + "\\Preset\\HSDSP";
            if (!System.IO.Directory.Exists(a)) System.IO.Directory.CreateDirectory(a);
            openFileDialog1.InitialDirectory = saveFileDialog1.InitialDirectory = a;
            새로고침ToolStripMenuItem.ToolTipText = PresetDir + "\n폴더에있는 프리셋의 목록을 새로고침 합니다.";
            toolStripComboBox1.ToolTipText = PresetDir + "\n폴더에있는 프리셋의 목록입니다.";

            새로고침ToolStripMenuItem_Click(null, null);

            this.Width -= this.Width - (groupBox4.Location.X + groupBox4.Width) - 5;
            dBBarInit();
#if DEBUG
#else
            dB바보이기ToolStripMenuItem.Checked = false;
            dB바보이기ToolStripMenuItem.Enabled = false;
#endif
        }
        HSAudioDSPHelper[] DSP = new HSAudioDSPHelper[5];


        bool On, Minus = true;
        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            DSP[0].Bypass = !checkBox1.Checked;
            if(AutoGainThread!=null)AutoGainThread.Abort();
            if (checkBox1.Checked) ThreadPool.QueueUserWorkItem(new WaitCallback(AutoGainThread_Loop));
            //if (AutoPeakThread != null) AutoPeakThread.Abort();
            //if (checkBox1.Checked) ThreadPool.QueueUserWorkItem(new WaitCallback(AutoPeakThread_Loop));
        }

        private void checkBox2_CheckedChanged(object sender, EventArgs e)
        {
            Minus =checkBox2.Checked;
        }
        public static float mag_sqrd(float re, float im) { return (re * re + im * im); }
        public static double mag_sqrd(double re, double im) { return (re * re + im * im); }
        public static double Decibels(float re, float im) { return ((re == 0 && im == 0) ? (0) : 10.0 * Math.Log10(mag_sqrd(re, im))); }
        public static double Decibels(double re, double im) { return ((re == 0 && im == 0) ? (0) : 10.0 * Math.Log10(mag_sqrd(re, im))); }

#region AGC
        double AGC; float AGC1;
        private void numericUpDown1_ValueChanged(object sender, EventArgs e)
        {
            AGC = (double)numericUpDown1.Value;
            AGC1 = (float)numericUpDown1.Value;
            MaxdB = (double)numericUpDown1.Value;
        }
        private void checkBox4_CheckedChanged(object sender, EventArgs e)
        {
            DSP[2].Bypass = chkGainControlBypass.Checked;
            if (chkGainControlEnable.Checked) DSP[2].ReRegisterDSP(chkGainControlMainOutput.Checked);
            else DSP[2].DisConnect();

            if (비트변환최우선ToolStripMenuItem.Checked &&
                chkConvertBit.Checked &&
                chkGainControlEnable.Checked) chkToInt_CheckedChanged(null, null);
        }

        private void numericUpDown5_ValueChanged(object sender, EventArgs e)
        {
            Gain = (float)numGainControlLevel.Value;
        }

        private void checkBox5_CheckedChanged(object sender, EventArgs e)
        {
            DSP[2].Bypass = chkGainControlBypass.Checked;
        }

        private void checkBox6_CheckedChanged(object sender, EventArgs e)
        {
            if (chkGainControlEnable.Checked) DSP[2].ReRegisterDSP(chkGainControlMainOutput.Checked);
        }
#endregion

#region HS™ 부스터
        private void chkBassBooster_CheckedChanged(object sender, EventArgs e)
        {
            dBBarHSBoosterEnable = !chkHSBoosterBypass.Checked && dB바보이기ToolStripMenuItem.Checked;

            DSP[1].Bypass = chkHSBoosterBypass.Checked;
            if (chkHSBooster.Checked) DSP[1].ReRegisterDSP(chkHSBoosterMainOutput.Checked);
            else DSP[1].DisConnect();

            if (비트변환최우선ToolStripMenuItem.Checked &&
                chkConvertBit.Checked &&
                chkHSBooster.Checked) chkToInt_CheckedChanged(null, null);
        }

        bool BassBooster_ValueChanged_Lock=false;
        private void BassBooster_ValueChanged(object sender, EventArgs e)
        {
            if (!BassBooster_ValueChanged_Lock) cbHSBoosterPreset.SelectedIndex = cbHSBoosterPreset.Items.Count - 1;

            //lpc.Clear();
            /*
            Hz = (float)numBassBoosterFrequency.Value;
            numBassBoosterWetMix.Minimum = -numBassBoosterFrequency.Value;
            numBassBoosterWetMix.Maximum = numBassBoosterFrequency.Value;
            Gain_Bass = (float)numBassBoosterRatio.Value;
            Ratio = (float)numBassBoosterWetMix.Value;*/
            if (chkHSBoosterAdvance.Checked) READCALLBACK_BassBoosterWetMix = (float)numHSBoosterWetMix.Value / 100;
            else READCALLBACK_BassBoosterMix = (float)numHSBoosterWetMix.Value / 100;

            READCALLBACK_BassBoosterFrequency = (float)numHSBoosterFrequency.Value;
            READCALLBACK_BassBoosterRatio = (float)numHSBoosterRatio.Value;
            READCALLBACK_BassBoosterDryMix = (float)numHSBoosterDryMix.Value/100;
        }
        private void cbBassBooster_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                ComboBox cb = sender as ComboBox;
                if (cb != null && cb.SelectedIndex == cbHSBoosterPreset.Items.Count - 1) return;
                BassBooster_ValueChanged_Lock = true;
                //chkBassBoosterStereo.Checked = false;
                switch (cbHSBoosterPreset.SelectedIndex)
                {
                    //베이스 1
                    case 0: numHSBoosterFrequency.Value = 95; numHSBoosterRatio.Value = 0.4m; numHSBoosterWetMix.Value = 40; chkHSBoosterAdvance.Checked = false; chkHSBoosterLow.Checked = true;break;
                    //약한 베이스 2 (전용), 50
                    case 1: numHSBoosterFrequency.Value = 90; numHSBoosterRatio.Value = 0.6m; numHSBoosterWetMix.Value = 60; numHSBoosterDryMix.Value = 60;chkHSBoosterAdvance.Checked= chkHSBoosterLow.Checked = true; break;
                    //약한 베이스 3 55
                    case 2: numHSBoosterFrequency.Value = 100; numHSBoosterRatio.Value = 0.3m; numHSBoosterWetMix.Value = 30; numHSBoosterDryMix.Value = 40; chkHSBoosterAdvance.Checked=chkHSBoosterLow.Checked = true; break;
                    //베이스 (75Hz), 45Hz
                    case 3: numHSBoosterFrequency.Value = 75; numHSBoosterRatio.Value = 0.3m; numHSBoosterWetMix.Value = 50; numHSBoosterDryMix.Value = 35;chkHSBoosterAdvance.Checked= chkHSBoosterLow.Checked = true; break;
                    //ClearAudio×Stereo
                    case 4: numHSBoosterFrequency.Value = 8000; numHSBoosterRatio.Value = 0.6m; numHSBoosterWetMix.Value = 80; numHSBoosterDryMix.Value = 80; chkHSBoosterAdvance.Checked = true; chkHSBoosterLow.Checked = false;chkHSBoosterMono.Checked = true; break;
                    //ClearAudio+ 1
                    case 5: numHSBoosterFrequency.Value = 8000; numHSBoosterRatio.Value = 0.6m; numHSBoosterWetMix.Value = 80; numHSBoosterDryMix.Value = 80; chkHSBoosterAdvance.Checked = true; chkHSBoosterLow.Checked = false; chkHSBoosterMono.Checked = false;break;
                    //case 5: numBassBoosterFrequency.Value = 50; numBassBoosterRatio.Value = 0.6m; numBassBoosterWetMix.Value = 40; break;
                    //값 테스트
                    case 6: if (chkHSBoosterAdvance.Checked) numHSBoosterDryMix.Value = 0; else numHSBoosterWetMix.Value = 100; break;
                    //효과만 끄기
                    case 7: numHSBoosterWetMix.Value = 0; break;
                }
            }
            finally{ BassBooster_ValueChanged_Lock = false; }
        }
        private void btnBassBoosterReset_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < bb.Length; i++) if(bb[i]!=null)bb[i].Clear();
        }
        private void chkBassBoosterMainOutput_CheckedChanged(object sender, EventArgs e)
        {
            if (chkHSBooster.Checked) DSP[1].ReRegisterDSP(chkHSBoosterMainOutput.Checked);
        }
        private void chkBassBoosterBypass_CheckedChanged(object sender, EventArgs e)
        {
            dBBarHSBoosterEnable = chkHSBoosterBypass.Checked && !chkHSBoosterBypass.Checked && dB바보이기ToolStripMenuItem.Checked;

            DSP[1].Bypass = chkHSBoosterBypass.Checked;
        }
        private void chkBassBoosterLow_CheckedChanged(object sender, EventArgs e)
        {
            for (int i = 0; i < bb.Length; i++) if (bb[i] != null) bb[i].Clear();
            READCALLBACK_BassBoosterLow = chkHSBoosterLow.Checked;
        }
        private void chkBassBoosterMono_CheckedChanged(object sender, EventArgs e)
        {
            //if (!BassBooster_ValueChanged_Lock) cbBassBooster.SelectedIndex = cbBassBooster.Items.Count - 1;
            READCALLBACK_BassBoosterMono = chkHSBoosterMono.Checked;
        }
#endregion

#region 스테레오 확장
        private void chkStereoExpand_CheckedChanged(object sender, EventArgs e)
        {
            dBBarStereoExpandEnable = chkStereoExpand.Checked && !chkStereoExpandBypass.Checked && dB바보이기ToolStripMenuItem.Checked;

            DSP[3].Bypass = chkStereoExpandBypass.Checked;
            if (chkStereoExpand.Checked) DSP[3].ReRegisterDSP(chkStereoExpandOutput.Checked);
            else DSP[3].DisConnect();

            if (비트변환최우선ToolStripMenuItem.Checked && 
                chkConvertBit.Checked &&
                chkStereoExpand.Checked) chkToInt_CheckedChanged(null, null);
        }

        private void chkStereoExpandBypass_CheckedChanged(object sender, EventArgs e)
        {
            dBBarStereoExpandEnable = chkStereoExpand.Checked && !chkStereoExpandBypass.Checked && dB바보이기ToolStripMenuItem.Checked;

            DSP[3].Bypass = chkStereoExpandBypass.Checked;
        }

        private void chkStereoExpandOutput_CheckedChanged(object sender, EventArgs e)
        {
            if (chkStereoExpand.Checked) DSP[3].ReRegisterDSP(chkStereoExpandOutput.Checked);
        }
        
        bool StereoExpand_ValueChanged_Lock = false;
        private void chkStereoExpandSideValue_ValueChanged(object sender, EventArgs e)
        {
            if(!StereoExpand_ValueChanged_Lock)comboBox1.SelectedIndex = comboBox1.Items.Count-1;
            float Value = (float)numStereoExpandSideValue.Value / 100.0f;
            float ApplyValue = 0.5f + (Value / 2);
            if (chkStereoExpandGainAuto.Checked) READCALLBACK_StereoExpandGainValue = (float)(numStereoExpandSideValue.Value * StereoExpandAutoGain);
            if (chkStereoExpandSideSame.Checked) READCALLBACK_StereoExpandSideValue = READCALLBACK_StereoExpandMidValue = ApplyValue;
            else READCALLBACK_StereoExpandSideValue = ApplyValue;
        }
        private void numStereoExpandMidValue_ValueChanged(object sender, EventArgs e)
        {
            if (!StereoExpand_ValueChanged_Lock) comboBox1.SelectedIndex = comboBox1.Items.Count - 1;
            float Value = (float)numStereoExpandMidValue.Value / 100.0f;
            float ApplyValue = 0.5f + (Value / 2);
            READCALLBACK_StereoExpandMidValue = ApplyValue;
        }
        private void checkBox7_CheckedChanged(object sender, EventArgs e)
        {
            if (!StereoExpand_ValueChanged_Lock) comboBox1.SelectedIndex = comboBox1.Items.Count - 1;
            if (chkStereoExpandSideSame.Checked)
            {
                numStereoExpandMidValue.Enabled = false;

                float Value = (float)numStereoExpandSideValue.Value / 100.0f;
                float ApplyValue = 0.5f + (Value / 2);

                READCALLBACK_StereoExpandSideValue = READCALLBACK_StereoExpandMidValue = ApplyValue;
            }
            else
            {
                numStereoExpandMidValue.Enabled = true;

                float Value = (float)numStereoExpandSideValue.Value / 100.0f;
                float ApplyValue = 0.5f + (Value / 2);
                READCALLBACK_StereoExpandSideValue = ApplyValue;

                Value = (float)numStereoExpandMidValue.Value / 100.0f;
                ApplyValue = 0.5f + (Value / 2);
                READCALLBACK_StereoExpandMidValue = ApplyValue;
            }
        }
        private void numStereoExpandGainValue_ValueChanged(object sender, EventArgs e)
        {
            if (!StereoExpand_ValueChanged_Lock) comboBox1.SelectedIndex = comboBox1.Items.Count - 1;
            READCALLBACK_StereoExpandGainValue = (float)numStereoExpandGainValue.Value;
        }
        private void numStereoExpandWetMix_ValueChanged(object sender, EventArgs e)
        {
            if (!StereoExpand_ValueChanged_Lock) comboBox1.SelectedIndex = comboBox1.Items.Count - 1;
            if (chkStereoExpandAdvance.Checked)READCALLBACK_StereoExpandWetMix = (float)numStereoExpandWetMix.Value/100f;
            else READCALLBACK_StereoExpandMix = (float)numStereoExpandWetMix.Value / 100f;
        }
        private void numStereoExpandDryMix_ValueChanged(object sender, EventArgs e){READCALLBACK_StereoExpandDryMix = (float)numStereoExpandDryMix.Value / 100f; }
        private void cbStereoExpandMethod_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!StereoExpand_ValueChanged_Lock) comboBox1.SelectedIndex = comboBox1.Items.Count - 1;
            switch (cbStereoExpandMethod.SelectedIndex)
            {
				/*
                case 0: READCALLBACK_StereoExpandMethod = 1f;
                        StereoExpandAutoGainValue = 0;
                        READCALLBACK_StereoExpandGainValue = 0;
                        numStereoExpandGainValue.Enabled = chkStereoExpandGainAuto.Enabled = label9.Enabled = label8.Enabled = false;
                        break;*/
						
                case 0: READCALLBACK_StereoExpandMethod = 1.414213562373095F;
                        StereoExpandAutoGainValue = 3f;
                        chkStereoExpandGainAuto.Enabled = label9.Enabled = label8.Enabled = true;
                        checkBox8_CheckedChanged(null, null);
                        break;

                default: READCALLBACK_StereoExpandMethod = 2f;
                         StereoExpandAutoGainValue = 0;
                         READCALLBACK_StereoExpandGainValue = 0;
                         numStereoExpandGainValue.Enabled = chkStereoExpandGainAuto.Enabled = label9.Enabled = label8.Enabled = false; 
                         break;
            }
        }
        private void checkBox8_CheckedChanged(object sender, EventArgs e)
        {
            numStereoExpandGainValue.Enabled =label8.Enabled=label9.Enabled= !chkStereoExpandGainAuto.Checked;
            if (chkStereoExpandGainAuto.Checked)
            {
                READCALLBACK_StereoExpandGainValue = (float)(numStereoExpandSideValue.Value * StereoExpandAutoGain);
                StereoExpandAutoGainValue = 5.5f;
            }
            else
            {
                READCALLBACK_StereoExpandGainValue = (float)numStereoExpandGainValue.Value;
                StereoExpandAutoGainValue = 5.5f;
            }
        }
        private void chkStereoExpandAdvance_CheckedChanged(object sender, EventArgs e)
        {
            if (!StereoExpand_ValueChanged_Lock) comboBox1.SelectedIndex = comboBox1.Items.Count - 1;
            if (chkStereoExpandAdvance.Checked)
            {
                READCALLBACK_StereoExpandAdvancd = true;
                label13.Text = "Wet";
                numStereoExpandWetMix.Value = (decimal)READCALLBACK_StereoExpandWetMix*100;
                numStereoExpandDryMix.Enabled = label18.Enabled = label16.Enabled = true;
            }
            else
            {
                READCALLBACK_StereoExpandAdvancd = false;
                label13.Text = "Mix";
                numStereoExpandWetMix.Value = (decimal)READCALLBACK_StereoExpandMix*100;
                numStereoExpandDryMix.Enabled = label18.Enabled = label16.Enabled = false;
            }
        }
        
        bool comboBox1_SelectedIndexChanged_First = true;
        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                ComboBox cb = sender as ComboBox;
                if(cb!=null&&cb.SelectedIndex == comboBox1.Items.Count-1)return;
                StereoExpand_ValueChanged_Lock = true;
                chkStereoExpandAdvance.Checked = false;
                //else comboBox1_SelectedIndexChanged_First = false;
                numStereoExpandWetMix.Value = 100;
                cbStereoExpandMethod.SelectedIndex = 0;
                switch (comboBox1.SelectedIndex)
                {
                    case 0: numStereoExpandSideValue.Value = 30; numStereoExpandMidValue.Value = 30; break;
                    case 1: chkStereoExpandSideSame.Checked = false; numStereoExpandSideValue.Value = 100; numStereoExpandMidValue.Value = 0; break;
                    case 2: chkStereoExpandSideSame.Checked = false; numStereoExpandSideValue.Value = 100; numStereoExpandMidValue.Value = 10; break;
                    case 3: chkStereoExpandSideSame.Checked = false; numStereoExpandSideValue.Value = 0; numStereoExpandMidValue.Value = 30; break;
                    case 4: chkStereoExpandSideSame.Checked = false; numStereoExpandSideValue.Value = 100; numStereoExpandMidValue.Value = 30; break;
                    case 5: chkStereoExpandSideSame.Checked = false; chkStereoExpandAdvance.Checked = true; numStereoExpandSideValue.Value = 90; numStereoExpandMidValue.Value = 0; numStereoExpandWetMix.Value = 70; numStereoExpandDryMix.Value = 20; cbStereoExpandMethod.SelectedIndex = 1; break;
                    case 6: numStereoExpandSideValue.Value = 0; numStereoExpandMidValue.Value = 0; break;
                }
            }finally{StereoExpand_ValueChanged_Lock = false;}
        }
#endregion

#region 모노 다운믹스
        private void chkMonoDownMix_CheckedChanged(object sender, EventArgs e)
        {
            DSP[4].Bypass = chkMonoDownMixBypass.Checked;
            if (chkMonoDownMix.Checked) DSP[4].ReRegisterDSP(chkMonoDownMixMainOutput.Checked);
            else DSP[4].DisConnect();

            if (비트변환최우선ToolStripMenuItem.Checked &&
                chkConvertBit.Checked &&
                chkMonoDownMix.Checked) chkToInt_CheckedChanged(null, null);
        }

        private void chkMonoDownMixBypass_CheckedChanged(object sender, EventArgs e)
        {
            DSP[4].Bypass = chkMonoDownMixBypass.Checked;
        }

        private void chkMonoDownMixMainOutput_CheckedChanged(object sender, EventArgs e)
        {
            if (chkMonoDownMix.Checked) DSP[4].ReRegisterDSP(chkMonoDownMixMainOutput.Checked);
        }

        private void numMonoDownMixMIX_ValueChanged(object sender, EventArgs e)
        {
            Mix_Mono = (float)numMonoDownMixMIX.Value / 100f;
        }

        private void cbMonoDownMix_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (cbMonoDownMix.SelectedIndex)
            {
                case 0: numMonoDownMixMIX.Value = 100; break;
                case 1: numMonoDownMixMIX.Value = 90; break;
                case 2: numMonoDownMixMIX.Value = 80; break;
                case 3: numMonoDownMixMIX.Value = 70; break;
                case 4: numMonoDownMixMIX.Value = 60; break;
                case 5: numMonoDownMixMIX.Value = 50; break;
                case 6: numMonoDownMixMIX.Value = 40; break;
                case 7: numMonoDownMixMIX.Value = 30; break;
                case 8: numMonoDownMixMIX.Value = 20; break;
                case 9: numMonoDownMixMIX.Value = 10; break;
                default: numMonoDownMixMIX.Value = 0; break;
            }
        }
#endregion

#region 사운드 반전
        private void chkSoundReverse_CheckedChanged(object sender, EventArgs e)
        {
            DSP[5].Bypass = chkSoundReverseBypass.Checked;
            if (chkSoundReverse.Checked) DSP[5].ReRegisterDSP(chkSoundReverseMainOutput.Checked);
            else DSP[5].DisConnect();

            if (chkSoundReverse.Checked &&
                chkConvertBit.Checked &&
                chkGainControlEnable.Checked) chkToInt_CheckedChanged(null, null);
        }

        private void chkSoundReverseBypass_CheckedChanged(object sender, EventArgs e)
        {
            DSP[5].Bypass = chkSoundReverseBypass.Checked;
        }

        private void chkSoundReverseMainOutput_CheckedChanged(object sender, EventArgs e)
        {
            if (chkSoundReverse.Checked) DSP[5].ReRegisterDSP(chkSoundReverseMainOutput.Checked);
        }

        private void chkBassBoosterAdvance_CheckedChanged(object sender, EventArgs e)
        {
            if (!BassBooster_ValueChanged_Lock) cbHSBoosterPreset.SelectedIndex = cbHSBoosterPreset.Items.Count - 1;
            if (chkHSBoosterAdvance.Checked)
            {
                READCALLBACK_BassBoosterAdvanced = true;
                numHSBoosterWetMix.Value = (decimal)READCALLBACK_BassBoosterWetMix * 100;
                label19.Text = "Wet";
                numHSBoosterDryMix.Enabled = label21.Enabled = label22.Enabled = true;
            }
            else
            {
                READCALLBACK_BassBoosterAdvanced = false;
                numHSBoosterWetMix.Value = (decimal)READCALLBACK_BassBoosterMix * 100;
                label19.Text = "Mix";
                numHSBoosterDryMix.Enabled = label21.Enabled = label22.Enabled = false;
            }
        }

        private void cbSoundReversChannels_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (cbSoundReversChannels.SelectedIndex)
            {
                case 1: SoundReverseChannel = Channels.L; break;
                case 2: SoundReverseChannel = Channels.R; break;
                default: SoundReverseChannel = Channels.Both; break;
            }
        }

        private void cbGainControlChannel_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (cbGainControlChannel.SelectedIndex)
            {
                case 1: GainControlChannel = Channels.L; break;
                case 2: GainControlChannel = Channels.R; break;
                default: GainControlChannel = Channels.Both; break;
            }
        }
#endregion

#region 정수변환
        private void chkToInt_CheckedChanged(object sender, EventArgs e)
        {
            DSP[6].Bypass = chkConvertBitBypass.Checked;
            if (chkConvertBit.Checked) DSP[6].ReRegisterDSP(chkConvertBitMainOutput.Checked);
            else DSP[6].DisConnect();
        }

        private void cbToInt_SelectedIndexChanged(object sender, EventArgs e)
        {
            toolTip1.Hide(cbConvertBitType);
            //chkToIntDistoration.Enabled = false;
            switch (cbConvertBitType.SelectedIndex)
            {
                case 0: HowFloat2Int = Float2Int.Int; toolTip1.Show(string.Format("{0} ~ {1}", int.MinValue, int.MaxValue), cbConvertBitType, 3000); break;
                case 1: HowFloat2Int = Float2Int.Int_24; toolTip1.Show(string.Format("{0} ~ {1}", Int24.MinValue, Int24.MaxValue), cbConvertBitType, 3000); break;
                case 2: HowFloat2Int = Float2Int.Short; toolTip1.Show(string.Format("{0} ~ {1}", short.MinValue, short.MaxValue), cbConvertBitType, 2000); break;
                case 3: HowFloat2Int = Float2Int.Bit12; toolTip1.Show("-2048 ~ 2047", cbConvertBitType, 1500); break;
                case 4: HowFloat2Int = Float2Int.Byte; toolTip1.Show(string.Format("{0}~{1}", sbyte.MinValue, sbyte.MaxValue), cbConvertBitType, 1000); break;
                case 5: HowFloat2Int = Float2Int.Bit7; toolTip1.Show("-64 ~ 63", cbConvertBitType, 500); break;
                case 6: HowFloat2Int = Float2Int.Bit6; toolTip1.Show("-32 ~ 31", cbConvertBitType, 500); break;
                case 7: HowFloat2Int = Float2Int.Bit5; toolTip1.Show("-16 ~ 15", cbConvertBitType, 500); break;
                case 8: HowFloat2Int = Float2Int.Bit4; toolTip1.Show("-8 ~ 7", cbConvertBitType, 500); break;
                case 9: HowFloat2Int = Float2Int.Bit3; toolTip1.Show("-4 ~ 3", cbConvertBitType, 500); break;
                case 10: HowFloat2Int = Float2Int.Byte_Ex; toolTip1.Show(string.Format("{0}~{1}", sbyte.MinValue, sbyte.MaxValue), cbConvertBitType, 1000); break;
                default: HowFloat2Int = Float2Int.Bypass; toolTip1.Show(string.Format("{0}~{1}", float.MinValue, float.MaxValue), cbConvertBitType, 3000); break;
            }
        }

        private void chkToIntMainOutput_CheckedChanged(object sender, EventArgs e)
        {
            if (chkConvertBitMainOutput.Checked) DSP[6].ReRegisterDSP(chkConvertBitMainOutput.Checked);
        }

        private void chkToIntBypass_CheckedChanged(object sender, EventArgs e)
        {
            DSP[6].Bypass = chkConvertBitBypass.Checked;
        }

        private void chkToIntDistoration_CheckedChanged(object sender, EventArgs e)
        {
            ToIntDistoration = chkConvertBitDistoration.Checked;
        }
   
        private void chkToIntEx_CheckedChanged(object sender, EventArgs e)
        {
            ToIntEx = chkConvertBitEx.Checked;
        }
        #endregion

        #region 프리셋 관련
        public Dictionary<string, string> DefaultPreset()
        {
            Dictionary<string, string> Settings = new Dictionary<string, string>();
            Settings.Add("[GainControl]", null);
            Settings.Add("GainControlEnable", false.ToString());
            Settings.Add("//0=모두, 1=좌, 2=우", null);
            Settings.Add("GainControlLevel", 0.ToString());
            Settings.Add("GainControlChannel", 0.ToString());
            Settings.Add("GainControlBypass", false.ToString());
            Settings.Add("GainControlMainOutput", true.ToString());

            Settings.Add("\r\n[MonoDownMix]", null);
            Settings.Add("MonoDownMixEnable", false.ToString());
            Settings.Add("MonoDownMixLevel", 100.ToString());
            Settings.Add("MonoDownMixBypass", false.ToString());
            Settings.Add("MonoDownMixMainOutput", true.ToString());

            Settings.Add("\r\n[SoundReverse]", null);
            Settings.Add("SoundReverseEnable", false.ToString());
            Settings.Add("//0=모두, 1=좌, 2=우 ", null);
            Settings.Add("SoundReversChannels", 0.ToString());
            Settings.Add("SoundReverseBypass", false.ToString());
            Settings.Add("SoundReverseMainOutput", true.ToString());

            Settings.Add("\r\n[StereoExpand]", null);
            Settings.Add("StereoExpandEnable", false.ToString());
            Settings.Add("StereoExpandBypass", chkStereoExpandBypass.ToString());
            Settings.Add("StereoExpandMainOutput", chkStereoExpandOutput.ToString());

            Settings.Add("\r\n[ConvertBit]", null);
            Settings.Add("ConvertBitEnable", false.ToString());
            Settings.Add("//0=32비트 (정수), 1=24비트, 2=16비트, 3=12비트, 4=8비트, 5=7비트, 6=6비트, 7=5비트, 8=4비트, 9=3비트", null);
            Settings.Add("ConvertBitType", 1.ToString());
            Settings.Add("ConvertBitDistoration", false.ToString());
            Settings.Add("ConvertBitEx", false.ToString());
            Settings.Add("ConvertBitBypass", false.ToString());
            Settings.Add("ConvertBitMainOutput", true.ToString());

            Settings.Add("\r\n[HSBooster]", null);
            Settings.Add("HSBoosterEnable", false.ToString());
            Settings.Add("HSBoosterBypass", false.ToString());
            Settings.Add("HSBoosterMainOutput", true.ToString());
            return Settings;
        }
        public Dictionary<string, string> HSDSPPreset
        {
            get
            {
                Dictionary<string, string> Settings = new Dictionary<string, string>();
                Settings.Add("[GainControl]", null);
                Settings.Add("GainControlEnable", chkGainControlEnable.Checked.ToString());
                Settings.Add("GainControlLevel", numGainControlLevel.Value.ToString());
                Settings.Add("//0=모두, 1=좌, 2=우", null);
                Settings.Add("GainControlChannel", cbGainControlChannel.ToString());
                Settings.Add("GainControlBypass", chkGainControlBypass.Checked.ToString());
                Settings.Add("GainControlMainOutput", chkGainControlMainOutput.Checked.ToString());

                Settings.Add("\r\n[MonoDownMix]", null);
                Settings.Add("MonoDownMixEnable", chkMonoDownMix.Checked.ToString());
                Settings.Add("MonoDownMixLevel", numMonoDownMixMIX.Value.ToString());
                Settings.Add("MonoDownMixBypass", chkMonoDownMixBypass.Checked.ToString());
                Settings.Add("MonoDownMixMainOutput", chkMonoDownMixMainOutput.Checked.ToString());

                Settings.Add("\r\n[SoundReverse]", null);
                Settings.Add("SoundReverseEnable", chkSoundReverse.Checked.ToString());
                Settings.Add("//0=모두, 1=좌, 2=우 ", null);
                Settings.Add("SoundReversChannels", cbSoundReversChannels.SelectedIndex.ToString());
                Settings.Add("SoundReverseBypass", chkSoundReverseBypass.Checked.ToString());
                Settings.Add("SoundReverseMainOutput", chkSoundReverseMainOutput.Checked.ToString());

                Settings.Add("\r\n[StereoExpand]", null);
                Settings.Add("StereoExpandEnable", chkStereoExpand.Checked.ToString());
                Settings.Add("StereoExpandSideValue", numStereoExpandSideValue.Value.ToString());
                Settings.Add("StereoExpandMidValue", numStereoExpandMidValue.Value.ToString());
                Settings.Add("StereoExpandSideSame", chkStereoExpandSideSame.Checked.ToString());
                Settings.Add("StereoExpandMix", (READCALLBACK_StereoExpandMix * 100.0f).ToString());
                Settings.Add("StereoExpandWetMix", (READCALLBACK_StereoExpandWetMix * 100.0f).ToString());
                Settings.Add("StereoExpandDryMix", numStereoExpandDryMix.Value.ToString());
                Settings.Add("StereoExpandAdvance", chkStereoExpandAdvance.Checked.ToString());
                Settings.Add("//0=√2 (1.4142135... ), 1=N (채널수)", null);
                Settings.Add("StereoExpandMethod", cbStereoExpandMethod.SelectedIndex.ToString());
                Settings.Add("StereoExpandGainValue", numStereoExpandGainValue.Value.ToString());
                Settings.Add("StereoExpandGainAuto", chkStereoExpandGainAuto.Checked.ToString());
                Settings.Add("StereoExpandBypass", chkStereoExpandBypass.Checked.ToString());
                Settings.Add("StereoExpandMainOutput", chkStereoExpandOutput.Checked.ToString());

                Settings.Add("\r\n[ConvertBit]", null);
                Settings.Add("ConvertBitEnable", chkConvertBit.Checked.ToString());
                Settings.Add("//0=32비트 (정수), 1=24비트, 2=16비트, 3=12비트, 4=8비트, 5=7비트, 6=6비트, 7=5비트, 8=4비트, 9=3비트", null);
                Settings.Add("ConvertBitType", cbConvertBitType.SelectedIndex.ToString());
                Settings.Add("ConvertBitDistoration", chkConvertBitDistoration.Checked.ToString());
                Settings.Add("ConvertBitEx", chkConvertBitEx.Checked.ToString());
                Settings.Add("ConvertBitBypass", chkConvertBitBypass.Checked.ToString());
                Settings.Add("ConvertBitMainOutput", chkConvertBitMainOutput.Checked.ToString());

                Settings.Add("\r\n[HSBooster]", null);
                Settings.Add("HSBoosterEnable", chkHSBooster.Checked.ToString());
                Settings.Add("HSBoosterLow", chkHSBoosterLow.Checked.ToString());
                Settings.Add("HSBoosterFrequency", numHSBoosterFrequency.Value.ToString());
                Settings.Add("HSBoosterRatio", numHSBoosterRatio.Value.ToString());
                Settings.Add("HSBoosterMono", chkHSBoosterMono.Checked.ToString());
                Settings.Add("HSBoosterMix", (READCALLBACK_BassBoosterMix * 100.0f).ToString());
                Settings.Add("HSBoosterWetMix", (READCALLBACK_BassBoosterWetMix * 100.0f).ToString());
                Settings.Add("HSBoosterDryMix", numHSBoosterDryMix.Value.ToString());
                Settings.Add("HSBoosterAdvance", chkHSBoosterAdvance.Checked.ToString());
                Settings.Add("HSBoosterBypass", chkHSBoosterBypass.Checked.ToString());
                Settings.Add("HSBoosterMainOutput", chkHSBoosterMainOutput.Checked.ToString());
                return Settings;
            }
            set
            {
                /*
                List<System.Windows.Forms.Control> c = new List<System.Windows.Forms.Control>();
                for (int i = 0; i < panel2.Controls.Count; i++)
                {
                    c.Add(panel2.Controls[i]);
                }
                */
                //foreach (KeyvaluePair<string, string> s in value) { Settings[s.Key] = s.Value; }

                try { chkGainControlEnable.Checked = bool.Parse(value["GainControlEnable"]); } catch { }
                try { numGainControlLevel.Value = Convert.ToDecimal(value["GainControlLevel"]); } catch { }
                try { cbGainControlChannel.SelectedIndex = Convert.ToInt16(value["GainControlChannel"]); } catch { }
                try { chkGainControlBypass.Checked = bool.Parse(value["GainControlBypass"]); } catch { }
                try { chkGainControlMainOutput.Checked = bool.Parse(value["GainControlMainOutput"]); } catch { }

                try { chkMonoDownMix.Checked = bool.Parse(value["MonoDownMixEnable"]); } catch { }
                try { numMonoDownMixMIX.Value = Convert.ToDecimal(value["MonoDownMixLevel"]); } catch { }
                try { chkMonoDownMixBypass.Checked = bool.Parse(value["MonoDownMixBypass"]); } catch { }
                try { chkMonoDownMixMainOutput.Checked = bool.Parse(value["MonoDownMixMainOutput"]); } catch { }

                try { chkSoundReverse.Checked = bool.Parse(value["SoundReverseEnable"]); } catch { }
                try { cbSoundReversChannels.SelectedIndex = Convert.ToInt16(value["SoundReversChannels"]); } catch { }
                try { chkSoundReverseBypass.Checked = bool.Parse(value["SoundReverseBypass"]); } catch { }
                try { chkSoundReverseMainOutput.Checked = bool.Parse(value["SoundReverseMainOutput"]); } catch { }

                try { chkStereoExpand.Checked = bool.Parse(value["StereoExpandEnable"]); } catch { }
                try { numStereoExpandSideValue.Value = Convert.ToDecimal(value["StereoExpandSideValue"]); } catch { }
                try { numStereoExpandMidValue.Value = Convert.ToDecimal(value["StereoExpandMidValue"]); } catch { }
                try { chkStereoExpandSideSame.Checked = bool.Parse(value["StereoExpandSideSame"]); } catch { }
                //try{numStereoExpandWetMix.Value = Convert.ToDecimal(value["StereoExpandWetMix"]);}catch{}            
                try { numStereoExpandWetMix.Value = chkStereoExpandAdvance.Checked ? Convert.ToDecimal(value["StereoExpandWetMix"]) : Convert.ToDecimal(value["StereoExpandMix"]); } catch { }
                try { numStereoExpandDryMix.Value = Convert.ToDecimal(value["StereoExpandDryMix"]); } catch { }
                try { chkStereoExpandAdvance.Checked = bool.Parse(value["StereoExpandAdvance"]); } catch { }
                try { READCALLBACK_StereoExpandMix = Convert.ToSingle(value["StereoExpandMix"]) / 100.0f; } catch { }
                try { READCALLBACK_StereoExpandWetMix = Convert.ToSingle(value["StereoExpandWetMix"]) / 100.0f; } catch { }
                try { numStereoExpandDryMix.Value = Convert.ToDecimal(value["StereoExpandDryMix"]); } catch { }
                try { numStereoExpandGainValue.Value = Convert.ToDecimal(value["StereoExpandGainValue"]); } catch { }
                try { chkStereoExpandGainAuto.Checked = bool.Parse(value["StereoExpandGainAuto"]); } catch { }
                try { cbStereoExpandMethod.SelectedIndex = Convert.ToInt16(value["StereoExpandMethod"]); cbStereoExpandMethod_SelectedIndexChanged(null, null); } catch { }
                try { chkStereoExpandBypass.Checked = bool.Parse(value["StereoExpandBypass"]); } catch { }
                try { chkStereoExpandOutput.Checked = bool.Parse(value["StereoExpandMainOutput"]); } catch { }

                try { chkConvertBit.Checked = bool.Parse(value["ConvertBitEnable"]); } catch { }
                try { cbConvertBitType.SelectedIndex = Convert.ToInt16(value["ConvertBitType"]); } catch { }
                try { chkConvertBitDistoration.Checked = bool.Parse(value["ConvertBitDistoration"]); } catch { }
                try { chkConvertBitEx.Checked = bool.Parse(value["ConvertBitEx"]); } catch { }
                try { chkConvertBitBypass.Checked = bool.Parse(value["ConvertBitBypass"]); } catch { }
                try { chkConvertBitMainOutput.Checked = bool.Parse(value["ConvertBitMainOutput"]); } catch { }

                try { chkHSBooster.Checked = bool.Parse(value["HSBoosterEnable"]); } catch { }
                try { chkHSBoosterLow.Checked = bool.Parse(value["HSBoosterLow"]); } catch { }
                try { numHSBoosterFrequency.Value = Convert.ToDecimal(value["HSBoosterFrequency"]); } catch { }
                try { numHSBoosterRatio.Value = Convert.ToDecimal(value["HSBoosterRatio"]); } catch { }
                try { chkHSBoosterMono.Checked = bool.Parse(value["HSBoosterMono"]); } catch { }
                try { numHSBoosterWetMix.Value = chkHSBoosterAdvance.Checked ? Convert.ToDecimal(value["HSBoosterWetMix"]) : Convert.ToDecimal(value["HSBoosterMix"]); } catch { }
                try { numHSBoosterDryMix.Value = Convert.ToDecimal(value["HSBoosterDryMix"]); } catch { }
                try { chkHSBoosterAdvance.Checked = bool.Parse(value["HSBoosterAdvance"]); } catch { }
                try { READCALLBACK_BassBoosterMix = Convert.ToSingle(value["HSBoosterMix"]) / 100.0f; } catch { }
                try { READCALLBACK_BassBoosterWetMix = Convert.ToSingle(value["HSBoosterWetMix"]) / 100.0f; } catch { }
                try { chkHSBoosterBypass.Checked = bool.Parse(value["HSBoosterBypass"]); } catch { }
                try { chkHSBoosterMainOutput.Checked = bool.Parse(value["HSBoosterMainOutput"]); } catch { }
                //Helper.UpdateMix();
            }
        }

        public string PresetDir
        {
            get
            {
                return Application.StartupPath.Replace("/", "\\") + "\\Preset\\HSDSP";
            }
        }
        string LastPreset = "(기본값)";

        Dictionary<string, string> PresetPath = new Dictionary<string, string>();
        System.Collections.Generic.List<Dictionary<string, string>> Presets = new List<Dictionary<string, string>>();
        private void 프리셋저장ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!System.IO.Directory.Exists(PresetDir)) System.IO.Directory.CreateDirectory(PresetDir);
            saveFileDialog1.InitialDirectory = Directory.Exists(LastFileSaveDirectory) ? LastFileSaveDirectory : PresetDir;

            saveFileDialog1.FileName = toolStripComboBox1.SelectedIndex == 0 ? "새 프리셋" : toolStripComboBox1.Text;
            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                LastFileSaveDirectory = Path.GetDirectoryName(saveFileDialog1.FileName);
                string[] a1 = HS_CSharpUtility.Utility.EtcUtility.SaveSettingWithoutFilter(HSDSPPreset);
                //string b =CSharpUtility.Utility.StringUtility.ConvertArrayToString(a, true);
                System.IO.File.WriteAllLines(saveFileDialog1.FileName, a1);
            }
        }

        private void 프리셋열기ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!System.IO.Directory.Exists(PresetDir)) { System.IO.Directory.CreateDirectory(PresetDir); }
            //LastFileDirectory
            openFileDialog1.InitialDirectory = Directory.Exists(LastFileOpenDirectory) ? LastFileOpenDirectory : PresetDir;

            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                string[] a1 = System.IO.File.ReadAllLines(openFileDialog1.FileName);
                HSDSPPreset = HS_CSharpUtility.Utility.EtcUtility.LoadSetting(a1);
                toolStripComboBox1.Text = toolStripComboBox1.Items[toolStripComboBox1.Items.Count - 1].ToString();
            }
        }

        private void 폴더선택ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!System.IO.Directory.Exists(PresetDir)) { System.IO.Directory.CreateDirectory(PresetDir); }
            folderBrowserDialog1.SelectedPath = Directory.Exists(LastFolderDirectory) ? LastFolderDirectory : PresetDir;
            if (DialogResult.OK == folderBrowserDialog1.ShowDialog())
            {
                LastFolderDirectory = folderBrowserDialog1.SelectedPath;
            }
        }

        private void 새로고침ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Presets.Clear(); toolStripComboBox1.Items.Clear();
            Presets.Add(DefaultPreset()); toolStripComboBox1.Items.Add("(기본값)");
            foreach (string a in Directory.GetFiles(LastFolderDirectory, "*.hsdsp"))
            {
                try
                {
                    Dictionary<string, string> b =
                        HS_CSharpUtility.Utility.EtcUtility.LoadSetting(File.ReadAllLines(a));
                    Presets.Add(b);
                    toolStripComboBox1.Items.Add(Path.GetFileNameWithoutExtension(a));
                }
                catch { }
            }
            if (toolStripComboBox1.Items.IndexOf(LastPreset) != -1) toolStripComboBox1.SelectedIndex = toolStripComboBox1.Items.IndexOf(LastPreset);
            else toolStripComboBox1.SelectedIndex = 0;
        }

        private void 제거ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (toolStripComboBox1.Items.Count > 1 && toolStripComboBox1.SelectedIndex != 0)
            {
                if (DialogResult.Yes == MessageBox.Show("정말 선택된 프리셋을 목록에서 제거 하시겠습니까?", "DSP 프리셋 선택된 목록 제거", MessageBoxButtons.YesNo, MessageBoxIcon.Warning))
                {
                    Presets.RemoveAt(toolStripComboBox1.SelectedIndex); toolStripComboBox1.Items.RemoveAt(toolStripComboBox1.SelectedIndex); toolStripComboBox1.SelectedIndex = 0;
                }
            }
        }

        private void toolStripComboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (toolStripComboBox1.SelectedIndex != -1)
            {
                if (toolStripComboBox1.SelectedIndex == 0)
                {
                    HSDSPPreset = DefaultPreset();
                    comboBox1.SelectedIndex = 1;
                    cbHSBoosterPreset.SelectedIndex = 4;
                    cbMonoDownMix.SelectedIndex = 0;
                    READCALLBACK_StereoExpandWetMix = 1;
                    READCALLBACK_StereoExpandDryMix = 0;
                    READCALLBACK_BassBoosterMix = 0.5f;
                }
                else HSDSPPreset = Presets[toolStripComboBox1.SelectedIndex];
                LastPreset = toolStripComboBox1.Items[toolStripComboBox1.SelectedIndex].ToString();
                제거ToolStripMenuItem.Enabled = !(toolStripComboBox1.SelectedIndex == 0);
            }
        }

        string LastFileOpenDirectory
        {
            get
            {
                if (File.Exists(PresetDir + "\\LastHSDSPPath.ini"))
                {
                    string[] a = File.ReadAllLines(PresetDir + "\\LastHSDSPPath.ini");
                    string a1 = HS_CSharpUtility.Utility.EtcUtility.LoadSetting(a)["LastFileOpenDirectory"];
                    if (a1 != null || a1 != "") { return Directory.Exists(a1) ? a1 : PresetDir; }
                    else { return PresetDir; }
                }
                else { return PresetDir; }
            }
            set
            {
                if (PresetPath.Count < 3)
                {
                    PresetPath.Clear(); PresetPath.Add("LastFileOpenDirectory", value);
                    PresetPath.Add("LastFileSaveDirectory", PresetDir);
                    PresetPath.Add("LastFolderDirectory", PresetDir);
                }
                else { PresetPath["LastFileOpenDirectory"] = value; }
                File.WriteAllLines(PresetDir + "\\LastHSDSPPath.ini", HS_CSharpUtility.Utility.EtcUtility.SaveSetting(PresetPath));
            }
        }
        string LastFileSaveDirectory
        {
            get
            {
                if (File.Exists(PresetDir + "\\LastHSDSPPath.ini"))
                {
                    string[] a = File.ReadAllLines(PresetDir + "\\LastHSDSPPath.ini");
                    string a1 = HS_CSharpUtility.Utility.EtcUtility.LoadSetting(a)["LastFileSaveDirectory"];
                    if (a1 != null || a1 != "") { return Directory.Exists(a1) ? a1 : PresetDir; }
                    else { return PresetDir; }
                }
                else { return PresetDir; }
            }
            set
            {
                if (PresetPath.Count < 3)
                {
                    PresetPath.Clear(); PresetPath.Add("LastFileOpenDirectory", PresetDir);
                    PresetPath.Add("LastFileSaveDirectory", value);
                    PresetPath.Add("LastFolderDirectory", PresetDir);
                }
                else { PresetPath["LastFileSaveDirectory"] = value; }
                File.WriteAllLines(PresetDir + "\\LastHSDSPPath.ini", HS_CSharpUtility.Utility.EtcUtility.SaveSetting(PresetPath));
            }
        }
        string LastFolderDirectory
        {
            get
            {
                if (File.Exists(PresetDir + "\\LastHSDSPPath.ini"))
                {
                    string[] a = File.ReadAllLines(PresetDir + "\\LastHSDSPPath.ini");
                    string a1 = HS_CSharpUtility.Utility.EtcUtility.LoadSetting(a)["LastFolderDirectory"];
                    if (a1 != null || a1 != "") { return Directory.Exists(a1) ? a1 : PresetDir; }
                    else { return PresetDir; }
                }
                else { return PresetDir; }
            }
            set
            {
                if (PresetPath.Count < 2)
                {
                    PresetPath.Clear(); PresetPath.Add("LastFileOpenDirectory", PresetDir);
                    PresetPath.Add("LastFileSaveDirectory", PresetDir);
                    PresetPath.Add("LastFolderDirectory", value);
                }
                else { PresetPath["LastFolderDirectory"] = value; }
                File.WriteAllLines(PresetDir + "\\LastHSDSPPath.ini", HS_CSharpUtility.Utility.EtcUtility.SaveSetting(PresetPath));
            }
        }

        private void 프리셋경로재설정ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("정말 모든 DSP관리자 프리셋 경로를 재설정 하시겠습니까?", "DSP관리자 프리셋 경로 모두 재설정",
                MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes) LastFolderDirectory = LastFileSaveDirectory = LastFolderDirectory = PresetDir;
        }

        private void 프리셋열기경로재설정ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("정말 DSP프리셋 열기 경로를 재설정 하시겠습니까?", "DSP관리자 프리셋 열기 경로 재설정",
                MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes) LastFileOpenDirectory = PresetDir;
        }

        private void 프리셋저장경로재설정ToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("정말 DSP관리자 프리셋 저장 경로를 재설정 하시겠습니까?", "DSP관리자 프리셋 열기 저장 재설정",
                MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes) LastFileSaveDirectory = PresetDir;
        }

        private void 프리셋폴더경로재설정ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("정말 DSP관리자 프리셋 폴더 경로를 재설정 하시겠습니까?", "DSP관리자 프리셋 폴더 경로 재설정",
                MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes) LastFolderDirectory = PresetDir;
        }

        private void 새프리셋ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (DialogResult.Yes == MessageBox.Show("정말 모두 초기화하고 새 프리셋을 작성하시겠습니까?", "DSP 프리셋 초기화", MessageBoxButtons.YesNo, MessageBoxIcon.Warning)) toolStripComboBox1.SelectedIndex = 0;
        }
#endregion

#region 이동패널 잡고 움직이기

        Point menuStrip1MouseDownLocation;
        MouseButtons menuStrip1MouseButtons = System.Windows.Forms.MouseButtons.None;
        private void menuStrip1_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                Cursor = Cursors.SizeAll;
                menuStrip1MouseButtons = System.Windows.Forms.MouseButtons.Left;
                menuStrip1MouseDownLocation = e.Location;
            }
        }

        private void menuStrip1_MouseMove(object sender, MouseEventArgs e)
        {
            if (menuStrip1MouseButtons == System.Windows.Forms.MouseButtons.Left)
            {
                this.Left += e.X - menuStrip1MouseDownLocation.X;
                this.Top += e.Y - menuStrip1MouseDownLocation.Y;
            }
        }

        private void menuStrip1_MouseUp(object sender, MouseEventArgs e)
        {
            menuStrip1MouseButtons = System.Windows.Forms.MouseButtons.None;
            Cursor = Cursors.Default;
            //try{if((menuStrip1MouseDownLocation.X!=e.Location.X)||(menuStrip1MouseDownLocation.Y!=e.Location.Y))MoveCompleted(this.Location);}catch{}
        }

        private void frmAGC_MouseMove(object sender, MouseEventArgs e)
        {
            if (menuStrip1MouseButtons == System.Windows.Forms.MouseButtons.Left)
            {
                this.Left += e.X - menuStrip1MouseDownLocation.X;
                this.Top += e.Y - menuStrip1MouseDownLocation.Y;
            }
        }

        private void frmAGC_MouseUp(object sender, MouseEventArgs e)
        {
            menuStrip1MouseButtons = System.Windows.Forms.MouseButtons.None;
            Cursor = Cursors.Default;
        }

        private void frmAGC_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                Cursor = Cursors.SizeAll;
                menuStrip1MouseButtons = System.Windows.Forms.MouseButtons.Left;
                menuStrip1MouseDownLocation = e.Location;
            }
        }
#endregion

        private void 개발자모드ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!개발자모드ToolStripMenuItem.Checked) groupBox3.Enabled = checkBox1.Enabled =
                개발자모드ToolStripMenuItem.Checked =true;
            else groupBox3.Enabled = checkBox1.Enabled = 
                개발자모드ToolStripMenuItem.Checked = false;

            dB바보이기ToolStripMenuItem.Enabled = 개발자모드ToolStripMenuItem.Checked;
        }

		public bool IsClosing = false;
        private void frmAGC_FormClosing(object sender, FormClosingEventArgs e)
        {
            for (int i = 0; i < DSP.Length; i++)
                if (DSP[i] != null) DSP[i].Dispose();
            IsClosing = true;
            //e.Cancel = !IsClosing;
            this.Hide();
        }

        private void 커스텀DSP에대해서ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("HS™ DSP는 내장된 DSP가 아닌 직접 개발한 DSP입니다.\n\n"+
                            "\"(커스텀)\"은 사용자가 세부설정을 할 수 있음을 뜻합니다.", "HS™ DSP에 대해서", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void frmAGC_SizeChanged(object sender, EventArgs e)
        {
            //button1.Location = new Point(this.Width - 3, 2);
            //label20.Location = new Point(this.Width - this.button1.Width, 2);
        }

#region dB바 관련
        int MindB = 50;

        private void dB바보이기ToolStripMenuItem_CheckedChanged(object sender, EventArgs e)
        {
            /*
            =======ON========
            스테레오확장 Size = 235, 210
            HS부스터 Size = 232, 225
            HS부스터 Loc. = 409, 35
            =======OFF========
            스테레오확장 Size = 189, 210 (-46)
            HS부스터 Size = 187, 225 (-43)
            HS부스터 Loc. = 363, 35 (-46)
            */
            if (dB바보이기ToolStripMenuItem.Checked)
            {
                pnl_dBBar_StereoExpand.Visible = pnl_dBBar_HSBooster.Visible = true;

                this.Width += pnl_dBBar_StereoExpand.Width + pnl_dBBar_HSBooster.Width;

                this.groupBox2.Width += pnl_dBBar_StereoExpand.Width;
                this.groupBox4.Location = new Point(groupBox4.Location.X + (pnl_dBBar_StereoExpand.Width) , groupBox4.Location.Y);
                this.groupBox4.Width += pnl_dBBar_HSBooster.Width;
            }
            else
            {
                pnl_dBBar_StereoExpand.Visible = pnl_dBBar_HSBooster.Visible = false;

                this.groupBox2.Width -= pnl_dBBar_StereoExpand.Width;
                this.groupBox4.Location = new Point(groupBox4.Location.X - (pnl_dBBar_StereoExpand.Width), groupBox4.Location.Y);
                this.groupBox4.Width -= pnl_dBBar_HSBooster.Width;

                this.Width -= pnl_dBBar_StereoExpand.Width + pnl_dBBar_HSBooster.Width;

                dBBar_StereoExpand_L.Value = dBBar_StereoExpand_R.Value = dBBar_HSBooster_L.Value = dBBar_HSBooster_R.Value = 0;
            }

            dBBarHSBoosterEnable = chkHSBoosterBypass.Checked && !chkHSBoosterBypass.Checked && dB바보이기ToolStripMenuItem.Checked;
            dBBarStereoExpandEnable = chkStereoExpand.Checked && !chkStereoExpandBypass.Checked && dB바보이기ToolStripMenuItem.Checked;
            
            dBBar_StereoExpand_L.Smooth = dBBar_StereoExpand_R.Smooth = dBBarStereoExpandEnable;
            dBBar_HSBooster_L.Smooth = dBBar_HSBooster_R.Smooth = dBBarHSBoosterEnable;
        }
        
        void dBBarInit()
        {
            /*
            dBBar_StereoExpand_L.MaximumValue = dBBar_StereoExpand_L.Height;
            dBBar_StereoExpand_R.MaximumValue = dBBar_StereoExpand_R.Height;
            dBBar_HSBooster_L.MaximumValue = dBBar_HSBooster_L.Height;
            dBBar_HSBooster_R.MaximumValue = dBBar_HSBooster_R.Height;*/
            dBBar_StereoExpand_L.Block = (uint)dBBar_StereoExpand_L.Height;
            dBBar_StereoExpand_R.Block = (uint)dBBar_StereoExpand_R.Height;
            dBBar_HSBooster_L.Block = (uint)dBBar_HSBooster_L.Height;
            dBBar_HSBooster_R.Block = (uint)dBBar_HSBooster_R.Height;

            dBBar_StereoExpand_L.MidColorPosition = dBBar_StereoExpand_R.MidColorPosition = 
            dBBar_HSBooster_L.MidColorPosition = dBBar_HSBooster_R.MidColorPosition = 0.35f;

            dBBar_StereoExpand_L.Smooth = dBBar_StereoExpand_R.Smooth = dBBarStereoExpandEnable;
            dBBar_HSBooster_L.Smooth = dBBar_HSBooster_R.Smooth = dBBarHSBoosterEnable;
        }

#region 스테레오 확장
        void dBBarStereoExpandCheck(float[] PeakChannel)
        {
            if (chkStereoExpand.Checked && !chkStereoExpandBypass.Checked && dB바보이기ToolStripMenuItem.Checked&&
                PeakChannel!=null&&PeakChannel.Length > 0)
            {
                dBBar_StereoExpand_L.Value = PeakChannel[0];

                if (PeakChannel.Length > 1) dBBar_StereoExpand_R.Value = PeakChannel[1];
                else dBBar_StereoExpand_R.Value = PeakChannel[0];
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            dBBarHSBoosterCheck(READCALLBACK_BassBooster_Peak);
        }
#endregion

#region HS™ 부스터
        void dBBarHSBoosterCheck(float[] PeakChannel)
        {
            if (chkHSBooster.Checked && !chkHSBoosterBypass.Checked && dB바보이기ToolStripMenuItem.Checked &&
                PeakChannel != null && PeakChannel.Length > 0)
            {
                dBBar_HSBooster_L.Value = PeakChannel[0];

                if (PeakChannel.Length > 1) dBBar_HSBooster_R.Value = PeakChannel[1];
                else dBBar_HSBooster_R.Value = PeakChannel[0];
            }
        }
#endregion

#endregion
    }
    public static class NumArrayExtension
    {
        public static float[] ValueInit(this float[] array, float Value = float.NaN)
        {
            for(int i=0;i<array.Length;i++)array[i] = Value;
            return array;
        }
        public static double[] ValueInit(this double[] array, double Value = double.NaN)
        {
            for (int i = 0; i < array.Length; i++) array[i] = Value;
            return array;
        }
    }
}
