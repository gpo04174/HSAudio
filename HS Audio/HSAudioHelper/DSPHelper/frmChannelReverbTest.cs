﻿using System;
using System.Windows.Forms;

namespace HS_Audio.Forms
{
    public partial class frmChannelReverbTest : Form
    {
        HS_Audio.HSAudioHelper Helper;
        HS_Audio_Lib.REVERB_CHANNELPROPERTIES rc = new HS_Audio_Lib.REVERB_CHANNELPROPERTIES { Room = -10000 };
        public frmChannelReverbTest(HS_Audio.HSAudioHelper Helper)
        {
            InitializeComponent(); this.Helper = Helper;
            Helper.MusicChanging+=new HSAudioHelper.MusicChangeEventHandler(Helper_MusicPathChanged);
            //try { Helper.channel.getReverbProperties(ref rc); } catch { }
        }

        private void trackBar1_ValueChanged(object sender, EventArgs e)
        {
            numericUpDown1.Value = trackBar1.Value;
        }

        private void trackBar2_ValueChanged(object sender, EventArgs e)
        {
            numericUpDown2.Value = trackBar2.Value;
        }


        void Helper_MusicPathChanged(HSAudioHelper Helper, int id, bool Error)
        {
            toolStripStatusLabel1.Text=Helper.channel.setReverbProperties(ref rc).ToString();
        }

        private void numericUpDown1_ValueChanged(object sender, EventArgs e)
        {
            //Helper_MusicPathChanged(null, -1);
        }

        private void trackBar3_ValueChanged(object sender, EventArgs e)
        {
            toolStripStatusLabel1.Text = Helper.sound.setMusicChannelVolume(0, (float)trackBar3.Value).ToString();
        }

        private void trackBar4_ValueChanged(object sender, EventArgs e)
        {
            //toolStripStatusLabel1.Text = Helper.system.setSoftwareFormat(44100, HS_Audio_Lib.SOUND_FORMAT.MPEG, 2, 6, HS_Audio_Lib.DSP_RESAMPLER.LINEAR).ToString();
            toolStripStatusLabel1.Text = Helper.sound.setMusicChannelVolume(1, (float)trackBar3.Value).ToString();
        }

        private void frmChannelReverbTest_Load(object sender, EventArgs e)
        {

        }
    }
}
