﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HS_Audio.DSP
{
    [Serializable]

    public class HSAudioDSPHelperEqualizer : HSAudioDSPHelper
    {
        internal HSAudioHelper help;
        //public FMODDSPHelper DSPHelper { get; private set; }
        /*
        public FMODDSPHelperEqualizer(FMODDSPHelper DSPhelp)
        {
            base.fh = DSPhelp.fh;
            base.dsp=DSPhelp.
        }*/
        bool System = true;
        public HSAudioDSPHelperEqualizer(HSAudioHelper help)
        {
            //FMODDSPHelper f = new FMODDSPHelper(help);
            HS_Audio_Lib.DSP dsp = null;
            base.Helper = help;
            try{base.INIT(System, HS_Audio_Lib.DSP_TYPE.PARAMEQ);}catch{}
        }
        public HSAudioDSPHelperEqualizer(HSAudioHelper help, DSPParameter Parameter)
        {
            this.help = help;
            base.Helper = help;
            base.INIT(System, HS_Audio_Lib.DSP_TYPE.PARAMEQ);
            base.SetParameter(Parameter);
        }
        [ObsoleteAttribute]
        public HSAudioDSPHelperEqualizer(HSAudioHelper help, DSPParameter[] Equalizer)
        {
            this.help = help;
            base.Helper = help;
            base.INIT(System, HS_Audio_Lib.DSP_TYPE.PARAMEQ);
            this.EqualizerArray = _EqualizerArray;
            for (int i = 0; i < Equalizer.Length; i++) 
            {
                base.DSP.setParameter((int)HS_Audio_Lib.DSP_PARAMEQ.CENTER, Equalizer[i].Index);
                base.DSP.setParameter((int)HS_Audio_Lib.DSP_PARAMEQ.BANDWIDTH, 1);
                base.DSP.setParameter((int)HS_Audio_Lib.DSP_PARAMEQ.GAIN, Equalizer[i].Value);
            }
        }
        DSPParameter[] _EqualizerArray;// = new List<DSPParameter>();
        public DSPParameter[] EqualizerArray
        {
            get { return _EqualizerArray; }
            set { _EqualizerArray = value; 
                for (int i = 0; i < value.Length; i++)
                {
                    base.DSP.setParameter((int)HS_Audio_Lib.DSP_PARAMEQ.CENTER, value[i].Index);
                    base.DSP.setParameter((int)HS_Audio_Lib.DSP_PARAMEQ.BANDWIDTH, 1);
                    base.DSP.setParameter((int)HS_Audio_Lib.DSP_PARAMEQ.GAIN, value[i].Value);
                }
            }
        }

        DSPParameter _Equalizer;
        public DSPParameter Equalizer
        {
            get { return _Equalizer; }
            set
            {
                _Equalizer = value;
                base.DSP.setParameter((int)HS_Audio_Lib.DSP_PARAMEQ.CENTER, value.Index);
                base.DSP.setParameter((int)HS_Audio_Lib.DSP_PARAMEQ.BANDWIDTH, 1);
                base.DSP.setParameter((int)HS_Audio_Lib.DSP_PARAMEQ.GAIN, value.Value);
            }
        }
    }
}
