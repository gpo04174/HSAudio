﻿namespace HS_Audio.Forms//HS_Audio.DSPHelper.Forms
{
    partial class frmSoundScript
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmSoundScript));
            FastColoredTextBoxNS.SyntaxHighlighter syntaxHighlighter1 = new FastColoredTextBoxNS.SyntaxHighlighter();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.파이ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.새스크립트문ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.열기ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.저장ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.다른이름으로스크립트저장ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.보기VToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.빌드컴파일ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.오류목록모눈선표시ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator7 = new System.Windows.Forms.ToolStripSeparator();
            this.경고숨기기ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.경고를오류로표시ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.편집기ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.줄번호표시ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.예제코드ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.설정TToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.일반편집기사용ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.빌드ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.빌드및컴파일ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.실행RToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.실행및적용ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.빌드및실행ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.스크립트PToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.효과초기화ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.디버그DToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.디버깅ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.fastColoredTextBox1 = new FastColoredTextBoxNS.FastColoredTextBox();
            this.txtCode = new System.Windows.Forms.RichTextBox();
            this.tabControl2 = new System.Windows.Forms.TabControl();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.listView1 = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader5 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.txtOutput = new System.Windows.Forms.RichTextBox();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.lblStatus = new System.Windows.Forms.ToolStripStatusLabel();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.btnOn = new System.Windows.Forms.ToolStripButton();
            this.toolStripComboBox1 = new System.Windows.Forms.ToolStripComboBox();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.btnNew1 = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.btnLoad1 = new System.Windows.Forms.ToolStripButton();
            this.btnSave1 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton2 = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.btnCompile = new System.Windows.Forms.ToolStripButton();
            this.btnRun = new System.Windows.Forms.ToolStripSplitButton();
            this.btnRunWithoutDebugging = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator9 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButton3 = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.btnBypass = new System.Windows.Forms.ToolStripButton();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.menuStrip1.SuspendLayout();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.tabControl2.SuspendLayout();
            this.tabPage4.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.파이ToolStripMenuItem,
            this.보기VToolStripMenuItem,
            this.설정TToolStripMenuItem,
            this.빌드ToolStripMenuItem,
            this.실행RToolStripMenuItem,
            this.스크립트PToolStripMenuItem,
            this.디버그DToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(804, 24);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // 파이ToolStripMenuItem
            // 
            this.파이ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.새스크립트문ToolStripMenuItem,
            this.toolStripSeparator3,
            this.열기ToolStripMenuItem,
            this.저장ToolStripMenuItem,
            this.다른이름으로스크립트저장ToolStripMenuItem});
            this.파이ToolStripMenuItem.Name = "파이ToolStripMenuItem";
            this.파이ToolStripMenuItem.Size = new System.Drawing.Size(57, 20);
            this.파이ToolStripMenuItem.Text = "파일(&F)";
            // 
            // 새스크립트문ToolStripMenuItem
            // 
            this.새스크립트문ToolStripMenuItem.Image = global::HS_Audio.Properties.Resources.Csharp_File;
            this.새스크립트문ToolStripMenuItem.Name = "새스크립트문ToolStripMenuItem";
            this.새스크립트문ToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.O)));
            this.새스크립트문ToolStripMenuItem.Size = new System.Drawing.Size(299, 22);
            this.새스크립트문ToolStripMenuItem.Text = "새 스크립트 작성";
            this.새스크립트문ToolStripMenuItem.Click += new System.EventHandler(this.새스크립트문ToolStripMenuItem_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(296, 6);
            // 
            // 열기ToolStripMenuItem
            // 
            this.열기ToolStripMenuItem.Image = global::HS_Audio.Properties.Resources.folder;
            this.열기ToolStripMenuItem.Name = "열기ToolStripMenuItem";
            this.열기ToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.O)));
            this.열기ToolStripMenuItem.Size = new System.Drawing.Size(299, 22);
            this.열기ToolStripMenuItem.Text = "스크립트 열기";
            this.열기ToolStripMenuItem.Click += new System.EventHandler(this.열기ToolStripMenuItem_Click);
            // 
            // 저장ToolStripMenuItem
            // 
            this.저장ToolStripMenuItem.Image = global::HS_Audio.Properties.Resources.disk;
            this.저장ToolStripMenuItem.Name = "저장ToolStripMenuItem";
            this.저장ToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
            this.저장ToolStripMenuItem.Size = new System.Drawing.Size(299, 22);
            this.저장ToolStripMenuItem.Text = "스크립트 저장";
            this.저장ToolStripMenuItem.Click += new System.EventHandler(this.저장ToolStripMenuItem_Click);
            // 
            // 다른이름으로스크립트저장ToolStripMenuItem
            // 
            this.다른이름으로스크립트저장ToolStripMenuItem.Image = global::HS_Audio.Properties.Resources.SaveAll;
            this.다른이름으로스크립트저장ToolStripMenuItem.Name = "다른이름으로스크립트저장ToolStripMenuItem";
            this.다른이름으로스크립트저장ToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift) 
            | System.Windows.Forms.Keys.S)));
            this.다른이름으로스크립트저장ToolStripMenuItem.Size = new System.Drawing.Size(299, 22);
            this.다른이름으로스크립트저장ToolStripMenuItem.Text = "다른 이름으로 스크립트 저장";
            this.다른이름으로스크립트저장ToolStripMenuItem.Click += new System.EventHandler(this.다른이름으로스크립트저장ToolStripMenuItem_Click);
            // 
            // 보기VToolStripMenuItem
            // 
            this.보기VToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.빌드컴파일ToolStripMenuItem,
            this.편집기ToolStripMenuItem,
            this.예제코드ToolStripMenuItem});
            this.보기VToolStripMenuItem.Name = "보기VToolStripMenuItem";
            this.보기VToolStripMenuItem.Size = new System.Drawing.Size(58, 20);
            this.보기VToolStripMenuItem.Text = "보기(&V)";
            // 
            // 빌드컴파일ToolStripMenuItem
            // 
            this.빌드컴파일ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.오류목록모눈선표시ToolStripMenuItem,
            this.toolStripSeparator7,
            this.경고숨기기ToolStripMenuItem,
            this.경고를오류로표시ToolStripMenuItem});
            this.빌드컴파일ToolStripMenuItem.Name = "빌드컴파일ToolStripMenuItem";
            this.빌드컴파일ToolStripMenuItem.Size = new System.Drawing.Size(142, 22);
            this.빌드컴파일ToolStripMenuItem.Text = "빌드(컴파일)";
            // 
            // 오류목록모눈선표시ToolStripMenuItem
            // 
            this.오류목록모눈선표시ToolStripMenuItem.CheckOnClick = true;
            this.오류목록모눈선표시ToolStripMenuItem.Name = "오류목록모눈선표시ToolStripMenuItem";
            this.오류목록모눈선표시ToolStripMenuItem.Size = new System.Drawing.Size(191, 22);
            this.오류목록모눈선표시ToolStripMenuItem.Text = "오류 목록 모눈선 표시";
            // 
            // toolStripSeparator7
            // 
            this.toolStripSeparator7.Name = "toolStripSeparator7";
            this.toolStripSeparator7.Size = new System.Drawing.Size(188, 6);
            // 
            // 경고숨기기ToolStripMenuItem
            // 
            this.경고숨기기ToolStripMenuItem.CheckOnClick = true;
            this.경고숨기기ToolStripMenuItem.Name = "경고숨기기ToolStripMenuItem";
            this.경고숨기기ToolStripMenuItem.Size = new System.Drawing.Size(191, 22);
            this.경고숨기기ToolStripMenuItem.Text = "경고 숨기기";
            // 
            // 경고를오류로표시ToolStripMenuItem
            // 
            this.경고를오류로표시ToolStripMenuItem.CheckOnClick = true;
            this.경고를오류로표시ToolStripMenuItem.Name = "경고를오류로표시ToolStripMenuItem";
            this.경고를오류로표시ToolStripMenuItem.Size = new System.Drawing.Size(191, 22);
            this.경고를오류로표시ToolStripMenuItem.Text = "경고를 오류로 표시";
            this.경고를오류로표시ToolStripMenuItem.Visible = false;
            // 
            // 편집기ToolStripMenuItem
            // 
            this.편집기ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.줄번호표시ToolStripMenuItem});
            this.편집기ToolStripMenuItem.Name = "편집기ToolStripMenuItem";
            this.편집기ToolStripMenuItem.Size = new System.Drawing.Size(142, 22);
            this.편집기ToolStripMenuItem.Text = "편집기";
            // 
            // 줄번호표시ToolStripMenuItem
            // 
            this.줄번호표시ToolStripMenuItem.Checked = true;
            this.줄번호표시ToolStripMenuItem.CheckOnClick = true;
            this.줄번호표시ToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.줄번호표시ToolStripMenuItem.Name = "줄번호표시ToolStripMenuItem";
            this.줄번호표시ToolStripMenuItem.Size = new System.Drawing.Size(140, 22);
            this.줄번호표시ToolStripMenuItem.Text = "줄 번호 표시";
            this.줄번호표시ToolStripMenuItem.CheckedChanged += new System.EventHandler(this.줄번호표시ToolStripMenuItem_CheckedChanged);
            // 
            // 예제코드ToolStripMenuItem
            // 
            this.예제코드ToolStripMenuItem.Enabled = false;
            this.예제코드ToolStripMenuItem.Name = "예제코드ToolStripMenuItem";
            this.예제코드ToolStripMenuItem.Size = new System.Drawing.Size(142, 22);
            this.예제코드ToolStripMenuItem.Text = "예제 코드";
            // 
            // 설정TToolStripMenuItem
            // 
            this.설정TToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.일반편집기사용ToolStripMenuItem});
            this.설정TToolStripMenuItem.Name = "설정TToolStripMenuItem";
            this.설정TToolStripMenuItem.Size = new System.Drawing.Size(58, 20);
            this.설정TToolStripMenuItem.Text = "설정(&T)";
            // 
            // 일반편집기사용ToolStripMenuItem
            // 
            this.일반편집기사용ToolStripMenuItem.Checked = true;
            this.일반편집기사용ToolStripMenuItem.CheckOnClick = true;
            this.일반편집기사용ToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.일반편집기사용ToolStripMenuItem.Name = "일반편집기사용ToolStripMenuItem";
            this.일반편집기사용ToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F4;
            this.일반편집기사용ToolStripMenuItem.Size = new System.Drawing.Size(183, 22);
            this.일반편집기사용ToolStripMenuItem.Text = "일반 편집기 사용";
            this.일반편집기사용ToolStripMenuItem.CheckedChanged += new System.EventHandler(this.일반편집기사용ToolStripMenuItem_CheckedChanged);
            // 
            // 빌드ToolStripMenuItem
            // 
            this.빌드ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.빌드및컴파일ToolStripMenuItem});
            this.빌드ToolStripMenuItem.Name = "빌드ToolStripMenuItem";
            this.빌드ToolStripMenuItem.Size = new System.Drawing.Size(58, 20);
            this.빌드ToolStripMenuItem.Text = "빌드(&B)";
            this.빌드ToolStripMenuItem.Click += new System.EventHandler(this.btnRunWithoutDebugging_Click);
            // 
            // 빌드및컴파일ToolStripMenuItem
            // 
            this.빌드및컴파일ToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("빌드및컴파일ToolStripMenuItem.Image")));
            this.빌드및컴파일ToolStripMenuItem.Name = "빌드및컴파일ToolStripMenuItem";
            this.빌드및컴파일ToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F7;
            this.빌드및컴파일ToolStripMenuItem.Size = new System.Drawing.Size(161, 22);
            this.빌드및컴파일ToolStripMenuItem.Text = "빌드(컴파일)";
            this.빌드및컴파일ToolStripMenuItem.Click += new System.EventHandler(this.btnCompile_Click);
            // 
            // 실행RToolStripMenuItem
            // 
            this.실행RToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.실행및적용ToolStripMenuItem,
            this.빌드및실행ToolStripMenuItem});
            this.실행RToolStripMenuItem.Name = "실행RToolStripMenuItem";
            this.실행RToolStripMenuItem.Size = new System.Drawing.Size(58, 20);
            this.실행RToolStripMenuItem.Text = "실행(&R)";
            // 
            // 실행및적용ToolStripMenuItem
            // 
            this.실행및적용ToolStripMenuItem.Image = global::HS_Audio.Properties.Resources.Run;
            this.실행및적용ToolStripMenuItem.Name = "실행및적용ToolStripMenuItem";
            this.실행및적용ToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F5;
            this.실행및적용ToolStripMenuItem.Size = new System.Drawing.Size(186, 22);
            this.실행및적용ToolStripMenuItem.Text = "스크립트 실행";
            this.실행및적용ToolStripMenuItem.Click += new System.EventHandler(this.실행및적용ToolStripMenuItem_Click);
            // 
            // 빌드및실행ToolStripMenuItem
            // 
            this.빌드및실행ToolStripMenuItem.Enabled = false;
            this.빌드및실행ToolStripMenuItem.Image = global::HS_Audio.Properties.Resources.RunWithoutDebugging;
            this.빌드및실행ToolStripMenuItem.Name = "빌드및실행ToolStripMenuItem";
            this.빌드및실행ToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.F5)));
            this.빌드및실행ToolStripMenuItem.Size = new System.Drawing.Size(186, 22);
            this.빌드및실행ToolStripMenuItem.Text = "빌드 및 실행";
            this.빌드및실행ToolStripMenuItem.Click += new System.EventHandler(this.btnRunWithoutDebugging_Click);
            // 
            // 스크립트PToolStripMenuItem
            // 
            this.스크립트PToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.효과초기화ToolStripMenuItem});
            this.스크립트PToolStripMenuItem.Name = "스크립트PToolStripMenuItem";
            this.스크립트PToolStripMenuItem.Size = new System.Drawing.Size(82, 20);
            this.스크립트PToolStripMenuItem.Text = "스크립트(&P)";
            // 
            // 효과초기화ToolStripMenuItem
            // 
            this.효과초기화ToolStripMenuItem.Image = global::HS_Audio.Properties.Resources.Reset;
            this.효과초기화ToolStripMenuItem.Name = "효과초기화ToolStripMenuItem";
            this.효과초기화ToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F4;
            this.효과초기화ToolStripMenuItem.Size = new System.Drawing.Size(156, 22);
            this.효과초기화ToolStripMenuItem.Text = "효과 초기화";
            this.효과초기화ToolStripMenuItem.ToolTipText = "모든 스크립트 인스턴스 초기화 합니다.";
            this.효과초기화ToolStripMenuItem.Click += new System.EventHandler(this.효과초기화ToolStripMenuItem_Click);
            // 
            // 디버그DToolStripMenuItem
            // 
            this.디버그DToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.디버깅ToolStripMenuItem});
            this.디버그DToolStripMenuItem.Enabled = false;
            this.디버그DToolStripMenuItem.Name = "디버그DToolStripMenuItem";
            this.디버그DToolStripMenuItem.Size = new System.Drawing.Size(71, 20);
            this.디버그DToolStripMenuItem.Text = "디버그(&D)";
            // 
            // 디버깅ToolStripMenuItem
            // 
            this.디버깅ToolStripMenuItem.Image = global::HS_Audio.Properties.Resources.Run;
            this.디버깅ToolStripMenuItem.Name = "디버깅ToolStripMenuItem";
            this.디버깅ToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F5;
            this.디버깅ToolStripMenuItem.Size = new System.Drawing.Size(156, 22);
            this.디버깅ToolStripMenuItem.Text = "디버깅 시작";
            // 
            // splitContainer1
            // 
            this.splitContainer1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.splitContainer1.BackColor = System.Drawing.Color.Transparent;
            this.splitContainer1.Location = new System.Drawing.Point(3, 1);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.fastColoredTextBox1);
            this.splitContainer1.Panel1.Controls.Add(this.txtCode);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.tabControl2);
            this.splitContainer1.Size = new System.Drawing.Size(793, 667);
            this.splitContainer1.SplitterDistance = 467;
            this.splitContainer1.TabIndex = 2;
            // 
            // fastColoredTextBox1
            // 
            this.fastColoredTextBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.fastColoredTextBox1.AutoScroll = true;
            this.fastColoredTextBox1.BackColor = System.Drawing.Color.White;
            this.fastColoredTextBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.fastColoredTextBox1.ChangedLineColor = System.Drawing.Color.Transparent;
            this.fastColoredTextBox1.CurrentLineColor = System.Drawing.Color.Transparent;
            this.fastColoredTextBox1.DescriptionFile = "";
            this.fastColoredTextBox1.FindSettings = FastColoredTextBoxNS.FindSettings.MatchCase;
            this.fastColoredTextBox1.FoldingIndicatorColor = System.Drawing.Color.LightGreen;
            this.fastColoredTextBox1.Font = new System.Drawing.Font("Consolas", 9.75F);
            this.fastColoredTextBox1.IndentBackColor = System.Drawing.Color.White;
            this.fastColoredTextBox1.IsChanged = true;
            this.fastColoredTextBox1.IsShortcutKey = true;
            this.fastColoredTextBox1.Language = FastColoredTextBoxNS.Language.CSharp;
            this.fastColoredTextBox1.LeftBracket = '(';
            this.fastColoredTextBox1.LineNumberColor = System.Drawing.Color.Teal;
            this.fastColoredTextBox1.Location = new System.Drawing.Point(-2, -2);
            this.fastColoredTextBox1.Name = "fastColoredTextBox1";
            this.fastColoredTextBox1.PreferredLineWidth = 10000;
            this.fastColoredTextBox1.RightBracket = ')';
            this.fastColoredTextBox1.SelectedText = "";
            this.fastColoredTextBox1.SelectionStart = 1032;
            this.fastColoredTextBox1.ServiceLinesColor = System.Drawing.Color.Silver;
            this.fastColoredTextBox1.Size = new System.Drawing.Size(792, 472);
            this.fastColoredTextBox1.SyntaxHighlighter = syntaxHighlighter1;
            this.fastColoredTextBox1.TabIndex = 3;
            this.fastColoredTextBox1.TText = resources.GetString("fastColoredTextBox1.TText");
            this.fastColoredTextBox1.Visible = false;
            this.fastColoredTextBox1.WordWrapMode = FastColoredTextBoxNS.WordWrapMode.WordWrapControlWidth;
            this.fastColoredTextBox1.TextChanging += new System.EventHandler<FastColoredTextBoxNS.TextChangingEventArgs>(this.txtCode_TextChanged);
            // 
            // txtCode
            // 
            this.txtCode.AcceptsTab = true;
            this.txtCode.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCode.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtCode.Location = new System.Drawing.Point(0, 0);
            this.txtCode.MaxLength = 999999999;
            this.txtCode.Name = "txtCode";
            this.txtCode.Size = new System.Drawing.Size(793, 467);
            this.txtCode.TabIndex = 4;
            this.txtCode.Text = resources.GetString("txtCode.Text");
            this.txtCode.WordWrap = false;
            this.txtCode.TextChanged += new System.EventHandler(this.txtCode_TextChanged);
            // 
            // tabControl2
            // 
            this.tabControl2.Controls.Add(this.tabPage4);
            this.tabControl2.Controls.Add(this.tabPage3);
            this.tabControl2.Controls.Add(this.tabPage5);
            this.tabControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl2.Location = new System.Drawing.Point(0, 0);
            this.tabControl2.Name = "tabControl2";
            this.tabControl2.SelectedIndex = 0;
            this.tabControl2.Size = new System.Drawing.Size(793, 196);
            this.tabControl2.TabIndex = 1;
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.listView1);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage4.Size = new System.Drawing.Size(785, 170);
            this.tabPage4.TabIndex = 1;
            this.tabPage4.Text = "오류 목록";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // listView1
            // 
            this.listView1.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader5,
            this.columnHeader2,
            this.columnHeader3,
            this.columnHeader4});
            this.listView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listView1.FullRowSelect = true;
            this.listView1.Location = new System.Drawing.Point(3, 3);
            this.listView1.Name = "listView1";
            this.listView1.Size = new System.Drawing.Size(779, 164);
            this.listView1.TabIndex = 0;
            this.listView1.UseCompatibleStateImageBehavior = false;
            this.listView1.View = System.Windows.Forms.View.Details;
            this.listView1.SelectedIndexChanged += new System.EventHandler(this.listView1_SelectedIndexChanged);
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "수준";
            this.columnHeader1.Width = 90;
            // 
            // columnHeader5
            // 
            this.columnHeader5.Text = "오류 코드";
            this.columnHeader5.Width = 70;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "위치";
            this.columnHeader2.Width = 100;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "오류 설명";
            this.columnHeader3.Width = 467;
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "소스 파일";
            this.columnHeader4.Width = 0;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.txtOutput);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(785, 170);
            this.tabPage3.TabIndex = 0;
            this.tabPage3.Text = "출력";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // txtOutput
            // 
            this.txtOutput.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtOutput.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtOutput.Location = new System.Drawing.Point(3, 3);
            this.txtOutput.Name = "txtOutput";
            this.txtOutput.ReadOnly = true;
            this.txtOutput.Size = new System.Drawing.Size(779, 164);
            this.txtOutput.TabIndex = 0;
            this.txtOutput.Text = "";
            this.txtOutput.WordWrap = false;
            // 
            // tabPage5
            // 
            this.tabPage5.Location = new System.Drawing.Point(4, 22);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Size = new System.Drawing.Size(785, 170);
            this.tabPage5.TabIndex = 2;
            this.tabPage5.Text = "콘솔";
            this.tabPage5.UseVisualStyleBackColor = true;
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.lblStatus});
            this.statusStrip1.Location = new System.Drawing.Point(0, 743);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(804, 22);
            this.statusStrip1.TabIndex = 3;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // lblStatus
            // 
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(31, 17);
            this.lblStatus.Text = "준비";
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Location = new System.Drawing.Point(0, 51);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(804, 694);
            this.tabControl1.TabIndex = 4;
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.Color.Transparent;
            this.tabPage1.Controls.Add(this.splitContainer1);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(796, 668);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "스크립트 (C#)";
            // 
            // tabPage2
            // 
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(796, 668);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "설정";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // toolStrip1
            // 
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnOn,
            this.toolStripComboBox1,
            this.toolStripSeparator5,
            this.btnNew1,
            this.toolStripSeparator4,
            this.btnLoad1,
            this.btnSave1,
            this.toolStripButton2,
            this.toolStripSeparator2,
            this.btnCompile,
            this.btnRun,
            this.toolStripSeparator9,
            this.toolStripButton3,
            this.toolStripSeparator1,
            this.btnBypass});
            this.toolStrip1.Location = new System.Drawing.Point(0, 24);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(804, 25);
            this.toolStrip1.TabIndex = 5;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // btnOn
            // 
            this.btnOn.Image = global::HS_Audio.Properties.Resources.Checkbox_UnCheck;
            this.btnOn.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnOn.Name = "btnOn";
            this.btnOn.Size = new System.Drawing.Size(51, 22);
            this.btnOn.Text = "켜기";
            this.btnOn.Click += new System.EventHandler(this.btnOn_Click);
            // 
            // toolStripComboBox1
            // 
            this.toolStripComboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.toolStripComboBox1.Items.AddRange(new object[] {
            "Main Output",
            "Channel"});
            this.toolStripComboBox1.Name = "toolStripComboBox1";
            this.toolStripComboBox1.Size = new System.Drawing.Size(121, 25);
            this.toolStripComboBox1.SelectedIndexChanged += new System.EventHandler(this.toolStripComboBox1_SelectedIndexChanged);
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(6, 25);
            // 
            // btnNew1
            // 
            this.btnNew1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnNew1.Image = global::HS_Audio.Properties.Resources.Csharp_File;
            this.btnNew1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnNew1.Name = "btnNew1";
            this.btnNew1.Size = new System.Drawing.Size(23, 22);
            this.btnNew1.Text = "세 스크립트 작성";
            this.btnNew1.Click += new System.EventHandler(this.새스크립트문ToolStripMenuItem_Click);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(6, 25);
            // 
            // btnLoad1
            // 
            this.btnLoad1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnLoad1.Image = global::HS_Audio.Properties.Resources.folder;
            this.btnLoad1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnLoad1.Name = "btnLoad1";
            this.btnLoad1.Size = new System.Drawing.Size(23, 22);
            this.btnLoad1.Text = "열기";
            this.btnLoad1.Click += new System.EventHandler(this.열기ToolStripMenuItem_Click);
            // 
            // btnSave1
            // 
            this.btnSave1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnSave1.Image = global::HS_Audio.Properties.Resources.disk;
            this.btnSave1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnSave1.Name = "btnSave1";
            this.btnSave1.Size = new System.Drawing.Size(23, 22);
            this.btnSave1.Text = "저장";
            this.btnSave1.Click += new System.EventHandler(this.저장ToolStripMenuItem_Click);
            // 
            // toolStripButton2
            // 
            this.toolStripButton2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton2.Image = global::HS_Audio.Properties.Resources.SaveAll;
            this.toolStripButton2.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton2.Name = "toolStripButton2";
            this.toolStripButton2.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton2.Text = "다른 이름으로 저장";
            this.toolStripButton2.Click += new System.EventHandler(this.다른이름으로스크립트저장ToolStripMenuItem_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // btnCompile
            // 
            this.btnCompile.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnCompile.Image = ((System.Drawing.Image)(resources.GetObject("btnCompile.Image")));
            this.btnCompile.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnCompile.Name = "btnCompile";
            this.btnCompile.Size = new System.Drawing.Size(23, 22);
            this.btnCompile.Text = "빌드(컴파일)";
            this.btnCompile.Click += new System.EventHandler(this.btnCompile_Click);
            // 
            // btnRun
            // 
            this.btnRun.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnRun.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnRunWithoutDebugging});
            this.btnRun.Image = global::HS_Audio.Properties.Resources.Run;
            this.btnRun.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnRun.Name = "btnRun";
            this.btnRun.Size = new System.Drawing.Size(32, 22);
            this.btnRun.Text = "스크립트 실행";
            this.btnRun.ButtonClick += new System.EventHandler(this.실행및적용ToolStripMenuItem_Click);
            // 
            // btnRunWithoutDebugging
            // 
            this.btnRunWithoutDebugging.Enabled = false;
            this.btnRunWithoutDebugging.Image = global::HS_Audio.Properties.Resources.RunWithoutDebugging;
            this.btnRunWithoutDebugging.Name = "btnRunWithoutDebugging";
            this.btnRunWithoutDebugging.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.F5)));
            this.btnRunWithoutDebugging.Size = new System.Drawing.Size(186, 22);
            this.btnRunWithoutDebugging.Text = "빌드 및 실행";
            this.btnRunWithoutDebugging.Click += new System.EventHandler(this.btnRunWithoutDebugging_Click);
            // 
            // toolStripSeparator9
            // 
            this.toolStripSeparator9.Name = "toolStripSeparator9";
            this.toolStripSeparator9.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripButton3
            // 
            this.toolStripButton3.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton3.Image = global::HS_Audio.Properties.Resources.Reset;
            this.toolStripButton3.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton3.Name = "toolStripButton3";
            this.toolStripButton3.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton3.Text = "모든 스크립트 인스턴스 초기화";
            this.toolStripButton3.Click += new System.EventHandler(this.효과초기화ToolStripMenuItem_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // btnBypass
            // 
            this.btnBypass.Image = ((System.Drawing.Image)(resources.GetObject("btnBypass.Image")));
            this.btnBypass.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnBypass.Name = "btnBypass";
            this.btnBypass.Size = new System.Drawing.Size(51, 22);
            this.btnBypass.Text = "우회";
            this.btnBypass.Click += new System.EventHandler(this.btnBypass_Click);
            // 
            // saveFileDialog1
            // 
            this.saveFileDialog1.DefaultExt = "*.hscs";
            this.saveFileDialog1.Filter = "사운드 스크립트|*.hscs|모든 파일|*.*";
            this.saveFileDialog1.Title = "사운드 스크립트 저장...";
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.DefaultExt = "*.hscs";
            this.openFileDialog1.Filter = "사운드 스크립트|*.hscs|모든 파일|*.*";
            this.openFileDialog1.Title = "사운드 스크립트 열기";
            // 
            // frmSoundScript
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(804, 765);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "frmSoundScript";
            this.Text = "제목 없음 - HS 사운드 스크립트";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmSoundScript_FormClosing);
            this.Load += new System.EventHandler(this.frmSoundScript_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.ResumeLayout(false);
            this.tabControl2.ResumeLayout(false);
            this.tabPage4.ResumeLayout(false);
            this.tabPage3.ResumeLayout(false);
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem 파이ToolStripMenuItem;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel lblStatus;
        private System.Windows.Forms.ToolStripMenuItem 새스크립트문ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 보기VToolStripMenuItem;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton btnLoad1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripButton btnBypass;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripButton btnCompile;
        private System.Windows.Forms.ToolStripComboBox toolStripComboBox1;
        private System.Windows.Forms.ToolStripButton btnSave1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripMenuItem 열기ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 저장ToolStripMenuItem;
        private System.Windows.Forms.ToolStripButton btnNew1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.TabControl tabControl2;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.ListView listView1;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private System.Windows.Forms.ColumnHeader columnHeader5;
        private System.Windows.Forms.ToolStripButton btnOn;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.ToolStripMenuItem 디버그DToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 디버깅ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 빌드ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 빌드및컴파일ToolStripMenuItem;
        private FastColoredTextBoxNS.FastColoredTextBox fastColoredTextBox1;
        private System.Windows.Forms.ToolStripMenuItem 설정TToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 일반편집기사용ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 빌드컴파일ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 오류목록모눈선표시ToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator7;
        private System.Windows.Forms.ToolStripMenuItem 경고숨기기ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 경고를오류로표시ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 편집기ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 줄번호표시ToolStripMenuItem;
        private System.Windows.Forms.RichTextBox txtOutput;
        private System.Windows.Forms.RichTextBox txtCode;
        private System.Windows.Forms.ToolStripMenuItem 다른이름으로스크립트저장ToolStripMenuItem;
        private System.Windows.Forms.ToolStripButton toolStripButton2;
        private System.Windows.Forms.ToolStripMenuItem 예제코드ToolStripMenuItem;
        private System.Windows.Forms.ToolStripButton toolStripButton3;
        private System.Windows.Forms.ToolStripSplitButton btnRun;
        private System.Windows.Forms.ToolStripMenuItem btnRunWithoutDebugging;
        private System.Windows.Forms.ToolStripMenuItem 스크립트PToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 효과초기화ToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator9;
        private System.Windows.Forms.ToolStripMenuItem 실행RToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 실행및적용ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 빌드및실행ToolStripMenuItem;
    }
}