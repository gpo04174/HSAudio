﻿using System;
using System.Windows.Forms;
using HS_Audio_Lib;
using HS_Audio_Helper;
using HS_Audio_Helper.DSP;
namespace HS_Audio_Helper.Forms
{
    public partial class frmDSPHelper : Form
    {
        internal HS_Audio_Helper.DSP.HSAudioDSPHelper fdh;
        HSAudioHelper snd;
        public frmDSPHelper(HS_Audio_Helper.HSAudioHelper Value)
        {
            InitializeComponent();
            fdh = Value.dsp;
            this.snd = Value;
            comboBox1.Text = "END_MS";
            fdh.Helper.system.createDSPByType(DSP_TYPE.PARAMEQ, );
        }
        
        private void DSPHelper_Load(object sender, EventArgs e)
        {//FMOD.DSP_TYPE.
            //d.showConfigDialog(this.Handle, true);

        }

        private void trackBar1_ValueChanged(object sender, EventArgs e)
        {
            HS_Audio_Lib.RESULT r= fdh.Helper.channel.setDelay((DELAYTYPE)Enum.Parse(typeof(DELAYTYPE), comboBox1.Text), (uint)trackBar1.Value, (uint)trackBar2.Value);
        }

        private void comboBox1_TextChanged(object sender, EventArgs e)
        {
            fdh.Helper.channel.setDelay((DELAYTYPE)Enum.Parse(typeof(DELAYTYPE), comboBox1.Text), (uint)trackBar1.Value, (uint)trackBar2.Value);
        }

        private void comboBox1_SelectedValueChanged(object sender, EventArgs e)
        {
            
        }
        void init() 
        { 

        }

        private void trackBar3_ValueChanged(object sender, EventArgs e)
        {

        }

        Enum DSPtype;
        private void comboBox3_SelectedValueChanged(object sender, EventArgs e)
        {
            comboBox3.Items.Clear();
            switch (comboBox3.Text)
            {
                case "OSCILLATOR":
                    foreach (string a in Enum.GetNames(typeof(HS_Audio_Lib.DSP_OSCILLATOR))){ comboBox3.Items.Add(a); }
                    lblFMODResult.Text=fdh.Helper.system.createDSPByType(DSP_TYPE.MIXER, ref fdh.DSP).ToString();break;
                case "LOWPASS":
                    foreach (string a in Enum.GetNames(typeof(HS_Audio_Lib.DSP_LOWPASS))) { comboBox3.Items.Add(a); }
                    lblFMODResult.Text = fdh.Helper.system.createDSPByType(DSP_TYPE.LOWPASS, ref fdh.DSP).ToString(); break;
                case "ITLOWPASS":
                    foreach (string a in Enum.GetNames(typeof(HS_Audio_Lib.DSP_ITLOWPASS))) { comboBox3.Items.Add(a); }
                    lblFMODResult.Text = fdh.Helper.system.createDSPByType(DSP_TYPE.ITLOWPASS, ref fdh.DSP).ToString(); break;
                case "HIGHPASS":
                    foreach (string a in Enum.GetNames(typeof(HS_Audio_Lib.DSP_HIGHPASS))) { comboBox3.Items.Add(a); }
                    lblFMODResult.Text = fdh.Helper.system.createDSPByType(DSP_TYPE.HIGHPASS, ref fdh.DSP).ToString(); break;
                case "ECHO":
                    foreach (string a in Enum.GetNames(typeof(HS_Audio_Lib.DSP_ECHO))) { comboBox3.Items.Add(a); }
                    lblFMODResult.Text = fdh.Helper.system.createDSPByType(DSP_TYPE.ECHO, ref fdh.DSP).ToString(); break;
                case "FLANGE":
                    foreach (string a in Enum.GetNames(typeof(HS_Audio_Lib.DSP_FLANGE))) { comboBox3.Items.Add(a); }
                    lblFMODResult.Text = fdh.Helper.system.createDSPByType(DSP_TYPE.FLANGE, ref fdh.DSP).ToString(); break;
                case "DISTORTION":
                    foreach (string a in Enum.GetNames(typeof(HS_Audio_Lib.DSP_DISTORTION))) { comboBox3.Items.Add(a); }
                    lblFMODResult.Text = fdh.Helper.system.createDSPByType(DSP_TYPE.DISTORTION, ref fdh.DSP).ToString(); break;
                case "NORMALIZE":
                    foreach (string a in Enum.GetNames(typeof(HS_Audio_Lib.DSP_NORMALIZE))) { comboBox3.Items.Add(a); }
                    lblFMODResult.Text = fdh.Helper.system.createDSPByType(DSP_TYPE.NORMALIZE, ref fdh.DSP).ToString(); break;
                case "PARAMEQ":
                    foreach (string a in Enum.GetNames(typeof(HS_Audio_Lib.DSP_PARAMEQ))) { comboBox3.Items.Add(a); }
                    lblFMODResult.Text = fdh.Helper.system.createDSPByType(DSP_TYPE.PARAMEQ, ref fdh.DSP).ToString(); break;
                case "PITCHSHIFT":
                    foreach (string a in Enum.GetNames(typeof(HS_Audio_Lib.DSP_PITCHSHIFT))) { comboBox3.Items.Add(a); }
                    lblFMODResult.Text = fdh.Helper.system.createDSPByType(DSP_TYPE.PITCHSHIFT, ref fdh.DSP).ToString(); break;
                case "CHORUS":
                    foreach (string a in Enum.GetNames(typeof(HS_Audio_Lib.DSP_CHORUS))) { comboBox3.Items.Add(a); }
                    lblFMODResult.Text = fdh.Helper.system.createDSPByType(DSP_TYPE.CHORUS, ref fdh.DSP).ToString(); break;
                case "ITECHO":
                    foreach (string a in Enum.GetNames(typeof(HS_Audio_Lib.DSP_ITECHO))) { comboBox3.Items.Add(a); }
                    lblFMODResult.Text = fdh.Helper.system.createDSPByType(DSP_TYPE.ITECHO, ref fdh.DSP).ToString(); break;
                case "COMPRESSOR":
                    foreach (string a in Enum.GetNames(typeof(HS_Audio_Lib.DSP_COMPRESSOR))) { comboBox3.Items.Add(a); }
                    lblFMODResult.Text = fdh.Helper.system.createDSPByType(DSP_TYPE.COMPRESSOR, ref fdh.DSP).ToString(); break;
                case "SFXREVERB":
                    foreach (string a in Enum.GetNames(typeof(HS_Audio_Lib.DSP_SFXREVERB))) { comboBox3.Items.Add(a); }
                    lblFMODResult.Text = fdh.Helper.system.createDSPByType(DSP_TYPE.SFXREVERB, ref fdh.DSP).ToString(); break;
                case "LOWPASS_SIMPLE":
                    foreach (string a in Enum.GetNames(typeof(HS_Audio_Lib.DSP_LOWPASS_SIMPLE))) { comboBox3.Items.Add(a); }
                    lblFMODResult.Text = fdh.Helper.system.createDSPByType(DSP_TYPE.LOWPASS_SIMPLE, ref fdh.DSP).ToString(); break;
                case "DELAY":
                    foreach (string a in Enum.GetNames(typeof(HS_Audio_Lib.DSP_DELAY))) { comboBox3.Items.Add(a); }
                    lblFMODResult.Text = fdh.Helper.system.createDSPByType(DSP_TYPE.DELAY, ref fdh.DSP).ToString(); break;
                case "TREMOLO":
                    foreach (string a in Enum.GetNames(typeof(HS_Audio_Lib.DSP_TREMOLO))) { comboBox3.Items.Add(a); }
                    lblFMODResult.Text = fdh.Helper.system.createDSPByType(DSP_TYPE.TREMOLO, ref fdh.DSP).ToString(); break;

                case "MIXER":
                    lblFMODResult.Text = fdh.Helper.system.createDSPByType(DSP_TYPE.MIXER, ref fdh.DSP).ToString(); break;
                case "LADSPAPLUGIN":
                    lblFMODResult.Text = fdh.Helper.system.createDSPByType(DSP_TYPE.LADSPAPLUGIN, ref fdh.DSP).ToString(); break;
                case "VSTPLUGIN":
                    lblFMODResult.Text = fdh.Helper.system.createDSPByType(DSP_TYPE.VSTPLUGIN, ref fdh.DSP).ToString(); break;
                case "WINAMPPLUGIN":
                    lblFMODResult.Text = fdh.Helper.system.createDSPByType(DSP_TYPE.WINAMPPLUGIN, ref fdh.DSP).ToString(); break;

                default: lblFMODResult.Text = fdh.Helper.system.createDSPByType(DSP_TYPE.UNKNOWN, ref fdh.DSP).ToString(); break;
            }
           
        }

        private void trackBar3_Scroll(object sender, EventArgs e)
        {

        }

        private void numericUpDown7_ValueChanged(object sender, EventArgs e)
        {
            //lblFMODResult.Text = fdh.dsp.setParameter((int)Enum.Parse(typeof(FMOD), 10f).ToString();
            //fdh.fh.system.
            //fdh.fh.system.createDSPByType(DSP_TYPE.ECHO, ref fdh.dsp);
        }
        frmDSP frmdsp;
        private void button1_Click(object sender, EventArgs e)
        {

        }
    }
}
