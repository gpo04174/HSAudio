﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HS_Helper.DSPHelper
{
    public static class CustomDSP
    {
        /// <summary>
        /// 원본 주소: http://www.musicdsp.org/archive.php?classid=1#187
        /// I use it in an unreleased additive synth. 
        ///There's no oversampling needed in my case since I feed it with a pure sinusoid and I control the order to not have frequencies above Fs/2. Otherwise you should oversample by the order you'll use in the function or bandlimit the signal before the waveshaper. unless you really want that aliasing effect... :)
        ///I hope the code is self-explaining, otherwise there's plenty of sites explaining chebyshev polynoms and their applications.

        /// </summary>
        /// <param name="x"></param>
        /// <param name="A"></param>
        /// <param name="order"></param>
        /// <returns></returns>
        public static float ChebyShev(this float x, float[] A, int order)
        {
            // To = 1
            // T1 = x
            // Tn = 2.x.Tn-1 - Tn-2
            // out = sum(Ai*Ti(x)) , i C {1,..,order}
            float Tn_2 = 1.0f;
            float Tn_1 = x;
            float Tn;
            float Out = A[0] * Tn_1;

            for (int n = 2; n <= order; n++)
            {
                Tn = 2.0f * x * Tn_1 - Tn_2;
                Out += A[n - 1] * Tn;
                Tn_2 = Tn_1;
                Tn_1 = Tn;
            }
            return Out;
        }

        /// <summary>
        /// 출처: http://www.musicdsp.org/archive.php?classid=4#42
        /// </summary>
        /// <param name="Value"></param>
        /// <param name="x">최소값: 0.0, 최댓값: 1.0</param>
        /// <returns></returns>
        public static float SoftSaturation(this float Value, float x)
        {
            if (x < Value) return x;
            if (x > Value) return Value + (x - Value) / (1f + ((x - Value) / (1 - Value)) * ((x - Value) / (1 - Value)));
            if (x > 1) return (Value + 1) / 2;
            else return Value;
        }

        /// <summary>
        /// 로우패스 필터
        /// 출처: http://www.codeproject.com/Tips/681745/Csharp-Discrete-Time-RLC-Low-High-Pass-Filter-Rout
        /// </summary>
        /// <param name="Input">입력 사운드</param>
        /// <param name="CUTOFF"></param>
        /// <param name="RESONANCE"></param>
        /// <param name="SampleRate">샘플레이트</param>
        /// <returns></returns>
        public static float LowPass(this float Input, float CUTOFF, float RESONANCE, float SampleRate = 44100)
        {
            float O = 2.0f * (float)Math.PI * CUTOFF / SampleRate;
            float C = RESONANCE / O;
            float L = 1f / RESONANCE / O;
            float V = 0, I = 0, T;
            T = (I - V) / C;
            I += (Input * O - V) / L;
            V += T;
            return V / O;
        }

        static float[] StereoWidener_OUT= new float[2];
        /// <summary>
        /// 
        /// </summary>
        /// <param name="Left">좌</param>
        /// <param name="Right">우</param>
        /// <param name="Width">'width' is the stretch factor of the stereo field:
        /// width < 1: decrease in stereo width
        /// width = 1: no change
        /// width > 1: increase in stereo width
        /// width = 0: mono</param>
        /// <returns></returns>
        public static float[] StereoWidener(float Left, float Right, float Width)
        {
            // calculate scale coefficient
            float coef_S = Width * 0.5f;

            // then do this per sample
            float m = (Left + Right) * 0.5f;
            float s = (Right - Left) * coef_S;

            StereoWidener_OUT[0] = m - s;
            StereoWidener_OUT[1] = m + s;
            return StereoWidener_OUT;
        }

        static float[] StereoEnhanca_OUT = new float[2];
        /// <summary>
        /// 출처: http://www.musicdsp.org/archive.php?classid=4#173
        /// </summary>
        /// <param name="Left"></param>
        /// <param name="Right"></param>
        /// <param name="WideCoeff"></param>
        /// <returns></returns>
        public static float[] StereoEnhanca(float Left, float Right, float WideCoeff)
        {
            float MonoSign = (Left + Right) / 2.0f;

            float DeltaLeft = Left - MonoSign;
            DeltaLeft *= WideCoeff;
            float DeltaRight = Right - MonoSign;
            DeltaRight *= WideCoeff;

            StereoEnhanca_OUT[0] += DeltaLeft;
            StereoEnhanca_OUT[1] += DeltaRight;
            return StereoEnhanca_OUT;
        }
    }
}
