﻿using System;
using System.Collections.Generic;

using System.Text;
using HS_Audio.Properties;

namespace HS_Audio
{
    public delegate void HSAudio_RESULT_ChangedEventHandler(HS_Audio_Lib.RESULT result, string Message);
    [Serializable]
    public enum HSAudioReverbPreset
    {
        없음=-1,
        일반,
        방음실,
        방,
        욕실,
        거실,
        석조실,
        강당,
        콘서트_홀,
        동굴,
        경기장,
        격납고,
        카페트가_깔린_복도,
        복도,
        석조_회랑,
        골목길,
        숲,
        도시,
        산,
        채석장,
        평원,
        주차장,
        하수도,
        지하수,
        사용자_설정
    }
    [Serializable]
    public partial class HSAudioHelperEx
    {
        /// <summary>
        ///상태가 변경되었을때 발생하는 이벤트 입니다.
        /// </summary>
        public event HSAudio_RESULT_ChangedEventHandler RESULT_Changed;
        internal HSAudioHelper help;
        internal HS_Audio_Lib.Reverb Reverb;
        public HSAudioHelperEx(HSAudioHelper Value) 
        { 
            help = Value;
            //Result= Value.system.createReverb(ref Reverb);
            if (help.channel != null)
            {
                Result = help.channel.getFrequency(ref _FMODChannelInfo.Frequency);
                Result = help.channel.getPan(ref _FMODChannelInfo.Pan);
                Result = help.channel.getLowPassGain(ref _FMODChannelInfo.LowPassGain);
                //_FMODChannelInfo.ChannelReverbProperties = new FMOD.REVERB_CHANNELPROPERTIES();
                //_FMODChannelInfo.ChannelReverbProperties = new FMOD.REVERB_CHANNELPROPERTIES();
                //if (_FMODChannelInfo.ChannelReverbProperties!=null)
                      Result = help.channel.getReverbProperties(ref _FMODChannelInfo.ChannelReverbProperties);
                help.channel.getSpeakerMix(ref _FMODChannelInfo.SpeakerMix.FrontLeft, ref _FMODChannelInfo.SpeakerMix.FrontRight,
                    ref _FMODChannelInfo.SpeakerMix.Center, ref _FMODChannelInfo.SpeakerMix.LFE, ref _FMODChannelInfo.SpeakerMix.BackLeft,
                    ref _FMODChannelInfo.SpeakerMix.BackRight, ref _FMODChannelInfo.SpeakerMix.SideLeft, ref _FMODChannelInfo.SpeakerMix.SideRight);
                FMODChannelInfo = _FMODChannelInfo;
            }
            //FMODChannelStruct f = FMODChannelInfo;
        }
        public HSAudioHelperEx() { }
        public static readonly HS_Audio_Lib.REVERB_PROPERTIES REVERB_PROPERTIES_GENERIC = new HS_Audio_Lib.REVERB_PROPERTIES(0, -1, 1.00f, -10000, -10000, 0, 1.00f, 1.00f, 1.0f, -2602, 0.007f, 200, 0.011f, 0.25f, 0.000f, 5000.0f, 250.0f, 0.0f, 0.0f, 0x33f);

        bool _LoudnessEqualization;
        public bool LoudnessEqualization
        {
            get { return _LoudnessEqualization; }
            set { _LoudnessEqualization = value; ReverbProperties = _ReverbProperties; }
        }

        private HS_Audio_Lib.REVERB_PROPERTIES _ReverbProperties = new HS_Audio_Lib.REVERB_PROPERTIES(0,      -1,    1.00f, -10000, -10000, 0,   1.00f,  1.00f, 1.0f,  -2602, 0.007f,   200, 0.011f, 0.25f, 0.000f, 5000.0f, 250.0f, 0.0f,   0.0f,   0x33f);
        /// <summary>
        /// 반향효과를 설정하거나 가져옵니다.
        /// </summary>
        public HS_Audio_Lib.REVERB_PROPERTIES ReverbProperties
        {
            get
            {
                if (help!=null&&help.sound != null)
                {
                    if (!LoudnessEqualization)
                    {
                        Result = help.system.getReverbProperties(ref _ReverbProperties);
                        if (Result != HS_Audio_Lib.RESULT.OK) { return _ReverbProperties; }
                        else { return _ReverbProperties; }
                    }
                    else
                    {
                        Result = help.system.setReverbAmbientProperties(ref _ReverbProperties);
                        if (Result != HS_Audio_Lib.RESULT.OK) { return _ReverbProperties; }
                        else { return _ReverbProperties; }
                    }
                }
                else { return REVERB_PROPERTIES_GENERIC; }
            }
            set
            {
                _ReverbProperties = value;
                if (Reverb == null && help!=null) Result = help.system.createReverb(ref Reverb);
                if (Reverb != null)
                {
                    if (!LoudnessEqualization)
                    {
                        //Result = Reverb.setProperties(ref _ReverbProperties);
                        Result = help.system.setReverbProperties(ref _ReverbProperties);
                        if (Result != HS_Audio_Lib.RESULT.OK) { throw new Exception("음향효과 설정중 오류 발생!!!\r\nFMOD값: " + Result.ToString() + "\n설명: " + HS_Audio_Lib.Error.String(Result)); }
                    }
                    else
                    {
                        Result = help.system.setReverbAmbientProperties(ref _ReverbProperties);
                        if (Result != HS_Audio_Lib.RESULT.OK) { throw new Exception("음향효과 설정중 오류 발생!!!\r\n값: " + Result.ToString()); }
                    }
                }
            }
        }

        private HSAudioReverbPreset _ReverbPreset;
        /// <summary>
        /// 지정된 반향효과를 가져오거나 설정합니다.
        /// </summary>
        public HSAudioReverbPreset ReverbPreset//(FMODReverbEnum Reverb)
        {
            get { return ConvertReverbToEnum(ReverbProperties); }
            set { if (help!=null&&help.sound != null) { _ReverbPreset = value; ReverbProperties = ConvertEnumToReverb(value); } }
        }

        HSAudioChannelStruct _FMODChannelInfo = new HSAudioChannelStruct();
        public HSAudioChannelStruct FMODChannelInfo_Current;
        /// <summary>
        /// FMOD.Channel에 대한 효과를 가져오거나 설정합니다. 
        /// </summary>
        public HSAudioChannelStruct FMODChannelInfo
        {
            get
            {
                HSAudioChannelStruct _FMODChannelInfo1= new HSAudioChannelStruct();
                if (help != null)
                {
                    Result = help.channel.getPan(ref _FMODChannelInfo1.Pan);
                    Result = help.channel.getLowPassGain(ref _FMODChannelInfo1.LowPassGain);
                    Result = help.channel.getFrequency(ref _FMODChannelInfo1.Frequency);
                    //if (_FMODChannelInfo1.ChannelReverbProperties != null)
                        Result = help.channel.getReverbProperties(ref _FMODChannelInfo1.ChannelReverbProperties);
                    _FMODChannelInfo1.Delay = new HSAudioDelay();
                    Result = help.channel.getDelay(_FMODChannelInfo1.Delay.DelayType, ref _FMODChannelInfo1.Delay.DelayHigh, ref _FMODChannelInfo1.Delay.DelayLow);
                }
                return _FMODChannelInfo1;
            }
            set
            {
                _FMODChannelInfo = value;
                Result=help.channel.setFrequency(_FMODChannelInfo.Frequency);
                Result=help.channel.setLowPassGain(_FMODChannelInfo.LowPassGain);
                Result=help.channel.setPan(_FMODChannelInfo.Pan);
                //FMOD.REVERB_CHANNELPROPERTIES rp = new FMOD.REVERB_CHANNELPROPERTIES();
                //if (value.ChannelReverbProperties != null)
                { Result=help.channel.setReverbProperties(ref _FMODChannelInfo.ChannelReverbProperties); }
                if (value.Delay != null)
                { Result=help.channel.setDelay(_FMODChannelInfo.Delay.DelayType,
                                                   _FMODChannelInfo.Delay.DelayHigh,
                                                   _FMODChannelInfo.Delay.DelayLow);
                }
                FMODChannelInfo_Current = value;
            }
        }

        HSAudio3DAttributes _FMOD3DAttributes = new HSAudio3DAttributes();
        public HSAudio3DAttributes FMOD3DAttributes_Current;
        public HSAudio3DAttributes FMOD3DAttributes
        {
            get {Result= help.channel.get3DAttributes(ref _FMOD3DAttributes.Pos, ref _FMOD3DAttributes.Vel); return _FMOD3DAttributes; }
            set {Result= help.channel.set3DAttributes(ref value.Pos, ref value.Vel); FMOD3DAttributes_Current = value; }
        }

        HS_Audio_Lib.RESULT _Result;
        /// <summary>
        /// 상태를 가져옵니다.
        /// </summary>
        public HS_Audio_Lib.RESULT Result
        {

            get { return _Result; }
            internal set { _Result = value; try { if(RESULT_Changed!=null) RESULT_Changed(value, HS_Audio_Lib.Error.String(value)); } catch { } }
        }


        private HS_Audio_Lib.REVERB_PROPERTIES ConvertEnumToReverb(HSAudioReverbPreset rev)
        {
            switch (rev)
            {
                case HSAudioReverbPreset.없음: return HS_Audio_Lib.PRESET.OFF;
                case HSAudioReverbPreset.일반: return HS_Audio_Lib.PRESET.GENERIC;
                case HSAudioReverbPreset.방음실: return HS_Audio_Lib.PRESET.PADDEDCELL;
                case HSAudioReverbPreset.방: return HS_Audio_Lib.PRESET.ROOM;
                case HSAudioReverbPreset.욕실: return HS_Audio_Lib.PRESET.BATHROOM;
                case HSAudioReverbPreset.거실: return HS_Audio_Lib.PRESET.LIVINGROOM ;
                case HSAudioReverbPreset.석조실: return HS_Audio_Lib.PRESET.STONEROOM;
                case HSAudioReverbPreset.강당: return HS_Audio_Lib.PRESET.AUDITORIUM;
                case HSAudioReverbPreset.콘서트_홀: return HS_Audio_Lib.PRESET.CONCERTHALL;
                case HSAudioReverbPreset.동굴: return HS_Audio_Lib.PRESET.CAVE;
                case HSAudioReverbPreset.경기장: return HS_Audio_Lib.PRESET.ARENA ;
                case HSAudioReverbPreset.격납고: return HS_Audio_Lib.PRESET.HANGAR;
                case HSAudioReverbPreset.카페트가_깔린_복도: return HS_Audio_Lib.PRESET.CARPETTEDHALLWAY;
                case HSAudioReverbPreset.복도: return HS_Audio_Lib.PRESET.HALLWAY;
                case HSAudioReverbPreset.석조_회랑: return HS_Audio_Lib.PRESET.STONECORRIDOR;
                case HSAudioReverbPreset.골목길: return HS_Audio_Lib.PRESET.ALLEY;
                case HSAudioReverbPreset.숲: return HS_Audio_Lib.PRESET.FOREST;
                case HSAudioReverbPreset.도시: return HS_Audio_Lib.PRESET.CITY;
                case HSAudioReverbPreset.산: return HS_Audio_Lib.PRESET.MOUNTAINS;
                case HSAudioReverbPreset.채석장: return HS_Audio_Lib.PRESET.QUARRY;
                case HSAudioReverbPreset.평원: return HS_Audio_Lib.PRESET.PLAIN;
                case HSAudioReverbPreset.주차장: return HS_Audio_Lib.PRESET.PARKINGLOT;
                case HSAudioReverbPreset.하수도: return HS_Audio_Lib.PRESET.SEWERPIPE;
                case HSAudioReverbPreset.지하수: return HS_Audio_Lib.PRESET.UNDERWATER;
                default: return ReverbProperties;
            }
        }
        private HSAudioReverbPreset ConvertReverbToEnum(HS_Audio_Lib.REVERB_PROPERTIES rev)
        {
            if (rev.Equals(HS_Audio_Lib.PRESET.OFF)) { return HSAudioReverbPreset.없음; }
            else if (rev.Equals(HS_Audio_Lib.PRESET.GENERIC)) { return HSAudioReverbPreset.일반; }
            else if (rev.Equals(HS_Audio_Lib.PRESET.PADDEDCELL )) { return HSAudioReverbPreset.방음실; }
            else if (rev.Equals(HS_Audio_Lib.PRESET.ROOM)) { return HSAudioReverbPreset.방; }
            else if (rev.Equals(HS_Audio_Lib.PRESET.BATHROOM)) { return HSAudioReverbPreset.욕실; }
            else if (rev.Equals(HS_Audio_Lib.PRESET.LIVINGROOM )) { return HSAudioReverbPreset.거실; }
            else if (rev.Equals(HS_Audio_Lib.PRESET.STONEROOM )) { return HSAudioReverbPreset.석조실; }
            else if (rev.Equals(HS_Audio_Lib.PRESET.AUDITORIUM)) { return HSAudioReverbPreset.강당; }
            else if (rev.Equals(HS_Audio_Lib.PRESET.CONCERTHALL)) { return HSAudioReverbPreset.콘서트_홀; }
            else if (rev.Equals(HS_Audio_Lib.PRESET.CAVE)) { return HSAudioReverbPreset.동굴; }
            else if (rev.Equals(HS_Audio_Lib.PRESET.ARENA)) { return HSAudioReverbPreset.경기장; }
            else if (rev.Equals(HS_Audio_Lib.PRESET.HANGAR )) { return HSAudioReverbPreset.격납고; }
            else if (rev.Equals(HS_Audio_Lib.PRESET.CARPETTEDHALLWAY)) { return HSAudioReverbPreset.카페트가_깔린_복도; }
            else if (rev.Equals(HS_Audio_Lib.PRESET.HALLWAY)) { return HSAudioReverbPreset.복도; }
            else if (rev.Equals(HS_Audio_Lib.PRESET.STONECORRIDOR)) { return HSAudioReverbPreset.석조_회랑; }
            else if (rev.Equals(HS_Audio_Lib.PRESET.ALLEY)) { return HSAudioReverbPreset.골목길; }
            else if (rev.Equals(HS_Audio_Lib.PRESET.FOREST)) { return HSAudioReverbPreset.숲; }
            else if (rev.Equals(HS_Audio_Lib.PRESET.CITY)) { return HSAudioReverbPreset.도시; }
            else if (rev.Equals(HS_Audio_Lib.PRESET.MOUNTAINS )) { return HSAudioReverbPreset.산; }
            else if (rev.Equals(HS_Audio_Lib.PRESET.QUARRY)) { return HSAudioReverbPreset.채석장; }
            else if (rev.Equals(HS_Audio_Lib.PRESET.PLAIN)) { return HSAudioReverbPreset.평원; }
            else if (rev.Equals(HS_Audio_Lib.PRESET.PARKINGLOT)) { return HSAudioReverbPreset.주차장; }
            else if (rev.Equals(HS_Audio_Lib.PRESET.SEWERPIPE)) { return HSAudioReverbPreset.하수도; }
            else if (rev.Equals(HS_Audio_Lib.PRESET.UNDERWATER)) { return HSAudioReverbPreset.지하수; }
            else {return HSAudioReverbPreset.사용자_설정;}
            //default: return CustomReverb;
        }
    }
}
namespace HS_Audio.Properties
{
    /// <summary>
    /// FMOD.Channel에 관한 구조체 입니다.
    /// </summary>
    [Serializable]
    public struct HSAudioChannelStruct
    {
        /// <summary>
        /// 주파수 입니다.
        /// </summary>
        public float Frequency;
        /// <summary>
        /// 팬 입니다.
        /// </summary>
        public float Pan;
        /// <summary>
        /// 게인값 입니다.
        /// </summary>
        public float LowPassGain;
        /// <summary>
        /// Direct값 최소: -10000, 최대: 1000, 기본: 0 / Room값 최소: -10000, 최대: 1000, 기본: 0 / Flage값: 
        /// REVERB_CHANNELFLAGS
        /// </summary>
        public HS_Audio_Lib.REVERB_CHANNELPROPERTIES ChannelReverbProperties;
        public HSAudioDelay Delay;
        public HSAudioSpeakerMix SpeakerMix;
    }
    [Serializable]
    public class HSAudioDelay
    {
        public HS_Audio_Lib.DELAYTYPE DelayType;
        public uint DelayHigh;
        public uint DelayLow;
    }
    [Serializable]
    public struct HSAudioSpeakerMix
    {
        public float FrontLeft;
        public float FrontRight;
        public float Center;
        public float LFE;
        public float BackLeft;
        public float BackRight;
        public float SideLeft;
        public float SideRight;
    }
    [Serializable]
    public struct HSAudio3DAttributes
    {
        public HS_Audio_Lib.VECTOR Pos;
        public HS_Audio_Lib.VECTOR Vel;
    }

}