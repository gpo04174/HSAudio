﻿using System;
using System.Collections.Generic;
using System.Threading;
using NAudio.Wave;

namespace HS_Audio
{
    
    public class HSWaveStream
    {
        public HSWaveStream(){}
        public HSWaveStream(int MaxLength){this.MaxLength=MaxLength;}
        public HSWaveStream(WaveFormat wf){ this.WaveFormat = wf;}
        public HSWaveStream(WaveFormat wf, int MaxLength){ this.WaveFormat = wf;this.MaxLength=MaxLength;}

        List<byte> Buf = new List<byte>();
        public int MaxLength = 1024;
        public WaveFormat WaveFormat{get;internal protected set;}

        private byte[] value24 = new byte[3];

        byte[] tmpbyte = new byte[0];

        public void WriteAsync(float[] sample) { ThreadPool.QueueUserWorkItem(new WaitCallback((object o)=>{foreach(float a in sample)Write(a);})); }
        public void WriteAsync(float sample) { ThreadPool.QueueUserWorkItem(new WaitCallback((object o)=>{Write(sample);})); }
        public void Write(float[] sample){foreach(float a in sample)Write(a);}
        public int Write(float sample)
        {
            try
            {
                if (_TotalLength < 1)
                {
                    if (WaveFormat.Encoding == WaveFormatEncoding.IeeeFloat){tmpbyte = BitConverter.GetBytes(sample);WritePosition += tmpbyte.Length;} 
                    else if (WaveFormat.BitsPerSample == 16)
                    {
                        tmpbyte = BitConverter.GetBytes((Int16)(Int16.MaxValue * sample));
                        WritePosition += 2; 
                    }
                    else if (WaveFormat.BitsPerSample == 24)
                    {
                        tmpbyte = BitConverter.GetBytes((Int32)(Int32.MaxValue * sample));
                        value24[0] = tmpbyte[1];
                        value24[1] = tmpbyte[2];
                        value24[2] = tmpbyte[3];
                        WritePosition += 3;
                    }
                    else if (WaveFormat.BitsPerSample == 32 && WaveFormat.Encoding == WaveFormatEncoding.Extensible)
                    {
                        tmpbyte = BitConverter.GetBytes(UInt16.MaxValue * (Int32)sample);
                        WritePosition += 4;
                    }
                    else {tmpbyte = BitConverter.GetBytes(sample);WritePosition += tmpbyte.Length;}
                    if (Buf.Count >= MaxLength){ Buf.RemoveRange(0, tmpbyte.Length);Buf.InsertRange(Buf.Count, tmpbyte);WritePosition=0;}//Buf.RemoveRange(0, tmpbyte.Length);
                    else Buf.AddRange(tmpbyte);
                    return tmpbyte.Length;
                }
                else {int a = Write(sample, WritePosition, false); if(a<0)WritePosition=0; return a;}
            }catch{Buf.Clear();if (_TotalLength < 1)Buf = new List<byte>(_TotalLength); WritePosition=0;return -1;}
        }
        public int Write(float sample, int Offset, bool Insert)
        {
            try
            {
                if (WaveFormat.Encoding == WaveFormatEncoding.IeeeFloat){tmpbyte = BitConverter.GetBytes(sample);WritePosition += tmpbyte.Length;} 
                else if (WaveFormat.BitsPerSample == 16)
                {
                    tmpbyte = BitConverter.GetBytes((Int16)(Int16.MaxValue * sample));
                    WritePosition += 2; //_Length += 2;
                }
                else if (WaveFormat.BitsPerSample == 24)
                {
                    tmpbyte = BitConverter.GetBytes((Int32)(Int32.MaxValue * sample));
                    value24[0] = tmpbyte[1];
                    value24[1] = tmpbyte[2];
                    value24[2] = tmpbyte[3];
                    WritePosition += 3; //_Length += 3;
                }
                else if (WaveFormat.BitsPerSample == 32 && WaveFormat.Encoding == WaveFormatEncoding.Extensible)
                {
                    tmpbyte = BitConverter.GetBytes(UInt16.MaxValue * (Int32)sample);
                    WritePosition += 4; //_Length += 4;
                }
                else {tmpbyte = BitConverter.GetBytes(sample);WritePosition += tmpbyte.Length;}
                if(Insert)Buf.RemoveRange(Offset, tmpbyte.Length);
                Buf.InsertRange(Offset, tmpbyte);
                return tmpbyte.Length;
            }
            catch{return -1;}
        }
        public int Write(byte Data)
        {
            if (_TotalLength < 1) {if(Buf.Count>=MaxLength)Buf.RemoveAt(0); Buf.Add(Data);WritePosition+=1;_Length +=1;return 1;}
            else {int a = Write(Data, WritePosition, false); if(a<0)WritePosition=0; return a;}
        }
        public new int Write(byte Data, int Offset, bool Insert)
        {
            try
            {
                if (Insert) Buf.RemoveAt(Offset);
                Buf.Insert(Offset, Data);
                WritePosition += 1;
                return 1;
            }catch{return -1;}
        }


        public int Position;
        public byte ReadNext()
        {
            Position++;
            try{return Buf[Position];}catch(ArgumentOutOfRangeException){Position=0; return Buf[0];}
        }
        public int Read(byte[] Bufer, int Offset)
        {
            try{if (Bufer != null && Bufer.Length > 0) for (int i = 0; i < Bufer.Length; i++) {Bufer[i] = Buf[Position + Offset + i];Position++;}}
            catch{Position = 0; for (int i = 0; i < Bufer.Length; i++) {Bufer[i] = Buf[Position + Offset + i];Position++;}}
            return Position;
        }
        public int Read(byte[] Bufer, int Offset, int Count)
        {
            byte[] tmp = new byte[Count];
            if (Buf.Count == 0) return 0;
            if(Position>=Buf.Count){Position=0;try{for (int i = 0; i < Count; i++)  {tmp[i] = Buf[Position + Offset]; Position++;}}catch{}}
            try  { for (int i = 0; i < Count; i++)  {tmp[i] = Buf[Position + Offset]; Position++; } }
            catch(ArgumentOutOfRangeException)
            {
                Position = 0; 
                try{for (int i = 0; i < Count; i++)  {tmp[i] = Buf[Position + Offset]; Position++;}}catch{}
            }
            Bufer = tmp;
            return Position;
        }

        public void Seek(int Offset){WritePosition = Offset;}

        public int WritePosition{get;private set;}
        int _Length;
        public new int Length{get{return Buf.Count;/*_Length;*/}}
        int _TotalLength;
        public int TotalLength{get{return Buf.Count;}}

        public new void Flush(){Flush(-1);}
        public new void Flush(int TotalLength)
        {
            Buf.Clear();// _TotalLength = TotalLength;
            if( TotalLength>0)Buf =new List<byte>(TotalLength);
            WritePosition = 0;
        }
        public void FlushAt(int Offset, int Count)
        {
            if (_TotalLength > 0) for (int i = Offset; i < Count + Offset; i++) Buf[i] = 0;
            else Buf.RemoveRange(Offset, Count);
        }
    }
}
