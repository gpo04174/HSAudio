﻿using CSCore.Streams;
using NAudio.CoreAudioApi;
using NAudio.Wave;
using System;
using System.Collections.Generic;
using System.Text;

namespace HS_Audio.Record
{
    class WaveInNAudio : IDisposable
    {
        public SoundInSource BaseWaveIn { get; private set; }
        IWaveIn common;
        WaveIn Wave;
        WasapiCapture WASAPI;
        WasapiLoopbackCapture WASAPILoopback;
        public WaveInNAudio(SoundInSource Source) { this.BaseWaveIn = Source; }
        public WaveInNAudio(WaveIn Wave) { this.Wave = Wave; BaseWaveIn = new SoundInSource(Wave); common = Wave; }
        public WaveInNAudio(WasapiCapture WASAPI) { this.WASAPI = WASAPI; BaseWaveIn = new SoundInSource(WASAPI); common = WASAPI; }
        public WaveInNAudio(WasapiLoopbackCapture WASAPILoopback) { this.WASAPILoopback = WASAPILoopback; BaseWaveIn = new SoundInSource(WASAPILoopback); common = WASAPILoopback; }

        #region Properties
        public WaveFormat WaveFormat
        {
            get
            {
                if (BaseWaveIn != null)
                {
                    if (BaseWaveIn.Base == null) BaseWaveIn.Base = common;
                    return BaseWaveIn.WaveFormat;
                }
                return null;
            }
            set
            {
                if (Wave != null) Wave.WaveFormat = value;
                if (WASAPI != null) WASAPI.WaveFormat = value;
                if (WASAPILoopback != null) WASAPILoopback.WaveFormat = value;
            }
        }

        public int DeviceNumber
        {
            get
            {
                if (Wave != null) return Wave.DeviceNumber;
                else return -1;
            }
            set {if (Wave != null) Wave.DeviceNumber = value; }
        }

        public int BufferMilliseconds
        {
            get
            {
                if (Wave != null) return Wave.BufferMilliseconds;
                else return -1;
            }
            set{if (Wave != null) Wave.BufferMilliseconds = value;}
        }

        /// <summary>
        /// Indicates recorded data is available 
        /// </summary>
        public event EventHandler<WaveInEventArgs> DataAvailable
        {
            add
            {
                if (BaseWaveIn != null)
                {
                    if (BaseWaveIn.Base == null) BaseWaveIn.Base = common;
                    BaseWaveIn.DataAvailable += value;
                }
            }
            remove
            {
                if (BaseWaveIn != null)
                {
                    if (BaseWaveIn.Base == null) BaseWaveIn.Base = common;
                    BaseWaveIn.DataAvailable -= value;
                }
            }
        }


        #region ReadOnly
        public bool IsStart { get; private set; }
        public bool IsDispose { get; private set; }
        #endregion
        #endregion


        #region Functions
        public void StartRecording()
        {
            if (BaseWaveIn != null)
            {
                if (BaseWaveIn.Base == null) BaseWaveIn.Base = common;
                BaseWaveIn.Base.StartRecording();
            }
            IsStart = true;
        }

        public void StopRecording()
        {
            if (BaseWaveIn != null)
            {
                if (BaseWaveIn.Base == null) BaseWaveIn.Base = common;
                BaseWaveIn.Base.StopRecording();
            }
            IsStart = false;
        }

        public NAudio.Mixer.MixerLine GetMixerLine()
        {
            if (Wave != null) return Wave.GetMixerLine();
            else throw new Exception("클래스가 NAudio.Wave.WaveIn 으로 초기화된 경우에만 사용할 수 있습니다.");
        }
        #endregion


        #region Override
        public void Dispose()
        {
            if (BaseWaveIn != null) { BaseWaveIn.Dispose(true);}
            IsDispose = true;
        }
        #endregion
    }
}
