﻿using System;
using System.Collections.Generic;

using System.Text;
using HS_Audio.DSP;
using HS_Audio.Bundles;

namespace HS_Audio.Recoder
{
    public class HSAudioRecoderHelper : HSAudioHelper
    {
        public HSAudioRecoderHelper(SoundDeviceFormat format)
        {
            HelperEx = new HSAudioHelperEx();
            HelperEx.Result = HS_Audio_Lib.RESULT.OK;
            //FMOD.RESULT result;

            /*
                Create a System object and initialize.
            */
            HelperEx.Result = HS_Audio_Lib.Factory.System_Create(ref system);
            ERRCHECK(HelperEx.Result);

            /*
            HelperEx.Result = system.getVersion(ref version);
            ERRCHECK(HelperEx.Result);
            if (version < FMOD.VERSION.number)
            {
                throw new Exception("Error!  You are using an old version of FMOD " + version.ToString("X") + ".  This program requires " + FMOD.VERSION.number.ToString("X") + ".");
            }*/
            HelperEx.Result =
            system.setSoftwareFormat(format.Samplerate, format.SoundFormat,
                                     format.OutputChannel, format.MaxInputChannel,
                                     format.ResamplerMethod);

            HelperEx.Result = system.init(32, HS_Audio_Lib.INITFLAGS.NORMAL, (IntPtr)null);
            if (HelperEx.Result == HS_Audio_Lib.RESULT.OK) PlayStatus = PlayingStatus.Ready; HelperEx = new HSAudioHelperEx(this); dsp = new HSAudioDSPHelper(this, true);
            //timer1.Start();
        }
        public HSAudioRecoderHelper(SoundDeviceFormat format, HS_Audio_Lib.CREATESOUNDEXINFO info)
        {
            SoundInformation=info;
            HelperEx = new HSAudioHelperEx();
            HelperEx.Result = HS_Audio_Lib.RESULT.OK;
            //FMOD.RESULT result;

            /*
                Create a System object and initialize.
            */
            HelperEx.Result = HS_Audio_Lib.Factory.System_Create(ref system);
            ERRCHECK(HelperEx.Result);

            /*
            HelperEx.Result = system.getVersion(ref version);
            ERRCHECK(HelperEx.Result);
            if (version < FMOD.VERSION.number)
            {
                throw new Exception("Error!  You are using an old version of FMOD " + version.ToString("X") + ".  This program requires " + FMOD.VERSION.number.ToString("X") + ".");
            }*/
            HelperEx.Result =
            system.setSoftwareFormat(format.Samplerate, format.SoundFormat,
                                     format.OutputChannel, format.MaxInputChannel,
                                     format.ResamplerMethod);

            HelperEx.Result = system.init(32, HS_Audio_Lib.INITFLAGS.NORMAL, (IntPtr)null);
            if (HelperEx.Result == HS_Audio_Lib.RESULT.OK) PlayStatus = PlayingStatus.Ready; HelperEx = new HSAudioHelperEx(this); dsp = new HSAudioDSPHelper(this, true);
            system.createStream((string)null, HS_Audio_Lib.MODE._2D, ref SoundInformation, ref sound);
            //timer1.Start();
        }

        public HS_Audio_Lib.CREATESOUNDEXINFO SoundInformation = new HS_Audio_Lib.CREATESOUNDEXINFO()
        {
            format = HS_Audio_Lib.SOUND_FORMAT.PCM16,
            numchannels = 2,
            defaultfrequency = 44100,
            length = (uint)(44100 * 2 * 2 * 5)
        };

        public void StartRecording()
        {

        }
        public void StopRecording()
        {

        }

        public new string MusicPath { get; set; }
    }
}
