﻿using NAudio.Wave;

namespace HS_Audio.Record
{
    class WaveInCapNAudio
    {
        string Name;
        WaveInCapabilities Wave;

        protected internal WaveInCapNAudio(string Name)
        {
            this.Name = Name;
        }
        public WaveInCapNAudio(WaveInCapabilities Wave)
        {
            this.Wave = Wave;
        }


        /// <summary>
        /// The product name
        /// </summary>
        public string ProductName
        {
            get{ return Name == null ? Wave.ProductName : Name; }
            protected internal set { Name = value; }
        }

        public WaveInCapabilities getWaveInCapabilities() { return Wave; }

        public override string ToString()
        {
            return Name == null ? Wave.ToString() : Name;
        }
    }
}
