﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;
using System.IO;
using System.Threading;
using HS_Audio.Sound.Wave;
using HS_Audio.Bundles;

namespace HS_Audio.Recoder
{
    public class SaveWaveDSPBuffer:MemoryStream
    {
        public SoundDeviceFormat Format { get; internal set; }
        internal HSAudioHelper Helper;
        Thread th;
        ThreadStart thstart;
        uint Length1, datalength;

        uint lastrecordpos, recordpos;

        public HS_Audio_Lib.DSP_READCALLBACK dspreadcallback;
        internal HS_Audio_Lib.DSP dsp_static;
        internal HS_Audio_Lib.DSP_DESCRIPTION dspdesc;

        private DSP.DSPCallBack cb = new DSP.DSPCallBack();
        public unsafe delegate void READCALLBACKOutBufferEventHandler(float* outbuffer, HS_Audio.DSP.DSPCallBack Value);
        public event READCALLBACKOutBufferEventHandler READCALLBACKOutBuffer;

        public HS_Audio_Lib.RESULT READCALLBACK(ref HS_Audio_Lib.DSP_STATE dsp_state, IntPtr inbuf, IntPtr outbuf, uint length, int inchannels, int outchannels)
        {
            uint count = 0;
            int count2 = 0;
            IntPtr thisdspraw = dsp_state.instance;
            if (dsp_static != null) { dsp_static.setRaw(thisdspraw); }
            else { dsp_static = new HS_Audio_Lib.DSP(); dsp_static.setRaw(thisdspraw); }
            cb.dsp_state = dsp_state; cb.inbuf = inbuf; cb.inchannels = inchannels; cb.length = length; cb.outbuf = outbuf; cb.outchannels = outchannels;
            unsafe
            {
                float* inbuffer = (float*)inbuf.ToPointer();
                float* outbuffer = (float*)outbuf.ToPointer();

                for (count = 0; count < length; count++)
                {
                    for (count2 = 0; count2 < outchannels; count2++)
                    {
                        outbuffer[(count * outchannels) + count2] = inbuffer[(count * inchannels) + count2];
                    }
                }
                if (READCALLBACKOutBuffer != null) READCALLBACKOutBuffer(outbuffer, cb);
            }

            return HS_Audio_Lib.RESULT.OK;
        }
        public HS_Audio_Lib.RESULT READCALLBACK(ref HS_Audio_Lib.DSP_STATE dsp_state, HS_Audio.DSP.DSPCallBack Value)
        {
            uint count = 0;
            int count2 = 0;
            IntPtr thisdspraw = dsp_state.instance;
            if (dsp_static != null) { dsp_static.setRaw(thisdspraw); }
            else { dsp_static = new HS_Audio_Lib.DSP(); dsp_static.setRaw(thisdspraw); }

            unsafe
            {
                float* inbuffer = (float*)Value.inbuf.ToPointer();
                float* outbuffer = (float*)Value.outbuf.ToPointer();

                for (count = 0; count < Value.length; count++)
                {
                    for (count2 = 0; count2 < Value.outchannels; count2++)
                    {
                        outbuffer[(count * Value.outchannels) + count2] = inbuffer[(count * Value.inchannels) + count2];
                    }
                }
            }

            return HS_Audio_Lib.RESULT.OK;
        }

        public SaveWaveDSPBuffer() { PUBLIC(null); }
        public SaveWaveDSPBuffer(HSAudioHelper Helper)
        { PUBLIC(Helper); Format = Helper.DeviceFormat; WriteWavHeader(0); }
        public SaveWaveDSPBuffer(HSAudioHelper Helper, FmtChunk fmtChunk, DataChunk dataChunk, WavHeader wavHeader, RiffChunk riffChunk)
        { PUBLIC(Helper); Format = Helper.DeviceFormat; WriteWavHeader(0, fmtChunk, dataChunk, wavHeader, riffChunk); }
        internal SaveWaveDSPBuffer(HSAudioHelper Helper, SoundDeviceFormat Format)
        {PUBLIC(Helper); this.Format = Format; }
        internal SaveWaveDSPBuffer(HSAudioHelper Helper, SoundDeviceFormat Format, FmtChunk fmtChunk, DataChunk dataChunk, WavHeader wavHeader, RiffChunk riffChunk)
        {PUBLIC(Helper); WriteWavHeader(0, fmtChunk, dataChunk, wavHeader, riffChunk);this.Format = Format;}
        void PUBLIC(HSAudioHelper Helper)
        {
            if(Helper!=null)this.Helper = Helper;
            thstart = new ThreadStart(WriteWave);
            th = new Thread(thstart) { Name = "웨이브 데이터 쓰기 스레드" };
            this.Format = Helper.DeviceFormat;
            if (Helper != null) Length1 = Helper.GetTotalTick(HS_Audio_Lib.TIMEUNIT.PCMBYTES);
            Helper.dsp.dspreadcallback += new HS_Audio_Lib.DSP_READCALLBACK(READCALLBACK);
            unsafe { READCALLBACKOutBuffer = new READCALLBACKOutBufferEventHandler(WriteWave1); }
        }

        public void StartWrite(){try { th.Start(); }catch { } }
        public void PauseWrite(){th.Suspend();}

        public Stream BaseStream { get { return this; } }

        /// <summary>
        /// 이 함수를 호출하게 되면 웨이브가 씌여지고 기존의 웨이브 스트림에는 더이상 쓸 수 없게됩니다. (Dispose와 같음)
        /// </summary>
        public Stream StopAndDispose()
        {
            th.Abort();
            MemoryStream s = new MemoryStream(this.GetBuffer());
            WriteWavHeader(datalength); this.Close(); return s;
        }

        /// <summary>
        /// 현재 웨이브 데이터를 반환합니다.
        /// </summary>
        /// <param name="Close">True면 데이터 헤더가 씌여진 형태이고 False면 데이터 헤더가 씌여지지 않은채로 반환합니다.</param>
        /// <returns></returns>
        public byte[] Data(/*bool Close*/)
        {
            //if(Close)WriteWavHeader(datalength);
            return this.GetBuffer();
        }

        public void WriteHeader() { WriteWavHeader(datalength); }

        private void WriteWave()
        {
            recordpos = Helper.GetCurrentTick(HS_Audio_Lib.TIMEUNIT.RAWBYTES);
            if (recordpos != lastrecordpos)
            {
                IntPtr ptr1 = IntPtr.Zero, ptr2 = IntPtr.Zero;
                uint blocklength;
                uint len1 = 0, len2 = 0;

                blocklength = recordpos - lastrecordpos;
                if (blocklength < 0)
                {blocklength += Length1;}

                /*
                    Lock the sound to get access to the raw data.
                */
                result = Helper.sound.@lock(lastrecordpos * 4, blocklength * 4, ref ptr1, ref ptr2, ref len1, ref len2); /* *4 = stereo 16bit.  1 sample = 4 bytes. */

                /*
                    Write it to disk.
                */
                if (ptr1 != IntPtr.Zero && len1 > 0)
                {
                    byte[] buf = new byte[len1];

                    Marshal.Copy(ptr1, buf, 0, (int)len1);

                    datalength += len1;

                    Write(buf, 0, (int)len1);

                }
                if (ptr2 != IntPtr.Zero && len2 > 0)
                {
                    byte[] buf = new byte[len2];

                    Marshal.Copy(ptr2, buf, 0, (int)len2);

                    datalength += len2;

                    Write(buf, 0, (int)len2);
                }

                /*
                    Unlock the sound to allow FMOD to use it again.
                */
              result = Helper.sound.unlock(ptr1, ptr1, len1, len2);
            }

            lastrecordpos = recordpos;
            if (Helper.system != null){Helper.system.update(); }
        }

        private unsafe void WriteWave1(float* outbuf, HS_Audio.DSP.DSPCallBack Value)
        {

        }

        HS_Audio_Lib.RESULT result;
        public MemoryStream WriteWaveNew(SaveWaveDSPBuffer stream)
        {
            MemoryStream ms = new MemoryStream(this.GetBuffer());
            HS_Audio_Lib.SOUND_TYPE type = HS_Audio_Lib.SOUND_TYPE.UNKNOWN;
            HS_Audio_Lib.SOUND_FORMAT format = HS_Audio_Lib.SOUND_FORMAT.NONE;
            int channels = 0, bits = 0, temp1 = 0;
            float rate = 0.0f;
            float temp = 0.0f;

            if (Helper.sound == null) { return null; }

            Helper.sound.getFormat(ref type, ref format, ref channels, ref bits);
            Helper.sound.getDefaults(ref rate, ref temp, ref temp, ref temp1);

            ms.Seek(0, SeekOrigin.Begin);

            FmtChunk fmtChunk = new FmtChunk();
            DataChunk dataChunk = new DataChunk();
            WavHeader wavHeader = new WavHeader();
            RiffChunk riffChunk = new RiffChunk();

            fmtChunk.chunk = new RiffChunk();
            fmtChunk.chunk.id = new char[4];
            fmtChunk.chunk.id[0] = 'f';
            fmtChunk.chunk.id[1] = 'm';
            fmtChunk.chunk.id[2] = 't';
            fmtChunk.chunk.id[3] = ' ';
            fmtChunk.chunk.size = Marshal.SizeOf(fmtChunk) - Marshal.SizeOf(riffChunk);
            fmtChunk.wFormatTag = 1;
            fmtChunk.nChannels = (ushort)channels;
            fmtChunk.nSamplesPerSec = (uint)rate;
            fmtChunk.nAvgBytesPerSec = (uint)(rate * channels * bits / 8);
            fmtChunk.nBlockAlign = (ushort)(1 * channels * bits / 8);
            fmtChunk.wBitsPerSample = (ushort)bits;

            dataChunk.chunk = new RiffChunk();
            dataChunk.chunk.id = new char[4];
            dataChunk.chunk.id[0] = 'd';
            dataChunk.chunk.id[1] = 'a';
            dataChunk.chunk.id[2] = 't';
            dataChunk.chunk.id[3] = 'a';
            dataChunk.chunk.size = stream.datalength ;

            wavHeader.chunk = new RiffChunk();
            wavHeader.chunk.id = new char[4];
            wavHeader.chunk.id[0] = 'R';
            wavHeader.chunk.id[1] = 'I';
            wavHeader.chunk.id[2] = 'F';
            wavHeader.chunk.id[3] = 'F';
            wavHeader.chunk.size = Marshal.SizeOf(fmtChunk) + Marshal.SizeOf(riffChunk) + datalength;
            wavHeader.rifftype = new char[4];
            wavHeader.rifftype[0] = 'W';
            wavHeader.rifftype[1] = 'A';
            wavHeader.rifftype[2] = 'V';
            wavHeader.rifftype[3] = 'E';

            /*
                Write out the WAV header.
            */
            IntPtr wavHeaderPtr = Marshal.AllocHGlobal(Marshal.SizeOf(wavHeader));
            IntPtr fmtChunkPtr = Marshal.AllocHGlobal(Marshal.SizeOf(fmtChunk));
            IntPtr dataChunkPtr = Marshal.AllocHGlobal(Marshal.SizeOf(dataChunk));
            byte[] wavHeaderBytes = new byte[Marshal.SizeOf(wavHeader)];
            byte[] fmtChunkBytes = new byte[Marshal.SizeOf(fmtChunk)];
            byte[] dataChunkBytes = new byte[Marshal.SizeOf(dataChunk)];

            Marshal.StructureToPtr(wavHeader, wavHeaderPtr, false);
            Marshal.Copy(wavHeaderPtr, wavHeaderBytes, 0, Marshal.SizeOf(wavHeader));

            Marshal.StructureToPtr(fmtChunk, fmtChunkPtr, false);
            Marshal.Copy(fmtChunkPtr, fmtChunkBytes, 0, Marshal.SizeOf(fmtChunk));

            Marshal.StructureToPtr(dataChunk, dataChunkPtr, false);
            Marshal.Copy(dataChunkPtr, dataChunkBytes, 0, Marshal.SizeOf(dataChunk));

            ms.Write(wavHeaderBytes, 0, Marshal.SizeOf(wavHeader));
            ms.Write(fmtChunkBytes, 0, Marshal.SizeOf(fmtChunk));
            ms.Write(dataChunkBytes, 0, Marshal.SizeOf(dataChunk));
            return ms;
        }
        private void WriteWavHeader(long length)
        {
            HS_Audio_Lib.SOUND_TYPE type = HS_Audio_Lib.SOUND_TYPE.UNKNOWN;
            HS_Audio_Lib.SOUND_FORMAT format = HS_Audio_Lib.SOUND_FORMAT.NONE;
            int channels = 0, bits = 0, temp1 = 0;
            float rate = 0.0f;
            float temp = 0.0f;

            if (Helper.sound == null) { return; }

            Helper.sound.getFormat(ref type, ref format, ref channels, ref bits);
            Helper.sound.getDefaults(ref rate, ref temp, ref temp, ref temp1);

            Seek(0, SeekOrigin.Begin);

            FmtChunk fmtChunk = new FmtChunk();
            DataChunk dataChunk = new DataChunk();
            WavHeader wavHeader = new WavHeader();
            RiffChunk riffChunk = new RiffChunk();

            fmtChunk.chunk = new RiffChunk();
            fmtChunk.chunk.id = new char[4];
            fmtChunk.chunk.id[0] = 'f';
            fmtChunk.chunk.id[1] = 'm';
            fmtChunk.chunk.id[2] = 't';
            fmtChunk.chunk.id[3] = ' ';
            fmtChunk.chunk.size = Marshal.SizeOf(fmtChunk) - Marshal.SizeOf(riffChunk);
            fmtChunk.wFormatTag = 1;
            fmtChunk.nChannels = (ushort)channels;
            fmtChunk.nSamplesPerSec = (uint)rate;
            fmtChunk.nAvgBytesPerSec = (uint)(rate * channels * bits / 8);
            fmtChunk.nBlockAlign = (ushort)(1 * channels * bits / 8);
            fmtChunk.wBitsPerSample = (ushort)bits;

            dataChunk.chunk = new RiffChunk();
            dataChunk.chunk.id = new char[4];
            dataChunk.chunk.id[0] = 'd';
            dataChunk.chunk.id[1] = 'a';
            dataChunk.chunk.id[2] = 't';
            dataChunk.chunk.id[3] = 'a';
            dataChunk.chunk.size = length;

            wavHeader.chunk = new RiffChunk();
            wavHeader.chunk.id = new char[4];
            wavHeader.chunk.id[0] = 'R';
            wavHeader.chunk.id[1] = 'I';
            wavHeader.chunk.id[2] = 'F';
            wavHeader.chunk.id[3] = 'F';
            wavHeader.chunk.size = Marshal.SizeOf(fmtChunk) + Marshal.SizeOf(riffChunk) + length;
            wavHeader.rifftype = new char[4];
            wavHeader.rifftype[0] = 'W';
            wavHeader.rifftype[1] = 'A';
            wavHeader.rifftype[2] = 'V';
            wavHeader.rifftype[3] = 'E';

            /*
                Write out the WAV header.
            */
            IntPtr wavHeaderPtr = Marshal.AllocHGlobal(Marshal.SizeOf(wavHeader));
            IntPtr fmtChunkPtr = Marshal.AllocHGlobal(Marshal.SizeOf(fmtChunk));
            IntPtr dataChunkPtr = Marshal.AllocHGlobal(Marshal.SizeOf(dataChunk));
            byte[] wavHeaderBytes = new byte[Marshal.SizeOf(wavHeader)];
            byte[] fmtChunkBytes = new byte[Marshal.SizeOf(fmtChunk)];
            byte[] dataChunkBytes = new byte[Marshal.SizeOf(dataChunk)];

            Marshal.StructureToPtr(wavHeader, wavHeaderPtr, false);
            Marshal.Copy(wavHeaderPtr, wavHeaderBytes, 0, Marshal.SizeOf(wavHeader));

            Marshal.StructureToPtr(fmtChunk, fmtChunkPtr, false);
            Marshal.Copy(fmtChunkPtr, fmtChunkBytes, 0, Marshal.SizeOf(fmtChunk));

            Marshal.StructureToPtr(dataChunk, dataChunkPtr, false);
            Marshal.Copy(dataChunkPtr, dataChunkBytes, 0, Marshal.SizeOf(dataChunk));

            Write(wavHeaderBytes, 0, Marshal.SizeOf(wavHeader));
            Write(fmtChunkBytes, 0, Marshal.SizeOf(fmtChunk));
            Write(dataChunkBytes, 0, Marshal.SizeOf(dataChunk));
        }
        private void WriteWavHeader(long length, FmtChunk fmtChunk, DataChunk dataChunk, WavHeader wavHeader, RiffChunk riffChunk)
        {
            HS_Audio_Lib.SOUND_TYPE type = HS_Audio_Lib.SOUND_TYPE.UNKNOWN;
            HS_Audio_Lib.SOUND_FORMAT format = HS_Audio_Lib.SOUND_FORMAT.NONE;
            int channels = 0, bits = 0, temp1 = 0;
            float rate = 0.0f;
            float temp = 0.0f;

            if (Helper.sound == null) { return; }

            Helper.sound.getFormat(ref type, ref format, ref channels, ref bits);
            Helper.sound.getDefaults(ref rate, ref temp, ref temp, ref temp1);

            Seek(0, SeekOrigin.Begin);

            /*
                Write out the WAV header.
            */
            IntPtr wavHeaderPtr = Marshal.AllocHGlobal(Marshal.SizeOf(wavHeader));
            IntPtr fmtChunkPtr = Marshal.AllocHGlobal(Marshal.SizeOf(fmtChunk));
            IntPtr dataChunkPtr = Marshal.AllocHGlobal(Marshal.SizeOf(dataChunk));
            byte[] wavHeaderBytes = new byte[Marshal.SizeOf(wavHeader)];
            byte[] fmtChunkBytes = new byte[Marshal.SizeOf(fmtChunk)];
            byte[] dataChunkBytes = new byte[Marshal.SizeOf(dataChunk)];

            Marshal.StructureToPtr(wavHeader, wavHeaderPtr, false);
            Marshal.Copy(wavHeaderPtr, wavHeaderBytes, 0, Marshal.SizeOf(wavHeader));

            Marshal.StructureToPtr(fmtChunk, fmtChunkPtr, false);
            Marshal.Copy(fmtChunkPtr, fmtChunkBytes, 0, Marshal.SizeOf(fmtChunk));

            Marshal.StructureToPtr(dataChunk, dataChunkPtr, false);
            Marshal.Copy(dataChunkPtr, dataChunkBytes, 0, Marshal.SizeOf(dataChunk));

            Write(wavHeaderBytes, 0, Marshal.SizeOf(wavHeader));
            Write(fmtChunkBytes, 0, Marshal.SizeOf(fmtChunk));
            Write(dataChunkBytes, 0, Marshal.SizeOf(dataChunk));
        }

        /*
        public static Stream WriteWavHeader(FMODHelper Helper)
        {
            MemoryStream ms = new MemoryStream();
            uint length = Helper.GetCurrentTick(FMOD.TIMEUNIT.RAWBYTES);
            FMOD.SOUND_TYPE type = FMOD.SOUND_TYPE.UNKNOWN;
            FMOD.SOUND_FORMAT format = FMOD.SOUND_FORMAT.NONE;
            int channels = 0, bits = 0, temp1 = 0;
            float rate = 0.0f;
            float temp = 0.0f;

            if (Helper.sound == null) { return null; }

            Helper.sound.getFormat(ref type, ref format, ref channels, ref bits);
            Helper.sound.getDefaults(ref rate, ref temp, ref temp, ref temp1);

            ms.Seek(0, SeekOrigin.Begin);

            FmtChunk fmtChunk = new FmtChunk();
            DataChunk dataChunk = new DataChunk();
            WavHeader wavHeader = new WavHeader();
            RiffChunk riffChunk = new RiffChunk();

            fmtChunk.chunk = new RiffChunk();
            fmtChunk.chunk.id = new char[4];
            fmtChunk.chunk.id[0] = 'f';
            fmtChunk.chunk.id[1] = 'm';
            fmtChunk.chunk.id[2] = 't';
            fmtChunk.chunk.id[3] = ' ';
            fmtChunk.chunk.size = Marshal.SizeOf(fmtChunk) - Marshal.SizeOf(riffChunk);
            fmtChunk.wFormatTag = 1;
            fmtChunk.nChannels = (ushort)channels;
            fmtChunk.nSamplesPerSec = (uint)rate;
            fmtChunk.nAvgBytesPerSec = (uint)(rate * channels * bits / 8);
            fmtChunk.nBlockAlign = (ushort)(1 * channels * bits / 8);
            fmtChunk.wBitsPerSample = (ushort)bits;

            dataChunk.chunk = new RiffChunk();
            dataChunk.chunk.id = new char[4];
            dataChunk.chunk.id[0] = 'd';
            dataChunk.chunk.id[1] = 'a';
            dataChunk.chunk.id[2] = 't';
            dataChunk.chunk.id[3] = 'a';
            dataChunk.chunk.size = (int)length;

            wavHeader.chunk = new RiffChunk();
            wavHeader.chunk.id = new char[4];
            wavHeader.chunk.id[0] = 'R';
            wavHeader.chunk.id[1] = 'I';
            wavHeader.chunk.id[2] = 'F';
            wavHeader.chunk.id[3] = 'F';
            wavHeader.chunk.size = (int)(Marshal.SizeOf(fmtChunk) + Marshal.SizeOf(riffChunk) + length);
            wavHeader.rifftype = new char[4];
            wavHeader.rifftype[0] = 'W';
            wavHeader.rifftype[1] = 'A';
            wavHeader.rifftype[2] = 'V';
            wavHeader.rifftype[3] = 'E';

            //Write out the WAV header.
         
            IntPtr wavHeaderPtr = Marshal.AllocHGlobal(Marshal.SizeOf(wavHeader));
            IntPtr fmtChunkPtr = Marshal.AllocHGlobal(Marshal.SizeOf(fmtChunk));
            IntPtr dataChunkPtr = Marshal.AllocHGlobal(Marshal.SizeOf(dataChunk));
            byte[] wavHeaderBytes = new byte[Marshal.SizeOf(wavHeader)];
            byte[] fmtChunkBytes = new byte[Marshal.SizeOf(fmtChunk)];
            byte[] dataChunkBytes = new byte[Marshal.SizeOf(dataChunk)];

            Marshal.StructureToPtr(wavHeader, wavHeaderPtr, false);
            Marshal.Copy(wavHeaderPtr, wavHeaderBytes, 0, Marshal.SizeOf(wavHeader));

            Marshal.StructureToPtr(fmtChunk, fmtChunkPtr, false);
            Marshal.Copy(fmtChunkPtr, fmtChunkBytes, 0, Marshal.SizeOf(fmtChunk));

            Marshal.StructureToPtr(dataChunk, dataChunkPtr, false);
            Marshal.Copy(dataChunkPtr, dataChunkBytes, 0, Marshal.SizeOf(dataChunk));

            ms.Write(wavHeaderBytes, 0, Marshal.SizeOf(wavHeader));
            ms.Write(fmtChunkBytes, 0, Marshal.SizeOf(fmtChunk));
            ms.Write(dataChunkBytes, 0, Marshal.SizeOf(dataChunk));
            return ms;
        }*/
        
        /*
        public static Stream WriteWavHeader(FMODHelper Helper, int Length)
        {
            MemoryStream ms = new MemoryStream();
            uint length = Helper.GetCurrentTick(FMOD.TIMEUNIT.RAWBYTES);
            FMOD.SOUND_TYPE type = FMOD.SOUND_TYPE.UNKNOWN;
            FMOD.SOUND_FORMAT format = FMOD.SOUND_FORMAT.NONE;
            int channels = 0, bits = 0, temp1 = 0;
            float rate = 0.0f;
            float temp = 0.0f;

            if (Helper.sound == null) { return null; }

            Helper.sound.getFormat(ref type, ref format, ref channels, ref bits);
            Helper.sound.getDefaults(ref rate, ref temp, ref temp, ref temp1);

            ms.Seek(0, SeekOrigin.Begin);

            FmtChunk fmtChunk = new FmtChunk();
            DataChunk dataChunk = new DataChunk();
            WavHeader wavHeader = new WavHeader();
            RiffChunk riffChunk = new RiffChunk();

            fmtChunk.chunk = new RiffChunk();
            fmtChunk.chunk.id = new char[4];
            fmtChunk.chunk.id[0] = 'f';
            fmtChunk.chunk.id[1] = 'm';
            fmtChunk.chunk.id[2] = 't';
            fmtChunk.chunk.id[3] = ' ';
            fmtChunk.chunk.size = Marshal.SizeOf(fmtChunk) - Marshal.SizeOf(riffChunk);
            fmtChunk.wFormatTag = 1;
            fmtChunk.nChannels = (ushort)channels;
            fmtChunk.nSamplesPerSec = (uint)rate;
            fmtChunk.nAvgBytesPerSec = (uint)(rate * channels * bits / 8);
            fmtChunk.nBlockAlign = (ushort)(1 * channels * bits / 8);
            fmtChunk.wBitsPerSample = (ushort)bits;

            dataChunk.chunk = new RiffChunk();
            dataChunk.chunk.id = new char[4];
            dataChunk.chunk.id[0] = 'd';
            dataChunk.chunk.id[1] = 'a';
            dataChunk.chunk.id[2] = 't';
            dataChunk.chunk.id[3] = 'a';
            dataChunk.chunk.size = (int)length;

            wavHeader.chunk = new RiffChunk();
            wavHeader.chunk.id = new char[4];
            wavHeader.chunk.id[0] = 'R';
            wavHeader.chunk.id[1] = 'I';
            wavHeader.chunk.id[2] = 'F';
            wavHeader.chunk.id[3] = 'F';
            wavHeader.chunk.size = (int)(Marshal.SizeOf(fmtChunk) + Marshal.SizeOf(riffChunk) + length);
            wavHeader.rifftype = new char[4];
            wavHeader.rifftype[0] = 'W';
            wavHeader.rifftype[1] = 'A';
            wavHeader.rifftype[2] = 'V';
            wavHeader.rifftype[3] = 'E';

            //Write out the WAV header.
         
            IntPtr wavHeaderPtr = Marshal.AllocHGlobal(Marshal.SizeOf(wavHeader));
            IntPtr fmtChunkPtr = Marshal.AllocHGlobal(Marshal.SizeOf(fmtChunk));
            IntPtr dataChunkPtr = Marshal.AllocHGlobal(Marshal.SizeOf(dataChunk));
            byte[] wavHeaderBytes = new byte[Marshal.SizeOf(wavHeader)];
            byte[] fmtChunkBytes = new byte[Marshal.SizeOf(fmtChunk)];
            byte[] dataChunkBytes = new byte[Marshal.SizeOf(dataChunk)];

            Marshal.StructureToPtr(wavHeader, wavHeaderPtr, false);
            Marshal.Copy(wavHeaderPtr, wavHeaderBytes, 0, Marshal.SizeOf(wavHeader));

            Marshal.StructureToPtr(fmtChunk, fmtChunkPtr, false);
            Marshal.Copy(fmtChunkPtr, fmtChunkBytes, 0, Marshal.SizeOf(fmtChunk));

            Marshal.StructureToPtr(dataChunk, dataChunkPtr, false);
            Marshal.Copy(dataChunkPtr, dataChunkBytes, 0, Marshal.SizeOf(dataChunk));

            ms.Write(wavHeaderBytes, 0, Marshal.SizeOf(wavHeader));
            ms.Write(fmtChunkBytes, 0, Marshal.SizeOf(fmtChunk));
            ms.Write(dataChunkBytes, 0, Marshal.SizeOf(dataChunk));
            return ms;
        }*/


        /*
WAV Structures 
*/
        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct RiffChunk
        {
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 4)]
            public char[] id;
            public long size;
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct FmtChunk
        {
            public RiffChunk chunk;
            public ushort wFormatTag;    /* format type  */
            public ushort nChannels;    /* number of channels (i.e. mono, stereo...)  */
            public uint nSamplesPerSec;    /* sample rate  */
            public uint nAvgBytesPerSec;    /* for buffer estimation  */
            public ushort nBlockAlign;    /* block size of data  */
            public ushort wBitsPerSample;    /* number of bits per sample of mono data */
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct DataChunk
        {
            public RiffChunk chunk;
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct WavHeader
        {
            public RiffChunk chunk;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 4)]
            public char[] rifftype;
        }
    }

    public class SaveWaveWaveData
    {
        HSAudioHelper Helper;
        Thread th;
        ParameterizedThreadStart pts;
        AudioWriter aw;
        public PcmSoundFormat Format = new PcmSoundFormat(44100, 16, 2);
        public SaveWaveWaveData(HS_Audio.HSAudioHelper Helper)
        {
            this.Helper = Helper;
            pts = new ParameterizedThreadStart(th_start);
            th = new Thread(pts);
            //th = new Thread();
            th = new Thread(pts);
        }
        public SaveWaveWaveData(HS_Audio.HSAudioHelper Helper, PcmSoundFormat Format)
        {
            this.Helper = Helper;
            pts = new ParameterizedThreadStart(th_start);
            th = new Thread(pts);
            //th = new Thread();
            AudioWriterConfig wc = new AudioWriterConfig();
        }
        public SaveWaveWaveData(HS_Audio.HSAudioHelper Helper, int Rate=44100, short Bits=16, short Channel=2)
        {
            this.Helper = Helper;
            //th = new Thread();
            pts = new ParameterizedThreadStart(th_start);
            th = new Thread(pts);
            Format = new PcmSoundFormat(Rate, Bits, Channel);
            AudioWriterConfig wc = new AudioWriterConfig();
        }

        public void th_start(object o)
        {

        }

    }

    public class PcmSoundFormat:IEquatable<PcmSoundFormat>
    {
        public static readonly PcmSoundFormat Pcm8kHz8bitMono = new PcmSoundFormat(8000, 8, 1);
        public static readonly PcmSoundFormat Pcm8kHz8bitStereo = new PcmSoundFormat(8000, 8, 2);
        public static readonly PcmSoundFormat Pcm8kHz16bitMono = new PcmSoundFormat(8000, 16, 1);
        public static readonly PcmSoundFormat Pcm8kHz16bitStereo = new PcmSoundFormat(8000, 16, 2);

        public static readonly PcmSoundFormat Pcm11kHz8bitMono = new PcmSoundFormat(11025, 8, 1);
        public static readonly PcmSoundFormat Pcm11kHz8bitStereo = new PcmSoundFormat(11025, 8, 2);
        public static readonly PcmSoundFormat Pcm11kHz16bitMono = new PcmSoundFormat(11025, 16, 1);
        public static readonly PcmSoundFormat Pcm11kHz16bitStereo = new PcmSoundFormat(11025, 16, 2);

        public static readonly PcmSoundFormat Pcm22kHz8bitMono = new PcmSoundFormat(22050, 8, 1);
        public static readonly PcmSoundFormat Pcm22kHz8bitStereo = new PcmSoundFormat(22050, 8, 2);
        public static readonly PcmSoundFormat Pcm22kHz16bitMono = new PcmSoundFormat(22050, 16, 1);
        public static readonly PcmSoundFormat Pcm22kHz16bitStereo = new PcmSoundFormat(22050, 16, 2);

        public static readonly PcmSoundFormat Pcm44kHz8bitMono = new PcmSoundFormat(44100, 8, 1);
        public static readonly PcmSoundFormat Pcm44kHz8bitStereo = new PcmSoundFormat(44100, 8, 2);
        public static readonly PcmSoundFormat Pcm44kHz16bitMono = new PcmSoundFormat(44100, 16, 1);
        public static readonly PcmSoundFormat Pcm44kHz16bitStereo = new PcmSoundFormat(44100, 16, 2);

        public static readonly PcmSoundFormat Pcm48kHz8bitMono = new PcmSoundFormat(48000, 8, 1);
        public static readonly PcmSoundFormat Pcm48kHz8bitStereo = new PcmSoundFormat(48000, 8, 2);
        public static readonly PcmSoundFormat Pcm48kHz16bitMono = new PcmSoundFormat(48000, 16, 1);
        public static readonly PcmSoundFormat Pcm48kHz16bitStereo = new PcmSoundFormat(48000, 16, 2);

        /// <summary>
        /// List of most popular Pcm formats.
        /// </summary>
        public static readonly IEnumerable<PcmSoundFormat> StandardFormats = new PcmSoundFormat[]
        {
            Pcm8kHz8bitMono,
            Pcm8kHz8bitStereo,
            Pcm8kHz16bitMono,
            Pcm8kHz16bitStereo,

            Pcm11kHz8bitMono,
            Pcm11kHz8bitStereo,
            Pcm11kHz16bitMono,
            Pcm11kHz16bitStereo,

            Pcm22kHz8bitMono,
            Pcm22kHz8bitStereo,
            Pcm22kHz16bitMono,
            Pcm22kHz16bitStereo,

            Pcm44kHz8bitMono,
            Pcm44kHz8bitStereo,
            Pcm44kHz16bitMono,
            Pcm44kHz16bitStereo,

            Pcm48kHz8bitMono,
            Pcm48kHz8bitStereo,
            Pcm48kHz16bitMono,
            Pcm48kHz16bitStereo,
        };

        private int sampleRate;
        private short bitsPerSample;
        private short channels;

        /// <summary>
        /// Samples per second
        /// </summary>
        public int SampleRate
        {
            get
            {
                return sampleRate;
            }
        }

        public short BitsPerSample
        {
            get
            {
                return bitsPerSample;
            }
        }

        /// <summary>
        /// 1 - mono, 2 - stereo.
        /// </summary>
        public short Channels
        {
            get
            {
                return channels;
            }
        }

        public short BlockAlign
        {
            get
            {
                return (short)(Channels * (BitsPerSample / 8));
            }
        }

        public int AverageBytesPerSecond
        {
            get
            {
                return BlockAlign * SampleRate;
            }
        }

        public string Description
        {
            get
            {
                return ToString();
            }
        }

        public static bool operator ==(PcmSoundFormat format1, PcmSoundFormat format2)
        {
            return format1.Equals(format2);
        }

        public static bool operator !=(PcmSoundFormat format1, PcmSoundFormat format2)
        {
            return !(format1 == format2);
        }

        public bool Equals(PcmSoundFormat other)
        {
            return ReferenceEquals(other, this)
                || (!ReferenceEquals(other, null)
                    && this.bitsPerSample == other.bitsPerSample
                    && this.channels == other.channels
                    && this.sampleRate == other.sampleRate);
        }

        public override bool Equals(object obj)
        {
            if (!(obj is PcmSoundFormat))
            {
                return false;
            }

            return Equals((PcmSoundFormat)obj);
        }

        public override int GetHashCode()
        {
            return bitsPerSample * channels * sampleRate;
        }

        public override string ToString()
        {
            string channelsText;
            switch (channels)
            {
                case 1:
                    channelsText = "Mono";
                    break;
                case 2:
                    channelsText = "Stereo";
                    break;
                default:
                    channelsText = channels + " channels";
                    break;
            };

            return string.Format("{0} Hz, {1} bit, {2}", sampleRate, bitsPerSample, channelsText);
        }

        public PcmSoundFormat(int sampleRate, short bitsPerSample, short channels)
        {
            this.sampleRate = sampleRate;
            this.bitsPerSample = bitsPerSample;
            this.channels = channels;
        }
    }
}
