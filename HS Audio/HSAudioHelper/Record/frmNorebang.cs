﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using HS_Audio.DSP;
using NAudio.Wave;
using HS_CSharpUtility.Extension;
using NAudio.CoreAudioApi;
using System.Threading;
using HS_Audio.Control;
using HS_Audio.Record;
using CSCore.Streams;
using CSCore;
using HS_Audio.Bundles;

namespace HS_Audio.Recoder
{
    public partial class frmNorebang : Form
    {
        bool WASAPIEnable;

        HSAudioHelper Helper;
        HSAudioDSPHelper dsp;
        WaveInNAudio MicWavein;

        Thread MicPeakThread;
        ThreadStart MicPeakThreadStart;
        public static bool WindowsXP_Higher{get{return Environment.OSVersion.Version.Major>5;}}
        #region 사운드 유틸리티
        string _ConvertdBToString;
        public string ConvertdBToString(float Sample, bool PlusWhen0Over = false)
        {
            if (Sample == float.PositiveInfinity) { _ConvertdBToString = "Inf."; }
            else if (DecibelsforSound(Sample) < -360 || Sample == float.NegativeInfinity || Sample == 0) _ConvertdBToString = "-Inf.";
            else
            {
                double db = DecibelsforSound(Sample);
                if (PlusWhen0Over && (db>-0.0004&&db<0))_ConvertdBToString = db.ToString(" 0.000");
                else if (PlusWhen0Over && db == 0) _ConvertdBToString = " 0.000";
                //else if (PlusWhen0Over && (db<0.0005&&db > 0)) _ConvertdBToString = db.ToString("+0.000");
                else if (PlusWhen0Over && db > 0) _ConvertdBToString = db.ToString("+0.000");
                else _ConvertdBToString = db.ToString("0.000");
            }
            return _ConvertdBToString;
        }
        public string ConvertdBToString(double Sample, bool PlusWhen0Over = false)
        {
            if (Sample == float.PositiveInfinity)  _ConvertdBToString = "Inf.";
            else if (DecibelsforSound(Sample) < -360 || Sample == float.NegativeInfinity || Sample == 0) _ConvertdBToString = "-Inf.";
            else
            {
                double db = DecibelsforSound(Sample);
                if (PlusWhen0Over && (db > -0.0004 && db < 0)) _ConvertdBToString = db.ToString(" 0.000");
                else if (PlusWhen0Over && db == 0) _ConvertdBToString = " 0.000";
                //else if (PlusWhen0Over && (db<0.0005&&db > 0)) _ConvertdBToString = db.ToString("+0.000");
                else if (PlusWhen0Over && db > 0) _ConvertdBToString = db.ToString("+0.000");
                else _ConvertdBToString = db.ToString("0.000");
            }
            return _ConvertdBToString;
        }
        public string ConvertdBToString(float Sample, string Format)
        {
            if (Sample == float.PositiveInfinity) { _ConvertdBToString = "Inf."; }
            else if (DecibelsforSound(Sample) < -360 || Sample == float.NegativeInfinity || Sample == 0) _ConvertdBToString = "-Inf.";
            else if(Sample==0)_ConvertdBToString = Sample.ToString(Format);
            return _ConvertdBToString;
        }

        public static double mag_sqrd(double re, double im) { return (re * re + im * im); }

        public static double Decibels(double re, double im) {if(re==double.NegativeInfinity)return re; return ((re == 0 && im == 0) ? (double.NaN) : 10.0 * Math.Log10(((mag_sqrd(re, im))))); }
        public static double DecibelsforSound(double Sample, double Peak = 0) {if(Sample==double.NegativeInfinity)return Sample; return ((Sample == 0 && Peak == 0) ? (double.NegativeInfinity) : 10.0 * Math.Log10(((mag_sqrd(Sample, Peak)))));}
        #endregion

        public static SoundDevice ConvertDeviceToWaveDevice(WaveInCapabilities device, bool ProductID = false)
        {
            return new SoundDevice(-1, device.ProductName, HS_Audio_Lib.GUID.GetGUID(ProductID?device.ProductGuid:device.ManufacturerGuid));
        }

        #region 마이크 입력
        float sample32, tmp;
        bool Nomalize = true;
        ulong Underflow = 0, Overflow = 0;
        float Peak = float.NegativeInfinity;
        public HS_Audio_Lib.RESULT READCALLBACK_Mic(ref HS_Audio_Lib.DSP_STATE dsp_state, IntPtr InBuf, IntPtr OutBuf, uint length, int inchannels, int outchannels)
        {
            int count = 0;
            int count2 = 0;
            sample32 = 0;

            unsafe
            {
                float* inbuffer = (float*)InBuf.ToPointer();
                float* outbuffer = (float*)OutBuf.ToPointer();
                //byte[] MicBuffer = MicQueue.Count>0?MicQueue.Dequeue():null;
                int tmp1;
                //if(A == b&c == d && e == f)

                for (count = 0; count < length; count++)
                {
                    for (count2 = 0; count2 < outchannels; count2++)
                    {
                        //b = &inbuffer[(count * outchannels) + count2];
                        tmp1 = (count * outchannels) + count2;

                        //sample32 = MicBuffer != null&&tmp1 < MicBuffer.Length / 2 ? (MicBuffer[tmp1 + 1] << 8 | MicBuffer[tmp1 + 0]) / 32768f:0;
                        //MicQueue.Count > 0 ? MicQueue.Dequeue() / 32768f : 0;
                        sample32 = GetSampleQueue(MaxBuffer);
                        //if (MicBuffer.Count > 2)  {MicBuffer.RemoveAt(0); MicBuffer.RemoveAt(1);  }
                        //if(Underflow == ulong.MaxValue)Underflow = 0;
                        //if(MicQueue.Count==0)Underflow++;
                        tmp = inbuffer[tmp1];
                        outbuffer[tmp1] = (tmp + sample32) / (Nomalize ? 2 : 1);//HSDSP.Volume(tmp/*b[0]*/, Gain);

                    }
                }
                //if (MicBuffer.Count > 4096) MicBuffer.RemoveRange(0, 2048);
            }
            return HS_Audio_Lib.RESULT.OK;
        }

        int i = 0, tmp1 = 0;
        byte[] tmpSample;
        //float[] tmpSample;
        byte[][] tmpArray;
        byte[] tmpFloatBuffer = new byte[4];

        //float[][] tmpArray;
        float GetSampleQueue(int Buffer = 2048)
        {
            try
            {       
                try
                {
                    if (MicQueue.Count < 2 || Break){ if (MicQueue.Count == 0 && !Break)Underflow++; return 0;}
                    /*
                    else tmpArray = MicQueue.ToArray();

                    for (int j = 0; j < MicQueue.Count; j++)
                    {
                        tmp1 += tmpArray[j].Length;
                        if (tmp1 >= Buffer) break;
                        else return 0;
                    }*/
                }catch{}

                if (tmpSample == null||(i >= tmpSample.Length && MicQueue.Count > 0))
                {
                    i = 0;
                    tmp1 = 0;
                    tmpSample = MicQueue.Dequeue();
                    //MicQueue.RemoveAt(0);
                }
                float aa = 0;
                if (MicWavein.WaveFormat.Encoding == WaveFormatEncoding.Pcm)
                {
                    if (MicWavein.WaveFormat.BitsPerSample == 16)
                    {
                        short sample = (short)(tmpSample[i + 1] << 8 | tmpSample[i + 0]);
                        aa = sample / (float)short.MaxValue; //32768f;
                        i += 2;
                    }
                    else if (MicWavein.WaveFormat.BitsPerSample == 24)
                    {
                        int sample = (int)(tmpSample[i + 2] << 16 | tmpSample[i + 1] << 8 | tmpSample[i + 0]);
                        aa = sample / (float)0xFFFFFF; //32768f;
                        i += 3;
                    }
                    else if (MicWavein.WaveFormat.BitsPerSample == 32)
                    {
                        int sample = (int)(tmpSample[i + 3] << 24 | tmpSample[i + 2] << 16 | tmpSample[i + 1] << 8 | tmpSample[i + 0]);
                        aa = sample / (float)int.MaxValue; //32768f;
                        i += 4;
                    }
                } 
                else if(MicWavein.WaveFormat.Encoding == WaveFormatEncoding.IeeeFloat)
                {
                    try
                    {
                        tmpFloatBuffer[0] = tmpSample[i + 0];
                        tmpFloatBuffer[1] = tmpSample[i + 1];
                        tmpFloatBuffer[2] = tmpSample[i + 2];
                        tmpFloatBuffer[3] = tmpSample[i + 3];
                        aa = BitConverter.ToSingle(tmpFloatBuffer, 0);
                    }
                    catch { i = 0; return 0; }
                     
                     /*
                    int sample = (int)(tmpSample[i + 3] << 24 | tmpSample[i + 2] << 16 | tmpSample[i + 1] << 8 | tmpSample[i + 0]);
                    unsafe { aa = (*(float*)sample); }*/
                    i += 4;
                }
                aa = aa.Volume(Amp);
                    //tmpSample[i];
                return aa;
            }catch{return 0;}

            /*
            if (MicQueue.Count > Buffer&&!Break)
            {
                return MicQueue.Dequeue();
                //_GetSampleQueue.AddRange(MicQueue.Dequeue());
                //return MicBuffer.Count > 2048 ? (MicBuffer[tmp1 + count2 + 1] << 8 | MicBuffer[tmp1 + count2 + 0]) / 32768f : 0;
            }
            else if(!Break)Underflow++;
            return 0;*/
        }

        int MaxBuffer = 5;
        //Queue<float[]> MicQueue = new Queue<float[]>();
        Queue<byte[]> MicQueue = new Queue<byte[]>();
        Queue<byte[]> MicPeakQueue = new Queue<byte[]>();
        //Queue<float> MicQueue = new Queue<float>();
        //volatile List<byte[]> MicQueue = new List<byte[]>();
        //volatile List<byte> MicBuffer = new List<byte>();
        //byte[] MicBuffer;
        void waveIn_DataAvailable(object sender, WaveInEventArgs e)
        {

            //if (MicBuffer.Count < MaxBuffer) MicBuffer.AddRange(e.Buffer);
            if (!chkMicBypass.Checked&&MicQueue.Count < MaxBuffer) MicQueue.Enqueue(e.Buffer);
            else if (!Break && !chkMicBypass.Checked) Overflow++;
            if(MicPeakQueue.Count<MaxBuffer)MicPeakQueue.Enqueue(e.Buffer);
            /*
            if (MicQueue.Count < MaxBuffer)
            {
                byte[] tmp = e.Buffer;
                float[] tmp1 = new float[tmp.Length];
                for (int index = 0; index < tmp.Length; index += 2)
                {
                    short sample = (short)((e.Buffer[index + 1] << 8) |
                                            e.Buffer[index + 0]);
                    tmp1[i] = sample / 32768f;
                }
                MicQueue.Enqueue(tmp1);
            }*/
            /*
            for (int index = 0; index < e.BytesRecorded; index += 2)
            {
                short sample = (short)((e.Buffer[index + 1] << 8) |
                                        e.Buffer[index + 0]);
                if (MicQueue.Count < 9600) MicQueue.Enqueue(sample / 32768f);
                else if(!Break)Overflow++;
            }*/
        }

        #region 마이크 Peak
        void MicPeakThread_Loop()
        {
            while (true)
            {
                try
                {
                    ProgressBarGradientInternal progressBar1 = progressBarGradient1;
                    ProgressBarGradientInternal progressBar2 = progressBarGradient2;

                    int tempFrameDelay = frameDelay;
                    int tempSampleDelay = _sampleDelay;

                    int MaxValue = progressBarGradient1.MaximumValue;
                    int MaxValue1 = progressBarGradient2.MaximumValue;

                    // for each channel, determine the step size necessary for each iteration
                    int leftGoal = 0;
                    int rightGoal = 0;

                    #region
                    {
                        Work();
                        double aa = Math.Max(DecibelsforSound(MIC_L), -60);
                        aa = _LED눈금갯수 * (aa/60f);
                        int b = (int)(MaxValue + aa);
                        leftGoal = (int)Math.Min(b, MaxValue);
                        leftGoal = Math.Max(leftGoal, 0);

                        aa = Math.Max(DecibelsforSound(MIC_R), -60);
                        aa = _LED눈금갯수 * (aa / 60f);
                        b = (int)(MaxValue1 + aa);
                        rightGoal = (int)Math.Min(b, MaxValue1);
                        rightGoal = Math.Max(rightGoal, 0);
                    }
                    #endregion
                    /*
				// average across all samples to get the goals
				for(int i = 0; i < SAMPLES; i++)
				{
					leftGoal += (Int16)samples.GetValue(i, 0, 0);
					rightGoal += (Int16)samples.GetValue(i, 1, 0);
				}

				leftGoal = (int)Math.Abs(leftGoal / SAMPLES);
				rightGoal = (int)Math.Abs(rightGoal / SAMPLES);*/

                    double range1 = leftGoal - progressBarGradient1.Value;
                    double range2 = rightGoal - progressBarGradient2.Value;

                    double exactValue1 = progressBarGradient1.Value;
                    double exactValue2 = progressBarGradient2.Value;

                    double stepSize1 = range1 / tempSampleDelay * tempFrameDelay;
                    if (Math.Abs(stepSize1) < .01)
                    {
                        stepSize1 = Math.Sign(range1) * .01;
                    }
                    double absStepSize1 = Math.Abs(stepSize1);

                    double stepSize2 = range2 / tempSampleDelay * tempFrameDelay;
                    if (Math.Abs(stepSize2) < .01)
                    {
                        stepSize2 = Math.Sign(range2) * .01;
                    }
                    double absStepSize2 = Math.Abs(stepSize2);

                    // increment/decrement the bars' values until both equal their desired goals,
                    // sleeping between iterations
                    if ((progressBar1.Value == leftGoal) && (progressBar2.Value == rightGoal))
                    {
                        Thread.Sleep(tempSampleDelay);
                    }
                    else
                    {
                        do
                        {
                            if (progressBar1.Value != leftGoal)
                            {
                                if (absStepSize1 < Math.Abs(leftGoal - progressBar1.Value))
                                {
                                    exactValue1 += stepSize1;
                                    progressBar1.Value = (int)Math.Round(exactValue1);
                                }
                                else
                                {
                                    progressBar1.Value = leftGoal;
                                }
                            }

                            if (progressBar2.Value != rightGoal)
                            {
                                if (absStepSize2 < Math.Abs(rightGoal - progressBar2.Value))
                                {
                                    exactValue2 += stepSize2;
                                    progressBar2.Value = (int)Math.Round(exactValue2);
                                }
                                else
                                {
                                    progressBar2.Value = rightGoal;
                                }
                            }

                            Thread.Sleep(tempFrameDelay);
                        } while ((progressBar1.Value != leftGoal) || (progressBar2.Value != rightGoal));
                    }
                }
                catch{Thread.Sleep(100);}
                MIC_L = MIC_R = float.NegativeInfinity;
            }
        }

        double MIC_L = double.NegativeInfinity, MIC_R = double.NegativeInfinity;
        private void Work()
        {
            float a = 0,LDB = 0, RDB = 0;
            #region
            if (MicPeakQueue!=null&&MicPeakQueue.Count > 0)
            {
                byte[] tmp = MicPeakQueue.Dequeue();
                //if (Ltmp.Length == 0) { Ltmp = new float[DefaultLED+1]; } if (Rtmp.Length == 0) { Rtmp = new float[DefaultLED]; }
                if (channels == 2)
                {
                    for (int i = 0, j = 0; i < tmp.Length; i += 2, j++)
                    {
                        if (MicWavein.WaveFormat.Encoding == WaveFormatEncoding.Pcm)
                        {
                            if (MicWavein.WaveFormat.BitsPerSample == 16)
                            {
                                short sample = (short)(tmpSample[i + 1] << 8 | tmpSample[i + 0]);
                                a = sample / (float)short.MaxValue; //32768f;
                                i += 2;
                            }
                            else if (MicWavein.WaveFormat.BitsPerSample == 24)
                            {
                                int sample = (int)(tmpSample[i + 2] << 16 | tmpSample[i + 1] << 8 | tmpSample[i + 0]);
                                a = sample / (float)0xFFFFFF; //32768f;
                                i += 3;
                            }
                            else if (MicWavein.WaveFormat.BitsPerSample == 32)
                            {
                                int sample = (int)(tmpSample[i + 3] << 24 | tmpSample[i + 2] << 16 | tmpSample[i + 1] << 8 | tmpSample[i + 0]);
                                a = sample / (float)int.MaxValue; //32768f;
                                i += 4;
                            }
                        }
                        else if (MicWavein.WaveFormat.Encoding == WaveFormatEncoding.IeeeFloat)
                        {
                            try
                            {
                                tmpFloatBuffer[0] = tmpSample[i + 0];
                                tmpFloatBuffer[1] = tmpSample[i + 1];
                                tmpFloatBuffer[2] = tmpSample[i + 2];
                                tmpFloatBuffer[3] = tmpSample[i + 3];
                                a = BitConverter.ToSingle(tmpFloatBuffer, 0);
                                i += 4;
                            }
                            catch { i = 0;}
                            /*
                           int sample = (int)(tmpSample[i + 3] << 24 | tmpSample[i + 2] << 16 | tmpSample[i + 1] << 8 | tmpSample[i + 0]);
                           unsafe { aa = (*(float*)sample); }*/
                        }

                        if(j % 2 == 0){ if (Math.Abs(a) > LDB) LDB = a;}
                        else if (Math.Abs(a) > RDB) RDB = a;
                    }
                    a = LDB;
                    this.MIC_L = HS_Audio.DSP.HSDSP.Volume(a, Amp);
                    a = RDB;
                    this.MIC_R = HS_Audio.DSP.HSDSP.Volume(a, Amp);
                }
                else
                {
                    /*
                    for (int i = 0; i < tmp.Length; i += 2)
                    {
                        a = (short)(tmp[i + 1] << 8 | tmp[i + 0]);
                        if (Math.Abs(a) > LDB) LDB = a;
                    }
                    Mic = a / 32767f;
                    this.MIC_R = this.MIC_L = HS_Audio.DSP.HSDSP.Volume(Mic, Amp);*/
                }

                //MIC_L = DecibelsforSound(LDB);
                //MIC_R = DecibelsforSound(RDB);
                Thread.Sleep(25);
            }else Thread.Sleep(75);
            #endregion
        }

        private int _sampleDelay = 60;
        public int SampleDelay
		{
			get{return _sampleDelay;}

			set
			{
				_sampleDelay = Math.Max(0, value);
				if(frameDelay > _sampleDelay)
                    frameDelay = _sampleDelay;
			}
		}

        private int frameDelay = 10;
		public int FrameDelay
		{
			get{return frameDelay;}
			set{frameDelay = Math.Max(0, Math.Min(_sampleDelay, value));}
		}

        bool Smooth = true;
        #endregion

        int channels = 2;
        public void Register(int selectedDevice)
        {
            try
            {
                if (MicWavein != null) MicWavein.Dispose();

                if (WASAPIEnable && selectedDevice == cbInputDevice.Items.Count - 1)
                {
                    MicWavein = new WaveInNAudio(new WasapiLoopbackCapture());
                    //MicWavein = new WaveInNAudio(new WasapiLoopbackCapture());
                }
                else
                {
                    WaveIn MicWavein = new WaveIn();
                    MicWavein.BufferMilliseconds = 50;
                    //MicWavein.NumberOfBuffers = 2;
                    MicWavein.DeviceNumber = selectedDevice;
                    int sampleRate = 48000; // 48 kHz
                    MicWavein.WaveFormat = new WaveFormat(sampleRate, channels);

                    this.MicWavein = new WaveInNAudio(MicWavein);
                }
                MicWavein.DataAvailable += waveIn_DataAvailable;
                var singleBlockNotificationStream = new SingleBlockNotificationStream(MicWavein.BaseWaveIn.ToSampleSource());
                IWaveSource wave = singleBlockNotificationStream.ToWaveSource();
                singleBlockNotificationStream.SingleBlockRead += SingleBlockNotificationStream_SingleBlockRead;
                ChangeDevice();
            }
            catch
            {
                
            }
        }

        private void SingleBlockNotificationStream_SingleBlockRead(object sender, SingleBlockReadEventArgs e)
        {
            
        }
        #endregion

        internal frmNorebang()
        {
            InitializeComponent();
        }
        public frmNorebang(HSAudioHelper Helper)
        {
            InitializeComponent();
            this.Helper = Helper;
            MicPeakThreadStart = new ThreadStart(MicPeakThread_Loop);
            dsp = new DSP.HSAudioDSPHelper(Helper, READCALLBACK_Mic, true);
            dsp.DisConnect();
            comboBox2.SelectedIndex = 0;
        }

        LIBRARY.HSVolumeControl_WinXP WindowsXPVolumeControl;
        WaveLib.AudioMixer.MixerLine WindowsXPLine;
        WaveLib.AudioMixer.Mixer mi;
        private void frmNorebang_Load(object sender, EventArgs e)
        {
#if DEBUG
            WASAPIEnable = WindowsXP_Higher;
#endif
            MessageBox.Show("먼저 도움말을 읽어주시기 바랍니다.\n\n만약 오류가 나거나 소리가 안나면 [마이크 켜기]를\n체크 해제했다가 다시 해주시기 바랍니다.", "마이크 입력 (노래방)", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            this.Icon = global::HS_Audio.Properties.Resources.Microphone;

            menuStrip1.ImageScalingSize = new Size(16, 16);
            progressBarGradient1.MaximumValue = short.MaxValue;

            btnSoundDeviceRefresh_Click(null, null);
            comboBox2.SelectedIndex = 0;
            cbMicChannelLock = true;
            cbMicChannel.SelectedIndex = 1;
            cbMicChannelLock = false;
            
            timer1.Start();
        }

        bool IsStart = false;
        Thread StartQueueThread;
        private void chkMic_CheckedChanged(object sender, EventArgs e)
        {
            IsStart = chkMic.Checked;

            try { MicPeakThread.Abort(); } catch { }
            try { StartQueueThread.Abort(); } catch { }

            if (chkMic.Checked)
            {
                Underflow =Overflow =0;
                dsp.ReRegisterDSP(comboBox2.SelectedIndex==0?true:false);
                MicPeakThread = new Thread(MicPeakThreadStart); MicPeakThread.Start();
                ChangeDevice();
            }
            else
            {
                dsp.DisConnect();
                if (MicWavein != null) MicWavein.StopRecording();
                lblMicdBL.Text = "-Inf. dB"; lblMicdBR.Text = "-Inf. dB";
                progressBarGradient1.Value = progressBarGradient2.Value = 0;
            }
        }
        void ChangeDevice()
        {
            try
            {
                Break = true;

                MicQueue.Clear();
                MicPeakQueue.Clear();
                Overflow = Underflow = 0;

                if (IsStart)
                {
                    if (MicWavein != null && !MicWavein.IsStart) MicWavein.StartRecording();
                    while (MicQueue.Count <= MaxBuffer && Break) { Thread.Sleep(1); Break = false; }
                }
            }
            
            catch(Exception ex) { }
            finally { Break = false; }
            /*
            StartQueueThread = new Thread(() =>
            {
                try
                {
                    Break = true;

                    MicQueue.Clear();
                    MicPeakQueue.Clear();
                    Overflow = Underflow = 0;

                    if (IsStart)
                    {
                        if (MicWavein != null && !MicWavein.IsStart) MicWavein.StartRecording();
                        while (MicQueue.Count <= MaxBuffer && Break) { Thread.Sleep(1); Break = false; }
                    }
                }
                catch { }
                finally { Break = false; }
            });
            StartQueueThread.Start();*/
        }

        int _LED눈금갯수 = short.MaxValue;
        private void timer1_Tick(object sender, EventArgs e)
        {
            try
            {
                //if (this.WindowState != FormWindowState.Minimized&&this.WindowShowStatus != WindowShowState.Hide)
                {
                    /*
                    timer1.Interval = 25;
                    float a = mmd == null ? 0 : mmd.AudioMeterInformation.MasterPeakValue;
                    double a1 = Decibels(a, 0);
                    float b = a * progressBarGradient1.MaximumValue;

                    //progressBar1.InvokeIfNeeded(() => this.progressBar1.Value = (int)a);
                    this.progressBarGradient1.Value = a1 < -40 ? 0 : Math.Min((int)(a1 * progressBarGradient1.MaximumValue), progressBarGradient1.MaximumValue);
                    label11.Text = string.Format("{0} dB", a1 < -171 ? "-Inf" : a1.ToString("0.00"));
                    this.progressBarGradient2.Value = (int)b;
                    label14.Text = string.Format("{0}", a1 < -1000 ? "-Inf" : a.ToString("0.0000000"));*/
                    toolStripStatusLabel3.Text = string.Format("/ 버퍼: {0} (언더플로: {1}, 오버플로: {2})", MicQueue.Count, Underflow, Overflow);
                    lblMicdBL.Text = ConvertdBToString(MIC_L, true) + " dB";
                    lblMicdBR.Text = ConvertdBToString(MIC_R, true) + " dB";

                    lblMicdBL.BackColor = (MIC_L > 1 || MIC_L < -1) && MIC_L != float.NegativeInfinity ? Color.Red : SystemColors.Control;
                    lblMicdBR.BackColor = (MIC_R > 1 || MIC_R < -1) && MIC_R != float.NegativeInfinity ? Color.Red : SystemColors.Control;
                    lblMicdBL.ForeColor = (MIC_L > 1 || MIC_L < -1) && MIC_L != float.NegativeInfinity ? Color.Black : Color.Blue;
                    lblMicdBR.ForeColor = (MIC_R > 1 || MIC_R < -1) && MIC_R != float.NegativeInfinity ? Color.Black : Color.Blue;
                }
            }
            catch
            {
                timer1.Interval = 1000;
            }
        }

        private void dB레이블보이기ToolStripMenuItem_CheckedChanged(object sender, EventArgs e)
        {
            progressBarGradient1.VisibledB = progressBarGradient2.VisibledB = dB레이블보이기ToolStripMenuItem.Checked;
        }

        private void checkBox3_CheckedChanged(object sender, EventArgs e)
        {
            dsp.Bypass = chkMicBypass.Checked;
        }

        MMDeviceEnumerator mm = new MMDeviceEnumerator();
        MMDeviceCollection mmdc;
        MMDevice mmd_Default;

        WaveInCapNAudio[] MicList;
        bool ComboBoxLock;
        bool LockRefreash = false;
        private void btnSoundDeviceRefresh_Click(object sender, EventArgs e)
        {
            LockRefreash = true;
            try
            {
                int waveInDevices = WaveIn.DeviceCount;
                try
                {
                    ComboBoxLock = true;
                    cbInputDevice.Enabled = trackBar3.Enabled /*= checkBox7.Enabled*/ = false;
                    checkBox1.Checked = false; label12.Text = "N/A";
                    lblMicdBL.Text = "-Inf. dB"; lblMicdBR.Text = "-Inf. dB";
                    progressBarGradient1.Value = progressBarGradient1.MinimumValue; progressBarGradient2.Value = progressBarGradient2.MinimumValue;
                    //string a = cbOutputDevice.SelectedItem.ToString();
                    LastDevice = cbInputDevice.SelectedIndex == -1 ? new WaveInCapabilities() : (WaveInCapabilities)cbInputDevice.SelectedItem;

                    cbInputDevice.Items.Clear();
                    cbInputDevice.Items.Add("(사운드 입력 장치가 없습니다!)");
                    cbInputDevice.Text = cbInputDevice.Items[0].ToString();
                    trackBar3.Value = 0;
                    ComboBoxLock = false;
                    if (waveInDevices == 0) return;

                }
                catch (Exception ex)
                {
                    ComboBoxLock = false; ex.Logging();
                }

                cbInputDevice.Items.Clear();

                MicList = new WaveInCapNAudio[waveInDevices];
                for (int waveInDevice = 0; waveInDevice < waveInDevices; waveInDevice++)
                {
                    WaveInCapNAudio deviceInfo = new WaveInCapNAudio(WaveIn.GetCapabilities(waveInDevice));
                    //string hh = deviceInfo.ToString();
                    MicList[waveInDevice] = deviceInfo;
                    cbInputDevice.Items.Add(deviceInfo);
                    //Debug.WriteLine("Device {0}: {1}, {2} channels", waveInDevice, deviceInfo.ProductName, deviceInfo.Channels);

                }
                if (WASAPIEnable)
                {
                    WaveInCapNAudio wc = new WaveInCapNAudio("[WASAPI Loopback (불안정 합니다)]");
                    cbInputDevice.Items.Add(wc); //윈도우 Vista 부터 6
                    cbInputDevice.Enabled = true;

                    //cbInputDevice.Items.Add(waveInDevices>0?"(기본 입력 장치)":"입력 장치가 존재하지 않습니다.");
                }
                else cbInputDevice.Enabled = waveInDevices > 0;
                cbInputDevice.SelectedIndex = 0;

                Register(cbInputDevice.SelectedIndex);

                if (WindowsXP_Higher)
                {
                    /*
                    mmd_Default = mm.GetDefaultAudioEndpoint(DataFlow.Capture, Role.Multimedia);
                    mmdc = mm.EnumerateAudioEndPoints(DataFlow.Capture, DeviceState.All);
                    foreach (MMDevice mmd in mmdc)
                    {
                        int a = mmd.ID.LastIndexOf(".");
                        string a1 = mmd.ID.Substring(a + 1);
                        WaveInCapabilities dd = cbInputDevice.SelectedIndex > 0 ? (WaveInCapabilities)cbInputDevice.SelectedItem : MicList[0];
                        if (a1.ToUpper() == ConvertDeviceToWaveDevice(dd, true).GUID.ToString (true).ToUpper()||
                            a1.ToUpper() == ConvertDeviceToWaveDevice(dd, false).GUID.ToString(true).ToUpper())
                        {

                            //if (this.mmd != null && this.mmd.AudioEndpointVolume != null) this.mmd.AudioEndpointVolume.OnVolumeNotification -= AudioEndpointVolume_OnVolumeNotification;
                            //this.mmd = mmd; mmd.AudioEndpointVolume.OnVolumeNotification += AudioEndpointVolume_OnVolumeNotification;

                        }
                    }*/

                    foreach (var t in MicWavein.GetMixerLine().Controls)
                        if (t.ControlType == NAudio.Mixer.MixerControlType.Volume) MicVolume = t as NAudio.Mixer.UnsignedMixerControl;
                        else if (t.ControlType == NAudio.Mixer.MixerControlType.Mute) MicMute = t as NAudio.Mixer.BooleanMixerControl;

                    progressBarGradient1.Visible = true;
                    trackBar3.Enabled = checkBox1.Enabled = VolMuteEnable && true;
                    trackBar3.Value = (int)(100 * MicVolume.Percent);

                    LockMute = true;
                    checkBox1.Checked = MicMute.Value;
                    LockMute = false;

                    timer1.Start();
                }
                else
                {
                    if (mi != null) mi.Dispose();
                    mi = new WaveLib.AudioMixer.Mixer(WaveLib.AudioMixer.MixerType.Recording);
                    WindowsXPLine = mi.Lines[0];
                    mi.MixerLineChanged += WindowsXPLine_MixerLineChanged;
                    trackBar3.Value = (int)(WindowsXPLine.VolumeScala * 10000);
                    trackBar3.Enabled = checkBox1.Enabled = VolMuteEnable && true;

                }
            }
            catch { }
            finally {LockRefreash = false; }
        }

        bool LockVolume, LockMute;
        private void AudioEndpointVolume_OnVolumeNotification(AudioVolumeNotificationData data)
        {
            if (!LockVolume) trackBar3.Value = (int)(10000 * mmd.AudioEndpointVolume.MasterVolumeLevelScalar);
            checkBox1.Checked = data.Muted;
        }
        public void WindowsXPLine_MixerLineChanged(WaveLib.AudioMixer.Mixer mixer, WaveLib.AudioMixer.MixerLine line)
        {
            if (!LockVolume) trackBar3.Value = (int)(mixer.Lines[0].VolumeScala * 10000);
            if (!LockMute) checkBox1.Checked = mixer.Lines[0].Mute;
        }

        NAudio.Mixer.UnsignedMixerControl MicVolume;
        NAudio.Mixer.BooleanMixerControl MicMute;
        WaveInCapabilities LastDevice;
        MMDevice mmd;
        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!LockRefreash)
            {
                Register(cbInputDevice.SelectedIndex);
                ChangeDevice();
                //try{if(chkMic.Checked&&!chkMicBypass.Checked)MicWavein.StartRecording();}catch{}
                MicPeakQueue.Clear();
            }
        }

        private void chkAutoDeviceDetect_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void trackBar3_Scroll(object sender, EventArgs e)
        {
            try
            {
                LockVolume = true;
                if (MicVolume != null) MicVolume.Percent = trackBar3.Value * 0.01;
                if (WindowsXPLine != null) WindowsXPLine.VolumeScala = trackBar3.Value * 0.01f;
            }
            finally {LockVolume = false;}
        }

        private void trackBar3_ValueChanged(object sender, EventArgs e)
        {
            if (MicVolume != null) label12.Text = MicVolume.Percent.ToString("0.00");
            else label12.Text = (trackBar3.Value / (WindowsXP_Higher ? 100 : 100.0f)).ToString("0.00");
        }

        private void checkBox2_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void chkNomalize_CheckedChanged(object sender, EventArgs e)
        {
            Nomalize = chkNomalize.Checked;
        }

        private void domainUpDown1_TextChanged(object sender, EventArgs e)
        {
            MaxBuffer = Convert.ToInt32(domainUpDown1.Text);
            //MicQueue.Clear();
        }

        private void domainUpDown1_KeyPress(object sender, KeyPressEventArgs e)
        {
            //숫자,백스페이스,마이너스,소숫점 만 입력받는다.
            if (!(Char.IsDigit(e.KeyChar)) && e.KeyChar != 8) //8:백스페이스,45:마이너스,46:소수점
            {
                e.Handled = true;
            }
        }

        bool IsClosing = false;
        private void frmNorebang_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = !IsClosing;
            this.Hide();
        }

        private void checkBox1_Click(object sender, EventArgs e)
        {
            try
            {
                LockMute = true;
                checkBox1.Checked = !checkBox1.Checked;

                //if (MicMute != null) MicMute.Value = checkBox1.Checked;
                if (WindowsXPLine != null) WindowsXPLine.Mute = checkBox1.Checked;
            }
            finally {LockMute = false;}
        }

        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (chkMic.Checked) dsp.ReRegisterDSP(comboBox2.SelectedIndex==0?true:false);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            MicQueue.Clear();
        }

        bool Break;
        private void checkBox3_CheckedChanged_1(object sender, EventArgs e)
        {
            Break = checkBox3.Checked;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Overflow = Underflow = 0;
        }

        private void 전개발자입니다ToolStripMenuItem_CheckedChanged(object sender, EventArgs e)
        {
            groupBox2.Enabled = 전개발자입니다ToolStripMenuItem.Checked;
        }

        bool cbMicChannelLock = false;
        private void cbMicChannel_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!cbMicChannelLock)
            {
                channels = cbInputDevice.SelectedIndex + 1;
                Register(cbInputDevice.SelectedIndex);
                //try {if (chkMic.Checked && !chkMicBypass.Checked) MicWavein.StartRecording(); }catch{ }
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            MicPeakQueue.Clear();
        }

        float Amp = 0;
        private void numericUpDown1_ValueChanged(object sender, EventArgs e)
        {
            Amp = (float)numericUpDown1.Value;
        }

        private void 에코음정설정법ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("[재생 설정] 창에가셔서\n"+
                "[음장 효과 설정(M)]-[DSP 설정]-[내장 DSP 설정] 를 누른후\n"+
                "마이크 할당된 부분이랑 똑같이 맞춰주시고 (대부분 Main Output)\n"+
                "[에코 설정]을 조절해주시면 되고 음정은 [피치 시프트]를 조절해 주세요.", "에코 / 음정 설정법", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }
        bool VolMuteEnable = false;
        private void 음량조절음소거ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("준비중 입니다.", "음량 조절, 음소거 도움말", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }
    }
}
