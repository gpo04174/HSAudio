﻿using System;
using System.Windows.Forms;
using System.Threading;
using NAudio.Wave;
using System.IO;
using HS_Audio_Lib;
using System.Runtime.InteropServices;
using System.Collections.Generic;

namespace HS_Audio_Helper.Recoder.Forms
{
    public partial class frmWaveDataOutputRecoding : Form
    {
        bool _IsCallbackWrite=true;
        public bool IsCallbackWrite
        {
            get { return _IsCallbackWrite; }
            set
            {
                _IsCallbackWrite = value;
                if (value)
                {
                     try { button2_Click(null, null); }catch{}
                     //try {writer.Dispose(); }catch { }
                     //try {writer1.Dispose(); } catch { }
                     //try { th[0].Abort(); }catch { }
                     Stop = true; button1_Clicking = false;
                }
            }
        }

        public int Samplate=48000;
        public int bits=24;
        public byte Channels = 2;
        //public byte CallbackChannels = 2;
        WaveFormat wavFormat = new WaveFormat();
        private WaveIn waveIn;
        private WaveFileWriter writer, writer1, CallbackWriter;
        string outputFilename = Environment.GetFolderPath(Environment.SpecialFolder.Desktop)+"\\새 녹음_좌.wav";
        string outputFilename1 = Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "\\새 녹음_우.wav";
        string outputFilenameCallback = Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "\\새 녹음.wav";
        HSAudioHelper Helper;
        ParameterizedThreadStart[] ps = new ParameterizedThreadStart[3];
        Thread[] th = new Thread[3];

        public bool NoiseMute;


        #region 콜백 녹음 관련

        internal HS_Audio_Lib.DSP DSP;
        public HS_Audio_Lib.DSP_READCALLBACK dspreadcallback;
        internal HS_Audio_Lib.DSP_DESCRIPTION dspdesc;
        public HS_Audio_Lib.DSPConnection DSPConnection = new HS_Audio_Lib.DSPConnection();

        public void Reregister(bool IsSystem)
        {
            if (dspreadcallback==null) dspreadcallback = new DSP_READCALLBACK(READCALLBACK);
            this.dspdesc.read = this.dspreadcallback;
            Helper.system.createDSP(ref dspdesc, ref DSP);
            if (IsSystem) Helper.system.addDSP(DSP, ref DSPConnection);
            else Helper.channel.addDSP(DSP, ref DSPConnection);
        }
        #endregion

        public frmWaveDataOutputRecoding(HSAudioHelper Helper)
        {
            InitializeComponent();
            this.Helper = Helper;
            ps[0] = new ParameterizedThreadStart(WaveThread);
            ps[1] = new ParameterizedThreadStart(WaveThread1);
            ps[2] = new ParameterizedThreadStart(WriteWaveThread);

            GetWaveEvent+=new WaveDelegate(frmWaveDataOutputRecoding_GetWaveEvent);

            waveIn = new WaveIn();
            waveIn.WaveFormat = new WaveFormat(Samplate, bits,(int)Channels);
            waveIn.DeviceNumber = 0;
            waveIn.DataAvailable += new EventHandler<WaveInEventArgs>( waveIn_DataAvailable);
            waveIn.WaveFormat.Encoding = WaveFormatEncoding.Pcm;  
            Helper.PlayingStatusChanged+=new HSAudioHelper.PlayingStatusChangedEventHandler(Helper_PlayingStatusChanged);
            textBox1.Text = Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "\\새 녹음_*.wav";
            dspreadcallback = new DSP_READCALLBACK(READCALLBACK);
            wc = new WaitCallback(WriteBufferThread);
            //Reregister(true);
            //writer = new WaveFileWriter(outputFilename, waveIn.WaveFormat);    
        }
        void Helper_PlayingStatusChanged(HS_Audio_Helper.HSAudioHelper.PlayingStatus ps, int index)
        {
            //if (ps == HSAudioHelper.PlayingStatus.Play) ;
            //else Stop = false;
        }

        private void frmWaveDataOutputRecoding_Load(object sender, EventArgs e)
        {
            comboBox1.SelectedIndex = 0;
            comboBox1.Text = comboBox1.Items[comboBox1.SelectedIndex].ToString();
            comboBox3.SelectedIndex = 5;
            comboBox3.Text = comboBox3.Items[comboBox3.SelectedIndex].ToString();
            comboBox2.SelectedIndex = 2;
            comboBox2.Text = comboBox2.Items[comboBox2.SelectedIndex].ToString();
        }

        private void menuStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void checkBox2_Click(object sender, EventArgs e)
        {
            if (checkBox2.Checked)
            { MessageBox.Show("파일로 실시간 저장하는 것을 추천드리지 않습니다.\n\n만약 실시간으로 저장하시려변 버퍼를 사용하는것을 추천드립니다."); }
        }

        int buf = 16380;
        MemoryStream ms, ms1,CallbackMemoryStream;
        bool button1_Clicking;
        private void button1_Click(object sender, EventArgs e)
        {
            if (Stop)
            {
                _UseThread = UseThread;
                _UseBuffer = UseBuffer;
                FileMaxBuffer = (int)numericUpDown5.Value;
                stopw.Clear();
                if(IsCallbackWrite)
                {
                    try
                    { 
                        try { CallbackWriter.Dispose();}catch{}
                        waveIn.WaveFormat.Channels = 2;
                        if (checkBox2.Checked)
                        { CallbackWriter = new WaveFileWriter(outputFilenameCallback, waveIn.WaveFormat); }
                        else
                        {
                            CallbackMemoryStream = new MemoryStream();
                            CallbackWriter = new WaveFileWriter(CallbackMemoryStream, waveIn.WaveFormat);
                        }
                        waveIn.StartRecording();
                        Reregister(IsSystem);
                        Stop = groupBox7.Enabled = checkBox5.Enabled = groupBox2.Enabled = radioButton1.Enabled = groupBox5.Enabled = false;
                        stopw.Start();
                        timer1.Start();
                        button1_Clicking = true; 
                        button2.Enabled = true;
                        button1.Text = "일시 정지";
                        tabPage2.Text = "설정 (녹음중이라 비활성화 되었습니다.)";
                    }
                    catch (IOException) { MessageBox.Show("파일을 다른 프로그램에서 사용중인것 같습니다.\n\n파일을 사용중인 프로그램을 닫고 다시 시도해 주세요."); }
                }
                else
                {
                    waveIn.WaveFormat.Channels = 1;
                    //buf = Helper.GetCurrentTick(HS_Audio_Lib.TIMEUNIT.PCMBYTES);
                    try { writer.Dispose(); }catch { }
                    try { writer1.Dispose(); }catch { }
                    buf = (int)numericUpDown3.Value;
                    try { th[0].Abort(); }catch { }
                    //try { th[2].Abort(); }catch{}
                    try
                    {
                        if (checkBox2.Checked)
                        {
                            writer = new WaveFileWriter(outputFilename, waveIn.WaveFormat);
                            writer1 = new WaveFileWriter(outputFilename1, waveIn.WaveFormat);
                        }
                        else
                        {
                            ms = new MemoryStream(); ms1 = new MemoryStream();
                            writer = new WaveFileWriter(ms, waveIn.WaveFormat);
                            writer1 = new WaveFileWriter(ms1, waveIn.WaveFormat);
                        }
                        th[0] = new Thread(ps[0]) { Priority = ThreadPriority.Normal };
                        //th[2] = new Thread(ps[2]) { Priority = ThreadPriority.Highest };
                        //th[2].Start();
                        Stop = groupBox7.Enabled = checkBox5.Enabled = groupBox2.Enabled = radioButton1.Enabled = groupBox5.Enabled = false;
                        button1_Clicking = true;
                        waveIn.StartRecording();
                        th[0].Start();
                        stopw.Start();
                        timer1.Start();
                        button2.Enabled = true;
                        button1.Text = "일시 정지";
                    }
                    catch (IOException) { MessageBox.Show("파일을 다른 프로그램에서 사용중인것 같습니다.\n\n파일을 사용중인 프로그램을 닫고 다시 시도해 주세요."); }
                }
            }
            else
            {
                if (button1_Clicking) { button1_Clicking = false; button1.Text = "재 시작"; stopw.Stop(); }
                else { button1_Clicking = true; button1.Text = "일시 정지"; stopw.Start(); }
            }
        }

        /*
        void WaveThread(object Buffer)
        {
            Thread.Sleep(2);
            uint buf = Convert.ToUInt32(Buffer);
            int buf1 = (int)buf;
            float[] buff = new float[buf*2];
            float[] buff1 = new float[buf];
            float[] buff2 = new float[buf];
            while(!Stop)
            {
                try
                {
                    Helper.system.getWaveData(buff1, buf1, 0);
                    Helper.system.getWaveData(buff2, buf1, 1);
                    for (int i = 0; i < buf1; i=i+2)
                    {
                        buff[i] = buff1[i];
                        buff[i + 1] = buff2[i];
                    }
                    //IntPtr bufferPtr = System.Runtime.InteropServices.Marshal.AllocCoTaskMem((int)buf);
                    //uint read = 0;
                    //Helper.sound.seekData(buf);
                    /*
                    IntPtr ptr1 = IntPtr.Zero;
                    IntPtr ptr2 = IntPtr.Zero;
                    uint len1 = 0;
                    uint len2 = 0;
                    Helper.sound.@lock(Offset, buf, ref ptr1, ref ptr2, ref len1, ref len2);*/
                    //Helper.sound.readData(bufferPtr, buf, ref read);
        /*
        byte[] buffer1 = new byte[buf];
        byte[] buffer2 = new byte[buf];
        for (uint i = 0; i < buf; i++)
            unsafe
            {
                try
                {
                    byte* buffer = (byte*)bufferPtr.ToPointer();
                    buffer1[i] = buffer[i * 1];//sourceChannels
                    buffer2[i] = buffer[i * 1 + 1];//sourceChannels
                }
                catch { }
            }
        //try { System.Runtime.InteropServices.Marshal.Copy(bufferPtr, buff, 0, buf1); }catch{}
        //Helper.sound.unlock(ptr1, ptr2, len1, len2);
    }
    finally { writer.WriteSamples(buff, 0, buf1); writer.Flush(); /*Offset = Offset + buf; Thread.Sleep((int)numericUpDown4.Value); }
    //string a = "";
}
writer.Close(); writer.Dispose();
}*/

        private void button2_Click(object sender, EventArgs e)
        {
            waveIn.StopRecording();
            try { writer.Dispose(); }catch { } try { writer1.Dispose(); } catch { }
            try { CallbackWriter.Dispose();}catch{}
            if (radioButton8.Checked) { try { if (DSP != null)DSP.remove(); DSP = null; } catch { } }
            if (!checkBox2.Checked)
            {
                if (IsCallbackWrite) { File.WriteAllBytes(outputFilenameCallback, CallbackMemoryStream.GetBuffer()); CallbackMemoryStream.Dispose(); }
                else
                {
                    File.WriteAllBytes(outputFilename, ms.GetBuffer());
                    File.WriteAllBytes(outputFilename1, ms1.GetBuffer());
                    ms.Dispose(); ms1.Dispose();
                }
            }
           Stop = groupBox7.Enabled = checkBox5.Enabled = groupBox2.Enabled = radioButton1.Enabled=groupBox5.Enabled = true;
            timer1.Stop(); try {stopw.Stop();}catch{} button1.Text = "녹음 시작";tabPage2.Text = "설정";
        }

        private void numericUpDown4_ValueChanged(object sender, EventArgs e)
        {
            Sleep = (int)numericUpDown4.Value;
        }

        HS_Audio.StopWatch stopw = new HS_Audio.StopWatch();
        public enum TickFormat{Millisecond,Millisecond10_1,Millisecond100_1,Second};
        long LastPosion, CurrentPosition, AvgPosition;
        private void timer1_Tick(object sender, EventArgs e)
        {
            if(radioButton8.Checked)CurrentPosition=CallbackWriter.Length;
            else CurrentPosition = writer.Length;
            AvgPosition=CurrentPosition-LastPosion;
            label11.Text = string.Format("녹음 크기: {0} Byte ({1} Byte)", CurrentPosition!=0?CurrentPosition.ToString("##,###"):"0", AvgPosition!=0?AvgPosition.ToString("##,###"):"0");
            label5.Text = string.Format("예상 시간: {0}", stopw.ToString(false));
            LastPosion = CurrentPosition;
            
        }

        private void button4_Click(object sender, EventArgs e)
        {
            try { System.Diagnostics.Process.Start(outputFilename); }
            catch { MessageBox.Show("외부 플레이어로 녹음된 파일을 재생하는데 실패하였습니다."); }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            try { System.Diagnostics.Process.Start(outputFilename1); }
            catch { MessageBox.Show("외부 플레이어로 녹음된 파일을 재생하는데 실패하였습니다."); }
        }
        System.Media.SoundPlayer sp = new System.Media.SoundPlayer();
        System.Media.SoundPlayer sp1 = new System.Media.SoundPlayer();
        bool button6_Clicking, button8_Clicking;
        private void button6_Click(object sender, EventArgs e)
        {
            if (!button6_Clicking)
            {
                sp.SoundLocation = outputFilename; //sp1.SoundLocation = outputFilename1;
                sp.LoadAsync(); //sp1.LoadAsync();
                try { sp.Play();  }catch{}
                //try { sp1.Play();  }catch{}
                button6.Text = "정지 (L)";
                button6_Clicking = true;
            }
            else
            { sp.Stop(); button6.Text = "미리 듣기 (L)"; button6_Clicking = false; }
        }
        private void button8_Click(object sender, EventArgs e)
        {
            if (!button8_Clicking)
            {
                sp1.SoundLocation = outputFilename1; //sp1.SoundLocation = outputFilename1;
                sp1.LoadAsync(); //sp1.LoadAsync();
                try { sp1.Play(); } catch { }
                //try { sp1.Play();  }catch{}
                button8.Text = "정지 (R)";
                button8_Clicking = true;
            }
            else
            { sp1.Stop(); button8.Text = "미리 듣기 (R)"; button8_Clicking = false; }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (outputFilenameCallback != null) try { saveFileDialog1.InitialDirectory = Path.GetDirectoryName(outputFilenameCallback); }catch{}
            if (saveFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                outputFilenameCallback = saveFileDialog1.FileName;
                string File =Path.GetFileNameWithoutExtension(saveFileDialog1.FileName);
                string Dir = Path.GetDirectoryName(saveFileDialog1.FileName);
                string Ext=Path.GetExtension(saveFileDialog1.FileName);
                outputFilename = Dir +"\\"+ File + "_좌" + Ext;
                outputFilename1 = Dir + "\\" + File + "_우" + Ext;
                textBox1.Text = Dir + "\\" + File + "_*" + Ext;
            }
        }

        private void numericUpDown2_ValueChanged(object sender, EventArgs e)
        {
            timer1.Interval = (int)numericUpDown2.Value;
        }

        private void trackBar1_ValueChanged(object sender, EventArgs e)
        {
            numericUpDown2.Value = trackBar1.Value;
        }

        private void button7_Click(object sender, EventArgs e)
        {
            if(!Stop)MessageBox.Show("현재 설정값은 다음번 녹음부터 적용됩니다.");
            Samplate = Convert.ToInt32(comboBox3.Items[comboBox3.SelectedIndex].ToString().Replace("Hz", ""));
            bits = Convert.ToInt32(comboBox2.Items[comboBox2.SelectedIndex]);
            waveIn.WaveFormat = new WaveFormat(Samplate, bits, (int)Channels);
            textBox3.Text = "Wave [PCM Float] {" + string.Format("{0} Hz, {1} bit, Stereo", Samplate, bits) + "}";
        }

        private void radioButton8_CheckedChanged(object sender, EventArgs e)
        {
            if (!radioButton8.Checked)
            {
                if (MessageBox.Show("일반 추출은 잡음이 있을수 있습니다. 그리고 좌우가 따로 저장 됩니다.\n그러나 콜백 추출 보다는 안전합니다. 하지만 권장하지 않습니다.\n\n"+
                    "정말 일반 추출을 사용하시겠습니까?", "콜백 추출 권장", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == System.Windows.Forms.DialogResult.Yes)
                { groupBox8.Enabled = groupBox9.Enabled = true; IsCallbackWrite = false; saveFileDialog1.Title = "저장할 경로 선택 (※메모: 이름뒤에 _좌, _우 가 붙여서 따로 저장됩니다.)"; }
                else { radioButton8.Checked =IsCallbackWrite= true; }
            }
            else { groupBox8.Enabled = groupBox9.Enabled = false; IsCallbackWrite = true; saveFileDialog1.Title = "저장할 경로 선택"; }
        }


        #region 일반, 콜백모드 공통 변수

        int FileMaxBuffer = 1024;
        public bool IsSystem { get { return radioButton4.Checked; } set { radioButton4.Checked = value; } }
        bool _IsSystem;

        public bool UseThread { get { return checkBox6.Checked; } set { checkBox6.Checked = value; } }
        bool _UseThread = true;
        public bool UseBuffer { get { return checkBox5.Checked; } set { checkBox5.Checked = value; } }
        bool _UseBuffer = false;

        float[] FilebufferArray = new float[1024];
        #endregion

        #region 일반 모드
        [Obsolete]
        public delegate void WaveDelegate(float[] L, float[] R, int Buffer);
        [Obsolete]
        public event WaveDelegate GetWaveEvent;
        bool Stop = true, Stop1;
        uint Offset = 0;
        int Sleep = 338;
        float[] buff = null;
        float[] buff1 = null;

        List<float> FilebufferList_L = new List<float>();
        List<float> FilebufferList_R = new List<float>();

        void WaveThread(object Buffer)
        {
            _IsSystem = IsSystem;
            //Thread.Sleep(2);
            //uint buf = Convert.ToUInt32(Buffer);
            int i = 0;
            int buf1 = buf;
            buff = new float[buf1];
            buff1 = new float[buf1];
            //buff = new float[buf];
            while (!Stop)
            {
                try
                {
                    if (_IsSystem)
                    {
                        if (Helper.system != null)
                        {
                            Helper.system.getWaveData(buff, buf1, 0);
                            Helper.system.getWaveData(buff1, buf1, 1);
                        }
                    }
                    else
                    {
                        if (Helper.channel != null)
                        {
                            Helper.channel.getWaveData(buff, buf1, 0);
                            Helper.channel.getWaveData(buff1, buf1, 1);
                        }
                    }
                    //IntPtr bufferPtr = System.Runtime.InteropServices.Marshal.AllocCoTaskMem((int)buf);
                    //uint read = 0;
                    //Helper.sound.seekData(buf);
                    /*
                    IntPtr ptr1 = IntPtr.Zero;
                    IntPtr ptr2 = IntPtr.Zero;
                    uint len1 = 0;
                    uint len2 = 0;
                    Helper.sound.@lock(Offset, buf, ref ptr1, ref ptr2, ref len1, ref len2);*/
                    //Helper.sound.readData(bufferPtr, buf, ref read);
                    /*
                    byte[] buffer1 = new byte[buf];
                    byte[] buffer2 = new byte[buf];
                    for (uint i = 0; i < buf; i++)
                        unsafe
                        {
                            try
                            {
                                byte* buffer = (byte*)bufferPtr.ToPointer();
                                buffer1[i] = buffer[i * 1];//sourceChannels
                                buffer2[i] = buffer[i * 1 + 1];//sourceChannels
                            }
                            catch { }
                        }*/
                    //try { System.Runtime.InteropServices.Marshal.Copy(bufferPtr, buff, 0, buf1); }catch{}
                    //Helper.sound.unlock(ptr1, ptr2, len1, len2);
                    if (button1_Clicking)
                    {
                        if ((writer != null && buff != null && !writer.IsDisposing)&&
                            (writer1 != null && buff1 != null && !writer1.IsDisposing))
                        {
                            for (i = 0; i < buf; i++)
                            {
                                if (NoiseMute) 
                                {
                                    if (Decibels(buff[i], 0) < 0)
                                        if (!_UseBuffer) writer.WriteSample(buff[i]);
                                        else FilebufferList_L.Add(buff[i]);
                                }
                                else
                                {
                                    if (!_UseBuffer) writer.WriteSample(buff[i]);
                                    else FilebufferList_L.Add(buff[i]);
                                }

                                if (NoiseMute)
                                {
                                    if (Decibels(buff1[i], 0) < 0)
                                        if (!_UseBuffer) writer.WriteSample(buff1[i]);
                                        else FilebufferList_R.Add(buff1[i]);
                                }
                                else
                                {
                                    if (!_UseBuffer) writer.WriteSample(buff[i]);
                                    else FilebufferList_R.Add(buff[i]);
                                }
                            }

                            if (FilebufferList.Count >= FileMaxBuffer)
                            {
                                if (_UseThread) ThreadPool.QueueUserWorkItem(wc, FilebufferList.ToArray());
                                else
                                {
                                    foreach (float a in FilebufferList)
                                        try { CallbackWriter.WriteSample(a); }
                                        catch { }
                                }
                                FilebufferList.Clear();
                            }
                        }
                    }
                }
                catch{}
                finally
                {
                    //if (GetWaveEvent != null) GetWaveEvent(buff, buff1, buf);

                    Thread.Sleep(Sleep);
                    /*Offset = Offset + buf;*/
                }
                //string a = "";
            }
            writer.Dispose(); writer1.Dispose();
            buff = null; buff1 = null; Stop = true; th[0].Abort();
        }

        void WriteWaveThread(object f)
        {
            while (!Stop)
            {
                try
                {
                    if (buff != null) { writer.WriteSamples(buff, 0, buf); }
                    if (buff1 != null) { writer1.WriteSamples(buff1, 0, buf); }
                }
                finally { Thread.Sleep(Sleep); }
            }
            writer.Dispose(); writer1.Dispose();
            writer = writer1 = null;
            th[2].Abort();
        }
        void WaveThread1(object Buffer)
        {
            //uint buf = Convert.ToUInt32(Buffer);
            int buf1 = (int)buf;
            float[] buff = new float[buf];
            while (!Stop)
            {
                try
                {
                    Helper.system.getWaveData(buff, buf1, 1);
                    //IntPtr bufferPtr = System.Runtime.InteropServices.Marshal.AllocCoTaskMem((int)buf);
                    //uint read = 0;
                    //Helper.sound.seekData(buf);
                    /*
                    IntPtr ptr1 = IntPtr.Zero;
                    IntPtr ptr2 = IntPtr.Zero;
                    uint len1 = 0;
                    uint len2 = 0;
                    Helper.sound.@lock(Offset, buf, ref ptr1, ref ptr2, ref len1, ref len2);*/
                    //Helper.sound.readData(bufferPtr, buf, ref read);
                    /*
                    byte[] buffer1 = new byte[buf];
                    byte[] buffer2 = new byte[buf];
                    for (uint i = 0; i < buf; i++)
                        unsafe
                        {
                            try
                            {
                                byte* buffer = (byte*)bufferPtr.ToPointer();
                                buffer1[i] = buffer[i * 1];//sourceChannels
                                buffer2[i] = buffer[i * 1 + 1];//sourceChannels
                            }
                            catch { }
                        }*/
                    //try { System.Runtime.InteropServices.Marshal.Copy(bufferPtr, buff, 0, buf1); }catch{}
                    //Helper.sound.unlock(ptr1, ptr2, len1, len2);
                }
                finally { writer1.WriteSamples(buff, 0, buf1); /*Offset = Offset + buf;*/ Thread.Sleep((int)numericUpDown4.Value); }
                //string a = "";
            }
            writer1.Close(); writer1.Dispose(); th[1].Abort();
        }

        void frmWaveDataOutputRecoding_GetWaveEvent(float[] L, float[] R, int Buffer)
        {
            if (button1_Clicking)
            {
                if (writer != null && buff != null && !writer.IsDisposing) { writer.WriteSamples(L, 0, buf); }
                if (writer1 != null && buff1 != null && !writer.IsDisposing) { writer1.WriteSamples(R, 0, buf); }
            }
        }
        #endregion

        #region 콜백 모드
        WaitCallback wc;
        List<float> FilebufferList = new List<float>();

        uint count = 0;
        int count2 = 0;
        IntPtr thisdspraw;
        public HS_Audio_Lib.RESULT READCALLBACK(ref HS_Audio_Lib.DSP_STATE State, IntPtr inbuf, IntPtr outbuf, uint length, int inchannels, int outchannels)
        {
            if (true)
            {
                thisdspraw = State.instance;
                //if (dsp_static != null) { dsp_static.setRaw(thisdspraw); }
                //else { dsp_static = new HS_Audio_Lib.DSP(); dsp_static.setRaw(thisdspraw); }

                unsafe
                {
                    float* inbuffer = (float*)inbuf.ToPointer();
                    float* outbuffer = (float*)outbuf.ToPointer();

                    for (count = 0; count < length; count++)
                    {
                        for (count2 = 0; count2 < outchannels; count2++)
                        {
                            outbuffer[(count * outchannels) + count2] = inbuffer[(count * inchannels) + count2];
                            try 
                            {
                                if (!Stop)
                                {
                                    if (NoiseMute)
                                    {
                                        if (Decibels(outbuffer[(count * inchannels) + count2], 0) < 0)
                                            if (!_UseBuffer) CallbackWriter.WriteSample(outbuffer[(count * inchannels) + count2]);
                                            else FilebufferList.Add(outbuffer[(count * inchannels) + count2]);
                                    }
                                    else
                                    {
                                        if (!_UseBuffer) CallbackWriter.WriteSample(outbuffer[(count * inchannels) + count2]);
                                        else FilebufferList.Add(outbuffer[(count * inchannels) + count2]);
                                    }
                                } }catch { }
                        }
                    }
                }
                if (FilebufferList.Count >= FileMaxBuffer)
                {
                    if (_UseThread) ThreadPool.QueueUserWorkItem(wc, FilebufferList.ToArray());
                    else
                    {
                        foreach (float a in FilebufferList)
                            try { CallbackWriter.WriteSample(a); }catch { }
                    }
                    FilebufferList.Clear();
                }
            }
            return HS_Audio_Lib.RESULT.OK;
        }

        float[] WriteBufferThread_Buf;
        int WriteBufferThread_i;
        object o = new object();
        private void WriteBufferThread(object Buffer)
        {
            lock (o)
            {
                WriteBufferThread_Buf = Buffer as float[];
                foreach (float a in WriteBufferThread_Buf)
                    try { CallbackWriter.WriteSample(a); }catch { }
            }
        }
        #endregion
        

        public static double mag_sqrd(double re, double im) { return (re * re + im * im); }

        public static double Decibels(double re, double im) { return ((re == 0 && im == 0) ? (0) : 10.0 * Math.Log10(((mag_sqrd(re, im))))); }

        private void checkBox4_CheckedChanged(object sender, EventArgs e)
        {
            NoiseMute = checkBox4.Checked;
        }

        private void 녹음도움말ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("녹음할때 반드시 Peak그래프와 같이 봐주시기 바랍니다.\n만약 0.000dB 보다 크면 출력사운드가 심하게 왜곡됩니다.\n\n" +
                "녹음시에 음량을 조금 낮게 맞추어놓고 Peak바가 -X.XXX dB 가 보이도록 해주시기 바랍니다. \n\n(대부분 0.000dB 이상 되면 왜곡됬었을 확률이 100% 입니다.)\n\n"+
                "그리고 만약 녹음중 노래가 떨린다면 녹음을 중지한 후 설정에서 버퍼사용을 체크해 주세요 \n아니면 파일로 실시간 기록을 해제하고 녹음하시기 바랍니다.\n\n" +
                "반드시 테스트로 미리 녹음해 보시길 권장합니다.",
                "녹음 도움말", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void checkBox5_EnabledChanged(object sender, EventArgs e) { checkBox5.Enabled = groupBox6.Enabled && checkBox5.Checked; }

        private void checkBox5_CheckedChanged(object sender, EventArgs e) { groupBox6.Enabled = checkBox5.Checked; }

        private void 끝내기XToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void frmWaveDataOutputRecoding_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = true;
            if (!Stop)
            {
                if (MessageBox.Show("현재 녹음중입니다.\n\n녹음을 중지하고 저장한후 종료하시겠습니까?", "현재 출력사운드 녹음하기", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == System.Windows.Forms.DialogResult.Yes)
                { button2_Click(null, null); e.Cancel = false; }
            }
            else { e.Cancel = false; }
        }
        void waveIn_DataAvailable(object sender, WaveInEventArgs e)
        {
            //label11.Text = "녹음 크기: " + e.BytesRecorded.ToString("##,###");
            //WriteData(e.Buffer, 0, e.BytesRecorded);
        }
        /*
        private static long TotalSecond;
        private static byte Hour, Minute, Second;
        private static long MilliSecond;
        private static System.Text.StringBuilder sb = new System.Text.StringBuilder();
        private static bool IsMinus;

        public static string GetTimeFormat(long Tick, TickFormat format = TickFormat.Millisecond, bool AttachZero=true, bool MilliSecondIsDot=true)
        {
            frmWaveDataOutputRecoding.sb.Remove(0, frmWaveDataOutputRecoding.sb.Length);
            IsMinus = Tick < 0;
            if (format == TickFormat.Millisecond)
            {
                TotalSecond = Tick / 1000;
                MilliSecond = Tick % 1000;
                if (TotalSecond > 60) { }
            }
        }*/
    }
}
