﻿using System;
using System.Windows.Forms;
using System.Threading;
using NAudio.Wave;
using System.IO;
using HS_Audio_Lib;
using System.Runtime.InteropServices;
using System.Collections.Generic;
using System.Text;
using HS_Audio.LIBRARY.FlacBox;

namespace HS_Audio.Recoder.Forms
{
    public partial class frmWaveDataOutputRecoding : Form
    {
        public enum WriteFormat{RAW,OGG,FLAC,WAVE,WAVE_Float,Text,Text_Float}

        bool _IsCallbackWrite=true;
        public bool IsCallbackWrite
        {
            get { return _IsCallbackWrite; }
            set
            {
                _IsCallbackWrite = value;
                if (value)
                {
                     try { button2_Click(null, null); }catch{}
                     //try {writer.Dispose(); }catch { }Thread[] th
                     //try {writer1.Dispose(); } catch { }
                     //try { th[0].Abort(); }catch { }
                     Stop = true; button1_Clicking = false;
                }
            }
        }

        public int Samplate=48000;
        public short bits=24;
        public byte Channels = 2;
        //public byte CallbackChannels = 2;
        WaveFormat wavFormat = new WaveFormat();
        FlacStreaminfo format_flac = new FlacStreaminfo();

        private FlacWriter FlacWrite;
        private WaveFileWriter WaveWriter;
        private HSBinaryWriter RAWWriter;

        private MemoryStream WriterMemoryStream;
        private FileStream WriterFileStream;

        volatile List<float> FilebufferList = new List<float>();

        public string DesktopPath { get { return Environment.GetFolderPath(Environment.SpecialFolder.Desktop)+"\\"; } }
        string outputFilename = Environment.GetFolderPath(Environment.SpecialFolder.Desktop)+"\\새 녹음.wav";
        HSAudioHelper Helper;
        ParameterizedThreadStart[] ps = new ParameterizedThreadStart[2];
        Thread[] th = new Thread[2];

        public bool NoiseMute;
        public WriteFormat Format { get; set; }


        #region 콜백 녹음 관련

        internal HS_Audio_Lib.DSP DSP;
        public HS_Audio_Lib.DSP_READCALLBACK dspreadcallback;
        internal HS_Audio_Lib.DSP_DESCRIPTION dspdesc;
        public HS_Audio_Lib.DSPConnection DSPConnection = new HS_Audio_Lib.DSPConnection();

        public void Reregister(bool IsSystem)
        {
            if (dspreadcallback==null) dspreadcallback = new DSP_READCALLBACK(READCALLBACK);
            this.dspdesc.read = this.dspreadcallback;
            Helper.system.createDSP(ref dspdesc, ref DSP);
            if (IsSystem) Helper.system.addDSP(DSP, ref DSPConnection);
            else Helper.channel.addDSP(DSP, ref DSPConnection);
        }
        #endregion

        public frmWaveDataOutputRecoding(HSAudioHelper Helper)
        {            
            try { DateTime dt = DateTime.Now; outputFilename=DesktopPath+string.Format("새 녹음 ({0}년 {1}월 {2}일 {3}시 {4}분 {5}초).wav", dt.Year, dt.Month.ToString("00"), dt.Day.ToString("00"), dt.Hour.ToString("00"), dt.Minute.ToString("00"), dt.Second.ToString("00")); } catch { }
            InitializeComponent();
            this.Helper = Helper;
            ps[0] = new ParameterizedThreadStart(WaveThread);
            //ps[1] = new ParameterizedThreadStart(WriteWaveThread);
            ps[1] = new ParameterizedThreadStart(WriteBufferThread);

            GetWaveEvent+=new WaveDelegate(frmWaveDataOutputRecoding_GetWaveEvent);

            //waveIn = new WaveIn();
            Samplate = Helper.DeviceFormat.Samplerate;
            bits = (short)Helper.DeviceFormat.Bits;
            Channels = (byte)Helper.DeviceFormat.OutputChannel;

            comboBox3.SelectedIndex = 0;
            comboBox2.Text = bits.ToString();
            PCMIndex = comboBox2.SelectedIndex;
            comboBox4.SelectedIndex = 0;
            textBox2.Text = "Wave [IEEE Float] {" + string.Format("{0} Hz, 32 bit, {1} Channel", Samplate, Channels) + "}";

            wavFormat = new WaveFormat(Samplate, bits,(int)Channels);
            //waveIn.DeviceNumber = 0;
            //waveIn.DataAvailable += new EventHandler<WaveInEventArgs>( waveIn_DataAvailable);
            //waveIn.WaveFormat.Encoding = WaveFormatEncoding.Pcm;  
            Helper.PlayingStatusChanged+=new HSAudioHelper.PlayingStatusChangedEventHandler(Helper_PlayingStatusChanged);
            textBox1.Text = Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "\\새 녹음.wav";
            dspreadcallback = new DSP_READCALLBACK(READCALLBACK);
            wc = new WaitCallback(WriteBufferThread);

            //Reregister(true);
            //writer = new WaveFileWriter(outputFilename, waveIn.WaveFormat);    
        }

        private void frmWaveDataOutputRecoding_GetWaveEvent(float[] L, float[] R, int Buffer)
        {
            throw new NotImplementedException();
        }
        void Helper_PlayingStatusChanged(HSAudioHelper.PlayingStatus ps, int index)
        {
            //if (ps == HSAudioHelper.PlayingStatus.Play) ;
            //else Stop = false;
        }

        private void frmWaveDataOutputRecoding_Load(object sender, EventArgs e)
        {
            comboBox1.SelectedIndex = 1;//다른 포맷 지원시 0으로 변경 
            comboBox1.Text = comboBox1.Items[comboBox1.SelectedIndex].ToString();
            /*
            comboBox3.SelectedIndex = 6;
            comboBox3.Text = comboBox3.Items[comboBox3.SelectedIndex].ToString();
            comboBox2.SelectedIndex = 1;
            comboBox2.Text = comboBox2.Items[comboBox2.SelectedIndex].ToString();
            comboBox4.SelectedIndex = 0;
            comboBox4.Text = comboBox4.Items[comboBox4.SelectedIndex].ToString();
            */
        }

        private void menuStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void checkBox2_Click(object sender, EventArgs e)
        {
            if (checkBox2.Checked)
            { MessageBox.Show("왠만하면 파일로 실시간 저장하는 것은 \n메모리가 부족하거나 디스크속도가 빠를 때만 사용해주세요\n\n이 옵션을 사용한다면 버퍼를 사용하는것을 추천드립니다.", "HS 플레이어 녹음 (베타)", MessageBoxButtons.OK, MessageBoxIcon.Warning); }
        }

        int buf = 16380;
        bool button1_Clicking;
        
        private void button1_Click(object sender, EventArgs e)
        {
            if (Stop)
            {
                
                _UseThread = UseThread;
                _UseBuffer = UseBuffer;
                TextWriteLength = 0;
                FileMaxBuffer = checkBox5.Checked?(int)numericUpDown5.Value:1024;
                stopw.Clear();
                if (wavFormat == null) {wavFormat = new WaveFormat(); btn적용_Click(null, null);}
                if(true)//IsCallbackWrite)
                {
                    try
                    { 
                        try { if(RAWWriter!=null)RAWWriter.Close();}catch{}
                        try {if(WaveWriter != null) WaveWriter.Close();}catch{}
                        try { if(WriterFileStream!=null)WriterFileStream.Dispose();}catch { }
                        try{ if(WriterMemoryStream!=null)WriterMemoryStream.Dispose(); }catch { }
                        try { if (FlacWrite != null) FlacWrite.Close(); } catch { }
                        //waveIn.WaveFormat.Channels = 2;
                        if (checkBox2.Checked) //실시간 파일로 저장
                        {
                            WriterFileStream = new FileStream(outputFilename, FileMode.Create, FileAccess.ReadWrite, FileShare.Read, FileMaxBuffer);

                            if (Format == WriteFormat.RAW) RAWWriter = new HSBinaryWriter(WriterFileStream);
                            else if (Format == WriteFormat.Text || Format == WriteFormat.Text_Float)
                            {
                                RAWWriter = new HSBinaryWriter(WriterFileStream, Encoding.ASCII, true);
                                RAWWriter.Write("                                                                                                       ");
                            }
                            else if (Format == WriteFormat.FLAC) { FlacWrite = new FlacWriter(WriterFileStream); FlacWrite.StartStream(format_flac);}
                            else WaveWriter = new WaveFileWriter(WriterFileStream, wavFormat);
                            //FlacEncodingPolicy fe = new FlacEncodingPolicy();FlacWrite.FindBestMethod()
                        }
                        else
                        {
                            WriterMemoryStream = new MemoryStream();
                            if(Format == WriteFormat.RAW)RAWWriter = new HSBinaryWriter(WriterMemoryStream);
                            else if(Format == WriteFormat.Text||Format == WriteFormat.Text_Float)
                            {
                                RAWWriter = new HSBinaryWriter(WriterMemoryStream,Encoding.ASCII, true);
                                RAWWriter.Write("                                                                                                       ");
                            }
                            else if(Format == WriteFormat.FLAC) { FlacWrite = new FlacWriter(WriterMemoryStream); FlacWrite.StartStream(format_flac);}
                            else { WaveWriter = new WaveFileWriter(WriterMemoryStream, wavFormat);}
                        }
                        //try { waveIn.StartRecording(); }catch{}
                        FlacCheck = 0;
                        Reregister(IsSystem);
                        Stop = panel2.Enabled = button6.Enabled = button4.Enabled = false;
                        stopw.Start();
                        timer1.Start();
                        button1_Clicking = true; 
                        button2.Enabled = true;
                        button1.Text = "일시 정지";
                        tabPage2.Text = "설정 (녹음중이라 비활성화 되었습니다.)";
                        if (!IsCallbackWrite) { try { th[0].Abort(); }catch { } th[0].Start();}
                        if (UseThread) {if(th[1]!=null)th[1].Abort();th[1] = new Thread(ps[1]);th[1].Start();}
                    }
                    catch (IOException) { MessageBox.Show("파일을 다른 프로그램에서 사용중인것 같습니다.\n\n파일을 사용중인 프로그램을 닫고 다시 시도해 주세요."); }
                    catch{}
                }
                   
            }
            else
            {
                if (button1_Clicking) { button1_Clicking = false; button1.Text = "재 시작"; stopw.Stop(); }
                else { button1_Clicking = true; button1.Text = "일시 정지"; stopw.Start(); }
            }
        }

        /*
        void WaveThread(object Buffer)
        {
            Thread.Sleep(2);
            uint buf = Convert.ToUInt32(Buffer);
            int buf1 = (int)buf;
            float[] buff = new float[buf*2];
            float[] buff1 = new float[buf];
            float[] buff2 = new float[buf];
            while(!Stop)
            {
                try
                {
                    Helper.system.getWaveData(buff1, buf1, 0);
                    Helper.system.getWaveData(buff2, buf1, 1);
                    for (int i = 0; i < buf1; i=i+2)
                    {
                        buff[i] = buff1[i];
                        buff[i + 1] = buff2[i];
                    }
                    //IntPtr bufferPtr = System.Runtime.InteropServices.Marshal.AllocCoTaskMem((int)buf);
                    //uint read = 0;
                    //Helper.sound.seekData(buf);
                    /*
                    IntPtr ptr1 = IntPtr.Zero;
                    IntPtr ptr2 = IntPtr.Zero;
                    uint len1 = 0;
                    uint len2 = 0;
                    Helper.sound.@lock(Offset, buf, ref ptr1, ref ptr2, ref len1, ref len2);*/
            //Helper.sound.readData(bufferPtr, buf, ref read);
            /*
            byte[] buffer1 = new byte[buf];
            byte[] buffer2 = new byte[buf];
            for (uint i = 0; i < buf; i++)
                unsafe
                {
                    try
                    {
                        byte* buffer = (byte*)bufferPtr.ToPointer();
                        buffer1[i] = buffer[i * 1];//sourceChannels
                        buffer2[i] = buffer[i * 1 + 1];//sourceChannels
                    }
                    catch { }
                }
            //try { System.Runtime.InteropServices.Marshal.Copy(bufferPtr, buff, 0, buf1); }catch{}
            //Helper.sound.unlock(ptr1, ptr2, len1, len2);
        }
        finally { writer.WriteSamples(buff, 0, buf1); writer.Flush(); /*Offset = Offset + buf; Thread.Sleep((int)numericUpDown4.Value); }
        //string a = "";
    }
    writer.Close(); writer.Dispose();
    }*/

        private void button2_Click(object sender, EventArgs e)
        {
            Stop = true;
            try{if(th[0]!=null)th[0].Abort();}catch{}
            try{if(th[1]!=null)th[1].Abort();}catch{}

            try{if (FilebufferList.Count > 0)foreach(float a in FilebufferList)this.WriteBuffer(a);}catch{}
            FilebufferList.Clear();
            if (Format == WriteFormat.Text || Format == WriteFormat.Text_Float)
            {
                try
                {
                    string a = string.Format("[ASCII {0}Hz, Channels: {1}, Samples: {2}", Samplate, Channels, TextWriteLength / Channels);
                    RAWWriter.Seek(0, SeekOrigin.Begin);
                    RAWWriter.Write(a);
                    if (Format == WriteFormat.Text_Float) RAWWriter.Write(string.Format(", Flags: {0}] \r\n", Channels == 1 ? "3" : "2"));
                    else if (Format == WriteFormat.Text && bits == 16) RAWWriter.Write(string.Format(", Flags: {0}]\r\n", Channels == 1 ? "5" : "4"));
                    else if (Format == WriteFormat.Text && bits == 24) RAWWriter.Write(string.Format(", Flags: {0}]\r\n", Channels == 1 ? "7" : "6"));
                }catch{}
            }

            try { WaveWriter.Close();WaveWriter = null;}catch{}
            try { RAWWriter.Close();RAWWriter = null;}catch{ }
            try { FlacWrite.Close(); FlacWrite = null; } catch { }
            //finally로 넣기
            if (radioButton8.Checked) { try { if (DSP != null)DSP.remove(); DSP = null; } catch { } }
            if (!checkBox2.Checked) File.WriteAllBytes(outputFilename, WriterMemoryStream.GetBuffer());

            try {WriterFileStream.Dispose();WriterFileStream = null;}catch { }
            try{WriterMemoryStream.Dispose();WriterMemoryStream = null; }catch { }

            button6.Enabled = button4.Enabled = true;
            panel2.Enabled = comboBox1.SelectedIndex != 5;
            timer1.Stop(); try {stopw.Stop();}catch{} button1.Text = "녹음 시작";tabPage2.Text = "설정";

            DateTime dt = DateTime.Now;
            string Dir = ""; try { Dir = Path.GetDirectoryName(outputFilename) + "\\"; }catch{}
            saveFileDialog1.FileName = outputFilename = textBox1.Text = Dir + string.Format("새 녹음 ({0}년 {1}월 {2}일 {3}시 {4}분 {5}초){6}", dt.Year, dt.Month.ToString("00"), dt.Day.ToString("00"), dt.Hour.ToString("00"), dt.Minute.ToString("00"), dt.Second.ToString("00"), Path.GetExtension(outputFilename));
        }

        private void numericUpDown4_ValueChanged(object sender, EventArgs e)
        {
            Sleep = (int)numericUpDown4.Value;
        }



        HS_Audio.StopWatch stopw = new HS_Audio.StopWatch();
        public enum TickFormat{Millisecond,Millisecond10_1,Millisecond100_1,Second};
        long LastPosion, CurrentPosition, AvgPosition;

        private void timer1_Tick(object sender, EventArgs e)
        {
            try {
            if (checkBox2.Checked) CurrentPosition = WriterFileStream.Length; else CurrentPosition = WriterMemoryStream.Length;
            AvgPosition=CurrentPosition-LastPosion;
            label11.Text = string.Format("녹음 크기: {0} Byte ({1} Byte)", CurrentPosition!=0?CurrentPosition.ToString("##,###"):"0", AvgPosition!=0?AvgPosition.ToString("##,###"):"0");
            label5.Text = string.Format("진행 시간: {0}", stopw.ToString(false));
            LastPosion = CurrentPosition;}catch{}
        }

        private void button4_Click(object sender, EventArgs e)
        {
            try { System.Diagnostics.Process.Start(outputFilename); }
            catch { MessageBox.Show("외부 플레이어로 녹음된 파일을 재생하는데 실패하였습니다."); }
        }
        //System.Media.SoundPlayer sp = new System.Media.SoundPlayer();
        //System.Media.SoundPlayer sp1 = new System.Media.SoundPlayer();
        bool button6_Clicking, button8_Clicking;
        private void button6_Click(object sender, EventArgs e)
        {
            try { System.Diagnostics.Process.Start(outputFilename); }
            catch { MessageBox.Show("외부 플레이어로 녹음된 파일을 재생하는데 실패하였습니다."); }
            /*
            if (!button6_Clicking)
            {
                sp.SoundLocation = outputFilename; //sp1.SoundLocation = outputFilename1;
                sp.Load(); //sp1.LoadAsync();
                try { sp.Play();  }catch{}
                //try { sp1.Play();  }catch{}
                button6.Text = "정지";
                button6_Clicking = true;
            }
            else
            { sp.Stop(); button6.Text = "미리 듣기"; button6_Clicking = false; }*/
        }

        private void button3_Click(object sender, EventArgs e)
        {
            DateTime dt = DateTime.Now;
            if (outputFilename != null) try { saveFileDialog1.InitialDirectory = Path.GetDirectoryName(outputFilename);}catch{}
            saveFileDialog1.FileName = string.Format("새 녹음 ({0}년 {1}월 {2}일 {3}시 {4}분 {5}초)", dt.Year, dt.Month.ToString("00"), dt.Day.ToString("00"), dt.Hour.ToString("00"), dt.Minute.ToString("00"), dt.Second.ToString("00"));
            if (saveFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                outputFilename = saveFileDialog1.FileName;
                textBox1.Text = saveFileDialog1.FileName;
            }
        }

        private void numericUpDown2_ValueChanged(object sender, EventArgs e)
        {
            timer1.Interval = (int)numericUpDown2.Value;
        }

        private void trackBar1_ValueChanged(object sender, EventArgs e)
        {
            numericUpDown2.Value = trackBar1.Value;
        }

        int PCMIndex = 1, FloatIndex = 0;
        private void btn적용_Click(object sender, EventArgs e)
        {
            //if (wavFormat == null) { wavFormat = new WaveIn();  }
            if(!Stop)MessageBox.Show("현재 설정값은 다음번 녹음부터 적용됩니다.");
            try
            {
                if (comboBox3.SelectedIndex > -1)
                    if (comboBox3.SelectedIndex == 0) Samplate = Helper.DeviceFormat.Samplerate;
                    else Samplate = Convert.ToInt32(comboBox3.Items[comboBox3.SelectedIndex].ToString().Replace("Hz", ""));
            }catch{ }

            //comboBox2.DropDownStyle = ComboBoxStyle.DropDown;
            
            byte channels = 2;
            {
                if (comboBox4.SelectedIndex == 1) channels = 2;
                else if (comboBox4.SelectedIndex == 2) channels = 1;
                else channels = this.Channels;
            }

            switch (comboBox1.SelectedIndex)
            {
                case 0:
                    format_flac.BitsPerSample = bits = Convert.ToInt16(comboBox2.Items[comboBox2.SelectedIndex]);
                    format_flac.ChannelsCount = Channels;
                    format_flac.SampleRate = Samplate;

                    format_flac.BitsPerSample = wavFormat.BitsPerSample;
                    format_flac.ChannelsCount = wavFormat.Channels;
                    format_flac.SampleRate = wavFormat.SampleRate;

                    textBox3.Text = "FLAC [PCM] {" + string.Format("{0} Hz, {1} Bit, {2} Channel", format_flac.SampleRate, format_flac.BitsPerSample, format_flac.ChannelsCount) + "}";
                    break;
                case 1:
                    if (comboBox2.SelectedIndex > -1)
                    {
                        comboBox2.Text = comboBox2.Items[comboBox2.SelectedIndex].ToString();
                        wavFormat.BitsPerSample = bits = Convert.ToInt16(comboBox2.Items[comboBox2.SelectedIndex]);
                    }
                    if (comboBox2.SelectedIndex == 3) wavFormat.Encoding = WaveFormatEncoding.Extensible;
                    else wavFormat.Encoding = WaveFormatEncoding.Pcm;
                    wavFormat = new WaveFormat(Samplate, bits, channels);
                    textBox3.Text = "Wave [PCM] {" + string.Format("{0} Hz, {1} Bit, {2} Channel", wavFormat.SampleRate, wavFormat.BitsPerSample, wavFormat.Channels) + "}";
                    break;
                case 2:
                    wavFormat = WaveFormat.CreateIeeeFloatWaveFormat(Samplate, Channels);
                    wavFormat.BitsPerSample = bits = comboBox2.SelectedIndex<0?(short)32:Convert.ToInt16(comboBox2.Items[comboBox2.SelectedIndex].ToString());
                    //waveIn.WaveFormat.Encoding = WaveFormatEncoding.IeeeFloat;
                    textBox3.Text = "Wave [IEEE Float] {" + string.Format("{0} Hz, {1} Bit, {2} Channel", wavFormat.SampleRate, wavFormat.BitsPerSample, wavFormat.Channels) + "}";
                    break;
                case 3: 
                    if (comboBox2.SelectedIndex > -1)
                    {
                        comboBox2.Text = comboBox2.Items[comboBox2.SelectedIndex].ToString();
                        bits = Convert.ToInt16(comboBox2.Items[comboBox2.SelectedIndex]);
                    }
                    wavFormat = new WaveFormat(Samplate, bits, channels);
                    textBox3.Text = "Text [PCM] {" + string.Format("{0} Hz, {1} Bit, {2} Channel", wavFormat.SampleRate, wavFormat.BitsPerSample, wavFormat.Channels) + "}";
                    break;
                case 4:
                    wavFormat = WaveFormat.CreateIeeeFloatWaveFormat(Samplate, Channels);
                    wavFormat.BitsPerSample = bits = comboBox2.SelectedIndex < 0 ? (short)32 : Convert.ToInt16(comboBox2.Items[comboBox2.SelectedIndex].ToString());
                    textBox3.Text = "Text [IEEE Float] {" + string.Format("{0} Hz, {1} Bit, {2} Channel", wavFormat.SampleRate, wavFormat.BitsPerSample, wavFormat.Channels) + "}";
                    break;
                default:
                    comboBox2.Text = "32 "; comboBox2.Enabled = false;
                    textBox3.Text = "RAW";
                    //textBox3.Text = "RAW [IEEE Float] {"+string.Format("{0} Hz, 32 bit, {1}", Samplate, Channels == 1 ? "Mono" : "Stereo")+"}"; 
                    break;
            }
        }

        private void radioButton8_CheckedChanged(object sender, EventArgs e)
        {
            if (!radioButton8.Checked)
            {
                if (MessageBox.Show("일반 추출은 잡음이 있을수 있습니다. 그리고 좌우가 따로 저장 됩니다.\n그러나 콜백 추출 보다는 안전합니다. 하지만 권장하지 않습니다.\n\n"+
                    "정말 일반 추출을 사용하시겠습니까?", "콜백 추출 권장", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == System.Windows.Forms.DialogResult.Yes)
                { groupBox8.Enabled = true; IsCallbackWrite = false; }
                else { radioButton8.Checked =IsCallbackWrite= true; }
            }
            else { groupBox8.Enabled =false; IsCallbackWrite = true; }
        }


        #region 일반, 콜백모드 공통 변수

        int FileMaxBuffer = 1024;
        public bool IsSystem { get { return radioButton4.Checked; } set { radioButton4.Checked = value; } }
        bool _IsSystem;

        public bool UseThread { get { return checkBox6.Checked; } set { checkBox6.Checked = value; } }
        bool _UseThread = true;
        public bool UseBuffer { get { return checkBox5.Checked; } set { checkBox5.Checked = value; } }
        bool _UseBuffer = false;

        float[] FilebufferArray = new float[1024];

        long TextWriteLength;
        int FlacCheck = 0;
        float[] flac_buf = new float[2];
        internal void WriteBuffer(float Sample)
        {
            try
            {
                if (Format == WriteFormat.FLAC && FlacWrite != null)
                {
                    if (FlacCheck < 1)
                    {
                        flac_buf[0] = Sample;
                        FlacCheck++;
                    }
                    else
                    {
                        flac_buf[1] = Sample;
                        FlacCheck = 0;

                        if (Channels == 1)FlacWrite.Write((flac_buf[0] + Sample) / 2f);
                        else FlacWrite.Write(flac_buf);
                    }

                }
                else if (Format == WriteFormat.RAW && RAWWriter != null) {RAWWriter.Write(Sample);RAWWriter.Flush();}
                else if (Format == WriteFormat.Text_Float&&RAWWriter!=null) {RAWWriter.Write(" \r\n");RAWWriter.Write(Sample.ToString("0.0000000"));RAWWriter.Flush(); TextWriteLength++;}
                else if (Format == WriteFormat.Text&&RAWWriter!=null)
                {
                    if (bits == 8 || bits == 16 || bits == 24 || bits == 32) RAWWriter.Write(" \r\n");
                    if (bits == 8)RAWWriter.Write(byte.MaxValue * Sample);
                    else if (bits == 16) RAWWriter.Write(Int16.MaxValue * Sample);
                    else if (bits == 24) RAWWriter.Write(Int32.MaxValue * Sample);
                    else if (bits == 32) RAWWriter.Write(UInt16.MaxValue * (Int32)Sample);
                    RAWWriter.Flush();
                    TextWriteLength++;
                }
                else if (WaveWriter!=null) WaveWriter.WriteSample(Sample);
            }
            catch(Exception ex){}
        }
        #endregion

        #region 일반 모드
        [Obsolete]
        public delegate void WaveDelegate(float[] L, float[] R, int Buffer);
        [Obsolete]
        public event WaveDelegate GetWaveEvent;
        bool Stop = true, Stop1;
        uint Offset = 0;
        int Sleep = 338;
        float[] buff = null;
        float[] buff1 = null;
        float[] writerbuff = null;

        void WaveThread(object Buffer)
        {
            _IsSystem = IsSystem;
            //Thread.Sleep(2);
            //uint buf = Convert.ToUInt32(Buffer);
            int i = 0;
            int buf1 =buf;
            buff = new float[buf1];
            buff1 = new float[buf1];
            writerbuff = new float[buf1 * 2];
            //writer.WaveFormat.SampleRate = Samplate / 2;
            //buff = new float[buf];
            while (!Stop)
            {
                try
                {
                    if (button1_Clicking)
                    {
                        if (_IsSystem)
                        {
                            if (Helper.system != null)
                            {
                                Helper.system.getWaveData(buff, buf1, 0);
                                Helper.system.getWaveData(buff1, buf1, 1);
                            }
                        }
                        else
                        {
                            if (Helper.channel != null)
                            {
                                Helper.channel.getWaveData(buff, buf1, 0);
                                Helper.channel.getWaveData(buff1, buf1, 1);
                            }
                        }
                        //IntPtr bufferPtr = System.Runtime.InteropServices.Marshal.AllocCoTaskMem((int)buf);
                        //uint read = 0;
                        //Helper.sound.seekData(buf);
                        /*
                        IntPtr ptr1 = IntPtr.Zero;
                        IntPtr ptr2 = IntPtr.Zero;
                        uint len1 = 0;
                        uint len2 = 0;
                        Helper.sound.@lock(Offset, buf, ref ptr1, ref ptr2, ref len1, ref len2);*/
                        //Helper.sound.readData(bufferPtr, buf, ref read);
                        /*
                        byte[] buffer1 = new byte[buf];
                        byte[] buffer2 = new byte[buf];
                        for (uint i = 0; i < buf; i++)
                            unsafe
                            {
                                try
                                {
                                    byte* buffer = (byte*)bufferPtr.ToPointer();
                                    buffer1[i] = buffer[i * 1];//sourceChannels
                                    buffer2[i] = buffer[i * 1 + 1];//sourceChannels
                                }
                                catch { }
                            }*/
                        //try { System.Runtime.InteropServices.Marshal.Copy(bufferPtr, buff, 0, buf1); }catch{}
                        //Helper.sound.unlock(ptr1, ptr2, len1, len2);

                        if (buff != null)
                        {
                            for (i = 0; i < buf1; i += 2){writerbuff[i] = buff[i]; writerbuff[i + 1] = buff1[i];}
                            //for (count = 0; count < buf1; count += 2)
                            for (count = 0; count < buf1; count += 2)
                            {
                                /*
                                for (count2 = 0; count2 < Channels; count2++)
                                {    
                                    if (NoiseMute)
                                    {
                                        if (Decibels(writerbuff[(count * Channels) + count2], 0) < 0)
                                            if (!_UseBuffer) writer.WriteSample(writerbuff[(count * Channels) + count2]);
                                            else FilebufferList.Add(writerbuff[(count * Channels) + count2]);
                                    }
                                    else
                                    {
                                        if (!_UseBuffer) writer.WriteSample(writerbuff[(count * Channels) + count2]);
                                        else FilebufferList.Add(writerbuff[(count * Channels) + count2]);
                                    }
                                }*/

                                if (NoiseMute)
                                {
                                    if (Decibels(writerbuff[count], 0) < 0 && Decibels(writerbuff[count + 1], 0) < 0)
                                        if (!_UseBuffer){this.WriteBuffer(writerbuff[count]); this.WriteBuffer(writerbuff[count + 1]);}
                                        else{ FilebufferList.Add(writerbuff[count]); FilebufferList.Add(writerbuff[count + 1]);}
                                }
                                else
                                {
                                    if (!_UseBuffer){this.WriteBuffer(writerbuff[count]); this.WriteBuffer(writerbuff[count + 1]); }
                                    else {FilebufferList.Add(writerbuff[count]); FilebufferList.Add(writerbuff[count + 1]);}
                                }
                            }
                            if (!_UseThread)
                            {
                                if (FilebufferList.Count >= FileMaxBuffer)
                                {
                                    //if (_UseThread) ThreadPool.QueueUserWorkItem(wc, FilebufferList.ToArray());
                                    if (!UseThread) try{for (int a = 0; a < WriteBufferThread_Buf.Length; a++) this.WriteBuffer(WriteBufferThread_Buf[a]);}catch{}
                                    FilebufferList.Clear();
                                }
                            }
                        }
                    }
                }
                catch (Exception ex) { System.Diagnostics.Debug.WriteLine(ex.ToString()); Console.WriteLine(ex.ToString()); }
                finally
                {
                    //if (GetWaveEvent != null) GetWaveEvent(buff, buff1, buf);

                    Thread.Sleep(Sleep);
                    /*Offset = Offset + buf;*/
                }
                //string a = "";
            }
            //writer.Dispose();
            buff = null; buff1 = null; Stop = true; th[0].Abort();
        }

        #endregion

        #region 콜백 모드
        WaitCallback wc;
        //List<float> FilebufferList_Callback = new List<float>();

        uint count = 0;
        int count2 = 0;
        IntPtr thisdspraw;
        public HS_Audio_Lib.RESULT READCALLBACK(ref HS_Audio_Lib.DSP_STATE State, IntPtr inbuf, IntPtr outbuf, uint length, int inchannels, int outchannels)
        {
            if (true)
            {
                thisdspraw = State.instance;
                //if (dsp_static != null) { dsp_static.setRaw(thisdspraw); }
                //else { dsp_static = new HS_Audio_Lib.DSP(); dsp_static.setRaw(thisdspraw); }

                unsafe
                {
                    float* inbuffer = (float*)inbuf.ToPointer();
                    float* outbuffer = (float*)outbuf.ToPointer();

                    for (count = 0; count < length; count++)
                    {
                        for (count2 = 0; count2 < outchannels; count2++)
                        {
                            outbuffer[(count * outchannels) + count2] = inbuffer[(count * inchannels) + count2];
                            try 
                            {
                                if (!Stop||button1_Clicking)
                                {
                                    if (NoiseMute)
                                    {
                                        //if (Decibels(outbuffer[(count * inchannels) + count2], 0) < 0)
                                            if (!_UseThread) this.WriteBuffer(outbuffer[(count * inchannels) + count2]);
                                            else FilebufferList.Add(outbuffer[(count * inchannels) + count2]);//FilebufferList_Callback.Add(outbuffer[(count * inchannels) + count2]);
                                    }
                                    else
                                    {
                                        if (!_UseThread) this.WriteBuffer(outbuffer[(count * inchannels) + count2]);
                                        else FilebufferList.Add(outbuffer[(count * inchannels) + count2]);//FilebufferList_Callback.Add(outbuffer[(count * inchannels) + count2]);
                                    }
                                } }catch { }
                        }
                    }
                }
                if (!_UseThread)
                {
                    if (FilebufferList.Count >= FileMaxBuffer && WriteBufferThread_Buf!=null)//FilebufferList_Callback.Count >= FileMaxBuffer)
                    {
                        for (int a = 0; a < WriteBufferThread_Buf.Length; a++) this.WriteBuffer(WriteBufferThread_Buf[a]);
                        FilebufferList.Clear();//FilebufferList_Callback.Clear();
                    }
                }
                
            }
            return HS_Audio_Lib.RESULT.OK;
        }

        float[] WriteBufferThread_Buf;
        int WriteBufferThread_i;
        object o = new object();
        private void WriteBufferThread(object Buffer)
        {
            while (!Stop)
            {
                if (checkBox5.Checked)
                {
                    while (FilebufferList.Count >= FileMaxBuffer)//FilebufferList_Callback.Count >= FileMaxBuffer)
                    {
                        WriteBufferThread_Buf = FilebufferList.ToArray();
                        //FilebufferList_Callback.RemoveRange(0, WriteBufferThread_Buf.Length - 1);
                        if(FilebufferList.Count >= FileMaxBuffer)FilebufferList.RemoveRange(0, WriteBufferThread_Buf.Length);
                        for (int a = 0; a < WriteBufferThread_Buf.Length; a++) this.WriteBuffer(WriteBufferThread_Buf[a]);
                        Thread.Sleep(1);
                    }
                }
                else
                {
                    while (FilebufferList.Count >= FileMaxBuffer)//FilebufferList_Callback.Count > 0)
                    {
                        this.WriteBuffer(FilebufferList[0]);//FilebufferList_Callback[0]);
                        FilebufferList.RemoveAt(0);//FilebufferList_Callback.RemoveAt(0);
                        Thread.Sleep(1);
                    }
                }
            }
        }
        #endregion
        

        public static double mag_sqrd(double re, double im) { return (re * re + im * im); }

        public static double Decibels(double re, double im) { return ((re == 0 && im == 0) ? (0) : 10.0 * Math.Log10(((mag_sqrd(re, im))))); }

        private void checkBox4_CheckedChanged(object sender, EventArgs e)
        {
            NoiseMute = checkBox4.Checked;
        }

        private void 녹음도움말ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("녹음할때 반드시 Peak그래프와 같이 봐주시기 바랍니다.\n만약 0.000dB 보다 크면 출력사운드가 심하게 왜곡됩니다. (IEEE Float 제외)\n\n" +
                "녹음시에 음량을 조금 낮게 맞추어놓고 Peak바가 -X.XXX dB 가 보이도록 해주시기 바랍니다. \n(대부분 0.000dB 이상 되면 왜곡됬었을 확률이 100% 입니다.)\n\n"+
                "그리고 녹음중 노래가 떨려도 녹음에 지장이 없습니다. (일반 모드 제외)\n\n"+
                //"그리고 만약 녹음중 노래가 떨린다면 녹음을 중지한 후 설정에서 버퍼사용을 체크해 주세요 \n아니면 파일로 실시간 기록을 해제하고 녹음하시기 바랍니다.\n\n" +
                "반드시 테스트로 미리 녹음해 보시길 권장합니다.",
                "녹음 도움말", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void checkBox5_EnabledChanged(object sender, EventArgs e) { groupBox6.Enabled = checkBox5.Enabled; }

        private void checkBox5_CheckedChanged(object sender, EventArgs e) { groupBox6.Enabled = checkBox5.Checked; }

        private void 끝내기XToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void frmWaveDataOutputRecoding_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = true;
            if (!Stop)
            {
                if (MessageBox.Show("현재 녹음중입니다.\n\n녹음을 중지하고 저장한후 종료하시겠습니까?", "현재 출력사운드 녹음하기", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == System.Windows.Forms.DialogResult.Yes)
                { button2_Click(null, null); e.Cancel = false; }
            }
            else { e.Cancel = false; }
        }
        void waveIn_DataAvailable(object sender, WaveInEventArgs e)
        {
            //label11.Text = "녹음 크기: " + e.BytesRecorded.ToString("##,###");
            //WriteData(e.Buffer, 0, e.BytesRecorded);
        }

        int lastindex_comboBox1 = 1;
        //True 면 병경가능 False면 변경 불가능
        bool IsFloatIndex, IsPCMIndex = true;
        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            comboBox2.DropDownStyle = ComboBoxStyle.DropDownList;
            if (comboBox1.SelectedIndex > 0) lastindex_comboBox1 = comboBox1.SelectedIndex;
            else
            {
                comboBox1.SelectedIndex = lastindex_comboBox1;
                MessageBox.Show("죄송하지만, FLAC 형식은 추후 지원예정입니다.\n그때까지 다른 저장형식을 선택해주세요.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            switch (comboBox1.SelectedIndex)
            {
                case 0:
                case 1:
                case 3:
                    if (IsFloatIndex) FloatIndex = comboBox2.SelectedIndex < 0 ? 0 : comboBox2.SelectedIndex;
                    IsFloatIndex = false;
                    IsPCMIndex = true;
                    comboBox2.Items.Clear();
                    comboBox2.Items.AddRange(new object[] { 8, 16, 24, 32 });
                    comboBox2.SelectedIndex = PCMIndex;
                    comboBox2.Enabled = true; break;

                default:
                    if(IsPCMIndex)PCMIndex = comboBox2.SelectedIndex;
                    IsFloatIndex = true;
                    IsPCMIndex = false;
                    comboBox2.Items.Clear();
                    comboBox2.Items.AddRange(new object[] { 32, 64 });
                    comboBox2.SelectedIndex = FloatIndex;
                    comboBox2.Enabled = false;
                    break;
            }
            
            DateTime dt = DateTime.Now;
            string Dir = ""; try { Dir = Path.GetDirectoryName(outputFilename) + "\\"; }catch{Dir=DesktopPath;}
            if(comboBox1.SelectedIndex == 0)
            {
                Format = WriteFormat.FLAC;

                saveFileDialog1.DefaultExt = "*.flac"; saveFileDialog1.Filter = "무손실 음원 파일 (*.flac)|*.flac|모든 파일 (*.*)|*.*";
                if (saveFileDialog1.FileName == null || saveFileDialog1.FileName == "") saveFileDialog1.FileName = outputFilename = Dir + string.Format("새 녹음 ({0}년 {1}월 {2}일 {3}시 {4}분 {5}초).flac", dt.Year, dt.Month.ToString("00"), dt.Day.ToString("00"), dt.Hour.ToString("00"), dt.Minute.ToString("00"), dt.Second.ToString("00"));
                else try { saveFileDialog1.FileName = outputFilename = saveFileDialog1.FileName.Remove(outputFilename.LastIndexOf(".")) + ".flac"; } catch { }
                textBox1.Text = outputFilename;
            }
            else if(comboBox1.SelectedIndex == 1||comboBox1.SelectedIndex == 2)
            {
                if (comboBox1.SelectedIndex == 1) Format = WriteFormat.WAVE; else Format = WriteFormat.WAVE_Float;

                groupBox2.Enabled = radioButton1.Enabled = true;
                saveFileDialog1.DefaultExt = "*.wav"; saveFileDialog1.Filter = "웨이브 파일 (*.wav)|*.wav|모든 파일 (*.*)|*.*";
                if (saveFileDialog1.FileName == null || saveFileDialog1.FileName == "") saveFileDialog1.FileName =outputFilename= Dir + string.Format("새 녹음 ({0}년 {1}월 {2}일 {3}시 {4}분 {5}초).wav", dt.Year, dt.Month.ToString("00"), dt.Day.ToString("00"), dt.Hour.ToString("00"), dt.Minute.ToString("00"), dt.Second.ToString("00"));
                else try { saveFileDialog1.FileName = outputFilename = saveFileDialog1.FileName.Remove(outputFilename.LastIndexOf(".")) + ".wav"; }catch{}
                textBox1.Text = outputFilename;
                //comboBox2.Enabled = comboBox1.SelectedIndex == 1;
            }
            else if(comboBox1.SelectedIndex == 3||comboBox1.SelectedIndex == 4)
            {
                if (comboBox1.SelectedIndex == 2) Format = WriteFormat.Text; else Format = WriteFormat.Text_Float;
                groupBox2.Enabled = radioButton1.Enabled = true;
                saveFileDialog1.DefaultExt = "*.txt"; saveFileDialog1.Filter = "텍스트 파일 (*.txt)|*.txt|모든 파일 (*.*)|*.*";
                if (saveFileDialog1.FileName == null || saveFileDialog1.FileName == "") saveFileDialog1.FileName =outputFilename= Dir + string.Format("새 녹음 ({0}년 {1}월 {2}일 {3}시 {4}분 {5}초).txt", dt.Year, dt.Month.ToString("00"), dt.Day.ToString("00"), dt.Hour.ToString("00"), dt.Minute.ToString("00"), dt.Second.ToString("00"));
                else try { saveFileDialog1.FileName = outputFilename = saveFileDialog1.FileName.Remove(outputFilename.LastIndexOf(".")) + ".txt"; }catch{}
                textBox1.Text = outputFilename;
                //comboBox2.Enabled = comboBox1.SelectedIndex == 1;
            }
            else
            {
                Format = WriteFormat.RAW; groupBox2.Enabled = radioButton1.Enabled = false;
                saveFileDialog1.DefaultExt = "*.raw"; saveFileDialog1.Filter = "원본 사운드 파일 (*.raw)|*.raw|모든 파일 (*.*)|*.*";
                if (saveFileDialog1.FileName == null || saveFileDialog1.FileName == "") saveFileDialog1.FileName =outputFilename= Dir+string.Format("새 녹음 ({0}년 {1}월 {2}일 {3}시 {4}분 {5}초).raw", dt.Year, dt.Month.ToString("00"), dt.Day.ToString("00"), dt.Hour.ToString("00"), dt.Minute.ToString("00"), dt.Second.ToString("00"));
                else try { saveFileDialog1.FileName = outputFilename = saveFileDialog1.FileName.Remove(outputFilename.LastIndexOf(".")) + ".raw"; }catch{}
                textBox1.Text = outputFilename;
            }
            btn적용_Click(null, null);
        }

        private void comboBox4_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBox4.SelectedIndex == 0) Channels = 2;
            else if (comboBox4.SelectedIndex == 1) Channels = 1;
            else Channels = (byte)Helper.DeviceFormat.OutputChannel;
        }
        /*
        private static long TotalSecond;
        private static byte Hour, Minute, Second;
        private static long MilliSecond;
        private static System.Text.StringBuilder sb = new System.Text.StringBuilder();
        private static bool IsMinus;

        public static string GetTimeFormat(long Tick, TickFormat format = TickFormat.Millisecond, bool AttachZero=true, bool MilliSecondIsDot=true)
        {
            frmWaveDataOutputRecoding.sb.Remove(0, frmWaveDataOutputRecoding.sb.Length);
            IsMinus = Tick < 0;
            if (format == TickFormat.Millisecond)
            {
                TotalSecond = Tick / 1000;
                MilliSecond = Tick % 1000;
                if (TotalSecond > 60) { }
            }
        }*/
    }

    [Serializable, ComVisible(true)]
    public class HSBinaryWriter : IDisposable
    {
        public Encoding BaseEncoding{get;private set;}
        StreamWriter sw;
        BinaryWriter bw;
        bool TextWriteMode;
        protected HSBinaryWriter(bool TextWriteMode = false){this.TextWriteMode = TextWriteMode;this.BaseEncoding = new UTF8Encoding(false, true);}

        public HSBinaryWriter(Stream output, bool TextWriteMode = false) : this(output, new UTF8Encoding(false, true),TextWriteMode){}

        public HSBinaryWriter(Stream output, Encoding encoding, bool TextWriteMode = false) 
        {
            OwnerStream = output;
            this.TextWriteMode = TextWriteMode;this.BaseEncoding = encoding;
            if(TextWriteMode)sw = new StreamWriter(output, encoding);
            else bw = new BinaryWriter(output, encoding);
        }
        public virtual void Write(bool value){if(TextWriteMode)sw.Write(value);else bw.Write(value);}

        public virtual void Write(byte value){if(TextWriteMode)sw.Write(value);else bw.Write(value);}
        public virtual void Write(char ch){if(TextWriteMode)sw.Write(ch);else bw.Write(ch); }

        public virtual void Write(byte[] buffer) {if (buffer == null)return; if(TextWriteMode)sw.Write(BaseEncoding.GetString(buffer, 0, buffer.Length));else bw.Write(buffer);}

        public virtual void Write(decimal value){if(TextWriteMode)sw.Write(value);else bw.Write(value);}

        public virtual void Write(double value){if(TextWriteMode)sw.Write(value);else bw.Write(value);}

        public virtual void Write(char[] chars) {if (chars == null)return;if(TextWriteMode)sw.Write(chars);else bw.Write(chars);}

        public virtual void Write(short value){if(TextWriteMode)sw.Write(value);else bw.Write(value);}

        public virtual void Write(int value){if(TextWriteMode)sw.Write(value);else bw.Write(value);}

        public virtual void Write(long value){if(TextWriteMode)sw.Write(value);else bw.Write(value);}

        public virtual void Write(sbyte value){if(TextWriteMode)sw.Write(value);else bw.Write(value);}

        public virtual void Write(float value){if(TextWriteMode)sw.Write(value);else bw.Write(value);}

        public virtual void Write(string value)
        {
            if (value == null)return;
            if (TextWriteMode) sw.Write(value); else bw.Write(value);
            //base.Write(this._encoding.GetBytes(value));
            /*
            int byteCount = this._encoding.GetByteCount(value);
            this.Write7BitEncodedInt(byteCount);
            if (this._largeByteBuffer == null)
            {
                this._largeByteBuffer = new byte[0x100];
                this._maxChars = 0x100 / this._encoding.GetMaxByteCount(1);
            }
            if (byteCount <= 0x100)
            {
                char[] ad = value[];
                this._encoding.GetBytes(value, 0, value.Length, this._largeByteBuffer, 0);
                this.OutStream.Write(this._largeByteBuffer, 0, byteCount);
            }*/
        }

        public virtual void Write(ushort value){if(TextWriteMode)sw.Write(value);else bw.Write(value);}

        public virtual void Write(uint value){if(TextWriteMode)sw.Write(value);else bw.Write(value);}

        public virtual void Write(ulong value){if(TextWriteMode)sw.Write(value);else bw.Write(value);}

        public virtual void Write(byte[] buffer, int index, int count){if(TextWriteMode)sw.Write(BaseEncoding.GetString(buffer, index, count));else bw.Write(buffer, index, count); }

        public virtual void Write(char[] chars, int index, int count) {if(TextWriteMode)sw.Write(chars, index, count);else bw.Write(chars, index, count); }

        //public override bool CanRead{get{return false;}}

        public virtual bool CanSeek{get{return !TextWriteMode;}}

        public virtual bool CanWrite{get{return true;}}

        public virtual void Flush(){if(TextWriteMode)sw.Flush();else bw.Flush();}

        //public override long Length{get{if(TextWriteMode)bw.}}

        //public override long Position{get{return bw.BaseStream.}}

        public virtual long Seek(int offset, SeekOrigin origin)
        {
            //return BaseStream.Seek(offset, origin);
            if (TextWriteMode) return BaseStream.Seek(offset, origin);//throw new NotImplementedException();
            else return bw.Seek(offset, origin);
        }

        protected void Close(bool All)
        {
            if (All) Close();
            else
            {
                if(TextWriteMode){if (sw != null) sw.Close();}
                else if (bw != null) bw.Close();
            }
        }
        public virtual void Close()
        {
            if (bw != null) bw.Close();
            if (sw != null) sw.Close();
        }

        #region IDisposable 멤버
        public void Dispose(bool All)
        {
            if (All) Dispose();
            else
            {
                if (TextWriteMode){ if (sw != null) sw.Dispose();}
                else if (bw != null) bw.Close();
            }
        }
        public void Dispose()
        {
            if(bw!=null)bw.Close();
            if(sw!=null)sw.Dispose();
        }

        #endregion

        

        protected Stream OwnerStream;
        public Stream UseBaseStream{get{return OwnerStream;}}
        public Stream BaseStream{get{return TextWriteMode?sw.BaseStream:bw.BaseStream;}}
    }
}
