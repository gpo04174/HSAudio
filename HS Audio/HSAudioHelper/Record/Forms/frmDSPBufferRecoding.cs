﻿using System;
using System.ComponentModel;

using System.Windows.Forms;
using System.IO;
using HS_Audio_Lib;
using NAudio.Wave;

namespace HS_Audio.Recoder.Forms
{
    public partial class frmRecoding : Form
    {
        HSAudioHelper helper;

        public int Samplate = 48000;
        public int bits = 16;
        public byte Channels = 2;

        internal HS_Audio_Lib.DSP dsp_static;
        public HS_Audio_Lib.DSP_READCALLBACK dspreadcallback;

        private WaveIn waveIn;
        private WaveFileWriter writer;

        bool WriteStart;
        public bool NoiseMute;
        
        public frmRecoding(HSAudioHelper Helper)
        {
            InitializeComponent();
            this.helper = Helper;

            waveIn = new WaveIn();
            waveIn.WaveFormat = new WaveFormat(Samplate, bits, (int)Channels);
            waveIn.DeviceNumber = 0;

            dspreadcallback = new HS_Audio_Lib.DSP_READCALLBACK(READCALLBACK);
            Reregister();
            //w = new Recoder.SaveWaveDSPBuffer(Helper);
        }
        public HS_Audio_Lib.DSP DSP;
        internal DSP_DESCRIPTION dspdesc;

        public HS_Audio_Lib.DSPConnection DSPConnection = new DSPConnection();

        
        public void Reregister()
        {
            try { if(DSP!=null)DSP.release(); }catch{}
            this.dspdesc.read = this.dspreadcallback;
            helper.system.createDSP(ref dspdesc, ref DSP);
            helper.system.addDSP(DSP, ref DSPConnection);
        }
        HS_Audio_Lib.RESULT Result { get; set; }

        HS_Audio.Recoder.SaveWaveDSPBuffer w;

        private void button1_Click(object sender, EventArgs e)
        {
           // timer1.Start();
            //w.StartWrite();
            try { writer.Dispose(); }catch { }
            writer = new WaveFileWriter(outputFilename, waveIn.WaveFormat);
            WriteStart = true;
        }
        FileStream fs;
        StreamWriter sw;
        private void button2_Click(object sender, EventArgs e)
        {

        }

        private void saveFileDialog1_FileOk(object sender, CancelEventArgs e)
        {
            fs = new FileStream(saveFileDialog1.FileName, FileMode.Append);
            byte[] a=w.WriteWaveNew(w).GetBuffer();
            fs.Write(a, 0, a.Length);
            sw = new StreamWriter(fs);
            sw.Flush(); sw.Close(); fs.Close();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            //w.StopAndDispose();
            WriteStart = false;
            writer.Dispose();
            button2.Enabled = true;
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            label1.Text = w.Data()[w.Data().Length / 2].ToString();
        }


        string outputFilename = Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "\\새 녹음.wav";
       // string outputFilename1 = Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "\\새 녹음1_우.wav";
        private void frmRecoding_Load(object sender, EventArgs e)
        {

        }



        uint count = 0;
        int count2 = 0;
        IntPtr thisdspraw;
        float[] outbuffer = new float[1024];
        public HS_Audio_Lib.RESULT READCALLBACK(ref HS_Audio_Lib.DSP_STATE State, IntPtr inbuf, IntPtr outbuf, uint length, int inchannels, int outchannels)
        {
            if (true)
            {
                thisdspraw = State.instance;
                //if (dsp_static != null) { dsp_static.setRaw(thisdspraw); }
                //else { dsp_static = new HS_Audio_Lib.DSP(); dsp_static.setRaw(thisdspraw); }

                unsafe
                {
                    float* inbuffer = (float*)inbuf.ToPointer();
                    float* outbuffer = (float*)outbuf.ToPointer();

                    for (count = 0; count < length; count++)
                    {
                        for (count2 = 0; count2 < outchannels; count2++)
                        {
                            outbuffer[(count * outchannels) + count2] = inbuffer[(count * inchannels) + count2];
                            try { if(WriteStart){
                                if(NoiseMute)
                                writer.WriteSample(outbuffer[(count * inchannels) + count2]);} }catch{}
                        }
                    }
                }
            }
            return HS_Audio_Lib.RESULT.OK;
        }
    }
}
