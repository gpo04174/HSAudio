﻿namespace HS_Audio.Recoder
{
    partial class frmNorebang
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            dsp.Dispose();
            try { MicWavein.Dispose(); } catch { }
            try { MicPeakThread.Abort(); } catch { }
            try { StartQueueThread.Abort(); } catch { }

            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.chkMic = new System.Windows.Forms.CheckBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label8 = new System.Windows.Forms.Label();
            this.numericUpDown1 = new System.Windows.Forms.NumericUpDown();
            this.label5 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.lblMicdBR = new System.Windows.Forms.Label();
            this.progressBarGradient2 = new HS_Audio.Control.ProgressBarGradientInternal();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.button3 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.checkBox3 = new System.Windows.Forms.CheckBox();
            this.button1 = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.domainUpDown1 = new System.Windows.Forms.DomainUpDown();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.label3 = new System.Windows.Forms.Label();
            this.cbMicChannel = new System.Windows.Forms.ComboBox();
            this.lblMicdBL = new System.Windows.Forms.Label();
            this.chkNomalize = new System.Windows.Forms.CheckBox();
            this.comboBox2 = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.chkMicBypass = new System.Windows.Forms.CheckBox();
            this.progressBarGradient1 = new HS_Audio.Control.ProgressBarGradientInternal();
            this.label12 = new System.Windows.Forms.Label();
            this.trackBar3 = new System.Windows.Forms.TrackBar();
            this.chkAutoDeviceDetect = new System.Windows.Forms.CheckBox();
            this.label13 = new System.Windows.Forms.Label();
            this.btnSoundDeviceRefresh = new System.Windows.Forms.Button();
            this.cbInputDevice = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel2 = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel3 = new System.Windows.Forms.ToolStripStatusLabel();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.파일FToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.닫기XToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.보기VToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dB레이블보이기ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.설정TToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.전개발자입니다ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.도움말HToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.에코음정설정법ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.음량조절음소거ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar3)).BeginInit();
            this.statusStrip1.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // chkMic
            // 
            this.chkMic.AutoSize = true;
            this.chkMic.ForeColor = System.Drawing.Color.Blue;
            this.chkMic.Location = new System.Drawing.Point(10, 1);
            this.chkMic.Name = "chkMic";
            this.chkMic.Size = new System.Drawing.Size(88, 16);
            this.chkMic.TabIndex = 0;
            this.chkMic.Text = "마이크 켜기";
            this.chkMic.UseVisualStyleBackColor = true;
            this.chkMic.CheckedChanged += new System.EventHandler(this.chkMic_CheckedChanged);
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.numericUpDown1);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.lblMicdBR);
            this.groupBox1.Controls.Add(this.progressBarGradient2);
            this.groupBox1.Controls.Add(this.groupBox2);
            this.groupBox1.Controls.Add(this.checkBox1);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.cbMicChannel);
            this.groupBox1.Controls.Add(this.lblMicdBL);
            this.groupBox1.Controls.Add(this.chkNomalize);
            this.groupBox1.Controls.Add(this.comboBox2);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.chkMicBypass);
            this.groupBox1.Controls.Add(this.progressBarGradient1);
            this.groupBox1.Controls.Add(this.label12);
            this.groupBox1.Controls.Add(this.trackBar3);
            this.groupBox1.Controls.Add(this.chkAutoDeviceDetect);
            this.groupBox1.Controls.Add(this.label13);
            this.groupBox1.Controls.Add(this.btnSoundDeviceRefresh);
            this.groupBox1.Controls.Add(this.cbInputDevice);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.chkMic);
            this.groupBox1.Location = new System.Drawing.Point(1, 28);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(512, 251);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(114, 227);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(20, 12);
            this.label8.TabIndex = 41;
            this.label8.Text = "dB";
            // 
            // numericUpDown1
            // 
            this.numericUpDown1.DecimalPlaces = 3;
            this.numericUpDown1.Location = new System.Drawing.Point(36, 222);
            this.numericUpDown1.Maximum = new decimal(new int[] {
            90,
            0,
            0,
            0});
            this.numericUpDown1.Minimum = new decimal(new int[] {
            90,
            0,
            0,
            -2147483648});
            this.numericUpDown1.Name = "numericUpDown1";
            this.numericUpDown1.Size = new System.Drawing.Size(74, 21);
            this.numericUpDown1.TabIndex = 40;
            this.numericUpDown1.ValueChanged += new System.EventHandler(this.numericUpDown1_ValueChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(3, 227);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(29, 12);
            this.label5.TabIndex = 39;
            this.label5.Text = "증폭";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.ForeColor = System.Drawing.Color.Blue;
            this.label7.Location = new System.Drawing.Point(1, 133);
            this.label7.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(17, 12);
            this.label7.TabIndex = 38;
            this.label7.Text = "우";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.ForeColor = System.Drawing.Color.Blue;
            this.label6.Location = new System.Drawing.Point(1, 109);
            this.label6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(17, 12);
            this.label6.TabIndex = 37;
            this.label6.Text = "좌";
            // 
            // lblMicdBR
            // 
            this.lblMicdBR.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblMicdBR.AutoSize = true;
            this.lblMicdBR.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblMicdBR.ForeColor = System.Drawing.Color.Blue;
            this.lblMicdBR.Location = new System.Drawing.Point(442, 131);
            this.lblMicdBR.Name = "lblMicdBR";
            this.lblMicdBR.Size = new System.Drawing.Size(59, 13);
            this.lblMicdBR.TabIndex = 36;
            this.lblMicdBR.Text = "-320.00 dB";
            // 
            // progressBarGradient2
            // 
            this.progressBarGradient2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.progressBarGradient2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.progressBarGradient2.IntervaldB = 5;
            this.progressBarGradient2.Location = new System.Drawing.Point(24, 130);
            this.progressBarGradient2.MaximumColor = System.Drawing.Color.Crimson;
            this.progressBarGradient2.MaximumValue = 30000;
            this.progressBarGradient2.MidColor = System.Drawing.Color.Goldenrod;
            this.progressBarGradient2.MidColorPosition = 0.7F;
            this.progressBarGradient2.MindB = -60;
            this.progressBarGradient2.MinimumColor = System.Drawing.Color.Lime;
            this.progressBarGradient2.MinimumValue = 0;
            this.progressBarGradient2.MouseInteractive = false;
            this.progressBarGradient2.Name = "progressBarGradient2";
            this.progressBarGradient2.Orientation = System.Windows.Forms.Orientation.Horizontal;
            this.progressBarGradient2.ShowMidColor = true;
            this.progressBarGradient2.ShowMinus = true;
            this.progressBarGradient2.ShowPlus = false;
            this.progressBarGradient2.Size = new System.Drawing.Size(414, 17);
            this.progressBarGradient2.TabIndex = 35;
            this.progressBarGradient2.Value = 10;
            this.progressBarGradient2.VisibledB = true;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.button3);
            this.groupBox2.Controls.Add(this.button2);
            this.groupBox2.Controls.Add(this.checkBox3);
            this.groupBox2.Controls.Add(this.button1);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.domainUpDown1);
            this.groupBox2.Enabled = false;
            this.groupBox2.ForeColor = System.Drawing.Color.Blue;
            this.groupBox2.Location = new System.Drawing.Point(156, 152);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox2.Size = new System.Drawing.Size(276, 92);
            this.groupBox2.TabIndex = 34;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "개발자 기능 (설정(&T) 에서 활성화 가능)";
            // 
            // button3
            // 
            this.button3.ForeColor = System.Drawing.Color.Black;
            this.button3.Location = new System.Drawing.Point(111, 18);
            this.button3.Margin = new System.Windows.Forms.Padding(2);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(139, 21);
            this.button3.TabIndex = 35;
            this.button3.Text = "Peak Queue Flush!";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button2
            // 
            this.button2.ForeColor = System.Drawing.Color.Black;
            this.button2.Location = new System.Drawing.Point(111, 43);
            this.button2.Margin = new System.Windows.Forms.Padding(2);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(156, 21);
            this.button2.TabIndex = 34;
            this.button2.Text = "Over/Under Flow Reset";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // checkBox3
            // 
            this.checkBox3.AutoSize = true;
            this.checkBox3.ForeColor = System.Drawing.Color.Black;
            this.checkBox3.Location = new System.Drawing.Point(6, 47);
            this.checkBox3.Margin = new System.Windows.Forms.Padding(2);
            this.checkBox3.Name = "checkBox3";
            this.checkBox3.Size = new System.Drawing.Size(101, 16);
            this.checkBox3.TabIndex = 33;
            this.checkBox3.Text = "Queue Pause";
            this.checkBox3.UseVisualStyleBackColor = true;
            this.checkBox3.CheckedChanged += new System.EventHandler(this.checkBox3_CheckedChanged_1);
            // 
            // button1
            // 
            this.button1.ForeColor = System.Drawing.Color.Black;
            this.button1.Location = new System.Drawing.Point(4, 18);
            this.button1.Margin = new System.Windows.Forms.Padding(2);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(96, 21);
            this.button1.TabIndex = 32;
            this.button1.Text = "Queue Flush!";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(5, 71);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(29, 12);
            this.label4.TabIndex = 29;
            this.label4.Text = "버퍼";
            // 
            // domainUpDown1
            // 
            this.domainUpDown1.Items.Add("100");
            this.domainUpDown1.Items.Add("90");
            this.domainUpDown1.Items.Add("80");
            this.domainUpDown1.Items.Add("70");
            this.domainUpDown1.Items.Add("60");
            this.domainUpDown1.Items.Add("50");
            this.domainUpDown1.Items.Add("40");
            this.domainUpDown1.Items.Add("30");
            this.domainUpDown1.Items.Add("20");
            this.domainUpDown1.Items.Add("10");
            this.domainUpDown1.Location = new System.Drawing.Point(38, 67);
            this.domainUpDown1.Name = "domainUpDown1";
            this.domainUpDown1.Size = new System.Drawing.Size(97, 21);
            this.domainUpDown1.TabIndex = 28;
            this.domainUpDown1.Text = "5";
            this.domainUpDown1.TextChanged += new System.EventHandler(this.domainUpDown1_TextChanged);
            this.domainUpDown1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.domainUpDown1_KeyPress);
            // 
            // checkBox1
            // 
            this.checkBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.checkBox1.AutoSize = true;
            this.checkBox1.Enabled = false;
            this.checkBox1.ForeColor = System.Drawing.Color.Purple;
            this.checkBox1.Location = new System.Drawing.Point(364, 47);
            this.checkBox1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(60, 16);
            this.checkBox1.TabIndex = 30;
            this.checkBox1.Text = "음소거";
            this.checkBox1.UseVisualStyleBackColor = true;
            this.checkBox1.Click += new System.EventHandler(this.checkBox1_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(3, 201);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(29, 12);
            this.label3.TabIndex = 27;
            this.label3.Text = "채널";
            // 
            // cbMicChannel
            // 
            this.cbMicChannel.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbMicChannel.Enabled = false;
            this.cbMicChannel.FormattingEnabled = true;
            this.cbMicChannel.Items.AddRange(new object[] {
            "1 (모노)",
            "2 (스테레오)"});
            this.cbMicChannel.Location = new System.Drawing.Point(36, 197);
            this.cbMicChannel.Name = "cbMicChannel";
            this.cbMicChannel.Size = new System.Drawing.Size(98, 20);
            this.cbMicChannel.TabIndex = 26;
            this.cbMicChannel.SelectedIndexChanged += new System.EventHandler(this.cbMicChannel_SelectedIndexChanged);
            // 
            // lblMicdBL
            // 
            this.lblMicdBL.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblMicdBL.AutoSize = true;
            this.lblMicdBL.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblMicdBL.ForeColor = System.Drawing.Color.Blue;
            this.lblMicdBL.Location = new System.Drawing.Point(443, 109);
            this.lblMicdBL.Name = "lblMicdBL";
            this.lblMicdBL.Size = new System.Drawing.Size(59, 13);
            this.lblMicdBL.TabIndex = 24;
            this.lblMicdBL.Text = "-320.00 dB";
            // 
            // chkNomalize
            // 
            this.chkNomalize.AutoSize = true;
            this.chkNomalize.Checked = true;
            this.chkNomalize.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkNomalize.Location = new System.Drawing.Point(6, 151);
            this.chkNomalize.Name = "chkNomalize";
            this.chkNomalize.Size = new System.Drawing.Size(84, 16);
            this.chkNomalize.TabIndex = 23;
            this.chkNomalize.Text = "노멀라이징";
            this.toolTip1.SetToolTip(this.chkNomalize, "음악과 마이크를 적절히 믹싱해주는 기능입니다.");
            this.chkNomalize.UseVisualStyleBackColor = true;
            this.chkNomalize.CheckedChanged += new System.EventHandler(this.chkNomalize_CheckedChanged);
            // 
            // comboBox2
            // 
            this.comboBox2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox2.FormattingEnabled = true;
            this.comboBox2.Items.AddRange(new object[] {
            "Main Output",
            "Channel"});
            this.comboBox2.Location = new System.Drawing.Point(36, 173);
            this.comboBox2.Name = "comboBox2";
            this.comboBox2.Size = new System.Drawing.Size(112, 20);
            this.comboBox2.TabIndex = 22;
            this.comboBox2.SelectedIndexChanged += new System.EventHandler(this.comboBox2_SelectedIndexChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(3, 177);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(29, 12);
            this.label2.TabIndex = 21;
            this.label2.Text = "할당";
            // 
            // chkMicBypass
            // 
            this.chkMicBypass.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.chkMicBypass.AutoSize = true;
            this.chkMicBypass.ForeColor = System.Drawing.Color.Blue;
            this.chkMicBypass.Location = new System.Drawing.Point(458, 0);
            this.chkMicBypass.Name = "chkMicBypass";
            this.chkMicBypass.Size = new System.Drawing.Size(48, 16);
            this.chkMicBypass.TabIndex = 20;
            this.chkMicBypass.Text = "우회";
            this.chkMicBypass.UseVisualStyleBackColor = true;
            this.chkMicBypass.CheckedChanged += new System.EventHandler(this.checkBox3_CheckedChanged);
            // 
            // progressBarGradient1
            // 
            this.progressBarGradient1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.progressBarGradient1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.progressBarGradient1.IntervaldB = 5;
            this.progressBarGradient1.Location = new System.Drawing.Point(24, 108);
            this.progressBarGradient1.MaximumColor = System.Drawing.Color.Crimson;
            this.progressBarGradient1.MaximumValue = 30000;
            this.progressBarGradient1.MidColor = System.Drawing.Color.Goldenrod;
            this.progressBarGradient1.MidColorPosition = 0.7F;
            this.progressBarGradient1.MindB = -60;
            this.progressBarGradient1.MinimumColor = System.Drawing.Color.Lime;
            this.progressBarGradient1.MinimumValue = 0;
            this.progressBarGradient1.MouseInteractive = false;
            this.progressBarGradient1.Name = "progressBarGradient1";
            this.progressBarGradient1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            this.progressBarGradient1.ShowMidColor = true;
            this.progressBarGradient1.ShowMinus = true;
            this.progressBarGradient1.ShowPlus = false;
            this.progressBarGradient1.Size = new System.Drawing.Size(414, 17);
            this.progressBarGradient1.TabIndex = 18;
            this.progressBarGradient1.Value = 10;
            this.progressBarGradient1.VisibledB = true;
            // 
            // label12
            // 
            this.label12.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Gulim", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label12.ForeColor = System.Drawing.Color.Purple;
            this.label12.Location = new System.Drawing.Point(468, 78);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(28, 12);
            this.label12.TabIndex = 16;
            this.label12.Text = "N/A";
            // 
            // trackBar3
            // 
            this.trackBar3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.trackBar3.AutoSize = false;
            this.trackBar3.Enabled = false;
            this.trackBar3.Location = new System.Drawing.Point(6, 72);
            this.trackBar3.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.trackBar3.Maximum = 10000;
            this.trackBar3.Name = "trackBar3";
            this.trackBar3.Size = new System.Drawing.Size(456, 31);
            this.trackBar3.SmallChange = 2;
            this.trackBar3.TabIndex = 15;
            this.trackBar3.TickFrequency = 100;
            this.trackBar3.Scroll += new System.EventHandler(this.trackBar3_Scroll);
            this.trackBar3.ValueChanged += new System.EventHandler(this.trackBar3_ValueChanged);
            // 
            // chkAutoDeviceDetect
            // 
            this.chkAutoDeviceDetect.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.chkAutoDeviceDetect.AutoSize = true;
            this.chkAutoDeviceDetect.Enabled = false;
            this.chkAutoDeviceDetect.ForeColor = System.Drawing.Color.Black;
            this.chkAutoDeviceDetect.Location = new System.Drawing.Point(433, 47);
            this.chkAutoDeviceDetect.Name = "chkAutoDeviceDetect";
            this.chkAutoDeviceDetect.Size = new System.Drawing.Size(76, 16);
            this.chkAutoDeviceDetect.TabIndex = 14;
            this.chkAutoDeviceDetect.Text = "자동 감지";
            this.chkAutoDeviceDetect.UseVisualStyleBackColor = true;
            this.chkAutoDeviceDetect.CheckedChanged += new System.EventHandler(this.chkAutoDeviceDetect_CheckedChanged);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Gulim", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label13.ForeColor = System.Drawing.Color.Purple;
            this.label13.Location = new System.Drawing.Point(9, 51);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(176, 12);
            this.label13.TabIndex = 13;
            this.label13.Text = "선택한 마이크 볼륨 조절하기";
            // 
            // btnSoundDeviceRefresh
            // 
            this.btnSoundDeviceRefresh.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSoundDeviceRefresh.ForeColor = System.Drawing.Color.Black;
            this.btnSoundDeviceRefresh.Location = new System.Drawing.Point(434, 21);
            this.btnSoundDeviceRefresh.Name = "btnSoundDeviceRefresh";
            this.btnSoundDeviceRefresh.Size = new System.Drawing.Size(73, 20);
            this.btnSoundDeviceRefresh.TabIndex = 12;
            this.btnSoundDeviceRefresh.Text = "새로고침";
            this.btnSoundDeviceRefresh.UseVisualStyleBackColor = true;
            this.btnSoundDeviceRefresh.Click += new System.EventHandler(this.btnSoundDeviceRefresh_Click);
            // 
            // cbInputDevice
            // 
            this.cbInputDevice.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cbInputDevice.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbInputDevice.FormattingEnabled = true;
            this.cbInputDevice.Location = new System.Drawing.Point(83, 20);
            this.cbInputDevice.Name = "cbInputDevice";
            this.cbInputDevice.Size = new System.Drawing.Size(345, 20);
            this.cbInputDevice.TabIndex = 2;
            this.cbInputDevice.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(8, 24);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(69, 12);
            this.label1.TabIndex = 1;
            this.label1.Text = "마이크 선택";
            // 
            // statusStrip1
            // 
            this.statusStrip1.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1,
            this.toolStripStatusLabel2,
            this.toolStripStatusLabel3});
            this.statusStrip1.Location = new System.Drawing.Point(0, 281);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(513, 22);
            this.statusStrip1.TabIndex = 2;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Font = new System.Drawing.Font("Gulim", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(36, 17);
            this.toolStripStatusLabel1.Text = "상태:";
            // 
            // toolStripStatusLabel2
            // 
            this.toolStripStatusLabel2.Font = new System.Drawing.Font("Gulim", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.toolStripStatusLabel2.ForeColor = System.Drawing.Color.Blue;
            this.toolStripStatusLabel2.Name = "toolStripStatusLabel2";
            this.toolStripStatusLabel2.Size = new System.Drawing.Size(24, 17);
            this.toolStripStatusLabel2.Text = "OK";
            // 
            // toolStripStatusLabel3
            // 
            this.toolStripStatusLabel3.Font = new System.Drawing.Font("Gulim", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.toolStripStatusLabel3.Name = "toolStripStatusLabel3";
            this.toolStripStatusLabel3.Size = new System.Drawing.Size(48, 17);
            this.toolStripStatusLabel3.Text = "버퍼: 0";
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.파일FToolStripMenuItem,
            this.보기VToolStripMenuItem,
            this.설정TToolStripMenuItem,
            this.도움말HToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional;
            this.menuStrip1.Size = new System.Drawing.Size(513, 24);
            this.menuStrip1.TabIndex = 3;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // 파일FToolStripMenuItem
            // 
            this.파일FToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.닫기XToolStripMenuItem});
            this.파일FToolStripMenuItem.Name = "파일FToolStripMenuItem";
            this.파일FToolStripMenuItem.Size = new System.Drawing.Size(57, 20);
            this.파일FToolStripMenuItem.Text = "파일(&F)";
            // 
            // 닫기XToolStripMenuItem
            // 
            this.닫기XToolStripMenuItem.Name = "닫기XToolStripMenuItem";
            this.닫기XToolStripMenuItem.Size = new System.Drawing.Size(113, 22);
            this.닫기XToolStripMenuItem.Text = "닫기(&X)";
            // 
            // 보기VToolStripMenuItem
            // 
            this.보기VToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.dB레이블보이기ToolStripMenuItem});
            this.보기VToolStripMenuItem.Name = "보기VToolStripMenuItem";
            this.보기VToolStripMenuItem.Size = new System.Drawing.Size(58, 20);
            this.보기VToolStripMenuItem.Text = "보기(&V)";
            // 
            // dB레이블보이기ToolStripMenuItem
            // 
            this.dB레이블보이기ToolStripMenuItem.Checked = true;
            this.dB레이블보이기ToolStripMenuItem.CheckOnClick = true;
            this.dB레이블보이기ToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.dB레이블보이기ToolStripMenuItem.Name = "dB레이블보이기ToolStripMenuItem";
            this.dB레이블보이기ToolStripMenuItem.Size = new System.Drawing.Size(188, 22);
            this.dB레이블보이기ToolStripMenuItem.Text = "데시벨 레이블 보이기";
            this.dB레이블보이기ToolStripMenuItem.CheckedChanged += new System.EventHandler(this.dB레이블보이기ToolStripMenuItem_CheckedChanged);
            // 
            // 설정TToolStripMenuItem
            // 
            this.설정TToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.전개발자입니다ToolStripMenuItem});
            this.설정TToolStripMenuItem.Name = "설정TToolStripMenuItem";
            this.설정TToolStripMenuItem.Size = new System.Drawing.Size(58, 20);
            this.설정TToolStripMenuItem.Text = "설정(&T)";
            // 
            // 전개발자입니다ToolStripMenuItem
            // 
            this.전개발자입니다ToolStripMenuItem.CheckOnClick = true;
            this.전개발자입니다ToolStripMenuItem.Name = "전개발자입니다ToolStripMenuItem";
            this.전개발자입니다ToolStripMenuItem.Size = new System.Drawing.Size(164, 22);
            this.전개발자입니다ToolStripMenuItem.Text = "전 개발자 입니다";
            this.전개발자입니다ToolStripMenuItem.CheckedChanged += new System.EventHandler(this.전개발자입니다ToolStripMenuItem_CheckedChanged);
            // 
            // 도움말HToolStripMenuItem
            // 
            this.도움말HToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.에코음정설정법ToolStripMenuItem,
            this.음량조절음소거ToolStripMenuItem});
            this.도움말HToolStripMenuItem.Name = "도움말HToolStripMenuItem";
            this.도움말HToolStripMenuItem.Size = new System.Drawing.Size(72, 20);
            this.도움말HToolStripMenuItem.Text = "도움말(&H)";
            // 
            // 에코음정설정법ToolStripMenuItem
            // 
            this.에코음정설정법ToolStripMenuItem.Name = "에코음정설정법ToolStripMenuItem";
            this.에코음정설정법ToolStripMenuItem.Size = new System.Drawing.Size(167, 22);
            this.에코음정설정법ToolStripMenuItem.Text = "에코, 음정 설정법";
            this.에코음정설정법ToolStripMenuItem.Click += new System.EventHandler(this.에코음정설정법ToolStripMenuItem_Click);
            // 
            // 음량조절음소거ToolStripMenuItem
            // 
            this.음량조절음소거ToolStripMenuItem.Name = "음량조절음소거ToolStripMenuItem";
            this.음량조절음소거ToolStripMenuItem.Size = new System.Drawing.Size(167, 22);
            this.음량조절음소거ToolStripMenuItem.Text = "음량 조절, 음소거";
            this.음량조절음소거ToolStripMenuItem.Click += new System.EventHandler(this.음량조절음소거ToolStripMenuItem_Click);
            // 
            // timer1
            // 
            this.timer1.Interval = 25;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // frmNorebang
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(513, 303);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.groupBox1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "frmNorebang";
            this.Text = "마이크 (노래방)";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmNorebang_FormClosing);
            this.Load += new System.EventHandler(this.frmNorebang_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar3)).EndInit();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.CheckBox chkMic;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ComboBox cbInputDevice;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel2;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem 파일FToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 닫기XToolStripMenuItem;
        private System.Windows.Forms.CheckBox chkAutoDeviceDetect;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Button btnSoundDeviceRefresh;
        private System.Windows.Forms.Label label12;
        private Control.ProgressBarGradientInternal progressBarGradient1;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.ToolStripMenuItem 보기VToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem dB레이블보이기ToolStripMenuItem;
        private System.Windows.Forms.CheckBox chkMicBypass;
        private System.Windows.Forms.ComboBox comboBox2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.CheckBox chkNomalize;
        private System.Windows.Forms.Label lblMicdBL;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox cbMicChannel;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.DomainUpDown domainUpDown1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.CheckBox checkBox3;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel3;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label lblMicdBR;
        private Control.ProgressBarGradientInternal progressBarGradient2;
        private System.Windows.Forms.ToolStripMenuItem 설정TToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 전개발자입니다ToolStripMenuItem;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.NumericUpDown numericUpDown1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.TrackBar trackBar3;
        private System.Windows.Forms.ToolStripMenuItem 도움말HToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 에코음정설정법ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 음량조절음소거ToolStripMenuItem;
        private System.Windows.Forms.ToolTip toolTip1;
    }
}