﻿
/********************************************
클래스를 쉽게 쓸 수 있게 만든 헬퍼 입니다
간단한 재생은 
HS_Audio_Helper.HSAudioHelper fh = new HS_Audio_Helper.HSAudioHelper(음악 경로);
fh.Play();
라고 하시면 됩니다.

HSAudioHelper Made by 박홍식
Copyright ⓒ 박홍식 All RIght Reserved
**********************************************/
using System;
using HS_Audio.DSP;
using System.Threading;
using System.ComponentModel;
using HS_Audio_Lib;
using System.Collections.Generic;
using HS_Audio.Bundles;

namespace HS_Audio
{

    /// <summary>
    /// 사운드가 생성된 방법 입니다.
    /// </summary>
    public enum CreateSoundInfo{None,File,Buffer, Buffer_Loop}

    public partial class HSAudioHelper : IDisposable
    {
        /// <summary>
        /// 재생상태 입니다.
        /// </summary>
        public enum PlayingStatus {None,Play,Pause,Stop,Ready,Error }
        
        /// <summary>
        /// 사운드가 할당된 개체 입니다.
        /// </summary>
        public enum CreateSoundInstance {System,Channel,Sound }

        /// <summary>
        /// 내부의 상태가 변경되었을때 발생하는 이벤트 입니다.
        /// </summary>
        public event HSAudio_RESULT_ChangedEventHandler HSAudio_RESULT_Changed;

        /// <summary>
        /// 재생상태가 변경될때 이벤트를 처리하는 대리자 입니다.
        /// </summary>
        /// <param name="Status">변경된 재생상태 입니다.</param>
        /// <param name="index">현재 이 헬퍼의 인덱스 입니다.</param>
        public delegate void PlayingStatusChangedEventHandler(PlayingStatus Status, int index);
        /// <summary>
        /// 음악경로를 바꿀때나 초기화할때 먼저 호출되는 이벤트를 처리하는 대리자 입니다..
        /// </summary>
        /// <param name="system">FMOD.System 입니다.</param>
        public delegate void InitializingEventHandler(HS_Audio_Lib.System system);

        /// <summary>
        /// 재생 상태가 변경될때 발생하는 이벤트 입니다.
        /// </summary>
        public event PlayingStatusChangedEventHandler PlayingStatusChanged;
        /// <summary>
        /// 음악경로를 바꿀때나 초기화할때 먼저 호출됩니다.
        /// </summary>
        public event InitializingEventHandler Initializing;

        [Obsolete]
        public delegate void PositionChangedEventHandler(HSAudioHelper Helper, uint Tick, bool Stop);
        [Obsolete]
        public event PositionChangedEventHandler PosionStatusChanged;

        public delegate void MusicChangeEventHandler(HSAudioHelper Helper, int index, bool Error);
        /// <summary>
        /// 음악이 바뀌기 전 발생합니다.
        /// </summary>
        public event MusicChangeEventHandler BeginMusicChanged;
        /// <summary>
        /// 음악이 바뀌는 도중 (이미 음악은 바꼈음, DSP 같은건 재등록 안 된 상태) 발생합니다.
        /// </summary>
        public event MusicChangeEventHandler MusicChanging;
        /// <summary>
        /// 음악이 바뀐후에 발생합니다.
        /// </summary>
        public event MusicChangeEventHandler MusicChanged;

        public delegate void DisposingEventHandler(object sender, bool All);
        /// <summary>
        /// 내부 리소스를 정리하기전에 발생하는 이벤트 입니다.
        /// </summary>
        public event DisposingEventHandler Disposing;

        public delegate void DisposedEventHandler(object sender, bool All);
        /// <summary>
        /// 내부 리소스를 정리한후에 발생하는 이벤트 입니다.
        /// </summary>
        public event DisposedEventHandler Disposed;

        public delegate void SystemCreatedEvendHandler(object sender, SoundDeviceFormat format);
        /// <summary>
        /// 처음 시스템을 만들때 발생합니다.
        /// </summary>
        public event SystemCreatedEvendHandler SystemCreated;

        System.Windows.Forms.Timer timer1 = new System.Windows.Forms.Timer() { Interval = 10 };
        System.Windows.Forms.Timer timer2 = new System.Windows.Forms.Timer() { Interval = 100 };
        /// <summary>
        /// 현재 FMOD버전을 설정하거나 가져옵1니다.
        /// </summary>
        public const uint CurrentFMODVersion = HS_Audio_Lib.VERSION.number;
        /// <summary>
        /// FMODEx.dll 의 내부버전을 가져옵니다.
        /// </summary>
        public uint AudioVersion { get; private set; }

        public void System_Created(SoundDeviceFormat Format = null)
        {
            /*
                Create a System object and initialize.
            */

            HelperEx.Result = HS_Audio_Lib.Factory.System_Create(ref system);
            ERRCHECK(HelperEx.Result);
            uint version = 0;
            HelperEx.Result = system.getVersion(ref version);
            AudioVersion = version;
            ERRCHECK(HelperEx.Result);
            //RESULT res = setOutputType(OUTPUTTYPE.AUTODETECT);
            
            //try {if(SystemCreated!=null) SystemCreated(this, format);}catch{}

            DefaultPlugins = getDefaultPlugins();
            Plugins = new PluginDSP[0];
            HS_Audio_Lib.SYSTEM_CALLBACK sc = new SYSTEM_CALLBACK(System_Callback);
            Result = system.setCallback(sc);

            //system.setOutput(OUTPUTTYPE.);
            if (Format != null && !Format.IsDefault)
            {
                Result = system.close();
                Result = system.setSoftwareFormat(Format.Samplerate, Format.SoundFormat, Format.OutputChannel, Format.MaxInputChannel, Format.ResamplerMethod);
                Result = system.setOutput(Format.OutputType);
            }

            SoundDevice[] de = getSoundDevices();
            _OutputDevice = DefaultSoundDevice;

            //else Result = system.setSoftwareFormat(48000, SOUND_FORMAT.PCMFLOAT, 2, 6, DSP_RESAMPLER.SPLINE);
            Result = system.init(32, INITFLAGS.NORMAL, IntPtr.Zero);
            _DeviceFormat = getDeviceFormat();
            LoadPluginCodec();
        }

        private RESULT System_Callback(IntPtr systemraw, SYSTEM_CALLBACKTYPE type, IntPtr commanddata1, IntPtr commanddata2)
        {
            //System.Windows.Forms.MessageBox.Show(type.ToString());
            if(type == SYSTEM_CALLBACKTYPE.DEVICELISTCHANGED||
               type == SYSTEM_CALLBACKTYPE.DEVICELOST)
                try{if(SoundDeviceChanged!=null)SoundDeviceChanged(getSoundDevices());}catch{}

            return Result;
        }

        /// <summary>
        /// 내부 클래스를 초기화합니다.
        /// </summary>
        /// <param name="MusicPath">음악 경로입니다.</param>
        /// <param name="Play3DSound">FMODHelper.Play3DSound [FMOD.System(Get/Set)3DSoundAttribute] 입니다.</param>
        [Obsolete("Play3DSound는 FMODHelper.Play3DSound 가 더이상 사용되지 않기때문입니다. 자세한 항목은 FMOD.System.(Get/Set)3DSoundAttribute 를 참고해주시기 바랍니다.")]
        public HSAudioHelper(string MusicPath, bool Play3DSound, bool AsyncMode = false)
        {
            this.AsyncMode = AsyncMode;
            LastVolume = LastMix = 1;
            HelperEx = new HSAudioHelperEx();
            timer2.Tick += new EventHandler(timer2_Tick);
            HelperEx.Result = HS_Audio_Lib.RESULT.OK;
            this.Play3D = Play3DSound;
            //FMOD.RESULT result;
            System_Created();
            //if (version < CurrentFMODVersion)
            //{
            //    throw new Exception("Error!  You are using an old version of FMOD " + version.ToString("X") + ".  This program requires " + FMOD.VERSION.number.ToString("X") + ".");
            //}

            //system.setSoftwareFormat(44100, FMOD.SOUND_FORMAT.PCM16, 2, 0, FMOD.DSP_RESAMPLER.CUBIC);
            this.MusicPath = MusicPath;
            HelperEx = new HSAudioHelperEx(this);
            ts = new System.Threading.ThreadStart(th_Tick);
            th = new System.Threading.Thread(ts);
            //th.Start();
            //timer2.Start();
            //timer1.Start();
        }
        /// <summary>
        /// 내부 클래스를 초기화합니다.
        /// </summary>
        /// <param name="MusicPath">음악 경로입니다.</param>
        public HSAudioHelper(string MusicPath, bool AsyncMode = false)
        {
            LastVolume = LastMix = 1;
            timer2.Tick += new EventHandler(timer2_Tick);
            HelperEx = new HSAudioHelperEx();
            HelperEx.Result = HS_Audio_Lib.RESULT.OK;

            System_Created();
            //if (version < CurrentFMODVersion)
            //{
            //    throw new Exception("Error!  You are using an old version of FMOD " + version.ToString("X") + ".  This program requires " + FMOD.VERSION.number.ToString("X") + ".");
            //}

            //system.setSoftwareFormat(44100, FMOD.SOUND_FORMAT.PCM16, 2, 0, FMOD.DSP_RESAMPLER.CUBIC);
            this.MusicPath = MusicPath;
            ts = new System.Threading.ThreadStart(th_Tick);
            th = new System.Threading.Thread(ts);
            timer2.Start();
            //timer1.Start();
        }
        /// <summary>
        /// 내부 클래스를 초기화합니다.
        /// </summary>
        /// <param name="MusicPath">음악 경로입니다.</param>
        public HSAudioHelper(string MusicPath, SoundDeviceFormat Format, bool AsyncMode = false)
        {
            this.AsyncMode = AsyncMode;
            LastVolume = LastMix = 1;
            timer2.Tick += new EventHandler(timer2_Tick);
            HelperEx = new HSAudioHelperEx();
            HelperEx.Result = HS_Audio_Lib.RESULT.OK;
            //FMOD.RESULT result;
            System_Created(Format);
            //if (version < CurrentFMODVersion)
            //{
            //    throw new Exception("Error!  You are using an old version of FMOD " + version.ToString("X") + ".  This program requires " + FMOD.VERSION.number.ToString("X") + ".");
            //}

            //system.setSoftwareFormat(44100, FMOD.SOUND_FORMAT.PCM16, 2, 0, FMOD.DSP_RESAMPLER.CUBIC);
            this.MusicPath = MusicPath;
            ts = new System.Threading.ThreadStart(th_Tick);
            th = new System.Threading.Thread(ts);
            timer2.Start();
            //timer1.Start();
        }
        /// <summary>
        /// 내부 클래스를 초기화합니다.
        /// </summary>
        public HSAudioHelper(SoundDeviceFormat Format, bool AsyncMode = false)
        {
            this.AsyncMode = AsyncMode;
            LastVolume = LastMix = 1;
            HelperEx = new HSAudioHelperEx();
            HelperEx.Result = HS_Audio_Lib.RESULT.OK;
            //FMOD.RESULT result;

            System_Created(Format);

            if (HelperEx.Result == HS_Audio_Lib.RESULT.OK)
                PlayStatus = PlayingStatus.None; //try { HelperEx = new FMODHelperEx(this); }catch { } 
            //try { dsp = new FMODDSPHelper(this); }catch { }
            ts = new System.Threading.ThreadStart(th_Tick);
            th = new System.Threading.Thread(ts);
            //timer1.Start();
        }
        /// <summary>
        /// 내부 클래스를 초기화합니다.
        /// </summary>
        public HSAudioHelper(bool AsyncMode = false)
        {
            this.AsyncMode = AsyncMode;
            LastVolume = LastMix = 1;
            HelperEx = new HSAudioHelperEx();
            HelperEx.Result = HS_Audio_Lib.RESULT.OK;
            //FMOD.RESULT result;

            System_Created();

            if (HelperEx.Result == HS_Audio_Lib.RESULT.OK) 
                PlayStatus = PlayingStatus.None; //try { HelperEx = new FMODHelperEx(this); }catch { } 
            //try { dsp = new FMODDSPHelper(this); }catch { }
            ts = new System.Threading.ThreadStart(th_Tick);
            th = new System.Threading.Thread(ts);
            //timer1.Start();
        }
        void NewInstance(string MusicPath)
        {
            LastVolume = LastMix = 1;
            timer2.Tick += new EventHandler(timer2_Tick);
            HelperEx = new HSAudioHelperEx();
            HelperEx.Result = HS_Audio_Lib.RESULT.OK;

            System_Created();

            sound = null; channel = null;
            this.MusicPath = MusicPath;
            timer2.Start();
            if (th != null && th.IsAlive) th.Abort();
            ts = new System.Threading.ThreadStart(th_Tick);
            th = new System.Threading.Thread(ts);
            //timer1.Start();
        }

        /// <summary>
        /// 내부 리소스를 제거합니다 (사운드만 정리합니다.) (이 클래스를 이제 사용안할때 이왕이면 이 함수를 호출해 주십시오.)
        /// </summary>
        public void Dispose() { Dispose(false); }
        /// <summary>
        /// 내부 리소스를 제거합니다. (이 클래스를 이제 사용안할때 이왕이면 이 함수를 호출해 주십시오.)
        /// </summary>
        /// <param name="All">True면 시스템을 포함한 전체를 False면 사운드만 정리합니다.</param>
        public void Dispose(bool All)
        {
            //bool disposing = true;
            try { if(Disposing!=null) this.Disposing(this, All); }catch { }
            timer1.Stop();
            timer2.Stop();
            if (th != null && th.IsAlive) th.Abort();
            if (sound != null)
            {
                HS_Audio_Lib.RESULT result = sound.release();
                try { ERRCHECK(result); }catch { }
                sound = null;
            }
            if (All)
            {
                if (system != null)
                {
                    HS_Audio_Lib.RESULT result1 = system.close();
                    try { ERRCHECK(result1); }catch { }
                    system = null;

                    //result = system.release();
                    //ERRCHECK(result);
                }
            }
            try { if(Disposed!=null) this.Disposed(this, All); }catch { }
        }

        public HS_Audio_Lib.MODE GetMode(bool IsSound = true)
        {
            HS_Audio_Lib.MODE Mode = HS_Audio_Lib.MODE.DEFAULT;
            if (!IsSound) Result = channel.getMode(ref Mode);
            else Result = sound.getMode(ref Mode);
            return Mode;
        }
        public HS_Audio_Lib.MODE GetMode(ref HS_Audio_Lib.RESULT Result,bool IsSound = true)
        {
            HS_Audio_Lib.MODE Mode = HS_Audio_Lib.MODE.DEFAULT;
            if (!IsSound) Result=channel.getMode(ref Mode);
            else Result = sound.getMode(ref Mode);
            return Mode;
        }

        public HS_Audio_Lib.RESULT SetMode(HS_Audio_Lib.MODE Mode,bool IsSound=true)
        {
            if (!IsSound) return channel.setMode(Mode);
            else return sound.setMode(Mode);
        }

        //private FMOD.DSP_DESCRIPTION dspdesc = new FMOD.DSP_DESCRIPTION();
        //private FMOD.DSP_READCALLBACK dspreadcallback = new FMOD.DSP_READCALLBACK(READCALLBACK);
        
        //private static FMOD.DSP thisdsp = new FMOD.DSP();

        

        /// <summary>
        /// 이 개체의 인덱스를 가져오거나 설정합니다.
        /// </summary>
        public int index;

        bool Stopped = false;
        private void timer2_Tick(object sender, System.EventArgs e)
        {
            
        }
        System.Threading.Mutex mtx;
        private void th_Tick()
        {
            //bool HelperThread;
            //mtx = new System.Threading.Mutex(true, "HelperThread", out HelperThread);
            //MessageBox.Show(HelperThread.ToString());
            while (true/*HelperThread*/)
            {
                System.Threading.Thread.Sleep(10);
                try
                {
                    if (TickErrorPatch)
                    {
                        if (TotalPosition - 20 < CurrentPosition)
                        {
                            if (!Loop) { Stopped = CustomComplete = true; Stop(); th.Abort(); break; }
                            else { Stopped = CustomComplete = false; Play(); }
                        }
                        else { if (CurrentPosition == 0) { System.Threading.Thread.Sleep(750); } Stopped = false; }
                    Here:
                        if (TickErrorPatch)
                        {
                            if (!Loop)
                            {
                                if (!Stopped && CurrentPosition < 10)
                                {
                                    Stop(); Stopped = CustomComplete = true; th.Abort();
                                }
                            }
                        }
                    }
                    else
                    {
                        if (TotalPosition - 20 < CurrentPosition)
                        {
                            if (!Loop) { Stopped = CustomComplete = true; /*Stop();*/ th.Abort(); break; }
                            else { Stopped = CustomComplete = false; /*Play();*/ }
                        }
                        else { if (CurrentPosition == 0) { System.Threading.Thread.Sleep(750); } Stopped = false; }
                    }
                    //if (PlayStatus == PlayingStatus.Play && posion < 10) { Stop(); }
                    //try { system.update(); }catch(Exception ex){CSharpUtility.Logging.LoggingT(ex);}
                }
                catch (ThreadAbortException) { }
                catch (Exception ex) { HS_CSharpUtility.LoggingUtility.Logging(ex); }//MessageBox.Show(ex.ToString(),"ThreadError!!"); }
                //if (PlayStatus == PlayingStatus.Play && posion < 10) { Stop(); }
                //system.update();
            }
            //System.Threading.Thread.Sleep(timer1.Interval);
        }
 

        #region 프로퍼티
        bool First;
        public HS_Audio_Lib.System system = null;
        public HS_Audio_Lib.Sound sound = null;
        public HS_Audio_Lib.Channel channel = null;
        public HSAudioDSPHelper dsp = null;
        public HS_Audio_Lib.Reverb reverb = null;
        System.Threading.Thread th;
        System.Threading.ThreadStart ts;

        HS_Audio.Properties.HSAudioChannelStruct cs = new Properties.HSAudioChannelStruct();

        HS_Audio_Lib.RESULT _Result;
        /// <summary>
        /// 내부 상태를 가져옵니다.
        /// </summary>
        public HS_Audio_Lib.RESULT Result
        {
            get { return _Result; }
            internal set { _Result = value; if(HSAudio_RESULT_Changed!=null) HSAudio_RESULT_Changed(value, HS_Audio_Lib.Error.String(value));}
        }

        bool _Complete;
        bool _Complete1;
        /// <summary>
        /// 재생을 완료했는지의 여부를 가져옵니다.
        /// </summary>
        public bool Complete { get { try { channel.isPlaying(ref _Complete);}catch{return true;} return !_Complete; } }

        [Obsolete]
        public bool CustomComplete { get ;internal set;  }

        /// <summary>
        /// 헬퍼의 확장클래스 입니다. 음장설정과 같은 고급 효과를 설정할수 있습니다.
        /// </summary>
        public HS_Audio.HSAudioHelperEx HelperEx
        {
            get; 
            set;
        }

        
        uint _GetCurrentTick;
        /// <summary>
        /// 현재 재생시간을 가져옵니다   
        /// </summary>
        /// <param name="TimeUnit">시간을 측정할 값 입니다. (MS는 MilliSecond 입니다.)</param>
        /// <returns></returns>
        public uint GetCurrentTick(HS_Audio_Lib.TIMEUNIT TimeUnit)
        {
            try
            {
                if (channel!=null) Result = channel.getPosition(ref _GetCurrentTick, TimeUnit);
                else return 0;
                return _GetCurrentTick;
            }
            catch { return 0; }
        }

        uint _GetTotalTick;
        /// <summary>
        /// 총 재생시간을 가져옵니다   
        /// </summary>
        /// <param name="TimeUnit">시간을 측정할 값 입니다. (MS는 MilliSecond 입니다.)</param>
        /// <returns></returns>
        public uint GetTotalTick(HS_Audio_Lib.TIMEUNIT TimeUnit)
        {
            try
            {
                if (sound == null) return 0;
                Result = sound.getLength(ref _GetTotalTick, TimeUnit);
                return _GetTotalTick;
            }
            catch { return 0; }
        }

        float _Frequency;
        /// <summary>
        /// 마지막으로 사운드의 주파수의 변경한 값을 가져오거나 설정합니다.
        /// </summary>
        public float LastFrequency=-1000000;
        /// <summary>
        /// 현재 사운드의 주파수를 가져오거나 설정합니다.
        /// </summary>
        [Obsolete("Pitch 속성을 사용해 주세요")]
        public float Frequency 
        {
            get { channel.getFrequency(ref _Frequency); return _Frequency; }
            set { LastFrequency = value; channel.setFrequency(value); }
        }
        /// <summary>
        /// 현재 사운드의 기본 주파수(피치)를 가져옵니다.
        /// </summary>
        public float DefaultFrequency {get;private set;}

        /// <summary>
        /// 마지막으로 피치의 변경한 값을 가져오거나 설정합니다.
        /// </summary>
        public float LastPitch=0;
        /// <summary>
        /// 현재 사운드의 피치를 가져오거나 설정합니다.
        /// </summary>
        public float Pitch
        {
            get { return Error?LastPitch:Frequency-DefaultFrequency; }
            set { LastPitch = value; channel.setFrequency(DefaultFrequency + value); }
        }

        public float LastMix{ get; private set; }
        /// <summary>
        /// 모든 DSP의(Reverb 포함) 믹서(Gain) 입니다.
        /// </summary>
        public float Mix
        {
            get { return dsp.Gain; }
            set { dsp.Gain = LastMix = value; }
        }

        #region 음량 조절
        public delegate void VolumeChangedEventHandler(HSAudioHelper Helper, float Volume);
        /// <summary>
        /// 음량 변경전에 발생하는 이벤트 입니다.
        /// </summary>
        public event VolumeChangedEventHandler VolumeChanged;
        public delegate void VolumeChangingEventHandler(HSAudioHelper Helper, float Volume);
        /// <summary>
        /// 음량 변경후에 발생하는 이벤트 입니다.
        /// </summary>
        public event VolumeChangedEventHandler VolumeChanging;

        /// <summary>
        /// 사용자가 마지막으로 설정한 음량을 가져옵니다. (1.0이 최대 0.0이 최소 입니다.)
        /// </summary>
        public float LastVolume { get; private set; }
        /// <summary>
        /// 현재 음량을 가져오거나 설정합니다. (1.0이 최대 0.0이 최소 입니다.)
        /// </summary>
        public float Volume
        {
            get { float a=1f; channel.getVolume(ref a); return a; }
            set
            {
                try{ if(VolumeChanging != null) VolumeChanging(this, LastVolume); }catch{}
                Result = channel.setVolume(value);
                LastVolume = value;
                try { if (VolumeChanged != null) VolumeChanged(this, value); }catch{}
                if(value>1.0f||value<0.0f){throw new SoundSystemException(HS_Audio_Lib.RESULT.ERR_INVALID_PARAM,"음량은 1.0 과 0.0사이에 있어야 합니다.");}
            }
        }
        #endregion

        /// <summary>
        /// 마지막으로 변경한 팬(밸런스) 을(를) 가져옵니다.
        /// </summary>
        public float LastPan{get;internal set;}
        float _Pan;
        /// <summary>
        /// 팬(밸런스) 을(를) 가져오거나 설정합니다. (-1이 최소 1이 최대 입니다.)
        /// </summary>
        public float Pan
        {
            get {channel.getPan(ref _Pan); return _Pan; }
            set{if(value<=1&&value>=-1){LastPan=value;if(channel!=null)Result=channel.setPan(value);}}
        }

        bool _TickErrorPatch;
        /// <summary>
        /// 재생 이동관련 오류 패치입니다. (재생 위치를 처음부분으로 이동할때는 반드시 False로 설정해주세요!)
        /// </summary>
        [Obsolete("사용하지 마십시오 (권장하지 않습니다.)")]
        public bool TickErrorPatch
        {
            get { return _TickErrorPatch; }
            set 
            {
                if (value) { timer1.Interval = 1; timer2.Start(); channel.setMode(HS_Audio_Lib.MODE.LOOP_NORMAL); }
                else { timer1.Interval = 10; if(channel!=null)channel.setMode(HS_Audio_Lib.MODE.LOOP_OFF); }
                _TickErrorPatch = value; 
            }
        }

        bool _ErrorHandling = true;
        /// <summary>
        /// 오류를 현재 이 클래스에서 핸들링 (메세지 박스로) 할지 결정합니다.
        /// </summary>
        public bool ErrorHandling { get { return _ErrorHandling; } set { _ErrorHandling = value; } }

        uint _CurrentPosition;
        System.Windows.Forms.DialogResult dr = System.Windows.Forms.DialogResult.None;
        /// <summary>
        /// 현재 재생위치를 가져오거나 설정합니다.
        /// </summary>
        public uint CurrentPosition
        {
            get
            {
                if (channel != null)
                    try { channel.getPosition(ref _CurrentPosition, HS_Audio_Lib.TIMEUNIT.MS); }
                    catch (Exception ex)
                    {
                        if (ErrorHandling)
                        {
                            if (dr == System.Windows.Forms.DialogResult.None)
                                dr = System.Windows.Forms.MessageBox.Show("내부에서 심각한 오류가 감지되었습니다");
                        }
                        else throw ex;
                    }
                else _CurrentPosition = 0; return _CurrentPosition;
            }
            set { channel.setPosition(value, HS_Audio_Lib.TIMEUNIT.MS); } }

        uint _TotalPosition;
        /// <summary>
        /// 총 재생시간을 가져옵니다.
        /// </summary>
        public uint TotalPosition { get { if (sound != null)sound.getLength(ref _TotalPosition, HS_Audio_Lib.TIMEUNIT.MS); else _TotalPosition = 0; return _TotalPosition; } }

        uint _SizeLength;
        /// <summary>
        /// 노래의 원본크기를 가져옵니다.
        /// </summary>
        public uint SizeLength { get { sound.getLength(ref _SizeLength, HS_Audio_Lib.TIMEUNIT.RAWBYTES); return _SizeLength; } }

        uint _WaveLength;
        /// <summary>
        /// 노래의 원본 웨이브크기를 가져옵니다.
        /// </summary>
        public uint WaveLength { get { sound.getLength(ref _WaveLength, HS_Audio_Lib.TIMEUNIT.PCMBYTES); return _WaveLength; } }

        /// <summary>
        /// 현재 재생상태를 업데이트할 시간(밀리초) 입니다. (※주의: 1000 이하로 설정하면 재생상태갱신이 정확해지지 않습니다.)
        /// </summary>
        public int PositionUpdateTime { get { return timer1.Interval; } set { timer1.Interval = value; } }

        /// <summary>
        /// 3D Attribute에 대한 bool값 입니다. (SRS의 3D사운드 같은게 아닙니다. X축Y축Z축 으로 움직이는 소리를 나타냅니다. (League of Legends 처럼))
        /// </summary>
        [Obsolete]
        public bool Play3D { get; set; }

        /// <summary>
        /// 비동기적으로 음악을 변경할지를 가져옵니다.
        /// </summary>
        public bool AsyncMode { get; set; }

        bool _Loop;
        /// <summary>
        /// 무한 반복할 여부를 가져오거나 설정합니다.
        /// </summary>
        public bool Loop
        {
            get {return _Loop;}
            set
            { /*if (value) { channel.setMode(FMOD.MODE.LOOP_NORMAL); } else { channel.setMode(FMOD.MODE.LOOP_OFF); }*/
                if (channel != null)
                {
                    _Loop = value;
                    Result = channel.setMode(value ? HS_Audio_Lib.MODE.LOOP_NORMAL : HS_Audio_Lib.MODE.LOOP_OFF);
                }
                /*
                HS_Audio_Lib.MODE mod = MODE.DEFAULT; channel.getMode(ref mod); 
                if(value)channel.setMode(mod|MODE.LOOP_NORMAL);
                else {mod &= ~MODE.LOOP_NORMAL; channel.setMode(mod);}*/
            }
        }

        int _LoopCount;
        /// <summary>
        /// 반복할 횟수를 가져오거나 설정합니다.
        /// </summary>
        public int LoopCount {
            get { try { channel.getLoopCount(ref _LoopCount); } catch { } return _LoopCount; } 
            set { channel.setLoopCount(value); }}

        PlayingStatus _PlayStatus;
        /// <summary>
        /// 현재 재생상태를 가져옵니다.
        /// </summary>
        public PlayingStatus PlayStatus
        {
            get { return _PlayStatus; }
            internal set { _PlayStatus = value; try { if(PlayingStatusChanged!=null) PlayingStatusChanged(value, index); } catch { } }
        }

        public string MD5Hash { get; private set; }

        public bool FrequencyAutoUpdate = true;

        bool AsyncComplete=true;
        string _MusicPath;
        /// <summary>
        /// 음악경로 입니다. (경로를 바꾸게되면 플레이중인 사운드는 멈추고 지정한 경로의 사운드로 초기화 합니다.)
        /// </summary>
        public string MusicPath
        {
            get { return _MusicPath; }
            set
            {
                _MusicPath = value;
                if (AsyncMode/*MusicPathWorker == null*/) { try { MusicPathWorker.Dispose(); }catch{} MusicPathWorker = new BackgroundWorker() { WorkerSupportsCancellation = true }; MusicPathWorker.DoWork += MusicPathWorker_DoWork; MusicPathWorker.RunWorkerCompleted += MusicPathWorker_RunWorkerCompleted; }
                //MusicPathWorker.CancelAsync();
                if (AsyncMode)
                    if (!MusicPathWorker.IsBusy) { AsyncComplete = false; MusicPathWorker.RunWorkerAsync(); }
                    else new Exception("이미 음악경로 변경작업이 비동기적으로 진행 중입니다.\n\n잠시후 다시 시도해 보십시오");
                else
                    if (MusicPathWorker !=null&& (MusicPathWorker.IsBusy || MusicPathWorker.CancellationPending)) throw new DuplicateWaitObjectException("이미 음악경로 변경작업이 비동기적으로 작업 중입니다.\n\n잠시후 다시 시도해 보십시오");
                    else MusicPathWorker_DoWork(value,null);
            }
        }
        BackgroundWorker MusicPathWorker;
        protected internal bool PreLoading = true;
        public bool Error{get;private set;}
        void MusicPathWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            string value=MusicPath;
            CustomComplete = false;
            try {
            /*try { th.Abort(); }catch { }
            if (ts == null) ts = new System.Threading.ThreadStart(th_Tick);
            th = new System.Threading.Thread(ts);*/
            try{if(BeginMusicChanged!=null) BeginMusicChanged(this, index, Error);}catch{}
            _MusicPath = value;
            Dispose(false);
            //if (system != null) { Result = system.release(); }
            //FMOD.Factory.System_Create(ref system);
            //try { if (system == null||Result != RESULT.OK){Dispose(true);Initializing(this.system);} }catch { }
            if (Play3D)
            {
                try
                {
                    HelperEx.Result = PreLoading?
                        system.createSound(value, HS_Audio_Lib.MODE._3D | HS_Audio_Lib.MODE.UNICODE, ref sound):
                        //HS_Audio_Lib.MODE.SOFTWARE | HS_Audio_Lib.MODE._3D | HS_Audio_Lib.MODE.LOOP_NORMAL | HS_Audio_Lib.MODE.CREATESTREAM,
                        system.createStream(value, HS_Audio_Lib.MODE._3D | HS_Audio_Lib.MODE.UNICODE, ref sound);
                    if (HelperEx.Result != HS_Audio_Lib.RESULT.OK) 
                    { 
                        Dispose(true);System_Created();
                        HelperEx.Result = PreLoading?
                            system.createSound(value, HS_Audio_Lib.MODE._3D | HS_Audio_Lib.MODE.UNICODE, ref sound) :
                            system.createStream(value, HS_Audio_Lib.MODE._3D | HS_Audio_Lib.MODE.UNICODE, ref sound);
                        if(HelperEx.Result != HS_Audio_Lib.RESULT.OK)throw new SoundSystemException(HelperEx.Result); 
                    }
                }
                catch (SoundSystemException ex)
                {
                    //system.release(); system.close();
                    ERRCHECK(ex.Result);
                }
            }
            else
            {
                try
                {
                    bool Check = true;
                    //createStream("", FMOD.MODE._2D )
                    //byte[] a = System.IO.File.ReadAllBytes(value);
                    Retry:
                    Result = PreLoading?
                        system.createSound(value, /*HS_Audio_Lib.MODE.SOFTWARE | */HS_Audio_Lib.MODE.UNICODE, ref sound) :
                        system.createStream(value, /*HS_Audio_Lib.MODE.SOFTWARE | */HS_Audio_Lib.MODE.UNICODE, ref sound);
                    if (Result != HS_Audio_Lib.RESULT.OK)
                        {
                            if (!PreLoading && Check) {
                                setSoundDevice(SoundDevice);
                                Check = false;
                                //System.Windows.Forms.MessageBox.Show(Result.ToString());
                                goto Retry;}
                            throw new SoundSystemException(Result); }
                }
                catch (SoundSystemException ex)
                {
                    Dispose(false);
                    //system.release(); system.close();
                    if (e != null) { e.Result = value; }
                    AsyncComplete = true;
                    Error = true;
                    PlayStatus = PlayingStatus.Error;
                    //ERRCHECK(ex.Result);
                    //if(ex.Result == RESULT.ERR_UNINITIALIZED)throw ex;
                    MusicChanging(this, index, Error);
                    return;
                }
            }
            try{MusicChanging.Invoke(this, index, Error);}catch{}
            //Application.DoEvents();
            //ERRCHECK(HelperEx.Result);
            //if (CurrentSoftwareFormat == null) CurrentSoftwareFormat = new SoftwareFormat();
            _DeviceFormat.Samplerate = 0;
            Result =
            system.getSoftwareFormat(ref _DeviceFormat.Samplerate, ref _DeviceFormat.SoundFormat,
                                     ref _DeviceFormat.OutputChannel, ref _DeviceFormat.MaxInputChannel,
                                     ref _DeviceFormat.ResamplerMethod, ref _DeviceFormat._Bits);

            if (_SoundFormat == null) _SoundFormat = new HS_Audio.SoundFormat(); else _SoundFormat.Init();
            Result = sound.getFormat(ref _SoundFormat.SountType, ref _SoundFormat.Format, ref _SoundFormat.Channel, ref _SoundFormat.Bits);
            //result = system.init(32, FMOD.INITFLAGS.NORMAL, (IntPtr)null);
            //ERRCHECK(result);
            if (SoundFormat.SountType == HS_Audio_Lib.SOUND_TYPE.FSB)
            {
                HS_Audio_Lib.Sound s1 = new HS_Audio_Lib.Sound();
                Result = sound.getSubSound(0, ref s1);
                Result = system.playSound(HS_Audio_Lib.CHANNELINDEX.FREE, s1, true, ref channel);
            }
            else { Result = system.playSound(HS_Audio_Lib.CHANNELINDEX.FREE, sound, true, ref channel);}
            
            try{UpdateMix();}catch{}
            if(Loop)channel.setMode(MODE.LOOP_NORMAL);
            //Application.DoEvents();
            /*
                Create DSP unit
            */

            float a1 = 0;
            if(channel.getFrequency(ref a1)== RESULT.OK) DefaultFrequency = a1; //Application.DoEvents();
            if (LastFrequency == -1000000) LastFrequency = a1;
            channel.setFrequency(DefaultFrequency + Pitch);
            //if (LastFrequency < 200) channel.setFrequency(DefaultFrequency);
            if (HelperEx == null) HelperEx = new HSAudioHelperEx(this); else HelperEx.help = this;
            //HelperEx.FMODChannelInfo = ab1;
            //ab1.Frequency = HelperEx.FMODChannelInfo_Current.Frequency - DefaultFrequency;
            try { if (HelperEx != null) HelperEx.FMOD3DAttributes = HelperEx.FMOD3DAttributes_Current; } catch {}
            //HelperEx.Result = system.createDSP(ref dspdesc, ref mydsp);
            //ERRCHECK(result);
            //HelperEx.Result = mydsp.setActive(true);


            if (First)
            {
                //Application.DoEvents();
                //cs.ChannelReverbProperties = new FMOD.REVERB_CHANNELPROPERTIES();
                Init();
                /*FMOD.REVERB_CHANNELPROPERTIES a =new FMOD.REVERB_CHANNELPROPERTIES();
                channel.getReverbProperties(ref a);*/
                /*
                channel.setPan(cs.Pan);
                channel.setLowPassGain(cs.LowPassGain);
                channel.setFrequency(cs.Frequency);
                if (cs.ChannelReverbProperties != null) channel.getReverbProperties(ref cs.ChannelReverbProperties);
                if (cs.Delay != null) channel.setDelay(cs.Delay.DelayType, cs.Delay.DelayHigh, cs.Delay.DelayLow);*/
                //HelperEx.FMODChannelInfo = cs;
                //ERRCHECK(result);
            }
            else { First = true; }
                
            try { ThreadPool.QueueUserWorkItem(new WaitCallback(Md5HashThread)); } catch { }
            if (e != null) { e.Result = value; }
            AsyncComplete =true;
            PlayStatus = PlayingStatus.Ready; Error = false;}
            catch{}
            finally{try{MusicChanged(this, index, Error);}catch{}}
            //timer2.Start();
        }

        HSAudioCustomSound _CustomCreateSound;
        public HSAudioCustomSound CustomCreateSound{get{return _CustomCreateSound;}}
        public void CreateSound(HSAudioCustomSound Sound)
        {
            CustomCreateSound.Dispose();
            _CustomCreateSound = null;
            _MusicPath="CUSTOM_SOUND";
            MD5Hash = "CUSTOM_SOUND";
            Dispose(false);
            /*
             * http://www.fmod.org/questions/question/pcmsetposcallback-never-is-called-during-playback/
             * https://www.google.co.kr/?gfe_rd=cr&ei=FUc0VsfrL6ST8QfR-YDICQ&gws_rd=ssl#q=fmod+SOUND_PCMREADCALLBACK
             * 앞으로 할 것
             * DSP, 사운드담당 분리할것
             * 1. 사운드 중지하고 System.CreateSound로 사운드 만들어서 내부 sound에 할당할것
             */
        }

        internal void Md5HashThread(object o) { MD5Hash = HS_Audio.Lyrics.GetLyrics.GenerateMusicHash(MusicPath); }
        private void MusicPathWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        { try { if (MusicChanging != null) MusicChanging(this, index, Error); } catch { }}

        SoundDeviceFormat _DeviceFormat = new SoundDeviceFormat();
        public SoundDeviceFormat DeviceFormat { get { return _DeviceFormat; } }
        internal SoundDeviceFormat getDeviceFormat()
        {
            SoundDeviceFormat format = new SoundDeviceFormat();
            RESULT Result =
            system.getSoftwareFormat(ref format.Samplerate, ref format.SoundFormat,
                         ref format.OutputChannel, ref format.MaxInputChannel,
                         ref format.ResamplerMethod, ref format._Bits);
            Result = system.getOutput(ref format.OutputType);

            return format;
        }


        SoundFormat _SoundFormat = new SoundFormat();
        [Obsolete]
        public SoundFormat SoundFormat { get { return _SoundFormat; } }
        public SoundFormat SoundFormat_Current
        {
            get
            {
                SoundFormat b = new SoundFormat();
                sound.getFormat(ref b.SountType, ref b.Format, ref b.Channel, ref b.Bits);
                return b;
            }
        }
        #endregion

        #region 메서드

        /// <summary>
        /// 내부 리소스를 새로 초기화 합니다. (시스템도 초기화합니다.)
        /// </summary>
        public void NewCreate()
        {
            HS_Audio_Lib.RESULT result = HS_Audio_Lib.RESULT.OK;
            channel.setPaused(false); //FMOD.Sound s;//channel.
            if (sound != null) sound.release();
            if (system != null) system.release();
            HS_Audio_Lib.Factory.System_Create(ref system);
            System_Created();
            try { Initializing(this.system); }catch { }
            if (Play3D) { result = system.createStream(MusicPath, HS_Audio_Lib.MODE.SOFTWARE | HS_Audio_Lib.MODE._3D, ref sound); }
            else { result = system.createStream(MusicPath, HS_Audio_Lib.MODE.SOFTWARE | HS_Audio_Lib.MODE._2D, ref sound); }
            Stop();
            //result = system.createSound(value, FMOD.MODE.SOFTWARE | FMOD.MODE.CREATESTREAM, ref sound);
            //ERRCHECK(result);
            ThreadReset(true);
            //timer1.Start();
        }
        void ThreadReset(bool Start)
        {
            try { th.Abort(); }catch{} 
            th = new System.Threading.Thread(ts);
            if (Start) th.Start();}
        /// <summary>
        /// 음악을 재생 합니다.
        /// </summary>
        public void Play()
        {
            //timer1.Start();
            CustomComplete = false;
            try{Mix = LastMix;}catch{}
            TickErrorPatch = TickErrorPatch;
            //Frequency = LastFrequency;
            ERRCHECK(channel.setPaused(false), false);
            //if (th != null && !th.IsAlive) ThreadReset(true);
            //try { th.Start(); }catch(System.Threading.ThreadStateException){ThreadReset(true);}
            try{if(dsp!=null)dsp.Gain = Mix;}catch{}
            //if (system != null) try{Volume = LastVolume;}catch(SoundSystemException){Volume=LastVolume<0f?0f:1.0f;}
            PlayStatus = PlayingStatus.Play;
            Result= channel.setPan(HelperEx.FMODChannelInfo_Current.Pan);
            //try { dsp.Mix = dsp.CurrentMix; }catch{}
            timer2.Start();
            //ERRCHECK(result);
        }

        /// <summary>
        /// 음악을 잠깐 멈춥니다.
        /// </summary>
        public void Pause() { bool a = false ; channel.isPlaying(ref a); channel.setPaused(true); PlayStatus = PlayingStatus.Pause; }

        HS_Audio_Lib.TIMEUNIT tu = HS_Audio_Lib.TIMEUNIT.MS;
        uint posion;
        /// <summary>
        /// 음악을 멈춥니다.
        /// </summary>
        public void Stop()
        {
            //channel.stop();
            RESULT res = channel.setPaused(true);
            res = channel.setPosition(0, TIMEUNIT.MS);
            //Result = system.playSound(HS_Audio_Lib.CHANNELINDEX.FREE, sound, true, ref channel);
            //Init();
            PlayStatus = PlayingStatus.Stop;
            //try { MusicPathChanged(MusicPath, index, false); }catch{}
        }

        public void Init()
        {
            try{
            if (Play3D) {
            //MusicPath = _MusicPath;
            UpdateMix();
            //Result=channel.setMode(HS_Audio_Lib.MODE._3D);
            channel.set3DAttributes(ref HelperEx.FMOD3DAttributes_Current.Pos, ref HelperEx.FMOD3DAttributes_Current.Vel); }
            //FMOD.VECTOR a = new FMOD.VECTOR(), b = new FMOD.VECTOR();
            //channel.get3DAttributes(ref a, ref b);
            channel.setVolume(LastVolume);
            dsp.Gain = Mix;
            channel.setPan(this.Pan);
            //channel.setPan(HelperEx.FMODChannelInfo_Current.Pan);
            //channel.setFrequency(LastFrequency);
            /*
            channel.setLowPassGain(HelperEx.FMODChannelInfo_Current.LowPassGain);
            try{channel.setSpeakerMix(HelperEx.FMODChannelInfo_Current.SpeakerMix.FrontLeft,
                                  HelperEx.FMODChannelInfo_Current.SpeakerMix.FrontRight,
                                  HelperEx.FMODChannelInfo_Current.SpeakerMix.Center,
                                  HelperEx.FMODChannelInfo_Current.SpeakerMix.LFE,
                                  HelperEx.FMODChannelInfo_Current.SpeakerMix.BackLeft,
                                  HelperEx.FMODChannelInfo_Current.SpeakerMix.BackRight,
                                  HelperEx.FMODChannelInfo_Current.SpeakerMix.SideLeft,
                                  HelperEx.FMODChannelInfo_Current.SpeakerMix.SideRight);}catch{}
            try{channel.setDelay(HelperEx.FMODChannelInfo_Current.Delay.DelayType,
                                 HelperEx.FMODChannelInfo_Current.Delay.DelayHigh,
                                 HelperEx.FMODChannelInfo_Current.Delay.DelayLow);}catch { }*/}
            catch{}
        }

        public void UpdateMix()
        {
            if (dsp == null) { dsp = new HSAudioDSPHelper(this, false);  }
            else { /*dsp.ReRegisterDSP();*/ }
            dsp.Connect();
            dsp.Gain = LastMix;
        }
        public void UpdateMix(bool IsSystem)
        {
            if (dsp == null) { dsp = new HSAudioDSPHelper(this, IsSystem); }
            else { dsp.Connect(IsSystem); /*dsp.ReRegisterDSP(IsSystem);*/}
            dsp.Gain = LastMix;
        }

        public bool IsRestart { get; set; }
        public void Restart()
        {
            system.release();
            //system.close();
            NewInstance(MusicPath);
            IsRestart = true;
        }
        #endregion

        #region DSP 플러그인

        public PluginDSP[] DefaultPlugins{get;private set;}
        private PluginDSP[] getDefaultPlugins()
        {
            System.Collections.Generic.List<PluginDSP> plug = new System.Collections.Generic.List<PluginDSP>();
            for (int i = 0; i <= 2; i++)
            {
                int numberOfPlugins = 0;
                RESULT result = system.getNumPlugins((PLUGINTYPE)i, ref numberOfPlugins);
                for (int j = 0; j < numberOfPlugins; j++)
                {
                    System.Text.StringBuilder sb = new System.Text.StringBuilder(256);
                    plug.Add(new PluginDSPInstance(j, (PLUGINTYPE)i));
                    result = system.getPluginHandle(plug[plug.Count - 1].PluginType, j, ref plug[plug.Count - 1]._Handle);
                    result = system.getPluginInfo(plug[plug.Count - 1].Handle, ref plug[plug.Count - 1]._PluginType, sb, 256, ref plug[plug.Count - 1]._Version);
                    plug[plug.Count - 1]._Name = sb.ToString();
                    plug[plug.Count - 1].IsSystemPlugin = true;
                }
            }
            return plug.ToArray();
        }

        string _WinAmpPluginPath;
        /// <summary>
        /// 윈앰프 플러그인이 들어있는 폴더를 설정하거나 가져옵니다.
        /// </summary>
        [Obsolete("플러그인 DSP를 이용해 주세요.")]
        public string WinAmpPluginPath
        {
            get {return _WinAmpPluginPath; }
            set
            {
                _WinAmpPluginPath = value;
                //Plugins = GetPlugins(value);
                if (Plugins == null && Plugins.Length < 1) throw new SoundSystemException(RESULT.ERR_PLUGIN, "윈앰프 플러그인 경로설정에 실패하였습니다.");
            }
        }

        public PluginDSP[] Plugins {get; private set; }
        internal void PriorityPlugin(PluginDSP p)
        {
            PluginDSP tmp = p;
            List<PluginDSP> a = new List<PluginDSP>(Plugins);
            for(int i=0;i<a.Count;i++)
                if(a[i].Handle == p.Handle){a.RemoveAt(i);a.Insert(0, tmp);break;}
            for(int i=0;i<a.Count;i++)a[i]._Priority = (uint)i;
            Plugins = a.ToArray();
        }
        /*
        public HS_Audio_Lib.DSP setDSPPlugin(Plugin plugin)
        {
            //List<Plugin> plug = new List<Plugin>(Plugins);
            IntPtr ip  = IntPtr.Zero;
            if(res != RESULT.OK)return null;
            res = system.createDSPByPlugin(plugin.Handle, ref ip);
            if (res != RESULT.OK) return null;
            dsp.setRaw(ip);
            //plug.Add(plugin);
            return dsp;
        }*/
        public bool RemovePlugin(PluginDSP plugin)
        {
            List<PluginDSP> plug = new List<PluginDSP>(Plugins);
            try
            {
                plug.Remove(plugin);
                plugin.dsp.DisConnect();
                /*
                if(dsp!=null){
                plugin.dsp.release();
                plugin.dsp.remove();}*/
                return system.unloadPlugin(plugin.Handle) == RESULT.OK;
            }
            catch{return false;}
            finally{Plugins = plug.ToArray();}
        }

        HS_Audio_Lib.DSP PerviousDSP;

        public RESULT CreateDSPByPlugin(PluginDSP p)
        {
            if (p.PluginType == PLUGINTYPE.DSP)
            {
                //if (PerviousDSP != null) PerviousDSP.release();
                //HS_Audio_Lib.DSP dsp = new HS_Audio_Lib.DSP();
                //system.loadPlugin(p.Path, ref p._Handle, p.Priority);
                RESULT res = system.createDSPByPlugin(p.Handle, ref p._DSPHandle);
                if (p.PluginKind == PluginKind.VST)
                {
                    HSAudioDSPHelper dp = new HSAudioDSPHelper(this, HS_Audio_Lib.DSP_TYPE.VSTPLUGIN); dp.Instance = p._DSPHandle; p.dsp = dp;
                    HS_Audio.Plugin.frmVSTPluginHost frmvst = new HS_Audio.Plugin.frmVSTPluginHost(p){Text = p.FullName + " - HS플레이어 VST 플러그인 창"};
                    p.Dialog = frmvst;
                    p.dsp.ShowConfigDialog(frmvst.vstPluginControl1.Handle, true);
                    frmvst.ReSize();
                }
                else
                {
                    //p.Dialog = null;
                    //p.dsp.showConfigDialog(IntPtr.Zero, false);
                    HSAudioDSPHelper dp = new HSAudioDSPHelper(this, HS_Audio_Lib.DSP_TYPE.WINAMPPLUGIN); dp.Instance = p._DSPHandle; p.dsp = dp;
                }
                //RESULT res = system.createDSPByType(DSP_TYPE.WINAMPPLUGIN, ref dsp);
                //p.dsp = PerviousDSP = dsp;
                return res;
            }
            return RESULT.ERR_INVALID_PARAM;
        }
        public RESULT ReleasePlugin(PluginDSP p) {return system.unloadPlugin(p.Handle);}

        public PluginDSP CreatePlugins(string PluginPath, uint Priority=0, bool ThrowException = false)
        {
            if (System.IO.File.Exists(PluginPath))
            {
                try
                {
                    PluginDSP p = new PluginDSPInstance();
                    p._Path = PluginPath; p._Priority = Priority;
                    RESULT res = system.loadPlugin(p.Path, ref p._Handle, p._Priority);
                    if (res != RESULT.OK) if(ThrowException)throw new SoundSystemException(res); else return null;
                    System.Text.StringBuilder sb = new System.Text.StringBuilder(256);
                    system.getPluginInfo(p.Handle, ref p._PluginType, sb, 256, ref p._Version);
                    p._Name = sb.ToString();

                    if (p.FullName.Remove(3).ToUpper() == "VST") p.PluginKind = PluginKind.VST;
                    else if (p.FullName.Remove(6).ToUpper() == "WINAMP") p.PluginKind = PluginKind.WINAMP;
                    else p.PluginKind = PluginKind.UNKNOWN;
                    CreateDSPByPlugin(p);
                    //system.unloadPlugin(p.Handle);
                    if (Plugins == null) Plugins = new PluginDSP[0];
                    List<PluginDSP> plug = new List<PluginDSP>(Plugins); plug.Add(p); this.Plugins = plug.ToArray();
                    
                    return p;
                }
                catch(SoundSystemException sex){if(ThrowException)throw sex;else return null;}
            }
            else {if (ThrowException)throw new Exception("플러그인 파일을 찾을 수 없습니다."); else return null;}
        }
        #endregion

        #region 사운드 장치
        public SoundDevice DefaultSoundDevice { get {return getSoundDevices()[0];}}
        //public RESULT setOutputDevice(Device OutputDevice) {return system.setDriver(OutputDevice.ID);}
        public SoundDevice LastSoundDevice{get;private set;}
        SoundDevice _OutputDevice;
        public SoundDevice SoundDevice {get{return _OutputDevice;} set{_OutputDevice=LastSoundDevice=value;Result  = system.setDriver(value.ID);} }
        public SoundDevice[] getSoundDevices(out RESULT[] results)
        {
            int Drivernum = 0;
            RESULT res = system.getNumDrivers(ref Drivernum);
            if (res != RESULT.OK) { results = new RESULT[] { res };return null; }

            System.Collections.Generic.List<SoundDevice> di = new System.Collections.Generic.List<SoundDevice>();
            RESULT[] result = new RESULT[Drivernum];

            for (int i = 0; i < Drivernum; i++) 
            {
                System.Text.StringBuilder sb = new System.Text.StringBuilder(256);
                HS_Audio_Lib.GUID guid = new GUID();
                //for (int j = 0; j < 100; j++)try{ result[i] = system.getDriverInfo(i, sb, j, ref guid); if (result[i] != RESULT.OK) break;}catch{ break;}
                result[i] = system.getDriverInfo(i, sb, 256, ref guid);
                di.Add(new SoundDevice(i, sb.ToString(), guid));

                /*
                OUTPUTTYPE type = OUTPUTTYPE.MAX;
                system.getOutput(ref type);*/
            }
            //DefaultSoundDevice = di[0];
            results = result;
            return di.ToArray();
        }
        public SoundDevice[] getSoundDevices() { RESULT[] tmp = null; return getSoundDevices(out tmp); }
        public RESULT setSoundDevice(SoundDevice Device){_OutputDevice = LastSoundDevice = Device;return system.setDriver(Device.ID); }
        internal RESULT setSoundDevice(int Device) { return system.setDriver(Device); }
        public delegate void SoundDeviceChangedEventHandler(SoundDevice[] SoundDevice);
        public event SoundDeviceChangedEventHandler SoundDeviceChanged;

        
        public HS_Audio_Lib.OUTPUTTYPE DefaultOutputType{get;private set;}
        public OUTPUTTYPE OutputType
        {
            get{OUTPUTTYPE type = OUTPUTTYPE.UNKNOWN; system.getOutput(ref type);return type;}
            set{Result = system.setOutput(value);}
        }

        public RESULT setOutputType(OUTPUTTYPE OutputType){return system.setOutput(OutputType);}
        public OUTPUTTYPE getOutputType(out RESULT Result)
        {
            OUTPUTTYPE type = OUTPUTTYPE.UNKNOWN;
            RESULT res = system.getOutput(ref type);
            Result = res;
            return type;
        }
        #endregion

        public static string ERRCHECK(HS_Audio_Lib.RESULT result, bool ThrowException)
        {
            string a = null;
            if (result != HS_Audio_Lib.RESULT.OK)
            {
                //timer1.Stop();fh.Complete bool
                a = "사운드 시스템 오류!  Result: " + result + " - " + HS_Audio_Lib.Error.String(result);
                if (ThrowException) { throw new SoundSystemException(result); }
                else { return a; }
                //Environment.Exit(-1);
            }
            else { return a; }
        }
        public static string ERRCHECK(HS_Audio_Lib.RESULT result)
        {
            string a=null;
            if (result != HS_Audio_Lib.RESULT.OK)
            {
                //timer1.Stop();
                a = "사운드 시스템 오류!  Result: " + result + " - " + HS_Audio_Lib.Error.String(result);
                throw new SoundSystemException(result);
                //Environment.Exit(-1);
            }
            return a;
        }
    }

    /// <summary>
    /// 사운드 시스템 관련 오류 예외 클래스 입니다.
    /// </summary>
    public class SoundSystemException : Exception
    {
        /// <summary>
        /// 사운드 시스템 예외 클래스를 초기화 합니다.
        /// </summary>
        public SoundSystemException() { /*Result = FMOD.RESULT.OK;*/this._Message = "사운드 시스템 내부에서 오류가 발생하였습니다."; }
        /// <summary>
        /// 사운드 시스템 예외 클래스를 초기화 합니다.
        /// </summary>
        /// <param name="Result">오류 이유 입니다. (HS_Audio_Lib.Result)</param>
        public SoundSystemException(HS_Audio_Lib.RESULT Result)
        { this.Result = Result; _PureMessage =HS_Audio_Lib.Error.String(Result);_Message = Result + " - " + _PureMessage;}//"사운드 시스템 오류! " + Result + " - " + _PureMessage; }
        /// <summary>
        /// 사운드 시스템 예외 클래스를 초기화 합니다.
        /// </summary>
        /// <param name="Result">오류 이유 입니다. (HS_Audio_Lib.Result)</param>
        /// <param name="Message">표시할 메세지 입니다.</param>
        public SoundSystemException(HS_Audio_Lib.RESULT Result, string Message)
        {
            this.Result = Result; _Message = Message;
            _PureMessage = HS_Audio_Lib.Error.String(Result);
        }

        HS_Audio_Lib.RESULT _Result;
        /// <summary>
        /// 사운드 시스템 내부 오류값 입니다.
        /// </summary>
        public HS_Audio_Lib.RESULT Result
        {
            get{return _Result;}
            private set{_Result = value;}
        }
        string _Message;
        /// <summary>
        /// 오류를 설명하는 메세지를 가져옵니다.
        /// </summary>
        public override string Message{ get{return _Message;} }

        string _PureMessage = null;
        public string PureMessage{ get{return _PureMessage;} }
        /// <summary>
        /// 지원 사이트 입니다.
        /// </summary>
        public override string HelpLink
        {
            get { return "http://www.fmod.org/support/"; }
        }
    }
    public class SoundFormat
    {
        public SoundFormat() { }
        public SoundFormat(HS_Audio_Lib.SOUND_TYPE SountType, HS_Audio_Lib.SOUND_FORMAT Format, int Channel, int Bits)
        { this.Format = Format; this.Channel = Channel; this.Bits = Bits; }

        public HS_Audio_Lib.SOUND_TYPE SountType;
        public HS_Audio_Lib.SOUND_FORMAT Format;
        public int Channel;
        public int Bits;

        public void Init()
        {
            Format = HS_Audio_Lib.SOUND_FORMAT.NONE;
            SountType = HS_Audio_Lib.SOUND_TYPE.UNKNOWN;
            Bits = Channel = 0;
        }
    }
}
