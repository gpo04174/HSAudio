﻿using System;
using System.Text;
using System.Windows.Forms;
using System.IO;
using TagLib;
using System.Collections.Generic;
using System.Drawing.Imaging;
using System.Drawing;
using System.Threading;
using TSampleType = System.Single;
using TLongSampleType = System.Double;
using XPicbox;
using HS_Audio.Languge;
using HS_Audio.LIBRARY.HSOther;

namespace HS_Audio
{
    public partial class frmSoundSystemInfo : Form
    {
        internal LocaleString LocalString = new ko_kr();

        System.Threading.Thread th;
        System.Threading.Thread th1;
        System.Threading.Thread TagThread;
        System.Threading.Thread BPMThread;
        internal frmSoundSystemInfo()
        {
            InitializeComponent();
            comboBox1.SelectedIndex = 0;
            comboBox1.SelectedText = "시스템";
            새로고침ToolStripMenuItem_Click(null, null);
            th = new System.Threading.Thread(new System.Threading.ThreadStart(CalulateCpu));
            th1 = new System.Threading.Thread(new System.Threading.ThreadStart(CalulateBps));
            BPMThread = new System.Threading.Thread(new System.Threading.ThreadStart(CaculatorFileBPM));
            th.Start(); //th1.Start();

            Program.localeManager.LocaleChanged += (Locale locale) => { LocalString = LocaleManager.GetLocaleString(locale); };
        }
        internal HSAudioHelper Helper;
        public frmSoundSystemInfo(HSAudioHelper Helper)
        {
            InitializeComponent();
            this.Helper = Helper;
            this.Helper.MusicChanged += Helper_MusicChanged;
            comboBox1.SelectedIndex = 0;
            comboBox1.SelectedText = "시스템";
            Update_FileInfo(); timer2_Tick(null, null); timer1_Tick(null, null); 새로고침ToolStripMenuItem_Click(null, null);
            th = new System.Threading.Thread(new System.Threading.ThreadStart(CalulateCpu)); th.Start();
            th1 = new System.Threading.Thread(new System.Threading.ThreadStart(CalulateBps));

            //BPMThread = new System.Threading.Thread(new System.Threading.ThreadStart(CaculatorFileBPM));
            bp = new LIBRARY.SoundTouch.BpmDetectFloat(Helper.DeviceFormat.OutputChannel, Helper.DeviceFormat.Samplerate);
            //BPMThread.Start();

            TICK_Before = Helper.GetCurrentTick(HS_Audio_Lib.TIMEUNIT.RAWBYTES); th1.Start();
            Helper.MusicChanged += Helper_MusicPathChanged;
            timer1.Start();
            timer2.Start();


            Program.localeManager.LocaleChanged += (Locale locale)=>{ LocalString = LocaleManager.GetLocaleString(locale);};
        }

        public void Update_FileInfo()
        {
            FileInfo fi =new FileInfo(Helper.MusicPath);
            StringBuilder MusicName = new StringBuilder();
            try { Helper.sound.getName(MusicName, 35); }catch{}
            FileInfo_txtFilePath.Text = Helper.MusicPath;
            FileInfo_lblFileName.Text = "파일 이름: " + fi.Name;//MusicName.ToString();
            FileInfo_txtFileHash.Text = HS_Audio.Lyrics.GetLyrics.GenerateMusicHash(Helper.MusicPath);
            FileInfo_lblFileSize.Text = "파일 크기: " + fi.Length.ToString("00,000") + " Byte";
            FileInfo_lblWaveSize.Text = "실제 크기: "+Helper.WaveLength.ToString("00,000") + " Byte";
            TimeSpan ts = TimeSpan.FromMilliseconds((double)Helper.TotalPosition);
            lblFileInfo_TotalTime.Text = string.Format("전체 시간: {0}시 {1}분 {2}초 {3}밀리초 [{4}:{5}:{6}.{7}]", 
                ts.Hours.ToString("00"), ts.Minutes.ToString("00"), ts.Seconds.ToString("00"), ts.Milliseconds.ToString("000"),
                ts.Hours.ToString("00"), ts.Minutes.ToString("00"), ts.Seconds.ToString("00"), ts.Milliseconds.ToString("000"));
            FileInfo_lblSoundFormat.Text = "사운드 포맷: " + Helper.SoundFormat_Current.Format.ToString();
            FileInfo_lblSoundType.Text = "사운드 타입: " + Helper.SoundFormat_Current.SountType.ToString();
            FileInfo_lblOriginalSample.Text = "원본 샘플링: " + Helper.DefaultFrequency.ToString() + " Hz";

            FileInfo_lblChannel.Text = "채널: " + Helper.SoundFormat_Current.Channel.ToString() + " 개";
            FileInfo_lblBit.Text = "비트: " + Helper.SoundFormat_Current.Bits.ToString() + " bit";

            PlayInfo_lblTotalTime.Text=string.Format("총 재생 시간    : {0}시 {1}분 {2}초 {3} 밀리초 [{4}:{5}:{6}.{7}]", 
                ts.Hours.ToString("00"), ts.Minutes.ToString("00"), ts.Seconds.ToString("00"), ts.Milliseconds.ToString("000"),
                ts.Hours.ToString("00"), ts.Minutes.ToString("00"), ts.Seconds.ToString("00"), ts.Milliseconds.ToString("000"));
        }
        HSAudioHelper Helper1;
        public void Update_FileInfo(HSAudioHelper Helper)
        {
            Helper1 = Helper;

        }
        /*
       /// <summary>
       /// 파일에서 음악의 MD5해쉬 값을 구합니다.
       /// </summary>
       /// <param name="Music">음악파일 경로 입니다.</param>
       /// <returns></returns>
       public static string GenerateMusicHash(string Music)
       {
           try
           {
               StreamReader sr = new StreamReader(Music);
               Stream Music1 = sr.BaseStream;
               if (Music1 == null) { return null; }
               int num = 0;
               Music1.Position = 0L;
               byte[] buffer = new byte[0x1b];
               Music1.Read(buffer, 0, 0x1b);
               if ((buffer.Length < 0x1b) || (Music1.Position > 0xc350L)) {return null; }
               long position = Music1.Position;
               byte[] destinationArray = new byte[4];
               Array.Copy(buffer, 0, destinationArray, 0, 4);
               byte[] buffer3 = new byte[3];
               Array.Copy(buffer, 0, buffer3, 0, 3);
               if (Encoding.ASCII.GetString(destinationArray) == "OggS")
               {
                   Music1.Position = position + 4L;
                   a1(Music1);
               }
               else if (Encoding.ASCII.GetString(buffer3, 0, 0) == "ID3")
               {
                   Music1.Position -= 0x11L;
                   byte[] buffer4 = new byte[4];
                   Array.Copy(buffer, 6, buffer4, 0, 4);

                   int num1 = 0;
                   while (num1 < 500000)//0x7a120
                   {
                       if (Encoding.ASCII.GetString(buffer3).ToString() == "ID3")
                       {
                           num = 10 + ((((buffer4[0] << 21)//*0x15
                                            | (buffer4[1] << 14)) | (buffer4[2] << 7)) | buffer4[3]);
                           if (Music1.CanSeek)
                           {
                               Music1.Position = (long)num;
                               break;
                           }
                       }
                       num++;
                   }
                   if (num == 0x7a120) {Music1.Position = 0L; }
                   Music1 = a1(Music1);
               }
               else
               {
                   Music1.Position = 0L;
                   a1(Music1);
               }
               byte[] buffer5 = new byte[0x28000];
               Music1.Read(buffer5, 0, 0x28000);
               return GetHash(buffer5);
           }
           catch { return null; }
       }
       private static Stream a1(Stream A_0)
       {
           for (int i = 0; i < 50000; i++)//0xc350=50000
           {
               if ((A_0.ReadByte() == 0xff) && ((A_0.ReadByte() >> 5) == 7))
               {
                   A_0.Position += -2L;
                   return A_0;
               }
           }
           return A_0;
       }
       private static string GetHash(byte[] A_0)
       {
           System.Security.Cryptography.MD5CryptoServiceProvider provider = 
               new System.Security.Cryptography.MD5CryptoServiceProvider();
           provider.ComputeHash(A_0);
           byte[] hash = provider.Hash;
           StringBuilder builder = new StringBuilder();
           foreach (byte num in hash)
           {
               builder.Append(string.Format("{0:X2}", num));
           }
           return builder.ToString();
       }*/

        List<byte[]> FMODTAGSTRING = new List<byte[]>();
        Dictionary<string, string> TAGFMOD = new Dictionary<string,string>();
        byte[] FMODTAG;
        private void frmSoundSystemInfo_Load(object sender, EventArgs e)
        {
            try{contextMenuStrip1.ImageScalingSize = contextMenuStrip2.ImageScalingSize = menuStrip1.ImageScalingSize = new Size(16, 16); }
            catch{}

            //decimal a = (decimal)Math.Log(4, 9);
            //decimal b = (decimal)Math.Log(27, 16);
            //decimal c = a * b;
            CurrentPreLoading = Helper.PreLoading;
            MemoryInfo = (uint)numericUpDown2.Value;
            imageList1.ImageSize = pictureBox1.Size;
            MusicPath = Helper.MusicPath;
            txtGenres.Items.AddRange(TagLib.Genres.Audio);

            try{comboBox2.SelectedIndex = 0;}catch{}

            
            HS_Audio_Lib.TAG tag = new HS_Audio_Lib.TAG();
            tag.datatype = HS_Audio_Lib.TAGDATATYPE.BINARY;
            for (int i = 0; i < 1000; i++)
            {
                HS_Audio_Lib.RESULT result = Helper.sound.getTag(null, i, ref tag);
                /*
                unsafe
                {
                    byte* aa = (byte*)tag.data.ToPointer();
                    byte[] bb = new byte[tag.datalen];
                    for (int j = 0; j < tag.datalen; j++) bb[j] = aa[j];
                    FMODTAGSTRING.Add(bb);
                    string a1 = Encoding.UTF8.GetString(bb);
                    ListViewItem li = new ListViewItem(tag.name);
                    string tmp = tag.type.ToString();
                    li.SubItems.Add(tmp.Substring(tmp.LastIndexOf(".")+1));
                    li.SubItems.Add(a1);
                    listView1.Items.Add(li);
                    //if (TAGFMOD.ContainsKey(tag.name))TAGFMOD[tag.name] = a1;
                    //else TAGFMOD.Add(tag.name, a1);
                }*/
                if(result!=HS_Audio_Lib.RESULT.OK)break;
            }
            
            if(tag.type == HS_Audio_Lib.TAGTYPE.FMOD||
               tag.type == HS_Audio_Lib.TAGTYPE.MIDI||
               tag.type == HS_Audio_Lib.TAGTYPE.ICECAST){
            try
            {
                unsafe
                {
                    void* aa = tag.data.ToPointer();
                    byte* ftag = (byte*)aa;
                    FMODTAG = new byte[tag.datalen];
                    for (int i = 0; i < tag.datalen; i++) FMODTAG[i] = ftag[i];

                }
            }catch{}}
            else 
            {
                comboBox3.Items.AddRange(Enum.GetNames(typeof(TagLib.PictureType)));
                //comboBox2.Items.AddRange(Enum.GetNames(typeof(TagLib.ReadStyle)));
                if (TagThread != null) TagThread.Abort();
                TagThread = new Thread(new ParameterizedThreadStart(UpdatgeTag)) { Name = "음악 태그 스레드" };
                tabPage3.Text = (Helper != null && !Helper.PreLoading) ? "음악 태그 (로딩중, 편집 불가능)" : "음악 태그 (로딩중)";
                TagThread.Start(Helper.MusicPath);// LoadMusicTag(Helper.MusicPath);
            }
            if(bPM자동업데이트ToolStripMenuItem.Checked) CaculateBPM(MusicPath, Helper);
        }

        TimeSpan ts;
        float dsp, stream, geometry, update, total;
        float CPU, CPUTotal;
        uint memoryused; HS_Audio_Lib.MEMORY_USAGE_DETAILS detail;
        private void timer1_Tick(object sender, EventArgs e)
        {
            ts = TimeSpan.FromMilliseconds((double)Helper.CurrentPosition);
            StringBuilder CurrentTime = new StringBuilder("현재 재생 시간 : ");
            CurrentTime.Append(ts.Hours.ToString("00"));CurrentTime.Append("시 ");
            CurrentTime.Append(ts.Minutes.ToString("00"));CurrentTime.Append("분 ");
            CurrentTime.Append(ts.Seconds.ToString("00"));CurrentTime.Append("초 ");
            CurrentTime.Append(ts.Milliseconds.ToString("000"));CurrentTime.Append(" 밀리초 [");
            CurrentTime.Append(ts.Hours.ToString("00"));CurrentTime.Append(":");
            CurrentTime.Append(ts.Minutes.ToString("00"));CurrentTime.Append(":");
            CurrentTime.Append(ts.Seconds.ToString("00"));CurrentTime.Append(".");
            CurrentTime.Append(ts.Milliseconds.ToString("000")); CurrentTime.Append("] (");
            CurrentTime.Append(((float)Helper.CurrentPosition / (float)Helper.TotalPosition * 100).ToString("0.000"));
            CurrentTime.Append("%)");
            PlayInfo_lblCurrentTime.Text = CurrentTime.ToString();

            Helper.system.getCPUUsage(ref dsp, ref stream, ref geometry, ref update, ref total);
            PlayInfo_lblTotalCPU.Text = string.Format("프로그램 CPU  : {0}% / {1}%, [백분율: (시스템: {2}%, IDLE: {3}%)]", 
                CPU.ToString(), CPUTotal.ToString("0.00"), (total).ToString("0.000"), (100f - (total)).ToString("0.000"));
            PlayInfo_lblCPUSystem.Text = string.Format("스트림      : {0}%", (stream).ToString("0.000"));
            PlayInfo_lblCPUUpdate.Text = string.Format("업데이트   : {0}%", (update).ToString("0.000"));
            PlayInfo_lblCPUDSP.Text = string.Format("DSP         : {0}%", (dsp).ToString("0.000"));
            PlayInfo_lblCPUGeometry.Text = string.Format("Geometry : {0}%", (geometry).ToString("0.000"));

            Freq=Helper.Frequency - Helper.DefaultFrequency;
            PlayInfo_lblCurrentSampling.Text = string.Format("현재 주파수 : {0} Hz {1}", Helper.Frequency.ToString("##,###.#"), Helper.Frequency == Helper.DefaultFrequency ? "" : Freq>0?string.Format("(+{0} Hz)",Freq.ToString("#.#")):string.Format("({0} Hz)",Freq.ToString("#.#")));

            HS_Audio_Lib.OUTPUTTYPE type = HS_Audio_Lib.OUTPUTTYPE.UNKNOWN;
            HS_Audio_Lib.RESULT res = HS_Audio_Lib.RESULT.OK;
            type = Helper.getOutputType(out res);
            PlayInfo_lblSoundOutput.Text = "사운드 출력 방법 : " + getOuputTypeString(type);

            //Helper.getde
            //PlayInfo_txtSoundSevice.Text = type.ToString();

            //Play_lblCurrentTime .Text="현재 재생 시간 : 00시 00분 00초 000 밀리초'
            //Inside_lblProgramRealMemory.Text = string.Format("프로그램 실제    메모리 : {0}", );
            //Inside_lblProgramPageMemory.Text= string.Format("프로그램 페이지 메모리 : {0}", );
        }
        public static string getOuputTypeString(HS_Audio_Lib.OUTPUTTYPE type)
        {
            string a = type.ToString();
            if (type == HS_Audio_Lib.OUTPUTTYPE.UNKNOWN) return "알 수 없음";
            else if (type == HS_Audio_Lib.OUTPUTTYPE.AUTODETECT) return "자동 선택";
            else if (type == HS_Audio_Lib.OUTPUTTYPE.WASAPI) return a + " [Windows Audio Session API]";
            else if (type == HS_Audio_Lib.OUTPUTTYPE.DSOUND) return a + " [DirectSound]";
            else if (type == HS_Audio_Lib.OUTPUTTYPE.WINMM) return a + " [Windows Multimedia]";
            else if (type == HS_Audio_Lib.OUTPUTTYPE.ASIO) return a + " [Low Latency ASIO 2.0 Driver]";
            else if (type == HS_Audio_Lib.OUTPUTTYPE.NOSOUND) return a + " [소리 없음]";
            else if (type == HS_Audio_Lib.OUTPUTTYPE.NOSOUND_NRT) return a + " [소리 없음 (실시간)]";
            else if (type == HS_Audio_Lib.OUTPUTTYPE.WAVWRITER) return a + " [Wave Writer]";
            else if (type == HS_Audio_Lib.OUTPUTTYPE.WAVWRITER_NRT) return a + " [Wave Writer (실시간)]";
            else if (type == HS_Audio_Lib.OUTPUTTYPE.WAVWRITER) return a + " [Wave Writer]";
            else return a;
        }
        float Freq;
        private void Helper_MusicPathChanged(HSAudioHelper Helper, int index, bool Error)
        {
            bp = new LIBRARY.SoundTouch.BpmDetectFloat(Helper.DeviceFormat.OutputChannel, Helper.DeviceFormat.Samplerate);
            Update_FileInfo();
            if(bPM자동업데이트ToolStripMenuItem.Checked) CaculateBPM(MusicPath, Helper);
            else PlayInfo_lblBPM.Text = "BPM : <OFF>";
            comboBox2_SelectedIndexChanged(null, null);
        }

        LIBRARY.HSOther.MusicStreamer1 bpm;

        HS_CSharpUtility.CpuUsage cpu = new HS_CSharpUtility.CpuUsage();
        void CalulateCpu()
        {
            while (true) { try { CPU = cpu.GetUsage(out CPUTotal); } catch { }
                System.Threading.Thread.Sleep(500); }
        }

        uint TICK_Before;
        void CalulateBps()
        {
            while (true)
            {
                try
                {
                    uint TICK = Helper.GetCurrentTick(CurrentPreLoading ? HS_Audio_Lib.TIMEUNIT.PCMBYTES: HS_Audio_Lib.TIMEUNIT.RAWBYTES);
                    uint bps = Math.Min(TICK-TICK_Before, TICK_Before-TICK);
                    float Kbps = (float)bps/1024f;
                    label5.InvokeControl(()=>{label5.Text = string.Format("비트 전송    : {0} Kbps ({1} KB/s){2}", (Kbps*8).ToString("0.00"), Kbps.ToString("0.00"), CurrentPreLoading?" [PCM]":"");});
                    TICK_Before = TICK;
                }
                catch{}
                System.Threading.Thread.Sleep(1010);
            }
        }
        void CaculateBPM(string Musicpath, HSAudioHelper Helper)
        {
            try
            {
                if (bpm != null) { bpm.Stop(); bpm.Dispose(); }
                HS_Library.HSConsole.WriteLine("Caculating BPM...\n└ Path:" + Musicpath);
                if (bPM자동업데이트ToolStripMenuItem.Checked)
                {
                    try { bpm = new LIBRARY.HSOther.MusicStreamer1(Musicpath, Helper); }
                    catch { if (bpm != null) bpm.Dispose(); PlayInfo_lblBPM.Text = "BPM : Error!!"; HS_Library.HSConsole.WriteLine("Caculate BPM Error!!\n└ Path:" + Musicpath); return; }
                    bpm.Progress += ((object o, uint total, uint current) =>
                    {
                        try
                        {
                            if (PlayInfo_lblBPM.InvokeRequired)
                                PlayInfo_lblBPM.Invoke(new Action(() => { PlayInfo_lblBPM.Text = string.Format("BPM (Calc) : {0} %", ((current / (float)total) * 100).ToString("0.00")); }));
                            else PlayInfo_lblBPM.Text = string.Format("BPM : Calc: {0} %", (current / (float)total).ToString("0.00"));
                        }
                        catch (Exception ex) { }
                    });
                    bpm.Complete += ((object sender, double[] BPMList, double BPM) =>
                    {
                        try
                        {
                            if (PlayInfo_lblBPM.InvokeRequired)
                            PlayInfo_lblBPM.Invoke(new Action(() => { PlayInfo_lblBPM.Text = string.Format("BPM : {0}", BPM.ToString("0.00")); }));
                            else PlayInfo_lblBPM.Text = string.Format("BPM : {0}", BPM.ToString("0.00"));
                        }
                        catch (Exception ex) { }
                        HS_Library.HSConsole.WriteLine("Caculte BPM Complete [" + HS_CSharpUtility.Utility.ArrayUtility.ConvertArrayToString(BPMList, ", ") + " BPM]\n└ Path: " + Musicpath);
                    });
                    bpm.Start_Async();
                }
            }
            catch (Exception ex) {HS_Library.HSConsole.WriteLine("Caculate BPM Error!!\n└ Message: "+ex.Message+ "\n└ Stack: "+ex.StackTrace+"\n└ Path:" + Musicpath); }
        }

        private void backgroundWorker2_DoWork(object sender, System.ComponentModel.DoWorkEventArgs e)
        {

        }

        private void tabPage5_Click(object sender, EventArgs e)
        {

        }

        private void timer2_Tick(object sender, EventArgs e)
        {
            if (Helper.SoundFormat_Current != null)
            {
                PlayInfo_lblSoundFormat.Text = string.Format("사운드 포맷 : {0}", Helper.SoundFormat_Current.Format.ToString());
                PlayInfo_lblSoundType.Text = string.Format("사운드 타입 : {0}", Helper.SoundFormat_Current.SountType.ToString());
                PlayInfo_lblChannel.Text = string.Format("채널 : {0} 개", Helper.SoundFormat_Current.Channel.ToString());
                PlayInfo_lblBit.Text = string.Format("비트 : {0} bit", Helper.SoundFormat_Current.Bits.ToString());
            }
            else
            {
                PlayInfo_lblSoundFormat.Text = "사운드 포맷 : -";
                PlayInfo_lblSoundType.Text = "사운드 타입 : -";
                PlayInfo_lblChannel.Text = "채널 : -";
                PlayInfo_lblBit.Text = "비트 : -";
            }



            PlayInfo_lblSoundFormat_System.Text = string.Format("사운드 포맷        : {0}", Helper.DeviceFormat.SoundFormat.ToString());
            PlayInfo_lblSamplingMethod.Text = string.Format("리 샘플링 방법    : {0}", Helper.DeviceFormat.ResamplerMethod.ToString());
            PlayInfo_lblCurrentSampling_System.Text = string.Format("출력 샘플링        : {0} Hz", Helper.DeviceFormat.Samplerate == 0 ? "0" : Helper.DeviceFormat.Samplerate.ToString("##,###"));
            PlayInfo_lblMaxInputChannel.Text = string.Format("최대 입력가능 채널 : {0} 개", Helper.DeviceFormat.MaxInputChannel);
            PlayInfo_lblOutputChannel.Text = string.Format("출력 채널              : {0} 개", Helper.DeviceFormat.OutputChannel.ToString());
            PlayInfo_lblBit_System.Text = string.Format("비트                     : {0} bit", Helper.DeviceFormat.Bits.ToString());
            

            if (comboBox1.SelectedIndex == 2) Helper.channel.getMemoryInfo(MemoryInfo, Event_MemoryInfo, ref memoryused, ref detail);
            else if (comboBox1.SelectedIndex == 1) Helper.sound.getMemoryInfo(MemoryInfo, Event_MemoryInfo, ref memoryused, ref detail);
            else Helper.system.getMemoryInfo(MemoryInfo, Event_MemoryInfo, ref memoryused, ref detail);

            Inside_lblSoundSystemMemory.Text = string.Format("사운드 시스템 메모리 : {0}", memoryused == 0 ? "0" : memoryused.ToString("##,###"));
            Inside_lblSoundSystemSystem.Text = string.Format("시스템 : {0}", detail.system == 0 ? "0" : detail.system.ToString("##,###"));
            Inside_lblSoundSystemChannel.Text = string.Format("채널    : {0}", detail.channel == 0 ? "0" : detail.channel.ToString("##,###"));
            Inside_lblSoundSystemSound.Text = string.Format("소리    : {0}", detail.sound == 0 ? "0" : detail.sound.ToString("##,###"));
            Inside_lblSoundSystemDSP.Text = string.Format("DSP    : {0}", detail.dsp == 0 ? "0" : detail.dsp.ToString("##,###"));
            Inside_lblSoundSystemDSPConn.Text = string.Format("└ Connection : {0}", detail.dspconnection == 0 ? "0" : detail.dspconnection.ToString("##,###"));
            Inside_lblSoundSystemOutput.Text = string.Format("출력        : {0}", detail.output == 0 ? "0" : detail.output.ToString("##,###"));
            Inside_lblSoundSystemCodec.Text = string.Format("코덱        : {0}", detail.codec == 0 ? "0" : detail.codec.ToString("##,###"));
            Inside_lblSoundSystemReverb.Text = string.Format("반향 효과 : {0}", detail.reverb == 0 ? "0" : detail.reverb.ToString("##,###"));
            Inside_lblSoundSystemPlugin.Text = string.Format("플러그인  : {0}", detail.plugins == 0 ? "0" : detail.plugins.ToString("##,###"));
            Inside_lblSoundSystemFilebuff.Text = string.Format("파일 버퍼 : {0}", detail.file == 0 ? "0" : detail.file.ToString("##,###"));
            Inside_lblSoundSystemStringdata.Text = string.Format("문자열 데이터 : {0}", detail.stringdata == 0 ? "0" : detail.stringdata.ToString("##,###"));
            Inside_lblSoundSystemSoundgroup.Text = string.Format("사운드 그룹    : {0}", detail.soundgroup == 0 ? "0" : detail.soundgroup.ToString("##,###"));
        }

        uint MemoryInfo, Event_MemoryInfo;
        private void numericUpDown1_ValueChanged(object sender, EventArgs e)
        {
            MemoryInfo=(uint)numericUpDown2.Value;
            Event_MemoryInfo = (uint)numericUpDown1.Value;
        }

        private void toolStripTextBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == Convert.ToChar(Keys.Enter)) { toolStripTextBox1_TextChanged(null, null); }
            if (!(Char.IsDigit(e.KeyChar)) && e.KeyChar != Convert.ToChar(Keys.Back))
            { e.Handled = true; }
        }

        int a;
        private void toolStripTextBox1_TextChanged(object sender, EventArgs e)
        {
            if (toolStripTextBox1.Text == "") a = 1;
            if (int.Parse(toolStripTextBox1.Text) > 0) { a = int.Parse(toolStripTextBox1.Text); }
            else a = 1;
            this.timer1.Interval = a > 0 ? a : 1;
        }

        private void 닫기XToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void comboBox1_SelectedValueChanged(object sender, EventArgs e)
        {
            try
            {
                if (comboBox1.SelectedIndex == 3) Helper.dsp.GetDSP.getMemoryInfo(MemoryInfo, Event_MemoryInfo, ref memoryused, ref detail);
                else if (comboBox1.SelectedIndex == 2) Helper.channel.getMemoryInfo(MemoryInfo, Event_MemoryInfo, ref memoryused, ref detail);
                else if (comboBox1.SelectedIndex == 1) Helper.sound.getMemoryInfo(MemoryInfo, Event_MemoryInfo, ref memoryused, ref detail);
                else Helper.system.getMemoryInfo(MemoryInfo, Event_MemoryInfo, ref memoryused, ref detail);

                Inside_lblSoundSystemMemory.Text = string.Format("사운드 시스템 메모리 : {0}", memoryused == 0 ? "0" : memoryused.ToString("##,###"));
                Inside_lblSoundSystemSystem.Text = string.Format("시스템 : {0}", detail.system == 0 ? "0" : detail.system.ToString("##,###"));
                Inside_lblSoundSystemChannel.Text = string.Format("채널    : {0}", detail.channel == 0 ? "0" : detail.channel.ToString("##,###"));
                Inside_lblSoundSystemSound.Text = string.Format("소리    : {0}", detail.sound == 0 ? "0" : detail.sound.ToString("##,###"));
                Inside_lblSoundSystemDSP.Text = string.Format("DSP    : {0}", detail.dsp == 0 ? "0" : detail.dsp.ToString("##,###"));
                Inside_lblSoundSystemDSPConn.Text = string.Format("└ Connection : {0}", detail.dspconnection == 0 ? "0" : detail.dspconnection.ToString("##,###"));
                Inside_lblSoundSystemOutput.Text = string.Format("출력        : {0}", detail.output == 0 ? "0" : detail.output.ToString("##,###"));
                Inside_lblSoundSystemCodec.Text = string.Format("코덱        : {0}", detail.codec == 0 ? "0" : detail.codec.ToString("##,###"));
                Inside_lblSoundSystemReverb.Text = string.Format("반향 효과 : {0}", detail.reverb == 0 ? "0" : detail.reverb.ToString("##,###"));
                Inside_lblSoundSystemPlugin.Text = string.Format("플러그인  : {0}", detail.plugins == 0 ? "0" : detail.plugins.ToString("##,###"));
                Inside_lblSoundSystemFilebuff.Text = string.Format("파일 버퍼 : {0}", detail.file == 0 ? "0" : detail.file.ToString("##,###"));
                Inside_lblSoundSystemStringdata.Text = string.Format("문자열 데이터 : {0}", detail.stringdata == 0 ? "0" : detail.stringdata.ToString("##,###"));
                Inside_lblSoundSystemSoundgroup.Text = string.Format("사운드 그룹    : {0}", detail.soundgroup == 0 ? "0" : detail.soundgroup.ToString("##,###"));
            }catch{}
        }

        private void 새로고침ToolStripMenuItem_Click(object sender, EventArgs e)
        {

            Inside_lblProgramGCMemoryBefore.Text = string.Format("가비지 컬렉터 메모리(정리전) : {0}", GC.GetTotalMemory(false).ToString("00,000"));
            Inside_lblProgramGCMemoryAfter.Text = string.Format("가비지 컬렉터 메모리(정리후) : {0}", GC.GetTotalMemory(true).ToString("00,000"));
        }


        bool CurrentPreLoading;
        private void Helper_MusicChanged(HSAudioHelper Helper, int index, bool Error)
        {
            CurrentPreLoading = Helper.PreLoading;
            //this.MusicPath = MusicPath;
            if (TagThread != null) TagThread.Abort();
            tabPage3.Text = (Helper != null && !Helper.PreLoading) ? "음악 태그 (로딩중, 편집 불가능)" : "음악 태그 (로딩중)";
            if (음악태그비동기적으로로딩ToolStripMenuItem.Checked)
            {
                TagThread = new Thread(new ParameterizedThreadStart(UpdatgeTag)) {Name = "음악 태그 스레드"};
                TagThread.Start(Helper.MusicPath);// LoadMusicTag(Helper.MusicPath);
            }
            else
            {
                UpdatgeTag(MusicPath);
                if (Helper != null && !Helper.PreLoading)
                {
                    button1.Enabled = txtGenres.Enabled = comboBox3.Enabled = 이미지지우기ToolStripMenuItem.Enabled = 이미지불러오기ToolStripMenuItem.Enabled = 이미지저장ToolStripMenuItem.Enabled = 이미지복사ToolStripMenuItem.Enabled = 이미지추가ToolStripMenuItem.Enabled = false;
                    textBox1.ReadOnly = txtArtist.ReadOnly = txtAlbum.ReadOnly = txtAlbumArtist.ReadOnly = txtComposer.ReadOnly = txtYear.ReadOnly = txtTrack.ReadOnly = txtComment.ReadOnly = txtLyric.ReadOnly = true;
                    tabPage3.Text = "음악 태그 (편집 불가능)";
                }
            }

            TICK_Before = 0;
            if(th1!=null)th1.Abort();
            th1 = new System.Threading.Thread(new System.Threading.ThreadStart(CalulateBps));
            th1.Start();
        }

        internal void ClearTag(bool FullClear = true)
        {
            try
            {
                textBox1.Text = null;
                txtArtist.Text = null;
                txtAlbum.Text = null;
                txtAlbumArtist.Text = null;
                txtComment.Text = null;
                txtLyric.Text = null;
                txtGenres.Text = null;
                lblPictureSize.Text= "해상도: (알 수 없음)";
                lblPictureMimeType.Text = "사진 타입: (알 수 없음)";
                lblPictureLength.Text = "사진 크기: (알 수 없음)";
                label14.Text = "태그 버전: (알 수 없음)";
                label15.Text = "인코딩: (알 수 없음)";
                hScrollBar1.Maximum = 0;
                hScrollBar1.Enabled = comboBox3.Enabled = false;
                label13.Text = "0/0";
                comboBox3.Text = "";
                txtTrack.Text = null;
                txtYear.Text = null;
                FileInfo_lblBitrate.Text = "비트레이트 : - Kbps";
                try{Pic.Clear();}catch{Pic = new List<IPicture>();}
            }
            catch{}
            try{pictureBox1.Image.Dispose();pictureBox1.Image = null;}catch{}
            try{if (TAGFILE!=null&&FullClear) TAGFILE.Dispose();}catch{}
        }

        TagLib.Tag tag;
        TagLib.File TAGFILE;
        List<TagLib.IPicture> Pic = new List<IPicture>();
        string PreviousMusicPath;
        bool IsTagChanged;
        FileStream MainStream;
        //BinaryWriter WriteStream;
        FileStream ReaderStream;
        bool AutoSave = true;
        public void UpdatgeTag(object Path)
        {
            try
            {
                if (TAGFILE != null)
                {
                    DialogResult dr = DialogResult.No;
                    if (!AutoSave&&IsTagChanged) dr = MessageBox.Show(MusicPath + "\n의 태그가 변경 되었습니다. 저장 하시겠습니까?", "HS 플레이어 태그 정보 창", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning);
                    if (dr == System.Windows.Forms.DialogResult.Yes) button1_Click(false, null);
                    else if (dr == System.Windows.Forms.DialogResult.Cancel) return;
                }
            }
            catch(Exception ex)
            {
                if (MessageBox.Show("태그 저장중 오류가 발생 하였습니다. ("+ex.Message+")\n자세한 정보는 로그를 분석해 주세요!\n계속 진행 하시겠습니까?" , "HS 플레이어 태그 정보 창", MessageBoxButtons.YesNo, MessageBoxIcon.Error)
                    == System.Windows.Forms.DialogResult.No) return;
            }
            //try{MainStream.Dispose();}catch{}
            
            this.MusicPath = Path as string;
            ClearTag();
            try {MainStream = new FileStream(Path as string, FileMode.Open, FileAccess.Read, FileShare.Read);}
            catch
            {
                Thread.Sleep(3000); 
                MainStream = new FileStream(Path as string, FileMode.Open, FileAccess.Read, FileShare.Read);
            }
            //try{ReaderStream = new FileStream(Path, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);}catch{}
            try {
                TAGFILE = TagLib.File.Create(new StreamFileAbstraction(Path as string, MainStream, MainStream));
               
                MainStream.Close();
                tag = TAGFILE.Tag;
                if (tag != null)
                {
                    this.InvokeControl(() =>
                    {
                        button1.Enabled = txtGenres.Enabled = comboBox3.Enabled = 이미지지우기ToolStripMenuItem.Enabled = 이미지불러오기ToolStripMenuItem.Enabled = 이미지추가ToolStripMenuItem.Enabled = !(Helper != null && !Helper.PreLoading);
                        textBox1.ReadOnly = txtArtist.ReadOnly = txtAlbum.ReadOnly = txtAlbumArtist.ReadOnly = txtComposer.ReadOnly = txtYear.ReadOnly = txtTrack.ReadOnly = txtComment.ReadOnly = txtLyric.ReadOnly = (Helper != null && !Helper.PreLoading);
                        tabPage3.Text = (Helper != null && !Helper.PreLoading) ? "음악 태그 (편집 불가능)" : "음악 태그";
                        
                        textBox1.Text = tag.Title;
                        if (tag.Artists != null && tag.Artists.Length > 0) txtArtist.Text = tag.Artists[0];
                        txtAlbum.Text = tag.Album;
                        if (tag.AlbumArtists != null && tag.AlbumArtists.Length > 0) txtAlbumArtist.Text = tag.AlbumArtists[0];
                        txtComment.Text = tag.Comment;
                        txtLyric.Text = tag.Lyrics;
                        txtYear.Text = tag.Year == 0 ? null : tag.Year.ToString();
                        txtTrack.Text = tag.Track.ToString();
                        if (tag.Genres != null && tag.Genres.Length > 0) txtGenres.Text = tag.Genres[0];
                        Pic.AddRange(tag.Pictures);
                        hScrollBar1.Maximum = Pic.Count == 0 ? 0 : Pic.Count - 1;
                        label14.Text = "태그 버전: (알 수 없음)";
                        label15.Text = "인코딩: (알 수 없음)";
                        if (Pic.Count > 0)
                        {
                            hScrollBar1.Enabled = true;
                            hScrollBar1.Value = 0;
                            hScrollBar1_ValueChanged(null, null);
                        }
                        FileInfo_lblBitrate.Text = string.Format("비트레이트 : {0} Kbps", TAGFILE.Properties.AudioBitrate);
                        //MainStream.Seek(TAGFILE.Length, SeekOrigin.Begin);
                    });
                    IsTagChanged = false;
                }
            }
            catch { }
            //fs.Close();
            PreviousMusicPath = MusicPath;
        }

        MemoryStream ms = null;
        private void hScrollBar1_ValueChanged(object sender, EventArgs e)
        {
            try{
            if (ms != null) ms.Dispose();
            if (Pic.Count > 0)
            {
                label13.Text= string.Format("{0}/{1}", hScrollBar1.Value+1, Pic.Count);
                IPicture a = Pic[hScrollBar1.Value];
                byte[] DATA = null;
                if (a != null)
                {
                    if (pictureBox1.Image != null) pictureBox1.Image.Dispose();
                    pictureBox1.Image = null;
                    try
                    {
                        DATA = a.Data.Data;
                        ms = new MemoryStream(DATA);/*
                        unsafe
                        {
                            fixed (void* aa = DATA){
                                IntPtr ip = new IntPtr(aa);
                                System.Runtime.InteropServices.Marshal.Release(ip);}
                        }*/
                        pictureBox1.Image = System.Drawing.Bitmap.FromStream(ms);
                    }
                    catch{}
                    try{lblPictureSize.Text = string.Format("해상도: {0} X {1}", pictureBox1.Image.Width, pictureBox1.Image.Height);}catch {lblPictureSize.Text = "해상도: (알 수 없음)";}
                    try{lblPictureMimeType.Text = "사진 타입: "+a.MimeType;}catch{lblPictureMimeType.Text = "사진 타입: (알 수 없음)";}
                    try{lblPictureLength.Text = string.Format("사진 크기: {0} KB",(DATA.Length/1024).ToString("0.0"));}catch{lblPictureLength.Text = "사진 크기: (알 수 없음)";}
                   //if (Helper != null) 
                    comboBox3.Enabled = (pictureBox1.Image != null && Helper.PreLoading);
                    comboBox3.SelectedItem = a.Type.ToString();
                }}
            }
            catch
            {
            }
        }

        private void contextMenuStrip1_Opening(object sender, System.ComponentModel.CancelEventArgs e)
        {
            이미지복사ToolStripMenuItem.Enabled = 원본크기로보기ToolStripMenuItem.Enabled = 이미지저장ToolStripMenuItem.Enabled = pictureBox1.Image != null;
            //contextMenuStrip1.Enabled = pictureBox1.Image != null;
            이미지불러오기ToolStripMenuItem.Enabled = 이미지지우기ToolStripMenuItem.Enabled = (pictureBox1.Image != null&&CurrentPreLoading);
            이미지추가ToolStripMenuItem.Enabled = CurrentPreLoading;
        }

        private void 이미지복사ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Clipboard.SetImage(pictureBox1.Image);
        }

        string _MusicPath;
        string MusicPath
        {
            get{return _MusicPath;}
            set
            {
                _MusicPath = value;
                textBox2.Text = value;
            }
        }
        private void 이미지저장ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string Name = Path.GetFileNameWithoutExtension(MusicPath);
            string Dir = Path.GetDirectoryName(MusicPath);
            saveFileDialog1.InitialDirectory = Dir;

            Image img = pictureBox1.Image;

            saveFileDialog1.FileName = Name + "." + FindFormat(img.RawFormat);
            //MemoryStream ms = new MemoryStream();
            //img.Save(ms, img.RawFormat);
            //byte[] a = ms.ToArray();
            if(saveFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                try
                {
                    switch (saveFileDialog1.FilterIndex)
                    {
                        //case 1:img.Save(saveFileDialog1.FileName + "." + FindFormat(img.RawFormat)); break;
                        case 2: img.Save(saveFileDialog1.FileName, ImageFormat.Jpeg); break;
                        case 3: img.Save(saveFileDialog1.FileName, ImageFormat.Png); break;
                        case 4: img.Save(saveFileDialog1.FileName, ImageFormat.Bmp); break;
                        case 5: img.Save(saveFileDialog1.FileName, ImageFormat.Gif); break;
                        case 6: img.Save(saveFileDialog1.FileName, ImageFormat.Tiff); break;
                        case 7: img.Save(saveFileDialog1.FileName, ImageFormat.Wmf); break;
                        case 8: img.Save(saveFileDialog1.FileName, ImageFormat.Emf); break;
                        default: img.Save(saveFileDialog1.FileName); break;
                    }
                }
                catch(Exception ex){MessageBox.Show("파일 저장 실패!\n\n"+ex.Message, "HS 플레이어 시스템 정보", MessageBoxButtons.OK, MessageBoxIcon.Error);}
            }
        }

        public string FindFormat(ImageFormat Format)
        {
            if (Format.Guid.Equals(ImageFormat.Bmp.Guid)) return "bmp";
            if (Format.Guid.Equals(ImageFormat.Jpeg.Guid)) return "jpg";
            if (Format.Guid.Equals(ImageFormat.Png.Guid)) return "png";
            if (Format.Guid.Equals(ImageFormat.Gif.Guid)) return "gif";
            if (Format.Guid.Equals(ImageFormat.Tiff.Guid)) return "tif";
            if (Format.Guid.Equals(ImageFormat.Icon.Guid)) return "ico";
            if (Format.Guid.Equals(ImageFormat.Exif.Guid)) return "exif";
            if (Format.Guid.Equals(ImageFormat.Emf.Guid)) return "emf";
            else return null;
        }

        private void 이미지지우기ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("정말 이 사진을 지우시겠습니까?", "HS 플레이어 시스템 정보 - 음악 태그", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == System.Windows.Forms.DialogResult.Yes)
            {
                IPicture a = Pic[hScrollBar1.Value];
                Pic.Remove(a);
                tag.Pictures = Pic.ToArray();

                if (pictureBox1.Image != null) pictureBox1.Image.Dispose();
                pictureBox1.Image = null;

                hScrollBar1.Maximum = Pic.Count == 0 ? 0 : Pic.Count - 1;
                hScrollBar1.Value = hScrollBar1.Value == 0 ? 0 : hScrollBar1.Value - 1;

                if (Pic.Count == 0)
                {
                    comboBox3.Enabled = false;
                    lblPictureSize.Text = "해상도: (알 수 없음)";
                    lblPictureMimeType.Text = "사진 타입: (알 수 없음)";
                    lblPictureLength.Text = "사진 크기: (알 수 없음)";
                    label13.Text = "0/0";
                    hScrollBar1.Enabled = false;
                }
                //hScrollBar1_ValueChanged(null, null);
            }
        }

        string BeforeLyrics;
        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                //MessageBox.Show(bpm.IsComplete.ToString());
                if (bpm != null && !bpm.IsComplete)
                {
                    MessageBox.Show("BPM 계산이 진행중입니다.\n\n완료할때까지 기다리셨다가 다시 시도해주시기 바랍니다.", "HS 플레이어 태그 편집", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }
                if (BeforeLyrics != txtLyric.Text)
                {
                    tag.Lyrics = null;
                    SaveTag();
                    tag.Lyrics = txtLyric.Text;
                }
                SaveTag();
                BeforeLyrics = txtLyric.Text;
                bool tmp = true;
                try{tmp = (bool)sender;}catch{}
                if (tmp) MessageBox.Show("저장 완료!", "HS 플레이어 태그 정보 창", MessageBoxButtons.OK, MessageBoxIcon.Information);
                IsTagChanged = false;
            }
            /*
            catch(IOException ex)
            {
                
                DialogResult dr = MessageBox.Show("파일을 다른곳에서 사용중인 것 같습니다.\n\n강제로 해제하고 저장하시겠습니까?", "HS 플레이어 태그 정보 창", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                if(dr == DialogResult.Yes)
                {
                    //List<HSFileHandle.FileHandle> list = HSFileHandle.GetProcessesLockingFile(TAGFILE.file_abstraction.Name);
                }
                HS_CSharpUtility.LoggingUtility.Logging(ex);
            }*/
            catch (Exception ex)
            {
                MessageBox.Show("태그 저장중 오류가 발생 하였습니다.\n(" + ex.Message + ")\n\n자세한 정보는 로그를 분석해 주세요!", "HS 플레이어 태그 정보 창", MessageBoxButtons.OK, MessageBoxIcon.Error); 
                HS_CSharpUtility.LoggingUtility.Logging(ex);
            }
        }
        internal void SaveTag()
        {
            if (Pic != null && Pic.Count > 0) tag.Pictures = Pic.ToArray();
            //tag.Comment = txtComment.Text+" ";
            MainStream.Dispose();
            MainStream = new FileStream(MusicPath, FileMode.Open, FileAccess.ReadWrite, FileShare.ReadWrite);
            TAGFILE.RegesterWriteStream(MainStream);
            if (TAGFILE != null) TAGFILE.Save();
            MainStream.Close();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            tag.Title = textBox1.Text == "" ? null : textBox1.Text;
            IsTagChanged = true;
        }

        private void txtComment_TextChanged(object sender, EventArgs e)
        {
            tag.Comment = txtComment.Text==""?null:txtComment.Text;
            IsTagChanged = true;
        }

        private void txtArtist_TextChanged(object sender, EventArgs e)
        {
            tag.Artists = txtArtist.Text == "" ? null : new string[1] { txtArtist.Text };
            IsTagChanged = true;
        }

        private void comboBox3_SelectedIndexChanged(object sender, EventArgs e)
        {
            Pic[hScrollBar1.Value].Type = (PictureType)Enum.Parse(typeof(PictureType), comboBox3.Text);
            IsTagChanged = true;
        }

        private void txtAlbum_TextChanged(object sender, EventArgs e)
        {
            tag.Album = txtAlbum.Text == "" ? null : txtAlbum.Text;
            IsTagChanged = true;
        }
        private void txtComposer_TextChanged(object sender, EventArgs e)
        {
            tag.Composers = txtComposer.Text == "" ? null : new string[1] { txtComposer.Text };
            IsTagChanged = true;
        }
        private void txtAlbumArtist_TextChanged(object sender, EventArgs e)
        {
            tag.AlbumArtists = txtAlbumArtist.Text == "" ? null : new string[1] { txtAlbumArtist.Text };
            IsTagChanged = true;
        }

        string BeforeYear;
        private void txtYear_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (txtYear.Text != "" && txtYear.Text != null)
                {
                    tag.Year = Convert.ToUInt32(txtYear.Text); 
                    IsTagChanged = true;
                    BeforeYear = txtYear.Text;
                }
            } 
            catch(FormatException){MessageBox.Show("숫자만 입력할 수 있습니다.", "HS 플레이어 태그 정보 창", MessageBoxButtons.OK, MessageBoxIcon.Error);txtYear.Text = BeforeYear;}
        }

        private void txtYear_KeyPress(object sender, KeyPressEventArgs e)
        {
            //try{tag.Year = Convert.ToUInt32(txtYear.Text);e.Handled = true;}
            //catch(FormatException){e.Handled = false;MessageBox.Show("숫자만 입력할 수 있습니다.", "HS 플레이어 태그 정보 창", MessageBoxButtons.OK, MessageBoxIcon.Error);}
        }

        private void 음악태그자동저장ToolStripMenuItem_CheckedChanged(object sender, EventArgs e)
        {
            AutoSave = 음악태그자동저장ToolStripMenuItem.Checked;
        }

        string BeforeTreack;
        private void txtTrack_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (txtTrack.Text != "" && txtTrack.Text != null) tag.Track = Convert.ToUInt32(txtTrack.Text);
                IsTagChanged = true;
                BeforeTreack = txtTrack.Text;
            }
            catch (FormatException)
            {
                MessageBox.Show("숫자만 입력할 수 있습니다.", "HS 플레이어 태그 정보 창", MessageBoxButtons.OK, MessageBoxIcon.Error); txtTrack.Text = BeforeTreack;
            }
            
        }

        private void txtTrack_KeyPress(object sender, KeyPressEventArgs e)
        {
            //try{tag.Track = Convert.ToUInt32(txtTrack.Text);e.Handled = true;}
            //catch(FormatException){e.Handled = false;MessageBox.Show("숫자만 입력할 수 있습니다.", "HS 플레이어 태그 정보 창", MessageBoxButtons.OK, MessageBoxIcon.Error);}
        }

        private void txtLyric_TextChanged(object sender, EventArgs e)
        {
            tag.Lyrics = txtLyric.Text;
            IsTagChanged = true;
        }

        private void 이미지추가ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                Image img = Image.FromFile(openFileDialog1.FileName);
                if (img != null)
                {
                    MemoryStream ms = new MemoryStream();
                    img.Save(ms, img.RawFormat);
                    IPicture a = new TagLib.Picture(ms.ToArray()); ms.Dispose();
                    Pic.Add(a);a.Type = PictureType.FrontCover;
                    //if (pictureBox1 != null) pictureBox1.Dispose();
                    if (Pic.Count == 1) pictureBox1.Image = img;
                    hScrollBar1.Maximum = Pic.Count == 0 ? 0 : Pic.Count - 1;
                    hScrollBar1.Value = hScrollBar1.Maximum;
                    //hScrollBar1_ValueChanged(null, null);
                }
            }
        }

        private void 이미지불러오기ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                Image img = Image.FromFile(openFileDialog1.FileName);
                if (img != null)
                {
                    MemoryStream ms = new MemoryStream();
                    img.Save(ms, img.RawFormat);
                    Pic[hScrollBar1.Value].Data = new ByteVector(ms.ToArray());ms.Dispose();
                    if (pictureBox1 != null) pictureBox1.Image.Dispose();
                    pictureBox1.Image = img;
                }
            }
        }

        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            listView1.Items.Clear();
            if (FMODTAGSTRING!=null&&FMODTAGSTRING.Count>1) FMODTAGSTRING.Clear();

            Encoding enc = null;
            switch (comboBox2.SelectedIndex)
            {
                case 1: enc = Encoding.Default; break;
                case 2: enc = Encoding.UTF8; break;
                case 3: enc = Encoding.UTF32; break;
                case 4: enc = Encoding.Unicode; break;
                case 5: enc = Encoding.GetEncoding("euc-kr"); break;
                case 6: enc = Encoding.GetEncoding("euc-jp"); break;
            }

            HS_Audio_Lib.TAG tag = new HS_Audio_Lib.TAG();
            tag.datatype = HS_Audio_Lib.TAGDATATYPE.BINARY;
            //HS_Audio_Lib.RESULT result = Helper.sound.getTag(null, 0, ref tag);
            for (int i = 0; i < 1000; i++)
            {
                HS_Audio_Lib.RESULT result = Helper.sound.getTag(null, i, ref tag);
                unsafe
                {
                    byte* aa = (byte*)tag.data.ToPointer();
                    byte[] bb = new byte[tag.datalen];
                    for (int j = 0; j < tag.datalen; j++) bb[j] = aa[j];
                    string a1 = null;
                    //FMODTAGSTRING.Add(bb);
                    if (enc == null)
                    {
                        StringBuilder sb = null;
                        switch (comboBox2.SelectedIndex)
                        {
                            case 0:
                            switch (tag.datatype)
                            {
                                case HS_Audio_Lib.TAGDATATYPE.FLOAT: a1 = BitConverter.ToSingle(bb, 0).ToString();break;
                                case HS_Audio_Lib.TAGDATATYPE.INT: a1 = BitConverter.ToInt32(bb, 0).ToString(); break;
                                case HS_Audio_Lib.TAGDATATYPE.STRING: a1 = Encoding.Default.GetString(bb); break;
                                case HS_Audio_Lib.TAGDATATYPE.STRING_UTF8: a1 = Encoding.UTF8.GetString(bb); break;
                                case HS_Audio_Lib.TAGDATATYPE.STRING_UTF16: a1 = Encoding.Unicode.GetString(bb); break;
                                case HS_Audio_Lib.TAGDATATYPE.STRING_UTF16BE: a1 = Encoding.UTF32.GetString(bb); break;
                                default: sb = new StringBuilder(string.Format("{0:X2}", bb[0])); for (int j = 1; j < bb.Length; j++) sb.Append(string.Format(" {0:X2}", bb[j])); break;
                            }break;
                            case 7: sb = new StringBuilder(bb[0].ToString("00")); for (int j = 1; j < bb.Length; j++) sb.Append(bb[j].ToString("00")); break;
                            case 8: sb = new StringBuilder(bb[0].ToString("00")); for (int j = 1; j < bb.Length; j++) sb.Append(bb[j].ToString(" 00")); break;
                            case 9: sb = new StringBuilder(string.Format("{0:X2}", bb[0])); for (int j = 1; j < bb.Length; j++) sb.Append(string.Format("{0:X2}", bb[j])); break;
                            case 10: sb = new StringBuilder(string.Format("{0:X2}", bb[0])); for (int j = 1; j < bb.Length; j++) sb.Append(string.Format(" {0:X2}", bb[j])); break;
                        }
                        if(sb!=null)a1 = sb.ToString();
                    }
                    else a1 = enc.GetString(bb);//chkUTF8.Checked ? Encoding.UTF8.GetString(bb) : Encoding.GetEncoding(comboBox2.Text).GetString(bb);

                    ListViewItem li = new ListViewItem(tag.name);
                    string tmp = tag.type.ToString();
                    li.SubItems.Add(tmp.Substring(tmp.LastIndexOf(".") + 1));
                    li.SubItems.Add(tag.datatype.ToString());
                    li.SubItems.Add(tag.datalen.ToString("0,0"));
                    li.SubItems.Add(a1);
                    listView1.Items.Add(li);
                    //if (TAGFMOD.ContainsKey(tag.name))TAGFMOD[tag.name] = a1;
                    //else TAGFMOD.Add(tag.name, a1);
                }
                if (result != HS_Audio_Lib.RESULT.OK) break;
            }
        }

        private void 복사ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            StringBuilder sb = new StringBuilder(listView1.SelectedItems[0].SubItems[2].Text);
            for (int i = 1; i < listView1.SelectedItems.Count; i++) {sb.Append("\r\n"); sb.Append(listView1.SelectedItems[i].SubItems[2].Text);}
            string a = sb.ToString();
            string Path = Application.StartupPath + "\\HSPlayerInformation_TAGtmp.txt";
            try
            {
                System.IO.File.WriteAllText(Path, a);
                System.IO.File.SetAttributes(Path, FileAttributes.Hidden);
            }catch{}
            Clipboard.SetText(a, TextDataFormat.Text);
            string b = Clipboard.GetText();
        }

        private void contextMenuStrip2_Opening(object sender, System.ComponentModel.CancelEventArgs e)
        {
            contextMenuStrip2.Enabled = listView1.Items.Count>0;
        }
        
        HS_Audio.LIBRARY.SoundTouch.BpmDetectFloat bp;

        XPicbox.XtendPicBoxForm xp;
        ComboBox cb = null;
        int W, H;
        private void pictureBox1_Click(object sender, EventArgs e)
        {
            if (pictureBox1.Image == null) MessageBox.Show(LocalString.picTagPic_Message, LocalString.ProgramName + " " + LocalString.태그뷰어, MessageBoxButtons.OK, MessageBoxIcon.Warning);
            else
            {
                try
                {
                    xp = new XPicbox.XtendPicBoxForm();
                    xp.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
                    Panel pnl = new Panel();
                    pnl.Dock = DockStyle.Fill;
                    xp.Controls.Add(pnl);

                    xp._WidthBorder = xp.Width - pnl.Width;

                    #region Init xpForm
                    {
                        Label lb = new Label()
                        {
                            Text = "맞춤:",
                            Location = new Point(1, 5),
                            AutoSize = true
                        };
                        xp.Controls.Add(lb);

                        cb = new ComboBox()
                        {
                            Location = new Point(lb.Width + 2, 0),
                            Size = new Size(70, 20)
                        };
                        cb.Items.AddRange(new string[] { "없음", "맞추기", "늘이기", "가운데" });
                        xp.Controls.Add(cb);

                        xp._HeightBorder = (xp.Height - pnl.Height) + cb.Height;

                        Button btn = new Button()
                        {
                            Text = "조정",
                            Location = new Point(cb.Location.X + cb.Width + 2, 1),
                            Size = new Size(40, cb.Height)
                        };

                        xp.FormClosing += ((object sender1, FormClosingEventArgs ea) => { xp.innerPicture.Image.Dispose(); });
                        cb.SelectedIndexChanged += (object sender1, EventArgs e1) =>
                        {
                            try
                             {
                               XtendPicBoxForm xp1 = (XtendPicBoxForm)((ComboBox)sender).Parent;
                               //xp.innerPicture.Anchor = AnchorStyles.Left | AnchorStyles.Right | AnchorStyles.Top | AnchorStyles.Bottom;
                               //xp.MaximumSize = new Size(0, 0);
                               switch (((ComboBox)sender).SelectedIndex)
                               {
                                    case 1: xp1.innerPicture.SizeMode = PictureBoxSizeMode.Zoom; break;
                                    case 2: xp1.innerPicture.SizeMode = PictureBoxSizeMode.StretchImage; break;
                                    case 3: xp1.innerPicture.SizeMode = PictureBoxSizeMode.CenterImage; break;
                                    default:
                                    xp1.innerPicture.Size = new System.Drawing.Size(xp1.Image.Size.Width, xp1.Image.Size.Height);
                                    xp1.innerPicture.SizeMode = PictureBoxSizeMode.Normal;
                                    //xp.Size = new Size(xp.Image.Size.Width + xp.WidthBorder, xp.Image.Size.Height + xp.HeightBorder); 
                                    //xp.MaximumSize = this.Size;
                                    //xp.innerPicture.Anchor = AnchorStyles.Top|AnchorStyles.Left;
                                    break;
                                }
                                xp_SizeChanged(xp1, null);
                            }
                            catch { }
                        };//((object sender1, EventArgs ea)=> { MessageBox.Show("앙 기모띠~");});
                                    btn.Click += btn_Click;
                        xp.Load += xp_Load;

                        cb.DropDownStyle = ComboBoxStyle.DropDownList;
                        cb.SelectedIndex = 0;

                        xp.Controls.Add(btn);

                        xp.innerPicture.Location = new Point(0, cb.Height + 2);

                        //H += cb.Height + 2;
                    }
                    #endregion
                    /*
                int BorderWidth = (xp.Width - xp.ClientSize.Width) / 2;
                int TitlebarHeight = xp.Height - xp.ClientSize.Height - (2 * BorderWidth);*/
                    //HS_CSharpUtility.Utility.Win32Utility.RECT rec;
                    //HS_CSharpUtility.Utility.Win32Utility.GetWindowRect(xp.Handle, out rec);

                    pnl.Dispose(); pnl = null;

                    xp.Text = string.Format(LocalString.해상도 + ": {0} X {1} [{2}] - " + LocalString.ProgramName + " " + LocalString.태그뷰어, pictureBox1.Image.Width, pictureBox1.Image.Height,
                            (tag == null || tag.Title == null) ? Path.GetFileName(Helper.MusicPath) : tag.Title);
                    xp.innerPicture.MouseClick += xp_MouseClick;
                    xp.SizeChanged += xp_SizeChanged;
                    xp.Image = new Bitmap(pictureBox1.Image);

                    xp.innerPicture.Size = new System.Drawing.Size(xp.Image.Size.Width, xp.Image.Size.Height);
                    //xp.Size = new Size(xp.Image.Size.Width + W, xp.Image.Size.Height + H);//xp.innerPicture.Size;//new Size(picTagPic.Image.Size.Width + BorderWidth, picTagPic.Image.Size.Height + (BorderWidth + TitlebarHeight));
                    //xp.MaximumSize = xp.Size;
                    xp.Show();
                }
                catch(Exception ex)
                {
                    MessageBox.Show("내부 오류가 발생하여 사진을 표시하지 못했습니다.", LocalString.태그뷰어, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    HS_Library.HSConsole.WriteLine(ex.ToString()+"\n\n└ HS_Audio::frmSoundSystemInfo::pictureBox1_Click(object sender, EventArgs e)");
                    HS_CSharpUtility.LoggingUtility.Logging(ex, "HS_Audio::frmSoundSystemInfo::pictureBox1_Click(object sender, EventArgs e)");
                }
            }
        }

        #region XtendPicBoxForm 각종 이벤트
        private void xp_Load(object sender, EventArgs e)
        {
            btn_Click(sender, e);
        }

        private void xp_SizeChanged(object sender, EventArgs e)
        {
            try
            {
                XtendPicBoxForm xp = (XtendPicBoxForm)sender;
                if (xp.innerPicture.SizeMode != PictureBoxSizeMode.Normal)
                {
                    xp.innerPicture.Size = new System.Drawing.Size(xp.Width - xp.WidthBorder, xp.Height - xp.HeightBorder);

                    ComboBox cb = null;
                    foreach (System.Windows.Forms.Control a in xp.Controls) { try { cb = (ComboBox)a; break; } catch { } }
                    xp.innerPicture.Location = new Point(0, cb.Height + 2);
                }
            }
            catch { }
        }
        private void btn_Click(object sender, EventArgs e)
        {
            try
            {
                XtendPicBoxForm xp = null;
                try { xp = (XtendPicBoxForm)((Button)sender).Parent; } catch { xp = (XtendPicBoxForm)sender; }
                xp.innerPicture.Size = new System.Drawing.Size(xp.Image.Size.Width, xp.Image.Size.Height);
                xp.innerPicture.SizeMode = PictureBoxSizeMode.Normal;
                xp.Size = new Size(xp.Image.Size.Width + xp.WidthBorder, xp.Image.Size.Height + xp.HeightBorder);
            }
            catch { }
        }

        private void bPM자동업데이트ToolStripMenuItem_CheckedChanged(object sender, EventArgs e)
        {
            if (bpm != null) { bpm.Stop(); bpm.Dispose(); }
            PlayInfo_lblBPM.Text = "BPM: -";
            if (bPM자동업데이트ToolStripMenuItem.Checked) CaculateBPM(MusicPath, Helper);
            PlayInfo_lblBPM.Visible = bPM자동업데이트ToolStripMenuItem.Checked;
        }

        public void xp_MouseClick(object sender, EventArgs e)
        {
            try
            {
                XtendPicBoxForm xp = (XtendPicBoxForm)((PictureBox)sender).Parent;
                xp.Dispose();
            }
            catch { }
        }
        #endregion


        static int MaxBuffer = 2048;
        float[] LData = new float[MaxBuffer];
        float[] RData = new float[MaxBuffer];
        float[] LRData = new float[MaxBuffer];
        LIBRARY.SoundTouch.Utility.ArrayPtr<float> arrayptr;
        void CaculatorFileBPM()
        {
            arrayptr = new LIBRARY.SoundTouch.Utility.ArrayPtr<float>(LRData, 0);
            while (true)
            {
                try
                {
                    if (bp != null)
                    {
                        Helper.system.getWaveData(LData, LData.Length, 0);
                        Helper.system.getWaveData(RData, RData.Length, 1);

                        for(int i=0;i<LData.Length;i++)LRData[i] = (LData[i]+RData[i]) / 2f;
                        
                        arrayptr._buffer = LRData;
                        arrayptr._index = 0;

                        bp.InputSamples(arrayptr, LRData.Length-1);
                        Thread.Sleep(25);

                        PlayInfo_lblBPM.InvokeControl(()=>{PlayInfo_lblBPM.Text = "BPM: "+bp.GetBpm().ToString();});

                    }else Thread.Sleep(100);
                }
                catch{Thread.Sleep(100);}
            }
        }

        
    }
    public static class ControlExtensions
    {
        /// <summary>
        /// 컨트롤의 쓰레드 동기화 확장 입니다. (컨트롤 크로스 쓰레드 오류가 있을떄 사용하십시오)
        /// 사용법1 (람다식 사용): textBox1.InvokeifNeeded(() => textBox1.Text="문자열");
        /// 출처: http://www.devpia.com/Maeul/Contents/Detail.aspx?BoardID=18&MAEULNO=8&no=1723&page=12
        /// </summary>
        public static void InvokeControl(this System.Windows.Forms.Control control, Action action)
        {
            if (control.InvokeRequired)
                control.BeginInvoke(action);
            else
                action();
        }
    }
}
