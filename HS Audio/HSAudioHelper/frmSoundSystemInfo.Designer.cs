﻿namespace HS_Audio
{
    partial class frmSoundSystemInfo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            Helper.MusicChanged -= Helper_MusicPathChanged;
            bpm.Stop();
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            try { th.Abort(); }catch{}
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmSoundSystemInfo));
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.파일FToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.닫기XToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.설정ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.업데이트빈도ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripTextBox1 = new System.Windows.Forms.ToolStripTextBox();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.바이트표시ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripComboBox1 = new System.Windows.Forms.ToolStripComboBox();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.새로고침ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.음악태그자동업데이트ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.음악태그자동저장ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.음악태그비동기적으로로딩ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator8 = new System.Windows.Forms.ToolStripSeparator();
            this.bPM자동업데이트ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.보기VToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.PlayInfo_lblCPUGeometry = new System.Windows.Forms.Label();
            this.PlayInfo_lblCPUUpdate = new System.Windows.Forms.Label();
            this.PlayInfo_lblCPUSystem = new System.Windows.Forms.Label();
            this.PlayInfo_lblCPUDSP = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.PlayInfo_txtSoundSevice = new System.Windows.Forms.TextBox();
            this.PlayInfo_lblSoundSevice = new System.Windows.Forms.Label();
            this.PlayInfo_lblSoundOutput = new System.Windows.Forms.Label();
            this.PlayInfo_lblOutputChannel = new System.Windows.Forms.Label();
            this.PlayInfo_lblCurrentSampling_System = new System.Windows.Forms.Label();
            this.PlayInfo_lblBit_System = new System.Windows.Forms.Label();
            this.PlayInfo_lblMaxInputChannel = new System.Windows.Forms.Label();
            this.PlayInfo_lblSamplingMethod = new System.Windows.Forms.Label();
            this.PlayInfo_lblSoundFormat_System = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.PlayInfo_lblBPM = new System.Windows.Forms.Label();
            this.PlayInfo_lblCurrentSampling = new System.Windows.Forms.Label();
            this.PlayInfo_lblBit = new System.Windows.Forms.Label();
            this.PlayInfo_lblChannel = new System.Windows.Forms.Label();
            this.PlayInfo_lblSoundType = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.PlayInfo_lblSoundFormat = new System.Windows.Forms.Label();
            this.PlayInfo_lblTotalCPU = new System.Windows.Forms.Label();
            this.PlayInfo_lblTotalTime = new System.Windows.Forms.Label();
            this.PlayInfo_lblCurrentTime = new System.Windows.Forms.Label();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.numericUpDown1 = new System.Windows.Forms.NumericUpDown();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.Inside_lblProgramGCMemoryAfter = new System.Windows.Forms.Label();
            this.Inside_lblProgramPageMemory = new System.Windows.Forms.Label();
            this.Inside_lblProgramGCMemoryBefore = new System.Windows.Forms.Label();
            this.Inside_lblProgramRealMemory = new System.Windows.Forms.Label();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.numericUpDown2 = new System.Windows.Forms.NumericUpDown();
            this.Inside_lblSoundSystemSoundgroup = new System.Windows.Forms.Label();
            this.Inside_lblSoundSystemStringdata = new System.Windows.Forms.Label();
            this.Inside_lblSoundSystemFilebuff = new System.Windows.Forms.Label();
            this.Inside_lblSoundSystemDSPConn = new System.Windows.Forms.Label();
            this.Inside_lblSoundSystemDSP = new System.Windows.Forms.Label();
            this.Inside_lblSoundSystemReverb = new System.Windows.Forms.Label();
            this.Inside_lblSoundSystemCodec = new System.Windows.Forms.Label();
            this.Inside_lblSoundSystemOutput = new System.Windows.Forms.Label();
            this.Inside_lblSoundSystemPlugin = new System.Windows.Forms.Label();
            this.Inside_lblSoundSystemSound = new System.Windows.Forms.Label();
            this.Inside_lblSoundSystemChannel = new System.Windows.Forms.Label();
            this.Inside_lblSoundSystemSystem = new System.Windows.Forms.Label();
            this.Inside_lblSoundSystemMemory = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.FileInfo_lblBPM = new System.Windows.Forms.Label();
            this.FileInfo_lblOriginalSample = new System.Windows.Forms.Label();
            this.FileInfo_lblBit = new System.Windows.Forms.Label();
            this.FileInfo_lblChannel = new System.Windows.Forms.Label();
            this.FileInfo_lblSoundType = new System.Windows.Forms.Label();
            this.FileInfo_lblBitrate = new System.Windows.Forms.Label();
            this.FileInfo_lblSoundFormat = new System.Windows.Forms.Label();
            this.FileInfo_txtFileHash = new System.Windows.Forms.TextBox();
            this.FileInfo_lblFileHash = new System.Windows.Forms.Label();
            this.FileInfo_txtFilePath = new System.Windows.Forms.TextBox();
            this.FileInfo_lblFilePath = new System.Windows.Forms.Label();
            this.FileInfo_lblFileName = new System.Windows.Forms.Label();
            this.lblFileInfo_TotalTime = new System.Windows.Forms.Label();
            this.FileInfo_lblWaveSize = new System.Windows.Forms.Label();
            this.FileInfo_lblFileSize = new System.Windows.Forms.Label();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.label13 = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.label12 = new System.Windows.Forms.Label();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.lblPictureLength = new System.Windows.Forms.Label();
            this.lblPictureMimeType = new System.Windows.Forms.Label();
            this.lblPictureSize = new System.Windows.Forms.Label();
            this.hScrollBar1 = new System.Windows.Forms.HScrollBar();
            this.txtTrack = new System.Windows.Forms.TextBox();
            this.txtYear = new System.Windows.Forms.TextBox();
            this.txtComment = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.txtComposer = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.txtGenres = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtAlbumArtist = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.txtLyric = new System.Windows.Forms.TextBox();
            this.comboBox3 = new System.Windows.Forms.ComboBox();
            this.txtAlbum = new System.Windows.Forms.TextBox();
            this.txtArtist = new System.Windows.Forms.TextBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.원본크기로보기ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator7 = new System.Windows.Forms.ToolStripSeparator();
            this.이미지추가ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator6 = new System.Windows.Forms.ToolStripSeparator();
            this.이미지복사ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.이미지저장ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.이미지불러오기ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.이미지지우기ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.label16 = new System.Windows.Forms.Label();
            this.comboBox2 = new System.Windows.Forms.ComboBox();
            this.listView1 = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader5 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.contextMenuStrip2 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.복사ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.복사원래데이터ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.label15 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.timer2 = new System.Windows.Forms.Timer(this.components);
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.menuStrip1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.tabPage5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).BeginInit();
            this.groupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown2)).BeginInit();
            this.tabPage2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.tabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.contextMenuStrip1.SuspendLayout();
            this.tabPage4.SuspendLayout();
            this.contextMenuStrip2.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.파일FToolStripMenuItem,
            this.설정ToolStripMenuItem,
            this.보기VToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Padding = new System.Windows.Forms.Padding(6, 1, 0, 1);
            this.menuStrip1.Size = new System.Drawing.Size(615, 24);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // 파일FToolStripMenuItem
            // 
            this.파일FToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.닫기XToolStripMenuItem});
            this.파일FToolStripMenuItem.Name = "파일FToolStripMenuItem";
            this.파일FToolStripMenuItem.Size = new System.Drawing.Size(57, 22);
            this.파일FToolStripMenuItem.Text = "파일(&F)";
            // 
            // 닫기XToolStripMenuItem
            // 
            this.닫기XToolStripMenuItem.Name = "닫기XToolStripMenuItem";
            this.닫기XToolStripMenuItem.Size = new System.Drawing.Size(113, 22);
            this.닫기XToolStripMenuItem.Text = "닫기(&X)";
            this.닫기XToolStripMenuItem.Click += new System.EventHandler(this.닫기XToolStripMenuItem_Click);
            // 
            // 설정ToolStripMenuItem
            // 
            this.설정ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.업데이트빈도ToolStripMenuItem,
            this.toolStripSeparator1,
            this.바이트표시ToolStripMenuItem,
            this.toolStripSeparator2,
            this.새로고침ToolStripMenuItem,
            this.toolStripSeparator3,
            this.음악태그자동업데이트ToolStripMenuItem,
            this.음악태그자동저장ToolStripMenuItem,
            this.음악태그비동기적으로로딩ToolStripMenuItem,
            this.toolStripSeparator8,
            this.bPM자동업데이트ToolStripMenuItem});
            this.설정ToolStripMenuItem.Name = "설정ToolStripMenuItem";
            this.설정ToolStripMenuItem.Size = new System.Drawing.Size(58, 22);
            this.설정ToolStripMenuItem.Text = "설정(&T)";
            // 
            // 업데이트빈도ToolStripMenuItem
            // 
            this.업데이트빈도ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripTextBox1});
            this.업데이트빈도ToolStripMenuItem.Name = "업데이트빈도ToolStripMenuItem";
            this.업데이트빈도ToolStripMenuItem.Size = new System.Drawing.Size(227, 22);
            this.업데이트빈도ToolStripMenuItem.Text = "업데이트 빈도";
            // 
            // toolStripTextBox1
            // 
            this.toolStripTextBox1.Name = "toolStripTextBox1";
            this.toolStripTextBox1.Size = new System.Drawing.Size(100, 23);
            this.toolStripTextBox1.Text = "25";
            this.toolStripTextBox1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.toolStripTextBox1_KeyPress);
            this.toolStripTextBox1.TextChanged += new System.EventHandler(this.toolStripTextBox1_TextChanged);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(224, 6);
            this.toolStripSeparator1.Visible = false;
            // 
            // 바이트표시ToolStripMenuItem
            // 
            this.바이트표시ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripComboBox1});
            this.바이트표시ToolStripMenuItem.Name = "바이트표시ToolStripMenuItem";
            this.바이트표시ToolStripMenuItem.Size = new System.Drawing.Size(227, 22);
            this.바이트표시ToolStripMenuItem.Text = "바이트 표시";
            this.바이트표시ToolStripMenuItem.Visible = false;
            // 
            // toolStripComboBox1
            // 
            this.toolStripComboBox1.Name = "toolStripComboBox1";
            this.toolStripComboBox1.Size = new System.Drawing.Size(121, 23);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(224, 6);
            // 
            // 새로고침ToolStripMenuItem
            // 
            this.새로고침ToolStripMenuItem.Name = "새로고침ToolStripMenuItem";
            this.새로고침ToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F5;
            this.새로고침ToolStripMenuItem.Size = new System.Drawing.Size(227, 22);
            this.새로고침ToolStripMenuItem.Text = "가비지 컬렉터 새로 고침";
            this.새로고침ToolStripMenuItem.Click += new System.EventHandler(this.새로고침ToolStripMenuItem_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(224, 6);
            // 
            // 음악태그자동업데이트ToolStripMenuItem
            // 
            this.음악태그자동업데이트ToolStripMenuItem.Checked = true;
            this.음악태그자동업데이트ToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.음악태그자동업데이트ToolStripMenuItem.Name = "음악태그자동업데이트ToolStripMenuItem";
            this.음악태그자동업데이트ToolStripMenuItem.Size = new System.Drawing.Size(227, 22);
            this.음악태그자동업데이트ToolStripMenuItem.Text = "음악 태그 자동 업데이트";
            // 
            // 음악태그자동저장ToolStripMenuItem
            // 
            this.음악태그자동저장ToolStripMenuItem.Name = "음악태그자동저장ToolStripMenuItem";
            this.음악태그자동저장ToolStripMenuItem.Size = new System.Drawing.Size(227, 22);
            this.음악태그자동저장ToolStripMenuItem.Text = "음악 태그 자동 저장";
            this.음악태그자동저장ToolStripMenuItem.CheckedChanged += new System.EventHandler(this.음악태그자동저장ToolStripMenuItem_CheckedChanged);
            // 
            // 음악태그비동기적으로로딩ToolStripMenuItem
            // 
            this.음악태그비동기적으로로딩ToolStripMenuItem.Checked = true;
            this.음악태그비동기적으로로딩ToolStripMenuItem.CheckOnClick = true;
            this.음악태그비동기적으로로딩ToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.음악태그비동기적으로로딩ToolStripMenuItem.Name = "음악태그비동기적으로로딩ToolStripMenuItem";
            this.음악태그비동기적으로로딩ToolStripMenuItem.Size = new System.Drawing.Size(227, 22);
            this.음악태그비동기적으로로딩ToolStripMenuItem.Text = "음악 태그 비동기적으로 로딩";
            // 
            // toolStripSeparator8
            // 
            this.toolStripSeparator8.Name = "toolStripSeparator8";
            this.toolStripSeparator8.Size = new System.Drawing.Size(224, 6);
            // 
            // bPM자동업데이트ToolStripMenuItem
            // 
            this.bPM자동업데이트ToolStripMenuItem.Checked = true;
            this.bPM자동업데이트ToolStripMenuItem.CheckOnClick = true;
            this.bPM자동업데이트ToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.bPM자동업데이트ToolStripMenuItem.Name = "bPM자동업데이트ToolStripMenuItem";
            this.bPM자동업데이트ToolStripMenuItem.Size = new System.Drawing.Size(227, 22);
            this.bPM자동업데이트ToolStripMenuItem.Text = "BPM 표시";
            this.bPM자동업데이트ToolStripMenuItem.CheckedChanged += new System.EventHandler(this.bPM자동업데이트ToolStripMenuItem_CheckedChanged);
            // 
            // 보기VToolStripMenuItem
            // 
            this.보기VToolStripMenuItem.Name = "보기VToolStripMenuItem";
            this.보기VToolStripMenuItem.Size = new System.Drawing.Size(58, 22);
            this.보기VToolStripMenuItem.Text = "보기(&V)";
            this.보기VToolStripMenuItem.Visible = false;
            // 
            // statusStrip1
            // 
            this.statusStrip1.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1});
            this.statusStrip1.Location = new System.Drawing.Point(0, 401);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(615, 22);
            this.statusStrip1.TabIndex = 2;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(31, 17);
            this.toolStripStatusLabel1.Text = "준비";
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage5);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 24);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(615, 377);
            this.tabControl1.TabIndex = 3;
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.tabPage1.Controls.Add(this.groupBox4);
            this.tabPage1.Controls.Add(this.groupBox3);
            this.tabPage1.Controls.Add(this.groupBox2);
            this.tabPage1.Controls.Add(this.PlayInfo_lblTotalCPU);
            this.tabPage1.Controls.Add(this.PlayInfo_lblTotalTime);
            this.tabPage1.Controls.Add(this.PlayInfo_lblCurrentTime);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(607, 351);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "재생 정보";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.PlayInfo_lblCPUGeometry);
            this.groupBox4.Controls.Add(this.PlayInfo_lblCPUUpdate);
            this.groupBox4.Controls.Add(this.PlayInfo_lblCPUSystem);
            this.groupBox4.Controls.Add(this.PlayInfo_lblCPUDSP);
            this.groupBox4.ForeColor = System.Drawing.Color.Blue;
            this.groupBox4.Location = new System.Drawing.Point(349, 64);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(167, 105);
            this.groupBox4.TabIndex = 12;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "시스템 사용률 세부 정보";
            // 
            // PlayInfo_lblCPUGeometry
            // 
            this.PlayInfo_lblCPUGeometry.AutoSize = true;
            this.PlayInfo_lblCPUGeometry.ForeColor = System.Drawing.Color.Black;
            this.PlayInfo_lblCPUGeometry.Location = new System.Drawing.Point(4, 79);
            this.PlayInfo_lblCPUGeometry.Name = "PlayInfo_lblCPUGeometry";
            this.PlayInfo_lblCPUGeometry.Size = new System.Drawing.Size(122, 12);
            this.PlayInfo_lblCPUGeometry.TabIndex = 3;
            this.PlayInfo_lblCPUGeometry.Text = "Geometry : 100.000%";
            // 
            // PlayInfo_lblCPUUpdate
            // 
            this.PlayInfo_lblCPUUpdate.AutoSize = true;
            this.PlayInfo_lblCPUUpdate.ForeColor = System.Drawing.Color.Black;
            this.PlayInfo_lblCPUUpdate.Location = new System.Drawing.Point(3, 39);
            this.PlayInfo_lblCPUUpdate.Name = "PlayInfo_lblCPUUpdate";
            this.PlayInfo_lblCPUUpdate.Size = new System.Drawing.Size(123, 12);
            this.PlayInfo_lblCPUUpdate.TabIndex = 1;
            this.PlayInfo_lblCPUUpdate.Text = "업데이트   : 100.000%";
            // 
            // PlayInfo_lblCPUSystem
            // 
            this.PlayInfo_lblCPUSystem.AutoSize = true;
            this.PlayInfo_lblCPUSystem.ForeColor = System.Drawing.Color.Black;
            this.PlayInfo_lblCPUSystem.Location = new System.Drawing.Point(3, 17);
            this.PlayInfo_lblCPUSystem.Name = "PlayInfo_lblCPUSystem";
            this.PlayInfo_lblCPUSystem.Size = new System.Drawing.Size(123, 12);
            this.PlayInfo_lblCPUSystem.TabIndex = 0;
            this.PlayInfo_lblCPUSystem.Text = "스트림      : 100.000%";
            // 
            // PlayInfo_lblCPUDSP
            // 
            this.PlayInfo_lblCPUDSP.AutoSize = true;
            this.PlayInfo_lblCPUDSP.ForeColor = System.Drawing.Color.Black;
            this.PlayInfo_lblCPUDSP.Location = new System.Drawing.Point(3, 59);
            this.PlayInfo_lblCPUDSP.Name = "PlayInfo_lblCPUDSP";
            this.PlayInfo_lblCPUDSP.Size = new System.Drawing.Size(123, 12);
            this.PlayInfo_lblCPUDSP.TabIndex = 2;
            this.PlayInfo_lblCPUDSP.Text = "DSP         : 100.000%";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.PlayInfo_txtSoundSevice);
            this.groupBox3.Controls.Add(this.PlayInfo_lblSoundSevice);
            this.groupBox3.Controls.Add(this.PlayInfo_lblSoundOutput);
            this.groupBox3.Controls.Add(this.PlayInfo_lblOutputChannel);
            this.groupBox3.Controls.Add(this.PlayInfo_lblCurrentSampling_System);
            this.groupBox3.Controls.Add(this.PlayInfo_lblBit_System);
            this.groupBox3.Controls.Add(this.PlayInfo_lblMaxInputChannel);
            this.groupBox3.Controls.Add(this.PlayInfo_lblSamplingMethod);
            this.groupBox3.Controls.Add(this.PlayInfo_lblSoundFormat_System);
            this.groupBox3.ForeColor = System.Drawing.Color.Blue;
            this.groupBox3.Location = new System.Drawing.Point(3, 64);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(339, 95);
            this.groupBox3.TabIndex = 11;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "사운드 출력 세부 정보";
            // 
            // PlayInfo_txtSoundSevice
            // 
            this.PlayInfo_txtSoundSevice.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.PlayInfo_txtSoundSevice.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.PlayInfo_txtSoundSevice.Location = new System.Drawing.Point(105, 97);
            this.PlayInfo_txtSoundSevice.Name = "PlayInfo_txtSoundSevice";
            this.PlayInfo_txtSoundSevice.ReadOnly = true;
            this.PlayInfo_txtSoundSevice.Size = new System.Drawing.Size(228, 14);
            this.PlayInfo_txtSoundSevice.TabIndex = 9;
            this.PlayInfo_txtSoundSevice.Text = "(없음)";
            // 
            // PlayInfo_lblSoundSevice
            // 
            this.PlayInfo_lblSoundSevice.AutoSize = true;
            this.PlayInfo_lblSoundSevice.ForeColor = System.Drawing.Color.Black;
            this.PlayInfo_lblSoundSevice.Location = new System.Drawing.Point(3, 97);
            this.PlayInfo_lblSoundSevice.Name = "PlayInfo_lblSoundSevice";
            this.PlayInfo_lblSoundSevice.Size = new System.Drawing.Size(101, 12);
            this.PlayInfo_lblSoundSevice.TabIndex = 8;
            this.PlayInfo_lblSoundSevice.Text = "사운드 출력 장치:";
            // 
            // PlayInfo_lblSoundOutput
            // 
            this.PlayInfo_lblSoundOutput.AutoSize = true;
            this.PlayInfo_lblSoundOutput.ForeColor = System.Drawing.Color.Black;
            this.PlayInfo_lblSoundOutput.Location = new System.Drawing.Point(3, 77);
            this.PlayInfo_lblSoundOutput.Name = "PlayInfo_lblSoundOutput";
            this.PlayInfo_lblSoundOutput.Size = new System.Drawing.Size(171, 12);
            this.PlayInfo_lblSoundOutput.TabIndex = 7;
            this.PlayInfo_lblSoundOutput.Text = "사운드 출력 방법: (알 수 없음)";
            // 
            // PlayInfo_lblOutputChannel
            // 
            this.PlayInfo_lblOutputChannel.AutoSize = true;
            this.PlayInfo_lblOutputChannel.ForeColor = System.Drawing.Color.Black;
            this.PlayInfo_lblOutputChannel.Location = new System.Drawing.Point(182, 37);
            this.PlayInfo_lblOutputChannel.Name = "PlayInfo_lblOutputChannel";
            this.PlayInfo_lblOutputChannel.Size = new System.Drawing.Size(139, 12);
            this.PlayInfo_lblOutputChannel.TabIndex = 6;
            this.PlayInfo_lblOutputChannel.Text = "출력 채널             : 0 개";
            // 
            // PlayInfo_lblCurrentSampling_System
            // 
            this.PlayInfo_lblCurrentSampling_System.AutoSize = true;
            this.PlayInfo_lblCurrentSampling_System.ForeColor = System.Drawing.Color.Black;
            this.PlayInfo_lblCurrentSampling_System.Location = new System.Drawing.Point(3, 57);
            this.PlayInfo_lblCurrentSampling_System.Name = "PlayInfo_lblCurrentSampling_System";
            this.PlayInfo_lblCurrentSampling_System.Size = new System.Drawing.Size(130, 12);
            this.PlayInfo_lblCurrentSampling_System.TabIndex = 5;
            this.PlayInfo_lblCurrentSampling_System.Text = "출력 샘플링       : 0 Hz";
            // 
            // PlayInfo_lblBit_System
            // 
            this.PlayInfo_lblBit_System.AutoSize = true;
            this.PlayInfo_lblBit_System.ForeColor = System.Drawing.Color.Black;
            this.PlayInfo_lblBit_System.Location = new System.Drawing.Point(182, 57);
            this.PlayInfo_lblBit_System.Name = "PlayInfo_lblBit_System";
            this.PlayInfo_lblBit_System.Size = new System.Drawing.Size(140, 12);
            this.PlayInfo_lblBit_System.TabIndex = 4;
            this.PlayInfo_lblBit_System.Text = "비트                    : 0 bit";
            // 
            // PlayInfo_lblMaxInputChannel
            // 
            this.PlayInfo_lblMaxInputChannel.AutoSize = true;
            this.PlayInfo_lblMaxInputChannel.ForeColor = System.Drawing.Color.Black;
            this.PlayInfo_lblMaxInputChannel.Location = new System.Drawing.Point(182, 17);
            this.PlayInfo_lblMaxInputChannel.Name = "PlayInfo_lblMaxInputChannel";
            this.PlayInfo_lblMaxInputChannel.Size = new System.Drawing.Size(139, 12);
            this.PlayInfo_lblMaxInputChannel.TabIndex = 3;
            this.PlayInfo_lblMaxInputChannel.Text = "최대 입력가능 채널: 0 개";
            // 
            // PlayInfo_lblSamplingMethod
            // 
            this.PlayInfo_lblSamplingMethod.AutoSize = true;
            this.PlayInfo_lblSamplingMethod.ForeColor = System.Drawing.Color.Black;
            this.PlayInfo_lblSamplingMethod.Location = new System.Drawing.Point(3, 37);
            this.PlayInfo_lblSamplingMethod.Name = "PlayInfo_lblSamplingMethod";
            this.PlayInfo_lblSamplingMethod.Size = new System.Drawing.Size(148, 12);
            this.PlayInfo_lblSamplingMethod.TabIndex = 2;
            this.PlayInfo_lblSamplingMethod.Text = "리 샘플링 방법   : LINEAR";
            // 
            // PlayInfo_lblSoundFormat_System
            // 
            this.PlayInfo_lblSoundFormat_System.AutoSize = true;
            this.PlayInfo_lblSoundFormat_System.ForeColor = System.Drawing.Color.Black;
            this.PlayInfo_lblSoundFormat_System.Location = new System.Drawing.Point(3, 17);
            this.PlayInfo_lblSoundFormat_System.Name = "PlayInfo_lblSoundFormat_System";
            this.PlayInfo_lblSoundFormat_System.Size = new System.Drawing.Size(172, 12);
            this.PlayInfo_lblSoundFormat_System.TabIndex = 0;
            this.PlayInfo_lblSoundFormat_System.Text = "사운드 포맷       : PCMFLOAT";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.PlayInfo_lblBPM);
            this.groupBox2.Controls.Add(this.PlayInfo_lblCurrentSampling);
            this.groupBox2.Controls.Add(this.PlayInfo_lblBit);
            this.groupBox2.Controls.Add(this.PlayInfo_lblChannel);
            this.groupBox2.Controls.Add(this.PlayInfo_lblSoundType);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.PlayInfo_lblSoundFormat);
            this.groupBox2.ForeColor = System.Drawing.Color.Blue;
            this.groupBox2.Location = new System.Drawing.Point(3, 165);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(339, 98);
            this.groupBox2.TabIndex = 10;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "재생 세부 정보";
            // 
            // PlayInfo_lblBPM
            // 
            this.PlayInfo_lblBPM.AutoSize = true;
            this.PlayInfo_lblBPM.ForeColor = System.Drawing.Color.Black;
            this.PlayInfo_lblBPM.Location = new System.Drawing.Point(210, 57);
            this.PlayInfo_lblBPM.Name = "PlayInfo_lblBPM";
            this.PlayInfo_lblBPM.Size = new System.Drawing.Size(46, 12);
            this.PlayInfo_lblBPM.TabIndex = 6;
            this.PlayInfo_lblBPM.Text = "BPM: -";
            // 
            // PlayInfo_lblCurrentSampling
            // 
            this.PlayInfo_lblCurrentSampling.AutoSize = true;
            this.PlayInfo_lblCurrentSampling.ForeColor = System.Drawing.Color.Black;
            this.PlayInfo_lblCurrentSampling.Location = new System.Drawing.Point(7, 57);
            this.PlayInfo_lblCurrentSampling.Name = "PlayInfo_lblCurrentSampling";
            this.PlayInfo_lblCurrentSampling.Size = new System.Drawing.Size(102, 12);
            this.PlayInfo_lblCurrentSampling.TabIndex = 5;
            this.PlayInfo_lblCurrentSampling.Text = "현재 주파수: 0 Hz";
            // 
            // PlayInfo_lblBit
            // 
            this.PlayInfo_lblBit.AutoSize = true;
            this.PlayInfo_lblBit.ForeColor = System.Drawing.Color.Black;
            this.PlayInfo_lblBit.Location = new System.Drawing.Point(210, 37);
            this.PlayInfo_lblBit.Name = "PlayInfo_lblBit";
            this.PlayInfo_lblBit.Size = new System.Drawing.Size(60, 12);
            this.PlayInfo_lblBit.TabIndex = 4;
            this.PlayInfo_lblBit.Text = "비트: 0 bit";
            // 
            // PlayInfo_lblChannel
            // 
            this.PlayInfo_lblChannel.AutoSize = true;
            this.PlayInfo_lblChannel.ForeColor = System.Drawing.Color.Black;
            this.PlayInfo_lblChannel.Location = new System.Drawing.Point(210, 17);
            this.PlayInfo_lblChannel.Name = "PlayInfo_lblChannel";
            this.PlayInfo_lblChannel.Size = new System.Drawing.Size(59, 12);
            this.PlayInfo_lblChannel.TabIndex = 3;
            this.PlayInfo_lblChannel.Text = "채널: 0 개";
            // 
            // PlayInfo_lblSoundType
            // 
            this.PlayInfo_lblSoundType.AutoSize = true;
            this.PlayInfo_lblSoundType.ForeColor = System.Drawing.Color.Black;
            this.PlayInfo_lblSoundType.Location = new System.Drawing.Point(7, 37);
            this.PlayInfo_lblSoundType.Name = "PlayInfo_lblSoundType";
            this.PlayInfo_lblSoundType.Size = new System.Drawing.Size(102, 12);
            this.PlayInfo_lblSoundType.TabIndex = 2;
            this.PlayInfo_lblSoundType.Text = "사운드 타입: MP3";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(7, 76);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(129, 12);
            this.label5.TabIndex = 1;
            this.label5.Text = "비트 전송   : - KBits/s";
            // 
            // PlayInfo_lblSoundFormat
            // 
            this.PlayInfo_lblSoundFormat.AutoSize = true;
            this.PlayInfo_lblSoundFormat.ForeColor = System.Drawing.Color.Black;
            this.PlayInfo_lblSoundFormat.Location = new System.Drawing.Point(7, 17);
            this.PlayInfo_lblSoundFormat.Name = "PlayInfo_lblSoundFormat";
            this.PlayInfo_lblSoundFormat.Size = new System.Drawing.Size(102, 12);
            this.PlayInfo_lblSoundFormat.TabIndex = 0;
            this.PlayInfo_lblSoundFormat.Text = "사운드 포맷: MP3";
            // 
            // PlayInfo_lblTotalCPU
            // 
            this.PlayInfo_lblTotalCPU.AutoSize = true;
            this.PlayInfo_lblTotalCPU.Location = new System.Drawing.Point(5, 47);
            this.PlayInfo_lblTotalCPU.Name = "PlayInfo_lblTotalCPU";
            this.PlayInfo_lblTotalCPU.Size = new System.Drawing.Size(404, 12);
            this.PlayInfo_lblTotalCPU.TabIndex = 2;
            this.PlayInfo_lblTotalCPU.Text = "프로그램 CPU  : 0% / 0.00% [백분율: (시스템: 00.000%, IDLE: 00.000%)]";
            // 
            // PlayInfo_lblTotalTime
            // 
            this.PlayInfo_lblTotalTime.AutoSize = true;
            this.PlayInfo_lblTotalTime.Location = new System.Drawing.Point(6, 27);
            this.PlayInfo_lblTotalTime.Name = "PlayInfo_lblTotalTime";
            this.PlayInfo_lblTotalTime.Size = new System.Drawing.Size(321, 12);
            this.PlayInfo_lblTotalTime.TabIndex = 1;
            this.PlayInfo_lblTotalTime.Text = "총 재생 시간    : 00시 00분 00초 000 밀리초 [00:00:00.000]";
            // 
            // PlayInfo_lblCurrentTime
            // 
            this.PlayInfo_lblCurrentTime.AutoSize = true;
            this.PlayInfo_lblCurrentTime.Location = new System.Drawing.Point(6, 7);
            this.PlayInfo_lblCurrentTime.Name = "PlayInfo_lblCurrentTime";
            this.PlayInfo_lblCurrentTime.Size = new System.Drawing.Size(321, 12);
            this.PlayInfo_lblCurrentTime.TabIndex = 0;
            this.PlayInfo_lblCurrentTime.Text = "현재 재생 시간 : 00시 00분 00초 000 밀리초 [00:00:00.000]";
            // 
            // tabPage5
            // 
            this.tabPage5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.tabPage5.Controls.Add(this.numericUpDown1);
            this.tabPage5.Controls.Add(this.comboBox1);
            this.tabPage5.Controls.Add(this.Inside_lblProgramGCMemoryAfter);
            this.tabPage5.Controls.Add(this.Inside_lblProgramPageMemory);
            this.tabPage5.Controls.Add(this.Inside_lblProgramGCMemoryBefore);
            this.tabPage5.Controls.Add(this.Inside_lblProgramRealMemory);
            this.tabPage5.Controls.Add(this.groupBox5);
            this.tabPage5.Location = new System.Drawing.Point(4, 22);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Size = new System.Drawing.Size(607, 351);
            this.tabPage5.TabIndex = 4;
            this.tabPage5.Text = "내부 정보";
            this.tabPage5.Click += new System.EventHandler(this.tabPage5_Click);
            // 
            // numericUpDown1
            // 
            this.numericUpDown1.ForeColor = System.Drawing.Color.DimGray;
            this.numericUpDown1.Location = new System.Drawing.Point(323, 84);
            this.numericUpDown1.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.numericUpDown1.Name = "numericUpDown1";
            this.numericUpDown1.Size = new System.Drawing.Size(71, 21);
            this.numericUpDown1.TabIndex = 16;
            this.numericUpDown1.ValueChanged += new System.EventHandler(this.numericUpDown1_ValueChanged);
            // 
            // comboBox1
            // 
            this.comboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox1.Font = new System.Drawing.Font("Gulim", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Items.AddRange(new object[] {
            "시스템",
            "사운드",
            "채널",
            "DSP"});
            this.comboBox1.Location = new System.Drawing.Point(143, 85);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(66, 20);
            this.comboBox1.TabIndex = 4;
            this.comboBox1.SelectedValueChanged += new System.EventHandler(this.comboBox1_SelectedValueChanged);
            // 
            // Inside_lblProgramGCMemoryAfter
            // 
            this.Inside_lblProgramGCMemoryAfter.AutoSize = true;
            this.Inside_lblProgramGCMemoryAfter.ForeColor = System.Drawing.Color.Black;
            this.Inside_lblProgramGCMemoryAfter.Location = new System.Drawing.Point(1, 67);
            this.Inside_lblProgramGCMemoryAfter.Name = "Inside_lblProgramGCMemoryAfter";
            this.Inside_lblProgramGCMemoryAfter.Size = new System.Drawing.Size(185, 12);
            this.Inside_lblProgramGCMemoryAfter.TabIndex = 1;
            this.Inside_lblProgramGCMemoryAfter.Text = "가비지 컬렉터 메모리(정리후) : 0";
            // 
            // Inside_lblProgramPageMemory
            // 
            this.Inside_lblProgramPageMemory.AutoSize = true;
            this.Inside_lblProgramPageMemory.ForeColor = System.Drawing.Color.Black;
            this.Inside_lblProgramPageMemory.Location = new System.Drawing.Point(3, 25);
            this.Inside_lblProgramPageMemory.Name = "Inside_lblProgramPageMemory";
            this.Inside_lblProgramPageMemory.Size = new System.Drawing.Size(183, 12);
            this.Inside_lblProgramPageMemory.TabIndex = 2;
            this.Inside_lblProgramPageMemory.Text = "프로그램 페이지 메모리         : 0";
            // 
            // Inside_lblProgramGCMemoryBefore
            // 
            this.Inside_lblProgramGCMemoryBefore.AutoSize = true;
            this.Inside_lblProgramGCMemoryBefore.ForeColor = System.Drawing.Color.Black;
            this.Inside_lblProgramGCMemoryBefore.Location = new System.Drawing.Point(1, 47);
            this.Inside_lblProgramGCMemoryBefore.Name = "Inside_lblProgramGCMemoryBefore";
            this.Inside_lblProgramGCMemoryBefore.Size = new System.Drawing.Size(185, 12);
            this.Inside_lblProgramGCMemoryBefore.TabIndex = 0;
            this.Inside_lblProgramGCMemoryBefore.Text = "가비지 컬렉터 메모리(정리전) : 0";
            // 
            // Inside_lblProgramRealMemory
            // 
            this.Inside_lblProgramRealMemory.AutoSize = true;
            this.Inside_lblProgramRealMemory.ForeColor = System.Drawing.Color.Black;
            this.Inside_lblProgramRealMemory.Location = new System.Drawing.Point(3, 4);
            this.Inside_lblProgramRealMemory.Name = "Inside_lblProgramRealMemory";
            this.Inside_lblProgramRealMemory.Size = new System.Drawing.Size(183, 12);
            this.Inside_lblProgramRealMemory.TabIndex = 1;
            this.Inside_lblProgramRealMemory.Text = "프로그램 실제 메모리            : 0";
            // 
            // groupBox5
            // 
            this.groupBox5.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox5.Controls.Add(this.numericUpDown2);
            this.groupBox5.Controls.Add(this.Inside_lblSoundSystemSoundgroup);
            this.groupBox5.Controls.Add(this.Inside_lblSoundSystemStringdata);
            this.groupBox5.Controls.Add(this.Inside_lblSoundSystemFilebuff);
            this.groupBox5.Controls.Add(this.Inside_lblSoundSystemDSPConn);
            this.groupBox5.Controls.Add(this.Inside_lblSoundSystemDSP);
            this.groupBox5.Controls.Add(this.Inside_lblSoundSystemReverb);
            this.groupBox5.Controls.Add(this.Inside_lblSoundSystemCodec);
            this.groupBox5.Controls.Add(this.Inside_lblSoundSystemOutput);
            this.groupBox5.Controls.Add(this.Inside_lblSoundSystemPlugin);
            this.groupBox5.Controls.Add(this.Inside_lblSoundSystemSound);
            this.groupBox5.Controls.Add(this.Inside_lblSoundSystemChannel);
            this.groupBox5.Controls.Add(this.Inside_lblSoundSystemSystem);
            this.groupBox5.Controls.Add(this.Inside_lblSoundSystemMemory);
            this.groupBox5.ForeColor = System.Drawing.Color.Blue;
            this.groupBox5.Location = new System.Drawing.Point(0, 89);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(608, 152);
            this.groupBox5.TabIndex = 2;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "메모리 세부정보 (Byte):                  이벤트 메모리 비트:\n\n                                  " +
    "                           메모리 비트:";
            // 
            // numericUpDown2
            // 
            this.numericUpDown2.Location = new System.Drawing.Point(323, 20);
            this.numericUpDown2.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.numericUpDown2.Name = "numericUpDown2";
            this.numericUpDown2.Size = new System.Drawing.Size(71, 21);
            this.numericUpDown2.TabIndex = 17;
            this.numericUpDown2.Value = new decimal(new int[] {
            13,
            0,
            0,
            0});
            this.numericUpDown2.ValueChanged += new System.EventHandler(this.numericUpDown1_ValueChanged);
            // 
            // Inside_lblSoundSystemSoundgroup
            // 
            this.Inside_lblSoundSystemSoundgroup.AutoSize = true;
            this.Inside_lblSoundSystemSoundgroup.ForeColor = System.Drawing.Color.Black;
            this.Inside_lblSoundSystemSoundgroup.Location = new System.Drawing.Point(349, 69);
            this.Inside_lblSoundSystemSoundgroup.Name = "Inside_lblSoundSystemSoundgroup";
            this.Inside_lblSoundSystemSoundgroup.Size = new System.Drawing.Size(99, 12);
            this.Inside_lblSoundSystemSoundgroup.TabIndex = 15;
            this.Inside_lblSoundSystemSoundgroup.Text = "사운드 그룹    : 0";
            // 
            // Inside_lblSoundSystemStringdata
            // 
            this.Inside_lblSoundSystemStringdata.AutoSize = true;
            this.Inside_lblSoundSystemStringdata.ForeColor = System.Drawing.Color.Black;
            this.Inside_lblSoundSystemStringdata.Location = new System.Drawing.Point(349, 48);
            this.Inside_lblSoundSystemStringdata.Name = "Inside_lblSoundSystemStringdata";
            this.Inside_lblSoundSystemStringdata.Size = new System.Drawing.Size(99, 12);
            this.Inside_lblSoundSystemStringdata.TabIndex = 14;
            this.Inside_lblSoundSystemStringdata.Text = "문자열 데이터 : 0";
            // 
            // Inside_lblSoundSystemFilebuff
            // 
            this.Inside_lblSoundSystemFilebuff.AutoSize = true;
            this.Inside_lblSoundSystemFilebuff.ForeColor = System.Drawing.Color.Black;
            this.Inside_lblSoundSystemFilebuff.Location = new System.Drawing.Point(190, 131);
            this.Inside_lblSoundSystemFilebuff.Name = "Inside_lblSoundSystemFilebuff";
            this.Inside_lblSoundSystemFilebuff.Size = new System.Drawing.Size(75, 12);
            this.Inside_lblSoundSystemFilebuff.TabIndex = 13;
            this.Inside_lblSoundSystemFilebuff.Text = "파일 버퍼 : 0";
            // 
            // Inside_lblSoundSystemDSPConn
            // 
            this.Inside_lblSoundSystemDSPConn.AutoSize = true;
            this.Inside_lblSoundSystemDSPConn.ForeColor = System.Drawing.Color.Black;
            this.Inside_lblSoundSystemDSPConn.Location = new System.Drawing.Point(6, 131);
            this.Inside_lblSoundSystemDSPConn.Name = "Inside_lblSoundSystemDSPConn";
            this.Inside_lblSoundSystemDSPConn.Size = new System.Drawing.Size(103, 12);
            this.Inside_lblSoundSystemDSPConn.TabIndex = 12;
            this.Inside_lblSoundSystemDSPConn.Text = "└ Connection : 0";
            // 
            // Inside_lblSoundSystemDSP
            // 
            this.Inside_lblSoundSystemDSP.AutoSize = true;
            this.Inside_lblSoundSystemDSP.ForeColor = System.Drawing.Color.Black;
            this.Inside_lblSoundSystemDSP.Location = new System.Drawing.Point(6, 111);
            this.Inside_lblSoundSystemDSP.Name = "Inside_lblSoundSystemDSP";
            this.Inside_lblSoundSystemDSP.Size = new System.Drawing.Size(59, 12);
            this.Inside_lblSoundSystemDSP.TabIndex = 11;
            this.Inside_lblSoundSystemDSP.Text = "DSP    : 0";
            // 
            // Inside_lblSoundSystemReverb
            // 
            this.Inside_lblSoundSystemReverb.AutoSize = true;
            this.Inside_lblSoundSystemReverb.ForeColor = System.Drawing.Color.Black;
            this.Inside_lblSoundSystemReverb.Location = new System.Drawing.Point(190, 89);
            this.Inside_lblSoundSystemReverb.Name = "Inside_lblSoundSystemReverb";
            this.Inside_lblSoundSystemReverb.Size = new System.Drawing.Size(75, 12);
            this.Inside_lblSoundSystemReverb.TabIndex = 10;
            this.Inside_lblSoundSystemReverb.Text = "반향 효과 : 0";
            // 
            // Inside_lblSoundSystemCodec
            // 
            this.Inside_lblSoundSystemCodec.AutoSize = true;
            this.Inside_lblSoundSystemCodec.ForeColor = System.Drawing.Color.Black;
            this.Inside_lblSoundSystemCodec.Location = new System.Drawing.Point(190, 69);
            this.Inside_lblSoundSystemCodec.Name = "Inside_lblSoundSystemCodec";
            this.Inside_lblSoundSystemCodec.Size = new System.Drawing.Size(75, 12);
            this.Inside_lblSoundSystemCodec.TabIndex = 9;
            this.Inside_lblSoundSystemCodec.Text = "코덱        : 0";
            // 
            // Inside_lblSoundSystemOutput
            // 
            this.Inside_lblSoundSystemOutput.AutoSize = true;
            this.Inside_lblSoundSystemOutput.ForeColor = System.Drawing.Color.Black;
            this.Inside_lblSoundSystemOutput.Location = new System.Drawing.Point(190, 48);
            this.Inside_lblSoundSystemOutput.Name = "Inside_lblSoundSystemOutput";
            this.Inside_lblSoundSystemOutput.Size = new System.Drawing.Size(75, 12);
            this.Inside_lblSoundSystemOutput.TabIndex = 8;
            this.Inside_lblSoundSystemOutput.Text = "출력        : 0";
            // 
            // Inside_lblSoundSystemPlugin
            // 
            this.Inside_lblSoundSystemPlugin.AutoSize = true;
            this.Inside_lblSoundSystemPlugin.ForeColor = System.Drawing.Color.Black;
            this.Inside_lblSoundSystemPlugin.Location = new System.Drawing.Point(190, 111);
            this.Inside_lblSoundSystemPlugin.Name = "Inside_lblSoundSystemPlugin";
            this.Inside_lblSoundSystemPlugin.Size = new System.Drawing.Size(75, 12);
            this.Inside_lblSoundSystemPlugin.TabIndex = 7;
            this.Inside_lblSoundSystemPlugin.Text = "플러그인  : 0";
            // 
            // Inside_lblSoundSystemSound
            // 
            this.Inside_lblSoundSystemSound.AutoSize = true;
            this.Inside_lblSoundSystemSound.ForeColor = System.Drawing.Color.Black;
            this.Inside_lblSoundSystemSound.Location = new System.Drawing.Point(6, 89);
            this.Inside_lblSoundSystemSound.Name = "Inside_lblSoundSystemSound";
            this.Inside_lblSoundSystemSound.Size = new System.Drawing.Size(59, 12);
            this.Inside_lblSoundSystemSound.TabIndex = 6;
            this.Inside_lblSoundSystemSound.Text = "소리    : 0";
            // 
            // Inside_lblSoundSystemChannel
            // 
            this.Inside_lblSoundSystemChannel.AutoSize = true;
            this.Inside_lblSoundSystemChannel.ForeColor = System.Drawing.Color.Black;
            this.Inside_lblSoundSystemChannel.Location = new System.Drawing.Point(6, 69);
            this.Inside_lblSoundSystemChannel.Name = "Inside_lblSoundSystemChannel";
            this.Inside_lblSoundSystemChannel.Size = new System.Drawing.Size(59, 12);
            this.Inside_lblSoundSystemChannel.TabIndex = 5;
            this.Inside_lblSoundSystemChannel.Text = "채널    : 0";
            // 
            // Inside_lblSoundSystemSystem
            // 
            this.Inside_lblSoundSystemSystem.AutoSize = true;
            this.Inside_lblSoundSystemSystem.ForeColor = System.Drawing.Color.Black;
            this.Inside_lblSoundSystemSystem.Location = new System.Drawing.Point(6, 48);
            this.Inside_lblSoundSystemSystem.Name = "Inside_lblSoundSystemSystem";
            this.Inside_lblSoundSystemSystem.Size = new System.Drawing.Size(59, 12);
            this.Inside_lblSoundSystemSystem.TabIndex = 4;
            this.Inside_lblSoundSystemSystem.Text = "시스템 : 0";
            // 
            // Inside_lblSoundSystemMemory
            // 
            this.Inside_lblSoundSystemMemory.AutoSize = true;
            this.Inside_lblSoundSystemMemory.Font = new System.Drawing.Font("Gulim", 9.209303F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.Inside_lblSoundSystemMemory.ForeColor = System.Drawing.Color.Black;
            this.Inside_lblSoundSystemMemory.Location = new System.Drawing.Point(3, 23);
            this.Inside_lblSoundSystemMemory.Name = "Inside_lblSoundSystemMemory";
            this.Inside_lblSoundSystemMemory.Size = new System.Drawing.Size(166, 13);
            this.Inside_lblSoundSystemMemory.TabIndex = 3;
            this.Inside_lblSoundSystemMemory.Text = "사운드 시스템 메모리 : 0";
            // 
            // tabPage2
            // 
            this.tabPage2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.tabPage2.Controls.Add(this.groupBox1);
            this.tabPage2.Controls.Add(this.FileInfo_txtFileHash);
            this.tabPage2.Controls.Add(this.FileInfo_lblFileHash);
            this.tabPage2.Controls.Add(this.FileInfo_txtFilePath);
            this.tabPage2.Controls.Add(this.FileInfo_lblFilePath);
            this.tabPage2.Controls.Add(this.FileInfo_lblFileName);
            this.tabPage2.Controls.Add(this.lblFileInfo_TotalTime);
            this.tabPage2.Controls.Add(this.FileInfo_lblWaveSize);
            this.tabPage2.Controls.Add(this.FileInfo_lblFileSize);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(607, 351);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "파일 정보";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.FileInfo_lblBPM);
            this.groupBox1.Controls.Add(this.FileInfo_lblOriginalSample);
            this.groupBox1.Controls.Add(this.FileInfo_lblBit);
            this.groupBox1.Controls.Add(this.FileInfo_lblChannel);
            this.groupBox1.Controls.Add(this.FileInfo_lblSoundType);
            this.groupBox1.Controls.Add(this.FileInfo_lblBitrate);
            this.groupBox1.Controls.Add(this.FileInfo_lblSoundFormat);
            this.groupBox1.ForeColor = System.Drawing.Color.Blue;
            this.groupBox1.Location = new System.Drawing.Point(1, 133);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(400, 98);
            this.groupBox1.TabIndex = 9;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "세부 정보";
            // 
            // FileInfo_lblBPM
            // 
            this.FileInfo_lblBPM.AutoSize = true;
            this.FileInfo_lblBPM.ForeColor = System.Drawing.Color.Black;
            this.FileInfo_lblBPM.Location = new System.Drawing.Point(196, 57);
            this.FileInfo_lblBPM.Name = "FileInfo_lblBPM";
            this.FileInfo_lblBPM.Size = new System.Drawing.Size(46, 12);
            this.FileInfo_lblBPM.TabIndex = 6;
            this.FileInfo_lblBPM.Text = "BPM: -";
            // 
            // FileInfo_lblOriginalSample
            // 
            this.FileInfo_lblOriginalSample.AutoSize = true;
            this.FileInfo_lblOriginalSample.ForeColor = System.Drawing.Color.Black;
            this.FileInfo_lblOriginalSample.Location = new System.Drawing.Point(7, 57);
            this.FileInfo_lblOriginalSample.Name = "FileInfo_lblOriginalSample";
            this.FileInfo_lblOriginalSample.Size = new System.Drawing.Size(102, 12);
            this.FileInfo_lblOriginalSample.TabIndex = 5;
            this.FileInfo_lblOriginalSample.Text = "원본 샘플링: 0 Hz";
            // 
            // FileInfo_lblBit
            // 
            this.FileInfo_lblBit.AutoSize = true;
            this.FileInfo_lblBit.ForeColor = System.Drawing.Color.Black;
            this.FileInfo_lblBit.Location = new System.Drawing.Point(196, 37);
            this.FileInfo_lblBit.Name = "FileInfo_lblBit";
            this.FileInfo_lblBit.Size = new System.Drawing.Size(60, 12);
            this.FileInfo_lblBit.TabIndex = 4;
            this.FileInfo_lblBit.Text = "비트: 0 bit";
            // 
            // FileInfo_lblChannel
            // 
            this.FileInfo_lblChannel.AutoSize = true;
            this.FileInfo_lblChannel.ForeColor = System.Drawing.Color.Black;
            this.FileInfo_lblChannel.Location = new System.Drawing.Point(196, 17);
            this.FileInfo_lblChannel.Name = "FileInfo_lblChannel";
            this.FileInfo_lblChannel.Size = new System.Drawing.Size(59, 12);
            this.FileInfo_lblChannel.TabIndex = 3;
            this.FileInfo_lblChannel.Text = "채널: 0 개";
            // 
            // FileInfo_lblSoundType
            // 
            this.FileInfo_lblSoundType.AutoSize = true;
            this.FileInfo_lblSoundType.ForeColor = System.Drawing.Color.Black;
            this.FileInfo_lblSoundType.Location = new System.Drawing.Point(7, 37);
            this.FileInfo_lblSoundType.Name = "FileInfo_lblSoundType";
            this.FileInfo_lblSoundType.Size = new System.Drawing.Size(102, 12);
            this.FileInfo_lblSoundType.TabIndex = 2;
            this.FileInfo_lblSoundType.Text = "사운드 타입: MP3";
            // 
            // FileInfo_lblBitrate
            // 
            this.FileInfo_lblBitrate.AutoSize = true;
            this.FileInfo_lblBitrate.ForeColor = System.Drawing.Color.Black;
            this.FileInfo_lblBitrate.Location = new System.Drawing.Point(7, 77);
            this.FileInfo_lblBitrate.Name = "FileInfo_lblBitrate";
            this.FileInfo_lblBitrate.Size = new System.Drawing.Size(116, 12);
            this.FileInfo_lblBitrate.TabIndex = 1;
            this.FileInfo_lblBitrate.Text = "비트레이트 : - Kbps";
            // 
            // FileInfo_lblSoundFormat
            // 
            this.FileInfo_lblSoundFormat.AutoSize = true;
            this.FileInfo_lblSoundFormat.ForeColor = System.Drawing.Color.Black;
            this.FileInfo_lblSoundFormat.Location = new System.Drawing.Point(7, 17);
            this.FileInfo_lblSoundFormat.Name = "FileInfo_lblSoundFormat";
            this.FileInfo_lblSoundFormat.Size = new System.Drawing.Size(102, 12);
            this.FileInfo_lblSoundFormat.TabIndex = 0;
            this.FileInfo_lblSoundFormat.Text = "사운드 포맷: MP3";
            // 
            // FileInfo_txtFileHash
            // 
            this.FileInfo_txtFileHash.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.FileInfo_txtFileHash.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.FileInfo_txtFileHash.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.FileInfo_txtFileHash.Location = new System.Drawing.Point(71, 48);
            this.FileInfo_txtFileHash.Name = "FileInfo_txtFileHash";
            this.FileInfo_txtFileHash.Size = new System.Drawing.Size(529, 14);
            this.FileInfo_txtFileHash.TabIndex = 8;
            this.FileInfo_txtFileHash.Text = "(계산할 수 없습니다.)";
            // 
            // FileInfo_lblFileHash
            // 
            this.FileInfo_lblFileHash.AutoSize = true;
            this.FileInfo_lblFileHash.Location = new System.Drawing.Point(6, 48);
            this.FileInfo_lblFileHash.Name = "FileInfo_lblFileHash";
            this.FileInfo_lblFileHash.Size = new System.Drawing.Size(61, 12);
            this.FileInfo_lblFileHash.TabIndex = 7;
            this.FileInfo_lblFileHash.Text = "음악 해시:";
            // 
            // FileInfo_txtFilePath
            // 
            this.FileInfo_txtFilePath.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.FileInfo_txtFilePath.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.FileInfo_txtFilePath.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.FileInfo_txtFilePath.Location = new System.Drawing.Point(71, 27);
            this.FileInfo_txtFilePath.Name = "FileInfo_txtFilePath";
            this.FileInfo_txtFilePath.Size = new System.Drawing.Size(529, 14);
            this.FileInfo_txtFilePath.TabIndex = 6;
            // 
            // FileInfo_lblFilePath
            // 
            this.FileInfo_lblFilePath.AutoSize = true;
            this.FileInfo_lblFilePath.Location = new System.Drawing.Point(6, 27);
            this.FileInfo_lblFilePath.Name = "FileInfo_lblFilePath";
            this.FileInfo_lblFilePath.Size = new System.Drawing.Size(61, 12);
            this.FileInfo_lblFilePath.TabIndex = 5;
            this.FileInfo_lblFilePath.Text = "파일 경로:";
            // 
            // FileInfo_lblFileName
            // 
            this.FileInfo_lblFileName.AutoSize = true;
            this.FileInfo_lblFileName.Location = new System.Drawing.Point(6, 7);
            this.FileInfo_lblFileName.Name = "FileInfo_lblFileName";
            this.FileInfo_lblFileName.Size = new System.Drawing.Size(61, 12);
            this.FileInfo_lblFileName.TabIndex = 4;
            this.FileInfo_lblFileName.Text = "파일 이름:";
            // 
            // lblFileInfo_TotalTime
            // 
            this.lblFileInfo_TotalTime.AutoSize = true;
            this.lblFileInfo_TotalTime.Location = new System.Drawing.Point(6, 111);
            this.lblFileInfo_TotalTime.Name = "lblFileInfo_TotalTime";
            this.lblFileInfo_TotalTime.Size = new System.Drawing.Size(207, 12);
            this.lblFileInfo_TotalTime.TabIndex = 3;
            this.lblFileInfo_TotalTime.Text = "전체 시간: 00시 00분 00초 000 밀리초";
            // 
            // FileInfo_lblWaveSize
            // 
            this.FileInfo_lblWaveSize.AutoSize = true;
            this.FileInfo_lblWaveSize.Location = new System.Drawing.Point(6, 89);
            this.FileInfo_lblWaveSize.Name = "FileInfo_lblWaveSize";
            this.FileInfo_lblWaveSize.Size = new System.Drawing.Size(100, 12);
            this.FileInfo_lblWaveSize.TabIndex = 2;
            this.FileInfo_lblWaveSize.Text = "실제 크기: 0 Byte";
            // 
            // FileInfo_lblFileSize
            // 
            this.FileInfo_lblFileSize.AutoSize = true;
            this.FileInfo_lblFileSize.Location = new System.Drawing.Point(6, 69);
            this.FileInfo_lblFileSize.Name = "FileInfo_lblFileSize";
            this.FileInfo_lblFileSize.Size = new System.Drawing.Size(100, 12);
            this.FileInfo_lblFileSize.TabIndex = 1;
            this.FileInfo_lblFileSize.Text = "파일 크기: 0 Byte";
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.label13);
            this.tabPage3.Controls.Add(this.button2);
            this.tabPage3.Controls.Add(this.label12);
            this.tabPage3.Controls.Add(this.textBox2);
            this.tabPage3.Controls.Add(this.lblPictureLength);
            this.tabPage3.Controls.Add(this.lblPictureMimeType);
            this.tabPage3.Controls.Add(this.lblPictureSize);
            this.tabPage3.Controls.Add(this.hScrollBar1);
            this.tabPage3.Controls.Add(this.txtTrack);
            this.tabPage3.Controls.Add(this.txtYear);
            this.tabPage3.Controls.Add(this.txtComment);
            this.tabPage3.Controls.Add(this.label11);
            this.tabPage3.Controls.Add(this.txtComposer);
            this.tabPage3.Controls.Add(this.label10);
            this.tabPage3.Controls.Add(this.label9);
            this.tabPage3.Controls.Add(this.txtGenres);
            this.tabPage3.Controls.Add(this.label8);
            this.tabPage3.Controls.Add(this.txtAlbumArtist);
            this.tabPage3.Controls.Add(this.label7);
            this.tabPage3.Controls.Add(this.label6);
            this.tabPage3.Controls.Add(this.txtLyric);
            this.tabPage3.Controls.Add(this.comboBox3);
            this.tabPage3.Controls.Add(this.txtAlbum);
            this.tabPage3.Controls.Add(this.txtArtist);
            this.tabPage3.Controls.Add(this.textBox1);
            this.tabPage3.Controls.Add(this.button1);
            this.tabPage3.Controls.Add(this.label4);
            this.tabPage3.Controls.Add(this.label3);
            this.tabPage3.Controls.Add(this.label2);
            this.tabPage3.Controls.Add(this.label1);
            this.tabPage3.Controls.Add(this.pictureBox1);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(607, 351);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "음악 태그";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(157, 159);
            this.label13.Margin = new System.Windows.Forms.Padding(1, 0, 1, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(23, 12);
            this.label13.TabIndex = 36;
            this.label13.Text = "0/0";
            // 
            // button2
            // 
            this.button2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.button2.Location = new System.Drawing.Point(584, 329);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(25, 20);
            this.button2.TabIndex = 35;
            this.button2.Text = "...";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Visible = false;
            // 
            // label12
            // 
            this.label12.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(3, 332);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(61, 12);
            this.label12.TabIndex = 34;
            this.label12.Text = "파일 경로:";
            // 
            // textBox2
            // 
            this.textBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox2.BackColor = System.Drawing.SystemColors.Window;
            this.textBox2.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox2.Location = new System.Drawing.Point(71, 332);
            this.textBox2.Name = "textBox2";
            this.textBox2.ReadOnly = true;
            this.textBox2.Size = new System.Drawing.Size(515, 14);
            this.textBox2.TabIndex = 33;
            // 
            // lblPictureLength
            // 
            this.lblPictureLength.AutoSize = true;
            this.lblPictureLength.Location = new System.Drawing.Point(0, 223);
            this.lblPictureLength.Name = "lblPictureLength";
            this.lblPictureLength.Size = new System.Drawing.Size(131, 12);
            this.lblPictureLength.TabIndex = 32;
            this.lblPictureLength.Text = "사진 크기: (알 수 없음)";
            // 
            // lblPictureMimeType
            // 
            this.lblPictureMimeType.AutoSize = true;
            this.lblPictureMimeType.Location = new System.Drawing.Point(0, 204);
            this.lblPictureMimeType.Name = "lblPictureMimeType";
            this.lblPictureMimeType.Size = new System.Drawing.Size(131, 12);
            this.lblPictureMimeType.TabIndex = 31;
            this.lblPictureMimeType.Text = "사진 타입: (알 수 없음)";
            // 
            // lblPictureSize
            // 
            this.lblPictureSize.AutoSize = true;
            this.lblPictureSize.Location = new System.Drawing.Point(15, 184);
            this.lblPictureSize.Name = "lblPictureSize";
            this.lblPictureSize.Size = new System.Drawing.Size(115, 12);
            this.lblPictureSize.TabIndex = 30;
            this.lblPictureSize.Text = "해상도: (알 수 없음)";
            // 
            // hScrollBar1
            // 
            this.hScrollBar1.LargeChange = 1;
            this.hScrollBar1.Location = new System.Drawing.Point(155, 139);
            this.hScrollBar1.Name = "hScrollBar1";
            this.hScrollBar1.Size = new System.Drawing.Size(27, 14);
            this.hScrollBar1.TabIndex = 29;
            this.hScrollBar1.ValueChanged += new System.EventHandler(this.hScrollBar1_ValueChanged);
            // 
            // txtTrack
            // 
            this.txtTrack.Location = new System.Drawing.Point(358, 103);
            this.txtTrack.Name = "txtTrack";
            this.txtTrack.Size = new System.Drawing.Size(46, 21);
            this.txtTrack.TabIndex = 28;
            this.txtTrack.TextChanged += new System.EventHandler(this.txtTrack_TextChanged);
            this.txtTrack.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtTrack_KeyPress);
            // 
            // txtYear
            // 
            this.txtYear.Location = new System.Drawing.Point(251, 104);
            this.txtYear.Name = "txtYear";
            this.txtYear.Size = new System.Drawing.Size(67, 21);
            this.txtYear.TabIndex = 27;
            this.txtYear.TextChanged += new System.EventHandler(this.txtYear_TextChanged);
            this.txtYear.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtYear_KeyPress);
            // 
            // txtComment
            // 
            this.txtComment.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtComment.Location = new System.Drawing.Point(251, 129);
            this.txtComment.Multiline = true;
            this.txtComment.Name = "txtComment";
            this.txtComment.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtComment.Size = new System.Drawing.Size(354, 61);
            this.txtComment.TabIndex = 26;
            this.txtComment.TextChanged += new System.EventHandler(this.txtComment_TextChanged);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(213, 133);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(33, 12);
            this.label11.TabIndex = 25;
            this.label11.Text = "설명:";
            // 
            // txtComposer
            // 
            this.txtComposer.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtComposer.Location = new System.Drawing.Point(461, 79);
            this.txtComposer.Name = "txtComposer";
            this.txtComposer.Size = new System.Drawing.Size(143, 21);
            this.txtComposer.TabIndex = 23;
            this.txtComposer.TextChanged += new System.EventHandler(this.txtComposer_TextChanged);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(413, 81);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(45, 12);
            this.label10.TabIndex = 22;
            this.label10.Text = "작곡가:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(323, 108);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(33, 12);
            this.label9.TabIndex = 21;
            this.label9.Text = "트랙:";
            // 
            // txtGenres
            // 
            this.txtGenres.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtGenres.FormattingEnabled = true;
            this.txtGenres.Location = new System.Drawing.Point(441, 105);
            this.txtGenres.Name = "txtGenres";
            this.txtGenres.Size = new System.Drawing.Size(164, 20);
            this.txtGenres.Sorted = true;
            this.txtGenres.TabIndex = 20;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(213, 108);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(33, 12);
            this.label8.TabIndex = 18;
            this.label8.Text = "년도:";
            // 
            // txtAlbumArtist
            // 
            this.txtAlbumArtist.Location = new System.Drawing.Point(251, 79);
            this.txtAlbumArtist.Name = "txtAlbumArtist";
            this.txtAlbumArtist.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtAlbumArtist.Size = new System.Drawing.Size(157, 21);
            this.txtAlbumArtist.TabIndex = 17;
            this.txtAlbumArtist.WordWrap = false;
            this.txtAlbumArtist.TextChanged += new System.EventHandler(this.txtAlbumArtist_TextChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(162, 81);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(85, 12);
            this.label7.TabIndex = 16;
            this.label7.Text = "앨범 아티스트:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(185, 199);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(61, 12);
            this.label6.TabIndex = 15;
            this.label6.Text = "태그 가사:";
            // 
            // txtLyric
            // 
            this.txtLyric.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtLyric.Location = new System.Drawing.Point(251, 196);
            this.txtLyric.MaxLength = 100000;
            this.txtLyric.Multiline = true;
            this.txtLyric.Name = "txtLyric";
            this.txtLyric.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtLyric.Size = new System.Drawing.Size(354, 129);
            this.txtLyric.TabIndex = 13;
            this.txtLyric.TextChanged += new System.EventHandler(this.txtLyric_TextChanged);
            // 
            // comboBox3
            // 
            this.comboBox3.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox3.Enabled = false;
            this.comboBox3.FormattingEnabled = true;
            this.comboBox3.Location = new System.Drawing.Point(3, 157);
            this.comboBox3.Name = "comboBox3";
            this.comboBox3.Size = new System.Drawing.Size(150, 20);
            this.comboBox3.TabIndex = 12;
            this.comboBox3.SelectedIndexChanged += new System.EventHandler(this.comboBox3_SelectedIndexChanged);
            // 
            // txtAlbum
            // 
            this.txtAlbum.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtAlbum.Location = new System.Drawing.Point(251, 55);
            this.txtAlbum.Name = "txtAlbum";
            this.txtAlbum.Size = new System.Drawing.Size(353, 21);
            this.txtAlbum.TabIndex = 10;
            this.txtAlbum.TextChanged += new System.EventHandler(this.txtAlbum_TextChanged);
            // 
            // txtArtist
            // 
            this.txtArtist.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtArtist.Location = new System.Drawing.Point(251, 31);
            this.txtArtist.Name = "txtArtist";
            this.txtArtist.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtArtist.Size = new System.Drawing.Size(353, 21);
            this.txtArtist.TabIndex = 9;
            this.txtArtist.WordWrap = false;
            this.txtArtist.TextChanged += new System.EventHandler(this.txtArtist_TextChanged);
            // 
            // textBox1
            // 
            this.textBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox1.Location = new System.Drawing.Point(251, 7);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(353, 21);
            this.textBox1.TabIndex = 8;
            this.textBox1.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // button1
            // 
            this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.button1.Location = new System.Drawing.Point(3, 301);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(241, 23);
            this.button1.TabIndex = 7;
            this.button1.Text = "저장";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(405, 108);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(33, 12);
            this.label4.TabIndex = 5;
            this.label4.Text = "장르:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(214, 57);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(33, 12);
            this.label3.TabIndex = 4;
            this.label3.Text = "앨범:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(189, 33);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(57, 12);
            this.label2.TabIndex = 3;
            this.label2.Text = "아티스트:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(214, 11);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(33, 12);
            this.label1.TabIndex = 2;
            this.label1.Text = "제목:";
            // 
            // pictureBox1
            // 
            this.pictureBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox1.ContextMenuStrip = this.contextMenuStrip1;
            this.pictureBox1.Location = new System.Drawing.Point(3, 3);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(150, 150);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 1;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.원본크기로보기ToolStripMenuItem,
            this.toolStripSeparator7,
            this.이미지추가ToolStripMenuItem,
            this.toolStripSeparator6,
            this.이미지복사ToolStripMenuItem,
            this.toolStripSeparator5,
            this.이미지저장ToolStripMenuItem,
            this.이미지불러오기ToolStripMenuItem,
            this.toolStripSeparator4,
            this.이미지지우기ToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(162, 160);
            this.contextMenuStrip1.Opening += new System.ComponentModel.CancelEventHandler(this.contextMenuStrip1_Opening);
            // 
            // 원본크기로보기ToolStripMenuItem
            // 
            this.원본크기로보기ToolStripMenuItem.Name = "원본크기로보기ToolStripMenuItem";
            this.원본크기로보기ToolStripMenuItem.Size = new System.Drawing.Size(161, 22);
            this.원본크기로보기ToolStripMenuItem.Text = "이미지 보기";
            this.원본크기로보기ToolStripMenuItem.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // toolStripSeparator7
            // 
            this.toolStripSeparator7.Name = "toolStripSeparator7";
            this.toolStripSeparator7.Size = new System.Drawing.Size(158, 6);
            // 
            // 이미지추가ToolStripMenuItem
            // 
            this.이미지추가ToolStripMenuItem.Name = "이미지추가ToolStripMenuItem";
            this.이미지추가ToolStripMenuItem.Size = new System.Drawing.Size(161, 22);
            this.이미지추가ToolStripMenuItem.Text = "이미지 추가";
            this.이미지추가ToolStripMenuItem.Click += new System.EventHandler(this.이미지추가ToolStripMenuItem_Click);
            // 
            // toolStripSeparator6
            // 
            this.toolStripSeparator6.Name = "toolStripSeparator6";
            this.toolStripSeparator6.Size = new System.Drawing.Size(158, 6);
            // 
            // 이미지복사ToolStripMenuItem
            // 
            this.이미지복사ToolStripMenuItem.Name = "이미지복사ToolStripMenuItem";
            this.이미지복사ToolStripMenuItem.Size = new System.Drawing.Size(161, 22);
            this.이미지복사ToolStripMenuItem.Text = "이미지 복사";
            this.이미지복사ToolStripMenuItem.Click += new System.EventHandler(this.이미지복사ToolStripMenuItem_Click);
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(158, 6);
            // 
            // 이미지저장ToolStripMenuItem
            // 
            this.이미지저장ToolStripMenuItem.Name = "이미지저장ToolStripMenuItem";
            this.이미지저장ToolStripMenuItem.Size = new System.Drawing.Size(161, 22);
            this.이미지저장ToolStripMenuItem.Text = "이미지 저장";
            this.이미지저장ToolStripMenuItem.Click += new System.EventHandler(this.이미지저장ToolStripMenuItem_Click);
            // 
            // 이미지불러오기ToolStripMenuItem
            // 
            this.이미지불러오기ToolStripMenuItem.Name = "이미지불러오기ToolStripMenuItem";
            this.이미지불러오기ToolStripMenuItem.Size = new System.Drawing.Size(161, 22);
            this.이미지불러오기ToolStripMenuItem.Text = "이미지 불러오기";
            this.이미지불러오기ToolStripMenuItem.Click += new System.EventHandler(this.이미지불러오기ToolStripMenuItem_Click);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(158, 6);
            // 
            // 이미지지우기ToolStripMenuItem
            // 
            this.이미지지우기ToolStripMenuItem.Name = "이미지지우기ToolStripMenuItem";
            this.이미지지우기ToolStripMenuItem.Size = new System.Drawing.Size(161, 22);
            this.이미지지우기ToolStripMenuItem.Text = "이미지 지우기";
            this.이미지지우기ToolStripMenuItem.Click += new System.EventHandler(this.이미지지우기ToolStripMenuItem_Click);
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.label16);
            this.tabPage4.Controls.Add(this.comboBox2);
            this.tabPage4.Controls.Add(this.listView1);
            this.tabPage4.Controls.Add(this.label15);
            this.tabPage4.Controls.Add(this.label14);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Size = new System.Drawing.Size(607, 351);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "정보 (구현 안됨)";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(4, 60);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(41, 12);
            this.label16.TabIndex = 46;
            this.label16.Text = "인코딩";
            // 
            // comboBox2
            // 
            this.comboBox2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox2.FormattingEnabled = true;
            this.comboBox2.Items.AddRange(new object[] {
            "자동 인코딩",
            "ANSI",
            "UTF-8",
            "UTF-32",
            "UTF-16 (유니코드)",
            "한국어 (ECU-KR)",
            "일본어 (ECU-JP)",
            "바이너리",
            "바이너리 (공백)",
            "바이너리 (HEX)",
            "바이너리 (공백, HEX)"});
            this.comboBox2.Location = new System.Drawing.Point(51, 56);
            this.comboBox2.Name = "comboBox2";
            this.comboBox2.Size = new System.Drawing.Size(145, 20);
            this.comboBox2.TabIndex = 43;
            this.comboBox2.SelectedIndexChanged += new System.EventHandler(this.comboBox2_SelectedIndexChanged);
            // 
            // listView1
            // 
            this.listView1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.listView1.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2,
            this.columnHeader4,
            this.columnHeader5,
            this.columnHeader3});
            this.listView1.ContextMenuStrip = this.contextMenuStrip2;
            this.listView1.FullRowSelect = true;
            this.listView1.Location = new System.Drawing.Point(2, 78);
            this.listView1.Margin = new System.Windows.Forms.Padding(2);
            this.listView1.Name = "listView1";
            this.listView1.Size = new System.Drawing.Size(605, 273);
            this.listView1.TabIndex = 41;
            this.listView1.UseCompatibleStateImageBehavior = false;
            this.listView1.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "태그 이름";
            this.columnHeader1.Width = 80;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "태그 유형";
            this.columnHeader2.Width = 70;
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "형식";
            this.columnHeader4.Width = 100;
            // 
            // columnHeader5
            // 
            this.columnHeader5.Text = "크기";
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "태그 데이터";
            this.columnHeader3.Width = 500;
            // 
            // contextMenuStrip2
            // 
            this.contextMenuStrip2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.복사ToolStripMenuItem,
            this.복사원래데이터ToolStripMenuItem});
            this.contextMenuStrip2.Name = "contextMenuStrip2";
            this.contextMenuStrip2.Size = new System.Drawing.Size(173, 48);
            this.contextMenuStrip2.Opening += new System.ComponentModel.CancelEventHandler(this.contextMenuStrip2_Opening);
            // 
            // 복사ToolStripMenuItem
            // 
            this.복사ToolStripMenuItem.Name = "복사ToolStripMenuItem";
            this.복사ToolStripMenuItem.Size = new System.Drawing.Size(172, 22);
            this.복사ToolStripMenuItem.Text = "복사";
            this.복사ToolStripMenuItem.Click += new System.EventHandler(this.복사ToolStripMenuItem_Click);
            // 
            // 복사원래데이터ToolStripMenuItem
            // 
            this.복사원래데이터ToolStripMenuItem.Name = "복사원래데이터ToolStripMenuItem";
            this.복사원래데이터ToolStripMenuItem.Size = new System.Drawing.Size(172, 22);
            this.복사원래데이터ToolStripMenuItem.Text = "복사 (원래 데이터)";
            this.복사원래데이터ToolStripMenuItem.Visible = false;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(22, 27);
            this.label15.Margin = new System.Windows.Forms.Padding(1, 0, 1, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(115, 12);
            this.label15.TabIndex = 40;
            this.label15.Text = "인코딩: (알 수 없음)";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(6, 7);
            this.label14.Margin = new System.Windows.Forms.Padding(1, 0, 1, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(131, 12);
            this.label14.TabIndex = 39;
            this.label14.Text = "태그 버전: (알 수 없음)";
            // 
            // timer1
            // 
            this.timer1.Interval = 25;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // timer2
            // 
            this.timer2.Interval = 200;
            this.timer2.Tick += new System.EventHandler(this.timer2_Tick);
            // 
            // imageList1
            // 
            this.imageList1.ColorDepth = System.Windows.Forms.ColorDepth.Depth16Bit;
            this.imageList1.ImageSize = new System.Drawing.Size(16, 16);
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            // 
            // saveFileDialog1
            // 
            this.saveFileDialog1.Filter = resources.GetString("saveFileDialog1.Filter");
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.Filter = "JPEG 파일 (*.jpg, *.jpeg)|*.jpg;*.jpeg|PNG 파일 (*.png)|*.png|비트맵 파일 (*.bmp)|*.bmp|GI" +
    "F 파일 (*.gif)|*.gif";
            // 
            // frmSoundSystemInfo
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(615, 423);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.MinimumSize = new System.Drawing.Size(506, 335);
            this.Name = "frmSoundSystemInfo";
            this.Text = "HS™ 플레이어 시스템 상세 정보";
            this.Load += new System.EventHandler(this.frmSoundSystemInfo_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.tabPage5.ResumeLayout(false);
            this.tabPage5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).EndInit();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown2)).EndInit();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.contextMenuStrip1.ResumeLayout(false);
            this.tabPage4.ResumeLayout(false);
            this.tabPage4.PerformLayout();
            this.contextMenuStrip2.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem 파일FToolStripMenuItem;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.ToolStripMenuItem 설정ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 업데이트빈도ToolStripMenuItem;
        private System.Windows.Forms.Label PlayInfo_lblCurrentTime;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label FileInfo_lblSoundType;
        private System.Windows.Forms.Label FileInfo_lblBitrate;
        private System.Windows.Forms.Label FileInfo_lblSoundFormat;
        private System.Windows.Forms.TextBox FileInfo_txtFileHash;
        private System.Windows.Forms.Label FileInfo_lblFileHash;
        private System.Windows.Forms.TextBox FileInfo_txtFilePath;
        private System.Windows.Forms.Label FileInfo_lblFilePath;
        private System.Windows.Forms.Label FileInfo_lblFileName;
        private System.Windows.Forms.Label lblFileInfo_TotalTime;
        private System.Windows.Forms.Label FileInfo_lblWaveSize;
        private System.Windows.Forms.Label FileInfo_lblFileSize;
        private System.Windows.Forms.Label FileInfo_lblChannel;
        private System.Windows.Forms.Label PlayInfo_lblTotalTime;
        private System.Windows.Forms.Label FileInfo_lblOriginalSample;
        private System.Windows.Forms.Label FileInfo_lblBit;
        private System.Windows.Forms.Label PlayInfo_lblTotalCPU;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem 바이트표시ToolStripMenuItem;
        private System.Windows.Forms.ToolStripComboBox toolStripComboBox1;
        private System.Windows.Forms.ToolStripMenuItem 보기VToolStripMenuItem;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label PlayInfo_lblCurrentSampling;
        private System.Windows.Forms.Label PlayInfo_lblBit;
        private System.Windows.Forms.Label PlayInfo_lblChannel;
        private System.Windows.Forms.Label PlayInfo_lblSoundType;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label PlayInfo_lblSoundFormat;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label PlayInfo_lblCurrentSampling_System;
        private System.Windows.Forms.Label PlayInfo_lblBit_System;
        private System.Windows.Forms.Label PlayInfo_lblMaxInputChannel;
        private System.Windows.Forms.Label PlayInfo_lblSamplingMethod;
        private System.Windows.Forms.Label PlayInfo_lblSoundFormat_System;
        private System.Windows.Forms.Label PlayInfo_lblOutputChannel;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Label PlayInfo_lblCPUGeometry;
        private System.Windows.Forms.Label PlayInfo_lblCPUUpdate;
        private System.Windows.Forms.Label PlayInfo_lblCPUSystem;
        private System.Windows.Forms.Label PlayInfo_lblCPUDSP;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.Label Inside_lblSoundSystemMemory;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Label Inside_lblProgramRealMemory;
        private System.Windows.Forms.Label Inside_lblProgramPageMemory;
        private System.Windows.Forms.Label Inside_lblProgramGCMemoryAfter;
        private System.Windows.Forms.Label Inside_lblProgramGCMemoryBefore;
        private System.Windows.Forms.Timer timer2;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Label Inside_lblSoundSystemSoundgroup;
        private System.Windows.Forms.Label Inside_lblSoundSystemStringdata;
        private System.Windows.Forms.Label Inside_lblSoundSystemFilebuff;
        private System.Windows.Forms.Label Inside_lblSoundSystemDSPConn;
        private System.Windows.Forms.Label Inside_lblSoundSystemDSP;
        private System.Windows.Forms.Label Inside_lblSoundSystemReverb;
        private System.Windows.Forms.Label Inside_lblSoundSystemCodec;
        private System.Windows.Forms.Label Inside_lblSoundSystemOutput;
        private System.Windows.Forms.Label Inside_lblSoundSystemPlugin;
        private System.Windows.Forms.Label Inside_lblSoundSystemSound;
        private System.Windows.Forms.Label Inside_lblSoundSystemChannel;
        private System.Windows.Forms.Label Inside_lblSoundSystemSystem;
        private System.Windows.Forms.NumericUpDown numericUpDown2;
        private System.Windows.Forms.NumericUpDown numericUpDown1;
        private System.Windows.Forms.ToolStripTextBox toolStripTextBox1;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.ToolStripMenuItem 닫기XToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem 새로고침ToolStripMenuItem;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.TextBox txtAlbum;
        private System.Windows.Forms.TextBox txtArtist;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtLyric;
        private System.Windows.Forms.ComboBox comboBox3;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripMenuItem 음악태그자동업데이트ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 음악태그자동저장ToolStripMenuItem;
        private System.Windows.Forms.TextBox txtComment;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txtComposer;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ComboBox txtGenres;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtAlbumArtist;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtTrack;
        private System.Windows.Forms.TextBox txtYear;
        private System.Windows.Forms.HScrollBar hScrollBar1;
        private System.Windows.Forms.ImageList imageList1;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem 이미지복사ToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.ToolStripMenuItem 이미지저장ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 이미지불러오기ToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripMenuItem 이미지지우기ToolStripMenuItem;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.ToolStripMenuItem 원본크기로보기ToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator6;
        private System.Windows.Forms.ToolStripMenuItem 이미지추가ToolStripMenuItem;
        private System.Windows.Forms.Label lblPictureSize;
        private System.Windows.Forms.Label lblPictureLength;
        private System.Windows.Forms.Label lblPictureMimeType;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator7;
        private System.Windows.Forms.Label PlayInfo_lblSoundSevice;
        private System.Windows.Forms.Label PlayInfo_lblSoundOutput;
        private System.Windows.Forms.TextBox PlayInfo_txtSoundSevice;
        private System.Windows.Forms.ListView listView1;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.ComboBox comboBox2;
        private System.Windows.Forms.ToolStripMenuItem 음악태그비동기적으로로딩ToolStripMenuItem;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip2;
        private System.Windows.Forms.ToolStripMenuItem 복사ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 복사원래데이터ToolStripMenuItem;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private System.Windows.Forms.ColumnHeader columnHeader5;
        private System.Windows.Forms.Label PlayInfo_lblBPM;
        private System.Windows.Forms.Label FileInfo_lblBPM;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator8;
        private System.Windows.Forms.ToolStripMenuItem bPM자동업데이트ToolStripMenuItem;
    }
}