﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;

using System.Text;
using System.Windows.Forms;
using System.Web;
using System.IO;
using System.Diagnostics;
using FMOD_Helper;
using System.Threading;

namespace FMOD_Audio
{
    public partial class Form1 : Form
    {
        const string ProgramName = "조장찡 플레이어";
        /// <summary>
        /// 파일의 인코딩을 구합니다
        /// </summary>
        /// <param name="FileName">인코딩을 구할 파일경로 입니다.</param>
        /// <returns>파일의 인코딩을 반환합니다</returns>
        private static Encoding GetFileEncoding(string FileName)
        {
            // *** Use Default of Encoding.Default (Ansi CodePage)
            Encoding enc = Encoding.Default;
            try
            {
                UTF8Encoding utf8 = new UTF8Encoding(false, true);
                StreamReader sr = new StreamReader(FileName, utf8);
                int a = sr.Read();sr.Close(); 
                if (a == -1) { throw new DecoderFallbackException(); }
                enc = utf8;
            }
            catch
            {
                // *** Detect byte order mark if any - otherwise assume default
                byte[] buffer = new byte[5];
                FileStream file = new FileStream(FileName, FileMode.Open);
                file.Read(buffer, 0, 5);
                file.Close();

                if (buffer[0] == 0xef && buffer[1] == 0xbb && buffer[2] == 0xbf)
                    enc = Encoding.UTF8;
                else if (buffer[0] == 0xfe && buffer[1] == 0xff)
                    enc = Encoding.Unicode;
                else if (buffer[0] == 0 && buffer[1] == 0 && buffer[2] == 0xfe && buffer[3] == 0xff)
                    enc = Encoding.UTF32;
                else if (buffer[0] == 0x2b && buffer[1] == 0x2f && buffer[2] == 0x76)
                    enc = Encoding.UTF7;
                else if (buffer[0] == 0xFE && buffer[1] == 0xFF)
                    // 1201 unicodeFFFE Unicode (Big-Endian)
                    enc = Encoding.GetEncoding(1201);
                else if (buffer[0] == 0xFF && buffer[1] == 0xFE)
                    // 1200 utf-16 Unicode
                    enc = Encoding.GetEncoding(1200);
            }
            return enc;
        }
        /// <summary>
        /// 파일의 인코딩을 구합니다
        /// </summary>
        /// <param name="FileName">인코딩을 구할 파일경로 입니다.</param>
        /// <param name="FastMode">True를 주면 구하는 과정이 빨라집니다.</param>/*빨라지긴 하지만 정확도가 떨어집니다.*/
        /// <returns>파일의 인코딩을 반환합니다</returns>
        private static Encoding GetFileEncoding(string FileName, bool FastMode)
        {
            // *** Use Default of Encoding.Default (Ansi CodePage)
            Encoding enc = Encoding.Default;
            try
            {
                UTF8Encoding utf8 = new UTF8Encoding(false, true);
                if (!FastMode)
                {
                    System.IO.File.ReadAllText(FileName, utf8);
                    //System.IO.File.ReadAllText();
                    enc = utf8;
                }
                else 
                {
                    StreamReader sr = new StreamReader(FileName, utf8);
                    int a = sr.Read();sr.Close(); 
                    if (a == -1) { throw new DecoderFallbackException(); }
                    enc = utf8;
                }
            }
            catch
            {
                // *** Detect byte order mark if any - otherwise assume default
                byte[] buffer = new byte[5];
                if (!System.IO.File.Exists(FileName)) return null;
                FileStream file = new FileStream(FileName, FileMode.Open);
                file.Read(buffer, 0, 5);
                file.Close();

                if (buffer[0] == 0xef && buffer[1] == 0xbb && buffer[2] == 0xbf)
                    enc = Encoding.UTF8;
                else if (buffer[0] == 0xfe && buffer[1] == 0xff)
                    enc = Encoding.Unicode;
                else if (buffer[0] == 0 && buffer[1] == 0 && buffer[2] == 0xfe && buffer[3] == 0xff)
                    enc = Encoding.UTF32;
                else if (buffer[0] == 0x2b && buffer[1] == 0x2f && buffer[2] == 0x76)
                    enc = Encoding.UTF7;
                else if (buffer[0] == 0xFE && buffer[1] == 0xFF)
                    // 1201 unicodeFFFE Unicode (Big-Endian)
                    enc = Encoding.GetEncoding(1201);
                else if (buffer[0] == 0xFF && buffer[1] == 0xFE)
                    // 1200 utf-16 Unicode
                    enc = Encoding.GetEncoding(1200);
            }
            return enc;
        }

        [System.Runtime.InteropServices.DllImport("kernel32.dll", SetLastError = true, ExactSpelling = true)]
        static extern bool CheckRemoteDebuggerPresent(IntPtr hProcess, ref bool isDebuggerPresent);

        const long DateSize = 2048;
        bool IsDebugging
        {get { bool dd = false; CheckRemoteDebuggerPresent(Process.GetCurrentProcess().Handle, ref dd); return dd; }}

        Lyrics.LyricsCache Lyricscache = new Lyrics.LyricsCache();
        public Form1()
        {
            InitializeComponent();
            if (IsDebugging) this.Text = this.Text.Insert(this.Text.Length, " (디버깅)");
            //MessageBox.Show(IsDebugging.ToString());
            frmlrcSearch.GetLyricsComplete += new Lyrics.frmLyricsSearch.GetLyricsCompleteEventHandler(frmlrcSearch_GetLyricsComplete);
            lrcf.Lyricscache = Lyricscache;
            lrcf.FormStatusChange+=new Lyrics.frmLyricsDesktop.FormStatusChangeEventHandler(lrcf_FormStatusChange);
            lrcf.UpdateLyricsIntervalChange+=new Lyrics.frmLyricsDesktop.UpdateLyricsIntervalChangeEventHandler(lrcf_UpdateLyricsIntervalChange);
        }
        public Form1(string[] args)
        {
            InitializeComponent();
            if (IsDebugging) this.Text = this.Text.Insert(this.Text.Length, " (디버깅)");
            //MessageBox.Show(IsDebugging.ToString());
            frmlrcSearch.GetLyricsComplete += new Lyrics.frmLyricsSearch.GetLyricsCompleteEventHandler(frmlrcSearch_GetLyricsComplete);
            lrcf.Lyricscache = Lyricscache;
            lrcf.FormStatusChange += new Lyrics.frmLyricsDesktop.FormStatusChangeEventHandler(lrcf_FormStatusChange);
            lrcf.UpdateLyricsIntervalChange += new Lyrics.frmLyricsDesktop.UpdateLyricsIntervalChangeEventHandler(lrcf_UpdateLyricsIntervalChange);
            //MessageBox.Show(args[0].ToLower());

            #region 인자 파싱
            if (args.Length > 0)
            {
                for (int i = 0; i < args.Length; i++)
                {
                    #region 파일이 존재하면
                    if (File.Exists(args[i])) 
                    {
                        try
                        {
                            bool Error = false;
                            try
                            {
                                //timer1.Start();
                                ListViewItem li = new ListViewItem(listView1.Items.Count.ToString());
                                if (listView1.Items.Count == 0)
                                {
                                    fh = new FMOD_Helper.FMODHelper();
                                    fh.PlayingStatusChanged += new FMOD_Helper.FMODHelper.PlayingStatusChangedEventHandler(fh_PlayingStatusChanged_NoMixing);
                                    fh.PlayingStatusChanged += new FMODHelper.PlayingStatusChangedEventHandler(lrcf.PlayingStatusChanged);
                                    fh.MusicPathChanged += new FMODHelper.MusicPathChangedEventHandler(fh_MusicPathChanged_NoMixing);
                                    fh.MusicPath = args[i];
                                    ps = new PlayerSetting(fh, System.IO.Path.GetFileName(fh.MusicPath), this);
                                    fm.Add(fh);
                                    fh.index = listView1.Items.Count;
                                    li.SubItems.Add("현재 (Ready)");
                                    if (ps == null) { ps = new PlayerSetting(fh, System.IO.Path.GetFileName(fh.MusicPath)); }
                                    ps.Title = string.Format("{0}", System.IO.Path.GetFileName(fh.MusicPath));
                                    button1.Enabled = button2.Enabled = button4.Enabled = true;
                                }
                                else { li.SubItems.Add(""); }
                                li.SubItems.Add(System.IO.Path.GetFileName(args[i]));
                                li.SubItems.Add(args[i]);
                                li.SubItems.Add("");
                                listView1.Items.Add(li);
                            }
                            catch (Exception ex) { sb.AppendLine(args[i]); CSharpUtility.LoggingUtility.LoggingT(ex); Error = true; }
                        }
                        finally
                        {
                            if (sb.Length > 0)
                            {
                                MessageBox.Show("일부항목은 파일이 존재하지 않거나\n올바르지 않은 음악 파일 이므로 추가하지 못했습니다.\n" +
                                    "더 자세한 정보를 알고싶다면 로그를 분석해주시기 바랍니다.\n\n추가 못한 경로:\n" + sb.ToString(),
                                    "일부 항목 추가실패!");
                            }
                        }
                    }
                    #endregion
                    #region /mixing 인자를 주었을때
                    if (args[i].ToLower() == "/mixing")
                    {
                        panel1.Enabled = true;
                        checkBox1.Visible = false;
                        음악믹싱모드ToolStripMenuItem.Checked = true;
                        checkBox1.Visible = false;
                        button4.Text = "음향효과";
                        원본크기구하기ToolStripMenuItem1.Visible = toolStripSeparator2.Visible = true;
                        파일로웨이브저장ToolStripMenuItem.Visible = toolStripSeparator7.Visible = false;
                        파일로웨이브저장ToolStripMenuItem.Text = "저장";
                        원본크기구하기ToolStripMenuItem1.Text = "파일로 현재 재생위치의 웨이브정보 저장";
                        파일로웨이브저장ToolStripMenuItem.Text = "웨이브정보 저장";
                        columnHeader5.Width = 139;
                        panel2.Location = new Point(167, 52); button5.Visible = true;
                        columnHeader2.Width = 50;
                        columnHeader4.Width = 223;
                        안전모드ToolStripMenuItem.Visible = false;
                        //listView1.GridLines = false;
                    }
                    #endregion
                    #region /repairsoundsystem 인자를 주었을떄
                    int copycount=0;
                    if (args[i].ToLower() == "/repairsoundsystem")
                    {
                    repair:
                        try
                        {
                            Program.DLLCopy_FMOD(false); Program.DLLCopy_FMOD(true);
                            copycount++;
                        }
                        catch (Exception ex)
                        {
                            if (copycount < 10) { System.Threading.Thread.Sleep(500); goto repair; }
                            else
                            {
                                MessageBox.Show("사운드 시스템 엔진 파일 복구를 실패하여 프로그램을 종료합니다.\n\n예외:\n " + ex.ToString(),
                                    "사운드 시스템 엔진 파일 복구실패 알림", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            }
                        }
                    }
                    #endregion
                }
            }
            #endregion
        }

        public bool MixingMode { get { return 음악믹싱모드ToolStripMenuItem.Checked; } set { 음악믹싱모드ToolStripMenuItem.Checked = value; } }
        List<FMOD_Helper.FMODHelper> fm= new List<FMOD_Helper.FMODHelper>();
        internal delegate void PlayChangeEventHandler(bool Play);
        internal event PlayChangeEventHandler PlayChanged;
        internal void button1_Click(object sender, EventArgs e)
        {
            if (음악믹싱모드ToolStripMenuItem.Checked) { 재생ToolStripMenuItem_Click(null, null); }//Play(listView1.SelectedItems[0].Index); }
            else 
            {
                if (button1.Text == "재생")
                { Play(0); try { PlayChanged(true); } catch { } button1.Text = "일시정지"; 
                  재생ToolStripMenuItem1.Image = FMOD_Audio.Properties.Resources.PauseHS; }
                else { Pause(0); try { PlayChanged(false); } catch { } button1.Text = "재생"; 
                  재생ToolStripMenuItem1.Image = FMOD_Audio.Properties.Resources.GoToNextRecord; }
                Init_Sort(false);
                timer1.Start(); tmpStop = false;
                
                //for(int i=0;i<listView1.Items.Count;i++)
                //{ listView1.Items[i].SubItems[1].Text = ""; if (fm[0].index == i) { listView1.Items[i].SubItems[1].Text = "현재 ("+Status_NoMixing.ToString()+")"; } }
            }
            timer1.Start();
        }
        internal void Play(int index) { fm[index].Play(); timer1.Start(); }//timer2.Start(); }
        internal void Pause(int index) { try { fm[index].Pause(); } catch { } }
        internal void Stop(int index) { try { fm[index].Stop(); timer2.Stop(); } catch { } }
        internal void Delete(int index) { try { fm[index].Dispose(); fm.RemoveAt(index); } catch { } }
        internal TimeSpan CurrentTick(int index) { if (fm.Count > 0) { return TimeSpan.FromMilliseconds((double)fm[index].CurrentPosition); } else { return ts; } }
        internal TimeSpan TotalTick(int index) { return TimeSpan.FromMilliseconds((double)fm[index].TotalPosition); }

        readonly TimeSpan ts= new TimeSpan();

        private void button3_Click(object sender, EventArgs e)
        {
            FMOD_Helper.FMODHelper fh;
            
            try {fh  = new FMOD_Helper.FMODHelper(textBox1.Text); }
            catch (SoundSystemException ex)
            {
                if(ex.Result == FMOD.RESULT.ERR_MEMORY)
                MessageBox.Show("더 이상 파일을 추가할 수 없습니다.\n\n다른 항목을 제거하고 다시 시도하세요!", "파일 더이상 추가 불가", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                if(ex.Result == FMOD.RESULT.ERR_FILE_NOTFOUND)
                MessageBox.Show("파일을 찾을 수 없거나 올바른 사운드파일이 아닙니다.", "파일 추가 실패 알림", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            fh.PlayingStatusChanged += new FMOD_Helper.FMODHelper.PlayingStatusChangedEventHandler(fh_PlayingStatusChanged);
            fh.MusicPathChanged += new FMODHelper.MusicPathChangedEventHandler(fh_MusicPathChanged);
            if (true/*fh.HelperEx.FMODResult == FMOD.RESULT.OK*/)
            {
                //if (MixingMode)
                {
                    //timer1.Start();
                    fm.Add(fh);
                    frmdsp1.Add(new FMOD_Helper.Forms.frmDSP(fh, false));
                    fh.Loop = true;
                    timer1.Start();
                    fh.index = fm.Count - 1;
                    ListViewItem li = new ListViewItem(fh.index.ToString());
                    li.SubItems.Add(fh.PlayStatus.ToString());
                    li.SubItems.Add(System.IO.Path.GetFileName(fh.MusicPath));
                    li.SubItems.Add(fh.MusicPath);
                    li.SubItems.Add("");
                    listView1.Items.Add(li);
                    ps1.Add(new PlayerSetting(fh, System.IO.Path.GetFileName(fh.MusicPath)));
                }
            }
            else { fh.Dispose(); }
        }

        FMOD_Helper.FMODHelper fh;
        private void openFileDialog1_FileOk(object sender, CancelEventArgs e)
        {
            
        }

        PlayerSetting ps;
        List<PlayerSetting> ps1 = new List<PlayerSetting>();
        private void button4_Click(object sender, EventArgs e)
        {
            if (음악믹싱모드ToolStripMenuItem.Checked)
            {
                for (int i = 0; i < listView1.SelectedItems.Count; i++)
                {
                    if (ps1.Count ==0)
                    {ps1.Add(new PlayerSetting(fm[listView1.SelectedItems[i].Index], listView1.SelectedItems[i].SubItems[2].Text));}
                    ps1[listView1.SelectedItems[i].Index].Show();
                    ps1[listView1.SelectedItems[i].Index].BringToFront();
                }
            }
            else
            {
                if (ps == null)
                {ps = new PlayerSetting(fm[0], System.IO.Path.GetFileName(fm[indexing].MusicPath));}
                //System.IO.Path.GetFileName(fm[0].MusicPath); 
                ps.Show();
            }
        }
        StringBuilder sb = new StringBuilder();
        private void listView1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (음악믹싱모드ToolStripMenuItem.Checked)
            {
                if (listView1.SelectedItems.Count > 0)
                {
                    if (listView1.SelectedItems.Count == 1) 
                    { 
                        indexing = listView1.SelectedItems[0].Index; 
                        원본크기구하기ToolStripMenuItem1.Enabled = true;
                        toolStripProgressBar2.Visible = true;
                    }
                    else { 원본크기구하기ToolStripMenuItem1.Enabled = false; }
                    sb.Append(fm[listView1.SelectedItems[0].Index].MusicPath);
                    for (int i = 1; i < listView1.SelectedItems.Count; i++)
                    {
                        sb.Append("\r\n" + fm[listView1.SelectedItems[i].Index].MusicPath);
                    }
                    textBox2.Text = sb.ToString();
                    sb.Remove(0, sb.Length);
                    button1.Enabled = button2.Enabled=button5.Enabled = button4.Enabled = 재생ToolStripMenuItem.Enabled = 정지ToolStripMenuItem.Enabled = 삭제ToolStripMenuItem.Enabled = 일시정지ToolStripMenuItem.Enabled = true;
                }
                else
                {
                    button1.Enabled = button2.Enabled = button5.Enabled = button4.Enabled = 재생ToolStripMenuItem.Enabled = 정지ToolStripMenuItem.Enabled = 삭제ToolStripMenuItem.Enabled = 일시정지ToolStripMenuItem.Enabled = false;
                    원본크기구하기ToolStripMenuItem1.Enabled = false;
                    Init_Sort(true);
                    textBox2.Text = "";
                }
            }
            else
            {
                /*for (int i = 0; i < listView1.Items.Count; i++)
                {
                    listView1.Items[i].Text = i.ToString();
                    if (fm[0].index.ToString() == listView1.Items[i].Text)
                    { listView1.Items[i].SubItems[1].Text = "현재"; }
                    else { listView1.Items[i].SubItems[1].Text = ""; }
                }*/
                if (listView1.SelectedItems.Count > 0)
                {
                    sb.Append(listView1.SelectedItems[0].SubItems[3].Text);
                    for (int i = 1; i < listView1.SelectedItems.Count; i++)
                    {sb.Append("\r\n" + listView1.SelectedItems[i].SubItems[3].Text); }
                    textBox2.Text = sb.ToString();
                    sb.Remove(0, sb.Length);
                }
                else { textBox2.Text = ""; }
                //Init_Sort(false);
                if (listView1.SelectedItems.Count == 1)
                {재생ToolStripMenuItem.Enabled = 정지ToolStripMenuItem.Enabled = 일시정지ToolStripMenuItem.Enabled = 삭제ToolStripMenuItem.Enabled = true; }
                else if (listView1.SelectedItems.Count > 0)
                { 재생ToolStripMenuItem.Enabled = 정지ToolStripMenuItem.Enabled = 일시정지ToolStripMenuItem.Enabled = false; 삭제ToolStripMenuItem.Enabled = true;  }
                else { 재생ToolStripMenuItem.Enabled = 정지ToolStripMenuItem.Enabled = 삭제ToolStripMenuItem.Enabled = 일시정지ToolStripMenuItem.Enabled = false; }
                //button1.Enabled = button2.Enabled = button4.Enabled = 재생ToolStripMenuItem.Enabled = 정지ToolStripMenuItem.Enabled = 삭제ToolStripMenuItem.Enabled = 일시정지ToolStripMenuItem.Enabled = true; 
            }
            if (syncToolStripMenuItem.Checked) Application.DoEvents();
        }

        private void 재생ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < listView1.SelectedItems.Count; i++)
            {
                if (음악믹싱모드ToolStripMenuItem.Checked)
                {
                    Play(listView1.SelectedItems[i].Index);
                    listView1.SelectedItems[i].SubItems[1].Text = "현재 (Play)";
                    frmdsp1[i].ReRegister(false);
                    //fm[i].PlayStatus.ToString();
                }
                else
                {
                    if (fm.Count > 0 && !새인스턴스시작ToolStripMenuItem.Checked&&
                        listView1.SelectedItems[0].Text == fm[0].index.ToString()) { Play(0); }
                    else
                    {
                        float Frequency=0;
                        if (fm.Count == 0)
                        {
                            fm.Add(new FMOD_Helper.FMODHelper(listView1.SelectedItems[i].SubItems[3].Text));
                            fm[0].PlayingStatusChanged += new FMODHelper.PlayingStatusChangedEventHandler(fh_PlayingStatusChanged_NoMixing);
                            fm[0].MusicPathChanged += new FMODHelper.MusicPathChangedEventHandler(fh_MusicPathChanged_NoMixing);
                        }
                        else { if(ps != null){ Frequency = (float)ps.numFrenq.Value - fm[0].DefaultFrequency;}fm[0].MusicPath = listView1.SelectedItems[i].SubItems[3].Text; }
                        if (ps != null) { ps.Helper = fm[0]; } else { ps = new PlayerSetting(fm[0]); }
                        if (frmdsp != null) frmdsp.ReRegister(false);
                        fm[0].Frequency = fm[0].DefaultFrequency + Frequency;
                        ps.numFrenq.Value = (decimal)fm[0].Frequency;
                        for (int j = 0; j < listView1.Items.Count; j++) { listView1.Items[j].Text = j.ToString(); listView1.Items[j].SubItems[1].Text = ""; }
                        listView1.SelectedItems[i].SubItems[1].Text = "현재 (Ready)";
                        button1.Text = "일시정지";
                        fm[0].index = int.Parse(listView1.SelectedItems[i].Text);
                        fm[0].Play();
                        button1.Enabled = button2.Enabled = button4.Enabled = true;
                        ps.Title = string.Format("{0}", System.IO.Path.GetFileName(fm[0].MusicPath));
                        timer1.Start();
                        tmpStop = false;
                    }
                }
            }
        }

        internal void 일시정지ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < listView1.SelectedItems.Count; i++)
            {
                if (음악믹싱모드ToolStripMenuItem.Checked)
                {
                    Pause(listView1.SelectedItems[i].Index);
                    listView1.SelectedItems[i].SubItems[1].Text = "Pause";
                }//fm[i].PlayStatus.ToString();
                else{Pause(0);}
            }
        }

        internal void 정지ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < listView1.SelectedItems.Count; i++)
            {
                if(음악믹싱모드ToolStripMenuItem.Checked){
                Stop(listView1.SelectedItems[i].Index);
                listView1.SelectedItems[i].SubItems[1].Text = "Stop";}//fm[i].PlayStatus.ToString();
                else{Stop(0);}
            }
        }

        private void 삭제ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DialogResult dr;
            if (listView1.SelectedItems.Count == 1) dr = MessageBox.Show("정말로 선택된 항목을 삭제하시겠습니까?", "항목삭제 알림", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
            else dr= MessageBox.Show("정말로 선택된 항목들을 삭제하시겠습니까?", "선택된 항목삭제 알림", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);

            if (dr == System.Windows.Forms.DialogResult.Yes)
            {
                tmpStop = true;
                for (int j = listView1.SelectedItems.Count - 1; j > -1; j--)
                {
                    if (안전모드ToolStripMenuItem.Checked)
                    {
                        if (IsDebugging)
                        {
                            this.Text = string.Format(ProgramName+" (디버깅, 안전모드) [{0}번째 목록 삭제중... 이름: {1} / 경로: {2}]", (j + 1).ToString(), listView1.SelectedItems[j].SubItems[2].Text, listView1.SelectedItems[j].SubItems[3].Text);
                            toolStripStatusLabel1.Text= string.Format("{0}번째 목록 삭제중... (디버깅, 안전모드) [이름: {1} / 경로: {2}]", (j + 1).ToString(), listView1.SelectedItems[j].SubItems[2].Text, listView1.SelectedItems[j].SubItems[3].Text);

                        }
                        else
                        {
                            this.Text = string.Format(ProgramName+" (안전모드) [{0}번째 목록 삭제중... 이름: {1} / 경로: {2}]", (j + 1).ToString(), listView1.SelectedItems[j].SubItems[2].Text, listView1.SelectedItems[j].SubItems[3].Text);
                            toolStripStatusLabel1.Text = string.Format("{0}번째 목록 삭제중... (안전모드) [이름: {1} / 경로: {2}]", (j + 1).ToString(), listView1.SelectedItems[j].SubItems[2].Text, listView1.SelectedItems[j].SubItems[3].Text);
                        }
                    }
                    else
                    {
                        if (IsDebugging)
                        {
                            this.Text = string.Format(ProgramName+" (디버깅) [{0}번째 목록 삭제중... 이름: {1} / 경로: {2}]", (j + 1).ToString(), listView1.SelectedItems[j].SubItems[2].Text, listView1.SelectedItems[j].SubItems[3].Text);
                            toolStripStatusLabel1.Text = string.Format("{0}번째 목록 삭제중... (디버깅) [이름: {1} / 경로: {2}]", (j + 1).ToString(), listView1.SelectedItems[j].SubItems[2].Text, listView1.SelectedItems[j].SubItems[3].Text);
                        }
                        else
                        {
                            this.Text = string.Format(ProgramName+" [{0}번째 목록 삭제중... 이름: {1} / 경로: {2}]", (j + 1).ToString(), listView1.SelectedItems[j].SubItems[2].Text, listView1.SelectedItems[j].SubItems[3].Text);
                            toolStripStatusLabel1.Text = string.Format("{0}번째 목록 삭제중... [이름: {1} / 경로: {2}]", (j + 1).ToString(), listView1.SelectedItems[j].SubItems[2].Text, listView1.SelectedItems[j].SubItems[3].Text);
                        }
                    }
                    int i = listView1.SelectedItems[j].Index;
                    if (음악믹싱모드ToolStripMenuItem.Checked)
                    { Delete(i); fm.RemoveAt(i); ps1.RemoveAt(i); Init_Sort(true); listView1.Items.RemoveAt(i); tmpStop = false; }
                    else
                    {
                        if (fm.Count>0&&fm[0].index.ToString() == listView1.Items[i].Text)
                        {
                            dr = MessageBox.Show("현재 재생중인 항목입니다. 재생을 멈추고 삭제 하시겠습니까?",
                                "재생중인 항목 삭제 알림", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                            if (dr == System.Windows.Forms.DialogResult.Yes)
                            {
                                if(ps!=null)ps.Hide();
                                toolStripProgressBar2.Visible = false;
                                timer1.Stop();
                                timer3.Stop();
                                Delete(0);
                                button1.Text = "재생";
                                button1.Enabled = button4.Enabled = button2.Enabled = false;
                                listView1.Items.RemoveAt(i);
                                Init_Sort(false);
                                
                            }
                        }
                        else
                        { try { listView1.Items.RemoveAt(i);}catch{}}
                        Init_Sort(false);
                    }
                    statusStrip1.Refresh();
                    if(syncToolStripMenuItem.Checked) Application.DoEvents();
                    /*
                    for (int i1 = 0; i1 < listView1.Items.Count; i1++)
                    {listView1.Items[i1].SubItems[1].Text = i1.ToString();}*/
                }
                #region
                
                if (안전모드ToolStripMenuItem.Checked)
                {
                    if (IsDebugging)
                    {
                        this.Text = ProgramName+" (디버깅, 안전모드)";
                        toolStripStatusLabel1.Text = "준비 (디버깅, 안전모드)";

                    }
                    else
                    {
                        this.Text = ProgramName+" (안전모드)";
                        toolStripStatusLabel1.Text = "준비 (안전모드)";
                    }
                }
                else
                {
                    if (IsDebugging)
                    {
                        this.Text = ProgramName+" (디버깅)";
                        toolStripStatusLabel1.Text = "준비 (디버깅) ";
                    }
                    else
                    {
                        this.Text = ProgramName;
                        toolStripStatusLabel1.Text = "준비";}
                }
                #endregion
            }
            tmpStop = false;
        }
        void EditTitle(string Text)
        {
            
        }

        public static string ShowTime(TimeSpan Time)
        {
            StringBuilder sb = new StringBuilder();
            /*sb.Append(Time.Hours);sb.Append(":");
            sb.Append(Time.Minutes);sb.Append(":");
            sb.Append(Time.Seconds);sb.Append(".");
            sb.Append(Time.Milliseconds);*/
            sb.Append(Time.Hours.ToString("00")); sb.Append(":");
            sb.Append(Time.Minutes.ToString("00")); sb.Append(":");
            sb.Append(Time.Seconds.ToString("00")); sb.Append(".");
            sb.Append(Time.Milliseconds.ToString("000"));
            return sb.ToString();
        }

        DateTime dt = new DateTime();
        int tmp;
        bool tmpStop;
        bool Once;
        private void timer1_Tick(object sender, EventArgs e)
        {
            //timer1.Interval = 50;
            if (!tmpStop)
            {
                if (!Once) Once = true; if (!음악믹싱모드ToolStripMenuItem.Checked) toolStripProgressBar2.Visible = true;
                try { if (!음악믹싱모드ToolStripMenuItem.Checked)toolStripProgressBar2.Value = (int)CurrentTick(0).TotalMilliseconds; }catch { }
                if (안전모드ToolStripMenuItem.Checked)
                {
                    if (IsDebugging)
                    {
                        if (음악믹싱모드ToolStripMenuItem.Checked)
                        {
                            this.Text = ProgramName+" (디버깅, 안전모드)"; toolStripStatusLabel1.Text = "준비 (디버깅, 안전모드)";
                        }
                        else
                        {
                            if (Status_NoMixing != FMODHelper.PlayingStatus.Ready)
                            {
                                toolStripStatusLabel1.Text = string.Format("{0} (디버깅, 안전모드) [{1} / {2}]",
                                    PlayStatustoString(Status_NoMixing), ShowTime(CurrentTick(0)), ShowTime(TotalTick(0)));
                            }
                            else { toolStripStatusLabel1.Text = "준비 (디버깅, 안전모드)"; }
                        }
                    }
                    else
                    {
                        if (음악믹싱모드ToolStripMenuItem.Checked)
                        {
                            this.Text = ProgramName+" (안전모드)"; toolStripStatusLabel1.Text = "준비 (안전모드)";
                        }
                        else
                        {
                            if (Status_NoMixing != FMODHelper.PlayingStatus.Ready)
                            {
                                toolStripStatusLabel1.Text = string.Format("{0} (안전모드) [{1} / {2}]",
                                    PlayStatustoString(Status_NoMixing), ShowTime(CurrentTick(0)), ShowTime(TotalTick(0)));
                            }
                            else { toolStripStatusLabel1.Text = "준비 (안전모드)"; }
                        }
                    }
                }
                else
                {
                    if (IsDebugging)
                    {
                        if (음악믹싱모드ToolStripMenuItem.Checked)
                        {
                            this.Text = ProgramName+" (디버깅)"; toolStripStatusLabel1.Text = "준비 (디버깅, 안전모드)";
                        }
                        else
                        {
                            if (Status_NoMixing != FMODHelper.PlayingStatus.Ready)
                            {
                                toolStripStatusLabel1.Text = string.Format("{0} (디버깅) [{1} / {2}]",
                                    PlayStatustoString(Status_NoMixing), ShowTime(CurrentTick(0)), ShowTime(TotalTick(0)));
                            }
                            else { toolStripStatusLabel1.Text = "준비 (디버깅)"; }
                        }
                    }
                    else
                    {
                        if (음악믹싱모드ToolStripMenuItem.Checked)
                        {
                            this.Text = ProgramName+" (디버깅)"; toolStripStatusLabel1.Text = "준비";
                        }
                        else
                        {
                            if (Status_NoMixing != FMODHelper.PlayingStatus.Ready)
                            {
                                /*
                                toolStripStatusLabel1.Text = string.Format("{0} [{1}:{2}:{3}:{4} / {5}:{6}:{7}:{8}]",
                                    PlayStatustoString(Status_NoMixing), 
                                    CurrentTick(0).Hours.ToString("00"),CurrentTick(0).Minutes.ToString("00"), CurrentTick(0).Seconds.ToString("00"), CurrentTick(0).Milliseconds.ToString("000"),
                                    TotalTick(0).Hours.ToString("00"), TotalTick(0).Minutes.ToString("00"), TotalTick(0).Seconds.ToString("00"), TotalTick(0).Milliseconds.ToString("000"));*/
                                toolStripStatusLabel1.Text = string.Format("{0} [{1} / {2}]",
                                    PlayStatustoString(Status_NoMixing), ShowTime(CurrentTick(0)), ShowTime(TotalTick(0)));
                            }
                            else { toolStripStatusLabel1.Text = "준비"; }
                        }
                    }
                }
            }
            else { toolStripProgressBar2.Visible = Once = false; }
            if (음악믹싱모드ToolStripMenuItem.Checked)
            {
                try
                {
                    for (int i = 0; i < listView1.Items.Count; i++)
                    {
                        //timer1.Interval = 100;
                        listView1.Items[i].SubItems[4].Text = string.Format("{0} / {1}",ShowTime(CurrentTick(i)), ShowTime(TotalTick(i)));
                        //dt.AddMilliseconds((double)Tick(i)).ToString("tt hh:mm:ss:ff");
                    }
                }
                catch { }
            }
            else 
            {
                if (lrcStop) timer3.Stop();else timer3.Start();
                if (checkBox1.Checked)
                {
                    if (fm.Count != 0)
                    {
                        if (fm[0].Complete /*|| fm[0].GetCurrentTick(FMOD.TIMEUNIT.PCMBYTES) == fm[0].GetTotalTick(FMOD.TIMEUNIT.PCMBYTES)*/)//
                        {
                            fm[0].Complete = false;
                            string MusicPath = "";
                            /*try*/ {
                                if (fm[0].index==listView1.Items.Count-1)
                                    if (checkBox1.Checked) MusicPath = listView1.Items[0].SubItems[3].Text; else goto Exit; 
                                else
                                    MusicPath = listView1.Items[fm[0].index + 1].SubItems[3].Text; }
                            //catch (ArgumentException) {}
                            if (ps == null) ps = new PlayerSetting(fm[0]);
                            float Frequency = (float)ps.numFrenq.Value - fm[0].DefaultFrequency;
                            //this.Text = (ProgramName+" (디버깅) (재생 완료! {플레이어 인덱스: " + fm[0].index+"}{횟수: "+tmp++.ToString()+"})");
                            //if (fm[0].index == fm.Count) {  }
                            try { fm[0].MusicPath = MusicPath; fm[0].index = int.Parse(listView1.Items[fm[0].index + 1].Text); 
                                ps.numFrenq.Value = (decimal)(fm[0].DefaultFrequency + Frequency); fm[0].Frequency = fm[0].DefaultFrequency + Frequency; fm[0].Play(); }
                            catch { try { if (checkBox1.Checked) { fm[0].index = 0; fm[0].MusicPath = MusicPath; 
                                fm[0].Frequency = fm[0].DefaultFrequency + Frequency; fm[0].Play(); fm[0].index = 0; } } catch { } }
                            try { if(ps!=null)ps.Title = string.Format("{0}", System.IO.Path.GetFileName(MusicPath)); }catch{ps.Title=null;}
                            if (frmdsp != null) frmdsp.ReRegister();
                        Exit:
                            MusicPath = "";
                        }
                    }
                }
            }
        }


        bool CompleteLock;

        bool OpenDialog=true;
        Dictionary<string, string> Setting = new Dictionary<string, string>();
        private void 재생목록열기ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (LastListOpenDirectory != null && Directory.Exists(LastListOpenDirectory)) 
            { openFileDialog2.InitialDirectory = LastListOpenDirectory; }
            DialogResult dr = DialogResult.OK;
            if (OpenDialog) { dr = openFileDialog2.ShowDialog(); }
            if (dr == System.Windows.Forms.DialogResult.OK)
            {
                LastListOpenDirectory = Path.GetDirectoryName(openFileDialog2.FileName);
                Encoding ec = GetFileEncoding(openFileDialog2.FileName, false);
                StreamReader sr = new StreamReader(openFileDialog2.FileName, ec);
                string a = sr.ReadToEnd(); sr.Close();//System.IO.File.ReadAllText(openFileDialog2.FileName,ec);

                if (안전모드ToolStripMenuItem.Checked){
                    if (IsDebugging) { this.Text = ProgramName+" (디버깅, 안전모드) [재생목록 여는중... {인코딩: " + ec.ToString() + "}]"; }
                    else { this.Text = ProgramName+" (안전모드) [재생목록 여는중... {인코딩: " + ec.ToString() + ")}"; }}
                else{if (IsDebugging) { this.Text = ProgramName+" (디버깅) [재생목록 여는중... {인코딩: " + ec.ToString() + ")}"; }
                     else { this.Text=ProgramName+" [재생목록 여는중... {인코딩: " + ec.ToString() + ")}"; }}

                
                if (안전모드ToolStripMenuItem.Checked){
                    if (IsDebugging) { toolStripStatusLabel1.Text = "재생목록 여는중... (디버깅, 안전모드) {인코딩: " + ec.ToString() + ")}"; }
                    else { toolStripStatusLabel1.Text = "재생목록 여는중... (안전모드) {인코딩: " + ec.ToString() + ")}"; } }
                else
                {
                    if (IsDebugging) { toolStripStatusLabel1.Text = "재생목록 여는중... (디버깅) {인코딩: " + ec.ToString() + ")}"; }
                    else { toolStripStatusLabel1.Text = "재생목록 여는중... {인코딩: " + ec.ToString() + ")}"; }
                }
                /*if(ANISToolStripMenuItem.Checked){System.IO.File.ReadAllText(openFileDialog2.FileName, Encoding.Default);}
                if (UTF8ToolStripMenuItem.Checked) { System.IO.File.ReadAllText(openFileDialog2.FileName, Encoding.UTF8); }
                if (유니코드ToolStripMenuItem.Checked) { System.IO.File.ReadAllText(openFileDialog2.FileName, Encoding.Unicode); }*/

                //if (encodingtest_3(a, Encoding.Default) != null) { a = encodingtest_3(a, Encoding.UTF8); }
                string[] b = CSharpUtility.Utility.StringUtility.ConvertStringToArray(a, null); 
                toolStripProgressBar1.Maximum = b.Length; toolStripProgressBar1.Visible = toolStripStatusLabel2.Visible = true;
                //List<string> h = b.ToList();
                if (syncToolStripMenuItem.Checked) Application.DoEvents();
                int i1 = 0;

                StringBuilder ErrFile = new StringBuilder();
                if (!음악믹싱모드ToolStripMenuItem.Checked)
                {
                    if (listView1.Items.Count == 0)
                    {
                        //if (fm.Count != 0) { fm[0].Dispose(); }
                    again:
                        try
                        {
                            for (; i1 < b.Length; )
                            {
                                if (System.IO.File.Exists(b[i1]))
                                {
                                    try 
                                    { 
                                        fm.Add(new FMOD_Helper.FMODHelper());
                                        fm[0].PlayingStatusChanged += new FMOD_Helper.FMODHelper.PlayingStatusChangedEventHandler(fh_PlayingStatusChanged_NoMixing);
                                        fm[0].MusicPathChanged += new FMODHelper.MusicPathChangedEventHandler(fh_MusicPathChanged_NoMixing);
                                        try { ps = new PlayerSetting(fm[0], System.IO.Path.GetFileName(b[i1]), this); }catch { }
                                        fm[0].MusicPath = b[i1];
                                    }
                                    catch (SoundSystemException ex)
                                    {
                                        CSharpUtility.LoggingUtility.LoggingT(ex);
                                        if (ex.Result == FMOD.RESULT.ERR_MEMORY)
                                        {
                                            DialogResult dr1 = MessageBox.Show("재생목록을 여는중 내부에서 메모리 오류가 발생했습니다.\n\n" +
                                            "프로그램을 다시 시작하여야만 합니다.\n\n다시 시작 하시겠습니까?", "프로그램 다시 시작 알림", MessageBoxButtons.YesNo, MessageBoxIcon.Stop);
                                            if (dr1 == System.Windows.Forms.DialogResult.Yes) { Application.Restart(); }
                                        }
                                        else { throw new SoundSystemException(); }
                                    }
                                    catch(Exception ex)
                                    {
                                        MessageBox.Show("재생목록 "+i1+"번째 파일을 여는 중 알 수없는 오류발생!\n로그를 확인해 주세요.", "오류 발생", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                        CSharpUtility.LoggingUtility.LoggingT(ex);
                                    }
                                    button1.Enabled = button2.Enabled = button4.Enabled = true;
                                    if (syncToolStripMenuItem.Checked) Application.DoEvents();
                                    break;
                                }
                                else { if (b[i1].ToLower() != "#extm3u")ErrFile.AppendLine("    " + b[i1]); throw new SoundSystemException(); }
                            }
                        }
                        catch { i1++; goto again; }
                }
                for (int i = i1; i < b.Length; i++)
                {
                    if (BreakLoop) { BreakLoop = true; break; }
                    #region File Exists
                    if (System.IO.File.Exists(b[i]))
                    {
                        if (안전모드ToolStripMenuItem.Checked)
                        {
                            if (IsDebugging) { toolStripStatusLabel1.Text = "목록 여는중... (디버깅, 안전모드) [파일 이름: { " + System.IO.Path.GetFileName(b[i]) + " }]"; }
                            else { toolStripStatusLabel1.Text = "목록 여는중... (안전모드) [파일 이름: { " + System.IO.Path.GetFileName(b[i]) + " }]"; }
                        }
                        else
                        {
                            if (IsDebugging) { toolStripStatusLabel1.Text = "목록 여는중... (디버깅) [파일 이름: { " + System.IO.Path.GetFileName(b[i]) + " }]"; }
                            else { toolStripStatusLabel1.Text = "목록 여는중... [파일 이름: { " + System.IO.Path.GetFileName(b[i]) + " }]"; }
                        }

                        try { toolStripProgressBar1.Value = i; }
                        catch {toolStripProgressBar1.Value=toolStripProgressBar1.Maximum; }
                        try
                        {
                            #region Checked
                            if (음악믹싱모드ToolStripMenuItem.Checked)
                            {
                                FMOD_Helper.FMODHelper fh;
                                try {fh= new FMOD_Helper.FMODHelper(b[i]); }
                                catch (SoundSystemException ex) { if(ex.Result ==FMOD.RESULT.ERR_MEMORY)
                                    MessageBox.Show("더 이상 목록을 추가할 수 없습니다.\n\n다른 항목을 제거하고 다시 시도하세요!", "목록 더이상 추가 불가 알림", MessageBoxButtons.OK, MessageBoxIcon.Error); break; }
                                
                                if (syncToolStripMenuItem.Checked) Application.DoEvents();
                                fh.PlayingStatusChanged += new FMOD_Helper.FMODHelper.PlayingStatusChangedEventHandler(fh_PlayingStatusChanged);
                                fh.MusicPathChanged += new FMODHelper.MusicPathChangedEventHandler(fh_MusicPathChanged_NoMixing);
                                if (fh.HelperEx.FMODResult == FMOD.RESULT.OK ||
                                    fh.HelperEx.FMODResult == FMOD.RESULT.ERR_REVERB_INSTANCE ||
                                    fh.HelperEx.FMODResult == FMOD.RESULT.ERR_NEEDS3D)
                                {
                                    fh.Loop = true;
                                    toolStripProgressBar1.Value = toolStripProgressBar1.Value + 1;
                                    fm.Add(fh);
                                    frmdsp1.Add(new FMOD_Helper.Forms.frmDSP(fh, false));
                                    if (!timer1.Enabled) { timer1.Start(); }
                                    fh.index = fm.Count - 1;
                                    ListViewItem li = new ListViewItem(fh.index.ToString());
                                    li.SubItems.Add(fh.PlayStatus.ToString());
                                    li.SubItems.Add(System.IO.Path.GetFileName(b[i]));
                                    li.SubItems.Add(b[i]);
                                    listView1.Items.Add(li);
                                    ps1.Add(new PlayerSetting(fh, System.IO.Path.GetFileName(fh.MusicPath)));
                                }
                                else { toolStripProgressBar1.Maximum = toolStripProgressBar1.Maximum - 1;}
                            }
                            #endregion
                            else
                            {
                                ListViewItem li = new ListViewItem(i.ToString());
                                if (i1 == i) { li.SubItems.Add("현재 (Ready)"); /*i = 1;*/ }
                                else { li.SubItems.Add(""); }
                                li.SubItems.Add(System.IO.Path.GetFileName(b[i]));
                                li.SubItems.Add(b[i]);
                                li.SubItems.Add("");
                                listView1.Items.Add(li);
                                //Init_Sort(false);
                            }
                        }
                        catch { ErrFile.AppendLine("    " + b[i]); }
                    }
                    #endregion
                    else { toolStripProgressBar1.Maximum = toolStripProgressBar1.Maximum - 1;
                        if(b[i].ToLower() != "#extm3u")ErrFile.AppendLine("    "+b[i]); }
                }
                if (ErrFile.Length > 0)
                {
                    MessageBox.Show("일부항목은 파일이 존재하지 않아서 추가하지 못했습니다.\n\n추가 못한 경로:\n" + ErrFile.ToString(),
                        "일부 항목 추가실패!");
                }
                Init_Sort(true);
                toolStripProgressBar1.Value = 0;

                if (안전모드ToolStripMenuItem.Checked)
                {
                    if (IsDebugging) 
                    { this.Text = ProgramName+" (디버깅, 안전모드)";toolStripStatusLabel1.Text = "준비 (디버깅, 안전모드)";}
                    else { this.Text = ProgramName+" (안전모드)"; toolStripStatusLabel1.Text = "준비 (안전모드)"; }
                }
                else
                {
                    if (IsDebugging) { this.Text = ProgramName+" (디버깅)";toolStripStatusLabel1.Text = "준비 (디버깅)";}
                    else { this.Text = ProgramName; toolStripStatusLabel1.Text = "준비"; }
                }
                toolStripProgressBar1.Visible = toolStripStatusLabel2.Visible = toolStripStatusLabel3.Visible = false; toolStripProgressBar1.Maximum = 0;
                상태표시줄업데이트ToolStripMenuItem_Click(null, null);
            }
          }
        }

        private void openFileDialog2_FileOk(object sender, CancelEventArgs e)
        {
            
        }

        internal FMOD_Helper.Forms.frmEqulazer fe;
        //internal List<FMOD_Helper.Forms.frmEqulazer> fe1 = new List<FMOD_Helper.Forms.frmEqulazer>();
        internal FMOD_Helper.Forms.frmDSP frmdsp;
        internal List<FMOD_Helper.Forms.frmDSP> frmdsp1 = new List<FMOD_Helper.Forms.frmDSP>();
        Lyrics.frmLyricsDesktop lrcf = new Lyrics.frmLyricsDesktop();
        private void Form1_Load(object sender, EventArgs e)
        {
            /*
            this.contextMenuStrip2.Items.Clear();
            this.contextMenuStrip2.Items.AddRange(new ToolStripItem[] {
            this.파일FToolStripMenuItem,
            this.설정ToolStripMenuItem,
            this.보기VToolStripMenuItem,
            this.도움말HToolStripMenuItem,
            this.toolStripSeparator15,
            this.재생컨트롤ToolStripMenuItem});*/
            cbMimetype.SelectedItem = cbMimetype.Items[0];
            
            //timer2.Start();
            lrcf.Form1Instance = this;
            lrcf.Show();
        }

        bool BreakLoop = false;
        private void toolStripStatusLabel2_Click(object sender, EventArgs e)
        {
            BreakLoop = true;
            toolStripProgressBar1.Visible = toolStripStatusLabel2.Visible = toolStripStatusLabel3.Visible = false; toolStripProgressBar1.Maximum = 0;
            if (안전모드ToolStripMenuItem.Checked)
            {
                if (IsDebugging)
                { this.Text = ProgramName+" (디버깅, 안전모드)"; toolStripStatusLabel1.Text = "준비 (디버깅, 안전모드)"; }
                else { this.Text = ProgramName+" (안전모드)"; toolStripStatusLabel1.Text = "준비 (안전모드)"; }
            }
            else
            {
                if (IsDebugging) { this.Text = ProgramName+" (디버깅)"; toolStripStatusLabel1.Text = "준비 (디버깅)"; }
                else { this.Text = ProgramName; toolStripStatusLabel1.Text = "준비"; }
            }
            toolStripProgressBar1.Value = 0;
        }

        private void 음악믹싱모드ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DialogResult dr = MessageBox.Show("변경사항을 적용하려면 프로그램을 다시 시작하여야 합니다.\n\n" +
            "다시 시작하시겠습니까?", "프로그램 다시시작 알림", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
            if (dr == System.Windows.Forms.DialogResult.Yes) 
            {
                if (!음악믹싱모드ToolStripMenuItem.Checked)
                {
                    System.Diagnostics.Process.Start(Application.ExecutablePath.Replace("/", "\\"),
                        "/Mixing");
                    System.Diagnostics.Process.GetCurrentProcess().Kill();
                }
                else
                {
                    System.Diagnostics.Process.Start(Application.ExecutablePath.Replace("/", "\\"));
                    System.Diagnostics.Process.GetCurrentProcess().Kill();
                }
            }
        }


        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (음악믹싱모드ToolStripMenuItem.Checked) { 정지ToolStripMenuItem_Click(null, null); }//Stop(indexing); }
            else { Stop(0); button1.Text = "재생"; Init_Sort(false); }
        }

        private void contextMenuStrip1_Opening(object sender, CancelEventArgs e)
        {
            try
            {
                if (!음악믹싱모드ToolStripMenuItem.Checked && fm[0].SizeLength > 0)
                {
                    saveFileDialog2.FileName = System.IO.Path.GetFileNameWithoutExtension(fm[0].MusicPath);
                    saveFileDialog2.Title = string.Format("웨이브 저장... (웨이브 크기: {0})", toolStripTextBox1.Text);
                    파일로웨이브저장ToolStripMenuItem.Enabled = true;
                }
                else
                {
                    일시정지ToolStripMenuItem.Enabled =
                        정지ToolStripMenuItem.Enabled = 
                        삭제ToolStripMenuItem.Enabled = false;
                }
                if (listView1.SelectedItems.Count == 1)
                {
                    if (listView1.SelectedItems[0].Text == fm[0].index.ToString())
                    {
                        일시정지ToolStripMenuItem.Enabled =
                        정지ToolStripMenuItem.Enabled =
                        삭제ToolStripMenuItem.Enabled = true;
                    }
                    else{삭제ToolStripMenuItem.Enabled = true; }
                    가사찾기ToolStripMenuItem.Enabled = true; 
                }
                else { 가사찾기ToolStripMenuItem.Enabled = false; 삭제ToolStripMenuItem.Enabled = true; }

                if (listView1.SelectedItems.Count > 0)
                {
                    복사ToolStripMenuItem.Enabled =
                    탐색기에서해당파일열기ToolStripMenuItem.Enabled = 
                    mD5해쉬값구하기ToolStripMenuItem.Enabled = true;
                }
                else
                {
                    삭제ToolStripMenuItem.Enabled=
                    복사ToolStripMenuItem.Enabled =
                    탐색기에서해당파일열기ToolStripMenuItem.Enabled =
                        mD5해쉬값구하기ToolStripMenuItem.Enabled = false;
                }
            }
            catch { }
        }

        private void 파일로웨이브저장ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            int indexing = this.indexing;
            string h = long.Parse(toolStripTextBox1.Text.Replace(",", "")).ToString("##,###");

            saveFileDialog2.Title = string.Format("웨이브 저장... (웨이브 크기: {0})", h);

            saveFileDialog2.FileName = CSharpUtility.Utility.StringUtility.GetDirectoryPath(fm[0].MusicPath, true) +
               CSharpUtility.Utility.StringUtility.GetFileName(fm[0].MusicPath, false) + "_웨이브 데이터 (크기： " + h+")";

            saveFileDialog2.InitialDirectory = CSharpUtility.Utility.StringUtility.GetDirectoryPath(fm[indexing].MusicPath, false);

            DialogResult dr = saveFileDialog2.ShowDialog();
            if (dr == System.Windows.Forms.DialogResult.OK)
            {
                ThreadPool.QueueUserWorkItem(new WaitCallback(파일로웨이브저장));
            }
        }
        void 파일로웨이브저장(object obj)
        {
            string Extend = System.IO.Path.GetExtension(saveFileDialog2.FileName);
            string Path = CSharpUtility.Utility.StringUtility.GetDirectoryPath(saveFileDialog2.FileName, true) + System.IO.Path.GetFileNameWithoutExtension(saveFileDialog2.FileName) + "_좌" + Extend;
            System.IO.StreamWriter sw = new System.IO.StreamWriter(Path);
            if (!syncToolStripMenuItem.Checked) { toolStripMenuItem1.Text = "저장중... "; }
            if (fm[indexing].system != null)
            {
                try
                {
                    toolStripStatusLabel2.Visible = toolStripStatusLabel3.Visible = true;
                    float[] f = new float[long.Parse(toolStripTextBox1.Text.Replace(",", ""))];
                    fm[indexing].system.getWaveData(f, f.Length - 1, 0);
                    //string a = CSharpUtility.Utility.StringUtility.AddString("0", f.LongLength);
                    toolStripProgressBar1.Visible = true;

                    // List<string> b = new List<string>();
                    //System.IO.File.Create(Path);
                    //bool fd =System.IO.File.Exists(Path);
                    toolStripProgressBar1.Maximum = f.Length;
                    //toolStripStatusLabel1.Invalidate();toolStripStatusLabel2.Invalidate();
                    //toolStripStatusLabel1.Text = "저장중... (좌 채널)";
                    for (long i = 0; i < f.LongLength; i++)
                    {
                        if (!BreakLoop)
                        {
                            //statusStrip1.Refresh();
                            toolStripStatusLabel3.Text = string.Format("저장중... ({0}%)[좌 채널 ({1} / {2})] ",
                                (((float)i / (float)f.Length) * 100).ToString("0.00"),
                                i.ToString("##,###"), f.LongLength.ToString("##,###"));

                            this.Text = string.Format(ProgramName + " (디버깅) ({0})", toolStripStatusLabel3.Text);
                            try { toolStripProgressBar1.Value = (int)i; }
                            catch { }
                            sw.Write(f[i].ToString() + "\r\n"/*string.Format("[{0}]{0}\r\n", i.ToString(a), f[i].ToString())*/);
                            //if (syncToolStripMenuItem.Checked) Application.DoEvents();
                        }
                        else { BreakLoop = false; break; }
                    }
                    sw.Close();
                    //System.IO.File.WriteAllLines(saveFileDialog2.FileName + "_좌", b.ToArray());
                    //b.Clear();

                    // toolStripStatusLabel1.Text = "저장중... (우 채널)";
                    fm[0].system.getWaveData(f, f.Length, 1);
                    //a = CSharpUtility.Utility.StringUtility.AddString("0", f.LongLength);
                    Path = CSharpUtility.Utility.StringUtility.GetDirectoryPath(saveFileDialog2.FileName, true) + System.IO.Path.GetFileNameWithoutExtension(saveFileDialog2.FileName) + "_우" + Extend;
                    sw = new System.IO.StreamWriter(Path);
                    toolStripProgressBar1.Maximum = f.Length;
                    for (long i = 0; i < f.LongLength; i++)
                    {
                        if (!BreakLoop)
                        {
                            //statusStrip1.Refresh();
                            toolStripStatusLabel3.Text = string.Format("저장중... ({0}%)[우 채널({1} / {2})]",
                                (((float)i / (float)f.Length) * 100).ToString("0.00"),
                                i.ToString("##,###"), f.LongLength.ToString("##,###"));

                            this.Text = string.Format(ProgramName + " (디버깅) ({0})", toolStripStatusLabel3.Text);
                            try { toolStripProgressBar1.Value = (int)i; }
                            catch { }
                            sw.Write(f[i].ToString() + "\r\n"/*string.Format("[{0}]{1}\r\n", i.ToString(a), f[i].ToString())*/);
                            //if (syncToolStripMenuItem.Checked) Application.DoEvents();
                        }
                        else { break; }
                    }
                }
                finally {
                toolStripStatusLabel3.Text = "";
                sw.Close();
                toolStripStatusLabel2.Visible = toolStripStatusLabel3.Visible = false;
                BreakLoop = false;
                this.Text = ProgramName + " (디버깅)";
                toolStripProgressBar1.Visible = false;
                toolStripStatusLabel1.Text = "준비";}
            }
        }

        private void 재생목록다른이름으로저장ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            saveFileDialog1.InitialDirectory = LastListSaveDirectory;
            saveFileDialog1.ShowDialog();
        }

        private void saveFileDialog2_FileOk(object sender, CancelEventArgs e)
        {
            
        }

        private void saveFileDialog1_FileOk(object sender, CancelEventArgs e)
        {
            LastListSaveDirectory = Path.GetDirectoryName(saveFileDialog1.FileName);
            StringBuilder sb = new StringBuilder("#EXTM3U");
            foreach(ListViewItem a in listView1.Items){ sb.Append("\r\n"+a.SubItems[3].Text);}
            System.IO.File.WriteAllText(saveFileDialog1.FileName, sb.ToString());
        }

        private void 파일FToolStripMenuItem_DropDownOpening(object sender, EventArgs e)
        {
            try { if (fm[0].SizeLength > 0) 
            {
                //saveFileDialog2.Title = string.Format("웨이브 저장... (웨이브 크기: {0})", toolStripTextBox1.Text);
                파일로웨이브저장ToolStripMenuItem.Enabled = true; } 
            }
            catch { }
            if (listView1.Items.Count > 0) { 재생목록다른이름으로저장ToolStripMenuItem.Enabled = true; }
            else { 재생목록다른이름으로저장ToolStripMenuItem.Enabled = false; }
        }

        private void toolStripTextBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!(char.IsDigit(e.KeyChar) || // 숫자만 입력
                e.KeyChar == Convert.ToChar(Keys.Back) || // backspace 키
                e.KeyChar == Convert.ToChar(".")) ||// . 키
                e.KeyChar == Convert.ToChar(Keys.Enter)) // Enter 키
            {
                if (e.KeyChar == Convert.ToChar(Keys.Enter)) { return; }
                e.Handled = true; // 이조건문으로 들어오는것은 키가 안먹게 하는부분
            }
            else
            {
                Input = true; try { toolStripTextBox1.Text = long.Parse(toolStripTextBox1.Text.Replace(",", "")).ToString("##,###"); }
                catch { toolStripTextBox1.Text = DateSize.ToString("##,###"); /*원본사이즈사용ToolStripMenuItem_Click(null, null);*/ }
            }
        }

        bool Input;
        private void 파일로웨이브저장ToolStripMenuItem_DropDownOpening(object sender, EventArgs e)
        {
            if(!Input)toolStripTextBox1.Text = DateSize.ToString("##,###"); 
        }

        int indexing;
        private void 원본사이즈사용ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show(string.Format("원본 데이터 크기: {0} Byte\n\n원본 웨이브 크기: {1} Byte", fm[indexing].SizeLength.ToString("##,###"),
                            fm[indexing].WaveLength.ToString("##,###")), "파일 이름: " + System.IO.Path.GetFileName(fm[indexing].MusicPath));
        }

        private void 정보ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AboutBox1 ab = new AboutBox1(); ab.Show();
        }

        private void Form1_SizeChanged(object sender, EventArgs e)
        {
            timer1_Tick(null, null);
        }

        void Init_Sort(bool MixingMode)
        {
            if (MixingMode)
            {
                for (int i = 0; i < listView1.Items.Count; i++)
                { try { listView1.Items[i].Text = i.ToString(); fm[i].index = i; }catch{} }
            }
            else
            {
                for (int i = 0; i < listView1.Items.Count; i++)
                {
                    if (fm.Count > 0)
                    {
                        listView1.Items[i].Text = i.ToString();
                        if (fm[0].index.ToString() == listView1.Items[i].Text)
                        { listView1.Items[i].SubItems[1].Text = "현재 (" + Status_NoMixing.ToString() + ")"; }
                        else { listView1.Items[i].SubItems[1].Text = ""; }
                    }
                }
            }
        }
        private void timer2_Tick(object sender, EventArgs e)
        {
            Init_Sort(false);
        }
        private void fh_PlayingStatusChanged(FMOD_Helper.FMODHelper.PlayingStatus Status, int index)
        {
            Init_Sort(true);
            //timer2_Tick(null, null);
            listView1.Items[index].SubItems[1].Text = Status.ToString();
        }

        FMOD_Helper.FMODHelper.PlayingStatus Status_NoMixing = FMOD_Helper.FMODHelper.PlayingStatus.Ready;
        private void fh_PlayingStatusChanged_NoMixing(FMOD_Helper.FMODHelper.PlayingStatus Status, int index)
        {
            Status_NoMixing = Status;
            if (Status == FMODHelper.PlayingStatus.Play) button1.Text = "일시정지"; 
            else if(Status == FMODHelper.PlayingStatus.Pause||Status  == FMODHelper.PlayingStatus.Stop) button1.Text = "재생";
            toolStripProgressBar2.Maximum = (int)TotalTick(0).TotalMilliseconds;
            /*
            for (int i = 0; i < listView1.Items.Count; i++)
            {
                if (fm[0].index.ToString() == listView1.Items[i].Text)//index.ToString() == (int.Parse(listView1.Items[i].Text)-1).ToString())
                { listView1.Items[i].SubItems[1].Text = "현재 (" + Status_NoMixing.ToString() + ")"; }
                else { listView1.Items[i].SubItems[1].Text = ""; }
            }*/
            Init_Sort(false);
        }
        public string PlayStatustoString(FMODHelper.PlayingStatus Status)
        {
            switch (Status)
            {
                case FMODHelper.PlayingStatus.Ready:
                    return "준비";
                case FMODHelper.PlayingStatus.Play:
                    return "재생";
                case FMODHelper.PlayingStatus.Stop:
                    return "정지";
                case FMODHelper.PlayingStatus.Pause:
                    return "일시정지";
                default:
                    return "초기화";
            }
        }

        private void 파일복구및재설치ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DialogResult dr = MessageBox.Show("사운드 시스템 엔진 파일 복구 및 재설치를 하려면 프로그램을 재시작하여야 합니다.\n\n" +
                "사운드 시스템 엔진 파일 복구 및 재설치 하시겠습니까?", "복구 및 재설치 알림", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation);
            if (dr == System.Windows.Forms.DialogResult.Yes) 
            {
                System.Diagnostics.Process.Start(Application.ExecutablePath.Replace("/", "\\"), "/RepairSoundSystem");
                System.Diagnostics.Process.GetCurrentProcess().Kill();
            }
        }


        private void toolStripTextBox2_Click(object sender, EventArgs e)
        {
            
        }

        private void toolStripMenuItem2_Click(object sender, EventArgs e)
        {
            원본사이즈사용ToolStripMenuItem_Click(null, null);
        }

        private void toolStripMenuItem1_Click(object sender, EventArgs e)
        {
            파일로웨이브저장ToolStripMenuItem_Click(null, null);
        }

        private void toolStripTextBox2_TextChanged(object sender, EventArgs e)
        {
            toolStripTextBox1.Text = toolStripTextBox2.Text;
        }

        private void toolStripTextBox2_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!(char.IsDigit(e.KeyChar) || // 숫자만 입력
                e.KeyChar == Convert.ToChar(Keys.Back) || // backspace 키
                e.KeyChar == Convert.ToChar(".")) ||// . 키
                e.KeyChar == Convert.ToChar(Keys.Enter)) // Enter 키
            {
                if (e.KeyChar == Convert.ToChar(Keys.Enter)) { return; }
                e.Handled = true; // 이조건문으로 들어오는것은 키가 안먹게 하는부분
                toolStripTextBox2.Text = DateSize.ToString("##,###");
            }
            else
            {
                Input = true; try { toolStripTextBox2.Text = long.Parse(toolStripTextBox2.Text.Replace(",", "")).ToString("##,###"); }
                catch { toolStripTextBox2.Text = DateSize.ToString("##,###") ; }
            }
        }

        private void listView1_DragEnter(object sender, DragEventArgs e)
        {
            /*if (e.Data.GetDataPresent(typeof(System.String)))
            {
                e.Effect = DragDropEffects.Copy;
                return;
            }*/

            if (e.Data.GetDataPresent(DataFormats.FileDrop))
            {
                string[] a = (string[])e.Data.GetData(
                                            DataFormats.FileDrop);
                if (a.Length > 0)
                {
                    e.Effect = DragDropEffects.All;
                    if (System.IO.Path.GetExtension(a[0]).ToLower() == ".m3u")
                    {
                        if (a.Length == 1) { e.Effect = DragDropEffects.All; }
                        else { e.Effect = DragDropEffects.None; }
                    }
                    if (!안전모드ToolStripMenuItem.Checked)
                    {
                        if (System.IO.Path.GetExtension(a[0]).ToLower() == ".mp3"||
                            System.IO.Path.GetExtension(a[0]).ToLower() == ".wma"||
                            System.IO.Path.GetExtension(a[0]).ToLower() == ".wav"||
                            System.IO.Path.GetExtension(a[0]).ToLower() == ".ogg"||
                            System.IO.Path.GetExtension(a[0]).ToLower() == ".flac")
                        {
                            e.Effect = DragDropEffects.All;
                        }
                        else { e.Effect = DragDropEffects.None; }
                    }
                }
            }
            else                                    // 아니면 이펙트를 모두 꺼서 금지표시가 나타나게 됨
                e.Effect = DragDropEffects.None;
        }

        private void 높음ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            높음ToolStripMenuItem.Checked = 높음ToolStripMenuItem1.Checked = true;
            높은우선순위ToolStripMenuItem.Checked = 높은우선순위ToolStripMenuItem1.Checked = false;
            보통권장ToolStripMenuItem.Checked = 보통권장ToolStripMenuItem1.Checked = false;
            낮은우선순위ToolStripMenuItem.Checked = 낮은우선순위ToolStripMenuItem1.Checked = false;
            System.Diagnostics.Process.GetCurrentProcess().PriorityClass = System.Diagnostics.ProcessPriorityClass.High;
        }

        private void 높은우선순위ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.GetCurrentProcess().PriorityClass = System.Diagnostics.ProcessPriorityClass.AboveNormal;
            높음ToolStripMenuItem.Checked = 높음ToolStripMenuItem1.Checked = false;
            높은우선순위ToolStripMenuItem.Checked = 높은우선순위ToolStripMenuItem1.Checked = true;
            보통권장ToolStripMenuItem.Checked = 보통권장ToolStripMenuItem1.Checked = false;
            낮은우선순위ToolStripMenuItem.Checked = 낮은우선순위ToolStripMenuItem1.Checked = false;
        }

        private void 보통권장ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.GetCurrentProcess().PriorityClass = System.Diagnostics.ProcessPriorityClass.Normal;
            높음ToolStripMenuItem.Checked = 높음ToolStripMenuItem1.Checked = false;
            높은우선순위ToolStripMenuItem.Checked = 높은우선순위ToolStripMenuItem1.Checked = false;
            보통권장ToolStripMenuItem.Checked = 보통권장ToolStripMenuItem1.Checked = true;
            낮은우선순위ToolStripMenuItem.Checked = 낮은우선순위ToolStripMenuItem1.Checked = false;
        }

        private void 낮은우선순위ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.GetCurrentProcess().PriorityClass = System.Diagnostics.ProcessPriorityClass.BelowNormal;
            높음ToolStripMenuItem.Checked = 높음ToolStripMenuItem1.Checked = false;
            높은우선순위ToolStripMenuItem.Checked = 높은우선순위ToolStripMenuItem1.Checked = false;
            보통권장ToolStripMenuItem.Checked = 보통권장ToolStripMenuItem1.Checked = false;
            낮은우선순위ToolStripMenuItem.Checked = 낮은우선순위ToolStripMenuItem1.Checked = true;
        }

        private void button5_Click(object sender, EventArgs e)
        {
            일시정지ToolStripMenuItem_Click(null, null);
        }

        System.Diagnostics.Process Process_LOL;
        private void 설정ToolStripMenuItem_DropDownOpening(object sender, EventArgs e)
        {
            foreach (System.Diagnostics.Process p in System.Diagnostics.Process.GetProcesses())
            {
                if (p.ProcessName == "League of Legends")
                {
                    string a= p.MainModule.FileName.ToLower();
                    if (a.IndexOf("solutions\\lol_game_client_sln\\releases") != -1&&
                        a.IndexOf("deploy\\league of legends.exe")!=-1)
                    {
                        Process_LOL = p;
                        LOL우선순위ToolStripMenuItem.Enabled =
                        LOL높은우선순위ToolStripMenuItem.Enabled =
                        LOL보통권장ToolStripMenuItem.Enabled = true;
                        if (p.PriorityClass == System.Diagnostics.ProcessPriorityClass.AboveNormal)
                        {
                            LOL높은우선순위ToolStripMenuItem.Enabled =true;
                            LOL보통권장ToolStripMenuItem.Enabled = false;
                        }
                        else if (p.PriorityClass == System.Diagnostics.ProcessPriorityClass.Normal)
                        {
                            LOL높은우선순위ToolStripMenuItem.Enabled = true;
                            LOL보통권장ToolStripMenuItem.Enabled = false;
                        }
                        break;
                    }
                    else
                    {
                        LOL우선순위ToolStripMenuItem.Enabled =
                        LOL높은우선순위ToolStripMenuItem.Enabled =
                        LOL보통권장ToolStripMenuItem.Enabled = false;
                    }
                }
            }
        }

        private void 높은우선순위ToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Process_LOL.PriorityClass = System.Diagnostics.ProcessPriorityClass.AboveNormal;
            보통권장ToolStripMenuItem.Checked = false;
            LOL높은우선순위ToolStripMenuItem.Checked = true;
        }

        private void 보통권장ToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Process_LOL.PriorityClass = System.Diagnostics.ProcessPriorityClass.Normal;
            보통권장ToolStripMenuItem.Checked = true;
            LOL높은우선순위ToolStripMenuItem.Checked = false;
        }

        private void listView1_DragDrop(object sender, DragEventArgs e)
        {
            if (e.KeyState == 8)
            {
                
            }
            else
            {
                if (e.Data.GetDataPresent(DataFormats.FileDrop))
                {
                    OpenDialog = false;
                    string[] a = (string[])e.Data.GetData(
                                        DataFormats.FileDrop);

                    //if (!음악믹싱모드ToolStripMenuItem.Checked)
                    StringBuilder ErrFile = new StringBuilder();
                    for (int i = 0; i < a.Length; i++)
                    {
                        if (System.IO.Path.GetExtension(a[i]).ToLower() == ".m3u")
                        {
                            //OpenDialog = true;
                            openFileDialog2.FileName = a[i];
                            재생목록열기ToolStripMenuItem_Click(null, null);
                        }
                        else
                        {
                            //e.Effect = DragDropEffects.All;
                            if (!음악믹싱모드ToolStripMenuItem.Checked)
                            {/*
                                if (System.IO.Path.GetExtension(a[i]).ToLower() == ".mp3" ||
                                    System.IO.Path.GetExtension(a[i]).ToLower() == ".ogg" ||
                                    System.IO.Path.GetExtension(a[i]).ToLower() == ".wav" ||
                                    System.IO.Path.GetExtension(a[i]).ToLower() == ".wma")*/
                                {
                                    openFileDialog1.FileName = a[i];
                                    파일열기ToolStripMenuItem_Click(null, null);
                                }
                                //else { ErrFile.AppendLine(a[i]); }
                            }
                            #region
                            else
                            {
                                //StringBuilder ErrFile = new StringBuilder();
                                try
                                {
                                    FMOD_Helper.FMODHelper fh = new FMOD_Helper.FMODHelper(a[i]);
                                    fh.PlayingStatusChanged += new FMOD_Helper.FMODHelper.PlayingStatusChangedEventHandler(fh_PlayingStatusChanged);
                                    fh.MusicPathChanged+=new FMODHelper.MusicPathChangedEventHandler(fh_MusicPathChanged);
                                    if (fh.HelperEx.FMODResult != FMOD.RESULT.ERR_FILE_BAD ||
                                        fh.HelperEx.FMODResult != FMOD.RESULT.ERR_FORMAT)
                                    {
                                        //if (MixingMode)
                                        {
                                            //timer1.Start();
                                            fm.Add(fh);
                                            fh.Loop = true;
                                            timer1.Start();
                                            fh.index = fm.Count - 1;
                                            ListViewItem li = new ListViewItem(fh.index.ToString());
                                            li.SubItems.Add(fh.PlayStatus.ToString());
                                            li.SubItems.Add(System.IO.Path.GetFileName(fh.MusicPath));
                                            li.SubItems.Add(fh.MusicPath);
                                            li.SubItems.Add("");
                                            listView1.Items.Add(li);
                                            ps1.Add(new PlayerSetting(fh, System.IO.Path.GetFileName(fh.MusicPath)));
                                        }
                                    }
                                    else { throw new Exception(); }
                                }
                                catch { ErrFile.AppendLine("    " + a[i]); }
                            }
                            #endregion
                        }
                    }
                    OpenDialog = true;

                    if (ErrFile.Length > 0)
                    {
                        MessageBox.Show("일부항목은 올바르지 않거나 손상된 음악 파일 이므로 추가하지 못했습니다.\n\n추가 못한 경로:\n" + ErrFile.ToString(),
                            "일부 항목 추가실패!");
                    }

                    // else { e.Effect = DragDropEffects.None; }

                }

            }
        }

        private void 안전모드ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (안전모드ToolStripMenuItem.Checked)
            {
                안전모드ToolStripMenuItem.Checked = false;
                openFileDialog1.Filter =
                    "지원하는 모든 음악 파일|*.mp3; *.ogg; *.wav; *.wma; *.flac; *.aif; *.aiff; *.mid|" +
                    "MP3 Audio 파일 (*.mp3)|*.mp3|" +
                    "Windows Media Audio 파일 (*.wma)|*.wma|" +
                    "Wave (Microsoft) 파일 (*.wav)|*.wav|" +
                    "OggVorbis 파일 (*.ogg)|*.ogg|" +
                    "Free Lossless Audio Codec 파일 (*.flac)|*.flac|" +
                    "Audio Interchange File Format 파일 (*.aif, *.aiff)|*.aif; *.aiff|" +
                    "Standard MIDI 파일 (*.mid)|*.mid|" +
                    "지원하는 모든 특수 파일|*.fsb; *.wax; *.asf; *.asx; *.dls|" +
                    "FMOD's Sample Bank 파일 (*.fsb)|*.fsb|" +
                    "Windows Media Audio Redirector 파일 (*.wax)|*.wax|" +
                    "네트워크 스트리밍 파일 (*.asf, *.asx, *.dls)|*.asf; *.asx; *.dls";
            }
            else
            {
                DialogResult dr = MessageBox.Show("안전모드를 사용하면 모든파일을 다 선택할수 있지만\n\n여는데 시간이 많이 걸립니다. 안전모드를 사용하시겠습니까?", 
                    "안전모드 사용", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                if (dr == System.Windows.Forms.DialogResult.Yes)
                {
                    안전모드ToolStripMenuItem.Checked = true;
                    openFileDialog1.Filter =
                    "지원하는 모든 음악 파일|*.mp3; *.ogg; *.wav; *.wma; *.flac; *.aif; *.aiff; *.mid|" +
                    "MP3 Audio 파일 (*.mp3)|*.mp3|" +
                    "Windows Media Audio 파일 (*.wma)|*.wma|" +
                    "Wave (Microsoft) 파일 (*.wav)|*.wav|" +
                    "OggVorbis 파일 (*.ogg)|*.ogg|" +
                    "Free Lossless Audio Codec 파일 (*.flac)|*.flac|" +
                    "Audio Interchange File Format 파일 (*.aif, *.aiff)|*.aif; *.aiff|" +
                    "Standard MIDI 파일 (*.mid)|*.mid|" +
                    "지원하는 모든 특수 파일|*.fsb; *.wax; *.asf; *.asx; *.dls|" +
                    "FMOD's Sample Bank 파일 (*.fsb)|*.fsb|" +
                    "Windows Media Audio Redirector 파일 (*.wax)|*.wax|" +
                    "네트워크 스트리밍 파일 (*.asf, *.asx, *.dls)|*.asf; *.asx; *.dls|"+
                    "모든 파일 (*.*)|*.*";
                }
            }
            if (안전모드ToolStripMenuItem.Checked)
            {
                if (IsDebugging)
                {
                    this.Text = ProgramName+" (디버깅, 안전모드)";
                    toolStripStatusLabel1.Text ="준비 (디버깅, 안전모드)";

                }
                else
                {
                    this.Text = ProgramName+" (안전모드)";
                    toolStripStatusLabel1.Text = "준비 (안전모드)";
                }
            }
            else
            {
                if (IsDebugging)
                {
                    this.Text = ProgramName+" (디버깅)";
                    toolStripStatusLabel1.Text = "준비 (디버깅)";
                }
                else
                {
                    this.Text = ProgramName;
                    toolStripStatusLabel1.Text = "준비";
                }
            }
        }

        bool Closing;
        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (!Closing)
            {
                e.Cancel = true;
                DialogResult dr = MessageBox.Show("정말 이 프로그램을 종료하시겠습니까?", "프로그램 종료 알림", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                if (dr == System.Windows.Forms.DialogResult.Yes) { /*fm[0].Dispose();*/ notifyIcon1.Dispose(); e.Cancel = false; Process.GetCurrentProcess().Kill(); }
            }
            else { e.Cancel = false; }
        }

        private void 끝내기ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void 디버깅ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DialogResult dr= MessageBox.Show("디버깅을 하면 이 프로그램은 즉시 멈추고 디버거를 호출합니다.\n\n"+
                "디버거가 무엇인지 모르거나 없는 컴퓨터는 사용하지 말아주시기 바랍니다.\n\n개발자용으로만 설계되었습니다. 정말 디버깅을 하시겠습니까?","디버깅 경고",
                MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
            if (dr == System.Windows.Forms.DialogResult.Yes)
            {
                if (dr == System.Windows.Forms.DialogResult.Yes){Debugger.Launch();}
                if (안전모드ToolStripMenuItem.Checked)
                {
                    if (IsDebugging) { this.Text = ProgramName + " (디버깅, 안전모드)"; toolStripStatusLabel1.Text = "준비 (디버깅, 안전모드)"; }
                    else { this.Text = ProgramName + " (안전모드)"; toolStripStatusLabel1.Text = "준비 (안전모드)"; }
                }
                else
                {
                    if (IsDebugging) { this.Text = ProgramName + " (디버깅)"; toolStripStatusLabel1.Text = "준비 (디버깅)"; }
                    else { this.Text = ProgramName; toolStripStatusLabel1.Text = "준비"; }
                }
            }

        }

        private void 도움말HToolStripMenuItem_DropDownOpening(object sender, EventArgs e)
        {
            if (IsDebugging) 
            { 디버깅ToolStripMenuItem.Enabled = false;
              디버깅ToolStripMenuItem.ToolTipText = "디버깅 중이므로 비활성화 되었습니다.";}
            else 
            {디버깅ToolStripMenuItem.Enabled = true;
             디버깅ToolStripMenuItem.ToolTipText = "디버깅을 시작합니다.";}
        }

        private void syncToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (syncToolStripMenuItem.Checked)
            {
                DialogResult dr = MessageBox.Show("ASync 모드를 해제하게되면 진행중인 작업" +
                    "\n\n예) 여러파일 및 재생목록 열기, 웨이브 데이터 저장하기, 여러 항목 선택하기\n\n" +
                    "이 완료될때까지 이 프로그램이 응답하지 않을 수 있습니다. (작업 중이었다면 즉시 적용됩니다.)\n\n" +
                    "대신 속도가 조금 빨라집니다. 상황에 맞게 사용하십시오.\n\n정말로 ASync 모드를 해제하시겠습니까?",
                    "Async 모드 해제 알림", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                if (dr == System.Windows.Forms.DialogResult.Yes) { syncToolStripMenuItem.Checked = false; }
            }
            else { syncToolStripMenuItem.Checked = true; }
        }

        private void listView1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {if (button1.Enabled) button1_Click(null, null);}
            if (e.KeyChar == 6)
            {
                if (listView1.SelectedItems.Count == 1) { 가사찾기ToolStripMenuItem_Click(null, null); }
                else { 싱크가사찾기ToolStripMenuItem_Click(null, null); }
            }
        }

        private void listView1_KeyUp(object sender, KeyEventArgs e)
        {
            if (listView1.Items.Count > 0)
            {
                //MessageBox.Show(e.KeyData.ToString(), e.KeyValue.ToString());
                /*
                if (e.KeyValue == 32) //Space 키
                {if (button1.Enabled) button1_Click(null, null);}*/
                if (e.KeyValue == 46)//Delete 키
                { if (listView1.SelectedItems.Count > 0) { 삭제ToolStripMenuItem_Click(null, null); } }
                else if (e.KeyValue == 13) { 재생ToolStripMenuItem_Click(null, null); }
            }
        }

        private void listView1_DoubleClick(object sender, EventArgs e)
        {
            if (listView1.SelectedItems.Count == 1){ 재생ToolStripMenuItem_Click(null, null); }
        }

        private void 프로그램다시시작ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DialogResult dr = MessageBox.Show("정말 프로그램을 다시 시작 하시겠습니까?", "프로그램 다시 시작", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
            if (dr == System.Windows.Forms.DialogResult.Yes)
            {
                //ps.Helper.Restart();
                //button1.Text = "재생";
                Process p = System.Diagnostics.Process.GetCurrentProcess();
                /*
                Dictionary<string, string> a = new Dictionary<string, string>();
                foreach (System.Collections.DictionaryEntry a1 in p.StartInfo.EnvironmentVariables)
                { a.Add(a1.Key.ToString(), a1.Value.ToString());}*/
                Process.Start(p.MainModule.FileName, p.StartInfo.Arguments);
                p.Kill();
            }
        }



        Dictionary<string, string> LastPath = new Dictionary<string, string>();
        string LastFolderOpenDirectory
        {
            get
            {
                if (File.Exists(Application.StartupPath + "\\LastPath.ini"))
                {
                    string[] a = File.ReadAllLines(Application.StartupPath + "\\LastPath.ini");
                    string a1 = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
                    if (LastPath.Count == 0)LastPath=CSharpUtility.Utility.EtcUtility.LoadSetting(a);
                    try { a1 = CSharpUtility.Utility.EtcUtility.LoadSetting(a)["LastFolderOpenDirectory"]; }
                    catch { a1 = Environment.GetFolderPath(Environment.SpecialFolder.Desktop); }
                    /*
                    try { LastPath["LastFileOpenDirectory"] = a1; }
                    catch { LastPath.Add("LastFileOpenDirectory", a1); }
                    try { string c = LastPath["LastListOpenDirectory"]; }
                    catch { LastPath.Add("LastListOpenDirectory", a1); }
                    try { string c = LastPath["LastListSaveDirectory"]; }
                    catch { LastPath.Add("LastListSaveDirectory", a1); }*/
                    if (a1 != null || a1 != "") { return Directory.Exists(a1) ? a1 : Environment.GetFolderPath(Environment.SpecialFolder.Desktop); }
                    else { return Environment.GetFolderPath(Environment.SpecialFolder.Desktop); }
                }
                else { return Environment.GetFolderPath(Environment.SpecialFolder.Desktop); }
            }
            set
            {
                /*
                if (LastPath.Count < 2)
                {
                    LastPath.Add("LastFileOpenDirectory", value);
                    LastPath.Add("LastListOpenDirectory", Environment.GetFolderPath(Environment.SpecialFolder.Desktop));
                    LastPath.Add("LastListSaveDirectory", Environment.GetFolderPath(Environment.SpecialFolder.Desktop));
                }*/
                try { LastPath["LastFolderOpenDirectory"] = value; }
                catch { LastPath.Add("LastFolderOpenDirectory", value); }
                File.WriteAllLines(Application.StartupPath + "\\LastPath.ini", CSharpUtility.Utility.EtcUtility.SaveSetting(LastPath));
            }
        }
        string LastFileOpenDirectory
        {
            get
            {
                if (File.Exists(Application.StartupPath + "\\LastPath.ini"))
                {
                    string[] a = File.ReadAllLines(Application.StartupPath + "\\LastPath.ini");
                    string a1 = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
                    if (LastPath.Count == 0) LastPath=CSharpUtility.Utility.EtcUtility.LoadSetting(a);
                    try { a1 = CSharpUtility.Utility.EtcUtility.LoadSetting(a)["LastFileOpenDirectory"]; }
                    catch { a1 = Environment.GetFolderPath(Environment.SpecialFolder.Desktop); }
                    /*
                    try { LastPath["LastFileOpenDirectory"] = a1; }
                    catch { LastPath.Add("LastFileOpenDirectory", a1); }
                    try { string c = LastPath["LastListOpenDirectory"]; }
                    catch { LastPath.Add("LastListOpenDirectory", a1); }
                    try { string c = LastPath["LastListSaveDirectory"]; }
                    catch { LastPath.Add("LastListSaveDirectory", a1); }
                     * */
                    if (a1 != null || a1 != "") { return Directory.Exists(a1) ? a1 : Environment.GetFolderPath(Environment.SpecialFolder.Desktop); }
                    else { return Environment.GetFolderPath(Environment.SpecialFolder.Desktop); }
                }
                else { return Environment.GetFolderPath(Environment.SpecialFolder.Desktop); }
            }
            set
            {
                /*
                if (LastPath.Count < 2)
                {
                    LastPath.Add("LastFileOpenDirectory", value);
                    LastPath.Add("LastListOpenDirectory", Environment.GetFolderPath(Environment.SpecialFolder.Desktop));
                    LastPath.Add("LastListSaveDirectory", Environment.GetFolderPath(Environment.SpecialFolder.Desktop));
                }*/
                try { LastPath["LastFileOpenDirectory"] = value; }
                catch { LastPath.Add("LastFileOpenDirectory", value); }
                File.WriteAllLines(Application.StartupPath + "\\LastPath.ini", CSharpUtility.Utility.EtcUtility.SaveSetting(LastPath));
            }
        }
        string LastListOpenDirectory
        {
            get
            {
                if (File.Exists(Application.StartupPath + "\\LastPath.ini"))
                {
                    string[] a = File.ReadAllLines(Application.StartupPath + "\\LastPath.ini");
                    string a1 = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
                    if (LastPath.Count == 0) LastPath=CSharpUtility.Utility.EtcUtility.LoadSetting(a);
                    try { a1 = CSharpUtility.Utility.EtcUtility.LoadSetting(a)["LastListOpenDirectory"]; }
                    catch { a1 = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);  }
                    /*
                    try { LastPath["LastListOpenDirectory"] = a1; }
                    catch { LastPath.Add("LastListOpenDirectory", a1); }
                    try { string c=LastPath["LastListSaveDirectory"]; }
                    catch { LastPath.Add("LastListSaveDirectory", a1); }
                    try { string c = LastPath["LastFileOpenDirectory"]; }
                    catch { LastPath.Add("LastFileOpenDirectory", a1); }*/
                    if (a1 != null || a1 != "") { return Directory.Exists(a1) ? a1 : Environment.GetFolderPath(Environment.SpecialFolder.Desktop); }
                    else { return Environment.GetFolderPath(Environment.SpecialFolder.Desktop); }
                }
                else { return Environment.GetFolderPath(Environment.SpecialFolder.Desktop); }
            }
            set
            {
                /*
                if (LastPath.Count < 3)
                {
                    LastPath.Clear(); LastPath.Add("LastFileOpenDirectory", Environment.GetFolderPath(Environment.SpecialFolder.Desktop));
                    LastPath.Add("LastListOpenDirectory", value);
                    LastPath.Add("LastListSaveDirectory", Environment.GetFolderPath(Environment.SpecialFolder.Desktop));
                }
                else { LastPath["LastListOpenDirectory"] = value; }*/
                try { LastPath["LastListOpenDirectory"] = value; }
                catch { LastPath.Add("LastListOpenDirectory", value); }
                File.WriteAllLines(Application.StartupPath + "\\LastPath.ini", CSharpUtility.Utility.EtcUtility.SaveSetting(LastPath));
            }
        }
        string LastListSaveDirectory
        {
            get
            {
                if (File.Exists(Application.StartupPath + "\\LastPath.ini"))
                {
                    string[] a = File.ReadAllLines(Application.StartupPath + "\\LastPath.ini");
                    string a1 = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
                    if (LastPath.Count == 0) CSharpUtility.Utility.EtcUtility.LoadSetting(a);
                    try { a1 = CSharpUtility.Utility.EtcUtility.LoadSetting(a)["LastListSaveDirectory"]; }
                    catch { a1 = Environment.GetFolderPath(Environment.SpecialFolder.Desktop); }
                    /*
                    try { LastPath["LastListSaveDirectory"] = a1; }
                    catch { LastPath.Add("LastListSaveDirectory", a1); }
                    try {string c=LastPath["LastFileOpenDirectory"]; }
                    catch { LastPath.Add("LastFileOpenDirectory", a1); }
                    try { string c = LastPath["LastListOpenDirectory"]; }
                    catch { LastPath.Add("LastListOpenDirectory", a1); }*/
                    if (a1 != null || a1 != "") { return Directory.Exists(a1) ? a1 : Environment.GetFolderPath(Environment.SpecialFolder.Desktop); }
                    else { return Environment.GetFolderPath(Environment.SpecialFolder.Desktop); }
                }
                else { return Environment.GetFolderPath(Environment.SpecialFolder.Desktop); }
            }
            set
            {
                /*
                if (LastPath.Count < 3)
                {
                    LastPath.Clear(); LastPath.Add("LastFileOpenDirectory", Environment.GetFolderPath(Environment.SpecialFolder.Desktop));
                    LastPath.Add("LastListOpenDirectory", Environment.GetFolderPath(Environment.SpecialFolder.Desktop));
                    LastPath.Add("LastListSaveDirectory", value);
                }
                else { LastPath["LastListSaveDirectory"] = value; }*/
                try { LastPath["LastListSaveDirectory"] = value; }
                catch { LastPath.Add("LastListSaveDirectory", value); }
                File.WriteAllLines(Application.StartupPath + "\\LastPath.ini", CSharpUtility.Utility.EtcUtility.SaveSetting(LastPath));
            }
        }

        private void 업데이트변경사항보기ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ViewReadme vr = new ViewReadme(); vr.Show();
        }

        private void 음악이름ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            StringBuilder sb = new StringBuilder(listView1.SelectedItems[0].SubItems[2].Text);
            for (int i = 1; i < listView1.SelectedItems.Count; i++)
            {sb.Append("\r\n"+listView1.SelectedItems[i].SubItems[2].Text); }
            Clipboard.SetText(sb.ToString());
        }

        private void 음악경로ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            StringBuilder sb = new StringBuilder(listView1.SelectedItems[0].SubItems[3].Text);
            for (int i = 1; i < listView1.SelectedItems.Count; i++)
            { sb.Append("\r\n" + listView1.SelectedItems[i].SubItems[3].Text); }
            Clipboard.SetText(sb.ToString());
        }

        private void 탐색기에서해당파일열기ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                string a = Environment.GetEnvironmentVariable("windir") + "\\explorer.exe";
                for (int i = 0; i < listView1.SelectedItems.Count; i++)
                {Process.Start(a, "/select," + listView1.SelectedItems[i].SubItems[3].Text);}
            }
            catch { }
        }

        private void 싱크가사찾기ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Lyrics.frmLyricsSearch frmlrcSearch = new Lyrics.frmLyricsSearch(); frmlrcSearch.SetForm1Class(this); frmlrcSearch.Show();
        }

        internal Lyrics.frmLyricsSearch frmlrcSearch = new Lyrics.frmLyricsSearch();
        private void 가사찾기ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Lyrics.frmLyricsSearch frmlrcSearch = new Lyrics.frmLyricsSearch(listView1.SelectedItems[0].SubItems[3].Text);
            frmlrcSearch.SetForm1Class(this); frmlrcSearch.Show();
        }

        private void 바탕화면싱크가사창띄우기ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try { timer3.Start(); lrcf.Show(); lrcf.BringToFront(); }
            catch { MessageBox.Show("바탕 화면 싱크가사를 띄울수 없습니다.\n\n프로그램을 재 시작 해주세요."); }
        }


        public void frmlrcSearch_GetLyricsComplete(Lyrics.LyricsClass lc)
        {
            try 
            {
                if (th != null) th.Abort();
                Lyrics.LyricsEz a = FMOD_Audio.Lyrics.LyricsEz.ConvertLyricsClassToLyricsArray(lc)[0];
                lrcf.Lyrics = a;
                if (싱크가사로컬캐시ToolStripMenuItem.Checked) Lyricscache.SetLyrics(a);
                //MessageBox.Show(a.LyricsInfo.ToString(), lc.MD5Hash);

                //ths = new System.Threading.T2hreadStart(th_Tick);
                //th = new System.Threading.Thread(ths);
                //th.Start();
            } 
            catch { } 
        }
        public void frmlrcSearch_GetLyricsComplete(Lyrics.LyricsEz lc)
        {
            try
            {
                if (th != null) th.Abort();
                lrcf.Lyrics = lc;
                if (싱크가사로컬캐시ToolStripMenuItem.Checked) Lyricscache.SetLyrics(lc);
                //MessageBox.Show(lc.LyricsInfo.ToString(), lc.MD5Hash);

                //ths = new System.Threading.T2hreadStart(th_Tick);
                //th = new System.Threading.Thread(ths);
                //th.Start();
            }
            catch { }
        }

        void fh_MusicPathChanged_NoMixing(string MusicPath, int index)
        {
            this.lrcf.MusicPath = MusicPath;
            try
            {
                string Hash = Lyrics.GetLyrics.GenerateMusicHash(MusicPath);
                string a = Lyricscache.GetLyrics(Hash);
                lrcf.MD5Hash = Lyricscache.MD5Hash_Music = Hash;
                lrcf.Lyric = "";
                if (a == null || a == "") frmlrcSearch.FindLyrics(MusicPath);
                else lrcf.Lyric = a;
            }
            catch { frmlrcSearch.FindLyrics(MusicPath); }
            
        }
        void fh_MusicPathChanged(string MusicPath, int index)
        {

        }

        System.Threading.Thread th;
        System.Threading.ThreadStart ths;
        private void th_Tick()
        {
            //lrcf.GetLyric(fm[0].CurrentPosition);
            //th.Start();
        }

        private void lrcf_FormStatusChange(FMOD_Audio.Lyrics.frmLyricsDesktop.FormStatus Status)
        { if (Status != Lyrics.frmLyricsDesktop.FormStatus.Show)timer3.Stop(); }
        private void lrcf_UpdateLyricsIntervalChange(int Uptime)
        { timer3.Interval = Uptime; }

        bool lrcStop;
        private void timer3_Tick(object sender, EventArgs e)
        {
            try { lrcf.GetLyric(fm[0].CurrentPosition * 0.001d); }catch {  }
        }

        private void timer4_Tick(object sender, EventArgs e)
        {
            MessageBox.Show("");
        }

        private void 맨위로설정ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.TopMost = 맨위로설정ToolStripMenuItem.Checked;
        }

        private void 상태표시줄업데이트ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (안전모드ToolStripMenuItem.Checked)
            {
                if (IsDebugging)
                {
                    this.Text = ProgramName + " (디버깅, 안전모드)";
                    toolStripStatusLabel1.Text = "준비 (디버깅, 안전모드)";

                }
                else
                {
                    this.Text = ProgramName + " (안전모드)";
                    toolStripStatusLabel1.Text = "준비 (안전모드)";
                }
            }
            else
            {
                if (IsDebugging)
                {
                    this.Text = ProgramName + " (디버깅)";
                    toolStripStatusLabel1.Text = "준비 (디버깅)";
                }
                else
                {
                    this.Text = ProgramName;
                    toolStripStatusLabel1.Text = "준비";
                }
            }
            //toolStripProgressBar1.Visible = toolStripStatusLabel2.Visible=false;
            if (!tmpStop) toolStripProgressBar1.Visible = toolStripStatusLabel2.Visible = toolStripStatusLabel2.Visible = false; 
            else toolStripProgressBar1.Visible = true;
            if (fh != null && fh.channel != null)
            {
                toolStripProgressBar2.Visible = true;
                toolStripStatusLabel1.Text = "준비 [00:00:00:000 / 00:00:00:000]";
            }
            else { toolStripProgressBar1.Visible = toolStripStatusLabel2.Visible = toolStripStatusLabel2.Visible = false;
                   toolStripStatusLabel1.Text = "준비";}
        }

        private void 파일열기ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            openFileDialog1.InitialDirectory = LastFileOpenDirectory;
            DialogResult dr = DialogResult.OK;
            if (OpenDialog) { dr = openFileDialog1.ShowDialog(); }
            if (dr == System.Windows.Forms.DialogResult.OK)
            {
                LastFileOpenDirectory = Path.GetDirectoryName(openFileDialog1.FileName);
                tmpStop = true;
                StringBuilder sb = new StringBuilder();
                if (!음악믹싱모드ToolStripMenuItem.Checked)
                {
                    try
                    {
                        toolStripProgressBar1.Maximum = openFileDialog1.FileNames.Length;
                        toolStripProgressBar1.Visible = toolStripStatusLabel2.Visible = true;
                        for (int i = 0; i < openFileDialog1.FileNames.Length; i++)
                        {
                            bool Error = false;
                            statusStrip1.Refresh();
                            try
                            {
                                if (안전모드ToolStripMenuItem.Checked)
                                {
                                    if (IsDebugging)
                                    {
                                        this.Text = string.Format(ProgramName + " (디버깅, 안전모드) [{0}번째 파일 여는중... 이름: {1} / 경로: {2}]", (i + 1).ToString(), System.IO.Path.GetFileName(openFileDialog1.FileNames[i]), openFileDialog1.FileNames[i]);
                                        toolStripStatusLabel1.Text = string.Format("{0}번째 파일 여는중... (디버깅, 안전모드) [이름: {1} / 경로: {2}]", (i + 1).ToString(), System.IO.Path.GetFileName(openFileDialog1.FileNames[i]), openFileDialog1.FileNames[i]);

                                    }
                                    else
                                    {
                                        this.Text = string.Format(ProgramName + " (안전모드) [{0}번째 파일 여는중... 이름: {1} / 경로: {2}]", (i + 1).ToString(), System.IO.Path.GetFileName(openFileDialog1.FileNames[i]), openFileDialog1.FileNames[i]);
                                        toolStripStatusLabel1.Text = string.Format("{0}번째 파일 여는중... (안전모드) [이름: {1} / 경로: {2}]", (i + 1).ToString(), System.IO.Path.GetFileName(openFileDialog1.FileNames[i]), openFileDialog1.FileNames[i]);
                                    }
                                }
                                else
                                {
                                    if (IsDebugging)
                                    {
                                        this.Text = string.Format(ProgramName + " (디버깅) [{0}번째 파일 여는중... 이름: {1} / 경로: {2}]", (i + 1).ToString(), System.IO.Path.GetFileName(openFileDialog1.FileNames[i]), openFileDialog1.FileNames[i]);
                                        toolStripStatusLabel1.Text = string.Format("{0}번째 파일 여는중... (디버깅) [이름: {1} / 경로: {2}]", (i + 1).ToString(), System.IO.Path.GetFileName(openFileDialog1.FileNames[i]), openFileDialog1.FileNames[i]);
                                    }
                                    else
                                    {
                                        this.Text = string.Format(ProgramName + " (디버깅) [{0}번째 파일 여는중... 이름: {1} / 경로: {2}]", (i + 1).ToString(), System.IO.Path.GetFileName(openFileDialog1.FileNames[i]), openFileDialog1.FileNames[i]);
                                        toolStripStatusLabel1.Text = string.Format("{0}번째 파일 여는중... [이름: {1} / 경로: {2}]", (i + 1).ToString(), System.IO.Path.GetFileName(openFileDialog1.FileNames[i]), openFileDialog1.FileNames[i]);
                                    }
                                }
                                try { toolStripProgressBar1.Value = i; }
                                catch { toolStripProgressBar1.Value = toolStripProgressBar1.Maximum; }
                                if (안전모드ToolStripMenuItem.Checked)
                                {
                                    toolStripProgressBar1.Visible =
                                    toolStripStatusLabel2.Visible = true;
                                    if (BreakLoop) { break; }
                                    FMOD_Helper.FMODHelper fh = new FMOD_Helper.FMODHelper(openFileDialog1.FileNames[i]);
                                    if (fh.Result == FMOD.RESULT.ERR_FORMAT ||
                                        fh.Result.ToString().IndexOf("ERR_FILE_") != -1)
                                    { throw new SoundSystemException(fh.Result); }
                                    fh.Dispose();
                                }
                                //timer1.Start();
                                ListViewItem li = new ListViewItem(listView1.Items.Count.ToString());
                                if (listView1.Items.Count == 0)
                                {
                                    fh = new FMOD_Helper.FMODHelper();
                                    fh.PlayingStatusChanged += new FMOD_Helper.FMODHelper.PlayingStatusChangedEventHandler(fh_PlayingStatusChanged_NoMixing);
                                    fh.PlayingStatusChanged += new FMODHelper.PlayingStatusChangedEventHandler(lrcf.PlayingStatusChanged);
                                    fh.MusicPathChanged += new FMODHelper.MusicPathChangedEventHandler(fh_MusicPathChanged_NoMixing);
                                    fh.MusicPath = openFileDialog1.FileNames[0];
                                    ps = new PlayerSetting(fh, System.IO.Path.GetFileName(openFileDialog1.FileNames[0]), this);
                                    fm.Add(fh);
                                    fh.index = listView1.Items.Count;
                                    li.SubItems.Add("현재 (Ready)");
                                    if (ps == null) { ps = new PlayerSetting(fh, System.IO.Path.GetFileName(fh.MusicPath)); }
                                    ps.Title = string.Format("{0}", System.IO.Path.GetFileName(fh.MusicPath));
                                    button1.Enabled = button2.Enabled = button4.Enabled = true;
                                }
                                else { li.SubItems.Add(""); }
                                li.SubItems.Add(System.IO.Path.GetFileName(openFileDialog1.FileNames[i]));
                                li.SubItems.Add(openFileDialog1.FileNames[i]);
                                li.SubItems.Add("");
                                listView1.Items.Add(li);
                            }
                            catch (Exception ex) { sb.AppendLine(openFileDialog1.FileNames[i]); CSharpUtility.LoggingUtility.LoggingT(ex); Error = true; }
                            if (syncToolStripMenuItem.Checked && !Error) Application.DoEvents();
                        }
                    }
                    finally
                    {
                        tmpStop = false;
                        toolStripProgressBar1.Value = 0;
                        toolStripProgressBar1.Visible = toolStripStatusLabel2.Visible = false;
                        상태표시줄업데이트ToolStripMenuItem_Click(null, null);
                        if (sb.Length > 0)
                        {
                            MessageBox.Show("일부항목은 파일이 존재하지 않거나\n올바르지 않은 음악 파일 이므로 추가하지 못했습니다.\n" +
                                "더 자세한 정보를 알고싶다면 로그를 분석해주시기 바랍니다.\n\n추가 못한 경로:\n" + sb.ToString(),
                                "일부 항목 추가실패!");
                        }
                    }
                }
                else { textBox1.Text = openFileDialog1.FileName; }
            }
        }

        private void 폴더열기ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            folderBrowserDialog1.SelectedPath = LastFolderOpenDirectory;
            if (folderBrowserDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                LastFolderOpenDirectory =folderBrowserDialog1.SelectedPath;
                tmpStop = true;
                List<string> path = new List<string>();
                if(!하위폴더검색ToolStripMenuItem.Checked)path.AddRange(Directory.GetFiles(folderBrowserDialog1.SelectedPath));
                else
                {
                    string[] a = new string[0];
                    try { a = Directory.GetDirectories(folderBrowserDialog1.SelectedPath, "*.*", SearchOption.TopDirectoryOnly); }catch { }
                    foreach (string b in a) { path.AddRange(Directory.GetFiles(b)); }
                }
                path = FilterFilePath(path, Filters[cbMimetype.SelectedIndex]);

                StringBuilder sb = new StringBuilder();
                if (!음악믹싱모드ToolStripMenuItem.Checked)
                {
                    try
                    {
                        toolStripProgressBar1.Maximum = path.Count;
                        toolStripProgressBar1.Visible = toolStripStatusLabel2.Visible = true;
                        decimal i = 0;
                        foreach (string p in path)
                        {
                            bool Error = false;
                            i++;
                            statusStrip1.Refresh();
                            try
                            {
                                if (안전모드ToolStripMenuItem.Checked)
                                {
                                    if (IsDebugging)
                                    {
                                        this.Text = string.Format(ProgramName + " (디버깅, 안전모드) [{0}번째 파일 여는중... 이름: {1} / 경로: {2}]", (i + 1).ToString(), System.IO.Path.GetFileName(p), p);
                                        toolStripStatusLabel1.Text = string.Format("{0}번째 파일 여는중... (디버깅, 안전모드) [이름: {1} / 경로: {2}]", (i + 1).ToString(), System.IO.Path.GetFileName(p), p);

                                    }
                                    else
                                    {
                                        this.Text = string.Format(ProgramName + " (안전모드) [{0}번째 파일 여는중... 이름: {1} / 경로: {2}]", (i + 1).ToString(), System.IO.Path.GetFileName(p), p);
                                        toolStripStatusLabel1.Text = string.Format("{0}번째 파일 여는중... (안전모드) [이름: {1} / 경로: {2}]", (i + 1).ToString(), System.IO.Path.GetFileName(p), p);
                                    }
                                }
                                else
                                {
                                    if (IsDebugging)
                                    {
                                        this.Text = string.Format(ProgramName + " (디버깅) [{0}번째 파일 여는중... 이름: {1} / 경로: {2}]", (i + 1).ToString(), System.IO.Path.GetFileName(p), p);
                                        toolStripStatusLabel1.Text = string.Format("{0}번째 파일 여는중... (디버깅) [이름: {1} / 경로: {2}]", (i + 1).ToString(), System.IO.Path.GetFileName(p), p);
                                    }
                                    else
                                    {
                                        this.Text = string.Format(ProgramName + " (디버깅) [{0}번째 파일 여는중... 이름: {1} / 경로: {2}]", (i + 1).ToString(), System.IO.Path.GetFileName(p), p);
                                        toolStripStatusLabel1.Text = string.Format("{0}번째 파일 여는중... [이름: {1} / 경로: {2}]", (i + 1).ToString(), System.IO.Path.GetFileName(p), p);
                                    }
                                }
                                try { toolStripProgressBar1.Value = (int)i; }
                                catch { toolStripProgressBar1.Value = toolStripProgressBar1.Maximum; }
                                if (안전모드ToolStripMenuItem.Checked)
                                {
                                    toolStripProgressBar1.Visible =
                                    toolStripStatusLabel2.Visible = true;
                                    if (BreakLoop) { break; }
                                    FMOD_Helper.FMODHelper fh = new FMOD_Helper.FMODHelper(p);
                                    if (fh.Result == FMOD.RESULT.ERR_FORMAT ||
                                       fh.Result.ToString().IndexOf("ERR_FILE_") != -1)
                                    { throw new Exception(); }
                                    fh.Dispose();
                                }
                                //timer1.Start();
                                ListViewItem li = new ListViewItem(listView1.Items.Count.ToString());
                                if (listView1.Items.Count == 0)
                                {
                                    fh = new FMOD_Helper.FMODHelper();
                                    fh.PlayingStatusChanged += new FMOD_Helper.FMODHelper.PlayingStatusChangedEventHandler(fh_PlayingStatusChanged_NoMixing);
                                    fh.MusicPathChanged += new FMODHelper.MusicPathChangedEventHandler(fh_MusicPathChanged_NoMixing);
                                    fm[0].MusicPathChanged += new FMODHelper.MusicPathChangedEventHandler(fh_MusicPathChanged_NoMixing);
                                    fh.MusicPath = openFileDialog1.FileNames[0];
                                    ps = new PlayerSetting(fh, System.IO.Path.GetFileName(p), this);
                                    fm.Add(fh);
                                    fh.index = listView1.Items.Count;
                                    li.SubItems.Add("현재 (Ready)");
                                    if (ps == null) { ps = new PlayerSetting(fh, System.IO.Path.GetFileName(fh.MusicPath)); }
                                    ps.Title = string.Format("{0}", System.IO.Path.GetFileName(fh.MusicPath));
                                    button1.Enabled = button2.Enabled = button4.Enabled = true;
                                }
                                else { li.SubItems.Add(""); }
                                li.SubItems.Add(System.IO.Path.GetFileName(p));
                                li.SubItems.Add(p);
                                li.SubItems.Add("");
                                listView1.Items.Add(li);
                            }
                            catch (Exception ex) { sb.AppendLine(p); CSharpUtility.LoggingUtility.LoggingT(ex); Error = true; }
                            if (syncToolStripMenuItem.Checked && !Error) Application.DoEvents();
                        }
                    }
                    finally
                    {

                        toolStripProgressBar1.Value = 0;
                        toolStripProgressBar1.Visible = toolStripStatusLabel2.Visible = false;
                        tmpStop = false;
                        상태표시줄업데이트ToolStripMenuItem_Click(null, null);
                        if (sb.Length > 0)
                        {
                            MessageBox.Show("일부항목은 파일이 존재하지 않거나\n올바르지 않은 음악 파일 이므로 추가하지 못했습니다.\n" +
                                "더 자세한 정보를 알고싶다면 로그를 분석해주시기 바랍니다.\n\n추가 못한 경로:\n" + sb.ToString(),
                                "일부 항목 추가실패!");
                        }
                    }
                }
                else { textBox1.Text = CSharpUtility.Utility.StringUtility.ConvertArrayToString(path.ToArray(), true); }
            }
        }

        readonly string[] Filters = { ".mp3; .ogg; .wav; .wma; .flac; .aif; .aiff; .mid",
                                      ".mp3",".wma",".wav",".ogg",".flac",".aif; .aiff",".mid",
                                      ".fsb; .wax; .asf; .asx; .dls",".fsb",".wax",".asf; .asx; .dls"};
        string[] FilterFilePath(string[] Path, string Filter)
        {
            string[] _Filter = Filter.Split(new string[]{";", " "}, StringSplitOptions.RemoveEmptyEntries);
            List<string> item = new List<string>();
            int j = 0;//decimal i=0;string tmp;
            foreach (string a in Path)
            {
                for(j=0;j<_Filter.Length;j++){
                    if(System.IO.Path.GetExtension(a)==_Filter[j].ToLower()) 
                        item.Add(a);}
            }
            return item.ToArray();
        }
        List<string> FilterFilePath(List<string> Path, string Filter)
        {
            string[] _Filter = Filter.Split(new string[] { ";", " " }, StringSplitOptions.RemoveEmptyEntries);
            List<string> item = new List<string>();
            int j = 0;//decimal i=0;string tmp;
            foreach (string a in Path)
            {
                for (j = 0; j < _Filter.Length; j++)
                {
                    if (System.IO.Path.GetExtension(a) == _Filter[j].ToLower())
                        item.Add(a);
                }
            }
            return item;
        }

        private void 프로젝트열기ToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void 재생컨트롤ToolStripMenuItem_DropDownOpening(object sender, EventArgs e)
        {
            정지ToolStripMenuItem1.Enabled = button2.Enabled;
            재생ToolStripMenuItem1.Enabled = button1.Enabled;
            재생ToolStripMenuItem1.Text = button1.Text;
        }

        private void notifyIcon1_MouseMove(object sender, MouseEventArgs e)
        {
            if (fm.Count>0&&fm[0] != null)
            {
                string a = null;
                string b = CurrentTick(0).ToString();
                string c = TotalTick(0).ToString();
                try{a = string.Format("{0} / {1}",b.Remove(b.LastIndexOf(".")), c.Remove(c.LastIndexOf(".")));}catch{}
                try
                {
                    notifyIcon1.Text = string.Format("조장찡 플레이어 ({0})\n{1}\n{2}", PlayStatustoString(Status_NoMixing),
                        System.IO.Path.GetFileName(fm[0].MusicPath), a == null ? "" : "재생시간: " + a);
                }
                catch
                {
                    try
                    {
                        notifyIcon1.Text = string.Format("{0}\n{2}", 
                            System.IO.Path.GetFileName(fm[0].MusicPath),a == null ? "" : "재생시간: " + a);
                    }
                    catch
                    {
                        try
                        {
                            notifyIcon1.Text = string.Format("조장찡 플레이어 ({0}) - [{1}]", PlayStatustoString(Status_NoMixing),
                            System.IO.Path.GetFileName(fm[0].MusicPath));
                        }
                        catch
                        {
                            try { notifyIcon1.Text = string.Format("{0}", System.IO.Path.GetFileName(fm[0].MusicPath)); }
                            catch
                            { notifyIcon1.Text = string.Format("조장찡 플레이어 ({0})" + PlayStatustoString(Status_NoMixing)); }
                        }
                    }
                }

            }
        }

        private void 메인창열기ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Show();
            this.BringToFront();
            this.TopMost = false;
            this.TopMost = true;
            this.TopMost = 맨위로설정ToolStripMenuItem.Checked;
        }

        private void 트레이로보내기ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void notifyIcon1_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            메인창열기ToolStripMenuItem_Click(null, null);
        }

        private void contextMenuStrip2_Opening(object sender, CancelEventArgs e)
        {
            재생설정열기ToolStripMenuItem.Enabled=button4.Enabled;
        }

        private void mD5해쉬값구하기ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                for (int i = 0; i < listView1.SelectedItems.Count; i++)
                {
                    MessageBox.Show("MD5 Hash: " + Lyrics.GetLyrics.GenerateMusicHash(listView1.SelectedItems[i].SubItems[3].Text),
                        Path.GetFileName(listView1.SelectedItems[i].SubItems[3].Text));
                }
            }
            catch { }
        }

        private void 싱크가사로컬캐시ToolStripMenuItem_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void 싱크가사캐시정리ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("싱크가사 로컬 캐시 정리를 하게되면 \n가사가 없는 해시값은 지워집니다.\n사용자의 플레이어에는 아무영향도 안미칩니다.\n\n계속 하시겠습니까?",
                "싱크가사 로컬 캐시 정리", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == System.Windows.Forms.DialogResult.Yes)
            { Lyricscache.CacheCleaner(); }
        }

        private void 싱크가사캐시관리ToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }
    }
}
