﻿namespace FMOD_Audio
{
    partial class Form1
    {
        /// <summary>
        /// 필수 디자이너 변수입니다.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 사용 중인 모든 리소스를 정리합니다.
        /// </summary>
        /// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 디자이너에서 생성한 코드

        /*
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.파일FToolStripMenuItem,
            this.설정ToolStripMenuItem,
            this.보기VToolStripMenuItem,
            this.도움말HToolStripMenuItem});
        */
        /*
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel2,
            this.toolStripProgressBar1,
            this.toolStripStatusLabel3,
            this.toolStripStatusLabel1,
            this.toolStripProgressBar2});
         */

        /// <summary>
        /// 디자이너 지원에 필요한 메서드입니다.
        /// 이 메서드의 내용을 코드 편집기로 수정하지 마십시오.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.button1 = new System.Windows.Forms.Button();
            this.trackBar1 = new System.Windows.Forms.TrackBar();
            this.button2 = new System.Windows.Forms.Button();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.button3 = new System.Windows.Forms.Button();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.파일FToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.트레이로보내기ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator21 = new System.Windows.Forms.ToolStripSeparator();
            this.열기OToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.파일열기ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.폴더열기ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cbMimetype = new System.Windows.Forms.ToolStripComboBox();
            this.하위폴더검색ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.재생목록열기ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ANISToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.UTF8ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.유니코드ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.재생목록다른이름으로저장ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem4 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator6 = new System.Windows.Forms.ToolStripSeparator();
            this.프로젝트열기ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.프로젝트저장ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator7 = new System.Windows.Forms.ToolStripSeparator();
            this.파일로웨이브저장ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.원본사이즈사용ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripTextBox1 = new System.Windows.Forms.ToolStripTextBox();
            this.toolStripSeparator8 = new System.Windows.Forms.ToolStripSeparator();
            this.끝내기ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.설정ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.맨위로설정ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator9 = new System.Windows.Forms.ToolStripSeparator();
            this.안전모드ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.syncToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.음악믹싱모드ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator22 = new System.Windows.Forms.ToolStripSeparator();
            this.싱크가사로컬캐시ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.싱크가사캐시정리ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator23 = new System.Windows.Forms.ToolStripSeparator();
            this.싱크가사캐시관리ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator10 = new System.Windows.Forms.ToolStripSeparator();
            this.파일복구및재설치ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator11 = new System.Windows.Forms.ToolStripSeparator();
            this.프로그램다시시작ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator12 = new System.Windows.Forms.ToolStripSeparator();
            this.우선순위ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.높음ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.높은우선순위ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.보통권장ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.낮은우선순위ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.LOL우선순위ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.LOL높은우선순위ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.LOL보통권장ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.보기VToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.싱크가사찾기ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.바탕화면싱크가사창띄우기ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator13 = new System.Windows.Forms.ToolStripSeparator();
            this.상태표시줄업데이트ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.도움말HToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.정보ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.업데이트변경사항보기ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator14 = new System.Windows.Forms.ToolStripSeparator();
            this.디버깅ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.button4 = new System.Windows.Forms.Button();
            this.listView1 = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader5 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.재생ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.새인스턴스시작ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.일시정지ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.정지ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.삭제ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.원본크기구하기ToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripTextBox2 = new System.Windows.Forms.ToolStripTextBox();
            this.mD5해쉬값구하기ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.가사찾기ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.복사ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.음악이름ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.음악경로ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.탐색기에서해당파일열기ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.openFileDialog2 = new System.Windows.Forms.OpenFileDialog();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel2 = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripProgressBar1 = new System.Windows.Forms.ToolStripProgressBar();
            this.toolStripStatusLabel3 = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripProgressBar2 = new System.Windows.Forms.ToolStripProgressBar();
            this.panel1 = new System.Windows.Forms.Panel();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.saveFileDialog2 = new System.Windows.Forms.SaveFileDialog();
            this.timer2 = new System.Windows.Forms.Timer(this.components);
            this.button5 = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.timer3 = new System.Windows.Forms.Timer(this.components);
            this.timer4 = new System.Windows.Forms.Timer(this.components);
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.openFileDialog3 = new System.Windows.Forms.OpenFileDialog();
            this.saveFileDialog3 = new System.Windows.Forms.SaveFileDialog();
            this.notifyIcon1 = new System.Windows.Forms.NotifyIcon(this.components);
            this.contextMenuStrip2 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.설정TToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.파일복구및재설치ToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator18 = new System.Windows.Forms.ToolStripSeparator();
            this.프로그램다시시작ToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator19 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripMenuItem7 = new System.Windows.Forms.ToolStripMenuItem();
            this.높음ToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.높은우선순위ToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.보통권장ToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.낮은우선순위ToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.보기VToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.바탕화면싱크가사창띄우기ToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.도움말HToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.정보ToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.업데이트변경사항보기ToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator17 = new System.Windows.Forms.ToolStripSeparator();
            this.디버깅ToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator15 = new System.Windows.Forms.ToolStripSeparator();
            this.재생컨트롤ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.재생ToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.정지ToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator16 = new System.Windows.Forms.ToolStripSeparator();
            this.재생설정열기ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.메인창열기ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator20 = new System.Windows.Forms.ToolStripSeparator();
            this.끝내기XToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar1)).BeginInit();
            this.menuStrip1.SuspendLayout();
            this.contextMenuStrip1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.contextMenuStrip2.SuspendLayout();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Enabled = false;
            this.button1.Location = new System.Drawing.Point(5, 47);
            this.button1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 21);
            this.button1.TabIndex = 0;
            this.button1.Text = "재생";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // trackBar1
            // 
            this.trackBar1.AutoSize = false;
            this.trackBar1.Location = new System.Drawing.Point(5, 85);
            this.trackBar1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.trackBar1.Name = "trackBar1";
            this.trackBar1.Size = new System.Drawing.Size(323, 29);
            this.trackBar1.TabIndex = 1;
            // 
            // button2
            // 
            this.button2.Enabled = false;
            this.button2.Location = new System.Drawing.Point(0, 0);
            this.button2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 21);
            this.button2.TabIndex = 2;
            this.button2.Text = "정지";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.DefaultExt = "*.mp3; *.ogg; *.wav; *.wma; *.flac; *.aif; *.aiff; *.mid";
            this.openFileDialog1.Filter = resources.GetString("openFileDialog1.Filter");
            this.openFileDialog1.Multiselect = true;
            this.openFileDialog1.Title = "음악파일 열기";
            this.openFileDialog1.FileOk += new System.ComponentModel.CancelEventHandler(this.openFileDialog1_FileOk);
            // 
            // button3
            // 
            this.button3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button3.Location = new System.Drawing.Point(491, 2);
            this.button3.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(41, 19);
            this.button3.TabIndex = 3;
            this.button3.Text = "추가";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // menuStrip1
            // 
            this.menuStrip1.AllowItemReorder = true;
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.파일FToolStripMenuItem,
            this.설정ToolStripMenuItem,
            this.보기VToolStripMenuItem,
            this.도움말HToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional;
            this.menuStrip1.Size = new System.Drawing.Size(537, 24);
            this.menuStrip1.TabIndex = 4;
            this.menuStrip1.TabStop = true;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // 파일FToolStripMenuItem
            // 
            this.파일FToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.트레이로보내기ToolStripMenuItem,
            this.toolStripSeparator21,
            this.열기OToolStripMenuItem,
            this.toolStripSeparator5,
            this.재생목록열기ToolStripMenuItem,
            this.재생목록다른이름으로저장ToolStripMenuItem,
            this.toolStripSeparator6,
            this.프로젝트열기ToolStripMenuItem,
            this.프로젝트저장ToolStripMenuItem,
            this.toolStripSeparator7,
            this.파일로웨이브저장ToolStripMenuItem,
            this.toolStripSeparator8,
            this.끝내기ToolStripMenuItem});
            this.파일FToolStripMenuItem.Name = "파일FToolStripMenuItem";
            this.파일FToolStripMenuItem.Size = new System.Drawing.Size(58, 20);
            this.파일FToolStripMenuItem.Text = "파일(&F)";
            this.파일FToolStripMenuItem.DropDownOpening += new System.EventHandler(this.파일FToolStripMenuItem_DropDownOpening);
            // 
            // 트레이로보내기ToolStripMenuItem
            // 
            this.트레이로보내기ToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("트레이로보내기ToolStripMenuItem.Image")));
            this.트레이로보내기ToolStripMenuItem.Name = "트레이로보내기ToolStripMenuItem";
            this.트레이로보내기ToolStripMenuItem.Size = new System.Drawing.Size(336, 22);
            this.트레이로보내기ToolStripMenuItem.Text = "트레이로 보내기";
            this.트레이로보내기ToolStripMenuItem.Click += new System.EventHandler(this.트레이로보내기ToolStripMenuItem_Click);
            // 
            // toolStripSeparator21
            // 
            this.toolStripSeparator21.Name = "toolStripSeparator21";
            this.toolStripSeparator21.Size = new System.Drawing.Size(333, 6);
            // 
            // 열기OToolStripMenuItem
            // 
            this.열기OToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.파일열기ToolStripMenuItem,
            this.폴더열기ToolStripMenuItem});
            this.열기OToolStripMenuItem.Name = "열기OToolStripMenuItem";
            this.열기OToolStripMenuItem.Size = new System.Drawing.Size(336, 22);
            this.열기OToolStripMenuItem.Text = "열기(&O)";
            // 
            // 파일열기ToolStripMenuItem
            // 
            this.파일열기ToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("파일열기ToolStripMenuItem.Image")));
            this.파일열기ToolStripMenuItem.Name = "파일열기ToolStripMenuItem";
            this.파일열기ToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.O)));
            this.파일열기ToolStripMenuItem.Size = new System.Drawing.Size(167, 22);
            this.파일열기ToolStripMenuItem.Text = "파일 열기";
            this.파일열기ToolStripMenuItem.ToolTipText = "파일을 직접 엽니다.";
            this.파일열기ToolStripMenuItem.Click += new System.EventHandler(this.파일열기ToolStripMenuItem_Click);
            // 
            // 폴더열기ToolStripMenuItem
            // 
            this.폴더열기ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cbMimetype,
            this.하위폴더검색ToolStripMenuItem});
            this.폴더열기ToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("폴더열기ToolStripMenuItem.Image")));
            this.폴더열기ToolStripMenuItem.Name = "폴더열기ToolStripMenuItem";
            this.폴더열기ToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.D)));
            this.폴더열기ToolStripMenuItem.Size = new System.Drawing.Size(167, 22);
            this.폴더열기ToolStripMenuItem.Text = "폴더 열기";
            this.폴더열기ToolStripMenuItem.ToolTipText = "지정한 폴더에있는 음악파일을 모두 목록에 추가합니다.";
            this.폴더열기ToolStripMenuItem.Click += new System.EventHandler(this.폴더열기ToolStripMenuItem_Click);
            // 
            // cbMimetype
            // 
            this.cbMimetype.AutoSize = false;
            this.cbMimetype.DropDownHeight = 200;
            this.cbMimetype.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbMimetype.DropDownWidth = 300;
            this.cbMimetype.IntegralHeight = false;
            this.cbMimetype.Items.AddRange(new object[] {
            "지원하는 모든 음악 파일",
            "MP3 Audio 파일 (*.mp3)",
            "Windows Media Audio 파일 (*.wma)",
            "Wave (Microsoft) 파일 (*.wav)",
            "OggVorbis 파일 (*.ogg)",
            "Free Lossless Audio Codec 파일 (*.flac)",
            "Audio Interchange File Format 파일 (*.aif, *.aiff)",
            "Standard MIDI 파일 (*.mid)",
            "지원하는 모든 특수 파일",
            "FMOD\'s Sample Bank 파일 (*.fsb)",
            "Windows Media Audio Redirector 파일 (*.wax)",
            "네트워크 스트리밍 파일 (*.asf, *.asx, *.dls)"});
            this.cbMimetype.Name = "cbMimetype";
            this.cbMimetype.Size = new System.Drawing.Size(230, 19);
            this.cbMimetype.ToolTipText = "폴더에서 열때 음악 파일 유형을 선택합니다.";
            // 
            // 하위폴더검색ToolStripMenuItem
            // 
            this.하위폴더검색ToolStripMenuItem.CheckOnClick = true;
            this.하위폴더검색ToolStripMenuItem.Name = "하위폴더검색ToolStripMenuItem";
            this.하위폴더검색ToolStripMenuItem.Size = new System.Drawing.Size(290, 22);
            this.하위폴더검색ToolStripMenuItem.Text = "하위 폴더 검색";
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(333, 6);
            // 
            // 재생목록열기ToolStripMenuItem
            // 
            this.재생목록열기ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ANISToolStripMenuItem,
            this.UTF8ToolStripMenuItem,
            this.유니코드ToolStripMenuItem});
            this.재생목록열기ToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("재생목록열기ToolStripMenuItem.Image")));
            this.재생목록열기ToolStripMenuItem.Name = "재생목록열기ToolStripMenuItem";
            this.재생목록열기ToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.L)));
            this.재생목록열기ToolStripMenuItem.Size = new System.Drawing.Size(336, 22);
            this.재생목록열기ToolStripMenuItem.Text = "재생목록 열기(&L)";
            this.재생목록열기ToolStripMenuItem.Click += new System.EventHandler(this.재생목록열기ToolStripMenuItem_Click);
            // 
            // ANISToolStripMenuItem
            // 
            this.ANISToolStripMenuItem.Checked = true;
            this.ANISToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.ANISToolStripMenuItem.Name = "ANISToolStripMenuItem";
            this.ANISToolStripMenuItem.Size = new System.Drawing.Size(124, 22);
            this.ANISToolStripMenuItem.Text = "ANIS";
            this.ANISToolStripMenuItem.Visible = false;
            // 
            // UTF8ToolStripMenuItem
            // 
            this.UTF8ToolStripMenuItem.Name = "UTF8ToolStripMenuItem";
            this.UTF8ToolStripMenuItem.Size = new System.Drawing.Size(124, 22);
            this.UTF8ToolStripMenuItem.Text = "UTF-8";
            this.UTF8ToolStripMenuItem.Visible = false;
            // 
            // 유니코드ToolStripMenuItem
            // 
            this.유니코드ToolStripMenuItem.Name = "유니코드ToolStripMenuItem";
            this.유니코드ToolStripMenuItem.Size = new System.Drawing.Size(124, 22);
            this.유니코드ToolStripMenuItem.Text = "유니코드";
            this.유니코드ToolStripMenuItem.Visible = false;
            // 
            // 재생목록다른이름으로저장ToolStripMenuItem
            // 
            this.재생목록다른이름으로저장ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem1,
            this.toolStripMenuItem3,
            this.toolStripMenuItem4});
            this.재생목록다른이름으로저장ToolStripMenuItem.Enabled = false;
            this.재생목록다른이름으로저장ToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("재생목록다른이름으로저장ToolStripMenuItem.Image")));
            this.재생목록다른이름으로저장ToolStripMenuItem.Name = "재생목록다른이름으로저장ToolStripMenuItem";
            this.재생목록다른이름으로저장ToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
            this.재생목록다른이름으로저장ToolStripMenuItem.Size = new System.Drawing.Size(336, 22);
            this.재생목록다른이름으로저장ToolStripMenuItem.Text = "재생목록 저장(&S)";
            this.재생목록다른이름으로저장ToolStripMenuItem.Click += new System.EventHandler(this.재생목록다른이름으로저장ToolStripMenuItem_Click);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Checked = true;
            this.toolStripMenuItem1.CheckState = System.Windows.Forms.CheckState.Checked;
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(124, 22);
            this.toolStripMenuItem1.Text = "ANIS";
            this.toolStripMenuItem1.Visible = false;
            // 
            // toolStripMenuItem3
            // 
            this.toolStripMenuItem3.Name = "toolStripMenuItem3";
            this.toolStripMenuItem3.Size = new System.Drawing.Size(124, 22);
            this.toolStripMenuItem3.Text = "UTF-8";
            this.toolStripMenuItem3.Visible = false;
            // 
            // toolStripMenuItem4
            // 
            this.toolStripMenuItem4.Name = "toolStripMenuItem4";
            this.toolStripMenuItem4.Size = new System.Drawing.Size(124, 22);
            this.toolStripMenuItem4.Text = "유니코드";
            this.toolStripMenuItem4.Visible = false;
            // 
            // toolStripSeparator6
            // 
            this.toolStripSeparator6.Name = "toolStripSeparator6";
            this.toolStripSeparator6.Size = new System.Drawing.Size(333, 6);
            this.toolStripSeparator6.Visible = false;
            // 
            // 프로젝트열기ToolStripMenuItem
            // 
            this.프로젝트열기ToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("프로젝트열기ToolStripMenuItem.Image")));
            this.프로젝트열기ToolStripMenuItem.Name = "프로젝트열기ToolStripMenuItem";
            this.프로젝트열기ToolStripMenuItem.Size = new System.Drawing.Size(336, 22);
            this.프로젝트열기ToolStripMenuItem.Text = "프로젝트 열기";
            this.프로젝트열기ToolStripMenuItem.Visible = false;
            this.프로젝트열기ToolStripMenuItem.Click += new System.EventHandler(this.프로젝트열기ToolStripMenuItem_Click);
            // 
            // 프로젝트저장ToolStripMenuItem
            // 
            this.프로젝트저장ToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("프로젝트저장ToolStripMenuItem.Image")));
            this.프로젝트저장ToolStripMenuItem.Name = "프로젝트저장ToolStripMenuItem";
            this.프로젝트저장ToolStripMenuItem.Size = new System.Drawing.Size(336, 22);
            this.프로젝트저장ToolStripMenuItem.Text = "프로젝트 저장";
            this.프로젝트저장ToolStripMenuItem.Visible = false;
            // 
            // toolStripSeparator7
            // 
            this.toolStripSeparator7.Name = "toolStripSeparator7";
            this.toolStripSeparator7.Size = new System.Drawing.Size(333, 6);
            // 
            // 파일로웨이브저장ToolStripMenuItem
            // 
            this.파일로웨이브저장ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.원본사이즈사용ToolStripMenuItem,
            this.toolStripTextBox1});
            this.파일로웨이브저장ToolStripMenuItem.Enabled = false;
            this.파일로웨이브저장ToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("파일로웨이브저장ToolStripMenuItem.Image")));
            this.파일로웨이브저장ToolStripMenuItem.Name = "파일로웨이브저장ToolStripMenuItem";
            this.파일로웨이브저장ToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.W)));
            this.파일로웨이브저장ToolStripMenuItem.Size = new System.Drawing.Size(336, 22);
            this.파일로웨이브저장ToolStripMenuItem.Text = "파일로 현재 재생위치의 웨이브정보 저장";
            this.파일로웨이브저장ToolStripMenuItem.DropDownOpening += new System.EventHandler(this.파일로웨이브저장ToolStripMenuItem_DropDownOpening);
            this.파일로웨이브저장ToolStripMenuItem.Click += new System.EventHandler(this.파일로웨이브저장ToolStripMenuItem_Click);
            // 
            // 원본사이즈사용ToolStripMenuItem
            // 
            this.원본사이즈사용ToolStripMenuItem.Name = "원본사이즈사용ToolStripMenuItem";
            this.원본사이즈사용ToolStripMenuItem.Size = new System.Drawing.Size(220, 22);
            this.원본사이즈사용ToolStripMenuItem.Text = "원본 사이즈 구하기";
            this.원본사이즈사용ToolStripMenuItem.Click += new System.EventHandler(this.원본사이즈사용ToolStripMenuItem_Click);
            // 
            // toolStripTextBox1
            // 
            this.toolStripTextBox1.Font = new System.Drawing.Font("굴림", 8.780488F);
            this.toolStripTextBox1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.toolStripTextBox1.MaxLength = 15;
            this.toolStripTextBox1.Name = "toolStripTextBox1";
            this.toolStripTextBox1.Size = new System.Drawing.Size(160, 20);
            this.toolStripTextBox1.Tag = "크기 입력 (숫자만 입력가능)";
            this.toolStripTextBox1.Text = "2,048";
            this.toolStripTextBox1.ToolTipText = "파일로 현재 재생위치의 웨이브정보 저장할 숫자를 입력합니다.\n※주의: FFT최대사이즈보다 크게 지정하면 0으로 저장될수 있습니다.\n적당한값을 입력" +
                "해 주세요";
            this.toolStripTextBox1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.toolStripTextBox1_KeyPress);
            // 
            // toolStripSeparator8
            // 
            this.toolStripSeparator8.Name = "toolStripSeparator8";
            this.toolStripSeparator8.Size = new System.Drawing.Size(333, 6);
            // 
            // 끝내기ToolStripMenuItem
            // 
            this.끝내기ToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("끝내기ToolStripMenuItem.Image")));
            this.끝내기ToolStripMenuItem.Name = "끝내기ToolStripMenuItem";
            this.끝내기ToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.F4)));
            this.끝내기ToolStripMenuItem.Size = new System.Drawing.Size(336, 22);
            this.끝내기ToolStripMenuItem.Text = "끝내기(&X)";
            this.끝내기ToolStripMenuItem.Click += new System.EventHandler(this.끝내기ToolStripMenuItem_Click);
            // 
            // 설정ToolStripMenuItem
            // 
            this.설정ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.맨위로설정ToolStripMenuItem,
            this.toolStripSeparator9,
            this.안전모드ToolStripMenuItem,
            this.syncToolStripMenuItem,
            this.음악믹싱모드ToolStripMenuItem,
            this.toolStripSeparator22,
            this.싱크가사로컬캐시ToolStripMenuItem,
            this.toolStripSeparator10,
            this.파일복구및재설치ToolStripMenuItem,
            this.toolStripSeparator11,
            this.프로그램다시시작ToolStripMenuItem,
            this.toolStripSeparator12,
            this.우선순위ToolStripMenuItem,
            this.LOL우선순위ToolStripMenuItem});
            this.설정ToolStripMenuItem.Name = "설정ToolStripMenuItem";
            this.설정ToolStripMenuItem.Size = new System.Drawing.Size(59, 20);
            this.설정ToolStripMenuItem.Text = "설정(&T)";
            this.설정ToolStripMenuItem.DropDownOpening += new System.EventHandler(this.설정ToolStripMenuItem_DropDownOpening);
            // 
            // 맨위로설정ToolStripMenuItem
            // 
            this.맨위로설정ToolStripMenuItem.CheckOnClick = true;
            this.맨위로설정ToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("맨위로설정ToolStripMenuItem.Image")));
            this.맨위로설정ToolStripMenuItem.Name = "맨위로설정ToolStripMenuItem";
            this.맨위로설정ToolStripMenuItem.Size = new System.Drawing.Size(292, 22);
            this.맨위로설정ToolStripMenuItem.Text = "맨 위로 설정";
            this.맨위로설정ToolStripMenuItem.Click += new System.EventHandler(this.맨위로설정ToolStripMenuItem_Click);
            // 
            // toolStripSeparator9
            // 
            this.toolStripSeparator9.Name = "toolStripSeparator9";
            this.toolStripSeparator9.Size = new System.Drawing.Size(289, 6);
            // 
            // 안전모드ToolStripMenuItem
            // 
            this.안전모드ToolStripMenuItem.Name = "안전모드ToolStripMenuItem";
            this.안전모드ToolStripMenuItem.Size = new System.Drawing.Size(292, 22);
            this.안전모드ToolStripMenuItem.Text = "안전 모드";
            this.안전모드ToolStripMenuItem.ToolTipText = "파일을 열때 파일이 올바른지의 여부를 확인합니다.";
            this.안전모드ToolStripMenuItem.Click += new System.EventHandler(this.안전모드ToolStripMenuItem_Click);
            // 
            // syncToolStripMenuItem
            // 
            this.syncToolStripMenuItem.Name = "syncToolStripMenuItem";
            this.syncToolStripMenuItem.Size = new System.Drawing.Size(292, 22);
            this.syncToolStripMenuItem.Text = "Async 모드";
            this.syncToolStripMenuItem.ToolTipText = "혹시 여러 파일이나 목록을 열다가 멈추거나 열지못한다면\r\n체크를 해제하세요.";
            this.syncToolStripMenuItem.Click += new System.EventHandler(this.syncToolStripMenuItem_Click);
            // 
            // 음악믹싱모드ToolStripMenuItem
            // 
            this.음악믹싱모드ToolStripMenuItem.Enabled = false;
            this.음악믹싱모드ToolStripMenuItem.Name = "음악믹싱모드ToolStripMenuItem";
            this.음악믹싱모드ToolStripMenuItem.Size = new System.Drawing.Size(292, 22);
            this.음악믹싱모드ToolStripMenuItem.Text = "음악 믹싱 모드";
            this.음악믹싱모드ToolStripMenuItem.Click += new System.EventHandler(this.음악믹싱모드ToolStripMenuItem_Click);
            // 
            // toolStripSeparator22
            // 
            this.toolStripSeparator22.Name = "toolStripSeparator22";
            this.toolStripSeparator22.Size = new System.Drawing.Size(289, 6);
            // 
            // 싱크가사로컬캐시ToolStripMenuItem
            // 
            this.싱크가사로컬캐시ToolStripMenuItem.Checked = true;
            this.싱크가사로컬캐시ToolStripMenuItem.CheckOnClick = true;
            this.싱크가사로컬캐시ToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.싱크가사로컬캐시ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.싱크가사캐시정리ToolStripMenuItem,
            this.toolStripSeparator23,
            this.싱크가사캐시관리ToolStripMenuItem});
            this.싱크가사로컬캐시ToolStripMenuItem.Name = "싱크가사로컬캐시ToolStripMenuItem";
            this.싱크가사로컬캐시ToolStripMenuItem.Size = new System.Drawing.Size(292, 22);
            this.싱크가사로컬캐시ToolStripMenuItem.Text = "싱크 가사 로컬 캐시";
            this.싱크가사로컬캐시ToolStripMenuItem.CheckedChanged += new System.EventHandler(this.싱크가사로컬캐시ToolStripMenuItem_CheckedChanged);
            // 
            // 싱크가사캐시정리ToolStripMenuItem
            // 
            this.싱크가사캐시정리ToolStripMenuItem.Name = "싱크가사캐시정리ToolStripMenuItem";
            this.싱크가사캐시정리ToolStripMenuItem.Size = new System.Drawing.Size(184, 22);
            this.싱크가사캐시정리ToolStripMenuItem.Text = "싱크 가사 캐시 정리";
            this.싱크가사캐시정리ToolStripMenuItem.Click += new System.EventHandler(this.싱크가사캐시정리ToolStripMenuItem_Click);
            // 
            // toolStripSeparator23
            // 
            this.toolStripSeparator23.Name = "toolStripSeparator23";
            this.toolStripSeparator23.Size = new System.Drawing.Size(181, 6);
            // 
            // 싱크가사캐시관리ToolStripMenuItem
            // 
            this.싱크가사캐시관리ToolStripMenuItem.Name = "싱크가사캐시관리ToolStripMenuItem";
            this.싱크가사캐시관리ToolStripMenuItem.Size = new System.Drawing.Size(184, 22);
            this.싱크가사캐시관리ToolStripMenuItem.Text = "싱크 가사 캐시 관리";
            this.싱크가사캐시관리ToolStripMenuItem.Click += new System.EventHandler(this.싱크가사캐시관리ToolStripMenuItem_Click);
            // 
            // toolStripSeparator10
            // 
            this.toolStripSeparator10.Name = "toolStripSeparator10";
            this.toolStripSeparator10.Size = new System.Drawing.Size(289, 6);
            // 
            // 파일복구및재설치ToolStripMenuItem
            // 
            this.파일복구및재설치ToolStripMenuItem.Name = "파일복구및재설치ToolStripMenuItem";
            this.파일복구및재설치ToolStripMenuItem.Size = new System.Drawing.Size(292, 22);
            this.파일복구및재설치ToolStripMenuItem.Text = "사운드 시스템 엔진 파일 복구 및 재설치";
            this.파일복구및재설치ToolStripMenuItem.Click += new System.EventHandler(this.파일복구및재설치ToolStripMenuItem_Click);
            // 
            // toolStripSeparator11
            // 
            this.toolStripSeparator11.Name = "toolStripSeparator11";
            this.toolStripSeparator11.Size = new System.Drawing.Size(289, 6);
            // 
            // 프로그램다시시작ToolStripMenuItem
            // 
            this.프로그램다시시작ToolStripMenuItem.Name = "프로그램다시시작ToolStripMenuItem";
            this.프로그램다시시작ToolStripMenuItem.Size = new System.Drawing.Size(292, 22);
            this.프로그램다시시작ToolStripMenuItem.Text = "프로그램 다시 시작";
            this.프로그램다시시작ToolStripMenuItem.Click += new System.EventHandler(this.프로그램다시시작ToolStripMenuItem_Click);
            // 
            // toolStripSeparator12
            // 
            this.toolStripSeparator12.Name = "toolStripSeparator12";
            this.toolStripSeparator12.Size = new System.Drawing.Size(289, 6);
            // 
            // 우선순위ToolStripMenuItem
            // 
            this.우선순위ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.높음ToolStripMenuItem,
            this.높은우선순위ToolStripMenuItem,
            this.보통권장ToolStripMenuItem,
            this.낮은우선순위ToolStripMenuItem});
            this.우선순위ToolStripMenuItem.Name = "우선순위ToolStripMenuItem";
            this.우선순위ToolStripMenuItem.Size = new System.Drawing.Size(292, 22);
            this.우선순위ToolStripMenuItem.Text = "프로그램 우선순위";
            // 
            // 높음ToolStripMenuItem
            // 
            this.높음ToolStripMenuItem.Name = "높음ToolStripMenuItem";
            this.높음ToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.높음ToolStripMenuItem.Text = "높음";
            this.높음ToolStripMenuItem.Click += new System.EventHandler(this.높음ToolStripMenuItem_Click);
            // 
            // 높은우선순위ToolStripMenuItem
            // 
            this.높은우선순위ToolStripMenuItem.Name = "높은우선순위ToolStripMenuItem";
            this.높은우선순위ToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.높은우선순위ToolStripMenuItem.Text = "이 프로그램 우선";
            this.높은우선순위ToolStripMenuItem.Click += new System.EventHandler(this.높은우선순위ToolStripMenuItem_Click);
            // 
            // 보통권장ToolStripMenuItem
            // 
            this.보통권장ToolStripMenuItem.Checked = true;
            this.보통권장ToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.보통권장ToolStripMenuItem.Name = "보통권장ToolStripMenuItem";
            this.보통권장ToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.보통권장ToolStripMenuItem.Text = "보통 (권장)";
            this.보통권장ToolStripMenuItem.Click += new System.EventHandler(this.보통권장ToolStripMenuItem_Click);
            // 
            // 낮은우선순위ToolStripMenuItem
            // 
            this.낮은우선순위ToolStripMenuItem.Name = "낮은우선순위ToolStripMenuItem";
            this.낮은우선순위ToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.낮은우선순위ToolStripMenuItem.Text = "다른 프로그램 우선";
            this.낮은우선순위ToolStripMenuItem.Click += new System.EventHandler(this.낮은우선순위ToolStripMenuItem_Click);
            // 
            // LOL우선순위ToolStripMenuItem
            // 
            this.LOL우선순위ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.LOL높은우선순위ToolStripMenuItem,
            this.LOL보통권장ToolStripMenuItem});
            this.LOL우선순위ToolStripMenuItem.Enabled = false;
            this.LOL우선순위ToolStripMenuItem.Name = "LOL우선순위ToolStripMenuItem";
            this.LOL우선순위ToolStripMenuItem.Size = new System.Drawing.Size(292, 22);
            this.LOL우선순위ToolStripMenuItem.Text = "리그 오브 레전드 우선순위";
            this.LOL우선순위ToolStripMenuItem.ToolTipText = "리그 오브 레전드도 사운드 재생에 FMOD를 씁니다.\r\n(리그 오브 레전드에서 게임이 시작되면 활성화 됩니다.)";
            // 
            // LOL높은우선순위ToolStripMenuItem
            // 
            this.LOL높은우선순위ToolStripMenuItem.Enabled = false;
            this.LOL높은우선순위ToolStripMenuItem.Name = "LOL높은우선순위ToolStripMenuItem";
            this.LOL높은우선순위ToolStripMenuItem.Size = new System.Drawing.Size(165, 22);
            this.LOL높은우선순위ToolStripMenuItem.Text = "LOL 우선 (권장)";
            this.LOL높은우선순위ToolStripMenuItem.Click += new System.EventHandler(this.높은우선순위ToolStripMenuItem1_Click);
            // 
            // LOL보통권장ToolStripMenuItem
            // 
            this.LOL보통권장ToolStripMenuItem.Enabled = false;
            this.LOL보통권장ToolStripMenuItem.Name = "LOL보통권장ToolStripMenuItem";
            this.LOL보통권장ToolStripMenuItem.Size = new System.Drawing.Size(165, 22);
            this.LOL보통권장ToolStripMenuItem.Text = "보통";
            this.LOL보통권장ToolStripMenuItem.Click += new System.EventHandler(this.보통권장ToolStripMenuItem1_Click);
            // 
            // 보기VToolStripMenuItem
            // 
            this.보기VToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.싱크가사찾기ToolStripMenuItem,
            this.바탕화면싱크가사창띄우기ToolStripMenuItem,
            this.toolStripSeparator13,
            this.상태표시줄업데이트ToolStripMenuItem});
            this.보기VToolStripMenuItem.Name = "보기VToolStripMenuItem";
            this.보기VToolStripMenuItem.Size = new System.Drawing.Size(59, 20);
            this.보기VToolStripMenuItem.Text = "보기(&V)";
            // 
            // 싱크가사찾기ToolStripMenuItem
            // 
            this.싱크가사찾기ToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("싱크가사찾기ToolStripMenuItem.Image")));
            this.싱크가사찾기ToolStripMenuItem.Name = "싱크가사찾기ToolStripMenuItem";
            this.싱크가사찾기ToolStripMenuItem.Size = new System.Drawing.Size(232, 22);
            this.싱크가사찾기ToolStripMenuItem.Text = "싱크 가사 검색";
            this.싱크가사찾기ToolStripMenuItem.Click += new System.EventHandler(this.싱크가사찾기ToolStripMenuItem_Click);
            // 
            // 바탕화면싱크가사창띄우기ToolStripMenuItem
            // 
            this.바탕화면싱크가사창띄우기ToolStripMenuItem.Name = "바탕화면싱크가사창띄우기ToolStripMenuItem";
            this.바탕화면싱크가사창띄우기ToolStripMenuItem.Size = new System.Drawing.Size(232, 22);
            this.바탕화면싱크가사창띄우기ToolStripMenuItem.Text = "바탕화면 싱크 가사창 띄우기";
            this.바탕화면싱크가사창띄우기ToolStripMenuItem.Click += new System.EventHandler(this.바탕화면싱크가사창띄우기ToolStripMenuItem_Click);
            // 
            // toolStripSeparator13
            // 
            this.toolStripSeparator13.Name = "toolStripSeparator13";
            this.toolStripSeparator13.Size = new System.Drawing.Size(229, 6);
            // 
            // 상태표시줄업데이트ToolStripMenuItem
            // 
            this.상태표시줄업데이트ToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("상태표시줄업데이트ToolStripMenuItem.Image")));
            this.상태표시줄업데이트ToolStripMenuItem.Name = "상태표시줄업데이트ToolStripMenuItem";
            this.상태표시줄업데이트ToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F6;
            this.상태표시줄업데이트ToolStripMenuItem.Size = new System.Drawing.Size(232, 22);
            this.상태표시줄업데이트ToolStripMenuItem.Text = "상태 표시줄 업데이트";
            this.상태표시줄업데이트ToolStripMenuItem.ToolTipText = "원래는 자동으로 업데이트 되지만\r\n디버깅 중도 아닌데 디버깅이라고 뜨거나 (디버거 에서 분리했을때)\r\n파일이나 목록 열기에 실패해서 상태 표시줄이" +
                " 멈춰있는경우\r\n기타 상태 표시줄이 이상하게 뜰때 클릭해 주세요";
            this.상태표시줄업데이트ToolStripMenuItem.Click += new System.EventHandler(this.상태표시줄업데이트ToolStripMenuItem_Click);
            // 
            // 도움말HToolStripMenuItem
            // 
            this.도움말HToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.정보ToolStripMenuItem,
            this.업데이트변경사항보기ToolStripMenuItem,
            this.toolStripSeparator14,
            this.디버깅ToolStripMenuItem});
            this.도움말HToolStripMenuItem.Name = "도움말HToolStripMenuItem";
            this.도움말HToolStripMenuItem.Size = new System.Drawing.Size(71, 20);
            this.도움말HToolStripMenuItem.Text = "도움말(&H)";
            this.도움말HToolStripMenuItem.DropDownOpening += new System.EventHandler(this.도움말HToolStripMenuItem_DropDownOpening);
            // 
            // 정보ToolStripMenuItem
            // 
            this.정보ToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("정보ToolStripMenuItem.Image")));
            this.정보ToolStripMenuItem.Name = "정보ToolStripMenuItem";
            this.정보ToolStripMenuItem.Size = new System.Drawing.Size(222, 22);
            this.정보ToolStripMenuItem.Text = "프로그램 정보...(&A)";
            this.정보ToolStripMenuItem.Click += new System.EventHandler(this.정보ToolStripMenuItem_Click);
            // 
            // 업데이트변경사항보기ToolStripMenuItem
            // 
            this.업데이트변경사항보기ToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("업데이트변경사항보기ToolStripMenuItem.Image")));
            this.업데이트변경사항보기ToolStripMenuItem.Name = "업데이트변경사항보기ToolStripMenuItem";
            this.업데이트변경사항보기ToolStripMenuItem.Size = new System.Drawing.Size(222, 22);
            this.업데이트변경사항보기ToolStripMenuItem.Text = "업데이트 변경사항 보기(&R)";
            this.업데이트변경사항보기ToolStripMenuItem.Click += new System.EventHandler(this.업데이트변경사항보기ToolStripMenuItem_Click);
            // 
            // toolStripSeparator14
            // 
            this.toolStripSeparator14.Name = "toolStripSeparator14";
            this.toolStripSeparator14.Size = new System.Drawing.Size(219, 6);
            // 
            // 디버깅ToolStripMenuItem
            // 
            this.디버깅ToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("디버깅ToolStripMenuItem.Image")));
            this.디버깅ToolStripMenuItem.Name = "디버깅ToolStripMenuItem";
            this.디버깅ToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F10;
            this.디버깅ToolStripMenuItem.Size = new System.Drawing.Size(222, 22);
            this.디버깅ToolStripMenuItem.Text = "디버깅 (개발자 전용)";
            this.디버깅ToolStripMenuItem.Click += new System.EventHandler(this.디버깅ToolStripMenuItem_Click);
            // 
            // textBox1
            // 
            this.textBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox1.Location = new System.Drawing.Point(59, 2);
            this.textBox1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textBox1.Size = new System.Drawing.Size(429, 19);
            this.textBox1.TabIndex = 5;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(1, 7);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(57, 12);
            this.label1.TabIndex = 6;
            this.label1.Text = "음악 경로";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(3, 129);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(29, 12);
            this.label2.TabIndex = 0;
            this.label2.Text = "음량";
            // 
            // button4
            // 
            this.button4.Enabled = false;
            this.button4.Location = new System.Drawing.Point(80, 0);
            this.button4.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(75, 21);
            this.button4.TabIndex = 7;
            this.button4.Text = "재생설정";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // listView1
            // 
            this.listView1.AllowDrop = true;
            this.listView1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.listView1.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2,
            this.columnHeader3,
            this.columnHeader4,
            this.columnHeader5});
            this.listView1.ContextMenuStrip = this.contextMenuStrip1;
            this.listView1.FullRowSelect = true;
            this.listView1.GridLines = true;
            this.listView1.Location = new System.Drawing.Point(0, 87);
            this.listView1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.listView1.Name = "listView1";
            this.listView1.Size = new System.Drawing.Size(537, 144);
            this.listView1.TabIndex = 8;
            this.listView1.UseCompatibleStateImageBehavior = false;
            this.listView1.View = System.Windows.Forms.View.Details;
            this.listView1.SelectedIndexChanged += new System.EventHandler(this.listView1_SelectedIndexChanged);
            this.listView1.DragDrop += new System.Windows.Forms.DragEventHandler(this.listView1_DragDrop);
            this.listView1.DragEnter += new System.Windows.Forms.DragEventHandler(this.listView1_DragEnter);
            this.listView1.DoubleClick += new System.EventHandler(this.listView1_DoubleClick);
            this.listView1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.listView1_KeyPress);
            this.listView1.KeyUp += new System.Windows.Forms.KeyEventHandler(this.listView1_KeyUp);
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "ID";
            this.columnHeader1.Width = 27;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "상태";
            this.columnHeader2.Width = 87;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "파일 이름";
            this.columnHeader3.Width = 127;
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "음악경로";
            this.columnHeader4.Width = 289;
            // 
            // columnHeader5
            // 
            this.columnHeader5.Text = "현재 시간";
            this.columnHeader5.Width = 0;
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.재생ToolStripMenuItem,
            this.일시정지ToolStripMenuItem,
            this.정지ToolStripMenuItem,
            this.toolStripSeparator1,
            this.삭제ToolStripMenuItem,
            this.toolStripSeparator2,
            this.원본크기구하기ToolStripMenuItem1,
            this.mD5해쉬값구하기ToolStripMenuItem,
            this.toolStripSeparator3,
            this.가사찾기ToolStripMenuItem,
            this.toolStripSeparator4,
            this.복사ToolStripMenuItem,
            this.탐색기에서해당파일열기ToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(217, 248);
            this.contextMenuStrip1.Opening += new System.ComponentModel.CancelEventHandler(this.contextMenuStrip1_Opening);
            // 
            // 재생ToolStripMenuItem
            // 
            this.재생ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.새인스턴스시작ToolStripMenuItem});
            this.재생ToolStripMenuItem.Enabled = false;
            this.재생ToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("재생ToolStripMenuItem.Image")));
            this.재생ToolStripMenuItem.Name = "재생ToolStripMenuItem";
            this.재생ToolStripMenuItem.Size = new System.Drawing.Size(216, 22);
            this.재생ToolStripMenuItem.Text = "재생";
            this.재생ToolStripMenuItem.Click += new System.EventHandler(this.재생ToolStripMenuItem_Click);
            // 
            // 새인스턴스시작ToolStripMenuItem
            // 
            this.새인스턴스시작ToolStripMenuItem.Checked = true;
            this.새인스턴스시작ToolStripMenuItem.CheckOnClick = true;
            this.새인스턴스시작ToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.새인스턴스시작ToolStripMenuItem.Name = "새인스턴스시작ToolStripMenuItem";
            this.새인스턴스시작ToolStripMenuItem.Size = new System.Drawing.Size(168, 22);
            this.새인스턴스시작ToolStripMenuItem.Text = "새 인스턴스 시작";
            // 
            // 일시정지ToolStripMenuItem
            // 
            this.일시정지ToolStripMenuItem.Enabled = false;
            this.일시정지ToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("일시정지ToolStripMenuItem.Image")));
            this.일시정지ToolStripMenuItem.Name = "일시정지ToolStripMenuItem";
            this.일시정지ToolStripMenuItem.Size = new System.Drawing.Size(216, 22);
            this.일시정지ToolStripMenuItem.Text = "일시정지";
            this.일시정지ToolStripMenuItem.Click += new System.EventHandler(this.일시정지ToolStripMenuItem_Click);
            // 
            // 정지ToolStripMenuItem
            // 
            this.정지ToolStripMenuItem.Enabled = false;
            this.정지ToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("정지ToolStripMenuItem.Image")));
            this.정지ToolStripMenuItem.Name = "정지ToolStripMenuItem";
            this.정지ToolStripMenuItem.Size = new System.Drawing.Size(216, 22);
            this.정지ToolStripMenuItem.Text = "정지";
            this.정지ToolStripMenuItem.Click += new System.EventHandler(this.정지ToolStripMenuItem_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(213, 6);
            // 
            // 삭제ToolStripMenuItem
            // 
            this.삭제ToolStripMenuItem.Enabled = false;
            this.삭제ToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("삭제ToolStripMenuItem.Image")));
            this.삭제ToolStripMenuItem.Name = "삭제ToolStripMenuItem";
            this.삭제ToolStripMenuItem.Size = new System.Drawing.Size(216, 22);
            this.삭제ToolStripMenuItem.Text = "삭제";
            this.삭제ToolStripMenuItem.Click += new System.EventHandler(this.삭제ToolStripMenuItem_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(213, 6);
            this.toolStripSeparator2.Visible = false;
            // 
            // 원본크기구하기ToolStripMenuItem1
            // 
            this.원본크기구하기ToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem2,
            this.toolStripTextBox2});
            this.원본크기구하기ToolStripMenuItem1.Enabled = false;
            this.원본크기구하기ToolStripMenuItem1.Image = ((System.Drawing.Image)(resources.GetObject("원본크기구하기ToolStripMenuItem1.Image")));
            this.원본크기구하기ToolStripMenuItem1.Name = "원본크기구하기ToolStripMenuItem1";
            this.원본크기구하기ToolStripMenuItem1.Size = new System.Drawing.Size(216, 22);
            this.원본크기구하기ToolStripMenuItem1.Text = "현재 위치의 웨이브 저장";
            this.원본크기구하기ToolStripMenuItem1.Visible = false;
            this.원본크기구하기ToolStripMenuItem1.Click += new System.EventHandler(this.toolStripMenuItem1_Click);
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(220, 22);
            this.toolStripMenuItem2.Text = "원본 크기 구하기";
            this.toolStripMenuItem2.Click += new System.EventHandler(this.toolStripMenuItem2_Click);
            // 
            // toolStripTextBox2
            // 
            this.toolStripTextBox2.Font = new System.Drawing.Font("굴림", 8.780488F);
            this.toolStripTextBox2.ForeColor = System.Drawing.SystemColors.ControlText;
            this.toolStripTextBox2.MaxLength = 15;
            this.toolStripTextBox2.Name = "toolStripTextBox2";
            this.toolStripTextBox2.Size = new System.Drawing.Size(160, 20);
            this.toolStripTextBox2.Tag = "크기 입력 (숫자만 입력가능)";
            this.toolStripTextBox2.Text = this.toolStripTextBox1.Text;
            this.toolStripTextBox2.ToolTipText = "파일로 현재 재생위치의 웨이브정보 저장할 숫자를 입력합니다.\n※주의: FFT최대사이즈보다 크게 지정하면 0으로 저장될수 있습니다.\n적당한값을 입력" +
                "해 주세요";
            this.toolStripTextBox2.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.toolStripTextBox2_KeyPress);
            this.toolStripTextBox2.Click += new System.EventHandler(this.toolStripTextBox2_Click);
            this.toolStripTextBox2.TextChanged += new System.EventHandler(this.toolStripTextBox2_TextChanged);
            // 
            // mD5해쉬값구하기ToolStripMenuItem
            // 
            this.mD5해쉬값구하기ToolStripMenuItem.Enabled = false;
            this.mD5해쉬값구하기ToolStripMenuItem.Name = "mD5해쉬값구하기ToolStripMenuItem";
            this.mD5해쉬값구하기ToolStripMenuItem.Size = new System.Drawing.Size(216, 22);
            this.mD5해쉬값구하기ToolStripMenuItem.Text = "MD5 해쉬값 구하기";
            this.mD5해쉬값구하기ToolStripMenuItem.Click += new System.EventHandler(this.mD5해쉬값구하기ToolStripMenuItem_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(213, 6);
            // 
            // 가사찾기ToolStripMenuItem
            // 
            this.가사찾기ToolStripMenuItem.Enabled = false;
            this.가사찾기ToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("가사찾기ToolStripMenuItem.Image")));
            this.가사찾기ToolStripMenuItem.Name = "가사찾기ToolStripMenuItem";
            this.가사찾기ToolStripMenuItem.Size = new System.Drawing.Size(216, 22);
            this.가사찾기ToolStripMenuItem.Text = "선택한 곡 싱크 가사 찾기";
            this.가사찾기ToolStripMenuItem.Click += new System.EventHandler(this.가사찾기ToolStripMenuItem_Click);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(213, 6);
            // 
            // 복사ToolStripMenuItem
            // 
            this.복사ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.음악이름ToolStripMenuItem,
            this.음악경로ToolStripMenuItem});
            this.복사ToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("복사ToolStripMenuItem.Image")));
            this.복사ToolStripMenuItem.Name = "복사ToolStripMenuItem";
            this.복사ToolStripMenuItem.Size = new System.Drawing.Size(216, 22);
            this.복사ToolStripMenuItem.Text = "복사";
            // 
            // 음악이름ToolStripMenuItem
            // 
            this.음악이름ToolStripMenuItem.Name = "음악이름ToolStripMenuItem";
            this.음악이름ToolStripMenuItem.Size = new System.Drawing.Size(128, 22);
            this.음악이름ToolStripMenuItem.Text = "음악 이름";
            this.음악이름ToolStripMenuItem.Click += new System.EventHandler(this.음악이름ToolStripMenuItem_Click);
            // 
            // 음악경로ToolStripMenuItem
            // 
            this.음악경로ToolStripMenuItem.Name = "음악경로ToolStripMenuItem";
            this.음악경로ToolStripMenuItem.Size = new System.Drawing.Size(128, 22);
            this.음악경로ToolStripMenuItem.Text = "음악 경로";
            this.음악경로ToolStripMenuItem.Click += new System.EventHandler(this.음악경로ToolStripMenuItem_Click);
            // 
            // 탐색기에서해당파일열기ToolStripMenuItem
            // 
            this.탐색기에서해당파일열기ToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("탐색기에서해당파일열기ToolStripMenuItem.Image")));
            this.탐색기에서해당파일열기ToolStripMenuItem.Name = "탐색기에서해당파일열기ToolStripMenuItem";
            this.탐색기에서해당파일열기ToolStripMenuItem.Size = new System.Drawing.Size(216, 22);
            this.탐색기에서해당파일열기ToolStripMenuItem.Text = "탐색기에서 해당파일 열기";
            this.탐색기에서해당파일열기ToolStripMenuItem.Click += new System.EventHandler(this.탐색기에서해당파일열기ToolStripMenuItem_Click);
            // 
            // textBox2
            // 
            this.textBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox2.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox2.Location = new System.Drawing.Point(103, 72);
            this.textBox2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.textBox2.Multiline = true;
            this.textBox2.Name = "textBox2";
            this.textBox2.ReadOnly = true;
            this.textBox2.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.textBox2.Size = new System.Drawing.Size(430, 11);
            this.textBox2.TabIndex = 10;
            this.textBox2.WordWrap = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(3, 72);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(101, 12);
            this.label3.TabIndex = 11;
            this.label3.Text = "선택한 음악경로: ";
            // 
            // timer1
            // 
            this.timer1.Interval = 25;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // openFileDialog2
            // 
            this.openFileDialog2.DefaultExt = "*.m3u";
            this.openFileDialog2.FileName = "새 재생목록.m3u";
            this.openFileDialog2.Filter = "재생목록 파일 (*.m3u)|*m3u";
            this.openFileDialog2.ShowReadOnly = true;
            this.openFileDialog2.Title = "재생목록 열기...";
            this.openFileDialog2.FileOk += new System.ComponentModel.CancelEventHandler(this.openFileDialog2_FileOk);
            // 
            // statusStrip1
            // 
            this.statusStrip1.AutoSize = false;
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel2,
            this.toolStripProgressBar1,
            this.toolStripStatusLabel3,
            this.toolStripStatusLabel1,
            this.toolStripProgressBar2});
            this.statusStrip1.Location = new System.Drawing.Point(0, 232);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(537, 20);
            this.statusStrip1.SizingGrip = false;
            this.statusStrip1.TabIndex = 12;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripStatusLabel2
            // 
            this.toolStripStatusLabel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.toolStripStatusLabel2.BorderSides = ((System.Windows.Forms.ToolStripStatusLabelBorderSides)((((System.Windows.Forms.ToolStripStatusLabelBorderSides.Left | System.Windows.Forms.ToolStripStatusLabelBorderSides.Top)
                        | System.Windows.Forms.ToolStripStatusLabelBorderSides.Right)
                        | System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom)));
            this.toolStripStatusLabel2.BorderStyle = System.Windows.Forms.Border3DStyle.Raised;
            this.toolStripStatusLabel2.Name = "toolStripStatusLabel2";
            this.toolStripStatusLabel2.Size = new System.Drawing.Size(33, 15);
            this.toolStripStatusLabel2.Text = "취소";
            this.toolStripStatusLabel2.Visible = false;
            this.toolStripStatusLabel2.Click += new System.EventHandler(this.toolStripStatusLabel2_Click);
            // 
            // toolStripProgressBar1
            // 
            this.toolStripProgressBar1.Name = "toolStripProgressBar1";
            this.toolStripProgressBar1.Size = new System.Drawing.Size(100, 14);
            this.toolStripProgressBar1.Visible = false;
            // 
            // toolStripStatusLabel3
            // 
            this.toolStripStatusLabel3.Name = "toolStripStatusLabel3";
            this.toolStripStatusLabel3.Size = new System.Drawing.Size(29, 15);
            this.toolStripStatusLabel3.Text = "준비";
            this.toolStripStatusLabel3.Visible = false;
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(29, 15);
            this.toolStripStatusLabel1.Text = "준비";
            this.toolStripStatusLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // toolStripProgressBar2
            // 
            this.toolStripProgressBar2.AutoSize = false;
            this.toolStripProgressBar2.Name = "toolStripProgressBar2";
            this.toolStripProgressBar2.Size = new System.Drawing.Size(199, 14);
            this.toolStripProgressBar2.Visible = false;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.button3);
            this.panel1.Controls.Add(this.textBox1);
            this.panel1.Enabled = false;
            this.panel1.Location = new System.Drawing.Point(0, 24);
            this.panel1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(537, 23);
            this.panel1.TabIndex = 13;
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Checked = true;
            this.checkBox1.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBox1.Location = new System.Drawing.Point(162, 3);
            this.checkBox1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(70, 16);
            this.checkBox1.TabIndex = 14;
            this.checkBox1.Text = "전체반복";
            this.checkBox1.UseVisualStyleBackColor = true;
            this.checkBox1.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
            // 
            // saveFileDialog1
            // 
            this.saveFileDialog1.DefaultExt = "*.m3u";
            this.saveFileDialog1.FileName = "새 재생목록";
            this.saveFileDialog1.Filter = "재생목록 파일(*.m3u)|*.m3u|Resource단위 재생목록 (*.lstres)|*.lstres";
            this.saveFileDialog1.Tag = "재생목록 파일(*.m3u)|*.m3u|Resource단위 재생목록 (*.lstres)|*.lstres";
            this.saveFileDialog1.Title = "재생목록 저장...";
            this.saveFileDialog1.FileOk += new System.ComponentModel.CancelEventHandler(this.saveFileDialog1_FileOk);
            // 
            // saveFileDialog2
            // 
            this.saveFileDialog2.Filter = "텍스트 파일 (*.txt)|*.txt|모든 파일 (*.*)|*.*";
            this.saveFileDialog2.SupportMultiDottedExtensions = true;
            this.saveFileDialog2.FileOk += new System.ComponentModel.CancelEventHandler(this.saveFileDialog2_FileOk);
            // 
            // timer2
            // 
            this.timer2.Interval = 500;
            this.timer2.Tick += new System.EventHandler(this.timer2_Tick);
            // 
            // button5
            // 
            this.button5.Enabled = false;
            this.button5.Location = new System.Drawing.Point(86, 47);
            this.button5.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(75, 21);
            this.button5.TabIndex = 15;
            this.button5.Text = "일시정지";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Visible = false;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.button2);
            this.panel2.Controls.Add(this.button4);
            this.panel2.Controls.Add(this.checkBox1);
            this.panel2.Location = new System.Drawing.Point(86, 47);
            this.panel2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(240, 21);
            this.panel2.TabIndex = 16;
            // 
            // timer3
            // 
            this.timer3.Interval = 1;
            this.timer3.Tick += new System.EventHandler(this.timer3_Tick);
            // 
            // timer4
            // 
            this.timer4.Tick += new System.EventHandler(this.timer4_Tick);
            // 
            // folderBrowserDialog1
            // 
            this.folderBrowserDialog1.Description = "음악 파일이 들어있는 폴더 선택";
            // 
            // openFileDialog3
            // 
            this.openFileDialog3.FileName = "제목 없음";
            this.openFileDialog3.Filter = "프로젝트 파일(*.project)|*.project";
            // 
            // notifyIcon1
            // 
            this.notifyIcon1.ContextMenuStrip = this.contextMenuStrip2;
            this.notifyIcon1.Icon = ((System.Drawing.Icon)(resources.GetObject("notifyIcon1.Icon")));
            this.notifyIcon1.Text = "조장찡 플레이어";
            this.notifyIcon1.Visible = true;
            this.notifyIcon1.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.notifyIcon1_MouseDoubleClick);
            this.notifyIcon1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.notifyIcon1_MouseMove);
            // 
            // contextMenuStrip2
            // 
            this.contextMenuStrip2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.설정TToolStripMenuItem,
            this.보기VToolStripMenuItem1,
            this.도움말HToolStripMenuItem1,
            this.toolStripSeparator15,
            this.재생컨트롤ToolStripMenuItem,
            this.toolStripSeparator16,
            this.재생설정열기ToolStripMenuItem,
            this.메인창열기ToolStripMenuItem,
            this.toolStripSeparator20,
            this.끝내기XToolStripMenuItem});
            this.contextMenuStrip2.Name = "contextMenuStrip2";
            this.contextMenuStrip2.Size = new System.Drawing.Size(157, 176);
            this.contextMenuStrip2.Opening += new System.ComponentModel.CancelEventHandler(this.contextMenuStrip2_Opening);
            // 
            // 설정TToolStripMenuItem
            // 
            this.설정TToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.파일복구및재설치ToolStripMenuItem1,
            this.toolStripSeparator18,
            this.프로그램다시시작ToolStripMenuItem1,
            this.toolStripSeparator19,
            this.toolStripMenuItem7});
            this.설정TToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("설정TToolStripMenuItem.Image")));
            this.설정TToolStripMenuItem.Name = "설정TToolStripMenuItem";
            this.설정TToolStripMenuItem.Size = new System.Drawing.Size(156, 22);
            this.설정TToolStripMenuItem.Text = "설정(&T)";
            // 
            // 파일복구및재설치ToolStripMenuItem1
            // 
            this.파일복구및재설치ToolStripMenuItem1.Name = "파일복구및재설치ToolStripMenuItem1";
            this.파일복구및재설치ToolStripMenuItem1.Size = new System.Drawing.Size(292, 22);
            this.파일복구및재설치ToolStripMenuItem1.Text = "사운드 시스템 엔진 파일 복구 및 재설치";
            this.파일복구및재설치ToolStripMenuItem1.Click += new System.EventHandler(this.파일복구및재설치ToolStripMenuItem_Click);
            // 
            // toolStripSeparator18
            // 
            this.toolStripSeparator18.Name = "toolStripSeparator18";
            this.toolStripSeparator18.Size = new System.Drawing.Size(289, 6);
            // 
            // 프로그램다시시작ToolStripMenuItem1
            // 
            this.프로그램다시시작ToolStripMenuItem1.Name = "프로그램다시시작ToolStripMenuItem1";
            this.프로그램다시시작ToolStripMenuItem1.Size = new System.Drawing.Size(292, 22);
            this.프로그램다시시작ToolStripMenuItem1.Text = "프로그램 다시 시작";
            this.프로그램다시시작ToolStripMenuItem1.Click += new System.EventHandler(this.프로그램다시시작ToolStripMenuItem_Click);
            // 
            // toolStripSeparator19
            // 
            this.toolStripSeparator19.Name = "toolStripSeparator19";
            this.toolStripSeparator19.Size = new System.Drawing.Size(289, 6);
            // 
            // toolStripMenuItem7
            // 
            this.toolStripMenuItem7.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.높음ToolStripMenuItem1,
            this.높은우선순위ToolStripMenuItem1,
            this.보통권장ToolStripMenuItem1,
            this.낮은우선순위ToolStripMenuItem1});
            this.toolStripMenuItem7.Name = "toolStripMenuItem7";
            this.toolStripMenuItem7.Size = new System.Drawing.Size(292, 22);
            this.toolStripMenuItem7.Text = "프로그램 우선순위";
            // 
            // 높음ToolStripMenuItem1
            // 
            this.높음ToolStripMenuItem1.Name = "높음ToolStripMenuItem1";
            this.높음ToolStripMenuItem1.Size = new System.Drawing.Size(180, 22);
            this.높음ToolStripMenuItem1.Text = "높음";
            this.높음ToolStripMenuItem1.Click += new System.EventHandler(this.높음ToolStripMenuItem_Click);
            // 
            // 높은우선순위ToolStripMenuItem1
            // 
            this.높은우선순위ToolStripMenuItem1.Name = "높은우선순위ToolStripMenuItem1";
            this.높은우선순위ToolStripMenuItem1.Size = new System.Drawing.Size(180, 22);
            this.높은우선순위ToolStripMenuItem1.Text = "이 프로그램 우선";
            this.높은우선순위ToolStripMenuItem1.Click += new System.EventHandler(this.높은우선순위ToolStripMenuItem_Click);
            // 
            // 보통권장ToolStripMenuItem1
            // 
            this.보통권장ToolStripMenuItem1.Checked = true;
            this.보통권장ToolStripMenuItem1.CheckState = System.Windows.Forms.CheckState.Checked;
            this.보통권장ToolStripMenuItem1.Name = "보통권장ToolStripMenuItem1";
            this.보통권장ToolStripMenuItem1.Size = new System.Drawing.Size(180, 22);
            this.보통권장ToolStripMenuItem1.Text = "보통 (권장)";
            this.보통권장ToolStripMenuItem1.Click += new System.EventHandler(this.보통권장ToolStripMenuItem_Click);
            // 
            // 낮은우선순위ToolStripMenuItem1
            // 
            this.낮은우선순위ToolStripMenuItem1.Name = "낮은우선순위ToolStripMenuItem1";
            this.낮은우선순위ToolStripMenuItem1.Size = new System.Drawing.Size(180, 22);
            this.낮은우선순위ToolStripMenuItem1.Text = "다른 프로그램 우선";
            this.낮은우선순위ToolStripMenuItem1.Click += new System.EventHandler(this.낮은우선순위ToolStripMenuItem_Click);
            // 
            // 보기VToolStripMenuItem1
            // 
            this.보기VToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.바탕화면싱크가사창띄우기ToolStripMenuItem1});
            this.보기VToolStripMenuItem1.Image = ((System.Drawing.Image)(resources.GetObject("보기VToolStripMenuItem1.Image")));
            this.보기VToolStripMenuItem1.Name = "보기VToolStripMenuItem1";
            this.보기VToolStripMenuItem1.Size = new System.Drawing.Size(156, 22);
            this.보기VToolStripMenuItem1.Text = "보기(&V)";
            // 
            // 바탕화면싱크가사창띄우기ToolStripMenuItem1
            // 
            this.바탕화면싱크가사창띄우기ToolStripMenuItem1.Name = "바탕화면싱크가사창띄우기ToolStripMenuItem1";
            this.바탕화면싱크가사창띄우기ToolStripMenuItem1.Size = new System.Drawing.Size(232, 22);
            this.바탕화면싱크가사창띄우기ToolStripMenuItem1.Text = "바탕화면 싱크 가사창 띄우기";
            this.바탕화면싱크가사창띄우기ToolStripMenuItem1.Click += new System.EventHandler(this.바탕화면싱크가사창띄우기ToolStripMenuItem_Click);
            // 
            // 도움말HToolStripMenuItem1
            // 
            this.도움말HToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.정보ToolStripMenuItem1,
            this.업데이트변경사항보기ToolStripMenuItem1,
            this.toolStripSeparator17,
            this.디버깅ToolStripMenuItem1});
            this.도움말HToolStripMenuItem1.Name = "도움말HToolStripMenuItem1";
            this.도움말HToolStripMenuItem1.Size = new System.Drawing.Size(156, 22);
            this.도움말HToolStripMenuItem1.Text = "도움말(&H)";
            this.도움말HToolStripMenuItem1.Click += new System.EventHandler(this.도움말HToolStripMenuItem_DropDownOpening);
            // 
            // 정보ToolStripMenuItem1
            // 
            this.정보ToolStripMenuItem1.Image = ((System.Drawing.Image)(resources.GetObject("정보ToolStripMenuItem1.Image")));
            this.정보ToolStripMenuItem1.Name = "정보ToolStripMenuItem1";
            this.정보ToolStripMenuItem1.Size = new System.Drawing.Size(222, 22);
            this.정보ToolStripMenuItem1.Text = "프로그램 정보...(&A)";
            this.정보ToolStripMenuItem1.Click += new System.EventHandler(this.정보ToolStripMenuItem_Click);
            // 
            // 업데이트변경사항보기ToolStripMenuItem1
            // 
            this.업데이트변경사항보기ToolStripMenuItem1.Image = ((System.Drawing.Image)(resources.GetObject("업데이트변경사항보기ToolStripMenuItem1.Image")));
            this.업데이트변경사항보기ToolStripMenuItem1.Name = "업데이트변경사항보기ToolStripMenuItem1";
            this.업데이트변경사항보기ToolStripMenuItem1.Size = new System.Drawing.Size(222, 22);
            this.업데이트변경사항보기ToolStripMenuItem1.Text = "업데이트 변경사항 보기(&R)";
            this.업데이트변경사항보기ToolStripMenuItem1.Click += new System.EventHandler(this.업데이트변경사항보기ToolStripMenuItem_Click);
            // 
            // toolStripSeparator17
            // 
            this.toolStripSeparator17.Name = "toolStripSeparator17";
            this.toolStripSeparator17.Size = new System.Drawing.Size(219, 6);
            // 
            // 디버깅ToolStripMenuItem1
            // 
            this.디버깅ToolStripMenuItem1.Name = "디버깅ToolStripMenuItem1";
            this.디버깅ToolStripMenuItem1.ShortcutKeys = System.Windows.Forms.Keys.F10;
            this.디버깅ToolStripMenuItem1.Size = new System.Drawing.Size(222, 22);
            this.디버깅ToolStripMenuItem1.Text = "디버깅 (개발자 전용)";
            this.디버깅ToolStripMenuItem1.Click += new System.EventHandler(this.디버깅ToolStripMenuItem_Click);
            // 
            // toolStripSeparator15
            // 
            this.toolStripSeparator15.Name = "toolStripSeparator15";
            this.toolStripSeparator15.Size = new System.Drawing.Size(153, 6);
            // 
            // 재생컨트롤ToolStripMenuItem
            // 
            this.재생컨트롤ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.재생ToolStripMenuItem1,
            this.정지ToolStripMenuItem1});
            this.재생컨트롤ToolStripMenuItem.Name = "재생컨트롤ToolStripMenuItem";
            this.재생컨트롤ToolStripMenuItem.Size = new System.Drawing.Size(156, 22);
            this.재생컨트롤ToolStripMenuItem.Text = "재생 컨트롤";
            this.재생컨트롤ToolStripMenuItem.DropDownOpening += new System.EventHandler(this.재생컨트롤ToolStripMenuItem_DropDownOpening);
            // 
            // 재생ToolStripMenuItem1
            // 
            this.재생ToolStripMenuItem1.Image = ((System.Drawing.Image)(resources.GetObject("재생ToolStripMenuItem1.Image")));
            this.재생ToolStripMenuItem1.Name = "재생ToolStripMenuItem1";
            this.재생ToolStripMenuItem1.Size = new System.Drawing.Size(100, 22);
            this.재생ToolStripMenuItem1.Text = "재생";
            this.재생ToolStripMenuItem1.Click += new System.EventHandler(this.button1_Click);
            // 
            // 정지ToolStripMenuItem1
            // 
            this.정지ToolStripMenuItem1.Image = ((System.Drawing.Image)(resources.GetObject("정지ToolStripMenuItem1.Image")));
            this.정지ToolStripMenuItem1.Name = "정지ToolStripMenuItem1";
            this.정지ToolStripMenuItem1.Size = new System.Drawing.Size(100, 22);
            this.정지ToolStripMenuItem1.Text = "정지";
            this.정지ToolStripMenuItem1.Click += new System.EventHandler(this.button2_Click);
            // 
            // toolStripSeparator16
            // 
            this.toolStripSeparator16.Name = "toolStripSeparator16";
            this.toolStripSeparator16.Size = new System.Drawing.Size(153, 6);
            // 
            // 재생설정열기ToolStripMenuItem
            // 
            this.재생설정열기ToolStripMenuItem.Name = "재생설정열기ToolStripMenuItem";
            this.재생설정열기ToolStripMenuItem.Size = new System.Drawing.Size(156, 22);
            this.재생설정열기ToolStripMenuItem.Text = "재생 설정 열기";
            this.재생설정열기ToolStripMenuItem.Click += new System.EventHandler(this.button4_Click);
            // 
            // 메인창열기ToolStripMenuItem
            // 
            this.메인창열기ToolStripMenuItem.Name = "메인창열기ToolStripMenuItem";
            this.메인창열기ToolStripMenuItem.Size = new System.Drawing.Size(156, 22);
            this.메인창열기ToolStripMenuItem.Text = "메인창 열기";
            this.메인창열기ToolStripMenuItem.Click += new System.EventHandler(this.메인창열기ToolStripMenuItem_Click);
            // 
            // toolStripSeparator20
            // 
            this.toolStripSeparator20.Name = "toolStripSeparator20";
            this.toolStripSeparator20.Size = new System.Drawing.Size(153, 6);
            // 
            // 끝내기XToolStripMenuItem
            // 
            this.끝내기XToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("끝내기XToolStripMenuItem.Image")));
            this.끝내기XToolStripMenuItem.Name = "끝내기XToolStripMenuItem";
            this.끝내기XToolStripMenuItem.Size = new System.Drawing.Size(156, 22);
            this.끝내기XToolStripMenuItem.Text = "끝내기(&X)";
            this.끝내기XToolStripMenuItem.Click += new System.EventHandler(this.끝내기ToolStripMenuItem_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 11F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(537, 252);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.listView1);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.trackBar1);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.menuStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "Form1";
            this.Text = "조장찡 플레이어";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.SizeChanged += new System.EventHandler(this.Form1_SizeChanged);
            ((System.ComponentModel.ISupportInitialize)(this.trackBar1)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.contextMenuStrip1.ResumeLayout(false);
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.contextMenuStrip2.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TrackBar trackBar1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem 파일FToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 열기OToolStripMenuItem;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.ListView listView1;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem 재생ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 정지ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 삭제ToolStripMenuItem;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.ToolStripMenuItem 일시정지ToolStripMenuItem;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private System.Windows.Forms.ColumnHeader columnHeader5;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.ToolStripMenuItem 재생목록열기ToolStripMenuItem;
        private System.Windows.Forms.OpenFileDialog openFileDialog2;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.ToolStripProgressBar toolStripProgressBar1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel2;
        private System.Windows.Forms.ToolStripMenuItem 설정ToolStripMenuItem;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.CheckBox checkBox1;
        public System.Windows.Forms.ToolStripMenuItem 음악믹싱모드ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 재생목록다른이름으로저장ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 파일로웨이브저장ToolStripMenuItem;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.SaveFileDialog saveFileDialog2;
        private System.Windows.Forms.ToolStripTextBox toolStripTextBox1;
        private System.Windows.Forms.ToolStripMenuItem 원본사이즈사용ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 도움말HToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 정보ToolStripMenuItem;
        private System.Windows.Forms.Timer timer2;
        private System.Windows.Forms.ToolStripMenuItem 끝내기ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 원본크기구하기ToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem2;
        private System.Windows.Forms.ToolStripTextBox toolStripTextBox2;
        private System.Windows.Forms.ToolStripMenuItem ANISToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem UTF8ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 유니코드ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem4;
        private System.Windows.Forms.ToolStripMenuItem 우선순위ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 높음ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 높은우선순위ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 보통권장ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 낮은우선순위ToolStripMenuItem;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.ToolStripMenuItem LOL우선순위ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem LOL높은우선순위ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem LOL보통권장ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 안전모드ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 디버깅ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem syncToolStripMenuItem;
        private System.Windows.Forms.ToolStripProgressBar toolStripProgressBar2;
        private System.Windows.Forms.ToolStripMenuItem 프로그램다시시작ToolStripMenuItem;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel3;
        private System.Windows.Forms.ToolStripMenuItem 업데이트변경사항보기ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 복사ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 음악이름ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 음악경로ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 탐색기에서해당파일열기ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 가사찾기ToolStripMenuItem;
        private System.Windows.Forms.Timer timer3;
        private System.Windows.Forms.Timer timer4;
        private System.Windows.Forms.ToolStripMenuItem 새인스턴스시작ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 맨위로설정ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 보기VToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 바탕화면싱크가사창띄우기ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 싱크가사찾기ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 상태표시줄업데이트ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 파일열기ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 폴더열기ToolStripMenuItem;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
        private System.Windows.Forms.ToolStripComboBox cbMimetype;
        private System.Windows.Forms.ToolStripMenuItem 하위폴더검색ToolStripMenuItem;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.ToolStripMenuItem 프로젝트저장ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 프로젝트열기ToolStripMenuItem;
        private System.Windows.Forms.OpenFileDialog openFileDialog3;
        private System.Windows.Forms.SaveFileDialog saveFileDialog3;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator6;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator7;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator8;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator9;
        private System.Windows.Forms.ToolStripMenuItem 파일복구및재설치ToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator10;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator11;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator12;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator13;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator14;
        private System.Windows.Forms.NotifyIcon notifyIcon1;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip2;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator15;
        private System.Windows.Forms.ToolStripMenuItem 재생컨트롤ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 재생ToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem 정지ToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem 메인창열기ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 트레이로보내기ToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator16;
        private System.Windows.Forms.ToolStripMenuItem 보기VToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem 바탕화면싱크가사창띄우기ToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem 설정TToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 파일복구및재설치ToolStripMenuItem1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator18;
        private System.Windows.Forms.ToolStripMenuItem 프로그램다시시작ToolStripMenuItem1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator19;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem7;
        private System.Windows.Forms.ToolStripMenuItem 높음ToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem 높은우선순위ToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem 보통권장ToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem 낮은우선순위ToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem 도움말HToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem 정보ToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem 업데이트변경사항보기ToolStripMenuItem1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator17;
        private System.Windows.Forms.ToolStripMenuItem 디버깅ToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem 끝내기XToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 재생설정열기ToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator20;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator21;
        private System.Windows.Forms.ToolStripMenuItem mD5해쉬값구하기ToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator22;
        private System.Windows.Forms.ToolStripMenuItem 싱크가사로컬캐시ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 싱크가사캐시정리ToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator23;
        private System.Windows.Forms.ToolStripMenuItem 싱크가사캐시관리ToolStripMenuItem;
    }
}

