﻿using System;
using System.Collections.Generic;
using System.Drawing;

using System.Text;
using System.Windows.Forms;
using HS_Audio_Lib;
using HS_Audio_Helper;
using HS_Audio_Helper.Forms;
using HS_Audio_Helper.Recoder.Forms;

namespace HS_Audio
{
    public partial class PlayerSetting : Form
    {
        HS_Audio_Helper.HSAudioHelper _Helper;
        public HS_Audio_Helper.HSAudioHelper Helper { get { return _Helper; }
            set
            {
                value.PlayingStatusChanged += new HSAudioHelper.PlayingStatusChangedEventHandler(snd_PlayingStatusChanged);
                value.MusicPathChanged += new HSAudioHelper.MusicPathChangedEventHandler(Helper_MusicPathChanged);
                _Helper = value;
                try { Helper_MusicPathChanged(null, -1); }catch{}
            }
        }
        frmMain frm;
        internal HS_Audio_Helper.Forms.frmEqulazer fe;
        public PlayerSetting(HS_Audio_Helper.HSAudioHelper Helper)
        {
            InitializeComponent(); Common();
            this.Helper = Helper;
            Helper.Initializing += new HSAudioHelper.InitializingEventHandler(Helper_Initializing);
            Helper.PlayingStatusChanged += new HSAudioHelper.PlayingStatusChangedEventHandler(snd_PlayingStatusChanged);
            comboBox3.Items.AddRange(Enum.GetNames(typeof(MODE)));
            comboBox4.Items.AddRange(Enum.GetNames(typeof(TIMEUNIT))); comboBox4.Text = "MS";
            checkBox1.Checked = Helper.HelperEx.LoudnessEqualization;
            try { comboBox1.Text = Helper.HelperEx.ReverbPreset.ToString().Replace("_", " "); }
            catch { comboBox1.Text = "없음"; }
            float speed = 0; Helper.sound.getMusicSpeed(ref speed); numSpeed.Value = (decimal)speed;
            //numGain.Value = (decimal)snd.HelperEx.FMODChannelInfo.LowPassGain;
            float frenq = 0; Helper.channel.getFrequency(ref frenq); numFrenq.Value = (decimal)frenq;
            float Pan = 0; Helper.channel.getPan(ref Pan); numPan.Value = (decimal)Pan;
            float Speed = 0; Helper.sound.getMusicSpeed(ref Speed); numSpeed.Value = (decimal)Speed;
            MODE mode = MODE._2D; Helper.sound.getMode(ref mode); comboBox3.Text = mode.ToString();
            fhp = Helper.HelperEx.FMODChannelInfo;
            f3 = new frm3D(Helper);
            fe = new frmEqulazer(Helper);
            timer1.Start();
        }
        public PlayerSetting(HS_Audio_Helper.HSAudioHelper Helper, frmMain frm)
        {
            InitializeComponent(); Common();
            this.Helper = Helper;
            Helper.Initializing += new HSAudioHelper.InitializingEventHandler(Helper_Initializing);
            Helper.PlayingStatusChanged += new HSAudioHelper.PlayingStatusChangedEventHandler(snd_PlayingStatusChanged);
            this.frm = frm;
            comboBox3.Items.AddRange(Enum.GetNames(typeof(MODE)));
            comboBox4.Items.AddRange(Enum.GetNames(typeof(TIMEUNIT))); comboBox4.Text = "MS";
            checkBox1.Checked = Helper.HelperEx.LoudnessEqualization;
            try { comboBox1.Text = Helper.HelperEx.ReverbPreset.ToString().Replace("_", " "); }
            catch { comboBox1.Text = "없음"; }
            float speed = 0; Helper.sound.getMusicSpeed(ref speed); numSpeed.Value = (decimal)speed;
            //numGain.Value = (decimal)snd.HelperEx.FMODChannelInfo.LowPassGain;
            float frenq = 0; Helper.channel.getFrequency(ref frenq); numFrenq.Value = (decimal)frenq;
            float Pan = 0; Helper.channel.getPan(ref Pan); numPan.Value = (decimal)Pan;
            float Speed = 0; Helper.sound.getMusicSpeed(ref Speed); numSpeed.Value = (decimal)Speed;
            MODE mode = MODE._2D; Helper.sound.getMode(ref mode); comboBox3.Text = mode.ToString();

            f3 = new frm3D(Helper);
            fe = new frmEqulazer(Helper);
            trackBar1.Maximum = (int)Helper.GetTotalTick(tu);
            timer1.Start();
        }
        public PlayerSetting(HS_Audio_Helper.HSAudioHelper Helper, string FileName)
        {
            InitializeComponent();
            comboBox3.Items.AddRange(Enum.GetNames(typeof(MODE)));
            comboBox4.Items.AddRange(Enum.GetNames(typeof(TIMEUNIT))); comboBox4.Text = "MS";
            Common();
            this.Helper = Helper;
            Helper.Initializing += new HSAudioHelper.InitializingEventHandler(Helper_Initializing);
            Helper.PlayingStatusChanged += new HSAudioHelper.PlayingStatusChangedEventHandler(snd_PlayingStatusChanged);
            checkBox1.Checked = Helper.HelperEx.LoudnessEqualization;
            comboBox1.Text = Helper.HelperEx.ReverbPreset.ToString().Replace("_", " ");
            float speed = 0; Helper.sound.getMusicSpeed(ref speed); numSpeed.Value = (decimal)speed;
            //numGain.Value = (decimal)snd.HelperEx.FMODChannelInfo.LowPassGain;
            float frenq = 0; Helper.channel.getFrequency(ref frenq); numFrenq.Value = (decimal)frenq;
            float Pan = 0; Helper.channel.getPan(ref Pan); numPan.Value = (decimal)Pan;
            float Speed = 0; Helper.sound.getMusicSpeed(ref Speed); numSpeed.Value = (decimal)Speed;
            MODE mode = MODE._2D; Helper.sound.getMode(ref mode); comboBox3.Text = mode.ToString();
            fhp = Helper.HelperEx.FMODChannelInfo;

            f3 = new frm3D(Helper);
            fe = new frmEqulazer(Helper);
            this.FileName = FileName;
            trackBar1.Maximum = (int)Helper.TotalPosition;
            timer1.Start();
        }
        public PlayerSetting(HS_Audio_Helper.HSAudioHelper Helper, string FileName, frmMain frm)
        {
            InitializeComponent();
            this.frm = frm;
            comboBox3.Items.AddRange(Enum.GetNames(typeof(MODE)));
            comboBox4.Items.AddRange(Enum.GetNames(typeof(TIMEUNIT))); comboBox4.Text = "MS";
            Common();
            this.Helper = Helper;
            Helper.Initializing+=new HSAudioHelper.InitializingEventHandler(Helper_Initializing);
            Helper.PlayingStatusChanged += new HSAudioHelper.PlayingStatusChangedEventHandler(snd_PlayingStatusChanged);
            checkBox1.Checked = Helper.HelperEx.LoudnessEqualization;
            comboBox1.Text = Helper.HelperEx.ReverbPreset.ToString().Replace("_", " ");
            float speed = 0; Helper.sound.getMusicSpeed(ref speed); numSpeed.Value = (decimal)speed;
            //numGain.Value = (decimal)snd.HelperEx.FMODChannelInfo.LowPassGain;
            float frenq = 0; Helper.channel.getFrequency(ref frenq); numFrenq.Value = (decimal)frenq;
            float Pan = 0; Helper.channel.getPan(ref Pan); numPan.Value = (decimal)Pan;
            float Speed = 0; Helper.sound.getMusicSpeed(ref Speed); numSpeed.Value = (decimal)Speed;
            MODE mode = MODE._2D; Helper.sound.getMode(ref mode); comboBox3.Text = mode.ToString();
            fhp = Helper.HelperEx.FMODChannelInfo;

            f3 = new frm3D(Helper);
            fe = new frmEqulazer(Helper);
            this.FileName = FileName;
            trackBar1.Maximum = (int)Helper.TotalPosition;
            timer1.Start();
        }

        private void Common()
        {
            toolStripComboBox1.Text = toolStripComboBox1.Items[0].ToString(); 
            reverb.CustomReverbValueChanged += new HS_Audio_Helper.Forms.frmReverb.CustomReverbValueChangedEventHandler(CustomReverb_CustomReverbValueChanged);
            reverb.CustomReverbGainValueChanged += new HS_Audio_Helper.Forms.frmReverb.CustomReverbGainValueChangedEventHandler(CustomReverb_CustomReverbGainValueChanged);
        }
        string FileName;
        public string Title { get { return FileName; } 
            set { FileName=value; this.Text = "음향 효과 설정" + (value==""||value==null?"":" - "+value); } }

        private void PlayerSetting_Load(object sender, EventArgs e)
        {
            //comboBox1.Text = "없음";
            //comboBox2.Text = "STEREO";
            //comboBox1.SelectedValue = -1;
            uint numBufferLength_Value = 0; int numNumBuffers_Value = 0;
            Helper.system.getDSPBufferSize(ref numBufferLength_Value, ref numNumBuffers_Value);
            numBufferLength.Value = numBufferLength_Value; numNumBuffers.Value = numNumBuffers_Value;
        }

        HS_Audio_Helper.Forms.frmReverb reverb = new HS_Audio_Helper.Forms.frmReverb() {IsClose = false };
        private void comboBox1_SelectedValueChanged(object sender, EventArgs e)
        {
            if (comboBox1.Text == "없음") { /*groupBox10.Enabled = */button1.Enabled = false; } else { groupBox10.Enabled = button1.Enabled = true; }
            if (comboBox1.Text != "(사용자 설정)")
                Helper.HelperEx.ReverbPreset = (HS_Audio_Helper.HSAudioReverbPreset)Enum.Parse(typeof(HS_Audio_Helper.HSAudioReverbPreset), comboBox1.Text.Replace(" ", "_"));
            else
            {
               // Helper.HelperEx.ReverbProperties = FMODHelperEx.REVERB_PROPERTIES_GENERIC;
                reverb.UpdatePreset();
                reverb.Show(); reverb.BringToFront();
            }
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox1.Checked) { Helper.HelperEx.LoudnessEqualization = true; }
            else { Helper.HelperEx.LoudnessEqualization = false; }
        }

        HS_Audio_Helper.Properties.HSAudioChannelStruct fhp = new HS_Audio_Helper.Properties.HSAudioChannelStruct();
        internal void numericUpDown2_ValueChanged(object sender, EventArgs e)
        {
            numFrenq.Value = (decimal)Helper.DefaultFrequency + numPitch.Value;
            UpdateSetting();
        }

        private void numericUpDown1_ValueChanged(object sender, EventArgs e)
        {
            fhp.Pan = ((float)numPan.Value);
            Helper.channel.setPan(fhp.Pan);
            //snd.HelperEx.FMODChannelInfo = fhp;
        }

        private void trbVolumeLeft_ValueChanged(object sender, EventArgs e)
        {//snd.channel.set
            this.lblVolumeLeft.Text = trbVolumeLeft.Value.ToString();

            if (chkGain.Checked)
            {
                if (trbVolumeLeft.Value == 0) { Helper.Volume = 0f; statusStrip1.Text = Helper.Result.ToString(); }
                else { Helper.Volume = (this.reverb.Gain * 0.01f + 1) + (float)(trbVolumeLeft.Value * 3 * 0.001f); statusStrip1.Text = Helper.Result.ToString(); }
            }
            else { Helper.Volume = (float)(trbVolumeLeft.Value * 0.01f); statusStrip1.Text = Helper.Result.ToString(); }

            if (trbVolumeLeft.Value > 100) { lblVolumeLeft.ForeColor = Color.Red; }
            else { lblVolumeLeft.ForeColor = Color.Black; }
        }

        internal void numericUpDown4_ValueChanged(object sender, EventArgs e)
        {
            //snd.channel.setFrequency((float)numFrenq.Value);
            numPitch.Value = numFrenq.Value-(decimal)Helper.DefaultFrequency;
            Helper.Frequency = (float)numFrenq.Value;
            Helper.system.update();
            //fhp.Frequency = (float)numFrenq.Value;
            //Helper.HelperEx.FMODChannelInfo = fhp;
        }

        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            SPEAKERMODE sm = (SPEAKERMODE)Enum.Parse(typeof(SPEAKERMODE), comboBox2.Text);
            Helper.system.getSpeakerMode(ref sm);
            Helper.system.setSpeakerMode((SPEAKERMODE)Enum.Parse(typeof(SPEAKERMODE), comboBox2.Text));
            Helper.system.update();
        }

        private void comboBox3_SelectedValueChanged(object sender, EventArgs e)
        {
            lblFMODResult.Text = "Wait";
            MODE md = MODE._2D; Helper.sound.getMode(ref md);
            lblFMODResult.Text = Helper.channel.setMode((MODE)Enum.Parse(typeof(MODE), comboBox3.Text)).ToString();
            if (comboBox3.SelectedIndex == 3 || comboBox3.SelectedIndex == 2) { lblFMODResult.Text = Helper.sound.setLoopPoints(1, HS_Audio_Lib.TIMEUNIT.PCMBYTES, Helper.GetTotalTick(HS_Audio_Lib.TIMEUNIT.PCMBYTES) - 1, HS_Audio_Lib.TIMEUNIT.PCMBYTES).ToString(); }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            frmReverb reverb = new frmReverb(Helper.HelperEx.ReverbProperties, comboBox1.Text, Gain);

            reverb.CustomReverbValueChanged += new HS_Audio_Helper.Forms.frmReverb.CustomReverbValueChangedEventHandler(CustomReverb_CustomReverbValueChanged);
            reverb.CustomReverbGainValueChanged += new HS_Audio_Helper.Forms.frmReverb.CustomReverbGainValueChangedEventHandler(CustomReverb_CustomReverbGainValueChanged);
            reverb.Show(); reverb.BringToFront();
            //try { reverb.Show(); }
            //catch(ObjectDisposedException) { reverb = new frmSetCustomReverb(snd.HelperEx.ReverbProperties, Gain); reverb.Show(); }
        }
        Dictionary<string, string> ReverbPreset;
        private void CustomReverb_CustomReverbValueChanged(HS_Audio_Lib.REVERB_PROPERTIES value, Dictionary<string, string> Preset)
        {
            ReverbPreset = Preset;
            Helper.HelperEx.ReverbProperties = value;
        }

        float Gain = 1;
        private void CustomReverb_CustomReverbGainValueChanged(float Gain)
        {
            if (Gain == 0) { Helper.Volume = 0; }
            else { string a = Gain.ToString(); Helper.Volume = (a == "NaN" ? 0 : Gain + (float)(trbVolumeLeft.Value * 0.001)); this.Gain = Gain; }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            Helper.Pause();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Helper.Play();
            timer1.Start();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            Helper.Stop();
        }

        private void numGain_ValueChanged(object sender, EventArgs e)
        {
            Helper.channel.setLowPassGain((float)numGain.Value);
            //snd.HelperEx.FMODChannelInfo = fhp;
        }

        private void numSpeed_ValueChanged(object sender, EventArgs e)
        {

        }

        private void button6_Click(object sender, EventArgs e)
        {
            MessageBox.Show("오른쪽 마우스 버튼을 많이 이용해 주세요 ^ㅇ^", "스펙트럼 이용하기전에 잠깐! :)");
            frmFFTGraph fg = new frmFFTGraph(Helper);
            fg.Show(); fg.StartDraw(); fg.BringToFront();
        }


        HS_Audio_Helper.Forms.frmDSP frmdsp;
        private void button7_Click(object sender, EventArgs e)
        {
            if (frmdsp == null) { frmdsp = new frmDSP(this.Helper); frmdsp.Show(); }
            else { frmdsp.Show();frmdsp.BringToFront(); }
            /*
            if (frm == null) { frmdsp = new frmDSP(this.Helper);frmdsp.Show();}
            else
            {
                if (frm.frmdsp == null) { frm.frmdsp = new frmDSP(this.Helper); frmdsp.Show(); }
                else { frm.frmdsp.Show(); frm.frmdsp.BringToFront(); }
            }*/
            //frmDSPHelper ds = new frmDSPHelper(snd); ds.Show();
        }

        private void trbVolumeLeft_Scroll(object sender, EventArgs e)
        {

        }

        private void trackBar2_ValueChanged(object sender, EventArgs e)
        {
            //trackBar2.Maximum = 500;
            Helper.Mix = trackBar2.Value * 0.002f;
            button12.Text = trackBar2.Value.ToString();
        }

        /*
        private void checkBox2_CheckedChanged(object sender, EventArgs e)
        {
            if (chkGain.Checked == true) { trbVolumeLeft.Maximum = 150; }
            else { trbVolumeLeft.Maximum = 100; }
        }*/

        private void chkGain_MouseHover(object sender, EventArgs e)
        {
            toolTip1.Show("음량을 반향 효과(Reverb)의 Gain값과 연결합니다.", chkGain, 4000);
        }

        private void groupBox10_MouseHover(object sender, EventArgs e)
        {
            toolTip1.Show("반향 효과(Reverb) 에서의 믹서 입니다.\n" +
                "(반향 효과(Reverb)를 선택하면 활성화 됩니다.) ", groupBox10, 5000);
        }

        frm3D f3;
        private void button2_Click(object sender, EventArgs e)
        {
            f3.Show(); f3.BringToFront();
        }

        private void trackBar1_ValueChanged(object sender, EventArgs e)
        {
            if (PositionChange) {  Helper.CurrentPosition = (uint)trackBar1.Value * 10; PositionChange = false; }
        }

        public static string ShowTime(TimeSpan Time)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(Time.Hours.ToString("00")); sb.Append(":");
            sb.Append(Time.Minutes.ToString("00")); sb.Append(":");
            sb.Append(Time.Seconds.ToString("00")); sb.Append(".");
            sb.Append(Time.Milliseconds.ToString("000"));
            return sb.ToString();
        }

        uint Currentpos;
        uint Totalpos;

        StringBuilder timer1_Tick_sb = new StringBuilder();
        private void timer1_Tick(object sender, EventArgs e)
        {
            try { Currentpos = Helper.GetCurrentTick(tu); Totalpos = Helper.GetTotalTick(tu); }
            catch { }
            try
            {
                StringBuilder sb = timer1_Tick_sb;
                sb.Append(Currentpos.ToString()); sb.Append(" / "); sb.Append(Currentpos.ToString());//##,###
                
                label8.Text = sb.ToString(); sb.Remove(0, sb.Length);

                sb.Append(Currentpos.ToString(ShowTime(TimeSpan.FromMilliseconds((double)Helper.CurrentPosition)))); sb.Append(" / ");
                sb.Append(Currentpos.ToString(ShowTime(TimeSpan.FromMilliseconds((double)Helper.TotalPosition))));
                label7.Text = sb.ToString(); sb.Remove(0, sb.Length);
            }
            catch { }
            try
            {
                trackBar1.Maximum = (int)Helper.GetTotalTick(TIMEUNIT.MS)/10;
                trackBar1.Value = (int)Helper.GetCurrentTick(TIMEUNIT.MS)/10;
                Helper.system.update();
                //string text = snd.GetTotalTick(tu).ToString();
            }
            catch { }
            //if()
        }

        bool PositionChange;
        private void trackBar1_Scroll(object sender, EventArgs e)
        {
            //snd.channel.setPosition((uint)trackBar1.Value,tu);
            PositionChange = true;
            //Helper.CurrentPosition = (uint)trackBar1.Value*10;
        }

        private void comboBox3_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void chk반복재생_CheckedChanged(object sender, EventArgs e)
        {
            Helper.Loop = chk반복재생.Checked;
        }

        HS_Audio_Lib.TIMEUNIT tu = TIMEUNIT.MS;
        private void comboBox4_SelectedIndexChanged(object sender, EventArgs e)
        {
            tu = (TIMEUNIT)Enum.Parse(typeof(TIMEUNIT), comboBox4.SelectedItem.ToString());
            //timer1_Tick(null, null);
        }
        private void snd_PlayingStatusChanged(HSAudioHelper.PlayingStatus status, int index)
        {/* trackBar1.Maximum = (int)snd.GetTotalTick(TIMEUNIT.MS);*/
            if (status == HSAudioHelper.PlayingStatus.Play) { Helper.Mix = trackBar2.Value * 0.002f; }
        }

        private void button8_Click(object sender, EventArgs e)
        {
            numFrenq.Value = (decimal)Helper.DefaultFrequency;
        }

        private void checkBox2_CheckedChanged(object sender, EventArgs e)
        {
            Helper.TickErrorPatch = checkBox2.Checked;
            //if (checkBox2.Checked) { snd.channel.setMode(MODE.LOOP_NORMAL); }
            //else { snd.channel.setMode(MODE.LOOP_OFF); }
        }

        private void trackBar2_Scroll(object sender, EventArgs e)
        {

        }


        private void button9_Click(object sender, EventArgs e)
        {
            if (/*frm != null*/false)
            {
                if (frm.fe == null) { frm.fe = new frmEqulazer(Helper); }
                frm.fe.Show(); frm.fe.BringToFront();
            }
            else
            {
                if (fe == null) { fe = new frmEqulazer(Helper); }
                fe.Show(); fe.BringToFront();
            }
        }

        private void PlayerSetting_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = true;
            this.Hide();
        }

        private void trackBar3_Scroll(object sender, EventArgs e)
        {

        }

        private void button10_Click(object sender, EventArgs e)
        {
            frmWaveDataOutputRecoding fr = new frmWaveDataOutputRecoding(Helper);
            fr.Show();
        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {

        }

        HS_Audio_Helper.Forms.frmReverb reverb1 = new HS_Audio_Helper.Forms.frmReverb();
        private void comboBox5_SelectedValueChanged(object sender, EventArgs e)
        {
            if (comboBox1.Text == "없음") { groupBox10.Enabled = button1.Enabled = false; } else { groupBox10.Enabled = button1.Enabled = true; }
            if (comboBox1.Text != "(사용자 설정)")
                Helper.HelperEx.ReverbPreset = (HS_Audio_Helper.HSAudioReverbPreset)Enum.Parse(typeof(HS_Audio_Helper.HSAudioReverbPreset), comboBox5.Text.Replace(" ", "_"));
            else
            {
                Helper.HelperEx.ReverbProperties = HSAudioHelperEx.REVERB_PROPERTIES_GENERIC;

                reverb1.CustomReverbValueChanged += new HS_Audio_Helper.Forms.frmReverb.CustomReverbValueChangedEventHandler(CustomReverb_CustomReverbValueChanged_1);
                reverb1.CustomReverbGainValueChanged += new HS_Audio_Helper.Forms.frmReverb.CustomReverbGainValueChangedEventHandler(CustomReverb_CustomReverbGainValueChanged_1);
                reverb1.Show();
            }
        }


        private void CustomReverb_CustomReverbValueChanged_1(REVERB_PROPERTIES pro, Dictionary<string, string> Preset)
        {

        }
        private void CustomReverb_CustomReverbGainValueChanged_1(float Gain)
        {

        }

        private void checkBox3_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox3.Checked)
            {
                comboBox1.Enabled = button1.Enabled = checkBox1.Enabled = true;
                if (Helper.system != null)
                {

                }
            }
            else
            {
                if (Helper.system != null)
                {
                    REVERB_PROPERTIES rv = HSAudioHelperEx.REVERB_PROPERTIES_GENERIC;
                    Helper.system.setReverbProperties(ref rv);
                }
                comboBox1.Enabled = button1.Enabled = checkBox1.Enabled = false;
            }
        }

        private void button12_Click(object sender, EventArgs e)
        {
            Helper.UpdateMix();
        }

        private void fMODChannelToolStripMenuItem_Click(object sender, EventArgs e)
        {
            fMODChannelToolStripMenuItem.Checked = true;
            fMODSystemToolStripMenuItem.Checked = false;
            Helper.dsp.IsSystem = false;
            Helper.UpdateMix();
        }

        private void fMODSystemToolStripMenuItem_Click(object sender, EventArgs e)
        {
            fMODChannelToolStripMenuItem.Checked = false;
            fMODSystemToolStripMenuItem.Checked = true;
            Helper.dsp.IsSystem = true;
            Helper.UpdateMix();
        }

        private void checkBox6_CheckedChanged(object sender, EventArgs e)
        {
            
        }

        private void label8_Click(object sender, EventArgs e)
        {

        }

        private void label7_Click(object sender, EventArgs e)
        {

        }

        private void groupBox10_Enter(object sender, EventArgs e)
        {

        }

        private void groupBox9_Enter(object sender, EventArgs e)
        {

        }

        private void groupBox8_Enter(object sender, EventArgs e)
        {

        }

        private void groupBox7_Enter(object sender, EventArgs e)
        {

        }

        private void groupBox6_Enter(object sender, EventArgs e)
        {

        }

        private void groupBox5_Enter(object sender, EventArgs e)
        {

        }

        private void groupBox4_Enter(object sender, EventArgs e)
        {

        }

        private void groupBox3_Enter(object sender, EventArgs e)
        {

        }

        private void groupBox2_Enter(object sender, EventArgs e)
        {

        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void toolStripComboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            
        }

        private void numBufferLength_ValueChanged(object sender, EventArgs e)
        {

        }

        private void numNumBuffers_ValueChanged(object sender, EventArgs e)
        {

        }

        internal void Helper_Initializing(HS_Audio_Lib.System system)
        {
            /*
            //toolStripComboBox1.SelectedItem = toolStripComboBox1.Items[0];
            //FMOD.RESULT r= system.init(2, INITFLAGS.NORMAL, IntPtr.Zero);
            lblFMODResult.Text = Helper.system.setOutput((OUTPUTTYPE)Enum.Parse(typeof(OUTPUTTYPE), toolStripComboBox1.SelectedItem.ToString())).ToString();
            lblFMODResult.Text = 
            system.setDSPBufferSize((uint)numBufferLength.Value, (int)numNumBuffers.Value).ToString();            
            lblFMODResult.Text = 
            system.setDSPBufferSize((uint)numBufferLength.Value, (int)numNumBuffers.Value).ToString();
            */
        }

        string ProjectDir
        {
            get
            {
                if(!System.IO.Directory.Exists(Application.StartupPath+"\\Project"))
                    System.IO.Directory.CreateDirectory(Application.StartupPath+"\\Project");
                return Application.StartupPath + "\\Project";
            }
        }

        public void UpdateSetting() { LastSetting1 = Setting; }
        public Dictionary<string, string> LastSetting, LastSetting1;
        public Dictionary<string, string> Setting
        {
            get
            {
                Dictionary<string, string> a = new Dictionary<string, string>();
                try{a.Add("EQPreset", HS_CSharpUtility.Utility.StringUtility.ConvertArrayToString(
                                  HS_CSharpUtility.Utility.EtcUtility.SaveSetting(fe.EQPreset), "##"));}catch{}
                try{a.Add("EQPresetUse", fe.EQUse.Checked.ToString()); }catch{}
                try{a.Add("DSPPreset",HS_CSharpUtility.Utility.StringUtility.ConvertArrayToString(
                                  HS_CSharpUtility.Utility.EtcUtility.SaveSetting(frmdsp.DSPPreset), "##"));}catch{}
                try{a.Add("DSPPresetUse", frmdsp.DSPUse.ToString()); }catch{}
                try{a.Add("ReverbPreset",HS_CSharpUtility.Utility.StringUtility.ConvertArrayToString(
                                  HS_CSharpUtility.Utility.EtcUtility.SaveSetting(ReverbPreset), "##"));}catch{}
                try{a.Add("ReverbName",comboBox1.Text);}catch{}
                try{a.Add("Volume",trbVolumeLeft.Value.ToString());}catch{}
                try{a.Add("MixerIsSystem",fMODSystemToolStripMenuItem.Checked.ToString());}catch{}
                try{a.Add("Mixer",trackBar2.Value.ToString());}catch{}
                try{a.Add("Pan",numPan.Value.ToString());}catch{}
                try{a.Add("Pitch", numPitch.Value.ToString());}catch{}
                try{a.Add("Frequency", numFrenq.Value.ToString());} catch{}
                try{a.Add("ErrorPatch",checkBox2.Checked.ToString());}catch{}
                try{a.Add("IsLoop",chk반복재생.Checked.ToString());}catch{}
                return a;
            }
            set
            {
                Dictionary<string, string> a = LastSetting = value;
                try{Dictionary<string, string> c=HS_CSharpUtility.Utility.EtcUtility.LoadSetting(
                                    HS_CSharpUtility.Utility.StringUtility.ConvertStringToArray(a["EQPreset"], "##"));
                if(fe==null){fe = new frmEqulazer(Helper);}fe.EQPreset=c;}catch{}
                if (frmdsp == null) frmdsp = new frmDSP(this.Helper); 
                try{frmdsp.DSPUse=bool.Parse(a["DSPPresetUse"]);}catch { }
                try
                {Dictionary<string, string> c=frmdsp.DSPPreset = HS_CSharpUtility.Utility.EtcUtility.LoadSetting(
                                    HS_CSharpUtility.Utility.StringUtility.ConvertStringToArray(a["DSPPreset"], "##")); frmdsp.DSPPreset = c;}catch { }
                try{fe.EQUse.Checked=bool.Parse(a["EQPresetUse"]);}catch { }
                try
                { HS_Audio_Helper.Forms.frmReverb fr = new frmReverb();
                    fr.ReverbPreset=HS_CSharpUtility.Utility.EtcUtility.LoadSetting(
                                    HS_CSharpUtility.Utility.StringUtility.ConvertStringToArray(a["ReverbPreset"], "##"));}catch{}

                try{comboBox1.Text=a["ReverbName"];}catch{}
                try{trbVolumeLeft.Value=int.Parse(a["Volume"]);}catch{}
                try{fMODSystemToolStripMenuItem.Checked=bool.Parse(a["MixerIsSystem"]);
                     fMODChannelToolStripMenuItem.Checked=!fMODSystemToolStripMenuItem.Checked;}catch{}
                try{trackBar2.Value=int.Parse(a["Mixer"]);}catch{}
                try{numFrenq.Value = decimal.Parse(a["Frequency"]);}catch { }
                try{numPitch.Value = Calculator(a["Pitch"],(decimal)Helper.DefaultFrequency);}catch { }
                try{numPan.Value=decimal.Parse(a["Pan"]);}catch{}
                try{checkBox2.Checked=bool.Parse(a["ErrorPatch"]);}catch{}
                try{chk반복재생.Checked=bool.Parse(a["IsLoop"]);}catch{}
            }
        }

        private void 프로젝트저장ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            saveFileDialog1.InitialDirectory = ProjectDir;
            string asd = HS_CSharpUtility.Utility.StringUtility.GetFileName(FileName, false);
            saveFileDialog1.FileName = asd==null||asd==""?"제목 없음":asd;
            if(saveFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                System.IO.File.WriteAllLines(saveFileDialog1.FileName, HS_CSharpUtility.Utility.EtcUtility.SaveSetting(Setting));
            }
        }

        internal string[] LastPreset; internal string LastPresetPath;
        private void 프로젝트열기OToolStripMenuItem_Click(object sender, EventArgs e)
        {
            openFileDialog1.InitialDirectory = ProjectDir;
            string asd =HS_CSharpUtility.Utility.StringUtility.GetFileName(FileName, false);
            openFileDialog1.FileName = asd == null || asd == "" ? "제목 없음" : asd;
            if (openFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                LastPresetPath = openFileDialog1.FileName;
                string[] b = LastPreset = System.IO.File.ReadAllLines(openFileDialog1.FileName);
                try { Setting = HS_CSharpUtility.Utility.EtcUtility.LoadSetting(b); 프리셋관리ToolStripMenuItem.Enabled = true; }
                catch { MessageBox.Show("프로젝트 파일이 올바르지 못합니다!!", "프리셋 열기 오류"); }
            }
        }

        internal decimal Calculator(string Number, decimal Value)
        {
            if (Number.Contains("x/")&&Value!=0)
            {return Value/decimal.Parse(Number.Split('/')[1]);}
            else if(Number.Contains("x*"))
            { return Value * decimal.Parse(Number.Split('*')[1]); }
            else return decimal.Parse(Number);
        }

        private void 맨위로설정ToolStripMenuItem_CheckedChanged(object sender, EventArgs e)
        {
            this.TopMost = 맨위로설정ToolStripMenuItem.Checked;
        }

        private void btnExit_Click(object sender, EventArgs e) { this.Hide(); }
        private void 닫기XToolStripMenuItem_Click(object sender, EventArgs e) { this.Hide(); }

        void Helper_MusicPathChanged(string Path, int index)
        {
            numPitch.Maximum = numFrenq.Maximum - (decimal)Helper.DefaultFrequency;
            numPitch.Minimum = numFrenq.Minimum - (decimal)Helper.DefaultFrequency;

            if (자동업데이트ToolStripMenuItem.Checked &&
                프리셋업데이트ToolStripMenuItem.Enabled)
            {
                if (파일에서업데이트ToolStripMenuItem.Checked && LastPreset != null && LastPreset.Length>0)
                {
                    try
                    {
                        string[] b = LastPreset = System.IO.File.ReadAllLines(LastPresetPath);
                        Setting = LastSetting = HS_CSharpUtility.Utility.EtcUtility.LoadSetting(b);
                    }
                    catch { MessageBox.Show("프로젝트를 파일에서 업데이트하지 못했습니다!!", "프로젝트 업데이트 오류"); }
                }
                else { try { Setting = Setting = HS_CSharpUtility.Utility.EtcUtility.LoadSetting(LastPreset); } catch { Setting = Setting; } }
            }
            else { Setting = Setting; }
        }

        private void 프리셋업데이트ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (LastPresetPath != null && LastPreset.Length == 0) { MessageBox.Show("프로젝트를 먼저 선택하여 주시기 바랍니다.", "프로젝트 업데이트 오류"); }
            else
            {
                if (파일에서업데이트ToolStripMenuItem.Checked && LastPresetPath != null && LastPreset.Length > 0)
                {
                    try
                    {
                        string[] b = LastPreset = System.IO.File.ReadAllLines(LastPresetPath);
                        Setting = LastSetting = HS_CSharpUtility.Utility.EtcUtility.LoadSetting(b);
                    }
                    catch (Exception ex) { MessageBox.Show("프로젝트를 파일에서 업데이트하지 못했습니다!!\n\n이유: " + ex.Message, "프로젝트 업데이트 오류"); }
                }
                else { Setting = HS_CSharpUtility.Utility.EtcUtility.LoadSetting(LastPreset); }
            }
        }

        private void 설정ToolStripMenuItem_DropDownOpening(object sender, EventArgs e)
        {
            프리셋관리ToolStripMenuItem.Enabled = LastPresetPath != null;
        }

        private void numPan_ValueChanged(object sender, EventArgs e)
        {
            Helper.channel.setPan((float)numPan.Value);
            Helper.system.update();
        }

        private void button13_Click(object sender, EventArgs e)
        {
            HS_Audio_Helper.Forms.frmChannelReverbTest rt = new frmChannelReverbTest(this.Helper);
            rt.Show();
        }
    }
}
