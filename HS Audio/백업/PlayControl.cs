﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace HS_Audio.Control
{
    public partial class PlayControl : UserControl
    {
        public delegate void PlayButtonPressEventHandler();
        public delegate void PauseButtonPressEventHandler();
        public delegate void StopButtonPressEventHandler();
        public event PlayButtonPressEventHandler PlayButtonPress;
        public event PauseButtonPressEventHandler PauseButtonPress;
        public event StopButtonPressEventHandler StopButtonPress;

        public PlayControl()
        {
            InitializeComponent();
        }

        private void btnPlay_Click(object sender, EventArgs e){try{PlayButtonPress();}catch{}}

        private void btnPause_Click(object sender, EventArgs e){try{PauseButtonPress();}catch{}}

        private void btnStop_Click(object sender, EventArgs e) {try{StopButtonPress();}catch{}}

        bool btnPlayFocused, btnPauseFocused,btnStopFocused;
        private void btnPlay_MouseEnter(object sender, EventArgs e) {btnPlayFocused=true; btnPlay.ForeColor = _PlayButtonFocusColor; }
        private void btnPlay_MouseLeave(object sender, EventArgs e) {btnPlayFocused=false; btnPlay.ForeColor = _PlayButtonDefaultColor; }
        private void btnPause_MouseEnter(object sender, EventArgs e) { btnPauseFocused=true;btnPause.ForeColor = _PauseButtonFocusColor; }
        private void btnPause_MouseLeave(object sender, EventArgs e) { btnPauseFocused=false;btnPause.ForeColor = _PauseButtonDefaultColor; }
        private void btnStop_MouseEnter(object sender, EventArgs e) {btnStopFocused=true; btnStop.ForeColor = _StopButtonFocusColor; }
        private void btnStop_MouseLeave(object sender, EventArgs e) {btnStopFocused=false; btnStop.ForeColor = _StopButtonDefaultColor; }


        #region 프로퍼티
        Color _PlayButtonFocusColor= Color.White,
              _PauseButtonFocusColor= Color.White,
              _StopButtonFocusColor = Color.White;
        /// <summary>
        /// 재생 버튼위로 커서를 올릴때의 색깔 입니다.
        /// </summary>
        [Description("재생 버튼위로 커서를 올릴때의 색깔을 지정합니다."),Category("버튼 색깔"),DefaultValue("White")]
        public Color PlayButtonFocusColor{get{return _PlayButtonFocusColor;}set{_PlayButtonFocusColor=value;if(btnPlayFocused)btnPlay.ForeColor=value;}}
        /// <summary>
        /// 일시정지 버튼위로 커서를 올릴때의 색깔 입니다.
        /// </summary>
        [Description("일시정지 버튼위로 커서를 올릴때의 색깔을 지정합니다."),Category("버튼 색깔"),DefaultValue("White")]
        public Color PauseButtonFocusColor{get{return _PauseButtonFocusColor;}set{_PauseButtonFocusColor=value;if(btnPauseFocused)btnPause.ForeColor=value;}}
        /// <summary>
        /// 정지 버튼위로 커서를 올릴때의 색깔 입니다.
        /// </summary>
        [Description("정지 버튼위로 커서를 올릴때의 색깔을 지정합니다."),Category("버튼 색깔"),DefaultValue("White")]
        public Color StopButtonFocusColor{get{return _StopButtonFocusColor;}set{_StopButtonFocusColor=value;if(btnStopFocused)btnStop.ForeColor=value;}}

        Color _PlayButtonDefaultColor = Color.Black,
              _PauseButtonDefaultColor = Color.Black,
              _StopButtonDefaultColor = Color.Black;
        /// <summary>
        /// 재생 버튼위로 커서를 올릴떄의 색깔 입니다.
        /// </summary>
        [Description("재생 버튼위로 커서를 올릴때의 색깔을 지정합니다."),Category("버튼 색깔"),DefaultValue("Black")]
        public Color PlayButtonDefaultColor{get{return _PlayButtonDefaultColor;}set{_PlayButtonDefaultColor=value;if(!btnPlayFocused)btnPlay.ForeColor=value;}}
        /// <summary>
        /// 일시정지 버튼위로 커서를 올릴떄의 색깔 입니다.
        /// </summary>
        [Description("일시정지 버튼위로 커서를 올릴때의 색깔을 지정합니다."),Category("버튼 색깔"),DefaultValue("Black")]
        public Color PauseButtonDefaultColor{get{return _PauseButtonDefaultColor;}set{_PauseButtonDefaultColor=value;if(!btnPlayFocused)btnPlay.ForeColor=value;}}
        /// <summary>
        /// 정지 버튼위로 커서를 올릴떄의 색깔 입니다.
        /// </summary>
        [Description("정지 버튼위로 커서를 올릴때의 색깔을 지정합니다."),Category("버튼 색깔"),DefaultValue("Black")]
        public Color StopButtonDefaultColor{get{return _StopButtonDefaultColor;}set{_StopButtonDefaultColor=value;if(!btnPlayFocused)btnPlay.ForeColor=value;}}
        #endregion
        #region 프로퍼티 숨기기
        private new Color ForeColor;
        #endregion
    }
}
