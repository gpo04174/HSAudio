﻿namespace HS_Audio
{
    partial class PlayerSetting
    {
        /// <summary> 
        /// 필수 디자이너 변수입니다.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 사용 중인 모든 리소스를 정리합니다.
        /// </summary>
        /// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
            if(Helper!=null)Helper.Dispose();
        }

        #region 구성 요소 디자이너에서 생성한 코드

        /// <summary> 
        /// 디자이너 지원에 필요한 메서드입니다. 
        /// 이 메서드의 내용을 코드 편집기로 수정하지 마십시오.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PlayerSetting));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.numPitch = new System.Windows.Forms.NumericUpDown();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.chkGain = new System.Windows.Forms.CheckBox();
            this.label2 = new System.Windows.Forms.Label();
            this.lblVolumeLeft = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.trbVolumeRight = new System.Windows.Forms.TrackBar();
            this.trbVolumeLeft = new System.Windows.Forms.TrackBar();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.checkBox5 = new System.Windows.Forms.CheckBox();
            this.checkBox4 = new System.Windows.Forms.CheckBox();
            this.button11 = new System.Windows.Forms.Button();
            this.checkBox3 = new System.Windows.Forms.CheckBox();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.button1 = new System.Windows.Forms.Button();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.comboBox5 = new System.Windows.Forms.ComboBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.numPan = new System.Windows.Forms.NumericUpDown();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.label5 = new System.Windows.Forms.Label();
            this.numSpeed = new System.Windows.Forms.NumericUpDown();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.button8 = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.numFrenq = new System.Windows.Forms.NumericUpDown();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.numGain = new System.Windows.Forms.NumericUpDown();
            this.button2 = new System.Windows.Forms.Button();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.comboBox2 = new System.Windows.Forms.ComboBox();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this.comboBox3 = new System.Windows.Forms.ComboBox();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel2 = new System.Windows.Forms.ToolStripStatusLabel();
            this.lblFMODResult = new System.Windows.Forms.ToolStripStatusLabel();
            this.button6 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.checkBox2 = new System.Windows.Forms.CheckBox();
            this.trackBar1 = new System.Windows.Forms.TrackBar();
            this.button12 = new System.Windows.Forms.Button();
            this.groupBox11 = new System.Windows.Forms.GroupBox();
            this.numNumBuffers = new System.Windows.Forms.NumericUpDown();
            this.label9 = new System.Windows.Forms.Label();
            this.numBufferLength = new System.Windows.Forms.NumericUpDown();
            this.label4 = new System.Windows.Forms.Label();
            this.groupBox10 = new System.Windows.Forms.GroupBox();
            this.label3 = new System.Windows.Forms.Label();
            this.trackBar2 = new System.Windows.Forms.TrackBar();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.fMODSystemToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.fMODChannelToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.label7 = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.chk반복재생 = new System.Windows.Forms.CheckBox();
            this.comboBox4 = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.button9 = new System.Windows.Forms.Button();
            this.button10 = new System.Windows.Forms.Button();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.파일FToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.프로젝트열기OToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.프로젝트저장ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.닫기XToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.설정ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.맨위로설정ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.사운드출력방법선택ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripComboBox1 = new System.Windows.Forms.ToolStripComboBox();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.프리셋관리ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.자동업데이트ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.파일에서업데이트ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.프리셋업데이트ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.btnExit = new System.Windows.Forms.Button();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.button13 = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numPitch)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trbVolumeRight)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trbVolumeLeft)).BeginInit();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numPan)).BeginInit();
            this.groupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numSpeed)).BeginInit();
            this.groupBox6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numFrenq)).BeginInit();
            this.groupBox7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numGain)).BeginInit();
            this.groupBox8.SuspendLayout();
            this.groupBox9.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar1)).BeginInit();
            this.groupBox11.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numNumBuffers)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numBufferLength)).BeginInit();
            this.groupBox10.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar2)).BeginInit();
            this.contextMenuStrip1.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.numPitch);
            this.groupBox1.Location = new System.Drawing.Point(136, 86);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox1.Size = new System.Drawing.Size(90, 48);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "피치설정";
            this.groupBox1.Enter += new System.EventHandler(this.groupBox1_Enter);
            // 
            // numPitch
            // 
            this.numPitch.DecimalPlaces = 1;
            this.numPitch.Increment = new decimal(new int[] {
            50,
            0,
            0,
            0});
            this.numPitch.Location = new System.Drawing.Point(3, 21);
            this.numPitch.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.numPitch.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.numPitch.Minimum = new decimal(new int[] {
            1000,
            0,
            0,
            -2147483648});
            this.numPitch.Name = "numPitch";
            this.numPitch.Size = new System.Drawing.Size(81, 21);
            this.numPitch.TabIndex = 6;
            this.toolTip1.SetToolTip(this.numPitch, "만약 음악이 바뀔때 업데이트 하고싶지 않으면\r\n[설정(T)]->[프리셋 관리]->[자동 업데이트] 를\r\n해제해 주시기 바랍니다.");
            this.numPitch.ValueChanged += new System.EventHandler(this.numericUpDown2_ValueChanged);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.chkGain);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.lblVolumeLeft);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Controls.Add(this.trbVolumeRight);
            this.groupBox2.Controls.Add(this.trbVolumeLeft);
            this.groupBox2.Location = new System.Drawing.Point(0, 2);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox2.Size = new System.Drawing.Size(65, 194);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "음량";
            this.groupBox2.Enter += new System.EventHandler(this.groupBox2_Enter);
            // 
            // chkGain
            // 
            this.chkGain.AutoSize = true;
            this.chkGain.Location = new System.Drawing.Point(7, 14);
            this.chkGain.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.chkGain.Name = "chkGain";
            this.chkGain.Size = new System.Drawing.Size(50, 16);
            this.chkGain.TabIndex = 66;
            this.chkGain.Text = "Gain";
            this.chkGain.UseVisualStyleBackColor = true;
            this.chkGain.CheckedChanged += new System.EventHandler(this.checkBox2_CheckedChanged);
            this.chkGain.MouseHover += new System.EventHandler(this.chkGain_MouseHover);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(71, 144);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(23, 12);
            this.label2.TabIndex = 6;
            this.label2.Text = "100";
            // 
            // lblVolumeLeft
            // 
            this.lblVolumeLeft.AutoSize = true;
            this.lblVolumeLeft.BackColor = System.Drawing.Color.Transparent;
            this.lblVolumeLeft.ForeColor = System.Drawing.Color.Black;
            this.lblVolumeLeft.Location = new System.Drawing.Point(16, 178);
            this.lblVolumeLeft.Name = "lblVolumeLeft";
            this.lblVolumeLeft.Size = new System.Drawing.Size(23, 12);
            this.lblVolumeLeft.TabIndex = 5;
            this.lblVolumeLeft.Text = "100";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(24, 33);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(69, 12);
            this.label1.TabIndex = 4;
            this.label1.Text = "좌          우";
            this.label1.Visible = false;
            // 
            // trbVolumeRight
            // 
            this.trbVolumeRight.AutoSize = false;
            this.trbVolumeRight.Location = new System.Drawing.Point(61, 27);
            this.trbVolumeRight.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.trbVolumeRight.Maximum = 100;
            this.trbVolumeRight.Name = "trbVolumeRight";
            this.trbVolumeRight.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.trbVolumeRight.Size = new System.Drawing.Size(45, 112);
            this.trbVolumeRight.TabIndex = 3;
            this.trbVolumeRight.TickFrequency = 5;
            this.trbVolumeRight.TickStyle = System.Windows.Forms.TickStyle.Both;
            this.trbVolumeRight.Value = 100;
            this.trbVolumeRight.Visible = false;
            // 
            // trbVolumeLeft
            // 
            this.trbVolumeLeft.AutoSize = false;
            this.trbVolumeLeft.Location = new System.Drawing.Point(9, 33);
            this.trbVolumeLeft.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.trbVolumeLeft.Maximum = 100;
            this.trbVolumeLeft.Name = "trbVolumeLeft";
            this.trbVolumeLeft.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.trbVolumeLeft.Size = new System.Drawing.Size(45, 142);
            this.trbVolumeLeft.TabIndex = 2;
            this.trbVolumeLeft.TickFrequency = 5;
            this.trbVolumeLeft.TickStyle = System.Windows.Forms.TickStyle.Both;
            this.trbVolumeLeft.Value = 100;
            this.trbVolumeLeft.Scroll += new System.EventHandler(this.trbVolumeLeft_Scroll);
            this.trbVolumeLeft.ValueChanged += new System.EventHandler(this.trbVolumeLeft_ValueChanged);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.checkBox5);
            this.groupBox3.Controls.Add(this.checkBox4);
            this.groupBox3.Controls.Add(this.button11);
            this.groupBox3.Controls.Add(this.checkBox3);
            this.groupBox3.Controls.Add(this.checkBox1);
            this.groupBox3.Controls.Add(this.button1);
            this.groupBox3.Controls.Add(this.comboBox1);
            this.groupBox3.Controls.Add(this.comboBox5);
            this.groupBox3.Location = new System.Drawing.Point(136, 2);
            this.groupBox3.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox3.Size = new System.Drawing.Size(275, 67);
            this.groupBox3.TabIndex = 2;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "반향 효과(Reverb) 설정";
            this.groupBox3.Enter += new System.EventHandler(this.groupBox3_Enter);
            // 
            // checkBox5
            // 
            this.checkBox5.AutoSize = true;
            this.checkBox5.Enabled = false;
            this.checkBox5.Location = new System.Drawing.Point(178, 14);
            this.checkBox5.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.checkBox5.Name = "checkBox5";
            this.checkBox5.Size = new System.Drawing.Size(172, 16);
            this.checkBox5.TabIndex = 9;
            this.checkBox5.Text = "라우드니스 이퀼라이제이션";
            this.checkBox5.UseVisualStyleBackColor = true;
            this.checkBox5.Visible = false;
            // 
            // checkBox4
            // 
            this.checkBox4.AutoSize = true;
            this.checkBox4.Location = new System.Drawing.Point(178, 59);
            this.checkBox4.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.checkBox4.Name = "checkBox4";
            this.checkBox4.Size = new System.Drawing.Size(71, 16);
            this.checkBox4.TabIndex = 8;
            this.checkBox4.Text = "Channel";
            this.checkBox4.UseVisualStyleBackColor = true;
            this.checkBox4.Visible = false;
            // 
            // button11
            // 
            this.button11.Enabled = false;
            this.button11.Location = new System.Drawing.Point(290, 57);
            this.button11.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button11.Name = "button11";
            this.button11.Size = new System.Drawing.Size(57, 21);
            this.button11.TabIndex = 7;
            this.button11.Text = "프리셋";
            this.button11.UseVisualStyleBackColor = true;
            this.button11.Visible = false;
            // 
            // checkBox3
            // 
            this.checkBox3.AutoSize = true;
            this.checkBox3.Checked = true;
            this.checkBox3.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBox3.Location = new System.Drawing.Point(3, 58);
            this.checkBox3.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.checkBox3.Name = "checkBox3";
            this.checkBox3.Size = new System.Drawing.Size(91, 16);
            this.checkBox3.TabIndex = 5;
            this.checkBox3.Text = "Reverb 사용";
            this.checkBox3.UseVisualStyleBackColor = true;
            this.checkBox3.Visible = false;
            this.checkBox3.CheckedChanged += new System.EventHandler(this.checkBox3_CheckedChanged);
            // 
            // checkBox1
            // 
            this.checkBox1.Location = new System.Drawing.Point(3, 14);
            this.checkBox1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(86, 19);
            this.checkBox1.TabIndex = 2;
            this.checkBox1.Text = "부드럽게";
            this.checkBox1.UseVisualStyleBackColor = true;
            this.checkBox1.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(212, 38);
            this.button1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(57, 21);
            this.button1.TabIndex = 1;
            this.button1.Text = "프리셋";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // comboBox1
            // 
            this.comboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.ItemHeight = 12;
            this.comboBox1.Items.AddRange(new object[] {
            "없음",
            "일반",
            "방음실",
            "방",
            "욕실",
            "거실",
            "석조실",
            "강당",
            "콘서트 홀",
            "동굴",
            "경기장",
            "격납고",
            "카페트가 깔린 복도",
            "복도",
            "석조 회랑",
            "골목길",
            "숲",
            "도시",
            "산",
            "채석장",
            "평원",
            "주차장",
            "하수도",
            "지하수",
            "(사용자 설정)"});
            this.comboBox1.Location = new System.Drawing.Point(3, 37);
            this.comboBox1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.comboBox1.MaxDropDownItems = 19;
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(202, 20);
            this.comboBox1.TabIndex = 0;
            this.comboBox1.SelectedValueChanged += new System.EventHandler(this.comboBox1_SelectedValueChanged);
            // 
            // comboBox5
            // 
            this.comboBox5.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox5.Enabled = false;
            this.comboBox5.FormattingEnabled = true;
            this.comboBox5.Items.AddRange(new object[] {
            "없음",
            "일반",
            "방음실",
            "방",
            "욕실",
            "거실",
            "석조실",
            "강당",
            "콘서트 홀",
            "동굴",
            "경기장",
            "격납고",
            "카페트가 깔린 복도",
            "복도",
            "석조 회랑",
            "골목길",
            "숲",
            "도시",
            "산",
            "채석장",
            "평원",
            "주차장",
            "하수도",
            "지하수",
            "(사용자 설정)"});
            this.comboBox5.Location = new System.Drawing.Point(178, 35);
            this.comboBox5.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.comboBox5.Name = "comboBox5";
            this.comboBox5.Size = new System.Drawing.Size(172, 20);
            this.comboBox5.TabIndex = 6;
            this.comboBox5.Visible = false;
            this.comboBox5.SelectedValueChanged += new System.EventHandler(this.comboBox5_SelectedValueChanged);
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.numPan);
            this.groupBox4.Location = new System.Drawing.Point(257, 141);
            this.groupBox4.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox4.Size = new System.Drawing.Size(83, 48);
            this.groupBox4.TabIndex = 3;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "팬 설정";
            this.groupBox4.Enter += new System.EventHandler(this.groupBox4_Enter);
            // 
            // numPan
            // 
            this.numPan.DecimalPlaces = 7;
            this.numPan.Increment = new decimal(new int[] {
            5,
            0,
            0,
            196608});
            this.numPan.Location = new System.Drawing.Point(3, 16);
            this.numPan.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.numPan.Maximum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numPan.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            -2147483648});
            this.numPan.Name = "numPan";
            this.numPan.Size = new System.Drawing.Size(73, 21);
            this.numPan.TabIndex = 4;
            this.numPan.ValueChanged += new System.EventHandler(this.numPan_ValueChanged);
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.label5);
            this.groupBox5.Controls.Add(this.numSpeed);
            this.groupBox5.Enabled = false;
            this.groupBox5.Location = new System.Drawing.Point(231, 86);
            this.groupBox5.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox5.Size = new System.Drawing.Size(79, 48);
            this.groupBox5.TabIndex = 8;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "속도설정";
            this.groupBox5.Enter += new System.EventHandler(this.groupBox5_Enter);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(66, 24);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(0, 12);
            this.label5.TabIndex = 7;
            // 
            // numSpeed
            // 
            this.numSpeed.DecimalPlaces = 1;
            this.numSpeed.Increment = new decimal(new int[] {
            5,
            0,
            0,
            65536});
            this.numSpeed.Location = new System.Drawing.Point(6, 21);
            this.numSpeed.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.numSpeed.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            -2147483648});
            this.numSpeed.Name = "numSpeed";
            this.numSpeed.Size = new System.Drawing.Size(65, 21);
            this.numSpeed.TabIndex = 6;
            this.numSpeed.ValueChanged += new System.EventHandler(this.numSpeed_ValueChanged);
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.button8);
            this.groupBox6.Controls.Add(this.label6);
            this.groupBox6.Controls.Add(this.numFrenq);
            this.groupBox6.Location = new System.Drawing.Point(136, 140);
            this.groupBox6.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox6.Size = new System.Drawing.Size(112, 62);
            this.groupBox6.TabIndex = 8;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "주파수";
            this.groupBox6.Enter += new System.EventHandler(this.groupBox6_Enter);
            // 
            // button8
            // 
            this.button8.Location = new System.Drawing.Point(6, 37);
            this.button8.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(99, 22);
            this.button8.TabIndex = 8;
            this.button8.Text = "기본값";
            this.button8.UseVisualStyleBackColor = true;
            this.button8.Click += new System.EventHandler(this.button8_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(85, 20);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(20, 12);
            this.label6.TabIndex = 7;
            this.label6.Text = "Hz";
            // 
            // numFrenq
            // 
            this.numFrenq.DecimalPlaces = 1;
            this.numFrenq.Increment = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.numFrenq.Location = new System.Drawing.Point(6, 14);
            this.numFrenq.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.numFrenq.Maximum = new decimal(new int[] {
            360000,
            0,
            0,
            0});
            this.numFrenq.Minimum = new decimal(new int[] {
            360000,
            0,
            0,
            -2147483648});
            this.numFrenq.Name = "numFrenq";
            this.numFrenq.Size = new System.Drawing.Size(74, 21);
            this.numFrenq.TabIndex = 6;
            this.toolTip1.SetToolTip(this.numFrenq, "만약 음악이 바뀔때 업데이트 하고싶지 않으면\r\n[설정(T)]->[프리셋 관리]->[자동 업데이트] 를\r\n해제해 주시기 바랍니다.");
            this.numFrenq.Value = new decimal(new int[] {
            44100,
            0,
            0,
            0});
            this.numFrenq.ValueChanged += new System.EventHandler(this.numericUpDown4_ValueChanged);
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.numGain);
            this.groupBox7.Enabled = false;
            this.groupBox7.Location = new System.Drawing.Point(316, 86);
            this.groupBox7.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox7.Size = new System.Drawing.Size(70, 48);
            this.groupBox7.TabIndex = 9;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Gain";
            this.groupBox7.Visible = false;
            this.groupBox7.Enter += new System.EventHandler(this.groupBox7_Enter);
            // 
            // numGain
            // 
            this.numGain.DecimalPlaces = 2;
            this.numGain.Increment = new decimal(new int[] {
            5,
            0,
            0,
            65536});
            this.numGain.Location = new System.Drawing.Point(6, 21);
            this.numGain.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.numGain.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.numGain.Minimum = new decimal(new int[] {
            1000,
            0,
            0,
            -2147483648});
            this.numGain.Name = "numGain";
            this.numGain.Size = new System.Drawing.Size(59, 21);
            this.numGain.TabIndex = 6;
            this.numGain.ValueChanged += new System.EventHandler(this.numGain_ValueChanged);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(344, 145);
            this.button2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(68, 48);
            this.button2.TabIndex = 10;
            this.button2.Text = "3D리스너 설정";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this.comboBox2);
            this.groupBox8.Location = new System.Drawing.Point(3, 204);
            this.groupBox8.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox8.Size = new System.Drawing.Size(131, 40);
            this.groupBox8.TabIndex = 11;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "체널 설정 (구현X)";
            this.groupBox8.Enter += new System.EventHandler(this.groupBox8_Enter);
            // 
            // comboBox2
            // 
            this.comboBox2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox2.FormattingEnabled = true;
            this.comboBox2.Items.AddRange(new object[] {
            "STEREO",
            "MONO",
            "QUAD",
            "SURROUND",
            "_5POINT1",
            "_7POINT1",
            "SRS5_1_MATRIX",
            "MYEARS",
            "MAX",
            "RAW"});
            this.comboBox2.Location = new System.Drawing.Point(6, 14);
            this.comboBox2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.comboBox2.Name = "comboBox2";
            this.comboBox2.Size = new System.Drawing.Size(119, 20);
            this.comboBox2.TabIndex = 0;
            this.comboBox2.SelectedIndexChanged += new System.EventHandler(this.comboBox2_SelectedIndexChanged);
            // 
            // groupBox9
            // 
            this.groupBox9.Controls.Add(this.comboBox3);
            this.groupBox9.Location = new System.Drawing.Point(140, 204);
            this.groupBox9.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox9.Size = new System.Drawing.Size(184, 40);
            this.groupBox9.TabIndex = 12;
            this.groupBox9.TabStop = false;
            this.groupBox9.Text = "사운드 모드 (전문가용)";
            this.groupBox9.Enter += new System.EventHandler(this.groupBox9_Enter);
            // 
            // comboBox3
            // 
            this.comboBox3.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox3.FormattingEnabled = true;
            this.comboBox3.Location = new System.Drawing.Point(6, 14);
            this.comboBox3.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.comboBox3.Name = "comboBox3";
            this.comboBox3.Size = new System.Drawing.Size(172, 20);
            this.comboBox3.TabIndex = 1;
            this.comboBox3.SelectedIndexChanged += new System.EventHandler(this.comboBox3_SelectedIndexChanged);
            this.comboBox3.SelectedValueChanged += new System.EventHandler(this.comboBox3_SelectedValueChanged);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(3, 248);
            this.button3.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(65, 23);
            this.button3.TabIndex = 13;
            this.button3.Text = "재생";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(75, 248);
            this.button4.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(66, 23);
            this.button4.TabIndex = 14;
            this.button4.Text = "일시정지";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(147, 248);
            this.button5.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(66, 23);
            this.button5.TabIndex = 15;
            this.button5.Text = "정지";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // statusStrip1
            // 
            this.statusStrip1.AutoSize = false;
            this.statusStrip1.BackColor = System.Drawing.Color.Transparent;
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1,
            this.toolStripStatusLabel2,
            this.lblFMODResult});
            this.statusStrip1.Location = new System.Drawing.Point(0, 413);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(449, 16);
            this.statusStrip1.TabIndex = 63;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(67, 11);
            this.toolStripStatusLabel1.Text = "내부 상태:";
            this.toolStripStatusLabel1.ToolTipText = "FMOD 상태 (OK가 아니면 설정실패!)";
            // 
            // toolStripStatusLabel2
            // 
            this.toolStripStatusLabel2.Font = new System.Drawing.Font("굴림", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.toolStripStatusLabel2.ForeColor = System.Drawing.SystemColors.Control;
            this.toolStripStatusLabel2.IsLink = true;
            this.toolStripStatusLabel2.Name = "toolStripStatusLabel2";
            this.toolStripStatusLabel2.Size = new System.Drawing.Size(0, 11);
            // 
            // lblFMODResult
            // 
            this.lblFMODResult.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblFMODResult.ForeColor = System.Drawing.Color.Blue;
            this.lblFMODResult.Name = "lblFMODResult";
            this.lblFMODResult.Size = new System.Drawing.Size(24, 11);
            this.lblFMODResult.Text = "OK";
            // 
            // button6
            // 
            this.button6.Location = new System.Drawing.Point(294, 248);
            this.button6.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(66, 23);
            this.button6.TabIndex = 64;
            this.button6.Text = "스펙트럼";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // button7
            // 
            this.button7.Location = new System.Drawing.Point(330, 216);
            this.button7.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(51, 23);
            this.button7.TabIndex = 65;
            this.button7.Text = "DSP";
            this.button7.UseVisualStyleBackColor = true;
            this.button7.Click += new System.EventHandler(this.button7_Click);
            // 
            // toolTip1
            // 
            this.toolTip1.AutoPopDelay = 5000;
            this.toolTip1.InitialDelay = 500;
            this.toolTip1.ReshowDelay = 50;
            this.toolTip1.ShowAlways = true;
            // 
            // checkBox2
            // 
            this.checkBox2.AutoSize = true;
            this.checkBox2.Location = new System.Drawing.Point(241, 273);
            this.checkBox2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.checkBox2.Name = "checkBox2";
            this.checkBox2.Size = new System.Drawing.Size(100, 16);
            this.checkBox2.TabIndex = 72;
            this.checkBox2.Text = "재생 오류패치";
            this.toolTip1.SetToolTip(this.checkBox2, "만약 음악재생이 끝났는데 뒤에 남은 부분이 있어서 \r\n계속 재생될경우 체크해주세요.\r\n(※주의: 반드시 처음으로 이동을할때는 이 기능을 끄고 이동" +
        "해 주세요)");
            this.checkBox2.UseVisualStyleBackColor = true;
            this.checkBox2.CheckedChanged += new System.EventHandler(this.checkBox2_CheckedChanged);
            // 
            // trackBar1
            // 
            this.trackBar1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.trackBar1.AutoSize = false;
            this.trackBar1.Location = new System.Drawing.Point(3, 315);
            this.trackBar1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.trackBar1.Maximum = 0;
            this.trackBar1.Name = "trackBar1";
            this.trackBar1.Size = new System.Drawing.Size(434, 33);
            this.trackBar1.TabIndex = 67;
            this.toolTip1.SetToolTip(this.trackBar1, "※주의: 처음으로 이동을할때는\r\n           반드시 재생 오류패치를 끄고 이동해 주세요!!");
            this.trackBar1.Scroll += new System.EventHandler(this.trackBar1_Scroll);
            this.trackBar1.ValueChanged += new System.EventHandler(this.trackBar1_ValueChanged);
            // 
            // button12
            // 
            this.button12.Location = new System.Drawing.Point(12, 171);
            this.button12.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button12.Name = "button12";
            this.button12.Size = new System.Drawing.Size(38, 20);
            this.button12.TabIndex = 7;
            this.button12.Text = "500";
            this.button12.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.toolTip1.SetToolTip(this.button12, "믹서를 새로고침 합니다.");
            this.button12.UseVisualStyleBackColor = true;
            this.button12.Click += new System.EventHandler(this.button12_Click);
            // 
            // groupBox11
            // 
            this.groupBox11.Controls.Add(this.numNumBuffers);
            this.groupBox11.Controls.Add(this.label9);
            this.groupBox11.Controls.Add(this.numBufferLength);
            this.groupBox11.Controls.Add(this.label4);
            this.groupBox11.Location = new System.Drawing.Point(3, 7);
            this.groupBox11.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox11.Name = "groupBox11";
            this.groupBox11.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox11.Size = new System.Drawing.Size(178, 69);
            this.groupBox11.TabIndex = 0;
            this.groupBox11.TabStop = false;
            this.groupBox11.Text = "DSP 버퍼";
            this.toolTip1.SetToolTip(this.groupBox11, "이 값은 사양이 낮은 컴퓨터나\r\n사운드 카드가 않 좋을때 설정해 주세요.");
            // 
            // numNumBuffers
            // 
            this.numNumBuffers.Location = new System.Drawing.Point(80, 43);
            this.numNumBuffers.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.numNumBuffers.Name = "numNumBuffers";
            this.numNumBuffers.Size = new System.Drawing.Size(94, 21);
            this.numNumBuffers.TabIndex = 3;
            this.numNumBuffers.Value = new decimal(new int[] {
            4,
            0,
            0,
            0});
            this.numNumBuffers.ValueChanged += new System.EventHandler(this.numNumBuffers_ValueChanged);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(3, 46);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(71, 12);
            this.label9.TabIndex = 2;
            this.label9.Text = "NumBuffers";
            // 
            // numBufferLength
            // 
            this.numBufferLength.Location = new System.Drawing.Point(80, 19);
            this.numBufferLength.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.numBufferLength.Maximum = new decimal(new int[] {
            192000,
            0,
            0,
            0});
            this.numBufferLength.Name = "numBufferLength";
            this.numBufferLength.Size = new System.Drawing.Size(94, 21);
            this.numBufferLength.TabIndex = 1;
            this.numBufferLength.Value = new decimal(new int[] {
            1024,
            0,
            0,
            0});
            this.numBufferLength.ValueChanged += new System.EventHandler(this.numBufferLength_ValueChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(5, 22);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(75, 12);
            this.label4.TabIndex = 0;
            this.label4.Text = "BufferLength";
            // 
            // groupBox10
            // 
            this.groupBox10.Controls.Add(this.button12);
            this.groupBox10.Controls.Add(this.label3);
            this.groupBox10.Controls.Add(this.trackBar2);
            this.groupBox10.Location = new System.Drawing.Point(66, 2);
            this.groupBox10.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox10.Name = "groupBox10";
            this.groupBox10.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox10.Size = new System.Drawing.Size(65, 194);
            this.groupBox10.TabIndex = 66;
            this.groupBox10.TabStop = false;
            this.groupBox10.Text = "믹서";
            this.groupBox10.Enter += new System.EventHandler(this.groupBox10_Enter);
            this.groupBox10.MouseHover += new System.EventHandler(this.groupBox10_MouseHover);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(71, 144);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(23, 12);
            this.label3.TabIndex = 6;
            this.label3.Text = "100";
            // 
            // trackBar2
            // 
            this.trackBar2.AutoSize = false;
            this.trackBar2.ContextMenuStrip = this.contextMenuStrip1;
            this.trackBar2.Location = new System.Drawing.Point(10, 14);
            this.trackBar2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.trackBar2.Maximum = 500;
            this.trackBar2.Name = "trackBar2";
            this.trackBar2.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.trackBar2.Size = new System.Drawing.Size(45, 153);
            this.trackBar2.TabIndex = 2;
            this.trackBar2.TickFrequency = 5;
            this.trackBar2.TickStyle = System.Windows.Forms.TickStyle.Both;
            this.trackBar2.Value = 500;
            this.trackBar2.Scroll += new System.EventHandler(this.trackBar2_Scroll);
            this.trackBar2.ValueChanged += new System.EventHandler(this.trackBar2_ValueChanged);
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fMODSystemToolStripMenuItem,
            this.fMODChannelToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(144, 48);
            // 
            // fMODSystemToolStripMenuItem
            // 
            this.fMODSystemToolStripMenuItem.Name = "fMODSystemToolStripMenuItem";
            this.fMODSystemToolStripMenuItem.Size = new System.Drawing.Size(143, 22);
            this.fMODSystemToolStripMenuItem.Text = "Main Output";
            this.fMODSystemToolStripMenuItem.Click += new System.EventHandler(this.fMODSystemToolStripMenuItem_Click);
            // 
            // fMODChannelToolStripMenuItem
            // 
            this.fMODChannelToolStripMenuItem.Checked = true;
            this.fMODChannelToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.fMODChannelToolStripMenuItem.Name = "fMODChannelToolStripMenuItem";
            this.fMODChannelToolStripMenuItem.Size = new System.Drawing.Size(143, 22);
            this.fMODChannelToolStripMenuItem.Text = "Channel";
            this.fMODChannelToolStripMenuItem.Click += new System.EventHandler(this.fMODChannelToolStripMenuItem_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(2, 276);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(151, 12);
            this.label7.TabIndex = 68;
            this.label7.Text = "00:00:00:000 / 00:00:00:000";
            this.label7.Click += new System.EventHandler(this.label7_Click);
            // 
            // timer1
            // 
            this.timer1.Interval = 25;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // chk반복재생
            // 
            this.chk반복재생.AutoSize = true;
            this.chk반복재생.Location = new System.Drawing.Point(159, 273);
            this.chk반복재생.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.chk반복재생.Name = "chk반복재생";
            this.chk반복재생.Size = new System.Drawing.Size(76, 16);
            this.chk반복재생.TabIndex = 69;
            this.chk반복재생.Text = "반복 재생";
            this.chk반복재생.UseVisualStyleBackColor = true;
            this.chk반복재생.CheckedChanged += new System.EventHandler(this.chk반복재생_CheckedChanged);
            // 
            // comboBox4
            // 
            this.comboBox4.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox4.FormattingEnabled = true;
            this.comboBox4.Location = new System.Drawing.Point(5, 292);
            this.comboBox4.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.comboBox4.Name = "comboBox4";
            this.comboBox4.Size = new System.Drawing.Size(178, 20);
            this.comboBox4.TabIndex = 70;
            this.comboBox4.SelectedIndexChanged += new System.EventHandler(this.comboBox4_SelectedIndexChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(189, 297);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(31, 12);
            this.label8.TabIndex = 71;
            this.label8.Text = "0 / 0";
            this.label8.Click += new System.EventHandler(this.label8_Click);
            // 
            // button9
            // 
            this.button9.Location = new System.Drawing.Point(325, 193);
            this.button9.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(66, 23);
            this.button9.TabIndex = 73;
            this.button9.Text = "EQ 설정";
            this.button9.UseVisualStyleBackColor = true;
            this.button9.Click += new System.EventHandler(this.button9_Click);
            // 
            // button10
            // 
            this.button10.Location = new System.Drawing.Point(366, 249);
            this.button10.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(59, 21);
            this.button10.TabIndex = 74;
            this.button10.Text = "녹음";
            this.button10.UseVisualStyleBackColor = true;
            this.button10.Visible = false;
            this.button10.Click += new System.EventHandler(this.button10_Click);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.파일FToolStripMenuItem,
            this.설정ToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(449, 24);
            this.menuStrip1.TabIndex = 76;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // 파일FToolStripMenuItem
            // 
            this.파일FToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.프로젝트열기OToolStripMenuItem,
            this.프로젝트저장ToolStripMenuItem,
            this.toolStripSeparator1,
            this.닫기XToolStripMenuItem});
            this.파일FToolStripMenuItem.Name = "파일FToolStripMenuItem";
            this.파일FToolStripMenuItem.Size = new System.Drawing.Size(57, 20);
            this.파일FToolStripMenuItem.Text = "파일(&F)";
            // 
            // 프로젝트열기OToolStripMenuItem
            // 
            this.프로젝트열기OToolStripMenuItem.Image = global::HS_Audio.Properties.Resources.folder;
            this.프로젝트열기OToolStripMenuItem.Name = "프로젝트열기OToolStripMenuItem";
            this.프로젝트열기OToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.O)));
            this.프로젝트열기OToolStripMenuItem.Size = new System.Drawing.Size(210, 22);
            this.프로젝트열기OToolStripMenuItem.Text = "프로젝트 열기(&O)";
            this.프로젝트열기OToolStripMenuItem.Click += new System.EventHandler(this.프로젝트열기OToolStripMenuItem_Click);
            // 
            // 프로젝트저장ToolStripMenuItem
            // 
            this.프로젝트저장ToolStripMenuItem.Image = global::HS_Audio.Properties.Resources.disk;
            this.프로젝트저장ToolStripMenuItem.Name = "프로젝트저장ToolStripMenuItem";
            this.프로젝트저장ToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
            this.프로젝트저장ToolStripMenuItem.Size = new System.Drawing.Size(210, 22);
            this.프로젝트저장ToolStripMenuItem.Text = "프로젝트 저장(&S)";
            this.프로젝트저장ToolStripMenuItem.Click += new System.EventHandler(this.프로젝트저장ToolStripMenuItem_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(207, 6);
            // 
            // 닫기XToolStripMenuItem
            // 
            this.닫기XToolStripMenuItem.Image = global::HS_Audio.Properties.Resources.FillLeftHS;
            this.닫기XToolStripMenuItem.Name = "닫기XToolStripMenuItem";
            this.닫기XToolStripMenuItem.Size = new System.Drawing.Size(210, 22);
            this.닫기XToolStripMenuItem.Text = "닫기(&X)";
            this.닫기XToolStripMenuItem.Click += new System.EventHandler(this.닫기XToolStripMenuItem_Click);
            // 
            // 설정ToolStripMenuItem
            // 
            this.설정ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.맨위로설정ToolStripMenuItem,
            this.toolStripSeparator2,
            this.사운드출력방법선택ToolStripMenuItem,
            this.toolStripSeparator3,
            this.프리셋관리ToolStripMenuItem});
            this.설정ToolStripMenuItem.Name = "설정ToolStripMenuItem";
            this.설정ToolStripMenuItem.Size = new System.Drawing.Size(57, 20);
            this.설정ToolStripMenuItem.Text = "설정(&T)";
            this.설정ToolStripMenuItem.DropDownOpening += new System.EventHandler(this.설정ToolStripMenuItem_DropDownOpening);
            // 
            // 맨위로설정ToolStripMenuItem
            // 
            this.맨위로설정ToolStripMenuItem.CheckOnClick = true;
            this.맨위로설정ToolStripMenuItem.Image = global::HS_Audio.Properties.Resources.PushpinHS;
            this.맨위로설정ToolStripMenuItem.Name = "맨위로설정ToolStripMenuItem";
            this.맨위로설정ToolStripMenuItem.Size = new System.Drawing.Size(194, 22);
            this.맨위로설정ToolStripMenuItem.Text = "맨 위로 설정";
            this.맨위로설정ToolStripMenuItem.CheckedChanged += new System.EventHandler(this.맨위로설정ToolStripMenuItem_CheckedChanged);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(191, 6);
            this.toolStripSeparator2.Visible = false;
            // 
            // 사운드출력방법선택ToolStripMenuItem
            // 
            this.사운드출력방법선택ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripComboBox1});
            this.사운드출력방법선택ToolStripMenuItem.Enabled = false;
            this.사운드출력방법선택ToolStripMenuItem.Name = "사운드출력방법선택ToolStripMenuItem";
            this.사운드출력방법선택ToolStripMenuItem.Size = new System.Drawing.Size(194, 22);
            this.사운드출력방법선택ToolStripMenuItem.Text = "사운드 출력 방법 선택";
            this.사운드출력방법선택ToolStripMenuItem.Visible = false;
            // 
            // toolStripComboBox1
            // 
            this.toolStripComboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.toolStripComboBox1.Font = new System.Drawing.Font("굴림", 8.780488F);
            this.toolStripComboBox1.Items.AddRange(new object[] {
            "AUTODETECT",
            "DSOUND",
            "WINMM",
            "WASAPI",
            "ASIO  ",
            "UNKNOWN",
            "NOSOUND",
            "WAVWRITER",
            "NOSOUND_NRT",
            "WAVWRITER_NRT"});
            this.toolStripComboBox1.Name = "toolStripComboBox1";
            this.toolStripComboBox1.Size = new System.Drawing.Size(121, 20);
            this.toolStripComboBox1.ToolTipText = "사운드 출력방법을 설정합니다.\r\n(다음번 재생시 적용됩니다.)";
            this.toolStripComboBox1.SelectedIndexChanged += new System.EventHandler(this.toolStripComboBox1_SelectedIndexChanged);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(191, 6);
            // 
            // 프리셋관리ToolStripMenuItem
            // 
            this.프리셋관리ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.자동업데이트ToolStripMenuItem,
            this.파일에서업데이트ToolStripMenuItem,
            this.toolStripSeparator4,
            this.프리셋업데이트ToolStripMenuItem});
            this.프리셋관리ToolStripMenuItem.Name = "프리셋관리ToolStripMenuItem";
            this.프리셋관리ToolStripMenuItem.Size = new System.Drawing.Size(194, 22);
            this.프리셋관리ToolStripMenuItem.Text = "프로젝트 관리";
            this.프리셋관리ToolStripMenuItem.ToolTipText = "만약 이 항목이 비활성화 되있으면\r\n파일에서 프로젝트 파일을 열어주시기 바랍니다.\r\n그럼 활성화 됩니다.";
            // 
            // 자동업데이트ToolStripMenuItem
            // 
            this.자동업데이트ToolStripMenuItem.Checked = true;
            this.자동업데이트ToolStripMenuItem.CheckOnClick = true;
            this.자동업데이트ToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.자동업데이트ToolStripMenuItem.Name = "자동업데이트ToolStripMenuItem";
            this.자동업데이트ToolStripMenuItem.Size = new System.Drawing.Size(194, 22);
            this.자동업데이트ToolStripMenuItem.Text = "자동 업데이트";
            // 
            // 파일에서업데이트ToolStripMenuItem
            // 
            this.파일에서업데이트ToolStripMenuItem.Checked = true;
            this.파일에서업데이트ToolStripMenuItem.CheckOnClick = true;
            this.파일에서업데이트ToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.파일에서업데이트ToolStripMenuItem.Name = "파일에서업데이트ToolStripMenuItem";
            this.파일에서업데이트ToolStripMenuItem.Size = new System.Drawing.Size(194, 22);
            this.파일에서업데이트ToolStripMenuItem.Text = "파일에서 업데이트";
            this.파일에서업데이트ToolStripMenuItem.ToolTipText = "체크를 해제하게 되면 마지막으로\r\n파일에서 업데이트한 프로젝트가 적용됩니다.";
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(191, 6);
            // 
            // 프리셋업데이트ToolStripMenuItem
            // 
            this.프리셋업데이트ToolStripMenuItem.Name = "프리셋업데이트ToolStripMenuItem";
            this.프리셋업데이트ToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F5;
            this.프리셋업데이트ToolStripMenuItem.Size = new System.Drawing.Size(194, 22);
            this.프리셋업데이트ToolStripMenuItem.Text = "프로젝트 업데이트";
            this.프리셋업데이트ToolStripMenuItem.Click += new System.EventHandler(this.프리셋업데이트ToolStripMenuItem_Click);
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 24);
            this.tabControl1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(449, 389);
            this.tabControl1.TabIndex = 77;
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.SystemColors.Control;
            this.tabPage1.Controls.Add(this.button13);
            this.tabPage1.Controls.Add(this.btnExit);
            this.tabPage1.Controls.Add(this.groupBox2);
            this.tabPage1.Controls.Add(this.button2);
            this.tabPage1.Controls.Add(this.groupBox1);
            this.tabPage1.Controls.Add(this.groupBox3);
            this.tabPage1.Controls.Add(this.button10);
            this.tabPage1.Controls.Add(this.groupBox4);
            this.tabPage1.Controls.Add(this.checkBox2);
            this.tabPage1.Controls.Add(this.button9);
            this.tabPage1.Controls.Add(this.label8);
            this.tabPage1.Controls.Add(this.groupBox5);
            this.tabPage1.Controls.Add(this.comboBox4);
            this.tabPage1.Controls.Add(this.groupBox6);
            this.tabPage1.Controls.Add(this.chk반복재생);
            this.tabPage1.Controls.Add(this.groupBox7);
            this.tabPage1.Controls.Add(this.label7);
            this.tabPage1.Controls.Add(this.trackBar1);
            this.tabPage1.Controls.Add(this.groupBox10);
            this.tabPage1.Controls.Add(this.button7);
            this.tabPage1.Controls.Add(this.groupBox8);
            this.tabPage1.Controls.Add(this.button6);
            this.tabPage1.Controls.Add(this.groupBox9);
            this.tabPage1.Controls.Add(this.button3);
            this.tabPage1.Controls.Add(this.button4);
            this.tabPage1.Controls.Add(this.button5);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tabPage1.Size = new System.Drawing.Size(441, 363);
            this.tabPage1.TabIndex = 1;
            this.tabPage1.Text = "기본 설정";
            // 
            // btnExit
            // 
            this.btnExit.Location = new System.Drawing.Point(348, 273);
            this.btnExit.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(52, 23);
            this.btnExit.TabIndex = 75;
            this.btnExit.Text = "닫기";
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Visible = false;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // tabPage2
            // 
            this.tabPage2.BackColor = System.Drawing.SystemColors.Control;
            this.tabPage2.Controls.Add(this.groupBox11);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tabPage2.Size = new System.Drawing.Size(441, 360);
            this.tabPage2.TabIndex = 0;
            this.tabPage2.Text = "고급 설정";
            // 
            // saveFileDialog1
            // 
            this.saveFileDialog1.DefaultExt = "*.proj";
            this.saveFileDialog1.FileName = "제목 없음";
            this.saveFileDialog1.Filter = "프로젝트 파일 (*.proj)|*.proj";
            this.saveFileDialog1.Title = "다른 이름으로 프로젝트 파일 저장";
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.DefaultExt = "*.proj";
            this.openFileDialog1.FileName = "제목 없음";
            this.openFileDialog1.Filter = "프로젝트 파일 (*.proj)|*.proj";
            this.openFileDialog1.Title = "프로젝트 파일 열기";
            // 
            // button13
            // 
            this.button13.Location = new System.Drawing.Point(386, 218);
            this.button13.Name = "button13";
            this.button13.Size = new System.Drawing.Size(47, 23);
            this.button13.TabIndex = 76;
            this.button13.Text = "Test";
            this.button13.UseVisualStyleBackColor = true;
            this.button13.Click += new System.EventHandler(this.button13_Click);
            // 
            // PlayerSetting
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(449, 429);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.menuStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "PlayerSetting";
            this.Text = "음향 효과 설정";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.PlayerSetting_FormClosing);
            this.Load += new System.EventHandler(this.PlayerSetting_Load);
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.numPitch)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trbVolumeRight)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trbVolumeLeft)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.numPan)).EndInit();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numSpeed)).EndInit();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numFrenq)).EndInit();
            this.groupBox7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.numGain)).EndInit();
            this.groupBox8.ResumeLayout(false);
            this.groupBox9.ResumeLayout(false);
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar1)).EndInit();
            this.groupBox11.ResumeLayout(false);
            this.groupBox11.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numNumBuffers)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numBufferLength)).EndInit();
            this.groupBox10.ResumeLayout(false);
            this.groupBox10.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar2)).EndInit();
            this.contextMenuStrip1.ResumeLayout(false);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblVolumeLeft;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TrackBar trbVolumeRight;
        private System.Windows.Forms.TrackBar trbVolumeLeft;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.NumericUpDown numPan;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.NumericUpDown numSpeed;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.NumericUpDown numGain;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.ComboBox comboBox2;
        private System.Windows.Forms.GroupBox groupBox9;
        private System.Windows.Forms.ComboBox comboBox3;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel2;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.ToolStripStatusLabel lblFMODResult;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.CheckBox chkGain;
        private System.Windows.Forms.GroupBox groupBox10;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TrackBar trackBar2;
        private System.Windows.Forms.TrackBar trackBar1;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.CheckBox chk반복재생;
        private System.Windows.Forms.ComboBox comboBox4;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.CheckBox checkBox2;
        internal System.Windows.Forms.NumericUpDown numFrenq;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.Button button10;
        private System.Windows.Forms.CheckBox checkBox5;
        private System.Windows.Forms.CheckBox checkBox4;
        private System.Windows.Forms.Button button11;
        private System.Windows.Forms.ComboBox comboBox5;
        private System.Windows.Forms.CheckBox checkBox3;
        private System.Windows.Forms.Button button12;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fMODSystemToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem fMODChannelToolStripMenuItem;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem 파일FToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 설정ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 사운드출력방법선택ToolStripMenuItem;
        private System.Windows.Forms.ToolStripComboBox toolStripComboBox1;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.GroupBox groupBox11;
        private System.Windows.Forms.NumericUpDown numBufferLength;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.NumericUpDown numNumBuffers;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ToolStripMenuItem 프로젝트저장ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 프로젝트열기OToolStripMenuItem;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.ToolStripMenuItem 맨위로설정ToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem 닫기XToolStripMenuItem;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        internal System.Windows.Forms.NumericUpDown numPitch;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripMenuItem 프리셋관리ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 파일에서업데이트ToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripMenuItem 프리셋업데이트ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 자동업데이트ToolStripMenuItem;
        private System.Windows.Forms.Button button13;
    }
}
