﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Reflection;

using HS_Audio_Lib;
using HS_Audio;
using HS_Audio.Forms;
using HS_Audio.Recoder.Forms;
using HS_Audio.Setting;

using HS_CSharpUtility;
using HS_CSharpUtility.Extension;
using System.IO;
using NAudio.CoreAudioApi;
using HS_Audio.LIBRARY;

namespace HS_Audio
{
    public partial class PlayerSetting : HS_Audio.Control.HSCustomForm, IHSSetting
    {
        public static bool WindowsXP_Higher{get{return Environment.OSVersion.Version.Major>5;}}
        HS_Audio.HSAudioHelper _Helper;
        public HS_Audio.HSAudioHelper Helper { get { return _Helper; }
            set
            {
                value.PlayingStatusChanged += new HSAudioHelper.PlayingStatusChangedEventHandler(snd_PlayingStatusChanged);
                value.MusicChanged += new HSAudioHelper.MusicChangeEventHandler(Helper_MusicPathChanged);
                value.VolumeChanging+=new HSAudioHelper.VolumeChangedEventHandler(Helper_VolumeChanging);
                _Helper = value;
                try { Helper_MusicPathChanged(null, -1, value.Error); }catch{}
            }
        }

        frmMain frm;
        internal HS_Audio.Forms.frmEqualizer fe;
        frmDSPPlugin frmplug;
        public PlayerSetting(HS_Audio.HSAudioHelper Helper)
        {
            CheckForIllegalCrossThreadCalls = false;
            InitializeComponent(); 
            this.Helper = Helper;
            Helper.Initializing += new HSAudioHelper.InitializingEventHandler(Helper_Initializing);
            Helper.PlayingStatusChanged += new HSAudioHelper.PlayingStatusChangedEventHandler(snd_PlayingStatusChanged);
            comboBox3.Items.AddRange(Enum.GetNames(typeof(MODE)));
            comboBox4.Items.AddRange(Enum.GetNames(typeof(TIMEUNIT))); comboBox4.Text = "MS";
            Common();
            checkBox1.Checked = Helper.HelperEx.LoudnessEqualization;
            try { comboBox1.Text = Helper.HelperEx.ReverbPreset.ToString().Replace("_", " "); }
            catch { comboBox1.Text = "없음"; }
            if (Helper.sound != null){
            float speed = 0; Helper.sound.getMusicSpeed(ref speed); numSpeed.Value = (decimal)speed;
            //numGain.Value = (decimal)snd.HelperEx.FMODChannelInfo.LowPassGain;
            //float Speed = 0; Helper.sound.getMusicSpeed(ref Speed); numSpeed.Value = (decimal)Speed;
            MODE mode = MODE._2D; Helper.sound.getMode(ref mode); comboBox3.Text = mode.ToString();}
            if (Helper.channel != null)
            {
                float frenq = 0; Helper.channel.getFrequency(ref frenq); numFrenq.Value = (decimal)frenq;
                float Pan = 0; Helper.channel.getPan(ref Pan); numPan.Value = (decimal)Pan;
            }
            fhp = Helper.HelperEx.FMODChannelInfo;
            f3 = new frm3D(Helper);
            try{fe = new frmEqualizer(Helper);}catch{}
            frmplug = new frmDSPPlugin(Helper);
            timer1.Start();
        }
        public PlayerSetting(HS_Audio.HSAudioHelper Helper, frmMain frm)
        {
            InitializeComponent(); 
            this.Helper = Helper;
            Helper.Initializing += new HSAudioHelper.InitializingEventHandler(Helper_Initializing);
            Helper.PlayingStatusChanged += new HSAudioHelper.PlayingStatusChangedEventHandler(snd_PlayingStatusChanged);
            this.frm = frm;
            comboBox3.Items.AddRange(Enum.GetNames(typeof(MODE)));
            comboBox4.Items.AddRange(Enum.GetNames(typeof(TIMEUNIT))); comboBox4.Text = "MS";
            Common();
            checkBox1.Checked = Helper.HelperEx.LoudnessEqualization;
            try { comboBox1.Text = Helper.HelperEx.ReverbPreset.ToString().Replace("_", " "); }
            catch { comboBox1.Text = "없음"; }
            float speed = 0; Helper.sound.getMusicSpeed(ref speed); numSpeed.Value = (decimal)speed;
            //numGain.Value = (decimal)snd.HelperEx.FMODChannelInfo.LowPassGain;
            float frenq = 0; Helper.channel.getFrequency(ref frenq); numFrenq.Value = (decimal)frenq;
            float Pan = 0; Helper.channel.getPan(ref Pan); numPan.Value = (decimal)Pan;
            float Speed = 0; Helper.sound.getMusicSpeed(ref Speed); numSpeed.Value = (decimal)Speed;
            MODE mode = MODE._2D; Helper.sound.getMode(ref mode); comboBox3.Text = mode.ToString();

            f3 = new frm3D(Helper);
            fe = new frmEqualizer(Helper);
            frmplug = new frmDSPPlugin(Helper);
            trackBar1.Maximum = (int)Helper.GetTotalTick(tu);
            timer1.Start();
        }
        public PlayerSetting(HS_Audio.HSAudioHelper Helper, string FileName)
        {
            InitializeComponent();
            comboBox3.Items.AddRange(Enum.GetNames(typeof(MODE)));
            comboBox4.Items.AddRange(Enum.GetNames(typeof(TIMEUNIT))); comboBox4.Text = "MS";
            this.Helper = Helper;
            Common();
            Helper.Initializing += new HSAudioHelper.InitializingEventHandler(Helper_Initializing);
            Helper.PlayingStatusChanged += new HSAudioHelper.PlayingStatusChangedEventHandler(snd_PlayingStatusChanged);
            checkBox1.Checked = Helper.HelperEx.LoudnessEqualization;
            comboBox1.Text = Helper.HelperEx.ReverbPreset.ToString().Replace("_", " ");
            float speed = 0; Helper.sound.getMusicSpeed(ref speed); numSpeed.Value = (decimal)speed;
            //numGain.Value = (decimal)snd.HelperEx.FMODChannelInfo.LowPassGain;
            float frenq = 0; Helper.channel.getFrequency(ref frenq); numFrenq.Value = (decimal)frenq;
            float Pan = 0; Helper.channel.getPan(ref Pan); numPan.Value = (decimal)Pan;
            float Speed = 0; Helper.sound.getMusicSpeed(ref Speed); numSpeed.Value = (decimal)Speed;
            MODE mode = MODE._2D; Helper.sound.getMode(ref mode); comboBox3.Text = mode.ToString();
            fhp = Helper.HelperEx.FMODChannelInfo;

            f3 = new frm3D(Helper);
            fe = new frmEqualizer(Helper);
            frmplug = new frmDSPPlugin(Helper);
            this.FileName = FileName;
            trackBar1.Maximum = (int)Helper.TotalPosition;
            timer1.Start();
        }
        public PlayerSetting(HS_Audio.HSAudioHelper Helper, string FileName, frmMain frm)
        {
            InitializeComponent();
            this.frm = frm;
            comboBox3.Items.AddRange(Enum.GetNames(typeof(MODE)));
            comboBox4.Items.AddRange(Enum.GetNames(typeof(TIMEUNIT))); comboBox4.Text = "MS";
            this.Helper = Helper;
            Common();
            Helper.Initializing+=new HSAudioHelper.InitializingEventHandler(Helper_Initializing);
            Helper.PlayingStatusChanged += new HSAudioHelper.PlayingStatusChangedEventHandler(snd_PlayingStatusChanged);
            checkBox1.Checked = Helper.HelperEx.LoudnessEqualization;
            comboBox1.Text = Helper.HelperEx.ReverbPreset.ToString().Replace("_", " ");
            float speed = 0; Helper.sound.getMusicSpeed(ref speed); numSpeed.Value = (decimal)speed;
            //numGain.Value = (decimal)snd.HelperEx.FMODChannelInfo.LowPassGain;
            float frenq = 0; Helper.channel.getFrequency(ref frenq); numFrenq.Value = (decimal)frenq;
            float Pan = 0; Helper.channel.getPan(ref Pan); numPan.Value = (decimal)Pan;
            float Speed = 0; Helper.sound.getMusicSpeed(ref Speed); numSpeed.Value = (decimal)Speed;
            MODE mode = MODE._2D; Helper.sound.getMode(ref mode); comboBox3.Text = mode.ToString();
            fhp = Helper.HelperEx.FMODChannelInfo;

            f3 = new frm3D(Helper);
            fe = new frmEqualizer(Helper);
            frmplug = new frmDSPPlugin(Helper);
            this.FileName = FileName;
            trackBar1.Maximum = (int)Helper.TotalPosition;
            timer1.Start();
        }

        private void Common()
        {
            toolStripComboBox1.Text = toolStripComboBox1.Items[0].ToString(); 
            reverb.CustomReverbValueChanged += new HS_Audio.Forms.frmReverb.CustomReverbValueChangedEventHandler(CustomReverb_CustomReverbValueChanged);
            reverb.CustomReverbGainValueChanged += new HS_Audio.Forms.frmReverb.CustomReverbGainValueChangedEventHandler(CustomReverb_CustomReverbGainValueChanged);
            _frmProjectUpdateSetting = new frmProjectUpdateSetting();
            _frmProjectUpdateSetting.ProjectUpdated+=_frmProjectUpdateSetting_ProjectUpdated;
            projSetting=_frmProjectUpdateSetting.CurrentSetting;
            try{trbVolumeLeft.Value = (int)(Helper.Volume * 100);}catch{}

            chkAutoDeviceDetect.Checked = true;
        }
        string FileName;
        public string Title { get { return FileName; } 
            set { FileName=value; this.Text = "재생 설정" + (value==""||value==null?"":" - "+value); } }


        MMDeviceEnumerator mm;
        MMDeviceCollection mmdc;
        MMDevice mmd_Default;

        LIBRARY.HSVolumeControl_WinXP WindowsXPVolumeControl;
        WaveLib.AudioMixer.MixerLine WindowsXPLine;
        WaveLib.AudioMixer.Mixer mi;
        
        private void PlayerSetting_Load(object sender, EventArgs e)
        {
#if DEBUG
            btn구간반복.Enabled = true;
#endif
            try
            {
                if (!WindowsXP_Higher)
                {
                    //uint a= SynchronizedVolumeControl.MM.mixerGetNumDevs();
                    //mi = new WaveLib.AudioMixer.Mixer(WaveLib.AudioMixer.MixerType.Playback);
                    //WindowsXPLine = mi.Lines[0];
                    //mi.MixerLineChanged += WindowsXPLine_MixerLineChanged;
                    //WindowsXPVolumeControl = new LIBRARY.HSVolumeControl_WinXP(this.Handle);
                    //WindowsXPVolumeControl.VolumeChanged+=WindowsXPVolumeControl_VolumeChanged;
                    //WindowsXPVolumeControl.MuteChanged+=WindowsXPVolumeControl_MuteChanged;
                    //checkBox2.Enabled = trackBar3.Enabled = true;
                    progressBar1.Visible = progressBar2.Visible = label11.Visible = label14.Visible = checkBox7.Visible = false;
                    groupBox12.Size = new Size(groupBox12.Size.Width, 125);

                }
                else mm = new MMDeviceEnumerator();
            }
            catch{}
            //comboBox1.Text = "없음";
            //comboBox2.Text = "STEREO";
            //comboBox1.SelectedValue = -1;
            uint numBufferLength_Value = 0; int numNumBuffers_Value = 0;
            Helper.system.getDSPBufferSize(ref numBufferLength_Value, ref numNumBuffers_Value);
            numBufferLength.Value = numBufferLength_Value; numNumBuffers.Value = numNumBuffers_Value;
            btnSoundDeviceRefresh_Click(null, null);

            //if (DectectDefaultSoundDevice_Thread == null) System.Threading.ThreadPool.QueueUserWorkItem(new System.Threading.WaitCallback(DectectDefaultSoundDevice_ThreadLoop));
            WASAPIPeak_Thread.Tick+=WASAPIPeak_ThreadLoop;
#if DEBUG
#else
            System.Threading.ThreadPool.QueueUserWorkItem(new System.Threading.WaitCallback(감시));
#endif
            //Device[] di = Helper.getSoundDevice();
            //cbOutputDevice.Items.AddRange(di);
            //cbOutputDevice.Text = cbOutputDevice.Items[0].ToString();
            

            cbOutputType.Items.AddRange(Enum.GetNames(typeof(HS_Audio_Lib.OUTPUTTYPE)));
        }

        System.Threading.Thread 감시_Thread;
        void 감시(object o)
        {
            감시_Thread = System.Threading.Thread.CurrentThread;
            while(true){
            //if(comboBox1.SelectedIndex==0)
            //    if(DectectDefaultSoundDevice_Thread!=null&&(DectectDefaultSoundDevice_Thread.ThreadState != System.Threading.ThreadState.Running && DectectDefaultSoundDevice_Thread.ThreadState != System.Threading.ThreadState.Background))
            try{if (DectectDefaultSoundDevice_Thread!=null)DectectDefaultSoundDevice_Thread.Abort();
            System.Threading.ThreadPool.QueueUserWorkItem(new System.Threading.WaitCallback(DectectDefaultSoundDevice_ThreadLoop));//MessageBox.Show("");
            }
            finally{System.Threading.Thread.Sleep(5000);} }
        }

        HS_Audio.Forms.frmReverb reverb = new HS_Audio.Forms.frmReverb() {IsClose = false };
        private void comboBox1_SelectedValueChanged(object sender, EventArgs e)
        {
            if (comboBox1.Text == "없음") { /*groupBox10.Enabled = */button1.Enabled = 반향효과Reverb설정ToolStripMenuItem.Enabled = false; } else { groupBox10.Enabled = 반향효과Reverb설정ToolStripMenuItem.Enabled = button1.Enabled = true; }
            if (comboBox1.Text != "(사용자 설정)")
                Helper.HelperEx.ReverbPreset = (HS_Audio.HSAudioReverbPreset)Enum.Parse(typeof(HS_Audio.HSAudioReverbPreset), comboBox1.Text.Replace(" ", "_"));
            else
            {
               // Helper.HelperEx.ReverbProperties = FMODHelperEx.REVERB_PROPERTIES_GENERIC;
                reverb.UpdatePreset();
                reverb.Show(); reverb.BringToFront();
            }
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox1.Checked) { Helper.HelperEx.LoudnessEqualization = true; }
            else { Helper.HelperEx.LoudnessEqualization = false; }
        }

        HS_Audio.Properties.HSAudioChannelStruct fhp = new HS_Audio.Properties.HSAudioChannelStruct();
        internal void numPitch_ValueChanged(object sender, EventArgs e)
        {
            if (!PitchLock)
            {
                FreqLock = true;
                numFrenq.Value = (decimal)Helper.DefaultFrequency+numPitch.Value;
                //numFrenq.Value = (decimal)Helper.Frequency;
                //Helper.Pitch = (float)numPitch.Value;
                //numFrenq.Value = (decimal)Helper.DefaultFrequency + numPitch.Value;
                FreqLock = false;
            }
        }

        bool PitchLock = false;
        bool FreqLock = false;
        internal void numericUpDown4_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                //snd.channel.setFrequency((float)numFrenq.Value);
                //numPitch.Value = numFrenq.Value - (decimal)Helper.DefaultFrequency;
                Helper.Frequency = (float)numFrenq.Value;
                if(!FreqLock)numPitch.Value = numFrenq.Value-(decimal)Helper.DefaultFrequency;
                //Helper.system.update();
                LoadSetting(Setting);
            }
            catch{}
            //fhp.Frequency = (float)numFrenq.Value;
            //Helper.HelperEx.FMODChannelInfo = fhp;
        }

        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                SPEAKERMODE sm = (SPEAKERMODE)Enum.Parse(typeof(SPEAKERMODE), comboBox2.Text);
                Helper.system.getSpeakerMode(ref sm);
                Helper.system.setSpeakerMode((SPEAKERMODE)Enum.Parse(typeof(SPEAKERMODE), comboBox2.Text));
                Helper.system.update();
            }catch{}
        }

        private void comboBox3_SelectedValueChanged(object sender, EventArgs e)
        {
            lblFMODResult.Text = "Wait";
            MODE md = MODE._2D; Helper.sound.getMode(ref md);
            lblFMODResult.Text = Helper.channel.setMode((MODE)Enum.Parse(typeof(MODE), comboBox3.Text)).ToString();
            if (comboBox3.SelectedIndex == 3 || comboBox3.SelectedIndex == 2) { lblFMODResult.Text = Helper.sound.setLoopPoints(1, HS_Audio_Lib.TIMEUNIT.PCMBYTES, Helper.GetTotalTick(HS_Audio_Lib.TIMEUNIT.PCMBYTES) - 1, HS_Audio_Lib.TIMEUNIT.PCMBYTES).ToString(); }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            frmReverb reverb = new frmReverb(Helper.HelperEx.ReverbProperties, comboBox1.Text, Gain);

            reverb.CustomReverbValueChanged += new HS_Audio.Forms.frmReverb.CustomReverbValueChangedEventHandler(CustomReverb_CustomReverbValueChanged);
            reverb.CustomReverbGainValueChanged += new HS_Audio.Forms.frmReverb.CustomReverbGainValueChangedEventHandler(CustomReverb_CustomReverbGainValueChanged);
            reverb.Show(); reverb.BringToFront();
            //try { reverb.Show(); }
            //catch(ObjectDisposedException) { reverb = new frmSetCustomReverb(snd.HelperEx.ReverbProperties, Gain); reverb.Show(); }
        }
        Dictionary<string, string> ReverbPreset;
        private void CustomReverb_CustomReverbValueChanged(HS_Audio_Lib.REVERB_PROPERTIES value, Dictionary<string, string> Preset)
        {
            ReverbPreset = Preset;
            Helper.HelperEx.ReverbProperties = value;
        }

        float Gain = 1;
        private void CustomReverb_CustomReverbGainValueChanged(float Gain)
        {
            if (Gain == 0) { Helper.Volume = 0; }
            else { string a = Gain.ToString(); Helper.Volume = (a == "NaN" ? 0 : Gain + (float)(trbVolumeLeft.Value * 0.001)); this.Gain = Gain; }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            Helper.Pause();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Helper.Play();
            timer1.Start();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            Helper.Stop();
            button12_Click(null, null);
        }

        private void numGain_ValueChanged(object sender, EventArgs e)
        {
            Helper.channel.setLowPassGain((float)numGain.Value);
            //snd.HelperEx.FMODChannelInfo = fhp;
        }

        private void trackBar2_ValueChanged(object sender, EventArgs e)
        {
            //trackBar2.Maximum = 500;
            Helper.Mix = trackBar2.Value * 0.002f;
            button12.Text = trackBar2.Value.ToString();
        }

        /*
        private void checkBox2_CheckedChanged(object sender, EventArgs e)
        {
            if (chkGain.Checked == true) { trbVolumeLeft.Maximum = 150; }
            else { trbVolumeLeft.Maximum = 100; }
        }*/

        private void chkGain_MouseHover(object sender, EventArgs e)
        {
            toolTip1.Show("음량을 반향 효과(Reverb)의 Gain값과 연결합니다.", chkGain, 4000);
        }

        private void groupBox10_MouseHover(object sender, EventArgs e)
        {
            toolTip1.Show("반향 효과(Reverb) 에서의 믹서 입니다.\n" +
                "(반향 효과(Reverb)를 선택하면 활성화 됩니다.) ", groupBox10, 5000);
        }

       

        private void trackBar1_ValueChanged(object sender, EventArgs e)
        {
            if (PositionChange) {  Helper.CurrentPosition = (uint)trackBar1.Value * 10; PositionChange = false; }
        }
        static StringBuilder ShowTime_sb = new StringBuilder(7,100);
        static string my_string;
        static byte ErrorCount;
        public string ShowTime(TimeSpan Time, bool MilliSecond = true)
        {
            try
            {
                my_string = null;
                ShowTime_sb.Remove(0, ShowTime_sb.Length);
                /*sb.Append(Time.Hours);sb.Append(":");
                sb.Append(Time.Minutes);sb.Append(":");
                sb.Append(Time.Seconds);sb.Append(".");
                sb.Append(Time.Milliseconds);*/
                ShowTime_sb.Append(Time.Hours.ToString("00")); ShowTime_sb.Append(":");
                ShowTime_sb.Append(Time.Minutes.ToString("00")); ShowTime_sb.Append(":");
                ShowTime_sb.Append(Time.Seconds.ToString("00"));
                if (MilliSecond) { ShowTime_sb.Append("."); ShowTime_sb.Append(Time.Milliseconds.ToString("000")); }
                //my_string = ShowTime_sb.ToString();
                ErrorCount = 0;
                my_string = (string)ShowTime_sb.GetType().GetField("m_StringValue", BindingFlags.NonPublic | BindingFlags.Instance).GetValue(ShowTime_sb);
                return my_string;
            }
            catch (Exception ex) { if (ErrorCount > 5)ex.Logging("HS_Audio::PlayerSetting::ShowTime(TimeSpan Time, bool MilliSecond=true) 에서 예외 발생!!"); GC.Collect(); ErrorCount++; return ""; }
        }

        long Hour_ShowTime = 0;
        int Second_ShowTime = 0;
        int Minute_ShowTime = 0;
        int MilliSecond_ShowTime = 0;
        public string ShowTime(long Tick, bool MilliSecond = true)
        {
            try
            {
                //my_string = null;
                ShowTime_sb.Remove(0, ShowTime_sb.Length);
                if (MilliSecond)
                {
                    /*
                    MilliSecond_ShowTime = (int)(Time % 1000);
                    Second_ShowTime = (int)(Time / 1000) % 60;
                    Minute_ShowTime = (int)((Time / (1000 * 60)) % 60);
                    Hour_ShowTime = (int)(Time / (1000 * 3600));//% 24);*/
                    MilliSecond_ShowTime = (int)(Tick % 1000);
                    Tick = Tick / 1000;
                    Hour_ShowTime = Tick / 3600;
                    Tick = Tick % 3600;
                    Minute_ShowTime = (int)(Tick / 60);
                    Second_ShowTime = (int)(Tick % 60);
                }
                else
                {
                    Tick = Tick / 1000;
                    Hour_ShowTime = Tick / 3600;
                    Tick = Tick % 3600;
                    Minute_ShowTime = (int)(Tick / 60);
                    Second_ShowTime = (int)(Tick % 60);
                    //Minute_ShowTime = (int)((Time / 60) % 60);
                    //Hour_ShowTime = (int)((Time / (1000 * 60 * 60)));
                }
                

                /*sb.Append(Time.Hours);sb.Append(":");
                sb.Append(Time.Minutes);sb.Append(":");
                sb.Append(Time.Seconds);sb.Append(".");
                sb.Append(Time.Milliseconds);*/
                //my_string = ShowTime_sb.ToString();

                ShowTime_sb.Append(Hour_ShowTime.ToString("00")); ShowTime_sb.Append(":");
                ShowTime_sb.Append(Minute_ShowTime.ToString("00")); ShowTime_sb.Append(":");
                ShowTime_sb.Append(Second_ShowTime.ToString("00"));
                if (MilliSecond) { ShowTime_sb.Append("."); ShowTime_sb.Append(MilliSecond_ShowTime.ToString("000")); }
                ErrorCount = 0;
                my_string = ShowTime_sb.ToString();//(string)ShowTime_sb.GetType().GetField("m_StringValue", BindingFlags.NonPublic | BindingFlags.Instance).GetValue(ShowTime_sb);
                return my_string;
            }
            catch (Exception ex) { if (ErrorCount > 5)ex.Logging("HS_Audio::PlayerSetting::ShowTime(TimeSpan Time, bool MilliSecond=true) 에서 예외 발생!!");ShowTime_sb = new StringBuilder(7,100);  GC.Collect(); ErrorCount++; return ""; }
        }
        
        public string ShowTime(TimeSpan Time)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(Time.Hours.ToString("00")); sb.Append(":");
            sb.Append(Time.Minutes.ToString("00")); sb.Append(":");
            sb.Append(Time.Seconds.ToString("00")); sb.Append(".");
            sb.Append(Time.Milliseconds.ToString("000"));
            return sb.ToString();
        }

        uint Currentpos;
        uint Totalpos;

        StringBuilder timer1_Tick_sb = new StringBuilder();
        private void timer1_Tick(object sender, EventArgs e)
        {
            try { Currentpos = Helper.GetCurrentTick(tu); Totalpos = Helper.GetTotalTick(tu); }
            catch { }
            try
            {
                StringBuilder sb = timer1_Tick_sb;
                sb.Append(Currentpos.ToString()); sb.Append(" / "); sb.Append(Totalpos.ToString());//##,###
                
                label8.Text = sb.ToString();
                sb.Remove(0, sb.Length);
                sb.Append("시간: ");
                sb.Append(ShowTime(Currentpos,true)); sb.Append(" / ");
                sb.Append(ShowTime(Totalpos , true));
                label7.Text = sb.ToString(); sb.Remove(0, sb.Length);
            }
            catch { }
            try
            {
                trackBar1.Maximum = (int)Helper.GetTotalTick(TIMEUNIT.MS)/10;
                trackBar1.Value = (int)Helper.GetCurrentTick(TIMEUNIT.MS)/10;
                //string text = snd.GetTotalTick(tu).ToString();
            }
            catch { }
            //if()
        }

        bool PositionChange;
        private void trackBar1_Scroll(object sender, EventArgs e)
        {
            //snd.channel.setPosition((uint)trackBar1.Value,tu);
            PositionChange = true;
            //Helper.CurrentPosition = (uint)trackBar1.Value*10;
        }

        private void comboBox3_SelectedIndexChanged(object sender, EventArgs e)
        {
            try { Helper.SetMode((HS_Audio_Lib.MODE)Enum.Parse(typeof(HS_Audio_Lib.MODE), comboBox3.Items[comboBox3.SelectedIndex].ToString())); }catch { }
        }

        private void chk반복재생_CheckedChanged(object sender, EventArgs e)
        {
            Helper.Loop = chk반복재생.Checked;
        }

        HS_Audio_Lib.TIMEUNIT tu = TIMEUNIT.MS;
        private void comboBox4_SelectedIndexChanged(object sender, EventArgs e)
        {
            tu = (TIMEUNIT)Enum.Parse(typeof(TIMEUNIT), comboBox4.SelectedItem.ToString());
            //timer1_Tick(null, null);
        }
        private void snd_PlayingStatusChanged(HSAudioHelper.PlayingStatus status, int index)
        {/* trackBar1.Maximum = (int)snd.GetTotalTick(TIMEUNIT.MS);*/
            if (status == HSAudioHelper.PlayingStatus.Play) { Helper.Mix = trackBar2.Value * 0.002f; }
        }

        private void button8_Click(object sender, EventArgs e)
        {
            Helper.Frequency = Helper.DefaultFrequency;
            numFrenq.Value = (decimal)Helper.Frequency;
        }

        private void checkBox2_CheckedChanged(object sender, EventArgs e)
        {
            Helper.TickErrorPatch = chk재생오류패치.Checked;
            //if (checkBox2.Checked) { snd.channel.setMode(MODE.LOOP_NORMAL); }
            //else { snd.channel.setMode(MODE.LOOP_OFF); }
        }

        private void PlayerSetting_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = true;
            CloseForm = false;
            this.Hide();
            timer1.Stop();
        }

        HS_Audio.Forms.frmReverb reverb1 = new HS_Audio.Forms.frmReverb();
        private void comboBox5_SelectedValueChanged(object sender, EventArgs e)
        {
            if (comboBox1.Text == "없음") { groupBox10.Enabled = button1.Enabled = false; } else { groupBox10.Enabled = button1.Enabled = true; }
            if (comboBox1.Text != "(사용자 설정)")
                Helper.HelperEx.ReverbPreset = (HS_Audio.HSAudioReverbPreset)Enum.Parse(typeof(HS_Audio.HSAudioReverbPreset), comboBox5.Text.Replace(" ", "_"));
            else
            {
                Helper.HelperEx.ReverbProperties = HSAudioHelperEx.REVERB_PROPERTIES_GENERIC;

                reverb1.CustomReverbValueChanged += new HS_Audio.Forms.frmReverb.CustomReverbValueChangedEventHandler(CustomReverb_CustomReverbValueChanged);
                reverb1.CustomReverbGainValueChanged += new HS_Audio.Forms.frmReverb.CustomReverbGainValueChangedEventHandler(CustomReverb_CustomReverbGainValueChanged);
                reverb1.Show();
            }
        }

        private void checkBox3_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox3.Checked)
            {
                comboBox1.Enabled = button1.Enabled = checkBox1.Enabled = true;
                if (Helper.system != null)
                {

                }
            }
            else
            {
                if (Helper.system != null)
                {
                    REVERB_PROPERTIES rv = HSAudioHelperEx.REVERB_PROPERTIES_GENERIC;
                    Helper.system.setReverbProperties(ref rv);
                }
                comboBox1.Enabled = button1.Enabled = checkBox1.Enabled = false;
            }
        }

        private void button12_Click(object sender, EventArgs e)
        {
            Helper.UpdateMix();
        }

        private void fMODChannelToolStripMenuItem_Click(object sender, EventArgs e)
        {
            fMODChannelToolStripMenuItem.Checked = true;
            fMODSystemToolStripMenuItem.Checked = false;
            Helper.dsp.IsSystem = false;
            Helper.UpdateMix();
        }

        private void fMODSystemToolStripMenuItem_Click(object sender, EventArgs e)
        {
            fMODChannelToolStripMenuItem.Checked = false;
            fMODSystemToolStripMenuItem.Checked = true;
            Helper.dsp.IsSystem = true;
            Helper.UpdateMix();
        }

        internal void Helper_Initializing(HS_Audio_Lib.System system)
        {
            /*
            //toolStripComboBox1.SelectedItem = toolStripComboBox1.Items[0];
            //FMOD.RESULT r= system.init(2, INITFLAGS.NORMAL, IntPtr.Zero);
            lblFMODResult.Text = Helper.system.setOutput((OUTPUTTYPE)Enum.Parse(typeof(OUTPUTTYPE), toolStripComboBox1.SelectedItem.ToString())).ToString();
            lblFMODResult.Text = 
            system.setDSPBufferSize((uint)numBufferLength.Value, (int)numNumBuffers.Value).ToString();            
            lblFMODResult.Text = 
            system.setDSPBufferSize((uint)numBufferLength.Value, (int)numNumBuffers.Value).ToString();
            */
        }

        string ProjectDir
        {
            get
            {
                if(!System.IO.Directory.Exists(Application.StartupPath+"\\Project"))
                    System.IO.Directory.CreateDirectory(Application.StartupPath+"\\Project");
                return Application.StartupPath + "\\Project";
            }
        }

        public void LoadSetting(Dictionary<string, string> Setting) { LastSetting1 = Setting; }
        public Dictionary<string, string> SaveSetting() { return Setting; }
        public Dictionary<string, string> LastSetting, LastSetting1;
        internal Dictionary<string, string> Setting
        {
            get
            {
                Dictionary<string, string> a = new Dictionary<string, string>();
                try{a.Add("EQPreset", HS_CSharpUtility.Utility.StringUtility.ConvertArrayToString(
                                  HS_CSharpUtility.Utility.EtcUtility.SaveSetting(fe.EQPreset), "##"));}catch{}
                try{a.Add("EQPresetUse", fe.EQUse.Checked.ToString()); }catch{}
                try{a.Add("DSPPreset",HS_CSharpUtility.Utility.StringUtility.ConvertArrayToString(
                                  HS_CSharpUtility.Utility.EtcUtility.SaveSetting(frmdsp.DSPPreset), "##"));}catch{}
                try{a.Add("DSPPresetUse", frmdsp.DSPUse.ToString()); }catch{}
                try{a.Add("ReverbPreset",HS_CSharpUtility.Utility.StringUtility.ConvertArrayToString(
                                  HS_CSharpUtility.Utility.EtcUtility.SaveSetting(ReverbPreset), "##"));}catch{}
                try{a.Add("ReverbName",comboBox1.Text);}catch{}
                try{a.Add("Volume",trbVolumeLeft.Value.ToString());}catch{}
                try{a.Add("MixerIsSystem",fMODSystemToolStripMenuItem.Checked.ToString());}catch{}
                try{a.Add("Mixer",trackBar2.Value.ToString());}catch{}
                try{a.Add("Pan",numPan.Value.ToString());}catch{}
                try{a.Add("Pitch", numPitch.Value.ToString());}catch{}
                //try{a.Add("Frequency", numFrenq.Value.ToString());} catch{}
                try{a.Add("ErrorPatch",chk재생오류패치.Checked.ToString());}catch{}
                try{a.Add("IsLoop",chk반복재생.Checked.ToString());}catch{}
                return a;
            }
            set
            {
                Dictionary<string, string> a = LastSetting = value;
                if(projSetting.DSPAutoUpdate){
                if (frmdsp == null) frmdsp = new frmDSP(this.Helper); 
                //if (frmdsp != null) frmdsp.AutoUpdate = false;
                try{frmdsp.DSPUse=bool.Parse(a["DSPPresetUse"]);}catch { }
                try{frmdsp.DSPPreset = HS_CSharpUtility.Utility.EtcUtility.LoadSetting(
                                    HS_CSharpUtility.Utility.StringUtility.ConvertStringToArray(a["DSPPreset"], "##"));}catch { }}
                
                if(projSetting.EQUpdate){
                try{fe.EQUse.Checked=bool.Parse(a["EQPresetUse"]);}catch { }
                try{fe.EQPreset=HS_CSharpUtility.Utility.EtcUtility.LoadSetting(
                                    HS_CSharpUtility.Utility.StringUtility.ConvertStringToArray(a["EQPreset"], "##"));}catch{}
                try{comboBox1.Text=a["ReverbName"];}catch{}}

                if (projSetting.ReverbUpdate){
                try{ if(reverb1==null)reverb1 = new frmReverb();
                     reverb1.ReverbPreset = HS_CSharpUtility.Utility.EtcUtility.LoadSetting(
                                    HS_CSharpUtility.Utility.StringUtility.ConvertStringToArray(a["ReverbPreset"], "##"));}catch{}}

                if (projSetting.MixerUpdate){
                try{fMODSystemToolStripMenuItem.Checked=bool.Parse(a["MixerIsSystem"]);
                     fMODChannelToolStripMenuItem.Checked=!fMODSystemToolStripMenuItem.Checked;}catch{}
                try{trackBar2.Value=int.Parse(a["Mixer"]);}catch{}}

                if(projSetting.VolumeUpdate)try{trbVolumeLeft.Value=int.Parse(a["Volume"]);}catch{}
                if(projSetting.FrequencyUpdate)try{numFrenq.Value = decimal.Parse(a["Frequency"]);}catch { }
                if(projSetting.PitchUpdate)try{numPitch.Value = Calculator(a["Pitch"],(decimal)Helper.DefaultFrequency);}catch { }
                if(projSetting.PanUpdate)try{numPan.Value=decimal.Parse(a["Pan"]);}catch{}

                if (projSetting.EtcSettingUpdate){
                try{chk재생오류패치.Checked=bool.Parse(a["ErrorPatch"]);}catch{}
                try{chk반복재생.Checked=bool.Parse(a["IsLoop"]);}catch{}}
            }
        }

        private void 프로젝트저장ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            saveFileDialog1.InitialDirectory = ProjectDir;
            string asd = HS_CSharpUtility.Utility.StringUtility.GetFileName(FileName, false);
            saveFileDialog1.FileName = asd==null||asd==""?"제목 없음":asd;
            if(saveFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                System.IO.File.WriteAllLines(saveFileDialog1.FileName, HS_CSharpUtility.Utility.EtcUtility.SaveSetting(Setting));
            }
        }

        internal string[] LastPreset; internal string LastPresetPath;
        private void 프로젝트열기OToolStripMenuItem_Click(object sender, EventArgs e){
            openFileDialog1.InitialDirectory = ProjectDir;
            string asd =HS_CSharpUtility.Utility.StringUtility.GetFileName(FileName, false);
            openFileDialog1.FileName = asd == null || asd == "" ? "제목 없음" : asd;
            if (openFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                LastPresetPath = openFileDialog1.FileName;
                string[] b = LastPreset = System.IO.File.ReadAllLines(openFileDialog1.FileName);
                try { Setting = HS_CSharpUtility.Utility.EtcUtility.LoadSetting(b); 프리셋관리ToolStripMenuItem.Enabled = true; 
                프리셋업데이트ToolStripMenuItem_Click(null,null);}
                catch { MessageBox.Show("프로젝트 파일이 올바르지 못합니다!!", "프로젝트 열기 실패"); }
            }
        }

        internal decimal Calculator(string Number, decimal Value)
        {
            if (Number.Contains("x/")&&Value!=0)
            {return Value/decimal.Parse(Number.Split('/')[1]);}
            else if(Number.Contains("x*"))
            { return Value * decimal.Parse(Number.Split('*')[1]); }
            else return decimal.Parse(Number);
        }

        private void 맨위로설정ToolStripMenuItem_CheckedChanged(object sender, EventArgs e)
        {
            this.TopMost = 맨위로설정ToolStripMenuItem.Checked;
        }

        private void btnExit_Click(object sender, EventArgs e) { this.Hide(); }
        private void 닫기XToolStripMenuItem_Click(object sender, EventArgs e) { this.Hide(); }

        void Helper_MusicPathChanged(string Path, int index, bool Error)
        {
            if (!Error){
            numPitch.Maximum = numFrenq.Maximum - (decimal)Helper.DefaultFrequency;
            numPitch.Minimum = numFrenq.Minimum - (decimal)Helper.DefaultFrequency;

            if (자동업데이트ToolStripMenuItem.Checked &&
                프리셋업데이트ToolStripMenuItem.Enabled)
            {
                if (파일에서업데이트ToolStripMenuItem.Checked && LastPreset != null && LastPreset.Length>0)
                {
                    try
                    {
                        string[] b = LastPreset = System.IO.File.ReadAllLines(LastPresetPath);
                        Setting = LastSetting = HS_CSharpUtility.Utility.EtcUtility.LoadSetting(b);
                    }
                    catch { MessageBox.Show("프로젝트를 파일에서 업데이트하지 못했습니다!!", "프로젝트 업데이트 오류"); }
                }
                else
                {
                    try { Setting = HS_CSharpUtility.Utility.EtcUtility.LoadSetting(LastPreset); }
                    catch { Setting = Setting; }
                }
            }
            else Setting = Setting;}
        }

        private void 프리셋업데이트ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (LastPresetPath != null && LastPreset.Length == 0) { MessageBox.Show("프로젝트를 먼저 선택하여 주시기 바랍니다.", "프로젝트 업데이트 오류"); }
            else
            {
                if (파일에서업데이트ToolStripMenuItem.Checked && LastPresetPath != null && LastPreset.Length > 0)
                {
                    try
                    {
                        string[] b = LastPreset = System.IO.File.ReadAllLines(LastPresetPath);
                        Setting = LastSetting = HS_CSharpUtility.Utility.EtcUtility.LoadSetting(b);
                    }
                    catch (Exception ex) { MessageBox.Show("프로젝트를 파일에서 업데이트하지 못했습니다!!\n\n이유: " + ex.Message, "프로젝트 업데이트 오류"); }
                }
                else { Setting = HS_CSharpUtility.Utility.EtcUtility.LoadSetting(LastPreset); }
            }
        }

        private void 설정ToolStripMenuItem_DropDownOpening(object sender, EventArgs e)
        {
            //프리셋관리ToolStripMenuItem.Enabled = LastPresetPath != null;
            프리셋관리ToolStripMenuItem.Enabled = LastPresetPath != null;
        }

        private void numPan_ValueChanged(object sender, EventArgs e)
        {
            //fhp.Pan = ((float)numPan.Value);
            Helper.Pan = (float)numPan.Value;
            //Helper.system.update();
            //snd.HelperEx.FMODChannelInfo = fhp;
        }

        private void button13_Click(object sender, EventArgs e)
        {
            HS_Audio.Recoder.Forms.frmRecoding rt = new HS_Audio.Recoder.Forms.frmRecoding(this.Helper);
            rt.Show();
        }

        private void 스펙트럼분석ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("오른쪽 마우스 버튼을 많이 이용해 주세요 ^ㅇ^", "스펙트럼 이용하기전에 잠깐! :)");
            frmFFTGraph fg = new frmFFTGraph(Helper);
            fg.Show(); fg.StartDraw(); fg.BringToFront();
        }

        frm3D f3;
        private void d리스너설정ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            f3.Show(); f3.BringToFront();
        }

        HS_Audio.Forms.frmDSP frmdsp;
        private void dSP설정ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (frmdsp == null) { frmdsp = new frmDSP(this.Helper); frmdsp.Show(); }
            else { frmdsp.Show(); frmdsp.BringToFront(); }
            /*
            if (frm == null) { frmdsp = new frmDSP(this.Helper);frmdsp.Show();}
            else
            {
                if (frm.frmdsp == null) { frm.frmdsp = new frmDSP(this.Helper); frmdsp.Show(); }
                else { frm.frmdsp.Show(); frm.frmdsp.BringToFront(); }
            }*/
            //frmDSPHelper ds = new frmDSPHelper(snd); ds.Show();
        }

        private void 이퀄라이저EQ설정ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (/*frm != null*/false)
            {
                if (frm.fe == null) { frm.fe = new frmEqualizer(Helper); }
                frm.fe.Show(); frm.fe.BringToFront();
            }
            else
            {
                if (fe == null) { fe = new frmEqualizer(Helper); }
                fe.Show(); fe.BringToFront();
            }
        }

        private void 반향효과Reverb설정ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            button1_Click(null, null);
        }

        private void 출력사운드녹음ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmWaveDataOutputRecoding fr = new frmWaveDataOutputRecoding(Helper);
            MessageBox.Show("반드시 [도움말(H)]-[녹음 도움말] 을 읽어보시기 바랍니다."); fr.Show();
        }

        private void button2_Click(object sender, EventArgs e) { numPan.Value = 0; }

        private void 이퀄라이저그래프ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmEqualizerGraph fe = new frmEqualizerGraph(this.Helper);
            fe.Show();
        }

        HS_Audio.DSP.HSAudioDSPHelper hd = HS_Audio.DSP.HSAudioDSPHelper.Empty();
        private void checkBox6_CheckedChanged(object sender, EventArgs e)
        {
            
        }


        frmProjectUpdateSetting.ProjectUpdateSetting projSetting = new frmProjectUpdateSetting.ProjectUpdateSetting();
        frmProjectUpdateSetting _frmProjectUpdateSetting;
        private void 업데이트설정ToolStripMenuItem_Click(object sender, EventArgs e)
        {_frmProjectUpdateSetting.ShowDialog();}

        private void _frmProjectUpdateSetting_ProjectUpdated(frmProjectUpdateSetting.ProjectUpdateSetting Setting){ projSetting = Setting;}


        bool Lock;
        private void trbVolumeLeft_Scroll(object sender, EventArgs e)
        {
            Lock = true;
            //snd.channel.set
            this.lblVolumeLeft.Text = trbVolumeLeft.Value.ToString();

            if (chkGain.Checked)
            {
                if (trbVolumeLeft.Value == 0) { Helper.Volume = 0f; statusStrip1.Text = Helper.Result.ToString(); }
                else { Helper.Volume = (this.reverb.Gain * 0.01f + 1) + (float)(trbVolumeLeft.Value * 3 * 0.001f); statusStrip1.Text = Helper.Result.ToString(); }
            }
            else { Helper.Volume = (float)(trbVolumeLeft.Value * 0.01f); statusStrip1.Text = Helper.Result.ToString(); }

            if (trbVolumeLeft.Value > 100) { lblVolumeLeft.ForeColor = Color.Red; }
            else { lblVolumeLeft.ForeColor = Color.Black; }
            Lock = false;
        }
        private void Helper_VolumeChanging(HSAudioHelper Helper, float Volume) {if(!Lock)try{trbVolumeLeft.Value = (int)(Volume * 100);}catch{}}

        private void trbVolumeLeft_ValueChanged(object sender, EventArgs e){lblVolumeLeft.Text =trbVolumeLeft.Value.ToString();}

        private void cbOutputType_SelectedIndexChanged(object sender, EventArgs e)
        {
            OUTPUTTYPE outt = (OUTPUTTYPE)Enum.Parse(typeof(OUTPUTTYPE), cbOutputType.SelectedItem.ToString());
            Helper.OutputType = outt;
            label10.Text = "내부 상태: "+Helper.Result.ToString();
        }

        private void btnOutputTypeDefault_Click(object sender, EventArgs e)
        {
            cbOutputDevice.Text = Helper.DefaultOutputType.ToString();
            label10.Text = "내부 상태: "+Helper.Result.ToString();
        }

        bool First_cbOutputDevice_Change;
        Device LastselectedIndex;
        MMDevice mmd;
        private void cbOutputDevice_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(!ComboBoxLock)
            try{
            if (First_cbOutputDevice_Change){

                //인덱스가 0 과같지 않으면 
                //chkAutoDeviceDetect.Enabled = cbOutputDevice.SelectedIndex != 0;
                if (cbOutputDevice.SelectedIndex == 0)
                {
                    Device d = (Device)cbOutputDevice.Items[1];
                    if (!d.Equals(Helper.SoundDevice)) Helper.SoundDevice = d;
                    /*
                    if (cbOutputDevice.Items.Count - 1 != Helper.getSoundDevices().Length && !ComboBoxLock) btnSoundDeviceRefresh_Click(null, null);
                    else
                    {
                        RESULT result = Helper.setSoundDevice(Helper.DefaultSoundDevice);
                        lblFMODResult.Text = result.ToString();
                        if (result == RESULT.ERR_UNINITIALIZED) MessageBox.Show("사운드 장치를 변경하던 중 심각한 문제가 발생했습니다. \n이는 해당 사운드장치가 연결이 안되있거나 문제가 발생했음을 의미합니다. \n\n프로그램을 재 시작 해주시기 바랍니다.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }*/
                }
                else
                {
                    //if (Helper.getSoundDevice().Length != comboBox1.Items.Count-1)

                    //if (!((Device)cbOutputDevice.SelectedItem).Equals(LastselectedIndex))
                    {
                        trackBar3.Value = 0; checkBox2.Checked = false; label12.Text = "N/A";
                        if (cbOutputDevice.Items.Count - 1 != Helper.getSoundDevices().Length && !ComboBoxLock) {try{btnSoundDeviceRefresh_Click("cbOutputDevice_SelectedIndexChanged", null);}catch{} }
                        LastselectedIndex = (Device)cbOutputDevice.SelectedItem;
                        RESULT result = Helper.setSoundDevice((Device)cbOutputDevice.SelectedItem);
                        lblFMODResult.Text = result.ToString();
                        if (result == RESULT.ERR_UNINITIALIZED) MessageBox.Show("사운드 장치를 변경하던 중 심각한 문제가 발생했습니다. \n이는 해당 사운드장치가 연결이 안되있거나 문제가 발생했음을 의미합니다. \n\n프로그램을 재 시작 해주시기 바랍니다.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }}catch{}
            try
            {
                if (WindowsXP_Higher)
                {
                    //if (WASAPIPeak_Thread != null) WASAPIPeak_Thread.Dispose();
                    WASAPIPeak_Thread.Stop();
                    this.mmd = null;
                    foreach (MMDevice mmd in mmdc)
                    {
                        int a = mmd.ID.LastIndexOf(".");
                        string a1 = mmd.ID.Substring(a + 1);
                        Device dd = Helper.SoundDevice;//cbOutputDevice.SelectedIndex > 0 ? (Device)cbOutputDevice.SelectedItem : Helper.DefaultSoundDevice;
                        if (a1.ToUpper() == dd.GUID.ToString(true).ToUpper())
                        {
                            if (this.mmd != null && this.mmd.AudioEndpointVolume != null) this.mmd.AudioEndpointVolume.OnVolumeNotification -= AudioEndpointVolume_OnVolumeNotification;
                            /*MessageBox.Show(mmd.DeviceFriendlyName);*/
                            this.mmd = mmd; mmd.AudioEndpointVolume.OnVolumeNotification += AudioEndpointVolume_OnVolumeNotification;
                            checkBox2.Checked = mmd.AudioEndpointVolume.Mute;
                            trackBar3.Value = (int)(10000 * (checkBox7.Checked ? mmd.AudioEndpointVolume.MasterVolumeLevelPercent : mmd.AudioEndpointVolume.MasterVolumeLevelScalar));
                            WASAPIPeak_Thread.Start();// = new System.Threading.Timer(new System.Threading.TimerCallback(WASAPIPeak_ThreadLoop)); break;
                        }
                    }
                }
                else
                {
                    /*
                    WindowsXPVolumeControl.Dispose();
                    GC.Collect();
                    WindowsXPVolumeControl = new LIBRARY.HSVolumeControl_WinXP(this.Handle,cbOutputDevice.SelectedIndex-1);
                    WindowsXPVolumeControl.VolumeChanged += WindowsXPVolumeControl_VolumeChanged;
                    WindowsXPVolumeControl.MuteChanged += WindowsXPVolumeControl_MuteChanged;*/
                    mi.DeviceId = LastselectedIndex.ID;
                    WindowsXPLine = mi.Lines[0];
                    WindowsXPLine_MixerLineChanged(mi, WindowsXPLine);
                }
            }
            catch{}
            //else{if(DectectDefaultSoundDevice_Thread!=null)DectectDefaultSoundDevice_Thread.Abort(); System.Threading.ThreadPool.QueueUserWorkItem(new System.Threading.WaitCallback(DectectDefaultSoundDevice_ThreadLoop));}
            First_cbOutputDevice_Change = true;

            //RESULT result = RESULT.OK;
            //label15.Text = string.Format("출력 타입: {0} (결과: {1})", Helper.getOutputType(out result), result);
        }

        System.Threading.Thread DectectDefaultSoundDevice_Thread;
       // Device LastDefaultDevice = new Device(0, "", new GUID());
        
        Device[] BeforeDevices, CurrentDevices;
        Device BeforeDevice;
        bool FirstLoop = true;
        //NAudio.Wave.WaveOutCapabilities[] LastWaveOutCapabilities;
        int waveOutDevices = 0;
        Exception DectectDefaultSoundDevice_ThreadLoop_Ex = null;
        void DectectDefaultSoundDevice_ThreadLoop(object o)
        {
            /*
            if (FirstLoop)
            {
                before = Helper != null ? Helper.getSoundDevice() : null;
                Before = Helper.DefaultSoundDevice;
                asdasd = null;
                FirstLoop = false;
            }*/

            Retry:
            try
            {
                while (Helper != null && (cbOutputDevice.SelectedIndex == 0 || chkAutoDeviceDetect.Checked))
                {
                    //MessageBox.Show(before.Name, Helper.DefaultSoundDevice.Name);
                    try
                    {
                        RESULT[] results = null;
                        CurrentDevices = Helper.getSoundDevices(out results);
                        //만약 사운드장치를 못찾겠으면 0.5 초 대기후 다시시도.
                        if (BeforeDevices.Length < 0) { System.Threading.Thread.Sleep(500); goto Retry; }

                        //만약 0번쨰 장치(기본장치)가 서로 틀리다면
                        if (!BeforeDevices[0].Equals(BeforeDevice) ||
                            //전의 장치갯수와 현재 장치갯수가 틀리다면
                            BeforeDevices.Length != CurrentDevices.Length ||
                            //현재 장치 목록하고 전의 장치목록이 틀리다면
                            !Device.MatchDeviceArray(BeforeDevices, CurrentDevices))
                        {
                            //UI Thread 에서 장치 새로고침 버튼 누름
                            this.InvokeIfNeeded(() => {btnSoundDeviceRefresh_Click(null, null);});
                            //2.5(finally +0.5) 초동안 대기
                            System.Threading.Thread.Sleep(2500);
                        }
                        /*
                        if (LastWaveOutCapabilities == null || waveOutDevices != NAudio.Wave.WaveOut.DeviceCount)
                            LastWaveOutCapabilities = new NAudio.Wave.WaveOutCapabilities[NAudio.Wave.WaveOut.DeviceCount];

                        DectectDefaultSoundDevice_Thread = System.Threading.Thread.CurrentThread;

                        bool Detect = false;
                        for (int i = 0; i < waveOutDevices; i++)
                        {
                            //Console.WriteLine("NAudio Device: {0}({1})\nLast Device: {2}({3}\n",NAudio.Wave.WaveOut.GetCapabilities(i).ProductGuid,NAudio.Wave.WaveOut.GetCapabilities(i).ProductName, LastWaveOutCapabilities[i].ProductGuid, LastWaveOutCapabilities[i].ProductName);
                            //FirstLoop
                            NAudio.Wave.WaveOutCapabilities wo = NAudio.Wave.WaveOut.GetCapabilities(i);

                            if (!LastWaveOutCapabilities[i].ProductGuid.Equals(new System.Guid()) && (
                                LastWaveOutCapabilities[i].ProductGuid.CompareTo(wo.ProductGuid) != 0 ||
                                LastWaveOutCapabilities[i].ManufacturerGuid.CompareTo(wo.ManufacturerGuid) != 0 ||
                                LastWaveOutCapabilities[i].ProductName != wo.ProductName ||
                                waveOutDevices != NAudio.Wave.WaveOut.DeviceCount) &&
                                !Detect)
                            {
                                Detect = true;
                                this.InvokeIfNeeded(() =>
                                {
                                    cbOutputDevice_SelectedIndexChanged(null, null);
                                    btnSoundDeviceRefresh_Click(null, null);
                                });
                                System.Threading.Thread.Sleep(500);
                            }
                            LastWaveOutCapabilities[i] = NAudio.Wave.WaveOut.GetCapabilities(i);
                        }
                        waveOutDevices = NAudio.Wave.WaveOut.DeviceCount;*/

                        /*
                        asdasd = Helper.getSoundDevice();
                        //Console.WriteLine(Before.Name + "," + Helper.DefaultSoundDevice.Name + "   " + asdasd.Length + "," + before.Length);

                        //MessageBox.Show(before.Length.ToString(), asd.Length.ToString());
                        if (Before.GUID.GetGuid != Helper.DefaultSoundDevice.GUID.GetGuid ||   before.Length != asdasd.Length)
                        {
                            before =asdasd;
                            Before = Helper.DefaultSoundDevice;

                            this.InvokeIfNeeded(()=>{btnSoundDeviceRefresh_Click(null, null);});
                            //btnSoundDeviceRefresh_Click_Safe();
                        }
                         }catch{/*mm = new MMDeviceEnumerator();*/
                    }
                    finally { System.Threading.Thread.Sleep(500); }
                }
            }
            catch (System.Threading.ThreadAbortException) { }
            catch (Exception ex) { if (DectectDefaultSoundDevice_ThreadLoop_Ex == null && !ex.Equals(DectectDefaultSoundDevice_ThreadLoop_Ex)) ex.Logging(); System.Threading.Thread.Sleep(500);  goto Retry; }
        }

        bool ComboBoxLock = false;
        private void btnSoundDeviceRefresh_Click(object sender, EventArgs e)
        {
            Device LastDevice = null;
            try
            {
                ComboBoxLock = true;

                cbOutputDevice.Enabled = trackBar3.Enabled = checkBox2.Enabled = checkBox7.Enabled = false;
                checkBox2.Checked = false; label12.Text = "N/A";
                label11.Text = "-Inf. dB";label14.Text = "-Inf.";
                progressBar1.Value = progressBar1.Minimum; progressBar2.Value = progressBar2.Minimum;
                //string a = cbOutputDevice.SelectedItem.ToString();
                LastDevice = cbOutputDevice.SelectedIndex == -1 ? null : cbOutputDevice.SelectedItem as Device;

                cbOutputDevice.Items.Clear();
                cbOutputDevice.Items.Add("(시스템에 사운드 장치가 없습니다!)");
                cbOutputDevice.Text = cbOutputDevice.Items[0].ToString();
                trackBar3.Value = 0;

                BeforeDevices = Helper.getSoundDevices();
                if (BeforeDevices.Length == 0 || BeforeDevices == null) return;

            }
            catch (Exception ex) {ex.Logging(); }
            
            BeforeDevice = BeforeDevices[0];

            if (BeforeDevices.Length > 0 && BeforeDevices != null)
            {
                cbOutputDevice.Items.Clear();
                cbOutputDevice.Items.Add("(기본 출력 장치)");
                cbOutputDevice.Items.AddRange(BeforeDevices);

                bool FindDevice = false;
                if (LastDevice != null)
                { 
                    for (int i = 0; i < cbOutputDevice.Items.Count; i++)
                    {
                        Device d = cbOutputDevice.Items[i] as Device;
                        if (d != null && d.Equals(LastDevice))
                        {
                            MessageBox.Show("Found!!");
                            FindDevice = true;
                            cbOutputDevice.SelectedIndex = i;
                            break;
                        }
                    }
                }
                if (First_cbOutputDevice_Change) cbOutputDevice.SelectedIndex = 0;
                else if (!FindDevice)
                {
                    cbOutputDevice.SelectedIndex = 0;
                    
                    RESULT result = Helper.setSoundDevice(Helper.DefaultSoundDevice);
                    lblFMODResult.Text = result.ToString();
                    if (result == RESULT.ERR_UNINITIALIZED) MessageBox.Show("사운드 장치를 변경하던 중 심각한 문제가 발생했습니다. \n이는 해당 사운드장치가 연결이 안되있거나 문제가 발생했음을 의미합니다. \n\n프로그램을 재 시작 해주시기 바랍니다.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                ComboBoxLock = false;
                cbOutputDevice.Enabled = true;
                //if (cbOutputDevice.Text == "") cbOutputDevice.SelectedIndex = 0;
                //trackBar3.Enabled = false;
                try
                {
                    if (WindowsXP_Higher)
                    {
                        //if (WASAPIPeak_Thread != null) WASAPIPeak_Thread.Dispose();
                        WASAPIPeak_Thread.Stop();
                        mmd_Default = mm.GetDefaultAudioEndpoint(DataFlow.Render, Role.Multimedia);
                        mmdc = mm.EnumerateAudioEndPoints(DataFlow.Render, DeviceState.All);
                        foreach (MMDevice mmd in mmdc)
                        {
                            int a = mmd.ID.LastIndexOf(".");
                            string a1 = mmd.ID.Substring(a + 1);
                            Device dd = cbOutputDevice.SelectedIndex > 0 ? (Device)cbOutputDevice.SelectedItem : Helper.DefaultSoundDevice;
                            if (a1.ToUpper() == dd.GUID.ToString(true).ToUpper())
                            {
                                if (this.mmd != null && this.mmd.AudioEndpointVolume != null) this.mmd.AudioEndpointVolume.OnVolumeNotification -= AudioEndpointVolume_OnVolumeNotification;
                                /*MessageBox.Show(mmd.DeviceFriendlyName);*/
                                this.mmd = mmd; mmd.AudioEndpointVolume.OnVolumeNotification += AudioEndpointVolume_OnVolumeNotification;
                                trackBar3.Enabled = checkBox2.Enabled = checkBox7.Enabled = checkBox7.Visible=progressBar1.Visible =true;
                                trackBar3.Value = (int)(10000 * (checkBox7.Checked?mmd.AudioEndpointVolume.MasterVolumeLevelPercent:mmd.AudioEndpointVolume.MasterVolumeLevelScalar));
                                //System.Threading.ThreadPool.QueueUserWorkItem(new System.Threading.WaitCallback(WASAPIPeak_ThreadLoop)); break;
                                 WASAPIPeak_Thread.Start();
                            }
                        }
                    }
                    else
                    {
                        if(mi!=null)mi.Dispose();
                        mi = new WaveLib.AudioMixer.Mixer(WaveLib.AudioMixer.MixerType.Playback);
                        WindowsXPLine = mi.Lines[0];
                        mi.MixerLineChanged += WindowsXPLine_MixerLineChanged;
                        trackBar3.Value = (int)(WindowsXPLine.VolumeScala * 10000);
                        trackBar3.Enabled = checkBox2.Enabled = true;
                    
                    }
                    /*
                    ComboBoxLock = true;
                    cbOutputDevice.SelectedIndex = 0;
                    ComboBoxLock = false;*/
                }
                catch
                {
                    cbOutputDevice.Enabled =false;
                    ComboBoxLock = true;
                    cbOutputDevice.Items.Clear();
                    cbOutputDevice.Items.Add("(시스템 사운드 장치오류!)");
                    cbOutputDevice.Text = cbOutputDevice.Items[0].ToString();
                    ComboBoxLock = false;
                }
            }
            //RESULT result = RESULT.OK;
            //label15.Text = string.Format("출력 타입: {0} (결과: {1})", Helper.getOutputType(out result), result);
            //if (DectectDefaultSoundDevice_Thread != null) DectectDefaultSoundDevice_Thread.Abort();
            //System.Threading.ThreadPool.QueueUserWorkItem(new System.Threading.WaitCallback(DectectDefaultSoundDevice_ThreadLoop));
        }

        private void WindowsXPVolumeControl_MuteChanged(object sender, LIBRARY.HSVolumeControl_WinXP.VolumeCategory Category, bool Mute)
        {
            if(Category == LIBRARY.HSVolumeControl_WinXP.VolumeCategory.Master) checkBox2.Checked = Mute;
        }

        private void WindowsXPVolumeControl_VolumeChanged(object sender, LIBRARY.HSVolumeControl_WinXP.VolumeCategory Category, int Volume)
        {
            HSVolumeControl_WinXP xp = sender as HSVolumeControl_WinXP;
            if (Category == LIBRARY.HSVolumeControl_WinXP.VolumeCategory.Master)
                if (xp != null) if(!LockVolume)trackBar3.Value = (int)(xp.VolumeScala*10000);
                else if(!LockVolume)trackBar3.Value = (int)(Volume/65535.0)*10000;
        }
        private void AudioEndpointVolume_OnVolumeNotification(AudioVolumeNotificationData data)
        {
            //float Vol = (mmd.AudioEndpointVolume.MasterVolumeLevel-mmd.AudioEndpointVolume.VolumeRange.MinDecibels)/(mmd.AudioEndpointVolume.VolumeRange.MaxDecibels-mmd.AudioEndpointVolume.VolumeRange.MinDecibels);
            if(!LockVolume)trackBar3.Value = (int)(10000 * (checkBox7.Checked ? mmd.AudioEndpointVolume.MasterVolumeLevelPercent : mmd.AudioEndpointVolume.MasterVolumeLevelScalar));
            checkBox2.Checked = data.Muted;
        }



        private void 플러그인설정ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmplug.Show();
        }

        private void PlayerSetting_Activated(object sender, EventArgs e)
        {
            timer1.Start();
        }

        private void PlayerSetting_VisibleChanged(object sender, EventArgs e)
        {
            if(this.Visible)timer1.Start();
            else timer1.Stop();
        }

        private void PlayerSetting_Resize(object sender, EventArgs e)
        {
            if (this.Location.X < -this.Size.Width || this.Location.Y < -this.Size.Height)
                timer1.Stop();
            else timer1.Start();
        }

        MemoryStream ms = new MemoryStream();
        private void 테스트ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            WaveForm wf = new WaveForm(Helper);wf.Show();
        }

        List<frmAGC> agc = new List<frmAGC>();
        private void aGC설정ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("이 창은 한번 열어두면 닫을 수 없습니다.", "HS™ 플레이어 재생 설정", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            frmAGC tmp = new frmAGC(Helper);
            agc.Add(tmp);
            tmp.Show();
        }

        bool LockVolume;
        
        private void trackBar3_Scroll(object sender, EventArgs e)
        {
            try
            {
                LockVolume = true;
                if (mmd != null) if (checkBox7.Checked) mmd.AudioEndpointVolume.MasterVolumeLevelPercent = trackBar3.Value * 0.0001f; else mmd.AudioEndpointVolume.MasterVolumeLevelScalar = trackBar3.Value * 0.0001f;
                if (WindowsXPLine != null) WindowsXPLine.VolumeScala = trackBar3.Value * 0.0001f;
            }
            finally{LockVolume = false;}
        }

        private void trackBar3_ValueChanged(object sender, EventArgs e)
        {
            if (mmd != null) label12.Text = checkBox7.Checked ?
                string.Format("{0} %\n({1} dB)", (trackBar3.Value / 100.0).ToString("0.00"), mmd.AudioEndpointVolume.MasterVolumeLevel.ToString("0.00")) : 
                string.Format("{0}\n({1} dB)", (trackBar3.Value / (WindowsXP_Higher ? 100 : 100.0f)).ToString("0.00"),mmd.AudioEndpointVolume.MasterVolumeLevel.ToString("0.00"));
            else label12.Text = checkBox7.Checked ? (trackBar3.Value / 100.0).ToString("0.00") + "%" : (trackBar3.Value / (WindowsXP_Higher?100:100.0f)).ToString("0.00");
        }

        internal void SetVolumePercent(float Value, bool Percent)
        {

        }
        internal float GetVolumePercent(bool Percent)
        {
            return 0;
        }

        bool LockMute;
        private void checkBox2_Click(object sender, EventArgs e)
        {
            try
            {
                LockMute = true;
                if (mmd != null) mmd.AudioEndpointVolume.Mute = checkBox2.Checked;
                if (WindowsXPLine != null) WindowsXPLine.Mute = checkBox2.Checked;
            }
            finally { LockMute = false; }
        }

        private void checkBox7_CheckedChanged(object sender, EventArgs e)
        {
            trackBar3.SmallChange = checkBox7.Checked?2:100;
            if (label12.Text != "N/A")
            {
                if(mmd!=null)trackBar3.Value = (int)(10000 * (checkBox7.Checked ? mmd.AudioEndpointVolume.MasterVolumeLevelPercent : mmd.AudioEndpointVolume.MasterVolumeLevelScalar));
                if (trackBar3.Value == trackBar3.Maximum) label12.Text = checkBox7.Checked? "100.00%" : "100.00";
                if (trackBar3.Value == trackBar3.Minimum) label12.Text = checkBox7.Checked ? "0.00%" : "0.00";
            }
        }

        public void WindowsXPLine_MixerLineChanged(WaveLib.AudioMixer.Mixer mixer, WaveLib.AudioMixer.MixerLine line)
        {
            if (!LockVolume) trackBar3.Value = (int)(mixer.Lines[0].VolumeScala * 10000);
            if (!LockMute) checkBox2.Checked = mixer.Lines[0].Mute;
        }

        Timer WASAPIPeak_Thread = new Timer(){Interval = 25};
        void WASAPIPeak_ThreadLoop(object o, EventArgs e)
        {
            //WASAPIPeak_Thread = System.Threading.Thread.CurrentThread;
            //while (mmd!=null)
            {
                try
                {
                    //if (this.WindowState != FormWindowState.Minimized&&this.WindowShowStatus != WindowShowState.Hide)
                    {
                        WASAPIPeak_Thread.Interval = 25;
                        float a = mmd == null ? 0 : mmd.AudioMeterInformation.MasterPeakValue;
                        double a1 = Decibels(a, 0);
                        float b = a * 10000;

                        //progressBar1.InvokeIfNeeded(() => this.progressBar1.Value = (int)a);
                        this.progressBar1.Value = a1 < -40 ? 0 : Math.Min((int)(41 + a1), 40);
                        label11.Text = string.Format("{0} dB", a1 < -171 ? "-Inf" : a1.ToString("0.00"));
                        this.progressBar2.Value = (int)b;
                        label14.Text = string.Format("{0}", a1 < -1000 ? "-Inf" : a.ToString("0.0000000"));
                    }
                }
                catch{WASAPIPeak_Thread.Interval = 1000;}
                finally{}
            }
        }
        public static double mag_sqrd(double re, double im) { return (re * re + im * im); }

        public static double Decibels(double re, double im) { return ((re == 0 && im == 0) ? (0) : 10.0 * Math.Log10(((mag_sqrd(re, im))))); }

        private void checkBox8_CheckedChanged(object sender, EventArgs e)
        {
            if(checkBox8.Checked)fMODSystemToolStripMenuItem_Click(null, null);
            else fMODChannelToolStripMenuItem_Click(null, null);
        }

        System.Threading.Thread DectectSoundDeviceThread;
        System.Threading.ParameterizedThreadStart DectectSoundDeviceThreadStart;
        private void chkAutoDeviceDetect_CheckedChanged(object sender, EventArgs e)
        {
            if (DectectSoundDeviceThreadStart==null)DectectSoundDeviceThreadStart = new System.Threading.ParameterizedThreadStart(DectectDefaultSoundDevice_ThreadLoop);
            if(DectectSoundDeviceThread != null)DectectSoundDeviceThread.Abort();

            if (chkAutoDeviceDetect.Checked)
            {
                DectectSoundDeviceThread = new System.Threading.Thread(DectectSoundDeviceThreadStart){Name = "HS 플레이어 사운드 출력 장치 감시 스레드"};
                DectectSoundDeviceThread.Start();
            }
            //if (chkAutoDeviceDetect.Checked){Before = (Device)cbOutputDevice.SelectedItem;before = new Device[cbOutputDevice.Items.Count-1];}
        }

        private void 개발자옵션ToolStripMenuItem_CheckedChanged(object sender, EventArgs e)
        {
            if (개발자옵션ToolStripMenuItem.Checked) MessageBox.Show("개발자 옵션을 켠다음 옵션을 변경해서 일어나는 버그들에 대해\n개발자는 책임을 지지 않습니다.\n그리고 버그에대한 모든 책임은 사용자에게 있습니다.", "개발자 옵션 경고", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            groupBox9.Enabled=groupBox8.Enabled = 개발자옵션ToolStripMenuItem.Checked;
        }

        private void peak그래프ToolStripMenuItem_Click(object sender, EventArgs e)
        {
        }

        bool timer1_Start;
        bool WASAPIPeak_Thread_Start;
        private void PlayerSetting_WindowStateChanged(object sender, EventArgs e)
        {
            if (WindowState == FormWindowState.Minimized || this.WindowShowStatus == HS_Audio.Control.WindowShowState.Hide)
            {
                timer1_Start = timer1.Enabled; timer1.Stop();
                WASAPIPeak_Thread_Start = WASAPIPeak_Thread.Enabled; WASAPIPeak_Thread.Stop();
            }
            else
            {
                if (timer1_Start) timer1.Start();
                if (WASAPIPeak_Thread_Start) WASAPIPeak_Thread.Start();
            }
        }

        frmPeakMeter peak;
        private void 번Peak그래프ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            peak = new frmPeakMeter(this.Helper);
            peak.Start();
            peak.Show();
            peak.BringToFront();
        }

        frmPeakMeter1 peak1;
        private void 번Peak그래프ToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            peak1 = new frmPeakMeter1(this.Helper);
            peak1.Show();
            peak1.BringToFront();
            peak1.Start();
        }

        int 구간반복Click =0;
        uint 구간반복Start = 0;
        uint 구간반복Stop = 0;
        System.Threading.Thread 구간반복Thread;
        
        private void btn구간반복_Click(object sender, EventArgs e)
        {
            try{if (구간반복Thread!=null)구간반복Thread.Abort();}catch{}

            if (구간반복Click == 0)
            {
                구간반복Start = Helper.CurrentPosition;
                btn구간반복.Text = "구간반복 (" + 구간반복Start + " ~ ?)";
                this.toolTip1.SetToolTip(this.btn구간반복, "한번 더 버튼을 클릭하여 구간반복 설정");
                toolTip1.Show("한번 더 버튼을 클릭하여 구간반복 설정", btn구간반복, 2000);
                구간반복Click = 1;
            }
            else if (구간반복Click == 1)
            {
                구간반복Stop = Helper.CurrentPosition;
                btn구간반복.Text = string.Format("구간반복 ({0} ~ {1})", 구간반복Start, 구간반복Stop);
                this.toolTip1.SetToolTip(this.btn구간반복, "버튼을 클릭하여 구간반복 해제");
                toolTip1.Show("버튼을 클릭하여 구간반복 해제", btn구간반복, 2000);
                구간반복Thread = new System.Threading.Thread(new System.Threading.ThreadStart(구간반복감시));
                구간반복Thread.Name = "구간반복 스레드 (SectionRepeat Thread)";
                구간반복Thread.Start();
                구간반복Click = 2;
            }
            else
            {
                구간반복Start = 0;
                구간반복Stop = 0;
                btn구간반복.Text = "구간반복";
                this.toolTip1.SetToolTip(this.btn구간반복, "버튼을 클릭하여 구간반복 설정");
                toolTip1.Show("버튼을 클릭하여 구간반복 설정", btn구간반복, 2000);
                구간반복Click = 0;
            }
        }
        void 구간반복감시()
        {
            long LastPosition = 0;
            //long b = 0;
            while (true)
            {
                long a = Helper.CurrentPosition;
                long tmp = 구간반복Start;
                long tmp1 = 구간반복Stop;
                //if ((a - LastPosition) > 0) Console.WriteLine("앙 기모띠");

                #region 1번째 방법
                
                if ((a - LastPosition) > 0) //역재생
                {
                    if ((tmp - tmp1) > 0)//구간반복이 역으로 되있으면
                    {
                        //현재 위치가 구간반복 끝시간보다 작거나 시작시간보다 크면
                        if (a < 구간반복Start && a > 구간반복Stop) Helper.CurrentPosition = 구간반복Start;//현재 위치를 처음지점 으로 옮김
                    }
                    else //구간반복이 제대로 되있으면
                    {
                        //현재 위치가 구간반복 끝시간보다 크거나 시작시간보다 작으면
                        if (a > 구간반복Stop || a < 구간반복Start) Helper.CurrentPosition = 구간반복Stop;//현재 위치를 끝 지점 으로 옮김
                    }
                }
                else //올바르게 재생
                {
                    if ((tmp - tmp1) > 0)//구간반복이 역으로 되있으면
                    {
                        //현재 위치가 구간반복 끝시간보다  작거나 시작시간보다 크면
                        if (a > 구간반복Start || a < 구간반복Stop) Helper.CurrentPosition = 구간반복Stop;//현재 위치를 끝지점 으로 옮김
                    }
                    else
                    {
                        //현재 위치가 구간반복 끝시간보다 크거나 시작시간보다 작으면
                        if (a > 구간반복Stop || a < 구간반복Start) Helper.CurrentPosition = 구간반복Start;//현재 위치를 처음지점 으로 옮김
                    }
                }
                #endregion

                #region 2번째 방법
                /*
                if ((현재위치 - LastPosition) > 0) //역재생
                {
                    if ((시작지점1 - 끝지점1) > 0)//구간반복이 역으로 되있으면
                    {
                        //현재 위치가 구간반복 끝시간보다 작거나 시작시간보다 크다는 조건이 성립되지 않으면
                        if (!(현재위치 < 시작지점 && 현재위치 > 끝지점)) Helper.CurrentPosition = 시작지점;//현재 위치를 처음지점 으로 옮김
                    }
                    else //구간반복이 제대로 되있으면
                    {
                        //현재 위치가 구간반복 끝시간보다 크거나 시작시간보다 작다는 조건이 성립되지 않으면
                        if (!(현재위치 < 끝지점 && 현재위치 > 시작지점)) Helper.CurrentPosition = 끝지점;//현재 위치를 끝 지점 으로 옮김
                    }
                }
                else //올바르게 재생
                {
                    if ((시작지점1 - 끝지점1) > 0)//구간반복이 역으로 되있으면
                    {
                        //현재 위치가 구간반복 끝시간보다  작거나 시작시간보다 크다는 조건이 성립되지 않으면
                        if (!(현재위치 < 끝지점 && 현재위치 > 시작지점)) Helper.CurrentPosition = 끝지점;//현재 위치를 끝지점 으로 옮김
                    }
                    else
                    {
                        //현재 위치가 구간반복 끝시간보다 크거나 시작시간보다 작다는 조건이 성립되지 않으면
                        if (!(현재위치 < 끝지점 && 현재위치 > 시작지점)) Helper.CurrentPosition = 시작지점;//현재 위치를 처음지점 으로 옮김
                    }
                }
                 */
                #endregion

                #region 3번째 방법
                
                if ((a - LastPosition)>0 || (tmp-tmp1) > 0)
                {
                    if ((a - LastPosition) > 0 && (a < 구간반복Stop)) Helper.CurrentPosition = 구간반복Stop;
                    if (((구간반복Start - 구간반복Stop) > 0)&&
                        (a < 구간반복Stop||a>구간반복Start)) Helper.CurrentPosition = 구간반복Stop;
                    //if (a < 구간반복Start) Helper.CurrentPosition = 구간반복Stop;
                }
                else
                {
                    if (a > 구간반복Stop) Helper.CurrentPosition = 구간반복Start;
                }
                #endregion

                LastPosition = a;
                //LastPosion = Helper.CurrentPosition - ;
                System.Threading.Thread.Sleep(1);
            }
        }

        private void 구간반복도움말ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("현재 준비중 입니다.");
        }

        private void 사운드스크립트ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmSoundScript fs = new frmSoundScript(Helper);
            fs.Show();
        }

        private void label7_Click(object sender, EventArgs e)
        {

        }

        HS_Audio.Recoder.frmNorebang nore;
        private void 마이크입력노래방ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if(nore==null)nore = new HS_Audio.Recoder.frmNorebang(Helper);
            nore.Show();
        }
    }
}
