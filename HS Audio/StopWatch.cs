﻿using System;
using System.Text;
using System.Threading;

namespace HS_Audio
{
    public class StopWatch:IDisposable
    {
        public enum TickFormat { Millisecond, Millisecond10_1, Millisecond100_1, Second };
        public enum StopWatchStatus { Ready, Start, StartPending, Stop, StopPending }
        public delegate void StopWatchStatusChangedEventhandler(StopWatchStatus Status);
        public event StopWatchStatusChangedEventhandler StopWatchStatusChanged;

        public StopWatch(TickFormat Format = TickFormat.Millisecond10_1)
        { this.Format = Format; ts = new ThreadStart(TickThread); th = new Thread(ts) { Name = "StopWatch Thread" }; }

        ThreadStart ts;
        Thread th;
        StopWatchStatus _Status = StopWatchStatus.Ready;

        public long Hour { get; private set; }
        public byte Second{ get; private set; }
        public byte Minute { get; private set; }
        public ushort MilliSecond { get; private set; }

        public ulong ClearCount { get; private set; }
        public StopWatchStatus Status { get { return _Status; } set { _Status = value; try { StopWatchStatusChanged(value);}catch{}  } }

        public TickFormat Format { get; private set; }
        //public uint Speed { get; private set; }


        bool Stopping;

        void TickThread()
        {
            while (true)
            {
                if (!Stopping)
                {
                    if (Format == TickFormat.Millisecond)
                    {
                        MilliSecond++;
                        if (MilliSecond > 999) { Second++; MilliSecond = 0; }
                        if (Second >60) { Second = 0; Minute++; }
                        if (Minute > 60) { Minute = 0; Hour++; }
                        Thread.Sleep(1);
                    }
                    else if (Format == TickFormat.Millisecond10_1)
                    {
                        MilliSecond+=10;
                        if (MilliSecond > 999) { Second++; MilliSecond = 0; }
                        if (Second > 60) { Second = 0; Minute++; }
                        if (Minute > 60) { Minute = 0; Hour++; }
                        Thread.Sleep(10);
                    }
                    else if (Format == TickFormat.Millisecond100_1)
                    {
                        MilliSecond += 100;
                        if (MilliSecond > 999) { Second++; MilliSecond = 0; }
                        if (Second > 60) { Second = 0; Minute++; }
                        if (Minute > 60) { Minute = 0; Hour++; }
                        Thread.Sleep(100);
                    }
                    else
                    {
                        Second++;
                        if (Second > 60) { Second = 0; Minute++; }
                        if (Minute > 60) { Minute = 0; Hour++; }
                        MilliSecond = 0;
                        Thread.Sleep(1000);
                    }
                }
            }
        }

        public void Start()
        {
            try
            {
                if (th.ThreadState == ThreadState.Unstarted) th.Start();
                th.Resume();
                Status = StopWatchStatus.Start;
            }
            catch { }
        }
        public void Stop()
        {
            th.Suspend();
            Status = StopWatchStatus.Stop;
        }
        public void Clear()
        {
            MilliSecond = 0; Second = Minute = 0; Hour = 0;
            ClearCount++;
        }

        public bool IsDisposed { get; private set; }
        public void Dispose()
        {
            try { th.Abort(); }catch{}
            th = null; ts = null;
            IsDisposed = true;
        }
        
        StringBuilder sb = new StringBuilder();
        public new string ToString(bool MilliSecondIsDot = true,bool AttachZero = true)
        {
            sb.Remove(0, sb.Length);
            if (AttachZero)
            {
                sb.Append(Hour.ToString("00")); sb.Append(":"); sb.Append(Minute.ToString("00")); sb.Append(":");
                sb.Append(Second.ToString("00"));
                if (Format == TickFormat.Millisecond) { sb.Append(MilliSecondIsDot ? "." : ":"); sb.Append(MilliSecond.ToString("000")); }
                else if (Format == TickFormat.Millisecond10_1) { sb.Append(MilliSecondIsDot ? "." : ":"); sb.Append((MilliSecond / 10).ToString("00")); }
                else if (Format == TickFormat.Millisecond100_1) { sb.Append(MilliSecondIsDot ? "." : ":"); sb.Append((MilliSecond/100).ToString("0")); }
            }
            else
            {
                sb.Append(Hour); sb.Append(":"); sb.Append(Minute); sb.Append(":"); sb.Append(Second);
                if (Format != TickFormat.Second) { sb.Append(MilliSecondIsDot ? "." : ":"); sb.Append(MilliSecond); }
            }
            return sb.ToString();
        }

        ~StopWatch()
        {
            try { th.Abort(); } catch { }
            th = null; ts = null;
        }
    }
}
