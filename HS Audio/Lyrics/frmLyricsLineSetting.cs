﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace HS_Audio.Lyrics
{
    public partial class frmLyricsLineSetting : Form
    {
        public delegate void CompleteEventHandler(frmLyricsLineSetting sender, Dictionary<int,int[]> Value);
        public event CompleteEventHandler Complete;
        /// <summary>
        /// 배열형식 으로 된 문자열을 문자열로 바꾸어줍니다. 
        /// /// </summary>
        /// <param name="Array">문자열 배열입니다.</param>
        /// <param name="NewLine">True면 줄 바꿈(\r\n)을 합니다.</param>
        /// <returns>배열형식 으로 된 문자열을 문자열로 바꾸어 반환합니다.</returns>
        internal static string ConvertArrayToString(int[] Array, string NewLine)
        {
            StringBuilder a = new StringBuilder(Array[0].ToString());
            for (long i = 1; i < Array.LongLength; i++){a.Append(NewLine); a.Append(Array[i]);}
            return a.ToString();
        }
        /// <summary>
        /// 문자열을 문자열배열 형식으로 바꿔 반환합니다.
        /// </summary>
        /// <param name="Text">문자열 입니다.</param>
        /// <param name="NewLine">줄 바꿀 문자열입니다.(null이거나 빈 문자열이면 \n과 \r을 기준으로 배열합니다.)</param>
        /// <returns>문자열을 문자열배열 형식으로 바꿔 반환합니다.</returns>
        internal static int[] ConvertStringToIntArray(string Text, char NewLine)
        {
            string[] a = Text.Split(NewLine);
            if(a == null&&a.Length==0)return null;

            int[] tmp = new int[a.Length];
            for(int i=0;i<a.Length;i++)tmp[i] = int.Parse(a[i]);
            return tmp;
        }

        private Dictionary<int,int[]> _Value = new Dictionary<int,int[]>();
        public Dictionary<int, int[]> Value
        {
            get{return _Value;}
            set{ _Value = value;UpdateValue();}
        }

        public frmLyricsLineSetting()
        {
            InitializeComponent(); 
            numericUpDown1.MouseWheel += numericUpDown1_MouseWheel;
        }

        Dictionary<int, int[]> OriginalValue;
        private void frmLyricsLineSetting_Load(object sender, EventArgs e)
        {
            OriginalValue = new Dictionary<int, int[]>(Value);
            UpdateValue();
        }

        void UpdateValue(){try{textBox1.Text = ConvertArrayToString(Value[(int)numericUpDown1.Value], ",");  }catch{textBox1.Text = null;}}

        private void button1_Click(object sender, EventArgs e)
        {
            StringBuilder sb = new StringBuilder();
            if (_Value == null || _Value.Count == 0) sb.Append("줄을 숨기지 않습니다.");
            else
            {
                foreach (int a in _Value.Keys)
                {
                    if (sb.Length > 0) sb.AppendLine("번");
                    sb.AppendFormat("{0}줄 가사는 ", a.ToString());
                    sb.Append(ConvertArrayToString(_Value[a], "번, "));
                }
                sb.Append("번\r\n\r\n줄을 숨깁니다.");
            }
            MessageBox.Show(sb.ToString(), "가사 줄 설정 확인", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            //_Value.Remove((int)numericUpDown1.Value);
            textBox1.Text = null;
        }

        private void button2_Click(object sender, EventArgs e)
        {
           OriginalValue = Value;
            this.Close();
        }

        private void frmLyricsLineSetting_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (Complete != null) Complete(this, OriginalValue);
        }

        private void numericUpDown1_ValueChanged(object sender, EventArgs e)
        {
            UpdateValue();
        }

        private void textBox1_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.KeyValue == 13)button2_Click(null, null);
        }

        private void button4_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            this.toolTip1.Show("※메모: 줄수는 \",\"(콤마)로 구분합니다.\r\n\r\n예) 3줄가사일때 1번줄 3번줄 제외하고 싶을때\r\n     \"1,3\" 이라고 텍스트 박스에 쳐주시면 됩니다. ", this.textBox1);
            if (textBox1.Text == null || textBox1.Text == "") {if(Value!=null&&Value.ContainsKey((int)numericUpDown1.Value))_Value.Remove((int)numericUpDown1.Value);return;}
            int[] a = null;
            try{a = ConvertStringToIntArray(textBox1.Text, ',');}
            catch{try{a = new int[]{Convert.ToInt32(textBox1.Text)};}catch{}}
            if(a!=null&&a.Length>0)
                if(_Value.ContainsKey((int)numericUpDown1.Value))_Value[(int)numericUpDown1.Value] = a;
                else _Value.Add((int)numericUpDown1.Value, a);
        }
        private void numericUpDown1_MouseWheel(object sender, MouseEventArgs e)
        {
            if (e.Delta < 0 && numericUpDown1.Value <= numericUpDown1.Minimum)
            {
                numericUpDown1.Value = numericUpDown1.Minimum;return;
            }
            else if (e.Delta > 0 && numericUpDown1.Value >= numericUpDown1.Maximum)
            {
                numericUpDown1.Value = numericUpDown1.Maximum; return;
            }
            else
            {
                numericUpDown1.Value = e.Delta < 0 ? numericUpDown1.Value-- : numericUpDown1.Value++;
                return;
            }
            //e.Delta = e.Delta<0?-1:1;
        }
    }
}
