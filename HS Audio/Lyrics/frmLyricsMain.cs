﻿using System;

using System.Windows.Forms;
using System.Threading;

namespace HS_Audio.Lyrics
{
    public partial class frmLyricsMain : Form
    {
        System.Threading.Thread th;
        System.Threading.ParameterizedThreadStart ps;
        LyricParser lp;
        LyricsParser_1 lp1;
        public frmLyricsMain()
        {
            InitializeComponent();
            listView1.Items.Clear();
            //ps = new System.Threading.ParameterizedThreadStart(LyricsTime);
            //th = new System.Threading.Thread(ps);
        }
        public frmLyricsMain(string Lyric)
        {
            InitializeComponent();
            listView1.Items.Clear();
            this.Lyric = Lyric;
        }

        private void frmLyricsMain_Load(object sender, EventArgs e)
        {
            
        }

        string _Lyric;
        public string Lyric
        {
            get { return _Lyric; }
            set
            {
                _Lyric = value;
                lp = new LyricParser(value);// { OffsetEnabled = true };
                lp1 = new LyricsParser_1(value);
                if (th != null) th.Abort();
                ps = new System.Threading.ParameterizedThreadStart(LyricsTime);
                th = new Thread(ps); th.IsBackground = true;
            }
        }
        void LyricsTime(object o)
        {

        }
    }
}
