﻿using System;
using System.ComponentModel;
using System.Windows.Forms;
using System.IO;
using System.Threading;
using HS_CSharpUtility;

namespace HS_Audio.Lyrics
{
    public partial class frmLyricsSearch : Form
    {
        #region 가사찾기 이벤트 관련
        private AsyncOperation asyncOperation;
        private readonly SendOrPostCallback asnycOnGetLyricsComplete;
        private readonly object OnGetLyricsComplete_obj;

        public delegate void GetLyricsCompleteEventHandler(LyricsClass Lyrics);
        public event GetLyricsCompleteEventHandler GetLyricsComplete
        {
            add { base.Events.AddHandler(asnycOnGetLyricsComplete, value); }
            remove { base.Events.RemoveHandler(asnycOnGetLyricsComplete, value); }
        }

        protected virtual void OnGetLyricsComplete(LyricsClass Lyrics)
        {
            GetLyricsCompleteEventHandler handler = (GetLyricsCompleteEventHandler)base.Events[OnGetLyricsComplete_obj];//if (this._GetLyricsComplete != null)this._GetLyricsComplete(LyricsClass); }
            if (handler != null) handler(Lyrics);
        }
        private void OnGetLyricsCompleteOperation(object args) { this.OnGetLyricsComplete((LyricsClass)args); }
        #endregion


        public bool ShowTime = true;
        Thread[] th = new Thread[2];
        ParameterizedThreadStart[] ts= new ParameterizedThreadStart[2];
        //public int Timeout = 7000;

        public frmLyricsSearch()
        {
            InitializeComponent();
            radioButton1.Enabled = false; radioButton2.Checked = true;
            this.asyncOperation = AsyncOperationManager.CreateOperation(null);
            this.asnycOnGetLyricsComplete = new SendOrPostCallback(OnGetLyricsCompleteOperation);
            this.GetLyricsComplete += new GetLyricsCompleteEventHandler(gl_GetLyricsComplete);
        }
        public frmLyricsSearch(string MusicPath, bool ShowTime)
        {
            InitializeComponent();
            radioButton1.Enabled = true; radioButton1.Checked = true;
            this.MusicPath = MusicPath; this.ShowTime = ShowTime;
            this.asyncOperation = AsyncOperationManager.CreateOperation(null);
            this.asnycOnGetLyricsComplete = new SendOrPostCallback(OnGetLyricsCompleteOperation);
            this.GetLyricsComplete += new GetLyricsCompleteEventHandler(gl_GetLyricsComplete);
            toolStripStatusLabel1.Text = "싱크 가사 찾는 중...";
            textBox1.Text = Path.GetFileNameWithoutExtension(MusicPath);
            panel1.Enabled = false;
            FindLyrics(MusicPath, ShowTime);
        }
        public frmLyricsSearch(string MusicPath)
        {
            InitializeComponent();
            radioButton1.Enabled = true; radioButton1.Checked = true;
            this.MusicPath = MusicPath;
            this.asyncOperation = AsyncOperationManager.CreateOperation(null);
            this.asnycOnGetLyricsComplete = new SendOrPostCallback(OnGetLyricsCompleteOperation);
            this.GetLyricsComplete += new GetLyricsCompleteEventHandler(gl_GetLyricsComplete);
            toolStripStatusLabel1.Text = "싱크 가사 찾는 중...";
            textBox1.Text = Path.GetFileNameWithoutExtension(MusicPath);
            panel1.Enabled = false;
            FindLyrics(MusicPath, true);
        }
        public frmLyricsSearch(LyricsInfo LyricsAssemble)
        {
            InitializeComponent();
            radioButton1.Enabled = false; radioButton2.Checked = true;
            this.LyricsAssembly = LyricsAssemble;
            this.asyncOperation = AsyncOperationManager.CreateOperation(null);
            this.asnycOnGetLyricsComplete = new SendOrPostCallback(OnGetLyricsCompleteOperation);
            this.GetLyricsComplete += new GetLyricsCompleteEventHandler(gl_GetLyricsComplete);
            toolStripStatusLabel1.Text = "싱크 가사 찾는 중...";
            panel1.Enabled = false;
            FindLyrics(MusicPath, ShowTime);
        }
        public frmLyricsSearch(string Song, string Artist)
        {
            InitializeComponent();
            radioButton1.Enabled = false; radioButton2.Checked = true;
            this.LyricsAssembly = new LyricsInfo(Song, Artist);
            this.asyncOperation = AsyncOperationManager.CreateOperation(null);
            this.asnycOnGetLyricsComplete = new SendOrPostCallback(OnGetLyricsCompleteOperation);
            this.GetLyricsComplete += new GetLyricsCompleteEventHandler(gl_GetLyricsComplete);
            toolStripStatusLabel1.Text = "싱크 가사 찾는 중...";
            panel1.Enabled = false;
            FindLyrics(MusicPath, ShowTime);
        }
        public frmLyricsSearch(string Song, string Artist, bool ShowTime)
        {
            InitializeComponent();
            radioButton1.Enabled = false; radioButton2.Checked = true;
            this.LyricsAssembly = new LyricsInfo(Song, Artist); this.ShowTime = ShowTime;
            this.asyncOperation = AsyncOperationManager.CreateOperation(null);
            this.asnycOnGetLyricsComplete = new SendOrPostCallback(OnGetLyricsCompleteOperation);
            this.GetLyricsComplete += new GetLyricsCompleteEventHandler(gl_GetLyricsComplete);
            toolStripStatusLabel1.Text = "싱크 가사 찾는 중...";
            panel1.Enabled = false;
            FindLyrics(MusicPath, ShowTime);
        }
        public frmLyricsSearch(LyricsInfo LyricsAssemble, bool ShowTime)
        {
            InitializeComponent();
            radioButton1.Enabled = false; radioButton2.Checked = true;
            this.LyricsAssembly = LyricsAssemble; this.ShowTime = ShowTime;
            //ts[0] = new ParameterizedThreadStart(GetLyrics);
            //th[0] = new Thread(ts[0]); th[0].IsBackground = true;
            this.asyncOperation = AsyncOperationManager.CreateOperation(null);
            this.asnycOnGetLyricsComplete = new SendOrPostCallback(OnGetLyricsCompleteOperation);
            this.GetLyricsComplete += new GetLyricsCompleteEventHandler(gl_GetLyricsComplete);
            toolStripStatusLabel1.Text = "싱크 가사 찾는 중...";
            panel1.Enabled = false;
            FindLyrics(MusicPath, ShowTime);
        }

        frmMain Form1Class;
        internal void SetForm1Class(frmMain Form1)
        { Form1Class = Form1; if (Form1 != null) button2.Visible = true; else button2.Enabled = false; }

        string _MusicPath;
        public string MusicPath
        {
            get { return _MusicPath; }
            set { _MusicPath = value; this.Text = "싱크 가사 찾기 - [" + System.IO.Path.GetFileName(value) + "]"; 
            radioButton1.Enabled=true;FindLyrics(value);}
        }
        /*
        public frmLyricsMain(string MusicPath)
        {
            InitializeComponent();
            this.MusicPath = MusicPath;
            ts[0] = new ParameterizedThreadStart(GetLyrics);
            th[0] = new Thread(ts[0]); th[0].IsBackground = true;
            this.GetLyricsComplete += new GetLyricsCompleteEventHandler(gl_GetLyricsComplete);
            th[0].Start(true);
            //lc = GetLyrics.GetLyricByMusic(MusicPath, true);
            if (lc != null) { try { this.comboBox1.Items.AddRange(lc.Maker); this.textBox3.Text = lc.Lyric[0]; } catch { } }
            radioButton1.Enabled=radioButton1.Checked = true;
            radioButton2.Enabled = true;
            textBox2.Text = Path.GetFileNameWithoutExtension(MusicPath);
        }*/

        void gl_GetLyricsProgress(int Timeout)
        {

        }

        private void Lyrics_Load(object sender, EventArgs e)
        {
            
        }
        LyricsClass lc;
        //GetLyrics gl = new GetLyrics();
        private void button1_Click(object sender, EventArgs e)
        {
            Flush();
            toolStripStatusLabel1.Text = "싱크 가사 찾는 중...";
            panel1.Enabled = false;
            if (radioButton2.Checked)
            {
                try { FindLyrics(textBox1.Text,textBox2.Text, ShowTime); }// toolStripStatusLabel1.Text = string.Format("싱크 가사 {0}개를 찾았습니다!", lc.Maker.Length);
                catch (LyricsException le) { toolStripStatusLabel1.Text = le.Message; }
            }
            else 
            {
                if (MusicPath != null) { FindLyrics(MusicPath, ShowTime); }
            }
            button1.Text = "다시 찾기";
            toolTip1.SetToolTip(this.button1, "가사가 나오지 않는 경우 가사를 다시 찾습니다.");
        }
        

        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {
            groupBox1.Enabled = radioButton2.Checked;
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lc != null && lc.Maker[0] != null)
                if (checkBox1.Checked) { try { this.txtLyric.Text = Lyric.Lyric[comboBox1.SelectedIndex]; } 
                catch(IndexOutOfRangeException) { comboBox1.Items.RemoveAt(comboBox1.SelectedIndex);comboBox1.SelectedItem=comboBox1.Items[0]; } }
                else this.txtLyric.Text = Lyric_Trim.Lyric[comboBox1.SelectedIndex];

        }

        private void 다른이름으로가사저장SToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if(MusicPath==null||MusicPath=="")saveFileDialog1.FileName = comboBox1.Text;
            else saveFileDialog1.FileName = Path.GetFileNameWithoutExtension(MusicPath)+" "+comboBox1.Text.Substring(2);
            saveFileDialog1.ShowDialog();
        }

        private void saveFileDialog1_FileOk(object sender, CancelEventArgs e)
        {
            File.WriteAllLines(saveFileDialog1.FileName, txtLyric.Lines);
        }
        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            int i = comboBox1.SelectedIndex;
            comboBox1.Items.Clear();
            try
            {
                button2.Enabled = checkBox1.Checked;
                if (checkBox1.Checked)
                {
                    comboBox1.Items.AddRange(lc.Maker);
                    comboBox1.Text = comboBox1.Items[i].ToString();
                    txtLyric.Text = lc.Lyric[i];
                    saveFileDialog1.Filter = "가사 파일(*.lrc)|*.lrc|모든 파일(*.*)|*.*";
                    saveFileDialog1.DefaultExt = ".lrc";
                }
                else
                {
                    if (Lyric_Trim==null) Lyric_Trim = LyricsTrimThread(Lyric);
                    comboBox1.Items.AddRange(Lyric_Trim.Maker);
                    comboBox1.Text = comboBox1.Items[i].ToString();
                    txtLyric.Text = Lyric_Trim.Lyric[i];
                    saveFileDialog1.Filter = "텍스트 파일(*.txt)|*.txt|모든 파일(*.*)|*.*";
                    saveFileDialog1.DefaultExt = ".txt";
                }
            }
            catch { }
        }

        /*
        delegate void Panel1EnableCallBack(bool Enable);
        void Panel1SetEnable(bool Enable)
        {
            if (this.panel1.InvokeRequired)
            {
                Panel1EnableCallBack sec = new Panel1EnableCallBack(Panel1SetEnable);
                panel1.Invoke(sec, Enable);
            }
            else { panel1.Enabled = Enable; }
        }*/

        LyricsClass Lyric, Lyric_Trim;
        void gl_GetLyricsComplete(LyricsClass Lyrics)
        {
            lc = Lyrics;
            try
            {
                if (lc.Maker[0] == "1 (by )" && lc.Lyric[0] == "") { lc.Maker[0] = ""; throw new LyricsException(); }
                toolStripStatusLabel1.Text = string.Format("싱크 가사 {0}개를 찾았습니다!", lc.Maker.LongLength); 
                if (lc != null)
                {
                    try
                    {
                        Lyric = lc;
                        if (checkBox1.Checked)
                        {
                            //#if DEBUG
                            //this.comboBox1.InvokeIfNeeded(() => comboBox1.Items.AddRange(Lyric.Maker));
                            //this.comboBox1.InvokeIfNeeded(() => comboBox1.Text = comboBox1.Items[0].ToString());
                            //this.txtLyric.InvokeIfNeeded(() => txtLyric.Text = Lyric.Lyric[0]);
                            //#else
                            comboBox1.Items.AddRange(Lyric.Maker);
                            comboBox1.Text = comboBox1.Items[0].ToString();
                            txtLyric.Text = Lyric.Lyric[0];
                            //#endif
                        }
                        else
                        {
                            Lyric_Trim = LyricsTrimThread(Lyric);
                            //#if DEBUG
                            //this.comboBox1.InvokeIfNeeded(() => comboBox1.Items.AddRange(Lyric_Trim.Maker));
                            //this.comboBox1.InvokeIfNeeded(() => comboBox1.Text = comboBox1.Items[0].ToString());
                            //this.txtLyric.InvokeIfNeeded(() => txtLyric.Text = Lyric_Trim.Lyric[0]);
                            //#else
                            comboBox1.Items.AddRange(Lyric_Trim.Maker);
                            comboBox1.Text = comboBox1.Items[0].ToString();
                            txtLyric.Text = Lyric_Trim.Lyric[0];
                            //#endif
                        }

                        //ts[1] = new ParameterizedThreadStart(LyricsTrimThread);
                        //th[1] = new Thread(ts[1]); th[1].IsBackground = true;
                        //th[1].Start(Lyric);
                    }
                    catch { }
                }
            }
            catch { toolStripStatusLabel1.Text = "싱크 가사를 찾을 수 없습니다!"; }
            //Panel1SetEnable(true);
            panel1.Enabled = true;
        }

        public void FindLyrics(LyricsInfo LyricsAssembly)
        {
            this.LyricsAssembly = LyricsAssembly;
            Common(false);
        }
        public void FindLyrics(LyricsInfo LyricsAssembly, bool ShowTime)
        {
            this.LyricsAssembly = LyricsAssembly;
            Common(false);
        }
        public void FindLyrics(string Song, string Artist)
        {
            this.LyricsAssembly = new LyricsInfo(Song, Artist);
            Common(false);
        }
        public void FindLyrics(string Song, string Artist, bool ShowTime)
        {
            this.LyricsAssembly = new LyricsInfo(Song, Artist);
            this.ShowTime = ShowTime;
            Common(false);
        }
        public string MD5Hash;
        public void FindLyrics(string MusicPath)
        {
            this._MusicPath = MusicPath;
            Common(true);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="MusicPath"></param>
        /// <param name="MD5Hash"></param>
        /// <param name="ThisIsDummy_ConstantTheNULL">쓰지 않습니다. 위의 매개변수 형식과 충돌을 막고자 부득이하게 넣은것 입니다. (넣어도 아무일 안일어납니다.)</param>
        public void FindLyrics(string MusicPath, string MD5Hash, string ThisIsDummy_ConstantTheNULL)
        {
            this._MusicPath = MusicPath;
            this.MD5Hash = MD5Hash; MD5HashLock = true;
            Common(true);
        }
        public void FindLyrics(string MusicPath, bool ShowTime)
        {
            this._MusicPath = MusicPath;
            this.ShowTime = ShowTime;
            Common(true);
        }
        public void FindLyrics(string MusicPath, bool ShowTime, string MD5Hash)
        {
            this._MusicPath = MusicPath;
            this.ShowTime = ShowTime;
            this.MD5Hash = MD5Hash; MD5HashLock = true;
            Common(true);
        }
        void Common(bool IsMusicPath)
        {
            //ts[0] = new ParameterizedThreadStart(GetLyrics);
            //th[0] = new Thread(ts[0]); //th[0].IsBackground = true;
            Lyric_Trim = null;
            ThreadPool.QueueUserWorkItem(new WaitCallback(GetLyrics), IsMusicPath);
            //th[0].Start(IsMusicPath);
        }

        public void Flush()
        {
            comboBox1.Items.Clear();
            txtLyric.Text = "";
        }
        #region 쓰레드 이벤트


        bool MD5HashLock;
        LyricsInfo LyricsAssembly;
        int Timeout = 1000;
        void GetLyrics(object IsMusic)
        {
            timer1.Start();
            if ((bool)IsMusic)
            {
                if (!MD5HashLock) MD5Hash = HS_Audio.Lyrics.GetLyrics.GenerateMusicHash(MusicPath);
                HS_Audio.Lyrics.GetLyrics lc = new HS_Audio.Lyrics.GetLyrics();
                lc.GetLyricsFromFile(MusicPath, ShowTime);
                try { asyncOperation.PostOperationCompleted(asnycOnGetLyricsComplete,new LyricsClass(lc.MakerLists, lc.LyricLists, MD5Hash)); }
                    //OnGetLyricsComplete
                finally { /*th[0].Abort();*/ }//th[0].Suspend(); }
                MD5HashLock = false;
            }
            else
            {
                HS_Audio.Lyrics.GetLyrics lc = new HS_Audio.Lyrics.GetLyrics();
                lc.GetLyricsFromName(LyricsAssembly.Artist, LyricsAssembly.Song, ShowTime);
                try { asyncOperation.PostOperationCompleted(asnycOnGetLyricsComplete, new LyricsClass(lc.MakerLists, lc.LyricLists, MD5Hash)); }
                finally {/* th[0].Abort();*/ }//th[0].Suspend(); }
            }
            //th[0].Abort();
        }


        //protected virtual OnGetLyricsComplete(GetLyricsCompleteEventHandler
        LyricsClass LyricsTrimThread(LyricsClass a)
        {
            LyricsClass b = a;
            LyricsEz[] lcs = LyricsEz.ConvertLyricsClassToLyricsArray(b);
            for (long i = 0; i < lcs.LongLength;i++ )
            { lcs[i] = TrimTimeLyrics(lcs[i]); }
            try { b = LyricsClass.ConvertLyricsArrayToLyricsClass(lcs); }
            finally { /*th[1].Abort();*/ }
            return b;
        }
        #endregion
        public LyricsEz TrimTimeLyrics(LyricsEz Lyric)
        {
            LyricsEz lrc = new LyricsEz(Lyric.Maker, Lyric.Lyric);
            string[] li = HS_CSharpUtility.Utility.StringUtility.ConvertStringToArray(Lyric.Lyric, "\r\n");
            System.Text.StringBuilder sb = new System.Text.StringBuilder(li[0].Substring(li[0].IndexOf("]") + 1));
            for (long i = 1; i < li.LongLength; i++)
            {
                try { li[i] = li[i].Substring(li[i].IndexOf("]") + 1); }
                catch { }finally { sb.Append("\r\n" + li[i]); }
            }
            lrc.Lyric=sb.ToString();
            return lrc;
        }
        public string TrimTimeforLyricString(string Lyric)
        {
            string[] li = HS_CSharpUtility.Utility.StringUtility.ConvertStringToArray(Lyric, "\r\n");
            System.Text.StringBuilder sb = new System.Text.StringBuilder(li[0].Substring(li[0].IndexOf("]") + 1));
            for (long i = 1; i < li.LongLength; i++)
            {
                try { li[i]=li[i].Substring(li[i].IndexOf("]") + 1); }
                catch { sb.Append("\r\n"+li[i]); }
            }
            return sb.ToString();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (Lyric==null||Lyric.Maker.Length == 0)
            {
                if (MessageBox.Show("빈 가사 입니다.\n정말 현재 싱크 가사창에 연결 하시겠습니까?", "빈 싱크가사 연결", 
                    MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == System.Windows.Forms.DialogResult.Yes)
                { Form1Class.frmlrcSearch_GetLyricsComplete(new LyricsClass(new string[0], new string[0])); }
            }
            else { //Form1Class.frmlrcSearch_GetLyricsComplete(new LyricsClass(new string[] { Lyric.Maker[comboBox1.SelectedIndex] }, new string[] { Lyric.Lyric[comboBox1.SelectedIndex] })); 
                   Form1Class.frmlrcSearch_GetLyricsComplete(new LyricsEz(Lyric.Maker[comboBox1.SelectedIndex], Lyric.Lyric[comboBox1.SelectedIndex]));}
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            button1.Text = "찾기";
        }

        private void button1_TextChanged(object sender, EventArgs e)
        {
            if (radioButton2.Checked)
            {
                if (button1.Text == "찾기")
                { toolTip1.SetToolTip(this.button1, "가수와 제목명으로 가사를 찾습니다."); }
                else if (button1.Text == "다시 찾기")
                { toolTip1.SetToolTip(this.button1, "가수와 제목명으로 가사를 다시 찾습니다."); }
                else
                    toolTip1.SetToolTip(this.button1, "버튼의 리소스값이나 문자열을 수정하지 마십시오!\n불법입니다.");
            }
            else
            {
                if (button1.Text == "찾기")
                { toolTip1.SetToolTip(this.button1, "음악 파일의 고유정보로 가사를 찾습니다."); }
                else if (button1.Text == "다시 찾기")
                { toolTip1.SetToolTip(this.button1, "음악 파일의 고유정보로 가사를 다시 찾습니다."); }
                else
                    toolTip1.SetToolTip(this.button1, "버튼의 리소스값이나 문자열을 수정하지 마십시오!\n불법입니다.");
            }
        }

        private void textBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13) { button1_Click(null, null); }
        }
    }
}
