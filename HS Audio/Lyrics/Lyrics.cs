﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;
using System.Xml;

namespace HS_Audio.Lyrics
{
    /// <summary>
    /// 싱크가사 추상클래스 입니다.
    /// </summary>
    [Serializable]
    public abstract class Lyric
    {
        public string Artist { get; set; }
        /// <summary>
        /// 앨범 입니다.
        /// </summary>
        public string Album { get; set; }
        /// <summary>
        /// 가사 등록자 입니다.
        /// </summary>
        public string Author { get; set; }
        /// <summary>
        /// 가사 타이틀(제목) 입니다.
        /// </summary>
        public string Title { get; set; }
        /// <summary>
        /// 가사 수정자 입니다.
        /// </summary>
        public string LyricsMaker { get; set; }
        /// <summary>
        /// 노래 길이 입니다.
        /// </summary>
        public string Length { get; set; }
        /// <summary>
        /// 가사 오프셋 입니다. (단위는 밀리세컨드(ms) 입니다.) [※메모: 이 값이 음수면 기존보다 빨리 나타나고 양수면 늦게 나타납니다.]
        /// </summary>
        public int Offset { get; set; }
        /// <summary>
        /// 가사에대한 코멘트(메모)입니다.
        /// </summary>
        public string Comment { get; set; }
        /// <summary>
        /// 순수한 싱크가사만 들어있습니다.
        /// </summary>
        public string PureLyric { get; set; }
        /// <summary>
        /// 현재 인스턴스가 비었는지 여부를 가져옵니다.
        /// </summary>
        public bool IsEmpty
        {
            get
            {
                if ((Artist == null||Artist=="") && (Album == null||Album == "") && (Author == null||Author == "") && (Title == null||Title == "") &&
                    (LyricsMaker==null||LyricsMaker=="")&&(Length == null||Length == "") && (Offset == 0) && (Comment == null||Comment == "")&&(PureLyric==null||PureLyric=="")) return true;
                else return false;
            }
        }
        /// <summary>
        /// 인스턴스의 내용을 초기화 합니다.
        /// </summary>
        public void Clear() {Artist = Album = Author = Title = LyricsMaker = Length = PureLyric = null; Offset = 0;}
        /// <summary>
        /// 현재 인스턴스의 내용을 표준 싱크가사 포맷으로 반환합니다.
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            if (Artist != null) sb.AppendFormat("[ar:{0}]\r\n", Artist);
            if (Album != null) sb.AppendFormat("[al:{0}]\r\n", Album);
            if (Author != null) sb.AppendFormat("[au:{0}]\r\n", Author);
            if (Title != null) sb.AppendFormat("[ti:{0}]\r\n", Title);
            if (LyricsMaker != null) sb.AppendFormat("[by:{0}]\r\n", LyricsMaker);
            if (Length != null) sb.AppendFormat("[length:{0}]\r\n", Length);
            if (Offset != 0) sb.AppendFormat("[offset:{0}]\r\n", Offset);
            if (Comment != null) sb.AppendFormat("[comment:{0}]\r\n", Comment);
            if (PureLyric != null) sb.Append(PureLyric);
            return sb.ToString();
        }
    }

    ///<summary>
    /// 싱크가사 메인 클래스 입니다.
    /// </summary>
    [Serializable]
    public class LyricClass : Lyric
    {
        internal HS_Audio.Lyrics.LyricParser lrcParser;
        /*
        public string Artist{get{return base.Artist;}set{base.Artist=value;}}
        public string Album{get{return base.Album;}set{base.Album=value;}}
        public string Author{get{return base.Author;}set{base.Author=value;}}
        public string Title{get{return base.Title;}set{base.Title=value;}}
        public string LyricsMaker{get{return base.LyricsMaker;}set{base.LyricsMaker=value;}}
        public string Length{get{return base.Length;}set{base.Length=value;}}
        public int Offset{get{return base.Offset;}set{base.Offset=value;}}
        public string Comment{get{return base.Comment;}set{base.Comment=value;}}
        public string PureLyric{get{return base.PureLyric;}set{base.PureLyric=value;}}*/

        //System.Collections.Hashtable lyricsDictionary;
        Dictionary<object, object> lyricsDictionary;

        public LyricClass(){}
        public LyricClass(string Artist, string Album, string Author, string Title, string LyricsMaker,
            string Length, int Offset, string Comment, string PureLyrics)
        {
            this.Album = Album; this.Title = Title; this.Artist = Artist; this.Author = Author;
            this.LyricsMaker = LyricsMaker; this.Length = Length; this.Offset = Offset; this.Comment = Comment;
            this.lrcParser = null; this.PureLyric = PureLyrics;lyricsDictionary = new Dictionary<object, object>();
            //lyricsDictionary.Add();
        }
        public LyricClass(string Lyric)
        {
            HS_Audio.Lyrics.LyricParser lrcParser = new LyricParser(Lyric);
            this.lrcParser = lrcParser; this.LyricsMaker = lrcParser.LyricsMaker;
            this.Album = lrcParser.Album; this.Artist = lrcParser.Artist; this.Author = lrcParser.Author; this.Comment = lrcParser.Comment;
            this.Length = lrcParser.Length; this.Offset = lrcParser.Offset; this.PureLyric = lrcParser.PurelrcCode; this.Title = lrcParser.Title;
            lyricsDictionary = new Dictionary<object, object>();
            /*
            #region
            String lrct = Lyrics.Replace("\r", "\n");
            Hashtable md = new Hashtable();
            String aline;
            List<String> av = new List<string>(lrct.Split('\n'));
            string a, b;
            int ab = av.Count;
            //for (int j = 0; j < ab; j++) { try { if (av[j == 0 ? 0 : j] == "[00:00.00]" || av[j == 0 ? 0 : j] == "") { av.RemoveAt(j); } } catch { if (j >= av.Count) { break; } } }
            int i;
            StringBuilder sb = new StringBuilder();
            for (i = 0; i < av.Count; i++)
            {
                if (av[i] != "" || av[i] != "[00:00.00]")
                {
                    aline = av[i].Substring(av[i].IndexOf("[") + 1);
                    if (aline.IndexOf("]") != -1)
                    {
                        a = aline.Substring(av[i].IndexOf("[")+1); 
                        b = a.Remove(av[i].IndexOf("]") == -1 ? 0 : av[i].IndexOf("]") - 1);
                        if (true)//aline.Split(']').GetLength(0) == 2
                        {
                            if (aline.IndexOf("ti:") != -1
                            || aline.IndexOf("ar:") != -1
                            || aline.IndexOf("al:") != -1
                            || aline.IndexOf("by:") != -1
                            || aline.IndexOf("au:") != -1
                            || aline.IndexOf("offset:") != -1
                            || aline.IndexOf("length:") != -1
                            || aline.IndexOf("comment:") != -1)
                            {
                                //aline = aline.Replace("]", "");
                                md[b.Split(':').GetValue(0)] = b.Split(':').GetValue(1);
                            }
                            else
                            {
                                //md[aline.Split(']').GetValue(0)] = aline.Split(']').GetValue(1);
                                try { md.Add(a, b); }//aline.Split(']').GetValue(0), aline.Split(']').GetValue(1)
                                catch (ArgumentException)
                                {
                                    //md[aline.Split(']').GetValue(0)]= md[aline.Split(']').GetValue(0)]+"\r\n"+aline.Split(']').GetValue(1);
                                    md[a] = md[a] + "\r\n" + b;
                                }
                                sb.AppendLine(a + b);
                            }
                        }
                        else
                        {
                            int subi;
                            for (subi = 0; subi < aline.Split(']').GetLength(0); subi++)
                            {
                                if (subi < aline.Split(']').GetLength(0) - 1)
                                    md[aline.Split(']').GetValue(subi)] = aline.Split(']').GetValue(aline.Split(']').GetLength(0) - 1);
                            }
                        }
                    }
                }
            }
            PureLyrics = sb.ToString();
            #endregion
            Album = lrcParser.Album; Title = lrcParser.Title; Artist = lrcParser.Artist; Author = lrcParser.Author;
            LyricsMaker = lrcParser.LyricsMaker; Length = lrcParser.Length; Offset = lrcParser.Offset; Comment = lrcParser.Comment;
            lyricsDictionary = new Hashtable();*/
        }
        public LyricClass(string Lyric, string LyricsMaker)
        {
            HS_Audio.Lyrics.LyricParser lrcParser = new LyricParser(Lyric); this.lrcParser = lrcParser;
            #region
            /*
            String lrct = Lyrics.Replace("\r", "\n");
            Hashtable md = new Hashtable();
            String aline;
            List<String> av = new List<string>(lrct.Split('\n'));
            string a, b;
            int ab = av.Count;
            //for (int j = 0; j < ab; j++) { try { if (av[j == 0 ? 0 : j] == "[00:00.00]" || av[j == 0 ? 0 : j] == "") { av.RemoveAt(j); } } catch { if (j >= av.Count) { break; } } }
            int i;
            StringBuilder sb = new StringBuilder();
            for (i = 0; i < av.Count; i++)
            {
                if (av[i] != "" || av[i] != "[00:00.00]")
                {
                    aline = av[i].Substring(av[i].IndexOf("[") + 1);
                    if (aline.IndexOf("]") != -1)
                    {
                        a = aline.Remove(av[i].IndexOf("]") == 0 ? 0 : av[i].IndexOf("]") - 1);
                        b = aline.Substring(av[i].IndexOf("]"));
                        if (true)//aline.Split(']').GetLength(0) == 2
                        {
                            if (aline.IndexOf("ti:") != -1
                            || aline.IndexOf("ar:") != -1
                            || aline.IndexOf("al:") != -1
                            || aline.IndexOf("by:") != -1
                            || aline.IndexOf("au:") != -1
                            || aline.IndexOf("offset:") != -1
                            || aline.IndexOf("length:") != -1
                            || aline.IndexOf("comment:") != -1)
                            {
                                //aline = aline.Replace("]", "");
                                md[a.Split(':').GetValue(0)] = a.Split(':').GetValue(1);
                            }
                            else
                            {
                                //md[aline.Split(']').GetValue(0)] = aline.Saplit(']').GetValue(1);
                                try { md.Add(a, b); }//aline.Split(']').GetValue(0), aline.Split(']').GetValue(1)
                                catch (ArgumentException)
                                {
                                    //md[aline.Split(']').GetValue(0)]= md[aline.Split(']').GetValue(0)]+"\r\n"+aline.Split(']').GetValue(1);
                                    md[a] = md[a] + "\r\n" + b;
                                }
                                sb.AppendLine("["+a+"]"+ b);
                            }
                        }
                        else
                        {
                            int subi;
                            for (subi = 0; subi < aline.Split(']').GetLength(0); subi++)
                            {
                                if (subi < aline.Split(']').GetLength(0) - 1)
                                    md[aline.Split(']').GetValue(subi)] = aline.Split(']').GetValue(aline.Split(']').GetLength(0) - 1);
                            }
                        }
                    }
                }
            }
            PureLyrics = sb.ToString();
            */
            #endregion
            PureLyric = lrcParser.PurelrcCode;
            Album = lrcParser.Album; Title = lrcParser.Title; Artist = lrcParser.Artist; Author = lrcParser.Author;
            if (LyricsMaker != null || LyricsMaker != "") { this.LyricsMaker = LyricsMaker; } else { this.LyricsMaker = lrcParser.LyricsMaker; } 
            Length = lrcParser.Length; Offset = lrcParser.Offset; Comment = lrcParser.Comment;
            lyricsDictionary = new Dictionary<object, object>();
        }
        public LyricClass(LyricParser lrcParser)
        {
            this.lrcParser = lrcParser;
            Album = lrcParser.Album; Title = lrcParser.Title; Artist = lrcParser.Artist; Author = lrcParser.Author; LyricsMaker = lrcParser.LyricsMaker;
            Length = lrcParser.Length; Offset = lrcParser.Offset; Comment = lrcParser.Comment; PureLyric = lrcParser.PurelrcCode;
            lyricsDictionary = lrcParser.LyricsDictionary;
        }


        public void SetLyrics(LyricParser lrcParser)
        {
            this.lrcParser = lrcParser;
            Album = lrcParser.Album; Title = lrcParser.Title; Artist = lrcParser.Artist; Author = lrcParser.Author; LyricsMaker = lrcParser.LyricsMaker;
            Length = lrcParser.Length; Offset = lrcParser.Offset; Comment = lrcParser.Comment; PureLyric = lrcParser.PurelrcCode;
            lyricsDictionary = lrcParser.LyricsDictionary;
        }

        public void Parse(string Lyrics)
        {
            HS_Audio.Lyrics.LyricParser lrcParser = new LyricParser(Lyrics);
            this.lrcParser = lrcParser; this.LyricsMaker = lrcParser.LyricsMaker;
            this.Album = lrcParser.Album; this.Artist = lrcParser.Artist; this.Author = lrcParser.Author; this.Comment = lrcParser.Comment;
            this.Length = lrcParser.Length; this.Offset = lrcParser.Offset; this.PureLyric = lrcParser.PurelrcCode; this.Title = lrcParser.Title;
        }

        public void SetLength(string Length) { this.Length = Length; }
        public void SetOffset(int Offset) { this.Offset = Offset; }
        public void SetTitle(string Title) { this.Title = Title; }
        public void SetAlbum(string Album) { this.Album = Album; }
        public void SetArtist(string Artist) { this.Artist = Artist; }
        public void SetAuthor(string Author) { this.Author = Author; }
        public void SetComment(string Comment) { this.Comment = Comment; }
        public void SetLyricsMaker(string LyricsMaker) { this.LyricsMaker = LyricsMaker; }
        public void SetPureLyric(string PureLyrics) { this.PureLyric = PureLyrics; }

        public override string ToString(){return base.ToString();}
    }

    /// <summary>
    /// 알송 싱크가사 클래스 입니다.
    /// </summary>
    [Serializable]
    public abstract class LyricAlsong
    {
        internal XmlDocument OriginXML;
        public LyricAlsong(string MD5Hash = null) { this.MD5Hash = MD5Hash; }
        public LyricAlsong(string OriginXML, string MD5Hash = null)
        {
            GetOriginText = OriginXML;
            this.MD5Hash = MD5Hash;
            //this.OriginXML = new XmlDocument();
            //this.OriginXML.LoadXml(OriginXML);
        }
        public LyricAlsong(XmlDocument OriginXML, string MD5Hash = null)
        {
            GetOriginText = OriginXML.InnerXml;
            this.MD5Hash = MD5Hash;
            this.OriginXML = new XmlDocument();
            this.OriginXML.LoadXml(OriginXML.InnerXml);
        }
        /// <summary>
        /// 원본가사 문자열을 가져옵니다.
        /// </summary>
        public string GetOriginText { get; protected set; }
        public string GetMaker  { get {return string.Format("{0}{1}", (FirstName==null||FirstName=="")?"":FirstName+" 님 등록", (Name == null||Name == "") ? "" : ", "+Name + " 님 수정"); } }

        /// <summary>
        /// 현재 가사 정보를 새로운 Lyrics 인스턴스로 반환합니다.
        /// </summary>
        public LyricClass GetLyrics { 
            get{StringBuilder sb = new StringBuilder();
                if((FirstComment != null && FirstComment != "") && (Comment != null && Comment != "")) sb.AppendFormat("등록자 코멘트: {0} , 수정자 코멘트: {1}", FirstComment, Comment);
                else if (FirstComment != null && FirstComment != "") sb.Append(FirstComment);
                else if (Comment != null && Comment != "") sb.Append(Comment);
                return new LyricClass(Artist, Album, FirstName, Title, Name, null, 0, sb.ToString(), Lyric); } }

        public string MD5Hash;
        public long StatusID;//<strStatusID>...</strStatusID>
        public long InfoID;//<strInfoID>...</strInfoID>
        public string Date;//<strRegistDate>...</strRegistDate>
        public string Title;//<strTitle>...</strTitle>
        //MD5일때 <strArtist>...</strArtist>
        //검색일때 <strArtistName>...</strArtistName>
        public string Artist;
        //MD5일때 <strAlbum>...</strAlbum>
        //검색일때 <strAlbumName>...</strAlbumName>
        public string Album;
        public string Lyric;//<strLyric>...</strLyric>
        public string FirstName;//<strRegisterFirstName>...</strRegisterFirstName> //xxx님 등록
        public string FirstEMail;//<strRegisterFirstEMail>...</strRegisterFirstEMail>
        public string FirstURL;//<strRegisterFirstURL>...</strRegisterFirstURL>
        public string FirstPhone;//<strRegisterFirstPhone />
        public string FirstComment;//<strRegisterFirstComment>...</strRegisterFirstComment>
        public string Name;//<strRegisterName>...</strRegisterName> //xxx님 수정
        public string EMail;//<strRegisterEMail>...</strRegisterEMail>
        public string URL;//<strRegisterURL />
        public string Phone;//<strRegisterPhone>...</strRegisterPhone>
        public string Comment;//<strRegisterComment>...</strRegisterComment>
        [Obsolete]
        public string Length;

        internal protected bool IsMD5Get{get; set;}

        /// <summary>
        /// 현재 가사 정보를 싱크가사 포맷에 맞게 재구성한 문자열 입니다.
        /// </summary>
        /// <returns></returns>
        public string GetLyric()
        {
            //Dictionary<string, string> dic = new Dictionary<string, string>();
            StringBuilder sb = new StringBuilder();
            sb.AppendFormat("[ti:{0}]\r\n", Title);
            sb.AppendFormat("[ar:{0}]\r\n", Artist);
            sb.AppendFormat("[al:{0}]\r\n", Album);
            sb.AppendFormat("[au:{0}]\r\n", FirstName);
            sb.AppendFormat("[by:{0}]\r\n", Name);
            sb.AppendFormat("[length:{0}]\r\n",Length);
            if((FirstComment!=null&&FirstComment!="")&&(Comment!=null&&Comment!=""))sb.AppendFormat("[comment:등록자 코멘트: {0} , 수정자 코멘트: {1}]\r\n", FirstComment.Replace("\r\n", " <br> "),Comment.Replace("\r\n", " <br> "));
            else if(FirstComment!=null&&FirstComment!="") sb.AppendFormat("[comment:{0}]\r\n", FirstComment.Replace("\r\n", "&lt;br&gt;"));
            else if(Comment!=null&&Comment!="")sb.AppendFormat("[comment:{0}]\r\n", Comment.Replace("\r\n", "&lt;br&gt;"));
            sb.Append(Lyric.Replace("&lt;br&gt;", "\r\n"));
            return sb.ToString();
        }
        /// <summary>
        /// 현재 정보를 XML형태로 바꿔서 반환합니다.
        /// </summary>
        /// <returns></returns>
        public override string ToString() { return ToString(false); }
        /// <summary>
        /// 현재 정보를 XML형태로 바꿔서 반환합니다.
        /// </summary>
        /// <param name="NewLine">줄바꿈을 할지의 여부 입니다.</param>
        /// <returns></returns>
        public string ToString(bool NewLine = false)
        {
            StringBuilder sb = new StringBuilder();
            if (!NewLine)
            {
                sb.Append("<?xml version=\"1.0\" encoding=\"utf-8\"?><soap:Envelope xmlns:soap=\"http://www.w3.org/2003/05/soap-envelope\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\"><soap:Body>");
                sb.AppendFormat("<strStatusID>{0}</strStatusID>", StatusID);
                sb.AppendFormat("<strInfoID>{0}</strInfoID>", InfoID);
                if (Date == null){}else if(Date == "")sb.Append("<strRegistDate />");else sb.AppendFormat("<strRegistDate>{0}</strRegistDate>", Date);
                if (Title == null) {}else if(Title == "")sb.Append("<strTitle />"); else sb.AppendFormat("<strTitle>{0}</strTitle>", Title);
                if (Artist == null){}else if(Artist == "")sb.Append(IsMD5Get?"<strArtist />":"<strArtistName />");  else sb.AppendFormat(IsMD5Get?"<strArtist>{0}</strArtist>":"<strArtistName>{0}</strArtistName>", Artist);
                if (Album == null){}else if(Album == "") sb.Append(IsMD5Get?"<strAlbum />":"<strAlbumName />"); else sb.AppendFormat(IsMD5Get?"<strAlbum>{0}</strAlbum>":"<strAlbumName>{0}</strAlbumName>", Album);
                if (Lyric == null){}else if(Lyric == "") sb.Append("<strLyric />"); else sb.AppendFormat("<strLyric>{0}</strLyric>", Lyric);
                if (FirstName == null){}else if(FirstName == "") sb.Append("<strRegisterFirstName />"); else sb.AppendFormat("<strRegisterFirstName>{0}</strRegisterFirstName>", FirstName);
                if (FirstEMail == null){}else if(FirstEMail == "") sb.Append("<strRegisterFirstEMail />"); else sb.AppendFormat("<strRegisterFirstEMail>{0}</strRegisterFirstEMail>", FirstEMail);
                if (FirstURL == null){}else if(FirstURL == "")  sb.Append("<strRegisterFirstURL />");else sb.AppendFormat("<strRegisterFirstURL>{0}</strRegisterFirstURL>", FirstURL);
                if (FirstPhone == null){} else if (FirstPhone == "")sb.Append("<strRegisterFirstPhone />"); else sb.AppendFormat("<strRegisterFirstPhone>{0}</strRegisterFirstPhone>", FirstPhone);
                if (FirstComment == null){}else if( FirstComment == "")sb.Append("<strRegisterFirstComment />");else  sb.AppendFormat("<strRegisterFirstComment>{0}</strRegisterFirstComment>", FirstComment);
                if (Name == null){}else if(Name == "") sb.Append("<strRegisterName />"); else sb.AppendFormat("<strRegisterName>{0}</strRegisterName>", Name);
                if (EMail == null){}else if(EMail == "") sb.Append("<strRegisterEMail />");else  sb.AppendFormat("<strRegisterEMail>{0}</strRegisterEMail>", EMail);
                if (URL == null){}else if( URL == "") sb.Append("<strRegisterURL />"); else sb.AppendFormat("<strRegisterURL>{0}</strRegisterURL>", URL);
                if (Phone == null){}else if(Phone == "") sb.Append("<strRegisterPhone />"); else sb.AppendFormat("<strRegisterPhone>{0}</strRegisterPhone>", Phone);
                if (Comment == null){}else if(Comment == "") sb.Append("<strRegisterComment />"); else sb.AppendFormat("<strRegisterComment>{0}</strRegisterComment>", Comment);
                sb.Append("</soap:Body></soap:Envelope>");
            }
            else
            {
                sb.AppendLine("<?xml version=\"1.0\" encoding=\"utf-8\"?>\r\n<soap:Envelope xmlns:soap=\"http://www.w3.org/2003/05/soap-envelope\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\">\r\n<soap:Body>");
                sb.AppendFormat("<strStatusID>{0}</strStatusID>\r\n", StatusID);
                sb.AppendFormat("<strInfoID>{0}</strInfoID>\r\n", InfoID);
                if (Date == null){}else if(Date == "")sb.Append("<strRegistDate />\r\n");else sb.AppendFormat("<strRegistDate>{0}</strRegistDate>\r\n", Date);
                if (Title == null) {}else if(Title == "")sb.Append("<strTitle />\r\n"); else sb.AppendFormat("<strTitle>{0}</strTitle>\r\n", Title);
                if (Artist == null){}else if(Artist == "")sb.Append(IsMD5Get?"<strArtist />\r\n":"<strArtistName />\r\n");  else sb.AppendFormat(IsMD5Get?"<strArtist>{0}</strArtist>\r\n":"<strArtistName>{0}</strArtistName>\r\n", Artist);
                if (Album == null){}else if(Album == "") sb.Append(IsMD5Get?"<strAlbum />\r\n":"<strAlbumName />\r\n"); else sb.AppendFormat(IsMD5Get?"<strAlbum>{0}</strAlbum>\r\n":"<strAlbumName>{0}</strAlbumName>\r\n", Album);
                if (Lyric == null){}else if(Lyric == "") sb.Append("<strLyric />\r\n"); else sb.AppendFormat("<strLyric>\r\n{0}</strLyric>\r\n", Lyric);
                if (FirstName == null){}else if(FirstName == "") sb.Append("<strRegisterFirstName />\r\n"); else sb.AppendFormat("<strRegisterFirstName>{0}</strRegisterFirstName>\r\n", FirstName);
                if (FirstEMail == null){}else if(FirstEMail == "") sb.Append("<strRegisterFirstEMail />\r\n"); else sb.AppendFormat("<strRegisterFirstEMail>{0}</strRegisterFirstEMail>\r\n", FirstEMail);
                if (FirstURL == null){}else if(FirstURL == "")  sb.Append("<strRegisterFirstURL />\r\n");else sb.AppendFormat("<strRegisterFirstURL>{0}</strRegisterFirstURL>\r\n", FirstURL);
                if (FirstPhone == null){} else if (FirstPhone == "")sb.Append("<strRegisterFirstPhone />\r\n"); else sb.AppendFormat("<strRegisterFirstPhone>{0}</strRegisterFirstPhone>\r\n", FirstPhone);
                if (FirstComment == null){}else if( FirstComment == "")sb.Append("<strRegisterFirstComment />\r\n");else  sb.AppendFormat("<strRegisterFirstComment>{0}</strRegisterFirstComment>\r\n", FirstComment);
                if (Name == null){}else if(Name == "") sb.Append("<strRegisterName />\r\n"); else sb.AppendFormat("<strRegisterName>\r\n{0}</strRegisterName>\r\n", Name);
                if (EMail == null){}else if(EMail == "") sb.Append("<strRegisterEMail />\r\n");else  sb.AppendFormat("<strRegisterEMail>{0}</strRegisterEMail>\r\n", EMail);
                if (URL == null){}else if( URL == "") sb.Append("<strRegisterURL />\r\n"); else sb.AppendFormat("<strRegisterURL>{0}</strRegisterURL>\r\n", URL);
                if (Phone == null){}else if(Phone == "") sb.Append("<strRegisterPhone />\r\n"); else sb.AppendFormat("<strRegisterPhone>{0}</strRegisterPhone>\r\n", Phone);
                if (Comment == null){}else if(Comment == "") sb.Append("<strRegisterComment />\r\n"); else sb.AppendFormat("<strRegisterComment>{0}</strRegisterComment>\r\n", Comment);
                sb.Append("</soap:Body>\r\n</soap:Envelope>");
            }
            return sb.ToString();
        }

        public static LyricAlsong Clone(LyricAlsong AlsongLyricClass)
        {
            LyricAlsong al = new AlsongLyricsInstance();
            
            if(AlsongLyricClass.Title!=null)al.Title = string.Copy(AlsongLyricClass.Title);
            if(AlsongLyricClass.Album!=null)al.Album = string.Copy(AlsongLyricClass.Album);
            if(AlsongLyricClass.Artist!=null)al.Artist = string.Copy(AlsongLyricClass.Artist);
            if(AlsongLyricClass.Date!=null)al.Date = string.Copy(AlsongLyricClass.Date);
            if(AlsongLyricClass.Name!=null)al.Name = string.Copy(AlsongLyricClass.Name);
            if(AlsongLyricClass.EMail!=null)al.EMail = string.Copy(AlsongLyricClass.EMail);
            if(AlsongLyricClass.URL!=null)al.URL = string.Copy(AlsongLyricClass.URL);
            if(AlsongLyricClass.Phone!=null)al.Phone = string.Copy(AlsongLyricClass.Phone);
            if(AlsongLyricClass.Comment!=null)al.Comment = string.Copy(AlsongLyricClass.Comment);
            if(AlsongLyricClass.FirstComment!=null)al.FirstComment = string.Copy(AlsongLyricClass.FirstComment);
            if(AlsongLyricClass.FirstEMail!=null)al.FirstEMail = string.Copy(AlsongLyricClass.FirstEMail);
            if(AlsongLyricClass.FirstName!=null)al.FirstName = string.Copy(AlsongLyricClass.FirstName);
            if(AlsongLyricClass.FirstPhone!=null)al.FirstPhone = string.Copy(AlsongLyricClass.FirstPhone);
            if(AlsongLyricClass.FirstURL!=null)al.FirstURL = string.Copy(AlsongLyricClass.FirstURL);
            al.InfoID = AlsongLyricClass.InfoID;
            al.StatusID = AlsongLyricClass.StatusID;
            if(AlsongLyricClass.Length!=null)al.Length = string.Copy(AlsongLyricClass.Length);
            if(AlsongLyricClass.MD5Hash!=null)al.MD5Hash = string.Copy(AlsongLyricClass.MD5Hash);
            if(AlsongLyricClass.GetOriginText!=null)al.GetOriginText = string.Copy(AlsongLyricClass.GetOriginText);
            if (AlsongLyricClass.OriginXML != null)
            {
                al.OriginXML = new XmlDocument();
                al.OriginXML.LoadXml(AlsongLyricClass.OriginXML.InnerXml);
            }
            return al;
        }
    }
    internal class AlsongLyricsInstance:LyricAlsong
    {        
        public AlsongLyricsInstance(string MD5Hash = null) { base.MD5Hash = MD5Hash; }
        public AlsongLyricsInstance(string OriginXML, string MD5Hash = null)
        {
            base.GetOriginText = OriginXML;
            base.MD5Hash = MD5Hash;
            //this.OriginXML = new XmlDocument();
            //this.OriginXML.LoadXml(OriginXML);
        }
        public AlsongLyricsInstance(XmlDocument OriginXML, string MD5Hash = null)
        {
            base.GetOriginText = OriginXML.InnerXml;
            base.MD5Hash = MD5Hash;
            base.OriginXML = new XmlDocument();
            base.OriginXML.LoadXml(OriginXML.InnerXml);
        }}
#region Obsolete
    /// <summary>
    /// 싱크가사를 묶어둔
    /// </summary>
    [Serializable, Obsolete("검색방식이 AlsongLyric 클래스로 바뀌어 AlsongLyric 클래스를 사용해 주십시오")]
    public class LyricsEz
    {
        public string Maker;
        public string MD5Hash;
        public LyricClass LyricsInfo;
        public string Lyric
        {
            get { return LyricsInfo.ToString(); }
            set { _Lyric = value; LyricsInfo = new LyricClass(value); }
        }
        public string _Lyric;

        public LyricsEz() { }
        public LyricsEz(string Maker, string Lyric)
        { this.Maker = Maker; this.Lyric = Lyric; LyricsInfo = new LyricClass(Lyric, TrimMakerforName(Maker)); }
        public LyricsEz(LyricClass LyricsInfo, string MD5Hash)
        { this.LyricsInfo = LyricsInfo; this.Maker = LyricsInfo.LyricsMaker; }
        public LyricsEz(string Maker, string Lyric, string MD5Hash)
        { this.Maker = Maker; this.Lyric = Lyric; this.MD5Hash = MD5Hash; LyricsInfo = new LyricClass(Lyric, TrimMakerforName(Maker)); }
        public string Maker_Name { get { return TrimMakerforName(Maker); } }

        /// <summary>
        /// 제작자 이름만 제외한 나머지를 지웁니다. (반드시 포맷은 "1 (by 철수)" 이어야 합니다 (" 제외하고) 실패하면 null을 반환합니다.
        /// </summary>
        /// <param name="MakerName"></param>
        /// <returns></returns>
        public static string TrimMakerforName(string MakerName)
        {
            try
            {
                string a = MakerName.Substring(MakerName.IndexOf("(by ") + 4);
                string b = a.Remove(a.LastIndexOf(")"));
                return b;
            }
            catch { return null; }
        }
        public static LyricsEz[] ConvertLyricsClassToLyricsArray(LyricsClass Lyrics)
        {
            LyricsEz[] lc = new LyricsEz[Lyrics.Maker.LongLength];
            for (long i = 0; i < lc.LongLength; i++)
            { lc[i] = new LyricsEz(Lyrics.Maker[i], Lyrics.Lyric[i], Lyrics.MD5Hash); }
            return lc;
        }
        public static LyricsEz[] ConvertLyricsClassToLyricsArray(LyricAlsong[] Lyrics)
        {
            LyricsEz[] lc = new LyricsEz[Lyrics.LongLength];
            for (long i = 0; i < lc.LongLength; i++)
            { lc[i] = new LyricsEz(Lyrics[i].FirstName, Lyrics[i].Lyric, Lyrics[i].MD5Hash); }
            return lc;
        }
    }

    /// <summary>
    /// 싱크가사를 검색할때 쓰는 클래스이므로 일반적으로는 불필요합니다.
    /// </summary>
    [Serializable]
    public struct LyricsInfo
    {
        public LyricsInfo(string Song, string Artist)
        { this.Song = Song; this.Artist = Artist; }
        public string Song;
        public string Artist;
    }

    /// <summary>
    /// 싱크가사를 검색했을떄 나온가사를 배열로 묶어놓은 클래스입니다.
    /// </summary>
    [Serializable, Obsolete("검색방식이 AlsongLyric 클래스로 바뀌어 AlsongLyric 클래스를 사용해 주십시오")]
    public class LyricsClass
    {
        public LyricsClass() { }
        public LyricsClass(string[] Maker, string[] Lyric) { this.Maker = Maker; this.Lyric = Lyric; }
        public LyricsClass(string[] Maker, string[] Lyric, string MD5Hash){ this.Maker = Maker; this.Lyric = Lyric; this.MD5Hash = MD5Hash;}
        public string[] Maker;
        public string[] Lyric;
        public string MD5Hash;
        public LyricsClass Copy()
        { LyricsClass lc = new LyricsClass(this.Maker, this.Lyric, MD5Hash); return lc; }
        public static LyricsClass ConvertLyricsArrayToLyricsClass(LyricsEz[] Lyrics)
        {
            string[] Maker = new string[Lyrics.LongLength];
            string[] Lyric = new string[Lyrics.LongLength];
            for (long i = 0; i < Lyric.LongLength; i++)
            { Maker[i] = Lyrics[i].Maker; Lyric[i] = Lyrics[i].Lyric; }
            return new LyricsClass(Maker, Lyric, Lyrics[0].MD5Hash);
        }
    }
#endregion
    /*
    public class GetLyricsCompleteEventArgs : EventArgs
    {
        public LyricsClass LyricsClass { get; private set; }
        public GetLyricsCompleteEventArgs(LyricsClass LyricsClass)
        { this.LyricsClass = LyricsClass; }

    }*/
    /// <summary>
    /// 싱크가사 예외 클래스 입니다.
    /// </summary>
    public class LyricException : Exception
    {
        /// <summary>
        /// 싱크가사 예외 클래스를 초기화 합니다.
        /// </summary>
        /// <param name="InnerExeption">내부의 예외를 설정합니다.</param>
        public LyricException(Exception InnerExeption=null) 
        {this.InnerException=InnerException; _Message = "싱크 가사를 찾을 수 없습니다!"; }
        /// <summary>
        /// 싱크가사 예외 클래스를 초기화 합니다.
        /// </summary>
        /// <param name="Message">표시할 메세지 입니다.</param>
        /// <param name="InnerExeption">내부의 예외를 설정합니다.</param>
        public LyricException(string Message,Exception InnerExeption=null)
        {this.InnerException=InnerException; _Message = Message; }

        string _Message;
        /// <summary>
        /// 오류를 설명하는 메세지를 가져옵니다.
        /// </summary>
        public override string Message
        {
            get { return _Message; }
        }

        protected new Exception InnerException{get;private set;}
    }

}
