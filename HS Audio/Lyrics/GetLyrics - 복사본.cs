﻿namespace HS_Audio.Lyrics
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Net;
    using System.Security.Cryptography;
    using System.Text;
    using System.Xml;

    public class GetLyrics
    {
        private string a = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><SOAP-ENV:Envelope xmlns:SOAP-ENV=\"http://www.w3.org/2003/05/soap-envelope\" xmlns:SOAP-ENC=\"http://www.w3.org/2003/05/soap-encoding\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:ns2=\"ALSongWebServer/Service1Soap\" xmlns:ns1=\"ALSongWebServer\" xmlns:ns3=\"ALSongWebServer/Service1Soap12\">";
        private string b = "<SOAP-ENV:Body><ns1:GetLyric5><ns1:stQuery><ns1:strChecksum>CheckSum</ns1:strChecksum><ns1:strVersion>2.0 beta2</ns1:strVersion><ns1:strMACAddress></ns1:strMACAddress><ns1:strIPAddress>255.255.255.0</ns1:strIPAddress></ns1:stQuery></ns1:GetLyric5></SOAP-ENV:Body></SOAP-ENV:Envelope>";
        private string c = "<SOAP-ENV:Body><ns1:GetResembleLyric2><ns1:stQuery><ns1:strTitle>TITLE</ns1:strTitle><ns1:strArtistName>ARTIST</ns1:strArtistName><ns1:nCurPage>0</ns1:nCurPage></ns1:stQuery></ns1:GetResembleLyric2></SOAP-ENV:Body></SOAP-ENV:Envelope>";
        private string d = "http://lyrics.alsong.co.kr/alsongwebservice/service1.asmx";
        private string[] e;
        private string[] f;

        public delegate void ExceptionEventHandler(Exception ex);
        public event ExceptionEventHandler ExceptionEvent;
        private static string Decoding(byte[] A_0, int A_1, int A_2)
        {
            if (A_2 > 0)
            {
                return Encoding.ASCII.GetString(A_0, A_1, A_2);
            }
            return Encoding.ASCII.GetString(A_0);
        }

        public static string GetHash(byte[] A_0)
        {
            MD5CryptoServiceProvider provider = new MD5CryptoServiceProvider();
            provider.ComputeHash(A_0);
            byte[] hash = provider.Hash;
            StringBuilder builder = new StringBuilder();
            foreach (byte num in hash)
            {
                builder.Append(string.Format("{0:X2}", num));
            }
            return builder.ToString();
        }

        private static int a10(char A_0)
        {
            int num;
            int num2 = Convert.ToInt32(A_0);
            if (num2 < 0x80)
            {
                return num2;
            }
            try
            {
                byte[] buffer;
                Encoding encoding = Encoding.Default;
                char[] chars = new char[] { A_0 };
                if (encoding.IsSingleByte)
                {
                    buffer = new byte[1];
                    encoding.GetBytes(chars, 0, 1, buffer, 0);
                    return buffer[0];
                }
                buffer = new byte[2];
                if (encoding.GetBytes(chars, 0, 1, buffer, 0) == 1)
                {
                    return buffer[0];
                }
                if (BitConverter.IsLittleEndian)
                {
                    byte num3 = buffer[0];
                    buffer[0] = buffer[1];
                    buffer[1] = num3;
                }
                num = BitConverter.ToInt16(buffer, 0);
            }
            catch (Exception exception)
            {
                throw exception;
            }
            return num;
        }

        private static Stream a1(Stream A_0)
        {
            for (int i = 0; i < /*0xc350*/50000; i++)
            {
                if ((A_0.ReadByte() == 0xff) && ((A_0.ReadByte() >> 5) == 7))
                {
                    A_0.Position += -2L;
                    return A_0;
                }
            }
            return A_0;
        }

        private static void a4(ref string[] A_0)
        {
            for (int i = 0; i < A_0.Length; i++)
            {
                string[] strArray = A_0[i].Replace("\n", "").Split(new char[] { Convert.ToChar("\r") });
                A_0[i] = A_0[i].Remove(0);
                for (int j = 0; j < strArray.Length; j++)
                {
                    if (strArray[j].Length > 9)
                    {
                        string[] strArray2;
                        IntPtr ptr;
                        strArray[j] = strArray[j].Substring(10, strArray[j].Length - 10);
                        (strArray2 = A_0)[(int) (ptr = (IntPtr) i)] = strArray2[(int) ptr] + strArray[j] + "\r\n";
                    }
                }
            }
        }

        private static string a5(string A_0)
        {
            byte[] bytes = Encoding.UTF8.GetBytes(A_0);
            string str = "";
            foreach (byte num in bytes)
            {
                str = str + string.Format("{0:0x}", Convert.ToString(num, 0x10));
            }
            return str;
        }

        /// <summary>
        /// 스트림에서 가져오기
        /// </summary>
        /// <param name="A_0"></param>
        /// <param name="A_1"></param>
        /// <param name="A_2"></param>
        /// <returns></returns>
        private XmlDocument SendLyrics(Stream A_0, string A_1, string A_2)
        {
            XmlDocument document;
            try
            {
                if (A_0 == null)
                {
                    return null;
                }
                int num = 0;
                A_0.Position = 0L;
                string str = "";
                byte[] buffer = new byte[0x1b];
                A_0.Read(buffer, 0, 0x1b);
                if ((buffer.Length < 0x1b) || (A_0.Position > 0xc350L))
                {
                    return null;
                }
                long position = A_0.Position;
                byte[] destinationArray = new byte[4];
                Array.Copy(buffer, 0, destinationArray, 0, 4);
                byte[] buffer3 = new byte[3];
                Array.Copy(buffer, 0, buffer3, 0, 3);
                if (Decoding(destinationArray, 0, 0) == "OggS")
                {
                    A_0.Position = position + 4L;
                    a1(A_0);
                }
                else if (Decoding(buffer3, 0, 0) == "ID3")
                {
                    A_0.Position -= 0x11L;
                    byte[] buffer4 = new byte[4];
                    Array.Copy(buffer, 6, buffer4, 0, 4);
                    Stream stream = b1(A_0, ref num, buffer3, buffer4);
                    stream = a1(stream);
                }
                else
                {
                    A_0.Position = 0L;
                    a1(A_0);
                }
                byte[] buffer5 = new byte[0x28000];
                A_0.Read(buffer5, 0, 0x28000);
                str = GetHash(buffer5);
                document = this.SendLyrics(str, A_1, A_2);
            }
            catch (Exception ex)
            {
                try { ExceptionEvent(ex); }catch{}
                return null;
            }
            return document;
        }


        string MD5Hash;
        /// <summary>
        /// 파일에서 가져오기
        /// </summary>
        /// <param name="A_0">체크섬</param>
        /// <param name="A_1"></param>
        /// <param name="A_2"></param>
        /// <returns></returns>
        private XmlDocument SendLyrics(string A_0, string A_1, string A_2)
        {
            MD5Hash = A_0;
            XmlDocument document = null;
            string str = null;
            str = A_1.Replace("^^", "'").Replace("CheckSum", A_0);
            HttpWebRequest request = null;
            WebResponse response = null;
            try
            {
                string requestUriString = A_2;
                request = (HttpWebRequest) WebRequest.Create(requestUriString);
                request.UserAgent = "gSOAP/2.7";
                request.Method = "POST";
                request.ContentType = "application/soap+xml; charset=utf-8";
                StreamWriter writer = new StreamWriter(request.GetRequestStream());
                writer.WriteLine(str);
                writer.Close();
                Stream responseStream = request.GetResponse().GetResponseStream();
                XmlDocument document2 = new XmlDocument();
                document2.Load(responseStream);
                document = document2;
            }
            catch (Exception exception)
            {
                try { ExceptionEvent(exception); }catch{}
                Console.WriteLine(exception.Message);
            }
            finally
            {
                if (request != null)
                {
                    request.GetRequestStream().Close();
                }
                if (response != null)
                {
                    response.GetResponseStream().Close();
                }
            }
            return document;
        }

        /// <summary>
        /// 음악 정보로 찾기
        /// </summary>
        /// <param name="A_0"></param>
        /// <param name="A_1"></param>
        /// <param name="A_2"></param>
        /// <param name="A_3"></param>
        /// <returns></returns>
        private XmlDocument SendLyrics(string A_0, string A_1, string A_2, string A_3)
        {
            MD5Hash = null;
            XmlDocument document = null;
            string str = null;
            str = A_2.Replace("^^", "\"").Replace("TITLE", A_0).Replace("ARTIST", A_1);
            WebRequest request = null;
            WebResponse response = null;
            try
            {
                string requestUriString = A_3;
                request = WebRequest.Create(requestUriString);
                request.Method = "POST";
                request.ContentType = "application/soap+xml";
                StreamWriter writer = new StreamWriter(request.GetRequestStream());
                writer.WriteLine(str);
                writer.Close();
                Stream responseStream = request.GetResponse().GetResponseStream();
                XmlDocument document2 = new XmlDocument();
                document2.Load(responseStream);
                document = document2;
            }
            finally
            {
                if (request != null)
                {
                    request.GetRequestStream().Close();
                }
                if (response != null)
                {
                    response.GetResponseStream().Close();
                }
            }
            return document;
        }

        private bool a3(XmlDocument A_0, ref string[] A_1, ref string[] A_2)
        {
            LyricsLists = new AlsongLyric[0];
            if (A_0 == null)
            {
                return false;
            }
            if (A_0.HasChildNodes)
            {
                XmlNodeList childNodes = A_0.ChildNodes[1].ChildNodes[0].ChildNodes[0].ChildNodes;
                
                XmlNode node = childNodes[0];
                if (childNodes.Count > 0)
                {
                    if (node.Name == "GetResembleLyric2Result")
                    {
                        int count = node.ChildNodes.Count;
                        if (count <= 0)
                        {
                            return false;
                        }
                        A_1 = new string[count];
                        A_2 = new string[count];
                        LyricsLists = new AlsongLyric[count];
                        for (int i = 0; i < count; i++)
                        {
                            AlsongLyric al = new AlsongLyric(node.ChildNodes[i].InnerXml,null);
                            for (int j = 0; j < node.ChildNodes[i].ChildNodes.Count; j++)
                            {
                                string info = node.ChildNodes[i].ChildNodes[j].InnerText;//==""?null:node.ChildNodes[i].ChildNodes[j].InnerText;
                                switch(node.ChildNodes[i].ChildNodes[j].Name)
                                {
                                    case "strStatusID": try { al.StatusID=Convert.ToInt64(info); }catch { }break;
                                    case "strInfoID": try { al.InfoID=Convert.ToInt64(info); }catch { }break;
                                    case "strRegistDate": try { al.Date=info; }catch { }break;
                                    case "strTitle": try { al.Title=info;}catch { }break;
                                    case "strArtist":try { al.Artist=info; }catch { }break;
                                    case "strAlbum":try { al.Album=info;}catch { }break;
                                    case "strLyric":try { al.Lyric=info.Replace("<br>", "\r\n"); }catch { }break;
                                    case "strRegisterFirstName":try { al.FirstName=info; }catch { }break;
                                    case "strRegisterFirstEMail": try { al.FirstEMail=info;}catch { }break;
                                    case "strRegisterFirstURL": try { al.FirstURL=info; }catch { }break;
                                    case "strRegisterFirstPhone": try { al.FirstPhone=info; }catch { }break;
                                    case "strRegisterFirstComment": try { al.FirstComment=info;}catch { }break;
                                    case "strRegisterName":try { al.Name=info; }catch { }break;
                                    case "strRegisterEMail": try { al.EMail=info; }catch { }break;
                                    case "strRegisterURL":try { al.URL=info; }catch { }break;
                                    case "strRegisterPhone": try { al.Phone=info; }catch { }break;
                                    case "strRegisterComment": try { al.Comment=info; }catch { }break;
                                }
                            }
                            LyricsLists[i] = al;
                            try { A_2[i] = Convert.ToString((int)(i + 1)) + " (by " + node.ChildNodes[i].ChildNodes[11].InnerText + ")"; }
                            catch { A_2[i] = Convert.ToString((int)(i + 1)) + " (by " + node.ChildNodes[i].ChildNodes[node.ChildNodes[i].ChildNodes.Count-1].InnerText + ")"; }
                            A_2[i] = Convert.ToString((int)(i + 1)) + node.ChildNodes[i].ChildNodes[11].InnerText ;
                            A_1[i] = A_0.ChildNodes[1].ChildNodes[0].ChildNodes[0].ChildNodes[0].ChildNodes[i].ChildNodes[3].InnerText.Replace("<br>", "\r\n");
                        }
                    }
                    else
                    {
                        AlsongLyric al = new AlsongLyric(A_0, MD5Hash);                                                                           
                        for (int j = 0; j < node.ChildNodes.Count; j++)
                        {
                            string info = node.ChildNodes[j].InnerText;//==""?null:node.ChildNodes[j].InnerText;
                            if (node.ChildNodes[j].Name == "strLyric")
                            {
                                A_1 = new string[1];
                                A_2 = new string[] { Convert.ToString(1) + " (by " + node.ChildNodes[j + 6].InnerText + ")" };
                                //A_2 = new string[] { Convert.ToString(1) + node.ChildNodes[j + 6].InnerText };
                                string str2 = info.Replace("<br>", "\r\n");
                                A_1[0] = str2;
                            }
                            switch(node.ChildNodes[j].Name)
                            {
                                case "strStatusID": try { al.StatusID=Convert.ToInt64(info); }catch { }break;
                                case "strInfoID": try { al.InfoID=Convert.ToInt64(info); }catch { }break;
                                case "strRegistDate": try { al.Date=info; }catch { }break;
                                case "strTitle": try { al.Title=info; }catch { }break;
                                case "strArtist":try { al.Artist=info; }catch { }break;
                                case "strAlbum":try { al.Album=info; }catch { }break;
                                case "strLyric":try { al.Lyric=info.Replace("<br>", "\r\n"); }catch { }break;
                                case "strRegisterFirstName":try { al.FirstName=info; }catch { }break;
                                case "strRegisterFirstEMail": try { al.FirstEMail=info; }catch { }break;
                                case "strRegisterFirstURL": try { al.FirstURL=info; }catch { }break;
                                case "strRegisterFirstPhone": try { al.FirstPhone=info; }catch { }break;
                                case "strRegisterFirstComment": try { al.FirstComment=info; }catch { }break;
                                case "strRegisterName":try { al.Name=info; }catch { }break;
                                case "strRegisterEMail": try { al.EMail=info; }catch { }break;
                                case "strRegisterURL":try { al.URL=info; }catch { }break;
                                case "strRegisterPhone": try { al.Phone=info; }catch { }break;
                                case "strRegisterComment": try { al.Comment=info; }catch { }break;
                            }
                        }
                        if ((al.Title != null && al.Title != "") || (al.Lyric != null && al.Lyric != "")) LyricsLists = new AlsongLyric[]{al};
                    }
                }
            }
            return true;
        }

        private static Stream b1(Stream A_0, ref int A_1, byte[] A_2, byte[] A_3)
        {
            int num = 0;
            while (num < 500000/*0x7a120*/)
            {
                if (Decoding(A_2, 0, 0).ToString() == "ID3")
                {
                    A_1 = 10 + ((((A_3[0] << 21/*0x15*/) | (A_3[1] << 14)) | (A_3[2] << 7)) | A_3[3]);
                    if (A_0.CanSeek)
                    {
                        A_0.Position = (long) A_1;
                        break;
                    }
                }
                num++;
            }
            if (num == 0x7a120)
            {
                A_0.Position = 0L;
            }
            return A_0;
        }

        private Stream b1(Stream A_0)
        {
            byte[] buffer = new byte[7];
            for (int i = 0; A_0.Length >= A_0.Position; i++)
            {
                A_0.Read(buffer, 0, 7);
                byte[] buffer2 = new byte[] { 5, 0x76, 0x6f, 0x72, 0x62, 0x69, 0x73 };
                if (Decoding(buffer, 0, 0) == Decoding(buffer2, 0, 0))
                {
                    long num2 = (A_0.Position - 7L) + 11L;
                    A_0.Position = num2;
                    return A_0;
                }
                A_0.Position = i;
            }
            return A_0;
        }

        private Stream b1(string A_0)
        {
            Stream stream = null;
            try
            {
                stream = new FileStream(A_0, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
            }
            catch{}
            return stream;
        }

        /*
        private XmlDocument b2(string A_0, string A_1, string A_2, string A_3)
        {
            return this.a(A_1, A_0, A_2, A_3);
        }*/

        public bool GetLyricsFromFile(string aFilePath, bool aViewTime)                                               
        {
            XmlDocument document = this.SendLyrics(this.b1(aFilePath), (this.a + this.b).Replace("^^", "'"), this.d);
            if (!this.a3(document, ref this.e, ref this.f))
            {
                return false;
            }
            if (this.e == null)
            {
                return false;
            }
            if (!aViewTime)
            {
                a4(ref this.e);
            }
            return true;
        }

        public bool GetLyricsFromName(string aArtist, string aTitle, bool aViewTime)
        {
            XmlDocument document = this.SendLyrics(aArtist, aTitle, (this.a + this.c).Replace("^^", "'"), this.d);
            if (!this.a3(document, ref this.e, ref this.f))
            {
                return false;
            }
            if (this.e == null)
            {
                return false;
            }
            if (!aViewTime)
            {
                a4(ref this.e);
            }
            return true;
        }

        public bool GetStreamLyrics(Stream aStream, bool aViewTime)
        {
            XmlDocument document = this.SendLyrics(aStream, (this.a + this.b).Replace("^^", "'"), this.d);
            if (!this.a3(document, ref this.e, ref this.f))
            {
                return false;
            }
            if (this.e == null)
            {
                return false;
            }
            if (!aViewTime)
            {
                a4(ref this.e);
            }
            return true;
        }

        public bool GetLyricsFromMD5Hash(string HD5Hash, bool aViewTime=true)
        {
            this.MD5Hash = HD5Hash;
            XmlDocument document = this.SendLyrics(HD5Hash, (this.a + this.b).Replace("^^", "'"), this.d);
            if (!this.a3(document, ref this.e, ref this.f)) return false;
            if (this.e == null) return false;
            if (!aViewTime) a4(ref this.e);
            return true;
        }

        public AlsongLyric[] LyricsLists
        {
            get;private set;
        }

        public string[] LyricLists
        {
            get
            {
                return this.e;
            }
        }

        public string[] MakerLists
        {
            get
            {
                return this.f;
            }
        }


        /// <summary>
        /// 음악 스트림에서 음악의 MD5해쉬 값을 구합니다.
        /// </summary>
        /// <param name="Music">음악 스트림 입니다.</param>
        /// <returns></returns>
        public static string GenerateMusicHash(Stream Music)
        {
            try
            {
                if (Music == null) { return null; }
                int num = 0;
                Music.Position = 0L;
                byte[] buffer = new byte[0x1b];
                Music.Read(buffer, 0, 0x1b);
                if ((buffer.Length < 0x1b) || (Music.Position > 0xc350L))
                {
                    return null;
                }
                long position = Music.Position;
                byte[] destinationArray = new byte[4];
                Array.Copy(buffer, 0, destinationArray, 0, 4);
                byte[] buffer3 = new byte[3];
                Array.Copy(buffer, 0, buffer3, 0, 3);
                if (Decoding(destinationArray, 0, 0) == "OggS")
                {
                    Music.Position = position + 4L;
                    a1(Music);
                }
                else if (Decoding(buffer3, 0, 0) == "ID3")
                {
                    Music.Position -= 0x11L;
                    byte[] buffer4 = new byte[4];
                    Array.Copy(buffer, 6, buffer4, 0, 4);
                    Stream stream = b1(Music, ref num, buffer3, buffer4);
                    stream = a1(stream);
                }
                else
                {
                    Music.Position = 0L;
                    a1(Music);
                }
                byte[] buffer5 = new byte[0x28000];
                Music.Read(buffer5, 0, 0x28000);
                return GetHash(buffer5);
            }
            catch { return null; }
        }
        /// <summary>
        /// 파일에서 음악의 MD5해쉬 값을 구합니다.
        /// </summary>
        /// <param name="Music">음악파일 경로 입니다.</param>
        /// <returns></returns>
        public static string GenerateMusicHash(string Music)
        {
            try
            {
                StreamReader sr = new StreamReader(Music);
                Stream Music1 = sr.BaseStream;
                if (Music1 == null) { return null; }
                int num = 0;
                Music1.Position = 0L;
                byte[] buffer = new byte[0x1b];
                Music1.Read(buffer, 0, 0x1b);
                if ((buffer.Length < 0x1b) || (Music1.Position > 0xc350L))
                {
                    return null;
                }
                long position = Music1.Position;
                byte[] destinationArray = new byte[4];
                Array.Copy(buffer, 0, destinationArray, 0, 4);
                byte[] buffer3 = new byte[3];
                Array.Copy(buffer, 0, buffer3, 0, 3);
                if (Decoding(destinationArray, 0, 0) == "OggS")
                {
                    Music1.Position = position + 4L;
                    a1(Music1);
                }
                else if (Decoding(buffer3, 0, 0) == "ID3")
                {
                    Music1.Position -= 0x11L;
                    byte[] buffer4 = new byte[4];
                    Array.Copy(buffer, 6, buffer4, 0, 4);
                    Stream stream = b1(Music1, ref num, buffer3, buffer4);
                    stream = a1(stream);
                }
                else
                {
                    Music1.Position = 0L;
                    a1(Music1);
                }
                byte[] buffer5 = new byte[0x28000];
                Music1.Read(buffer5, 0, 0x28000);
                return GetHash(buffer5);
            }
            catch { return null; }
        }
    }
}

