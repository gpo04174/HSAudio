﻿using System;
using System.IO;
using System.Text;
using System.Windows.Forms;

namespace HS_Audio.Lyrics
{
    public partial class frmLyrics : Form
    {
        public frmLyrics()
        {
            InitializeComponent();
        }
        HS_Audio.Lyrics.LyricParser lp= new LyricParser();
        string Orignal;
        public frmLyrics(string Lyrics)
        {
            InitializeComponent();
            Orignal = Lyrics;
            this.Lyrics = Lyrics;
        }

        public string Lyrics
        {
            get { return lp==null?null:lp.ToString(); }
            set
            {
                DialogResult dr = System.Windows.Forms.DialogResult.Yes;
                /*
                if((this.WindowState != FormWindowState.Minimized||!FormClosed)&&Edited) 
                    dr=MessageBox.Show("싱크가사가 변경되었습니다.\r\n다시 새로고침 하시겠습니까?", "싱크 가사 편집기", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                 */
                if(/*dr == System.Windows.Forms.DialogResult.Yes||*/자동업데이트ToolStripMenuItem.Checked){
                Orignal = value;
                lp.LrcCode = value;
                txtAlbum.Text = lp.Album;
                txtArtist.Text = lp.Artist;
                txtAuthor.Text = lp.Author;
                txtComment.Text = lp.Comment;
                numOffset.Value = lp.Offset;
                txtLength.Text = lp.Length;
                txtMaker.Text = lp.LyricsMaker;
                txtTitle.Text = lp.Title;
                textBox1.Text = lp.PurelrcCode;}
            }
        }

        public delegate void LyricsInputCompleteEventHandler(LyricClass Lyric);
        public event LyricsInputCompleteEventHandler LyricsInputComplete;

        private void button1_Click(object sender, EventArgs e)
        {
            if(lp.PurelrcCode==null||lp.PurelrcCode==""){MessageBox.Show("가사를 입력해 주세요!", "싱크 가사 편집", MessageBoxButtons.OK, MessageBoxIcon.Error);return;}
            Edited=false;
            if (!원본ToolStripMenuItem.Checked) textBox2.Text = lp.ToString();
            if (LyricsInputComplete != null)
            {
                if (전체보기ToolStripMenuItem.Checked) LyricsInputComplete(lp.Lyrics);
                else { if(lp.PurelrcCode!=null||lp.PurelrcCode!="")LyricsInputComplete(lp.Lyrics); }
            }
        }

        public bool FormClose;
        internal bool FormClosed=true;
        private void frmLyrics_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = !FormClose;
            FormClosed = true;
            this.Hide();
        }

        private void frmLyrics_Load(object sender, EventArgs e)
        {

        }

        private void 싱크가사파일에서열기ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                this.Lyrics = System.IO.File.ReadAllText(openFileDialog1.FileName, HS_CSharpUtility.Utility.IOUtility.GetFileEncoding(openFileDialog1.FileName)); 
                //if(this.Lyrics)
            }
        }

        private void txtTitle_TextChanged(object sender, EventArgs e) { lp.Title = txtTitle.Text; }

        private void txtArtist_TextChanged(object sender, EventArgs e) { lp.Artist = txtArtist.Text; }

        private void txtAlbum_TextChanged(object sender, EventArgs e){lp.Album=txtAlbum.Text;}

        private void txtAuthor_TextChanged(object sender, EventArgs e) { lp.Author = txtAuthor.Text; }

        private void txtMaker_TextChanged(object sender, EventArgs e) { lp.LyricsMaker = txtMaker.Text; }

        private void numOffset_ValueChanged(object sender, EventArgs e) { lp.Offset = (int)numOffset.Value; }

        private void txtLength_TextChanged(object sender, EventArgs e) { lp.Length = txtLength.Text; }

        private void txtComment_TextChanged(object sender, EventArgs e) { lp.Comment = txtComment.Text; }

        private void textBox1_TextChanged(object sender, EventArgs e) { lp.PurelrcCode = textBox1.Text; }

        string _MusicPath;
        public string MusicPath
        {
            get { return _MusicPath; }
            set
            {
                _MusicPath = value;
                if (value == null || value == "") { openFileDialog1.FileName = saveFileDialog1.FileName = ""; return; }
                string Path = null;
                try { Path =value.Substring(value.LastIndexOf("\\") + 1); }catch{}
                if (How) this.Text = "싱크 가사 입력" + (Path == "" || Path == null ? "" : " - " + Path);
                else this.Text = "싱크 가사 편집" + (Path == "" || Path == null ? "" : " - " + Path);

                openFileDialog1.FileName = value.Remove(value.LastIndexOf(".")) + ".lrc";
                saveFileDialog1.FileName = value.Remove(value.LastIndexOf("."))+".lrc";
            }
        }

        string _Title;
        public string Title
        {
            get { return _Title; }
            set
            {
                _Title = value;
                if (How) this.Text = "싱크 가사 입력" + value == "" || value == null ? "" : " - " + value;
                else this.Text = "싱크 가사 편집" + value == "" || value == null ? "" : " - " + value; 
            }
        }
        public bool How;
        private void 전체보기ToolStripMenuItem_CheckedChanged(object sender, EventArgs e)
        {
            textBox2.Visible = 전체보기ToolStripMenuItem.Checked;
            How = 전체보기ToolStripMenuItem.Checked;
        }

        private void 원본ToolStripMenuItem_CheckedChanged(object sender, EventArgs e)
        {
            if (원본ToolStripMenuItem.Checked)
            { textBox2.Text = Orignal; }
            else { textBox2.Text = lp.ToString(); }
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {
            if (!원본ToolStripMenuItem.Checked)
            { lp.LrcCode = textBox2.Text; }
        }

        private void 새로고침ToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void 싱크가사다른이름으로저장ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (saveFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            { System.IO.File.WriteAllText(saveFileDialog1.FileName, this.Lyrics); }
        }

        private void 닫기ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        bool Edited;
        private void Lyrics_Edited(object sender, EventArgs e)
        {
            Edited = true;
            if (자동적용ToolStripMenuItem.Checked) button1_Click(null, null);
        }

        private void 자동업데이트ToolStripMenuItem_CheckedChanged(object sender, EventArgs e)
        {
            if(자동업데이트ToolStripMenuItem.Checked)
            MessageBox.Show("반드시 가사를 수정하고 나면 적용버튼을 눌러서 저장해주세요!!\n노래가 바뀌게되면 저용안한 가사들은 변경된 노래의 가사로 바뀝니다.\n\n"+
            "만약 원하지 않으신다면 [설정(T)]에서 [자동 업데이트]를 체크 해제해주시기 바랍니다.","싱크가사 직접 입력 경고!!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            else MessageBox.Show("가사를 수정했는데 만약 노래가 바뀌고나서 적용버튼을 누르면\n바뀐 노래의 싱크가사가 지금 수정하신 가사로 적용됩니다.\n\n"+
            "만약 수정할때마다 적용하고 싶으시면 [설정(T)]에서 [자동 적용]을 체크하여주시기 바랍니다.","싱크가사 직접 입력 경고!!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
        }
    }

}
