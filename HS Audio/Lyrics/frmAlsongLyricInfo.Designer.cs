﻿namespace HS_Audio.Lyrics
{
    partial class frmAlsongLyricInfo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtFirstEMail = new System.Windows.Forms.TextBox();
            this.txtFirstComment = new System.Windows.Forms.TextBox();
            this.txtFirstPhone = new System.Windows.Forms.TextBox();
            this.txtFirstURL = new System.Windows.Forms.TextBox();
            this.txtFirstName = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.lblFirstURL = new System.Windows.Forms.Label();
            this.lblFirstEMail = new System.Windows.Forms.Label();
            this.lblFirstName = new System.Windows.Forms.Label();
            this.lblInfoID = new System.Windows.Forms.Label();
            this.lblStatusID = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.txtEMail = new System.Windows.Forms.TextBox();
            this.txtComment = new System.Windows.Forms.TextBox();
            this.txtPhone = new System.Windows.Forms.TextBox();
            this.txtURL = new System.Windows.Forms.TextBox();
            this.txtName = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.txtOriginText = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txtDate = new System.Windows.Forms.TextBox();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.파일FToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.원본XML스키마저장SToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.xML스키마저장ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.닫기XToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.설정TToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.자동업데이트ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveFileDialog2 = new System.Windows.Forms.SaveFileDialog();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.lblAlbum = new System.Windows.Forms.Label();
            this.lblArtist = new System.Windows.Forms.Label();
            this.txtAlbum = new System.Windows.Forms.TextBox();
            this.txtArtist = new System.Windows.Forms.TextBox();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.txtFirstEMail);
            this.groupBox1.Controls.Add(this.txtFirstComment);
            this.groupBox1.Controls.Add(this.txtFirstPhone);
            this.groupBox1.Controls.Add(this.txtFirstURL);
            this.groupBox1.Controls.Add(this.txtFirstName);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.lblFirstURL);
            this.groupBox1.Controls.Add(this.lblFirstEMail);
            this.groupBox1.Controls.Add(this.lblFirstName);
            this.groupBox1.ForeColor = System.Drawing.Color.Blue;
            this.groupBox1.Location = new System.Drawing.Point(1, 87);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(649, 116);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "등록자 정보";
            // 
            // txtFirstEMail
            // 
            this.txtFirstEMail.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtFirstEMail.BackColor = System.Drawing.SystemColors.Control;
            this.txtFirstEMail.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtFirstEMail.Location = new System.Drawing.Point(73, 35);
            this.txtFirstEMail.Name = "txtFirstEMail";
            this.txtFirstEMail.ReadOnly = true;
            this.txtFirstEMail.Size = new System.Drawing.Size(570, 14);
            this.txtFirstEMail.TabIndex = 9;
            // 
            // txtFirstComment
            // 
            this.txtFirstComment.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtFirstComment.BackColor = System.Drawing.SystemColors.Control;
            this.txtFirstComment.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtFirstComment.Location = new System.Drawing.Point(73, 93);
            this.txtFirstComment.Name = "txtFirstComment";
            this.txtFirstComment.ReadOnly = true;
            this.txtFirstComment.Size = new System.Drawing.Size(570, 14);
            this.txtFirstComment.TabIndex = 8;
            // 
            // txtFirstPhone
            // 
            this.txtFirstPhone.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtFirstPhone.BackColor = System.Drawing.SystemColors.Control;
            this.txtFirstPhone.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtFirstPhone.Location = new System.Drawing.Point(73, 74);
            this.txtFirstPhone.Name = "txtFirstPhone";
            this.txtFirstPhone.ReadOnly = true;
            this.txtFirstPhone.Size = new System.Drawing.Size(570, 14);
            this.txtFirstPhone.TabIndex = 7;
            // 
            // txtFirstURL
            // 
            this.txtFirstURL.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtFirstURL.BackColor = System.Drawing.SystemColors.Control;
            this.txtFirstURL.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtFirstURL.Location = new System.Drawing.Point(73, 55);
            this.txtFirstURL.Name = "txtFirstURL";
            this.txtFirstURL.ReadOnly = true;
            this.txtFirstURL.Size = new System.Drawing.Size(570, 14);
            this.txtFirstURL.TabIndex = 6;
            // 
            // txtFirstName
            // 
            this.txtFirstName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtFirstName.BackColor = System.Drawing.SystemColors.Control;
            this.txtFirstName.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtFirstName.Location = new System.Drawing.Point(73, 15);
            this.txtFirstName.Name = "txtFirstName";
            this.txtFirstName.ReadOnly = true;
            this.txtFirstName.Size = new System.Drawing.Size(570, 14);
            this.txtFirstName.TabIndex = 5;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(6, 92);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(61, 12);
            this.label2.TabIndex = 4;
            this.label2.Text = "코멘트    :";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(6, 73);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(61, 12);
            this.label1.TabIndex = 3;
            this.label1.Text = "전화번호 :";
            // 
            // lblFirstURL
            // 
            this.lblFirstURL.AutoSize = true;
            this.lblFirstURL.ForeColor = System.Drawing.Color.Black;
            this.lblFirstURL.Location = new System.Drawing.Point(7, 54);
            this.lblFirstURL.Name = "lblFirstURL";
            this.lblFirstURL.Size = new System.Drawing.Size(60, 12);
            this.lblFirstURL.TabIndex = 2;
            this.lblFirstURL.Text = "URL       :";
            // 
            // lblFirstEMail
            // 
            this.lblFirstEMail.AutoSize = true;
            this.lblFirstEMail.ForeColor = System.Drawing.Color.Black;
            this.lblFirstEMail.Location = new System.Drawing.Point(6, 35);
            this.lblFirstEMail.Name = "lblFirstEMail";
            this.lblFirstEMail.Size = new System.Drawing.Size(61, 12);
            this.lblFirstEMail.TabIndex = 1;
            this.lblFirstEMail.Text = "이메일    :";
            // 
            // lblFirstName
            // 
            this.lblFirstName.AutoSize = true;
            this.lblFirstName.ForeColor = System.Drawing.Color.Black;
            this.lblFirstName.Location = new System.Drawing.Point(6, 17);
            this.lblFirstName.Name = "lblFirstName";
            this.lblFirstName.Size = new System.Drawing.Size(61, 12);
            this.lblFirstName.TabIndex = 0;
            this.lblFirstName.Text = "이름       :";
            // 
            // lblInfoID
            // 
            this.lblInfoID.AutoSize = true;
            this.lblInfoID.Location = new System.Drawing.Point(7, 32);
            this.lblInfoID.Name = "lblInfoID";
            this.lblInfoID.Size = new System.Drawing.Size(90, 12);
            this.lblInfoID.TabIndex = 1;
            this.lblInfoID.Text = "가사 정보 ID : 0";
            // 
            // lblStatusID
            // 
            this.lblStatusID.AutoSize = true;
            this.lblStatusID.Location = new System.Drawing.Point(7, 52);
            this.lblStatusID.Name = "lblStatusID";
            this.lblStatusID.Size = new System.Drawing.Size(90, 12);
            this.lblStatusID.TabIndex = 2;
            this.lblStatusID.Text = "가사 상태 ID : 0";
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox2.Controls.Add(this.txtEMail);
            this.groupBox2.Controls.Add(this.txtComment);
            this.groupBox2.Controls.Add(this.txtPhone);
            this.groupBox2.Controls.Add(this.txtURL);
            this.groupBox2.Controls.Add(this.txtName);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.ForeColor = System.Drawing.Color.Blue;
            this.groupBox2.Location = new System.Drawing.Point(1, 209);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(649, 116);
            this.groupBox2.TabIndex = 10;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "수정자 정보";
            // 
            // txtEMail
            // 
            this.txtEMail.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtEMail.BackColor = System.Drawing.SystemColors.Control;
            this.txtEMail.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtEMail.Location = new System.Drawing.Point(73, 35);
            this.txtEMail.Name = "txtEMail";
            this.txtEMail.ReadOnly = true;
            this.txtEMail.Size = new System.Drawing.Size(570, 14);
            this.txtEMail.TabIndex = 9;
            // 
            // txtComment
            // 
            this.txtComment.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtComment.BackColor = System.Drawing.SystemColors.Control;
            this.txtComment.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtComment.Location = new System.Drawing.Point(73, 93);
            this.txtComment.Name = "txtComment";
            this.txtComment.ReadOnly = true;
            this.txtComment.Size = new System.Drawing.Size(570, 14);
            this.txtComment.TabIndex = 8;
            // 
            // txtPhone
            // 
            this.txtPhone.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtPhone.BackColor = System.Drawing.SystemColors.Control;
            this.txtPhone.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtPhone.Location = new System.Drawing.Point(73, 74);
            this.txtPhone.Name = "txtPhone";
            this.txtPhone.ReadOnly = true;
            this.txtPhone.Size = new System.Drawing.Size(570, 14);
            this.txtPhone.TabIndex = 7;
            // 
            // txtURL
            // 
            this.txtURL.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtURL.BackColor = System.Drawing.SystemColors.Control;
            this.txtURL.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtURL.Location = new System.Drawing.Point(73, 55);
            this.txtURL.Name = "txtURL";
            this.txtURL.ReadOnly = true;
            this.txtURL.Size = new System.Drawing.Size(570, 14);
            this.txtURL.TabIndex = 6;
            // 
            // txtName
            // 
            this.txtName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtName.BackColor = System.Drawing.SystemColors.Control;
            this.txtName.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtName.Location = new System.Drawing.Point(73, 15);
            this.txtName.Name = "txtName";
            this.txtName.ReadOnly = true;
            this.txtName.Size = new System.Drawing.Size(570, 14);
            this.txtName.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(6, 92);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(61, 12);
            this.label3.TabIndex = 4;
            this.label3.Text = "코멘트    :";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(6, 73);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(61, 12);
            this.label4.TabIndex = 3;
            this.label4.Text = "전화번호 :";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(7, 54);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(60, 12);
            this.label5.TabIndex = 2;
            this.label5.Text = "URL       :";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(6, 35);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(61, 12);
            this.label6.TabIndex = 1;
            this.label6.Text = "이메일    :";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.ForeColor = System.Drawing.Color.Black;
            this.label7.Location = new System.Drawing.Point(6, 17);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(61, 12);
            this.label7.TabIndex = 0;
            this.label7.Text = "이름       :";
            // 
            // label8
            // 
            this.label8.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(5, 332);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(63, 12);
            this.label8.TabIndex = 11;
            this.label8.Text = "원본 XML:";
            // 
            // txtOriginText
            // 
            this.txtOriginText.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtOriginText.BackColor = System.Drawing.SystemColors.Control;
            this.txtOriginText.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtOriginText.Location = new System.Drawing.Point(74, 332);
            this.txtOriginText.Name = "txtOriginText";
            this.txtOriginText.ReadOnly = true;
            this.txtOriginText.Size = new System.Drawing.Size(573, 14);
            this.txtOriginText.TabIndex = 10;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(6, 71);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(81, 12);
            this.label9.TabIndex = 12;
            this.label9.Text = "등록 날짜     :";
            // 
            // txtDate
            // 
            this.txtDate.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtDate.BackColor = System.Drawing.SystemColors.Control;
            this.txtDate.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtDate.Location = new System.Drawing.Point(88, 71);
            this.txtDate.Name = "txtDate";
            this.txtDate.ReadOnly = true;
            this.txtDate.Size = new System.Drawing.Size(558, 14);
            this.txtDate.TabIndex = 10;
            this.txtDate.Text = "0000-00-00 00:00:00.000";
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.파일FToolStripMenuItem,
            this.설정TToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(651, 24);
            this.menuStrip1.TabIndex = 13;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // 파일FToolStripMenuItem
            // 
            this.파일FToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.원본XML스키마저장SToolStripMenuItem,
            this.xML스키마저장ToolStripMenuItem,
            this.toolStripSeparator1,
            this.닫기XToolStripMenuItem});
            this.파일FToolStripMenuItem.Name = "파일FToolStripMenuItem";
            this.파일FToolStripMenuItem.Size = new System.Drawing.Size(57, 20);
            this.파일FToolStripMenuItem.Text = "파일(&F)";
            // 
            // 원본XML스키마저장SToolStripMenuItem
            // 
            this.원본XML스키마저장SToolStripMenuItem.Name = "원본XML스키마저장SToolStripMenuItem";
            this.원본XML스키마저장SToolStripMenuItem.Size = new System.Drawing.Size(209, 22);
            this.원본XML스키마저장SToolStripMenuItem.Text = "원본 XML 스키마 저장(&S)";
            this.원본XML스키마저장SToolStripMenuItem.Click += new System.EventHandler(this.원본XML스키마저장SToolStripMenuItem_Click);
            // 
            // xML스키마저장ToolStripMenuItem
            // 
            this.xML스키마저장ToolStripMenuItem.Name = "xML스키마저장ToolStripMenuItem";
            this.xML스키마저장ToolStripMenuItem.Size = new System.Drawing.Size(209, 22);
            this.xML스키마저장ToolStripMenuItem.Text = "XML 스키마 저장(&S)";
            this.xML스키마저장ToolStripMenuItem.Click += new System.EventHandler(this.XML스키마저장ToolStripMenuItem_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(206, 6);
            // 
            // 닫기XToolStripMenuItem
            // 
            this.닫기XToolStripMenuItem.Name = "닫기XToolStripMenuItem";
            this.닫기XToolStripMenuItem.Size = new System.Drawing.Size(209, 22);
            this.닫기XToolStripMenuItem.Text = "닫기(&X)";
            // 
            // 설정TToolStripMenuItem
            // 
            this.설정TToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.자동업데이트ToolStripMenuItem});
            this.설정TToolStripMenuItem.Name = "설정TToolStripMenuItem";
            this.설정TToolStripMenuItem.Size = new System.Drawing.Size(57, 20);
            this.설정TToolStripMenuItem.Text = "설정(&T)";
            // 
            // 자동업데이트ToolStripMenuItem
            // 
            this.자동업데이트ToolStripMenuItem.Checked = true;
            this.자동업데이트ToolStripMenuItem.CheckOnClick = true;
            this.자동업데이트ToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.자동업데이트ToolStripMenuItem.Name = "자동업데이트ToolStripMenuItem";
            this.자동업데이트ToolStripMenuItem.Size = new System.Drawing.Size(150, 22);
            this.자동업데이트ToolStripMenuItem.Text = "자동 업데이트";
            // 
            // saveFileDialog2
            // 
            this.saveFileDialog2.DefaultExt = "*.xml";
            this.saveFileDialog2.Filter = "XML 스키마 (*.xml)|*.xml|텍스트 파일 (*.txt)|*.xml|모든 파일 (*.*)|*.*";
            this.saveFileDialog2.Title = "XML파일 저장...";
            // 
            // saveFileDialog1
            // 
            this.saveFileDialog1.DefaultExt = "*.xml";
            this.saveFileDialog1.Filter = "XML 스키마 (*.xml)|*.xml|텍스트 파일 (*.txt)|*.xml|모든 파일 (*.*)|*.*";
            this.saveFileDialog1.Title = "원본 XML파일 저장...";
            // 
            // lblAlbum
            // 
            this.lblAlbum.AutoSize = true;
            this.lblAlbum.Location = new System.Drawing.Point(270, 32);
            this.lblAlbum.Name = "lblAlbum";
            this.lblAlbum.Size = new System.Drawing.Size(61, 12);
            this.lblAlbum.TabIndex = 14;
            this.lblAlbum.Text = "앨범명    :";
            // 
            // lblArtist
            // 
            this.lblArtist.AutoSize = true;
            this.lblArtist.Location = new System.Drawing.Point(270, 51);
            this.lblArtist.Name = "lblArtist";
            this.lblArtist.Size = new System.Drawing.Size(61, 12);
            this.lblArtist.TabIndex = 15;
            this.lblArtist.Text = "아티스트 :";
            // 
            // txtAlbum
            // 
            this.txtAlbum.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtAlbum.BackColor = System.Drawing.SystemColors.Control;
            this.txtAlbum.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtAlbum.Location = new System.Drawing.Point(332, 32);
            this.txtAlbum.Name = "txtAlbum";
            this.txtAlbum.ReadOnly = true;
            this.txtAlbum.Size = new System.Drawing.Size(315, 14);
            this.txtAlbum.TabIndex = 10;
            // 
            // txtArtist
            // 
            this.txtArtist.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtArtist.BackColor = System.Drawing.SystemColors.Control;
            this.txtArtist.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtArtist.Location = new System.Drawing.Point(332, 52);
            this.txtArtist.Name = "txtArtist";
            this.txtArtist.ReadOnly = true;
            this.txtArtist.Size = new System.Drawing.Size(315, 14);
            this.txtArtist.TabIndex = 16;
            // 
            // frmAlsongLyricInfo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(651, 352);
            this.Controls.Add(this.txtAlbum);
            this.Controls.Add(this.txtArtist);
            this.Controls.Add(this.txtDate);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.txtOriginText);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.lblStatusID);
            this.Controls.Add(this.lblInfoID);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.lblAlbum);
            this.Controls.Add(this.lblArtist);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "frmAlsongLyricInfo";
            this.ShowIcon = false;
            this.Text = "가사 등록 정보 자세히 보기";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmAlsongLyricInfo_FormClosing);
            this.Load += new System.EventHandler(this.frmAlsongLyricInfo_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label lblFirstEMail;
        private System.Windows.Forms.Label lblFirstName;
        private System.Windows.Forms.Label lblInfoID;
        private System.Windows.Forms.Label lblStatusID;
        private System.Windows.Forms.Label lblFirstURL;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtFirstEMail;
        private System.Windows.Forms.TextBox txtFirstComment;
        private System.Windows.Forms.TextBox txtFirstPhone;
        private System.Windows.Forms.TextBox txtFirstURL;
        private System.Windows.Forms.TextBox txtFirstName;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox txtEMail;
        private System.Windows.Forms.TextBox txtComment;
        private System.Windows.Forms.TextBox txtPhone;
        private System.Windows.Forms.TextBox txtURL;
        private System.Windows.Forms.TextBox txtName;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtOriginText;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtDate;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem 파일FToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem xML스키마저장ToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem 닫기XToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 설정TToolStripMenuItem;
        internal System.Windows.Forms.ToolStripMenuItem 자동업데이트ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 원본XML스키마저장SToolStripMenuItem;
        private System.Windows.Forms.SaveFileDialog saveFileDialog2;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.Label lblAlbum;
        private System.Windows.Forms.Label lblArtist;
        private System.Windows.Forms.TextBox txtAlbum;
        private System.Windows.Forms.TextBox txtArtist;
    }
}