﻿using System.Text;
namespace HS_Audio
{
    partial class frmMain
    {
        /// <summary>
        /// 필수 디자이너 변수입니다.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 사용 중인 모든 리소스를 정리합니다.
        /// </summary>
        /// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
        protected override void Dispose(bool disposing)
        {
            StringBuilder sb = new StringBuilder("#EXTM3U");
            foreach (System.Windows.Forms.ListViewItem a in listView1.Items) sb.Append("\r\n" + a.SubItems[3].Text);
            Program.Playlist = sb.ToString();

            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
            try{if (NextMusicThread != null) NextMusicThread.Abort();}catch{}
            try{if (LyricsThread != null) LyricsThread.Abort();}catch{}
        }

        #region Windows Form 디자이너에서 생성한 코드

        /*
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.파일FToolStripMenuItem,
            this.설정ToolStripMenuItem,
            this.보기VToolStripMenuItem,
            this.도움말HToolStripMenuItem});
        */
        /*
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel2,
            this.toolStripProgressBar1,
            this.toolStripStatusLabel3,
            this.toolStripStatusLabel1,
            this.toolStripProgressBar2});
         */

        /// <summary>
        /// 디자이너 지원에 필요한 메서드입니다.
        /// 이 메서드의 내용을 코드 편집기로 수정하지 마십시오.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMain));
            this.btn재생 = new System.Windows.Forms.Button();
            this.btn정지 = new System.Windows.Forms.Button();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.btn추가 = new System.Windows.Forms.Button();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.파일FToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.트레이로보내기ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator21 = new System.Windows.Forms.ToolStripSeparator();
            this.열기OToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.파일열기ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.폴더열기ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.폴더열기ToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator36 = new System.Windows.Forms.ToolStripSeparator();
            this.cbMimetype = new System.Windows.Forms.ToolStripComboBox();
            this.하위폴더검색ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.재생목록열기ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ANISToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.UTF8ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.유니코드ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.재생목록다른이름으로저장ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem4 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator38 = new System.Windows.Forms.ToolStripSeparator();
            this.현재상태저장ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator6 = new System.Windows.Forms.ToolStripSeparator();
            this.프로젝트열기ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.프로젝트저장ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator7 = new System.Windows.Forms.ToolStripSeparator();
            this.파일로웨이브저장ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.원본사이즈사용ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripTextBox1 = new System.Windows.Forms.ToolStripTextBox();
            this.toolStripSeparator8 = new System.Windows.Forms.ToolStripSeparator();
            this.끝내기ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.설정ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.맨위로설정ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.재생목록창꾸미기ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.모눈선표시ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.선택한항목강조표시ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator32 = new System.Windows.Forms.ToolStripSeparator();
            this.예제사진ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.러브라이브1ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.일러스트1ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.글자색상설정ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.배경색상설정ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.삽입색상설정ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator33 = new System.Windows.Forms.ToolStripSeparator();
            this.이미지불러오기ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripComboBox3 = new System.Windows.Forms.ToolStripComboBox();
            this.toolStripComboBox4 = new System.Windows.Forms.ToolStripComboBox();
            this.toolStripSeparator34 = new System.Windows.Forms.ToolStripSeparator();
            this.타일보기ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator31 = new System.Windows.Forms.ToolStripSeparator();
            this.지우기ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator28 = new System.Windows.Forms.ToolStripSeparator();
            this.언어ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripComboBox1 = new System.Windows.Forms.ToolStripComboBox();
            this.기본프로그램으로설정ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator11 = new System.Windows.Forms.ToolStripSeparator();
            this.핫키설정ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator9 = new System.Windows.Forms.ToolStripSeparator();
            this.프리로드모드ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.안전모드ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.asyncToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.음악믹싱모드ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator22 = new System.Windows.Forms.ToolStripSeparator();
            this.파일복구및재설치ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.프로그램다시시작ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator12 = new System.Windows.Forms.ToolStripSeparator();
            this.우선순위ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.높음ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.높은우선순위ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.보통권장ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.낮은우선순위ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.LOL우선순위ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.LOL높은우선순위ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.LOL보통권장ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator25 = new System.Windows.Forms.ToolStripSeparator();
            this.가비지커렉션가동ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.보기VToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.밀리초표시ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator42 = new System.Windows.Forms.ToolStripSeparator();
            this.재생상세정보ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator41 = new System.Windows.Forms.ToolStripSeparator();
            this.스케일모드ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripComboBox5 = new System.Windows.Forms.ToolStripComboBox();
            this.자동스케일ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator24 = new System.Windows.Forms.ToolStripSeparator();
            this.로그관리ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.로그폴더열기ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator29 = new System.Windows.Forms.ToolStripSeparator();
            this.로그지우기ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator13 = new System.Windows.Forms.ToolStripSeparator();
            this.화면끄기ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator39 = new System.Windows.Forms.ToolStripSeparator();
            this.상태표시줄업데이트ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.testToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.가사LToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.싱크가사찾기ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator40 = new System.Windows.Forms.ToolStripSeparator();
            this.싱크가사로컬캐시ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.싱크가사캐시정리ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.태그없는항목자동삽입ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator23 = new System.Windows.Forms.ToolStripSeparator();
            this.싱크가사캐시관리ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.싱크가사우선순위ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator10 = new System.Windows.Forms.ToolStripSeparator();
            this.바탕화면싱크가사창띄우기ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.플러그인PToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.플러그인이존재하지않습니다ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.도움말HToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.정보ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.업데이트변경사항보기ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator14 = new System.Windows.Forms.ToolStripSeparator();
            this.업데이트확인ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.업데이트서버설정ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator26 = new System.Windows.Forms.ToolStripSeparator();
            this.디버깅ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.lbl파일경로 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.btn재생설정 = new System.Windows.Forms.Button();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.재생ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.새인스턴스시작ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.일시정지ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.정지ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.삭제ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.현재위치의웨이브저장ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripTextBox2 = new System.Windows.Forms.ToolStripTextBox();
            this.toolStripSeparator35 = new System.Windows.Forms.ToolStripSeparator();
            this.mD5해쉬값구하기ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.가사찾기ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.복사ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.파일이름ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.파일경로ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.탐색기에서해당파일열기ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.lblSelectMusicPath = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.openFileDialog2 = new System.Windows.Forms.OpenFileDialog();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel2 = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripProgressBar1 = new System.Windows.Forms.ToolStripProgressBar();
            this.toolStripStatusLabel3 = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripProgressBar2 = new System.Windows.Forms.ToolStripProgressBar();
            this.panel1 = new System.Windows.Forms.Panel();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.saveFileDialog2 = new System.Windows.Forms.SaveFileDialog();
            this.timer2 = new System.Windows.Forms.Timer(this.components);
            this.button5 = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.cb반복 = new System.Windows.Forms.ComboBox();
            this.timer3 = new System.Windows.Forms.Timer(this.components);
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.openFileDialog3 = new System.Windows.Forms.OpenFileDialog();
            this.saveFileDialog3 = new System.Windows.Forms.SaveFileDialog();
            this.notifyIcon1 = new System.Windows.Forms.NotifyIcon(this.components);
            this.contextMenuStrip2 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.설정TToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem5 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripComboBox2 = new System.Windows.Forms.ToolStripComboBox();
            this.toolStripSeparator27 = new System.Windows.Forms.ToolStripSeparator();
            this.파일복구및재설치ToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator18 = new System.Windows.Forms.ToolStripSeparator();
            this.프로그램다시시작ToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator19 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripMenuItem7 = new System.Windows.Forms.ToolStripMenuItem();
            this.높음ToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.높은우선순위ToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.보통권장ToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.낮은우선순위ToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.보기VToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.바탕화면싱크가사창띄우기ToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.도움말HToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.정보ToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.업데이트변경사항보기ToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator17 = new System.Windows.Forms.ToolStripSeparator();
            this.디버깅ToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator15 = new System.Windows.Forms.ToolStripSeparator();
            this.재생컨트롤ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.재생ToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.다음ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator37 = new System.Windows.Forms.ToolStripSeparator();
            this.정지ToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator16 = new System.Windows.Forms.ToolStripSeparator();
            this.재생설정열기ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.메인창열기ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator20 = new System.Windows.Forms.ToolStripSeparator();
            this.끝내기XToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.btnExpand = new System.Windows.Forms.Button();
            this.btnScrollDown = new System.Windows.Forms.Button();
            this.btnScrollUp = new System.Windows.Forms.Button();
            this.btnItemFocus = new System.Windows.Forms.Button();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btn프로그램다시시작 = new System.Windows.Forms.Button();
            this.colorDialog1 = new System.Windows.Forms.ColorDialog();
            this.colorDialog2 = new System.Windows.Forms.ColorDialog();
            this.timer_파일추가 = new System.Windows.Forms.Timer(this.components);
            this.btnSearchPlayList = new System.Windows.Forms.Button();
            this.cdLabelText = new System.Windows.Forms.ColorDialog();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.saveFileDialog4 = new System.Windows.Forms.SaveFileDialog();
            this.listView1 = new HS_CSharpUtility.Control.HSListview();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader5 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.menuStrip1.SuspendLayout();
            this.contextMenuStrip1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.contextMenuStrip2.SuspendLayout();
            this.SuspendLayout();
            // 
            // btn재생
            // 
            this.btn재생.Enabled = false;
            this.btn재생.Location = new System.Drawing.Point(3, 75);
            this.btn재생.Margin = new System.Windows.Forms.Padding(4, 2, 4, 2);
            this.btn재생.Name = "btn재생";
            this.btn재생.Size = new System.Drawing.Size(74, 22);
            this.btn재생.TabIndex = 0;
            this.btn재생.Text = "재생";
            this.btn재생.UseVisualStyleBackColor = true;
            this.btn재생.EnabledChanged += new System.EventHandler(this.button1_EnabledChanged);
            this.btn재생.Click += new System.EventHandler(this.button1_Click);
            // 
            // btn정지
            // 
            this.btn정지.Enabled = false;
            this.btn정지.Location = new System.Drawing.Point(0, 0);
            this.btn정지.Margin = new System.Windows.Forms.Padding(4, 2, 4, 2);
            this.btn정지.Name = "btn정지";
            this.btn정지.Size = new System.Drawing.Size(74, 22);
            this.btn정지.TabIndex = 2;
            this.btn정지.Text = "정지";
            this.btn정지.UseVisualStyleBackColor = true;
            this.btn정지.EnabledChanged += new System.EventHandler(this.btn정지_EnabledChanged);
            this.btn정지.Click += new System.EventHandler(this.button2_Click);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.DefaultExt = "*.mp3; *.ogg; *.wav; *.wma; *.flac; *.aif; *.aiff; *.mid";
            this.openFileDialog1.Filter = resources.GetString("openFileDialog1.Filter");
            this.openFileDialog1.Multiselect = true;
            this.openFileDialog1.Title = "음악파일 열기";
            this.openFileDialog1.FileOk += new System.ComponentModel.CancelEventHandler(this.openFileDialog1_FileOk);
            // 
            // btn추가
            // 
            this.btn추가.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn추가.Location = new System.Drawing.Point(522, 2);
            this.btn추가.Margin = new System.Windows.Forms.Padding(4, 2, 4, 2);
            this.btn추가.Name = "btn추가";
            this.btn추가.Size = new System.Drawing.Size(52, 21);
            this.btn추가.TabIndex = 3;
            this.btn추가.Text = "추가";
            this.btn추가.UseVisualStyleBackColor = true;
            this.btn추가.Click += new System.EventHandler(this.button3_Click);
            // 
            // menuStrip1
            // 
            this.menuStrip1.AllowItemReorder = true;
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.파일FToolStripMenuItem,
            this.설정ToolStripMenuItem,
            this.보기VToolStripMenuItem,
            this.가사LToolStripMenuItem,
            this.플러그인PToolStripMenuItem,
            this.도움말HToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional;
            this.menuStrip1.Size = new System.Drawing.Size(578, 24);
            this.menuStrip1.TabIndex = 4;
            this.menuStrip1.TabStop = true;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // 파일FToolStripMenuItem
            // 
            this.파일FToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.트레이로보내기ToolStripMenuItem,
            this.toolStripSeparator21,
            this.열기OToolStripMenuItem,
            this.toolStripSeparator5,
            this.재생목록열기ToolStripMenuItem,
            this.재생목록다른이름으로저장ToolStripMenuItem,
            this.toolStripSeparator38,
            this.현재상태저장ToolStripMenuItem,
            this.toolStripSeparator6,
            this.프로젝트열기ToolStripMenuItem,
            this.프로젝트저장ToolStripMenuItem,
            this.toolStripSeparator7,
            this.파일로웨이브저장ToolStripMenuItem,
            this.toolStripSeparator8,
            this.끝내기ToolStripMenuItem});
            this.파일FToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.파일FToolStripMenuItem.Name = "파일FToolStripMenuItem";
            this.파일FToolStripMenuItem.ShowShortcutKeys = false;
            this.파일FToolStripMenuItem.Size = new System.Drawing.Size(57, 20);
            this.파일FToolStripMenuItem.Text = "파일(&F)";
            this.파일FToolStripMenuItem.DropDownOpening += new System.EventHandler(this.파일FToolStripMenuItem_DropDownOpening);
            // 
            // 트레이로보내기ToolStripMenuItem
            // 
            this.트레이로보내기ToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("트레이로보내기ToolStripMenuItem.Image")));
            this.트레이로보내기ToolStripMenuItem.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.트레이로보내기ToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.트레이로보내기ToolStripMenuItem.Name = "트레이로보내기ToolStripMenuItem";
            this.트레이로보내기ToolStripMenuItem.Padding = new System.Windows.Forms.Padding(0);
            this.트레이로보내기ToolStripMenuItem.Size = new System.Drawing.Size(339, 20);
            this.트레이로보내기ToolStripMenuItem.Text = "트레이로 보내기";
            this.트레이로보내기ToolStripMenuItem.Click += new System.EventHandler(this.트레이로보내기ToolStripMenuItem_Click);
            // 
            // toolStripSeparator21
            // 
            this.toolStripSeparator21.Name = "toolStripSeparator21";
            this.toolStripSeparator21.Size = new System.Drawing.Size(336, 6);
            // 
            // 열기OToolStripMenuItem
            // 
            this.열기OToolStripMenuItem.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.열기OToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.파일열기ToolStripMenuItem,
            this.폴더열기ToolStripMenuItem});
            this.열기OToolStripMenuItem.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.열기OToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.열기OToolStripMenuItem.Name = "열기OToolStripMenuItem";
            this.열기OToolStripMenuItem.Padding = new System.Windows.Forms.Padding(0);
            this.열기OToolStripMenuItem.Size = new System.Drawing.Size(339, 20);
            this.열기OToolStripMenuItem.Text = "열기(&O)";
            // 
            // 파일열기ToolStripMenuItem
            // 
            this.파일열기ToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("파일열기ToolStripMenuItem.Image")));
            this.파일열기ToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.파일열기ToolStripMenuItem.Name = "파일열기ToolStripMenuItem";
            this.파일열기ToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.O)));
            this.파일열기ToolStripMenuItem.Size = new System.Drawing.Size(169, 22);
            this.파일열기ToolStripMenuItem.Text = "파일 열기";
            this.파일열기ToolStripMenuItem.ToolTipText = "파일을 직접 엽니다.";
            this.파일열기ToolStripMenuItem.Click += new System.EventHandler(this.파일열기ToolStripMenuItem_Click);
            // 
            // 폴더열기ToolStripMenuItem
            // 
            this.폴더열기ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.폴더열기ToolStripMenuItem1,
            this.toolStripSeparator36,
            this.cbMimetype,
            this.하위폴더검색ToolStripMenuItem});
            this.폴더열기ToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("폴더열기ToolStripMenuItem.Image")));
            this.폴더열기ToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.폴더열기ToolStripMenuItem.Name = "폴더열기ToolStripMenuItem";
            this.폴더열기ToolStripMenuItem.Size = new System.Drawing.Size(169, 22);
            this.폴더열기ToolStripMenuItem.Text = "폴더 열기";
            this.폴더열기ToolStripMenuItem.ToolTipText = "지정한 폴더에있는 음악파일을 모두 목록에 추가합니다.";
            this.폴더열기ToolStripMenuItem.Click += new System.EventHandler(this.폴더열기ToolStripMenuItem_Click);
            // 
            // 폴더열기ToolStripMenuItem1
            // 
            this.폴더열기ToolStripMenuItem1.Image = ((System.Drawing.Image)(resources.GetObject("폴더열기ToolStripMenuItem1.Image")));
            this.폴더열기ToolStripMenuItem1.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.폴더열기ToolStripMenuItem1.Name = "폴더열기ToolStripMenuItem1";
            this.폴더열기ToolStripMenuItem1.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.D)));
            this.폴더열기ToolStripMenuItem1.Size = new System.Drawing.Size(290, 22);
            this.폴더열기ToolStripMenuItem1.Text = "폴더 열기";
            this.폴더열기ToolStripMenuItem1.ToolTipText = "지정한 폴더에있는 음악파일을 모두 목록에 추가합니다.";
            this.폴더열기ToolStripMenuItem1.Click += new System.EventHandler(this.폴더열기ToolStripMenuItem_Click);
            // 
            // toolStripSeparator36
            // 
            this.toolStripSeparator36.Name = "toolStripSeparator36";
            this.toolStripSeparator36.Size = new System.Drawing.Size(287, 6);
            // 
            // cbMimetype
            // 
            this.cbMimetype.AutoSize = false;
            this.cbMimetype.DropDownHeight = 200;
            this.cbMimetype.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbMimetype.DropDownWidth = 300;
            this.cbMimetype.IntegralHeight = false;
            this.cbMimetype.Items.AddRange(new object[] {
            "지원하는 모든 음악 파일",
            "MP3 Audio 파일 (*.mp3)",
            "Windows Media Audio 파일 (*.wma)",
            "Wave (Microsoft) 파일 (*.wav)",
            "OggVorbis 파일 (*.ogg)",
            "Free Lossless Audio Codec 파일 (*.flac)",
            "Audio Interchange File Format 파일 (*.aif, *.aiff)",
            "Standard MIDI 파일 (*.mid)",
            "지원하는 모든 특수 파일",
            "FMOD\'s Sample Bank 파일 (*.fsb)",
            "Windows Media Audio Redirector 파일 (*.wax)",
            "네트워크 스트리밍 파일 (*.asf, *.asx, *.dls)"});
            this.cbMimetype.Name = "cbMimetype";
            this.cbMimetype.Size = new System.Drawing.Size(230, 23);
            this.cbMimetype.ToolTipText = "폴더에서 열때 음악 파일 유형을 선택합니다.";
            // 
            // 하위폴더검색ToolStripMenuItem
            // 
            this.하위폴더검색ToolStripMenuItem.Checked = true;
            this.하위폴더검색ToolStripMenuItem.CheckOnClick = true;
            this.하위폴더검색ToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.하위폴더검색ToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.하위폴더검색ToolStripMenuItem.Name = "하위폴더검색ToolStripMenuItem";
            this.하위폴더검색ToolStripMenuItem.Size = new System.Drawing.Size(290, 22);
            this.하위폴더검색ToolStripMenuItem.Text = "하위 폴더 검색";
            this.하위폴더검색ToolStripMenuItem.CheckedChanged += new System.EventHandler(this.하위폴더검색ToolStripMenuItem_CheckedChanged);
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(336, 6);
            // 
            // 재생목록열기ToolStripMenuItem
            // 
            this.재생목록열기ToolStripMenuItem.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.재생목록열기ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ANISToolStripMenuItem,
            this.UTF8ToolStripMenuItem,
            this.유니코드ToolStripMenuItem});
            this.재생목록열기ToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("재생목록열기ToolStripMenuItem.Image")));
            this.재생목록열기ToolStripMenuItem.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.재생목록열기ToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.재생목록열기ToolStripMenuItem.Name = "재생목록열기ToolStripMenuItem";
            this.재생목록열기ToolStripMenuItem.Padding = new System.Windows.Forms.Padding(0);
            this.재생목록열기ToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.L)));
            this.재생목록열기ToolStripMenuItem.Size = new System.Drawing.Size(339, 20);
            this.재생목록열기ToolStripMenuItem.Text = "재생목록 열기(&L)";
            this.재생목록열기ToolStripMenuItem.Click += new System.EventHandler(this.재생목록열기ToolStripMenuItem_Click);
            // 
            // ANISToolStripMenuItem
            // 
            this.ANISToolStripMenuItem.Checked = true;
            this.ANISToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.ANISToolStripMenuItem.Name = "ANISToolStripMenuItem";
            this.ANISToolStripMenuItem.Size = new System.Drawing.Size(122, 22);
            this.ANISToolStripMenuItem.Text = "ANIS";
            this.ANISToolStripMenuItem.Visible = false;
            // 
            // UTF8ToolStripMenuItem
            // 
            this.UTF8ToolStripMenuItem.Name = "UTF8ToolStripMenuItem";
            this.UTF8ToolStripMenuItem.Size = new System.Drawing.Size(122, 22);
            this.UTF8ToolStripMenuItem.Text = "UTF-8";
            this.UTF8ToolStripMenuItem.Visible = false;
            // 
            // 유니코드ToolStripMenuItem
            // 
            this.유니코드ToolStripMenuItem.Name = "유니코드ToolStripMenuItem";
            this.유니코드ToolStripMenuItem.Size = new System.Drawing.Size(122, 22);
            this.유니코드ToolStripMenuItem.Text = "유니코드";
            this.유니코드ToolStripMenuItem.Visible = false;
            // 
            // 재생목록다른이름으로저장ToolStripMenuItem
            // 
            this.재생목록다른이름으로저장ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem1,
            this.toolStripMenuItem3,
            this.toolStripMenuItem4});
            this.재생목록다른이름으로저장ToolStripMenuItem.Enabled = false;
            this.재생목록다른이름으로저장ToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("재생목록다른이름으로저장ToolStripMenuItem.Image")));
            this.재생목록다른이름으로저장ToolStripMenuItem.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.재생목록다른이름으로저장ToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.재생목록다른이름으로저장ToolStripMenuItem.Name = "재생목록다른이름으로저장ToolStripMenuItem";
            this.재생목록다른이름으로저장ToolStripMenuItem.Padding = new System.Windows.Forms.Padding(0);
            this.재생목록다른이름으로저장ToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
            this.재생목록다른이름으로저장ToolStripMenuItem.Size = new System.Drawing.Size(339, 20);
            this.재생목록다른이름으로저장ToolStripMenuItem.Text = "재생목록 저장(&S)";
            this.재생목록다른이름으로저장ToolStripMenuItem.Click += new System.EventHandler(this.재생목록다른이름으로저장ToolStripMenuItem_Click);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Checked = true;
            this.toolStripMenuItem1.CheckState = System.Windows.Forms.CheckState.Checked;
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(122, 22);
            this.toolStripMenuItem1.Text = "ANIS";
            this.toolStripMenuItem1.Visible = false;
            // 
            // toolStripMenuItem3
            // 
            this.toolStripMenuItem3.Name = "toolStripMenuItem3";
            this.toolStripMenuItem3.Size = new System.Drawing.Size(122, 22);
            this.toolStripMenuItem3.Text = "UTF-8";
            this.toolStripMenuItem3.Visible = false;
            // 
            // toolStripMenuItem4
            // 
            this.toolStripMenuItem4.Name = "toolStripMenuItem4";
            this.toolStripMenuItem4.Size = new System.Drawing.Size(122, 22);
            this.toolStripMenuItem4.Text = "유니코드";
            this.toolStripMenuItem4.Visible = false;
            // 
            // toolStripSeparator38
            // 
            this.toolStripSeparator38.Name = "toolStripSeparator38";
            this.toolStripSeparator38.Size = new System.Drawing.Size(336, 6);
            // 
            // 현재상태저장ToolStripMenuItem
            // 
            this.현재상태저장ToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("현재상태저장ToolStripMenuItem.Image")));
            this.현재상태저장ToolStripMenuItem.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.현재상태저장ToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.현재상태저장ToolStripMenuItem.Name = "현재상태저장ToolStripMenuItem";
            this.현재상태저장ToolStripMenuItem.Padding = new System.Windows.Forms.Padding(0);
            this.현재상태저장ToolStripMenuItem.Size = new System.Drawing.Size(339, 20);
            this.현재상태저장ToolStripMenuItem.Text = "현재 상태 저장";
            this.현재상태저장ToolStripMenuItem.Click += new System.EventHandler(this.현재상태저장ToolStripMenuItem_Click);
            // 
            // toolStripSeparator6
            // 
            this.toolStripSeparator6.Name = "toolStripSeparator6";
            this.toolStripSeparator6.Size = new System.Drawing.Size(336, 6);
            this.toolStripSeparator6.Visible = false;
            // 
            // 프로젝트열기ToolStripMenuItem
            // 
            this.프로젝트열기ToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("프로젝트열기ToolStripMenuItem.Image")));
            this.프로젝트열기ToolStripMenuItem.Name = "프로젝트열기ToolStripMenuItem";
            this.프로젝트열기ToolStripMenuItem.Size = new System.Drawing.Size(339, 22);
            this.프로젝트열기ToolStripMenuItem.Text = "프로젝트 열기";
            this.프로젝트열기ToolStripMenuItem.Visible = false;
            this.프로젝트열기ToolStripMenuItem.Click += new System.EventHandler(this.프로젝트열기ToolStripMenuItem_Click);
            // 
            // 프로젝트저장ToolStripMenuItem
            // 
            this.프로젝트저장ToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("프로젝트저장ToolStripMenuItem.Image")));
            this.프로젝트저장ToolStripMenuItem.Name = "프로젝트저장ToolStripMenuItem";
            this.프로젝트저장ToolStripMenuItem.Size = new System.Drawing.Size(339, 22);
            this.프로젝트저장ToolStripMenuItem.Text = "프로젝트 저장";
            this.프로젝트저장ToolStripMenuItem.Visible = false;
            // 
            // toolStripSeparator7
            // 
            this.toolStripSeparator7.Name = "toolStripSeparator7";
            this.toolStripSeparator7.Size = new System.Drawing.Size(336, 6);
            // 
            // 파일로웨이브저장ToolStripMenuItem
            // 
            this.파일로웨이브저장ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.원본사이즈사용ToolStripMenuItem,
            this.toolStripTextBox1});
            this.파일로웨이브저장ToolStripMenuItem.Enabled = false;
            this.파일로웨이브저장ToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("파일로웨이브저장ToolStripMenuItem.Image")));
            this.파일로웨이브저장ToolStripMenuItem.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.파일로웨이브저장ToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.파일로웨이브저장ToolStripMenuItem.Name = "파일로웨이브저장ToolStripMenuItem";
            this.파일로웨이브저장ToolStripMenuItem.Padding = new System.Windows.Forms.Padding(0);
            this.파일로웨이브저장ToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.W)));
            this.파일로웨이브저장ToolStripMenuItem.Size = new System.Drawing.Size(339, 20);
            this.파일로웨이브저장ToolStripMenuItem.Text = "파일로 현재 재생위치의 웨이브정보 저장";
            this.파일로웨이브저장ToolStripMenuItem.DropDownOpening += new System.EventHandler(this.파일로웨이브저장ToolStripMenuItem_DropDownOpening);
            this.파일로웨이브저장ToolStripMenuItem.Click += new System.EventHandler(this.파일로웨이브저장ToolStripMenuItem_Click);
            // 
            // 원본사이즈사용ToolStripMenuItem
            // 
            this.원본사이즈사용ToolStripMenuItem.Name = "원본사이즈사용ToolStripMenuItem";
            this.원본사이즈사용ToolStripMenuItem.Size = new System.Drawing.Size(220, 22);
            this.원본사이즈사용ToolStripMenuItem.Text = "원본 사이즈 구하기";
            this.원본사이즈사용ToolStripMenuItem.Click += new System.EventHandler(this.원본사이즈사용ToolStripMenuItem_Click);
            // 
            // toolStripTextBox1
            // 
            this.toolStripTextBox1.Font = new System.Drawing.Font("굴림", 8.780488F);
            this.toolStripTextBox1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.toolStripTextBox1.MaxLength = 15;
            this.toolStripTextBox1.Name = "toolStripTextBox1";
            this.toolStripTextBox1.Size = new System.Drawing.Size(160, 21);
            this.toolStripTextBox1.Tag = "크기 입력 (숫자만 입력가능)";
            this.toolStripTextBox1.Text = "2,048";
            this.toolStripTextBox1.ToolTipText = "파일로 현재 재생위치의 웨이브정보 저장할 숫자를 입력합니다.\r\n※주의: FFT최대사이즈보다 크게 지정하면 0으로 저장될수 있습니다.\r\n1~1638" +
    "5 사이의 값을 입력해 주세요";
            this.toolStripTextBox1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.toolStripTextBox1_KeyPress);
            // 
            // toolStripSeparator8
            // 
            this.toolStripSeparator8.Name = "toolStripSeparator8";
            this.toolStripSeparator8.Size = new System.Drawing.Size(336, 6);
            // 
            // 끝내기ToolStripMenuItem
            // 
            this.끝내기ToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("끝내기ToolStripMenuItem.Image")));
            this.끝내기ToolStripMenuItem.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.끝내기ToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.끝내기ToolStripMenuItem.Name = "끝내기ToolStripMenuItem";
            this.끝내기ToolStripMenuItem.Padding = new System.Windows.Forms.Padding(0);
            this.끝내기ToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.F4)));
            this.끝내기ToolStripMenuItem.Size = new System.Drawing.Size(339, 20);
            this.끝내기ToolStripMenuItem.Text = "끝내기(&X)";
            this.끝내기ToolStripMenuItem.Click += new System.EventHandler(this.끝내기ToolStripMenuItem_Click);
            // 
            // 설정ToolStripMenuItem
            // 
            this.설정ToolStripMenuItem.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.설정ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.맨위로설정ToolStripMenuItem,
            this.재생목록창꾸미기ToolStripMenuItem,
            this.toolStripSeparator28,
            this.언어ToolStripMenuItem,
            this.기본프로그램으로설정ToolStripMenuItem,
            this.toolStripSeparator11,
            this.핫키설정ToolStripMenuItem,
            this.toolStripSeparator9,
            this.프리로드모드ToolStripMenuItem,
            this.안전모드ToolStripMenuItem,
            this.asyncToolStripMenuItem,
            this.음악믹싱모드ToolStripMenuItem,
            this.toolStripSeparator22,
            this.파일복구및재설치ToolStripMenuItem,
            this.프로그램다시시작ToolStripMenuItem,
            this.toolStripSeparator12,
            this.우선순위ToolStripMenuItem,
            this.LOL우선순위ToolStripMenuItem,
            this.toolStripSeparator25,
            this.가비지커렉션가동ToolStripMenuItem});
            this.설정ToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.설정ToolStripMenuItem.Name = "설정ToolStripMenuItem";
            this.설정ToolStripMenuItem.Size = new System.Drawing.Size(57, 20);
            this.설정ToolStripMenuItem.Text = "설정(&T)";
            this.설정ToolStripMenuItem.DropDownOpening += new System.EventHandler(this.설정ToolStripMenuItem_DropDownOpening);
            // 
            // 맨위로설정ToolStripMenuItem
            // 
            this.맨위로설정ToolStripMenuItem.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.맨위로설정ToolStripMenuItem.CheckOnClick = true;
            this.맨위로설정ToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("맨위로설정ToolStripMenuItem.Image")));
            this.맨위로설정ToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.맨위로설정ToolStripMenuItem.Name = "맨위로설정ToolStripMenuItem";
            this.맨위로설정ToolStripMenuItem.Size = new System.Drawing.Size(305, 22);
            this.맨위로설정ToolStripMenuItem.Text = "맨 위로 설정";
            this.맨위로설정ToolStripMenuItem.Click += new System.EventHandler(this.맨위로설정ToolStripMenuItem_Click);
            // 
            // 재생목록창꾸미기ToolStripMenuItem
            // 
            this.재생목록창꾸미기ToolStripMenuItem.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.재생목록창꾸미기ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.모눈선표시ToolStripMenuItem,
            this.선택한항목강조표시ToolStripMenuItem,
            this.toolStripSeparator32,
            this.예제사진ToolStripMenuItem,
            this.글자색상설정ToolStripMenuItem,
            this.배경색상설정ToolStripMenuItem,
            this.삽입색상설정ToolStripMenuItem,
            this.toolStripSeparator33,
            this.이미지불러오기ToolStripMenuItem,
            this.toolStripSeparator31,
            this.지우기ToolStripMenuItem});
            this.재생목록창꾸미기ToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.재생목록창꾸미기ToolStripMenuItem.Name = "재생목록창꾸미기ToolStripMenuItem";
            this.재생목록창꾸미기ToolStripMenuItem.Size = new System.Drawing.Size(305, 22);
            this.재생목록창꾸미기ToolStripMenuItem.Text = "재생 목록 창 꾸미기";
            // 
            // 모눈선표시ToolStripMenuItem
            // 
            this.모눈선표시ToolStripMenuItem.Checked = true;
            this.모눈선표시ToolStripMenuItem.CheckOnClick = true;
            this.모눈선표시ToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.모눈선표시ToolStripMenuItem.Name = "모눈선표시ToolStripMenuItem";
            this.모눈선표시ToolStripMenuItem.Size = new System.Drawing.Size(221, 22);
            this.모눈선표시ToolStripMenuItem.Text = "모눈선 표시";
            this.모눈선표시ToolStripMenuItem.CheckedChanged += new System.EventHandler(this.모눈선표시ToolStripMenuItem_CheckedChanged);
            // 
            // 선택한항목강조표시ToolStripMenuItem
            // 
            this.선택한항목강조표시ToolStripMenuItem.CheckOnClick = true;
            this.선택한항목강조표시ToolStripMenuItem.Name = "선택한항목강조표시ToolStripMenuItem";
            this.선택한항목강조표시ToolStripMenuItem.Size = new System.Drawing.Size(221, 22);
            this.선택한항목강조표시ToolStripMenuItem.Text = "선택한 항목 강조표시";
            this.선택한항목강조표시ToolStripMenuItem.ToolTipText = "다른 곳을 클릭 할 때 선택한 항목을 옅은 회색으로\r\n표시할지 여부 입니다.";
            this.선택한항목강조표시ToolStripMenuItem.CheckedChanged += new System.EventHandler(this.선택한항목강조표시ToolStripMenuItem_CheckedChanged);
            // 
            // toolStripSeparator32
            // 
            this.toolStripSeparator32.Name = "toolStripSeparator32";
            this.toolStripSeparator32.Size = new System.Drawing.Size(218, 6);
            // 
            // 예제사진ToolStripMenuItem
            // 
            this.예제사진ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.러브라이브1ToolStripMenuItem,
            this.일러스트1ToolStripMenuItem});
            this.예제사진ToolStripMenuItem.Name = "예제사진ToolStripMenuItem";
            this.예제사진ToolStripMenuItem.Size = new System.Drawing.Size(221, 22);
            this.예제사진ToolStripMenuItem.Text = "샘플 사진 (일부 눈갱 주의!)";
            // 
            // 러브라이브1ToolStripMenuItem
            // 
            this.러브라이브1ToolStripMenuItem.Name = "러브라이브1ToolStripMenuItem";
            this.러브라이브1ToolStripMenuItem.Size = new System.Drawing.Size(235, 22);
            this.러브라이브1ToolStripMenuItem.Text = "러브 라이브1 (TV 애니메이션)";
            this.러브라이브1ToolStripMenuItem.ToolTipText = "1728X1080";
            this.러브라이브1ToolStripMenuItem.Click += new System.EventHandler(this.러브라이브1ToolStripMenuItem_Click);
            // 
            // 일러스트1ToolStripMenuItem
            // 
            this.일러스트1ToolStripMenuItem.Name = "일러스트1ToolStripMenuItem";
            this.일러스트1ToolStripMenuItem.Size = new System.Drawing.Size(235, 22);
            this.일러스트1ToolStripMenuItem.Text = "일러스트1 (캐릭터)";
            this.일러스트1ToolStripMenuItem.ToolTipText = "960X540";
            this.일러스트1ToolStripMenuItem.Click += new System.EventHandler(this.일러스트1ToolStripMenuItem_Click);
            // 
            // 글자색상설정ToolStripMenuItem
            // 
            this.글자색상설정ToolStripMenuItem.Name = "글자색상설정ToolStripMenuItem";
            this.글자색상설정ToolStripMenuItem.Size = new System.Drawing.Size(221, 22);
            this.글자색상설정ToolStripMenuItem.Text = "글자 색상 설정";
            this.글자색상설정ToolStripMenuItem.Click += new System.EventHandler(this.글자색상설정ToolStripMenuItem_Click);
            // 
            // 배경색상설정ToolStripMenuItem
            // 
            this.배경색상설정ToolStripMenuItem.Name = "배경색상설정ToolStripMenuItem";
            this.배경색상설정ToolStripMenuItem.Size = new System.Drawing.Size(221, 22);
            this.배경색상설정ToolStripMenuItem.Text = "배경 색상 설정";
            this.배경색상설정ToolStripMenuItem.Click += new System.EventHandler(this.색상설정ToolStripMenuItem_Click);
            // 
            // 삽입색상설정ToolStripMenuItem
            // 
            this.삽입색상설정ToolStripMenuItem.Name = "삽입색상설정ToolStripMenuItem";
            this.삽입색상설정ToolStripMenuItem.Size = new System.Drawing.Size(221, 22);
            this.삽입색상설정ToolStripMenuItem.Text = "삽입 색상 설정";
            this.삽입색상설정ToolStripMenuItem.Click += new System.EventHandler(this.삽입색상설정ToolStripMenuItem_Click);
            // 
            // toolStripSeparator33
            // 
            this.toolStripSeparator33.Name = "toolStripSeparator33";
            this.toolStripSeparator33.Size = new System.Drawing.Size(218, 6);
            // 
            // 이미지불러오기ToolStripMenuItem
            // 
            this.이미지불러오기ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripComboBox3,
            this.toolStripComboBox4,
            this.toolStripSeparator34,
            this.타일보기ToolStripMenuItem});
            this.이미지불러오기ToolStripMenuItem.Name = "이미지불러오기ToolStripMenuItem";
            this.이미지불러오기ToolStripMenuItem.Size = new System.Drawing.Size(221, 22);
            this.이미지불러오기ToolStripMenuItem.Text = "이미지 불러오기 (클릭!)";
            this.이미지불러오기ToolStripMenuItem.Click += new System.EventHandler(this.이미지불러오기ToolStripMenuItem_Click);
            // 
            // toolStripComboBox3
            // 
            this.toolStripComboBox3.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.toolStripComboBox3.Items.AddRange(new object[] {
            "Strech (↕,↔)",
            "Nomal (□)"});
            this.toolStripComboBox3.Name = "toolStripComboBox3";
            this.toolStripComboBox3.Size = new System.Drawing.Size(121, 23);
            this.toolStripComboBox3.ToolTipText = "재생 목록 창의 이미지를 조정 합니다.\r\n\r\nStrech=이미지를 재생 목록 창에 딱 맞게 이미지를 조정 합니다.\r\nNomal=일반적인 표시방법 " +
    "입니다. (이 항목을 선택하면 밑의 설정이 활성화 됩니다.)";
            this.toolStripComboBox3.SelectedIndexChanged += new System.EventHandler(this.toolStripComboBox3_SelectedIndexChanged);
            // 
            // toolStripComboBox4
            // 
            this.toolStripComboBox4.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.toolStripComboBox4.Enabled = false;
            this.toolStripComboBox4.Items.AddRange(new object[] {
            "TopLeft (↖)",
            "TopRight (↗)",
            "BottomLeft (↙)",
            "BottomRight (↘)"});
            this.toolStripComboBox4.Name = "toolStripComboBox4";
            this.toolStripComboBox4.Size = new System.Drawing.Size(121, 23);
            this.toolStripComboBox4.ToolTipText = "일반일때의 이미지의 위치를 조정합니다.\r\n\r\nTopLeft(↖)=왼쪽 위 입니다. \r\nTopRight(↗)=오른쪽 위 입니다.\r\nBottomLef" +
    "t(↙)=왼쪽 아래 입니다\r\nBottomRight(↘)=오른쪽 아래 입니다.\r\n";
            this.toolStripComboBox4.SelectedIndexChanged += new System.EventHandler(this.toolStripComboBox3_SelectedIndexChanged);
            // 
            // toolStripSeparator34
            // 
            this.toolStripSeparator34.Name = "toolStripSeparator34";
            this.toolStripSeparator34.Size = new System.Drawing.Size(178, 6);
            // 
            // 타일보기ToolStripMenuItem
            // 
            this.타일보기ToolStripMenuItem.Checked = true;
            this.타일보기ToolStripMenuItem.CheckOnClick = true;
            this.타일보기ToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.타일보기ToolStripMenuItem.Name = "타일보기ToolStripMenuItem";
            this.타일보기ToolStripMenuItem.Size = new System.Drawing.Size(181, 22);
            this.타일보기ToolStripMenuItem.Text = "타일 보기";
            this.타일보기ToolStripMenuItem.ToolTipText = "체크하면 아래로 스크롤 시 사진이 고정되고\r\n바둑판 형태로 표시됩니다. (재생목록 창보다 큰 그림 제외)";
            this.타일보기ToolStripMenuItem.CheckedChanged += new System.EventHandler(this.타일보기ToolStripMenuItem_CheckedChanged);
            // 
            // toolStripSeparator31
            // 
            this.toolStripSeparator31.Name = "toolStripSeparator31";
            this.toolStripSeparator31.Size = new System.Drawing.Size(218, 6);
            // 
            // 지우기ToolStripMenuItem
            // 
            this.지우기ToolStripMenuItem.Name = "지우기ToolStripMenuItem";
            this.지우기ToolStripMenuItem.Size = new System.Drawing.Size(221, 22);
            this.지우기ToolStripMenuItem.Text = "이미지 지우기";
            this.지우기ToolStripMenuItem.Click += new System.EventHandler(this.지우기ToolStripMenuItem_Click);
            // 
            // toolStripSeparator28
            // 
            this.toolStripSeparator28.Name = "toolStripSeparator28";
            this.toolStripSeparator28.Size = new System.Drawing.Size(302, 6);
            // 
            // 언어ToolStripMenuItem
            // 
            this.언어ToolStripMenuItem.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.언어ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripComboBox1});
            this.언어ToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.언어ToolStripMenuItem.Name = "언어ToolStripMenuItem";
            this.언어ToolStripMenuItem.Size = new System.Drawing.Size(305, 22);
            this.언어ToolStripMenuItem.Text = "언어";
            // 
            // toolStripComboBox1
            // 
            this.toolStripComboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.toolStripComboBox1.Items.AddRange(new object[] {
            "한국어",
            "日本語"});
            this.toolStripComboBox1.Name = "toolStripComboBox1";
            this.toolStripComboBox1.Size = new System.Drawing.Size(121, 23);
            this.toolStripComboBox1.SelectedIndexChanged += new System.EventHandler(this.toolStripComboBox1_SelectedIndexChanged);
            // 
            // 기본프로그램으로설정ToolStripMenuItem
            // 
            this.기본프로그램으로설정ToolStripMenuItem.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.기본프로그램으로설정ToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.기본프로그램으로설정ToolStripMenuItem.Name = "기본프로그램으로설정ToolStripMenuItem";
            this.기본프로그램으로설정ToolStripMenuItem.Size = new System.Drawing.Size(305, 22);
            this.기본프로그램으로설정ToolStripMenuItem.Text = "기본 음악 프로그램으로 설정";
            this.기본프로그램으로설정ToolStripMenuItem.Click += new System.EventHandler(this.기본프로그램으로설정ToolStripMenuItem_Click);
            // 
            // toolStripSeparator11
            // 
            this.toolStripSeparator11.Name = "toolStripSeparator11";
            this.toolStripSeparator11.Size = new System.Drawing.Size(302, 6);
            // 
            // 핫키설정ToolStripMenuItem
            // 
            this.핫키설정ToolStripMenuItem.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.핫키설정ToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.핫키설정ToolStripMenuItem.Name = "핫키설정ToolStripMenuItem";
            this.핫키설정ToolStripMenuItem.Size = new System.Drawing.Size(305, 22);
            this.핫키설정ToolStripMenuItem.Text = "단축키(핫키) 설정";
            this.핫키설정ToolStripMenuItem.Click += new System.EventHandler(this.핫키설정ToolStripMenuItem_Click);
            // 
            // toolStripSeparator9
            // 
            this.toolStripSeparator9.Name = "toolStripSeparator9";
            this.toolStripSeparator9.Size = new System.Drawing.Size(302, 6);
            // 
            // 프리로드모드ToolStripMenuItem
            // 
            this.프리로드모드ToolStripMenuItem.Checked = true;
            this.프리로드모드ToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.프리로드모드ToolStripMenuItem.Name = "프리로드모드ToolStripMenuItem";
            this.프리로드모드ToolStripMenuItem.Size = new System.Drawing.Size(305, 22);
            this.프리로드모드ToolStripMenuItem.Text = "프리 로드 모드 (로딩 느림, 태그 변경 가능)";
            this.프리로드모드ToolStripMenuItem.ToolTipText = "메모리가 부족하여 재생이 안되는 환경에서는\r\n반드시 이 기능을 꺼주시기 바랍니다.\r\n\r\n(프로그램 다음번 실행시 적용됩니다.)";
            this.프리로드모드ToolStripMenuItem.CheckedChanged += new System.EventHandler(this.프리로드모드ToolStripMenuItem_CheckedChanged);
            this.프리로드모드ToolStripMenuItem.Click += new System.EventHandler(this.프리로드모드ToolStripMenuItem_Click);
            // 
            // 안전모드ToolStripMenuItem
            // 
            this.안전모드ToolStripMenuItem.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.안전모드ToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.안전모드ToolStripMenuItem.Name = "안전모드ToolStripMenuItem";
            this.안전모드ToolStripMenuItem.Size = new System.Drawing.Size(305, 22);
            this.안전모드ToolStripMenuItem.Text = "안전 모드";
            this.안전모드ToolStripMenuItem.ToolTipText = "파일을 열때 파일이 올바른지의 여부를 확인합니다.";
            this.안전모드ToolStripMenuItem.Click += new System.EventHandler(this.안전모드ToolStripMenuItem_Click);
            // 
            // asyncToolStripMenuItem
            // 
            this.asyncToolStripMenuItem.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.asyncToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.asyncToolStripMenuItem.Name = "asyncToolStripMenuItem";
            this.asyncToolStripMenuItem.Size = new System.Drawing.Size(305, 22);
            this.asyncToolStripMenuItem.Text = "비동기 모드";
            this.asyncToolStripMenuItem.ToolTipText = "혹시 여러 파일이나 목록을 열다가 멈추거나 열지 못한다거나,\r\n자동으로 다음곡으로 넘어갈떄 오류가 있으면\r\n체크를 해제하세요.";
            this.asyncToolStripMenuItem.Click += new System.EventHandler(this.syncToolStripMenuItem_Click);
            // 
            // 음악믹싱모드ToolStripMenuItem
            // 
            this.음악믹싱모드ToolStripMenuItem.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.음악믹싱모드ToolStripMenuItem.Enabled = false;
            this.음악믹싱모드ToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.음악믹싱모드ToolStripMenuItem.Name = "음악믹싱모드ToolStripMenuItem";
            this.음악믹싱모드ToolStripMenuItem.Size = new System.Drawing.Size(305, 22);
            this.음악믹싱모드ToolStripMenuItem.Text = "음악 믹싱 모드";
            this.음악믹싱모드ToolStripMenuItem.Click += new System.EventHandler(this.음악믹싱모드ToolStripMenuItem_Click);
            // 
            // toolStripSeparator22
            // 
            this.toolStripSeparator22.Name = "toolStripSeparator22";
            this.toolStripSeparator22.Size = new System.Drawing.Size(302, 6);
            // 
            // 파일복구및재설치ToolStripMenuItem
            // 
            this.파일복구및재설치ToolStripMenuItem.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.파일복구및재설치ToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.파일복구및재설치ToolStripMenuItem.Name = "파일복구및재설치ToolStripMenuItem";
            this.파일복구및재설치ToolStripMenuItem.Size = new System.Drawing.Size(305, 22);
            this.파일복구및재설치ToolStripMenuItem.Text = "사운드 시스템 엔진 파일 복구 및 재설치";
            this.파일복구및재설치ToolStripMenuItem.Click += new System.EventHandler(this.파일복구및재설치ToolStripMenuItem_Click);
            // 
            // 프로그램다시시작ToolStripMenuItem
            // 
            this.프로그램다시시작ToolStripMenuItem.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.프로그램다시시작ToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("프로그램다시시작ToolStripMenuItem.Image")));
            this.프로그램다시시작ToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.프로그램다시시작ToolStripMenuItem.Name = "프로그램다시시작ToolStripMenuItem";
            this.프로그램다시시작ToolStripMenuItem.Size = new System.Drawing.Size(305, 22);
            this.프로그램다시시작ToolStripMenuItem.Text = "프로그램 다시 시작";
            this.프로그램다시시작ToolStripMenuItem.Click += new System.EventHandler(this.프로그램다시시작ToolStripMenuItem_Click);
            // 
            // toolStripSeparator12
            // 
            this.toolStripSeparator12.Name = "toolStripSeparator12";
            this.toolStripSeparator12.Size = new System.Drawing.Size(302, 6);
            // 
            // 우선순위ToolStripMenuItem
            // 
            this.우선순위ToolStripMenuItem.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.우선순위ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.높음ToolStripMenuItem,
            this.높은우선순위ToolStripMenuItem,
            this.보통권장ToolStripMenuItem,
            this.낮은우선순위ToolStripMenuItem});
            this.우선순위ToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.우선순위ToolStripMenuItem.Name = "우선순위ToolStripMenuItem";
            this.우선순위ToolStripMenuItem.Size = new System.Drawing.Size(305, 22);
            this.우선순위ToolStripMenuItem.Text = "프로그램 우선순위";
            // 
            // 높음ToolStripMenuItem
            // 
            this.높음ToolStripMenuItem.Name = "높음ToolStripMenuItem";
            this.높음ToolStripMenuItem.Size = new System.Drawing.Size(178, 22);
            this.높음ToolStripMenuItem.Text = "높음";
            this.높음ToolStripMenuItem.Click += new System.EventHandler(this.높음ToolStripMenuItem_Click);
            // 
            // 높은우선순위ToolStripMenuItem
            // 
            this.높은우선순위ToolStripMenuItem.Name = "높은우선순위ToolStripMenuItem";
            this.높은우선순위ToolStripMenuItem.Size = new System.Drawing.Size(178, 22);
            this.높은우선순위ToolStripMenuItem.Text = "이 프로그램 우선";
            this.높은우선순위ToolStripMenuItem.Click += new System.EventHandler(this.높은우선순위ToolStripMenuItem_Click);
            // 
            // 보통권장ToolStripMenuItem
            // 
            this.보통권장ToolStripMenuItem.Checked = true;
            this.보통권장ToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.보통권장ToolStripMenuItem.Name = "보통권장ToolStripMenuItem";
            this.보통권장ToolStripMenuItem.Size = new System.Drawing.Size(178, 22);
            this.보통권장ToolStripMenuItem.Text = "보통 (권장)";
            this.보통권장ToolStripMenuItem.Click += new System.EventHandler(this.보통권장ToolStripMenuItem_Click);
            // 
            // 낮은우선순위ToolStripMenuItem
            // 
            this.낮은우선순위ToolStripMenuItem.Name = "낮은우선순위ToolStripMenuItem";
            this.낮은우선순위ToolStripMenuItem.Size = new System.Drawing.Size(178, 22);
            this.낮은우선순위ToolStripMenuItem.Text = "다른 프로그램 우선";
            this.낮은우선순위ToolStripMenuItem.Click += new System.EventHandler(this.낮은우선순위ToolStripMenuItem_Click);
            // 
            // LOL우선순위ToolStripMenuItem
            // 
            this.LOL우선순위ToolStripMenuItem.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.LOL우선순위ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.LOL높은우선순위ToolStripMenuItem,
            this.LOL보통권장ToolStripMenuItem});
            this.LOL우선순위ToolStripMenuItem.Enabled = false;
            this.LOL우선순위ToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.LOL우선순위ToolStripMenuItem.Name = "LOL우선순위ToolStripMenuItem";
            this.LOL우선순위ToolStripMenuItem.Size = new System.Drawing.Size(305, 22);
            this.LOL우선순위ToolStripMenuItem.Text = "리그 오브 레전드 우선순위";
            this.LOL우선순위ToolStripMenuItem.ToolTipText = "리그 오브 레전드도 사운드 재생에 FMOD를 씁니다.\r\n(리그 오브 레전드에서 게임이 시작되면 활성화 됩니다.)";
            // 
            // LOL높은우선순위ToolStripMenuItem
            // 
            this.LOL높은우선순위ToolStripMenuItem.Enabled = false;
            this.LOL높은우선순위ToolStripMenuItem.Name = "LOL높은우선순위ToolStripMenuItem";
            this.LOL높은우선순위ToolStripMenuItem.Size = new System.Drawing.Size(159, 22);
            this.LOL높은우선순위ToolStripMenuItem.Text = "LOL 우선 (권장)";
            this.LOL높은우선순위ToolStripMenuItem.Click += new System.EventHandler(this.높은우선순위ToolStripMenuItem1_Click);
            // 
            // LOL보통권장ToolStripMenuItem
            // 
            this.LOL보통권장ToolStripMenuItem.Enabled = false;
            this.LOL보통권장ToolStripMenuItem.Name = "LOL보통권장ToolStripMenuItem";
            this.LOL보통권장ToolStripMenuItem.Size = new System.Drawing.Size(159, 22);
            this.LOL보통권장ToolStripMenuItem.Text = "보통";
            this.LOL보통권장ToolStripMenuItem.Click += new System.EventHandler(this.보통권장ToolStripMenuItem1_Click);
            // 
            // toolStripSeparator25
            // 
            this.toolStripSeparator25.Name = "toolStripSeparator25";
            this.toolStripSeparator25.Size = new System.Drawing.Size(302, 6);
            // 
            // 가비지커렉션가동ToolStripMenuItem
            // 
            this.가비지커렉션가동ToolStripMenuItem.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.가비지커렉션가동ToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.가비지커렉션가동ToolStripMenuItem.Name = "가비지커렉션가동ToolStripMenuItem";
            this.가비지커렉션가동ToolStripMenuItem.Size = new System.Drawing.Size(305, 22);
            this.가비지커렉션가동ToolStripMenuItem.Text = "가비지 컬렉션 가동";
            this.가비지커렉션가동ToolStripMenuItem.Click += new System.EventHandler(this.가비지커렉션가동ToolStripMenuItem_Click);
            // 
            // 보기VToolStripMenuItem
            // 
            this.보기VToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.밀리초표시ToolStripMenuItem,
            this.toolStripSeparator42,
            this.재생상세정보ToolStripMenuItem,
            this.toolStripSeparator41,
            this.스케일모드ToolStripMenuItem,
            this.toolStripSeparator24,
            this.로그관리ToolStripMenuItem,
            this.toolStripSeparator13,
            this.화면끄기ToolStripMenuItem,
            this.toolStripSeparator39,
            this.상태표시줄업데이트ToolStripMenuItem,
            this.testToolStripMenuItem});
            this.보기VToolStripMenuItem.Name = "보기VToolStripMenuItem";
            this.보기VToolStripMenuItem.Size = new System.Drawing.Size(59, 20);
            this.보기VToolStripMenuItem.Text = "보기(&V)";
            this.보기VToolStripMenuItem.DropDownOpening += new System.EventHandler(this.보기VToolStripMenuItem_DropDownOpening);
            // 
            // 밀리초표시ToolStripMenuItem
            // 
            this.밀리초표시ToolStripMenuItem.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.밀리초표시ToolStripMenuItem.Checked = true;
            this.밀리초표시ToolStripMenuItem.CheckOnClick = true;
            this.밀리초표시ToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.밀리초표시ToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.밀리초표시ToolStripMenuItem.Name = "밀리초표시ToolStripMenuItem";
            this.밀리초표시ToolStripMenuItem.Size = new System.Drawing.Size(210, 22);
            this.밀리초표시ToolStripMenuItem.Text = "밀리초 표시";
            this.밀리초표시ToolStripMenuItem.CheckedChanged += new System.EventHandler(this.밀리초표시ToolStripMenuItem_CheckedChanged);
            // 
            // toolStripSeparator42
            // 
            this.toolStripSeparator42.Name = "toolStripSeparator42";
            this.toolStripSeparator42.Size = new System.Drawing.Size(207, 6);
            // 
            // 재생상세정보ToolStripMenuItem
            // 
            this.재생상세정보ToolStripMenuItem.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.재생상세정보ToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.재생상세정보ToolStripMenuItem.Name = "재생상세정보ToolStripMenuItem";
            this.재생상세정보ToolStripMenuItem.Size = new System.Drawing.Size(210, 22);
            this.재생상세정보ToolStripMenuItem.Text = "시스템 상세 정보";
            this.재생상세정보ToolStripMenuItem.Click += new System.EventHandler(this.재생중인파일상세정보ToolStripMenuItem_Click);
            // 
            // toolStripSeparator41
            // 
            this.toolStripSeparator41.Name = "toolStripSeparator41";
            this.toolStripSeparator41.Size = new System.Drawing.Size(207, 6);
            // 
            // 스케일모드ToolStripMenuItem
            // 
            this.스케일모드ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripComboBox5,
            this.자동스케일ToolStripMenuItem});
            this.스케일모드ToolStripMenuItem.Name = "스케일모드ToolStripMenuItem";
            this.스케일모드ToolStripMenuItem.Size = new System.Drawing.Size(210, 22);
            this.스케일모드ToolStripMenuItem.Text = "스케일 모드";
            this.스케일모드ToolStripMenuItem.Visible = false;
            // 
            // toolStripComboBox5
            // 
            this.toolStripComboBox5.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.toolStripComboBox5.DropDownWidth = 135;
            this.toolStripComboBox5.Items.AddRange(new object[] {
            "폰트 기준 (기본값)",
            "DPI 기준",
            "사용자 정의"});
            this.toolStripComboBox5.Name = "toolStripComboBox5";
            this.toolStripComboBox5.Size = new System.Drawing.Size(150, 23);
            this.toolStripComboBox5.SelectedIndexChanged += new System.EventHandler(this.toolStripComboBox5_SelectedIndexChanged);
            // 
            // 자동스케일ToolStripMenuItem
            // 
            this.자동스케일ToolStripMenuItem.Checked = true;
            this.자동스케일ToolStripMenuItem.CheckOnClick = true;
            this.자동스케일ToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.자동스케일ToolStripMenuItem.Name = "자동스케일ToolStripMenuItem";
            this.자동스케일ToolStripMenuItem.Size = new System.Drawing.Size(210, 22);
            this.자동스케일ToolStripMenuItem.Text = "자동 스케일";
            this.자동스케일ToolStripMenuItem.CheckedChanged += new System.EventHandler(this.자동스케일ToolStripMenuItem_CheckedChanged);
            // 
            // toolStripSeparator24
            // 
            this.toolStripSeparator24.Name = "toolStripSeparator24";
            this.toolStripSeparator24.Size = new System.Drawing.Size(207, 6);
            this.toolStripSeparator24.Visible = false;
            // 
            // 로그관리ToolStripMenuItem
            // 
            this.로그관리ToolStripMenuItem.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.로그관리ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.로그폴더열기ToolStripMenuItem,
            this.toolStripSeparator29,
            this.로그지우기ToolStripMenuItem});
            this.로그관리ToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.로그관리ToolStripMenuItem.Name = "로그관리ToolStripMenuItem";
            this.로그관리ToolStripMenuItem.Size = new System.Drawing.Size(210, 22);
            this.로그관리ToolStripMenuItem.Text = "로그 관리";
            this.로그관리ToolStripMenuItem.DropDownOpening += new System.EventHandler(this.로그관리ToolStripMenuItem_DropDownOpening);
            // 
            // 로그폴더열기ToolStripMenuItem
            // 
            this.로그폴더열기ToolStripMenuItem.Name = "로그폴더열기ToolStripMenuItem";
            this.로그폴더열기ToolStripMenuItem.Size = new System.Drawing.Size(154, 22);
            this.로그폴더열기ToolStripMenuItem.Text = "로그 폴더 열기";
            this.로그폴더열기ToolStripMenuItem.Click += new System.EventHandler(this.로그폴더열기ToolStripMenuItem_Click);
            // 
            // toolStripSeparator29
            // 
            this.toolStripSeparator29.Name = "toolStripSeparator29";
            this.toolStripSeparator29.Size = new System.Drawing.Size(151, 6);
            // 
            // 로그지우기ToolStripMenuItem
            // 
            this.로그지우기ToolStripMenuItem.Name = "로그지우기ToolStripMenuItem";
            this.로그지우기ToolStripMenuItem.Size = new System.Drawing.Size(154, 22);
            this.로그지우기ToolStripMenuItem.Text = "로그  지우기";
            this.로그지우기ToolStripMenuItem.Click += new System.EventHandler(this.로그지우기ToolStripMenuItem_Click);
            // 
            // toolStripSeparator13
            // 
            this.toolStripSeparator13.Name = "toolStripSeparator13";
            this.toolStripSeparator13.Size = new System.Drawing.Size(207, 6);
            // 
            // 화면끄기ToolStripMenuItem
            // 
            this.화면끄기ToolStripMenuItem.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.화면끄기ToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.화면끄기ToolStripMenuItem.Name = "화면끄기ToolStripMenuItem";
            this.화면끄기ToolStripMenuItem.Size = new System.Drawing.Size(210, 22);
            this.화면끄기ToolStripMenuItem.Text = "모니터 끄기";
            this.화면끄기ToolStripMenuItem.Click += new System.EventHandler(this.화면끄기ToolStripMenuItem_Click);
            // 
            // toolStripSeparator39
            // 
            this.toolStripSeparator39.Name = "toolStripSeparator39";
            this.toolStripSeparator39.Size = new System.Drawing.Size(207, 6);
            // 
            // 상태표시줄업데이트ToolStripMenuItem
            // 
            this.상태표시줄업데이트ToolStripMenuItem.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.상태표시줄업데이트ToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("상태표시줄업데이트ToolStripMenuItem.Image")));
            this.상태표시줄업데이트ToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.상태표시줄업데이트ToolStripMenuItem.Name = "상태표시줄업데이트ToolStripMenuItem";
            this.상태표시줄업데이트ToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F6;
            this.상태표시줄업데이트ToolStripMenuItem.Size = new System.Drawing.Size(210, 22);
            this.상태표시줄업데이트ToolStripMenuItem.Text = "상태 표시줄 새로고침";
            this.상태표시줄업데이트ToolStripMenuItem.ToolTipText = "원래는 자동으로 업데이트 되지만\r\n디버깅 중도 아닌데 디버깅이라고 뜨거나 (디버거 에서 분리했을때)\r\n파일이나 목록 열기에 실패해서 상태 표시줄이" +
    " 멈춰있는경우\r\n기타 상태 표시줄이 이상하게 뜰때 클릭해 주세요";
            this.상태표시줄업데이트ToolStripMenuItem.Click += new System.EventHandler(this.상태표시줄업데이트ToolStripMenuItem_Click);
            // 
            // testToolStripMenuItem
            // 
            this.testToolStripMenuItem.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.testToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.testToolStripMenuItem.Name = "testToolStripMenuItem";
            this.testToolStripMenuItem.Size = new System.Drawing.Size(210, 22);
            this.testToolStripMenuItem.Text = "Test";
            this.testToolStripMenuItem.Visible = false;
            this.testToolStripMenuItem.Click += new System.EventHandler(this.testToolStripMenuItem_Click);
            // 
            // 가사LToolStripMenuItem
            // 
            this.가사LToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.싱크가사찾기ToolStripMenuItem,
            this.toolStripSeparator40,
            this.싱크가사로컬캐시ToolStripMenuItem,
            this.싱크가사우선순위ToolStripMenuItem,
            this.toolStripSeparator10,
            this.바탕화면싱크가사창띄우기ToolStripMenuItem});
            this.가사LToolStripMenuItem.Name = "가사LToolStripMenuItem";
            this.가사LToolStripMenuItem.Size = new System.Drawing.Size(57, 20);
            this.가사LToolStripMenuItem.Text = "가사(&L)";
            // 
            // 싱크가사찾기ToolStripMenuItem
            // 
            this.싱크가사찾기ToolStripMenuItem.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.싱크가사찾기ToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("싱크가사찾기ToolStripMenuItem.Image")));
            this.싱크가사찾기ToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.싱크가사찾기ToolStripMenuItem.Name = "싱크가사찾기ToolStripMenuItem";
            this.싱크가사찾기ToolStripMenuItem.Size = new System.Drawing.Size(230, 22);
            this.싱크가사찾기ToolStripMenuItem.Text = "싱크 가사 검색";
            this.싱크가사찾기ToolStripMenuItem.Click += new System.EventHandler(this.싱크가사찾기ToolStripMenuItem_Click);
            // 
            // toolStripSeparator40
            // 
            this.toolStripSeparator40.Name = "toolStripSeparator40";
            this.toolStripSeparator40.Size = new System.Drawing.Size(227, 6);
            // 
            // 싱크가사로컬캐시ToolStripMenuItem
            // 
            this.싱크가사로컬캐시ToolStripMenuItem.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.싱크가사로컬캐시ToolStripMenuItem.Checked = true;
            this.싱크가사로컬캐시ToolStripMenuItem.CheckOnClick = true;
            this.싱크가사로컬캐시ToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.싱크가사로컬캐시ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.싱크가사캐시정리ToolStripMenuItem,
            this.태그없는항목자동삽입ToolStripMenuItem,
            this.toolStripSeparator23,
            this.싱크가사캐시관리ToolStripMenuItem});
            this.싱크가사로컬캐시ToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.싱크가사로컬캐시ToolStripMenuItem.Name = "싱크가사로컬캐시ToolStripMenuItem";
            this.싱크가사로컬캐시ToolStripMenuItem.Size = new System.Drawing.Size(230, 22);
            this.싱크가사로컬캐시ToolStripMenuItem.Text = "싱크 가사 로컬 캐시";
            // 
            // 싱크가사캐시정리ToolStripMenuItem
            // 
            this.싱크가사캐시정리ToolStripMenuItem.Enabled = false;
            this.싱크가사캐시정리ToolStripMenuItem.Name = "싱크가사캐시정리ToolStripMenuItem";
            this.싱크가사캐시정리ToolStripMenuItem.Size = new System.Drawing.Size(210, 22);
            this.싱크가사캐시정리ToolStripMenuItem.Text = "싱크 가사 캐시 정리";
            this.싱크가사캐시정리ToolStripMenuItem.Click += new System.EventHandler(this.싱크가사캐시정리ToolStripMenuItem_Click);
            // 
            // 태그없는항목자동삽입ToolStripMenuItem
            // 
            this.태그없는항목자동삽입ToolStripMenuItem.Checked = true;
            this.태그없는항목자동삽입ToolStripMenuItem.CheckOnClick = true;
            this.태그없는항목자동삽입ToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.태그없는항목자동삽입ToolStripMenuItem.Name = "태그없는항목자동삽입ToolStripMenuItem";
            this.태그없는항목자동삽입ToolStripMenuItem.Size = new System.Drawing.Size(210, 22);
            this.태그없는항목자동삽입ToolStripMenuItem.Text = "태그 없는 항목 자동 삽입";
            this.태그없는항목자동삽입ToolStripMenuItem.CheckedChanged += new System.EventHandler(this.태그없는항목자동삽입ToolStripMenuItem_CheckedChanged);
            // 
            // toolStripSeparator23
            // 
            this.toolStripSeparator23.Name = "toolStripSeparator23";
            this.toolStripSeparator23.Size = new System.Drawing.Size(207, 6);
            // 
            // 싱크가사캐시관리ToolStripMenuItem
            // 
            this.싱크가사캐시관리ToolStripMenuItem.Enabled = false;
            this.싱크가사캐시관리ToolStripMenuItem.Name = "싱크가사캐시관리ToolStripMenuItem";
            this.싱크가사캐시관리ToolStripMenuItem.Size = new System.Drawing.Size(210, 22);
            this.싱크가사캐시관리ToolStripMenuItem.Text = "싱크 가사 캐시 관리";
            this.싱크가사캐시관리ToolStripMenuItem.Click += new System.EventHandler(this.싱크가사캐시관리ToolStripMenuItem_Click);
            // 
            // 싱크가사우선순위ToolStripMenuItem
            // 
            this.싱크가사우선순위ToolStripMenuItem.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.싱크가사우선순위ToolStripMenuItem.CheckOnClick = true;
            this.싱크가사우선순위ToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.싱크가사우선순위ToolStripMenuItem.Name = "싱크가사우선순위ToolStripMenuItem";
            this.싱크가사우선순위ToolStripMenuItem.Size = new System.Drawing.Size(230, 22);
            this.싱크가사우선순위ToolStripMenuItem.Text = "온라인 싱크 가사 전용";
            // 
            // toolStripSeparator10
            // 
            this.toolStripSeparator10.Name = "toolStripSeparator10";
            this.toolStripSeparator10.Size = new System.Drawing.Size(227, 6);
            // 
            // 바탕화면싱크가사창띄우기ToolStripMenuItem
            // 
            this.바탕화면싱크가사창띄우기ToolStripMenuItem.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.바탕화면싱크가사창띄우기ToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.바탕화면싱크가사창띄우기ToolStripMenuItem.Name = "바탕화면싱크가사창띄우기ToolStripMenuItem";
            this.바탕화면싱크가사창띄우기ToolStripMenuItem.Size = new System.Drawing.Size(230, 22);
            this.바탕화면싱크가사창띄우기ToolStripMenuItem.Text = "바탕화면 싱크 가사창 띄우기";
            this.바탕화면싱크가사창띄우기ToolStripMenuItem.Click += new System.EventHandler(this.바탕화면싱크가사창띄우기ToolStripMenuItem_Click);
            // 
            // 플러그인PToolStripMenuItem
            // 
            this.플러그인PToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.플러그인이존재하지않습니다ToolStripMenuItem});
            this.플러그인PToolStripMenuItem.Name = "플러그인PToolStripMenuItem";
            this.플러그인PToolStripMenuItem.Size = new System.Drawing.Size(82, 20);
            this.플러그인PToolStripMenuItem.Text = "플러그인(&P)";
            this.플러그인PToolStripMenuItem.DropDownOpening += new System.EventHandler(this.플러그인PToolStripMenuItem_DropDownOpening);
            // 
            // 플러그인이존재하지않습니다ToolStripMenuItem
            // 
            this.플러그인이존재하지않습니다ToolStripMenuItem.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.플러그인이존재하지않습니다ToolStripMenuItem.Enabled = false;
            this.플러그인이존재하지않습니다ToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.플러그인이존재하지않습니다ToolStripMenuItem.Name = "플러그인이존재하지않습니다ToolStripMenuItem";
            this.플러그인이존재하지않습니다ToolStripMenuItem.Size = new System.Drawing.Size(249, 22);
            this.플러그인이존재하지않습니다ToolStripMenuItem.Text = "(플러그인이 존재하지 않습니다.)";
            // 
            // 도움말HToolStripMenuItem
            // 
            this.도움말HToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.정보ToolStripMenuItem,
            this.업데이트변경사항보기ToolStripMenuItem,
            this.toolStripSeparator14,
            this.업데이트확인ToolStripMenuItem,
            this.toolStripSeparator26,
            this.디버깅ToolStripMenuItem});
            this.도움말HToolStripMenuItem.Name = "도움말HToolStripMenuItem";
            this.도움말HToolStripMenuItem.Size = new System.Drawing.Size(72, 20);
            this.도움말HToolStripMenuItem.Text = "도움말(&H)";
            this.도움말HToolStripMenuItem.DropDownOpening += new System.EventHandler(this.도움말HToolStripMenuItem_DropDownOpening);
            // 
            // 정보ToolStripMenuItem
            // 
            this.정보ToolStripMenuItem.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.정보ToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("정보ToolStripMenuItem.Image")));
            this.정보ToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.정보ToolStripMenuItem.Name = "정보ToolStripMenuItem";
            this.정보ToolStripMenuItem.Size = new System.Drawing.Size(217, 22);
            this.정보ToolStripMenuItem.Text = "프로그램 정보...(&A)";
            this.정보ToolStripMenuItem.Click += new System.EventHandler(this.정보ToolStripMenuItem_Click);
            // 
            // 업데이트변경사항보기ToolStripMenuItem
            // 
            this.업데이트변경사항보기ToolStripMenuItem.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.업데이트변경사항보기ToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("업데이트변경사항보기ToolStripMenuItem.Image")));
            this.업데이트변경사항보기ToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.업데이트변경사항보기ToolStripMenuItem.Name = "업데이트변경사항보기ToolStripMenuItem";
            this.업데이트변경사항보기ToolStripMenuItem.Size = new System.Drawing.Size(217, 22);
            this.업데이트변경사항보기ToolStripMenuItem.Text = "업데이트 변경사항 보기(&R)";
            this.업데이트변경사항보기ToolStripMenuItem.Click += new System.EventHandler(this.업데이트변경사항보기ToolStripMenuItem_Click);
            // 
            // toolStripSeparator14
            // 
            this.toolStripSeparator14.Name = "toolStripSeparator14";
            this.toolStripSeparator14.Size = new System.Drawing.Size(214, 6);
            // 
            // 업데이트확인ToolStripMenuItem
            // 
            this.업데이트확인ToolStripMenuItem.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.업데이트확인ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.업데이트서버설정ToolStripMenuItem});
            this.업데이트확인ToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.업데이트확인ToolStripMenuItem.Name = "업데이트확인ToolStripMenuItem";
            this.업데이트확인ToolStripMenuItem.Size = new System.Drawing.Size(217, 22);
            this.업데이트확인ToolStripMenuItem.Text = "업데이트 확인 (클릭!)";
            this.업데이트확인ToolStripMenuItem.Click += new System.EventHandler(this.업데이트확인ToolStripMenuItem_Click);
            // 
            // 업데이트서버설정ToolStripMenuItem
            // 
            this.업데이트서버설정ToolStripMenuItem.Name = "업데이트서버설정ToolStripMenuItem";
            this.업데이트서버설정ToolStripMenuItem.Size = new System.Drawing.Size(178, 22);
            this.업데이트서버설정ToolStripMenuItem.Text = "업데이트 서버 설정";
            this.업데이트서버설정ToolStripMenuItem.Click += new System.EventHandler(this.업데이트서버설정ToolStripMenuItem_Click);
            // 
            // toolStripSeparator26
            // 
            this.toolStripSeparator26.Name = "toolStripSeparator26";
            this.toolStripSeparator26.Size = new System.Drawing.Size(214, 6);
            // 
            // 디버깅ToolStripMenuItem
            // 
            this.디버깅ToolStripMenuItem.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.디버깅ToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("디버깅ToolStripMenuItem.Image")));
            this.디버깅ToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.디버깅ToolStripMenuItem.Name = "디버깅ToolStripMenuItem";
            this.디버깅ToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F10;
            this.디버깅ToolStripMenuItem.Size = new System.Drawing.Size(217, 22);
            this.디버깅ToolStripMenuItem.Text = "디버깅 (개발자 전용)";
            this.디버깅ToolStripMenuItem.Click += new System.EventHandler(this.디버깅ToolStripMenuItem_Click);
            // 
            // textBox1
            // 
            this.textBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox1.Location = new System.Drawing.Point(66, 2);
            this.textBox1.Margin = new System.Windows.Forms.Padding(4, 2, 4, 2);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textBox1.Size = new System.Drawing.Size(452, 20);
            this.textBox1.TabIndex = 5;
            // 
            // lbl파일경로
            // 
            this.lbl파일경로.AutoSize = true;
            this.lbl파일경로.Location = new System.Drawing.Point(1, 8);
            this.lbl파일경로.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbl파일경로.Name = "lbl파일경로";
            this.lbl파일경로.Size = new System.Drawing.Size(57, 12);
            this.lbl파일경로.TabIndex = 6;
            this.lbl파일경로.Text = "파일 경로";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(4, 141);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(29, 12);
            this.label2.TabIndex = 0;
            this.label2.Text = "음량";
            // 
            // btn재생설정
            // 
            this.btn재생설정.Enabled = false;
            this.btn재생설정.Location = new System.Drawing.Point(78, 0);
            this.btn재생설정.Margin = new System.Windows.Forms.Padding(4, 2, 4, 2);
            this.btn재생설정.Name = "btn재생설정";
            this.btn재생설정.Size = new System.Drawing.Size(74, 22);
            this.btn재생설정.TabIndex = 7;
            this.btn재생설정.Text = "재생설정";
            this.btn재생설정.UseVisualStyleBackColor = true;
            this.btn재생설정.Click += new System.EventHandler(this.button4_Click);
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.ImageScalingSize = new System.Drawing.Size(18, 18);
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.재생ToolStripMenuItem,
            this.일시정지ToolStripMenuItem,
            this.정지ToolStripMenuItem,
            this.toolStripSeparator1,
            this.삭제ToolStripMenuItem,
            this.toolStripSeparator2,
            this.현재위치의웨이브저장ToolStripMenuItem,
            this.toolStripSeparator35,
            this.mD5해쉬값구하기ToolStripMenuItem,
            this.toolStripSeparator3,
            this.가사찾기ToolStripMenuItem,
            this.toolStripSeparator4,
            this.복사ToolStripMenuItem,
            this.탐색기에서해당파일열기ToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(217, 250);
            this.contextMenuStrip1.Opening += new System.ComponentModel.CancelEventHandler(this.contextMenuStrip1_Opening);
            // 
            // 재생ToolStripMenuItem
            // 
            this.재생ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.새인스턴스시작ToolStripMenuItem});
            this.재생ToolStripMenuItem.Enabled = false;
            this.재생ToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("재생ToolStripMenuItem.Image")));
            this.재생ToolStripMenuItem.Name = "재생ToolStripMenuItem";
            this.재생ToolStripMenuItem.Size = new System.Drawing.Size(216, 24);
            this.재생ToolStripMenuItem.Text = "재생";
            this.재생ToolStripMenuItem.Click += new System.EventHandler(this.재생ToolStripMenuItem_Click);
            // 
            // 새인스턴스시작ToolStripMenuItem
            // 
            this.새인스턴스시작ToolStripMenuItem.Checked = true;
            this.새인스턴스시작ToolStripMenuItem.CheckOnClick = true;
            this.새인스턴스시작ToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.새인스턴스시작ToolStripMenuItem.Name = "새인스턴스시작ToolStripMenuItem";
            this.새인스턴스시작ToolStripMenuItem.Size = new System.Drawing.Size(166, 22);
            this.새인스턴스시작ToolStripMenuItem.Text = "새 인스턴스 시작";
            // 
            // 일시정지ToolStripMenuItem
            // 
            this.일시정지ToolStripMenuItem.Enabled = false;
            this.일시정지ToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("일시정지ToolStripMenuItem.Image")));
            this.일시정지ToolStripMenuItem.Name = "일시정지ToolStripMenuItem";
            this.일시정지ToolStripMenuItem.Size = new System.Drawing.Size(216, 24);
            this.일시정지ToolStripMenuItem.Text = "일시정지";
            this.일시정지ToolStripMenuItem.Click += new System.EventHandler(this.일시정지ToolStripMenuItem_Click);
            // 
            // 정지ToolStripMenuItem
            // 
            this.정지ToolStripMenuItem.Enabled = false;
            this.정지ToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("정지ToolStripMenuItem.Image")));
            this.정지ToolStripMenuItem.Name = "정지ToolStripMenuItem";
            this.정지ToolStripMenuItem.Size = new System.Drawing.Size(216, 24);
            this.정지ToolStripMenuItem.Text = "정지";
            this.정지ToolStripMenuItem.Click += new System.EventHandler(this.정지ToolStripMenuItem_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(213, 6);
            // 
            // 삭제ToolStripMenuItem
            // 
            this.삭제ToolStripMenuItem.Enabled = false;
            this.삭제ToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("삭제ToolStripMenuItem.Image")));
            this.삭제ToolStripMenuItem.Name = "삭제ToolStripMenuItem";
            this.삭제ToolStripMenuItem.Size = new System.Drawing.Size(216, 24);
            this.삭제ToolStripMenuItem.Text = "삭제";
            this.삭제ToolStripMenuItem.Click += new System.EventHandler(this.삭제ToolStripMenuItem_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(213, 6);
            this.toolStripSeparator2.Visible = false;
            // 
            // 현재위치의웨이브저장ToolStripMenuItem
            // 
            this.현재위치의웨이브저장ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem2,
            this.toolStripTextBox2});
            this.현재위치의웨이브저장ToolStripMenuItem.Enabled = false;
            this.현재위치의웨이브저장ToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("현재위치의웨이브저장ToolStripMenuItem.Image")));
            this.현재위치의웨이브저장ToolStripMenuItem.Name = "현재위치의웨이브저장ToolStripMenuItem";
            this.현재위치의웨이브저장ToolStripMenuItem.Size = new System.Drawing.Size(216, 24);
            this.현재위치의웨이브저장ToolStripMenuItem.Text = "현재 위치의 웨이브 저장";
            this.현재위치의웨이브저장ToolStripMenuItem.Visible = false;
            this.현재위치의웨이브저장ToolStripMenuItem.Click += new System.EventHandler(this.toolStripMenuItem1_Click);
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(220, 22);
            this.toolStripMenuItem2.Text = "원본 크기 구하기";
            this.toolStripMenuItem2.Click += new System.EventHandler(this.toolStripMenuItem2_Click);
            // 
            // toolStripTextBox2
            // 
            this.toolStripTextBox2.Font = new System.Drawing.Font("굴림", 8.780488F);
            this.toolStripTextBox2.ForeColor = System.Drawing.SystemColors.ControlText;
            this.toolStripTextBox2.MaxLength = 15;
            this.toolStripTextBox2.Name = "toolStripTextBox2";
            this.toolStripTextBox2.Size = new System.Drawing.Size(160, 21);
            this.toolStripTextBox2.Tag = "크기 입력 (숫자만 입력가능)";
            this.toolStripTextBox2.Text = this.toolStripTextBox1.Text;
            this.toolStripTextBox2.ToolTipText = "파일로 현재 재생위치의 웨이브정보 저장할 숫자를 입력합니다.\n※주의: FFT최대사이즈보다 크게 지정하면 0으로 저장될수 있습니다.\n적당한값을 입력" +
    "해 주세요";
            this.toolStripTextBox2.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.toolStripTextBox2_KeyPress);
            this.toolStripTextBox2.Click += new System.EventHandler(this.toolStripTextBox2_Click);
            this.toolStripTextBox2.TextChanged += new System.EventHandler(this.toolStripTextBox2_TextChanged);
            // 
            // toolStripSeparator35
            // 
            this.toolStripSeparator35.Name = "toolStripSeparator35";
            this.toolStripSeparator35.Size = new System.Drawing.Size(213, 6);
            // 
            // mD5해쉬값구하기ToolStripMenuItem
            // 
            this.mD5해쉬값구하기ToolStripMenuItem.Enabled = false;
            this.mD5해쉬값구하기ToolStripMenuItem.Name = "mD5해쉬값구하기ToolStripMenuItem";
            this.mD5해쉬값구하기ToolStripMenuItem.Size = new System.Drawing.Size(216, 24);
            this.mD5해쉬값구하기ToolStripMenuItem.Text = "MD5 해쉬값 구하기";
            this.mD5해쉬값구하기ToolStripMenuItem.Click += new System.EventHandler(this.mD5해쉬값구하기ToolStripMenuItem_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(213, 6);
            // 
            // 가사찾기ToolStripMenuItem
            // 
            this.가사찾기ToolStripMenuItem.Enabled = false;
            this.가사찾기ToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("가사찾기ToolStripMenuItem.Image")));
            this.가사찾기ToolStripMenuItem.Name = "가사찾기ToolStripMenuItem";
            this.가사찾기ToolStripMenuItem.Size = new System.Drawing.Size(216, 24);
            this.가사찾기ToolStripMenuItem.Text = "선택한 곡 싱크 가사 찾기";
            this.가사찾기ToolStripMenuItem.Click += new System.EventHandler(this.가사찾기ToolStripMenuItem_Click);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(213, 6);
            // 
            // 복사ToolStripMenuItem
            // 
            this.복사ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.파일이름ToolStripMenuItem,
            this.파일경로ToolStripMenuItem});
            this.복사ToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("복사ToolStripMenuItem.Image")));
            this.복사ToolStripMenuItem.Name = "복사ToolStripMenuItem";
            this.복사ToolStripMenuItem.Size = new System.Drawing.Size(216, 24);
            this.복사ToolStripMenuItem.Text = "복사";
            // 
            // 파일이름ToolStripMenuItem
            // 
            this.파일이름ToolStripMenuItem.Name = "파일이름ToolStripMenuItem";
            this.파일이름ToolStripMenuItem.Size = new System.Drawing.Size(126, 22);
            this.파일이름ToolStripMenuItem.Text = "파일 이름";
            this.파일이름ToolStripMenuItem.Click += new System.EventHandler(this.음악이름ToolStripMenuItem_Click);
            // 
            // 파일경로ToolStripMenuItem
            // 
            this.파일경로ToolStripMenuItem.Name = "파일경로ToolStripMenuItem";
            this.파일경로ToolStripMenuItem.Size = new System.Drawing.Size(126, 22);
            this.파일경로ToolStripMenuItem.Text = "파일 경로";
            this.파일경로ToolStripMenuItem.Click += new System.EventHandler(this.음악경로ToolStripMenuItem_Click);
            // 
            // 탐색기에서해당파일열기ToolStripMenuItem
            // 
            this.탐색기에서해당파일열기ToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("탐색기에서해당파일열기ToolStripMenuItem.Image")));
            this.탐색기에서해당파일열기ToolStripMenuItem.Name = "탐색기에서해당파일열기ToolStripMenuItem";
            this.탐색기에서해당파일열기ToolStripMenuItem.Size = new System.Drawing.Size(216, 24);
            this.탐색기에서해당파일열기ToolStripMenuItem.Text = "탐색기에서 해당파일 열기";
            this.탐색기에서해당파일열기ToolStripMenuItem.Click += new System.EventHandler(this.탐색기에서해당파일열기ToolStripMenuItem_Click);
            // 
            // textBox2
            // 
            this.textBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox2.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox2.Location = new System.Drawing.Point(122, 56);
            this.textBox2.Margin = new System.Windows.Forms.Padding(4, 2, 4, 2);
            this.textBox2.Multiline = true;
            this.textBox2.Name = "textBox2";
            this.textBox2.ReadOnly = true;
            this.textBox2.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textBox2.Size = new System.Drawing.Size(455, 16);
            this.textBox2.TabIndex = 10;
            this.textBox2.WordWrap = false;
            // 
            // lblSelectMusicPath
            // 
            this.lblSelectMusicPath.AutoSize = true;
            this.lblSelectMusicPath.Location = new System.Drawing.Point(1, 56);
            this.lblSelectMusicPath.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblSelectMusicPath.Name = "lblSelectMusicPath";
            this.lblSelectMusicPath.Size = new System.Drawing.Size(101, 12);
            this.lblSelectMusicPath.TabIndex = 11;
            this.lblSelectMusicPath.Text = "선택한 음악경로: ";
            // 
            // timer1
            // 
            this.timer1.Interval = 25;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // openFileDialog2
            // 
            this.openFileDialog2.DefaultExt = "*.m3u";
            this.openFileDialog2.FileName = "새 재생목록.m3u";
            this.openFileDialog2.Filter = "재생목록 파일 (*.m3u)|*m3u|UTF-8 로 인코딩 된 재생목록 파일 (*.m3u8)|*.m3u8";
            this.openFileDialog2.ShowReadOnly = true;
            this.openFileDialog2.Title = "재생목록 열기...";
            this.openFileDialog2.FileOk += new System.ComponentModel.CancelEventHandler(this.openFileDialog2_FileOk);
            // 
            // statusStrip1
            // 
            this.statusStrip1.AutoSize = false;
            this.statusStrip1.ImageScalingSize = new System.Drawing.Size(22, 22);
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel2,
            this.toolStripProgressBar1,
            this.toolStripStatusLabel3,
            this.toolStripStatusLabel1,
            this.toolStripProgressBar2});
            this.statusStrip1.Location = new System.Drawing.Point(0, 292);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(578, 22);
            this.statusStrip1.SizingGrip = false;
            this.statusStrip1.TabIndex = 12;
            this.statusStrip1.Text = "statusStrip1";
            this.statusStrip1.SizeChanged += new System.EventHandler(this.statusStrip1_SizeChanged);
            // 
            // toolStripStatusLabel2
            // 
            this.toolStripStatusLabel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.toolStripStatusLabel2.BorderSides = ((System.Windows.Forms.ToolStripStatusLabelBorderSides)((((System.Windows.Forms.ToolStripStatusLabelBorderSides.Left | System.Windows.Forms.ToolStripStatusLabelBorderSides.Top) 
            | System.Windows.Forms.ToolStripStatusLabelBorderSides.Right) 
            | System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom)));
            this.toolStripStatusLabel2.BorderStyle = System.Windows.Forms.Border3DStyle.Raised;
            this.toolStripStatusLabel2.Name = "toolStripStatusLabel2";
            this.toolStripStatusLabel2.Size = new System.Drawing.Size(35, 17);
            this.toolStripStatusLabel2.Text = "취소";
            this.toolStripStatusLabel2.Visible = false;
            this.toolStripStatusLabel2.Click += new System.EventHandler(this.toolStripStatusLabel2_Click);
            // 
            // toolStripProgressBar1
            // 
            this.toolStripProgressBar1.Name = "toolStripProgressBar1";
            this.toolStripProgressBar1.Size = new System.Drawing.Size(101, 16);
            this.toolStripProgressBar1.Visible = false;
            // 
            // toolStripStatusLabel3
            // 
            this.toolStripStatusLabel3.Name = "toolStripStatusLabel3";
            this.toolStripStatusLabel3.Size = new System.Drawing.Size(31, 17);
            this.toolStripStatusLabel3.Text = "준비";
            this.toolStripStatusLabel3.Visible = false;
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(31, 17);
            this.toolStripStatusLabel1.Text = "준비";
            this.toolStripStatusLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // toolStripProgressBar2
            // 
            this.toolStripProgressBar2.AutoSize = false;
            this.toolStripProgressBar2.Name = "toolStripProgressBar2";
            this.toolStripProgressBar2.Size = new System.Drawing.Size(200, 16);
            this.toolStripProgressBar2.ToolTipText = "(재생할 수 있는 경우) 클릭하여 음악의 위치를 변경합니다.";
            this.toolStripProgressBar2.Visible = false;
            this.toolStripProgressBar2.MouseDown += new System.Windows.Forms.MouseEventHandler(this.toolStripProgressBar2_MouseDown);
            this.toolStripProgressBar2.MouseEnter += new System.EventHandler(this.toolStripProgressBar2_MouseEnter);
            this.toolStripProgressBar2.MouseLeave += new System.EventHandler(this.toolStripProgressBar2_MouseLeave);
            this.toolStripProgressBar2.MouseMove += new System.Windows.Forms.MouseEventHandler(this.toolStripProgressBar2_MouseMove);
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.Controls.Add(this.lbl파일경로);
            this.panel1.Controls.Add(this.btn추가);
            this.panel1.Controls.Add(this.textBox1);
            this.panel1.Enabled = false;
            this.panel1.Location = new System.Drawing.Point(0, 26);
            this.panel1.Margin = new System.Windows.Forms.Padding(4, 2, 4, 2);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(578, 26);
            this.panel1.TabIndex = 13;
            // 
            // saveFileDialog1
            // 
            this.saveFileDialog1.DefaultExt = "*.m3u";
            this.saveFileDialog1.FileName = "새 재생목록.m3u";
            this.saveFileDialog1.Filter = "재생목록 파일 (*.m3u)|*m3u|UTF-8 로 인코딩 된 재생목록 파일 (*.m3u8)|*.m3u8|Resource단위 재생목록 (*.lst" +
    "res)|*.lstres";
            this.saveFileDialog1.Tag = "재생목록 파일(*.m3u)|*.m3u|Resource단위 재생목록 (*.lstres)|*.lstres";
            this.saveFileDialog1.Title = "재생목록 저장...";
            this.saveFileDialog1.FileOk += new System.ComponentModel.CancelEventHandler(this.saveFileDialog1_FileOk);
            // 
            // saveFileDialog2
            // 
            this.saveFileDialog2.Filter = "텍스트 파일 (*.txt)|*.txt|모든 파일 (*.*)|*.*";
            this.saveFileDialog2.SupportMultiDottedExtensions = true;
            this.saveFileDialog2.FileOk += new System.ComponentModel.CancelEventHandler(this.saveFileDialog2_FileOk);
            // 
            // timer2
            // 
            this.timer2.Interval = 500;
            this.timer2.Tick += new System.EventHandler(this.timer2_Tick);
            // 
            // button5
            // 
            this.button5.Enabled = false;
            this.button5.Location = new System.Drawing.Point(0, 0);
            this.button5.Margin = new System.Windows.Forms.Padding(4, 2, 4, 2);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(74, 22);
            this.button5.TabIndex = 15;
            this.button5.Text = "일시정지";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Visible = false;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.cb반복);
            this.panel2.Controls.Add(this.btn정지);
            this.panel2.Controls.Add(this.btn재생설정);
            this.panel2.Controls.Add(this.button5);
            this.panel2.Location = new System.Drawing.Point(80, 75);
            this.panel2.Margin = new System.Windows.Forms.Padding(4, 2, 4, 2);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(256, 22);
            this.panel2.TabIndex = 16;
            // 
            // cb반복
            // 
            this.cb반복.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb반복.FormattingEnabled = true;
            this.cb반복.Items.AddRange(new object[] {
            "전체 반복",
            "랜덤 재생",
            "1곡 반복",
            "1번만 재생",
            "반복 없음"});
            this.cb반복.Location = new System.Drawing.Point(158, 2);
            this.cb반복.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.cb반복.Name = "cb반복";
            this.cb반복.Size = new System.Drawing.Size(98, 20);
            this.cb반복.TabIndex = 28;
            // 
            // timer3
            // 
            this.timer3.Tick += new System.EventHandler(this.timer3_Tick);
            // 
            // folderBrowserDialog1
            // 
            this.folderBrowserDialog1.Description = "음악 파일이 들어있는 폴더 선택";
            // 
            // openFileDialog3
            // 
            this.openFileDialog3.FileName = "제목 없음";
            this.openFileDialog3.Filter = "프로젝트 파일(*.project)|*.project";
            // 
            // notifyIcon1
            // 
            this.notifyIcon1.ContextMenuStrip = this.contextMenuStrip2;
            this.notifyIcon1.Icon = ((System.Drawing.Icon)(resources.GetObject("notifyIcon1.Icon")));
            this.notifyIcon1.Text = "HS 플레이어";
            this.notifyIcon1.Visible = true;
            this.notifyIcon1.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.notifyIcon1_MouseDoubleClick);
            this.notifyIcon1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.notifyIcon1_MouseMove);
            // 
            // contextMenuStrip2
            // 
            this.contextMenuStrip2.ImageScalingSize = new System.Drawing.Size(18, 18);
            this.contextMenuStrip2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.설정TToolStripMenuItem,
            this.보기VToolStripMenuItem1,
            this.도움말HToolStripMenuItem1,
            this.toolStripSeparator15,
            this.재생컨트롤ToolStripMenuItem,
            this.toolStripSeparator16,
            this.재생설정열기ToolStripMenuItem,
            this.메인창열기ToolStripMenuItem,
            this.toolStripSeparator20,
            this.끝내기XToolStripMenuItem});
            this.contextMenuStrip2.Name = "contextMenuStrip2";
            this.contextMenuStrip2.Size = new System.Drawing.Size(157, 190);
            this.contextMenuStrip2.Opening += new System.ComponentModel.CancelEventHandler(this.contextMenuStrip2_Opening);
            // 
            // 설정TToolStripMenuItem
            // 
            this.설정TToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem5,
            this.toolStripSeparator27,
            this.파일복구및재설치ToolStripMenuItem1,
            this.toolStripSeparator18,
            this.프로그램다시시작ToolStripMenuItem1,
            this.toolStripSeparator19,
            this.toolStripMenuItem7});
            this.설정TToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("설정TToolStripMenuItem.Image")));
            this.설정TToolStripMenuItem.Name = "설정TToolStripMenuItem";
            this.설정TToolStripMenuItem.Size = new System.Drawing.Size(156, 24);
            this.설정TToolStripMenuItem.Text = "설정(&T)";
            // 
            // toolStripMenuItem5
            // 
            this.toolStripMenuItem5.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripComboBox2});
            this.toolStripMenuItem5.Name = "toolStripMenuItem5";
            this.toolStripMenuItem5.Size = new System.Drawing.Size(290, 22);
            this.toolStripMenuItem5.Text = "언어";
            // 
            // toolStripComboBox2
            // 
            this.toolStripComboBox2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.toolStripComboBox2.Items.AddRange(new object[] {
            "한국어",
            "日本語"});
            this.toolStripComboBox2.Name = "toolStripComboBox2";
            this.toolStripComboBox2.Size = new System.Drawing.Size(121, 23);
            this.toolStripComboBox2.SelectedIndexChanged += new System.EventHandler(this.toolStripComboBox2_SelectedIndexChanged);
            // 
            // toolStripSeparator27
            // 
            this.toolStripSeparator27.Name = "toolStripSeparator27";
            this.toolStripSeparator27.Size = new System.Drawing.Size(287, 6);
            // 
            // 파일복구및재설치ToolStripMenuItem1
            // 
            this.파일복구및재설치ToolStripMenuItem1.Name = "파일복구및재설치ToolStripMenuItem1";
            this.파일복구및재설치ToolStripMenuItem1.Size = new System.Drawing.Size(290, 22);
            this.파일복구및재설치ToolStripMenuItem1.Text = "사운드 시스템 엔진 파일 복구 및 재설치";
            this.파일복구및재설치ToolStripMenuItem1.Click += new System.EventHandler(this.파일복구및재설치ToolStripMenuItem_Click);
            // 
            // toolStripSeparator18
            // 
            this.toolStripSeparator18.Name = "toolStripSeparator18";
            this.toolStripSeparator18.Size = new System.Drawing.Size(287, 6);
            // 
            // 프로그램다시시작ToolStripMenuItem1
            // 
            this.프로그램다시시작ToolStripMenuItem1.Name = "프로그램다시시작ToolStripMenuItem1";
            this.프로그램다시시작ToolStripMenuItem1.Size = new System.Drawing.Size(290, 22);
            this.프로그램다시시작ToolStripMenuItem1.Text = "프로그램 다시 시작";
            this.프로그램다시시작ToolStripMenuItem1.Click += new System.EventHandler(this.프로그램다시시작ToolStripMenuItem_Click);
            // 
            // toolStripSeparator19
            // 
            this.toolStripSeparator19.Name = "toolStripSeparator19";
            this.toolStripSeparator19.Size = new System.Drawing.Size(287, 6);
            // 
            // toolStripMenuItem7
            // 
            this.toolStripMenuItem7.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.높음ToolStripMenuItem1,
            this.높은우선순위ToolStripMenuItem1,
            this.보통권장ToolStripMenuItem1,
            this.낮은우선순위ToolStripMenuItem1});
            this.toolStripMenuItem7.Name = "toolStripMenuItem7";
            this.toolStripMenuItem7.Size = new System.Drawing.Size(290, 22);
            this.toolStripMenuItem7.Text = "프로그램 우선순위";
            // 
            // 높음ToolStripMenuItem1
            // 
            this.높음ToolStripMenuItem1.Name = "높음ToolStripMenuItem1";
            this.높음ToolStripMenuItem1.Size = new System.Drawing.Size(178, 22);
            this.높음ToolStripMenuItem1.Text = "높음";
            this.높음ToolStripMenuItem1.Click += new System.EventHandler(this.높음ToolStripMenuItem_Click);
            // 
            // 높은우선순위ToolStripMenuItem1
            // 
            this.높은우선순위ToolStripMenuItem1.Name = "높은우선순위ToolStripMenuItem1";
            this.높은우선순위ToolStripMenuItem1.Size = new System.Drawing.Size(178, 22);
            this.높은우선순위ToolStripMenuItem1.Text = "이 프로그램 우선";
            this.높은우선순위ToolStripMenuItem1.Click += new System.EventHandler(this.높은우선순위ToolStripMenuItem_Click);
            // 
            // 보통권장ToolStripMenuItem1
            // 
            this.보통권장ToolStripMenuItem1.Checked = true;
            this.보통권장ToolStripMenuItem1.CheckState = System.Windows.Forms.CheckState.Checked;
            this.보통권장ToolStripMenuItem1.Name = "보통권장ToolStripMenuItem1";
            this.보통권장ToolStripMenuItem1.Size = new System.Drawing.Size(178, 22);
            this.보통권장ToolStripMenuItem1.Text = "보통 (권장)";
            this.보통권장ToolStripMenuItem1.Click += new System.EventHandler(this.보통권장ToolStripMenuItem_Click);
            // 
            // 낮은우선순위ToolStripMenuItem1
            // 
            this.낮은우선순위ToolStripMenuItem1.Name = "낮은우선순위ToolStripMenuItem1";
            this.낮은우선순위ToolStripMenuItem1.Size = new System.Drawing.Size(178, 22);
            this.낮은우선순위ToolStripMenuItem1.Text = "다른 프로그램 우선";
            this.낮은우선순위ToolStripMenuItem1.Click += new System.EventHandler(this.낮은우선순위ToolStripMenuItem_Click);
            // 
            // 보기VToolStripMenuItem1
            // 
            this.보기VToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.바탕화면싱크가사창띄우기ToolStripMenuItem1});
            this.보기VToolStripMenuItem1.Image = ((System.Drawing.Image)(resources.GetObject("보기VToolStripMenuItem1.Image")));
            this.보기VToolStripMenuItem1.Name = "보기VToolStripMenuItem1";
            this.보기VToolStripMenuItem1.Size = new System.Drawing.Size(156, 24);
            this.보기VToolStripMenuItem1.Text = "보기(&V)";
            // 
            // 바탕화면싱크가사창띄우기ToolStripMenuItem1
            // 
            this.바탕화면싱크가사창띄우기ToolStripMenuItem1.Name = "바탕화면싱크가사창띄우기ToolStripMenuItem1";
            this.바탕화면싱크가사창띄우기ToolStripMenuItem1.Size = new System.Drawing.Size(230, 22);
            this.바탕화면싱크가사창띄우기ToolStripMenuItem1.Text = "바탕화면 싱크 가사창 띄우기";
            this.바탕화면싱크가사창띄우기ToolStripMenuItem1.Click += new System.EventHandler(this.바탕화면싱크가사창띄우기ToolStripMenuItem_Click);
            // 
            // 도움말HToolStripMenuItem1
            // 
            this.도움말HToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.정보ToolStripMenuItem1,
            this.업데이트변경사항보기ToolStripMenuItem1,
            this.toolStripSeparator17,
            this.디버깅ToolStripMenuItem1});
            this.도움말HToolStripMenuItem1.Name = "도움말HToolStripMenuItem1";
            this.도움말HToolStripMenuItem1.Size = new System.Drawing.Size(156, 24);
            this.도움말HToolStripMenuItem1.Text = "도움말(&H)";
            this.도움말HToolStripMenuItem1.Click += new System.EventHandler(this.도움말HToolStripMenuItem_DropDownOpening);
            // 
            // 정보ToolStripMenuItem1
            // 
            this.정보ToolStripMenuItem1.Image = ((System.Drawing.Image)(resources.GetObject("정보ToolStripMenuItem1.Image")));
            this.정보ToolStripMenuItem1.Name = "정보ToolStripMenuItem1";
            this.정보ToolStripMenuItem1.Size = new System.Drawing.Size(217, 22);
            this.정보ToolStripMenuItem1.Text = "프로그램 정보...(&A)";
            this.정보ToolStripMenuItem1.Click += new System.EventHandler(this.정보ToolStripMenuItem_Click);
            // 
            // 업데이트변경사항보기ToolStripMenuItem1
            // 
            this.업데이트변경사항보기ToolStripMenuItem1.Image = ((System.Drawing.Image)(resources.GetObject("업데이트변경사항보기ToolStripMenuItem1.Image")));
            this.업데이트변경사항보기ToolStripMenuItem1.Name = "업데이트변경사항보기ToolStripMenuItem1";
            this.업데이트변경사항보기ToolStripMenuItem1.Size = new System.Drawing.Size(217, 22);
            this.업데이트변경사항보기ToolStripMenuItem1.Text = "업데이트 변경사항 보기(&R)";
            this.업데이트변경사항보기ToolStripMenuItem1.Click += new System.EventHandler(this.업데이트변경사항보기ToolStripMenuItem_Click);
            // 
            // toolStripSeparator17
            // 
            this.toolStripSeparator17.Name = "toolStripSeparator17";
            this.toolStripSeparator17.Size = new System.Drawing.Size(214, 6);
            // 
            // 디버깅ToolStripMenuItem1
            // 
            this.디버깅ToolStripMenuItem1.Name = "디버깅ToolStripMenuItem1";
            this.디버깅ToolStripMenuItem1.ShortcutKeys = System.Windows.Forms.Keys.F10;
            this.디버깅ToolStripMenuItem1.Size = new System.Drawing.Size(217, 22);
            this.디버깅ToolStripMenuItem1.Text = "디버깅 (개발자 전용)";
            this.디버깅ToolStripMenuItem1.Click += new System.EventHandler(this.디버깅ToolStripMenuItem_Click);
            // 
            // toolStripSeparator15
            // 
            this.toolStripSeparator15.Name = "toolStripSeparator15";
            this.toolStripSeparator15.Size = new System.Drawing.Size(153, 6);
            // 
            // 재생컨트롤ToolStripMenuItem
            // 
            this.재생컨트롤ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.재생ToolStripMenuItem1,
            this.다음ToolStripMenuItem,
            this.toolStripSeparator37,
            this.정지ToolStripMenuItem1});
            this.재생컨트롤ToolStripMenuItem.Name = "재생컨트롤ToolStripMenuItem";
            this.재생컨트롤ToolStripMenuItem.Size = new System.Drawing.Size(156, 24);
            this.재생컨트롤ToolStripMenuItem.Text = "재생 컨트롤";
            this.재생컨트롤ToolStripMenuItem.DropDownOpening += new System.EventHandler(this.재생컨트롤ToolStripMenuItem_DropDownOpening);
            // 
            // 재생ToolStripMenuItem1
            // 
            this.재생ToolStripMenuItem1.Image = ((System.Drawing.Image)(resources.GetObject("재생ToolStripMenuItem1.Image")));
            this.재생ToolStripMenuItem1.Name = "재생ToolStripMenuItem1";
            this.재생ToolStripMenuItem1.Size = new System.Drawing.Size(98, 22);
            this.재생ToolStripMenuItem1.Text = "재생";
            this.재생ToolStripMenuItem1.Click += new System.EventHandler(this.button1_Click);
            // 
            // 다음ToolStripMenuItem
            // 
            this.다음ToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("다음ToolStripMenuItem.Image")));
            this.다음ToolStripMenuItem.Name = "다음ToolStripMenuItem";
            this.다음ToolStripMenuItem.Size = new System.Drawing.Size(98, 22);
            this.다음ToolStripMenuItem.Text = "다음";
            this.다음ToolStripMenuItem.Click += new System.EventHandler(this.다음ToolStripMenuItem_Click);
            // 
            // toolStripSeparator37
            // 
            this.toolStripSeparator37.Name = "toolStripSeparator37";
            this.toolStripSeparator37.Size = new System.Drawing.Size(95, 6);
            // 
            // 정지ToolStripMenuItem1
            // 
            this.정지ToolStripMenuItem1.Image = ((System.Drawing.Image)(resources.GetObject("정지ToolStripMenuItem1.Image")));
            this.정지ToolStripMenuItem1.Name = "정지ToolStripMenuItem1";
            this.정지ToolStripMenuItem1.Size = new System.Drawing.Size(98, 22);
            this.정지ToolStripMenuItem1.Text = "정지";
            this.정지ToolStripMenuItem1.Click += new System.EventHandler(this.button2_Click);
            // 
            // toolStripSeparator16
            // 
            this.toolStripSeparator16.Name = "toolStripSeparator16";
            this.toolStripSeparator16.Size = new System.Drawing.Size(153, 6);
            // 
            // 재생설정열기ToolStripMenuItem
            // 
            this.재생설정열기ToolStripMenuItem.Name = "재생설정열기ToolStripMenuItem";
            this.재생설정열기ToolStripMenuItem.Size = new System.Drawing.Size(156, 24);
            this.재생설정열기ToolStripMenuItem.Text = "재생 설정 열기";
            this.재생설정열기ToolStripMenuItem.Click += new System.EventHandler(this.button4_Click);
            // 
            // 메인창열기ToolStripMenuItem
            // 
            this.메인창열기ToolStripMenuItem.Name = "메인창열기ToolStripMenuItem";
            this.메인창열기ToolStripMenuItem.Size = new System.Drawing.Size(156, 24);
            this.메인창열기ToolStripMenuItem.Text = "메인창 열기";
            this.메인창열기ToolStripMenuItem.Click += new System.EventHandler(this.메인창열기ToolStripMenuItem_Click);
            // 
            // toolStripSeparator20
            // 
            this.toolStripSeparator20.Name = "toolStripSeparator20";
            this.toolStripSeparator20.Size = new System.Drawing.Size(153, 6);
            // 
            // 끝내기XToolStripMenuItem
            // 
            this.끝내기XToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("끝내기XToolStripMenuItem.Image")));
            this.끝내기XToolStripMenuItem.Name = "끝내기XToolStripMenuItem";
            this.끝내기XToolStripMenuItem.Size = new System.Drawing.Size(156, 24);
            this.끝내기XToolStripMenuItem.Text = "끝내기(&X)";
            this.끝내기XToolStripMenuItem.Click += new System.EventHandler(this.끝내기ToolStripMenuItem_Click);
            // 
            // btnExpand
            // 
            this.btnExpand.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnExpand.ForeColor = System.Drawing.Color.Blue;
            this.btnExpand.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.btnExpand.Location = new System.Drawing.Point(200, 54);
            this.btnExpand.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btnExpand.Name = "btnExpand";
            this.btnExpand.Size = new System.Drawing.Size(230, 20);
            this.btnExpand.TabIndex = 18;
            this.btnExpand.Text = "▲";
            this.btnExpand.UseVisualStyleBackColor = true;
            this.btnExpand.Visible = false;
            // 
            // btnScrollDown
            // 
            this.btnScrollDown.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnScrollDown.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnScrollDown.Location = new System.Drawing.Point(557, 190);
            this.btnScrollDown.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btnScrollDown.Name = "btnScrollDown";
            this.btnScrollDown.Size = new System.Drawing.Size(18, 44);
            this.btnScrollDown.TabIndex = 17;
            this.btnScrollDown.Text = "↓";
            this.toolTip1.SetToolTip(this.btnScrollDown, "맨 아래로 스크롤");
            this.btnScrollDown.UseVisualStyleBackColor = true;
            this.btnScrollDown.Click += new System.EventHandler(this.btnScrollDown_Click);
            // 
            // btnScrollUp
            // 
            this.btnScrollUp.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnScrollUp.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnScrollUp.Location = new System.Drawing.Point(557, 147);
            this.btnScrollUp.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btnScrollUp.Name = "btnScrollUp";
            this.btnScrollUp.Size = new System.Drawing.Size(18, 44);
            this.btnScrollUp.TabIndex = 19;
            this.btnScrollUp.Text = "↑";
            this.toolTip1.SetToolTip(this.btnScrollUp, "맨 위로 스크롤");
            this.btnScrollUp.UseVisualStyleBackColor = true;
            this.btnScrollUp.Click += new System.EventHandler(this.btnScrollUp_Click);
            // 
            // btnItemFocus
            // 
            this.btnItemFocus.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnItemFocus.Enabled = false;
            this.btnItemFocus.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnItemFocus.Location = new System.Drawing.Point(557, 102);
            this.btnItemFocus.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btnItemFocus.Name = "btnItemFocus";
            this.btnItemFocus.Size = new System.Drawing.Size(18, 44);
            this.btnItemFocus.TabIndex = 20;
            this.btnItemFocus.Text = "※";
            this.toolTip1.SetToolTip(this.btnItemFocus, "현재 항목 포커스");
            this.btnItemFocus.UseVisualStyleBackColor = true;
            this.btnItemFocus.Click += new System.EventHandler(this.btnItemFocus_Click);
            // 
            // textBox3
            // 
            this.textBox3.Location = new System.Drawing.Point(343, 76);
            this.textBox3.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(155, 21);
            this.textBox3.TabIndex = 21;
            this.toolTip1.SetToolTip(this.textBox3, "검색어를 입력 하세요.");
            this.textBox3.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox3_KeyPress);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.ForeColor = System.Drawing.Color.Blue;
            this.label1.Location = new System.Drawing.Point(531, 79);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(27, 12);
            this.label1.TabIndex = 26;
            this.label1.Text = "0 개";
            this.toolTip1.SetToolTip(this.label1, "일치하는 갯수");
            // 
            // btn프로그램다시시작
            // 
            this.btn프로그램다시시작.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn프로그램다시시작.Image = ((System.Drawing.Image)(resources.GetObject("btn프로그램다시시작.Image")));
            this.btn프로그램다시시작.Location = new System.Drawing.Point(550, 2);
            this.btn프로그램다시시작.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btn프로그램다시시작.Name = "btn프로그램다시시작";
            this.btn프로그램다시시작.Size = new System.Drawing.Size(28, 24);
            this.btn프로그램다시시작.TabIndex = 27;
            this.toolTip1.SetToolTip(this.btn프로그램다시시작, "프로그램 다시 시작");
            this.btn프로그램다시시작.UseVisualStyleBackColor = true;
            this.btn프로그램다시시작.Click += new System.EventHandler(this.프로그램다시시작ToolStripMenuItem_Click);
            // 
            // colorDialog1
            // 
            this.colorDialog1.AnyColor = true;
            this.colorDialog1.Color = System.Drawing.Color.White;
            this.colorDialog1.FullOpen = true;
            // 
            // colorDialog2
            // 
            this.colorDialog2.AnyColor = true;
            this.colorDialog2.Color = System.Drawing.Color.Lime;
            this.colorDialog2.FullOpen = true;
            // 
            // timer_파일추가
            // 
            this.timer_파일추가.Interval = 25;
            this.timer_파일추가.Tick += new System.EventHandler(this.timer_파일추가_Tick);
            // 
            // btnSearchPlayList
            // 
            this.btnSearchPlayList.Image = ((System.Drawing.Image)(resources.GetObject("btnSearchPlayList.Image")));
            this.btnSearchPlayList.Location = new System.Drawing.Point(504, 75);
            this.btnSearchPlayList.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btnSearchPlayList.Name = "btnSearchPlayList";
            this.btnSearchPlayList.Size = new System.Drawing.Size(24, 22);
            this.btnSearchPlayList.TabIndex = 25;
            this.btnSearchPlayList.UseVisualStyleBackColor = true;
            this.btnSearchPlayList.Click += new System.EventHandler(this.btnSearchPlayList_Click);
            // 
            // saveFileDialog4
            // 
            this.saveFileDialog4.DefaultExt = "*.bat";
            this.saveFileDialog4.Filter = "Windows 배치 파일 (*.bat)|*.bat|Windows 명령어 스크립트 (*.cmd)|*.cmd";
            this.saveFileDialog4.Title = "현재 상태 저장";
            // 
            // listView1
            // 
            this.listView1.Activation = System.Windows.Forms.ItemActivation.OneClick;
            this.listView1.AllowDrop = true;
            this.listView1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.listView1.BackColor = System.Drawing.Color.White;
            this.listView1.BackgroundImageTiled = true;
            this.listView1.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2,
            this.columnHeader3,
            this.columnHeader4,
            this.columnHeader5});
            this.listView1.ContextMenuStrip = this.contextMenuStrip1;
            this.listView1.ForeColor = System.Drawing.Color.Black;
            this.listView1.FullRowSelect = true;
            this.listView1.GridLines = true;
            this.listView1.LabelWrap = false;
            this.listView1.Location = new System.Drawing.Point(0, 102);
            this.listView1.Margin = new System.Windows.Forms.Padding(4, 2, 4, 2);
            this.listView1.Name = "listView1";
            this.listView1.Size = new System.Drawing.Size(557, 188);
            this.listView1.TabIndex = 8;
            this.listView1.UseCompatibleStateImageBehavior = false;
            this.listView1.View = System.Windows.Forms.View.Details;
            this.listView1.SelectedIndexChanged += new System.EventHandler(this.listView1_SelectedIndexChanged);
            this.listView1.SizeChanged += new System.EventHandler(this.listView1_SizeChanged);
            this.listView1.DragDrop += new System.Windows.Forms.DragEventHandler(this.listView1_DragDrop);
            this.listView1.DragEnter += new System.Windows.Forms.DragEventHandler(this.listView1_DragEnter);
            this.listView1.DragOver += new System.Windows.Forms.DragEventHandler(this.listView1_DragOver);
            this.listView1.DoubleClick += new System.EventHandler(this.listView1_DoubleClick);
            this.listView1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.listView1_KeyPress);
            this.listView1.KeyUp += new System.Windows.Forms.KeyEventHandler(this.listView1_KeyUp);
            this.listView1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.listView1_MouseDown);
            this.listView1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.listView1_MouseMove);
            this.listView1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.listView1_MouseUp);
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "ID";
            this.columnHeader1.Width = 34;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "상태";
            this.columnHeader2.Width = 102;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "파일 이름";
            this.columnHeader3.Width = 127;
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "파일 경로";
            this.columnHeader4.Width = 289;
            // 
            // columnHeader5
            // 
            this.columnHeader5.Text = "현재 시간";
            this.columnHeader5.Width = 0;
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(578, 314);
            this.Controls.Add(this.btn프로그램다시시작);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnSearchPlayList);
            this.Controls.Add(this.textBox3);
            this.Controls.Add(this.btnItemFocus);
            this.Controls.Add(this.btnScrollUp);
            this.Controls.Add(this.btnExpand);
            this.Controls.Add(this.btnScrollDown);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.lblSelectMusicPath);
            this.Controls.Add(this.listView1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.btn재생);
            this.Controls.Add(this.menuStrip1);
            this.DoubleBuffered = true;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.MainMenuStrip = this.menuStrip1;
            this.Margin = new System.Windows.Forms.Padding(4, 2, 4, 2);
            this.Name = "frmMain";
            this.Text = "HS 플레이어 (Made by 박홍식)";
            this.MinimumSizeChanged += new System.EventHandler(this.frmMain_MinimumSizeChanged);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.SizeChanged += new System.EventHandler(this.Form1_SizeChanged);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.frmMain_KeyDown);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.contextMenuStrip1.ResumeLayout(false);
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.contextMenuStrip2.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btn정지;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Button btn추가;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem 파일FToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 열기OToolStripMenuItem;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label lbl파일경로;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btn재생설정;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem 재생ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 정지ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 삭제ToolStripMenuItem;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.ToolStripMenuItem 일시정지ToolStripMenuItem;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.Label lblSelectMusicPath;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private System.Windows.Forms.ColumnHeader columnHeader5;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.ToolStripMenuItem 재생목록열기ToolStripMenuItem;
        private System.Windows.Forms.OpenFileDialog openFileDialog2;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.ToolStripProgressBar toolStripProgressBar1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel2;
        private System.Windows.Forms.ToolStripMenuItem 설정ToolStripMenuItem;
        private System.Windows.Forms.Panel panel1;
        public System.Windows.Forms.ToolStripMenuItem 음악믹싱모드ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 재생목록다른이름으로저장ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 파일로웨이브저장ToolStripMenuItem;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.SaveFileDialog saveFileDialog2;
        private System.Windows.Forms.ToolStripTextBox toolStripTextBox1;
        private System.Windows.Forms.ToolStripMenuItem 원본사이즈사용ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 도움말HToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 정보ToolStripMenuItem;
        private System.Windows.Forms.Timer timer2;
        private System.Windows.Forms.ToolStripMenuItem 끝내기ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 현재위치의웨이브저장ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem2;
        private System.Windows.Forms.ToolStripTextBox toolStripTextBox2;
        private System.Windows.Forms.ToolStripMenuItem ANISToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem UTF8ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 유니코드ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem4;
        private System.Windows.Forms.ToolStripMenuItem 우선순위ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 높음ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 높은우선순위ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 보통권장ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 낮은우선순위ToolStripMenuItem;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.ToolStripMenuItem LOL우선순위ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem LOL높은우선순위ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem LOL보통권장ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 안전모드ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 디버깅ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem asyncToolStripMenuItem;
        private System.Windows.Forms.ToolStripProgressBar toolStripProgressBar2;
        private System.Windows.Forms.ToolStripMenuItem 프로그램다시시작ToolStripMenuItem;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel3;
        private System.Windows.Forms.ToolStripMenuItem 업데이트변경사항보기ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 복사ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 파일이름ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 파일경로ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 탐색기에서해당파일열기ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 가사찾기ToolStripMenuItem;
        private System.Windows.Forms.Timer timer3;
        private System.Windows.Forms.ToolStripMenuItem 새인스턴스시작ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 맨위로설정ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 보기VToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 상태표시줄업데이트ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 파일열기ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 폴더열기ToolStripMenuItem;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
        private System.Windows.Forms.ToolStripComboBox cbMimetype;
        private System.Windows.Forms.ToolStripMenuItem 하위폴더검색ToolStripMenuItem;
        private System.Windows.Forms.Button btn재생;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.ToolStripMenuItem 프로젝트저장ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 프로젝트열기ToolStripMenuItem;
        private System.Windows.Forms.OpenFileDialog openFileDialog3;
        private System.Windows.Forms.SaveFileDialog saveFileDialog3;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator6;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator7;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator8;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator9;
        private System.Windows.Forms.ToolStripMenuItem 파일복구및재설치ToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator11;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator12;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator13;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator14;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip2;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator15;
        private System.Windows.Forms.ToolStripMenuItem 재생컨트롤ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 재생ToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem 정지ToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem 메인창열기ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 트레이로보내기ToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator16;
        private System.Windows.Forms.ToolStripMenuItem 보기VToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem 바탕화면싱크가사창띄우기ToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem 설정TToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 파일복구및재설치ToolStripMenuItem1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator18;
        private System.Windows.Forms.ToolStripMenuItem 프로그램다시시작ToolStripMenuItem1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator19;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem7;
        private System.Windows.Forms.ToolStripMenuItem 높음ToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem 높은우선순위ToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem 보통권장ToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem 낮은우선순위ToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem 도움말HToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem 정보ToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem 업데이트변경사항보기ToolStripMenuItem1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator17;
        private System.Windows.Forms.ToolStripMenuItem 디버깅ToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem 끝내기XToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 재생설정열기ToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator20;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator21;
        private System.Windows.Forms.ToolStripMenuItem mD5해쉬값구하기ToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator22;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator24;
        private System.Windows.Forms.ToolStripMenuItem 재생상세정보ToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator25;
        private System.Windows.Forms.ToolStripMenuItem 가비지커렉션가동ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 업데이트확인ToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator26;
        internal System.Windows.Forms.NotifyIcon notifyIcon1;
        private System.Windows.Forms.ToolStripMenuItem 업데이트서버설정ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem testToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 언어ToolStripMenuItem;
        private System.Windows.Forms.ToolStripComboBox toolStripComboBox1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator28;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem5;
        private System.Windows.Forms.ToolStripComboBox toolStripComboBox2;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator27;
        private System.Windows.Forms.ToolStripMenuItem 로그관리ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 로그폴더열기ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 로그지우기ToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator29;
        private System.Windows.Forms.Button btnExpand;
        private System.Windows.Forms.Button btnScrollDown;
        private System.Windows.Forms.Button btnScrollUp;
        private System.Windows.Forms.Button btnItemFocus;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.ToolStripMenuItem 재생목록창꾸미기ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 배경색상설정ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 이미지불러오기ToolStripMenuItem;
        private System.Windows.Forms.ToolStripComboBox toolStripComboBox3;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator31;
        private System.Windows.Forms.ToolStripMenuItem 지우기ToolStripMenuItem;
        private System.Windows.Forms.ColorDialog colorDialog1;
        private System.Windows.Forms.ToolStripComboBox toolStripComboBox4;
        private System.Windows.Forms.ToolStripMenuItem 모눈선표시ToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator32;
        private System.Windows.Forms.ToolStripMenuItem 삽입색상설정ToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator33;
        private System.Windows.Forms.ColorDialog colorDialog2;
        private System.Windows.Forms.ToolStripMenuItem 타일보기ToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator34;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator35;
        private System.Windows.Forms.ToolStripMenuItem 기본프로그램으로설정ToolStripMenuItem;
        private System.Windows.Forms.Timer timer_파일추가;
        private System.Windows.Forms.ToolStripMenuItem 예제사진ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 러브라이브1ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 일러스트1ToolStripMenuItem;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.Button btnSearchPlayList;
        private System.Windows.Forms.ToolStripMenuItem 글자색상설정ToolStripMenuItem;
        private System.Windows.Forms.ColorDialog cdLabelText;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ToolStripMenuItem 플러그인PToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 폴더열기ToolStripMenuItem1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator36;
        private System.Windows.Forms.ToolStripMenuItem 선택한항목강조표시ToolStripMenuItem;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private System.Windows.Forms.Button btn프로그램다시시작;
        private System.Windows.Forms.ComboBox cb반복;
        private System.Windows.Forms.ToolStripMenuItem 플러그인이존재하지않습니다ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 다음ToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator37;
        internal HS_CSharpUtility.Control.HSListview listView1;
        private System.Windows.Forms.ToolStripMenuItem 현재상태저장ToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator38;
        private System.Windows.Forms.SaveFileDialog saveFileDialog4;
        private System.Windows.Forms.ToolStripMenuItem 핫키설정ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 화면끄기ToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator39;
        private System.Windows.Forms.ToolStripMenuItem 스케일모드ToolStripMenuItem;
        private System.Windows.Forms.ToolStripComboBox toolStripComboBox5;
        private System.Windows.Forms.ToolStripMenuItem 자동스케일ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 밀리초표시ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 프리로드모드ToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator41;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator42;
        private System.Windows.Forms.ToolStripMenuItem 가사LToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 싱크가사찾기ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 바탕화면싱크가사창띄우기ToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator40;
        private System.Windows.Forms.ToolStripMenuItem 싱크가사로컬캐시ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 싱크가사캐시정리ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 태그없는항목자동삽입ToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator23;
        private System.Windows.Forms.ToolStripMenuItem 싱크가사캐시관리ToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator10;
        private System.Windows.Forms.ToolStripMenuItem 싱크가사우선순위ToolStripMenuItem;
    }
}

