﻿using System;
using System.Collections.Generic;
using System.Text;
//using HS_Audio.CsharpSqlite3.SQLiteClient;
using System.IO;

namespace HS_Audio.Lyrics
{
    /// <summary>
    /// 아직 구현이 안된 클래스 입니다.
    /// </summary>
    [Obsolete]
    class LyricsCacheDB : IDisposable
    {
        
        public enum LyricsDBFormat{Default,AlsongAndroid}
        public readonly string DefaultDBPath = System.Windows.Forms.Application.StartupPath+"LyricsCacheDB.db";

        public string LyricsDBPath
        {
            get;
            private set;
        }
        public LyricsDBFormat GetLyricsDBFormat{get;private set;}

        //SqliteConnection sc;
        /// <summary>
        /// 싱크가사 로컬캐시를 초기화 합니다.
        /// </summary>
        /// <param name="IsHash">True면 GetLyrics이나 SetLyrics 할때 Hash값을 넣어야되고
        /// <para>False면 음악 태그를 넣어도 됩니다.</para></param>
        /// <param name="DBFormat"></param>>
        public LyricsCacheDB(bool IsHash, LyricsDBFormat DBFormat)
        {
            try{if (!File.Exists(DefaultDBPath)) { File.Create(DefaultDBPath); }}catch {}
            LyricsDBPath = DefaultDBPath;
            GetLyricsDBFormat = DBFormat;
            //sc = new SqliteConnection(DefaultDBPath);

        }
        public LyricsCacheDB(string Path, bool IsHash, LyricsDBFormat DBFormat)
        {
            if (!Directory.Exists(HS_CSharpUtility.Utility.StringUtility.GetDirectoryPath(Path)))
            {
                Directory.CreateDirectory(HS_CSharpUtility.Utility.StringUtility.GetDirectoryPath(Path));
                if (!File.Exists(Path)) { File.Create(Path); }
            }
            LyricsDBPath = Path;
            GetLyricsDBFormat = DBFormat;
            //sc = new SqliteConnection(Path);
        }
        void Common(){}


        Dictionary<string, string> Lrc = new Dictionary<string, string>();
        /*
        public bool SetLyrics(string Key, Lyrics Value)
        {
             //Value.
        }

        public Lyrics GetLyrics(string Key)
        {
        }*/

        public void Dispose()
        {
            throw new NotImplementedException();
        }
    }

    public class LyricsCache : IDisposable
    {
        public static readonly string LyricsCachePath=System.Windows.Forms.Application.StartupPath + "\\LyricsCache.db";

        StreamWriter sw;

        string _Path = LyricsCachePath;
        /// <summary>
        /// 가사가 저장된 경로를 바꿉니다. (주의: 경로를 바꾸게되면 지금 버퍼에있는 가사는 초기화 됩니다.
        /// </summary>
        public string Path
        {
            get {return _Path; }
            set 
            {
                _Path=value;
                if (File.Exists(value)) lrcCache = HS_CSharpUtility.Utility.EtcUtility.LoadSetting(System.IO.File.ReadAllLines(value));
                else { try { File.Create(value); } catch { System.Windows.Forms.MessageBox.Show(value+"\n에 대한 경로를 찾을 수 없습니다."); } }
            }
        }
        public LyricsCache() { Path = _Path; /*sw = new StreamWriter(Path); */}
        public LyricsCache(string CachePath) { this.Path = CachePath; /*sw = new StreamWriter(Path, false);*/ }


        internal Dictionary<string, string> lrcCache = new Dictionary<string, string>();

        /// <summary>
        /// 가사를 파일에 플러시 합니다.
        /// </summary>
        public void Flush()
        {
            string[] a=HS_CSharpUtility.Utility.EtcUtility.SaveSetting(lrcCache);
            File.WriteAllLines(Path, a);
            //foreach (string a in HS_CSharpUtility.Utility.EtcUtility.SaveSetting(lrcCache)) {sw.WriteLine(a);}
        }

        /// <summary>
        /// 가사를 캐싱할때 자동으로 플러시할지 여부 입니다. (기본값: True)
        /// </summary>
        public bool AutoFlush = true;
        /// <summary>
        /// 가사를 가져올때 MD5Hash값을 자동으로 등록하는지 여부를 나타냅니다. (기본값: True)
        /// </summary>
        public bool AutoFocus = true;
        public string MD5Hash_Music = "00000000000000000000000000000000";

        public Exception SetLyrics(string Lyrics, string MD5Hash)
        {
            try
            {
                //System.Windows.Forms.MessageBox.Show(Lyrics, MD5Hash);
                if (MD5Hash != null && MD5Hash != "")
                {
                    MD5Hash = MD5Hash_Music;
                    try { if(Lyrics!=null||Lyrics!="")lrcCache.Add(MD5Hash, Lyrics.Replace("\r\n", "&lt;br&gt;"));}
                    catch (ArgumentException) { lrcCache[MD5Hash] = Lyrics.Replace("\r\n", "&lt;br&gt;"); }
                }
                else
                {
                    try { lrcCache.Add(MD5Hash_Music, Lyrics.Replace("\r\n", "&lt;br&gt;")); }
                    catch (ArgumentException) { lrcCache[MD5Hash_Music] = Lyrics.Replace("\r\n", "&lt;br&gt;"); }
                }
                if (AutoFlush) Flush(); return null;
            }
            catch(Exception ex) { return ex; }
        }
        public Exception SetLyrics(LyricsEz lc)
        {
            try
            {
                if (lc.MD5Hash != null && lc.MD5Hash != "")
                {
                    MD5Hash_Music = lc.MD5Hash;
                    //try { try { lrcCache.Add(lc.MD5Hash, lc.LyricsInfo.ToString().Replace("\r\n", "<br></br>")); } catch (NullReferenceException) { lrcCache.Add(lc.MD5Hash, lc.Lyric.Replace("\r\n", "<br></br>")); } }
                    //catch { try { lrcCache[lc.MD5Hash] = lc.LyricsInfo.ToString().Replace("\r\n", "<br></br>"); } catch (NullReferenceException) {lrcCache[lc.MD5Hash] = lc.Lyric.Replace("\r\n", "<br></br>");  }}
                    try { if (lc != null || lc.Lyric !=null||lc.Lyric != "") lrcCache.Add(lc.MD5Hash, lc.LyricsInfo.ToString().Replace("\r\n", "&lt;br&gt;"));}
                    catch{lrcCache[lc.MD5Hash] = lc.LyricsInfo.ToString().Replace("\r\n", "&lt;br&gt;");}
                }
                else
                {
                    //try { try { lrcCache.Add(MD5Hash_Music, lc.LyricsInfo.ToString().Replace("\r\n", "<br></br>")); } catch (NullReferenceException) { lrcCache.Add(MD5Hash_Music, lc.Lyric.Replace("\r\n", "<br></br>")); } }
                    //catch { try { lrcCache[MD5Hash_Music] = lc.LyricsInfo.ToString().Replace("\r\n", "<br></br>"); } catch (NullReferenceException) { lrcCache[MD5Hash_Music] = lc.Lyric.Replace("\r\n", "<br></br>"); } }
                    try { lrcCache.Add(MD5Hash_Music, lc.LyricsInfo.ToString().Replace("\r\n", "&lt;br&gt;")); }
                    catch (ArgumentException) { lrcCache[MD5Hash_Music] = lc.LyricsInfo.ToString().Replace("\r\n", "&lt;br&gt;"); }
                }
                if (AutoFlush) Flush(); return null;
            }
            catch (Exception ex) { return ex; }
        }
        public Exception SetLyrics(LyricsEz lc, string Title)
        {
            try
            {
                if (lc.MD5Hash != null && lc.MD5Hash != "")
                {
                    MD5Hash_Music = lc.MD5Hash;
                    //try { try { lrcCache.Add(lc.MD5Hash, lc.LyricsInfo.ToString().Replace("\r\n", "<br></br>")); } catch (NullReferenceException) { lrcCache.Add(lc.MD5Hash, lc.Lyric.Replace("\r\n", "<br></br>")); } }
                    //catch { try { lrcCache[lc.MD5Hash] = lc.LyricsInfo.ToString().Replace("\r\n", "<br></br>"); } catch (NullReferenceException) {lrcCache[lc.MD5Hash] = lc.Lyric.Replace("\r\n", "<br></br>");  }}
                    try { if (lc != null || lc.Lyric !=null||lc.Lyric != "") lrcCache.Add(lc.MD5Hash, lc.LyricsInfo.ToString().Replace("\r\n", "&lt;br&gt;"));}
                    catch{lrcCache[lc.MD5Hash] = lc.LyricsInfo.ToString().Replace("\r\n", "&lt;br&gt;");}
                }
                else
                {
                    //try { try { lrcCache.Add(MD5Hash_Music, lc.LyricsInfo.ToString().Replace("\r\n", "<br></br>")); } catch (NullReferenceException) { lrcCache.Add(MD5Hash_Music, lc.Lyric.Replace("\r\n", "<br></br>")); } }
                    //catch { try { lrcCache[MD5Hash_Music] = lc.LyricsInfo.ToString().Replace("\r\n", "<br></br>"); } catch (NullReferenceException) { lrcCache[MD5Hash_Music] = lc.Lyric.Replace("\r\n", "<br></br>"); } }
                    try { lrcCache.Add(MD5Hash_Music, lc.LyricsInfo.ToString().Replace("\r\n", "&lt;br&gt;")); }
                    catch (ArgumentException) { lrcCache[MD5Hash_Music] = lc.LyricsInfo.ToString().Replace("\r\n", "&lt;br&gt;"); }
                }
                if (AutoFlush) Flush(); return null;
            }
            catch (Exception ex) { return ex; }
        }
        public string GetLyrics(string MD5Hash)
        {
            try { if (AutoFocus)this.MD5Hash_Music = MD5Hash; return lrcCache[MD5Hash].Replace("<br></br>", "\r\n").Replace("&lt;br&gt;", "\r\n"); }
            catch { return null; }
        }


        public void CacheCleaner()
        {
            foreach (string s in lrcCache.Keys)
            {
                if (lrcCache[s].Length <2) { lrcCache.Remove(s); }
            }
        }

        /// <summary>
        /// frmLyricsSearch의 GetLyricsComplete 이벤트를 등록합니다
        /// </summary>
        /// <param name="lc">싱크가사 클래스 입니다. (만약 LyricsClass안에 MD5Hash 값이 없다면 현재 설정된 MD5Hash값이 사용됩니다.)</param>
        public void Link_GetLyricsCompleteEvent(LyricsEz lc)
        {
            if (lc.MD5Hash != null || lc.MD5Hash!="")
            {
                //Lin
                MD5Hash_Music = lc.MD5Hash;
                try { try { lrcCache.Add(lc.MD5Hash, lc.LyricsInfo.ToString().Replace("\r\n", "&lt;br&gt;")); } catch (NullReferenceException) { lrcCache.Add(lc.MD5Hash, lc.Lyric.Replace("\r\n", "&lt;br&gt;")); } }
                catch { try { lrcCache[lc.MD5Hash] = lc.LyricsInfo.ToString().Replace("\r\n", "&lt;br&gt;"); } catch (NullReferenceException) { lrcCache[lc.MD5Hash] = lc.Lyric.Replace("\r\n", "&lt;br&gt;"); } }
            }
            else
            {
                try { lrcCache.Add(MD5Hash_Music, lc.Lyric); }
                catch { lrcCache[MD5Hash_Music] = lc.Lyric; }
            }
        }

        /*
        public void Dispose() { sw.Close(); }
        public void Dispose(bool StartGC)
        {
            sw.Close();
            if (StartGC) GC.Collect();
        }*/

        /*
        private string SetParseLine(string LineWordwrap)
        {

        }*/

        #region IDisposable 멤버

        public void Dispose()
        {
            
        }

        #endregion
    }
}
