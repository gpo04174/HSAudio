﻿using System;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace FMOD_Audio.Lyrics
{
    class GetLyrics
    {
        Thread[] th = new Thread[2];
        ThreadStart[] ts = new ThreadStart[2];
        JLyrics.Lyrics l = new JLyrics.Lyrics();
        public string Artist, Title;
        string MusicPath;
        bool ShowTime;
        public int Timeout = 7000;

        public delegate void GetLyricsCompleteEventHandler(LyricsClass Lyrics);
        public event GetLyricsCompleteEventHandler GetLyricsComplete;
        public delegate void GetLyricsProgressEventHandler(int Time);
        public event GetLyricsProgressEventHandler GetLyricsProgress = new GetLyricsProgressEventHandler(GetLyrics_GetLyricsProgress);
        static void GetLyrics_GetLyricsProgress(int Timeout) { }

        public GetLyrics()
        { ts[0] = new ThreadStart(GetLyricss); th[0] = new Thread(ts[0]); 
          ts[1] = new ThreadStart(GetLyricss_1); th[1] = new Thread(ts[1]);}
        public GetLyrics(string MusicPath)
        { ts[0] = new ThreadStart(GetLyricss); th[0] = new Thread(ts[0]);
          ts[1] = new ThreadStart(GetLyricss_1); th[1] = new Thread(ts[1]); 
            this.MusicPath = MusicPath;}
        public GetLyrics(string Artist, string Title)
        { ts[0] = new ThreadStart(GetLyricss); th[0] = new Thread(ts[0]);
          ts[1] = new ThreadStart(GetLyricss_1); th[1] = new Thread(ts[1]); 
          this.Artist = Artist; this.Title = Title; }

        int Time;
        public void GetLyricByName()
        {
            //th[1].Start();
            Action action1 = () => { try { if(!l.GetLyricsFromName(Artist, Title, ShowTime)){throw new LyricsException(); } }
            catch(ArgumentOutOfRangeException){ throw new LyricsException(); } };
            Task t1 = Task.Factory.StartNew(action1);
            GetLyricsProgress(0);
            while (true) { if (l.MakerLists == null) { Thread.Sleep(10); Time = Time + 10; if (Timeout < Time) { break; } } else { break; } GetLyricsProgress(Time); }
            Task<LyricsClass> retTask = t1.ContinueWith<LyricsClass>((_t1) => { return new LyricsClass(l.MakerLists, l.LyricLists); });
            try { GetLyricsComplete(retTask.Result); }catch { }finally {Time=0; }
            //return new LyricsClass(l.MakerLists, l.LyricLists);
        }
        public void GetLyricByNameSync()
        {
            //th[1].Start();
            Action action1 = () => { try { if(!l.GetLyricsFromName(Artist, Title, ShowTime)){throw new LyricsException(); } } 
            catch (ArgumentOutOfRangeException) { throw new LyricsException(); } };
            Task t1 = Task.Factory.StartNew(action1);
            while (true) { if (l.MakerLists == null) { Thread.Sleep(10); Time = Time + 10; if (Timeout < Time) { break; } } else { break; } }
            Task<LyricsClass> retTask = t1.ContinueWith<LyricsClass>((_t1) => { return new LyricsClass(l.MakerLists, l.LyricLists); });
            try { GetLyricsComplete(retTask.Result); }catch { } finally { Time = 0; }
            //return new LyricsClass(l.MakerLists, l.LyricLists);
        }
        public void GetLyricByNameNew(string Artist, string Title, bool ShowTime)
        {
            //th[1].Start();
            l = new JLyrics.Lyrics();
            Action action1 = () => { try { if(!l.GetLyricsFromName(Artist, Title, ShowTime)){ } }
            catch (ArgumentOutOfRangeException) { }};
            Task t1 = Task.Factory.StartNew(action1);
            GetLyricsProgress(0);
            while (true) { if (l.MakerLists == null) { Thread.Sleep(10); Time = Time + 10; if (Timeout < Time) { break; } } else { break; } GetLyricsProgress(Time); }
            Task<LyricsClass> retTask = t1.ContinueWith<LyricsClass>((_t1) => { return new LyricsClass(l.MakerLists, l.LyricLists); });
            try { GetLyricsComplete(retTask.Result); }catch { }finally {Time=0; }
            //return new LyricsClass(l.MakerLists, l.LyricLists);
        }
        public static LyricsClass GetLyricByName(string Artist, string Title, bool ShowTime, int Timeout)
        {
            //th[1].Start();
            JLyrics.Lyrics l = new JLyrics.Lyrics();
            Action action1 = () =>
            {
                try { if (!l.GetLyricsFromName(Artist, Title, ShowTime)) { throw new LyricsException(); } }
            catch(ArgumentOutOfRangeException){ throw new LyricsException(); } };
            Task t1 = Task.Factory.StartNew(action1);
            while (true) { if (l.MakerLists == null) { Application.DoEvents(); Thread.Sleep(10); Time_Static = Time_Static + 10; if (Timeout < Time_Static) { break; } } else { break; } }
            Task<LyricsClass> retTask = t1.ContinueWith<LyricsClass>((_t1) => { return new LyricsClass(l.MakerLists, l.LyricLists); });
            Time_Static = 0;
            return new LyricsClass(l.MakerLists, l.LyricLists);
        }
        public static LyricsClass GetLyricByNameSync(string Artist, string Title, bool ShowTime)
        {
            //th[1].Start();
            JLyrics.Lyrics l = new JLyrics.Lyrics();
            try { if (!l.GetLyricsFromName(Artist, Title, ShowTime)) { throw new LyricsException(); } }
            catch (ArgumentOutOfRangeException) { throw new LyricsException(); }
            return new LyricsClass(l.MakerLists, l.LyricLists);
        }

        public void GetLyricByMusicSync()
        {
            //th[1].Start();
            try { if (!l.GetLyricsFromFile(MusicPath, ShowTime)) { throw new LyricsException(); } }
            catch (ArgumentOutOfRangeException) { throw new LyricsException(); }
            LyricsClass cl = new LyricsClass(l.MakerLists, l.LyricLists);
            try { GetLyricsComplete(cl); }catch { }
            //return cl;
        }
        public void GetLyricByMusicNewSync(string MusicPath, bool ShowTime)
        {
            //th[1].Start();
            try { if (!l.GetLyricsFromFile(MusicPath, ShowTime)) { throw new LyricsException(); } }
            catch (ArgumentOutOfRangeException) { throw new LyricsException(); }
            LyricsClass cl = new LyricsClass(l.MakerLists, l.LyricLists);
            try{GetLyricsComplete(cl); }catch{}
            //return cl;
        }
        public void GetLyricByMusic()
        {
            //th[0].Start();
            l = new JLyrics.Lyrics();
            Action action1 = () => { try { if(!l.GetLyricsFromFile(MusicPath, ShowTime)){ } }
            catch(ArgumentOutOfRangeException) {} };
            Task t1 = Task.Factory.StartNew(action1);
            GetLyricsProgress(0);
            while (true) { if (l.MakerLists == null) { Thread.Sleep(10); Time = Time + 10; if (Timeout < Time) { break; } } else { break; } GetLyricsProgress(Time); }
            Task<LyricsClass> retTask = t1.ContinueWith<LyricsClass>((_t1) => { return new LyricsClass(l.MakerLists, l.LyricLists); });
            try { GetLyricsComplete(retTask.Result); }catch { }finally {Time=0; }
            //return new LyricsClass(l.MakerLists, l.LyricLists);
        }
        public void GetLyricByMusicNew(string MusicPath, bool ShowTime)
        {
            //th[0].Start();
            l = new JLyrics.Lyrics();
            Action action1 = () => { try { if(!l.GetLyricsFromFile(MusicPath, ShowTime)){ } } 
            catch (ArgumentOutOfRangeException) {  } };
            Task t1 = Task.Factory.StartNew(action1);
            GetLyricsProgress(0);
            while (true) { if (l.MakerLists == null) { Thread.Sleep(10); Time = Time + 10; if (Timeout < Time) { break; } } else { break; } GetLyricsProgress(Time); }
            Task<LyricsClass> retTask = t1.ContinueWith<LyricsClass>((_t1) => { return new LyricsClass(l.MakerLists, l.LyricLists); });
            try { GetLyricsComplete(retTask.Result); }catch { }finally {Time=0; }
            //return new LyricsClass(l.MakerLists, l.LyricLists);
        }
        static int Time_Static;
        public static LyricsClass GetLyricByMusic(string MusicPath,bool ShowTime, int Timeout)
        {
            //th[1].Start();
            JLyrics.Lyrics l = new JLyrics.Lyrics();
            Action action1 = () => { try { if(!l.GetLyricsFromFile(MusicPath, ShowTime)){throw new LyricsException(); } } 
            catch (ArgumentOutOfRangeException) { throw new LyricsException(); } };
            Task t1 = Task.Factory.StartNew(action1);
            while (true) { if (l.MakerLists == null) { Thread.Sleep(10); Time_Static = Time_Static + 10; if (Timeout < Time_Static) { break; } } else { break; } }
            Task<LyricsClass> retTask = t1.ContinueWith<LyricsClass>((_t1) => { return new LyricsClass(l.MakerLists, l.LyricLists); });
            Time_Static = 0;
            return new LyricsClass(l.MakerLists, l.LyricLists);
        }
        public static LyricsClass GetLyricByMusicSync(string MusicPath, bool ShowTime)
        {
            //th[1].Start();
            JLyrics.Lyrics l = new JLyrics.Lyrics();
            try { if (!l.GetLyricsFromFile(MusicPath, ShowTime)) {throw new LyricsException(); } }
            catch (ArgumentOutOfRangeException) { throw new LyricsException(); }
            LyricsClass cl = new LyricsClass(l.MakerLists, l.LyricLists);
            return cl;
        }

        void GetLyricss()
        {
            try{if (!l.GetLyricsFromFile(MusicPath, ShowTime)) {throw new LyricsException(); }}
            catch{}
            try { GetLyricsComplete(new LyricsClass(l.MakerLists, l.LyricLists)); }catch{}
            th[0].Abort();
        }
        void GetLyricss_1()
        {
            if (!l.GetLyricsFromName(Artist,Title, ShowTime)) { try { GetLyricsComplete(null); } catch { } }
            else { try { new LyricsClass(l.MakerLists, l.LyricLists); } catch { } }
            th[1].Abort();
        }
    }
    class LyricsClass
    {
        public LyricsClass() { }
        public LyricsClass(string[] Maker, string[] Lyric) { this.Maker = Maker; this.Lyric = Lyric; }
        public string[] Maker;
        public string[] Lyric;
    }
    /// <summary>
    /// 싱크가사 예외 클래스 입니다.
    /// </summary>
    public class LyricsException : Exception
    {
        /// <summary>
        /// 싱크가사 예외 클래스를 초기화 합니다.
        /// </summary>
        public LyricsException() { _Message = "싱크 가사를 찾을수 없습니다!"; }
        /// <summary>
        /// 싱크가사 예외 클래스를 초기화 합니다.
        /// </summary>
        /// <param name="Message">표시할 메세지 입니다.</param>
        public LyricsException(string Message) { _Message = Message; }

        string _Message;
        /// <summary>
        /// 오류를 설명하는 메세지를 가져옵니다.
        /// </summary>
        public override string Message
        {
            get { return _Message; }
        }
    }
}

