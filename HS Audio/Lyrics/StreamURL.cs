﻿namespace HS_Audio.Lyrics
{
    using System;
    using System.IO;
    using System.Net;
    using System.Threading;

    public class StreamURL
    {
        private string a;
        private EventHandler b;
        public bool fRunning;
        public Stream fStream;
        public bool fTerminal;

        public event EventHandler StreamLoadComplete
        {
            add
            {
                EventHandler handler2;
                EventHandler b = this.b;
                do
                {
                    handler2 = b;
                    EventHandler handler3 = (EventHandler) Delegate.Combine(handler2, value);
                    b = Interlocked.CompareExchange<EventHandler>(ref this.b, handler3, handler2);
                }
                while (b != handler2);
            }
            remove
            {
                EventHandler handler2;
                EventHandler b = this.b;
                do
                {
                    handler2 = b;
                    EventHandler handler3 = (EventHandler) Delegate.Remove(handler2, value);
                    b = Interlocked.CompareExchange<EventHandler>(ref this.b, handler3, handler2);
                }
                while (b != handler2);
            }
        }

        public StreamURL()
        {
        }

        public StreamURL(string aURL)
        {
            this.a = aURL;
        }

        public void ThLoadURLStream()
        {
            MemoryStream stream = new MemoryStream();
            Console.WriteLine("url스트림읽는중");
            this.fRunning = true;
            try
            {
                if ((this.a == "") || (this.a.IndexOf("http") < 0))
                {
                    this.fTerminal = true;
                    this.b(this, new EventArgs());
                }
                else
                {
                    HttpWebRequest request = (HttpWebRequest) WebRequest.Create(this.a);
                    HttpWebResponse response = (HttpWebResponse) request.GetResponse();
                    Stream responseStream = response.GetResponseStream();
                    stream = new MemoryStream();
                    byte[] buffer = new byte[0x7ff];
                    int count = responseStream.Read(buffer, 0, buffer.Length);
                    for (int i = 0; i < 0x7ff; i++)
                    {
                        stream.Write(buffer, 0, count);
                        count = responseStream.Read(buffer, 0, buffer.Length);
                    }
                    this.fStream = stream;
                    stream.Flush();
                    responseStream.Close();
                    response.Close();
                    this.b(this, new EventArgs());
                }
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception.Message + " - CStreamURL");
                this.fTerminal = true;
                this.fRunning = false;
                this.b(this, new EventArgs());
            }
        }
    }
}

