﻿using System;
using System.ComponentModel;
using System.Windows.Forms;
using System.IO;
using System.Threading;
using HS_CSharpUtility;
using HS_CSharpUtility.Extension;

namespace HS_Audio.Lyrics
{
    public partial class frmLyricsSearch : Form
    {
        public delegate void GetLyricsCompleteEventHandler(LyricAlsong[] Lyrics);
        public delegate void GetLyricsFailedEventHandler(LyricException ex);
        //public delegate void GetLyricsCompleteEventHandler(LyricsClass Lyrics);
        public event GetLyricsCompleteEventHandler GetLyricsComplete;
        /*
        {
            add{this._GetLyricsComplete += value; }
            remove {  this._GetLyricsComplete -= value;}
        }*/

        public event GetLyricsFailedEventHandler GetLyricsFailed;
        /*
        {
            add { this._GetLyricsFailed += value; }
            remove {this._GetLyricsFailed -= value;}
        }*/

        //protected virtual void OnGetLyricsComplete(LyricsClass lyc)
        [Obsolete]
        protected virtual void OnGetLyricsComplete(LyricAlsong[] lyc)
        { if (this._GetLyricsComplete != null)this._GetLyricsComplete(lyc); }
        [Obsolete]
        protected virtual void OnGetLyricsFailed(LyricException ex, string Message)
        { if (this._GetLyricsFailed != null)this._GetLyricsFailed(new LyricException(Message,ex)); }
        private object myEventLock = new object();

        private GetLyricsCompleteEventHandler _GetLyricsComplete;
        private GetLyricsFailedEventHandler _GetLyricsFailed;

        public bool ShowTime = true;
        Thread[] th = new Thread[2];
        ParameterizedThreadStart[] ts= new ParameterizedThreadStart[2];
        //public int Timeout = 7000;

        public frmLyricsSearch()
        {
            //크로스 쓰레드 예외 끄기
            CheckForIllegalCrossThreadCalls = false;
            InitializeComponent();
            radioButton1.Enabled = false; radioButton2.Checked = true;
            //this.GetLyricsComplete += new GetLyricsCompleteEventHandler(gl_GetLyricsComplete);
            //this._GetLyricsFailed += new GetLyricsFailedEventHandler(gl_GetLyricsFailed);
        }
        public frmLyricsSearch(string MusicPath, bool ShowTime)
        {
            InitializeComponent();
            radioButton1.Enabled = true; radioButton1.Checked = true;
            this.MusicPath=MusicPath;this.ShowTime = ShowTime;
            //this.GetLyricsComplete += new GetLyricsCompleteEventHandler(gl_GetLyricsComplete);
            toolStripStatusLabel1.Text = "싱크 가사 찾는 중...";
            textBox1.Text = Path.GetFileNameWithoutExtension(MusicPath);
#if DEBUG
            panel1.Enabled = true;
#else
            panel1.Enabled = false;
#endif
            FindLyrics(MusicPath, ShowTime);
        }
        public frmLyricsSearch(string MusicPath)
        {
            InitializeComponent();
            radioButton1.Enabled = true; radioButton1.Checked = true;
            this.MusicPath = MusicPath;
            //this.GetLyricsComplete += new GetLyricsCompleteEventHandler(gl_GetLyricsComplete);
            toolStripStatusLabel1.Text = "싱크 가사 찾는 중...";
            textBox1.Text = Path.GetFileNameWithoutExtension(MusicPath);
#if DEBUG
            panel1.Enabled = true;
#else
            panel1.Enabled = false;
#endif
            FindLyrics(MusicPath, true);
        }
        public frmLyricsSearch(LyricsInfo LyricsAssemble)
        {
            InitializeComponent();
            radioButton1.Enabled = false; radioButton2.Checked = true;
            this.LyricsAssembly= LyricsAssemble;
            //this.GetLyricsComplete += new GetLyricsCompleteEventHandler(gl_GetLyricsComplete);
            toolStripStatusLabel1.Text = "싱크 가사 찾는 중...";
#if RELEASE
            panel1.Enabled = false;
#endif
            FindLyrics(MusicPath, ShowTime);
        }
        public frmLyricsSearch(string Song, string Artist)
        {
            InitializeComponent();
            radioButton1.Enabled = false; radioButton2.Checked = true;
            this.LyricsAssembly = new LyricsInfo(Song, Artist);
            //this.GetLyricsComplete += new GetLyricsCompleteEventHandler(gl_GetLyricsComplete);
            toolStripStatusLabel1.Text = "싱크 가사 찾는 중...";
#if DEBUG
            panel1.Enabled = true;
#else
            panel1.Enabled = false;
#endif
            FindLyrics(MusicPath, ShowTime);
        }
        public frmLyricsSearch(string Song, string Artist, bool ShowTime)
        {
            InitializeComponent();
            radioButton1.Enabled = false; radioButton2.Checked = true;
            this.LyricsAssembly = new LyricsInfo(Song, Artist); this.ShowTime = ShowTime;
            //this.GetLyricsComplete += new GetLyricsCompleteEventHandler(gl_GetLyricsComplete);
            toolStripStatusLabel1.Text = "싱크 가사 찾는 중...";
#if DEBUG
            panel1.Enabled = true;
#else
            panel1.Enabled = false;
#endif
            FindLyrics(MusicPath, ShowTime);
        }
        public frmLyricsSearch(LyricsInfo LyricsAssemble, bool ShowTime)
        {
            InitializeComponent();
            radioButton1.Enabled = false; radioButton2.Checked = true;
            this.LyricsAssembly = LyricsAssemble; this.ShowTime = ShowTime;
            //ts[0] = new ParameterizedThreadStart(GetLyrics);
            //th[0] = new Thread(ts[0]); th[0].IsBackground = true;
            //this.GetLyricsComplete += new GetLyricsCompleteEventHandler(gl_GetLyricsComplete);
            toolStripStatusLabel1.Text = "싱크 가사 찾는 중...";
#if DEBUG
            panel1.Enabled = true;
#else
            panel1.Enabled = false;
#endif
            FindLyrics(MusicPath, ShowTime);
        }

        frmMain Form1Class;
        internal void SetForm1Class(frmMain Form1)
        { Form1Class = Form1; if (Form1 != null) button2.Visible = true; else button2.Enabled = false; }

        string _MusicPath;
        public string MusicPath
        {
            get { return _MusicPath; }
            set { _MusicPath = value; this.Text = "싱크 가사 찾기 - [" + System.IO.Path.GetFileName(value) + "]"; 
            radioButton1.Enabled=true;FindLyrics(value);}
        }
        /*
        public frmLyricsMain(string MusicPath)
        {
            InitializeComponent();
            this.MusicPath = MusicPath;
            ts[0] = new ParameterizedThreadStart(GetLyrics);
            th[0] = new Thread(ts[0]); th[0].IsBackground = true;
            this.GetLyricsComplete += new GetLyricsCompleteEventHandler(gl_GetLyricsComplete);
            th[0].Start(true);
            //lc = GetLyrics.GetLyricByMusic(MusicPath, true);
            if (lc != null) { try { this.comboBox1.Items.AddRange(lc.Maker); this.textBox3.Text = lc.Lyric[0]; } catch { } }
            radioButton1.Enabled=radioButton1.Checked = true;
            radioButton2.Enabled = true;
            textBox2.Text = Path.GetFileNameWithoutExtension(MusicPath);
        }*/

        void gl_GetLyricsProgress(int Timeout)
        {

        }

        private void Lyrics_Load(object sender, EventArgs e)
        {
            
        }
        //LyricsClass lc;
        LyricAlsong[] lc;
        //GetLyrics gl = new GetLyrics();
        private void button1_Click(object sender, EventArgs e)
        {
            if(radioButton1.Checked){}
            else if(radioButton2.Checked){
            if(textBox1.Text==""&&textBox2.Text=="")
            {MessageBox.Show("곡명 또는 노래제목을 입력해 주십시오!!", "경고", MessageBoxButtons.OK, MessageBoxIcon.Warning);return;}}
            else {if (textBox3.Text == "" || textBox3.Text.Length != 32) {
                  MessageBox.Show("올바른 MD5해쉬값을 입력해 주십시오!", "경고", MessageBoxButtons.OK, MessageBoxIcon.Warning); return;}}
            Flush();
            toolStripStatusLabel1.Text = "싱크 가사 찾는 중...";
#if DEBUG
            panel1.Enabled = true;
#else
            panel1.Enabled = false;
#endif
            if (radioButton2.Checked)
            {
                try { FindLyrics(textBox1.Text,textBox2.Text, ShowTime); }// toolStripStatusLabel1.Text = string.Format("싱크 가사 {0}개를 찾았습니다!", lc.Maker.Length);
                catch (LyricException le) { toolStripStatusLabel1.Text = le.Message; }
            }
            else if (radioButton1.Checked){if (MusicPath != null) {  FindLyrics(MusicPath, ShowTime); }}
            else FindLyrics(textBox3.Text, ShowTime, null);
            button1.Text = "다시 찾기";
            toolTip1.SetToolTip(this.button1, "가사가 나오지 않는 경우 가사를 다시 찾습니다.");
        }
        

        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {
            groupBox1.Enabled = radioButton2.Checked;
            button1.Text = "찾기";
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lc != null/* && lc.Maker[0] != null*/&&info!=null)
                if (checkBox1.Checked) { try {if(info.자동업데이트ToolStripMenuItem.Checked)info.Alsong= Lyric[comboBox1.SelectedIndex]; this.txtLyric.Text = Lyric[comboBox1.SelectedIndex].Lyric; } 
                catch(IndexOutOfRangeException) { comboBox1.Items.RemoveAt(comboBox1.SelectedIndex);comboBox1.SelectedItem=comboBox1.Items[0]; } }
                else {if(info.자동업데이트ToolStripMenuItem.Checked)info.Alsong= Lyric_Trim[comboBox1.SelectedIndex];}
            else this.txtLyric.Text = checkBox1.Checked?Lyric[comboBox1.SelectedIndex].Lyric:Lyric_Trim[comboBox1.SelectedIndex].Lyric;
        }

        private void 다른이름으로가사저장SToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if(MusicPath==null||MusicPath=="")saveFileDialog1.FileName = comboBox1.Text;
            else saveFileDialog1.FileName = Path.GetFileNameWithoutExtension(MusicPath)+" "+comboBox1.Text.Substring(2);
            saveFileDialog1.ShowDialog();
        }

        private void saveFileDialog1_FileOk(object sender, CancelEventArgs e)
        {
            File.WriteAllLines(saveFileDialog1.FileName, txtLyric.Lines);
        }
        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            int i = comboBox1.SelectedIndex==-1?0:comboBox1.SelectedIndex;
            comboBox1.Items.Clear();
            try
            {
                button2.Enabled = checkBox1.Checked;
                if (checkBox1.Checked)
                {
                    foreach (LyricAlsong a in lc) { comboBox1.Items.Add(string.Format("{0}> {1} [{2}]", a.Artist,a.Title, a.GetMaker));}
                    comboBox1.Text = comboBox1.Items[i].ToString();
                    txtLyric.Text = lc[i].Lyric;
                    saveFileDialog1.Filter = "가사 파일(*.lrc)|*.lrc|모든 파일(*.*)|*.*";
                    saveFileDialog1.DefaultExt = ".lrc";
                }
                else
                {
                    if (Lyric_Trim==null) Lyric_Trim = TrimAlsongLyricsThread(Lyric);
                    foreach (LyricAlsong a in Lyric_Trim) { comboBox1.Items.Add(string.Format("{0}> {1} [{2}]", a.Artist,a.Title, a.GetMaker));}
                    comboBox1.Text = comboBox1.Items[i].ToString();
                    txtLyric.Text = Lyric_Trim[i].Lyric;
                    saveFileDialog1.Filter = "텍스트 파일(*.txt)|*.txt|모든 파일(*.*)|*.*";
                    saveFileDialog1.DefaultExt = ".txt";
                }
            }
            catch { }
        }

        /*
        delegate void Panel1EnableCallBack(bool Enable);
        void Panel1SetEnable(bool Enable)
        {
            if (this.panel1.InvokeRequired)
            {
                Panel1EnableCallBack sec = new Panel1EnableCallBack(Panel1SetEnable);
                panel1.Invoke(sec, Enable);
            }
            else { panel1.Enabled = Enable; }
        }*/

        void gl_GetLyricsFailed(Exception ex,string Message)
        {
            if(Message==null)toolStripStatusLabel1.Text = "싱크 가사를 찾는중 오류가 발생했습니다!";
            else toolStripStatusLabel1.Text = Message;
            try { panel1.Enabled = true; }
            catch { panel1.InvokeIfNeeded(() => panel1.Enabled = true); }
        }

        LyricAlsong[] Lyric, Lyric_Trim;
        void gl_GetLyricsComplete(LyricAlsong[] Lyrics)
        {
            Flush();
            lc = Lyrics;
            try
            {
                try{GetLyricsComplete(Lyrics);}catch{}
                button3.Enabled=false;
                //if (Lyrics.Maker[0] == "1 (by )" && Lyrics.Lyric[0] == "") { Lyrics.Maker[0] = ""; throw new LyricsException(); }
                if (Lyrics==null) { throw new LyricException(); }
                //if (Lyrics.Maker == null && Lyrics.Lyric != null)
                toolStripStatusLabel1.Text = string.Format("싱크 가사 {0}개를 찾았습니다!", Lyrics.LongLength); 
                if(Lyrics.LongLength>0)button3.Enabled=true;
                if (lc != null)
                {
                    try
                    {
                        Lyric = Lyrics;
                        if (checkBox1.Checked)
                        {
                            //#if DEBUG
                            //this.comboBox1.InvokeIfNeeded(() => comboBox1.Items.AddRange(Lyric.Maker));
                            //this.comboBox1.InvokeIfNeeded(() => comboBox1.Text = comboBox1.Items[0].ToString());
                            //this.txtLyric.InvokeIfNeeded(() => txtLyric.Text = Lyric.Lyric[0]);
                            //#else
                            foreach (LyricAlsong a in Lyric) { comboBox1.Items.Add(string.Format("{0}> {1} [{2}]", a.Artist,a.Title, a.GetMaker)); }
                            comboBox1.Text = comboBox1.Items[0].ToString();
                            txtLyric.Text = Lyric[0].Lyric;
                            //#endif
                        }
                        else
                        {
                            Lyric_Trim = TrimAlsongLyricsThread(Lyric);
                            //#if DEBUG
                            //this.comboBox1.InvokeIfNeeded(() => comboBox1.Items.AddRange(Lyric_Trim.Maker));
                            //this.comboBox1.InvokeIfNeeded(() => comboBox1.Text = comboBox1.Items[0].ToString());
                            //this.txtLyric.InvokeIfNeeded(() => txtLyric.Text = Lyric_Trim.Lyric[0]);
                            //#else
                            foreach (LyricAlsong a in Lyric_Trim) { comboBox1.Items.Add(string.Format("{0}> {1} [{2}]", a.Artist,a.Title, a.GetMaker));}
                            comboBox1.Text = comboBox1.Items[0].ToString();
                            txtLyric.Text = Lyric_Trim[0].Lyric;
                            //#endif
                        }

                        //ts[1] = new ParameterizedThreadStart(LyricsTrimThread);
                        //th[1] = new Thread(ts[1]); th[1].IsBackground = true;
                        //th[1].Start(Lyric);
                    }
                    catch { }
                }
            }
            catch { toolStripStatusLabel1.Text = "싱크 가사를 찾을 수 없습니다!"; }
            //Panel1SetEnable(true);
            try { panel1.Enabled = true; }
            catch { panel1.InvokeIfNeeded(() => panel1.Enabled = true); }
            if(Lyrics!=null&&Lyrics.Length!=0&&info!=null){info.Alsong = Lyrics[0];}
        }

        public void FindLyrics(LyricsInfo LyricsAssembly)
        {
            this.LyricsAssembly = LyricsAssembly;
            Common(FindMethod.LyricAssembly);
        }
        public void FindLyrics(LyricsInfo LyricsAssembly, bool ShowTime)
        {
            this.LyricsAssembly = LyricsAssembly;
            Common(FindMethod.LyricAssembly);
        }
        public void FindLyrics(string Song, string Artist)
        {
            this.LyricsAssembly = new LyricsInfo(Song, Artist);
            Common(FindMethod.SongInfo);
        }
        public void FindLyrics(string Song, string Artist, bool ShowTime)
        {
            this.LyricsAssembly = new LyricsInfo(Song, Artist);
            this.ShowTime = ShowTime;
            Common(FindMethod.SongInfo);
        }
        public string MD5Hash;
        public void FindLyrics(string MusicPath)
        {
            this._MusicPath = MusicPath;
            Common(FindMethod.MusicPath);
        }
        public void FindLyrics(string MusicPath, bool ShowTime)
        {
            this._MusicPath = MusicPath;
            this.ShowTime = ShowTime;
            Common(FindMethod.MusicPath);
        }
       
        /// <summary>
        /// 
        /// </summary>
        /// <param name="MD5Hash"></param>
        /// <param name="ShowTime"></param>
        /// <param name="ThisIsDummy_ConstantTheNULL">쓰지 않습니다. 위의 매개변수 형식과 충돌을 막고자 부득이하게 넣은것 입니다. (넣어도 아무일 안일어납니다.)</param>
        public void FindLyrics(string MD5Hash,bool ShowTime, string ThisIsDummy_ConstantTheNULL)
        {
            //this.MusicPath = null;
            this.ShowTime = ShowTime;
            this.MD5Hash = MD5Hash; MD5HashLock = true;
            Common(FindMethod.MD5Hash);
        }
        private enum FindMethod{MusicPath,SongInfo,MD5Hash,LyricAssembly}
        void Common(FindMethod IsMusicPath)
        {
            //ts[0] = new ParameterizedThreadStart(GetLyrics);
            //th[0] = new Thread(ts[0]); //th[0].IsBackground = true;
            Lyric_Trim = null;
            //ThreadPool.QueueUserWorkItem(new WaitCallback(GetLyrics), IsMusicPath);
            IAsyncResult ar= new FindLyricsInvokeHandler(GetLyrics).BeginInvoke(IsMusicPath, AsyncCallbackImplementation,this);
            //th[0].Start(IsMusicPath);
        }
        internal static readonly AsyncCallback AsyncCallbackImplementation;
        private delegate void FindLyricsInvokeHandler(FindMethod IsMusic);
        public void Flush()
        {
            comboBox1.Items.Clear();
            txtLyric.Text = "";
        }
        #region 쓰레드 이벤트

        bool MD5HashLock;
        LyricsInfo LyricsAssembly;
        int Timeout = 1000;

        void GetLyrics(FindMethod IsMusic)
        {
            timer1.Start();
            HS_Audio.Lyrics.GetLyrics lc = new HS_Audio.Lyrics.GetLyrics();
            lc.ExceptionEvent+=lc_ExceptionEvent;
            if ((FindMethod)IsMusic == FindMethod.MusicPath)
            {
                if (!MD5HashLock) MD5Hash = HS_Audio.Lyrics.GetLyrics.GenerateMusicHash(MusicPath);
                lc.GetLyricsFromFile(MusicPath, ShowTime);
                try {gl_GetLyricsComplete(lc.LyricsLists);  }finally { /*th[0].Abort();*/ }//th[0].Suspend(); }=
                try{if(GetLyricsComplete!=null)GetLyricsComplete(lc.LyricsLists); }finally {}
                //try { OnGetLyricsComplete(new LyricsClass(lc.MakerLists, lc.LyricLists));  }finally { /*th[0].Abort();*/ }//th[0].Suspend(); }
                MD5HashLock = false;
            }
            else if((FindMethod)IsMusic == FindMethod.MD5Hash)
            {
                lc.GetLyricsFromMD5Hash(MD5Hash);
                try {gl_GetLyricsComplete(lc.LyricsLists);  }finally { /*th[0].Abort();*/ }//th[0].Suspend(); }=
                //try{GetLyricsComplete(lc.LyricsLists); }finally {}
            }
            else
            {
                lc.GetLyricsFromName(LyricsAssembly.Artist, LyricsAssembly.Song, ShowTime);
                try { gl_GetLyricsComplete(lc.LyricsLists);  }finally { /*th[0].Abort();*/ }//th[0].Suspend(); }
                //try{GetLyricsComplete(lc.LyricsLists); }finally {}
            }
            //th[0].Abort();
        }

        private void lc_ExceptionEvent(Exception ex)
        {try { GetLyricsFailed(new LyricException("일시적인 네트워크 오류가 발생했습니다.",ex)); }finally {}}//인터넷 연결을 확인해 주세요!!
        

        //protected virtual OnGetLyricsComplete(GetLyricsCompleteEventHandler
        LyricsClass TrimLyricsClassThread(LyricsClass a)
        {
            LyricsClass b = a;
            LyricsEz[] lcs = LyricsEz.ConvertLyricsClassToLyricsArray(b);
            for (long i = 0; i < lcs.LongLength;i++ )
            { lcs[i] = TrimTimeLyrics(lcs[i]); }
            try { b = LyricsClass.ConvertLyricsArrayToLyricsClass(lcs); }
            finally { /*th[1].Abort();*/ }
            return b;
        }
        LyricAlsong[] TrimAlsongLyricsThread(LyricAlsong[] a)
        {
            //LyricsEz[] lcs = LyricsEz.ConvertLyricsClassToLyricsArray(a);
            LyricAlsong[] lcs = new LyricAlsong[a.LongLength];
            for (long i = 0; i < a.LongLength; i++)
            { lcs[i] = TrimTimeLyrics(a[i]); }
            //try { b = LyricsClass.ConvertLyricsArrayToLyricsClass(lcs); }
            //finally { /*th[1].Abort();*/ }
            return lcs;
        }
        #endregion
        public LyricsEz TrimTimeLyrics(LyricsEz Lyric)
        {
            LyricsEz lrc = new LyricsEz(Lyric.Maker, Lyric.Lyric);
            string[] li = HS_CSharpUtility.Utility.StringUtility.ConvertStringToArray(Lyric.Lyric, "\r\n");
            System.Text.StringBuilder sb = new System.Text.StringBuilder(li[0].Substring(li[0].IndexOf("]") + 1));
            for (long i = 1; i < li.LongLength; i++)
            {
                try { li[i] = li[i].Substring(li[i].IndexOf("]") + 1); }
                catch { }finally { sb.Append("\r\n" + li[i]); }
            }
            lrc.Lyric=sb.ToString();
            return lrc;
        }
        public LyricAlsong TrimTimeLyrics(LyricAlsong Lyric)
        {
            LyricAlsong al = LyricAlsong.Clone(Lyric);
            string[] li = HS_CSharpUtility.Utility.StringUtility.ConvertStringToArray(Lyric.Lyric, "\r\n");
            System.Text.StringBuilder sb = new System.Text.StringBuilder(li[0].Substring(li[0].IndexOf("]") + 1));
            for (long i = 1; i < li.LongLength; i++)
            {
                try { li[i] = li[i].Substring(li[i].IndexOf("]") + 1); }catch { }
                finally { sb.Append("\r\n" + li[i]); }
            }
            al.Lyric = sb.ToString();
            return al;
        }
        public string TrimTimeforLyricString(string Lyric)
        {
            string[] li = HS_CSharpUtility.Utility.StringUtility.ConvertStringToArray(Lyric, "\r\n");
            System.Text.StringBuilder sb = new System.Text.StringBuilder(li[0].Substring(li[0].IndexOf("]") + 1));
            for (long i = 1; i < li.LongLength; i++)
            {
                try { li[i]=li[i].Substring(li[i].IndexOf("]") + 1); }
                catch { sb.Append("\r\n"+li[i]); }
            }
            return sb.ToString();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (Lyric == null || Lyric.Length<1|| Lyric[0].Lyric == null)
            {
                if (MessageBox.Show("빈 가사 입니다.\n정말 현재 싱크 가사창에 연결 하시겠습니까?", "빈 싱크가사 연결", 
                    MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == System.Windows.Forms.DialogResult.Yes)
                { Form1Class.frmlrcSearch_GetLyricsComplete(new AlsongLyricsInstance());//(new LyricsClass(new string[0], new string[0])); }
            }}
            else { //Form1Class.frmlrcSearch_GetLyricsComplete(new LyricsEz(Lyric.Maker[comboBox1.SelectedIndex], Lyric.Lyric[comboBox1.SelectedIndex]));
                Form1Class.frmlrcSearch_GetLyricsComplete(Lyric[comboBox1.SelectedIndex==-1?0:comboBox1.SelectedIndex]);}
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            button1.Text = "찾기";
        }

        private void button1_TextChanged(object sender, EventArgs e)
        {
            if (radioButton2.Checked)
            {
                if (button1.Text == "찾기")
                { toolTip1.SetToolTip(this.button1, "가수와 제목명으로 가사를 찾습니다."); }
                else if (button1.Text == "다시 찾기")
                { toolTip1.SetToolTip(this.button1, "가수와 제목명으로 가사를 다시 찾습니다."); }
                else
                    toolTip1.SetToolTip(this.button1, "버튼의 리소스값이나 문자열을 수정하지 마십시오!\n불법입니다.");
            }
            else
            {
                if (button1.Text == "찾기")
                { toolTip1.SetToolTip(this.button1, "음악 파일의 고유정보로 가사를 찾습니다."); }
                else if (button1.Text == "다시 찾기")
                { toolTip1.SetToolTip(this.button1, "음악 파일의 고유정보로 가사를 다시 찾습니다."); }
                else
                    toolTip1.SetToolTip(this.button1, "버튼의 리소스값이나 문자열을 수정하지 마십시오!\n불법입니다.");
            }
        }

        private void textBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13) { button1_Click(null, null); }
        }

        private void 개발자모드DToolStripMenuItem_CheckedChanged(object sender, EventArgs e)
        {
            button3.Visible = 개발자모드DToolStripMenuItem.Checked;
        }
        frmAlsongLyricInfo info;// = new frmAlsongLyricInfo(){IsClosing=false};
        private void button3_Click(object sender, EventArgs e)
        {
            try
            {
                if(info==null)info = new frmAlsongLyricInfo(Lyric[comboBox1.SelectedIndex]){IsClosing = false};
                //info.Alsong = Lyric[comboBox1.SelectedIndex];
                info.Show();
                info.BringToFront();
            }catch{}
        }

        private void frmLyricsSearch_FormClosing(object sender, FormClosingEventArgs e){if(info!=null)info.Dispose();}

        private void checkBox2_CheckedChanged(object sender, EventArgs e)
        {textBox3.CharacterCasing = checkBox2.Checked ? CharacterCasing.Lower : CharacterCasing.Upper; }

        private void radioButton3_CheckedChanged(object sender, EventArgs e)
        {
            groupBox1.Enabled = radioButton3.Checked;
            if (radioButton3.Checked) {panel3.Visible=true;this.panel2.Visible=false;}
            else {this.panel3.Visible=false;panel2.Visible=true;}
            button1.Text = "찾기";
        }

        string PreviousText;
        private void textBox3_TextChanged(object sender, EventArgs e)
        {
            //if (!isNumAlpha(textBox3.Text)) textBox3.Text = PreviousText;
            //else if (textBox3.Text != "") PreviousText = textBox3.Text;
        }
        private bool isNumAlpha(string str)
        {
            bool result = false;
            char[] c = str.ToCharArray();
            for (int i = 0; i < c.Length; i++)
            {
                if ('0' <= c[i] && c[i] <= '9') result = true;
                else if ('a' <= c[i] && c[i] <= 'z') result = true;
                else if ('A' <= c[i] && c[i] <= 'Z') result = true;
                else return false;
            }
            return result;
        }

        private void textBox3_KeyPress(object sender, KeyPressEventArgs e)
        {
            //e.Handled = !(char.IsLetter(e.KeyChar) || e.KeyChar == (char)Keys.Back || char.IsDigit(e.KeyChar));
            if (e.KeyChar != '\b')
                e.Handled = !System.Uri.IsHexDigit(e.KeyChar);
        }
    }
}
