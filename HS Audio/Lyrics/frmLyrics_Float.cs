﻿using System;
using System.Windows.Forms;
using System.Threading;
using CSharpUtility;
using System.Drawing;

namespace FMOD_Audio.Lyrics
{
    public partial class frmLyrics_Float : Form
    {
        #region 폼 리사이즈, 이동하기

        private Point m_PrevPoint;

        public delegate void OnMoveEvent(Point pt);
        public OnMoveEvent MoveEventHandler;
        private void MoveEventHandler_Body(Point pt)
        {
            /*
            if (this.InvokeRequired)
            {
                this.Invoke(MoveEventHandler, new object[] { pt });
            }
            else
            {
                this.Location = pt;
            }*/
        }

        /*
        protected override CreateParams CreateParams
        {
            get
            {
                const int WS_CAPTION = 0x00C00000;
                CreateParams baseParams = base.CreateParams;
                //Get rid of caption
                baseParams.Style = baseParams.Style & ~WS_CAPTION;
                return baseParams;
            }
        }*/

        public enum Msg
        {
                    //.... 일부 생략
            WM_NCHITTEST = 0x84,
                   //.... 일부 생략
        }

        /// <summary>
        /// Indicates the position of the cursor hot spot.
        /// </summary>
        public enum NCHITTESTFLAGS : int
        {
            /// <summary>
            /// On the screen background or on a dividing line between windows (same as HTNOWHERE, except that the DefWindowProc function produces a system beep to indicate an error).
            /// </summary>
            HTERROR = -2,

            /// <summary>
            /// In a window currently covered by another window in the same thread (the message will be sent to underlying windows in the same thread until one of them returns a code that is not HTTRANSPARENT).
            /// </summary>
            HTTRANSPARENT = -1,

            /// <summary>
            /// On the screen background or on a dividing line between windows.
            /// </summary>
            HTNOWHERE = 0,

            /// <summary>
            /// In a client area.
            /// </summary>
            HTCLIENT = 1,

            /// <summary>
            /// In a title bar.
            /// </summary>
            HTCAPTION = 2,

            /// <summary>
            /// In a window menu or in a Close button in a child window.
            /// </summary>
            HTSYSMENU = 3,

            /// <summary>
            /// In a size box (same as HTSIZE).
            /// </summary>
            HTGROWBOX = 4,

            /// <summary>
            /// In a size box (same as HTGROWBOX).
            /// </summary>
            HTSIZE = 4,

            /// <summary>
            /// In a menu.
            /// </summary>
            HTMENU = 5,

            /// <summary>
            /// In a horizontal scroll bar.
            /// </summary>
            HTHSCROLL = 6,

            /// <summary>
            /// In the vertical scroll bar.
            /// </summary>
            HTVSCROLL = 7,

            /// <summary>
            /// In a Minimize button.
            /// </summary>
            HTMINBUTTON = 8,

            /// <summary>
            /// In a Minimize button.
            /// </summary>
            HTREDUCE = 8,

            /// <summary>
            /// In a Maximize button.
            /// </summary>
            HTMAXBUTTON = 9,

            /// <summary>
            /// In a Maximize button.
            /// </summary>
            HTZOOM = 9,

            /// <summary>
            /// In the left border of a resizable window (the user can click the mouse to resize the window horizontally).
            /// </summary>
            HTLEFT = 10,

            /// <summary>
            /// In the right border of a resizable window (the user can click the mouse to resize the window horizontally).
            /// </summary>
            HTRIGHT = 11,

            /// <summary>
            /// In the upper-horizontal border of a window.
            /// </summary>
            HTTOP = 12,

            /// <summary>
            /// In the upper-left corner of a window border.
            /// </summary>
            HTTOPLEFT = 13,

            /// <summary>
            /// In the upper-right corner of a window border.
            /// </summary>
            HTTOPRIGHT = 14,

            /// <summary>
            /// In the lower-horizontal border of a resizable window (the user can click the mouse to resize the window vertically).
            /// </summary>
            HTBOTTOM = 15,

            /// <summary>
            /// In the lower-left corner of a border of a resizable window (the user can click the mouse to resize the window diagonally).
            /// </summary>
            HTBOTTOMLEFT = 16,

            /// <summary>
            /// In the lower-right corner of a border of a resizable window (the user can click the mouse to resize the window diagonally).
            /// </summary>
            HTBOTTOMRIGHT = 17,

            /// <summary>
            /// In the border of a window that does not have a sizing border.
            /// </summary>
            HTBORDER = 18,

            /// <summary>
            /// In a Close button.
            /// </summary>
            HTCLOSE = 20,

            /// <summary>
            /// In a Help button.
            /// </summary>
            HTHELP = 21,
        }

        private const int m_Gap = 4;
        private NCHITTESTFLAGS HitTest(Point pt)
        {
            NCHITTESTFLAGS ht = NCHITTESTFLAGS.HTCLIENT;
            if (Rectangle.FromLTRB(m_Gap, 0, Width - m_Gap, m_Gap).Contains(pt))
                ht = NCHITTESTFLAGS.HTTOP;
            else if (Rectangle.FromLTRB(m_Gap, Height - m_Gap, Width - m_Gap, Height).Contains(pt))
                ht = NCHITTESTFLAGS.HTBOTTOM;
            else if (Rectangle.FromLTRB(0, 0, m_Gap, m_Gap).Contains(pt))
                ht = NCHITTESTFLAGS.HTBOTTOMLEFT;
            //...... 생략
            return ht;
        }

        /*
              // DefWndProc를 override
        protected override void DefWndProc(ref Message message)
        {
            base.DefWndProc(ref message);
            if (message.Msg == (int)Msg.WM_NCHITTEST)
            {
                if ((int)message.Result == (int)NCHITTESTFLAGS.HTCLIENT)
                {
                    message.Result = (IntPtr)((int)HitTest(new Point((int)message.LParam)));
                }
            }
        }
        protected override void WndProc(ref Message m)
        {
            switch (m.Msg)
            {
                case 0x84:
                    base.WndProc(ref m);
                    if ((int)m.Result == 0x1)
                        m.Result = (IntPtr)0x2;
                    return;
            }

            base.WndProc(ref m);
        }
         */
        #endregion

        public delegate void CurrentLyricsEventHandler(string CurrentLyrics, double time);
        public event CurrentLyricsEventHandler CurrentLyrics;

        LyricParser lp = new LyricParser();
        LyricsParser_1 lp1;
        Thread th;
        ParameterizedThreadStart pth;

        string _MusicPath; 
        public string MusicPath
        { get { return _MusicPath; } set { _MusicPath = value;if(value!=null||value !=""){
        this.Text = "싱크 가사 창 - "+value.Substring(value.LastIndexOf("\\")+1);}else{
        this.Text = "싱크 가사 창";}}}

        Lyrics _lc = new Lyrics();
        public Lyrics Lyrics
        {
            get { return _lc; }
            set
            {
                _lc = new Lyrics(value.Maker, value.Lyric);
                Lyric = value.Lyric;
                lblMaker.InvokeIfNeeded(()=>lblMaker.Text = "제작자: " + value.Maker_Name);
            }
        }

        string _Lyric;
        public string Lyric
        {
            get { return _Lyric; }
            set
            {
                if (value != null)
                {
                    _Lyric = value;
                    label1.InvokeIfNeeded(() => label1.Text = "(잠시만 기다려 주세요)");
                    try { lp = new LyricParser(value);/*if (lp != null)lp.LrcCode = value; else lp = new LyricParser(value);*/ }
                    catch { }// { OffsetEnabled = true };

                    //lp1 = new LyricsParser_1(value);
                    /*if (th != null) th.Abort();
                    pth = new ParameterizedThreadStart(Lyrics_Change);
                    th = new Thread(pth); th.IsBackground = true;*/
                    //currentlrc = lp1.ReadLine();
                }
                else { /*throw new Exception("Lyric값은 Null일수 없습니다.");*/
                    lp = new LyricParser(""); label1.InvokeIfNeeded(() => label1.Text = "(싱크 가사를 사용할 수 없습니다.)");
                }
            }
        }
        public uint MaxMilliSecond;
        public uint Offset;
        public frmLyrics_Float()
        {
            InitializeComponent();
            timer1.Start();
            MoveEventHandler = new OnMoveEvent(MoveEventHandler_Body);
            m_PrevPoint = new Point();
            this.Location = new System.Drawing.Point(SystemInformation.PrimaryMonitorSize.Width - this.Width - 10, SystemInformation.PrimaryMonitorSize.Height - this.Height - 10); 
            this.SetStyle(ControlStyles.SupportsTransparentBackColor, true);
            frl.LyricsInputComplete += new frmLyrics.LyricsInputCompleteEventHandler(frl_LyricsInputComplete);
        }
        public frmLyrics_Float(Lyrics Lyric, uint MaxMillisecond)
        {
            InitializeComponent();
            timer1.Start();
            MoveEventHandler = new OnMoveEvent(MoveEventHandler_Body);
            m_PrevPoint = new Point();
            this.Location = new System.Drawing.Point(SystemInformation.PrimaryMonitorSize.Width - this.Width - 10, SystemInformation.PrimaryMonitorSize.Height - this.Height - 10); 
            this.SetStyle(ControlStyles.SupportsTransparentBackColor, true);
            frl.LyricsInputComplete += new frmLyrics.LyricsInputCompleteEventHandler(frl_LyricsInputComplete);
            this.Lyric = Lyric.Lyric;
            this.MaxMilliSecond = MaxMillisecond;
        }

        private void frmLyrics_Load(object sender, EventArgs e)
        {
            
        }


        public void Start() { StartT = true; } //th.Start(); }
        public void Pause() { StartT = false; } //timer1.Stop(); th.Suspend(); }
        void Stop() {}

        string currentlrc;
        void Lyrics_Change(object o)
        {
            
        }

        bool StartT;
        private void timer1_Tick(object sender, EventArgs e)
        {
            if (StartT)
            {
                Offset++;
                if (lp1.currentTime == Offset)
                {
                    currentlrc = lp1.ReadLine();
                    if (CurrentLyrics != null) CurrentLyrics(currentlrc, Offset);
                    if (Lyric != null) label1.Text = currentlrc;
                    else label1.Text = "(싱크 가사를 사용할 수 없습니다.)";
                }
            }
        }

        uint NextOffset;
        string Nextlrc;
        public void GetLyric(double Time)
        {
            lp.Refresh(Time);
            currentlrc = lp.CurrentLyrics;
            if (CurrentLyrics != null) CurrentLyrics(currentlrc, Time);
            if (Lyric != null) label1.InvokeIfNeeded(() => label1.Text = currentlrc);
            else label1.InvokeIfNeeded(() => label1.Text = "(싱크 가사를 사용할 수 없습니다.)");
            /*
            NextOffset = (uint)lp1.tlrc[lp1.mark + 1].ms;
            if (lp1.currentTime+10 >= Offset&&lp1.currentTime+10 >= Offset)
            {
                currentlrc = lp1.ReadLine();
                if (CurrentLyrics != null) CurrentLyrics(currentlrc, Offset);
                if (Lyric != null) label1.InvokeIfNeeded(() => label1.Text = currentlrc);
                else label1.InvokeIfNeeded(() => label1.Text = "(싱크 가사를 사용할 수 없습니다.)");
            }*/
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        bool FormClose;
        private void frmLyrics_Float_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = !FormClose;
            this.Hide();
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox1.Checked) this.TransparencyKey = this.BackColor;
            else this.TransparencyKey = System.Drawing.Color.Empty;
        }

        private void trackBar1_ValueChanged(object sender, EventArgs e)
        {
            this.Opacity = trackBar1.Value * 0.01f;
        }

        void frl_LyricsInputComplete(string Lyric)
        {
            this.Lyric = Lyric;
        }

        frmLyrics frl = new frmLyrics();
        private void 싱크가사직접입력ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frl.Lyrics = Lyric;
            frl.Show();
            frl.openFileDialog1.InitialDirectory = System.IO.Path.GetDirectoryName(MusicPath);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void checkBox2_CheckedChanged(object sender, EventArgs e)
        {
            this.TopMost = checkBox2.Checked;
        }

        private void frmLyrics_Float_MouseDown(object sender, MouseEventArgs e)
        {
            //pointMouseOffset = new Point(-e.X, -e.Y);
        }

        private void frmLyrics_Float_MouseMove(object sender, MouseEventArgs e)
        {
            /*
            if (!(sender is System.Windows.Forms.Control)) return;

            if (e.Button == MouseButtons.Left)
            {

                Control objControl = (Control)sender;
                ScrollableControl objScrollControl = (ScrollableControl)objControl.Parent;
                Point pointMouse = Control.MousePosition;

                if (sender.Equals(this))
                {
                    pointMouse.Offset(pointMouseOffset.X, pointMouseOffset.Y);
                }
                else
                {
                    pointMouse.Offset(pointMouseOffset.X - objScrollControl.DockPadding.Left - objControl.Left
                      , pointMouseOffset.Y - objScrollControl.DockPadding.Top - objControl.Top);
                }

                this.Location = pointMouse;
            }*/
        }

        private Point pointMouseOffset = new Point(0, 0);
        private void menuStrip1_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Left)
            {
                this.MoveEventHandler(
                    JPointCalculator.Add(
                        this.Location,
                        JPointCalculator.Subtract(Cursor.Position, m_PrevPoint)));
                //JPointCalculator.Set(Cursor.Position, ref m_PrevPoint);
             }
        }

        private void menuStrip1_MouseDown(object sender, MouseEventArgs e)
        {
            JPointCalculator.Set(Cursor.Position, ref m_PrevPoint);
        }

        private void menuStrip1_MouseUp(object sender, MouseEventArgs e)
        {
            JPointCalculator.Set(new Point(0, 0), ref m_PrevPoint);
        }

        private void vScrollBar1_ValueChanged(object sender, EventArgs e)
        {
            label1.Font = new Font(label1.Font.FontFamily, (float)vScrollBar1.Value, label1.Font.Style, label1.Font.Unit); ;
        }

        private void 폰트설정ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            fontDialog1.Font = label1.Font;
            fontDialog1.Color = label1.ForeColor;
            if (fontDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {label1.Font = fontDialog1.Font;}
        }

        private void fontDialog1_Apply(object sender, EventArgs e)
        {
            label1.Font = fontDialog1.Font;
            label1.ForeColor = fontDialog1.Color;
        }

        private void 배경색깔바꾸기ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (DialogResult.OK == colorDialog1.ShowDialog())
            {
                this.BackColor = colorDialog1.Color;
                //this.TransparencyKey = this.BackColor;
            }
        }
    }
    public class JPointCalculator
    {
        public static Point Add(Point x, Point y)
        {
            return new Point(x.X + y.X, x.Y + y.Y);
        }

        public static void Add(Point x, Point y, ref Point dst)
        {
            dst.Offset(x.X + y.X, x.Y + y.Y);
        }

        public static Point Subtract(Point x, Point y)
        {
            return new Point(x.X - y.X, x.Y - y.Y);
        }

        public static void Subtract(Point x, Point y, ref Point dst)
        {
            dst.Offset(x.X - y.X, x.Y - y.Y);
        }

        public static void Set(Point x, ref Point dst)
        {
            dst.X = x.X;
            dst.Y = x.Y;
        }
    }
}
