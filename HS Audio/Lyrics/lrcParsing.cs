﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace HS_Audio.Lyrics
{
    [Serializable]
    /// <summary>
    /// LRC 포맷 가사를 나타냅니다 (表示一个LRC格式的歌词) (코드 출처: http://equinox1993.blog.163.com/blog/static/32205137201031141228418/)
    /// </summary>
    public class LyricParser : Lyric
    {
        #region values

        Dictionary<string, string> _IndexlrcCode;
        public Dictionary<string, string> IndexlrcCode { get { return _IndexlrcCode; } }

        LyricClass _Lyrics = new LyricClass();
        public LyricClass Lyrics
        { get { _Lyrics.SetLyrics(this); return _Lyrics; } }

        new string PureLyrics;
        /// <summary>
        /// 시간하고 가사만 들어있는 LrcCode 입니다. (Lyrics.PureLyrics와 같습니다.)
        /// </summary>
        public String PurelrcCode { get { return PureLyrics; }protected internal set { PureLyrics = value; base.PureLyric = value;} }

        String lrcCode;
        /// <summary>
        /// LRC 코드를 설정 하거나 가져옵니다 (获取或设置LRC歌词代码)
        /// </summary>
        public String LrcCode
        {
            get{return lrcCode;}
            set
            {
                lrcCode = value;
                _lyricsDictionary = lrcToDictionary(lrcCode, out PureLyrics, out _IndexlrcCode);
                timeArray = sortedKeysInDictionary(_lyricsDictionary);
                if (_lyricsDictionary.Count > 0&& _lyricsDictionary!=null)
                if (_lyricsDictionary.ContainsKey("ti")) title = _lyricsDictionary["ti"].ToString();
                if (_lyricsDictionary.ContainsKey("ar")) artist = _lyricsDictionary["ar"].ToString();
                if (_lyricsDictionary.ContainsKey("by")) lyricsMaker = _lyricsDictionary["by"].ToString();
                if (_lyricsDictionary.ContainsKey("al")) album = _lyricsDictionary["al"].ToString();
                if (_lyricsDictionary.ContainsKey("au")) author = _lyricsDictionary["au"].ToString();
                if (_lyricsDictionary.ContainsKey("length")) length = _lyricsDictionary["length"].ToString();
                if (_lyricsDictionary.ContainsKey("comment")) comment = _lyricsDictionary["comment"].ToString();
                
                switch (isOffsetEnabled)
                {
                    case true:
                        if (_lyricsDictionary.ContainsKey("offset"))
                            int.TryParse(_lyricsDictionary["offset"].ToString(), out offset);
                        else
                            offset = 0;
                        break;
                    case false:
                        offset = 0;
                        break;
                }
            }
        }
        int offset;
        /// <summary>
        /// LRC 가사 오프셋을 가져오거나 설정합니다. (获取或设置LRC歌词的偏移)
        /// </summary>
        public new int Offset
        {
            get{return offset;}
            set
            {
                offset = value;
                base.Offset = value;
                //int _offset;
                try { _lyricsDictionary["offset"] = value; }catch {_lyricsDictionary.Add("offset" ,value); }
            }
        }

        //System.Collections.Hashtable _lyricsDictionary;
        Dictionary<object, object> _lyricsDictionary;
        /// <summary>
        ///  获取或设置歌词时间为Key,内容为Value的哈希表
        /// </summary>
        public Dictionary<object, object> LyricsDictionary
        {
            get
            {
                return _lyricsDictionary;
            }
            set
            {
                _lyricsDictionary = value;
                timeArray = sortedKeysInDictionary(_lyricsDictionary);
                try{title = _lyricsDictionary["ti"].ToString();}catch { }
                try{artist = _lyricsDictionary["ar"].ToString();}catch { }
                try{lyricsMaker = _lyricsDictionary[@"by"].ToString();}catch { }
                try {album = _lyricsDictionary["al"].ToString();}catch { }
                try {author = _lyricsDictionary["au"].ToString();}catch { }
                try {length = _lyricsDictionary["length"].ToString();}catch { }
                try {comment = _lyricsDictionary["comment"].ToString();}catch { }
                switch (isOffsetEnabled)
                {
                    case true:
                        if (_lyricsDictionary.ContainsKey("offset"))
                            int.TryParse(_lyricsDictionary["offset"].ToString(), out offset);
                        else
                            offset = 0;
                        break;
                    case false:
                        offset = 0;
                        break;
                }
            }
        }
        //System.Collections.ArrayList timeArray;
        List<string> timeArray;
        String currentLyrics;
        /// <summary>
        /// 返回当前的歌词,使用前请先调用Refresh()函数
        /// </summary>
        public String CurrentLyrics
        {
            get {return currentLyrics; }set{currentLyrics = value;}
        }
        String nextLyrics;
        /// <summary>
        /// 返回下一个歌词,使用前请先调用Refresh()函数
        /// </summary>
        public String NextLyrics
        {
            get{return nextLyrics;}set{nextLyrics = value;}
        }
        String previousLyrics;
        /// <summary>
        /// 返回上一个歌词,使用前请先调用Refresh()函数
        /// </summary>
        public String PreviousLyrics
        {
            get{return previousLyrics;}
            set{previousLyrics = value;}
        }
        int currentIndex;
        /// <summary>
        /// 返回当前歌词的Index,使用前请先调用Refresh()函数
        /// </summary>
        public int CurrentIndex
        {
            get
            {
                return currentIndex;
            }
            set
            {
                currentIndex = value;
            }
        }
        String title;
        /// <summary>
        /// 返回歌词的标题
        /// </summary>
        public String Title 
        {   get { if (_lyricsDictionary != null&& _lyricsDictionary.ContainsKey("ti")) return _lyricsDictionary["ti"].ToString(); else return null; }
            set { try { _lyricsDictionary["ti"] = value; } catch { _lyricsDictionary.Add("ti", value); } base.Title = value; }
        }
        String album;
        /// <summary>
        /// 返回歌词的专辑名称
        /// </summary>
        public String Album
        {
            get { if (_lyricsDictionary != null&& _lyricsDictionary.ContainsKey("al")) return _lyricsDictionary["al"].ToString(); else return null; }//return album;}
            set { try { _lyricsDictionary["al"] = value; } catch { _lyricsDictionary.Add("al", value); } base.Album = value; }
        }
        String artist;
        /// <summary>
        /// 返回歌词的表演者
        /// </summary>
        public String Artist
        {
            get { if (_lyricsDictionary != null&& _lyricsDictionary.ContainsKey("ar")) return _lyricsDictionary["ar"].ToString(); else return null; }//return artist;}
            set { try { _lyricsDictionary["ar"] = value; } catch { _lyricsDictionary.Add("ar", value); } base.Artist = value; }
        }
        String comment;
        public String Comment
        {
            get { if (_lyricsDictionary != null&& _lyricsDictionary.ContainsKey("comment")) return _lyricsDictionary["comment"].ToString(); else return null; }//return comment
            set { try { _lyricsDictionary["comment"] = value; } catch { _lyricsDictionary.Add("comment", value); } base.Comment = value; }
        }
        String author;
        public String Author 
        {
            get { if (_lyricsDictionary != null&& _lyricsDictionary.ContainsKey("au")) return _lyricsDictionary["au"].ToString(); else return null;}//return author; }
            set { try { _lyricsDictionary["au"] = value; } catch { _lyricsDictionary.Add("au", value); } base.Author = value; }
        }

        String length;
        public String Length 
        {
            get { if (_lyricsDictionary != null&& _lyricsDictionary.ContainsKey("length")) return _lyricsDictionary["length"].ToString(); else return null; }//return length; } 
            set { try { _lyricsDictionary["length"] = value; } catch { _lyricsDictionary.Add("length", value); } base.Length = value; }
        }

        String lyricsMaker;
        /// <summary>
        /// 返回歌词的制作者
        /// </summary>
        public String LyricsMaker
        {
            get { if (_lyricsDictionary != null&& _lyricsDictionary.ContainsKey(@"by")) return _lyricsDictionary[@"by"].ToString(); else return null; }//return lyricsMaker;}
            set { try { _lyricsDictionary[@"by"] = value; } catch { _lyricsDictionary.Add(@"by", value); } base.LyricsMaker = value; }
        }
        bool isOffsetEnabled;
        /// <summary>
        /// 시작 오프셋 여부를 설정하거나 가져옵니다. (获取或设置是否启动偏移)
        /// </summary>
        public bool OffsetEnabled
        {
            set
            {
                switch (value)
                {
                    case true:
                        offset = _lyricsDictionary == null||_lyricsDictionary["offset"] == null ? 0 : (int)_lyricsDictionary["offset"];
                        break;
                    case false:
                        offset = 0;
                        break;
                }
                isOffsetEnabled = value;
            }
            get
            {
                return isOffsetEnabled;
            }
        }
        #endregion
        
        #region build
        /// <summary>
        /// 初始化新的LyricParser实例
        /// </summary>
        public LyricParser()
        {
            OffsetEnabled = true;
            offset = 0;
            currentIndex = 0;
        }
        /// <summary>
        /// 通过指定的Lrc文件初始化LyricParser实例
        /// </summary>
        /// <param name="filePath">文件路径</param>
        /// <param name="enc">文件编码</param>
        public LyricParser(String filePath, Encoding enc)
        {
            isOffsetEnabled = true;
            offset = 0;
            currentIndex = 0;
            StreamReader streamReader = new StreamReader(filePath, enc);
            LrcCode = streamReader.ReadToEnd();
        }
        /// <summary>
        /// 通过指定的Lrc文件初始化LyricParser实例
        /// </summary>
        /// <param name="filePath">文件路径</param>
        /// <param name="enc">文件编码</param>
        public LyricParser(String filePath, Encoding enc, bool EncodingUse)
        {
            isOffsetEnabled = true;
            offset = 0;
            currentIndex = 0;
            //StreamReader streamReader;
            if (EncodingUse) LrcCode = File.ReadAllText(filePath, enc); //streamReader = new StreamReader(filePath, enc);
            else LrcCode = File.ReadAllText(filePath);
            //LrcCode = streamReader.ReadToEnd();
        }
        /// <summary>
        ///  通过指定的Lrc代码初始化LyricParser实例
        /// </summary>
        /// <param name="code">Lrc代码</param>
        public LyricParser(String code)
        {
            isOffsetEnabled = true;
            offset = 0;
            currentIndex = 0;
            LrcCode = code;
        }
        public void SetLyrics(String code)
        { LrcCode = code; }
        #endregion
        #region protected functions
        protected static Dictionary<object, object> lrcToDictionary(String lrc)
        {
            String lrct = lrc.Replace("\r", "\n");
            Dictionary<object, object> md = new Dictionary<object, object>();
            String aline;
            List<string> av = new List<string>(lrct.Split('\n'));
            string a, b;
            int ab = av.Count;
            //for (int j = 0; j < ab; j++) { try { if (av[j == 0 ? 0 : j] == "[00:00.00]" || av[j == 0 ? 0 : j] == "") { av.RemoveAt(j); } } catch { if (j >= av.Count) { break; } } }
            int i;
            StringBuilder sb = new StringBuilder();
            for (i = 0; i < av.Count; i++)
            {
                if (av[i] != "" || av[i] != "[00:00.00]")
                {
                    aline = av[i].Substring(av[i].IndexOf("[")+1);
                    if (aline.IndexOf("]") != -1)
                    {
                        a = aline.Remove(av[i].IndexOf("]")==0 ? 0 : av[i].IndexOf("]") - 1);
                        b = aline.Substring(av[i].IndexOf("]"));
                        if (true/*aline.Split(']').GetLength(0) == 2*/)
                        {
                            if (aline.IndexOf("ti:") != -1
                            || aline.IndexOf("ar:") != -1
                            || aline.IndexOf("al:") != -1
                            || aline.IndexOf("by:") != -1
                            || aline.IndexOf("au:") != -1
                            || aline.IndexOf("offset:") != -1
                            || aline.IndexOf("length:")!=-1
                            || aline.IndexOf("comment:")!=-1)
                            {
                                //aline = aline.Replace("]", "");
                                md[a.Remove(a.IndexOf(':'))] = a.Substring(a.IndexOf(':')+1);
                            }
                            else
                            {
                                //md[aline.Split(']').GetValue(0)] = aline.Split(']').GetValue(1);
                                try { md.Add(a, b/*aline.Split(']').GetValue(0), aline.Split(']').GetValue(1)*/); }
                                catch(ArgumentException) {
                                    //md[aline.Split(']').GetValue(0)]= md[aline.Split(']').GetValue(0)]+"\r\n"+aline.Split(']').GetValue(1);
                                    md[a] = md[a]+"\r\n"+b; }
                                sb.AppendLine("["+a+"]" + b);
                            }
                        }
                        else
                        {
                            int subi;
                            for (subi = 0; subi < aline.Split(']').GetLength(0); subi++)
                            {
                                if (subi < aline.Split(']').GetLength(0) - 1)
                                    md[aline.Split(']').GetValue(subi)] = aline.Split(']').GetValue(aline.Split(']').GetLength(0) - 1);
                            }
                        }
                    }
                }
            }
            return md;
        }
        protected static Dictionary<object, object> lrcToDictionary(String lrc, out string Purelrc)
        {
            String lrct = lrc.Replace("\r\n", "\n").Replace("\r", "\n");
            Dictionary<object, object> md = new Dictionary<object, object>();
            String aline;
            List<string> av = new List<string>(lrct.Split('\n'));
            List<string> tmp = new List<string>(); 
            string a="", b="";
            int ab = av.Count;
            //for (int j = 0; j < ab; j++) { try { if (av[j == 0 ? 0 : j] == "[00:00.00]" || av[j == 0 ? 0 : j] == "") { av.RemoveAt(j); } } catch { if (j >= av.Count) { break; } } }
            int i;
            StringBuilder sb = new StringBuilder();
            for (i = 0; i < av.Count; i++)
            {
                if (av[i] != ""/* || av[i] != "[00:00.00]"*/)
                {
                    aline = av[i].Substring(av[i].IndexOf("[") + 1);
                    if (aline.IndexOf("]") != -1)
                    {
                        try { a = aline.Remove(av[i].IndexOf("]") == 0 ? 0 : av[i].IndexOf("]") - 1); }catch{}
                        try { b = aline.Substring(av[i].IndexOf("]")); }catch{}
                        if (true/*aline.Split(']').GetLength(0) == 2*/)
                        {
                            if (aline.IndexOf("ti:") != -1
                            || aline.IndexOf("ar:") != -1
                            || aline.IndexOf("al:") != -1
                            || aline.IndexOf("by:") != -1
                            || aline.IndexOf("au:") != -1
                            || aline.IndexOf("offset:") != -1
                            || aline.IndexOf("length:") != -1
                            || aline.IndexOf("comment:") != -1)
                            {
                                //aline = aline.Replace("]", "");
                                try { md[a.Remove(a.IndexOf(':'))] = a.Substring(a.IndexOf(':')+1); }catch{}
                            }
                            else
                            {
                                //md[aline.Split(']').GetValue(0)] = aline.Split(']').GetValue(1);
                                try { md.Add(a, b/*aline.Split(']').GetValue(0), aline.Split(']').GetValue(1)*/); }
                                catch (ArgumentException)
                                {
                                    //md[aline.Split(']').GetValue(0)]= md[aline.Split(']').GetValue(0)]+"\r\n"+aline.Split(']').GetValue(1);
                                    md[a] = md[a] + "\r\n" + b;
                                }
                                sb.AppendLine("[" + a + "]" + b);
                            }
                        }
                        else
                        {
                            int subi;
                            for (subi = 0; subi < aline.Split(']').GetLength(0); subi++)
                            {
                                if (subi < aline.Split(']').GetLength(0) - 1)
                                    md[aline.Split(']').GetValue(subi)] = aline.Split(']').GetValue(aline.Split(']').GetLength(0) - 1);
                            }
                        }
                    }
                }
            }
            Purelrc = sb.ToString();
            return md;
        }
        protected static Dictionary<object, object> lrcToDictionary(String lrc, out string Purelrc, out Dictionary<string, string> IndexLrc)
        {
            String lrct = lrc.Replace("\r\n", "\n").Replace("\r", "\n");
            Dictionary<object, object> md = new Dictionary<object, object>();
            String aline;
            List<String> av = new List<string>(lrct.Split('\n'));
            Dictionary<string, string> tmp = new Dictionary<string, string>();
            string a = "", b = "";
            int ab = av.Count;
            //for (int j = 0; j < ab; j++) { try { if (av[j == 0 ? 0 : j] == "[00:00.00]" || av[j == 0 ? 0 : j] == "") { av.RemoveAt(j); } } catch { if (j >= av.Count) { break; } } }
            int i;
            StringBuilder sb = new StringBuilder();
            for (i = 0; i < av.Count; i++)
            {
                if (av[i] != ""/* || av[i] != "[00:00.00]"*/)
                {
                    aline = av[i].Substring(av[i].IndexOf("[") + 1);
                    if (aline.IndexOf("]") != -1)
                    {
                        try { b = aline.Substring(av[i].IndexOf("]")); }catch { }
                        if (true/*aline.Split(']').GetLength(0) == 2*/)
                        {
                            if (aline.IndexOf("ti:") != -1
                            || aline.IndexOf("ar:") != -1
                            || aline.IndexOf("al:") != -1
                            || aline.IndexOf("by:") != -1
                            || aline.IndexOf("au:") != -1
                            || aline.IndexOf("offset:") != -1
                            || aline.IndexOf("length:") != -1
                            || aline.IndexOf("comment:") != -1)
                            {
                                try{a = aline.Remove(av[i].IndexOf("]") == 0 ? 0 : av[i].IndexOf("]") - 1); }catch { }
                                //aline = aline.Replace("]", "");
                                try { tmp[a.Remove(a.IndexOf(':'))] = a.Substring(a.IndexOf(':')+1); } catch { }
                                try { md.Add(a.Remove(a.IndexOf(':')), a.Substring(a.IndexOf(':')+1)); }catch { }
                            }
                            else
                            {
                                try { a = aline.Remove(av[i].IndexOf("]") == 0 ? 0 : av[i].IndexOf("]") - 1); }catch { }
                                //md[aline.Split(']').GetValue(0)] = aline.Split(']').GetValue(1);
                                try
                                {
                                    if(md.ContainsKey(a)) md[a] = md[a] + "\r\n" + b;
                                    else md.Add(a, b);
                                }
                                catch (ArgumentException)
                                {
                                    //md[aline.Split(']').GetValue(0)]= md[aline.Split(']').GetValue(0)]+"\r\n"+aline.Split(']').GetValue(1);

                                }
                                sb.AppendLine("[" + a + "]" + b);
                            }
                        }
                        else
                        {
                            int subi;
                            for (subi = 0; subi < aline.Split(']').GetLength(0); subi++)
                            {
                                if (subi < aline.Split(']').GetLength(0) - 1)
                                    md[aline.Split(']').GetValue(subi)] = aline.Split(']').GetValue(aline.Split(']').GetLength(0) - 1);
                            }
                        }
                    }
                }
            }
            Purelrc = sb.ToString();
            IndexLrc = tmp;
            return md;
        }
        protected static List<string> sortedKeysInDictionary(Dictionary<object, object> dictionary)
        {
            String[] av = new String[dictionary.Keys.Count];
            List<string> al;
            dictionary.Keys.CopyTo(av, 0);
            al = new List<string>(av);
            al.Sort(); List<string> a = new List<string>(dictionary.Keys.Count);
            return al;
        }
        protected static List<string> sortedKeysInDictionary_String(Dictionary<object, object> dictionary)
        {
            string[] av = new string[dictionary.Keys.Count];
            dictionary.Keys.CopyTo(av, 0);
            List<string> a = new List<string>(av); a.Sort();
            return a;
        }
        protected static String intervalToString(double interval)
        {
            int min;
            float sec;
            min = (int)interval / 60;
            sec = (float)(interval - (float)min * 60.0);
            String smin = String.Format("{0:d2}", min);
            String ssec = String.Format("{0:00.00}", sec);
            return smin + ":" + ssec;
        }
        protected static double stringToInterval(String str)
        {
            string[] a = str.Split(':');
            try
            {
                if (a.Length > 1)
                {

                    double min = double.Parse(a[0].ToString());
                    double sec = double.Parse(a[1].ToString());
                    return min * 60.0 + sec;
                }
            }
            catch (FormatException ex)
            {
                HS_Library.HSConsole.WriteLine("Wrong Lyric format!!\n└ Contain Value: " + HS_CSharpUtility.Utility.ArrayUtility.ConvertArrayToString(a, ", "));
            }
            catch {}
            return uint.MaxValue;
        }
        #endregion
        #region refresh functions
        /// <summary>
        /// 현재 가사를 새로 지정된 시간의 예 (使用指定的时间刷新实例的当前歌词)
        /// </summary>
        /// <param name="time">시간 (时间)</param>
        public void Refresh(double time)
        {
            try
            {
                currentLyrics = null;
                if (timeArray.Count > 0  && (time - (double)offset / 1000.0 >= stringToInterval(timeArray[currentIndex].ToString()) && time - (double)offset / 1000.0 < stringToInterval(timeArray[currentIndex + 1].ToString())))
                {
                    currentLyrics = _lyricsDictionary[timeArray[currentIndex]].ToString();
                    if (currentIndex + 1 < timeArray.Count)
                        nextLyrics = _lyricsDictionary[timeArray[currentIndex + 1]].ToString();
                    if (currentIndex - 1 >= 0)
                        previousLyrics = _lyricsDictionary[timeArray[currentIndex - 1]].ToString();
                }
                else if (timeArray.Count > 0 && (time - (double)offset / 1000.0 >= stringToInterval(timeArray[currentIndex + 1].ToString()) && time - (double)offset / 1000.0 < stringToInterval(timeArray[currentIndex + 2].ToString())))
                {
                    currentIndex++;
                    currentLyrics = _lyricsDictionary[timeArray[currentIndex]].ToString();
                    if (currentIndex + 1 < timeArray.Count)
                        nextLyrics = _lyricsDictionary[timeArray[currentIndex + 1]].ToString();
                    if (currentIndex - 1 >= 0)
                        previousLyrics = _lyricsDictionary[timeArray[currentIndex - 1]].ToString();
                }
                else
                {
                    int i;
                    for (i = 0; i < timeArray.Count; i++)
                    {
                        try
                        {
                            if (time - (double)offset / 1000.0 >= stringToInterval(timeArray[i].ToString()) && time - (double)offset / 1000.0 < stringToInterval(timeArray[i + 1].ToString()))
                            {
                                currentIndex = i;
                                currentLyrics = _lyricsDictionary[timeArray[i]].ToString();
                                if (i + 1 < timeArray.Count)
                                    nextLyrics = _lyricsDictionary[timeArray[i + 1]].ToString();
                                if (i - 1 >= 0)
                                    previousLyrics = _lyricsDictionary[timeArray[i - 1]].ToString();
                                break;
                            }
                        }
                        catch { break; }
                    }
                }
            }
            catch(ArgumentOutOfRangeException ex)
            {
                try
                {
                    currentLyrics = _lyricsDictionary[timeArray[currentIndex + 1]].ToString();
                    nextLyrics = "";
                    if (currentIndex >= 0)
                        previousLyrics = _lyricsDictionary[timeArray[currentIndex - 1]].ToString();
                }
                catch { }
            }
        }
        /// <summary>
        /// 지정된 시간 문자열 현재 인스턴스 가사의 형식을 새로 고침 (使用指定的时间字符串刷新实例的当前歌词,格式为)"mm:ss.ss"
        /// </summary>
        /// <param name="aString">시간 문자열 (时间字符串)</param>
        public void Refresh(String aString)
        {
            Refresh(stringToInterval(aString));
        }
        #endregion
        #region directly get lyrics functions
        /// <summary>
        /// 연결 안가 null을 반환하는 경우, 시간에 주어진 지점의 가사에 직접 액세스 (直接获取指定时间点的歌词,若没有此时间点则返回null)
        /// </summary>
        /// <param name="time">시간 (时间点)</param>
        /// <returns>가사 (歌词)</returns>
        public String Lyric(double time)
        {
            try
            {
                return _lyricsDictionary[intervalToString(time - (double)offset / 1000.0)].ToString();
            }
            catch
            {
                return null;
            }
        }
        /// <summary>
        /// 시간 문자열 가사의 지정된 지점에 직접 액세스, 그렇지 않은 경우이 시간은 NULL을 반환 (直接获取指定时间点字符串的歌词,若没有此时间点则返回null)
        /// </summary>
        /// <param name="aString">시간 문자열 (时间点字符串)</param>
        /// <returns>가사 (歌词)</returns>
        public String Lyric(String aString)
        {
            try
            {
                return _lyricsDictionary[intervalToString(stringToInterval(aString) - (double)offset / 1000.0)].ToString();
            }
            catch
            {
                return null;
            }
        }
        /// <summary>
        /// 지정된 인덱스의 가사를 가져옵니다 (获取指定索引的歌词)
        /// </summary>
        /// <param name="index">인덱스  입니다. (索引)</param>
        /// <returns>가사 (歌词)</returns>
        public String LyricsAtIndex(int index)
        {
            try
            {
                return _lyricsDictionary[timeArray[index]].ToString();
            }
            catch
            {
                return null;
            }
        }
        #endregion
        #region override
        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            List<string> a = sortedKeysInDictionary_String(_lyricsDictionary);
            for (int i = 0; i < _lyricsDictionary.Count; i++)
            {
                if (a[i].IndexOf("ti") != -1
                 || a[i].IndexOf("ar") != -1
                 || a[i].IndexOf("al") != -1
                 || a[i].IndexOf("by") != -1
                 || a[i].IndexOf("au") != -1
                 || a[i].IndexOf("offset") != -1
                 || a[i].IndexOf("length") != -1
                 || a[i].IndexOf("comment") != -1) { sb.AppendFormat("[{0}:{1}]\r\n", a[i], _lyricsDictionary[a[i]]); }
                else
                {
                    //sb.AppendLine(string.Format("[{0}]",a[i])+_lyricsDictionary[a[i]].ToString().Replace("\r\n", string.Format("\r\n[{0}]", a[i])));
                    //sb.AppendFormat("[{0}]{1}\r\n", a[i], _lyricsDictionary[a[i]].ToString().Replace("\r\n", a[i]+"\r\n"));
                }
            }
            sb.Append(PureLyrics); return sb.ToString();
            //return base.ToString();
        }

        Array CopyforICollection(ICollection Collection)
        {
            object[] o = new object[Collection.Count];
            Collection.CopyTo(o, 0);
            return o;
        }
        string[] ICollectionToStringArray(ICollection Collection)
        {
            string[] o = new string[Collection.Count];
            Collection.CopyTo(o, 0);
            return o;
        }
        #endregion
    }
    /// <summary>
    /// LRC가사 파싱 클래스2 (코드 출처: http://blog.csdn.net/www314599782/article/details/6400034)
    /// </summary>
    public class LyricsParser_1
    {
        public struct TLrc
        {
            public int ms;//毫秒  
            public string lrc;//对应的歌词  
        }

        /// <summary>  
        /// 가사 구조체 표준 배열입니다. (标准的歌词结构数组 )
        /// </summary>  
        public TLrc[] tlrc;
        /// <summary>  
        /// 输入歌词文件路径处理歌词信息  
        /// </summary>  
        /// <param name="FilePath"></param>  
        public LyricsParser_1(string FilePath, Encoding Enc)
        {
            if (Enc == null)Enc= Encoding.Default;
            StreamReader sr = new StreamReader(getLrcFile(FilePath), Enc);
            string[] lrc_1 = sr.ReadToEnd().Split(new char[] { '[', ']' });
            sr.Close();

            format_1(lrc_1);
            format_2(lrc_1);
            format_3();
        }
        public LyricsParser_1(string LyricString)
        {
            string[] lrc_1 = LyricString.Split(new char[] { '[', ']' });

            format_1(lrc_1);
            format_2(lrc_1);
            //format_3();
        }
        /// <summary>  
        /// 格式化不同时间相同字符如“[00:34.52][00:34.53][00:34.54]因为我已经没有力气”  
        /// </summary>  
        /// <param name="lrc_1"></param>  
        private void format_1(string[] lrc_1)
        {
            for (int i = 2, j = 0; i < lrc_1.Length; i += 2, j = i)
            {
                while (lrc_1[j] == string.Empty)
                {
                    lrc_1[i] = lrc_1[j += 2];
                }
            }
        }
        /// <summary>  
        /// 数据添加到结构体  
        /// </summary>  
        /// <param name="lrc_1"></param>  
        private void format_2(string[] lrc_1)
        {
            tlrc = new TLrc[lrc_1.Length / 2];
            for (int i = 1, j = 0; i < lrc_1.Length; i++, j++)
            {
                tlrc[j].ms = timeToMs(lrc_1[i]);
                tlrc[j].lrc = lrc_1[++i];
            }
        }
        /// <summary>  
        /// 时间格式”00:37.61“转毫秒  
        /// </summary>  
        /// <param name="lrc_t"></param>  
        /// <returns></returns>  
        private int timeToMs(string lrc_t)
        {
            float m, s, ms;
            string[] lrc_t_1 = lrc_t.Split(':');
            //这里不能用TryParse如“by:253057646”则有问题  
            try
            {
                m = float.Parse(lrc_t_1[0]);
            }
            catch
            {
                return 0;
            }
            float.TryParse(lrc_t_1[1], out s);
            ms = m * 60000 + s * 1000;
            return (int)ms;
        }
        /// <summary>  
        /// 排序，时间顺序  
        /// </summary>  
        private void format_3()
        {
            TLrc tlrc_temp;
            bool b = true;
            for (int i = 0; i < tlrc.Length - 1; i++, b = true)
            {
                for (int j = 0; j < tlrc.Length - i - 1; j++)
                {
                    if (tlrc[j].ms > tlrc[j + 1].ms)
                    {
                        tlrc_temp = tlrc[j];
                        tlrc[j] = tlrc[j + 1];
                        tlrc[j + 1] = tlrc_temp;
                        b = false;
                    }
                }
                if (b) break;
            }
        }

        public int mark;
        /// <summary>  
        /// 다음 레코드를 읽고, 다음 레코드로 이동 (读取下一条记录,并跳到下一条记录)
        /// </summary>  
        /// <returns></returns>  
        public string ReadLine()
        {
            if (mark < tlrc.Length)
            {
                return tlrc[mark++].lrc;
            }
            else
            {
                return null;
            }

        }
        /// <summary>  
        /// 현재 행의 가사를 읽을 시간 (读取当前行的歌词的时间)
        /// </summary>  
        /// <returns></returns>  
        public int currentTime
        {
            get
            {
                if (mark < tlrc.Length)
                {
                    return tlrc[mark].ms;
                }
                else
                {
                    return -1;
                }
            }
        }

        /// <summary>  
        /// LRC 가사 (현재 디렉토리) 파일 받기 (得到lrc歌词文件(当前目录) )
        /// </summary>  
        /// <param name="file"></param>  
        /// <returns></returns>  
        private string getLrcFile(string file)
        {
            return Path.GetDirectoryName(file) + "//" + Path.GetFileNameWithoutExtension(file) + ".lrc";
        }
    }
}
