﻿using System;
using System.Windows.Forms;
using System.Threading;
using HS_CSharpUtility;
using System.Drawing;
using System.Collections.Generic;
using HS_CSharpUtility.Extension;

namespace HS_Audio.Lyrics
{
    public partial class frmLyricsDesktop : Form
    {
        void InitResource()
        {
        }

        #region 폼 리사이즈, 이동하기

        private Point m_PrevPoint;

        public delegate void OnMoveEvent(Point pt);
        public OnMoveEvent MoveEventHandler;
        private void MoveEventHandler_Body(Point pt)
        {
            /*
            if (this.InvokeRequired)
            {
                this.Invoke(MoveEventHandler, new object[] { pt });
            }
            else
            {
                this.Location = pt;
            }*/
        }

        /*
        protected override CreateParams CreateParams
        {
            get
            {
                const int WS_CAPTION = 0x00C00000;
                CreateParams baseParams = base.CreateParams;
                //Get rid of caption
                baseParams.Style = baseParams.Style & ~WS_CAPTION;
                return baseParams;
            }
        }*/

        public enum Msg
        {
                    //.... 일부 생략
            WM_NCHITTEST = 0x84,
                   //.... 일부 생략
        }

        /// <summary>
        /// Indicates the position of the cursor hot spot.
        /// </summary>
        public enum NCHITTESTFLAGS : int
        {
            /// <summary>
            /// On the screen background or on a dividing line between windows (same as HTNOWHERE, except that the DefWindowProc function produces a system beep to indicate an error).
            /// </summary>
            HTERROR = -2,

            /// <summary>
            /// In a window currently covered by another window in the same thread (the message will be sent to underlying windows in the same thread until one of them returns a code that is not HTTRANSPARENT).
            /// </summary>
            HTTRANSPARENT = -1,

            /// <summary>
            /// On the screen background or on a dividing line between windows.
            /// </summary>
            HTNOWHERE = 0,

            /// <summary>
            /// In a client area.
            /// </summary>
            HTCLIENT = 1,

            /// <summary>
            /// In a title bar.
            /// </summary>
            HTCAPTION = 2,

            /// <summary>
            /// In a window menu or in a Close button in a child window.
            /// </summary>
            HTSYSMENU = 3,

            /// <summary>
            /// In a size box (same as HTSIZE).
            /// </summary>
            HTGROWBOX = 4,

            /// <summary>
            /// In a size box (same as HTGROWBOX).
            /// </summary>
            HTSIZE = 4,

            /// <summary>
            /// In a menu.
            /// </summary>
            HTMENU = 5,

            /// <summary>
            /// In a horizontal scroll bar.
            /// </summary>
            HTHSCROLL = 6,

            /// <summary>
            /// In the vertical scroll bar.
            /// </summary>
            HTVSCROLL = 7,

            /// <summary>
            /// In a Minimize button.
            /// </summary>
            HTMINBUTTON = 8,

            /// <summary>
            /// In a Minimize button.
            /// </summary>
            HTREDUCE = 8,

            /// <summary>
            /// In a Maximize button.
            /// </summary>
            HTMAXBUTTON = 9,

            /// <summary>
            /// In a Maximize button.
            /// </summary>
            HTZOOM = 9,

            /// <summary>
            /// In the left border of a resizable window (the user can click the mouse to resize the window horizontally).
            /// </summary>
            HTLEFT = 10,

            /// <summary>
            /// In the right border of a resizable window (the user can click the mouse to resize the window horizontally).
            /// </summary>
            HTRIGHT = 11,

            /// <summary>
            /// In the upper-horizontal border of a window.
            /// </summary>
            HTTOP = 12,

            /// <summary>
            /// In the upper-left corner of a window border.
            /// </summary>
            HTTOPLEFT = 13,

            /// <summary>
            /// In the upper-right corner of a window border.
            /// </summary>
            HTTOPRIGHT = 14,

            /// <summary>
            /// In the lower-horizontal border of a resizable window (the user can click the mouse to resize the window vertically).
            /// </summary>
            HTBOTTOM = 15,

            /// <summary>
            /// In the lower-left corner of a border of a resizable window (the user can click the mouse to resize the window diagonally).
            /// </summary>
            HTBOTTOMLEFT = 16,

            /// <summary>
            /// In the lower-right corner of a border of a resizable window (the user can click the mouse to resize the window diagonally).
            /// </summary>
            HTBOTTOMRIGHT = 17,

            /// <summary>
            /// In the border of a window that does not have a sizing border.
            /// </summary>
            HTBORDER = 18,

            /// <summary>
            /// In a Close button.
            /// </summary>
            HTCLOSE = 20,

            /// <summary>
            /// In a Help button.
            /// </summary>
            HTHELP = 21,
        }

        private const int m_Gap = 4;
        private NCHITTESTFLAGS HitTest(Point pt)
        {
            NCHITTESTFLAGS ht = NCHITTESTFLAGS.HTCLIENT;
            if (Rectangle.FromLTRB(m_Gap, 0, Width - m_Gap, m_Gap).Contains(pt))
                ht = NCHITTESTFLAGS.HTTOP;
            else if (Rectangle.FromLTRB(m_Gap, Height - m_Gap, Width - m_Gap, Height).Contains(pt))
                ht = NCHITTESTFLAGS.HTBOTTOM;
            else if (Rectangle.FromLTRB(0, 0, m_Gap, m_Gap).Contains(pt))
                ht = NCHITTESTFLAGS.HTBOTTOMLEFT;
            //...... 생략
            return ht;
        }

        /*
              // DefWndProc를 override
        protected override void DefWndProc(ref Message message)
        {
            base.DefWndProc(ref message);
            if (message.Msg == (int)Msg.WM_NCHITTEST)
            {
                if ((int)message.Result == (int)NCHITTESTFLAGS.HTCLIENT)
                {
                    message.Result = (IntPtr)((int)HitTest(new Point((int)message.LParam)));
                }
            }
        }
        protected override void WndProc(ref Message m)
        {
            switch (m.Msg)
            {
                case 0x84:
                    base.WndProc(ref m);
                    if ((int)m.Result == 0x1)
                        m.Result = (IntPtr)0x2;
                    return;
            }

            base.WndProc(ref m);
        }
         */
        #endregion
        public enum FormStatus{Show,Hide,Close}

        public delegate void CurrentLyricsEventHandler(string CurrentLyrics, double time);
        public event CurrentLyricsEventHandler CurrentLyrics;
        public delegate void FormStatusChangeEventHandler(FormStatus Status);
        public event FormStatusChangeEventHandler FormStatusChange;
        public delegate void UpdateLyricsIntervalChangeEventHandler(int Interval);
        public event UpdateLyricsIntervalChangeEventHandler UpdateLyricsIntervalChange;


        LyricParser lp;
        LyricsParser_1 lp1;
        Thread th;
        ParameterizedThreadStart pth;
        Dictionary<string, string> Lyrics_FloatSetting = new Dictionary<string, string>();
        void UpdateSetting()
        {
            try
            {
                string[] a = Utility.EtcUtility.SaveSetting(Lyrics_FloatSetting);
                System.IO.File.WriteAllLines(SettingPath, a);
            }catch{}
        }

        internal string SettingPath
        {
            get
            {
                if (!System.IO.Directory.Exists(Application.StartupPath + "\\Settings")) 
                    System.IO.Directory.CreateDirectory(Application.StartupPath + "\\Settings");
                //if (!System.IO.File.Exists(Application.StartupPath + "\\Settings\\frmLyricsDesktop.ini"))
                return Application.StartupPath + "\\Settings\\frmLyricsDesktop.ini";
            }
        }

        string _MusicPath; 
        public string MusicPath
        { get { return _MusicPath; } set { _MusicPath = value;if(value!=null||value !=""){
        this.Text = "싱크 가사 창 - "+value.Substring(value.LastIndexOf("\\")+1);}else{
        this.Text = "싱크 가사 창";}}}

        LyricsEz _lc;
        public LyricsEz Lyrics
        {
            get { return _lc; }
            set
            {
                LastTime = 0;
                _lc = new LyricsEz(value.Maker, value.Lyric, value.MD5Hash);
                if (Lyricscache != null) Lyricscache.SetLyrics(value);
                if (value.MD5Hash!=null) MD5Hash = value.MD5Hash;
                Lyric = value.Lyric;
                lblMaker.InvokeIfNeeded(()=>lblMaker.Text = "제작자: " + value.Maker_Name);

            }
        }


        object _Lyric;
        /// <summary>
        /// 싱크가사 입니다. (반드시 LyricsException 예외나 문자열 싱크가사만 넣어주세요!)
        /// </summary>
        public object Lyric
        {
            get { return _Lyric; }
            set
            {
                if (value != null)
                {
                   LastTime = 0;
                    _Lyric = value;
                    label1.InvokeIfNeeded(() => label1.Text = "(잠시만 기다려 주시거나\r\n\r\n재생 버튼을 눌러 주세요)");
                    LastLyrics = "(잠시만 기다려 주시거나\r\n\r\n재생 버튼을 눌러 주세요)";
                    LyricException ex=value as LyricException;
                    if(ex==null){
                    try { lp = new LyricParser(value as string); lblMaker.InvokeIfNeeded(() => 
                        lblMaker.Text = string.Format("제작자: {0}{1}", (lp.Author==null||lp.Author=="")?"":lp.Author+" 님 등록", (lp.LyricsMaker == null||lp.LyricsMaker == "") ? "" : ((lp.Author == null||lp.Author == "")?"":", ")+lp.LyricsMaker + " 님 수정"));
                        /*if (lp != null)lp.LrcCode = value; else lp = new LyricParser(value);*/ }
                    catch { }// { OffsetEnabled = true };

                    //lp1 = new LyricsParser_1(value);
                    /*if (th != null) th.Abort();
                    pth = new ParameterizedThreadStart(Lyrics_Change);
                    th = new Thread(pth); th.IsBackground = true;*/
                    //currentlrc = lp1.ReadLine();
                }else{label1.InvokeIfNeeded(() => label1.Text = string.Format("({0})",ex.Message));}
                    if(frl!=null)frl.Lyrics = Lyric as string;
                    frl.MusicPath = MusicPath;
                }
                else { /*throw new Exception("Lyric값은 Null일수 없습니다.");*/
                    lp = new LyricParser(""); label1.InvokeIfNeeded(() => label1.Text = "(싱크 가사가 없습니다.)");
                }
                this.InvokeIfNeeded(() => this.Refresh());
            }
        }

        public void LyricsFailedException(LyricException ex)
        {
            //MessageBox.Show(ex.Message);
            LoggingUtility.Logging(ex, "싱크가사 가져오는 중 예외 발생");
            lblMaker.InvokeIfNeeded(()=>lblMaker.Text = "제작자: ");

            if(경고메세지표시ToolStripMenuItem.Checked)
                if(Lyric==null||Lyric as string==null||Lyric as string=="")
                    label1.InvokeIfNeeded(() => label1.Text = "("+ex.Message+")");
        }

        LyricAlsong _AlsongLyric;
        public LyricAlsong LyricAlsong
        {
            get { return _AlsongLyric; }
            set
            {
                if (value != null)
                {
                    LastTime = 0;
                    _AlsongLyric = value;
                    label1.InvokeIfNeeded(() => label1.Text = "(잠시만 기다려 주세요)");
                    try { lp = new LyricParser(_AlsongLyric.GetLyric()); lblMaker.InvokeIfNeeded(() => lblMaker.Text = "제작자: " + lp.LyricsMaker);
                        /*if (lp != null)lp.LrcCode = value; else lp = new LyricParser(value);*/ }
                    catch { }// { OffsetEnabled = true };

                    //lp1 = new LyricsParser_1(value);
                    /*if (th != null) th.Abort();
                    pth = new ParameterizedThreadStart(Lyrics_Change);
                    th = new Thread(pth); th.IsBackground = true;*/
                    //currentlrc = lp1.ReadLine();
                }
                else { /*throw new Exception("Lyric값은 Null일수 없습니다.");*/
                    lp = new LyricParser(""); label1.InvokeIfNeeded(() => label1.Text = "(싱크 가사가 없습니다.)");
                }
            }
        }
        public LyricsCache Lyricscache;
        public string MD5Hash;
        public uint MaxMilliSecond;
        public uint Offset;
        public frmLyricsDesktop()
        {
            InitializeComponent();
            this.SetStyle(ControlStyles.SupportsTransparentBackColor, true);
            //timer1.Start();
            MoveEventHandler = new OnMoveEvent(MoveEventHandler_Body);
            m_PrevPoint = new Point();
            frl.LyricsInputComplete += new frmLyrics.LyricsInputCompleteEventHandler(frl_LyricsInputComplete);
        }
        public frmLyricsDesktop(LyricsEz Lyric, uint MaxMillisecond)
        {
            InitializeComponent();
            this.SetStyle(ControlStyles.SupportsTransparentBackColor, true);
            //timer1.Start();
            MoveEventHandler = new OnMoveEvent(MoveEventHandler_Body);
            m_PrevPoint = new Point();
            this.Location = new System.Drawing.Point(SystemInformation.PrimaryMonitorSize.Width - this.Width - 10, SystemInformation.PrimaryMonitorSize.Height - this.Height - 10); 
            frl.LyricsInputComplete += new frmLyrics.LyricsInputCompleteEventHandler(frl_LyricsInputComplete);
            this.Lyric = Lyric.Lyric;
            this.MaxMilliSecond = MaxMillisecond;
        }

        public void Start() { timer1.Start(); }
        public void Stop() { timer1.Stop(); }

        private void frmLyrics_Load(object sender, EventArgs e)
        {
            Dictionary<string, string> Lyrics_FloatSetting = new Dictionary<string,string>();

            try { if (!System.IO.File.Exists(SettingPath))return;
            string[] Path = System.IO.File.ReadAllLines(SettingPath);
            재생컨트롤원래대로ToolStripMenuItem_Click(null, null);
            Lyrics_FloatSetting = Utility.EtcUtility.LoadSetting(Path); } catch {return; }
            ResizeLock=true;
            try {if (Lyrics_FloatSetting.ContainsKey("TransparentBackgroundValue"))trackBar1.Value = int.Parse(Lyrics_FloatSetting["TransparentBackgroundValue"]); }catch {this.Opacity = 0.9; }
            try {if (Lyrics_FloatSetting.ContainsKey("ShowTitleBar"))타이틀바숨기기ToolStripMenuItem.Checked = bool.Parse(Lyrics_FloatSetting["ShowTitleBar"]);}catch {}
            try {if (Lyrics_FloatSetting.ContainsKey("TextLocation"))cb가사위치.SelectedIndex = Convert.ToInt32(Lyrics_FloatSetting["TextLocation"]);}catch {cb가사위치.SelectedIndex = 4;}
            try
            {
                if (Lyrics_FloatSetting.ContainsKey("LyricsDesktopFormSize"))
                {
                    string[] a = Lyrics_FloatSetting["LyricsDesktopFormSize"].Split(',');
                    int X = int.Parse(a[0]); int Y = int.Parse(a[1]);
                    this.Size = b = new Size(X, Y);
                }
            }
            catch { /*this.Size =b= new Size(563, 188);*/ }
            try
            {
                if (Lyrics_FloatSetting.ContainsKey("LyricsDesktopFormLocation"))
                {
                    string[] a = Lyrics_FloatSetting["LyricsDesktopFormLocation"].Split(',');
                    int X = int.Parse(a[0]) > SystemInformation.PrimaryMonitorSize.Width ?
                        SystemInformation.PrimaryMonitorSize.Width - this.Size.Width : int.Parse(a[0]);
                    int Y = int.Parse(a[1]) > SystemInformation.PrimaryMonitorSize.Height ?
                        SystemInformation.PrimaryMonitorSize.Height - this.Size.Height : int.Parse(a[1]);
                    this.Location = Loc = new Point(X, Y);
                }
            } 
            catch { this.Location = Loc = new System.Drawing.Point(SystemInformation.PrimaryMonitorSize.Width - this.Width - 10, SystemInformation.PrimaryMonitorSize.Height - this.Height - 10); }
            try
            {
                if (Lyrics_FloatSetting.ContainsKey("IsFormFullScreenLyricsFloatForm") && bool.Parse(Lyrics_FloatSetting["IsFormFullScreenLyricsFloatForm"]))
                {panel2_DoubleClick(null, null);}
            }catch { }
            try
            {
                if (Lyrics_FloatSetting.ContainsKey("BackgroundColor"))
                {
                    string[] a = Lyrics_FloatSetting["BackgroundColor"].Split(',');
                    int A = int.Parse(a[0]); int R = int.Parse(a[1]); int G = int.Parse(a[2]); int B = int.Parse(a[3]);
                    this.BackColor = Color.FromArgb(A, R, G, B);
                }
                //this.btnPlay.FlatAppearance.BorderColor = this.btnPause.FlatAppearance.BorderColor = this.btnPlay.FlatAppearance.BorderColor = this.BackColor;
            }
            catch { }
            try
            {
                if (Lyrics_FloatSetting.ContainsKey("FontColor"))
                {
                    string[] a = Lyrics_FloatSetting["FontColor"].Split(',');
                    int A = int.Parse(a[0]); int R = int.Parse(a[1]); int G = int.Parse(a[2]); int B = int.Parse(a[3]);
                    this.label1.ForeColor = lblMaker.ForeColor = colorDialog2.Color = Color.FromArgb(A, R, G, B);
                }
            }
            catch { }

            FontFamily ff = null;
            try
            {
                if (Lyrics_FloatSetting.ContainsKey("FontFamily"))
                {
                    byte[] tmpff = Utility.EtcUtility.ConvertStringToByteArray(Lyrics_FloatSetting["FontFamily"], false);

                    System.IO.MemoryStream ms = new System.IO.MemoryStream(tmpff);
                    var dump = new Polenter.Serialization.SharpSerializer();
                    ff = dump.Deserialize(ms) as FontFamily;
                    ms.Dispose();
                }
            }
            catch {  }
            try
            {
                if (Lyrics_FloatSetting.ContainsKey("Font"))
                {
                    byte[] a = Utility.EtcUtility.ConvertStringToByteArray(Lyrics_FloatSetting["Font"], false);
                    Font f = Utility.EtcUtility.SerializableOpen(a) as Font;
                    if (f != null && ff != null) label1.Font = new Font(ff,  f.Size, f.Style);
                    if (f != null) label1.Font = f;
                    else if(ff!=null) label1.Font = new Font(ff, label1.Font.Size, label1.Font.Style);
                    else throw new Exception();
                }
            }
            catch { }
            try
            {
                if (Lyrics_FloatSetting.ContainsKey("PlayControlLocation"))
                {

                    string[] a = Lyrics_FloatSetting["PlayControlLocation"].Split(',');
                    int X = int.Parse(a[0]); int Y = int.Parse(a[1]);
                    playControl1.Location = new Point(X, Y);
                }
            }catch {playControl1.Location = new Point(553, 26);}//playControl1.Location = new Point(this.Width-playControl1.Width, menuStrip1.Height);}//this.playControl1.Width,menuStrip1.Height);}
            try {if(Lyrics_FloatSetting.ContainsKey("IsTransparentBackground")) checkBox1.Checked = bool.Parse(Lyrics_FloatSetting["IsTransparentBackground"]);checkBox1_CheckedChanged(null,null); }catch { }
            try {if(Lyrics_FloatSetting.ContainsKey("IsShowTaskbar"))작업표시줄에표시ToolStripMenuItem.Checked = bool.Parse(Lyrics_FloatSetting["IsShowTaskbar"]);}catch {}
            try {if(Lyrics_FloatSetting.ContainsKey("ShowTitleBar"))타이틀바숨기기ToolStripMenuItem.Checked = bool.Parse(Lyrics_FloatSetting["ShowTitleBar"]);}catch {}
            try {if(Lyrics_FloatSetting.ContainsKey("LyricClickThru"))가사클릭통과ToolStripMenuItem.Checked = bool.Parse(Lyrics_FloatSetting["LyricClickThru"]);}catch {}
            try {if(Lyrics_FloatSetting.ContainsKey("LyricLeftClickThru"))우클릭통과ToolStripMenuItem.Checked = bool.Parse(Lyrics_FloatSetting["LyricLeftClickThru"]);}catch {}


            try {if(Lyrics_FloatSetting.ContainsKey("IsTopMostLyricsFloatFormAutoUpdate"))자동업데이트ToolStripMenuItem.Checked = bool.Parse(Lyrics_FloatSetting["IsTopMostLyricsFloatFormAutoUpdate"]); }catch { }
            try {if(Lyrics_FloatSetting.ContainsKey("IsHideJapanese"))일본어숨기기숨덕용ToolStripMenuItem.Checked = bool.Parse(Lyrics_FloatSetting["IsHideJapanese"]); }catch { }
            try {if(Lyrics_FloatSetting.ContainsKey("IsExcludeOneLine"))한줄일땐제외ToolStripMenuItem.Checked = bool.Parse(Lyrics_FloatSetting["IsExcludeOneLine"]); }catch { }
            try
            {
                if (Lyrics_FloatSetting.ContainsKey("ExcludeLineValue"))
                {
                    string[] a = Lyrics_FloatSetting["ExcludeLineValue"].Split(new string[] { "##" }, StringSplitOptions.RemoveEmptyEntries);
                    Dictionary<string, string> b = HS_CSharpUtility.Utility.EtcUtility.LoadSetting(a);
                    Dictionary<int, int[]> Value = new Dictionary<int, int[]>();
                    foreach (string c in b.Keys)
                    {
                        string[] tmp = b[c].Split(',');
                        int[] tmp1 = new int[tmp.Length];
                        for (int i = 0; i < tmp1.Length; i++) tmp1[i] = Convert.ToInt32(tmp[i]);
                        Value.Add(Convert.ToInt32(c), tmp1);
                    }
                    LyricsLine = Value;
                }
            }
            catch{}
            try { if(Lyrics_FloatSetting.ContainsKey("IsShowWarnMessage"))가사줄설정켜기ToolStripMenuItem.Checked = bool.Parse(Lyrics_FloatSetting["IsUseExcludeLineValue"]); }catch { }
            try { if(Lyrics_FloatSetting.ContainsKey("IsShowWarnMessage"))경고메세지표시ToolStripMenuItem.Checked = bool.Parse(Lyrics_FloatSetting["IsShowWarnMessage"]); }catch { }
            try { if(Lyrics_FloatSetting.ContainsKey("IsPlayControlTransparent"))재생컨트롤투명화영향안받기ToolStripMenuItem.Checked = bool.Parse(Lyrics_FloatSetting["IsPlayControlTransparent"]);}catch{}
            try { if (Lyrics_FloatSetting.ContainsKey("IsTopMostLyricsFloatForm")) checkBox2.Checked = this.TopMost=bool.Parse(Lyrics_FloatSetting["IsTopMostLyricsFloatForm"]); }catch { }
            try { FormStatusChange(frmLyricsDesktop.FormStatus.Show); } catch { }
            this.Lyrics_FloatSetting = Lyrics_FloatSetting;
            UpdateSetting();

            contextMenuStrip1.ImageScalingSize = new Size(16, 16);
            재생컨트롤투명화영향안받기ToolStripMenuItem_CheckedChanged(null, null);
        }


        //public void Start() { StartT = true; } //th.Start(); }
        //public void Pause() { StartT = false; } //timer1.Stop(); th.Suspend(); }
        //void Stop() {}

        string currentlrc;
        void Lyrics_Change(object o)
        {
            
        }

        int _Uptime;
        public int UpdateTime { get { return _Uptime; } set { _Uptime = value; UpdateLyricsIntervalChange(value); } }

        bool StartT=true;
        private void timer1_Tick(object sender, EventArgs e)
        {
            if (자동업데이트ToolStripMenuItem.Checked) this.TopMost = checkBox2.Checked;
            if (StartT)
            {
                Offset = Offset>=uint.MaxValue?0:Offset++;
                if (lp1 != null&&lp1.currentTime == Offset)
                {
                    
                    currentlrc = lp1.ReadLine();
                    if (CurrentLyrics != null) CurrentLyrics(currentlrc, Offset);
                    if (Lyric != null) label1.Text = currentlrc;
                    else label1.Text = IsShowWarnMessage?"(싱크 가사를 사용할 수 없습니다.)":"";
                }
            }
        }

        bool Norebang = false;

        uint NextOffset;
        string Nextlrc;
        bool JapaneseHide = false;
        bool Ignore1Line_JapaneseHide = true;
        double LastTime;
        string LastLyrics;
        string LastText;
        public void GetLyric(double Time,bool ForceUpdate = false)
        {
            LastTime = Time;
            if (lp==null){label1.InvokeIfNeeded(() => label1.Text ="");return;}
            else lp.Refresh(Time);
            currentlrc = lp.CurrentLyrics;
            if(LastLyrics!=currentlrc||ForceUpdate)LastLyrics = currentlrc;
            //else if(currentlrc==null){if(LastText!= ""&&LastText!=null)label1.InvokeIfNeeded(() => label1.Text ="");return;}
            else return;//label1.InvokeIfNeeded(() => label1.Text ="");
            if (CurrentLyrics != null) CurrentLyrics(currentlrc, Time);

            List<string> a = null;
            string[] b = null;
            try{if(currentlrc!=null&&currentlrc!="")a = new List<string>(HS_CSharpUtility.Utility.StringUtility.ConvertStringToArray(currentlrc, null));}catch{}

            if (IsUseExcludeLineValue && (LyricsLine != null && LyricsLine.Count > 0) && (a != null && LyricsLine.ContainsKey(a.Count)))
            {
                try
                {
                    int[] cc = LyricsLine[a.Count];
                    Array.Sort(cc);
                    for (int i = cc.Length - 1; i >= 0; i--)if (cc[i] <= a.Count&&cc[i]>0) a.RemoveAt(cc[i] - 1);
                    currentlrc = a.Count == 0 ? IsShowWarnMessage?"(가사 설정에 의하여 가사 표시를 중지했습니다.)":"" : HS_CSharpUtility.Utility.StringUtility.ConvertArrayToString(a.ToArray(), true);
                }
                catch{}
            }
            if (JapaneseHide && (currentlrc != null && currentlrc!=""))
            {
                if(Ignore1Line_JapaneseHide&&a.Count<=1)goto Exit;
                b = HSWordFiltering.ExcludeLyricsLine(a.ToArray(), HSWordFiltering.LyricsLanguage.Japanese);
                currentlrc = b.Length == 0 ? currentlrc=IsShowWarnMessage?"(가사 설정에 의하여 가사 표시를 중지했습니다.)":"": HS_CSharpUtility.Utility.StringUtility.ConvertArrayToString(b, true);
            Exit:
                if(b!=null)Array.Clear(b, 0, b.Length);
            }
            if (Lyric != null) label1.InvokeIfNeeded(() => label1.Text = currentlrc);
            else label1.InvokeIfNeeded(() => label1.Text = IsShowWarnMessage?"(싱크 가사를 사용할 수 없습니다.)":null);
            /*
            NextOffset = (uint)lp1.tlrc[lp1.mark + 1].ms;
            if (lp1.currentTime+10 >= Offset&&lp1.currentTime+10 >= Offset)
            {
                currentlrc = lp1.ReadLine();
                if (CurrentLyrics != null) CurrentLyrics(currentlrc, Offset);
                if (Lyric != null) label1.InvokeIfNeeded(() => label1.Text = currentlrc);
                else label1.InvokeIfNeeded(() => label1.Text = "(싱크 가사를 사용할 수 없습니다.)");
            }*/
            LastText = label1.Text;
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            this.Hide();
            if(FirstCloseMessage){MessageBox.Show("싱크가사창 다시 띄우기는 프로그램 메인창에서\n[가사(L)]->[바탕화면 싱크 가사창 띄우기]를 누르시면 됩니다.", "HS 플레이어 바탕화면 싱크 가사창 도움말", MessageBoxButtons.OK, MessageBoxIcon.Information);FirstCloseMessage=false;}
        }

        bool FirstCloseMessage = true;
        bool FormClose;
        internal HS_Audio.Lyrics.frmLyricsDesktop.FormStatus Status = FormStatus.Hide;
        private void frmLyrics_Float_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = !FormClose;
            if (FormClose) { Status = FormStatus.Close;  try { FormStatusChange(frmLyricsDesktop.FormStatus.Close); } catch { } }
            else { this.Hide(); try {  Status = FormStatus.Hide;FormStatusChange(frmLyricsDesktop.FormStatus.Hide); } catch { } this.Hide(); }
            if(FirstCloseMessage){MessageBox.Show("싱크가사창 다시 띄우기는 프로그램 메인창에서\n[가사(L)]->[바탕화면 싱크 가사창 띄우기]를 누르시면 됩니다.", "HS 플레이어 바탕화면 싱크 가사창 도움말", MessageBoxButtons.OK, MessageBoxIcon.Information);FirstCloseMessage=false;}
            Stop();
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            가사창투명ToolStripMenuItem.Checked = checkBox1.Checked;
            if (checkBox1.Checked) { this.TransparencyKey = this.BackColor;
            if(재생컨트롤투명화영향안받기ToolStripMenuItem.Checked)playControl1.BackColor=Color.Green;}
            else { this.TransparencyKey = System.Drawing.Color.Empty; playControl1.BackColor = this.BackColor; }
            try { Lyrics_FloatSetting["IsTransparentBackground"] = checkBox1.Checked.ToString(); }
            catch { Lyrics_FloatSetting.Add("IsTransparentBackground", checkBox1.Checked.ToString()); }
            UpdateSetting();
        }

        private void trackBar1_ValueChanged(object sender, EventArgs e)
        {
            this.Opacity = trackBar1.Value * 0.01f;
            try { Lyrics_FloatSetting["TransparentBackgroundValue"] = trackBar1.Value.ToString(); }
            catch { Lyrics_FloatSetting.Add("TransparentBackgroundValue", trackBar1.Value.ToString()); }
            UpdateSetting();
        }

        
        void frl_LyricsInputComplete(LyricClass Lyric)
        {
            this.Lyric = Lyric.ToString();
            if (Lyricscache != null&&Lyric!=null&&(Lyric.PureLyric!=null||Lyric.PureLyric!="")) Lyricscache.SetLyrics(Lyric.ToString(), MD5Hash);
        }

        frmLyrics frl = new frmLyrics();
        private void 싱크가사직접입력ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frl.Lyrics = Lyric as string;
            frl.MusicPath = MusicPath;
            if(frl.자동업데이트ToolStripMenuItem.Checked)
            MessageBox.Show("반드시 가사를 수정하고 나면 적용버튼을 눌러서 저장해주세요!!\n노래가 바뀌게되면 적용안한 가사들은 변경된 노래의 가사로 바뀝니다.\n\n"+
            "만약 원하지 않으신다면 [설정(T)]에서 [자동 업데이트]를 체크 해제해주시기 바랍니다.","싱크가사 직접 입력 경고!!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            else MessageBox.Show("가사를 수정했는데 만약 노래가 바뀌고나서 적용버튼을 누르면\n바뀐 노래의 싱크가사가 지금 수정하신 가사로 적용됩니다.\n\n"+
            "만약 수정할때마다 적용하고 싶으시면 [설정(T)]에서 [자동 적용]을 체크하여주시기 바랍니다.","싱크가사 직접 입력 경고!!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            frl.Show();
            frl.FormClosed = false;
            frl.openFileDialog1.InitialDirectory = System.IO.Path.GetDirectoryName(MusicPath);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Hide();
            if(FirstCloseMessage){MessageBox.Show("싱크가사창 다시 띄우기는 프로그램 메인창에서\n[가사(L)]->[바탕화면 싱크 가사창 띄우기]를 누르시면 됩니다.", "HS 플레이어 바탕화면 싱크 가사창 도움말", MessageBoxButtons.OK, MessageBoxIcon.Information);FirstCloseMessage=false;}
            try { FormStatusChange(frmLyricsDesktop.FormStatus.Hide); }catch { }
        }

        private void checkBox2_CheckedChanged(object sender, EventArgs e)
        {
            this.TopMost = checkBox2.Checked;
            try { Lyrics_FloatSetting["IsTopMostLyricsFloatForm"] = checkBox2.Checked.ToString(); }
            catch { Lyrics_FloatSetting.Add("IsTopMostLyricsFloatForm", checkBox2.Checked.ToString()); }
            UpdateSetting();
        }

        private void frmLyrics_Float_MouseDown(object sender, MouseEventArgs e)
        {
            //pointMouseOffset = new Point(-e.X, -e.Y);
        }

        private void frmLyrics_Float_MouseMove(object sender, MouseEventArgs e)
        {
            /*
            if (!(sender is System.Windows.Forms.Control)) return;

            if (e.Button == MouseButtons.Left)
            {

                Control objControl = (Control)sender;
                ScrollableControl objScrollControl = (ScrollableControl)objControl.Parent;
                Point pointMouse = Control.MousePosition;

                if (sender.Equals(this))
                {
                    pointMouse.Offset(pointMouseOffset.X, pointMouseOffset.Y);
                }
                else
                {
                    pointMouse.Offset(pointMouseOffset.X - objScrollControl.DockPadding.Left - objControl.Left
                      , pointMouseOffset.Y - objScrollControl.DockPadding.Top - objControl.Top);
                }

                this.Location = pointMouse;
            }*/
        }

        #region 이동
        private Point pointMouseOffset = new Point(0, 0);

        Point menuStrip1MouseDownLocation;
        MouseButtons menuStrip1MouseButtons = System.Windows.Forms.MouseButtons.None;
        private void menuStrip1_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Left)
            {
                /*
                this.MoveEventHandler(
                    JPointCalculator.Add(
                        this.Location,
                        JPointCalculator.Subtract(Cursor.Position, m_PrevPoint)));
                JPointCalculator.Set(Cursor.Position, ref m_PrevPoint);*/
                this.Left += e.X - menuStrip1MouseDownLocation.X;
                this.Top += e.Y - menuStrip1MouseDownLocation.Y;
             }
        }
        private void menuStrip1_MouseDown(object sender, MouseEventArgs e)
        {
            //JPointCalculator.Set(Cursor.Position, ref m_PrevPoint);
            menuStrip1MouseButtons = System.Windows.Forms.MouseButtons.Left;
            menuStrip1MouseDownLocation = e.Location;
            this.Cursor = Cursors.SizeAll;
        }
        private void menuStrip1_MouseUp(object sender, MouseEventArgs e)
        {
            //JPointCalculator.Set(new Point(0, 0), ref m_PrevPoint);
            menuStrip1MouseButtons = System.Windows.Forms.MouseButtons.None;
            this.Cursor = Cursors.Default;
        }
        private void menuStrip1_MouseLeave(object sender, EventArgs e){menuStrip1_MouseUp(null,null);}



        Point panel2MouseDownLocation;
        MouseButtons panel2MouseButtons = System.Windows.Forms.MouseButtons.None;
        private void panel2_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Left)
            {
                this.Left += e.X - menuStrip1MouseDownLocation.X;
                this.Top += e.Y - menuStrip1MouseDownLocation.Y;
            }
        }
        private void panel2_MouseUp(object sender, MouseEventArgs e)
        {
            menuStrip1MouseButtons = System.Windows.Forms.MouseButtons.None;
            this.Cursor = Cursors.Default;
        }
        private void panel2_MouseDown(object sender, MouseEventArgs e)
        {
            menuStrip1MouseButtons = System.Windows.Forms.MouseButtons.Left;
            menuStrip1MouseDownLocation = e.Location;
            this.Cursor = Cursors.SizeAll;
        }
        private void panel2_MouseLeave(object sender, EventArgs e){panel2_MouseUp(null, null);}

        
        Point label1MouseDownLocation;
        MouseButtons label1MouseButtons = System.Windows.Forms.MouseButtons.None;
        private void label1_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Left)
            {
                this.Left += e.X - menuStrip1MouseDownLocation.X;
                this.Top += e.Y - menuStrip1MouseDownLocation.Y;
            }
        }
        private void label1_MouseUp(object sender, MouseEventArgs e)
        {
            menuStrip1MouseButtons = System.Windows.Forms.MouseButtons.None;
            this.Cursor = Cursors.Default;
        }
        private void label1_MouseDown(object sender, MouseEventArgs e)
        {
            menuStrip1MouseButtons = System.Windows.Forms.MouseButtons.Left;
            menuStrip1MouseDownLocation = e.Location;
            this.Cursor = Cursors.SizeAll;
        }
        private void label1_MouseLeave(object sender, EventArgs e){label1_MouseUp(null,null);}
        #endregion

        private void vScrollBar1_ValueChanged(object sender, EventArgs e)
        {
            label1.Font = new Font(label1.Font.FontFamily, (float)vScrollBar1.Value, label1.Font.Style, label1.Font.Unit);
            byte[] a = Utility.EtcUtility.SerializableSaveToByteArray(label1.Font);
            try { Lyrics_FloatSetting["Font"] = Utility.EtcUtility.ConvertByteArrayTostring(a); }
            catch { Lyrics_FloatSetting.Add("Font", Utility.EtcUtility.ConvertByteArrayTostring(a)); }
            UpdateSetting();
        }

        private void 폰트설정ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            fontDialog1.Font = label1.Font;
            fontDialog1.Color = label1.ForeColor;
            if (fontDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {label1.Font = fontDialog1.Font;label1.ForeColor = fontDialog1.Color;

            try{Lyrics_FloatSetting["\r\n//아래의 값 2개는 절때 건드리지 말것!!"] = null;}
            catch{Lyrics_FloatSetting.Add("\r\n//아래의 값 2개는 절때 건드리지 말것!!", null);}

                /*
            var dump = new Polenter.Serialization.SharpSerializer();
            System.IO.MemoryStream ms = new System.IO.MemoryStream();
            dump.Serialize(fontDialog1.Font.FontFamily, ms);
            byte[] a = ms.ToArray();ms.Dispose();

            try { Lyrics_FloatSetting["FontFamily"] = Utility.EtcUtility.ConvertByteArrayTostring(a); }
            catch { Lyrics_FloatSetting.Add("FontFamily", Utility.EtcUtility.ConvertByteArrayTostring(a)); }
                */
            try { Lyrics_FloatSetting["//"] = label1.Font.ToString(); }
            catch { Lyrics_FloatSetting.Add("//", label1.Font.ToString());}
                
            byte[] a = Utility.EtcUtility.SerializableSaveToByteArray(label1.Font);
            try { Lyrics_FloatSetting["Font"] = Utility.EtcUtility.ConvertByteArrayTostring(a); }
            catch { Lyrics_FloatSetting.Add("Font", Utility.EtcUtility.ConvertByteArrayTostring(a)); }}
            UpdateSetting();
        }

        private void fontDialog1_Apply(object sender, EventArgs e)
        {
            label1.Font = fontDialog1.Font;
            label1.ForeColor = fontDialog1.Color;
            byte[] a = Utility.EtcUtility.SerializableSaveToByteArray(label1.Font);

            try { Lyrics_FloatSetting["Font"] = Utility.EtcUtility.ConvertByteArrayTostring(a); }
            catch {Lyrics_FloatSetting.Add("Font", Utility.EtcUtility.ConvertByteArrayTostring(a)); }
            try { Lyrics_FloatSetting["FontColor"] = label1.ForeColor.A + "," + label1.ForeColor.R + "," + label1.ForeColor.G + "," + label1.ForeColor.B; }
            catch { Lyrics_FloatSetting.Add("FontColor", label1.ForeColor.A + "," + label1.ForeColor.R + "," + label1.ForeColor.G + "," + label1.ForeColor.B); }
            UpdateSetting();
        }

        private void 배경색깔바꾸기ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            colorDialog1.Color = this.BackColor;
            if (DialogResult.OK == colorDialog1.ShowDialog())
            {
                this.BackColor = colorDialog1.Color;
                if(checkBox1.Checked)this.TransparencyKey = this.BackColor;
                //this.btnPlay.FlatAppearance.BorderColor=this.btnPause.FlatAppearance.BorderColor=this.btnPlay.FlatAppearance.BorderColor=this.BackColor;
                try { Lyrics_FloatSetting["BackgroundColor"] = this.BackColor.A + "," + this.BackColor.R + "," + this.BackColor.G + "," + this.BackColor.B; }
                catch { Lyrics_FloatSetting.Add("BackgroundColor", this.BackColor.A + "," + this.BackColor.R + "," + this.BackColor.G + "," + this.BackColor.B); }
                UpdateSetting();
                //this.TransparencyKey = this.BackColor;
            }
        }

        private void contextMenuStrip1_Opening(object sender, System.ComponentModel.CancelEventArgs e)
        {
            맨위로설정ToolStripMenuItem.Checked = checkBox2.Checked;
            가사창투명ToolStripMenuItem.Checked = checkBox1.Checked;
        }

        bool ResizeLock;
        private void frmLyricsDesktop_Resize(object sender, EventArgs e)
        {
            if (!ResizeLock)
            {
                if (FullScreen)
                {
                    ResizeLock=true;
                    this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                    this.Width = Screen.PrimaryScreen.WorkingArea.Width;
                    this.Height = Screen.PrimaryScreen.WorkingArea.Height;
                    ResizeLock = false;
                    //Left = Top = 0;
                    //lblMaker.Location = new Point(1, 3);
                    label1.Size = new System.Drawing.Size(Width, this.Height - lblMaker.Height - 5);//label1.Height + lblMaker.Height + 3);
                    label1.Location = new Point(0, lblMaker.Height + 5);
                    //FullScreen = true;
                    타이틀바숨기기ToolStripMenuItem.Enabled = false;
                }
                else
                {
                    try{Lyrics_FloatSetting["LyricsDesktopFormSize"] = this.Width.ToString() + "," + this.Height.ToString();}
                    catch{ Lyrics_FloatSetting.Add("LyricsDesktopFormSize", this.Width.ToString() + "," + this.Height.ToString()); }
                    UpdateSetting();
                }
            }
        }


        private void frmLyricsDesktop_Move(object sender, EventArgs e)
        {
            try { Lyrics_FloatSetting["LyricsDesktopFormLocation"] = this.Location.X.ToString() + "," + this.Location.Y.ToString(); }
            catch { Lyrics_FloatSetting.Add("LyricsDesktopFormLocation", this.Location.X.ToString() + "," + this.Location.Y.ToString()); }
            UpdateSetting();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("정말 바탕화면 싱크가사창 설정을 재 설정 하시겠습니까?",
               "재 설정 경고", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
            {
                Lyrics_FloatSetting.Clear();
                UpdateSetting();
                frmLyricsDesktop lrcf = new frmLyricsDesktop();
                lrcf.Form1Instance = this.Form1Instance;
                lrcf.Lyric = this.Lyric;
                Form1Instance.lrcf = lrcf;
                Form1Instance.바탕화면싱크가사창띄우기ToolStripMenuItem_Click(null, null);
                this.Dispose();
                //panel1.Visible = false; panel1.Dispose();
                //for (int i = 0; i < this.Controls.Count; i++) this.Controls[i].Dispose();
                //InitializeComponent();
                //frmLyrics_Load(null, null);
            }
        }

        internal protected frmMain Form1Instance;

        internal void PlayingStatusChanged(HS_Audio.HSAudioHelper.PlayingStatus Status, int index)
        {
            if (Status == HS_Audio.HSAudioHelper.PlayingStatus.Stop) { GetLyric(0);}
        }

        FormBorderStyle a;
        Size b;
        Point Loc;
        bool FullScreen;
        internal void panel2_DoubleClick(object sender, EventArgs e)
        {
            //타이틀바숨기기ToolStripMenuItem.Checked;
            if (!FullScreen)
            {
                FullScreen = true;
                ResizeLock = true;
                a = this.FormBorderStyle; //b = new Rectangle(this.Top, this.Left, this.Width, this.Height);
                b = new Size(this.Width, this.Height);
                Loc = this.Location;
                this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                Width = Screen.PrimaryScreen.WorkingArea.Width;
                Height = Screen.PrimaryScreen.WorkingArea.Height;
                Left = Top = 0;
                lblMaker.Location = new Point(1, 3);
                menuStrip1.Visible = vScrollBar1.Visible = button1.Visible = panel3.Visible=false;//label2.Visible = trackBar1.Visible = checkBox1.Visible = checkBox2.Visible = button2.Visible = false;
                playControl1.Location = new Point(playControl1.Location.X + 21, playControl1.Location.Y);
                label1.Size = new System.Drawing.Size(label1.Width + vScrollBar1.Width, Screen.PrimaryScreen.WorkingArea.Height - lblMaker.Height - 5);//label1.Height + lblMaker.Height + 3);
                label1.Location = new Point(0, lblMaker.Height+5);
                타이틀바숨기기ToolStripMenuItem.Enabled = false;
                ResizeLock = false;
            }
            else
            {
                FullScreen = false;
                Color tmp = this.TransparencyKey;
                this.TransparencyKey = Color.Empty;
                this.FormBorderStyle = 타이틀바숨기기ToolStripMenuItem.Checked?System.Windows.Forms.FormBorderStyle.None:System.Windows.Forms.FormBorderStyle.SizableToolWindow;
                //else this.FormBorderStyle = a;
                /*this.Top = b.X; this.Left = b.Y;*/
                this.TransparencyKey = tmp;
                menuStrip1.Visible = vScrollBar1.Visible = button1.Visible = panel3.Visible = true;//label2.Visible = trackBar1.Visible = checkBox1.Visible =  vScrollBar1.Visible=button1.Visible=checkBox2.Visible = button2.Visible = true; 
                this.Location = new Point(Loc.X, Loc.Y);
                this.Width = b.Width; this.Height = b.Height;
                //playControl1.Location = new Point(playControl1.Location.X - 21, playControl1.Location.Y);
                lblMaker.Location = new Point(1, menuStrip1.Height + 3);
                label1.Size = new System.Drawing.Size(b.Width - vScrollBar1.Width, this.Height - menuStrip1.Height - SystemInformation.CaptionHeight-playControl1.Height);
                label1.Location = new Point(0, menuStrip1.Height + lblMaker.Height + 5);
                타이틀바숨기기ToolStripMenuItem.Enabled = true;
            }
            try { Lyrics_FloatSetting["IsFormFullScreenLyricsFloatForm"] = FullScreen.ToString(); }
            catch { Lyrics_FloatSetting.Add("IsFormFullScreenLyricsFloatForm", FullScreen.ToString()); }
            UpdateSetting();
        }

        Color TransparentColor = Color.Empty;

        private void 배경색깔이랑연결ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (배경색깔이랑연결ToolStripMenuItem.Checked)
            { checkBox1.Enabled = false; }
        }

        private void 가사창투명ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            checkBox1.Checked = 가사창투명ToolStripMenuItem.Checked;
        }

        private void 글자색지정ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            colorDialog2.Color = label1.ForeColor;
            if (colorDialog2.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                label1.ForeColor =lblMaker.ForeColor =colorDialog2.Color;
                try { Lyrics_FloatSetting["FontColor"] = label1.ForeColor.A + "," + label1.ForeColor.R + "," + label1.ForeColor.G + "," + label1.ForeColor.B; }
                catch { Lyrics_FloatSetting.Add("FontColor", label1.ForeColor.A + "," + label1.ForeColor.R + "," + label1.ForeColor.G + "," + label1.ForeColor.B); }
                UpdateSetting();
            }
        }

        private void 재생컨트롤투명화영향안받기ToolStripMenuItem_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox1.Checked)
            {
                if (재생컨트롤투명화영향안받기ToolStripMenuItem.Checked)
                { playControl1.BackColor = Color.Green; }
                else { playControl1.BackColor = Color.Transparent; }
            }
            else
            {
                if (재생컨트롤투명화영향안받기ToolStripMenuItem.Checked)
                { playControl1.BackColor = this.BackColor; }
                else { playControl1.BackColor = Color.Transparent; }
            }
            try { Lyrics_FloatSetting["IsPlayControlTransparent"] = 재생컨트롤투명화영향안받기ToolStripMenuItem.Checked.ToString(); }
            catch { Lyrics_FloatSetting.Add("IsPlayControlTransparent", 재생컨트롤투명화영향안받기ToolStripMenuItem.Checked.ToString()); }
            UpdateSetting();
        }

        private void 창닫기ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            if(FirstCloseMessage){MessageBox.Show("싱크가사창 다시 띄우기는 프로그램 메인창에서\n[가사(L)]->[바탕화면 싱크 가사창 띄우기]를 누르시면 됩니다.", "HS 플레이어 바탕화면 싱크 가사창 도움말", MessageBoxButtons.OK, MessageBoxIcon.Information);FirstCloseMessage=false;}
            try { FormStatusChange(frmLyricsDesktop.FormStatus.Hide); } catch { }
            Stop();
        }

        private void 작업표시줄에표시ToolStripMenuItem_CheckedChanged(object sender, EventArgs e)
        {
            this.ShowInTaskbar = 작업표시줄에표시ToolStripMenuItem.Checked;
            try { Lyrics_FloatSetting["IsShowTaskbar"] = this.ShowInTaskbar.ToString(); }
            catch { Lyrics_FloatSetting.Add("IsShowTaskbar", this.ShowInTaskbar.ToString()); }
        }

        private void toolStripTextBox1_TextChanged(object sender, EventArgs e)
        {
            if (toolStripTextBox1.Text == "" || toolStripTextBox1.Text == "0") { toolStripTextBox1.Text = "1"; }
            try { UpdateTime = int.Parse(toolStripTextBox1.Text); }catch{}
        }

        private void toolStripTextBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == Convert.ToChar(Keys.Enter)) { toolStripTextBox1_TextChanged(null, null); }
            if (!(Char.IsDigit(e.KeyChar)) && e.KeyChar != Convert.ToChar(Keys.Back))
            { e.Handled = true; }
        }

        private void 맨위로설정ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            checkBox2.Checked = 맨위로설정ToolStripMenuItem.Checked;
        }

        private void 자동업데이트ToolStripMenuItem_CheckedChanged(object sender, EventArgs e)
        {
            if (자동업데이트ToolStripMenuItem.Checked) timer1.Start();
            else timer1.Stop();
            try { Lyrics_FloatSetting["IsTopMostLyricsFloatFormAutoUpdate"] = 자동업데이트ToolStripMenuItem.Checked.ToString(); }
            catch { Lyrics_FloatSetting.Add("IsTopMostLyricsFloatFormAutoUpdate", 자동업데이트ToolStripMenuItem.Checked.ToString()); }
            UpdateSetting();
        }



        private void 재생컨트롤원래대로ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            playControl1.Location = new Point(this.Width - playControl1.Size.Width-38,menuStrip1.Height);
        }

        private void playControl1_PlayButtonPress(){try { Form1Instance.Play(); }catch{}}

        private void playControl1_StopButtonPress(){try { Form1Instance.Stop(); }catch{}}

        private void playControl1_PauseButtonPress(){try {Form1Instance.Pause(); }catch {} }

        private void playControl1_MoveCompleted(Point Location)
        {
            try { Lyrics_FloatSetting["PlayControlLocation"] = Location.X.ToString() + "," + Location.Y.ToString(); }
            catch { Lyrics_FloatSetting.Add("PlayControlLocation", Location.X.ToString() + "," + Location.Y.ToString()); }
            UpdateSetting();
        }

        private void 타이틀바숨기기ToolStripMenuItem_CheckedChanged(object sender, EventArgs e)
        {
            if (타이틀바숨기기ToolStripMenuItem.Checked){this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None; }
            else  { this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow; }

            try{Lyrics_FloatSetting["ShowTitleBar"] = 타이틀바숨기기ToolStripMenuItem.Checked.ToString();}
            catch {Lyrics_FloatSetting.Add("ShowTitleBar", 타이틀바숨기기ToolStripMenuItem.Checked.ToString()); }
            UpdateSetting();
        }

        private void cb가사위치_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (cb가사위치.SelectedIndex)
            {
                case 0: label1.TextAlign = ContentAlignment.TopCenter; break;
                case 1: label1.TextAlign = ContentAlignment.BottomCenter; break;
                case 2: label1.TextAlign = ContentAlignment.MiddleLeft; break;
                case 3: label1.TextAlign = ContentAlignment.MiddleRight; break;
                case 5: label1.TextAlign = ContentAlignment.TopLeft; break;
                case 6: label1.TextAlign = ContentAlignment.TopRight; break;
                case 7: label1.TextAlign = ContentAlignment.BottomLeft; break;
                case 8: label1.TextAlign = ContentAlignment.BottomRight; break;
                default: label1.TextAlign = ContentAlignment.MiddleCenter; break;
            } 
            try { Lyrics_FloatSetting["TextLocation"] = this.cb가사위치.SelectedIndex.ToString(); }
            catch { Lyrics_FloatSetting.Add("TextLocation", this.cb가사위치.SelectedIndex.ToString()); }
            UpdateSetting();
        }

        private void 일본어숨기기숨덕용ToolStripMenuItem_CheckedChanged(object sender, EventArgs e)
        {
            System.Diagnostics.Debug.WriteLine("일본어숨기기숨덕용ToolStripMenuItem_CheckedChanged 이벤트 발생함!!");
            System.Diagnostics.Trace.WriteLine("일본어숨기기숨덕용ToolStripMenuItem_CheckedChanged 이벤트 발생함!!");

            JapaneseHide = 일본어숨기기숨덕용ToolStripMenuItem.Checked;
            try { Lyrics_FloatSetting["IsHideJapanese"]=일본어숨기기숨덕용ToolStripMenuItem.Checked.ToString();}
            catch{Lyrics_FloatSetting.Add("IsHideJapanese", 일본어숨기기숨덕용ToolStripMenuItem.Checked.ToString());}
            UpdateSetting();
            GetLyric(LastTime, true);
        }

        private void 한줄일땐제외ToolStripMenuItem_CheckedChanged(object sender, EventArgs e)
        {
            Ignore1Line_JapaneseHide = 한줄일땐제외ToolStripMenuItem.Checked;
            try { Lyrics_FloatSetting["IsExcludeOneLine"]=한줄일땐제외ToolStripMenuItem.Checked.ToString();}
            catch{Lyrics_FloatSetting.Add("IsExcludeOneLine", 한줄일땐제외ToolStripMenuItem.Checked.ToString());}
            UpdateSetting();
            try{GetLyric(LastTime, true);}catch{}
        }

        Dictionary<int, int[]> LyricsLine = new Dictionary<int, int[]>();
        private void 가사줄설정ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmLyricsLineSetting fls = new frmLyricsLineSetting();
            fls.Complete += LyricsLine_Complete;
            fls.Value = LyricsLine;
            fls.ShowDialog();
        }
        void LyricsLine_Complete(frmLyricsLineSetting sender, Dictionary<int, int[]> Value)
        {
            System.Diagnostics.Debug.WriteLine("LyricsLine_Complete 이벤트 발생함!!");
            System.Diagnostics.Trace.WriteLine("LyricsLine_Complete 이벤트 발생함!!");
            LyricsLine = Value;

            Dictionary<string, string> Set = new Dictionary<string,string>();
            foreach(int a in Value.Keys)Set.Add(a.ToString(), HS_CSharpUtility.Utility.ArrayUtility.ConvertArrayToString(Value[a], ","));

            string[] b = HS_CSharpUtility.Utility.EtcUtility.SaveSetting(Set);

            try{Lyrics_FloatSetting["ExcludeLineValue"] = HS_CSharpUtility.Utility.StringUtility.ConvertArrayToString(b, "##");}
            catch{Lyrics_FloatSetting.Add("ExcludeLineValue",  HS_CSharpUtility.Utility.StringUtility.ConvertArrayToString(b, "##"));}

            UpdateSetting();
            try{GetLyric(LastTime, true);}catch{}
        }

        bool IsShowWarnMessage= true;
        private void 경고메세지표시ToolStripMenuItem_CheckedChanged(object sender, EventArgs e)
        {
            IsShowWarnMessage = 경고메세지표시ToolStripMenuItem.Checked;
            try { Lyrics_FloatSetting["IsShowWarnMessage"]=경고메세지표시ToolStripMenuItem.Checked.ToString();}
            catch{Lyrics_FloatSetting.Add("IsShowWarnMessage", 경고메세지표시ToolStripMenuItem.Checked.ToString());}
            UpdateSetting();
            try{GetLyric(LastTime, true);}catch{}
        }

        bool IsUseExcludeLineValue = true;
        private void 가사줄설정켜기ToolStripMenuItem_CheckedChanged(object sender, EventArgs e)
        {
            IsUseExcludeLineValue = 가사줄설정켜기ToolStripMenuItem.Checked;
            try { Lyrics_FloatSetting["IsUseExcludeLineValue"]=가사줄설정켜기ToolStripMenuItem.Checked.ToString();}
            catch{Lyrics_FloatSetting.Add("IsUseExcludeLineValue", 가사줄설정켜기ToolStripMenuItem.Checked.ToString());}
            UpdateSetting();
            try{GetLyric(LastTime, true);}catch{}
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void 가사클릭통과ToolStripMenuItem_CheckedChanged(object sender, EventArgs e)
        {
            if (가사클릭통과ToolStripMenuItem.Checked)
            {
                label1.MouseMove -= label1_MouseMove;
                label1.MouseDown -= label1_MouseDown;
                label1.DoubleClick -= panel2_DoubleClick;
            }
            else
            {
                label1.MouseMove += label1_MouseMove;
                label1.MouseDown += label1_MouseDown;
                label1.DoubleClick += panel2_DoubleClick;
            }

            if (Lyrics_FloatSetting.ContainsKey("LyricClickThru")) Lyrics_FloatSetting["LyricClickThru"] = 가사클릭통과ToolStripMenuItem.Checked.ToString();
            else Lyrics_FloatSetting.Add("LyricClickThru", 가사클릭통과ToolStripMenuItem.Checked.ToString());
            UpdateSetting();
        }

        private void 우클릭통과ToolStripMenuItem_CheckedChanged(object sender, EventArgs e)
        {
            panel1.ContextMenuStrip = 우클릭통과ToolStripMenuItem.Checked ? null : contextMenuStrip1;
            //this.panel2.ContextMenuStrip = this.contextMenuStrip1;

            if (Lyrics_FloatSetting.ContainsKey("LyricLeftClickThru")) Lyrics_FloatSetting["LyricLeftClickThru"] = 우클릭통과ToolStripMenuItem.Checked.ToString();
            else Lyrics_FloatSetting.Add("LyricLeftClickThru", 우클릭통과ToolStripMenuItem.Checked.ToString());
            UpdateSetting();
        }

    }
    public class JPointCalculator
    {
        public static Point Add(Point x, Point y)
        {
            return new Point(x.X + y.X, x.Y + y.Y);
        }

        public static void Add(Point x, Point y, ref Point dst)
        {
            dst.Offset(x.X + y.X, x.Y + y.Y);
        }

        public static Point Subtract(Point x, Point y)
        {
            return new Point(x.X - y.X, x.Y - y.Y);
        }

        public static void Subtract(Point x, Point y, ref Point dst)
        {
            dst.Offset(x.X - y.X, x.Y - y.Y);
        }

        public static void Set(Point x, ref Point dst)
        {
            dst.X = x.X;
            dst.Y = x.Y;
        }
    }
}
