﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Forms;

namespace HS_Audio.Lyrics
{
    public partial class frmLyricsCacheManager : Form
    {
        HS_Audio.Lyrics.LyricParser lp= new LyricParser();
        Dictionary<string, string> lrcCache;
        LyricsCache lc;
        string Orignal;
        public frmLyricsCacheManager(LyricsCache Lyrics)
        {
            InitializeComponent();
            this.lc = Lyrics;
            lrcCache = new Dictionary<string, string>(lc.lrcCache);
        }
        public frmLyricsCacheManager()
        {
            InitializeComponent();
            this.lc = new LyricsCache();
            lrcCache =new  Dictionary<string, string>(lc.lrcCache);
        }

        public string Lyrics
        {
            get { return lp.ToString(); }
            set
            {
                lp = new LyricParser(value);
                Orignal = value;
                lp.LrcCode = value;
                txtAlbum.Text = lp.Album;
                txtArtist.Text = lp.Artist;
                txtAuthor.Text = lp.Author;
                txtComment.Text = lp.Comment;
                numOffset.Value = lp.Offset;
                txtLength.Text = lp.Length;
                txtMaker.Text = lp.LyricsMaker;
                txtTitle.Text = lp.Title;
                textBox1.Text = lp.PurelrcCode;
            }
        }

        public delegate void LyricsInputCompleteEventHandler(string Lyrics);
        public event LyricsInputCompleteEventHandler LyricsInputComplete;

        private void button1_Click(object sender, EventArgs e)
        {
            if (!원본ToolStripMenuItem.Checked) textBox2.Text = lp.ToString();
            if (LyricsInputComplete != null)
            {
                if (전체보기ToolStripMenuItem.Checked) LyricsInputComplete(textBox2.Text);
                else { LyricsInputComplete(lp.ToString()); }
            }
        }

        bool FormClose;
        private void frmLyrics_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = !FormClose;
            this.Hide();
        }

        private void frmLyrics_Load(object sender, EventArgs e)
        {

        }

        private void openFileDialog1_FileOk(object sender, CancelEventArgs e)
        {
            this.Lyrics = System.IO.File.ReadAllText(openFileDialog1.FileName);
        }

        private void 싱크가사파일에서열기ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            openFileDialog1.ShowDialog();
        }

        private void txtTitle_TextChanged(object sender, EventArgs e) { lp.Title = txtTitle.Text; }

        private void txtArtist_TextChanged(object sender, EventArgs e) { lp.Artist = txtArtist.Text; }

        private void txtAlbum_TextChanged(object sender, EventArgs e){lp.Album=txtAlbum.Text;}

        private void txtAuthor_TextChanged(object sender, EventArgs e) { lp.Author = txtAuthor.Text; }

        private void txtMaker_TextChanged(object sender, EventArgs e) { lp.LyricsMaker = txtMaker.Text; }

        private void numOffset_ValueChanged(object sender, EventArgs e) { lp.Offset = (int)numOffset.Value; Change = true; }

        private void txtComment_TextChanged(object sender, EventArgs e) { lp.Comment = txtComment.Text; }

        private void textBox1_TextChanged(object sender, EventArgs e) { lp.PurelrcCode = textBox1.Text; }

        private void 전체보기ToolStripMenuItem_CheckedChanged(object sender, EventArgs e)
        {
            textBox2.Visible = 전체보기ToolStripMenuItem.Checked;
            if (전체보기ToolStripMenuItem.Checked) this.Text = "싱크 가사 입력";
            else this.Text = "싱크 가사 편집";
        }

        private void 원본ToolStripMenuItem_CheckedChanged(object sender, EventArgs e)
        {
            if (원본ToolStripMenuItem.Checked)
            { textBox2.Text = Orignal; }
            else { textBox2.Text = lp.ToString(); }
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {
            this.Lyrics = textBox2.Text;
            lrcCache[CurrentHash] = textBox2.Text;
        }

        private void 새로고침ToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void comboBox1_TextChanged(object sender, EventArgs e)
        {
            if (comboBox1.Items.IndexOf(comboBox1.Text) == -1) { button3.Enabled = false; button2.Enabled = true; }
            else { button3.Enabled = true; button2.Enabled = false; }
        }

        private void button2_Click(object sender, EventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {
            
        }

        private void comboBox1_SelectedValueChanged(object sender, EventArgs e)
        {
            
            /*
            if (Change) 
            {
                if (Not_Save_Warning()) lc.lrcCache[comboBox1.Text] = "";
                else
                {
                    comboBox1.Text = PreviousString;
                    comboBox1.Text = comboBox1.Items[comboBox1.SelectedIndex].ToString();
                }
            }
            else
            {
                
            }*/
        }

        bool Change;
        private void LyricsTextChanged(object sender, EventArgs e)
        {
            Change = true;
            lrcCache[CurrentHash] = Lyrics;
            textBox2.Text = Lyrics;
        }

        private void comboBox1_Click(object sender, EventArgs e)
        {

        }

        private void comboBox1_KeyDown(object sender, KeyEventArgs e)
        {
            PreviousString = comboBox1.Text;
            /*
            if (e.KeyData == Keys.Down || e.KeyData == Keys.Up ||
                e.KeyData == Keys.Right || e.KeyData == Keys.Left)
            { if (Change)e.Handled = !Not_Save_Warning(); }*/
        }

        bool Not_Save_Warning()
        {
            if (MessageBox.Show("변경 내용이 저장되지 않았습니다.\n변경된 내용을 저장 하시겠습니까?", "싱크 가사 관리자",
               MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == System.Windows.Forms.DialogResult.Yes) return true;
            else return false;
        }

        string PreviousString;
        private void comboBox1_MouseMove(object sender, MouseEventArgs e)
        {
            Lyrics=PreviousString = comboBox1.Text;
        }

        private void button4_Click(object sender, EventArgs e)
        {

        }

        public bool UpdateList()
        {
            if (lc != null)
            {
                foreach (KeyValuePair<string, string> a in lrcCache) 
                { comboBox1.Items.Add(a.Key); }
                return true;
            }
            else { return false; }
        }

        string CurrentHash;
        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            CurrentHash = comboBox1.Items[comboBox1.SelectedIndex].ToString();
            string a = lrcCache[comboBox1.Items[comboBox1.SelectedIndex].ToString()];

        }
    }
}
