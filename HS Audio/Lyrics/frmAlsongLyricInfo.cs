﻿using System;
using System.Windows.Forms;
using HS_CSharpUtility.Extension;

namespace HS_Audio.Lyrics
{
    public partial class frmAlsongLyricInfo : Form
    {
        public frmAlsongLyricInfo()
        {
            InitializeComponent();
        }
        public frmAlsongLyricInfo(LyricAlsong al)
        {
            InitializeComponent();
            Alsong = al;
        }

        LyricAlsong _Alsong;
        public LyricAlsong Alsong
        {
            get { return _Alsong; }
            set 
            {
                _Alsong = value;
                if (value.Title != null || value.Title != "") this.Text = string.Format("가사 등록 정보 자세히 보기 - [{0}]", value.Title);
                else this.Text = "가사 등록 정보 자세히 보기";
                if (lblInfoID!=null) lblInfoID.Text = string.Format("가사 정보 ID : {0}", value.InfoID);
                if (lblStatusID != null) lblStatusID.Text = string.Format("가사 상태 ID : {0}", value.StatusID);
                if (txtAlbum != null) txtAlbum.Text = value.Album;
                if (txtArtist != null) txtArtist.Text = value.Artist;
                if (txtDate != null) txtDate.Text = value.Date;
                if (txtFirstName != null) txtFirstName.Text = value.FirstName;
                if (txtName != null) txtName.Text = value.Name;
                if (txtFirstEMail != null) txtFirstEMail.Text = value.FirstEMail;
                if (txtEMail != null) txtEMail.Text = value.EMail;
                if (txtFirstURL != null) txtFirstURL.Text = value.FirstURL;
                if (txtURL != null) txtURL.Text = value.URL;
                if (txtFirstPhone != null) txtFirstPhone.Text = value.FirstPhone;
                if (txtPhone != null) txtPhone.Text = value.Phone;
                if (txtFirstComment!=null) txtFirstComment.Text = value.FirstComment;
                if (txtComment != null) txtComment.Text = value.Comment;
                if (txtOriginText != null) txtOriginText.Text = (value.GetOriginText == null || value.GetOriginText == "") ? value.ToString() : value.GetOriginText;
            }
        }

        private void frmAlsongLyricInfo_Load(object sender, EventArgs e)
        {

        }

        public bool IsClosing;
        private void frmAlsongLyricInfo_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = !IsClosing;
            this.Hide();
        }

        private void 원본XML스키마저장SToolStripMenuItem_Click(object sender, EventArgs e)
        {
            saveFileDialog1.FileName = Alsong.Title.MakeValidFileName()+"_원본";
            if(saveFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {System.IO.File.WriteAllText(saveFileDialog1.FileName,Alsong.GetOriginText);}
        }

        private void XML스키마저장ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            saveFileDialog1.FileName = Alsong.Title.MakeValidFileName();
            if(saveFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {System.IO.File.WriteAllText(saveFileDialog1.FileName,Alsong.ToString(true));}
        }
    }
}
