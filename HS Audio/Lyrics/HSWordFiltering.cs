﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HS_Audio.Lyrics
{
    public class HSWordFiltering
    {
        #region PrivateMember
        private static readonly string[] Japanese_Hiragana = 
                                          {"あ", "い", "う", "え", "お",
                                           "か", "き", "く", "け", "こ",// "きゃ", "きゅ", "きょ",
                                           "さ", "し", "す", "せ", "そ",// "しゃ", "しゅ", "しょ",
                                           "た", "ち", "つ", "て", "と",// "ちゃ", "ちゅ", "ちょ",
                                           "な", "に", "ぬ", "ね", "の",// "にゃ", "にゅ", "にょ",
                                           "は", "ひ", "ふ", "へ", "ほ",// "ひゃ", "ひゅ", "ひょ",
                                           "ま", "み", "む", "め", "も",// "みゃ", "みょ", "みゅ",
                                           "や", "ゆ", "よ", "ゃ", "ゅ", "ょ", 
                                           "ら", "り", "る", "れ", "ろ",
                                           "わ", "う", "を", "ん", "つ",
                                           "が", "が", "ぐ", "げ", "ご",// "ぎゃ", "", "",
                                           "ざ", "じ", "ず", "ぜ", "ぞ",// "ざゃ", "", "",
                                           "だ", "ぢ", "づ", "で", "ど",// "ぎゃ", "", "",
                                           "ば", "び", "ぶ", "べ", "ぼ",// "ぎゃ", "", "",
                                           "ぱ", "ぴ", "ぷ", "ぺ", "ぽ"};
        private static readonly string[] Japanese_Katakana = 
                                          {"ア", "イ", "ウ", "エ", "オ",
                                           "カ", "キ", "ク", "ケ", "コ",// "きゃ", "きゅ", "きょ",
                                           "サ", "シ", "ス", "セ", "ソ",// "しゃ", "しゅ", "しょ",
                                           "タ", "シ", "ツ", "テ", "ト",// "ちゃ", "ちゅ", "ちょ",
                                           "ナ", "ニ", "ヌ", "ネ", "ノ",// "にゃ", "にゅ", "にょ",
                                           "ハ", "ヒ", "フ", "ヘ", "ホ",// "ひゃ", "ひゅ", "ひょ",
                                           "マ", "ミ", "ム", "メ", "モ",// "みゃ", "みょ", "みゅ",
                                           "ヤ", "ユ", "ヨ", "ャ", "ュ", "ョ", 
                                           "ラ", "リ", "ル", "レ", "ロ",
                                           "ワ", "ウ", "ヲ", "ン", "ツ",
                                           "ガ", "ギ", "グ", "ゲ", "ゴ",// "ぎゃ", "", "",
                                           "ザ", "ジ", "ズ", "ゼ", "ゾ",// "ざゃ", "", "",
                                           "ダ", "ヂ", "ヅ", "デ", "ド",// "ぎゃ", "", "",
                                           "バ", "ビ", "ブ", "ベ", "ボ",// "ぎゃ", "", "",
                                           "パ", "ピ", "プ", "ペ", "ポ"};
        /// <summary>
        /// String 배열에서 해당개체가 있는지 검사합니다.
        /// </summary>
        /// <param name="Item">개체를 검사할 배열입니다.</param>
        /// <param name="Match">일치할 개체 입니다.</param>
        /// <param name="IgnoreCapital">대,소문자를 무시할지 여부 입니다.</param>
        /// <returns>개체가 존재하면 true 존재하지 않으면 false를 반환합니다.</returns>
        internal static List<int> IsArrayItem(string[] Item, string Match, bool IgnoreCapital = false)
        {
            List<int> Line = null;
            for (int i = 0; i < Item.LongLength; i++)
            {
                if (!IgnoreCapital)
                {
                    if (Item[i].IndexOf(Match) != -1){if(Line == null)Line = new List<int>(); Line.Add(i);}
                }
                else if (Item[i].ToLower().IndexOf(Match.ToLower()) != -1) {if (Line == null) Line = new List<int>(); Line.Add(i);}
            }
            return Line;
        }
        #endregion

        public enum LyricsLanguage{Japanese}
        public static string[] ExcludeLyricsLine(string[] Lyrics, LyricsLanguage Language)
        {
            List<string> a = null;
            List<int> b = null;
            for (int i = 0; i < Japanese_Hiragana.Length; i++) {b = IsArrayItem(Lyrics, Japanese_Hiragana[i]); if (b != null)break;}
            if (b != null)
            {
                a = new List<string>(Lyrics);
                for (int i = b.Count - 1; i >= 0; i--) a.RemoveAt(b[i]);
            }
            for (int i = 0; i < Japanese_Katakana.Length; i++){b = IsArrayItem(a!=null?a.ToArray():Lyrics, Japanese_Katakana[i]);if (b != null)break;}
            if (b != null)
            {
                if(a == null)a = new List<string>(Lyrics);
                for (int i = b.Count - 1; i >= 0; i--) a.RemoveAt(b[i]);
            }
            return a!=null?a.ToArray():Lyrics;
        }
    }
}
