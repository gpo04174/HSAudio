﻿namespace HS_Audio.Lyrics
{
    partial class frmLyricsDesktop
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmLyricsDesktop));
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.싱크가사직접입력ToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.가사스타일ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.폰트설정ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.글자색지정ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.배경색깔바꾸기ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.가사창설정ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.맨위로설정ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.자동업데이트ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.가사창투명ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.가사클릭통과ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.우클릭통과ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator8 = new System.Windows.Forms.ToolStripSeparator();
            this.타이틀바숨기기ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator7 = new System.Windows.Forms.ToolStripSeparator();
            this.작업표시줄에표시ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator11 = new System.Windows.Forms.ToolStripSeparator();
            this.가사창리셋ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.재생컨트롤설정ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.재생컨트롤원래대로ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.재생컨트롤투명화영향안받기ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.투명색지정ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.배경색깔이랑연결ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.가사설정ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.가사줄설정ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.가사줄설정켜기ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator9 = new System.Windows.Forms.ToolStripSeparator();
            this.일본어숨기기숨덕용ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.한줄일땐제외ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.가사위치ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cb가사위치 = new System.Windows.Forms.ToolStripComboBox();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.가사업데이트시간msToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripTextBox1 = new System.Windows.Forms.ToolStripTextBox();
            this.toolStripSeparator10 = new System.Windows.Forms.ToolStripSeparator();
            this.경고메세지표시ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator6 = new System.Windows.Forms.ToolStripSeparator();
            this.창닫기ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.싱크가사직접입력ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.label1 = new HS_Audio.Control.HSCustomLable();
            this.vScrollBar1 = new System.Windows.Forms.VScrollBar();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.trackBar1 = new System.Windows.Forms.TrackBar();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.checkBox2 = new System.Windows.Forms.CheckBox();
            this.lblMaker = new System.Windows.Forms.Label();
            this.playControl1 = new HS_Audio.Control.PlayControl();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.fontDialog1 = new System.Windows.Forms.FontDialog();
            this.colorDialog1 = new System.Windows.Forms.ColorDialog();
            this.colorDialog2 = new System.Windows.Forms.ColorDialog();
            this.노래방자막효과ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator12 = new System.Windows.Forms.ToolStripSeparator();
            this.menuStrip1.SuspendLayout();
            this.contextMenuStrip1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar1)).BeginInit();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            resources.ApplyResources(this.menuStrip1, "menuStrip1");
            this.menuStrip1.BackColor = System.Drawing.Color.Green;
            this.menuStrip1.ContextMenuStrip = this.contextMenuStrip1;
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.싱크가사직접입력ToolStripMenuItem});
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.DoubleClick += new System.EventHandler(this.panel2_DoubleClick);
            this.menuStrip1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.menuStrip1_MouseDown);
            this.menuStrip1.MouseLeave += new System.EventHandler(this.menuStrip1_MouseLeave);
            this.menuStrip1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.menuStrip1_MouseMove);
            this.menuStrip1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.menuStrip1_MouseUp);
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.싱크가사직접입력ToolStripMenuItem1,
            this.toolStripSeparator4,
            this.가사스타일ToolStripMenuItem,
            this.가사창설정ToolStripMenuItem,
            this.재생컨트롤설정ToolStripMenuItem,
            this.toolStripSeparator5,
            this.투명색지정ToolStripMenuItem,
            this.toolStripSeparator2,
            this.가사설정ToolStripMenuItem,
            this.toolStripSeparator6,
            this.창닫기ToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            resources.ApplyResources(this.contextMenuStrip1, "contextMenuStrip1");
            this.contextMenuStrip1.Opening += new System.ComponentModel.CancelEventHandler(this.contextMenuStrip1_Opening);
            // 
            // 싱크가사직접입력ToolStripMenuItem1
            // 
            this.싱크가사직접입력ToolStripMenuItem1.Name = "싱크가사직접입력ToolStripMenuItem1";
            resources.ApplyResources(this.싱크가사직접입력ToolStripMenuItem1, "싱크가사직접입력ToolStripMenuItem1");
            this.싱크가사직접입력ToolStripMenuItem1.Click += new System.EventHandler(this.싱크가사직접입력ToolStripMenuItem_Click);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            resources.ApplyResources(this.toolStripSeparator4, "toolStripSeparator4");
            // 
            // 가사스타일ToolStripMenuItem
            // 
            this.가사스타일ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.폰트설정ToolStripMenuItem,
            this.글자색지정ToolStripMenuItem,
            this.배경색깔바꾸기ToolStripMenuItem,
            this.toolStripSeparator12,
            this.노래방자막효과ToolStripMenuItem});
            this.가사스타일ToolStripMenuItem.Name = "가사스타일ToolStripMenuItem";
            resources.ApplyResources(this.가사스타일ToolStripMenuItem, "가사스타일ToolStripMenuItem");
            // 
            // 폰트설정ToolStripMenuItem
            // 
            resources.ApplyResources(this.폰트설정ToolStripMenuItem, "폰트설정ToolStripMenuItem");
            this.폰트설정ToolStripMenuItem.Name = "폰트설정ToolStripMenuItem";
            this.폰트설정ToolStripMenuItem.Click += new System.EventHandler(this.폰트설정ToolStripMenuItem_Click);
            // 
            // 글자색지정ToolStripMenuItem
            // 
            resources.ApplyResources(this.글자색지정ToolStripMenuItem, "글자색지정ToolStripMenuItem");
            this.글자색지정ToolStripMenuItem.Name = "글자색지정ToolStripMenuItem";
            this.글자색지정ToolStripMenuItem.Click += new System.EventHandler(this.글자색지정ToolStripMenuItem_Click);
            // 
            // 배경색깔바꾸기ToolStripMenuItem
            // 
            resources.ApplyResources(this.배경색깔바꾸기ToolStripMenuItem, "배경색깔바꾸기ToolStripMenuItem");
            this.배경색깔바꾸기ToolStripMenuItem.Name = "배경색깔바꾸기ToolStripMenuItem";
            this.배경색깔바꾸기ToolStripMenuItem.Click += new System.EventHandler(this.배경색깔바꾸기ToolStripMenuItem_Click);
            // 
            // 가사창설정ToolStripMenuItem
            // 
            this.가사창설정ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.맨위로설정ToolStripMenuItem,
            this.가사창투명ToolStripMenuItem,
            this.가사클릭통과ToolStripMenuItem,
            this.우클릭통과ToolStripMenuItem,
            this.toolStripSeparator8,
            this.타이틀바숨기기ToolStripMenuItem,
            this.toolStripSeparator7,
            this.작업표시줄에표시ToolStripMenuItem,
            this.toolStripSeparator11,
            this.가사창리셋ToolStripMenuItem});
            this.가사창설정ToolStripMenuItem.Name = "가사창설정ToolStripMenuItem";
            resources.ApplyResources(this.가사창설정ToolStripMenuItem, "가사창설정ToolStripMenuItem");
            // 
            // 맨위로설정ToolStripMenuItem
            // 
            this.맨위로설정ToolStripMenuItem.Checked = true;
            this.맨위로설정ToolStripMenuItem.CheckOnClick = true;
            this.맨위로설정ToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.맨위로설정ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.자동업데이트ToolStripMenuItem});
            this.맨위로설정ToolStripMenuItem.Image = global::HS_Audio.Properties.Resources.PushpinHS;
            this.맨위로설정ToolStripMenuItem.Name = "맨위로설정ToolStripMenuItem";
            resources.ApplyResources(this.맨위로설정ToolStripMenuItem, "맨위로설정ToolStripMenuItem");
            this.맨위로설정ToolStripMenuItem.Click += new System.EventHandler(this.맨위로설정ToolStripMenuItem_Click);
            // 
            // 자동업데이트ToolStripMenuItem
            // 
            this.자동업데이트ToolStripMenuItem.CheckOnClick = true;
            this.자동업데이트ToolStripMenuItem.Name = "자동업데이트ToolStripMenuItem";
            resources.ApplyResources(this.자동업데이트ToolStripMenuItem, "자동업데이트ToolStripMenuItem");
            this.자동업데이트ToolStripMenuItem.CheckedChanged += new System.EventHandler(this.자동업데이트ToolStripMenuItem_CheckedChanged);
            // 
            // 가사창투명ToolStripMenuItem
            // 
            this.가사창투명ToolStripMenuItem.CheckOnClick = true;
            this.가사창투명ToolStripMenuItem.Name = "가사창투명ToolStripMenuItem";
            resources.ApplyResources(this.가사창투명ToolStripMenuItem, "가사창투명ToolStripMenuItem");
            this.가사창투명ToolStripMenuItem.Click += new System.EventHandler(this.가사창투명ToolStripMenuItem_Click);
            // 
            // 가사클릭통과ToolStripMenuItem
            // 
            this.가사클릭통과ToolStripMenuItem.CheckOnClick = true;
            this.가사클릭통과ToolStripMenuItem.Name = "가사클릭통과ToolStripMenuItem";
            resources.ApplyResources(this.가사클릭통과ToolStripMenuItem, "가사클릭통과ToolStripMenuItem");
            this.가사클릭통과ToolStripMenuItem.CheckedChanged += new System.EventHandler(this.가사클릭통과ToolStripMenuItem_CheckedChanged);
            // 
            // 우클릭통과ToolStripMenuItem
            // 
            this.우클릭통과ToolStripMenuItem.CheckOnClick = true;
            this.우클릭통과ToolStripMenuItem.Name = "우클릭통과ToolStripMenuItem";
            resources.ApplyResources(this.우클릭통과ToolStripMenuItem, "우클릭통과ToolStripMenuItem");
            this.우클릭통과ToolStripMenuItem.CheckedChanged += new System.EventHandler(this.우클릭통과ToolStripMenuItem_CheckedChanged);
            // 
            // toolStripSeparator8
            // 
            this.toolStripSeparator8.Name = "toolStripSeparator8";
            resources.ApplyResources(this.toolStripSeparator8, "toolStripSeparator8");
            // 
            // 타이틀바숨기기ToolStripMenuItem
            // 
            this.타이틀바숨기기ToolStripMenuItem.CheckOnClick = true;
            this.타이틀바숨기기ToolStripMenuItem.Name = "타이틀바숨기기ToolStripMenuItem";
            resources.ApplyResources(this.타이틀바숨기기ToolStripMenuItem, "타이틀바숨기기ToolStripMenuItem");
            this.타이틀바숨기기ToolStripMenuItem.CheckedChanged += new System.EventHandler(this.타이틀바숨기기ToolStripMenuItem_CheckedChanged);
            // 
            // toolStripSeparator7
            // 
            this.toolStripSeparator7.Name = "toolStripSeparator7";
            resources.ApplyResources(this.toolStripSeparator7, "toolStripSeparator7");
            // 
            // 작업표시줄에표시ToolStripMenuItem
            // 
            this.작업표시줄에표시ToolStripMenuItem.CheckOnClick = true;
            this.작업표시줄에표시ToolStripMenuItem.Name = "작업표시줄에표시ToolStripMenuItem";
            resources.ApplyResources(this.작업표시줄에표시ToolStripMenuItem, "작업표시줄에표시ToolStripMenuItem");
            this.작업표시줄에표시ToolStripMenuItem.CheckedChanged += new System.EventHandler(this.작업표시줄에표시ToolStripMenuItem_CheckedChanged);
            // 
            // toolStripSeparator11
            // 
            this.toolStripSeparator11.Name = "toolStripSeparator11";
            resources.ApplyResources(this.toolStripSeparator11, "toolStripSeparator11");
            // 
            // 가사창리셋ToolStripMenuItem
            // 
            this.가사창리셋ToolStripMenuItem.Name = "가사창리셋ToolStripMenuItem";
            resources.ApplyResources(this.가사창리셋ToolStripMenuItem, "가사창리셋ToolStripMenuItem");
            this.가사창리셋ToolStripMenuItem.Click += new System.EventHandler(this.button2_Click);
            // 
            // 재생컨트롤설정ToolStripMenuItem
            // 
            this.재생컨트롤설정ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.재생컨트롤원래대로ToolStripMenuItem,
            this.재생컨트롤투명화영향안받기ToolStripMenuItem});
            this.재생컨트롤설정ToolStripMenuItem.Name = "재생컨트롤설정ToolStripMenuItem";
            resources.ApplyResources(this.재생컨트롤설정ToolStripMenuItem, "재생컨트롤설정ToolStripMenuItem");
            // 
            // 재생컨트롤원래대로ToolStripMenuItem
            // 
            this.재생컨트롤원래대로ToolStripMenuItem.Name = "재생컨트롤원래대로ToolStripMenuItem";
            resources.ApplyResources(this.재생컨트롤원래대로ToolStripMenuItem, "재생컨트롤원래대로ToolStripMenuItem");
            this.재생컨트롤원래대로ToolStripMenuItem.Click += new System.EventHandler(this.재생컨트롤원래대로ToolStripMenuItem_Click);
            // 
            // 재생컨트롤투명화영향안받기ToolStripMenuItem
            // 
            this.재생컨트롤투명화영향안받기ToolStripMenuItem.CheckOnClick = true;
            this.재생컨트롤투명화영향안받기ToolStripMenuItem.Name = "재생컨트롤투명화영향안받기ToolStripMenuItem";
            resources.ApplyResources(this.재생컨트롤투명화영향안받기ToolStripMenuItem, "재생컨트롤투명화영향안받기ToolStripMenuItem");
            this.재생컨트롤투명화영향안받기ToolStripMenuItem.Click += new System.EventHandler(this.재생컨트롤투명화영향안받기ToolStripMenuItem_CheckedChanged);
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            resources.ApplyResources(this.toolStripSeparator5, "toolStripSeparator5");
            // 
            // 투명색지정ToolStripMenuItem
            // 
            this.투명색지정ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.배경색깔이랑연결ToolStripMenuItem});
            this.투명색지정ToolStripMenuItem.Name = "투명색지정ToolStripMenuItem";
            resources.ApplyResources(this.투명색지정ToolStripMenuItem, "투명색지정ToolStripMenuItem");
            // 
            // 배경색깔이랑연결ToolStripMenuItem
            // 
            this.배경색깔이랑연결ToolStripMenuItem.Checked = true;
            this.배경색깔이랑연결ToolStripMenuItem.CheckOnClick = true;
            this.배경색깔이랑연결ToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.배경색깔이랑연결ToolStripMenuItem.Name = "배경색깔이랑연결ToolStripMenuItem";
            resources.ApplyResources(this.배경색깔이랑연결ToolStripMenuItem, "배경색깔이랑연결ToolStripMenuItem");
            this.배경색깔이랑연결ToolStripMenuItem.Click += new System.EventHandler(this.배경색깔이랑연결ToolStripMenuItem_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            resources.ApplyResources(this.toolStripSeparator2, "toolStripSeparator2");
            // 
            // 가사설정ToolStripMenuItem
            // 
            this.가사설정ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.가사줄설정ToolStripMenuItem,
            this.가사줄설정켜기ToolStripMenuItem,
            this.toolStripSeparator9,
            this.일본어숨기기숨덕용ToolStripMenuItem,
            this.한줄일땐제외ToolStripMenuItem,
            this.toolStripSeparator3,
            this.가사위치ToolStripMenuItem,
            this.toolStripSeparator1,
            this.가사업데이트시간msToolStripMenuItem,
            this.toolStripSeparator10,
            this.경고메세지표시ToolStripMenuItem});
            this.가사설정ToolStripMenuItem.Name = "가사설정ToolStripMenuItem";
            resources.ApplyResources(this.가사설정ToolStripMenuItem, "가사설정ToolStripMenuItem");
            // 
            // 가사줄설정ToolStripMenuItem
            // 
            this.가사줄설정ToolStripMenuItem.Name = "가사줄설정ToolStripMenuItem";
            resources.ApplyResources(this.가사줄설정ToolStripMenuItem, "가사줄설정ToolStripMenuItem");
            this.가사줄설정ToolStripMenuItem.Click += new System.EventHandler(this.가사줄설정ToolStripMenuItem_Click);
            // 
            // 가사줄설정켜기ToolStripMenuItem
            // 
            this.가사줄설정켜기ToolStripMenuItem.Checked = true;
            this.가사줄설정켜기ToolStripMenuItem.CheckOnClick = true;
            this.가사줄설정켜기ToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.가사줄설정켜기ToolStripMenuItem.Name = "가사줄설정켜기ToolStripMenuItem";
            resources.ApplyResources(this.가사줄설정켜기ToolStripMenuItem, "가사줄설정켜기ToolStripMenuItem");
            this.가사줄설정켜기ToolStripMenuItem.CheckedChanged += new System.EventHandler(this.가사줄설정켜기ToolStripMenuItem_CheckedChanged);
            // 
            // toolStripSeparator9
            // 
            this.toolStripSeparator9.Name = "toolStripSeparator9";
            resources.ApplyResources(this.toolStripSeparator9, "toolStripSeparator9");
            // 
            // 일본어숨기기숨덕용ToolStripMenuItem
            // 
            this.일본어숨기기숨덕용ToolStripMenuItem.CheckOnClick = true;
            this.일본어숨기기숨덕용ToolStripMenuItem.Name = "일본어숨기기숨덕용ToolStripMenuItem";
            resources.ApplyResources(this.일본어숨기기숨덕용ToolStripMenuItem, "일본어숨기기숨덕용ToolStripMenuItem");
            this.일본어숨기기숨덕용ToolStripMenuItem.CheckedChanged += new System.EventHandler(this.일본어숨기기숨덕용ToolStripMenuItem_CheckedChanged);
            // 
            // 한줄일땐제외ToolStripMenuItem
            // 
            this.한줄일땐제외ToolStripMenuItem.Checked = true;
            this.한줄일땐제외ToolStripMenuItem.CheckOnClick = true;
            this.한줄일땐제외ToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.한줄일땐제외ToolStripMenuItem.Name = "한줄일땐제외ToolStripMenuItem";
            resources.ApplyResources(this.한줄일땐제외ToolStripMenuItem, "한줄일땐제외ToolStripMenuItem");
            this.한줄일땐제외ToolStripMenuItem.CheckedChanged += new System.EventHandler(this.한줄일땐제외ToolStripMenuItem_CheckedChanged);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            resources.ApplyResources(this.toolStripSeparator3, "toolStripSeparator3");
            // 
            // 가사위치ToolStripMenuItem
            // 
            this.가사위치ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cb가사위치});
            this.가사위치ToolStripMenuItem.Name = "가사위치ToolStripMenuItem";
            resources.ApplyResources(this.가사위치ToolStripMenuItem, "가사위치ToolStripMenuItem");
            // 
            // cb가사위치
            // 
            this.cb가사위치.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb가사위치.Items.AddRange(new object[] {
            resources.GetString("cb가사위치.Items"),
            resources.GetString("cb가사위치.Items1"),
            resources.GetString("cb가사위치.Items2"),
            resources.GetString("cb가사위치.Items3"),
            resources.GetString("cb가사위치.Items4"),
            resources.GetString("cb가사위치.Items5"),
            resources.GetString("cb가사위치.Items6"),
            resources.GetString("cb가사위치.Items7"),
            resources.GetString("cb가사위치.Items8")});
            this.cb가사위치.Name = "cb가사위치";
            resources.ApplyResources(this.cb가사위치, "cb가사위치");
            this.cb가사위치.SelectedIndexChanged += new System.EventHandler(this.cb가사위치_SelectedIndexChanged);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            resources.ApplyResources(this.toolStripSeparator1, "toolStripSeparator1");
            // 
            // 가사업데이트시간msToolStripMenuItem
            // 
            this.가사업데이트시간msToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripTextBox1});
            this.가사업데이트시간msToolStripMenuItem.Name = "가사업데이트시간msToolStripMenuItem";
            resources.ApplyResources(this.가사업데이트시간msToolStripMenuItem, "가사업데이트시간msToolStripMenuItem");
            // 
            // toolStripTextBox1
            // 
            this.toolStripTextBox1.Name = "toolStripTextBox1";
            resources.ApplyResources(this.toolStripTextBox1, "toolStripTextBox1");
            this.toolStripTextBox1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.toolStripTextBox1_KeyPress);
            this.toolStripTextBox1.TextChanged += new System.EventHandler(this.toolStripTextBox1_TextChanged);
            // 
            // toolStripSeparator10
            // 
            this.toolStripSeparator10.Name = "toolStripSeparator10";
            resources.ApplyResources(this.toolStripSeparator10, "toolStripSeparator10");
            // 
            // 경고메세지표시ToolStripMenuItem
            // 
            this.경고메세지표시ToolStripMenuItem.Checked = true;
            this.경고메세지표시ToolStripMenuItem.CheckOnClick = true;
            this.경고메세지표시ToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.경고메세지표시ToolStripMenuItem.Name = "경고메세지표시ToolStripMenuItem";
            resources.ApplyResources(this.경고메세지표시ToolStripMenuItem, "경고메세지표시ToolStripMenuItem");
            this.경고메세지표시ToolStripMenuItem.CheckedChanged += new System.EventHandler(this.경고메세지표시ToolStripMenuItem_CheckedChanged);
            // 
            // toolStripSeparator6
            // 
            this.toolStripSeparator6.Name = "toolStripSeparator6";
            resources.ApplyResources(this.toolStripSeparator6, "toolStripSeparator6");
            // 
            // 창닫기ToolStripMenuItem
            // 
            this.창닫기ToolStripMenuItem.Name = "창닫기ToolStripMenuItem";
            resources.ApplyResources(this.창닫기ToolStripMenuItem, "창닫기ToolStripMenuItem");
            this.창닫기ToolStripMenuItem.Click += new System.EventHandler(this.창닫기ToolStripMenuItem_Click);
            // 
            // 싱크가사직접입력ToolStripMenuItem
            // 
            this.싱크가사직접입력ToolStripMenuItem.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.싱크가사직접입력ToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.싱크가사직접입력ToolStripMenuItem.Name = "싱크가사직접입력ToolStripMenuItem";
            resources.ApplyResources(this.싱크가사직접입력ToolStripMenuItem, "싱크가사직접입력ToolStripMenuItem");
            this.싱크가사직접입력ToolStripMenuItem.Click += new System.EventHandler(this.싱크가사직접입력ToolStripMenuItem_Click);
            // 
            // label1
            // 
            resources.ApplyResources(this.label1, "label1");
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label1.ForeColor = System.Drawing.Color.Lime;
            this.label1.Name = "label1";
            this.label1.NewText = null;
            this.label1.RotateAngle = 0;
            this.label1.DoubleClick += new System.EventHandler(this.panel2_DoubleClick);
            this.label1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.label1_MouseDown);
            this.label1.MouseLeave += new System.EventHandler(this.label1_MouseLeave);
            this.label1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.label1_MouseMove);
            this.label1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.label1_MouseUp);
            // 
            // vScrollBar1
            // 
            resources.ApplyResources(this.vScrollBar1, "vScrollBar1");
            this.vScrollBar1.LargeChange = 1;
            this.vScrollBar1.Maximum = 50;
            this.vScrollBar1.Minimum = 5;
            this.vScrollBar1.Name = "vScrollBar1";
            this.toolTip1.SetToolTip(this.vScrollBar1, resources.GetString("vScrollBar1.ToolTip"));
            this.vScrollBar1.Value = 9;
            this.vScrollBar1.ValueChanged += new System.EventHandler(this.vScrollBar1_ValueChanged);
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.Transparent;
            this.panel2.Controls.Add(this.panel1);
            resources.ApplyResources(this.panel2, "panel2");
            this.panel2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.panel2.Name = "panel2";
            this.panel2.DoubleClick += new System.EventHandler(this.panel2_DoubleClick);
            this.panel2.MouseDown += new System.Windows.Forms.MouseEventHandler(this.panel2_MouseDown);
            this.panel2.MouseLeave += new System.EventHandler(this.panel2_MouseLeave);
            this.panel2.MouseMove += new System.Windows.Forms.MouseEventHandler(this.panel2_MouseMove);
            this.panel2.MouseUp += new System.Windows.Forms.MouseEventHandler(this.panel2_MouseUp);
            // 
            // panel1
            // 
            this.panel1.ContextMenuStrip = this.contextMenuStrip1;
            this.panel1.Controls.Add(this.panel3);
            this.panel1.Controls.Add(this.vScrollBar1);
            this.panel1.Controls.Add(this.lblMaker);
            this.panel1.Controls.Add(this.playControl1);
            this.panel1.Controls.Add(this.menuStrip1);
            this.panel1.Controls.Add(this.label1);
            resources.ApplyResources(this.panel1, "panel1");
            this.panel1.Name = "panel1";
            this.panel1.Paint += new System.Windows.Forms.PaintEventHandler(this.panel1_Paint);
            // 
            // panel3
            // 
            resources.ApplyResources(this.panel3, "panel3");
            this.panel3.BackColor = System.Drawing.Color.Green;
            this.panel3.ContextMenuStrip = this.contextMenuStrip1;
            this.panel3.Controls.Add(this.label2);
            this.panel3.Controls.Add(this.trackBar1);
            this.panel3.Controls.Add(this.button1);
            this.panel3.Controls.Add(this.button2);
            this.panel3.Controls.Add(this.checkBox1);
            this.panel3.Controls.Add(this.checkBox2);
            this.panel3.Name = "panel3";
            this.panel3.DoubleClick += new System.EventHandler(this.panel2_DoubleClick);
            this.panel3.MouseDown += new System.Windows.Forms.MouseEventHandler(this.label1_MouseDown);
            this.panel3.MouseLeave += new System.EventHandler(this.label1_MouseLeave);
            this.panel3.MouseMove += new System.Windows.Forms.MouseEventHandler(this.label1_MouseMove);
            this.panel3.MouseUp += new System.Windows.Forms.MouseEventHandler(this.label1_MouseUp);
            // 
            // label2
            // 
            resources.ApplyResources(this.label2, "label2");
            this.label2.BackColor = System.Drawing.Color.Green;
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Name = "label2";
            // 
            // trackBar1
            // 
            resources.ApplyResources(this.trackBar1, "trackBar1");
            this.trackBar1.BackColor = System.Drawing.Color.Green;
            this.trackBar1.Maximum = 99;
            this.trackBar1.Minimum = 10;
            this.trackBar1.Name = "trackBar1";
            this.trackBar1.TickFrequency = 5;
            this.trackBar1.Value = 90;
            this.trackBar1.ValueChanged += new System.EventHandler(this.trackBar1_ValueChanged);
            // 
            // button1
            // 
            resources.ApplyResources(this.button1, "button1");
            this.button1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.button1.ForeColor = System.Drawing.Color.White;
            this.button1.Name = "button1";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            resources.ApplyResources(this.button2, "button2");
            this.button2.BackColor = System.Drawing.Color.Blue;
            this.button2.ForeColor = System.Drawing.Color.White;
            this.button2.Name = "button2";
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // checkBox1
            // 
            resources.ApplyResources(this.checkBox1, "checkBox1");
            this.checkBox1.BackColor = System.Drawing.Color.Green;
            this.checkBox1.Checked = true;
            this.checkBox1.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBox1.ForeColor = System.Drawing.Color.Transparent;
            this.checkBox1.Name = "checkBox1";
            this.toolTip1.SetToolTip(this.checkBox1, resources.GetString("checkBox1.ToolTip"));
            this.checkBox1.UseVisualStyleBackColor = false;
            this.checkBox1.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
            // 
            // checkBox2
            // 
            resources.ApplyResources(this.checkBox2, "checkBox2");
            this.checkBox2.BackColor = System.Drawing.Color.Green;
            this.checkBox2.Checked = true;
            this.checkBox2.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBox2.ForeColor = System.Drawing.Color.Transparent;
            this.checkBox2.Name = "checkBox2";
            this.checkBox2.UseVisualStyleBackColor = false;
            this.checkBox2.CheckedChanged += new System.EventHandler(this.checkBox2_CheckedChanged);
            // 
            // lblMaker
            // 
            resources.ApplyResources(this.lblMaker, "lblMaker");
            this.lblMaker.BackColor = System.Drawing.Color.Transparent;
            this.lblMaker.ContextMenuStrip = this.contextMenuStrip1;
            this.lblMaker.ForeColor = System.Drawing.Color.Lime;
            this.lblMaker.Name = "lblMaker";
            // 
            // playControl1
            // 
            resources.ApplyResources(this.playControl1, "playControl1");
            this.playControl1.BackColor = System.Drawing.Color.Transparent;
            this.playControl1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.playControl1.ContextMenuStrip = this.contextMenuStrip1;
            this.playControl1.MovePanelColor = System.Drawing.Color.Black;
            this.playControl1.MovePanelSize = new System.Drawing.Size(8, 27);
            this.playControl1.Name = "playControl1";
            this.playControl1.PauseButtonDefaultColor = System.Drawing.Color.Black;
            this.playControl1.PauseButtonFocusColor = System.Drawing.Color.White;
            this.playControl1.PlayButtonDefaultColor = System.Drawing.Color.Black;
            this.playControl1.PlayButtonFocusColor = System.Drawing.Color.White;
            this.playControl1.StopButtonDefaultColor = System.Drawing.Color.Black;
            this.playControl1.StopButtonFocusColor = System.Drawing.Color.White;
            this.playControl1.UseMove = true;
            this.playControl1.PlayButtonPress += new HS_Audio.Control.PlayControl.PlayButtonPressEventHandler(this.playControl1_PlayButtonPress);
            this.playControl1.PauseButtonPress += new HS_Audio.Control.PlayControl.PauseButtonPressEventHandler(this.playControl1_PauseButtonPress);
            this.playControl1.StopButtonPress += new HS_Audio.Control.PlayControl.StopButtonPressEventHandler(this.playControl1_StopButtonPress);
            this.playControl1.MoveCompleted += new HS_Audio.Control.PlayControl.MoveCompletedEventHandler(this.playControl1_MoveCompleted);
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 5000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // fontDialog1
            // 
            this.fontDialog1.Color = System.Drawing.SystemColors.ControlText;
            this.fontDialog1.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.fontDialog1.ShowApply = true;
            this.fontDialog1.Apply += new System.EventHandler(this.fontDialog1_Apply);
            // 
            // colorDialog1
            // 
            this.colorDialog1.AnyColor = true;
            this.colorDialog1.Color = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.colorDialog1.FullOpen = true;
            // 
            // colorDialog2
            // 
            this.colorDialog2.AnyColor = true;
            this.colorDialog2.Color = System.Drawing.Color.Lime;
            this.colorDialog2.FullOpen = true;
            // 
            // 노래방자막효과ToolStripMenuItem
            // 
            this.노래방자막효과ToolStripMenuItem.CheckOnClick = true;
            this.노래방자막효과ToolStripMenuItem.Name = "노래방자막효과ToolStripMenuItem";
            resources.ApplyResources(this.노래방자막효과ToolStripMenuItem, "노래방자막효과ToolStripMenuItem");
            // 
            // toolStripSeparator12
            // 
            this.toolStripSeparator12.Name = "toolStripSeparator12";
            resources.ApplyResources(this.toolStripSeparator12, "toolStripSeparator12");
            // 
            // frmLyricsDesktop
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            resources.ApplyResources(this, "$this");
            this.Controls.Add(this.panel2);
            this.DoubleBuffered = true;
            this.ForeColor = System.Drawing.Color.Wheat;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
            this.KeyPreview = true;
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "frmLyricsDesktop";
            this.Opacity = 0.99D;
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.TopMost = true;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmLyrics_Float_FormClosing);
            this.Load += new System.EventHandler(this.frmLyrics_Load);
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.frmLyrics_Float_MouseDown);
            this.MouseMove += new System.Windows.Forms.MouseEventHandler(this.frmLyrics_Float_MouseMove);
            this.Move += new System.EventHandler(this.frmLyricsDesktop_Move);
            this.Resize += new System.EventHandler(this.frmLyricsDesktop_Resize);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.contextMenuStrip1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem 싱크가사직접입력ToolStripMenuItem;
        private System.Windows.Forms.VScrollBar vScrollBar1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.TrackBar trackBar1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button button1;
        public System.Windows.Forms.Label lblMaker;
        private System.Windows.Forms.CheckBox checkBox2;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.FontDialog fontDialog1;
        private System.Windows.Forms.ColorDialog colorDialog1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.ToolStripMenuItem 투명색지정ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 배경색깔이랑연결ToolStripMenuItem;
        private System.Windows.Forms.ColorDialog colorDialog2;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem 창닫기ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 싱크가사직접입력ToolStripMenuItem1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator6;
        private System.Windows.Forms.ToolStripMenuItem 가사스타일ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 폰트설정ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 글자색지정ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 배경색깔바꾸기ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 가사창설정ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 맨위로설정ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 자동업데이트ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 가사창투명ToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator8;
        private System.Windows.Forms.ToolStripMenuItem 타이틀바숨기기ToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator7;
        private System.Windows.Forms.ToolStripMenuItem 재생컨트롤설정ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 재생컨트롤원래대로ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 재생컨트롤투명화영향안받기ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 작업표시줄에표시ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 가사설정ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 가사줄설정ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 가사위치ToolStripMenuItem;
        private System.Windows.Forms.ToolStripComboBox cb가사위치;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem 일본어숨기기숨덕용ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 한줄일땐제외ToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator9;
        private System.Windows.Forms.ToolStripMenuItem 가사업데이트시간msToolStripMenuItem;
        private System.Windows.Forms.ToolStripTextBox toolStripTextBox1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator10;
        private System.Windows.Forms.ToolStripMenuItem 경고메세지표시ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 가사줄설정켜기ToolStripMenuItem;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator11;
        private System.Windows.Forms.ToolStripMenuItem 가사창리셋ToolStripMenuItem;
        internal System.Windows.Forms.CheckBox checkBox1;
        internal HS_Audio.Control.HSCustomLable label1;
        internal Control.PlayControl playControl1;
        private System.Windows.Forms.ToolStripMenuItem 가사클릭통과ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 우클릭통과ToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator12;
        private System.Windows.Forms.ToolStripMenuItem 노래방자막효과ToolStripMenuItem;
    }
}