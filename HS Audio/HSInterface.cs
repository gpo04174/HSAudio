﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HS_Audio_Helper
{
    public abstract class HSInterfaceForm:System.Windows.Forms.Form
    {
        public Dictionary<string, string> Setting
        { get; set; }

        public bool IsClosing;
    }
    public interface IHSInterface
    { Dictionary<string, string> Setting { get; set; } }
}
