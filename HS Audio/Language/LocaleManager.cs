﻿using System.Collections;
namespace HS_Audio.Languge
{
    public enum Locale { ko_kr, en_us, ja_jp }
    public class LocaleManager
    {
        public delegate void LocaleChangesEventhandler(Locale locale);
        /// <summary>
        /// 언어 국가가 변경되면 발생 합니다.
        /// </summary>
        public event LocaleChangesEventhandler LocaleChanged;

        Locale _CurrentLocale = Locale.ko_kr;
        public Locale CurrentLocale
        {
            get { return _CurrentLocale; }
            set { _CurrentLocale = value; try { LocaleChanged(value); }catch{} }
        }

        internal LocaleManager() { GetLocaleString(Locale.ko_kr); }
        internal LocaleManager(Locale locale)
        {
            CurrentLocale = locale;
            GetLocaleString(locale);
        }
        public static LocaleString GetLocaleString(Locale locale)
        {
            switch (locale)
            {
                case Locale.ja_jp:
                    return new ja_jp();
                case Locale.en_us:
                    return new en_us();
                default:
                    return new ko_kr();
            }
        }
    }

    public abstract class LocaleString:IEnumerable
    {
        #region 공통
        public string ProgramName = "HS™ 플레이어";
        public string ProgramArthur = "(Made by 박홍식)";
        public string 파일FToolStripMenuItem = "파일";
        public string 보기VToolStripMenuItem = "보기(&V)";
        public string 설정ToolStripMenuItem = "설정(&T)";
        public string 열기OToolStripMenuItem = "열기(&O)";
        public string 도움말HToolStripMenuItem = "도움말(&H)";
        public string 추가 = "추가";
        public string 재생 = "재생";
        public string 일시정지 = "일시정지";
        public string 정지 = "정지";
        public string 파일이름 = "파일 이름";
        public string 음악경로 = "음악 경로";
        public string 파일경로 = "파일 경로";
        public string 경로 = "경로";
        public string 취소 = "취소";
        public string 준비 = "준비";
        public string 맨위로설정 = "맨 위로 설정";
        public string 끝내기 = "끝내기(&X)";
        public string 닫기 = "닫기";
        public string 언어 = "언어";
        public string 상태 = "상태";
        public string 삭제 = "삭제";
        public string 현재 = "현재";
        public string 복사 = "복사";
        public string 이름 = "이름";
        public string 재생시간 = "재생시간";
        public string 디버깅 = "디버깅";
        public string 크기 = "크기";
        public string 데이터 = "데이터";
        public string 웨이브 = "웨이브";
        public string 저장 = "저장";
        public string 원본="원본";
        public string 초기화 = "초기화";
        public string 안전모드 = "안전모드";
        public string 저장중 = "저장중";
        public string 오류 = "오류";
        public string 개 = "개";
        public string 좌 = "좌";
        public string 우 = "우";
        public string 채널 = "채널";
        public string 이동 = "이동";
        public string 다음 = "다음";
        public string 이전 = "이전";
        public string 밀리초표시 = "밀리초 표시";
        public string 검색어를입력하세요 = "검색어를 입력 하세요";
        public string 가사 = "가사";
        public string 제목 = "제목";
        public string 앨범 = "앨범";
        public string 아티스트 = "아티스트";
        public string 비트레이트 = "비트레이트";
        public string 샘플레이트 = "샘플레이트";
        public string 명 = "명";
        #endregion

        #region frmMain.cs
        
        public string 트레이로보내기ToolStripMenuItem = "트레이로 보내기";
        public string 파일열기ToolStripMenuItem = "파일 열기";
         public string 파일열기ToolStripMenuItem_Message = "일부항목은 파일이 존재하지 않거나\n올바르지 않은 음악 파일 이므로 추가하지 못했습니다.\n" +"더 자세한 정보를 알고싶다면 로그를 분석해주시기 바랍니다.\n\n추가 못한 경로:\n";
         public string 파일열기ToolStripMenuItem_MessageTitle = "일부 항목 추가실패!";
        public string 폴더열기ToolStripMenuItem = "폴더 열기";
        public string 하위폴더검색ToolStripMenuItem = "하위 폴더 검색";
        public string 재생목록열기ToolStripMenuItem = "재생목록 열기(&L)";
        public string 유니코드ToolStripMenuItem = "유니코드";
        public string 재생목록다른이름으로저장ToolStripMenuItem = "재생목록 저장(&S)";
        public string 현재상태저장 = "현재 상태 저장";
        public string 파일로웨이브저장ToolStripMenuItem = "파일로 현재 재생위치의 웨이브정보 저장";
         public string SaveWaveMessage="웨이브 저장... (웨이브 크기: {0})";
        public string 원본사이즈사용ToolStripMenuItem = "원본 사이즈 구하기";

        public string 재생목록창꾸미기ToolStripMenuItem = "재생 목록 창 꾸미기";
         public string 모눈선표시ToolStripMenuItem="모눈선 표시";
         public string 선택한항목강조표시ToolStripMenuItem = "선택한 항목 강조 표시";
         public string 배경색상설정ToolStripMenuItem="배경 색상 설정";
         public string 삽입색상설정ToolStripMenuItem = "삽입 색상 설정";
         public string 이미지불러오기ToolStripMenuItem = "이미지 불러오기 (클릭!)";
          public string 이미지불러오기ToolStripMenuItem_Message = "이미지 열기 실패!!\n\n자세한 예외:\n";
          public string 이미지불러오기ToolStripMenuItem_MessageTitle = "이미지 열기 실패!!";
         public string 타일보기ToolStripMenuItem = "타일 보기";
         public string ImageDialog_Title = "재생 목록창에 사용할 사진 열기...";
         public string ImageDialog_Filter = "일반 사진 파일|*.jpg;*.jpeg;*.bmp;*.png;*.gif;*.tif;*.tiff;|특수 그림 파일 (*.wmf, *.Exif, *.Emf)|*.wmf;*.Exif;*.Emf";
         public string 지우기ToolStripMenuItem = "이미지 지우기";
          public string 지우기ToolStripMenuItem_Message = "정말 재생목록 배경 이미지를 지우시겠습니까?";
          public string 지우기ToolStripMenuItem_MessageTitle = "재생목록 배경 이미지를 지우기";
        public string 핫키설정ToolStripMenuItem = "단축키(핫키) 설정";
        public string 기본프로그램으로설정ToolStripMenuItem = "기본 음악 프로그램으로 설정";
         public string 기본프로그램으로설정ToolStripMenuItem_Message = "기본 음악 프로그램으로 등록 실패!!\n\n이 프로그램을 관리자 권한으로 실행하고 다시 시도해 보시거나\n로그를 분석해 주세요.\n\n" +
             "[※메모※]\n일부 백신에서는 바이러스로 진단을 한다는 제보가 있었는데\n본 프로그램은 바이러스가 100%아님을 밝힙니다.\n만약 관리자 권한으로 실행했는데 이 메세지가 계속 보이신다면\n실행중인 백신의 실시간 검사를 꺼보고 다시 시도해 보세요.";
        public string 안전모드ToolStripMenuItem = "안전 모드";
        public string syncToolStripMenuItem = "Async 모드";
        public string 음악믹싱모드ToolStripMenuItem = "음악 믹싱 모드";
        public string 싱크가사로컬캐시ToolStripMenuItem = "싱크 가사 로컬 캐시";
        public string 싱크가사캐시정리ToolStripMenuItem = "싱크 가사 캐시 정리";
        public string 태그없는항목자동삽입ToolStripMenuItem = "태그 없는 항목 자동 삽입";
        public string 싱크가사캐시관리ToolStripMenuItem = "싱크 가사 캐시 관리";
        public string 싱크가사우선순위ToolStripMenuItem = "온라인 싱크 가사 전용";
        public string 파일복구및재설치ToolStripMenuItem = "사운드 시스템 엔진 파일 복구 및 재설치";
         public string 파일복구및재설치ToolStripMenuItem_Message = "사운드 시스템 엔진 파일 복구 및 재설치를 하려면 프로그램을 재시작 하여야 합니다.\n\n사운드 시스템 엔진 파일 복구 및 재설치 하시겠습니까?";
         public string 파일복구및재설치ToolStripMenuItem_MessageTitle = "사운드 시스템 엔진 파일 복구 및 재설치 알림";
        public string 프로그램다시시작ToolStripMenuItem = "프로그램 다시 시작";
         public string 프로그램다시시작ToolStripMenuItem_Message="프로그램을 이어서 다시 시작 하시겠습니까?\n\nYes: 이어서 다시 시작, No: 새로 다시 시작, Cancle: 취소";
         public string 프로그램다시시작ToolStripMenuItem_MessageTitle = "프로그램 다시 시작";
        public string 우선순위ToolStripMenuItem = "프로그램 우선순위";
        public string 높음ToolStripMenuItem = "높음";
        public string 높은우선순위ToolStripMenuItem = "이 프로그램 우선";
        public string 보통권장ToolStripMenuItem = "보통 (권장)";
        public string 낮은우선순위ToolStripMenuItem = "다른 프로그램 우선";
        public string LOL우선순위ToolStripMenuItem = "LOL 우선 (권장)";
        public string LOL보통권장ToolStripMenuItem = "보통";
        public string 가비지커렉션가동ToolStripMenuItem = "가비지 컬렉션 가동";
         public string 가비지커렉션가동ToolStripMenuItem_Message = "정말 가비지 컬렉션을 실행 하시겠습니까?\n\n애플리케이션이 잠깐 응답하지 않을 수 있습니다.\n\nGC메모리 할당: {0}";
         public string 가비지커렉션가동ToolStripMenuItem_MessageTitle = "가비지 컬렉션 가동 (개발자 전용)";

        public string 싱크가사찾기ToolStripMenuItem = "싱크 가사 검색";
        public string 바탕화면싱크가사창띄우기ToolStripMenuItem = "바탕화면 싱크 가사 창 띄우기";
        public string 바탕화면싱크가사창닫기ToolStripMenuItem = "바탕화면 싱크 가사 창 닫기";
        public string 바탕화면싱크가사창새로고침 = "바탕화면 싱크 가사 창 새로 고침";
        public string 재생상세정보ToolStripMenuItem = "재생 상세 정보";
        public string 모니터끄기ToolStripMenuItem = "모니터 끄기";
        public string 로그관리ToolStripMenuItem = "로그 관리";
        public string 로그폴더열기ToolStripMenuItem = "로그 폴더 열기";
        public string 로그지우기ToolStripMenuItem = "로그 지우기";
         public string 로그지우기ToolStripMenuItem_Message="정말 모든 로그를 지우시겠습니까?";
         public string 로그지우기ToolStripMenuItem_MessageEx = "로그를 비우지 못했습니다.\n\n자세한 이유:\n";
        public string 상태표시줄업데이트ToolStripMenuItem = "상태 표시줄 업데이트";

        public string 플러그인PToolStripMenuItem = "플러그인(&P)";

        public string 정보ToolStripMenuItem = "프로그램 정보...(&A)";
        public string 업데이트변경사항보기ToolStripMenuItem = "업데이트 변경사항 보기(&R)";
        public string 업데이트확인ToolStripMenuItem = "업데이트 확인 (클릭!)";
        public string 업데이트서버설정ToolStripMenuItem = "업데이트 서버 설정";
        public string 디버깅ToolStripMenuItem = "디버깅 (개발자 전용)";
         public string 디버깅ToolStripMenuItem_Message = "디버깅을 하면 이 프로그램은 즉시 멈추고 디버거를 호출합니다.\n\n디버거가 무엇인지 모르거나 없는 컴퓨터는 사용하지 말아주시기 바랍니다.\n\n개발자용으로만 설계되었습니다. 정말 디버깅을 하시겠습니까?";
         public string 디버깅ToolStripMenuItem_MessageTitle = "디버깅 경고";

        public string lbl파일경로 = "파일 경로";
        public string btn재생설정 = "재생설정";
        public string[] cb반복 = {"전체 반복","랜덤 재생","한곡 반복","한번만 재생","반복 없음"};
        public string 목록여는중 = "목록 여는중";
        public string lblSelectMusicPath = "선택한 음악경로: ";

        public string 닫기_Message = "정말 이 프로그램을 종료하시겠습니까?";
        public string 닫기_MessageTitle = "프로그램 종료 알림";
        public string 삭제_Message = "현재 재생중인 항목입니다. 재생을 멈추고 삭제 하시겠습니까?";
        public string 삭제_MessageTitle = "재생중인 항목 삭제 알림";

        public string 새인스턴스시작ToolStripMenuItem = "새 인스턴스 시작";
        public string 현재위치의웨이브저장ToolStripMenuItem = "현재 위치의 웨이브 저장";
        public string mD5해쉬값구하기ToolStripMenuItem = "MD5 해쉬값 구하기";
        public string 가사찾기ToolStripMenuItem = "선택한 곡 싱크 가사 찾기";
        public string 탐색기에서해당파일열기ToolStripMenuItem = "탐색기에서 해당파일 열기";
        public string 재생컨트롤ToolStripMenuItem = "재생 컨트롤";
        public string 재생설정열기ToolStripMenuItem = "재생 설정 열기";
        public string 메인창열기ToolStripMenuItem = "메인창 열기";

        public string 삭제ToolStripMenuItem_Message = "정말로 선택된 항목을 삭제하시겠습니까?";
        public string 삭제ToolStripMenuItem_Message1 = "정말로 선택된 항목들을 삭제하시겠습니까?";
        public string 삭제ToolStripMenuItem_MessageTitle = "항목삭제 알림";
        public string 목록삭제중 = "번째 목록 삭제중";
        public string 재생목록여는중 = "재생목록 여는 중";
        public string 파일여는중 = "번째 파일 여는중";
        public string 인코딩 = "인코딩";


        public string 폴더열기_Message = "\"*%1*\" 폴더에 음악 파일이 없는것 같습니다.\n\n[파일(F)]->[열기(O)]->[폴더 열기]->[하위 폴더 검색]을\n체크해 보시고 다시 시도 해보시기 바랍니다.";
        public string 폴더열기_MessageTitle = "이렇게 해보시면 어떨까요?";
        public string 목록열기_Message = "일부항목은 파일이 존재하지 않아서 추가하지 못했습니다.\n\n추가 못한 경로:\n";
        public string 목록열기_MessageTitle = "일부 항목 추가실패! (재생목록 이름: ";

        public string 글자색상설정ToolStripMenuItem = "글자 색상 설정";
        public string 예제사진ToolStripMenuItem = "샘플 사진 (일부 사진 취향 주의!)";
         public string 크리스마스1ToolStripMenuItem = "크리스마스 (일러스트)";
         public string 일러스트1ToolStripMenuItem = "일러스트1 (캐릭터)";
          public string 일러스트1ToolStripMenuItem_Tooltip = "사토카즈마 (TV 애니메이션「이 멋진세계에 축복을!」) 등장인물 [1920 X 1080]";
         public string 일러스트2ToolStripMenuItem = "일러스트2 (캐릭터)";
          public string 일러스트2ToolStripMenuItem_Tooltip = "메구밍 (TV 애니메이션「이 멋진세계에 축복을!」) 등장인물 [1980 X 1118]";
        //public string 이름_경로 = "[이름: {0} / 경로: {1}]";
        #endregion

        public string CopyAndWriteLanguage = "복사한 다음 번역 입력";

        #region 음악 태그 관련
        public string 아티스트존재X = "아티스트명이 없습니다.";
        public string 앨범존재X = "앨범명이 없습니다.";
        public string 앨범사진존재X = "앨범 커버 이미지가 없습니다.";
        public string 앨범커버손상 = "앨범 커버 이미지가 손상 되었습니다.";
        public string 태그뷰어 = "태그 뷰어";
        public string 앨범커버사진 = "앨범 커버 사진";
        public string 해상도 = "해상도";
        public string picTagPic_Message = "앨범 커버 이미지가 존재하지않아\n사진을 표시할 수 없습니다.";
        #endregion
        #region IEnumerable 멤버

        public IEnumerator GetEnumerator()
        {
            throw new System.NotImplementedException();
        }

        #endregion
    }

    /// <summary>
    /// 한국어 번역입니다.
    /// </summary>
    class ko_kr : LocaleString { }
}