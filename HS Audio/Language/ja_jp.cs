﻿namespace HS_Audio.Languge
{
    /// <summary>
    /// 일본 지역 일본어
    /// </summary>
    class ja_jp : LocaleString
    {
        public ja_jp()
        {
            #region 공통
            base.ProgramName = "HS™ プレーヤー";
            base.ProgramArthur = "(Made by 朴弘植)";
            base.파일FToolStripMenuItem = "ファイル";
            base.보기VToolStripMenuItem = "ビュー(&V)";
            base.설정ToolStripMenuItem = "設定(&T)";
            base.열기OToolStripMenuItem = "オープン(&O)";
            base.도움말HToolStripMenuItem = "ヘルプ(&H)";
            base.추가 = "追加";
            base.재생 = "再生";
            base.일시정지 = "一時停止";
            base.정지 = "停止";
            base.파일이름 = "パイル名";
            base.음악경로 = "音楽のパス";
            base.파일경로 = "ファイルのパス";
            base.경로 = "経路";
            base.취소 = "キャンセル";
            base.준비 = "準備";
            base.맨위로설정 = "トップに戻る設定";
            base.끝내기 = "終了(&X)";
            base.닫기 = "閉じる";
            base.상태 = "状態";
            base.언어 = "言語";
            base.삭제 = "削除";
            base.현재 = "現在";
            base.복사 = "コピー";
            base.이름 = "名前";
            base.재생시간 = "再生時間";
            base.디버깅 = "デバッグ";
            base.데이터 = "データ";
            base.크기 = "サイズ";
            base.웨이브 = "ウェーブ";
            base.저장 = "保存";
            base.원본 = "オリジナル";
            base.초기화 = "初期化";
            base.저장중 = "保存中";
            base.오류 = "エラー";
            base.개 = "つ";
            base.좌 = "左";
            base.우 = "右";
            base.채널 = "チャンネル";
            base.이동 = "移動";
            base.다음 = "次";
            base.이전 = "以前";
            base.밀리초표시 = "ミリ秒を表示";
            base.검색어를입력하세요 = "キーワードを入力してください。";
            base.가사 = "家事";
            base.제목 = "タイトル";
            base.앨범 = "アルバム";
            base.아티스트 = "アーティスト";
            base.비트레이트 = "ビットレート";
            base.샘플레이트 = "サンプルレート";
            base.명 = "名";
            #endregion
            #region frmMain.cs
            base.트레이로보내기ToolStripMenuItem = "トレイに送信";
            base.재생목록열기ToolStripMenuItem = "プレイリストを展開される(&L)";
            base.재생목록다른이름으로저장ToolStripMenuItem = "プレイリストを保存(&S)";
            base.폴더열기ToolStripMenuItem = "フォルダを開く";
            base.파일열기ToolStripMenuItem = "ファイルを開く";
             base.파일열기ToolStripMenuItem_Message = "いくつかの項目は、ファイルが存在しないか、\n正しくない音楽ファイルであるため、追加できませんでした。\n" + "より詳細な情報を知りたい場合は、ログを分析してください。\n\n追加しないパス:\n";
             base.파일열기ToolStripMenuItem_MessageTitle = "いくつかの項目追加に失敗！";
            base.하위폴더검색ToolStripMenuItem = "サブフォルダの検索";
            base.유니코드ToolStripMenuItem = "ユニコード";
            base.현재상태저장 = "現在の状態を保存する";
            base.파일로웨이브저장ToolStripMenuItem = "ファイルに現在の再生位置のウェーブ情報を保存";
            base.SaveWaveMessage = "ウェーブ 保存... (ウェーブ サイズ: {0})";
            base.원본사이즈사용ToolStripMenuItem = "オリジナルサイズを入手する";

            base.재생목록창꾸미기ToolStripMenuItem = "プレイリストウィンドウを飾る";
             base.모눈선표시ToolStripMenuItem = "グリッド線を表示";
             base.선택한항목강조표시ToolStripMenuItem = "選択した項目を強調表示";
             base.배경색상설정ToolStripMenuItem = "背景色の設定";
             base.삽입색상설정ToolStripMenuItem = "差し色の設定";
             base.이미지불러오기ToolStripMenuItem = "イメージの読み込み (クリック!)";
              base.이미지불러오기ToolStripMenuItem_Message = "イメージロード失敗!!\n\n詳細例外:\n";
              base.이미지불러오기ToolStripMenuItem_MessageTitle = "イメージロード失敗!!";
             base.타일보기ToolStripMenuItem = "タイル表示";
             base.ImageDialog_Title = "プレイリストウィンドウに使用する画像を開く...";
             base.ImageDialog_Filter = "一般的な画像ファイル|*.jpg;*.jpeg;*.bmp;*.png;*.gif;*.tif;*.tiff;|特殊画像ファイル (*.wmf, *.Exif, *.Emf)|*.wmf;*.Exif;*.Emf";
             base.지우기ToolStripMenuItem = "イメージクリア";
              base.지우기ToolStripMenuItem_Message = "本当にプレイリストの背景画像を削除しますか？";
              base.지우기ToolStripMenuItem_MessageTitle = "プレイリストの背景画像を消去";
            base.핫키설정ToolStripMenuItem = "ショートカット（ホットキー）を設定";
             base.기본프로그램으로설정ToolStripMenuItem = "デフォルトの音楽プログラム設定";
             base.기본프로그램으로설정ToolStripMenuItem_Message = "デフォルトの音楽プログラムでの登録に失敗!!\n\nこのプログラムを管理者権限で実行して再度お試しになるか、\nログを分析してください。\n\n" +
                  "[※メモ※]\nいくつかのワクチンは、ウイルスと診断をするという情報提供がありました。\nしかし、このプログラムは、100％ウイルスがないことを宣言します。\nもし、管理者権限で 実行したがこのメッセージが引き続き表示されたら\n実行中のワクチンのリアルタイムスキャンを切り、再試行してください。";
            base.안전모드ToolStripMenuItem = 안전모드 = "セーフモード";
            base.syncToolStripMenuItem = "Async モード";
            base.음악믹싱모드ToolStripMenuItem = "音楽のミキシングモード";
            base.싱크가사로컬캐시ToolStripMenuItem = "シンク歌詞ローカルキャッシュ";
            base.싱크가사캐시정리ToolStripMenuItem = "シンク歌詞キャッシュのクリーンアップ";
            base.태그없는항목자동삽입ToolStripMenuItem = "タグのない項目の自動挿入";
            base.싱크가사캐시관리ToolStripMenuItem = "シンク歌詞キャッシュ管理";
            base.싱크가사우선순위ToolStripMenuItem = "オンラインシンク歌詞専用";
            base.파일복구및재설치ToolStripMenuItem = "サウンドシステムエンジンファイルの修復と再インストール";
             base.파일복구및재설치ToolStripMenuItem_Message="サウンドシステムエンジンファイルの修復と再インストールをするには、プログラムを再起動しなければなり。\n\nサウンドシステムエンジンファイルの修復と再インストールしますか?";
             base.파일복구및재설치ToolStripMenuItem_MessageTitle = "サウンドシステムエンジンファイルの修復と再インストールの通知";
            base.프로그램다시시작ToolStripMenuItem = "プログラムの再起動";
             base.프로그램다시시작ToolStripMenuItem_Message = "プログラムを続いて、再起動しますか?\n\nYes: 続いて再起動, No: 新たに再起動, Cancle: キャンセル";
             base.프로그램다시시작ToolStripMenuItem_MessageTitle = "プログラムの再起動";
            base.우선순위ToolStripMenuItem = "プログラムの優先順位";
            base.높음ToolStripMenuItem = "高";
            base.높은우선순위ToolStripMenuItem = "このプログラムの優先";
            base.보통권장ToolStripMenuItem = "通常（推奨）";
            base.낮은우선순위ToolStripMenuItem = "他のプログラムの優先";
            base.LOL우선순위ToolStripMenuItem = "LOL まず、（推奨）";
            base.LOL보통권장ToolStripMenuItem = "通常";
            base.가비지커렉션가동ToolStripMenuItem = "ガベージコレクション操作";
             base.가비지커렉션가동ToolStripMenuItem_Message = "本当にガベージコレクションを実行しますか?\n\nアプリケーションがしばらく応答しないことがあります。\n\nGCメモリの割り当て: {0}";
             base.가비지커렉션가동ToolStripMenuItem_MessageTitle = "ガベージコレクション操作 (開発者向け)";

            base.싱크가사찾기ToolStripMenuItem = "シンク歌詞検索";
            base.바탕화면싱크가사창띄우기ToolStripMenuItem = "デスクトップシンク歌詞ウィンドウ表示";
            base.바탕화면싱크가사창닫기ToolStripMenuItem = "デスクトップシンク歌詞ウィンドウを閉じる";
            base.재생상세정보ToolStripMenuItem = "再生詳細情報";
            base.모니터끄기ToolStripMenuItem = "モニターの電源を切る";
            base.로그관리ToolStripMenuItem = "ログ管理";
            base.로그폴더열기ToolStripMenuItem = "ログフォルダを開く";
            base.로그지우기ToolStripMenuItem = "ログのクリア";
             base.로그지우기ToolStripMenuItem_Message = "本当にすべてのログを消去しますか？";
             base.로그지우기ToolStripMenuItem_MessageEx = "ログを消去することができません。\n\nこの理由の詳細:\n";
            base.상태표시줄업데이트ToolStripMenuItem = "ステータスバーの更新";

            base.바탕화면싱크가사창새로고침 = "デスクトップシンク歌詞ウィンドウの更新";

            base.플러그인PToolStripMenuItem = "プラグイン(&P)";

            base.정보ToolStripMenuItem = "プログラム情報...(&A)";
            base.업데이트변경사항보기ToolStripMenuItem = "更新変更を表示(&R)";
            base.업데이트확인ToolStripMenuItem = "更新プログラムの確認 (クリック!)";
            base.업데이트서버설정ToolStripMenuItem = "更新サーバーの設定";
            base.디버깅ToolStripMenuItem = "デバッグ (開発者向け)";
             base.디버깅ToolStripMenuItem_Message = "デバッグをすると、このプログラムは、すぐに停止し、デバッガを起動します。\n\nデバッガが何なのか知らないし、ないコンピュータは使用しないでください。\n\n開発者向けにのみ設計されて。本当にデバッグをしますか？";
             base.디버깅ToolStripMenuItem_MessageTitle = "デバッグ警告";

            base.btn재생설정 = "再生設定";
            base.cb반복 = new string[]{"全繰り返し","ランダム再生","一曲リピート","一度だけ再生","繰り返しなし"};
            base.lbl파일경로 = "ファイルのパス";
            base.목록여는중 = "リストを開いてい";
            base.lblSelectMusicPath = "選択した音楽のパス: ";

            base.닫기_Message = "本当にこのプログラムを終了します?";
            base.닫기_MessageTitle = "プログラム終了のお知らせ";
            base.삭제_Message = "現在再生中の項目です。 再生を停止し削除しますか？";
            base.삭제_MessageTitle = "再生中の項目の削除通知";

            base.새인스턴스시작ToolStripMenuItem = "新 しいインスタンスを 開始";
            base.현재위치의웨이브저장ToolStripMenuItem = "現在の位置のウェーブ保存";
            base.mD5해쉬값구하기ToolStripMenuItem = "MD5ハッシュ値を入手";
            base.가사찾기ToolStripMenuItem = "選択した曲のシンク歌詞検索";
            base.탐색기에서해당파일열기ToolStripMenuItem = "エクスプローラで、ファイルを開く";
            base.재생컨트롤ToolStripMenuItem = "再生コントロール";
            base.재생설정열기ToolStripMenuItem = "再生設定を開く";
            base.메인창열기ToolStripMenuItem = "メインウィンドウを開く";

            base.삭제ToolStripMenuItem_Message = base.삭제ToolStripMenuItem_Message1 = "本当に選択した項目を削除しますか？";
            base.삭제ToolStripMenuItem_MessageTitle = "項目の削除通知";
            base.목록삭제중 = "{0}番目のリストの削除中";
            base.재생목록여는중 = "プレイリストを開いてい";
            base.파일여는중 = "番目のファイルを開いてい";
            base.인코딩 = "エンコード";

            base.폴더열기_Message = "\"*%1*\" フォルダに音楽ファイルがないようです。\n\n[ファイル(F)] - >[オープン](O)]->[フォルダを開く]->[サブフォルダの検索]を\nチェック見て再試行してみてください。";
            base.폴더열기_MessageTitle = "このようにすれば良いの何か?";
            base.목록열기_Message = "いくつかの項目は、ファイルが存在しないため、追加できませんでした。\n\n追加できなかったパス：\n";
            base.목록열기_MessageTitle = "いくつかの項目追加に失敗！（プレイリストの名前:";

            base.글자색상설정ToolStripMenuItem = "文字色の設定";
            base.예제사진ToolStripMenuItem = "サンプル画像(いくつかの写真の好みに注意!)";
             base.크리스마스1ToolStripMenuItem = "クリスマス (イラスト)";
             base.일러스트1ToolStripMenuItem = "佐藤和真 [さとうかずま] (キャラ)";
            　base.일러스트1ToolStripMenuItem_Tooltip = "TVアニメ「この素晴らし世界に祝福お！」 [1920 X 1080]";
            base.일러스트2ToolStripMenuItem = "めぐみん (キャラ)";
             base.일러스트2ToolStripMenuItem_Tooltip = "TVアニメ「この素晴らし世界に祝福お！」 [1980 X 1118]";

            //base.이름_경로 = "[名前: {0} / パス: {1}]";
            #endregion

            #region 음악 태그 관련
            base.아티스트존재X = "アーティスト名はありません。";
           base.앨범존재X = "アルバム名はありません。";
           base.앨범사진존재X = "アルバムカバーの画像がありません。";
           base.앨범커버손상 = "アルバムカバーの画像が破損してい。";
           base.태그뷰어 = "タグビューア";
           base.해상도 = "解像度";
           base.picTagPic_Message = "アルバムカバーの画像が存在しない\n写真を表示することができません。";
           #endregion
        }
    }
}