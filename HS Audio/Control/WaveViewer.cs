using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using HS_Audio;

namespace HS_Audio.Control
{
    /// <summary>
    /// Control for viewing waveforms
    /// </summary>
    public class HSWaveViewer : System.Windows.Forms.UserControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.Container components = null;
        private HSWaveStream waveStream;
        private int samplesPerPixel = 128;
        private int startPosition;
        private int bytesPerSample;
        /// <summary>
        /// Creates a new WaveViewer control
        /// </summary>
        public HSWaveViewer()
        {
            // This call is required by the Windows.Forms Form Designer.
            InitializeComponent();
            this.DoubleBuffered = true;			

        }

        /// <summary>
        /// sets the associated wavestream
        /// </summary>
        public HSWaveStream WaveStream
        {
            get
            {
                return waveStream;
            }
            set
            {
                waveStream = value;
                if (waveStream != null)
                {
                    bytesPerSample = (waveStream.WaveFormat.BitsPerSample / 8) * waveStream.WaveFormat.Channels;
                }
            }
        }

        /// <summary>
        /// The zoom level, in samples per pixel
        /// </summary>
        [Description()]
        public int SamplesPerPixel
        {
            get
            {
                return samplesPerPixel;
            }
            set
            {
                samplesPerPixel = value;
                this.Invalidate();
            }
        }

        /// <summary>
        /// Start position (currently in bytes)
        /// </summary>
        public int StartPosition
        {
            get
            {
                return startPosition;
            }
            set
            {
                startPosition = value;
            }
        }

        public void Draw(){ this.Invalidate();}

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose( bool disposing )
        {
            if( disposing )
            {
                if(components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose( disposing );
        }

        /// <summary>
        /// <see cref="Control.OnPaint"/>
        /// </summary>
        protected override void OnPaint(PaintEventArgs e)
        {
            if(waveStream != null)
            {
                waveStream.Position = 0;
                int bytesRead = 0; System.IO.MemoryStream ms;
                byte[] waveData = new byte[samplesPerPixel*bytesPerSample];
                waveStream.Position = startPosition + (e.ClipRectangle.Left * bytesPerSample * samplesPerPixel);
                
                for(float x = e.ClipRectangle.X; x < e.ClipRectangle.Right; x+=1)
                {
                    short low = 0;
                    short high = 0;
                    bytesRead = waveStream.Read(waveData, 0);//, samplesPerPixel * bytesPerSample);
                    if(bytesRead == 0)
                        break;
                    for(int n = 0; n < bytesRead; n+=2)
                    {
                        short sample = n>=waveData.Length?(short)0:BitConverter.ToInt16(waveData, n);
                        if(sample < low) low = sample;
                        if(sample > high) high = sample;
                    }
                    float lowPercent = ((((float) low) - short.MinValue) / ushort.MaxValue);
                    float highPercent = ((((float) high) - short.MinValue) / ushort.MaxValue);
                    e.Graphics.DrawLine(Pens.Black,x,this.Height * lowPercent,x,this.Height * highPercent);					
                } 
            }
            base.OnPaint (e);
        }


        #region Component Designer generated code
        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();
        }
        #endregion
    }

}
