﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using System.Threading;

namespace HS_Audio.Control
{
    public partial class ProgressBarGradientDecibel : ProgressBarGradientInternal
    {
        Thread SmoothThread;
        ThreadStart SmoothThreadStart;

        public ProgressBarGradientDecibel()
        {
            SmoothThreadStart = new ThreadStart(Process);
        }

        private void ProgressBarGradientDecibel_Load(object sender, EventArgs e)
        {

        }

        #region Properties

        bool _Smooth;
        [Category("Me")]
        public bool Smooth
        {
            get{return _Smooth;}
            set
            {
                _Smooth = value;
                if (SmoothThread != null) try { SmoothThread.Abort(); } catch (Exception ex) { }
                if (value)
                {
                    SmoothThread = new Thread(SmoothThreadStart);
                    SmoothThread.Start();
                }
            }
        }

        float _MindB = -60;
        [Category("Me")]
        public new float MindB
        {
            get {return _MindB;}
            set
            {
                if (value >= MaxdB) return;
                if (_Value > value) _Value = value;
                _MindB = value;
                base.MindB = (int)value;
            }
        }
        float _MaxdB = 0;
        [Category("Me")]
        public new float MaxdB
        {
            get{return _MaxdB; }
            set
            {
                if (value <= MindB) return; 
                if(Value > value)Value = value;
                _MaxdB = value;
                base.MaxdB = (int)value;
                base.MaximumValue = (int)(MaxdB*Block);
            }
        }
        float _Value;
        [Category("Me")]
        public float Value
        {
            get{return _Value;}
            set
            {
                if (value < MindB) _Value = MindB;
                else if(value > MaxdB) _Value = MaxdB;
                else _Value = value;
                if(!Smooth)Process();
            }
        }

        uint _Block = 1024;
        [Category("Me")]
        public uint Block
        {
            get {return _Block; }
            set
            {
                _Block = value;
                base.MaximumValue = (int)(MaxdB * Block);
            }
        }

        private int sampleDelay = 30;
        [Category("Me")]
        public int ReleaseTime
        {
            get{return sampleDelay; }
            set
            {
                sampleDelay = Math.Max(0, value);
                if (frameDelay > sampleDelay)
                    frameDelay = sampleDelay;
            }
        }

        private int frameDelay = 10;
        [Category("Me")]
        public int AttackTime
        {
            get {return frameDelay; }
            set{ frameDelay = Math.Max(0, Math.Min(sampleDelay, value)); }
        }


        [Category("Me")]
        public new int MaximumValue
        {
            get {return base.MaximumValue; }
            private set { base.MaximumValue = (int)(MaxdB*Block); }
        }
        [Category("Me")]
        public new int MinimumValue{ get; private set;}

        #endregion

        void Process()
        {
            while (true)
            {
                WORK:
                try
                {
                    int tempFrameDelay = frameDelay;
                    int tempSampleDelay = sampleDelay;

                    int MaxValue = MaximumValue;

                    // for each channel, determine the step size necessary for each iteration
                    int Goal = 0;

                    #region
                    {
                        double aa = Block * Value;
                        aa = Math.Max(aa, -MaxValue);
                        int b = (int)(MaxValue + aa);

                        if (Smooth) Goal = (int)Math.Min(b, MaxValue);
                        else
                        {
                            base.Value = (int)Math.Min(b, MaxValue);
                            Thread.Sleep(AttackTime);
                            return;
                            //goto WORK;
                        }
                    }
                    #endregion


                    double range1 = Goal - base.Value;

                    double exactValue1 = base.Value;

                    double stepSize1 = range1 / tempSampleDelay * tempFrameDelay;
                    if (Math.Abs(stepSize1) < .01)
                    {
                        stepSize1 = Math.Sign(range1) * .01;
                    }
                    double absStepSize1 = Math.Abs(stepSize1);

                    // increment/decrement the bars' values until both equal their desired goals,
                    // sleeping between iterations
                    if (base.Value == Goal) Thread.Sleep(tempSampleDelay);
                    else
                    {
                        do
                        {
                            if (base.Value != Goal)
                            {
                                if (absStepSize1 < Math.Abs(Goal - base.Value))
                                {
                                    exactValue1 += stepSize1;
                                    base.Value = (int)Math.Round(exactValue1);
                                }
                                else base.Value = Goal;
                            }

                            Thread.Sleep(tempFrameDelay);
                        } while (base.Value != Goal);
                    }
                }
                catch (ThreadAbortException ex) { }
                catch (Exception ex)
                {
                    Thread.Sleep(100);
                }
            }
        }
    }
}
