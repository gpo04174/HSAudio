﻿using System;
using System.Windows.Forms;
using HS_CSharpUtility.Extension;

namespace HS_Audio.Control
{
    public partial class TriangleControl : UserControl
    {
        public TriangleControl()
        {
            InitializeComponent();
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            e.Graphics.Rotate(45, this.Width, this.Height);
            //base.OnPaint(e);
        }
        private void TriangleControl_Load(object sender, EventArgs e)
        {

        }
    }
}
