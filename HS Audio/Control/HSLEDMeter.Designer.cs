﻿namespace HS_Audio.Control
{
    partial class HSLEDMeter
    {
        /// <summary> 
        /// 필수 디자이너 변수입니다.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 사용 중인 모든 리소스를 정리합니다.
        /// </summary>
        /// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 구성 요소 디자이너에서 생성한 코드

        /// <summary> 
        /// 디자이너 지원에 필요한 메서드입니다. 
        /// 이 메서드의 내용을 코드 편집기로 수정하지 마십시오.
        /// </summary>
        private void InitializeComponent()
        {
            this.ledBulb7 = new HS_Audio.Control.LedBulb();
            this.ledBulb8 = new HS_Audio.Control.LedBulb();
            this.ledBulb5 = new HS_Audio.Control.LedBulb();
            this.ledBulb6 = new HS_Audio.Control.LedBulb();
            this.ledBulb3 = new HS_Audio.Control.LedBulb();
            this.ledBulb4 = new HS_Audio.Control.LedBulb();
            this.ledBulb2 = new HS_Audio.Control.LedBulb();
            this.ledBulb1 = new HS_Audio.Control.LedBulb();
            this.SuspendLayout();
            // 
            // ledBulb7
            // 
            this.ledBulb7.BackColor = System.Drawing.Color.Transparent;
            this.ledBulb7.Location = new System.Drawing.Point(3, 323);
            this.ledBulb7.Name = "ledBulb7";
            this.ledBulb7.On = true;
            this.ledBulb7.Size = new System.Drawing.Size(24, 24);
            this.ledBulb7.TabIndex = 7;
            this.ledBulb7.Text = "ledBulb7";
            // 
            // ledBulb8
            // 
            this.ledBulb8.BackColor = System.Drawing.Color.Transparent;
            this.ledBulb8.Location = new System.Drawing.Point(3, 293);
            this.ledBulb8.Name = "ledBulb8";
            this.ledBulb8.On = true;
            this.ledBulb8.Size = new System.Drawing.Size(24, 24);
            this.ledBulb8.TabIndex = 6;
            this.ledBulb8.Text = "ledBulb8";
            // 
            // ledBulb5
            // 
            this.ledBulb5.BackColor = System.Drawing.Color.Transparent;
            this.ledBulb5.Location = new System.Drawing.Point(3, 380);
            this.ledBulb5.Name = "ledBulb5";
            this.ledBulb5.On = true;
            this.ledBulb5.Size = new System.Drawing.Size(24, 24);
            this.ledBulb5.TabIndex = 5;
            this.ledBulb5.Text = "ledBulb5";
            // 
            // ledBulb6
            // 
            this.ledBulb6.BackColor = System.Drawing.Color.Transparent;
            this.ledBulb6.Location = new System.Drawing.Point(3, 351);
            this.ledBulb6.Name = "ledBulb6";
            this.ledBulb6.On = true;
            this.ledBulb6.Size = new System.Drawing.Size(24, 24);
            this.ledBulb6.TabIndex = 4;
            this.ledBulb6.Text = "ledBulb6";
            // 
            // ledBulb3
            // 
            this.ledBulb3.BackColor = System.Drawing.Color.Transparent;
            this.ledBulb3.Location = new System.Drawing.Point(3, 437);
            this.ledBulb3.Name = "ledBulb3";
            this.ledBulb3.On = true;
            this.ledBulb3.Size = new System.Drawing.Size(24, 24);
            this.ledBulb3.TabIndex = 3;
            this.ledBulb3.Text = "ledBulb3";
            // 
            // ledBulb4
            // 
            this.ledBulb4.BackColor = System.Drawing.Color.Transparent;
            this.ledBulb4.Location = new System.Drawing.Point(3, 407);
            this.ledBulb4.Name = "ledBulb4";
            this.ledBulb4.On = true;
            this.ledBulb4.Size = new System.Drawing.Size(24, 24);
            this.ledBulb4.TabIndex = 2;
            this.ledBulb4.Text = "ledBulb4";
            // 
            // ledBulb2
            // 
            this.ledBulb2.BackColor = System.Drawing.Color.Transparent;
            this.ledBulb2.Location = new System.Drawing.Point(3, 494);
            this.ledBulb2.Name = "ledBulb2";
            this.ledBulb2.On = true;
            this.ledBulb2.Size = new System.Drawing.Size(24, 24);
            this.ledBulb2.TabIndex = 1;
            this.ledBulb2.Text = "ledBulb2";
            // 
            // ledBulb1
            // 
            this.ledBulb1.BackColor = System.Drawing.Color.Transparent;
            this.ledBulb1.Location = new System.Drawing.Point(3, 464);
            this.ledBulb1.Name = "ledBulb1";
            this.ledBulb1.On = true;
            this.ledBulb1.Size = new System.Drawing.Size(24, 24);
            this.ledBulb1.TabIndex = 0;
            this.ledBulb1.Text = "ledBulb1";
            // 
            // HSLEDMeter
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackgroundImage = global::HS_Audio.Properties.Resources.BG_Wood1;
            this.Controls.Add(this.ledBulb7);
            this.Controls.Add(this.ledBulb8);
            this.Controls.Add(this.ledBulb5);
            this.Controls.Add(this.ledBulb6);
            this.Controls.Add(this.ledBulb3);
            this.Controls.Add(this.ledBulb4);
            this.Controls.Add(this.ledBulb2);
            this.Controls.Add(this.ledBulb1);
            this.Name = "HSLEDMeter";
            this.Size = new System.Drawing.Size(87, 520);
            this.Load += new System.EventHandler(this.HSLEDMeter_Load);
            this.Resize += new System.EventHandler(this.HSLEDMeter_Resize);
            this.ResumeLayout(false);

        }

        #endregion

        private HS_Audio.Control.LedBulb ledBulb1;
        private HS_Audio.Control.LedBulb ledBulb2;
        private HS_Audio.Control.LedBulb ledBulb3;
        private HS_Audio.Control.LedBulb ledBulb4;
        private HS_Audio.Control.LedBulb ledBulb5;
        private HS_Audio.Control.LedBulb ledBulb6;
        private HS_Audio.Control.LedBulb ledBulb7;
        private HS_Audio.Control.LedBulb ledBulb8;
    }
}
