﻿using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using System.Threading;
using System.ComponentModel;

namespace HS_Audio.Control
{
    public interface IHSWaveDraw
    {
        bool DoubleBuffer{get;set;}
        float[] Data{get;}
        Pen pen{get;set;}
        Color DrawColor{get;set;}

        bool DrawRow{get;set;}
        Image ForegroundImage{get;set;}
        Color DrawLineColor{get;set;}
        bool DrawLine{get;set;}

        [Category("Row"), Description("모눈선에 대한 정의 입니다.")]
        Row Row{get;set;}

        int BORDER_WIDTH{get;set;}

        [Category("그리기"), Description("사용자가 직접그릴지의 여부 입니다."), DefaultValue(false)]
        bool CustomDraw{get;set;}

        void DrawNormalizedAudio();
        void DrawNormalizedAudio(float[] data);
        void DrawNormalizedAudio(List<float> Data);
        void DrawNormalizedAudio(float[] data, Pen pen, Color BGColor, int BORDER_WIDTH = 0);
        void DrawNormalizedAudio(List<float> Data, Pen pen, Color BGColor, int BORDER_WIDTH = 0);
        void ReDraw();

        [Category("그리기"), Description("점과 점사이에 선을 추가합니다. 이 기능을 끄면 그리기 성능이 향상될 수 있습니다."), DefaultValue(true)]
        bool AddLine{get;set;}
        //public Bit
    }
}
