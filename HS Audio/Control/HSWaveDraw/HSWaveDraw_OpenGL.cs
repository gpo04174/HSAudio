﻿using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using System.Threading;
using System.ComponentModel;
using SharpGL;

namespace HS_Audio.Control
{
    public partial class HSWaveDraw_OpenGL : OpenGLControl, IHSWaveDraw
    {
        public HSWaveDraw_OpenGL()
        {
            InitializeComponent();
            base.BackColor = Color.White;
            
            OpenGLDraw += new SharpGL.RenderEventHandler(this.DrawWave_GL);
        }

        Point[] pt = new Point[2];
        //List<float> _Data = new List<float>();
        float[] _Data = new float[0];

        Pen _pen = new Pen(Color.Navy);
        //Color _BGColor = Color.White;

        public bool DoubleBuffer{get{return base.DoubleBuffered;}set{base.DoubleBuffered=value;}}
        public float[] Data{get{return _Data;}}//_Data.ToArray();}}
        public Pen pen{get{return _pen;}set{if(value!=null)_pen = value;}}
        //public new Color BackColor{get{return _BGColor;}set{_BGColor = value;base.BackColor = value;}}
        private new Color ForeClor{get;set;}
        public Color DrawColor{get{return pen.Color;}set{pen.Color = value;}}

        bool _DrawRow = true;
        public bool DrawRow{get{return _DrawRow;}set{_DrawRow = value;try{this.Invalidate();}catch{}}}

        public Image ForegroundImage{get;set;}

        Pen _DrawLineColor = new Pen(Color.Navy);
        public Color DrawLineColor{get{return _DrawLineColor.Color;}set{_DrawLineColor.Color = value;ReDraw();}}

        bool _DrawLine = true;
        public bool DrawLine{get{return _DrawLine;}set{_DrawLine = value;ReDraw();}}

        Row _Row = new Row(Color.FromArgb(200, 128, 128, 128), 30);
        [Category("Row"), Description("모눈선에 대한 정의 입니다.")]
        public Row Row{get{return _Row;} set{_Row = value;try{this.Invalidate();}catch{}}}
        public int BORDER_WIDTH{get;set;}

        bool _CustomDraw = false;
        [Category("그리기"), Description("사용자가 직접그릴지의 여부 입니다."), DefaultValue(false)]
        public bool CustomDraw{get{return _CustomDraw;} set{_CustomDraw = value;try{this.Invalidate();}catch{}}}

        public void DrawNormalizedAudio(){this.Invalidate();}
        public void DrawNormalizedAudio(/*ref */float[] data)
        {
            //if (_Data != null) _Data = new List<float>(); this._Data.Clear(); this._Data.AddRange(data);
            this._Data = data;
            if(bmp!=null)bmp.Dispose();
            bmp = null;
            Bitmap bmp1 = new Bitmap(this.Width, this.Height);
            using(Graphics g = Graphics.FromImage(bmp1))
            {
                //try{if(ForegroundImage!=null)g.DrawImage(ForegroundImage,0,0);}catch{}
                //Row.Drawing(g, this.Width, this.Height, false);
                
                if (_Data != null && _Data.Length > 0 &&!CustomDraw)DrawWave(g, _Data, bmp1.Width, bmp1.Height);
            }
            //this.BackgroundImage = bmp1;
            this.bmp = bmp1;
            this.Invalidate();
        }
        public void DrawNormalizedAudio(List<float> Data)
        {
            this._Data = Data.ToArray();
            if(bmp!=null)bmp.Dispose();
            bmp = null;
            Bitmap bmp1 = new Bitmap(this.Width, this.Height);
            using(Graphics g = Graphics.FromImage(bmp1)){try{if(ForegroundImage!=null)g.DrawImage(ForegroundImage,0,0);}catch{}if (_Data != null && _Data.Length > 0 &&!CustomDraw)DrawWave(g, _Data, bmp1.Width, bmp1.Height);}
            //this.BackgroundImage = bmp1;
            this.bmp = bmp1;
            this.Invalidate();
        }
        public void DrawNormalizedAudio(/*ref */float[] data, Pen pen, Color BGColor, int BORDER_WIDTH = 0)
        {
            if(_Data!=null)_Data =data;
            //this._Data.Clear();this._Data.AddRange(data); 
            this.pen = pen; this.BackColor = BGColor; this.BORDER_WIDTH = BORDER_WIDTH;
            this.Invalidate();
        }
        public void DrawNormalizedAudio(List<float> Data, Pen pen, Color BGColor, int BORDER_WIDTH = 0)
        {
            this._Data = Data.ToArray(); this.pen = pen; this.BackColor = BGColor; this.BORDER_WIDTH = BORDER_WIDTH;
            this.Invalidate();
        }
        public void ReDraw(){try{this.Invalidate();}catch{}}

        Bitmap bmp;
        int width, height;
        private void DrawWave_GL(object sender, RenderEventArgs e)
        {
            int CurrentWidth = this.Width,
                CurrentHeight = this.Height;
            try
            {
                Graphics g = e.Graphics;
                if (CustomDraw) g.Clear(this.BackColor);
                try{if (this.ForegroundImage != null) g.DrawImage(this.ForegroundImage, 0, 0);}catch{}
                if (DrawRow/* && (width != this.Width || height != this.Height)*/) Row.Drawing(g, CurrentWidth, CurrentHeight, false);
                if (DrawLine /*&& (width != this.Width || height != this.Height)*/) g.DrawLine(_DrawLineColor, 0, CurrentHeight / 2, CurrentWidth, CurrentHeight / 2);

                width = CurrentWidth;// - (2 * BORDER_WIDTH);
                height = CurrentHeight;// - (2 * BORDER_WIDTH); 

                //int BORDER_WIDTH = 5;
                if (bmp != null && !CustomDraw) g.DrawImage(bmp, 0, 0);
                //else base.OnPaint(new PaintEventArgs(e.Graphics, new Rectangle(0, 0, CurrentWidth, CurrentHeight)));
            }catch{/*base.OnPaint(new PaintEventArgs(e.Graphics, new Rectangle(0, 0, CurrentWidth, CurrentHeight)));*/}
        }
        protected void DrawWave(Graphics g, List<float> Data, int width, int height)
        {
            //if (CustomDraw)
            {
                int size = Data.Count;
                for (int iPixel = 0; iPixel < width; iPixel++)
                {
                    // determine start and end points within WAV
                    int start = (int)((float)iPixel * ((float)size / (float)width));
                    int end = (int)((float)(iPixel + 1) * ((float)size / (float)width));
                    float min = float.MaxValue;
                    float max = float.MinValue;
                    for (int i = start; i < end; i++)
                    {
                        float val = Data[i];
                        min = val < min ? val : min;
                        max = val > max ? val : max;
                    }
                    int yMax = BORDER_WIDTH + height - (int)((max + 1) * .5 * height);
                    int yMin = BORDER_WIDTH + height - (int)((min + 1) * .5 * height);
                    pt[0].X = pt[1].X = iPixel + BORDER_WIDTH;
                    pt[0].Y = yMax;
                    pt[1].Y = yMin;
                    g.DrawLines(pen, pt);
                    //return;
                }
            }
        }
        Point[] BeforePoint = new Point[2];
        Point[] AfterPoint = new Point[2];
        List<Point[]> Buf = new List<Point[]>();

        public bool AddLine{get;set;}

        protected void DrawWave(Graphics g, float[] Data, int width, int height)
        {
            //if (CustomDraw)
            {
                bool OverBuffer =!AddLine;// (width*3)<Data.Length;
                if (!OverBuffer){BeforePoint[0].X = BeforePoint[1].X = -1; BeforePoint[0].Y = BeforePoint[1].Y =height/2;}
                //Buf.Clear();
                int size = Data.Length;
                for (int iPixel = 0; iPixel < width; iPixel++)
                {
                    // determine start and end points within WAV
                    int start = (int)((float)iPixel * ((float)size / (float)width));
                    int end = (int)((float)(iPixel + 1) * ((float)size / (float)width));
                    float min = float.MaxValue;
                    float max = float.MinValue;
                    for (int i = start; i < end; i++)
                    {
                        float val = Data[i];
                        min = val < min ? val : min;
                        max = val > max ? val : max;
                    }
                    int yMax = BORDER_WIDTH + height - (int)((max + 1) * .5 * height);
                    int yMin = BORDER_WIDTH + height - (int)((min + 1) * .5 * height);
                    pt[0].X = pt[1].X = iPixel + BORDER_WIDTH;
                    pt[0].Y = yMax;
                    pt[1].Y = yMin;

                    if (!OverBuffer && pt[1].Y>-1)
                    {
                        //Point[] p = new Point[2]{pt[0], pt[1]};
                        //Buf.Add(p);
                        g.DrawLine(pen, BeforePoint[0], pt[1]);
                        BeforePoint[0].X = BeforePoint[1].X = iPixel + BORDER_WIDTH;
                        BeforePoint[0].Y = yMax;
                        BeforePoint[1].Y = yMin;
                    }
                    /*
                    if (pt[0].Y > 0&&iPixel>0)
                    {
                        if ((pt[0].X - BeforePoint[0].X) > 1)
                        {
                            //AfterPoint.X = 
                        }
                    }*/
                    g.DrawLines(pen, pt);
                    //return;
                }
            }
        }
        //public Bit

        #region 구성 요소 디자이너에서 생성한 코드
        /// <summary> 
        /// 필수 디자이너 변수입니다.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 사용 중인 모든 리소스를 정리합니다.
        /// </summary>
        /// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }


        /// <summary> 
        /// 디자이너 지원에 필요한 메서드입니다. 
        /// 이 메서드의 내용을 코드 편집기로 수정하지 마십시오.
        /// </summary>
        private void InitializeComponent()
        {
            this.SuspendLayout();
            // 
            // HSWaveDraw
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.Name = "HSWaveDraw";
            this.Load += new System.EventHandler(this.HSWaveDraw_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private void HSWaveDraw_Load(object sender, System.EventArgs e)
        {

        }
    }
}
