﻿///////////////////////////////////////////////////////////////////////////////
//  Copyright (c) 2008 Ernest Laurentin (elaurentin@netzero.net)
//
// This software is provided 'as-is', without any express or implied
// warranty. In no event will the authors be held liable for any damages
// arising from the use of this software.
//
// Permission is granted to anyone to use this software for any purpose,
// including commercial applications, and to alter it and redistribute it
// freely, subject to the following restrictions:
//
// 1. The origin of this software must not be misrepresented; you must not
// claim that you wrote the original software. If you use this software
// in a product, an acknowledgment in the product documentation would be
// appreciated but is not required.
//
// 2. Altered source versions must be plainly marked as such, and must not be
// misrepresented as being the original software.
//
// 3. This notice may not be removed or altered from any source distribution.
///////////////////////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using System.Threading;

namespace Ernzo.WinForms.Controls
{
    public delegate void PropertiesChangedEventHandler(string Name, Type type, object Value);
    public enum PeakMeterStyle
    {
        PMS_Horizontal = 0,
        PMS_Vertical   = 1
    }

    public struct PeakMeterData
    {
        public int Value;
        public int Falloff;
        public int Speed;
    }

    [ToolboxBitmap(typeof(MyResourceNamespace), "PeakMeterCtrl.pmicon.bmp"),
     DefaultEvent("PropertiesChanged")]
    public partial class PeakMeterCtrl : Control
    {
        private const byte DarkenByDefault = 40;
        private const byte LightenByDefault = 150;
        private const int MinRangeDefault = 70;
        private const int MedRangeDefault = 90;
        private const int MaxRangeDefault = 100;
        private const int FalloffFast = 1;
        private const int FalloffNormal = 10;
        private const int FalloffSlow = 100;
        private const int FalloffDefault = 20;
        private const int DecrementPercent = 10;
        private const int BandsMin = 1;
        private const int BandsMax = 1000;
        private const int BandsDefault = 8;
        private const int LEDMin  = 1;
        private const int LEDMax  = 1000;
        private const int LEDDefault = 8;
        private const int cxyMargin = 1;

        private int _AnimDelay;
        private int _MinRangeValue;
        private int _MedRangeValue;
        private int _MaxRangeValue;
        private PeakMeterData[] _meterData;
        private System.Threading.Timer _animationTimer;
        public PeakMeterCtrl()
        {
            InitializeComponent();
            this.DoubleBuffered = true;
            this.SetStyle(ControlStyles.OptimizedDoubleBuffer, true);
            this.SetStyle(ControlStyles.AllPaintingInWmPaint, true);
            this.SetStyle(ControlStyles.UserPaint, true);
            this.SetStyle(ControlStyles.SupportsTransparentBackColor, true);
            InitDefault();
            //this.SetStyle(ControlStyles.AllPaintingInWmPaint, true);
        }

        private void InitDefault()
        {
            _AnimDelay = Timeout.Infinite;
            _MinRangeValue = MinRangeDefault; // [0,60[
            _MedRangeValue = MedRangeDefault; // [60,80[
            _MaxRangeValue = MaxRangeDefault; // [80,100[
            ti.Interval = 20;
            _meterData = null;
            _animationTimer = null;
            _모눈선표시 = true;
            _색상표시 = false;
            _모눈선색상 = Color.Gainsboro;
            _ColorNormal = Color.Green;
            _ColorMedium = Color.Yellow;
            _ColorHigh = Color.Red;
            _ColorNormalBack = LightenColor(_ColorNormal, LightenByDefault);
            _ColorMediumBack = LightenColor(_ColorMedium, LightenByDefault);
            _ColorHighBack = LightenColor(_ColorHigh, LightenByDefault);
            _BandsCount = BandsDefault;
            _LED눈금갯수 = LEDDefault;
            _Peak속도 = FalloffNormal;
            _Peak표시 = true;
            _Peak색상 = DarkenColor(_모눈선색상, DarkenByDefault);
            _Peak굵기 = 1;
            ResetControl();
        }

        #region Control properties

        [Category("동작"), Description("컨트롤의 속성이 변경될 발생합니다.")]
        public event PropertiesChangedEventHandler PropertiesChanged;

        private PeakMeterStyle _PmsMeterStyle;
        [Category("Appearance"), DefaultValue(PeakMeterStyle.PMS_Horizontal)]
        public PeakMeterStyle MeterStyle
        {
            get { return _PmsMeterStyle; }
            set { if(MeterStyle!=value){_PmsMeterStyle = value; Refresh(); if (PropertiesChanged != null) { PropertiesChanged("MeterStyle", typeof(PeakMeterStyle), value); } }}
        }

        private bool _모눈선표시;
        [Category("Appearance"), DefaultValue(true)]
        public bool 모눈선표시
        {
            get { return _모눈선표시; }
            set { if (_모눈선표시 != value) { _모눈선표시 = value; Refresh(); if (PropertiesChanged != null) { PropertiesChanged("모눈선표시", typeof(bool), value); } } }
        }

        private bool _색상표시;
        [Category("Appearance"), DefaultValue(false)]
        public bool 색상표시
        {
            get { return _색상표시; }
            set { if (_색상표시 != value) { _색상표시 = value; Refresh(); if (PropertiesChanged != null) { PropertiesChanged("색상표시", typeof(bool), value); } } }
        }

        private Color _모눈선색상;
        [Category("Appearance")]
        public Color 모눈선색상
        {
            get { return _모눈선색상; }
            set { if(_모눈선색상!=value){_모눈선색상 = value; /*Refresh(); if (PropertiesChanged != null) { PropertiesChanged("모눈선색상",typeof(Color), value); }*/ }}
        }

        private Color _ColorNormal;
        [Category("Appearance")]
        public Color ColorNormal
        {
            get { return _ColorNormal; }
            set { if (_ColorNormal != value) { _ColorNormal = value; /*Refresh(); if (PropertiesChanged != null) { PropertiesChanged("ColorNormal", typeof(Color), value); }*/ } }
        }

        private Color _ColorMedium;
        [Category("Appearance")]
        public Color ColorMedium
        {
            get { return _ColorMedium; }
            set { if (_ColorMedium != value) { _ColorMedium = value; /*Refresh(); if (PropertiesChanged != null) { PropertiesChanged("ColorMedium", typeof(Color), value); }*/ } }
        }

        private Color _ColorHigh;
        [Category("Appearance")]
        public Color ColorHigh
        {
            get { return _ColorHigh; }
            set { if (_ColorHigh != value) { _ColorHigh = value; /*Refresh(); if (PropertiesChanged != null) { PropertiesChanged("ColorHigh", typeof(Color), value); }*/ } }
        }

        private Color _ColorNormalBack;
        [Category("Appearance")]
        public Color ColorNormalBack
        {
            get { return _ColorNormalBack; }
            set { if (_ColorNormalBack != value) { _ColorNormalBack = value; Refresh(); if (PropertiesChanged != null) { PropertiesChanged("ColorNormalBack", typeof(Color), value); } } }
        }

        private Color _ColorMediumBack;
        [Category("Appearance")]
        public Color ColorMediumBack
        {
            get { return _ColorMediumBack; }
            set { if (_ColorMediumBack != value) { _ColorMediumBack = value; Refresh(); if (PropertiesChanged != null) { PropertiesChanged("ColorMediumBack", typeof(Color), value); } } }
        }

        private Color _ColorHighBack;
        [Category("Appearance")]
        public Color ColorHighBack
        {
            get { return _ColorHighBack; }
            set { if (_ColorHighBack != value) { _ColorHighBack = value; Refresh(); if (PropertiesChanged != null) { PropertiesChanged("ColorHighBack", typeof(Color), value); } } }
        }

        private int _BandsCount;
        [Category("Appearance"), DefaultValue(BandsDefault)]
        public int BandsCount
        {
            get { return _BandsCount; }
            set {
                if (_BandsCount != value)
                {
                    if (value >= BandsMin && value <= BandsMax)
                    {
                        _BandsCount = value;
                        ResetControl();
                        Refresh();
                        if (PropertiesChanged != null) { PropertiesChanged("BandsCount", typeof(int), value); }
                    }
                }
            }
        }

        private int _LED눈금갯수;
        [Category("Appearance"), DefaultValue(LEDDefault)]
        public int LED눈금갯수
        {
            get { return _LED눈금갯수; }
            set {
                if (_LED눈금갯수 != value)
                {
                    if (value >= LEDMin && value <= LEDMax)
                    {
                        _LED눈금갯수 = value;
                        Refresh();
                        if (PropertiesChanged != null) { PropertiesChanged("LED눈금갯수", typeof(int), value); }
                    }
                }
            }
        }

        private int _Peak속도=30;
        [Category("Peak설정"), DefaultValue(FalloffDefault)]
        public int Peak속도
        {
            get { return _Peak속도; }
            set { _Peak속도 = value; if (PropertiesChanged != null) { PropertiesChanged("Peak속도", typeof(int), value); } }
        }

        private bool _Peak표시;
        [Category("Peak설정"), DefaultValue(true)]
        public bool Peak표시
        {
            get { return _Peak표시; }
            set { _Peak표시 = value; if (PropertiesChanged != null) { PropertiesChanged("Peak표시", typeof(bool), value); } }
        }

        private Color _Peak색상;
        [Category("Peak설정")]
        public Color Peak색상
        {
            get { return _Peak색상; }
            set { _Peak색상 = value; if (PropertiesChanged != null) { PropertiesChanged("Peak색상", typeof(Color), value); } }
        }
        private int _Peak굵기;
        [Category("Peak설정"), DefaultValue(1)]
        public int Peak굵기
        {
            get { return _Peak굵기; }
            set { _Peak굵기 = value; if (PropertiesChanged != null) { PropertiesChanged("Peak굵기", typeof(int), value); } }
        }
	
        #endregion

        [Browsable(false)]
        public bool IsActive
        {
            get { return (_animationTimer != null); }
        }

        #region Control Methods

        // support for thread-safe version
        delegate bool StartDelegate(int delay);
        /// <summary>
        /// Start animation
        /// </summary>
        /// <param name="delay"></param>
        /// <returns></returns>
        public void Start(int delay)
        {
            if (this.InvokeRequired)
            {
                //StartDelegate startDelegate = new StartDelegate(this.Start);
                //return (bool)this.Invoke(startDelegate, delay);
            }
            ti.Interval = delay;
            _AnimDelay = delay;
            //return StartAnimation(delay);
        }

        // support for thread-safe version
        delegate bool StopDelegate();
        /// <summary>
        /// Stop Animation
        /// </summary>
        /// <returns></returns>
        public bool Stop()
        {
            if (this.InvokeRequired)
            {
                StopDelegate stopDelegate = new StopDelegate(this.Stop);
                return (bool)this.Invoke(stopDelegate);
            }
            _AnimDelay = Timeout.Infinite;
            return StopAnimation();
        }

        /// <summary>
        /// Set number of LED bands
        /// </summary>
        /// <param name="BandsCount">Number of bands</param>
        /// <param name="LED눈금갯수">Number of LED per bands</param>
        public void SetMeterBands(int BandsCount, int LED눈금갯수)
        {
            if (BandsCount < BandsMin || BandsCount > BandsMax)
                throw new ArgumentOutOfRangeException("BandsCount");
            if (LED눈금갯수 < LEDMin || LED눈금갯수 > LEDMax)
                throw new ArgumentOutOfRangeException("LED눈금갯수");
            _BandsCount = BandsCount;
            _LED눈금갯수 = LED눈금갯수;
            ResetControl();
            Refresh();
        }

        /// <summary>
        /// Set range info
        /// </summary>
        /// <param name="minRangeVal">Min Range</param>
        /// <param name="medRangeVal">Medium Range</param>
        /// <param name="maxRangeVal">High Range</param>
        public void SetRange(int minRangeVal, int medRangeVal, int maxRangeVal)
        {
        	if (maxRangeVal <= medRangeVal || medRangeVal < minRangeVal )
                throw new ArgumentOutOfRangeException("minRangeVal");
            _MinRangeValue = minRangeVal;
            _MedRangeValue = medRangeVal;
            _MaxRangeValue = maxRangeVal;
            ResetControl();
            Refresh();
        }

        // support for thread-safe version
        private delegate bool SetDataDelegate(int[] arrayValue, int offset, int size, bool throwEX);
        /// <summary>
        /// Set meter band value
        /// </summary>
        /// <param name="arrayValue">Array value for the bands</param>
        /// <param name="초기값">Starting offset position</param>
        /// <param name="size">Number of values to set</param>
        /// <returns></returns>
        public bool SetData(int[] arrayValue, int 초기값, int size, bool throwException)
        {
            ti.Start();
            if (throwException)
            {
                if (arrayValue == null)
                    throw new ArgumentNullException("arrayValue");
                if (arrayValue.Length < (초기값 + size))
                    throw new ArgumentOutOfRangeException("arrayValue");
            }

            if (this.InvokeRequired)
            {
                SetDataDelegate setDelegate = new SetDataDelegate(this.SetData);
                return (bool)this.Invoke(setDelegate, arrayValue, 초기값, size);
            }
            bool isRunning = this.IsActive;

            //Monitor.Enter(this._meterData);
            
            int maxIndex = 초기값 + size;
            for (int i = 초기값; i < maxIndex; i++)
            {
                if (i < this._meterData.Length)
                {
                    PeakMeterData pm = this._meterData[i];
                    pm.Value = Math.Min(arrayValue[i], this._MaxRangeValue);
                    pm.Value = Math.Max(pm.Value, 0);
                    if (pm.Falloff < pm.Value)
                    {
                        pm.Falloff = pm.Value;
                        pm.Speed = this._Peak속도;
                    }
                    this._meterData[i] = pm;
                }
            }
            //Monitor.Exit(this._meterData);
 
            // check that timer should be restarted
            /*
            if (_AnimDelay != Timeout.Infinite)
            {
                if (_animationTimer == null)
                {
                    StartAnimation(_AnimDelay);
                
            }
            else
            {
                
            }
            */
            this.Invalidate();
            return isRunning;
        }
        #endregion

        /// <summary>
        /// Make a color darker
        /// </summary>
        /// <param name="color">Color to darken</param>
        /// <param name="darkenBy">Value to decrease by</param>
        protected virtual Color DarkenColor(Color color, byte darkenBy)
        {
            byte red = (byte)(color.R > darkenBy ? (color.R - darkenBy) : 0);
            byte green = (byte)(color.G > darkenBy ? (color.G - darkenBy) : 0);
            byte blue = (byte)(color.B > darkenBy ? (color.B - darkenBy) : 0);
            return Color.FromArgb(red, green, blue);
        }
        /// <summary>
        /// Make a color lighter
        /// </summary>
        /// <param name="color"></param>
        /// <param name="lightenBy"></param>
        /// <returns></returns>
        protected virtual Color LightenColor(Color color, byte lightenBy)
        {
            byte red = (byte)((color.R + lightenBy) <= 255 ? (color.R + lightenBy) : 255);
            byte green = (byte)((color.G + lightenBy) <= 255 ? (color.G + lightenBy) : 255);
            byte blue = (byte)((color.B + lightenBy) <= 255 ? (color.B + lightenBy) : 255);
            return Color.FromArgb(red, green, blue);
        }

        protected static bool InRange(int value, int rangeMin, int rangeMax)
        {
            return (value >= rangeMin && value <= rangeMax);
        }
        
        protected void ResetControl()
        {
            _meterData = new PeakMeterData[_BandsCount];
            PeakMeterData pm;
            pm.Value = _MaxRangeValue;
            pm.Falloff = _MaxRangeValue;
            pm.Speed = _Peak속도;
            for (int i = 0; i < _meterData.Length; i++)
            {
                _meterData[i] = pm;
            }
        }
        
        
        protected void StartAnimation(int period)
        {
            if ( !IsActive )
            {
                /*
                TimerCallback timerDelegate = 
                    new TimerCallback(TimerCallback);
                _animationTimer = new System.Threading.Timer(timerDelegate, this, Timeout.Infinite, Timeout.Infinite);
                 */
               
            }
            //return _animationTimer.Change(period, period);
            ti.Start();
        }
        protected bool StopAnimation()
        {
            bool result = false;
            if ( IsActive )
            {
                try
                {
                    result = _animationTimer.Change(Timeout.Infinite, Timeout.Infinite);
                    _animationTimer.Dispose();
                    _animationTimer = null;
                    result = true;
                }
                catch (Exception){}
            }
            ti.Stop();
            return result;
        }

        protected override void OnHandleDestroyed(EventArgs e)
        {
            Stop();
            base.OnHandleDestroyed(e);
        }
        protected override void OnBackColorChanged(EventArgs e)
        {
            Refresh();
        }
       protected override void OnPaint(PaintEventArgs e)
       {
            // Calling the base class OnPaint
            base.OnPaint(e);

            Graphics g = e.Graphics;
            Rectangle rect = new Rectangle(0, 0, this.Width, this.Height);
            Brush backColorBrush = new SolidBrush(this.BackColor);

            g.FillRectangle(backColorBrush, rect);
            //rect.Inflate(-this.Margin.Left, -this.Margin.Top);
            if (MeterStyle == PeakMeterStyle.PMS_Horizontal){DrawHorzBand(g, rect); }
            else{DrawVertBand(g, rect);}
        }
       System.Timers.Timer t2 = new System.Timers.Timer();
        protected void TimerCallback(Object thisObject, EventArgs e)
        {
            /*try
                
            {
                // refresh now!
                Control thisControl = thisObject as Control;
                if (thisControl != null && thisControl.IsHandleCreated)
                {
                    thisControl.Invoke(new MethodInvoker(Refresh));
                }
                else
                {
                    return;
                }
            }
            catch (Exception)
            {
                // just ignore
            }
            */
            
            int nDecValue  = _MaxRangeValue / _LED눈금갯수;
            bool noUpdate = true;

            Monitor.Enter(this._meterData);
            for (int i = 0; i < _meterData.Length; i++)
            {
                PeakMeterData pm = _meterData[i];

                if (pm.Value > 0)
                {
                    pm.Value -= (_LED눈금갯수 > 1 ? nDecValue : (_MaxRangeValue * DecrementPercent) / 100);
                    
                    if (pm.Value < 1)
                        pm.Value = 0;
                    noUpdate = false;
                }

                if (pm.Speed > 0)
                {
                    pm.Speed -= 1;
                    noUpdate = false;
                }

                if (pm.Speed == 0 && pm.Falloff > 0)
                {
                    int h;
                    if (_LED눈금갯수 > 1) { h = nDecValue << 1; }else{ h = 5; }
                    pm.Falloff -= h;
                    if (pm.Falloff < 0)
                        pm.Falloff = 0;
                    noUpdate = false;
                }

                // re-assign PeakMeterData
                _meterData[i] = pm;
            }
            Monitor.Exit(this._meterData);

            if (noUpdate) // Stop timer if no more data but do not reset ID
            {
                StopAnimation();
            }
        }
        protected void DrawHorzBand(Graphics g, Rectangle rect)
        {
            int nMaxRange = (_MedRangeValue == 0) ? Math.Abs(_MaxRangeValue - _MinRangeValue) : _MaxRangeValue;
            int nVertBands = (_LED눈금갯수 > 1 ? _LED눈금갯수 : (nMaxRange * DecrementPercent) / 100);
            int nMinVertLimit = _MinRangeValue * nVertBands / nMaxRange;
            int nMedVertLimit = _MedRangeValue * nVertBands / nMaxRange;
            int nMaxVertLimit = nVertBands-1;

            if (_MedRangeValue == 0)
            {
                nMedVertLimit = Math.Abs(_MinRangeValue) * nVertBands / nMaxRange;
                nMinVertLimit = 0;
            }
            Size size = new Size(rect.Width/_BandsCount, rect.Height/nVertBands);
            Rectangle rcBand = new Rectangle(rect.Location, size);

            // Draw band from bottom
            rcBand.Offset(0, rect.Height-size.Height);
            int xDecal = (_BandsCount>1 ? cxyMargin : 0);
            //int yDecal = 0;

            Color gridPenColor = (this.모눈선표시 ? 모눈선색상 : BackColor);
            Pen gridPen = new Pen(gridPenColor);
            Pen fallPen = new Pen(this.Peak색상, this._Peak굵기);
            int MaxRng = _MaxRangeValue;
            for(int nHorz=0; nHorz < _BandsCount; nHorz++)
            {
                int nValue = _meterData[nHorz].Value;
                int nVertValue = nValue*nVertBands/nMaxRange;
                Rectangle rcPrev = rcBand;

                for(int nVert=0; nVert < nVertBands; nVert++)
                {
                    // Find color based on range value
                    Color colorBand = gridPenColor;

                    // Draw grid line (level) bar
                    if ( this.모눈선표시 && (nVert == nMinVertLimit || nVert == nMedVertLimit || nVert == (nVertBands-1)))
                    {
                        Point []points = new Point[2];
                        points[0].X = rcBand.Left;
                        points[0].Y = rcBand.Top + (rcBand.Height>>1);
                        points[1].X = rcBand.Right;
                        points[1].Y = points[0].Y;
                        g.DrawPolygon(gridPen, points);
                        //MaxRng = 1000;
                    }

                    if ( _MedRangeValue == 0 )
                    {
                        int nVertStart = nMedVertLimit+nVertValue;
                        if ( InRange(nVert, nVertStart, nMedVertLimit-1) )
                            colorBand = this.ColorNormal;
                        else if ( nVert >= nMedVertLimit && InRange(nVert, nMedVertLimit, nVertStart) )
                            colorBand = this.ColorHigh;
                        else {
                            colorBand = (nVert < nMedVertLimit) ? this.ColorNormalBack : this.ColorHighBack;
                        }
                    }
                    else if ( nVertValue < nVert )
                    {
                        if ( this.모눈선표시 && this.색상표시 )
                        {
                            if ( InRange(nVert, 0, nMinVertLimit) )
                                colorBand = this.ColorNormalBack;
                            else if ( InRange(nVert, nMinVertLimit+1, nMedVertLimit) )
                                colorBand = this.ColorMediumBack;
                            else if ( InRange(nVert, nMedVertLimit+1, nMaxVertLimit) )
                                colorBand = this.ColorHighBack;
                        }
                    } else {
                        if (nValue == 0)
                        {
                            if (this.모눈선표시 && this.색상표시)
                                colorBand = this.ColorNormalBack;
                        }
                        else if ( InRange(nVert, 0, nMinVertLimit) )
                            colorBand = this.ColorNormal;
                        else if ( InRange(nVert, nMinVertLimit+1, nMedVertLimit) )
                            colorBand = this.ColorMedium;
                        else if ( InRange(nVert, nMedVertLimit+1, nMaxVertLimit) )
                            colorBand = this.ColorHigh;
                    }

                    if (colorBand != this.BackColor)
                    {
                        SolidBrush fillBrush = new SolidBrush(colorBand);
                        if (this._LED눈금갯수 > 1)
                            rcBand.Inflate(-cxyMargin, -cxyMargin);
                        g.FillRectangle(fillBrush, rcBand.Left, rcBand.Top, rcBand.Width+1, rcBand.Height);
                        if (this._LED눈금갯수 > 1)
                            rcBand.Inflate(cxyMargin, cxyMargin);
                    }
                    rcBand.Offset(0, -size.Height);
                }
                
                // Draw falloff effect
                if (this.Peak표시 || this.IsActive)
                {
                    int nMaxHeight = size.Height*nVertBands;
                    Point []points = new Point[2];
                    points[0].X = rcPrev.Left + xDecal;
                    points[0].Y = (rcPrev.Bottom - (_meterData[nHorz].Falloff * nMaxHeight) / MaxRng)-rcBand.Height;
                    points[1].X = rcPrev.Right - xDecal;
                    points[1].Y = points[0].Y;
                    g.DrawPolygon(fallPen, points);
                    //_meterData[nHorz].Falloff = 0;
                }
                // Move to Next Horizontal band
                rcBand.Offset(size.Width, size.Height * nVertBands);
            }
        }
        protected void DrawVertBand(Graphics g, Rectangle rect)
        {
            int nMaxRange = (_MedRangeValue == 0) ? Math.Abs(_MaxRangeValue - _MinRangeValue) : _MaxRangeValue;
            int nHorzBands = (_LED눈금갯수 > 1 ? _LED눈금갯수 : (nMaxRange * DecrementPercent) / 100);
            int nMinHorzLimit = nMaxRange!=0?_MinRangeValue * nHorzBands / nMaxRange:0;
            int nMedHorzLimit = nMaxRange != 0 ? _MedRangeValue * nHorzBands / nMaxRange : 0;
	        int nMaxHorzLimit = nHorzBands;

	        if ( _MedRangeValue == 0 )
	        {
		        nMedHorzLimit = Math.Abs(_MinRangeValue)*nHorzBands/nMaxRange;
		        nMinHorzLimit = 0;
	        }

            Size size = new Size(nHorzBands != 0 ? rect.Width / nHorzBands : 0, _BandsCount!=0?rect.Height / _BandsCount:0);
            Rectangle rcBand = new Rectangle(rect.Location, size);

	        // Draw band from top
            rcBand.Offset(0, rect.Height-size.Height*_BandsCount);
	        //int xDecal = 0;
	        int yDecal = (_BandsCount>1 ? cxyMargin : 0);

            Color gridPenColor = (this.모눈선표시 ? 모눈선색상 : BackColor);
            Pen gridPen = new Pen(gridPenColor);
            Pen fallPen = new Pen( this.Peak색상 ,this._Peak굵기);

            for(int nVert=0; nVert < _BandsCount; nVert++)
	        {
                int nValue = _meterData[nVert].Value;
                int nHorzValue = nMaxRange!=0?nValue * nHorzBands / nMaxRange:0;
                Rectangle rcPrev = rcBand;

		        for(int nHorz=0; nHorz < nHorzBands; nHorz++)
		        {
                    // Find color based on range value
                    Color colorBand = gridPenColor;

			        if ( this.모눈선표시 && (nHorz == nMinHorzLimit || nHorz == nMedHorzLimit || nHorz == (nHorzBands-1)))
			        {
                        Point []points = new Point[2];
				        points[0].X = rcBand.Left + (rcBand.Width>>1);
				        points[0].Y = rcBand.Top;
				        points[1].X = points[0].X;
				        points[1].Y = rcBand.Bottom;
                        g.DrawPolygon(gridPen, points);
                    }

                    if (_MedRangeValue == 0)
			        {
				        int nHorzStart = nMedHorzLimit+nHorzValue;
				        if ( InRange(nHorz, nHorzStart, nMedHorzLimit-1) )
					        colorBand = this.ColorNormal;
				        else if ( nHorz >= nMedHorzLimit && InRange(nHorz, nMedHorzLimit, nHorzStart) )
					        colorBand = this.ColorHigh;
				        else {
					        colorBand = (nHorz < nMedHorzLimit) ? this.ColorNormalBack : this.ColorHighBack;
				        }
			        }
			        else if ( nHorzValue < nHorz )
			        {
				        if ( this.모눈선표시 && this.색상표시 )
				        {
					        if ( InRange(nHorz, 0, nMinHorzLimit) )
						        colorBand = this.ColorNormalBack;
					        else if ( InRange(nHorz, nMinHorzLimit+1, nMedHorzLimit) )
						        colorBand = this.ColorMediumBack;
					        else if ( InRange(nHorz, nMedHorzLimit+1, nMaxHorzLimit) )
						        colorBand = this.ColorHighBack;
				        }
			        } else {
                        if (nValue == 0)
                        {
                            if (this.모눈선표시 && this.색상표시)
                                colorBand = this.ColorNormalBack;
                        }
                        else if (InRange(nHorz, 0, nMinHorzLimit))
					        colorBand = this.ColorNormal;
				        else if ( InRange(nHorz, nMinHorzLimit+1, nMedHorzLimit) )
					        colorBand = this.ColorMedium;
				        else if ( InRange(nHorz, nMedHorzLimit+1, nMaxHorzLimit) )
					        colorBand = this.ColorHigh;
			        }

                    if (colorBand != this.BackColor)
                    {
                        SolidBrush fillBrush = new SolidBrush(colorBand);
                        if (this._LED눈금갯수 > 1)
                            rcBand.Inflate(-cxyMargin, -cxyMargin);
                        g.FillRectangle(fillBrush, rcBand.Left, rcBand.Top, rcBand.Width, rcBand.Height+1);
                        if (this._LED눈금갯수 > 1)
                            rcBand.Inflate(cxyMargin, cxyMargin);
                    }
                    rcBand.Offset(size.Width, 0);
		        }

                // Draw falloff effect
                if (this.Peak표시 || this.IsActive)
                {
                    int nMaxWidth = size.Width * nHorzBands;
                    Point[] points = new Point[2];
                    points[0].X = rcPrev.Left+rcBand.Width  + (_MaxRangeValue != 0 ? (_meterData[nVert].Falloff * nMaxWidth) / _MaxRangeValue : 0);
                    points[0].Y = rcPrev.Top + yDecal;
                    points[1].X = points[0].X;
                    points[1].Y = rcPrev.Bottom - yDecal;
                    g.DrawPolygon(fallPen, points);
                    //_meterData[nVert].Falloff = 0;
                }
                
                // Move to Next Vertical band
		        rcBand.Offset(-size.Width*nHorzBands, size.Height);
	        }
        }

        private void ti_Tick(object sender, EventArgs e)
        {

        }
    }
}

// use this to find resource namespace
internal class MyResourceNamespace
{
}
