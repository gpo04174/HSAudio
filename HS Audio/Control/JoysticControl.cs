﻿using System;
using System.ComponentModel;
using System.Drawing;

using System.Windows.Forms;
using System.Drawing.Drawing2D;
using System.Runtime.InteropServices;

namespace HS_Audio.Control
{
    [DefaultEvent("JoysticControlValueChanged")]
    public partial class JoysticControl : UserControl
    {
        /// <summary>
        /// 도형의 모양 입니다.
        /// </summary>
        public enum Shape { 
            /// <summary>
            /// 동그라미 입니다.
            /// </summary>
            Round = 0, 
            /// <summary>
            /// 정사각형 입니다.
            /// </summary>
            Rectangle }
        public delegate void JoysticControlValueChangedEventHandler(Point Location, PointF value);

        public JoysticControl()
        {
            InitializeComponent();
            ResetDefault();
            Init();
        }

        //internal static readonly Bitmap bpNull = new Bitmap(10, 10);
        //internal Graphics g = Graphics.FromImage(bpNull); 
        internal Rectangle rec = new Rectangle(0, 0, 10, 10);
        PaintEventArgs pe;
        protected override void OnPaint(PaintEventArgs e)
        {
            pe=e;
            //배경색 초기화
            e.Graphics.Clear(_BackColor);
            //가로축 그리기
            tmp1.Y = (this.Size.Height / 2); tmp1.X = 0;
            tmp2.Y = (this.Size.Height / 2); tmp2.X = this.Size.Width;
            pe.Graphics.DrawLine(pen, tmp1, tmp2);
            //세로축 그리기
            tmp3.X = (this.Size.Width / 2); tmp3.Y = 0;
            tmp4.X = (this.Size.Width / 2); tmp4.Y = this.Size.Height;
            pe.Graphics.DrawLine(pen, tmp3, tmp4);
            if (!e.ClipRectangle.Equals(rec)) { DrawDotLine(false); }
            rec = e.ClipRectangle;

            DrawDotLine(e.Graphics);
            base.OnPaint(pe);
        }

        internal Pen pen = new Pen(Color.Black);
        Point tmp1 = new Point(), tmp2 = new Point(),
              tmp3 = new Point(), tmp4 = new Point();
        public void Init()
        {
            GraphicsPath gp = new GraphicsPath();
            Rectangle rec = new Rectangle(0, 0, this.Width, this.Height);
            if (ControlShape == Shape.Round) { gp.AddEllipse(rec); this.Region = new Region(gp); }
            else { this.Region = new Region(rec); }
        }

        #region 프로퍼티
        public new bool Enabled
        {
            get { return base.Enabled; }
            set
            {
                if (value)
                {
                    _BackColor = BackColor;
                    _ForeColor = ForeColor;
                    pen.Color = LineColor;
                }
                else { _BackColor = _ForeColor = SystemColors.ControlDark; pen.Color = SystemColors.ControlDarkDark; }

                base.Enabled = value;
                roundButton1.Enabled = value;

                

                DrawDotLine(true);
            }
        }    

        /// <summary>
        /// 값이 변경되면 발생하는 이벤트 입니다.
        /// </summary>
        [Description("값이 변경되면 발생합니다."), Category("프로퍼티")]
        public event JoysticControlValueChangedEventHandler JoysticControlValueChanged;

        Color _ForeColor;
        public new Color ForeColor
        {
            get { return roundButton1.ForeColor; }
            set
            {
                roundButton1.ForeColor = _ForeColor = value;
                if (Enabled) _ForeColor = value;

                Invalidate();
            }
        }

        Color _BackColor;
        public new Color BackColor
        {
            get { return base.BackColor; }
            set
            {
                base.BackColor = value;
                roundButton1.BackColor = value;

                if (Enabled) _BackColor = value;

                Invalidate();
            }
        }

        Color _DotLineColor = Color.Blue;
        /// <summary>
        /// 원점과 버튼사이의 선 색깔 입니다.
        /// </summary>
        public Color DotLineColor
        {
            get { return _DotLineColor; }
            set { _DotLineColor = value; DrawDotLine(true); }
        }


        Shape _ControlShape = Shape.Rectangle;
        /// <summary>
        /// 컨트롤의 모양을 가져오거나 설정합니다.
        /// </summary>
        [Description("컨트롤의 모양 입니다"), Category("프로퍼티"), DefaultValue(Shape.Rectangle)]
        public Shape ControlShape { get { return _ControlShape; } set { _ControlShape = value; Init(); LimitRange = LimitRange; } }

        /// <summary>
        /// 버튼 사이즈를 가져오거나 설정합니다.
        /// </summary>
        [Description("버튼 사이즈 입니다"), Category("프로퍼티"), DefaultValue(20)]
        public byte ButtonSize
        {
            get { return (byte)roundButton1.Size.Width; }
            set { roundButton1.Size = new Size(value, value); }
        }
        
        /// <summary>
        /// 그래프위의 숫자를 그릴여부를 가져오거나 설정합니다.
        /// </summary>
        [Description("그래프위의 숫자를 그릴여부 입니다"), Category("프로퍼티"), DefaultValue(false)]
        public bool DrawGraph { get; set; }

        /// <summary>
        /// 좌표의 색깔을 가져오거나 설정합니다.
        /// </summary>
        [Description("좌표의 색깔 입니다."), Category("프로퍼티"), DefaultValue(typeof(Color), "Black")]
        public Color LineColor { get { return pen.Color; } set { pen.Color = value; } }

        PointF _Maximum = new PointF(100, 100);
        /// <summary>
        /// 컨트롤의 최대값을 가져오거나 설정합니다.
        /// </summary>
        [Description("컨트롤의 최대값 입니다."), Category("프로퍼티"), DefaultValue(typeof(PointF), "100, 100")]
        public PointF Maximum { get { return _Maximum; } set { _Maximum = value; } }

        bool _LimitRange = true;
        /// <summary>
        /// 원이 밖으로 벗어나도 되는지의 여부입니다. (절대 좌표값도 커집니다.)
        /// </summary>
        [Description("원이 밖으로 벗어나도 되는지의 여부입니다. (절대 좌표값도 커집니다.)"), Category("프로퍼티"), DefaultValue(true)]
        public bool LimitRange
        {
            get { return _LimitRange; }
            set
            {
                _LimitRange = value;
                if(value)
                {
                    int X = roundButton1.Location.X;
                    int Y = roundButton1.Location.Y;

                    Point pt = CheckLimitRangeAbs(X, Y);
                    roundButton1.Location = pt;

                    DrawDotLine(true);

                    if(pt.X != X || pt.Y != Y)
                        try { JoysticControlValueChanged(ValueLocation, Value); } catch { }
                }
            }
        }

        bool _AutoMoveZero = false;
        /// <summary>
        /// 원이 자동으로 원점(제자리)로 돌아오는지의 여부 입니다.
        /// </summary>
        [Description("원이 자동으로 원점(제자리)로 돌아오는지의 여부 입니다."), Category("프로퍼티"), DefaultValue(false)]
        public bool AutoMoveZero
        {
            get { return _AutoMoveZero; }
            set { _AutoMoveZero = value; if(value)ResetDefault(); }
        }

        /*
        /// <summary>
        /// 컨트롤의 최소값을 가져오거나 설정합니다.
        /// </summary>
        [Description("컨트롤의 최소값 입니다."), Category("프로퍼티"), DefaultValue(PointF)]
        public PointF Minimum { get; set; }*/

        /// <summary>
        /// 현재값을 가져오거나 설정합니다.
        /// </summary>
        [Description("현재값 입니다."), Category("프로퍼티"), DefaultValue(typeof(Point), "0, 0")]
        public PointF Value
        {
            get 
            {
                PointF Loc = ValueLocation;
                //Maximum = new Point(200, 200);
                Loc.X = (int)(Maximum.X * (Loc.X / MaximumSizeMatrix.Width));
                Loc.Y = (int)(Maximum.Y * (Loc.Y / MaximumSizeMatrix.Height));

                //RealValue.X = Math.Max(Math.Min(RealValue.X, Maximum.X), -Maximum.X);
                //RealValue.Y = Math.Max(Math.Min(RealValue.Y, Maximum.Y), -Maximum.Y);
                return Loc;
            }
            set
            {
                /*
                //최댓값 영역 검사
                if (value.X > Maximum.X && LimitRange) { RealValue.X = Maximum.X; }
                else if (value.X < Maximum.X * -1f && LimitRange) { RealValue.X = Maximum.X * -1; }
                else { RealValue.X = value.X; }

                if (value.Y > Maximum.Y && LimitRange) { RealValue.Y = Maximum.Y; }
                else if (value.Y < Maximum.Y * -1f && LimitRange) { RealValue.Y = Maximum.Y * -1; }
                else { RealValue.Y = value.Y; }*/

                /*
                ValueLocation = new Point(
                    (int)(RealValue.X != 0 ? RealValue.X * (((this.Size.Width / 2) - (roundButton1.Size.Width / 2)) / Maximum.X) : RealValue.X),
                    (int)(RealValue.Y != 0 ? RealValue.Y * (((this.Size.Height / 2) - (roundButton1.Size.Height / 2)) / Maximum.Y) : RealValue.Y));*/
                ValueLocation = new Point(
                    (int)(((float)value.X / Maximum.X) * MaximumSizeMatrix.Width), 
                    (int)(((float)value.Y / Maximum.Y) * MaximumSizeMatrix.Height));

                //DrawDot(ValueLocation);
            }
        }
        /// <summary>
        /// 현재 좌표값(상대 위치)을 가져오거나 설정합니다.
        /// </summary>
        [Description("현재 좌표값(상대 위치) 입니다."), Category("프로퍼티"), DefaultValue(typeof(Point), "0, 0")]
        public Point ValueLocation
        {
            get
            {
                Point Loc = GetLocPoint(roundButton1.Location);
                return Loc;
            }
            set
            {
                /*
                float X1 = this.Width / 2;
                float Y1 = this.Height / 2;
                
                float X = (value.X / X1) * Maximum.X;
                float Y = (value.Y / Y1) * Maximum.Y;
                
                PointF RealValue = new PointF(X, Y);*/

                DrawDot(value);
                try { JoysticControlValueChanged(ValueLocation, Value); } catch { }
            }
        }

        #region Private
        public new Size MaximumSize
        {
            get {return this.Size - roundButton1.Size;}
        }
        public SizeF MaximumSizeMatrix
        {
            get
            {
                SizeF pt = new SizeF(
                    ((this.Size.Width / 2f) - (roundButton1.Size.Width / 2f)),
                    ((this.Size.Height / 2f) - (roundButton1.Size.Height / 2f)));
                return pt;
            }
        }
        public RectangleF MaximumArea
        {
            get
            {
                SizeF sf = MaximumSizeMatrix;
                RectangleF r = new RectangleF(new PointF(-sf.Width, -sf.Height), sf);
                //r.Location = ValueLocation;
                return r;
            }
        }
        #endregion

        #endregion

        #region 함수
        /// <summary>
        /// 버튼을 상대좌표에 그립니다.
        /// </summary>
        /// <param name="X">X 좌표입니다.</param>
        /// <param name="Y">Y 좌표입니다.</param>
        void DrawDot(int X, int Y)
        {
            Point pt = CheckLimitRange(X, Y);
            pt = GetAbsPoint(pt);
            roundButton1.Location = pt;
            DrawDotLine(true);
        }
        /// <summary>
        /// 버튼을 상대좌표에 그립니다.
        /// </summary>
        /// <param name="Loc">상대 좌표입니다.</param>
        void DrawDot(Point Loc)
        {
            Point pt = CheckLimitRange(Loc);
            pt = GetAbsPoint(pt);
            roundButton1.Location = pt;
            DrawDotLine(true);
        }
        /// <summary>
        /// 버튼의 위치를 기본으로 설정합니다.
        /// </summary>
        public void ResetDefault()
        {
            /*
            roundButton1.Location = new Point(
                (this.Size.Width / 2) - (roundButton1.Size.Width / 2),
                (this.Size.Height / 2) - (roundButton1.Size.Height / 2));*/

            ValueLocation = new Point(0, 0);
        }

        /// <summary>
        /// 현재 버튼의 위치까지 선을 그립니다.
        /// </summary>
        public void DrawDotLine(bool ReDraw = true)
        {
            if (true)
            {
                if (ReDraw)this.Refresh();
                DrawDotLine(this.CreateGraphics());
            }
        }
        Point pt1, pt2;
        /// <summary>
        /// 현재 버튼의 위치까지 선을 그립니다.
        /// </summary>
        public void DrawDotLine(Graphics g)
        {
            //선 그리기
            pt1.X = (this.Size.Width / 2);// - (roundButton1.Size.Width / 2);
            pt1.Y = (this.Size.Height / 2);// - (roundButton1.Size.Height / 2);
            pt2.X = roundButton1.Location.X + (roundButton1.Size.Width / 2);
            pt2.Y = roundButton1.Location.Y + (roundButton1.Size.Height / 2);
            g.DrawLine(new Pen(Enabled ? DotLineColor : SystemColors.ControlDarkDark), pt1, pt2);
        }

        #region 상대좌표 -> 절대좌표 변환
        Point GetAbsPoint(int X, int Y)
        {
            Point pt = new Point(
                //X 축은 원래대로
                (int)(X + MaximumSizeMatrix.Width),
                //Y 축은 들어오는 Y 값을 반전시켜야 ValueLocation 에서 제대로 좌표값을 가져올 수 있음
                (int)(-Y + MaximumSizeMatrix.Height));

            return pt;
        }
        Point GetAbsPoint(Point Loc)
        {
            //X 축은 원래대로
            Loc.X = (int)(Loc.X + MaximumSizeMatrix.Width);
            //Y 축은 들어오는 Y 값을 반전시켜야 ValueLocation 에서 제대로 좌표값을 가져올 수 있음
            Loc.Y = (int)(-Loc.Y + MaximumSizeMatrix.Height);

            return Loc;
        }
        #endregion

        #region 절대좌표 -> 상대좌표 변환
        Point GetLocPoint(int X_Abs, int Y_Abs)
        {
            Point Loc = new Point(
                (int)(X_Abs - ((this.Size.Width / 2f) - (roundButton1.Size.Width / 2f))),
                (int)((this.Size.Height / 2f) - Y_Abs - (roundButton1.Size.Height / 2f)));

            return Loc;
        }
        Point GetLocPoint(Point Abs)
        {
            Abs.X = (int)(Abs.X - ((this.Size.Width / 2f) - (roundButton1.Size.Width / 2f)));
            Abs.Y = (int)((this.Size.Height / 2f) - Abs.Y - (roundButton1.Size.Height / 2f));

            return Abs;
        }
        #endregion

        #region 점이 범위를 넘어가는지 검사
        Point CheckLimitRangeAbs(int X_Abs, int Y_Abs)
        {
            if (LimitRange)
            {
                if (ControlShape == Shape.Rectangle)
                {
                    int X1 = this.Width - roundButton1.Width;
                    int Y1 = this.Height - roundButton1.Height;

                    X1 = Math.Min(Math.Max(X_Abs, 0), X1);
                    Y1 = Math.Min(Math.Max(Y_Abs, 0), Y1);

                    return new Point(X1, Y1);
                }
                else
                {
                    //정확도 향상을 위해 X, Y 따로 구함
                    double a = (this.Width - roundButton1.Width) / 2f;
                    double b = (this.Height - roundButton1.Height) / 2f;

                    double x = X_Abs - a;
                    double y = b - Y_Abs;

                    double chk =
                        (Math.Pow(x, 2) / (Math.Pow(a, 2))) +
                        (Math.Pow(y, 2) / (Math.Pow(b, 2)));

                    if (chk > 1)
                    {
                        double StartAngle = 0;

                        if (x >= 0)
                            StartAngle = Math.Atan(y / x);//처음 각도
                        else
                            StartAngle = Math.Atan(y / x) + Math.PI;//처음 각도

                        double X = a * Math.Cos(StartAngle); //원 X 좌표
                        double Y = b * Math.Sin(StartAngle); //원 Y 좌표

                        return GetAbsPoint((int)X, (int)Y);
                    }

                    //일부러 원은 길이을 구해서 계산하고 타원은 원의 방정식을 동해 계산
                    /*
                    //정확도 향상을 위해 X, Y 따로 구함
                    double r_w = (this.Width - roundButton1.Width) / 2f;
                    double r_h = (this.Height - roundButton1.Height) / 2f;

                    double X1 = X_Abs - r_w;
                    double Y1 = r_h - Y_Abs;

                    double r_now = Math.Sqrt((X1 * X1) + (Y1 * Y1));

                    if (r_w == r_h) //원일때
                    {//현재위치까지의 길이 구하기
                        if (r_now > r_w)
                        {
                            double StartAngle = 0;

                            if (X1 >= 0)
                                StartAngle = Math.Atan(Y1 / X1);//처음 각도
                            else
                                StartAngle = Math.Atan(Y1 / X1) + Math.PI;//처음 각도

                            double X = r_w * Math.Cos(StartAngle); //원 X 좌표
                            double Y = r_h * Math.Sin(StartAngle); //원 Y 좌표

                            return GetAbsPoint((int)X, (int)Y);
                        }
                    }
                    else //타원일때
                    {
                        double a = r_w;
                        double b = r_h;
                        double x = X1;
                        double y = Y1;

                        //타원의 방정식
                        double chk =
                            (Math.Pow(x, 2) / (Math.Pow(a, 2))) + // (Math.Pow(x, 2) / (Math.Pow(a - r_now, 2)))
                            (Math.Pow(y, 2) / (Math.Pow(b, 2))); //(Math.Pow(y, 2) / (Math.Pow(b - r_now, 2)))


                        if (chk > 1)
                        {
                            double StartAngle = 0;

                            if (x >= 0)
                                StartAngle = Math.Atan(y / x);//처음 각도
                            else
                                StartAngle = Math.Atan(y / x) + Math.PI;//처음 각도

                            //double X = (a - r_now) * Math.Cos(StartAngle); //원 X 좌표
                            //double Y = (b - r_now) * Math.Cos(StartAngle); //원 Y 좌표
                            double X = a * Math.Cos(StartAngle); //원 X 좌표
                            double Y = b * Math.Sin(StartAngle); //원 Y 좌표

                            return GetAbsPoint((int)X, (int)Y);
                        }
                    }*/
                }
            }
            return new Point(X_Abs, Y_Abs);
        }
        Point CheckLimitRangeAbs(Point Abs)
        {
            if (LimitRange)
            {
                if (ControlShape == Shape.Rectangle)
                {
                    int X1 = this.Width - roundButton1.Width;
                    int Y1 = this.Height - roundButton1.Height;

                    Abs.X = Math.Min(Math.Max(Abs.X, 0), X1);
                    Abs.Y = Math.Min(Math.Max(Abs.Y, 0), Y1);

                    return Abs;
                }
                else //원일때 (타원 포함)
                {
                    //정확도 향상을 위해 X, Y 따로 구함
                    double a = (this.Width - roundButton1.Width) / 2f;
                    double b = (this.Height - roundButton1.Height) / 2f;

                    double x = Abs.X - a;
                    double y = b - Abs.Y;

                    //타원의 방정식
                    double chk =
                        (Math.Pow(x, 2) / (Math.Pow(a, 2))) +
                        (Math.Pow(y, 2) / (Math.Pow(b, 2))); 

                    if (chk > 1)
                    {
                        double StartAngle = 0;

                        if (x >= 0)
                            StartAngle = Math.Atan(y / x);//처음 각도
                        else
                            StartAngle = Math.Atan(y / x) + Math.PI;//처음 각도

                        double X = a * Math.Cos(StartAngle); //원 X 좌표
                        double Y = b * Math.Sin(StartAngle); //원 Y 좌표

                        Abs = GetAbsPoint((int)X, (int)Y);
                    }
                }
            }
            return Abs;
        }
        Point CheckLimitRange(int X, int Y)
        {
            if (LimitRange)
            {
                if (ControlShape == Shape.Rectangle)
                {
                    SizeF pt = MaximumSizeMatrix;
                    return new Point(
                        (int)Math.Min(Math.Max(X, -pt.Width), pt.Width),
                        (int)Math.Min(Math.Max(Y, -pt.Height), pt.Height));
                }
                else
                {
                    //정확도 향상을 위해 X, Y 따로 구함
                    double a = (this.Width - roundButton1.Width) / 2f;
                    double b = (this.Height - roundButton1.Height) / 2f; 
                    double x = X;
                    double y = Y;

                    //타원의 방정식
                    double chk =
                        (Math.Pow(x, 2) / (Math.Pow(a, 2))) +
                        (Math.Pow(y, 2) / (Math.Pow(b, 2))); 

                    if (chk > 1)
                    {
                        double StartAngle = 0;

                        if (x >= 0)
                            StartAngle = Math.Atan(y / x);//처음 각도
                        else
                            StartAngle = Math.Atan(y / x) + Math.PI;//처음 각도

                        double X1 = a * Math.Cos(StartAngle); //원 X 좌표
                        double Y1 = b * Math.Sin(StartAngle); //원 Y 좌표

                        return new Point((int)X1, (int)Y1);
                    }
                }
            }
            return new Point(X, Y);
        }
        Point CheckLimitRange(Point Loc)
        {
            if (LimitRange)
            {
                if (ControlShape == Shape.Rectangle)
                {
                    SizeF pt = MaximumSizeMatrix;
                    Loc.X = (int)Math.Min(Math.Max(Loc.X, -pt.Width), pt.Width);
                    Loc.Y = (int)Math.Min(Math.Max(Loc.Y, -pt.Height), pt.Height);
                    return Loc;
                }
                else
                {
                    //정확도 향상을 위해 X, Y 따로 구함
                    double a = (this.Width - roundButton1.Width) / 2f;
                    double b = (this.Height - roundButton1.Height) / 2f;
                    double X = Loc.X;
                    double Y = Loc.Y;

                    //타원의 방정식
                    double chk =
                        (Math.Pow(X, 2) / (Math.Pow(a, 2))) +
                        (Math.Pow(Y, 2) / (Math.Pow(b, 2)));

                    if (chk > 1)
                    {
                        double StartAngle = 0;

                        if (X >= 0)
                            StartAngle = Math.Atan(Y / X);//처음 각도
                        else
                            StartAngle = Math.Atan(Y / X) + Math.PI;//처음 각도

                        Loc.X = (int)(a * Math.Cos(StartAngle)); //원 X 좌표
                        Loc.Y = (int)(b * Math.Sin(StartAngle)); //원 Y 좌표
                    }
                }
            }
            return Loc;
        }
        #endregion

        #region 키보드 방향키
        int locX, locY;
        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            const int WM_KEYDOWN = 0x100;
            const int WM_SYSKEYDOWN = 0x104;

            if (Enabled)
                if (((msg.Msg == WM_KEYDOWN) || (msg.Msg == WM_SYSKEYDOWN)) &&
                    (keyData >= Keys.Left && keyData <= Keys.Down))
                {
                    //포인터 잼 (작동 잘됨)
                    /*
                    unsafe
                    {
                        IntPtr tmpptr = Marshal.AllocCoTaskMem(sizeof(Point)); //구조체로 쓸 메모리 할당
                        Marshal.StructureToPtr(ValueLocation, tmpptr, true); //tmpptr 변수로 구조체 데이터 복사
                        Point* pt = (Point*)tmpptr; //포인터 변수로 캐스팅
                        switch (keyData)
                        {
                            case Keys.Up:
                                pt->Y++; break;

                            case Keys.Down:
                                pt->Y--; break;

                            case Keys.Right:
                                pt->X++; break;

                            case Keys.Left:
                                pt->X--; break;
                        }
                        ValueLocation = *pt; //포인터에 있는 겂을 할당
                        Marshal.DestroyStructure(tmpptr, typeof(Point)); //구조체 포인터 해제
                    }*/
                    Point pt = ValueLocation;
                    switch (keyData)
                    {
                        case Keys.Up:
                            pt.Y++; break;

                        case Keys.Down:
                            pt.Y--; break;

                        case Keys.Right:
                            pt.X++; break;

                        case Keys.Left:
                            pt.X--; break;
                    }
                    ValueLocation = pt;

                    /*
                    case Keys.Control | Keys.M:
                        this.Parent.Text = "<CTRL> + M Captured";
                        break;*/
                }

            return base.ProcessCmdKey(ref msg, keyData);
        }
        #endregion

        #endregion

        internal Size thisSize;
        private void JoysticControl_SizeChanged(object sender, EventArgs e)
        {
            Init();
            /*
            if (this.Size.Width % 2 == 0 && this.Size.Height % 2 == 0)
            {
                int X=this.Size.Width - thisSize.Width, 
                    Y=this.Size.Height - thisSize.Height;

                if (X > 0) { X = roundButton1.Location.X + X/2; }
                else { X = roundButton1.Location.X - X/2; }

                if (Y > 0) { Y = roundButton1.Location.Y + Y/2; }
                else { Y = roundButton1.Location.Y - Y/2; }

                roundButton1.Location = new Point(X, Y);
            }*/

            thisSize = this.Size;

            //범위 검사 (버튼위치가 달라지면 어차피 이벤트가 발생되므로)
            LimitRange = LimitRange;
            //try { JoysticControlValueChanged(ValueLocation, Value); }catch { }
        }

        
        private void roundButton1_Move(object sender, EventArgs e)
        {
            /*
            if (MoveMouse)
            {
                //locX = roundButton1.Location.X + (roundButton1.Size.Width / 2);
                //locY = roundButton1.Location.Y + (roundButton1.Size.Height / 2);
                if (roundButton1.Location.X + (roundButton1.Size.Width / 2) < 0 &&
                    roundButton1.Location.Y + (roundButton1.Size.Height / 2) < 0 &&
                    roundButton1.Location.X + (roundButton1.Size.Width / 2) > this.Width &&
                    roundButton1.Location.Y + (roundButton1.Size.Height / 2) > this.Height)
                { }


                if (ValueLocation.X == locX && ValueLocation.Y == locY)
                {
                    roundButton1.Location = new Point(
                    locX + ((this.Size.Width / 2) - (roundButton1.Size.Width / 2)),
                    locY + ((this.Size.Height / 2) - (roundButton1.Size.Height / 2)));
                }

                DrawDotLine(true);

                locX = ValueLocation.X; locY = ValueLocation.Y;
            }*/
        }

        Point mousePoint = new Point();
        private void roundButton1_MouseDown(object sender, MouseEventArgs e)
        {
            mousePoint.X = e.X; mousePoint.Y = e.Y;MoveMouse=true;
        }

        bool MoveMouse = true;
        Point tmpLoc;
        private void roundButton1_MouseMove(object sender, MouseEventArgs e)
        {
            if ((e.Button & MouseButtons.Left) == MouseButtons.Left && MoveMouse &&
                (tmpLoc.X != e.X && tmpLoc.Y != e.Y))
            {
                //Maximum = new Point(200, 200) ;
                //if ((e.X < 0 && e.X > this.Width) && (e.X < 0 && e.X > this.Width))
                //roundButton1.Location = 

                /*
                System.Diagnostics.Debug.WriteLine("X = " + e.X + ", Y = " + e.Y);
                System.Diagnostics.Debug.WriteLine("X1 = " + mousePoint.X + ", Y1 = " + mousePoint.Y);*/

                /*
                if (ValueLocation.X == locX && ValueLocation.Y == locY)
                {
                    Point Offset = new Point(
                        locX + ((this.Size.Width / 2) - (roundButton1.Size.Width / 2)),
                        locY + ((this.Size.Height / 2) - (roundButton1.Size.Height / 2)));

                    ValueAbs = Offset;
                }

                locX = ValueLocation.X; locY = ValueLocation.Y;*/

                Point Loc =
                    new Point(roundButton1.Left - (mousePoint.X - e.X),
                    roundButton1.Top - (mousePoint.Y - e.Y));

                Point pt = CheckLimitRangeAbs(Loc);
                roundButton1.Location = pt;



                //Point Offset = new Point(Loc.X + (roundButton1.Size.Width / 2), Loc.Y + (roundButton1.Size.Height / 2));

                tmpLoc.X = e.X; tmpLoc.Y = e.Y;
                
                DrawDotLine(true);

                try { JoysticControlValueChanged.Invoke(ValueLocation, Value); } catch { }
                //ValueAbs = Offset;
            }
        }

        private void JoysticControl_MouseEnter(object sender, EventArgs e)
        {
            //ResetDefault();
            //MoveMouse = MoveMouseCtr?true:false;
        }

        bool MoveMouseCtr = false;
        private void roundButton1_MouseEnter(object sender, EventArgs e)
        {
            MoveMouseCtr = true;
        }

        private void roundButton1_MouseLeave(object sender, EventArgs e)
        {
            MoveMouseCtr = false;
        }

        private void roundButton1_MouseUp(object sender, MouseEventArgs e)
        {
            if (AutoMoveZero) ResetDefault();
        }

        private void JoysticControl_Resize(object sender, EventArgs e)
        {
            DrawDotLine(true);
        }

        private void JoysticControl_DoubleClick(object sender, EventArgs e)
        {
            ResetDefault();
        }

    }
}
