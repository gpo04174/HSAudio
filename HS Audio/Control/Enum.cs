﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HS_Audio.Control
{
    public enum Orientation
    {
        /// <summary>
        /// 가로 방향 입니다.
        /// </summary>
        Horizontal, 
        /// <summary>
        /// 세로 방향 입니다.
        /// </summary>
        Vertical
    }
}
