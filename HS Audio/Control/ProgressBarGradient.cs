﻿namespace HS_Audio.Control
{
    using System;
using System.ComponentModel;
using System.Drawing;
using System.Threading;
using System.Windows.Forms;

    public class ProgressBarGradient : ProgressBarGradientInternal
    {
        internal static int Count = 0;

        Thread SmoothThread;
        ThreadStart SmoothThreadStart;
        public ProgressBarGradient()
        {
            Count++;
            SmoothThreadStart = new ThreadStart(updateProgress);
        }
        /*
        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);
        }*/

        [Category("Animation"), Description("그래프를 부드럽게 그릴지의 여부 입니다.")]
        bool _Smooth = false;
        public bool Smooth
        {
            get{return _Smooth;}
            set
            { 
                _Smooth = value;
                if (SmoothThread != null) SmoothThread.Abort();
                if (value)
                {
                    SmoothThread = new Thread(SmoothThreadStart){Name = Count+"번 ProgressBarGradient 스레드"};
                    SmoothThread.Start();
                }
            }
        }

        int _Step = 1;
        [DefaultValue(1), Description("")]
        public int Step
        {
            get{return _Step;}
            set
            {
                _Step = value;
                /*if (base.MaximumValue < value) */
                if(value > 0)base.MaximumValue = base.MaximumValue * value;
            }
        }

        public new int MaximumValue
        {
            get{return base.MaximumValue / Step;}
            set{base.MaximumValue = value * Step;}
        }

        private int sampleDelay = 30;
        public int ReleaseTime
		{
			get{return sampleDelay;}
			set
			{
				sampleDelay = Math.Max(0, value);
				if(frameDelay > sampleDelay)
                    frameDelay = sampleDelay;
			}
		}

        private int frameDelay = 10;
		public int AttackTime
		{
			get
			{
				return frameDelay;
			}

			set
			{
				frameDelay = Math.Max(0, Math.Min(sampleDelay, value));
			}
		}
        
        public int Value
        {
            get{return base.Value;}
            set
            {
                if (value < base.MinimumValue) SetValue(base.MinimumValue);
                else if (value > base.MaximumValue) SetValue(base.MaximumValue);
                else SetValue(value);
            }
        }
        public double Value_Double
        {
            get { return Value / base.MaximumValue; }
            set
            {
                double Calc = (double)base.MinimumValue / (double)int.MaxValue;
                if (value < Calc) SetValue(Calc);
                else if (value > 1) SetValue(1d);
                else SetValue(value);
            }
        }

        void SetValue(int Value)
        {
            //_Value = Value;
            //if(!Smooth)base.Value = Value;
            base.Value = Value;
        }
        void SetValue(double Value)
        {
            if (!Smooth)
            {
                double aa = Value;

                aa = Math.Max(aa, -1);
                aa = Math.Max(aa, 1); 
                /*
                aa *= (Step < 1 ?
                      base.Orientation == System.Windows.Forms.Orientation.Horizontal ? 
                        base.Width : 
                        base.Height
                        : Step);*/
                aa *= base.MaximumValue;
                Value = (int)aa;

            }
        }

        private void updateProgress()
        {
            while (Smooth)
            {
                try
                {
                    int tempFrameDelay = frameDelay;
                    int tempSampleDelay = sampleDelay;

                    int MaxValue = base.MaximumValue;

                    // for each channel, determine the step size necessary for each iteration
                    int leftGoal = 0;

                    #region
                    {
                        long aa = Value * (Step < 1 ? 
                            base.Orientation == System.Windows.Forms.Orientation.Horizontal ? base.Width : base.Height
                            : base.MaximumValue);

                        aa = Math.Max(aa, -MaxValue);
                        int b = (int)(MaxValue + aa);
                        leftGoal = (int)Math.Min(b, MaxValue);
                    }
                    #endregion
                    /*
				// average across all samples to get the goals
				for(int i = 0; i < SAMPLES; i++)
				{
					leftGoal += (Int16)samples.GetValue(i, 0, 0);
					rightGoal += (Int16)samples.GetValue(i, 1, 0);
				}

				leftGoal = (int)Math.Abs(leftGoal / SAMPLES);
				rightGoal = (int)Math.Abs(rightGoal / SAMPLES);*/

                    double range1 = leftGoal - Value;

                    double exactValue1 = Value;

                    double stepSize1 = range1 / tempSampleDelay * tempFrameDelay;
                    if (Math.Abs(stepSize1) < .01)
                    {
                        stepSize1 = Math.Sign(range1) * .01;
                    }
                    double absStepSize1 = Math.Abs(stepSize1);

                    // increment/decrement the bars' values until both equal their desired goals,
                    // sleeping between iterations
                    if ((Value == leftGoal))
                    {
                        System.Threading.Thread.Sleep(tempSampleDelay);
                    }
                    else
                    {
                        do
                        {
                            if (Value != leftGoal)
                            {
                                if (absStepSize1 < Math.Abs(leftGoal - Value))
                                {
                                    exactValue1 += stepSize1;
                                    //base.Value = (int)Math.Round(exactValue1);
                                    base.Value = (int)((MaximumValue/100f)*50);
                                }
                                else
                                {
                                    //base.Value = leftGoal;
                                    base.Value = (int)((MaximumValue / 100f) * 50);
                                }
                            }

                            System.Threading.Thread.Sleep(tempFrameDelay);
                        } while ((Value != leftGoal));
                    }
                }
                catch
                {
                    System.Threading.Thread.Sleep(100);
                }
            }
        }

        protected override void Dispose(bool disposing)
        {
            Count--;
            base.Dispose(disposing);
        }
    }
}

