﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using System.Drawing.Drawing2D;
using HS_CSharpUtility.Extension;

namespace HS_Audio.Control
{
    public partial class PlayControl : UserControl
    {
        public delegate void PlayButtonPressEventHandler();
        public delegate void PauseButtonPressEventHandler();
        public delegate void StopButtonPressEventHandler();
        public delegate void MoveCompletedEventHandler(Point Location);
        /// <summary>
        /// 재생 버튼(▶)을 누를때 발생합니다.
        /// </summary>
        [Description("재생 버튼(▶)을 누를때 발생합니다."),Category("이벤트")]
        public event PlayButtonPressEventHandler PlayButtonPress;
        /// <summary>
        /// 일지정지(∥) 버튼을 누를때 발생합니다.
        /// </summary>
        [Description("일지정지(∥) 버튼을 누를때 발생합니다."),Category("이벤트")]
        public event PauseButtonPressEventHandler PauseButtonPress;
        /// <summary>
        /// 정지 버튼(■)을 누를때 발생합니다.
        /// </summary>
        [Description("정지 버튼(■)을 누를때 발생합니다."),Category("이벤트")]
        public event StopButtonPressEventHandler StopButtonPress;
        /// <summary>
        /// 컨트롤의 이동이 완료되면 발생합니다.
        /// </summary>
        [Description("컨트롤 이동이 완료되면 발생합니다."), Category("이벤트")]
        public event MoveCompletedEventHandler MoveCompleted;



        public PlayControl(){InitializeComponent();}

        private void btnPlay_Click(object sender, EventArgs e){try{PlayButtonPress();}catch{}}

        private void btnPause_Click(object sender, EventArgs e){try{PauseButtonPress();}catch{}}

        private void btnStop_Click(object sender, EventArgs e) {try{StopButtonPress();}catch{}}

        bool btnPlayFocused, btnPauseFocused,btnStopFocused;
        private void btnPlay_MouseEnter(object sender, EventArgs e) {btnPlayFocused=true;btnPlay.BackgroundImage=Properties.Resources.Play_29;}// btnPlay.ForeColor = _PlayButtonFocusColor; }
        private void btnPlay_MouseLeave(object sender, EventArgs e) {btnPlayFocused=false;btnPlay.BackgroundImage = InvertImage(Properties.Resources.Play_29); }//btnPlay.ForeColor = _PlayButtonDefaultColor; }
        private void btnPause_MouseEnter(object sender, EventArgs e) { btnPauseFocused=true;btnPause.BackgroundImage=Properties.Resources.Pause_29;}//btnPause.ForeColor = _PauseButtonFocusColor; }
        private void btnPause_MouseLeave(object sender, EventArgs e) { btnPauseFocused=false;btnPause.BackgroundImage = InvertImage(Properties.Resources.Pause_29);}//btnPause.ForeColor = _PauseButtonDefaultColor; }
        private void btnStop_MouseEnter(object sender, EventArgs e) {btnStopFocused=true; btnStop.BackgroundImage=Properties.Resources.Stop_29;}//btnStop.ForeColor = _StopButtonFocusColor; }
        private void btnStop_MouseLeave(object sender, EventArgs e) {btnStopFocused=false; btnStop.BackgroundImage = InvertImage(Properties.Resources.Stop_29);}//btnStop.ForeColor = _StopButtonDefaultColor; }

        public static Bitmap InvertImage(Bitmap OriginImage)
        {
            for(int i=0;i<OriginImage.Width;i++)
                for (int j = 0; j < OriginImage.Height; j++)
                    OriginImage.SetPixel(i, j, HS_CSharpUtility.Extension.ColorExtension.NegativeColor(OriginImage.GetPixel(i, j)));
            return OriginImage;
        }

        #region 가장자리잡고 움직이기
        Point MouseDownLocation;
        MouseButtons MouseButton = System.Windows.Forms.MouseButtons.None;
        private void PlayControl_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                MouseButton = System.Windows.Forms.MouseButtons.Left;
                MouseDownLocation = e.Location;
                Cursor = Cursors.SizeAll;
            }
        }

        private void PlayControl_MouseUp(object sender, MouseEventArgs e)
        {
            MouseButton = System.Windows.Forms.MouseButtons.None;
            Cursor = Cursors.Default;
        }

        private void PlayControl_MouseMove(object sender, MouseEventArgs e)
        {
            //if(!Cursor.Equals(Cursors.Default))Cursor = Cursors.Default;
            if(MouseButton == System.Windows.Forms.MouseButtons.Left&&UseMove){
            this.Left += e.X - MouseDownLocation.X;
            this.Top += e.Y - MouseDownLocation.Y;}
        }
        #endregion

        #region 이동패널 잡고 움직이기

        Point panel1MouseDownLocation;
        MouseButtons panel1MouseButtons = System.Windows.Forms.MouseButtons.None;
        private void panel1_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                panel1MouseButtons = System.Windows.Forms.MouseButtons.Left;
                MouseDownLocation = e.Location;
            }
        }

        private void panel1_MouseUp(object sender, MouseEventArgs e)
        {
            panel1MouseButtons = System.Windows.Forms.MouseButtons.None;
            try{if((panel1MouseDownLocation.X!=e.Location.X)||(panel1MouseDownLocation.Y!=e.Location.Y))MoveCompleted(this.Location);}catch{}
        }

        private void panel1_MouseMove(object sender, MouseEventArgs e)
        {
            if(panel1MouseButtons == System.Windows.Forms.MouseButtons.Left){
            this.Left += e.X - MouseDownLocation.X;
            this.Top += e.Y - MouseDownLocation.Y;}
        }
        #endregion

        #region 프로퍼티
        [Description("컨트롤의 가장자리를 잡고 이동할지를 결정합니다."), Category("기타"), DefaultValue(false)]
        public bool UseMove{get;set;}
        [Description("잡고 이동할수 있는 패널을 표시할지 여부를 결정합니다."), Category("이동 패널"), DefaultValue(true)]
        public bool ShowMovePanel{get{return panel1.Visible;}set{panel1.Visible=value;}}
        [Description("잡고 이동할수 있는 패널의 색깔을 지정합니다."), Category("이동 패널"), DefaultValue("Gray")]
        public Color MovePanelColor{get{return panel1.BackColor;}set{ panel1.BackColor=value;}}
        [Description("잡고 이동할수 있는 패널의 크기를 지정합니다."), Category("이동 패널"), DefaultValue("6, 27")]
        public Size MovePanelSize{get{return panel1.Size;}set{ panel1.Size=value;}}

        Color _PlayButtonFocusColor= Color.White,
              _PauseButtonFocusColor= Color.White,
              _StopButtonFocusColor = Color.White;
        /// <summary>
        /// 재생 버튼위로 커서를 올릴때의 색깔 입니다.
        /// </summary>
        [Description("재생 버튼위로 커서를 올릴때의 색깔을 지정합니다."),Category("버튼 색깔"),DefaultValue("White")]
        public Color PlayButtonFocusColor{get{return _PlayButtonFocusColor;}set{_PlayButtonFocusColor=value;if(btnPlayFocused)btnPlay.ForeColor=value;}}
        /// <summary>
        /// 일시정지 버튼위로 커서를 올릴때의 색깔 입니다.
        /// </summary>
        [Description("일시정지 버튼위로 커서를 올릴때의 색깔을 지정합니다."),Category("버튼 색깔"),DefaultValue("White")]
        public Color PauseButtonFocusColor{get{return _PauseButtonFocusColor;}set{_PauseButtonFocusColor=value;if(btnPauseFocused)btnPause.ForeColor=value;}}
        /// <summary>
        /// 정지 버튼위로 커서를 올릴때의 색깔 입니다.
        /// </summary>
        [Description("정지 버튼위로 커서를 올릴때의 색깔을 지정합니다."),Category("버튼 색깔"),DefaultValue("White")]
        public Color StopButtonFocusColor{get{return _StopButtonFocusColor;}set{_StopButtonFocusColor=value;if(btnStopFocused)btnStop.ForeColor=value;}}

        Color _PlayButtonDefaultColor = Color.Black,
              _PauseButtonDefaultColor = Color.Black,
              _StopButtonDefaultColor = Color.Black;
        /// <summary>
        /// 재생 버튼위로 커서를 올릴떄의 색깔 입니다.
        /// </summary>
        [Description("재생 버튼위로 커서를 올릴때의 색깔을 지정합니다."),Category("버튼 색깔"),DefaultValue("Black")]
        public Color PlayButtonDefaultColor{get{return _PlayButtonDefaultColor;}set{_PlayButtonDefaultColor=value;if(!btnPlayFocused)btnPlay.ForeColor=value;}}
        /// <summary>
        /// 일시정지 버튼위로 커서를 올릴떄의 색깔 입니다.
        /// </summary>
        [Description("일시정지 버튼위로 커서를 올릴때의 색깔을 지정합니다."),Category("버튼 색깔"),DefaultValue("Black")]
        public Color PauseButtonDefaultColor{get{return _PauseButtonDefaultColor;}set{_PauseButtonDefaultColor=value;if(!btnPlayFocused)btnPlay.ForeColor=value;}}
        /// <summary>
        /// 정지 버튼위로 커서를 올릴떄의 색깔 입니다.
        /// </summary>
        [Description("정지 버튼위로 커서를 올릴때의 색깔을 지정합니다."),Category("버튼 색깔"),DefaultValue("Black")]
        public Color StopButtonDefaultColor{get{return _StopButtonDefaultColor;}set{_StopButtonDefaultColor=value;if(!btnPlayFocused)btnPlay.ForeColor=value;}}
        #endregion
        #region 프로퍼티 숨기기
        private new Color ForeColor;
        #endregion

        private void PlayControl_Load(object sender, EventArgs e)
        {
            btnPlay.BackgroundImage = InvertImage(Properties.Resources.Play_29);
            btnPauseFocused = false; btnPause.BackgroundImage = InvertImage(Properties.Resources.Pause_29);
            btnStopFocused = false; btnStop.BackgroundImage = InvertImage(Properties.Resources.Stop_29);

            //panel1.Scale(1, 1);btnPlay.Scale(1, 1);btnPause.Scale(1, 1);btnStop.Scale(1, 1);
            //int W = (int)(panel1.Width + btnPlay.Width + btnPause.Width + btnStop.Width);
            //int H = panel1.DisplayRectangle.Height + btnPlay.DisplayRectangle.Height + btnPause.DisplayRectangle.Height + btnStop.DisplayRectangle.Height;
            
            //this.MaximumSize=MinimumSize=Size = new Size(W, 30);
            //this.ClientSize= new Size(W, H);
        }

    }
}
