﻿using System;
using System.ComponentModel;
using HS_CSharpUtility.Extension;

namespace HS_Audio.Control
{

    public class HSCustomControl :System.Windows.Forms.Control
    {
        /*
        public delegate void WindowStateChangedEventHandler(object sender, EventArgs e);
        [Description("창의 상태가 변경되었을때 발생하는 이벤트 입니다.")]
        public event WindowStateChangedEventHandler WindowStateChanged;*/
        [Description("창의 숨김/보이기가 변경되었을때 발생합니다.")]
        public event WindowShowChangedEventHandler WindowShowChanged;

        public HSCustomControl() : base() {}

        /*
        public System.Windows.Forms.FormWindowState WindowState
        {
            get{return base.WindowState;}
            set
            {
                base.WindowState = value; 
                try{if(WindowStateChanged!=null)WindowStateChanged(this, new EventArgs());}
                catch{}
            } 
        }*/

        WindowShowState _WindowShowStatus;
        /// <summary>
        /// 창이 숨겨졌는지 보여졌는지에 대한 값을 가져옵니다.
        /// </summary>
        public WindowShowState WindowShowStatus {get{return _WindowShowStatus;}/* set{_WindowShowStatus = value; if(value == WindowShowState.Hide)this.Hide();else this.Show();}*/}

        System.Windows.Forms.FormWindowState _State;
        protected override void WndProc(ref System.Windows.Forms.Message m)
        {
            base.WndProc(ref m);
            /*
            if (m.Msg == 0x0005)
            {
                if (_State != this.WindowState)
                {
                    _State = this.WindowState;
                    try{if(WindowStateChanged!=null)WindowStateChanged(this, new EventArgs());}catch{}
                }
            }*/
            if (m.Msg == 0x0018) //WM_SHOWWINDOW
            {
                _WindowShowStatus = (WindowShowState)m.WParam.ToInt32();
                try{if(WindowShowChanged!=null)WindowShowChanged(this, new EventArgs());}catch{}
            }
        }
    }
}
