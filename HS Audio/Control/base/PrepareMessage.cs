﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace HS_Audio.Control
{
    public partial class PrepareMessage : UserControl
    {
        public PrepareMessage()
        {
            InitializeComponent();
        }
        
        [Description("글꼴을 설정합니다."), Category("Misc.")]
        public new Font Font { get { return label1.Font; }set { label1.Font = value; } }
        [Description("텍스트를 설정합니다."), Category("Misc.")]
        public new string Text { get { return label1.Text; } set { label1.Text = value; } }

        private void PrepareMessage_Load(object sender, EventArgs e)
        {
            PrepareMessage_SizeChanged(sender, e);
        }

        private void PrepareMessage_SizeChanged(object sender, EventArgs e)
        {
            int X = this.Width / 2;
            int Y = this.Height / 2;
            int X1 = this.label1.Width / 2;
            int Y1 = this.label1.Height / 2;
            this.label1.Location = new Point(X - X1, Y - Y1);
        }
    }
}
