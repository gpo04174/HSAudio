﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace HS_Audio.Control.Base
{
    public partial class dBSpaceControl: UserControl
    {
        public dBSpaceControl()
        {
            InitializeComponent();
            SetStyle(ControlStyles.SupportsTransparentBackColor|ControlStyles.OptimizedDoubleBuffer, true);
            this.BackColor = Color.Transparent;
        }

        private HS_Audio.Control.Orientation _Orientation;
        public HS_Audio.Control.Orientation Orientation {get{return _Orientation;}set{_Orientation = value;this.Draw();} }

        private int _MindB = -40;
        /// <summary>
        /// 표시할 최소 데시벨을 가져오거나 설정합니다.
        /// </summary>
        [Description("표시할 최소 데시벨입니다."), DefaultValue(-40)]
        public int MindB{get{return _MindB;}set{_MindB = value;this.Draw();}}

        private int _MaxdB = 0;
        /// <summary>
        /// 표시할 최대 데시벨을 가져오거나 설정합니다.
        /// </summary>
        [Description("표시할 최대 데시벨입니다."), DefaultValue(0)]
        public int MaxdB{get{return _MaxdB;}set{_MaxdB = value;this.Draw();}}

        private bool _ShowdB = false;
        /// <summary>
        /// 뒤에 dB 표시 여부를 가져오거나 설정합니다.
        /// </summary>
        [Description("뒤에 dB 표시 여부 입니다."), DefaultValue(false)]
        public bool ShowdB{get{return _ShowdB;}set{_ShowdB = value;this.Draw();}}

        private System.Drawing.Text.TextRenderingHint _TextRendering = System.Drawing.Text.TextRenderingHint.SystemDefault;
        /// <summary>
        /// 데시벨 레이블의 질감을 가져오거나 설정합니다.
        /// </summary>
        [Description("데시벨 레이블의 질감 입니다."), DefaultValue(System.Drawing.Text.TextRenderingHint.SystemDefault)]
        public System.Drawing.Text.TextRenderingHint TextRendering{get{return _TextRendering;}set{_TextRendering = value;}}

        /// <summary>
        /// 다시 그립니다.
        /// </summary>
        public void Draw(){this.Refresh();}

        private void dBSpaceControl_SizeChanged(object sender, EventArgs e)
        {
            
        }

        PointF pt = new PointF();
        SolidBrush brush = new SolidBrush(Color.Black);
        protected override void OnPaint(PaintEventArgs e)
        {
            using (Graphics g = e.Graphics)
            {
                g.Clear(Color.Transparent);
                brush.Color = ForeColor;
                int j = Orientation== Control.Orientation.Horizontal?
                    this.Width/(Math.Abs(MaxdB)+Math.Abs(MindB)):
                    this.Height/(Math.Abs(MaxdB)+Math.Abs(MindB));

                int tmp = j*2;
                for (int i = MaxdB-1; i > MindB; i--, tmp+=j)
                {
                    pt.X = tmp;pt.Y=0;
                    g.TextRenderingHint = TextRendering;
                    g.DrawString(i.ToString(), this.Font, brush, pt);
                }
                g.RotateTransform(270);
            }
            base.OnPaint(e);
        }
    }
}
