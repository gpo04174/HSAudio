﻿using System;
using System.ComponentModel;
using System.Runtime.InteropServices;
using HS_CSharpUtility.Extension;

namespace HS_Audio.Control
{
    /// <summary>
    /// 창이 숨겨졌는지 보여졌는지에 대한 값 입니다.
    /// </summary>
    public enum WindowShowState{Show = 1, Hide = 0};

    /// <summary>
    /// 창의 숨김/보이기가 변경되었을때 발생하는 이벤트 대리자 입니다.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    public delegate void WindowShowChangedEventHandler(object sender, EventArgs e);

    public class HSCustomForm :System.Windows.Forms.Form
    {
        public delegate void WindowStateChangedEventHandler(object sender, EventArgs e);
        [Description("창의 상태가 변경되었을때 발생합니다.")]
        public event WindowStateChangedEventHandler WindowStateChanged;
        [Description("창의 숨김/보이기가 변경되었을때 발생합니다.")]
        public event WindowShowChangedEventHandler WindowShowChanged;

        public HSCustomForm() : base() {}

        /*
        public System.Windows.Forms.FormWindowState WindowState
        {
            get{return base.WindowState;}
            set
            {
                base.WindowState = value; 
                try{if(WindowStateChanged!=null)WindowStateChanged(this, new EventArgs());}
                catch{}
            } 
        }*/

        WindowShowState _WindowShowStatus;
        private System.Windows.Forms.CheckedListBox checkedListBox1;
        /// <summary>
        /// 창이 숨겨졌는지 보여졌는지에 대한 값을 가져옵니다.
        /// </summary>
        public WindowShowState WindowShowStatus {get{return _WindowShowStatus;}/* set{_WindowShowStatus = value; if(value == WindowShowState.Hide)this.Hide();else this.Show();}*/}

        private bool _XButtonEnable = true;
        [DefaultValue(true), Description("폼상의 X버튼 (닫기 버튼)되있는지 여부를 가져오거나 설정합니다.")]
        public bool XButtonEnable{get{return _XButtonEnable;}set{int a = HideXButton(this.Handle, !value);_XButtonEnable = value;}}

        #region 함수들
        #region Win32 메세지 처리 함수
        System.Windows.Forms.FormWindowState _State;
        protected override void WndProc(ref System.Windows.Forms.Message m)
        {
            base.WndProc(ref m);
            if (m.Msg == 0x0005)
            {
                if (_State != this.WindowState)
                {
                    _State = this.WindowState;
                    try{if(WindowStateChanged!=null)WindowStateChanged(this, new EventArgs());}catch{}
                }
            }
            if (m.Msg == 0x0018) //WM_SHOWWINDOW
            {
                _WindowShowStatus = (WindowShowState)m.WParam.ToInt32();
                try{if(WindowShowChanged!=null)WindowShowChanged(this, new EventArgs());}catch{}
            }
            if(m.Msg == 0x135){};
        }
        #endregion

        #region X버튼 숨기기
        internal const int SC_CLOSE = 0xF060;
        internal const int MF_ENABLED = 0x0;
        internal const int MF_GRAYED = 0x1;
        internal const int MF_DISABLED = 0x2;

        [DllImport("user32.dll")]
        private static extern int EnableMenuItem(IntPtr hMenu, int wIDEnableItem, int wEnable);

        [DllImport("User32")]
        private static extern IntPtr GetSystemMenu(IntPtr hWnd, bool bRevert);
        /// <summary>
        /// 폼의 X버튼을 비활성화 합니다.
        /// </summary>
        /// <param name="Handle">숨길 창 (폼) 핸들입니다.</param>
        internal static int HideXButton(IntPtr Handle, bool Hide = true)
        {
            IntPtr hMenu = GetSystemMenu(Handle, false);// 시스템 메뉴를 가져옵니다.
            return EnableMenuItem(hMenu, SC_CLOSE, Hide?MF_DISABLED:MF_ENABLED);
        }
        #endregion

        #region 기타 함수
        bool _IsClosing = false;
        bool _Closing = true;
        public bool CloseForm {get{return _IsClosing?true:_Closing;}set{_Closing = value;}}
        protected override void OnClosing(CancelEventArgs e)
        {
            e.Cancel = !CloseForm;
            base.OnClosing(e);
        }
        protected override void Dispose(bool disposing)
        {
            _IsClosing = true;
            base.Dispose(disposing);
        }
        #endregion
        #endregion
    }
}
