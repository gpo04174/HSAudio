﻿namespace HS_Audio.Control
{
    partial class IntellisenseTextBox
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary> 
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code 

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary> 
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(IntellisenseTextBox));
            this.toolTip = new System.Windows.Forms.ToolTip(this.components);
            this.autoCompletionImageList = new System.Windows.Forms.ImageList(this.components);
            this.SuspendLayout();
            // 
            // toolTip
            // 
            this.toolTip.AutomaticDelay = 0;
            this.toolTip.UseAnimation = false;
            // 
            // autoCompletionImageList
            // 
            this.autoCompletionImageList.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("autoCompletionImageList.ImageStream")));
            this.autoCompletionImageList.TransparentColor = System.Drawing.Color.Transparent;
            this.autoCompletionImageList.Images.SetKeyName(0, "Default.png");
            this.autoCompletionImageList.Images.SetKeyName(1, "Type.png");
            this.autoCompletionImageList.Images.SetKeyName(2, "PublicMethod.png");
            this.autoCompletionImageList.Images.SetKeyName(3, "PrivateMethod.png");
            this.autoCompletionImageList.Images.SetKeyName(4, "InternalMethod.png");
            this.autoCompletionImageList.Images.SetKeyName(5, "ProtectedMethod.png");
            this.autoCompletionImageList.Images.SetKeyName(6, "PublicProperty.png");
            this.autoCompletionImageList.Images.SetKeyName(7, "PrivateProperty.png");
            this.autoCompletionImageList.Images.SetKeyName(8, "InternalProperty.png");
            this.autoCompletionImageList.Images.SetKeyName(9, "ProtectProperty.png");
            this.autoCompletionImageList.Images.SetKeyName(10, "PublicField.png");
            this.autoCompletionImageList.Images.SetKeyName(11, "PrivateField.png");
            this.autoCompletionImageList.Images.SetKeyName(12, "InternalField.png");
            this.autoCompletionImageList.Images.SetKeyName(13, "ProtectedField.png");
            this.autoCompletionImageList.Images.SetKeyName(14, "Keyword.png");
            this.autoCompletionImageList.Images.SetKeyName(15, "ExtensionMethod.png");
            // 
            // IntellisenseTextBox
            // 
            this.MouseClick += new System.Windows.Forms.MouseEventHandler(this.IntellisenseTextBox_MouseClick);
            this.Enter += new System.EventHandler(this.IntellisenseTextBox_Enter);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.IntellisenseTextBox_KeyDown);
            this.Leave += new System.EventHandler(this.IntellisenseTextBox_Leave);
            this.ResumeLayout(false);

        }

        #endregion
        
        private System.Windows.Forms.ToolTip toolTip;
        private System.Windows.Forms.ImageList autoCompletionImageList;
    }
}
