﻿namespace HS_Audio.Control
{
    using System;
    using System.ComponentModel;
    using System.Drawing;
    using System.Drawing.Drawing2D;
    using System.Windows.Forms;

    public class ProgressBarGradientInternal : UserControl, IProgressBarGradientInternal
    {
        private ColorBlend colorBlend = new ColorBlend(3);
        private bool cursorChanged;
        private bool dragging;
        private Rectangle gradientArea;
        private Point lastPosition;
        private Color maximumColor = Color.Crimson;
        private int maximumValue = 100;
        private Color midColor = Color.Goldenrod;
        private Color minimumColor = Color.Lime;
        private int minimumValue = 0;
        private bool mouseInteractive = false;
        private Rectangle nonGradientArea;
        private Cursor oldCursor;
        private System.Windows.Forms.Orientation orientation = System.Windows.Forms.Orientation.Horizontal;
        private int progressValue;
        private int range;
        private bool showMidColor = true;

        public ProgressBarGradientInternal()
        {
            //| ControlStyles.Opaque
            base.SetStyle(ControlStyles.AllPaintingInWmPaint | ControlStyles.ResizeRedraw | ControlStyles.UserPaint | ControlStyles.SupportsTransparentBackColor, true);
            //base.SetStyle(ControlStyles.OptimizedDoubleBuffer|ControlStyles.AllPaintingInWmPaint | ControlStyles.ResizeRedraw | ControlStyles.UserPaint | ControlStyles.SupportsTransparentBackColor, true);
            //base.DoubleBuffered = true;
            this.gradientArea = Rectangle.Empty;
            this.nonGradientArea = base.ClientRectangle;
            this.colorBlend.Positions[0] = 0f;
            this.colorBlend.Positions[1] = 0.4f;
            this.colorBlend.Positions[2] = 1f;
            this.colorBlend.Colors = new Color[] { this.minimumColor, this.midColor, this.maximumColor };
            //base.InitialiseOpenGL();
        }

        private void adjustGradientArea()
        {
            if (this.orientation == System.Windows.Forms.Orientation.Horizontal)
            {
                this.gradientArea.Width = (int) ((((double) (this.progressValue - this.minimumValue)) / ((double) this.range)) * base.Width);
                this.gradientArea.Height = base.Height;
                this.gradientArea.Location = new Point(0, 0);
                this.nonGradientArea.Width = base.Width - this.gradientArea.Width;
                this.nonGradientArea.Height = this.gradientArea.Height;
                this.nonGradientArea.Location = new Point(this.gradientArea.Right, 0);
            }
            else
            {
                this.gradientArea.Width = base.Width;
                this.gradientArea.Height = (int) ((((double) (this.progressValue - this.minimumValue)) / ((double) this.range)) * base.Height);
                this.gradientArea.Location = new Point(0, base.Height - this.gradientArea.Height);
                this.nonGradientArea.Width = this.gradientArea.Width;
                this.nonGradientArea.Height = base.Height - this.gradientArea.Height;
                this.nonGradientArea.Location = new Point(0, 0);
            }
        }

        public void ForcePaint()
        {
            //this.OnPaint(null);
            this.Refresh();
        }

        private void InitializeComponent()
        {
            this.SuspendLayout();
            // 
            // ProgressBarGradientInternal
            // 
            this.Name = "ProgressBarGradientInternal";
            this.Size = new System.Drawing.Size(152, 24);
            this.Load += new System.EventHandler(this.ProgressBarGradientInternal_Load);
            this.ResumeLayout(false);
        }

        protected override void OnClick(EventArgs e)
        {
            if (this.mouseInteractive)
            {
                int num = 0;
                if (this.orientation == System.Windows.Forms.Orientation.Horizontal)
                {
                    num = (int) Math.Round((double) ((((double) this.lastPosition.X) / ((double) base.Width)) * this.Range));
                }
                else
                {
                    num = (int) Math.Round((double) ((((double) (base.Height - this.lastPosition.Y)) / ((double) base.Height)) * this.Range));
                }
                if (num > this.maximumValue)
                {
                    this.Value = this.maximumValue;
                }
                else if (num < this.minimumValue)
                {
                    this.Value = this.minimumValue;
                }
                else
                {
                    this.Value = num;
                }
            }
            base.OnClick(e);
        }

        protected override void OnMouseDown(MouseEventArgs e)
        {
            if (this.mouseInteractive)
            {
                this.dragging = true;
                this.oldCursor = this.Cursor;
                this.lastPosition = new Point(e.X, e.Y);
            }
            base.OnMouseDown(e);
        }

        protected override void OnMouseEnter(EventArgs e)
        {
            if (this.mouseInteractive)
            {
                base.Focus();
            }
        }

        protected override void OnMouseMove(MouseEventArgs e)
        {
            if (this.mouseInteractive && this.dragging)
            {
                if (!this.cursorChanged)
                {
                    if (this.orientation == System.Windows.Forms.Orientation.Horizontal)
                    {
                        this.Cursor = Cursors.VSplit;
                    }
                    else
                    {
                        this.Cursor = Cursors.HSplit;
                    }
                    this.cursorChanged = true;
                }
                this.OnClick(e);
                this.lastPosition.X = e.X;
                this.lastPosition.Y = e.Y;
            }
            base.OnMouseMove(e);
        }

        protected override void OnMouseUp(MouseEventArgs e)
        {
            if (this.mouseInteractive)
            {
                this.Cursor = this.oldCursor;
                this.cursorChanged = false;
                this.dragging = false;
            }
            base.OnMouseUp(e);
        }

        protected override void OnMouseWheel(MouseEventArgs e)
        {
            if (this.mouseInteractive)
            {
                if ((this.Value + (e.Delta / 10)) > this.MaximumValue)
                {
                    this.Value = this.MaximumValue;
                }
                else if ((this.Value + (e.Delta / 10)) < this.MinimumValue)
                {
                    this.Value = this.MinimumValue;
                }
                else
                {
                    this.Value += e.Delta / 10;
                }
            }
            base.OnMouseWheel(e);
        }

        int Wi, Hei;
        protected override void OnPaint(PaintEventArgs e)
        {
            base.SuspendLayout();
            base.OnPaint(e);
            if(FlickerReduce)OnPaint2(e);
            else OnPaint1(e);
            base.ResumeLayout(true);
        }
        internal void OnPaint1(PaintEventArgs e)
        {
            bool dBEnable = VisibledB;
            if ((base.Visible && (base.Height > 0)) && (base.Width > 0))
            {
                //using (Graphics graphics = base.CreateGraphics())
                if(e.Graphics!=null/*&&!Lock*/){
                    /*
                    if (dBEnable)
                    {
                        if (Wi != this.Width || Hei != this.Height)
                        {
                            if (Lablebmp != null) Lablebmp.Dispose();
                            Lablebmp = new Bitmap(this.Width, this.Height);
                            using(Graphics g = Graphics.FromImage(Lablebmp))DrawLable(g, this.Width, this.Height);
                            Wi = this.Width; Hei = this.Height;
                        }
                    }*/

                    using (Graphics graphics = e.Graphics)
                    {
                        //if(dBEnable)graphics.Clear(this.BackColor);
                        if (this.orientation == System.Windows.Forms.Orientation.Horizontal)
                        {
                            if (this.nonGradientArea.Width > 0)
                            {
                                using (SolidBrush brush = new SolidBrush(this.BackColor))
                                {
                                    graphics.FillRectangle(brush, this.nonGradientArea);
                                }
                            }
                            if (this.gradientArea.Width <= 0)
                            {
                                //if (_VisibledB&&Lablebmp!=null)graphics.DrawImage(Lablebmp, 0, 0);
                                //if if (_VisibledB&&Lablebmp!=null)DrawLable(graphics, this.Width, this.Height);
                                goto Label_0154;
                            }
                            using (LinearGradientBrush brush2 = new LinearGradientBrush(base.DisplayRectangle, this.minimumColor, this.maximumColor, LinearGradientMode.Horizontal))
                            {
                                if (this.showMidColor)
                                {
                                    brush2.InterpolationColors = this.colorBlend;
                                }
                                graphics.FillRectangle(brush2, this.gradientArea);
                                //if (_VisibledB&&Lablebmp!=null)graphics.DrawImage(Lablebmp, 0, 0);
                                //if (_VisibledB&&Lablebmp!=null)DrawLable(graphics, this.Width, this.Height);
                                goto Label_0154;
                            }
                        }
                        if (this.nonGradientArea.Height > 0)
                        {
                            using (SolidBrush brush3 = new SolidBrush(this.BackColor))
                            {
                                graphics.FillRectangle(brush3, this.nonGradientArea);
                            }
                        }
                        if (this.gradientArea.Height > 0)
                        {
                            using (LinearGradientBrush brush4 = new LinearGradientBrush(base.DisplayRectangle, this.maximumColor, this.minimumColor, LinearGradientMode.Vertical))
                            {
                                if (this.showMidColor)
                                {
                                    brush4.InterpolationColors = this.colorBlend;
                                }
                                graphics.FillRectangle(brush4, this.gradientArea);
                            }
                        }
                        
                    Label_0154:
                        //if if (_VisibledB&&Lablebmp!=null)DrawLable(graphics, this.Width, this.Height);//graphics.DrawImage(Lablebmp, 0, 0);
                        if(dBEnable)DrawLable(graphics, this.Width, this.Height);
                        /*
                        if (dBEnable && Lablebmp != null)
                        {
                            //DrawLable(graphics, Lablebmp.Width, Lablebmp.Height);//graphics.DrawImage(Lablebmp, 0, 0);
                            e.Graphics.DrawImage(Lablebmp, 0, 0);
                        }*/
                    }
                }
                //base.OnPaint(e);
            }
        }
        internal void OnPaint2(PaintEventArgs e)
        {
            bool dBEnable = VisibledB;
            if ((base.Visible && (base.Height > 0)) && (base.Width > 0))
            {
                //using (Graphics graphics = base.CreateGraphics())
                if(e.Graphics!=null/*&&!Lock*/){
                    if (dBEnable)
                    {
                        if (Wi != this.Width || Hei != this.Height)
                        {
                            if (Lablebmp != null) Lablebmp.Dispose();
                            Lablebmp = new Bitmap(this.Width, this.Height);
                            Wi = this.Width; Hei = this.Height;
                        }
                    }

                    using (Graphics graphics = dBEnable?Graphics.FromImage(Lablebmp):e.Graphics)
                    {
                        //if(dBEnable)graphics.Clear(this.BackColor);
                        if (this.orientation == System.Windows.Forms.Orientation.Horizontal)
                        {
                            if (this.nonGradientArea.Width > 0)
                            {
                                using (SolidBrush brush = new SolidBrush(this.BackColor))
                                {
                                    graphics.FillRectangle(brush, this.nonGradientArea);
                                }
                            }
                            if (this.gradientArea.Width <= 0)
                            {
                                //if (_VisibledB&&Lablebmp!=null)graphics.DrawImage(Lablebmp, 0, 0);
                                //if if (_VisibledB&&Lablebmp!=null)DrawLable(graphics, this.Width, this.Height);
                                goto Label_0154;
                            }
                            using (LinearGradientBrush brush2 = new LinearGradientBrush(base.DisplayRectangle, this.minimumColor, this.maximumColor, LinearGradientMode.Horizontal))
                            {
                                if (this.showMidColor)
                                {
                                    brush2.InterpolationColors = this.colorBlend;
                                }
                                graphics.FillRectangle(brush2, this.gradientArea);
                                //if (_VisibledB&&Lablebmp!=null)graphics.DrawImage(Lablebmp, 0, 0);
                                //if (_VisibledB&&Lablebmp!=null)DrawLable(graphics, this.Width, this.Height);
                                goto Label_0154;
                            }
                        }
                        if (this.nonGradientArea.Height > 0)
                        {
                            using (SolidBrush brush3 = new SolidBrush(this.BackColor))
                            {
                                graphics.FillRectangle(brush3, this.nonGradientArea);
                            }
                        }
                        if (this.gradientArea.Height > 0)
                        {
                            using (LinearGradientBrush brush4 = new LinearGradientBrush(base.DisplayRectangle, this.maximumColor, this.minimumColor, LinearGradientMode.Vertical))
                            {
                                if (this.showMidColor)
                                {
                                    brush4.InterpolationColors = this.colorBlend;
                                }
                                graphics.FillRectangle(brush4, this.gradientArea);
                            }
                        }
                        
                    Label_0154:
                        //if if (_VisibledB&&Lablebmp!=null)DrawLable(graphics, this.Width, this.Height);
                        if (dBEnable && Lablebmp != null)
                        {
                            DrawLable(graphics, this.Width, this.Height);
                            //DrawLable(graphics, Lablebmp.Width, Lablebmp.Height);//graphics.DrawImage(Lablebmp, 0, 0);
                            e.Graphics.DrawImage(Lablebmp, 0, 0);
                        }
                    }
                }
                //base.OnPaint(e);
            }
        }

        PointF pt = new PointF();
        SolidBrush brush = new SolidBrush(Color.Black);
        void DrawLable(Graphics g, int Width, int Height)
        {
            brush.Color = ForeColor;
            if (Orientation == System.Windows.Forms.Orientation.Horizontal)
            {
                float j = (float)Width / (Math.Abs(MaxdB) + Math.Abs(MindB));
                float tmp = j;
                for (int i = MindB+1; i < MaxdB; i++, tmp += j)
                {
                    pt.X = tmp-Font.Size; pt.Y = 1;
                    g.TextRenderingHint = TextRendering;
                    if (i % IntervaldB == 0)
                    {
                        string tmp1 = ShowMinus ?
                            i.ToString() + (ShowdB ? SpacedB?" dB":"dB" : null) :
                            ((int)Math.Abs(i)).ToString() + (ShowdB ? SpacedB?" dB":"dB" : null);
                        if (i > 0 && ShowPlus) tmp1 = string.Format("+{0}{1}", tmp1, ShowdB ? SpacedB?" dB":"dB" : "");
                        //g.RotateTransform(90);
                        g.DrawString(tmp1, this.Font, brush, pt);
                    }
                }
            }
            else
            {
                float j = (float)Height / (Math.Abs(MaxdB) + Math.Abs(MindB));
                float tmp = j;
                for (int i = MaxdB-1; i > MindB; i--, tmp += j)
                {
                    pt.X = 1; pt.Y = tmp-Font.Size;
                    g.TextRenderingHint = TextRendering;
                    if (i % IntervaldB == 0)
                    {
                        //string tmp1 = ShowMinus?i.ToString():((int)Math.Abs(i)).ToString();
                        string tmp1 = ShowMinus ?
                            i.ToString() + (ShowdB ? SpacedB?" dB":"dB" : null) :
                            ((int)Math.Abs(i)).ToString() + (ShowdB ? SpacedB?" dB":"dB" : null);
                        //if (i > 0 && ShowPlus) tmp1 = "+" + tmp1;
                        if (i > 0 && ShowPlus) tmp1 = string.Format("+{0}{1}", tmp1, ShowdB ? SpacedB?" dB":"dB" : "");
                        g.DrawString(tmp1, this.Font, brush, pt);
                    }
                }
            }
        }

        protected override void OnPaintBackground(PaintEventArgs pevent)
        {
        }

        bool Lock = false;
        Bitmap Lablebmp;
        protected override void OnResize(EventArgs e)
        {
            Graphics g = null;
            try
            {
                /*
                if (VisibledB)
                {
                    Lock = true;
                    
                    if (Lablebmp != null) Lablebmp.Dispose();
                    Lablebmp = new Bitmap(this.Width, this.Height);
                    g = Graphics.FromImage(Lablebmp);
                    DrawLable(g, Lablebmp.Width, Lablebmp.Height);
                }
                else
                {
                    Lock = true;
                    if (Lablebmp != null) Lablebmp.Dispose();
                    Lablebmp = null;
                }*/
                this.Invalidate(true);
            }
            finally
            {
                Lock = false; 
                if (g != null) g.Dispose();
                
                base.OnResize(e);
                this.adjustGradientArea();
            }
        }

        #region 원래 속성
        public Color MaximumColor
        {
            get
            {
                return this.maximumColor;
            }
            set
            {
                this.maximumColor = value;
                if (this.orientation == System.Windows.Forms.Orientation.Horizontal)
                {
                    this.colorBlend.Colors[2] = this.maximumColor;
                }
                else
                {
                    this.colorBlend.Colors[0] = this.maximumColor;
                }
                //this.OnPaint(null);
                this.ForcePaint();
            }
        }

        public int MaximumValue
        {
            get
            {
                return this.maximumValue;
            }
            set
            {
                if (value < this.minimumValue)
                {
                    throw new ArgumentOutOfRangeException("MaximumValue", value, "Maximum value must be strcitly greater than Minimum value");
                }
                this.maximumValue = value;
                this.range = this.maximumValue - this.minimumValue;
            }
        }

        public Color MidColor
        {
            get
            {
                return this.midColor;
            }
            set
            {
                this.midColor = value;
                this.colorBlend.Colors[1] = this.midColor;
                //this.OnPaint(null);
                this.ForcePaint();

            }
        }

        public float MidColorPosition
        {
            get
            {
                return this.colorBlend.Positions[1];
            }
            set
            {
                if ((value < 1f) && (value > 0f))
                {
                    if (this.orientation == System.Windows.Forms.Orientation.Horizontal) this.colorBlend.Positions[1] = value;
                    else this.colorBlend.Positions[1] = 1f - value;
                    //this.OnPaint(null);
                    this.ForcePaint();
                }
            }
        }

        public Color MinimumColor
        {
            get
            {
                return this.minimumColor;
            }
            set
            {
                this.minimumColor = value;
                if (this.orientation == System.Windows.Forms.Orientation.Horizontal)
                {
                    this.colorBlend.Colors[0] = this.minimumColor;
                }
                else
                {
                    this.colorBlend.Colors[2] = this.minimumColor;
                }
                //this.OnPaint(null);
                this.ForcePaint();
            }
        }

        public int MinimumValue
        {
            get
            {
                return this.minimumValue;
            }
            set
            {
                if (value >= this.maximumValue)
                {
                    throw new ArgumentOutOfRangeException("MinimumValue", value, "Minimum value must be strictly less than Maximum value");
                }
                this.minimumValue = value;
                this.range = this.maximumValue - this.minimumValue;
            }
        }

        public bool MouseInteractive
        {
            get
            {
                return this.mouseInteractive;
            }
            set
            {
                this.mouseInteractive = value;
            }
        }

        public System.Windows.Forms.Orientation Orientation
        {
            get
            {
                return this.orientation;
            }
            set
            {
                if (this.orientation != value)
                {
                    this.orientation = value;
                    Color color = this.colorBlend.Colors[0];
                    this.colorBlend.Colors[0] = this.colorBlend.Colors[2];
                    this.colorBlend.Colors[2] = color;
                    this.colorBlend.Positions[1] = 1f - this.colorBlend.Positions[1];
                    this.OnResize(null);
                    //this.OnPaint(null);
                    this.ForcePaint();
                }
            }
        }

        public int Range
        {
            get
            {
                return this.range;
            }
        }

        public bool ShowMidColor
        {
            get
            {
                return this.showMidColor;
            }
            set
            {
                this.showMidColor = value;
                //this.OnPaint(null);
                this.ForcePaint();
            }
        }

        object o = new object();
        public int Value
        {
            get {return this.progressValue; }
            set
            {
                lock (o)
                {
                    if (value != this.progressValue)
                    {
                        /*
                        if ((value < this.minimumValue) || (value > this.maximumValue))
                        {
                            throw new ArgumentOutOfRangeException("Value", value, string.Concat(new object[] { "Value must be between Minimum and Maximum value (", this.minimumValue, " to ", this.maximumValue, ")" }));
                        }*/
                        if (value < this.minimumValue) this.progressValue = this.minimumValue;
                        else if (value > this.maximumValue) this.progressValue = this.maximumValue;
                        else this.progressValue = value;
                        //Console.Write(value);
                        this.adjustGradientArea();


                        //this.OnPaint(null);
                        this.ForcePaint();
                    }
                }
            }
        }
        #endregion


        //private HS_Audio.Control.Orientation _Orientation;
        //public HS_Audio.Control.Orientation Orientation {get{return _Orientation;}set{_Orientation = value;this.Draw();} }

        internal const string CategoryName = "dB Lable";

        private int _MindB = -40;
        /// <summary>
        /// 표시할 최소 데시벨을 가져오거나 설정합니다.
        /// </summary>
        [Description("표시할 최소 데시벨입니다."), DefaultValue(-40), Category(CategoryName)]
        public int MindB{get{return _MindB;}set{_MindB = value;this.Draw();}}

        private int _MaxdB = 0;
        /// <summary>
        /// 표시할 최대 데시벨을 가져오거나 설정합니다.
        /// </summary>
        [Description("표시할 최대 데시벨입니다."), DefaultValue(0), Category(CategoryName)]
        public int MaxdB{get{return _MaxdB;}set{_MaxdB = value;this.Draw();}}

        private bool _ShowdB = false;
        /// <summary>
        /// 뒤에 dB 문자 표시 여부를 가져오거나 설정합니다.
        /// </summary>
        [Description("뒤에 dB 문자 표시 여부 입니다."), DefaultValue(false), Category(CategoryName)]
        public bool ShowdB{get{return _ShowdB;}set{_ShowdB = value;this.Draw();}}        
        
        private bool _SpacedB = false;
        /// <summary>
        /// dB 문자 사이 공백을 포함 여부를 가져오거나 설정합니다.
        /// </summary>
        [Description("dB 문자 사이 공백을 포함 여부 입니다."), DefaultValue(false), Category(CategoryName)]
        public bool SpacedB{get{return _SpacedB;}set{_SpacedB = value;this.Draw();}}

        private bool _ShowMinus = false;
        /// <summary>
        /// 뒤에 dB 표시 여부를 가져오거나 설정합니다.
        /// </summary>
        [Description("숫자 뒤에 음수 부호 표시 여부 입니다."), DefaultValue(false), Category(CategoryName)]
        public bool ShowMinus{get{return _ShowMinus;}set{_ShowMinus = value;this.Draw();}}

        private bool _ShowPlus = true;
        /// <summary>
        /// 뒤에 dB 표시 여부를 가져오거나 설정합니다.
        /// </summary>
        [Description("숫자 뒤에 양수 부호 표시 여부 입니다."), DefaultValue(true), Category(CategoryName)]
        public bool ShowPlus{get{return _ShowPlus;}set{_ShowPlus = value;this.Draw();}}

        private bool _ShowEdge = false;
        /// <summary
        /// 데시벨 레이블에 테두리 표시 여부를 가져오거나 설정합니다.
        [Description("데시벨 레이블에 테두리 표시 여부 입니다."), DefaultValue(false), Category(CategoryName)]
        public bool ShowEdge{get{return _ShowEdge;}set{_ShowEdge = value;this.Draw();}}

        private System.Drawing.Text.TextRenderingHint _TextRendering = System.Drawing.Text.TextRenderingHint.SystemDefault;
        /// <summary>
        /// 데시벨 레이블의 질감을 가져오거나 설정합니다.
        /// </summary>
        [Description("데시벨 레이블의 질감 입니다."), DefaultValue(System.Drawing.Text.TextRenderingHint.SystemDefault), Category(CategoryName)]
        public System.Drawing.Text.TextRenderingHint TextRendering{get{return _TextRendering;}set{_TextRendering = value;}}

        /// <summary>
        /// 데시벨 레이블의 글꼴을 가져오거나 설정합니다.
        /// </summary>
        [Description("데시벨 레이블의 글꼴 입니다."), Category(CategoryName)]
        public new Font Font{get{return base.Font;}set{base.Font = value; this.Draw();}}

        private bool _VisibledB = false;
        /// <summary>
        /// 데시벨 레이블의 표시 여부를 가져오거나 설정합니다.
        /// </summary>
        [Description("데시벨 레이블의 표시 여부 입니다."), DefaultValue(false), Category(CategoryName)]
        public bool VisibledB{get{return _VisibledB;}set{_VisibledB = value; this.Draw();}}

        private int _IntervaldB = 2;
        /// <summary>
        /// 데시벨 레이블의 표시 여부를 가져오거나 설정합니다.
        /// </summary>
        [Description("데시벨 레이블의 표시 간격 입니다."), DefaultValue(2), Category(CategoryName)]
        public int IntervaldB{get{return _IntervaldB;}set{_IntervaldB = value<0?1:value; this.Draw();}}

        private Color _ForeColor = Color.Black;
        /// <summary>
        /// 데시벨 레이블의 색상을 가져오거나 설정합니다.
        /// </summary>
        [Description("데시벨 레이블의 색상 입니다."), DefaultValue(typeof(Color), "Black"), Category(CategoryName)]
        public new Color ForeColor{get{return _ForeColor;}set{_ForeColor = value; this.Draw();}}

        private bool _FlickerReduce = true;
        /// <summary
        /// 데시벨 레이블의 깜빡거림 감소 여부를 가져오거나 설정합니다. (대신 성능은 감소 합니다.)
        [Description("데시벨 레이블의 깜빡거림 감소 여부 입니다. (대신 성능은 감소 합니다.)"), DefaultValue(true), Category(CategoryName)]
        public bool FlickerReduce{get{return _FlickerReduce;}set{_FlickerReduce = value;this.Draw();}}

        public void Draw(){OnResize(null);}

        private void ProgressBarGradientInternal_Load(object sender, EventArgs e)
        {

        }
    }
}

