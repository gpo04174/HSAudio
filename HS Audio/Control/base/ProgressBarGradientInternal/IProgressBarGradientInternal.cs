﻿using System.ComponentModel;
using System.Drawing;

namespace HS_Audio.Control
{
    interface IProgressBarGradientInternal
    {
        #region 원래 속성
        Color MaximumColor {get;set;}

        int MaximumValue{get;set;}

        Color MidColor{get;set;}

        float MidColorPosition{get;set;}

        Color MinimumColor{get;set;}

        int MinimumValue{get;set;}

        bool MouseInteractive{get;set;}

        System.Windows.Forms.Orientation Orientation{get;set;}

        int Range{get;}

        bool ShowMidColor{get;set;}

        int Value{get;set;}
        #endregion

        /// <summary>
        /// 표시할 최소 데시벨을 가져오거나 설정합니다.
        /// </summary>
        [Description("표시할 최소 데시벨입니다."), DefaultValue(-40), Category(ProgressBarGradientInternal.CategoryName)]
        int MindB{get;set;}

        /// <summary>
        /// 표시할 최대 데시벨을 가져오거나 설정합니다.
        /// </summary>
        [Description("표시할 최대 데시벨입니다."), DefaultValue(0), Category(ProgressBarGradientInternal.CategoryName)]
        int MaxdB{get;set;}

        /// <summary>
        /// 뒤에 dB 문자 표시 여부를 가져오거나 설정합니다.
        /// </summary>
        [Description("뒤에 dB 문자 표시 여부 입니다."), DefaultValue(false), Category(ProgressBarGradientInternal.CategoryName)]
        bool ShowdB{get;set;}    
        
        /// <summary>
        /// dB 문자 사이 공백을 포함 여부를 가져오거나 설정합니다.
        /// </summary>
        [Description("dB 문자 사이 공백을 포함 여부 입니다."), DefaultValue(false), Category(ProgressBarGradientInternal.CategoryName)]
        bool SpacedB{get;set;}

        /// <summary>
        /// 뒤에 dB 표시 여부를 가져오거나 설정합니다.
        /// </summary>
        [Description("숫자 뒤에 음수 부호 표시 여부 입니다."), DefaultValue(false), Category(ProgressBarGradientInternal.CategoryName)]
        bool ShowMinus{get;set;}

        /// <summary>
        /// 뒤에 dB 표시 여부를 가져오거나 설정합니다.
        /// </summary>
        [Description("숫자 뒤에 양수 부호 표시 여부 입니다."), DefaultValue(true), Category(ProgressBarGradientInternal.CategoryName)]
        bool ShowPlus{get;set;}

        /// <summary
        /// 데시벨 레이블에 테두리 표시 여부를 가져오거나 설정합니다.
        [Description("데시벨 레이블에 테두리 표시 여부 입니다."), DefaultValue(false), Category(ProgressBarGradientInternal.CategoryName)]
        bool ShowEdge{get;set;}

        /// <summary>
        /// 데시벨 레이블의 질감을 가져오거나 설정합니다.
        /// </summary>
        [Description("데시벨 레이블의 질감 입니다."), DefaultValue(System.Drawing.Text.TextRenderingHint.SystemDefault), Category(ProgressBarGradientInternal.CategoryName)]
        System.Drawing.Text.TextRenderingHint TextRendering{get;set;}

        /// <summary>
        /// 데시벨 레이블의 글꼴을 가져오거나 설정합니다.
        /// </summary>
        [Description("데시벨 레이블의 글꼴 입니다."), Category(ProgressBarGradientInternal.CategoryName)]
        Font Font{get;set;}

        /// <summary>
        /// 데시벨 레이블의 표시 여부를 가져오거나 설정합니다.
        /// </summary>
        [Description("데시벨 레이블의 표시 여부 입니다."), DefaultValue(false), Category(ProgressBarGradientInternal.CategoryName)]
        bool VisibledB{get;set;}

        /// <summary>
        /// 데시벨 레이블의 표시 여부를 가져오거나 설정합니다.
        /// </summary>
        [Description("데시벨 레이블의 표시 간격 입니다."), DefaultValue(2), Category(ProgressBarGradientInternal.CategoryName)]
        int IntervaldB{get;set;}

        /// <summary>
        /// 데시벨 레이블의 색상을 가져오거나 설정합니다.
        /// </summary>
        [Description("데시벨 레이블의 색상 입니다."), DefaultValue(typeof(Color), "Black"), Category(ProgressBarGradientInternal.CategoryName)]
         new Color ForeColor{get;set;}

        /// <summary
        /// 데시벨 레이블의 깜빡거림 감소 여부를 가져오거나 설정합니다. (대신 성능은 감소 합니다.)
        [Description("데시벨 레이블의 깜빡거림 감소 여부 입니다. (대신 성능은 감소 합니다.)"), DefaultValue(true), Category(ProgressBarGradientInternal.CategoryName)]
         bool FlickerReduce{get;set;}

         void Draw();
    }
}
