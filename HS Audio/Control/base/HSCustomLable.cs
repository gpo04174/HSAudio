﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using System.ComponentModel;
using System.Windows.Forms;

namespace HS_Audio.Control
{
    class HSCustomLable : System.Windows.Forms.Label
    {
        public HSCustomLable() : base()
        {
            SetStyle(ControlStyles.SupportsTransparentBackColor, true);
        }
        #region Properties
        [Description("회전 (각도) 입니다.")]
        public int RotateAngle
        {
            get;
            set;
        }

        [Description("표시할 문자 입니다.")]
        // to rotate your text
        public string NewText
        {
            get;
            set;
        }
        [Description(""), DefaultValue(false)]
        public bool DrawColor{get;set;}

        bool _DrawColorAnimation = false;
        [Description(""), DefaultValue(false)]
        public bool DrawColorAnimation{get{return _DrawColorAnimation;}set{_DrawColorAnimation = value;}}

        int _DrawColorAnimationInterval = 10;
        [Description(""), DefaultValue(10)]
        public int DrawColorAnimationInterval{get{return _DrawColorAnimationInterval;}set{_DrawColorAnimationInterval = value==0?1:Math.Abs(value);}}

        [Description("")]
        public string[] Words{get{return Text.Split(null);}}
        #endregion

        class SetDrawColorClass{public SetDrawColorClass(){} public SetDrawColorClass(int Index, Color Color){this.Index = Index;this.Color = Color;} public int Index; public Color Color;}
        Dictionary<int,Color> draw = new Dictionary<int,Color>();
        public void SetDrawColor(int index, Color Color)
        {
            brush = null;
            //draw.Add(new SetDrawColorClass(index, Color));
            if(draw.ContainsKey(index))draw[index] = Color;
            else draw.Add(index, Color);
            this.Invalidate();
        }
        Brush brush = null;
        public void SetDrawColor(Brush Brush)
        {
            brush = Brush;
            this.Invalidate();
        }
        public Color GetDrawColor(int Index, bool ThrowException = true){if(draw.ContainsKey(Index))return draw[Index];else if(ThrowException)throw new Exception("d");else return ForeColor;}
        public void ClearDrawColor(){draw.Clear();}
        public bool DeleteDrawColor(int Index){if(draw.ContainsKey(Index))draw.Remove(Index); return draw.ContainsKey(Index);}

        // to draw text
        protected override void OnPaint(System.Windows.Forms.PaintEventArgs e)
        {
           
            /*
            Brush b = new SolidBrush(this.ForeColor);
            e.Graphics.TranslateTransform(this.Width / 2, this.Height / 2);
            e.Graphics.RotateTransform(this.RotateAngle);
            e.Graphics.DrawString(this.Text, this.Font, b, 0f, 0f);
            base.OnPaint(e);*/
            /*
            Func<double, double> DegToRad = (angle) => Math.PI * angle / 180.0;

            Brush b = new SolidBrush(this.ForeColor);
            SizeF size = e.Graphics.MeasureString(this.NewText, this.Font, this.Parent.Width);

            int normalAngle = ((RotateAngle % 360) + 360) % 360;
            double normaleRads = DegToRad(normalAngle);

            int hSinTheta = (int)Math.Ceiling((size.Height * Math.Sin(normaleRads)));
            int wCosTheta = (int)Math.Ceiling((size.Width * Math.Cos(normaleRads)));
            int wSinTheta = (int)Math.Ceiling((size.Width * Math.Sin(normaleRads)));
            int hCosTheta = (int)Math.Ceiling((size.Height * Math.Cos(normaleRads)));

            int rotatedWidth = Math.Abs(hSinTheta) + Math.Abs(wCosTheta);
            int rotatedHeight = Math.Abs(wSinTheta) + Math.Abs(hCosTheta);

            this.Width = rotatedWidth;
            this.Height = rotatedHeight;

            int numQuadrants =
                (normalAngle >= 0 && normalAngle < 90) ? 1 :
                (normalAngle >= 90 && normalAngle < 180) ? 2 :
                (normalAngle >= 180 && normalAngle < 270) ? 3 :
                (normalAngle >= 270 && normalAngle < 360) ? 4 :
                0;

            int horizShift = 0;
            int vertShift = 0;

            if (numQuadrants == 1)
            {
                horizShift = Math.Abs(hSinTheta);
            }
            else if (numQuadrants == 2)
            {
                horizShift = rotatedWidth;
                vertShift = Math.Abs(hCosTheta);
            }
            else if (numQuadrants == 3)
            {
                horizShift = Math.Abs(wCosTheta);
                vertShift = rotatedHeight;
            }
            else if (numQuadrants == 4)
            {
                vertShift = Math.Abs(wSinTheta);
            }

            e.Graphics.TranslateTransform(horizShift, vertShift);
            e.Graphics.RotateTransform(this.RotateAngle);

            e.Graphics.DrawString(this.NewText, this.Font, b, 0f, 0f);
            base.OnPaint(e);*/
            /*
            float vlblControlWidth;
            float vlblControlHeight;
            float vlblTransformX;
            float vlblTransformY;

            Color controlBackColor = BackColor;
            Pen labelBorderPen;
            SolidBrush labelBackColorBrush;

            if (_transparentBG)
            {
                labelBorderPen = new Pen(Color.Empty, 0);
                labelBackColorBrush = new SolidBrush(Color.Empty);
            }
            else
            {
                labelBorderPen = new Pen(controlBackColor, 0);
                labelBackColorBrush = new SolidBrush(controlBackColor);
            }

            SolidBrush labelForeColorBrush = new SolidBrush(base.ForeColor);
            base.OnPaint(e);
            vlblControlWidth = this.Size.Width;
            vlblControlHeight = this.Size.Height;
            e.Graphics.DrawRectangle(labelBorderPen, 0, 0,
                                     vlblControlWidth, vlblControlHeight);
            e.Graphics.FillRectangle(labelBackColorBrush, 0, 0,
                                     vlblControlWidth, vlblControlHeight);
            e.Graphics.TextRenderingHint = this._renderMode;
            e.Graphics.SmoothingMode =
                       System.Drawing.Drawing2D.SmoothingMode.HighQuality;

            //if (this.TextDrawMode == DrawMode.BottomUp)
            if (this.RotateAngle == DrawMode.BottomUp)
            {
                vlblTransformX = 0;
                vlblTransformY = vlblControlHeight;
                e.Graphics.TranslateTransform(vlblTransformX, vlblTransformY);
                e.Graphics.RotateTransform(RotateAngle);
                e.Graphics.DrawString(labelText, Font, labelForeColorBrush, 0, 0);
            }
            else
            {
                vlblTransformX = vlblControlWidth;
                vlblTransformY = vlblControlHeight;
                e.Graphics.TranslateTransform(vlblControlWidth, 0);
                e.Graphics.RotateTransform(90);
                e.Graphics.DrawString(labelText, Font, labelForeColorBrush, 0, 0,
                                      StringFormat.GenericTypographic);
            }*/
            base.OnPaint(e);

        }
    }
}
