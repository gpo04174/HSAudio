﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace HS_Audio.Control
{
    public partial class HSLEDMeter : UserControl
    {
        public enum ControlDirection {Horizon,Vertical }
        public HSLEDMeter()
        {
            InitializeComponent();
        }
        
        internal ControlDirection Direction {get;set;}

        int _MaxValue = 100;
        public int MaxValue{get{return _MaxValue;}set{_MaxValue = value;}}

        int _MinValue = 0;
        public int MinValue{get{return _MinValue;}set{_MinValue = value;}}

        int _Value = 0;
        public int Value{get{return _Value;}set{if(value>MaxValue)_Value = MaxValue;else if(value<MinValue)_Value=MinValue;else _Value = value;Redraw();}}

        Color _HighColor = Color.Red;
        public Color HighColor{get{return _HighColor;}set{_HighColor = value;Redraw();}}

        Color _MidColor = Color.Yellow;
        public Color MidColor{get{return _MidColor;}set{_MidColor = value;Redraw();}}

        Color _LowColor = Color.Lime;
        public Color LowColor{get{return _LowColor;}set{_LowColor = value;Redraw();}}

        int _HighLED = 2;
        public int HighLED{get{return _HighLED;}set{_HighLED = Math.Max(1,value);}}

        int _LEDSize = 24;
        public int LEDSize{get{return _LEDSize;}set{_LEDSize = Math.Max(1,value);Redraw();}}


        List<LedBulb> bulb = new List<LedBulb>(30);
    
        private void HSLEDMeter_Load(object sender, EventArgs e)
        {

        }

        private void HSLEDMeter_Resize(object sender, EventArgs e)
        {
            Redraw();
        }

        internal void Redraw()
        {
            for (int i = 0; i < bulb.Count; i++)
            {
                bulb[i].Size = new Size(LEDSize, LEDSize);
            }
        }
        
    }
}
