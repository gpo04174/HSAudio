﻿using System;
using System.Drawing;
using System.Windows.Forms;
using System.Collections;
using System.Drawing.Imaging;
using System.Collections.Generic;

namespace HS_Audio.Control
{
    public partial class HSSpectogramViewer : UserControl
    {

        public enum DrawKind{Spectrogram,FrequencyDomain,TimeDomain}
        public float[] Data{get;set;}

        AudioFrame af = new AudioFrame();
        volatile Bitmap bmp = new Bitmap(832, 129);
        public HSSpectogramViewer()
        {
            InitializeComponent();
            this.DoubleBuffered = true;
            using (Graphics g = Graphics.FromImage(bmp)){try{g.Clear(BackColor);g.DrawImage(BackgroundImage, 0, 0);}catch{}}
        }

        DrawKind _Kind = DrawKind.Spectrogram;
        public DrawKind Kind{get{return _Kind;}set{_Kind = value;try{this.Invalidate();}catch{}}}

        Color _DrawColor = Color.White;
        public Color DrawColor{get{return _DrawColor;}set{_DrawColor = value;}}

        public bool UseAlpha{get;set;}

        volatile float _MindB = AudioFrame.MinimamdB;
        public float MindB{get{return _MindB;}set{_MindB = value;af.MindB = value;}}

        public bool Reverse{get;set;}

        public void Draw(float[] Data, float 오차 = 1)
        {
            this.Draw(Data, 오차, DrawWidth?this.Width:this.Height);
        }

        public bool DrawWidth = true;
        volatile bool LOCK;
        List<float[]> FFTSpec = new List<float[]>();
        public void Draw(float[] Data, float 오차 = 1, int Maxbuffer = 50000)
        {
            LOCK = true;
            try
            {
                this.Data = Data;
                try {if (FFTSpec.Count > Maxbuffer) FFTSpec.RemoveRange(0, Math.Max(Maxbuffer-FFTSpec.Count, 0));}catch{}finally{FFTSpec.Add(Data);}
                //bmp1 = new Bitmap(this.Width, this.Height);
                //using (Graphics g = Graphics.FromImage(bmp1)){try{g.Clear(Color.Transparent);}catch{bmp1.Dispose();return;}/*try{g.DrawImage(bmp, 0, 0);}catch{}*/}
                switch (Kind)
                {
                    case DrawKind.Spectrogram: 
                        if (DrawWidth) try{bmp = af.RenderSpectrogram_Width(FFTSpec, bmp, DrawColor, false, true, 오차,1, Reverse);}catch{}
                        else {bmp = af.RenderSpectrogram_Height(FFTSpec, bmp, DrawColor, false, true, 오차, Reverse);/*this.BackgroundImage = bmp1;*/}break;
                }
            }
            finally{/*try{bmp2.Dispose();}catch{}*/this.Invalidate(); LOCK = false;}
        }
        public void DisposeInstance(){ try{af.Dispose();}catch{}}
        public void CreateInstance(){DisposeInstance();af = new AudioFrame();try{bmp.Dispose();}catch{}bmp = new Bitmap(this.Width, this.Height);using (Graphics g = Graphics.FromImage(bmp)){try{g.DrawImage(BackgroundImage, 0, 0);}catch{}}}

        private void HSSpectogramViewer_SizeChanged(object sender, EventArgs e)
        {
            try
            {
                if(!LOCK){
                Bitmap bmp1 = new Bitmap(this.Width, this.Height);
                using (Graphics g = Graphics.FromImage(bmp1)) g.DrawImage(bmp, 0, 0);
                Bitmap tmp = bmp;
                bmp = bmp1;
                tmp.Dispose();}
            }catch{}
        }
        
        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);
            try{e.Graphics.DrawImage(bmp, 0, 0);}catch{}
        }
    }
    public class AudioFrame:IDisposable
    {
        public const int MinimamdB = -100;
        //private double[] _wave;
        //private double[] _fft;
        private List<float[]> _fftSpect = new List<float[]>();
        private int _maxHeightLeftSpect = 0;
        public float MindB = MinimamdB;
        //public bool IsDetectingEvents = false;
        //public bool IsEventActive = false;
        public int AmplitudeThreshold = 16384;

        public AudioFrame()
        {
        }

        /// <summary>
        /// Process 16 bit sample
        /// </summary>
        /// <param name="wave"></param>
        public static double[] Process(ref byte[] wave)
        {
            double[] wav = new double[wave.Length / 2];

            // Split out channels from sample
            int h = 0;
            for (int i = 0; i < wave.Length; i += 2)
            {
                wav[h] = (double)BitConverter.ToInt16(wave, i);
                h++;
            }
            return wav;
        }

        /// <summary>
        /// Render time domain to PictureBox
        /// </summary>
        public void RenderTimeDomain(float[] sample, Bitmap canvas)
        {
            // Set up for drawing
            //Bitmap canvas = new Bitmap(Width, Height);
            Graphics offScreenDC = Graphics.FromImage(canvas);
            Pen pen = new System.Drawing.Pen(Color.WhiteSmoke);

            // Determine channnel boundries
            int width = canvas.Width;
            int height = canvas.Height;
            float center = height / 2;

            // Draw left channel (필요하면 0.5 떼기)
            float scale = 0.5f * height;  // a 16 bit sample has values from -32768 to 32767
            int xPrev = 0, yPrev = 0;
            for (int x = 0; x < width; x++)
            {
                int y = (int)(center + (sample[sample.Length / width * x] * scale));
                if (x == 0)
                {
                    xPrev = 0;
                    yPrev = y;
                }
                else
                {
                    pen.Color = Color.Green;
                    offScreenDC.DrawLine(pen, xPrev, yPrev, x, y);
                    xPrev = x;
                    yPrev = y;
                }
            }

            // Clean up
            //img.Image = canvas;
            offScreenDC.Dispose();
        }
        /// <summary>
        /// Render time domain to PictureBox
        /// </summary>
        public void RenderTimeDomain(float[] sample, Graphics g, int Width, int Height,bool Dispose = false)
        {
            // Set up for drawing
            //Bitmap canvas = new Bitmap(Width, Height);
            Graphics offScreenDC = g;
            Pen pen = new System.Drawing.Pen(Color.WhiteSmoke);

            // Determine channnel boundries
            int width = Width;
            int height = Height;
            float center = height / 2;

            // Draw left channel (필요하면 0.5 떼기)
            float scale = 0.5f * height;  // a 16 bit sample has values from -32768 to 32767
            int xPrev = 0, yPrev = 0;
            for (int x = 0; x < width; x++)
            {
                int y = (int)(center + (sample[sample.Length / width * x] * scale));
                if (x == 0)
                {
                    xPrev = 0;
                    yPrev = y;
                }
                else
                {
                    pen.Color = Color.Green;
                    offScreenDC.DrawLine(pen, xPrev, yPrev, x, y);
                    xPrev = x;
                    yPrev = y;
                }
            }

            // Clean up
            //img.Image = canvas;
            if (Dispose) offScreenDC.Dispose();
        }

        /// <summary>
        /// Render frequency domain to PictureBox
        /// </summary>
        /// <param name="pictureBox"></param>
        /// <param name="samples"></param>
        public void RenderFrequencyDomain(Bitmap canvas, float[] FFT, int samples)
        {
            // Set up for drawing
            //Bitmap canvas = new Bitmap(pictureBox.Width, pictureBox.Height);
            Graphics offScreenDC = Graphics.FromImage(canvas);
            SolidBrush brush = new System.Drawing.SolidBrush(Color.FromArgb(128, 255, 255, 255));
            Pen pen = new System.Drawing.Pen(Color.WhiteSmoke);
            Font font = new Font("Arial", 10);

            // Determine channnel boundries
            int width = canvas.Width;
            int height = canvas.Height;

            float min = float.MaxValue;
            float minHz = 0;
            float max = float.MinValue;
            float maxHz = 0;
            float range = 0;
            float scale = 0;
            float scaleHz = (float)(samples / 2) / (float)FFT.Length;

            // get left min/max
            for (int x = 0; x < FFT.Length; x++)
            {
                float amplitude = FFT[x];
                if (min > amplitude)
                {
                    min = amplitude;
                    minHz = (float)x * scaleHz;
                }
                if (max < amplitude)
                {
                    max = amplitude;
                    maxHz = (float)x * scaleHz;
                }
            }

            // get left range
            if (min < 0 || max < 0)
                if (min < 0 && max < 0)
                    range = max - min;
                else
                    range = Math.Abs(min) + max;
            else
                range = max - min;
            scale = range / height;

            // draw left channel
            for (int xAxis = 0; xAxis < width; xAxis++)
            {
                float amplitude = FFT[(int)(((float)(FFT.Length) / width) * xAxis)];
                if (amplitude == double.NegativeInfinity || amplitude == float.PositiveInfinity || amplitude == float.MinValue || amplitude == double.MaxValue)
                    amplitude = 0;
                int yAxis;
                if (amplitude < 0)
                    yAxis = (int)(height - ((amplitude - min) / scale));
                else
                    yAxis = (int)(0 + ((max - amplitude) / scale));
                if (yAxis < 0)
                    yAxis = 0;
                if (yAxis > height)
                    yAxis = height;
                pen.Color = pen.Color = Color.FromArgb(0, (int)GetColor(min, max, range, amplitude), 0);
                offScreenDC.DrawLine(pen, xAxis, height, xAxis, yAxis);
            }
            offScreenDC.DrawString("Min: " + minHz.ToString(".#") + " Hz (?" + scaleHz.ToString(".#") + ") = " + min.ToString(".###") + " dB", font, brush, 0 + 1, 0 + 1);
            offScreenDC.DrawString("Max: " + maxHz.ToString(".#") + " Hz (?" + scaleHz.ToString(".#") + ") = " + max.ToString(".###") + " dB", font, brush, 0 + 1, 0 + 18);

            // Clean up
            //pictureBox.Image = canvas;
            offScreenDC.Dispose();
        }

        /// <summary>
        /// Render waterfall spectrogram to PictureBox
        /// </summary>
        public void RenderSpectrogram(float[] FFT, Bitmap canvas, int DrawLength, bool Log10 = true, float 오차 = 1, bool Reverse = false)
        {
            if (_fftSpect.Count > DrawLength) _fftSpect.RemoveRange(0, Math.Max(_fftSpect.Count - DrawLength+1,0));
            _fftSpect.Add(FFT);
            //Bitmap canvas = new Bitmap(pictureBox.Width, pictureBox.Height);
            //Graphics offScreenDC = Graphics.FromImage(canvas);

            // Determine channnel boundries
            int width = canvas.Width;
            int height = canvas.Height;

            float min = this.MindB;//float.MaxValue
            float max = float.MinValue;
            float range = 0;

            //if (width > _maxHeightLeftSpect)
            //    _maxHeightLeftSpect = width;

            // get min/max
            //for (int w = 0; w < _fftSpect.Count; w++)
                //for (int x = 0; x < (_fftSpect[w]).Length; x++)
                for (int x = 0; x < FFT.Length; x++)
                {
                    //float amplitude = (float)HS_Audio_Helper.Forms.frmFFTGraph.Decibels(_fftSpect[w][x] * 오차, 0);
                    float amplitude = Log10 ? (float)HS_Audio.Forms.frmFFTGraph.Decibels(FFT[x], 0) : FFT[x];
                    //if (min > amplitude) min = amplitude;
                    if (max < amplitude) max = amplitude;
                }

            // get range
            if (min < 0 || max < 0)
                if (min < 0 && max < 0)
                    range = max - min;
                else
                    range = Math.Abs(min) + max;
            else
                range = max - min;

            // lock image
            PixelFormat format = canvas.PixelFormat;
            BitmapData data = canvas.LockBits(new Rectangle(0, 0, width, height), ImageLockMode.ReadOnly, format);
            // unlock image
            canvas.UnlockBits(data);
            int stride = data.Stride;
            int offset = stride - width * 4;

            try
            {
                bool _Reverse = Reverse;
                float _오차 = 오차;
                unsafe
                {
                    byte* pixel = (byte*)data.Scan0.ToPointer();

                    int y = 0;
                    //pixel += 4;
                    for (int x = 0; x <= width; x++)
                    {
                        if (x < _fftSpect.Count)
                        {
                            //pixel -= x;
                            // for each row
                                for (y = 0; y < height; y++)//, pixel += 4)
                                {
                                    try
                                    {
                                        float[] Sam = (float[])_fftSpect[_fftSpect.Count - x - 1];
                                        float size = (float)Sam.Length / (float)height;
                                        int index = (int)(size * y);

                                        float amplitude = (float)HS_Audio.Forms.frmFFTGraph.Decibels(Sam[index] * _오차, 0);
                                        float color = amplitude <= min?0:
                                                      amplitude >= max?255:
                                                      GetColorLimit(min, max, range, amplitude);

                                        byte c = (byte)color;
                                        if ((stride * (height - y - 1)) - (4 * x) >= 0 && _Reverse)
                                        {
                                            pixel[(stride * (height - y - 1)) - (4 * x)] = c;
                                            pixel[(stride * (height - y - 1)) - (4 * x) + 1] = c;
                                            pixel[(stride * (height - y - 1)) - (4 * x) + 2] = c;
                                            pixel[(stride * (height - y - 1)) - (4 * x) + 3] = (byte)255;
                                        }
                                        else
                                        {
                                            pixel[(stride * (height - y - 1)) + (4 * x)] = c;
                                            pixel[(stride * (height - y - 1)) + (4 * x) + 1] = c;
                                            pixel[(stride * (height - y - 1)) + (4 * x) + 2] = c;
                                            pixel[(stride * (height - y - 1)) + (4 * x) + 3] = (byte)255;
                                        }
                                    }
                                    catch (AccessViolationException){break;}
                                    catch{}
                                }
                            pixel += offset;
                        }
                        else{break;}
                    }

                    /*
                    // for each cloumn
                    for (int y = 0; y <= height; y++)
                    {
                        if (y < _fftSpect.Count)
                        {
                            // for each row
                            for (int x = 0; x < width; x++, pixel += 4)
                            {
                                try{
                                float[] Sam = (float[])_fftSpect[_fftSpect.Count - y - 1];
                                float size = (float)Sam.Length / (float)width;
                                int index = (int)(size * x);

                                double amplitude = HS_Audio_Helper.Forms.frmFFTGraph.Decibels(Sam[index] * 오차, 0);
                                float color = amplitude <= min?0:GetColor(min, max, range, amplitude);
                                pixel[0] = (byte)0;
                                pixel[1] = (byte)color;
                                pixel[2] = (byte)0;
                                pixel[3] = (byte)255;
                                //canvas.SetPixel(x, y, Color.FromArgb(0, (byte)color, 0, 255));
                                }
                                catch(AccessViolationException){break;}
                                catch{}
                            }
                            pixel += offset;
                        }
                        else{}
                    }*/
                }
            }
            catch (Exception ex)
            {
                HS_Library.HSConsole.WriteLine(ex.ToString());
            }


            // Clean up
            //pictureBox.Image = canvas;
            //offScreenDC.Dispose();
        }

        unsafe byte* pixel;
        Bitmap Lockbmp;
        bool RenderSpectrogramDispose;
        public Bitmap RenderSpectrogram_Width(List<float[]> FFT, Bitmap canvas, Color CustomColor, bool UseAlpha, bool Log10 = true, float 오차 = 1, byte Bold = 1, bool Reverse = false)
        {
            Bitmap canvas1 = canvas;
            int _Bold = Bold<2?0:Bold;
            //Graphics offScreenDC = Graphics.FromImage(canvas);

            //int _DrawLength =DrawLength;
            float min = MindB;//float.MaxValue
            float max = float.MinValue;
            float range = 0;

            // Determine channnel boundries
            int width = canvas1.Width;
            int height = canvas1.Height;

            bool _Log10 = Log10;
            List<float[]> _fftSpect = FFT;
            
            try
            {
                //if (width > _maxHeightLeftSpect)
                //    _maxHeightLeftSpect = width;

                // get min/max
                //for (int w = 0; w < _fftSpect.Count; w++)
                //for (int x = 0; x < (_fftSpect[w]).Length; x++)
                for (int x = 0; x < _fftSpect[_fftSpect.Count - 1].Length; x++)
                {
                    //float amplitude = (float)HS_Audio_Helper.Forms.frmFFTGraph.Decibels(_fftSpect[w][x] * 오차, 0);
                    float amplitude = _Log10?(float)HS_Audio.Forms.frmFFTGraph.Decibels(_fftSpect[_fftSpect.Count - 1][x], 0):
                                             _fftSpect[_fftSpect.Count - 1][x];
                    //if (min > amplitude) min = amplitude;
                    if (max < amplitude) max = amplitude;
                }

                // get range
                if (min < 0 || max < 0)
                    if (min < 0 && max < 0)
                        range = max - min;
                    else
                        range = Math.Abs(min) + max;
                else
                    range = max - min;
            }catch{}
            //Lockbmp = canvas;
            // lock image
            PixelFormat format = canvas1.PixelFormat;
            //try{canvas.UnlockBits(data);}catch{}
            BitmapData data = canvas1.LockBits(new Rectangle(0, 0, width, height), ImageLockMode.ReadWrite, format);

            int stride = data.Stride;
            int offset = stride - width * 4;

            try
            {
                bool _Reverse = Reverse;
                float _오차 = 오차;
                unsafe
                {
                    pixel = (byte*)data.Scan0.ToPointer();

                    int y = 0;
                    int a = 0;
                    //pixel += 4;
                    for (int x = 0; x < width+_Bold; x++)
                    {
                        if (x < _fftSpect.Count)
                        {
                            float[] Sam = _fftSpect[_fftSpect.Count-1 - x];
                            //pixel -= x;
                            // for each row
                            for (y = 0; y < height; y++)//, pixel += 4)
                            {
                                try
                                {
                                    float size = (float)Sam.Length / (float)height;
                                    int index = (int)(size * y);

                                    float amplitude = _Log10?(float)HS_Audio.Forms.frmFFTGraph.Decibels(Sam[index] * _오차, 0):
                                                             Sam[index] * _오차;
                                    float color = amplitude <= min?0:
                                                  amplitude >= max?255:
                                                  GetColor(min, max, range, amplitude);
                                    if (y < height-1 && _Reverse)
                                    {
                                        a = (stride * (height - y - 1)) - (x << 2);//x<<2 = x*4
                                        pixel[a] = (byte)((CustomColor.B * color) / 255);
                                        pixel[a + 1] = (byte)((CustomColor.G * color) / 255);
                                        pixel[a + 2] = (byte)((CustomColor.R * color) / 255);
                                        if (offset == 0) pixel[a + 3] = (byte)(UseAlpha ? (CustomColor.A * color) / 255 : 255);
                                    }
                                    else
                                    {
                                        a = (stride * (height - y - 1)) + (x << 2);//x<<2 = x*4
                                        pixel[a] = (byte)((CustomColor.B * color) / 255f);
                                        pixel[a + 1] = (byte)((CustomColor.G * color) / 255f);
                                        pixel[a + 2] = (byte)((CustomColor.R * color) / 255f);
                                        if (offset == 0) pixel[a + 3] = (byte)((CustomColor.A * color) / 255f);
                                    }
                                }
                                catch (AccessViolationException){ break; }
                                catch{ break; }
                            }
                            pixel += offset;
                        }
                        else break;
                    }
                    /* 새로로
                    // for each cloumn
                    for (int y = 0; y <= height; y++)
                    {
                        if (y < _fftSpect.Count)
                        {
                            // for each row
                            for (int x = 0; x < width; x++, pixel += 4)
                            {
                                try{
                                float[] Sam = (float[])_fftSpect[_fftSpect.Count - y - 1];
                                float size = (float)Sam.Length / (float)width;
                                int index = (int)(size * x);

                                double amplitude = HS_Audio_Helper.Forms.frmFFTGraph.Decibels(Sam[index] * 오차, 0);
                                float color = amplitude <= min?0:GetColor(min, max, range, amplitude);
                                pixel[0] = (byte)0;
                                pixel[1] = (byte)color;
                                pixel[2] = (byte)0;
                                pixel[3] = (byte)255;
                                //canvas.SetPixel(x, y, Color.FromArgb(0, (byte)color, 0, 255));
                                }
                                catch(AccessViolationException){break;}
                                catch{}
                            }
                            pixel += offset;
                        }
                        else{}
                    }*/
                }
            }
            catch (Exception ex)
            {
                HS_Library.HSConsole.WriteLine(ex.ToString());
            }
            //data = null;

            // Clean up
            //pictureBox.Image = canvas;
            //offScreenDC.Dispose();
            finally
            {
                // unlock image
                if (data != null) canvas1.UnlockBits(data); 
                if (RenderSpectrogramDispose) canvas1.Dispose();
            }
            return canvas1;
        }

        public void RenderSpectrogram_Height(float[] FFT, Bitmap canvas, Color CustomColor, bool UseAlpha, int DrawLength, bool Log10 = true, float 오차 = 1, bool Reverse = false)
        {
            //Bitmap canvas = new Bitmap(pictureBox.Width, pictureBox.Height);
            //Graphics offScreenDC = Graphics.FromImage(canvas);

            // Determine channnel boundries
            int width = canvas.Width;
            int height = canvas.Height;

            float min = MindB;//float.MaxValue
            float max = float.MinValue;
            float range = 0;

            try
            {
                if (_fftSpect.Count > DrawLength) _fftSpect.RemoveRange(0, Math.Max(_fftSpect.Count - DrawLength + 1, 0));
                _fftSpect.Add(FFT);

                //if (width > _maxHeightLeftSpect)
                //    _maxHeightLeftSpect = width;

                // get min/max
                //for (int w = 0; w < _fftSpect.Count; w++)
                //for (int x = 0; x < (_fftSpect[w]).Length; x++)
                for (int x = 0; x < FFT.Length; x++)
                {
                    //float amplitude = (float)HS_Audio_Helper.Forms.frmFFTGraph.Decibels(_fftSpect[w][x] * 오차, 0);
                    float amplitude = (float)HS_Audio.Forms.frmFFTGraph.Decibels(FFT[x], 0);
                    //if (min > amplitude) min = amplitude;
                    if (max < amplitude) max = amplitude;
                }

                // get range
                if (min < 0 || max < 0)
                    if (min < 0 && max < 0)
                        range = max - min;
                    else
                        range = Math.Abs(min) + max;
                else
                    range = max - min;
            }catch{}
            Lockbmp = canvas;
            // lock image
            PixelFormat format = canvas.PixelFormat;
            //try{canvas.UnlockBits(data);}catch{}
            BitmapData data = canvas.LockBits(new Rectangle(0, 0, width, height), ImageLockMode.ReadWrite, format);
            // unlock image
            if (data != null) canvas.UnlockBits(data);

            int stride = data.Stride;
            int offset = stride - height * 4;

            try
            {
                bool _Reverse = Reverse;
                float _오차 = 오차;
                unsafe
                {
                    pixel = (byte*)data.Scan0.ToPointer();

                    // for each cloumn
                    for (int y = 0; y <= height; y++)
                    {
                        if (y < _fftSpect.Count)
                        {
                            // for each row
                            for (int x = 0; x < width; x++, pixel += 4)
                            {
                                try{
                                float[] Sam = (float[])_fftSpect[_fftSpect.Count - y - 1];
                                //float size = (float)Sam.Length / (float)width;
                                int index = (int)(((float)FFT.Length / (float)width) * x);//(int)(size * x);

                                float amplitude = (float)HS_Audio.Forms.frmFFTGraph.Decibels(Sam[index] * 오차, 0);
                                float color = amplitude <= min?0:
                                              amplitude >= max?255:
                                              GetColor(min, max, range, amplitude);
                                pixel[0] = (byte)0;
                                pixel[1] = (byte)color;
                                pixel[2] = (byte)0;
                                pixel[3] = (byte)255;
                                //canvas.SetPixel(x, y, Color.FromArgb(0, (byte)color, 0, 255));
                                }
                                catch(AccessViolationException){break;}
                                catch{}
                            }
                            pixel += offset;
                        }
                        else{}
                    }
                }
            }
            catch (Exception ex)
            {
                HS_Library.HSConsole.WriteLine(ex.ToString());
            }
            //data = null;

            // Clean up
            //pictureBox.Image = canvas;
            //offScreenDC.Dispose();
        }
        public Bitmap RenderSpectrogram_Height(List<float[]> FFT, Bitmap canvas, Color CustomColor, bool UseAlpha,  bool Log10 = true, float 오차 = 1, bool Reverse = false)
        {
            Bitmap canvas1 = canvas;
            //Bitmap canvas = new Bitmap(pictureBox.Width, pictureBox.Height);
            //Graphics offScreenDC = Graphics.FromImage(canvas);

            // Determine channnel boundries
            int width = canvas1.Width;
            int height = canvas1.Height;

            float min = MindB;//float.MaxValue
            float max = float.MinValue;
            float range = 0;

            bool _Log10 = Log10;
            List<float[]> _fftSpect = FFT;
            
            try
            {
                //if (width > _maxHeightLeftSpect)
                //    _maxHeightLeftSpect = width;

                // get min/max
                //for (int w = 0; w < _fftSpect.Count; w++)
                //for (int x = 0; x < (_fftSpect[w]).Length; x++)
                for (int x = 0; x < _fftSpect[_fftSpect.Count - 1].Length; x++)
                {
                    //float amplitude = (float)HS_Audio_Helper.Forms.frmFFTGraph.Decibels(_fftSpect[w][x] * 오차, 0);
                    float amplitude = _Log10 ? (float)HS_Audio.Forms.frmFFTGraph.Decibels(_fftSpect[_fftSpect.Count - 1][x], 0) :
                                               _fftSpect[_fftSpect.Count - 1][x];
                    //if (min > amplitude) min = amplitude;
                    if (max < amplitude) max = amplitude;
                }

                // get range
                if (min < 0 || max < 0)
                    if (min < 0 && max < 0)
                        range = max - min;
                    else
                        range = Math.Abs(min) + max;
                else
                    range = max - min;
            }catch{}
            //Lockbmp = canvas;
            // lock image
            PixelFormat format = canvas1.PixelFormat;
            //try{canvas.UnlockBits(data);}catch{}
            BitmapData data = canvas1.LockBits(new Rectangle(0, 0, width, height), ImageLockMode.ReadWrite, format);

            int stride = data.Stride;
            int offset = stride - width * 4;

            try
            {
                bool _Reverse = Reverse;
                float _오차 = 오차;
                int a = 0;
                unsafe
                {
                    pixel = (byte*)data.Scan0.ToPointer();

                    // for each cloumn
                    for (int y = 0; y < height; y++)
                    {
                        if (y < _fftSpect.Count)
                        {
                            // for each row
                            for (int x = 0; x < width; x++)
                            {
                                try{

                                float[] Sam = _fftSpect[_fftSpect.Count - y - 1];
                                float size = (float)Sam.Length / (float)width;
                                int index = (int)(size * x);//(int)(size * x);

                                float amplitude = _Log10 ? (float)HS_Audio.Forms.frmFFTGraph.Decibels(Sam[index] * _오차, 0):
                                                           Sam[index] * _오차;
                                float color = amplitude <= min?0:
                                              amplitude >= max?255:
                                              GetColor(min, max, range, amplitude);

                                if (_Reverse)
                                {
                                    a = (stride*(height - y - 1)) + (x<<2);
                                    pixel[a] = (byte)((CustomColor.B * color) / 255);
                                    pixel[a+1] = (byte)((CustomColor.G * color) / 255);
                                    pixel[a+2] = (byte)((CustomColor.R * color) / 255);
                                    if (offset == 0) pixel[a + 3] = (byte)(UseAlpha ? (CustomColor.A * color) / 255 : color);
                                }
                                else
                                {
                                    pixel[0] = (byte)((CustomColor.B * color) / 255);
                                    pixel[1] = (byte)((CustomColor.G * color) / 255);
                                    pixel[2] = (byte)((CustomColor.R * color) / 255);
                                    if (offset == 0) pixel[3] = (byte)(UseAlpha ? (CustomColor.A * color) / 255 : color);
                                    pixel+=4;
                                }
                                //canvas.SetPixel(x, y, Color.FromArgb(0, (byte)color, 0, 255));
                                }
                                catch(AccessViolationException){break;}
                                catch{}
                            }
                            pixel += offset;
                        }
                        else break;
                    }
                }
            }
            catch (Exception ex) {HS_Library.HSConsole.WriteLine(ex.ToString());}
            finally
            {
                // unlock image
                if (data != null) canvas1.UnlockBits(data);
                if (RenderSpectrogramDispose) canvas1.Dispose();
            }
            return canvas1;
            //data = null;

            // Clean up
            //pictureBox.Image = canvas;
            //offScreenDC.Dispose();
        }
        /*
        public void RenderSpectrogram(float[] FFT, Bitmap canvas, int DrawLength, float 오차 = 1)
        {
            if (_fftSpect.Count >= DrawLength) _fftSpect.RemoveAt(0);
            _fftSpect.Add(FFT);
            //Bitmap canvas = new Bitmap(pictureBox.Width, pictureBox.Height);
            Graphics offScreenDC = Graphics.FromImage(canvas);

            // Determine channnel boundries
            int width = canvas.Width;
            int height = canvas.Height;

            float min = float.MaxValue;
            float max = float.MinValue;
            float range = 0;

            //if (width > _maxHeightLeftSpect)
            //    _maxHeightLeftSpect = width;

            // get min/max
            for (int w = 0; w < _fftSpect.Count; w++)
                for (int x = 0; x < (_fftSpect[w]).Length; x++)
                {
                    float amplitude = _fftSpect[w][x] * 오차;
                    if (min > amplitude)
                    {
                        min = amplitude;
                    }
                    if (max < amplitude)
                    {
                        max = amplitude;
                    }
                }

            // get range
            if (min < 0 || max < 0)
                if (min < 0 && max < 0)
                    range = max - min;
                else
                    range = Math.Abs(min) + max;
            else
                range = max - min;

            // lock image
            PixelFormat format = canvas.PixelFormat;
            BitmapData data = canvas.LockBits(new Rectangle(0, 0, width, height), ImageLockMode.ReadOnly, format);
            int stride = data.Stride;
            int offset = stride - width * 4;

            try
            {
                unsafe
                {
                    byte* pixel = (byte*)data.Scan0.ToPointer();

                    // for each cloumn
                    for (int y = 0; y <= height; y++)
                    {
                        if (y < _fftSpect.Count)
                        {
                            // for each row
                            for (int x = 0; x < width; x++, pixel += 4)
                            {
                                float amplitude = ((float[])_fftSpect[_fftSpect.Count - y - 1])[(int)((FFT.Length / (float)(width)) * x)] * 오차;
                                float color = GetColor(min, max, range, amplitude);
                                pixel[0] = (byte)0;
                                pixel[1] = (byte)color;
                                pixel[2] = (byte)0;
                                pixel[3] = (byte)255;
                            }
                            pixel += offset;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }

            // unlock image
            canvas.UnlockBits(data);

            // Clean up
            //pictureBox.Image = canvas;
            offScreenDC.Dispose();
        }*/

        /// <summary>
        /// Get color in the range of 0-255 for amplitude sample
        /// </summary>
        /// <param name="min"></param>
        /// <param name="max"></param>
        /// <param name="range"></param>
        /// <param name="amplitude"></param>
        /// <returns></returns>
        private static float GetColor(float min, float max, float range, float amplitude)
        {
            float color;
            if (min != float.NegativeInfinity && min != float.MaxValue & max != float.PositiveInfinity && max != float.MinValue && range != 0)
            {
                if (min < 0 || max < 0)
                    if (min < 0 && max < 0)
                        color = (255 / range) * (Math.Abs(min) - Math.Abs(amplitude));
                    else
                        if (amplitude < 0)
                            color = (255 / range) * (Math.Abs(min) - Math.Abs(amplitude));
                        else
                            color = (255 / range) * (amplitude + Math.Abs(min));
                else
                    color = (255 / range) * (amplitude - min);
            }
            else
                color = 0;
            return color;
        }
        /// <summary>
        /// Get color in the range of 0-255 for amplitude sample
        /// </summary>
        /// <param name="min"></param>
        /// <param name="max"></param>
        /// <param name="range"></param>
        /// <param name="amplitude"></param>
        /// <returns></returns>
        private static float GetColorLimit(float min, float max, float range, float amplitude)
        {
            float color;
            if (min != float.NegativeInfinity && min != float.MaxValue & max != float.PositiveInfinity && max != float.MinValue && range != 0)
            {
                //if (amplitude>=max)return 255;
                if (min < 0 || max < 0)
                    if (min < 0 && max < 0)
                        color = (255 / range) * (Math.Abs(min) - Math.Abs(amplitude));
                    else
                        if (amplitude < 0)
                            color = (255 / range) * (Math.Abs(min) - Math.Abs(amplitude));
                        else
                            color = (255 / range) * (amplitude + Math.Abs(min));
                else
                    color = (255 / range) * (amplitude - min);
            }
            else
                color = 0;
            return color;
        }
        /// <summary>
        /// Get color in the range of 0-255 for amplitude sample
        /// </summary>
        /// <param name="min"></param>
        /// <param name="max"></param>
        /// <param name="range"></param>
        /// <param name="amplitude"></param>
        /// <returns></returns>
        private static double GetColor(double min, double max, double range, double amplitude)
        {
            double color;
            if (min != double.NegativeInfinity && min != double.MaxValue & max != double.PositiveInfinity && max != double.MinValue && range != 0)
            {
                if (min < 0 || max < 0)
                    if (min < 0 && max < 0)
                        color = (255 / range) * (Math.Abs(min) - Math.Abs(amplitude));
                    else
                        if (amplitude < 0)
                            color = (255 / range) * (Math.Abs(min) - Math.Abs(amplitude));
                        else
                            color = (255 / range) * (amplitude + Math.Abs(min));
                else
                    color = (255 / range) * (amplitude - min);
            }
            else
                color = 0;
            return color;
        }

        #region IDisposable 멤버

        public void Dispose(){_fftSpect.Clear();/*if(Lockbmp!=null&&data!=null)Lockbmp.UnlockBits(data);*/}

        #endregion
    }
}
