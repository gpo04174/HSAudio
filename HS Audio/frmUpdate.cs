﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Threading;
using System.Windows.Forms;
using HS_CSharpUtility.Extension;
using System.IO;
using ICSharpCode.SharpZipLib.Zip;
using System.Diagnostics;
using System.Runtime.InteropServices;
using HS_Audio.LIBRARY.Windows_API;

namespace HS_Audio
{
    public partial class frmUpdate : Form
    {
        internal frmMain frmmain;
        public frmUpdate(frmMain frmmain)
        {
            Wc_DownloadProgressChanged_ts = new ThreadStart(Wc_DownloadProgressChanged_prog);
            CheckForIllegalCrossThreadCalls = false;
            this.frmmain = frmmain;
            InitializeComponent();
            UpdaterSetting = Program.UpdaterSetting;
            frmUpdaterSetting.UptaterSettingComplete += new frmUpdaterSetting.UptaterSettingCompleteEventHandler(frmUpdaterSetting_UptaterSettingComplete);
        }

        private void frmUpdate_Load(object sender, EventArgs e)
        {
            label1.Text = "현재 버전: v" + Application.ProductVersion;//Properties.Resources.PlayerVersion;
            HS_CSharpUtility.Utility.Win32Utility.HideXButton(this.Handle);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            button1.Enabled = button2.Enabled = button4.Enabled = false;
            ThreadPool.QueueUserWorkItem(new WaitCallback(VersionCheck_Loop), new Program.UpdaterStruct(this, UpdaterSetting, true));
        }
        
        Dictionary<string, string> UpdaterSetting;
        void frmUpdaterSetting_UptaterSettingComplete(Dictionary<string, string> UpdaterSetting){this.UpdaterSetting = UpdaterSetting;}

        private void button2_Click(object sender, EventArgs e)
        {
            frmUpdaterSetting sett = new frmUpdaterSetting();
            sett.ShowDialog();
        }
         private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            try{ System.Diagnostics.Process.Start("http://cafe.naver.com/hstmplayer");}
            catch(Exception ex) {MessageBox.Show("브라우저를 열지 못했습니다.\n" + ex.Message + "\n\n직접 http://cafe.naver.com/hstmplayer 이 주소로 방문해주시기 바랍니다.");}
        }

        LIBRARY.HSOther.HSWebClient wc;
        void VersionCheck_Loop(object o)
        {
            bool IsGetVersion = false;
            try
            {
                this.InvokeIfNeeded(()=>
                {
                    if (!IsGetVersion) label3.Text = "서버 버전: (버전 가져오기 준비중...)";
                    button2.Enabled = button1.Enabled = false;
                });
                Program.UpdaterStruct Struct = (Program.UpdaterStruct)o;

                label3.InvokeIfNeeded(()=>{if (!IsGetVersion) textBox1.Text = "(업데이트 서버에 연결중...)"; });

                List<string> ServerList = new List<string>();
                ServerList.AddRange(Struct.UptaterSetting["Server"].Split(';'));
                ServerList.Add("http://112.76.42.174/home/user1634/HS%20Player/Version.ini");

                for (int i = 0; i < ServerList.Count; i++)
                {
                    wc = new LIBRARY.HSOther.HSWebClient();
                    try
                    {
                        if (i > 0) label3.InvokeIfNeeded(() => { textBox1.Text = "(업데이트 서버 " + (i + 1) + "번에 연결중...)"; });
                        string URL = ServerList[i] + (i == ServerList.Count - 1 ? null : UpdaterSetting["ServerVersionFilePath"]);
                        wc.Timeout = 3000;
                        string a = wc.DownloadString(URL);

                        if (a != null)
                        {
                            UpdateTemp ut = new UpdateTemp();

                            Dictionary<string, string> c = HS_CSharpUtility.Utility.EtcUtility.LoadSetting(HS_CSharpUtility.Utility.StringUtility.ConvertStringToArray(a, "\r\n"));
                            ut.info = c;
                            textBox1.InvokeIfNeeded(() => textBox1.Text = "v" + c["Version"]);

                            IsGetVersion = c != null;
                            this.InvokeIfNeeded(() => button1.Enabled = button2.Enabled = button4.Enabled = true);

                            if (c["Version"] != Application.ProductVersion)
                            {
                                if (MessageBox.Show(string.Format("프로그램의 새 버전이 나왔습니다. ({0} v{1})\n\n새 버전을 업데이트 하시겠습니까?", c["ProgramName"], c["Version"]), "새 버전 출시 업데이트 알림", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.Yes)
                                {
                                    ut.Name = c["ProgramName"] + ".tmp";
                                    ut.Path = System.Diagnostics.Process.GetCurrentProcess().MainModule.FileName;
                                    ut.Dir = ut.Path.Substring(0, ut.Path.LastIndexOf("\\") + 1);

                                    ut.Type = c["DownloadURL"].Substring(c["DownloadURL"].LastIndexOf("."));
                                    Thread.Sleep(500);

                                    this.InvokeIfNeeded(() => UpdateProg(ut));
                                }
                                return;
                            }
                            else { MessageBox.Show("최신 버전을 사용하고 계십니다.\n\n업데이트 해주셔서 감사합니다!!", "HS 플레이어 업데이터", MessageBoxButtons.OK, MessageBoxIcon.Information); return; }
                        }
                    }
                    catch (System.Net.WebException ex) { Thread.Sleep(500); }
                    catch (ThreadAbortException) { }
                    catch { label3.InvokeIfNeeded(() => { if (!IsGetVersion) textBox1.Text = "(버전을 가져올 수 없습니다.)"; }); }
                    finally { wc.Dispose(); }
                }
            }
            catch{}
            finally{this.InvokeIfNeeded(()=>
            {
                Thread.Sleep(500);
                if (!IsGetVersion) { textBox1.Text = "(버전을 가져오지 못했습니다.)"; button2.Enabled = button1.Enabled = button4.Enabled = true; GoToCafe(); }
            });}
        }
        public void GoToCafe()
        {
            if (MessageBox.Show("업데이트에 실패하였습니다.\n\n네이버 카페에서 다운로드 받으시겠습니까?", "HS 플레이어 업데이터",
                MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes) Process.Start("http://cafe.naver.com/hstmplayer");
        }

        internal void UpdateProg(UpdateTemp ut)
        {
            button1.Enabled = button2.Enabled = button4.Enabled = false;
            this.Height += (label2.Height + 2 + progressBar1.Height + 10);

            try {Directory.SetCurrentDirectory(ut.Dir);}
            catch (Exception ex) {MessageBox.Show("업데이트 중 오류가 발생하였습니다.\n업데이트가 불안정할 수 있습니다.\n\n"+ex.Message, "HS 플레이어 업데이터", MessageBoxButtons.OK, MessageBoxIcon.Error);}

            if (wc!=null)wc.Dispose(); wc = new LIBRARY.HSOther.HSWebClient();
            wc.Timeout = 3000;
            wc.Tag = ut;
            wc.DownloadProgressChanged += Wc_DownloadProgressChanged;
            wc.DownloadFileCompleted += DownloadFileCompleted;//(object sender, AsyncCompletedEventArgs e) => { frmmain.프로그램다시시작ToolStripMenuItem_Click(Dir+Name, null); };
            
            string URL = ut.info["DownloadURL"];
#if DEBUG
            //URL = URL.Replace("parkhongsic.iptime.org", "192.168.2.10");
#endif
            //try { File.Delete(ut.Name + ut.Type);  File.Create(ut.Name + ut.Type).Dispose(); File.SetAttributes(ut.Name + ut.Type, FileAttributes.Hidden); } catch { }
            wc.DownloadFileAsync(new Uri(URL), ut.Dir + ut.Name + ut.Type);
        }

        ThreadStart Wc_DownloadProgressChanged_ts;
        void Wc_DownloadProgressChanged_prog()
        {
            while (true)
            {
                float Percent = (BytesReceived / (float)TotalBytesToReceive) * 100;
                this.InvokeIfNeeded(() =>
                {
                    label2.Text = string.Format("다운로드 진행률: {0}% ({1} KB / {2} KB)", Percent.ToString("0.00"), (BytesReceived / 1024f).ToString("0.00"), (TotalBytesToReceive / 1024f).ToString("0.00"));
                    if (Percent <= 100) progressBar1.Value = (int)(Percent * 100);
                });
                Thread.Sleep(10);
            }
        }
        Thread Wc_DownloadProgressChanged_th;
        long BytesReceived, TotalBytesToReceive;

        private void Wc_DownloadProgressChanged(object sender, System.Net.DownloadProgressChangedEventArgs e)
        {
            if(Wc_DownloadProgressChanged_th == null)
            {
                Wc_DownloadProgressChanged_th = new Thread(Wc_DownloadProgressChanged_ts);
                Wc_DownloadProgressChanged_th.Start();
            }
            BytesReceived = e.BytesReceived;
            TotalBytesToReceive = e.TotalBytesToReceive;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            wc.Dispose();

            label2.Text = "준비중...";
            textBox1.Text = "(업데이트 확인버튼을 눌러주세요)";

            this.Height -= (label2.Height + 2 + progressBar1.Height + 10);
        }

        void DownloadFileCompleted(object sender, AsyncCompletedEventArgs e)
        {
            if (Wc_DownloadProgressChanged_th != null) Wc_DownloadProgressChanged_th.Abort();
            Wc_DownloadProgressChanged_th = null;

            if (e.Error == null)
            {
                UpdateTemp ut = (UpdateTemp)wc.Tag;
                label2.Text = "다운로드 진행률: " + (ut.Type == ".zip" ? "압축 해제 준비중..." : "복사 준비중...");
                progressBar1.Value = 1;
                progressBar1.Style = ProgressBarStyle.Marquee;
                ZipConstants.DefaultCodePage = 949;

                new Thread(new ThreadStart(() =>
                {
                    try
                    {
                        Directory.SetCurrentDirectory(ut.Dir);
                        if (ut.Type == ".zip")
                        {
                            ZipFile zip;
                            label2.InvokeIfNeeded(() => label2.Text = "다운로드 진행률: 업데이트 중...");
                            string zippath = ut.Name + ut.Type;
                            //FileStream fs = new FileStream(zippath, FileMode.Open);
                            zip = new ZipFile(zippath);
                            for (int i = 0; i < zip.Count; i++)
                            {
                                if (zip[i].IsFile)
                                {
                                    if (zip[i].Name.Substring(zip[i].Name.LastIndexOf(".")) == ".exe")
                                    {
                                        string DescPath = Process.GetCurrentProcess().MainModule.FileName;
                                        string FileName = DescPath.Substring(DescPath.LastIndexOf("\\") + 1);
                                        if (FileName == zip[i].Name)
                                        {
                                            string version = null; try {version = FileVersionInfo.GetVersionInfo(DescPath).ProductVersion; } catch { }

                                            int index = FileName.LastIndexOf(".");
                                            FileName = FileName.Remove(index);

                                            int Succ = HSMoveFileEx.MoveFileEx(FileName + ".exe", FileName + (version == null ? null : " v" + version) + ".exe",
                                                HSMoveFileEx.MoveFileFlags.MOVEFILE_REPLACE_EXISTING
                                                //|HSMoveFileEx.MoveFileFlags.MOVEFILE_WRITE_THROUGH
                                                );
                                            if (Succ != 0 && File.Exists(FileName))
                                            {
                                                this.InvokeIfNeeded(() => this.Dispose());
                                                MessageBox.Show("프로그램 업데이트에 실패하였습니다.\n\n카페에서 수동으로 업데이트 해주시기 바랍니다.", "HS플레이어 업데이터", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                                //button1.InvokeIfNeeded(() => button1.Enabled = button2.Enabled = button4.Enabled = true);

                                                try{zip.Close();} catch { } try {File.Delete(zippath); } catch { }
                                            }
                                        }
                                    }
                                    Stream ZIP = zip.GetInputStream(zip[i]);
                                    FileStream fs1 = new FileStream(ut.Dir + zip[i].Name, FileMode.Create);
                                    HS_CSharpUtility.Utility.IOUtility.CopyStream(ZIP, fs1, 4096).Close();
                                    fs1.Close(); ZIP.Close();
                                }
                                else if (zip[i].IsDirectory)
                                {

                                }
                            }

                            try{zip.Close();} catch { } try {File.Delete(zippath); } catch { }
                            label2.InvokeIfNeeded(() => label2.Text = "다운로드 진행률: 업데이트 완료!!");
                            DialogResult dr = MessageBox.Show("업데이트가 완료되었습니다.\n\n프로그램을 이어서 다시 시작합니다.", "HS 플레이어 업데이터", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            frmmain.InvokeIfNeeded(() => frmmain.프로그램다시시작ToolStripMenuItem_Click(true, null));
                            this.InvokeIfNeeded(() => this.Close());
                            //if (dr == DialogResult.Yes) frmmain.InvokeIfNeeded(()=>frmmain.프로그램다시시작ToolStripMenuItem_Click(true, null));
                            //else MessageBox.Show("프로그램을 수동으로 다시 시작해주시기 바랍니다.", "HS 플레이어 업데이터", MessageBoxButtons.YesNo, MessageBoxIcon.Information);

                            this.InvokeIfNeeded(() => this.Close());
                        }
                        else if (ut.Type == ".exe")
                        {

                        }
                    }
                    catch (Exception ex)
                    {
                        HS_Library.HSConsole.WriteLine("업데이트에 실패하였습니다.\n\n" + ex.ToString());
                        MessageBox.Show("프로그램 업데이트에 실패하였습니다.\n\n카페에서 수동으로 업데이트 해주시기 바랍니다.", "HS플레이어 업데이터", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        this.InvokeIfNeeded(() =>
                        {
                            textBox1.Text = "업데이트에 실패하였습니다. (" + ex.Message + ")";
                            this.Height -= (label2.Height + 2 + progressBar1.Height + 10);
                            button1.Enabled = button2.Enabled = button4.Enabled = true;
                        });
                    }
                    
                })).Start();
            }
            else
            {
                textBox1.Text = "업데이트에 실패하였습니다. (" + e.Error.Message + ")";
                this.Height -= (label2.Height + 2 + progressBar1.Height + 10);
                button1.Enabled = button2.Enabled = button4.Enabled = true;
                MessageBox.Show("프로그램 업데이트에 실패하였습니다.\n\n카페에서 수동으로 업데이트 해주시기 바랍니다.", "HS플레이어 업데이터", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        public class UpdateTemp
        {
            public Dictionary<string, string> info;
            public string Name;
            public string Path;
            public string Dir;
            public string Type;
        }
    }
}
