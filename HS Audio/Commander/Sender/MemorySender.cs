﻿using System;
using System.Collections.Generic;
using System.Text;
using SharedMemoryLib;
using System.Drawing;
using System.IO;
using System.Drawing.Imaging;

namespace HS_Audio.Commander.Sender
{
    //1 계층 (공유메모리 사용)
    internal class MemorySender
    {
        //SharedMemory sm = new SharedMemory();

        SharedMemory sm_MusicInfo; //음악정보
        SharedMemory sm_Commander; //명령
        SharedMemory sm_AlbumArt; //앨범아트
        SharedMemory sm_Event; //이벤트
        internal MemorySender()
        {
            /*
            //명령 1MB
            sm.Create(1048576, "HSPlayerCommander");
            //음악정보 1MB
            sm.Create(1048576, "HSPlayerMusicInfo");
            //앨범아트 10MB
            sm.Create(10485760, "HSPlayerAlbumArt");
            //이벤트 0.5MB
            sm.Create(524288, "HSPlayerEvent");
            */

            
            //10KB
            sm_MusicInfo = new SharedMemory(10240, "HSPlayerMusicInfo", 0, Win32_Kernel.SectionTypes.SecNone, Win32_Kernel.AccessTypes.Full);
            //10KB
            sm_Commander = new SharedMemory(10240, "HSPlayerCommander", 0, Win32_Kernel.SectionTypes.SecNone, Win32_Kernel.AccessTypes.Full);
            //10MB
            sm_AlbumArt = new SharedMemory(10485760, "HSPlayerAlbumArt", 0, Win32_Kernel.SectionTypes.SecNone, Win32_Kernel.AccessTypes.Full);
            //100KB
            sm_Event = new SharedMemory(102400, "HSPlayerEvent", 0, Win32_Kernel.SectionTypes.SecNone, Win32_Kernel.AccessTypes.Full); 
        }

        public void SetMusicInfo(string InfoString)
        {
            sm_MusicInfo.Clear();
            sm_MusicInfo.WriteString(InfoString);
        }
        public string GetMusicInfo()
        {
            string read = null;
            sm_MusicInfo.ReadString(ref read);
            return read;
        }

        public void SetAlbumArt(byte[] AlbumArt)
        {
            sm_AlbumArt.Clear();
            if(AlbumArt != null) sm_AlbumArt.WriteBytes(AlbumArt, 0);
        }
        public void SetAlbumArt(Bitmap AlbumArt)
        {
            sm_AlbumArt.Clear();
            MemoryStream ms = new MemoryStream();
            AlbumArt.Save(ms, ImageFormat.Png);
            sm_AlbumArt.WriteInt32((int)ms.Length, 0);
            sm_AlbumArt.WriteBytes(ms.GetBuffer(), 3, (int)ms.Length);
        }
        public byte[] GetAlbumArt()
        {
            int length = sm_AlbumArt.ReadInt32(0);
            byte[] read = new byte[length];
            sm_AlbumArt.ReadBytes(read, 3);
            return read;
        }

        public void Dispose()
        {
            sm_MusicInfo.Dispose();
            sm_Commander.Dispose();
            sm_AlbumArt.Dispose();
            sm_Event.Dispose();
        }
    }
}
