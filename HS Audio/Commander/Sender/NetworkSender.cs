﻿using System;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.ComponentModel;
using System.Collections.Generic;

namespace HS_Audio.Commander.Sender
{
    internal class NetworkSender : Sender, IDisposable
    {
        IPEndPoint server_ip, server_ip_byte, server_ip_web;
        EndPoint client_ip, client_ip_byte;
        
        Socket server_soc, server_soc_byte;
        TcpListener listner;
        SocketAsyncEventArgs rec = new SocketAsyncEventArgs();
        SocketAsyncEventArgs rec1 = new SocketAsyncEventArgs();

        Thread th, th1;
        ThreadStart ts, ts1;

        int Port_Commander, Port_Commander_Byte;

        byte[] buffer;
        internal NetworkSender(int Port_Commander = 10702, int Port_Commander_Byte = 10703)
        {
            this.Port_Commander = Port_Commander;
            this.Port_Commander_Byte = Port_Commander_Byte;

            //Get local IP       
            server_ip = new IPEndPoint(IPAddress.Any, Port_Commander);
            server_ip_byte = new IPEndPoint(IPAddress.Any, Port_Commander_Byte);

            ts = new ThreadStart(th_Loop);
            ts1 = new ThreadStart(th1_Loop);

            buffer = new byte[255];
            rec.SetBuffer(buffer, 0, buffer.Length);
            rec.RemoteEndPoint = server_ip;
        }

        public void Init_soc()
        {
            //UDP
            server_soc = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
            //Bind ip
            server_soc.Bind(server_ip);

            //get the client IP
            //client_ip = new IPEndPoint(IPAddress.Any, 0);
            //client = (EndPoint)(client_ip);

            if (th == null ? true : th.ThreadState != ThreadState.Running)
            {
                th = new Thread(ts);
                th.Start();
            }
            //server_soc.ReceiveFromAsync(rec);
        }
        public void Init_soc_byte()
        {
            //TCP
            //if (server_soc_byte != null) server_soc_byte.Close();
            //listner = new TcpListener(IPAddress.Any, Port_AlbumArt);
            
            //UDP
            server_soc_byte = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
            server_soc_byte.Bind(server_ip_byte);
            

            if (th1 == null ? true : th1.ThreadState != ThreadState.Running)
            {
                th1 = new Thread(ts1);
                th1.Start();
            }
        }

        public new void setEvent(string Event, params object[] Data)
        {
            if (client_ip != null)
            {
                try
                {
                    byte[] data = base.setEvent(Event, Data);
                    #if DEBUG
                    Console.WriteLine(Encoding.ASCII.GetString(data));
                    System.Diagnostics.Debug.WriteLine(Encoding.ASCII.GetString(data));
                    #endif
                    server_soc.SendTo(data, client_ip);
                }
                catch (Exception ex) { System.Diagnostics.Debug.WriteLine(ex.ToString()); }
            }
        }

        public void Dispose()
        {
            Dispose_Message();
            Dispose_Byte();
        }
        public void Dispose_All()
        {
            Dispose_Message();
            Dispose_Byte();
        }
        public void Dispose_Message()
        {
            try { server_soc.Close(); } catch { }
            try { rec.Dispose();} catch { }
            try { if (th != null) th.Abort(); } catch { }
            th = null;
        }
        public void Dispose_Byte()
        {
            try { server_soc_byte.Close(); } catch { }
            try { rec1.Dispose(); } catch { }
            try { if (th != null) th1.Abort(); } catch { }
            th1 = null;
        }

        void th_Loop()
        {
            IPEndPoint sender = new IPEndPoint(IPAddress.Any, 0);
            EndPoint Remote = (EndPoint)(sender);
            byte[] data = new byte[BufferSizeRecv];
            while (true)
            {
                int recv = 0;
                try
                {
                    recv = server_soc.ReceiveFrom(data, ref Remote);

                    client_ip = Remote;
                    rec.RemoteEndPoint = client_ip;

                    string[] cmd_arr = Encoding.UTF8.GetString(data, 0, recv).Split('\n');
                    CommandData cd = CommandParse(true, cmd_arr);
                    if(cd != null)server_soc.SendTo(cd.Data, Remote);
                    //rec.SetBuffer(cmd_data, 0, cmd_data.Length);
                    //server_soc.SendToAsync(rec);
                }
                catch (Exception ex)
                {
                    System.Diagnostics.Debug.WriteLine(ex.Message);
                    Thread.Sleep(30);
                }
                Array.Clear(data, 0, recv);
            }
        }
        void th1_Loop()
        {
            IPEndPoint sender = new IPEndPoint(IPAddress.Any, 0);
            EndPoint Remote = (EndPoint)(sender);
            byte[] data = new byte[BufferSizeRecv];
            byte[] msg = new byte[20];
            //TCP
            //listner.Start();
            //server_soc_byte = listner.AcceptSocket();
            while (true)
            {
                int recv = 0, recv_msg = 0;
                try
                {
                    recv = server_soc_byte.ReceiveFrom(data, ref Remote);
                    //Console.WriteLine(Encoding.ASCII.GetString(data, 0, recv));
                    client_ip_byte = Remote;
                    rec1.RemoteEndPoint = client_ip_byte;
                    string[] cmd_arr = Encoding.UTF8.GetString(data, 0, recv).Split('\n');
                    for (int i = 0; i < cmd_arr.Length; i++)
                    {
                        string command = cmd_arr[i].Replace("\r", "");
                        CommandData cmd_data = CommandParse(false, command);
                        byte[] albumart = cmd_data.Data;
                        if (albumart != null && albumart.Length > 0)
                        {
                            server_soc_byte.SendBufferSize = BufferSizeSend;
                            /*
                            server_soc_byte.SendBufferSize = albumart.Length;
                            rec1.RemoteEndPoint = Remote;
                            rec1.SetBuffer(albumart, 0, albumart.Length);
                            server_soc_byte.SendAsync(rec1);
                            */

                            int len = albumart.Length;
                            string info = "[Result]\nResult=Success\nMessage=No AlbumArt\nLength=" + len;
                            byte[] len_data = Encoding.ASCII.GetBytes(info);
                            //rec1.SetBuffer(data, 0, data.Length);
                            //rec.SetBuffer(albumart, 0, albumart.Length);
                            //server_soc_byte.SendAsync(rec);
                            server_soc_byte.SendTo(len_data, Remote);
                            
                            recv_msg = server_soc_byte.ReceiveFrom(msg, ref Remote);
                            Array.Clear(msg, 0, msg.Length);
                            if (true) //Encoding.UTF8.GetString(data, 0, recv).ToUpper() == "OK"
                            {
                                //server_soc_byte.SendFile(path);

                                byte[] buf = new byte[BufferSizeSend];
                                for (int j = 0; j < len / buf.Length; j++)
                                {
                                    for (int k = 0; k < buf.Length; k++)
                                        buf[k] = albumart[(j * buf.Length) + k];

                                    server_soc_byte.SendTo(buf, Remote);
                                    Array.Clear(msg, 0, msg.Length);
                                    recv_msg = server_soc_byte.ReceiveFrom(msg, ref Remote);
                                }

                                if (len % buf.Length < buf.Length)
                                {
                                    //Array.Clear(buf, (len % buf.Length) - 1, buf.Length);
                                    int le = (len % buf.Length) - 1;
                                    for (int j = le; j >= 0; j--)
                                        buf[le - j] = albumart[albumart.Length - j - 1];
                                    server_soc_byte.SendTo(buf, le + 1, SocketFlags.None, Remote);
                                }
                                //server_soc_byte.SendToAsync(rec1);
                            }

#if DEBUG
                            string MD5Hash = GetMd5Hash(albumart);
                            System.Diagnostics.Debug.WriteLine(MD5Hash);
                            Console.WriteLine(MD5Hash);
#endif
                        }
                        else
                        {
                            string info = "Length=0";
                            byte[] buf = Encoding.ASCII.GetBytes(info);
                            server_soc_byte.SendTo(buf, Remote);
                        }
                    }
                }
                catch (Win32Exception ex)
                {
                    System.Diagnostics.Debug.WriteLine(ex.ToString());
                    //server_soc_byte = listner.AcceptSocket();
                }
                catch (Exception ex)
                {
                    System.Diagnostics.Debug.WriteLine(ex.ToString());
                    Thread.Sleep(30);
                }
                finally { Array.Clear(data, 0, data.Length <= recv ? recv : data.Length); recv = 0; }
            }
        }

        public byte[] setMessage()
        {
            throw new NotImplementedException();
        }

        public void setEvent()
        {
            throw new NotImplementedException();
        }
    }
}
