﻿using System;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.ComponentModel;
using System.Collections.Generic;
using System.IO;
using System.Security.Cryptography;
using WebSockets.Common;
using WebSockets.Server.WebSocket;
using WebSockets.Events;
using System.Diagnostics;

namespace HS_Audio.Commander.Sender
{
    internal delegate void SenderNeedsEventHandler(object instance, ref Sender sender);
    internal class WebSocketSender : Sender, ISender, IDisposable
    {
        public event SenderNeedsEventHandler SenderNeeds;
        IPEndPoint server_ip, server_ip_byte, server_ip_web;
        EndPoint client_ip, client_ip_byte;
        
        TcpListener listener, listener_byte;
        SocketAsyncEventArgs rec = new SocketAsyncEventArgs();
        SocketAsyncEventArgs rec1 = new SocketAsyncEventArgs();

        Thread th, th1;
        ThreadStart ts, ts1;

        int Port_Commander, Port_Commander_Byte;

        //private List<SocketPack> clients = new List<SocketPack>();
        //private List<SocketPack> clients_byte = new List<SocketPack>();

        List<WebSocketServiceMT> clients = new List<WebSocketServiceMT>();
        List<WebSocketServiceMT> clients_byte = new List<WebSocketServiceMT>();
        byte[] buffer;
        internal WebSocketSender(int Port_Commander = 10702, int Port_Commander_Byte = 10703)
        {
            this.Port_Commander = Port_Commander;
            this.Port_Commander_Byte = Port_Commander_Byte;

            //Get local IP       
            server_ip = new IPEndPoint(IPAddress.Any, Port_Commander);
            server_ip_byte = new IPEndPoint(IPAddress.Any, Port_Commander_Byte);

            ts = new ThreadStart(Listen);
            ts1 = new ThreadStart(Listen_Byte);

            buffer = new byte[255];
            rec.SetBuffer(buffer, 0, buffer.Length);
            rec.RemoteEndPoint = server_ip;
        }

        public void Init_soc()
        {
            //TCP
            listener = new TcpListener(server_ip);
            listener.Start();

            //get the client IP
            //client_ip = new IPEndPoint(IPAddress.Any, 0);
            //client = (EndPoint)(client_ip);

            if (th == null ? true : th.ThreadState != System.Threading.ThreadState.Running)
            {
                th = new Thread(ts);
                th.Start();
            }
            //server_soc.ReceiveFromAsync(rec);
        }
        public void Init_soc_byte()
        {
            //TCP
            listener_byte = new TcpListener(server_ip_byte);
            listener_byte.Start();

            if (th1 == null ? true : th1.ThreadState != System.Threading.ThreadState.Running)
            {
                th1 = new Thread(ts1);
                th1.Start();
            }
        }


        public new void setEvent(string Event, params object[] Data)
        {
            Thread thread = new Thread(new ThreadStart(() =>
            {
                if (clients.Count > 0)
                {
                    try
                    {
                        byte[] data = base.setEvent(Event, Data);
#if DEBUG
                        Console.WriteLine(Encoding.UTF8.GetString(data));
                        System.Diagnostics.Debug.WriteLine(Encoding.UTF8.GetString(data));
#endif
                        for (int i = 0; i < clients.Count; i++) clients[i].Write(WebSocketOpCode.TextFrame, data);
                    }
                    catch (Exception ex) { System.Diagnostics.Debug.WriteLine(ex.ToString()); }
                }
            } 
            ));
            thread.Start();
        }
        public new void setMessage(GetKind kind, string Message)
        {
            Thread thread = new Thread(new ThreadStart(() =>
            {
                if (clients.Count > 0)
                {
                    try
                    {
                        byte[] data = base.setMessage(kind, Message);
#if DEBUG
                        Console.WriteLine(Encoding.UTF8.GetString(data));
                        System.Diagnostics.Debug.WriteLine(Encoding.UTF8.GetString(data));
#endif
                        for (int i = 0; i < clients.Count; i++) clients[i].Write(WebSocketOpCode.TextFrame, data);
                    }
                    catch (Exception ex) { System.Diagnostics.Debug.WriteLine(ex.ToString()); }
                }
            }
            ));
            thread.Start();
        }

        public void Dispose()
        {
            Dispose_Message();
            Dispose_Byte();
            Remove(null, true);
        }
        public void Dispose_Message()
        {
            try { if (th != null) th.Abort(); } catch { }
            try { listener.Stop(); } catch { }
            try { rec.Dispose();} catch { }
            th = null;
        }
        public void Dispose_Byte()
        {
            try { if (th1 != null) th1.Abort(); } catch { }
            try { listener_byte.Stop(); } catch { }
            try { rec1.Dispose(); } catch { }
            th1 = null;
        }

        private void Listen()
        {
            // 클라이언트로부터 accept를 받는 부분
            // block이 걸리므로 별도의 스레드 처리를 하는게 낫다
            // ...그 block이 걸려야 하는데
            while (true)
            {
#if DEBUG
                // 먼저 들어오는 연결을 받는다
                Console.WriteLine("[TcpSock] Accept Waiting");
#endif
                // 연결된 후 서버와 통신하는 동작을 별도의 스레드로 처리한다

                //SocketPack pack = new SocketPack(listener.AcceptTcpClient(), new Thread(new ParameterizedThreadStart(th_Loop)));
                //pack.Client.SendBufferSize = BufferSizeSend;
                //clients.Add(pack);
                //pack.Thread.Start(pack);

                TcpClient tc = listener.AcceptTcpClient();
                new Thread(new ParameterizedThreadStart(th_Loop)).Start(tc);
            }
        }
        private void Listen_Byte()
        {
            // 클라이언트로부터 accept를 받는 부분
            // block이 걸리므로 별도의 스레드 처리를 하는게 낫다
            // ...그 block이 걸려야 하는데
            while (true)
            {
#if DEBUG
                // 먼저 들어오는 연결을 받는다
                Console.WriteLine("[TcpSock] Accept Waiting");
#endif

                TcpClient tc = listener_byte.AcceptTcpClient();
                new Thread(new ParameterizedThreadStart(th1_Loop)).Start(tc);
            }
        }

        
        void th_Loop(object pack)
        {
            WebSocketFrameReader wf = new WebSocketFrameReader();
            byte[] buf = new byte[BufferSizeRecv];
            MemoryStream ms = new MemoryStream();
            //SocketPack sp = (SocketPack)pack;
            TcpClient client = (TcpClient)pack;//sp.Client;
            int recv = 0;
            try
            {
                while (client.Connected)
                {
                    try
                    {
                        NetworkStream stream = client.GetStream();
                        //if (recv < buf.Length) 는 데이터가 버퍼크기만큼 들어오지않았을때 다음데이터를 받으려하면 대기가 걸리므로 break로 탈출
                        while ((recv = stream.Read(buf, 0, buf.Length)) > 0) { ms.Write(buf, 0, recv); if (recv < buf.Length) break; }
                        int read = ms.GetBuffer().Length > 0 ? ms.GetBuffer()[0] : 0;
                        if (read > 0)
                        {
                            string response = Encoding.UTF8.GetString(ms.GetBuffer(), 0, (int)ms.Length);

                            Sender s = null;
                            SenderNeeds(this, ref s);
                            WebSocketServiceMT ws = new WebSocketServiceMT(new WebSocketService(stream, client, response, true, null), s);
                            ws.ConnectionClose += (object sender, ConnectionCloseEventArgs e) => { Remove(sender as WebSocketService); };
                            ws.Tag = DateTime.Now.Ticks;
                            Add(ws);
                            ws.Respond();
                            break;
                        }
                        else Thread.Sleep(1);// 스트림이 브라우저로 부터 읽지 못한 경우이므로 패스

                        /*
                        string msg;
                        //byte[] header = Encoding.UTF8.GetBytes("HTTP: 1.1 200 OK");
                        Header header = HttpRecvHeader(response, out msg);

                        client_ip = client.Client.RemoteEndPoint;
                        rec.RemoteEndPoint = client_ip;

                        if (header.reqMethod.ToUpper() == "GET" || header.reqMethod == "POST".ToUpper())
                        {
                            if (msg.Trim() == "")
                            {
                                StringBuilder sb = new StringBuilder("HTTP/1.1 101 Switching Protocols\r\n");
                                sb.AppendLine("Upgrade: websocket");
                                sb.AppendLine("Connection: Upgrade");
                                sb.AppendLine("Sec-WebSocket-Protocol: chat");
                                //웹소켓 Handshake
                                string key = string.Concat(header.headers["Sec-WebSocket-Key"], "258EAFA5-E914-47DA-95CA-C5AB0DC85B11").Trim();
                                using (SHA1 sha1 = SHA1.Create())
                                {
                                    byte[] hash = sha1.ComputeHash(Encoding.UTF8.GetBytes(key));
                                    sb.Append("Sec-WebSocket-Accept: ").AppendLine(Convert.ToBase64String(hash));
                                }
                                sb.AppendLine();

                                byte[] MSG = Encoding.UTF8.GetBytes(sb.ToString());
                                sp.stream.Write(MSG, 0, MSG.Length);
                                sp.stream.Flush();
                                return;
                            }
                            else
                            {
                                ms.Position = 0;
                                WebSocketFrame sf = wf.Read(ms, client.Client);

                                string[] cmd_arr = Encoding.UTF8.GetString(sf.DecodedPayload).Split('\n');
                                CommandData cd = CommandParse(cmd_arr);
                                if (cd != null)
                                {
                                    sp.stream.Write(cd.Data, 0, cd.Data.Length);
                                    sp.stream.Flush();
                                }
                            }
                            */
                        }

                        //rec.SetBuffer(cmd_data, 0, cmd_data.Length);
                        //server_soc.SendToAsync(rec);
                        catch (Exception ex)
                        {
                            //HResult == 0x80131620 : Connection force closed
                            System.Diagnostics.Debug.WriteLine(ex.Message);
                            Thread.Sleep(30);
                        }
                        finally { ms.SetLength(0); }
                        //Array.Clear(data, 0, recv);
                }
            }
            catch { }
            //finally { if (sp.stream != null) sp.stream.Close(); Remove(sp); }
        }


        void th1_Loop(object pack)
        {
            WebSocketFrameReader wf = new WebSocketFrameReader();
            byte[] buf = new byte[BufferSizeRecv];
            MemoryStream ms = new MemoryStream();
            //SocketPack sp = (SocketPack)pack;
            TcpClient client = (TcpClient)pack;//sp.Client;
            int recv = 0;
            try
            {
                while (client.Connected)
                {
                    try
                    {
                        //recv = client.Client.Receive(data);
                        //sp.stream = client.GetStream();
                        //Array.Clear(buf, 0, recv);

                        NetworkStream stream = client.GetStream();
                        while ((recv = stream.Read(buf, 0, buf.Length)) > 0) { ms.Write(buf, 0, recv); if (recv < buf.Length) break; }
                        int read = ms.GetBuffer()[0];
                        if (read == -1) { Thread.Sleep(1); continue; } // 스트림이 브라우저로 부터 읽지 못한 경우이므로 패스
                        string response = Encoding.UTF8.GetString(ms.GetBuffer(), 0, (int)ms.Length);

                        Sender s = null;
                        SenderNeeds(this, ref s);
                        WebSocketServiceMT ws = new WebSocketServiceMT(new WebSocketService(stream, client, response, true, null), s);
                        ws.ConnectionClose += (object sender, ConnectionCloseEventArgs e) => { Remove(sender as WebSocketService); };
                        ws.Tag = DateTime.Now.Ticks;
                        Add(ws);
                        ws.Respond();
                        break;
                    }

                    //rec.SetBuffer(cmd_data, 0, cmd_data.Length);
                    //server_soc.SendToAsync(rec);
                    catch (Exception ex)
                    {
                        //HResult == 0x80131620 : Connection force closed
                        System.Diagnostics.Debug.WriteLine(ex.Message);
                        Thread.Sleep(30);
                    }
                    finally { ms.Dispose(); }
                    //Array.Clear(data, 0, recv);
                }
            }
            catch { }
            //finally { if (sp.stream != null) sp.stream.Close(); Remove(sp); }
        }

        /*
void th1_Loop(object pack)
{
   byte[] data = new byte[BufferSizeRecv];
   SocketPack sp = (SocketPack)pack;
   TcpClient client = sp.Client;
   try
   {
       while (client.Connected)
       {
           int recv = 0;
           try
           {
               sp.stream = client.GetStream();
               StreamReader sr = new StreamReader(sp.stream);

               client_ip = client.Client.RemoteEndPoint;
               rec.RemoteEndPoint = client_ip;

               string msg = sr.ReadToEnd();
               string[] cmd_arr = msg.Split('\n');
               CommandData cd = CommandParse(cmd_arr);
               if (cd != null)
               {
                   sp.stream.Write(cd.Data, 0, cd.Data.Length);
                   sp.stream.Flush();
               }
               //rec.SetBuffer(cmd_data, 0, cmd_data.Length);
               //server_soc.SendToAsync(rec);
           }
           catch (Exception ex)
           {
               System.Diagnostics.Debug.WriteLine(ex.Message);
               Thread.Sleep(30);
           }
           //Array.Clear(data, 0, recv);
       }
   }
   catch { }
   finally { if (sp.stream != null) sp.stream.Close(); Remove_Byte(sp); }
}
*/

        string LOCK_Remove = "LOCK_Remove";
        private void Remove(WebSocketService pack, bool All = false)
        {
            lock (LOCK_Remove)
            {
                for (int i = clients.Count - 1; i >= 0; i--)
                    if (All || clients[i].Tag == pack.Tag)
                    {
                        try
                        {
                            clients[i].Dispose();
                            //try { clients[i].Thread.Abort(); } catch { }
                            clients.RemoveAt(i);
                            if(!All)break;
                        }
                        catch(Exception ex) { Debug.WriteLine(ex.Message); }
                    }
            }
        }
        string LOCK_Add = "LOCK_Add";
        private void Add(WebSocketServiceMT soc)
        {
            lock (LOCK_Add) { clients.Add(soc); }
        }

        /*
string LOCK_Remove_Byte = "LOCK_Remove_Byte";
private void Remove_Byte(SocketPack pack, bool All = false)
{
   lock (LOCK_Remove_Byte)
   {
       for (int i = 0; i < clients_byte.Count; i++)
           if (All || clients_byte[i].ID == pack.ID)
           {
               clients_byte[i].Client.Close();
               try { clients_byte[i].Thread.Abort(); } catch { }
               clients_byte.RemoveAt(i);
           }
   }
}*/

        /*
    private class SocketPack
    {
        public SocketPack(TcpClient Client, Thread Thread) { this.Client = Client; this.Thread = Thread; }
        public SocketPack(TcpClient Client) { this.Client = Client; this.stream = Client.; }
        public int ID { get { return this.Thread.ManagedThreadId; } }
        public Thread Thread;
        public TcpClient Client;
        public NetworkStream stream;
    }
    */

        private class WebSocketServiceMT : IDisposable
        {
            public event EventHandler<ConnectionCloseEventArgs> ConnectionClose;
            public WebSocketService socket;
            public WebSocketServiceMT(WebSocketService socket, Sender sender)
            {
                socket.ConnectionOpened += (object s, EventArgs e) => { writer = new WebSocketFrameWriter(socket.Stream, false); };
                socket.ConnectionClose += (object s, ConnectionCloseEventArgs e) => { ConnectionClose?.Invoke(this, e); };
                socket.TextFrame += (object s, TextFrameEventArgs e) =>
                {
                    WebSocketService serv = s as WebSocketService;
                    string[] cmd_arr = e.Text.Split('\n');
                    //if (cmd_arr[0].ToLower() == "echo")
                    if (e.Text.Length > 4 && e.Text.Substring(0, 5).ToLower() == "echo ")
                        Write(WebSocketOpCode.TextFrame, Encoding.UTF8.GetBytes(e.Text.Substring(5)));//cmd_arr.Length > 1 ? cmd_arr[1] : cmd_arr[0].Substring(4)));
                    else
                    {
                        CommandData cd = sender.CommandParse(true, cmd_arr);
                        if (cd != null)
                        {
                            if (e.Text.IndexOf("AlbumArt") > 0) { }
                            Write(cd.Kind == SendKind.Text ? WebSocketOpCode.TextFrame : WebSocketOpCode.BinaryFrame, cd.Data);
                        }
                    }
                };
                socket.BinaryFrame += (object s, BinaryFrameEventArgs e) => { };
                this.socket = socket;
            }

            public void Respond() { socket.Respond(); }

            string LOCK_Write = "LOCK_Write";
            public WebSocketFrameWriter writer;
            public void Write(WebSocketOpCode code,  byte[] data)
            {
                lock (LOCK_Write)
                {
                    if (writer != null)
                    {
                        try
                        {
                            writer.Write(code, data);
                            writer.Flush();
                        }
                        catch { }
                    }
                }
            }

            public void Dispose()
            {
                socket.Dispose();
            }

            public object Tag { get { return socket.Tag; } set { socket.Tag = value; } }
        }
    }
}
