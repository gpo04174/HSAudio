﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HS_Audio.Commander.Sender
{
    internal interface ISender
    {
        event SenderNeedsEventHandler SenderNeeds;
    }
}
