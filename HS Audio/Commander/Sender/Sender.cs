﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using System.Threading;

namespace HS_Audio.Commander.Sender
{
    internal enum Command_PlayListMode {Add, Remove, Change, Play }
    internal enum EventKind { Unknown, MusicChanged, PlayStatusChanged, VolumeChanged, PlayListChanged }
    internal enum GetKind { PlayInfo, MusicInfo, PlayList, Lyrics, SoundInfo }
    internal enum SendKind { Byte, Text, Image, Sound }
    internal enum RestartKind {Socket_All, Socket_Message, Socket_Byte, WebServer, Player }

    internal delegate string CommandReceivedEventArgs(GetKind kind, params string[] Command);
    internal delegate Exception RestartReceivedEventHandler(RestartKind Kind);
    internal delegate void NetworkSenderInfoEventHandler(GetKind kind, ref string MusicInfo, params object[] ExtraData);
    internal delegate void NetworkSenderAlbumArtEventHandler(string MusicPath, out byte[] AlbumArt, out string MIMEtype, int index = 0);
    internal class Sender
    {
        internal int BufferSizeRecv = 1024;
        internal int BufferSizeSend = 10240;

        public event CommandReceivedEventArgs CommandReceived;
        public event RestartReceivedEventHandler RestartReceived;
        public event NetworkSenderInfoEventHandler GiveMeInfo;
        public event NetworkSenderAlbumArtEventHandler GiveMeAlbumArt;

        internal List<CommandData> CommandParses(string[] cmd_arr)
        {
            if (cmd_arr == null || cmd_arr.Length == 0) return null;
            List<CommandData> data = new List<CommandData>();
            for (int i = 0; i < cmd_arr.Length; i++)
            {
                string command = cmd_arr[i].Trim().Replace("\r", "");
                data.Add(CommandParse(true, command));
            }
            return data;
        }

        string LOCK_CommandParse_AlbumArt = "LOCK_CommandParse_AlbumArt";
        internal CommandData CommandParse(bool IncludeMessage = true, params string[] command)
        {
            if (command == null || command.Length == 0) return null;
            #region echo (Loopback)
            else if (command[0].Length > 4 && command[0].Substring(0, 5).ToLower() == "echo ")
            {
                StringBuilder sb = new StringBuilder(command[0].Substring(5) + "\n");
                for (int i = 1; i < command.Length; i++) sb.Append(command[i] + "\n");
                return new CommandData(SendKind.Text, Encoding.UTF8.GetBytes(sb.ToString()), "text/plane; charset=utf-8");
            }
            #endregion

            string cmd = command[0].Trim().ToLower();
            #region set
            #region set PlayInfo
            if (cmd == "set PlayInfo".ToLower())
            {
                string Error = null;
                if (CommandReceived != null)
                {
                    string[] cmds = new string[command.Length - 1];
                    for (int i = 1; i < command.Length; i++) cmds[i - 1] = command[i];
                    Error = CommandReceived(GetKind.PlayInfo, cmds);
                }
                return new CommandData(SendKind.Text, Error != null ? Encoding.UTF8.GetBytes("[Result]\nResult=Fail\nMessage=(" + Error + ")") : Encoding.UTF8.GetBytes("[Result]\nResult=Success"), "text/plane; charset=utf-8");
            }
            #endregion
            #region set PlayList
            else if (cmd == "set PlayList".ToLower())
            {
                string Error = null;
                if (CommandReceived != null)
                {
                    string[] cmds = new string[command.Length - 1];
                    for (int i = 1; i < command.Length; i++) cmds[i - 1] = command[i];
                    Error = CommandReceived(GetKind.PlayList, cmds);
                }
                return new CommandData(SendKind.Text, Error != null ? Encoding.UTF8.GetBytes("[Result]\nResult=Fail\nMessage=(" + Error + ")") : Encoding.UTF8.GetBytes("[Result]\nResult=Success"), "text/plane; charset=utf-8");
            }
            #endregion
            #endregion

            #region get
            #region get PlayInfo
            if (cmd == "get PlayInfo".ToLower())
            {
                string info = "[PlayInfo]\n";
                GiveMeInfo(GetKind.PlayInfo, ref info, null);
                CommandData data = new CommandData(SendKind.Text, Encoding.UTF8.GetBytes(info));
                return data;
            }
            #endregion
            #region get MusicInfo
            else if (cmd == "get MusicInfo".ToLower())
            {
                string info = "[MusicInfo]\n";

                bool AlbumArt = false;
                string MusicPath = null;
                for (int i = 1; i < command.Length; i++)
                {
                    try
                    {
                        string[] par = new string[2];
                        if (command[i].IndexOf("=") > -1)
                        {
                            par[0] = command[i].Remove(command[i].IndexOf("=")).TrimStart().ToLower();
                            par[1] = command[i].Substring(command[i].IndexOf("=") + 1).TrimEnd();
                        }
                        else continue;//spl[0] = info[i];

                        if (par[0] == "MusicPath".ToLower()) MusicPath = par[1];
                        else if (par[0] == "AlbumArt".ToLower()) AlbumArt = Convert.ToBoolean(par[1]);
                    }
                    catch { }
                }
                GiveMeInfo.Invoke(GetKind.MusicInfo, ref info, MusicPath, AlbumArt);
                CommandData data = new CommandData(SendKind.Text, Encoding.UTF8.GetBytes(info));
                return data;
            }
            #endregion
            #region get PlayList
            else if (cmd == "get PlayList".ToLower())
            {
                bool IsDetail = false;
                bool AlbumArt = false;
                List<int> Index = new List<int>();
                if (command.Length > 1)
                {
                    for (int i = 1; i < command.Length; i++)
                    {
                        try
                        {
                            string[] par = command[i].Trim().ToLower().Split('=');
                            if (par[0] == "IsDetail".ToLower()) IsDetail = Convert.ToBoolean(par[1]);
                            else if (par[0] == "AlbumArt".ToLower()) AlbumArt = Convert.ToBoolean(par[1]);
                            else if (par[0] == "Index".ToLower())
                            {
                                string[] a = par[1].Split(',');
                                for (int j = 0; j < a.Length; j++) try { Index.Add(Convert.ToInt32(a[j])); } catch { }
                            }
                        }
                        catch { }
                    }
                }
                string info = "[PlayList]\n";
                if (GiveMeInfo != null) GiveMeInfo(GetKind.PlayList, ref info, IsDetail, AlbumArt, Index.ToArray());
                CommandData data = new CommandData(SendKind.Text, Encoding.UTF8.GetBytes(info));
                return data;
            }
            #endregion
            #region get AlbumArt
            else if (cmd == "get AlbumArt".ToLower())
            {
                lock (LOCK_CommandParse_AlbumArt)
                {
                    byte[] albumart = null;
                    string MIMEType = null;
                    CommandData data = null;
                    if (command.Length > 1)
                    {
                        try
                        {
                            int index = -1; string MusicPath = null;
                            for (int i = 1; i < command.Length; i++)
                            {
                                string[] spl = new string[2];
                                if (command[i].IndexOf("=") > -1)
                                {
                                    spl[0] = command[i].Remove(command[i].IndexOf("=")).TrimStart().ToLower();
                                    spl[1] = command[i].Substring(command[i].IndexOf("=") + 1).TrimEnd();
                                }
                                else continue;//spl[0] = info[i];

                                if (spl[0] == "index") index = Convert.ToInt32(spl[1]);
                                else if (spl[0] == "musicpath") MusicPath = spl[1];
                            }

                            if (index < 0) GiveMeAlbumArt(MusicPath, out albumart, out MIMEType);
                            else GiveMeAlbumArt(MusicPath, out albumart, out MIMEType, index);

                        }
                        catch { return new CommandData(SendKind.Text, Encoding.UTF8.GetBytes("Wrong index"), "text/plane; charset=utf-8"); }
                    }
                    else GiveMeAlbumArt(null, out albumart, out MIMEType);

                    if (albumart != null) data = new CommandData(SendKind.Image, albumart, MIMEType);
                    //"[Result]\nMessage=No AlbumArt"
                    else data = IncludeMessage ? new CommandData(SendKind.Text, Encoding.UTF8.GetBytes("[Result]\nResult=Success\nMessage=No AlbumArt\nLength=0"), "text/plane; charset=utf-8") : null;
                    return data;
                }
            }
            #endregion
            #region get Lyric
            else if (cmd == "get Lyrics".ToLower())
            {
                string info = "[Lyrics]\n";

                long Position = -1;
                bool IncTime = true;
                string MusicPath = null;
                string Hash = null;
                for (int i = 1; i < command.Length; i++)
                {
                    try
                    {
                        string[] par = new string[2];
                        if (command[i].IndexOf("=") > -1)
                        {
                            par[0] = command[i].Remove(command[i].IndexOf("=")).TrimStart().ToLower();
                            par[1] = command[i].Substring(command[i].IndexOf("=") + 1).TrimEnd();
                        }
                        else continue;//spl[0] = info[i];
                        
                        if (par[0] == "Position".ToLower()) Position = Convert.ToInt64(par[1]);
                        else if (par[0] == "IncTime".ToLower()) IncTime = Convert.ToBoolean(par[1]);
                        else if (par[0] == "Hash".ToLower()) Hash = par[1];
                        else if (par[0] == "MusicPath".ToLower()) MusicPath = par[1];
                    }
                    catch {}
                }
                GiveMeInfo.Invoke(GetKind.Lyrics, ref info, Position, IncTime, Hash, MusicPath);
                CommandData data = new CommandData(SendKind.Text, Encoding.UTF8.GetBytes(info));
                return data;
            }
            #endregion
            #endregion

            #region restart
            #region restart Socket
            else if (cmd == "restart Socket_All".ToLower())
            {
                if (RestartReceived != null)
                {
                    Exception Suc = RestartReceived(RestartKind.Socket_All);
                    return new CommandData(SendKind.Text, Encoding.UTF8.GetBytes("[Result]\nMessage=Restart All Socket...\nResult=" + (Suc == null ? "Success" : Suc.Message)), "text/plane; charset=utf-8");
                }
                return new CommandData(SendKind.Text, Encoding.UTF8.GetBytes("[Result]\n\nMessage=Restart All Socket..."), "text/plane; charset=utf-8");
            }
            #endregion
            #region restart Socket
            else if (cmd == "restart Socket_Message".ToLower())
            {
                if (RestartReceived != null)
                {
                    Exception Suc = RestartReceived(RestartKind.Socket_Message);
                    return new CommandData(SendKind.Text, Encoding.UTF8.GetBytes("[Result]\nMessage=Restart Message Socket...\nResult=" + (Suc == null ? "Success" : Suc.Message)), "text/plane; charset=utf-8");
                }
                return new CommandData(SendKind.Text, Encoding.UTF8.GetBytes("[Result]\n\nMessage=Restart Message Socket..."), "text/plane; charset=utf-8");
            }
            #endregion
            #region restart Socket_Byte
            else if (cmd == "restart Socket_Byte".ToLower())
            {
                if (RestartReceived != null)
                {
                    Exception Suc = RestartReceived(RestartKind.Socket_Byte);
                    return new CommandData(SendKind.Text, Encoding.UTF8.GetBytes("[Result]\nRestart Message Socket... " + (Suc == null ? "Success" : Suc.Message)), "text/plane; charset=utf-8");
                }
                return new CommandData(SendKind.Text, Encoding.UTF8.GetBytes("[Result]\nRestart Message Socket..."), "text/plane; charset=utf-8");
            }
            #endregion
            #region restart WebServer
            else if (cmd == "restart WebServer".ToLower())
            {
                if (RestartReceived != null)
                {
                    Exception Suc = RestartReceived(RestartKind.WebServer);
                    return new CommandData(SendKind.Text, Encoding.UTF8.GetBytes("[Result]\nRestart WebServer... " + (Suc == null ? "Success" : Suc.Message)), "text/plane; charset=utf-8");
                }
                return new CommandData(SendKind.Text, Encoding.UTF8.GetBytes("[Result]\nRestart WebServer..."), "text/plane; charset=utf-8");
            }
            #endregion
            #region restart Socket_Byte
            else if (cmd == "restart Player".ToLower())
            {
                Exception Suc = RestartReceived(RestartKind.Player);
                //return new CommandData(SendKind.Text, Encoding.UTF8.GetBytes("Restarted WebServer..."), "text/plane; charset=utf-8");
            }
            #endregion
            #endregion

            #region Help

            if (cmd == "Help".ToLower())
            {
                string info = null;
                try
                {
                    Assembly _assembly = Assembly.GetExecutingAssembly();
                    StreamReader sr = new StreamReader(_assembly.GetManifestResourceStream("HS_Audio.Commander.Sender.Help.txt"));
                    info = sr.ReadToEnd();
                    sr.ReadToEnd();
                    sr.Close();
                }
                catch (Exception ex) { info = "[Result]\r\nStatus=Fail\r\nMessage=An error has occurred to showing help. (도움말말을 표시하는중 오류발생.)\r\n\r\n" + ex.Message; }
                CommandData data = new CommandData(SendKind.Text, Encoding.UTF8.GetBytes(info));
                return data;
            }
            #endregion

            return null;
        }

        string LOCK_setEvent = "LOCK_setEvent";
        internal byte[] setEvent(string Event, params object[] Data)
        {
            lock (LOCK_setEvent)
            {
                StringBuilder sb = new StringBuilder("[Event]\nKind=");
                sb.Append(Event).Append("\n");
                /*
                if (Event == "MusicChanged")
                {
                    string mi = null;
                    GiveMeInfo(GetKind.PlayInfo, ref mi, null);
                    info += "\n" + mi;
                }
                */
                /*else*/
                for (int i = 0; i < Data.Length; i++)
                    sb.Append("Data=").Append(Data[i]).Append("\n");
                byte[] info_buf = Encoding.UTF8.GetBytes(sb.ToString());
                return info_buf;
            }
        }
        internal byte[] setMessage(GetKind kind, string Message)
        {
            StringBuilder sb = new StringBuilder("[").Append(kind).Append("]\n");
            sb.Append(Message);
            byte[] info_buf = Encoding.UTF8.GetBytes(sb.ToString());
            return info_buf;
        }


        public static string GetMd5Hash(byte[] input)
        {
            using (MD5 md5Hash = MD5.Create())
            {
                // Convert the input string to a byte array and compute the hash.
                byte[] data = md5Hash.ComputeHash(input);

                // Create a new Stringbuilder to collect the bytes
                // and create a string.
                StringBuilder sBuilder = new StringBuilder();

                // Loop through each byte of the hashed data 
                // and format each one as a hexadecimal string.
                for (int i = 0; i < data.Length; i++)
                {
                    sBuilder.Append(data[i].ToString("x2"));
                }

                // Return the hexadecimal string.
                return sBuilder.ToString();
            }
        }

        string LOCK_HttpRecvHeader = "LOCK_HttpRecvHeader";
        public Header HttpRecvHeader(Stream ns, bool Close = false)
        {
            /* ******************
             * HTTP 헤더 예시 from www.joinc.co.kr
             * 1 GET /cgi-bin/http_trace.pl HTTP/1.1\r\n
             * 2 ACCEPT_ENCODING: gzip,deflate,sdch\r\n
             * 3 CONNECTION: keep-alive\r\n
             * 4 ACCEPT: text/html,application/xhtml+xml,application/xml;q=0.9,* /*;q=0.8\r\n
             * 5 ACCEPT_CHARSET: windows-949,utf-8;q=0.7,*;q=0.3\r\n
             * 6 USER_AGENT: Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.1 (KHTML, like Gecko) Chrome/13.0.782.24\r\n 
             * 7 ACCEPT_LANGUAGE: ko-KR,ko;q=0.8,en-US;q=0.6,en;q=0.4\rn
             * 8 HOST: www.joinc.co.kr\r\n
             * 9 \r\n
             * 
             * (중요, 4번 라인의 q=0.9, 이후의 *와 /*는 공백없이 붙어있어야 함)
             * 
             * HTTP 헤더의 구성 (최상위라인 헤더 3개)
             * 1. 요청 메소드: GET, PUT, POST, PUSH, OPTION 중 하나의 요청방식
             * 2. 요청 URI: 요청하는 자원의 위치 명시
             * 3. HTTP 프로토콜 버전: 웹 브라우저가 사용하는 프로토콜 버전
             * 
             * HTTP 헤더는 캐리지리턴(\r\n)을 사용함을 주의
             * 첫 줄은 그냥 읽고, 다음 줄 부터는 콜론과 캐리지리턴을 기준으로 split하면 될것
             */

            lock (LOCK_HttpRecvHeader)
            {
                // 클라이언트로부터 헤더를 입력 받는 부분
                // 여기서는 첫 줄만 입력 받음
                StringBuilder sb = new StringBuilder();
                while (true)
                {
                    int readbyte = ns.ReadByte();
                    if (readbyte == '\r') { continue; }
                    if (readbyte == '\n') { break; }
                    if (readbyte == -1) { Thread.Sleep(1); continue; } // 스트림이 브라우저로 부터 읽지 못한 경우이므로 패스
                    sb.Append((char)readbyte);
                }
                string read = sb.ToString();

                Header hd = new Header();
                // 헤더 첫 줄 분석
                string[] headerFirst = read.Split(' ');
                hd.reqMethod = headerFirst[0];
                hd.httpVer = headerFirst[2];
                hd.reqURI = headerFirst[1];
                hd.reqURIs = headerFirst[1].Split('?');
#if DEBUG
                Console.WriteLine("{0} / {1} / {2}", hd.reqMethod, hd.reqURI, hd.httpVer);
#endif

                StringBuilder key = new StringBuilder();
                StringBuilder value = new StringBuilder();
                bool iskey = true;
                byte[] headerLeft = new byte[4096];


                // 나머지 헤더 분석
                hd.headers = new Dictionary<string, string>();
                for (int i = 0; i < headerLeft.Length; i++)
                {
                    int readbyte = headerLeft[i];
                    if (readbyte == ':') { iskey = false; continue; }
                    if (readbyte == '\n')
                    {
                        iskey = true;
                        hd.headers.Add(key.ToString(), value.ToString());
                        key.Remove(0, key.Length);
                        value.Remove(0, value.Length);
                        continue;
                    }
                    if (readbyte == '\r') continue;
                    if (readbyte == -1) { Thread.Sleep(1); continue; }
                    if (iskey == true) key.Append((char)readbyte);
                    else value.Append((char)readbyte);
                }

                for (int i = 0; i < hd.headers.Count; i++)
                {
                    //Console.WriteLine("{0}: {1}", headers.ElementAt(i).Key, headers.ElementAt(i).Value);
                }
                Console.WriteLine("[TcpSock] Header Anaysis Complete\n");
                if (Close) ns.Close();
                return hd;
            }
        }
        string LOCK_HttpRecvHeader1 = "LOCK_HttpRecvHeader1";
        public Header HttpRecvHeader(string response, out string body)
        {
            /* ******************
             * HTTP 헤더 예시 from www.joinc.co.kr
             * 1 GET /cgi-bin/http_trace.pl HTTP/1.1\r\n
             * 2 ACCEPT_ENCODING: gzip,deflate,sdch\r\n
             * 3 CONNECTION: keep-alive\r\n
             * 4 ACCEPT: text/html,application/xhtml+xml,application/xml;q=0.9,* /*;q=0.8\r\n
             * 5 ACCEPT_CHARSET: windows-949,utf-8;q=0.7,*;q=0.3\r\n
             * 6 USER_AGENT: Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.1 (KHTML, like Gecko) Chrome/13.0.782.24\r\n 
             * 7 ACCEPT_LANGUAGE: ko-KR,ko;q=0.8,en-US;q=0.6,en;q=0.4\rn
             * 8 HOST: www.joinc.co.kr\r\n
             * 9 \r\n
             * 
             * (중요, 4번 라인의 q=0.9, 이후의 *와 /*는 공백없이 붙어있어야 함)
             * 
             * HTTP 헤더의 구성 (최상위라인 헤더 3개)
             * 1. 요청 메소드: GET, PUT, POST, PUSH, OPTION 중 하나의 요청방식
             * 2. 요청 URI: 요청하는 자원의 위치 명시
             * 3. HTTP 프로토콜 버전: 웹 브라우저가 사용하는 프로토콜 버전
             * 
             * HTTP 헤더는 캐리지리턴(\r\n)을 사용함을 주의
             * 첫 줄은 그냥 읽고, 다음 줄 부터는 콜론과 캐리지리턴을 기준으로 split하면 될것
             */

            lock (LOCK_HttpRecvHeader1)
            {
                // 클라이언트로부터 헤더를 입력 받는 부분
                // 여기서는 첫 줄만 입력 받음
                StringBuilder sb = new StringBuilder();
                int cr = response.IndexOf('\n');
                string read = cr < 0 ? response : response.Remove(cr);
                string bd = response.Substring(cr + 1);

                Header hd = new Header();
                // 헤더 첫 줄 분석
                string[] headerFirst = read.Split(' ');
                if (headerFirst.Length < 3) { body = response; return null; }
                hd.reqMethod = headerFirst[0];
                hd.httpVer = headerFirst[2];
                hd.reqURI = headerFirst[1];
                hd.reqURIs = headerFirst[1].Split('?');
#if DEBUG
                Console.WriteLine("{0} / {1} / {2}", hd.reqMethod, hd.reqURI, hd.httpVer);
#endif

                StringBuilder key = new StringBuilder();
                StringBuilder value = new StringBuilder();
                bool iskey = true;

                // 나머지 헤더 분석
                hd.headers = new Dictionary<string, string>();
                int i = 0;
                for (; i < bd.Length; i++)
                {
                    char readbyte = bd[i];
                    if (readbyte == ':') { iskey = false; continue; }
                    if (readbyte == '\n')
                    {
                        iskey = true;
                        hd.headers.Add(key.ToString(), value.ToString());
                        key.Remove(0, key.Length);
                        value.Remove(0, value.Length);
                        continue;
                    }
                    if (readbyte == '\r') continue;
                    //5if (readbyte == -1) { Thread.Sleep(1); continue; }
                    if (iskey == true) key.Append(readbyte);
                    else value.Append(readbyte);
                }
                body = bd.Substring(i);

                for (i = 0; i < hd.headers.Count; i++)
                {
                    //Console.WriteLine("{0}: {1}", headers.ElementAt(i).Key, headers.ElementAt(i).Value);
                }
                Console.WriteLine("[TcpSock] Header Anaysis Complete\n");
                return hd;
            }
        }
    }
    internal class CommandData
    {
        public CommandData(SendKind Kind, byte[] Data, string MIMEType = null) { this.Kind = Kind; this.Data = Data; this.MIMEType = MIMEType; }
        public SendKind Kind;
        public byte[] Data;
        public string MIMEType;
    }
    internal class Header
    {
        // 헤더 분석
        public string reqMethod;
        public string reqURI;
        public string httpVer;
        public string[] reqURIs;
        public Dictionary<string, string> headers;
    }
}
