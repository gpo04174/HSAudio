﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;

namespace HS_Audio.Commander.Sender
{
    //참고할만한 사이트
    //http://nowonbun.tistory.com/25
    internal class WebServerSender : Sender, ISender, IDisposable
    {
        public event SenderNeedsEventHandler SenderNeeds;
        internal enum ResponseCode {OK = 200, Redirect = 301, Denied_Access = 403, NotAuth = 401, NotFound = 404, Error_Request = 406, Error_Server = 500 }
        internal enum MIMEType { Text, Image, Audio, Video, Application }
        internal enum MIMETypeSub { ALL, CUSTOM, plane, html, css, png, jpg, gif, tif, mpeg, ogg, wav, webm, octet_stream }

        Thread th;
        private IPEndPoint ipep;
        private TcpListener listener;
        private List<SocketPack> clients = new List<SocketPack>();

        public WebServerSender(int WebPort = 10704)
        {
            ipep = new IPEndPoint(IPAddress.Any, WebPort);
        }

        #region public 메서드
        public void Init()
        {
            // IP 및 포트를 할당하여 EndPoint를 만듦
            listener = new TcpListener(ipep);
            listener.Start();
            Console.WriteLine("[TcpSock] TcpListener Opened");
            // Socket으로 치면 바인딩 후 리슨 시작
            //Listen();
            th = new Thread(Listen);
            th.Start();
        }
        public void Dispose()
        {
            try { listener.Stop(); } catch { }
            try { th.Abort(); } catch { }
            Remove(null, true);
        }
        #endregion

        #region Working
        private void Listen()
        {
            // 클라이언트로부터 accept를 받는 부분
            // block이 걸리므로 별도의 스레드 처리를 하는게 낫다
            // ...그 block이 걸려야 하는데
            while (true)
            {
                try
                {
                    // 먼저 들어오는 연결을 받는다
                    Console.WriteLine("[TcpSock] Accept Waiting");
                    // 연결된 후 서버와 통신하는 동작을 별도의 스레드로 처리한다

                    TcpClient client = listener.AcceptTcpClient();
                    Sender s = null;
                    SenderNeeds(this, ref s);
                    SocketPack pack = new SocketPack(client, s, new Thread(new ParameterizedThreadStart(HttpWork)));

                    pack.Client.SendBufferSize = BufferSizeSend;
                    clients.Add(pack);
                    pack.Thread.Start(pack);
                }
                catch(Exception ex) { System.Diagnostics.Debug.WriteLine(ex.Message); Thread.Sleep(1000); }
            }
        }

        private void HttpWork(Object pack)
        {
            SocketPack sp = (SocketPack)pack;
            TcpClient client = sp.Client;
            NetworkStream ns  = null;
            try
            {
                while (client.Connected)
                {
                    // 클라이언트로부터 메시지를 받기 위한 스트림 생성
                    ns = client.GetStream();

                    // 클라이언트에서 헤더 메시지 받기
                    Header header = new Header();
                    #region HttpRecvHeader
                    {
                        byte[] buf = new byte[1024];
                        MemoryStream ms = new MemoryStream();
                        int recv = 0;
                        while ((recv = ns.Read(buf, 0, buf.Length)) > 0) { ms.Write(buf, 0, recv); if (recv < buf.Length) break; }

                        // 클라이언트로부터 헤더를 입력 받는 부분
                        // 여기서는 첫 줄만 입력 받음
                        string read = Encoding.UTF8.GetString(ms.GetBuffer()).Trim();
                        ms.Dispose();
                        
                        // 헤더 첫 줄 분석
                        string[] headerFirst = read.Split(' ');
                        header.reqMethod = headerFirst[0];
                        header.httpVer = headerFirst[2];
                        header.reqURI = headerFirst[1];
                        header.reqURIs = headerFirst[1].Split('?');
#if DEBUG
                        Console.WriteLine("{0} / {1} / {2}", header.reqMethod, header.reqURI, header.httpVer);
#endif

                        StringBuilder key = new StringBuilder();
                        StringBuilder value = new StringBuilder();
                        bool iskey = true;

                        // 나머지 헤더 분석
                        header.headers = new Dictionary<string, string>();


                        for (int i = 0; i < read.Length; i++)
                        {
                            char readbyte = read[i];
                            if (readbyte == ':') { iskey = false; continue; }
                            if (readbyte == '\n')
                            {
                                iskey = true;
                                header.headers.Add(key.ToString(), value.ToString());
                                key.Remove(0, key.Length);
                                value.Remove(0, value.Length);
                                continue;
                            }
                            if (readbyte == '\r') continue;
                            if (readbyte == -1) { Thread.Sleep(1); continue; }
                            if (iskey == true) key.Append(readbyte);
                            else value.Append(readbyte);
                        }
                    }
                    #endregion
                    sp.Tag = header;

                    // 웹페이지 내용 전송하기
                    //HttpSendMessage(ns, header);
                    byte[] msg = null;
                    string MIME = "text/html; charset=UTF-8";
                    ResponseCode code = ResponseCode.NotFound;

                    #region HttpSendMessage
                    {
                        // 지원 가능한 파일에 대해 웹페이지 파일을 열고 클라이언트로 전송
                        StreamReader sr = new StreamReader(ns);

                        // 파일 내용을 보내기 전에 HTTP 프로토콜에 대한 리턴 메시지 전송
                        if (header.reqMethod.ToUpper() == "GET" || header.reqMethod == "POST".ToUpper())
                        {
                            if (header.reqURIs.Length == 1)
                            {
                                string Msg = "<h1>Hello, HS™ Player Web Server!!</h1>";
                                msg = Encoding.UTF8.GetBytes(Msg);
                                code = ResponseCode.OK;
                            }
                            else
                            {
                                string[] Params = header.reqURIs[1].Split('&');
                                Params = URLParamToCommand(Params);
                                //for (int i = 0; i < Params.Length; i++)
                                //string[] cmds = Params[0].Split('=');

                                CommandData data = sp.Sender.CommandParse(true, Params);
                                if (data == null)
                                {
                                    msg = Encoding.UTF8.GetBytes("잘못된 파라미터(인수) 입니다. (Wrong Command)");
                                    // 클라이언트로 서버 메시지 보내기
                                    code = ResponseCode.Error_Request;
                                }
                                else if (data.Data == null)
                                {
                                    msg = Encoding.UTF8.GetBytes("명령어를 잘못 입력하셨습니다. (Wrong Command)");
                                    // 클라이언트로 서버 메시지 보내기
                                    code = ResponseCode.Error_Request;
                                }
                                else
                                {
                                    MIMEManager mm = null;
                                    if (data.MIMEType == null ? true : data.MIMEType.Trim() == "")
                                    {
                                        switch (data.Kind)
                                        {
                                            case SendKind.Image: mm = new MIMEManager(MIMEType.Image, MIMETypeSub.ALL); break;
                                            case SendKind.Text: mm = new MIMEManager(MIMEType.Text, MIMETypeSub.plane); break;
                                            case SendKind.Sound: mm = new MIMEManager(MIMEType.Audio); break;
                                            default: mm = new MIMEManager(MIMEType.Application); break;
                                        }
                                        MIME = mm.ContentType;
                                    }
                                    else MIME = data.MIMEType;
                                    // 클라이언트로 서버 메시지 보내기
                                    msg = data.Data;
                                    code = data == null ? ResponseCode.NotFound : ResponseCode.OK;
                                }
                            }
                        }
                        else Console.WriteLine("GET, POST 이외의 방식 미지원");
                    }
                    #endregion

                    #region HttpSendHeader
#if DEBUG
                    Console.WriteLine("GET Message Send");
#endif
                    StringBuilder httpMsg = new StringBuilder(string.Format("HTTP/1.1 {0} {1}\r\n", (int)code, code.ToString()));
                    httpMsg.AppendLine("Connection: keep-alive");
                    httpMsg.AppendLine("Server: HSPlayerWebServer");
                    httpMsg.Append("Content-Length: ").AppendLine((msg == null ? 0 : msg.Length).ToString());
                    if (MIME != null) httpMsg.Append("Content-Type: ").AppendLine(MIME).AppendLine();

                    byte[] head = Encoding.UTF8.GetBytes(httpMsg.ToString());
                    ns.Write(head, 0, head.Length);
                    if (msg != null && msg.Length > 0) ns.Write(msg, 0, msg.Length);
                    ns.Flush();
#if DEBUG
                    Console.WriteLine(httpMsg.ToString());
                    Console.WriteLine("[TcpSock] GET Message Send Complete");
#endif
                    #endregion

                    //스트림 닫기
                    ns.Close();
                }
            }
            catch(Exception ex)
            {
                try { HttpSendHeader(ns, Encoding.UTF8.GetBytes("500 Internal Error<br/><br</>" + ex.Message), "text/html; charset=UTF-8", ResponseCode.Error_Server, client.SendBufferSize); } catch { }
                //try { ns.Close(); } catch { }
            }
            finally { ns.Close(); Remove(sp); }
        }
        #endregion

        #region HTTP 처리

        string LOCK_HttpSendHeader = "LOCK_HttpSendHeader";
        private void HttpSendHeader(NetworkStream ns, byte[] data, string MIME, ResponseCode code, int BufferSize = 10240)
        {
            lock (LOCK_HttpSendHeader)
            {
#if DEBUG
                Console.WriteLine("GET Message Send");
#endif
                StringBuilder httpMsg = new StringBuilder(string.Format("HTTP/1.1 {0} {1}\r\n", (int)code, code.ToString()));
                //httpMsg.AppendLine("Upgrade: websocket");
                httpMsg.AppendLine("Connection: keep-alive");
                //httpMsg.AppendLine("Connection: Upgrade");
                httpMsg.AppendLine("Server: HSPlayerWebServer");
                httpMsg.Append("Content-Length: ").AppendLine((data == null ? 0 : data.Length).ToString());
                if (MIME != null) httpMsg.Append("Content-Type: ").AppendLine(MIME).AppendLine();
                //httpMsg.AppendLine("WebSocket-Origin: http://localhost:8080");
                //httpMsg.AppendLine("WebSocket-Location: ws://localhost:8081");
                //httpMsg.Append("Data: ");httpMsg.AppendLine(data);

                byte[] head = Encoding.UTF8.GetBytes(httpMsg.ToString());
                ns.Write(head, 0, head.Length);
                if (data != null && data.Length > 0) ns.Write(data, 0, data.Length);
                ns.Flush();
                //WriteBuffer(ns, head, BufferSize);
                //if(data != null && data.Length > 0)WriteBuffer(ns, data, BufferSize);
#if DEBUG
                Console.WriteLine(httpMsg.ToString());
                Console.WriteLine("[TcpSock] GET Message Send Complete");
#endif
            }
        }

        [Obsolete]
        void WriteBuffer(Stream stream, byte[] data, int BufferSize)
        {
            if (data.Length > BufferSize)
            {
                int len = data.Length;
                int len_cnt = len / BufferSize;
                int i = 0;
                for (; i < len_cnt; i++)
                {
                    stream.Write(data, (i * (BufferSize - 1)), len_cnt);
                    stream.Flush();
                }
                
                int le = len % BufferSize;
                if (le > 0)
                {
                    stream.Write(data, (i * (BufferSize - 1)), le);
                    stream.Flush();
                }
            }
            else
            {
                stream.Write(data, 0, data.Length);
                stream.Flush();
            }
        }
        string LOCK = "LOCK";
        private void Remove(SocketPack pack, bool All = false)
        {
            lock (LOCK)
            {
                for (int i = 0; i < clients.Count; i++)
                    if (All || clients[i].ID == pack.ID)
                    {
                        clients[i].Client.Close();
                        try { clients[i].Thread.Abort(); } catch { }
                        clients.RemoveAt(i);
                    }
            }
        }
        #endregion

        string LOCK_HttpSendMessage = "LOCK_HttpSendMessage";
        private void HttpSendMessage(NetworkStream ns, Header header, Sender sender)
        {
            lock (LOCK_HttpSendMessage)
            {
                // 지원 가능한 파일에 대해 웹페이지 파일을 열고 클라이언트로 전송
                StreamReader sr = new StreamReader(ns);

                // 파일 내용을 보내기 전에 HTTP 프로토콜에 대한 리턴 메시지 전송
                if (header.reqMethod.ToUpper() == "GET" || header.reqMethod == "POST".ToUpper())
                {
                    if (header.reqURIs.Length == 1)
                    {
                        string Msg = "<h1>Hello, HS™ Player Web Server!!</h1>";
                        HttpSendHeader(ns, Encoding.UTF8.GetBytes(Msg), "text/html; charset=utf-8", ResponseCode.OK);
                    }
                    else
                    {
                        string[] Params = header.reqURIs[1].ToLower().Split('&');
                        Params = URLParamToCommand(Params);
                        //for (int i = 0; i < Params.Length; i++)
                        //string[] cmds = Params[0].Split('=');

                        CommandData data = sender.CommandParse(true, Params);
                        if (data == null)
                        {
                            byte[] msg = Encoding.UTF8.GetBytes("잘못된 파라미터(인수) 입니다. (Wrong Command)");
                            // 클라이언트로 서버 메시지 보내기
                            HttpSendHeader(ns, msg, "text/html; charset=UTF-8", ResponseCode.Error_Request);
                        }
                        else if (data.Data == null)
                        {
                            byte[] msg = Encoding.UTF8.GetBytes("명령어를 잘못 입력하셨습니다. (Wrong Command)");
                            // 클라이언트로 서버 메시지 보내기
                            HttpSendHeader(ns, msg, "text/html; charset=UTF-8", ResponseCode.Error_Request);
                        }
                        else
                        {
                            MIMEManager mm = null;
                            if (data.MIMEType == null ? true : data.MIMEType.Trim() == "")
                            {
                                switch (data.Kind)
                                {
                                    case SendKind.Image: mm = new MIMEManager(MIMEType.Image, MIMETypeSub.ALL); break;
                                    case SendKind.Text: mm = new MIMEManager(MIMEType.Text, MIMETypeSub.plane); break;
                                    case SendKind.Sound: mm = new MIMEManager(MIMEType.Audio); break;
                                    default: mm = new MIMEManager(MIMEType.Application); break;
                                }
                                // 클라이언트로 서버 메시지 보내기
                                HttpSendHeader(ns, data.Data, mm.ContentType, data == null ? ResponseCode.NotFound : ResponseCode.OK);
                            }
                            else HttpSendHeader(ns, data.Data, data.MIMEType, ResponseCode.OK);
                        }
                    }
                }
                else Console.WriteLine("GET, POST 이외의 방식 미지원");
            }
        }

        public string WebURLParser(string URL)
        {
            try { return Uri.UnescapeDataString(URL); }
            catch
            {
                return URL.ToLower().
                        Replace("%20", " ").
                        Replace("%0a", "\n").
                        Replace("%3d", "=");
            }
                    
        }
        public string[] URLParamToCommand(string[] URLParam)
        {
            string[] a = new string[URLParam.Length];
            a[0] = URLParam[0].Replace("=", " ");
            for (int i = 1; i < a.Length; i++)
                a[i] = WebURLParser(URLParam[i]);
            return a;
        }

        /*
        private void WebViewer(NetworkStream ns)
        {
            // 지원 가능한 파일에 대해 웹페이지 파일을 열고 클라이언트로 전송
            string path = "";
            string data = "";

            // index 여부 확인
            path = GetPath(reqURI);
            if (path == "NO_INDEX")
            {
                data = "<html><body>Cannot find index</body></html>";
            }
            else if (path == "")
            {
                data = "";
            }
            else
            {
                FileStream fs = File.Open(path, FileMode.Open);
                Console.WriteLine("Index: {0}", new FileInfo(path).FullName);
                StreamReader fsr = new StreamReader(fs);
                data = fsr.ReadToEnd();
            }

            // 메시지 추가 전송
            string aMsg = "Content-Length: " + data.Length + "\r\n"
            + "\r\n"
            + data;

            byte[] send = Encoding.UTF8.GetBytes(aMsg);
            ns.Write(send, 0, send.Length);
            Console.WriteLine(aMsg);
            Console.WriteLine("[TcpSock] GET Message Send Complete\n\n\n");
        }

        private string GetPath(string uri)
        {
            string path = "";
            if (uri == "/")
            {
                // index인 경우 default.dat 열기
                FileStream fs = File.Open("./data/default.dat", FileMode.Open);
                StreamReader fsr = new StreamReader(fs);
                while (fsr.Peek() != -1)
                {
                    string data = fsr.ReadLine();
                    FileInfo file = new FileInfo("./root/" + data);
                    if (file.Exists == true)
                    {
                        path = "./root/" + data;
                        break;
                    }
                }
                if (path == "") path = "NO_INDEX";
            }
            else
            {
                bool exist = new FileInfo("./root" + uri).Exists;
                if (exist == true) path = "./root" + uri;
                else path = "";
            }

            // 파일 존재여부 확인

            return path;
        }
         */

        #region Inner Class
        private class MIMEManager
        {
            public MIMEManager(MIMEType type, MIMETypeSub subtype = MIMETypeSub.ALL)
            {
                this.Type = type; this.SubType = subtype;
                ContentType = type == MIMEType.Text ?
                    string.Format("{0}/{1}; charset=utf-8", type.ToString().ToLower(), subtype == MIMETypeSub.ALL ? "*" : subtype.ToString()) :
                    string.Format("{0}/{1}", type.ToString().ToLower(), subtype == MIMETypeSub.ALL ? "*" : subtype.ToString());
            }
            public MIMEManager(MIMEType type, string custom)
            {
                this.Type = type; this.SubType = MIMETypeSub.CUSTOM;
                string.Format("{0}/{1}", type, custom);
            }

            public MIMEType Type { get; private set; }
            public MIMETypeSub SubType { get; private set; }
            public string ContentType { get; private set; }
        }

        
        private class SocketPack
        {
            public SocketPack(TcpClient Client, Sender Sender, Thread Thread) { this.Client = Client; this.Sender = Sender; this.Thread = Thread; }
            public int ID { get { return this.Thread.ManagedThreadId; } }
            public Sender Sender;
            public TcpClient Client;
            public Thread Thread;

            public object Tag;
        }
        #endregion
    }
}
