﻿get PlayInfo : Get Play Information (재생정보를 가져옵니다)
get MusicInfo : Get Music Information (음악 정보를 가져옵니다)
get AlbumArt : Get AlbumArt to byte array (앨범아트를 바이트 배열로 가져옵니다.
    MusicPath=<String> : Optional, get music album art to path (If the value is empty, get the currently playing music album art) (옵션, 음악경로에서 앨범아트를 가져옵니다. (만약 값이 비어있으면 현재 재생중인 앨범아트를 가져옵니다.)) //ex: MusicPath=C:\Classic.mp3
    index=<Number> : Optional, set to index (옵션, index 를 설정할 수 있습니다.)) //ex: index=0
get PlayList : Get playing lists (재생목록을 가져옵니다.)
    IsDetail=True | False : Optional, Whether or not to show more information (including tags) (옵션, 자세한 정보 (태그포함)를 표시할지의 여부입니다.))
    AlbumArt=True | False : Optional, Whether or not include album art (옵션, 앨범아트를 포함할지의 여부입니다.))
    Index=<Number> : Optional, Specifies the index to import. (Separated by ',') (옵션, 가져올 index 를 지정합니다. (',' 로 구분합니다.))
get Lyrics : Get lyrics (가사를 가져옵니다.)
    Position=<Number> : 가사의 특정 시간을 지정합니다.
    IncTime=True | False : 맨 앞에 시간을 표시할지의 여부입니다.
    MusicPath=<String> : 가사를 가져올 경로입니다.

set PlayInfo : Set Play Information (재생정보를 설정합니다.)
    PlayStatus=Play | Pause | Stop | Next | Prev
    Volume=<Number> : Volume (음량) //ex: Volume=50
    Position=<Number> : Enter the position (Millisecond) (재생위치를 입력하십시오 (밀리초 단위입니다.)) //ex=100000

set PlayList : Set Play list;
    Mode=Play : Essential option.
    Data=<Number> : Essential, Set to index

echo (Some Text) : Echo the reply data (입력한 데이터를 되돌려줍니다)
help : View to help (도움말을 봅니다.)
