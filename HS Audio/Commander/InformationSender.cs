﻿using System;
using System.Collections.Generic;
using System.Text;
using HS_Audio.Commander.Sender;
using System.Threading;
using System.IO;
using TagLib;
using HS_Audio.Commander.Struct;
using System.Diagnostics;

namespace HS_Audio.Commander
{
    class InformationSender
    {
        frmMain frm;

        WebSocketSender sender_network;
        WebServerSender sender_webserver;

        PlayInformation info_Play;
        MusicInformation info_Music = new MusicInformation();

        delegate string Action(GetKind knd, string[] Command);

        public InformationSender(frmMain frm)
        {
            this.frm = frm;
            frm.Helper.MusicChanged += Helper_MusicChanged;
            Thread th = new Thread(new ThreadStart(() =>
            {
                InitNetwork();
                InitWebServer();
            }));
            th.Start();
            frm.PlaylistChanged += (object sender, PlayListChange change, int[] index) =>
            {
                if (sender_network != null )
                {
                    object[] obj = new object[index.Length];
                    if(obj.Length > 0)index.CopyTo(obj, 0);

                    //EventSender
                    StringBuilder sb1 = new StringBuilder();
                    sb1.Append(EventKind.PlayListChanged.ToString()).Append("\n");
                    sb1.Append("Mode=").Append(Command_PlayListMode.Add.ToString()).Append("\n");
                    SetEvent(sb1.ToString(), obj);
                }
            };

            frm.Helper.PlayingStatusChanged += (HSAudioHelper.PlayingStatus Status, int index)=> 
            {
                if(sender_network != null) SetEvent("PlayStatusChanged", Status.ToString());
                //sender_webserver.setEvent("PlayStatusChanged=" + Status.ToString());
            };
            frm.Helper.VolumeChanged += (HSAudioHelper Helper, float Volume)=> 
            {
                if (sender_network != null) SetEvent("VolumeChanged", (Volume * 100));
                //sender_webserver.setEvent("VolumeChanged=" + (Volume * 100));
            };
            frm.Helper.PosionStatusChanged += (HSAudioHelper Helper, uint Tick, bool Stop) => 
            {
                if (sender_network != null) SetEvent("PlayStatusChanged", null);
                //sender_webserver.setEvent("PlayStatusChanged");
            };
        }

        private void Helper_MusicChanged(HSAudioHelper Helper, int index, bool Error)
        {
            info_Music.Path = Helper.MusicPath;
            info_Music.AlbumArtMIME = null;
            info_Music.AlbumArtCount = -1;
            ThreadStart ts = new ThreadStart(() =>
            {
                Tag tag = GetTag(info_Music.Path);
                if (tag != null)
                {
                    info_Music.Title = (tag.Title == null) ? Path.GetFileName(info_Music.Path) : tag.Title;
                    info_Music.Artist = (tag.Artists != null && tag.Artists.Length > 0) ? tag.Artists[0] : null;
                    info_Music.Album = tag.Album;

                    piclist = tag.Pictures;
                    info_Music.AlbumArtCount = piclist.Length;
                    if (piclist.Length > 0) { info_Music.AlbumArt = piclist[0].Data.Data; info_Music.AlbumArtMIME = piclist[0].MimeType; }
                    else info_Music.AlbumArt = null;
                }
                else
                {
                    info_Music.Album = null;
                    info_Music.Artist = null;
                    info_Music.Title = null;
                    info_Music.AlbumArt = null;
                    info_Music.AlbumArtCount = -1;
                }

                if (sender_network != null)sender_network.setEvent("MusicChanged", Helper.MusicPath);
                //sender_webserver.setEvent("MusicChanged=" + info_Music.Path);
            });
            Thread th = new Thread(ts);
            th.Start();
        }
        
        public void Dispose()
        {
            sender_network.Dispose();
            sender_webserver.Dispose();
        }

        #region 초기화
        private bool InitNetwork()
        {
            try
            {
                if (sender_network != null) sender_network.Dispose();
                sender_network = new WebSocketSender();
                sender_network.SenderNeeds += (object instance, ref Sender.Sender sender)=> 
                {
                    sender = new Sender.Sender();
                    sender.GiveMeAlbumArt += new NetworkSenderAlbumArtEventHandler(GetAlbumArt);
                    sender.GiveMeInfo += new NetworkSenderInfoEventHandler(GetInfo);
                    sender.CommandReceived += Sender_CommandReceived;
                    sender.RestartReceived += Sender_network_RestartReceived;
                };
                sender_network.Init_soc();
                sender_network.Init_soc_byte();
                return true;
            }
            catch(Exception ex) { Debug.WriteLine(ex.Message); return false; }
        }
        private bool InitWebServer()
        {
            try
            {
                if (sender_webserver != null) sender_webserver.Dispose();
                sender_webserver = new WebServerSender();
                sender_webserver.SenderNeeds += (object instance, ref Sender.Sender sender) =>
                {
                    sender = new Sender.Sender();
                    sender.GiveMeAlbumArt += new NetworkSenderAlbumArtEventHandler(GetAlbumArt);
                    sender.GiveMeInfo += new NetworkSenderInfoEventHandler(GetInfo);
                    sender.CommandReceived += Sender_CommandReceived;
                    sender.RestartReceived += Sender_network_RestartReceived;
                };
                sender_webserver.Init();
                return true;
            }
            catch (Exception ex) { Debug.WriteLine(ex.Message); return false; }
        }
        #endregion

        #region 이벤트 전용
        private void GetInfo(GetKind kind, ref string info, object[] ExtraData)
        {
            try
            {
                StringBuilder sb = new StringBuilder();
                #region PlayInfo
                if (kind == GetKind.PlayInfo)
                {
                    PlayInformation info_play = GetPlayInformation();
                    info_play.Volume = frm.Helper.Volume * 100;
                    if (info_play != null)
                    {
                        sb.Append("TotalTime="); sb.Append(info_play.TotalTime.ToString()); sb.AppendLine();
                        sb.Append("CurrentTime="); sb.Append(info_play.CurrentTime.ToString()); sb.AppendLine();
                        sb.Append("Volume="); sb.Append(info_play.Volume.ToString()); sb.AppendLine();
                        sb.Append("PlayStatus="); sb.Append((int)info_play.Status); sb.AppendLine();
                        sb.Append("EnableControl="); sb.Append(info_play.EnableControl.ToString()); sb.AppendLine();
                    }
                }
                #endregion
                #region MusicInfo
                else if (kind == GetKind.MusicInfo)
                {
                    string MusicPath = (string)ExtraData[0];
                    bool AlbumArt = (bool)ExtraData[1];

                    string Title = null, Artist = null, Album = null, Path = null, AlbumArtMIME = null, MD5Hash = null;
                    int AlbumArtCount = 0;
                    byte[] AlbumArt_Local = null;
                    if (MusicPath == null ? true : MusicPath.Trim() == "")
                    {
                        Title = info_Music.Title;
                        Artist = info_Music.Artist;
                        Album = info_Music.Album;
                        Path = info_Music.Path;
                        AlbumArtMIME = info_Music.AlbumArtMIME;
                        AlbumArtCount = info_Music.AlbumArtCount;
                        AlbumArt_Local = info_Music.AlbumArt;
                        MD5Hash = Lyrics.GetLyrics.GenerateMusicHash(Path);
                    }
                    else
                    {
                        Tag tag = GetTag(MusicPath);
                        IPicture[] piclist = tag.Pictures;
                        if (piclist != null && piclist.Length > 0)
                        {
                            if (piclist.Length > 0)
                            {
                                AlbumArtCount = piclist.Length;
                                AlbumArt_Local = piclist[0].Data.Data;
                                AlbumArtMIME = piclist[0].MimeType;
                            }
                        }
                        Title = tag.Title;
                        Artist = tag.FirstArtist;
                        Album = tag.Album;
                        Path = MusicPath.Replace("\n", "\\n");
                        MD5Hash = Lyrics.GetLyrics.GenerateMusicHash(MusicPath);
                    }
                    //if (info.Title == null && Helper.MusicPath != null) GetTag(Helper.MusicPath);
                    sb.Append("Title="); if (Title != null) sb.Append(Title.Replace("\n", "\\n")); sb.AppendLine();
                    sb.Append("Artist="); if (Artist != null) sb.Append(Artist.Replace("\n", "\\n")); sb.AppendLine();
                    sb.Append("Album="); if (Album != null) sb.Append(Album.Replace("\n", "\\n")); sb.AppendLine();
                    sb.Append("Path="); if (Path != null) sb.Append(Path.Replace("\n", "\\n")); sb.AppendLine();
                    sb.Append("HashMD5="); if (MD5Hash != null) sb.Append(MD5Hash.Replace("\n", "\\n")); sb.AppendLine();
                    sb.Append("AlbumArtCount=").Append(AlbumArtCount).AppendLine();
                    sb.Append("AlbumArtMIME=").Append(AlbumArtMIME).AppendLine();
                    if (AlbumArt) { sb.Append("AlbumArt="); if (AlbumArt_Local != null) sb.Append(Convert.ToBase64String(AlbumArt_Local)); sb.AppendLine(); }
                }
                #endregion
                #region PlayList
                else if (kind == GetKind.PlayList)
                {
                    bool IsDetail = (bool)ExtraData[0];
                    bool AlbumArt = (bool)ExtraData[1];
                    int[] Index = (int[])ExtraData[2];
                    for (int i = 0; i < (Index.Length > 0 ? Index.Length : frm.listView1.Items.Count); i++)
                    {
                        System.Windows.Forms.ListViewItem li = frm.listView1.Items[Index.Length > 0 ? Index[i] : i];
                        sb.AppendLine("{");
                        try
                        {
                            try
                            {
                                string MD5Hash = Lyrics.GetLyrics.GenerateMusicHash(li.SubItems[3].Text);
                                sb.Append("   HashMD5="); if (MD5Hash != null) sb.Append(MD5Hash.Replace("\n", "\\n")); sb.AppendLine();
                            }catch { }

                            Tag tag = GetTag(li.SubItems[3].Text);
                            IPicture[] pic = tag.Pictures;
                            if (IsDetail)
                            {
                                sb.Append("   Title=").Append(tag.Title != null ? tag.Title.Replace("\n", "\\n") : li.SubItems[2].Text).AppendLine();
                                sb.Append("   Artist="); if (tag.FirstArtist != null) sb.Append(tag.FirstArtist.Replace("\n", "\\n")); sb.AppendLine();
                                sb.Append("   Album="); if (tag.Album != null) sb.Append(tag.Album.Replace("\n", "\\n")); sb.AppendLine();
                                sb.Append("   AlbumArtsCount=").Append(pic.Length).AppendLine();
                                //sb.Append("   AlbumArtMIME=").Append(tag.Pictures == null ? 0 : tag.AlbumArtMIME).AppendLine();
                            }
                            if (AlbumArt)
                            {
                                sb.Append("   AlbumArts=");
                                if (pic.Length > 0)
                                {
                                    sb.Append(Convert.ToBase64String(pic[0].Data.Data));
                                    //for (int j = 1; j < 2; j++) sb.Append(",").Append(Convert.ToBase64String(pic[j].Data.Data));
                                }
                                sb.AppendLine().Append("   AlbumArtsMIME=");
                                if (pic.Length > 0)
                                {
                                    sb.Append(pic[0].MimeType);
                                    //for (int j = 1; j < 2; j++) sb.Append(",").Append(pic[j].MimeType);
                                }
                                sb.AppendLine();
                            }
                        }
                        catch { sb.Append("   Title=").Append(li.SubItems[2].Text).AppendLine(); }
                        sb.Append("   Path="); sb.AppendLine(li.SubItems[3].Text.Replace("\n", "\\n"));
                        sb.AppendLine("},");
                    }
                }
                #endregion
                #region Lyrics
                else if (kind == GetKind.Lyrics)
                {
                    double Position = (long)ExtraData[0] / 1000d;
                    bool IncTime = (bool)ExtraData[1];
                    string Hash = (string)ExtraData[2];
                    string MusicPath = (string)ExtraData[3];

                    if (frm.lrcf != null && (Hash == null || Hash.Trim() == "") && (MusicPath == null || MusicPath.Trim() == ""))
                    {
                        sb.AppendLine("Result=Success");
                        string a = frm.lrcf.Lyric + "";

                        if (a == null || a.Trim() == "") sb.Append("Message=No Lyrics");
                        else
                        {
                            if (!(Position < 0))
                            {
                                //Null Reference 방지
                                Lyrics.LyricParser lc = new Lyrics.LyricParser(a);
                                lc.Refresh(Position);
                                a = lc.CurrentLyrics;
                            }
                            else if (!IncTime) try { a = GetLyricsNoTime(a).Trim(); } catch { }

                            if (!(Position < 0)) sb.AppendLine("Position=" + (Position * 1000));
                            sb.Append("Lyrics=").Append((a != null ? a.Trim().Replace("\r\n", "\\n") : null));
                        }
                    }
                    else
                    {
                        if (Hash == null || Hash.Trim() == "") Hash = Lyrics.GetLyrics.GenerateMusicHash(MusicPath);

                        Lyrics.LyricsCache Lyricscache = new Lyrics.LyricsCache();
                        string a = Lyricscache.GetLyrics(Hash);
                        if (a == null || a.Trim() == "")
                        {
                            Lyrics.frmLyricsSearch frmlrcSearch = new Lyrics.frmLyricsSearch();
                            frmlrcSearch.GetLyricsFailed += (Lyrics.LyricException ex) => { SetMessage(GetKind.Lyrics, "Result=Fail\nMessage=" + ex.Message); };
                            frmlrcSearch.GetLyricsComplete += (Lyrics.LyricAlsong[] Lyrics) =>
                            {
                                StringBuilder sb1 = new StringBuilder();
                                try
                                {
                                    string b = Lyrics == null || Lyrics.Length == 0 ? null : Lyrics[0].Lyric;

                                    sb1.AppendLine("Result=Success");
                                    if (b == null) sb1.Append("Message=No Lyrics");
                                    else
                                    {
                                        if (!(Position < 0))
                                        {
                                            //Null Reference 방지
                                            Lyrics.LyricParser lc = new Lyrics.LyricParser(b);
                                            lc.Refresh(Position);
                                            b = lc.CurrentLyrics;
                                        }
                                        else if (!IncTime) try { b = GetLyricsNoTime(b).Trim(); } catch { }

                                        if (!(Position < 0)) sb1.AppendLine("Position=" + (Position * 1000));
                                        sb1.Append("Lyrics=").Append((b != null ? b.Trim().Replace("\r\n", "\\n") : null));
                                    }
                                    SetMessage(GetKind.Lyrics, sb1.ToString());
                                }
                                catch(Exception ex)
                                {
                                    sb1.Capacity = 0;
                                    sb1.AppendLine("Result=Fail\r\nMessage=").Append(ex.Message);
                                    SetMessage(GetKind.Lyrics, sb1.ToString());
                                }
                            };
                            frmlrcSearch.FindLyrics(Hash, true, null);
                            sb.Append("Result=Wait");
                        }
                        else
                        {
                            if (!(Position < 0))
                            {
                                //Null Reference 방지
                                Lyrics.LyricParser lc = new Lyrics.LyricParser(a);
                                lc.Refresh(Position);
                                a = lc.CurrentLyrics;
                            }
                            else if (!IncTime) try { a = GetLyricsNoTime(a).Trim(); } catch { }

                            sb.AppendLine("Result=Success");
                            if (!(Position < 0)) sb.AppendLine("Position=" + (Position * 1000));
                            sb.Append("Lyrics=").Append((a != null ? a.Trim().Replace("\r\n", "\\n") : null));
                        }
                    }
                }
                #endregion
                info += sb.ToString();
            }
            catch(Exception ex) { info += ex.Message; }
        }

        private void GetAlbumArt(string MusicPath, out byte[] AlbumArt, out string MIMEType, int index)
        {
            string MIME = null;
            byte[] Data = null;
            if (MusicPath == null ? true : MusicPath.Trim() == "")
            {
                Data = index < 1 ? info_Music.AlbumArt : piclist[index].Data.Data;
                MIME = index < 0 ? piclist[index].MimeType : info_Music.AlbumArtMIME;
            }
            else
            {
                Tag tag = GetTag(MusicPath);
                IPicture[] piclist = tag.Pictures;
                if (piclist != null && piclist.Length > 0)
                {
                    Data = index < 1 ? piclist[0].Data.Data : piclist[index].Data.Data;
                    MIME = index < 0 ? piclist[0].MimeType : piclist[index].MimeType;
                }
            }
            AlbumArt = Data;
            MIMEType = MIME;
        }
        #endregion


        PlayInformation info_play = new PlayInformation();
        public PlayInformation GetPlayInformation()
        {
            if (frm.Helper != null)
            {
                info_play.TotalTime = frm.Helper.TotalPosition;
                info_play.CurrentTime = frm.Helper.CurrentPosition;
                switch (frm.Helper.PlayStatus)
                {
                    case HSAudioHelper.PlayingStatus.Play: info_play.Status = PlayStatus.Play; break;
                    case HSAudioHelper.PlayingStatus.Pause: info_play.Status = PlayStatus.Pause; break;
                    case HSAudioHelper.PlayingStatus.Ready:
                    case HSAudioHelper.PlayingStatus.Stop: info_play.Status = PlayStatus.Stop; break;
                    default: info_play.Status = PlayStatus.Unknown; break;
                }
                info_play.EnableControl = frm.btn재생.Enabled;
                return info_play;
            }
            return null;
        }

        public void SetEvent(string kind, params object[] Data)
        {
            try { if (sender_network != null) sender_network.setEvent(kind, Data); } catch { }
        }
        public void SetMessage(GetKind kind, string Message)
        {
            try { if (sender_network != null) sender_network.setMessage(kind, Message); } catch { }
            //try { if (sender_memory != null) sender_memory.setMessage(kind, Message); } catch { }
        }

        private string Sender_CommandReceived(GetKind kind, string[] Command)
        {
            try
            {
                if (frm.InvokeRequired) return (string)frm.Invoke(new Action(Sender_CommandReceived_Onvoke), kind, Command);
                else return Sender_CommandReceived_Onvoke(kind, Command);
            }
            catch(Exception ex) { return ex.Message; }
        }
        string Sender_CommandReceived_Onvoke(GetKind kind, string[] Commands)
        {
            string reason = null;
            try
            {
                #region PlayInfo
                if (kind == GetKind.PlayInfo)
                {
                    string[] spl = Commands[0].Split('=');
                    if (spl[0].ToUpper() == "Volume".ToUpper()) frm.Helper.Volume = Convert.ToSingle(spl[1]) / 100;
                    else if (spl[0].ToUpper() == "Position".ToUpper()) frm.Helper.CurrentPosition = Convert.ToUInt32(spl[1]);
                    else if (spl[0].ToUpper() == "PlayCommand".ToUpper() || spl[0].ToUpper() == "PlayStatus".ToUpper())
                    {
                        if (spl.Length == 1) return null;
                        else if (spl[1].ToUpper() == "Next".ToUpper()) { frm.NextMusic(); }
                        else if (spl[1].ToUpper() == "Previous".ToUpper() || spl[1].ToUpper() == "Prev".ToUpper()) frm.BeforeMusic();
                        else if (spl[1].ToUpper() == "Play".ToUpper()) frm.Play();
                        else if (spl[1].ToUpper() == "Pause".ToUpper()) frm.Pause();
                        else if (spl[1].ToUpper() == "Stop".ToUpper()) frm.Stop();
                        else reason = spl[1];
                        Thread.Sleep(20);
                    }
                    else reason = spl[0];
                }
                #endregion
                #region PlayList
                else if (kind == GetKind.PlayList)
                {
                    for (int i = 0; i < Commands.Length; i++)
                    {
                        string[] spl = new string[2];
                        if (Commands[i].IndexOf("=") > -1)
                        {
                            spl[0] = Commands[i].Remove(Commands[i].IndexOf("=")).Trim().ToLower();
                            spl[1] = Commands[i].Substring(Commands[i].IndexOf("=") + 1).TrimEnd();
                        }
                        else continue;//spl[0] = info[i];

                        if(spl[0] == "Mode".ToLower())
                        {
                            Command_PlayListMode mode = (Command_PlayListMode)Enum.Parse(typeof(Command_PlayListMode), spl[1]);
                            if (mode == Command_PlayListMode.Play) continue;
                            else break;
                        }
                        else if(spl[0] == "Data".ToLower())
                            frm.재생ToolStripMenuItem_Click(frm.listView1.Items[Convert.ToInt32(spl[1])], null);
                    }
                }
                #endregion
                return reason;
            }
            catch (Exception ex) { return ex.Message; }
            finally { Thread.Sleep(20); }
        }

        private Exception Sender_network_RestartReceived(RestartKind Kind)
        {
            if(Kind == RestartKind.Socket_Byte)
            {
                try
                {
                    sender_network.Dispose_Byte();
                    sender_network.Init_soc_byte();
                    return null;
                }
                catch(Exception ex) { return ex; }
            }
            if (Kind == RestartKind.Socket_Message)
            {
                try
                {
                    sender_network.Dispose_Message();
                    sender_network.Init_soc();
                    return null;
                }
                catch (Exception ex) { return ex; }
            }
            else if(Kind == RestartKind.Socket_All)
            {
                Exception ex1 = null;
                sender_network.Dispose();
                try { sender_network.Init_soc(); } catch (Exception ex) { ex1 = ex; }
                try { sender_network.Init_soc_byte(); } catch (Exception ex) { ex1 = ex; }
                return ex1;
            }
            else if(Kind == RestartKind.WebServer)
            {
                try
                {
                    sender_webserver.Dispose();
                    sender_webserver.Init();
                    return null;
                }
                catch (Exception ex) { return ex; }
            }
            return new Exception("Wrong restart method.");
        }


        internal Tag Tag;
        internal IPicture[] piclist;
        public Tag GetTag(string MusicPath)
        {
            try
            {
                int Error = 0;
                FileStream MainStream = null;
                ReadFile:
                try { MainStream = new FileStream(MusicPath, FileMode.Open, FileAccess.Read, FileShare.Read); }
                catch
                {
                    Thread.Sleep(1000);
                    //MainStream = new FileStream(fh.MusicPath, FileMode.Open, FileAccess.Read, FileShare.Read);
                    Error++;
                    if (Error < 5) goto ReadFile;
                }

                TagLib.File TAGFILE = TagLib.File.Create(new StreamFileAbstraction(MusicPath, MainStream, MainStream));
                MainStream.Close();
                Tag = TAGFILE.Tag;
                return Tag;
            }
            catch
            {
                return null;
            }
        }

        public string GetLyricsNoTime(string Lyrics)
        {
            string[] li = HS_CSharpUtility.Utility.StringUtility.ConvertStringToArray(Lyrics, "\r\n");
            System.Text.StringBuilder sb = new System.Text.StringBuilder(li[0].Substring(li[0].IndexOf("]") + 1));
            for (long i = 1; i < li.LongLength; i++)
            {
                try { li[i] = li[i].Substring(li[i].IndexOf("]") + 1); }
                catch { }
                finally { sb.Append("\r\n" + li[i]); }
            }
            string l = sb.ToString();
            return l;
        }
    }
}
