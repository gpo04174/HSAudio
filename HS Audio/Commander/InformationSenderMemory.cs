﻿using HS_Audio.Commander.Sender;
using HS_Audio.Commander.Struct;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading;
using TagLib;

namespace HS_Audio.Commander
{
    class InformationSenderMemory
    {
        static MemorySender sender_memory;
        WebSocketSender sender_network;

        HSAudioHelper Helper;
        frmMain frm;

        MusicInformation info_music = new MusicInformation();

        Thread th;
        ThreadStart ts;
        public InformationSenderMemory(frmMain frm)
        {
            this.Helper = frm.Helper;
            this.frm = frm;
            frm.Helper.MusicChanged += Helper_MusicChanged;
            if(sender_memory == null) sender_memory = new MemorySender();
            ts = new ThreadStart(th_Loop);
        }

        private void Helper_MusicChanged(HSAudioHelper Helper, int index, bool Error)
        {
            ThreadPool.QueueUserWorkItem(new WaitCallback((object state)=>
            {
                GetTag(Helper.MusicPath);
                Update();
                sender_memory.SetAlbumArt(info_music.AlbumArt);
            }));
        }

        public bool AutoUpdate { get; set; }
        int _AutoUpdateInterval = 25;
        public int AutoUpdateInterval { get { return _AutoUpdateInterval; } set { _AutoUpdateInterval = value; } }

        public void Update()
        {
            if(sender_memory != null)
            {
                PlayInformation info_play = GetPlayInformation();
                if (info_play == null || info_music == null) { sender_memory.SetMusicInfo(""); return; }

                StringBuilder sb = new StringBuilder();
                sb.Append("TotalTime="); sb.Append(info_play.TotalTime.ToString()); sb.Append("\n");
                sb.Append("CurrentTime="); sb.Append(info_play.CurrentTime.ToString()); sb.Append("\n");
                sb.Append("Volume="); sb.Append(info_play.Volume.ToString()); sb.Append("\n");
                sb.Append("Status="); sb.Append((int)info_play.Status); sb.Append("\n");
                sb.Append("EnableControl="); sb.Append(info_play.EnableControl.ToString()); sb.Append("\n");
                if (info_music.Title != null)
                {
                    //if (info.Title == null && Helper.MusicPath != null) GetTag(Helper.MusicPath);
                    sb.Append("Title="); if (info_music.Title != null) sb.Append(info_music.Title.Replace("\n", "\\n")); sb.Append("\n");
                    sb.Append("Artist="); if (info_music.Artist != null) sb.Append(info_music.Artist.Replace("\n", "\\n")); sb.Append("\n");
                    sb.Append("Album="); if (info_music.Album != null) sb.Append(info_music.Album.Replace("\n", "\\n")); sb.Append("\n");
                }
                sender_memory.SetMusicInfo(sb.ToString());
            }
        }

        PlayInformation info_play = new PlayInformation();
        public PlayInformation GetPlayInformation()
        {
            if (frm.Helper != null)
            {
                info_play.TotalTime = Helper.TotalPosition;
                info_play.CurrentTime = Helper.CurrentPosition;
                info_play.Volume = Helper.Volume;
                switch (Helper.PlayStatus)
                {
                    case HSAudioHelper.PlayingStatus.Play: info_play.Status = PlayStatus.Play; break;
                    case HSAudioHelper.PlayingStatus.Pause: info_play.Status = PlayStatus.Pause; break;
                    case HSAudioHelper.PlayingStatus.Ready:
                    case HSAudioHelper.PlayingStatus.Stop: info_play.Status = PlayStatus.Stop; break;
                    default: info_play.Status = PlayStatus.Unknown; break;
                }
                info_play.EnableControl = frm.btn재생.Enabled;
                return info_play;
            }
            return null;
        }


        IPicture[] piclist;
        TagLib.Tag tag;
        TagLib.File TAGFILE;
        void GetTag(string MusicPath)
        {
            int Error = 0;
            FileStream MainStream = null;
            try
            {
                ReadFile:
                try { MainStream = new FileStream(MusicPath, FileMode.Open, FileAccess.Read, FileShare.Read); }
                catch
                {
                    Thread.Sleep(1000);
                    //MainStream = new FileStream(fh.MusicPath, FileMode.Open, FileAccess.Read, FileShare.Read);
                    Error++;
                    if (Error < 10) goto ReadFile;
                }

                if (TAGFILE != null) TAGFILE.Dispose();
                TAGFILE = TagLib.File.Create(new StreamFileAbstraction(MusicPath, MainStream, MainStream));
                MainStream.Close();
                tag = TAGFILE.Tag;

                if (tag != null)
                {
                    info_music.Title = (tag.Title == null) ? Path.GetFileName(MusicPath) : tag.Title;
                    info_music.Artist = (tag.Artists != null && tag.Artists.Length > 0) ? tag.Artists[0] : null;
                    info_music.Album = tag.Album;

                    piclist = tag.Pictures;
                    if (tag.Pictures.Length > 0) info_music.AlbumArt = piclist[0].Data.Data;
                }
                else
                {
                    info_music.Album = null;
                    info_music.Artist = null;
                    info_music.Title = null;
                    info_music.AlbumArt = null;
                }
            }
            catch
            {
                info_music.Album = null;
                info_music.Artist = null;
                info_music.Title = null;
                info_music.AlbumArt = null;
            }
        }

        public void Start()
        {
            if (!AutoUpdate) throw new Exception("Set the true to AutoUpdate");
            Stop();
            th = new Thread(ts);
            th.Start();
        }
        public void Stop()
        {
            if (th != null) th.Abort();
            th = null;
        }

        public void Dispose()
        {
            if(th != null)th.Abort();
            th = null;
            ts = null;
            TAGFILE.Dispose();
            sender_memory.SetMusicInfo("");
        }

        void th_Loop()
        {
            try
            {
                while (true)
                {
                    Update();
                    Thread.Sleep(25);
                }
            }
            catch { }
        }
    }
}
