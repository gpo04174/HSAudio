﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HS_Audio.Commander
{
    enum PlayStatus { Unknown = -1, Stop = 0,  Play = 1, Pause = 2 }
    class MusicInformation
    {
        public MusicInformation() { }
        public MusicInformation(MusicInformation clone)
        {
            Path = clone.Path;
            Title = clone.Title;
            Album = clone.Album;
            Artist = clone.Artist;
            AlbumArtCount = clone.AlbumArtCount;
            AlbumArtMIME = clone.AlbumArtMIME;
            AlbumArt = clone.AlbumArt;
        }

        string _Path;
        string _Title;
        string _Album;
        string _Artist;
        int _AlbumArtCount;
        string _AlbumArtMIME;
        byte[] _AlbumArt;

        public string Path { get { return _Path; } set { _Path = value; } }
        public string Title { get { return _Title; } set { _Title = value; } }
        public string Album { get { return _Album; } set { _Album = value; } }
        public string Artist { get { return _Artist; } set { _Artist = value; } }
        public int AlbumArtCount { get { return _AlbumArtCount; } set { _AlbumArtCount = value; } }
        public string AlbumArtMIME { get { return _AlbumArtMIME; } set { _AlbumArtMIME = value; } }
        public byte[] AlbumArt { get { return _AlbumArt; } set { _AlbumArt = value; } }

        public override bool Equals(object obj)
        {
            MusicInformation mi = obj as MusicInformation;
            if (mi == null) return false;
            else return Title == mi.Title && Album == mi.Album && Artist == mi.Artist && Path == mi.Path;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}
