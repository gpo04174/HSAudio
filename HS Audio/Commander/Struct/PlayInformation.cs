﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HS_Audio.Commander.Struct
{
    class PlayInformation
    {
        long _TotalTime;
        long _CurrentTime;
        PlayStatus _Status;
        bool _EnableControl;
        public long TotalTime { get { return _TotalTime; } set { _TotalTime = value; } }
        public long CurrentTime { get { return _CurrentTime; } set { _CurrentTime = value; } }
        public PlayStatus Status { get { return _Status; } set { _Status = value; } }
        public bool EnableControl { get { return _EnableControl; } set { _EnableControl = value; } }
        public float Volume { get; set; }
    }
}
