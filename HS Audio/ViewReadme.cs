﻿using System;
using System.ComponentModel;

using System.Windows.Forms;

namespace HS_Audio
{
    public partial class ViewReadme : Form
    {
        public ViewReadme()
        {
            InitializeComponent();
            txtReadme.Rtf = global::HS_Audio.Properties.Resources.Readme_RTF;
            this.Text = string.Format("{0} {1} 변경사항",global::HS_Audio.Properties.Resources.PlayerName,global::HS_Audio.Properties.Resources.PlayerVersion);
            saveFileDialog1.Title = string.Format("{0} {1} 변경사항 저장", global::HS_Audio.Properties.Resources.PlayerName, global::HS_Audio.Properties.Resources.PlayerVersion);
            saveFileDialog1.FileName = string.Format("{0} {1} 변경사항", global::HS_Audio.Properties.Resources.PlayerName, global::HS_Audio.Properties.Resources.PlayerVersion);
        }

        private void 자동줄바꿈WToolStripMenuItem_CheckedChanged(object sender, EventArgs e)
        {
            txtReadme.WordWrap = 자동줄바꿈WToolStripMenuItem.Checked;
        }

        private void 다른이름으로변경사항저장ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            saveFileDialog1.ShowDialog();
        }

        private void 끝내기ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        float i = 0.02f;
        private void timer1_Tick(object sender, EventArgs e)
        {
            if (i > 0.98f) { timer1.Stop(); }
            else{ i=i+0.01f; this.Opacity = i; }
        }

        private void ViewReadme_Load(object sender, EventArgs e)
        {
            timer1.Start();
        }

        bool FormClose;
        bool CloseForm;
        private void ViewReadme_FormClosing(object sender, FormClosingEventArgs e)
        {
            timer1.Stop();
            if (!FormClose) e.Cancel = !FormClose;
            else e.Cancel = false;
            if (!FormClose) timer2.Start();
            //if (CloseForm) e.Cancel = false;
        }

        private void timer2_Tick(object sender, EventArgs e)
        {
            if (i < 0.02f) { timer2.Stop(); FormClose = true; this.Close();}
            else { i = i - 0.01f; this.Opacity = i; }
        }

        private void saveFileDialog1_FileOk(object sender, CancelEventArgs e)
        {
            if (System.IO.Path.GetExtension(saveFileDialog1.FileName).ToLower() == ".rtf")
                System.IO.File.WriteAllText(saveFileDialog1.FileName, global::HS_Audio.Properties.Resources.Readme_RTF);
            else System.IO.File.WriteAllText(saveFileDialog1.FileName,HS_Audio.Properties.Resources.Readme);
        }
    }
}
