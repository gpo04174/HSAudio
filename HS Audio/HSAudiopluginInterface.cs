﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HS_Audio.Plugin
{
    public abstract class HSInterfaceForm
    {
        public Dictionary<string, string> Setting { get;set; }

        public bool IsClosing;
    }
    //[Serializable]
    public interface IHSAudioPlugin: IDisposable
    { 
        //internal IHSAudioPlugin();
        //internal IHSAudioPlugin(string Name) { }
        Dictionary<string, string> Setting { get; set; }
        string Name { get; set; }
        void NewCreate();
        void Show();
        //void Dispose();
        void Hide();
        bool IsEnabled { get;}
        bool Closing { get; set; }
        bool IsDisposed { get; }
        HS_Audio.HSAudioHelper Helper{ get; set; }
        System.Drawing.Image Icon { get; }
        IHSAudioPluginHost Host { get; set; }
        HS_Audio.Languge.LocaleManager.LocaleChangesEventhandler LocaleChanged{ get; set; }
    }
    public interface IHSAudioPluginHost
    {
        bool Register(IHSAudioPlugin HSi);
        void Command(CommandStatus cmd, int index);
        void Command(CommandStatus cmd);
    }
    public enum CommandStatus{Play, Pause, Stop, Next};
}
