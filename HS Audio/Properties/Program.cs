﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Threading;
using System.Diagnostics;
using System.Text;
using System.IO;
using HS_Audio;
using ICSharpCode.SharpZipLib;
using ICSharpCode.SharpZipLib.Zip;
using Microsoft.Win32;
using System.Runtime.InteropServices;
using HS_CSharpUtility.Extension;
using HS_Audio.LIBRARY.Windows_API;

namespace HS_Audio
{
    static class Program
    {
        public static bool DisposeForm = false;
        public static string Playlist = "#EXTM3U";
        public static Dictionary<string, string> Project = new Dictionary<string,string>();

        public static string Version{get{return Application.ProductVersion.ToString();}}//"2.18.3.1";
        public static HS_Audio.Languge.LocaleManager localeManager = new Languge.LocaleManager();
        /// <summary>
        /// 해당 응용 프로그램의 주 진입점입니다.
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {
            #region 파라메타 등록

            #region -addext
            if (args.Length > 0 && args[0].ToLower() == "-addext")
            {
                if (args.Length < 5)
                {
                    ThreadPool.QueueUserWorkItem(new WaitCallback(Close));
                    //MessageBox(IntPtr.Zero, "파라메터 오류", "홍식이의 파일 확장자 등록", 0); 
                    MessageBox.Show("파일 확장자 등록 파라메터 오류", "HS 플레이어 파일 확장자 등록", 0);

                    return;
                }
                int index = 0;
                if (args[0] == Process.GetCurrentProcess().MainModule.FileName) index = 1;
                try
                {
                    if (args[index].ToLower() == "-addext")
                    {
                        HS_Library.HSConsole.NewConsole();
                        Console.Title = "HS™ Player extension register... [" + Process.GetCurrentProcess().Id + "]";
                        //HS_Library.HSConsole.SetInputAvailable();

                        int error = 0;
                        string ProgramName = args[index + 3];
                        Console.WriteLine("mp3 확장자 등록중...\n");
                        try { FileAssociation.Associate(".mp3", "HS.Audio.MP3", ProgramName + " mp3 파일", null, args[index + 1], args[index + 2]); }
                        catch { error++; Console.WriteLine("mp3 확장자 등록실패!!"); }

                        Console.WriteLine("m3u 확장자 등록중...\n");
                        try { FileAssociation.Associate(".m3u", "HS.Audio.M3U", ProgramName + " 재생목록 파일", null, args[index + 1], args[index + 2]); }
                        catch { error++; Console.WriteLine("m3u 확장자 등록실패!!"); }

                        Console.WriteLine("wav 확장자 등록중...\n");
                        try { FileAssociation.Associate(".wav", "HS.Audio.WAV", ProgramName + " 웨이브 파일", null, args[index + 1], args[index + 2]); }
                        catch { error++; Console.WriteLine("wav 확장자 등록실패!!"); }

                        Console.WriteLine("wma 확장자 등록중...\n");
                        try { FileAssociation.Associate(".wma", "HS.Audio.WMA", ProgramName + " WMA 파일", null, args[index + 1], args[index + 2]); }
                        catch { error++; Console.WriteLine("wma 확장자 등록실패!!"); }

                        Console.WriteLine("ogg 확장자 등록중...\n");
                        try { FileAssociation.Associate(".ogg", "HS.Audio.WMA", ProgramName + " OGG 파일", null, args[index + 1], args[index + 2]); }
                        catch { error++; Console.WriteLine("ogg 확장자 등록실패!!"); }

                        Console.WriteLine("flac 확장자 등록중...\n");
                        try { FileAssociation.Associate(".flac", "HS.Audio.FLAC", ProgramName + " FLAC 파일", null, args[index + 1], args[index + 2]); }
                        catch { error++; Console.WriteLine("flac 확장자 등록실패!!"); }

                        Console.WriteLine("mid 확장자 등록중...\n");
                        try { FileAssociation.Associate(".mid", "HS.Audio.MID", ProgramName + " Standard MIDI 파일", null, args[index + 1], args[index + 2]); }
                        catch { error++; Console.WriteLine("mid 확장자 등록실패!!"); }

                        Console.WriteLine("aif 확장자 등록중...");
                        try { FileAssociation.Associate(".aif", "HS.Audio.AIFF", ProgramName + " AIFF파일", null, args[index + 1], args[index + 2]); }
                        catch { error++; Console.WriteLine("aif 확장자 등록실패!!"); }

                        Console.WriteLine("aiff 확장자 등록중...");
                        try { FileAssociation.Associate(".aiff", "HS.Audio.AIFF", ProgramName + " AIFF파일", null, args[index + 1], args[index + 2]); }
                        catch { error++; Console.WriteLine("aiff 확장자 등록실패!!"); }

                        Console.WriteLine("\nfsb 확장자 등록중...");
                        try { FileAssociation.Associate(".fsb", "HS.Audio.FSB", ProgramName + " FMOD Sound Bank 파일", null, args[index + 1], args[index + 2]); }
                        catch { error++; Console.WriteLine("fsb 확장자 등록실패!!"); }

                        Console.WriteLine("m3u8 확장자 등록중...\n");
                        try { FileAssociation.Associate(".m3u8", "HS.Audio.M3U8", "조장찡 플레이어 UTF-8 로 인코딩 된 재생목록 파일", null, args[index + 1], args[index + 2]); }
                        catch { error++; Console.WriteLine("m3u8 확장자 등록실패!!"); }

                        Console.WriteLine("폴더 윈도우 탐색기 오른쪽 마우스에 등록중...");
                        RegistryKey reg = Registry.LocalMachine.CreateSubKey(@"SOFTWARE\Classes\Directory\shell");
                        reg.CreateSubKey("HS.Audio.Add").SetValue("", args[index + 2]);
                        //reg.CreateSubKey("HS.Audio.Add\\command").SetValue("", "\""+Process.GetCurrentProcess().MainModule.FileName+ "\" /add \"%1\"");
                        reg.CreateSubKey("HS.Audio.Add\\command").SetValue("", "\"" + Process.GetCurrentProcess().MainModule.FileName + "\" \"%1\"");
                        //MessageBox.Show(reg.CreateSubKey("HS.Audio.Add\\command").GetValue("").ToString());

                        ThreadPool.QueueUserWorkItem(new WaitCallback(Close));
                        if (error < 1) MessageBox.Show("기본 음악 프로그램으로 등록 성공!", args[index + 3], 0);//MessageBox(IntPtr.Zero, "기본 음악 프로그램으로 등록 성공!", args[index + 3], 0);
                        else MessageBox.Show("기본 음악 프로그램으로 일부만 등록 성공!", args[index + 3], 0);//MessageBox(IntPtr.Zero, "기본 음악 프로그램으로 일부만 등록 성공!", args[index + 3], 0);
                        return;
                    }
                    else if (args[0].ToLower() == "-removeext")
                    {

                    }
                    else
                    {
                        ThreadPool.QueueUserWorkItem(new WaitCallback(Close));
                        //MessageBox(IntPtr.Zero, "파라메터 오류", "홍식이의 파일 확장자 등록", 0);
                        MessageBox.Show("파일 확장자 등록 파라메터 오류", "HS 플레이어 파일 확장자 등록", 0);
                        return;
                    }
                }
                catch (Exception ex)
                {
                    ThreadPool.QueueUserWorkItem(new WaitCallback(Close));
                    LoggingUtility.Logging(ex);
                    //MessageBox_Native(IntPtr.Zero, "기본 프로그램으로 등록 실패!", args[index + 3], 0);
                    MessageBox.Show("기본 프로그램으로 등록 실패!", args[index + 3], 0);
                    return;
                }
            }
            #endregion

            else
            {
                #region Wait
                long index = -1;
                HS_CSharpUtility.Utility.ArrayUtility.IsArrayItem(args, "-Wait", out index, true);
                if (index > -1)
                    try
                    {
                        string[] spl = args[index + 1].Split('=');
                        int PID = Convert.ToInt32(spl[1]);
                        while (Process.GetProcessById(PID) != null) { Thread.Sleep(20); }
                    }
                    catch { }
                #endregion

                #region -Delay
                index = -1;
                HS_CSharpUtility.Utility.ArrayUtility.IsArrayItem(args, "-Delay", out index, true);
                if(index > -1) try { Thread.Sleep(Convert.ToInt32(args[index + 1])); } catch { }
                #endregion

                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);
                if(args.Length >= 3 && args[0].ToLower() == "--killprocess")
                {
                    try
                    {
                        Process p = null;
                        if (args[1].ToLower() == "-pid") try { p = System.Diagnostics.Process.GetProcessById(Convert.ToInt32(args[2])); p.Kill(); MessageBox.Show("Kill Process!!", "HS Player Process Killer", 0); }
                            catch (Exception ex)
                            {
                                if (p != null)
                                {
                                    Console.WriteLine("Kill process error!! (PID: " + p.Id + ", Name: " + p.ProcessName + ")\n" + ex.Message);
                                    ThreadPool.QueueUserWorkItem(new WaitCallback(Close));
                                    MessageBox.Show("Kill process error!! (PID: " + p.Id + ", Name: " + p.ProcessName + ")\n\n" + ex.Message, "HS Player Process Killer", 0);
                                }
                            }
                        else if (args[1].ToLower() == "-name")
                        {
                            Process[] p1 = System.Diagnostics.Process.GetProcessesByName(args[2]);
                            if (p1 == null) return;
                            for (int i = 0; i < p1.Length; i++) try { p1[i].Kill(); }
                                catch (Exception ex)
                                {
                                    Console.WriteLine("Kill process error!! (PID: " + p1[i].Id + ", Name: " + p1[i].ProcessName + ")\n" + ex.Message);
                                    ThreadPool.QueueUserWorkItem(new WaitCallback(Close));
                                    MessageBox.Show("Kill process error!! (PID: " + p1[i].Id + ", Name: " + p1[i].ProcessName + ")\n\n" + ex.Message, "HS Player Process Killer", 0);
                                }
                        }
                    }
                    catch(Exception ex) 
					{
						Console.WriteLine(ex.Message); 
						ThreadPool.QueueUserWorkItem(new WaitCallback(Close));
						MessageBox.Show("Error!!n\\n"+ex.Message, "HS Player Process Killer", 0);
					}
                    return;
                }

                if (args.Length > 0 && args[0].ToLower() == "--updateprogram")
                {
                    if(args.Length < 2)
                    {
                        ThreadPool.QueueUserWorkItem(new WaitCallback(Close));
                        //MessageBox(IntPtr.Zero, "파라메터 ", "홍식이의 파일 확장자 등록", 0);
                        MessageBox.Show("업데이트 파라메터 오류\n\nUpdate parameter error", "HS 플레이어 업데이터", 0);
                    }
                    else
                    {
                        string OriginFile = args[1];
                        string DescPath = args[2];

                        //int index = DescPath.LastIndexOf(".");
                        //DescPath 오류= DescPath.Remove(index, DescPath.Length - DescPath.Length);
                        //File.Copy(DescPath, DescPath + (version == null ? null : " v" + version) + ".exe");

                        if (args.Length > 5 && args[4].ToLower() == "--codepage")
                            try { ZipConstants.DefaultCodePage = Convert.ToInt32(args[6]); } catch { Console.WriteLine("Wrong code page!!"); }

                        if (args.Length > 3&&args[3].ToLower() == "-type")
                        {
                            if (args[4].ToLower() == ".zip")
                            {
                                ZipFile zip;
                                string zippath = args[1];
                                //FileStream fs = new FileStream(zippath, FileMode.Open);
                                try
                                {
                                    DirectoryInfo dir = new DirectoryInfo(DescPath);
                                    FileInfo[] fi = dir.GetFiles();
                                    zip = new ZipFile(zippath);
                                    for (int i = 0; i < zip.Count; i++)
                                    {
                                        if (zip[i].IsFile)
                                        {
                                            if (zip[i].Name.Substring(zip[i].Name.LastIndexOf(".")) == ".exe")
                                            {
                                                string version = null; try { version = FileVersionInfo.GetVersionInfo(DescPath).ProductVersion; } catch { }
                                                if (FindFile(fi, zip[i].Name)!=null)
                                                {
                                                    string ver = null; try { ver = FileVersionInfo.GetVersionInfo(DescPath).ProductVersion; } catch { }

                                                    int index1 = DescPath.LastIndexOf(".");
                                                    DescPath = DescPath.Remove(index1);

                                                    bool Succ = HSMoveFileEx.Move(DescPath + ".exe", DescPath + (version == null ? null : " v" + version) + ".exe");
                                                    if (!Succ) { Console.WriteLine("Fail to extract file!!"); }
                                                }
                                            }
                                            Stream ZIP = zip.GetInputStream(zip[i]);
                                            FileStream fs1 = new FileStream(zip[i].Name, FileMode.Create);
                                            HS_CSharpUtility.Utility.IOUtility.CopyStream(ZIP, fs1, 4096).Close();
                                            fs1.Close(); ZIP.Close();
                                        }
                                    }
                                }
                                catch { }
                            }
                            else if (args[4].ToLower() == ".exe")
                            {
                                string version = null; try { version = FileVersionInfo.GetVersionInfo(DescPath).ProductVersion; } catch { }
                                HSMoveFileEx.Move(DescPath + ".exe", DescPath + (version == null ? null : " v" + version) + ".exe");
                                HSMoveFileEx.Move(OriginFile, DescPath + ".exe");
                            }
                        }
                        
                        if (args.Length > 6&&args[5].ToLower() == "--params")
                        {
                            DescPath += ".exe";
                            StringBuilder sb = new StringBuilder();
                            for (int i = 6; i < args.Length; i++) sb.Append(args[i] + " ");
                            Process.Start(DescPath, sb.ToString());
                        }
                    }
                    return;
                }
            }
            #endregion
            //if(HS_CSharpUtility.Utility.ArrayUtility.IsArrayItem(args, "/64Bit", true))if (!System.IO.File.Exists(Application.StartupPath + "\\hongsic_sound_system_ex64.dll")){ DLLCopy_FMOD(true); }
            //else if (!System.IO.File.Exists(Application.StartupPath + "\\hongsic_sound_system_ex.dll")){ DLLCopy_FMOD(false); }

            //64비트
            if (IntPtr.Size == 8){ if (!System.IO.File.Exists(Application.StartupPath + "\\hongsic_sound_system_ex.dll")) DLLCopy_SoundEngin(true);}
            //32비트
            //else if (IntPtr.Size == 4)
            else if (!System.IO.File.Exists(Application.StartupPath + "\\hongsic_sound_system_ex.dll")) DLLCopy_SoundEngin(false);
            /*
#if WIN64
            if (!System.IO.File.Exists(Application.StartupPath + "\\hongsic_sound_system_ex64.dll")){ DLLCopy_FMOD(true); }
#else
            if (!System.IO.File.Exists(Application.StartupPath + "\\hongsic_sound_system_ex.dll")){ DLLCopy_FMOD(false); }
#endif*/

            {
#if DEBUG
                UpdaterSetting.Add("Server", "http://192.168.2.10:81");
#else
                UpdaterSetting.Add("Server", "http://parkhongsic.iptime.org:81;http://112.76.42.174;http://parkhongsic.goanygate.com:81");
#endif
                //a.Add("Port", numericUpDown1.Value.ToString());
                UpdaterSetting.Add("AdvancedOption", "False");
                UpdaterSetting.Add("ServerVersionFilePath", "/%ED%94%84%EB%A1%9C%EA%B7%B8%EB%9E%A8/C%23/%EC%A1%B0%EC%9E%A5%EC%B0%A1%20%ED%94%8C%EB%A0%88%EC%9D%B4%EC%96%B4/Version.ini");
                UpdaterSetting.Add("ServerUpdateFilePathUse", "True");
                UpdaterSetting.Add("ServerUpdateFilePath", null);
                UpdaterSetting.Add("IsAnonymous", "True");
                UpdaterSetting.Add("ID", null);
                UpdaterSetting.Add("Password", null);
                frmUpdaterSetting.UptaterSettingComplete += (Dictionary<string, string> Value) => { UpdaterSetting = Value; };
            }
            
            frmMain f =new frmMain(args);

            //ThreadPool.QueueUserWorkItem(new WaitCallback(VersionCheckerThread_1), f);
            frmUpdate fu = new frmUpdate(f);
            ThreadPool.QueueUserWorkItem(new WaitCallback(VersionCheckerThread_1), fu);

            f.Show();
            try { Application.Run(); }
            catch (Exception ex) { 
                HS_CSharpUtility.LoggingUtility.Logging(ex, "HS_Audio::Program::Main(string[] args) 의 System::Windows::Forms::Application::Run(); 에서 예외 발생!!");
                //ps.Helper.Restart();
                //button1.Text = "재생";
                Process p = System.Diagnostics.Process.GetCurrentProcess();
                //StringBuilder sb = new StringBuilder("#EXTM3U");
                //foreach (ListViewItem a in f.listView1.Items) { sb.Append("\r\n" + a.SubItems[3].Text); }

                string path = Path.GetTempFileName();
                try { File.Delete(path); File.Create(path + ".m3u").Close(); path = path + ".m3u"; }catch { }
                try { System.IO.File.WriteAllText(path, Playlist/*sb.ToString()*/); } catch { }

                string ProfilePath = Path.GetTempFileName();
                try { File.Delete(ProfilePath); File.Create(ProfilePath + ".proj").Close(); ProfilePath = ProfilePath + ".proj"; }catch { }
                //if (f.ps != null) 
                   try { File.WriteAllLines(ProfilePath, HS_CSharpUtility.Utility.EtcUtility.SaveSetting(Project/*f.ps.Setting*/)); }catch { }

                string Args = ""; 
                try { Args = string.Format("/Restart {0} {1} \"{2}\" -index {3} -Posion {4} -Profile \"{5}\"", f.NewInstance ? "/NewInstance" : "", f.Helper.PlayStatus == HSAudioHelper.PlayingStatus.Play ? "/Play" : "", path, f.CurrentItem != null ? f.CurrentItem.Index : 0, f.Helper.CurrentPosition, ProfilePath, p.StartInfo.Arguments); }catch { }
                /*
                Dictionary<string, string> a = new Dictionary<string, string>();
                foreach (System.Collections.DictionaryEntry a1 in p.StartInfo.EnvironmentVariables)
                { a.Add(a1.Key.ToString(), a1.Value.ToString());}*/

                try { f.mtx.ReleaseMutex();f.notifyIcon1.Dispose();  }catch { }
                Process.Start(p.MainModule.FileName, Args);
                if (!f.NoDelete) { try { File.Delete(f.tmpProfileSetting); } catch { } try { File.Delete(f.tmpPlaylist); } catch { } }
            }
        }
        
        public static FileInfo FindFile(FileInfo[] fi, string PathorName)
        {
            for (int i = 0; i < fi.Length; i++)
                if (fi[i].FullName == PathorName || fi[i].Name == PathorName)
                    return fi[i];
            return null;
        }
        
        public static void DLLCopy_SoundEngin(bool bit64)
        {    
            Stream ms = new MemoryStream(HS_Audio.Properties.Resources.hongsic_sound_system);
            //ICSharpCode.SharpZipLib.Zip.FastZip.ConfirmOverwriteDelegate asd = new ICSharpCode.SharpZipLib.Zip.FastZip.ConfirmOverwriteDelegate(asdad);
            ZipFile zip = new ZipFile(ms);
            Stream ZIP = null;
            //ZipInputStream str = new ZipInputStream(ms);
            //zip.ExtractZip(ms, "D:\\", ICSharpCode.SharpZipLib.Zip.FastZip.Overwrite.Always, asd, "", "", false, false);
            //ICSharpCode.SharpZipLib.Zip.ZipFile zip1 = new ICSharpCode.SharpZipLib.Zip.ZipFile(@"D:\D데이터\프로그램 개발\Microsoft Visual Studio\C#\프로그램\프로그램1\FMOD Audio\FMOD Audio\LIBRARY\FMOD\hongsic_sound_system.zip");
            // CreateFile(HS_Audio.Properties.Resources.hongsic_sound_system_dll, Application.StartupPath + "\\hongsic_sound_system.dll");
            if (!bit64)
            {
                var ent = zip.GetEntry("hongsic_sound_system_ex/hongsic_sound_system_ex.dll");
                //ent.ProcessExtraData(false);
                using (FileStream fs = new FileStream(Application.StartupPath + "\\hongsic_sound_system_ex.dll", FileMode.Append))
                {
                    ZIP = zip.GetInputStream(ent);
                    HS_CSharpUtility.Utility.IOUtility.CopyStream(ZIP, fs, 4096).Close();
                }
                //FastZip fz = new FastZip();
                //fz.ExtractZip(ms, "D:\\", ICSharpCode.SharpZipLib.Zip.FastZip.Overwrite.Always, asd, "dll", "", false, false);
            }
            else
            {
                var ent = zip.GetEntry("hongsic_sound_system_ex/hongsic_sound_system_ex64.dll");//
                using (FileStream fs = new FileStream(Application.StartupPath + "\\hongsic_sound_system_ex.dll", FileMode.Append))//\\hongsic_sound_system_ex64.dll
                {
                    ZIP = zip.GetInputStream(ent);
                    HS_CSharpUtility.Utility.IOUtility.CopyStream(ZIP, fs, 4096).Close();
                }
                //CreateFile(zip.GetEntry("hongsic_sound_system_ex/hongsic_sound_system_ex64.dll").ExtraData, Application.StartupPath + "\\hongsic_sound_system_ex64.dll");
            }
            ZIP.Dispose(); zip.Close(); ms.Dispose();
        }

        private static bool CreateFile(byte[] ByteArray, string Path)
        {
            try
            {
                System.IO.File.WriteAllBytes(Path, ByteArray);
                return true;
            }
            catch { return false; }
        }

#region a
        public struct UpdaterStruct
        {
            internal UpdaterStruct(frmUpdate frmUpdate, Dictionary<string, string> UptaterSetting, bool ShowMessage)
            { this.frmUpdate = frmUpdate; this.UptaterSetting = UptaterSetting; this.ShowMessage = ShowMessage; this.ThisValueCanChangeOnlyProgram_cs = null; }
            internal UpdaterStruct(frmUpdate frmUpdate, Dictionary<string, string> UptaterSetting, bool ShowMessage, PreUpdate ThisValueCanChangeOnlyProgram_cs)
            { this.frmUpdate = frmUpdate; this.UptaterSetting = UptaterSetting; this.ShowMessage = ShowMessage; this.ThisValueCanChangeOnlyProgram_cs = ThisValueCanChangeOnlyProgram_cs; }
            public frmUpdate frmUpdate;
            public bool ShowMessage;
            public Dictionary<string, string> UptaterSetting;

            public PreUpdate ThisValueCanChangeOnlyProgram_cs;

            public class PreUpdate
            {
                public string DownloadPath;
                public string GetVersion;
            }

        }
        /*
        internal static void VersionCheckerThread(object obj) 
        { 
            string Version;
            VersionChecker((UpdaterStruct)obj, out Version); 

        }*/

        internal static Dictionary<string, string> UpdaterSetting = new Dictionary<string, string>();
        private static void VersionCheckerThread_1(object obj) { string Version; VersionChecker((frmUpdate)obj, out Version); }
        internal static bool VersionChecker(frmUpdate main, out string Version)
        {
            string VersionFile = UpdaterSetting.ContainsKey("ServerVersionFilePath")?
                UpdaterSetting["ServerVersionFilePath"]:
                "/%ED%94%84%EB%A1%9C%EA%B7%B8%EB%9E%A8/C%23/%EC%A1%B0%EC%9E%A5%EC%B0%A1%20%ED%94%8C%EB%A0%88%EC%9D%B4%EC%96%B4/Version.ini";

            List<string> DownloadURL = new List<string>();
            DownloadURL.Add("http://112.76.42.174/home/user1634/HS%20Player/Version.ini");
            if(UpdaterSetting.ContainsKey("Server"))DownloadURL.AddRange(UpdaterSetting["Server"].Split(';'));
            for (int i = 1; i < DownloadURL.Count; i++) DownloadURL[i] += VersionFile;

            foreach (string a in DownloadURL)
            {
                string b = VersionnDownloader(a);
                if (b != null)
                {
                    Dictionary<string, string> c = HS_CSharpUtility.Utility.EtcUtility.LoadSetting(HS_CSharpUtility.Utility.StringUtility.ConvertStringToArray(b, "\r\n"));
                    Version = VersionCheckerProcess(main, c);
                    return true;
                }
            }
            Version = null;
            return false;
        }
        /*
        internal static bool VersionChecker(UpdaterStruct UpdaterStruct, out string Version)
        {
            try
            {
                string Version1;
                if (UpdaterStruct.UptaterSetting == null)
                    if (!VersionChecker(UpdaterStruct.frmUpdate, out Version1))
                    {
                        Version = Version1;
                        MessageBox.Show(string.Format("프로그램이 최신 버전이거나\n\n업데이트 서버에 연결할 수 없습니다.\n\n\n현재 버전: v{0}\n최신 버전: {1}", Application.ProductVersion, Version1 == null ? "(버전을 가져오는데 실패하였습니다.)" : "v"+Version1)
                        , "업데이트 알림", MessageBoxButtons.OK, MessageBoxIcon.Information); return false; }
                    else { Version = Version1; return true; }
                else
                {
                    Dictionary<string, string> UptaterSetting = UpdaterStruct.UptaterSetting;
                    System.Net.WebClient wc = new System.Net.WebClient();
                    string VRPath = bool.Parse(UptaterSetting["AdvancedOption"]) ? UptaterSetting["ServerVersionFilePath"] : "/%ED%94%84%EB%A1%9C%EA%B7%B8%EB%9E%A8/C%23/%EC%A1%B0%EC%9E%A5%EC%B0%A1%20%ED%94%8C%EB%A0%88%EC%9D%B4%EC%96%B4/Version.ini";
                    Dictionary<string, string> a = HS_CSharpUtility.Utility.EtcUtility.LoadSetting(
                        HS_CSharpUtility.Utility.StringUtility.ConvertStringToArray(wc.DownloadString(UptaterSetting["Server"] + VRPath), "\r\n"));
                    Version = a["Version"];
                    if (a["Version"] != Application.ProductVersion)
                    {
                        if (MessageBox.Show(string.Format("프로그램의 새 버전이 나왔습니다. ({0} v{1})\n\n새 버전을 다운로드 하시겠습니까?",  a["ProgramName"],a["Version"]), "새 버전 출시 업데이트 알림", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.Yes)
                        {
                            //WebBrowser wb = new WebBrowser();
                            //wb.Navigate(a["DownloadURL"], true); return true;
                            //WebBrowser wb = new WebBrowser();
                            if (bool.Parse(UptaterSetting["ServerUpdateFilePathUse"]))
                                System.Diagnostics.Process.Start(UptaterSetting["ServerUpdateFilePath"]);
                            else System.Diagnostics.Process.Start(a["DownloadURL"]);

                            if (MessageBox.Show("정말 프로그램을 종료 하시겠습니까?", "프로그램 종료 알림", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == System.Windows.Forms.DialogResult.Yes)
                            {
                                if (UpdaterStruct.frmUpdate.frmmain != null) { UpdaterStruct.frmUpdate.frmmain.notifyIcon1.Dispose(); }
                                System.Diagnostics.Process.GetCurrentProcess().Kill();
                            }
                            return true;
                        }
                        else { return true; }
                    }
                }
            }
            catch (Exception ex) { Version = null; }
            return false;
        }*/


        internal static string VersionnDownloader(string URL)
        {
            System.Net.WebClient wc = new System.Net.WebClient();
            try
            {
                string a = wc.DownloadString(URL);
                return a;
            }
            catch{return null;}
            finally{wc.Dispose();}
        }
        internal static string VersionCheckerProcess(frmUpdate main, Dictionary<string, string> a)
        {
            try
            {
                if (a["Version"] != Application.ProductVersion && (frmMain.fu==null || frmMain.fu.IsDisposed))
                {
                    if (MessageBox.Show(string.Format("프로그램의 새 버전이 나왔습니다. ({0} v{1})\n\n새 버전을 다운로드 하시겠습니까?", a["ProgramName"], a["Version"]), "새 버전 출시 업데이트 알림", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.Yes)
                    {
                        frmMain.fu = main;
                        main.InvokeIfNeeded(() => main.textBox1.Text = "v" + a["Version"]);

                        frmUpdate.UpdateTemp ut = new frmUpdate.UpdateTemp();
                        ut.info = a;
                        ut.Name = a["ProgramName"] + ".tmp";
                        ut.Path = System.Diagnostics.Process.GetCurrentProcess().MainModule.FileName;
                        ut.Dir = ut.Path.Substring(0, ut.Path.LastIndexOf("\\") + 1);

                        ut.Type = a["DownloadURL"].Substring(a["DownloadURL"].LastIndexOf("."));

                        main.InvokeIfNeeded(() =>
                        {
                            main.UpdateProg(ut);
                            main.ShowDialog();
                        });

                        /*
                        //WebBrowser wb = new WebBrowser();
                        //wb.Navigate(a["DownloadURL"], true); return true;
                        System.Diagnostics.Process.Start(a["DownloadURL"]);
                        if (MessageBox.Show("정말 프로그램을 종료 하시겠습니까?", "프로그램 종료 알림", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == System.Windows.Forms.DialogResult.Yes)
                        {
                            if (main != null) main.frmmain.notifyIcon1.Dispose();
                            System.Diagnostics.Process.GetCurrentProcess().Kill();
                        }*/
                    }
                }
                return a["Version"];
            }catch{return null;}
        }
#endregion


#region b

        //[DllImport("user32.dll", CharSet = CharSet.Unicode)]
        //public static extern int MessageBox(IntPtr hWnd, String text, String caption, uint type);

        static void Close(object o)
        {
            Thread.Sleep(4000);
            Process.GetCurrentProcess().Kill();
        }

        public class FileAssociation
        {
            // Associate file extension with progID, description, icon and application
            public static void Associate(string extension,
                   string progID, string description, string icon, string application, string Explorer)
            {
                Registry.ClassesRoot.CreateSubKey(extension).SetValue("", progID);
                if (progID != null && progID.Length > 0)
                {
                    using (RegistryKey key = Registry.ClassesRoot.CreateSubKey(progID))
                    {
                        if (description != null)
                            key.SetValue("", description);
                        if (icon != null)
                            key.CreateSubKey("DefaultIcon").SetValue("", ToShortPathName(icon));
                        if (application != null)
                        {
                            key.CreateSubKey(@"Shell\Open\Command").SetValue("",
                                        ToShortPathName(application) + " \"%1\"");
                            if (Explorer != null)
                            {
                                key.CreateSubKey(@"Shell\Enqueue").SetValue("", Explorer);
                                key.CreateSubKey(@"Shell\Enqueue\Command").SetValue("", ToShortPathName(application) + " /add \"%1\"");
                            }
                        }
                    }
                    using (RegistryKey key = Registry.CurrentUser.CreateSubKey(@"Software\Microsoft\Windows\CurrentVersion\Explorer\FileExts\" + extension))
                    {
                        key.SetValue("Application", ToShortPathName(application) + " \"%1\"");
                        key.CreateSubKey("OpenWithList").SetValue("a", System.IO.Path.GetFileName(application));
                        key.CreateSubKey("UserChoice").SetValue("Progid", progID);
                    }
                }
            }
        }

        // Return true if extension already associated in registry
        public static bool IsAssociated(string extension)
        {
            return (Registry.ClassesRoot.OpenSubKey(extension, false) != null);
        }

        [DllImport("Kernel32.dll")]
        public static extern uint GetShortPathName(string lpszLongPath,
            [Out] StringBuilder lpszShortPath, uint cchBuffer);

        // Return short path format of a file name
        public static string ToShortPathName(string longName)
        {
            StringBuilder s = new StringBuilder(1000);
            uint iSize = (uint)s.Capacity;
            uint iRet = GetShortPathName(longName, s, iSize);
            return s.ToString();
        }
        public class LoggingUtility
        {
            public static string ProgramPath = Path.GetDirectoryName(Process.GetCurrentProcess().MainModule.FileName);
            public static bool Logging(string LogMessage)
            {
                if (!Directory.Exists(ProgramPath + "\\Log")) Directory.CreateDirectory(ProgramPath + "\\Log");
                string Path = string.Format("{0}\\Log\\{1}-{2}-{3}.log", ProgramPath, DateTime.Today.Year, DateTime.Today.Month, DateTime.Today.Day);
                string time = DateTime.Now.ToLongTimeString();
                File.AppendAllText(Path, "==========" + time + "==========\r\n   " + LogMessage + "\r\n\r\n");
                return true;
            }
            public static bool Logging(Exception ex)
            {
                if (!Directory.Exists(ProgramPath + "\\Log")) Directory.CreateDirectory(ProgramPath + "\\Log");
                string Path = string.Format("{0}\\Log\\{1}-{2}-{3}.log", ProgramPath, DateTime.Today.Year, DateTime.Today.Month, DateTime.Today.Day);
                string time = DateTime.Now.ToLongTimeString();
                File.AppendAllText(Path, string.Format("=========={0}==========\r\n예외:   {1}\r\n└객체:   {2}\r\n└메세지: {3}\r\n{4}\r\n└메서드: {5}\r\n\r\n", time, ex.GetType(), ex.Source, ex.Message, ex.StackTrace.Substring(3).Insert(0, "└").Insert(4, "  "), ex.TargetSite.ToString()));
                return true;
            }
            public static bool LoggingT<T>(T ex)
            {
                if (!Directory.Exists(ProgramPath + "\\Log")) Directory.CreateDirectory(ProgramPath + "\\Log");
                string Path = string.Format("{0}\\Log\\{1}-{2}-{3}.log", ProgramPath, DateTime.Today.Year, DateTime.Today.Month, DateTime.Today.Day);
                string time = DateTime.Now.ToLongTimeString();
                File.AppendAllText(Path, string.Format("=========={0}==========\r\n{1}\r\n\r\n", time, ex.ToString()));
                return true;
            }
            public static bool LoggingT<T>(string Add, T ex)
            {
                if (!Directory.Exists(ProgramPath + "\\Log")) Directory.CreateDirectory(ProgramPath + "\\Log");
                string Path = string.Format("{0}\\Log\\{1}-{2}-{3}.log", ProgramPath, DateTime.Today.Year, DateTime.Today.Month, DateTime.Today.Day);
                string time = DateTime.Now.ToLongTimeString();
                File.AppendAllText(Path, string.Format("=========={0}==========\r\n{1}\r\n{2}\r\n\r\n", time, Add, ex.ToString()));
                return true;
            }
        }
#endregion
    }
}
