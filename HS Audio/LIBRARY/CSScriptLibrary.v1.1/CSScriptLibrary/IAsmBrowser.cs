﻿namespace CSScriptLibrary
{
    using System;

    internal interface IAsmBrowser : IDisposable
    {
        object CreateInstance(string typeName);
        string[] GetMembersOf(object obj);
        FastInvokeDelegate GetMethodInvoker(string methodName);
        FastInvokeDelegate GetMethodInvoker(string methodName, object[] list);
        FastInvokeDelegate GetMethodInvoker(string methodName, Type[] list);
        object Invoke(string methodName, params object[] list);
        object Invoke_Full(object obj, string methodName, params object[] list);

        bool CachingEnabled { get; set; }

        string[] ProbingDirs { get; set; }
    }
}

