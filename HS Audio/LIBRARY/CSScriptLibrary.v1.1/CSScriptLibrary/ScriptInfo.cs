﻿namespace CSScriptLibrary
{
    using csscript;
    using System;

    internal class ScriptInfo
    {
        public string fileName;
        public ParsingParams parseParams = new ParsingParams();

        public ScriptInfo(CSharpParser.ImportInfo info)
        {
            this.fileName = info.file;
            this.parseParams.AddRenameNamespaceMap(info.renaming);
            this.parseParams.preserveMain = info.preserveMain;
        }
    }
}

