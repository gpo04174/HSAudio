﻿namespace CSScriptLibrary
{
    using csscript;
    using System;
    using System.Collections;
    using System.IO;
    using System.Reflection;

    public class AsmHelper : IDisposable
    {
        private IAsmBrowser asmBrowser;
        private bool deleteOnExit;
        private bool disposed;
        private AppDomain remoteAppDomain;

        public AsmHelper(Assembly asm)
        {
            this.asmBrowser = new AsmBrowser(asm);
            this.InitProbingDirs();
        }

        public AsmHelper(string asmFile, string domainName, bool deleteOnExit)
        {
            this.deleteOnExit = deleteOnExit;
            AppDomainSetup info = new AppDomainSetup {
                ApplicationBase = Path.GetDirectoryName(asmFile),
                PrivateBinPath = AppDomain.CurrentDomain.BaseDirectory,
                ApplicationName = Path.GetFileName(Assembly.GetExecutingAssembly().Location),
                ShadowCopyFiles = "true",
                ShadowCopyDirectories = Path.GetDirectoryName(asmFile)
            };
            this.remoteAppDomain = AppDomain.CreateDomain((domainName != null) ? domainName : "", null, info);
            AsmRemoteBrowser browser = (AsmRemoteBrowser) this.remoteAppDomain.CreateInstanceFromAndUnwrap(Assembly.GetExecutingAssembly().Location, typeof(AsmRemoteBrowser).ToString());
            browser.AsmFile = asmFile;
            browser.CustomHashing = ExecuteOptions.options.customHashing;
            this.asmBrowser = browser;
            this.InitProbingDirs();
        }

        public object CreateObject(string typeName)
        {
            object obj2 = this.TryCreateObject(typeName);
            if (obj2 == null)
            {
                throw new ApplicationException(typeName + " cannot be instantiated. Make sure the type name is correct.");
            }
            return obj2;
        }

        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (this.asmBrowser != null)
                {
                    this.asmBrowser.Dispose();
                }
                this.Unload();
            }
            this.disposed = true;
        }

        ~AsmHelper()
        {
            this.Dispose(false);
        }

        public string[] GetMembersOf(object obj)
        {
            return this.asmBrowser.GetMembersOf(obj);
        }

        private void InitProbingDirs()
        {
            ArrayList list = new ArrayList();
            if (CSScript.AssemblyResolvingEnabled && CSScript.ShareHostRefAssemblies)
            {
                foreach (Assembly assembly in AppDomain.CurrentDomain.GetAssemblies())
                {
                    try
                    {
                        if (!CSSUtils.IsDynamic(assembly) && File.Exists(assembly.Location))
                        {
                            list.Add(Path.GetDirectoryName(assembly.Location));
                        }
                    }
                    catch
                    {
                    }
                }
            }
            if (CSScript.AssemblyResolvingEnabled)
            {
                foreach (string str in CSScript.GlobalSettings.SearchDirs.Split(new char[] { ';' }))
                {
                    if (str != "")
                    {
                        list.Add(Environment.ExpandEnvironmentVariables(str));
                    }
                }
            }
            this.ProbingDirs = CSScript.RemovePathDuplicates((string[]) list.ToArray(typeof(string)));
        }

        public object Invoke(string methodName, params object[] list)
        {
            if (this.disposed)
            {
                throw new ObjectDisposedException(this.ToString());
            }
            return this.asmBrowser.Invoke(methodName, list);
        }

        public object InvokeInst(object obj, string methodName, params object[] list)
        {
            if (this.disposed)
            {
                throw new ObjectDisposedException(this.ToString());
            }
            return this.asmBrowser.Invoke_Full(obj, methodName, list);
        }

        public object TryCreateObject(string typeName)
        {
            if (this.disposed)
            {
                throw new ObjectDisposedException(this.ToString());
            }
            return this.asmBrowser.CreateInstance(typeName);
        }

        private void Unload()
        {
            try
            {
                if (this.remoteAppDomain != null)
                {
                    string asmFile = ((AsmRemoteBrowser) this.asmBrowser).AsmFile;
                    AppDomain.Unload(this.remoteAppDomain);
                    this.remoteAppDomain = null;
                    if (this.deleteOnExit)
                    {
                        Utils.FileDelete(asmFile);
                    }
                }
            }
            catch
            {
            }
        }

        public bool CachingEnabled
        {
            get
            {
                return this.asmBrowser.CachingEnabled;
            }
            set
            {
                this.asmBrowser.CachingEnabled = value;
            }
        }

        public string[] ProbingDirs
        {
            get
            {
                return this.asmBrowser.ProbingDirs;
            }
            set
            {
                this.asmBrowser.ProbingDirs = value;
            }
        }

        public MarshalByRefObject RemoteObject
        {
            get
            {
                return (this.asmBrowser as AsmRemoteBrowser);
            }
        }

        public AppDomain ScriptExecutionDomain
        {
            get
            {
                if (this.remoteAppDomain == null)
                {
                    return AppDomain.CurrentDomain;
                }
                return this.remoteAppDomain;
            }
        }
    }
}

