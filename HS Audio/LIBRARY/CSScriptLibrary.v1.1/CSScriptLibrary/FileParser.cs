﻿namespace CSScriptLibrary
{
    using csscript;
    using System;
    using System.Collections;
    using System.IO;
    using System.Text;
    using System.Threading;

    internal class FileParser
    {
        private static bool _throwOnError = true;
        public string fileName;
        public string fileNameImported;
        public static string headerTemplate = ("/*" + Environment.NewLine + " Created by {0} Original location: {1}" + Environment.NewLine + " C# source equivalent of {2}" + Environment.NewLine + " compiler-generated file created {3} - DO NOT EDIT!" + Environment.NewLine + "*/" + Environment.NewLine);
        private bool imported;
        private ArrayList packages;
        public CSharpParser parser;
        public ParsingParams prams;
        private ArrayList referencedAssemblies;
        private ArrayList referencedNamespaces;
        private ArrayList referencedResources;
        private ArrayList referencedScripts;
        internal static ResolveSourceFileHandler ResolveFileAlgorithm = new ResolveSourceFileHandler(FileParser.ResolveFileDefault);
        private string[] searchDirs;

        public FileParser()
        {
            this.fileNameImported = "";
            this.referencedScripts = new ArrayList();
            this.referencedNamespaces = new ArrayList();
            this.referencedAssemblies = new ArrayList();
            this.packages = new ArrayList();
            this.referencedResources = new ArrayList();
            this.fileName = "";
        }

        public FileParser(string fileName, ParsingParams prams, bool process, bool imported, string[] searchDirs, bool throwOnError)
        {
            this.fileNameImported = "";
            this.referencedScripts = new ArrayList();
            this.referencedNamespaces = new ArrayList();
            this.referencedAssemblies = new ArrayList();
            this.packages = new ArrayList();
            this.referencedResources = new ArrayList();
            this.fileName = "";
            _throwOnError = throwOnError;
            this.imported = imported;
            this.prams = prams;
            this.searchDirs = searchDirs;
            this.fileName = ResolveFile(fileName, searchDirs);
            if (process)
            {
                this.ProcessFile();
            }
        }

        public string ComposeHeader(string path)
        {
            return string.Format(headerTemplate, new object[] { AppInfo.appLogoShort, path, this.fileName, DateTime.Now });
        }

        public void ProcessFile()
        {
            this.packages.Clear();
            this.referencedAssemblies.Clear();
            this.referencedScripts.Clear();
            this.referencedNamespaces.Clear();
            this.referencedResources.Clear();
            this.parser = new CSharpParser(this.fileName, true);
            foreach (CSharpParser.ImportInfo info in this.parser.Imports)
            {
                this.referencedScripts.Add(new ScriptInfo(info));
            }
            this.referencedAssemblies.AddRange(this.parser.RefAssemblies);
            this.referencedNamespaces.AddRange(Utils.Except(this.parser.RefNamespaces, this.parser.IgnoreNamespaces));
            this.referencedResources.AddRange(this.parser.ResFiles);
            if (this.imported)
            {
                if (this.prams != null)
                {
                    this.parser.DoRenaming(this.prams.RenameNamespaceMap, this.prams.preserveMain);
                }
                if (this.parser.ModifiedCode == "")
                {
                    this.fileNameImported = this.fileName;
                }
                else
                {
                    this.fileNameImported = Path.Combine(CSExecutor.ScriptCacheDir, string.Format("i_{0}_{1}{2}", Path.GetFileNameWithoutExtension(this.fileName), CSSUtils.GetHashCodeEx(Path.GetDirectoryName(this.fileName)), Path.GetExtension(this.fileName)));
                    if (!Directory.Exists(Path.GetDirectoryName(this.fileNameImported)))
                    {
                        Directory.CreateDirectory(Path.GetDirectoryName(this.fileNameImported));
                    }
                    if (File.Exists(this.fileNameImported))
                    {
                        File.SetAttributes(this.fileNameImported, FileAttributes.Normal);
                        Utils.FileDelete(this.fileNameImported, true);
                    }
                    using (StreamWriter writer = new StreamWriter(this.fileNameImported, false, Encoding.UTF8))
                    {
                        writer.WriteLine(this.parser.ModifiedCode);
                        writer.WriteLine("///////////////////////////////////////////");
                        writer.WriteLine("// Compiler-generated file - DO NOT EDIT!");
                        writer.WriteLine("///////////////////////////////////////////");
                    }
                    File.SetAttributes(this.fileNameImported, FileAttributes.ReadOnly);
                }
            }
        }

        public static string ResolveFile(string fileName, string[] extraDirs)
        {
            return ResolveFile(fileName, extraDirs, _throwOnError);
        }

        public static string ResolveFile(string file, string[] extraDirs, bool throwOnError)
        {
            return ResolveFileAlgorithm(file, extraDirs, throwOnError);
        }

        private static string ResolveFile(string file, string[] extraDirs, string extension)
        {
            string path = file;
            if (Path.GetExtension(path) == "")
            {
                path = path + extension;
            }
            if (File.Exists(path))
            {
                return Path.GetFullPath(path);
            }
            if (extraDirs != null)
            {
                foreach (string str2 in extraDirs)
                {
                    string str3 = str2;
                    if (File.Exists(Path.Combine(str3, path)))
                    {
                        return Path.GetFullPath(Path.Combine(str3, path));
                    }
                }
            }
            foreach (string str4 in Environment.GetEnvironmentVariable("PATH").Replace("\"", "").Split(new char[] { ';' }))
            {
                string str5 = str4;
                if (File.Exists(Path.Combine(str5, path)))
                {
                    return Path.GetFullPath(Path.Combine(str5, path));
                }
            }
            return "";
        }

        internal static string ResolveFileDefault(string file, string[] extraDirs, bool throwOnError)
        {
            string str = ResolveFile(file, extraDirs, "");
            if (str == "")
            {
                str = ResolveFile(file, extraDirs, ".cs");
            }
            if (str == "")
            {
                str = ResolveFile(file, extraDirs, ".csl");
            }
            if (str == "")
            {
                if (throwOnError)
                {
                    throw new FileNotFoundException(string.Format("Could not find file \"{0}\"", file));
                }
                str = file;
                if (!str.EndsWith(".cs"))
                {
                    str = str + ".cs";
                }
            }
            return str;
        }

        public string[] CompilerOptions
        {
            get
            {
                return this.parser.CompilerOptions;
            }
        }

        public string[] ExtraSearchDirs
        {
            get
            {
                return this.parser.ExtraSearchDirs;
            }
        }

        public string FileToCompile
        {
            get
            {
                if (!this.imported)
                {
                    return this.fileName;
                }
                return this.fileNameImported;
            }
        }

        public string[] IgnoreNamespaces
        {
            get
            {
                return this.parser.IgnoreNamespaces;
            }
        }

        public bool Imported
        {
            get
            {
                return this.imported;
            }
        }

        public string[] Packages
        {
            get
            {
                return this.parser.NuGets;
            }
        }

        public string[] Precompilers
        {
            get
            {
                return this.parser.Precompilers;
            }
        }

        public string[] ReferencedAssemblies
        {
            get
            {
                return this.parser.RefAssemblies;
            }
        }

        public string[] ReferencedNamespaces
        {
            get
            {
                return Utils.Except(this.parser.RefNamespaces, this.parser.IgnoreNamespaces);
            }
        }

        public string[] ReferencedResources
        {
            get
            {
                return this.parser.ResFiles;
            }
        }

        public ScriptInfo[] ReferencedScripts
        {
            get
            {
                return (ScriptInfo[]) this.referencedScripts.ToArray(typeof(ScriptInfo));
            }
        }

        public string[] SearchDirs
        {
            get
            {
                return this.searchDirs;
            }
        }

        public ApartmentState ThreadingModel
        {
            get
            {
                if (this.parser == null)
                {
                    return ApartmentState.Unknown;
                }
                return this.parser.ThreadingModel;
            }
        }
    }
}

