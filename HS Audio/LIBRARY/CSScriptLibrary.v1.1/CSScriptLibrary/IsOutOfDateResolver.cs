﻿namespace CSScriptLibrary
{
    using System;
    using System.Runtime.CompilerServices;

    public delegate bool IsOutOfDateResolver(string scriptSource, string scriptAssembly);
}

