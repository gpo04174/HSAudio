﻿namespace CSScriptLibrary
{
    using System;
    using System.Reflection;
    using System.Reflection.Emit;

    public class FastInvoker
    {
        public MethodInfo info;
        private FastInvokeDelegate method;

        public FastInvoker(MethodInfo info)
        {
            this.info = info;
            this.method = this.GenerateMethodInvoker(info);
        }

        private static void EmitBoxIfNeeded(ILGenerator il, Type type)
        {
            if (type.IsValueType)
            {
                il.Emit(OpCodes.Box, type);
            }
        }

        private static void EmitCastToReference(ILGenerator il, Type type)
        {
            throw new NotImplementedException("This version of AsmHelper does not implement EmitCastToReference().\nPlease use CSScriptLibrarly.dll which is compiled against at least CLR v2.0");
        }

        private static void EmitFastInt(ILGenerator il, int value)
        {
            switch (value)
            {
                case -1:
                    il.Emit(OpCodes.Ldc_I4_M1);
                    return;

                case 0:
                    il.Emit(OpCodes.Ldc_I4_0);
                    return;

                case 1:
                    il.Emit(OpCodes.Ldc_I4_1);
                    return;

                case 2:
                    il.Emit(OpCodes.Ldc_I4_2);
                    return;

                case 3:
                    il.Emit(OpCodes.Ldc_I4_3);
                    return;

                case 4:
                    il.Emit(OpCodes.Ldc_I4_4);
                    return;

                case 5:
                    il.Emit(OpCodes.Ldc_I4_5);
                    return;

                case 6:
                    il.Emit(OpCodes.Ldc_I4_6);
                    return;

                case 7:
                    il.Emit(OpCodes.Ldc_I4_7);
                    return;

                case 8:
                    il.Emit(OpCodes.Ldc_I4_8);
                    return;
            }
            if ((value > -129) && (value < 0x80))
            {
                il.Emit(OpCodes.Ldc_I4_S, (sbyte) value);
            }
            else
            {
                il.Emit(OpCodes.Ldc_I4, value);
            }
        }

        private FastInvokeDelegate GenerateMethodInvoker(MethodInfo methodInfo)
        {
            throw new NotImplementedException("This version of AsmHelper does not implement GenerateMethodInvoker()\nPlease use CSScriptLibrarly.dll which is compiled against at least CLR v2.0");
        }

        public FastInvokeDelegate GetMethodInvoker()
        {
            return this.method;
        }

        public object Invoke(object instance, params object[] paramters)
        {
            return this.method(instance, paramters);
        }
    }
}

