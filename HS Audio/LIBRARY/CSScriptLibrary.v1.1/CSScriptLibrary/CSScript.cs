﻿namespace CSScriptLibrary
{
    using csscript;
    using System;
    using System.Collections;
    using System.Diagnostics;
    using System.IO;
    using System.Reflection;
    using System.Text;
    using System.Threading;

    public class CSScript
    {
        private static bool assemblyResolvingEnabled = true;
        public static bool CacheEnabled = true;
        private static Assembly callingResolveEnabledAssembly;
        private static string dummy = "";
        private static Hashtable dynamicScriptsAssemblies = new Hashtable();
        private static string evalNamespaces;
        public static Settings GlobalSettings = Settings.Load(Environment.ExpandEnvironmentVariables(@"%CSSCRIPT_DIR%\css_config.xml"));
        private static IsOutOfDateResolver isOutOfDateAlgorithm = CachProbing.Advanced;
        private static object LoadAutoCodeSynch = new object();
        private static bool rethrow = false;
        private static ArrayList scriptCache = new ArrayList();
        private static bool shareHostRefAssemblies = true;
        private static ArrayList tempFiles;

        static CSScript()
        {
            AssemblyResolvingEnabled = true;
        }

        public static string[] AggregateReferencedAssemblies(ScriptParser parser, string[] searchDirs, string defaultRefAssemblies)
        {
            CSExecutor executor = new CSExecutor();
            executor.GetOptions().searchDirs = searchDirs;
            executor.GetOptions().defaultRefAssemblies = defaultRefAssemblies;
            return executor.AggregateReferencedAssemblies(parser);
        }

        public static string Compile(string scriptFile, params string[] refAssemblies)
        {
            return Compile(scriptFile, null, false, refAssemblies);
        }

        public static string Compile(string scriptFile, string assemblyFile, bool debugBuild, params string[] refAssemblies)
        {
            return CompileWithConfig(scriptFile, assemblyFile, debugBuild, GlobalSettings, null, refAssemblies);
        }

        public static string CompileCode(string scriptText, params string[] refAssemblies)
        {
            return CompileCode(scriptText, null, false, refAssemblies);
        }

        public static string CompileCode(string scriptText, string assemblyFile, bool debugBuild, params string[] refAssemblies)
        {
            string str2;
            lock (typeof(CSScript))
            {
                string scriptTempFile = CSExecutor.GetScriptTempFile();
                try
                {
                    using (StreamWriter writer = new StreamWriter(scriptTempFile))
                    {
                        writer.Write(scriptText);
                    }
                    str2 = Compile(scriptTempFile, assemblyFile, debugBuild, refAssemblies);
                }
                finally
                {
                    if (!debugBuild)
                    {
                        Utils.FileDelete(scriptTempFile);
                    }
                    else
                    {
                        if (tempFiles == null)
                        {
                            tempFiles = new ArrayList();
                            AppDomain.CurrentDomain.DomainUnload += new EventHandler(CSScript.CurrentDomain_DomainUnload);
                        }
                        tempFiles.Add(scriptTempFile);
                    }
                }
            }
            return str2;
        }

        public static string CompileFiles(string[] sourceFiles, params string[] refAssemblies)
        {
            return CompileFiles(sourceFiles, null, false, refAssemblies);
        }

        public static string CompileFiles(string[] sourceFiles, string assemblyFile, bool debugBuild, params string[] refAssemblies)
        {
            StringBuilder builder = new StringBuilder();
            foreach (string str in sourceFiles)
            {
                builder.AppendFormat("//css_inc {0};{1}", str, Environment.NewLine);
            }
            return CompileCode(builder.ToString(), assemblyFile, debugBuild, refAssemblies);
        }

        public static string CompileWithConfig(string scriptFile, string assemblyFile, bool debugBuild, string cssConfigFile)
        {
            return CompileWithConfig(scriptFile, assemblyFile, debugBuild, cssConfigFile, null, null);
        }

        public static string CompileWithConfig(string scriptFile, string assemblyFile, bool debugBuild, Settings scriptSettings, string compilerOptions, params string[] refAssemblies)
        {
            string str4;
            lock (typeof(CSScript))
            {
                using (Mutex mutex = new Mutex(false, GetCompilerLockName(assemblyFile, scriptSettings)))
                {
                    ExecuteOptions options = CSExecutor.options;
                    try
                    {
                        int tickCount = Environment.TickCount;
                        mutex.WaitOne(0x1388, false);
                        CSExecutor executor = new CSExecutor {
                            Rethrow = true
                        };
                        InitExecuteOptions(CSExecutor.options, scriptSettings, compilerOptions, ref scriptFile);
                        CSExecutor.options.DBG = debugBuild;
                        ExecuteOptions.options.useSmartCaching = CacheEnabled;
                        if ((refAssemblies != null) && (refAssemblies.Length != 0))
                        {
                            foreach (string str2 in refAssemblies)
                            {
                                string directoryName = Path.GetDirectoryName(str2);
                                CSExecutor.options.AddSearchDir(directoryName);
                                GlobalSettings.AddSearchDir(directoryName);
                            }
                            CSExecutor.options.refAssemblies = refAssemblies;
                        }
                        if (CacheEnabled)
                        {
                            if (assemblyFile != null)
                            {
                                if (!IsOutOfDateAlgorithm(scriptFile, assemblyFile))
                                {
                                    return assemblyFile;
                                }
                            }
                            else
                            {
                                string cachedScriptAssemblyFile = GetCachedScriptAssemblyFile(scriptFile);
                                if (cachedScriptAssemblyFile != null)
                                {
                                    return cachedScriptAssemblyFile;
                                }
                            }
                        }
                        str4 = executor.Compile(scriptFile, assemblyFile, debugBuild);
                    }
                    finally
                    {
                        CSExecutor.options = options;
                        try
                        {
                            mutex.ReleaseMutex();
                        }
                        catch
                        {
                        }
                    }
                }
            }
            return str4;
        }

        public static string CompileWithConfig(string scriptFile, string assemblyFile, bool debugBuild, string cssConfigFile, string compilerOptions, params string[] refAssemblies)
        {
            lock (typeof(CSScript))
            {
                Settings scriptSettings = Settings.Load(ResolveConfigFilePath(cssConfigFile));
                if (scriptSettings == null)
                {
                    throw new ApplicationException("The configuration file \"" + cssConfigFile + "\" cannot be loaded");
                }
                return CompileWithConfig(scriptFile, assemblyFile, debugBuild, scriptSettings, compilerOptions, refAssemblies);
            }
        }

        public static Mutex CreateCompilerLock(string compiledScriptFile, bool optimisticConcurrencyModel)
        {
            return new Mutex(false, GetCompilerLockName(compiledScriptFile, optimisticConcurrencyModel));
        }

        private static void CurrentDomain_DomainUnload(object sender, EventArgs e)
        {
            OnApplicationExit(sender, e);
        }

        private static void DefaultPrint(string msg)
        {
        }

        public static void Execute(CSScriptLibrary.PrintDelegate print, string[] args)
        {
            lock (typeof(CSScript))
            {
                ExecuteOptions options = CSExecutor.options;
                try
                {
                    AppInfo.appName = Path.GetFileName(Assembly.GetExecutingAssembly().Location);
                    CSExecutor executor = new CSExecutor {
                        Rethrow = Rethrow
                    };
                    InitExecuteOptions(CSExecutor.options, GlobalSettings, null, ref dummy);
                    executor.Execute(args, new csscript.PrintDelegate(((print != null) ? print : new CSScriptLibrary.PrintDelegate(CSScript.DefaultPrint)).Invoke), null);
                }
                finally
                {
                    CSExecutor.options = options;
                }
            }
        }

        public void Execute(CSScriptLibrary.PrintDelegate print, string[] args, bool rethrow)
        {
            lock (typeof(CSScript))
            {
                ExecuteOptions options = CSExecutor.options;
                try
                {
                    AppInfo.appName = Path.GetFileName(Assembly.GetExecutingAssembly().Location);
                    CSExecutor executor = new CSExecutor {
                        Rethrow = rethrow
                    };
                    InitExecuteOptions(CSExecutor.options, GlobalSettings, null, ref dummy);
                    executor.Execute(args, new csscript.PrintDelegate(((print != null) ? print : new CSScriptLibrary.PrintDelegate(CSScript.DefaultPrint)).Invoke), null);
                }
                finally
                {
                    CSExecutor.options = options;
                }
            }
        }

        public static string GetCachedScriptAssemblyFile(string file)
        {
            string fullPath = Path.GetFullPath(file);
            foreach (LoadedScript script in ScriptCache)
            {
                if ((script.script == fullPath) && !IsOutOfDateAlgorithm(fullPath, script.asm.Location))
                {
                    return script.asm.Location;
                }
            }
            string cachedScriptPath = GetCachedScriptPath(fullPath);
            if (File.Exists(cachedScriptPath) && !IsOutOfDateAlgorithm(fullPath, cachedScriptPath))
            {
                return cachedScriptPath;
            }
            return null;
        }

        public static string GetCachedScriptPath(string scriptFile)
        {
            string fullPath = Path.GetFullPath(scriptFile);
            return Path.Combine(CSSEnvironment.GetCacheDirectory(fullPath), Path.GetFileName(fullPath) + ".compiled");
        }

        private static string GetCompilerLockName(string script, Settings scriptSettings)
        {
            return GetCompilerLockName(script, scriptSettings.OptimisticConcurrencyModel);
        }

        private static string GetCompilerLockName(string script, bool optimisticConcurrencyModel)
        {
            if (optimisticConcurrencyModel)
            {
                return Process.GetCurrentProcess().Id.ToString();
            }
            return string.Format("{0}.{1}", Process.GetCurrentProcess().Id, CSSUtils.GetHashCodeEx(script));
        }

        public static Assembly GetLoadedCachedScriptAssembly(string file)
        {
            string fullPath = Path.GetFullPath(file);
            foreach (LoadedScript script in ScriptCache)
            {
                if ((script.script == fullPath) && !IsOutOfDateAlgorithm(fullPath, script.asm.Location))
                {
                    return script.asm;
                }
            }
            string cachedScriptPath = GetCachedScriptPath(fullPath);
            if (File.Exists(cachedScriptPath) && !IsOutOfDateAlgorithm(fullPath, cachedScriptPath))
            {
                return Assembly.LoadFrom(cachedScriptPath);
            }
            return null;
        }

        public static string GetScriptName(Assembly assembly)
        {
            object[] customAttributes = assembly.GetCustomAttributes(typeof(AssemblyDescriptionAttribute), true);
            int index = 0;
            while (index < customAttributes.Length)
            {
                AssemblyDescriptionAttribute attribute = (AssemblyDescriptionAttribute) customAttributes[index];
                return attribute.Description;
            }
            return null;
        }

        public static string GetScriptTempDir()
        {
            return CSExecutor.GetScriptTempDir();
        }

        public static string GetScriptTempFile()
        {
            return CSExecutor.GetScriptTempFile();
        }

        private static ExecuteOptions InitExecuteOptions(ExecuteOptions options, Settings scriptSettings, string compilerOptions, ref string scriptFile)
        {
            Settings settings = (scriptSettings == null) ? GlobalSettings : scriptSettings;
            options.altCompiler = settings.ExpandUseAlternativeCompiler();
            options.compilerOptions = (compilerOptions != null) ? compilerOptions : "";
            options.apartmentState = settings.DefaultApartmentState;
            options.reportDetailedErrorInfo = settings.ReportDetailedErrorInfo;
            options.cleanupShellCommand = settings.CleanupShellCommand;
            options.customHashing = settings.CustomHashing;
            options.inMemoryAsm = settings.InMemoryAssembly;
            options.hideCompilerWarnings = settings.HideCompilerWarnings;
            options.TargetFramework = settings.TargetFramework;
            options.doCleanupAfterNumberOfRuns = settings.DoCleanupAfterNumberOfRuns;
            options.useCompiled = CacheEnabled;
            options.useSurrogateHostingProcess = false;
            ArrayList list = new ArrayList();
            options.shareHostRefAssemblies = ShareHostRefAssemblies;
            if (options.shareHostRefAssemblies)
            {
                foreach (Assembly assembly in AppDomain.CurrentDomain.GetAssemblies())
                {
                    try
                    {
                        if (!CSSUtils.IsDynamic(assembly) && File.Exists(assembly.Location))
                        {
                            list.Add(Path.GetDirectoryName(assembly.Location));
                        }
                    }
                    catch
                    {
                    }
                }
            }
            string str = Environment.ExpandEnvironmentVariables("%CSSCRIPT_DIR%" + Path.DirectorySeparatorChar + "lib");
            if (!str.StartsWith("%"))
            {
                list.Add(str);
            }
            if (settings != null)
            {
                list.AddRange(Environment.ExpandEnvironmentVariables(settings.SearchDirs).Split(",;".ToCharArray(), StringSplitOptions.RemoveEmptyEntries));
            }
            if (scriptFile != "")
            {
                scriptFile = FileParser.ResolveFile(scriptFile, (string[]) list.ToArray(typeof(string)));
                list.Add(Path.GetDirectoryName(scriptFile));
            }
            options.searchDirs = RemovePathDuplicates((string[]) list.ToArray(typeof(string)));
            options.scriptFileName = scriptFile;
            return options;
        }

        public static Assembly Load(string scriptFile)
        {
            return Load(scriptFile, null, false, null);
        }

        public static Assembly Load(string scriptFile, params string[] refAssemblies)
        {
            return Load(scriptFile, null, false, refAssemblies);
        }

        public static Assembly Load(string scriptFile, string assemblyFile, bool debugBuild, params string[] refAssemblies)
        {
            return LoadWithConfig(scriptFile, assemblyFile, debugBuild, null, "", refAssemblies);
        }

        public static Assembly LoadCode(string scriptText, params string[] refAssemblies)
        {
            return LoadCode(scriptText, null, false, refAssemblies);
        }

        public static Assembly LoadCode(string scriptText, string assemblyFile, bool debugBuild, params string[] refAssemblies)
        {
            return LoadCode(scriptText, "", assemblyFile, debugBuild, refAssemblies);
        }

        public static Assembly LoadCode(string scriptText, string tempFileExtension, string assemblyFile, bool debugBuild, params string[] refAssemblies)
        {
            Assembly assembly2;
            lock (typeof(CSScript))
            {
                uint key = 0;
                if (CacheEnabled)
                {
                    key = Crc32.Compute(Encoding.UTF8.GetBytes(scriptText));
                    if (dynamicScriptsAssemblies.ContainsKey(key))
                    {
                        try
                        {
                            return Assembly.LoadFrom(dynamicScriptsAssemblies[key].ToString());
                        }
                        catch
                        {
                        }
                    }
                }
                string scriptTempFile = CSExecutor.GetScriptTempFile("dynamic");
                if ((tempFileExtension != null) && (tempFileExtension != ""))
                {
                    scriptTempFile = Path.ChangeExtension(scriptTempFile, tempFileExtension);
                }
                try
                {
                    new Mutex(false, scriptTempFile.Replace(Path.DirectorySeparatorChar, '|').ToLower()).WaitOne(1);
                    using (StreamWriter writer = new StreamWriter(scriptTempFile))
                    {
                        writer.Write(scriptText);
                    }
                    Assembly assembly = Load(scriptTempFile, assemblyFile, debugBuild, refAssemblies);
                    if (CacheEnabled && !Utils.IsNullOrWhiteSpace(assembly.Location))
                    {
                        if (dynamicScriptsAssemblies.ContainsKey(key))
                        {
                            dynamicScriptsAssemblies[key] = assembly.Location;
                        }
                        else
                        {
                            dynamicScriptsAssemblies.Add(key, assembly.Location);
                        }
                    }
                    assembly2 = assembly;
                }
                finally
                {
                    if (!debugBuild)
                    {
                        Utils.FileDelete(scriptTempFile);
                    }
                    else
                    {
                        if (tempFiles == null)
                        {
                            tempFiles = new ArrayList();
                            AppDomain.CurrentDomain.DomainUnload += new EventHandler(CSScript.CurrentDomain_DomainUnload);
                        }
                        tempFiles.Add(scriptTempFile);
                    }
                }
            }
            return assembly2;
        }

        public static Assembly LoadCodeFrom(string scriptFile, params string[] refAssemblies)
        {
            return LoadCode(File.ReadAllText(scriptFile), refAssemblies);
        }

        public static Assembly LoadCodeFrom(string scriptFile, string assemblyFile, bool debugBuild, params string[] refAssemblies)
        {
            return LoadCode(File.ReadAllText(scriptFile), assemblyFile, debugBuild, refAssemblies);
        }

        public static Assembly LoadCodeFrom(string scriptFile, string tempFileExtension, string assemblyFile, bool debugBuild, params string[] refAssemblies)
        {
            return LoadCodeFrom(File.ReadAllText(scriptFile), tempFileExtension, assemblyFile, debugBuild, refAssemblies);
        }

        public static T LoadDelegate<T>(string methodCode) where T: class
        {
            return LoadDelegate<T>(methodCode, null, false, new string[0]);
        }

        public static T LoadDelegate<T>(string methodCode, string assemblyFile, bool debugBuild, params string[] refAssemblies) where T: class
        {
            lock (LoadAutoCodeSynch)
            {
                MethodInfo method = LoadCode(WrapMethodToAutoClass(methodCode, true), assemblyFile, debugBuild, refAssemblies).GetType("Scripting.DynamicClass").GetMethods()[0];
                return (Delegate.CreateDelegate(typeof(T), method) as T);
            }
        }

        public static Assembly LoadFiles(string[] sourceFiles, params string[] refAssemblies)
        {
            return Assembly.LoadFrom(CompileFiles(sourceFiles, refAssemblies));
        }

        public static Assembly LoadFiles(string[] sourceFiles, string assemblyFile, bool debugBuild, params string[] refAssemblies)
        {
            return Assembly.LoadFrom(CompileFiles(sourceFiles, assemblyFile, debugBuild, refAssemblies));
        }

        public static Assembly LoadMethod(string methodCode, params string[] refAssemblies)
        {
            return LoadMethod(methodCode, null, false, refAssemblies);
        }

        public static Assembly LoadMethod(string methodCode, string assemblyFile, bool debugBuild, params string[] refAssemblies)
        {
            lock (LoadAutoCodeSynch)
            {
                return LoadCode(WrapMethodToAutoClass(methodCode, true), assemblyFile, debugBuild, refAssemblies);
            }
        }

        public static Assembly LoadWithConfig(string scriptFile, string assemblyFile, bool debugBuild, Settings scriptSettings, string compilerOptions, params string[] refAssemblies)
        {
            Assembly assembly2;
            lock (typeof(CSScript))
            {
                using (Mutex mutex = new Mutex(false, GetCompilerLockName(assemblyFile, GlobalSettings)))
                {
                    ExecuteOptions options = CSExecutor.options;
                    options.compilerOptions = "/unsafe";
                    try
                    {
                        int tickCount = Environment.TickCount;
                        mutex.WaitOne(0x1388, false);
                        CSExecutor executor = new CSExecutor {
                            Rethrow = true
                        };
                        InitExecuteOptions(CSExecutor.options, scriptSettings, compilerOptions, ref scriptFile);
                        CSExecutor.options.DBG = debugBuild;
                        ExecuteOptions.options.useSmartCaching = CacheEnabled;
                        if ((refAssemblies != null) && (refAssemblies.Length != 0))
                        {
                            foreach (string str2 in refAssemblies)
                            {
                                string directoryName = Path.GetDirectoryName(str2);
                                CSExecutor.options.AddSearchDir(directoryName);
                                GlobalSettings.AddSearchDir(directoryName);
                            }
                            CSExecutor.options.refAssemblies = refAssemblies;
                        }
                        Assembly asm = null;
                        if (CacheEnabled)
                        {
                            if (assemblyFile != null)
                            {
                                if (!IsOutOfDateAlgorithm(scriptFile, assemblyFile))
                                {
                                    asm = Assembly.LoadFrom(assemblyFile);
                                }
                            }
                            else
                            {
                                asm = GetLoadedCachedScriptAssembly(scriptFile);
                            }
                        }
                        if (asm == null)
                        {
                            string str3 = executor.Compile(scriptFile, assemblyFile, debugBuild);
                            if (!ExecuteOptions.options.inMemoryAsm)
                            {
                                asm = Assembly.LoadFrom(str3);
                            }
                            else
                            {
                                byte[] buffer = new byte[0];
                                using (FileStream stream = new FileStream(str3, FileMode.Open))
                                {
                                    buffer = new byte[stream.Length];
                                    stream.Read(buffer, 0, buffer.Length);
                                }
                                string path = Path.ChangeExtension(str3, ".pdb");
                                if (debugBuild && File.Exists(path))
                                {
                                    byte[] buffer2 = new byte[0];
                                    using (FileStream stream2 = new FileStream(path, FileMode.Open))
                                    {
                                        buffer2 = new byte[stream2.Length];
                                        stream2.Read(buffer2, 0, buffer2.Length);
                                    }
                                    asm = Assembly.Load(buffer, buffer2);
                                }
                                else
                                {
                                    asm = Assembly.Load(buffer);
                                }
                            }
                            if (asm != null)
                            {
                                scriptCache.Add(new LoadedScript(scriptFile, asm));
                            }
                        }
                        assembly2 = asm;
                    }
                    finally
                    {
                        try
                        {
                            mutex.ReleaseMutex();
                        }
                        catch
                        {
                        }
                        CSExecutor.options = options;
                    }
                }
            }
            return assembly2;
        }

        private static void OnApplicationExit(object sender, EventArgs e)
        {
            if (tempFiles != null)
            {
                foreach (string str in tempFiles)
                {
                    Utils.FileDelete(str);
                }
            }
        }

        private static Assembly OnAssemblyResolve(object sender, ResolveEventArgs args)
        {
            Assembly assembly = null;
            if (args.Name == "GetExecutingAssembly()")
            {
                return callingResolveEnabledAssembly;
            }
            if (args.Name == "GetEntryAssembly()")
            {
                return Assembly.GetEntryAssembly();
            }
            foreach (string str in InitExecuteOptions(new ExecuteOptions(), GlobalSettings, null, ref dummy).searchDirs)
            {
                assembly = AssemblyResolver.ResolveAssembly(args.Name, str, false);
                if (assembly != null)
                {
                    return assembly;
                }
            }
            return assembly;
        }

        internal static string[] RemovePathDuplicates(string[] list)
        {
            lock (typeof(CSScript))
            {
                return Utils.RemovePathDuplicates(list);
            }
        }

        private static string ResolveConfigFilePath(string cssConfigFile)
        {
            if (cssConfigFile == null)
            {
                return Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "css_config.xml");
            }
            return cssConfigFile;
        }

        private static string SplitNamespaces(string text)
        {
            StringBuilder builder = new StringBuilder();
            foreach (string str in text.Split(new char[] { ';' }))
            {
                builder.Append("using ");
                builder.Append(str);
                builder.Append(";\n");
            }
            return builder.ToString();
        }

        internal static string WrapMethodToAutoClass(string methodCode, bool injectStatic)
        {
            StringBuilder builder = new StringBuilder(0x1000);
            builder.Append("//Auto-generated file\r\n");
            builder.Append("using System;\r\n");
            bool flag = false;
            using (StringReader reader = new StringReader(methodCode))
            {
                string str;
                while ((str = reader.ReadLine()) != null)
                {
                    if (!flag && !str.TrimStart(new char[0]).StartsWith("using "))
                    {
                        string str2 = str.Trim();
                        if (!str2.StartsWith("//") && (str2 != ""))
                        {
                            flag = true;
                            builder.Append("namespace Scripting\r\n");
                            builder.Append("{\r\n");
                            builder.Append("   public class DynamicClass\r\n");
                            builder.Append("   {\r\n");
                            string[] strArray = str.Split("\t ".ToCharArray(), 3, StringSplitOptions.RemoveEmptyEntries);
                            if ((injectStatic && (strArray[0] != "static")) && (strArray[1] != "static"))
                            {
                                builder.Append("   static\r\n");
                            }
                            if ((strArray[0] != "public") && (strArray[1] != "public"))
                            {
                                builder.Append("   public\r\n");
                            }
                        }
                    }
                    builder.Append(str);
                    builder.Append("\r\n");
                }
            }
            builder.Append("   }\r\n");
            builder.Append("}\r\n");
            return builder.ToString();
        }

        public static bool AssemblyResolvingEnabled
        {
            get
            {
                return assemblyResolvingEnabled;
            }
            set
            {
                if (value)
                {
                    AppDomain.CurrentDomain.AssemblyResolve += new ResolveEventHandler(CSScript.OnAssemblyResolve);
                    callingResolveEnabledAssembly = Assembly.GetCallingAssembly();
                }
                else
                {
                    AppDomain.CurrentDomain.AssemblyResolve -= new ResolveEventHandler(CSScript.OnAssemblyResolve);
                }
                assemblyResolvingEnabled = value;
            }
        }

        public static string EvalNamespaces
        {
            private get
            {
                if (evalNamespaces == null)
                {
                    evalNamespaces = SplitNamespaces("System;System.IO;System.Diagnostics;System.Collections.Generic;System.Threading");
                }
                return evalNamespaces;
            }
            set
            {
                evalNamespaces = SplitNamespaces(value);
            }
        }

        public static IsOutOfDateResolver IsOutOfDate
        {
            get
            {
                return isOutOfDateAlgorithm;
            }
            set
            {
                isOutOfDateAlgorithm = value;
            }
        }

        public static IsOutOfDateResolver IsOutOfDateAlgorithm
        {
            get
            {
                return isOutOfDateAlgorithm;
            }
            set
            {
                isOutOfDateAlgorithm = value;
            }
        }

        public static ResolveAssemblyHandler ResolveAssemblyAlgorithm
        {
            get
            {
                return AssemblyResolver.FindAssemblyAlgorithm;
            }
            set
            {
                AssemblyResolver.FindAssemblyAlgorithm = value;
            }
        }

        public static ResolveSourceFileHandler ResolveSourceAlgorithm
        {
            get
            {
                return FileParser.ResolveFileAlgorithm;
            }
            set
            {
                FileParser.ResolveFileAlgorithm = value;
            }
        }

        public static bool Rethrow
        {
            get
            {
                return rethrow;
            }
            set
            {
                rethrow = value;
            }
        }

        public static LoadedScript[] ScriptCache
        {
            get
            {
                return (LoadedScript[]) scriptCache.ToArray(typeof(LoadedScript));
            }
        }

        public static bool ShareHostRefAssemblies
        {
            get
            {
                return shareHostRefAssemblies;
            }
            set
            {
                if (shareHostRefAssemblies != value)
                {
                    shareHostRefAssemblies = value;
                    if (shareHostRefAssemblies)
                    {
                        AssemblyResolvingEnabled = true;
                    }
                }
            }
        }

        public class CachProbing
        {
            internal static bool ScriptAsmOutOfDateAdvanced(string scriptFileName, string assemblyFileName)
            {
                return ((File.GetLastWriteTimeUtc(scriptFileName) != File.GetLastWriteTimeUtc(assemblyFileName)) || MetaDataItems.IsOutOfDate(scriptFileName, assemblyFileName));
            }

            internal static bool ScriptAsmOutOfDateSimplified(string scriptFileName, string assemblyFileName)
            {
                return (File.GetLastWriteTimeUtc(scriptFileName) != File.GetLastWriteTimeUtc(assemblyFileName));
            }

            public static IsOutOfDateResolver Advanced
            {
                get
                {
                    return new IsOutOfDateResolver(CSScript.CachProbing.ScriptAsmOutOfDateAdvanced);
                }
            }

            public static IsOutOfDateResolver Simplified
            {
                get
                {
                    return new IsOutOfDateResolver(CSScript.CachProbing.ScriptAsmOutOfDateSimplified);
                }
            }
        }

        public class LoadedScript
        {
            public Assembly asm;
            public string script;

            public LoadedScript(string script, Assembly asm)
            {
                this.script = Path.GetFullPath(script);
                this.asm = asm;
            }
        }
    }
}

