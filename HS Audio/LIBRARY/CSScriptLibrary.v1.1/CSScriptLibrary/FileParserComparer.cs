﻿namespace CSScriptLibrary
{
    using System;
    using System.Collections;

    internal class FileParserComparer : IComparer
    {
        public int Compare(object x, object y)
        {
            if ((x == null) && (y == null))
            {
                return 0;
            }
            int num = (x == null) ? -1 : ((y == null) ? 1 : 0);
            if (num == 0)
            {
                FileParser parser = (FileParser) x;
                FileParser parser2 = (FileParser) y;
                num = string.Compare(parser.fileName, parser2.fileName, true);
                if (num == 0)
                {
                    num = ParsingParams.Compare(parser.prams, parser2.prams);
                }
            }
            return num;
        }
    }
}

