﻿namespace CSScriptLibrary
{
    using System;
    using System.Collections;

    internal class ParsingParams
    {
        public bool preserveMain;
        private ArrayList renameNamespaceMap = new ArrayList();

        public void AddRenameNamespaceMap(string[][] names)
        {
            this.renameNamespaceMap.AddRange(names);
        }

        public static int Compare(ParsingParams xPrams, ParsingParams yPrams)
        {
            if ((xPrams == null) && (yPrams == null))
            {
                return 0;
            }
            int num = (xPrams == null) ? -1 : ((yPrams == null) ? 1 : 0);
            if (num == 0)
            {
                string[][] renameNamespaceMap = xPrams.RenameNamespaceMap;
                string[][] strArray2 = yPrams.RenameNamespaceMap;
                num = Comparer.Default.Compare(renameNamespaceMap.Length, strArray2.Length);
                if (num != 0)
                {
                    return num;
                }
                for (int i = 0; (i < renameNamespaceMap.Length) && (num == 0); i++)
                {
                    num = Comparer.Default.Compare(renameNamespaceMap[i].Length, strArray2[i].Length);
                    if (num == 0)
                    {
                        for (int j = 0; j < renameNamespaceMap[i].Length; j++)
                        {
                            num = Comparer.Default.Compare(renameNamespaceMap[i][j], strArray2[i][j]);
                        }
                    }
                }
            }
            return num;
        }

        public string[][] RenameNamespaceMap
        {
            get
            {
                return (string[][]) this.renameNamespaceMap.ToArray(typeof(string[]));
            }
        }
    }
}

