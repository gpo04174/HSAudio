﻿namespace CSScriptLibrary
{
    using csscript;
    using System;
    using System.Collections;
    using System.IO;
    using System.Reflection;

    public class AssemblyResolver
    {
        private static bool cacheProbingResults;
        internal static ResolveAssemblyHandler FindAssemblyAlgorithm = new ResolveAssemblyHandler(AssemblyResolver.DefaultFindAssemblyAlgorithm);
        public static string ignoreFileName = "";
        private static readonly char[] illegalChars = ":*?<>|\"".ToCharArray();
        private static readonly Hashtable NotFoundAssemblies = new Hashtable();

        private static int BuildHashSetValue(string assemblyName, string directory)
        {
            return CSSUtils.GetHashCodeEx((assemblyName ?? "") + (directory ?? ""));
        }

        private static string[] DefaultFindAssemblyAlgorithm(string name, string[] searchDirs)
        {
            ArrayList list = new ArrayList();
            if (IsLegalPathToken(name))
            {
                try
                {
                    if (Path.IsPathRooted(name) && File.Exists(name))
                    {
                        list.Add(name);
                    }
                }
                catch
                {
                }
            }
            else
            {
                foreach (string str in searchDirs)
                {
                    foreach (string str2 in FindLocalAssembly(name, str))
                    {
                        list.Add(str2);
                    }
                    if (list.Count != 0)
                    {
                        break;
                    }
                }
                if (list.Count == 0)
                {
                    foreach (string str4 in FindGlobalAssembly(Utils.RemoveAssemblyExtension(name)))
                    {
                        list.Add(str4);
                    }
                }
            }
            return (string[]) list.ToArray(typeof(string));
        }

        public static string[] FindAssembly(string name, string[] searchDirs)
        {
            return FindAssemblyAlgorithm(name, searchDirs);
        }

        public static string[] FindGlobalAssembly(string namespaceStr)
        {
            ArrayList list = new ArrayList();
            try
            {
                AssemblyEnum enum2 = new AssemblyEnum(namespaceStr);
                string strB = "";
                string strA = "";
                do
                {
                    strA = enum2.GetNextAssembly();
                    if (string.Compare(strA, strB) > 0)
                    {
                        strB = strA;
                    }
                }
                while (!namespaceStr.Contains(", Version=") && (strA != null));
                if (strB != "")
                {
                    string str3 = AssemblyCache.QueryAssemblyInfo(strB);
                    list.Add(str3);
                }
            }
            catch
            {
            }
            if ((list.Count == 0) && namespaceStr.ToLower().EndsWith(".dll"))
            {
                list.Add(namespaceStr);
            }
            return (string[]) list.ToArray(typeof(string));
        }

        public static string[] FindLocalAssembly(string name, string dir)
        {
            try
            {
                string path = Path.Combine(dir, name);
                if (Directory.Exists(Path.GetDirectoryName(path)))
                {
                    foreach (string str2 in new string[] { "", ".dll", ".exe", ".compiled" })
                    {
                        string str3 = path + str2;
                        if ((ignoreFileName != Path.GetFileName(str3)) && File.Exists(str3))
                        {
                            return new string[] { str3 };
                        }
                    }
                    if ((path != Path.GetFileName(path)) && File.Exists(path))
                    {
                        return new string[] { path };
                    }
                }
            }
            catch
            {
            }
            return new string[0];
        }

        public static bool IsLegalPathToken(string name)
        {
            return (name.IndexOfAny(illegalChars) != -1);
        }

        public static bool IsNamespaceDefinedInAssembly(string asmFileName, string namespaceStr)
        {
            if (File.Exists(asmFileName))
            {
                try
                {
                    Assembly assembly = Assembly.LoadFrom(asmFileName);
                    if (assembly != null)
                    {
                        foreach (Module module in assembly.GetModules())
                        {
                            foreach (Type type in module.GetTypes())
                            {
                                if (namespaceStr == type.Namespace)
                                {
                                    return true;
                                }
                            }
                        }
                    }
                }
                catch
                {
                }
            }
            return false;
        }

        private static Assembly LoadAssemblyFrom(string assemblyName, string asmFile, bool throwException)
        {
            try
            {
                if (asmFile.EndsWith(".cs"))
                {
                    return null;
                }
                AssemblyName name = AssemblyName.GetAssemblyName(asmFile);
                if ((name != null) && (name.FullName == assemblyName))
                {
                    return Assembly.LoadFrom(asmFile);
                }
                if ((assemblyName.IndexOf(",") == -1) && name.FullName.StartsWith(assemblyName))
                {
                    return Assembly.LoadFrom(asmFile);
                }
            }
            catch
            {
                if (throwException)
                {
                    throw;
                }
            }
            return null;
        }

        private bool ProbeAssembly(string file)
        {
            try
            {
                Assembly.LoadFrom(Path.GetFullPath(file));
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public static Assembly ResolveAssembly(string assemblyName, string dir, bool throwExceptions)
        {
            if (dir != null)
            {
                int key = -1;
                if (CacheProbingResults)
                {
                    key = BuildHashSetValue(assemblyName, dir);
                    lock (NotFoundAssemblies)
                    {
                        if (NotFoundAssemblies.Contains(key))
                        {
                            return null;
                        }
                    }
                }
                try
                {
                    if (Directory.Exists(dir))
                    {
                        Assembly assembly = null;
                        string[] strArray = assemblyName.Split(",".ToCharArray(), 5);
                        string path = Path.Combine(dir, strArray[0]);
                        if (((ignoreFileName != Path.GetFileName(path)) && File.Exists(path)) && ((assembly = LoadAssemblyFrom(assemblyName, path, throwExceptions)) != null))
                        {
                            return assembly;
                        }
                        path = Path.Combine(dir, strArray[0]) + ".dll";
                        if (((ignoreFileName != Path.GetFileName(path)) && File.Exists(path)) && ((assembly = LoadAssemblyFrom(assemblyName, path, throwExceptions)) != null))
                        {
                            return assembly;
                        }
                        foreach (string str2 in Directory.GetFiles(dir, strArray[0] + "*"))
                        {
                            assembly = LoadAssemblyFrom(assemblyName, str2, throwExceptions);
                            if (assembly != null)
                            {
                                return assembly;
                            }
                        }
                    }
                }
                catch
                {
                    if (throwExceptions)
                    {
                        throw;
                    }
                }
                if (CacheProbingResults)
                {
                    lock (NotFoundAssemblies)
                    {
                        NotFoundAssemblies.Add(key, null);
                    }
                }
            }
            return null;
        }

        public static bool CacheProbingResults
        {
            get
            {
                return cacheProbingResults;
            }
            set
            {
                cacheProbingResults = value;
                if (!value)
                {
                    lock (NotFoundAssemblies)
                    {
                        NotFoundAssemblies.Clear();
                    }
                }
            }
        }
    }
}

