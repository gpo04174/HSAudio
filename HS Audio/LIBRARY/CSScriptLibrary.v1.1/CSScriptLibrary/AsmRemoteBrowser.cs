﻿namespace CSScriptLibrary
{
    using csscript;
    using System;
    using System.IO;
    using System.Reflection;

    internal class AsmRemoteBrowser : MarshalByRefObject, IAsmBrowser, IDisposable
    {
        private CSScriptLibrary.AsmBrowser asmBrowser;
        private string asmFile;
        private bool cachingEnabled = true;
        private bool customHashing;
        private string[] probingDirs = new string[0];
        private string workingDir;

        public AsmRemoteBrowser()
        {
            AppDomain.CurrentDomain.AssemblyResolve += new System.ResolveEventHandler(this.ResolveEventHandler);
        }

        public object CreateInstance(string typeName)
        {
            if (this.asmBrowser == null)
            {
                if (this.AsmFile == null)
                {
                    throw new ApplicationException("Assembly name (asmFile) was not set");
                }
                this.workingDir = Path.GetDirectoryName(this.AsmFile);
                this.asmBrowser = new CSScriptLibrary.AsmBrowser(Assembly.LoadFrom(this.AsmFile));
            }
            return this.asmBrowser.CreateInstance(typeName);
        }

        public void Dispose()
        {
            if (this.asmBrowser != null)
            {
                this.asmBrowser.Dispose();
            }
            AppDomain.CurrentDomain.AssemblyResolve -= new System.ResolveEventHandler(this.ResolveEventHandler);
        }

        public string[] GetMembersOf(object obj)
        {
            return Reflector.GetMembersOf(obj);
        }

        public FastInvokeDelegate GetMethodInvoker(string methodName)
        {
            return this.AsmBrowser.GetMethodInvoker(methodName, new Type[0]);
        }

        public FastInvokeDelegate GetMethodInvoker(string methodName, object[] list)
        {
            return this.AsmBrowser.GetMethodInvoker(methodName, list);
        }

        public FastInvokeDelegate GetMethodInvoker(string methodName, Type[] list)
        {
            return this.AsmBrowser.GetMethodInvoker(methodName, list);
        }

        public object Invoke(string methodName, params object[] list)
        {
            return this.AsmBrowser.Invoke(methodName, list);
        }

        public object Invoke_Full(object obj, string methodName, params object[] list)
        {
            return this.AsmBrowser.Invoke_Full(obj, methodName, list);
        }

        private Assembly ResolveEventHandler(object sender, ResolveEventArgs args)
        {
            Assembly assembly = AssemblyResolver.ResolveAssembly(args.Name, this.workingDir, false);
            if (assembly == null)
            {
                foreach (string str in this.probingDirs)
                {
                    assembly = AssemblyResolver.ResolveAssembly(args.Name, Environment.ExpandEnvironmentVariables(str), false);
                    if (assembly != null)
                    {
                        return assembly;
                    }
                }
            }
            return assembly;
        }

        public CSScriptLibrary.AsmBrowser AsmBrowser
        {
            get
            {
                if (this.AsmFile == null)
                {
                    throw new ApplicationException("Assembly name (asmFile) was not set");
                }
                return this.asmBrowser;
            }
        }

        public string AsmFile
        {
            get
            {
                return this.asmFile;
            }
            set
            {
                this.asmFile = value;
                this.workingDir = Path.GetDirectoryName(this.AsmFile);
                this.asmBrowser = new CSScriptLibrary.AsmBrowser(Assembly.LoadFrom(this.AsmFile));
                this.asmBrowser.CachingEnabled = this.cachingEnabled;
            }
        }

        public bool CachingEnabled
        {
            get
            {
                return this.cachingEnabled;
            }
            set
            {
                this.cachingEnabled = value;
                if (this.asmBrowser != null)
                {
                    this.asmBrowser.CachingEnabled = this.cachingEnabled;
                }
            }
        }

        internal bool CustomHashing
        {
            get
            {
                return this.customHashing;
            }
            set
            {
                this.customHashing = value;
                ExecuteOptions.options.customHashing = value;
            }
        }

        public string[] ProbingDirs
        {
            get
            {
                return this.probingDirs;
            }
            set
            {
                this.probingDirs = value;
            }
        }
    }
}

