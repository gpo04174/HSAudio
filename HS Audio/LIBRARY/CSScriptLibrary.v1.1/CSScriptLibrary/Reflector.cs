﻿namespace CSScriptLibrary
{
    using System;
    using System.Collections;
    using System.Reflection;

    internal class Reflector
    {
        public static string[] GetMembersOf(object obj)
        {
            ArrayList list = new ArrayList();
            foreach (MemberInfo info in obj.GetType().GetMembers())
            {
                list.Add(string.Format("MemberType:{0};Name:{1};DeclaringType:{2};Signature:{3}", new object[] { info.MemberType, info.Name, info.DeclaringType.FullName, info.ToString() }));
            }
            return (string[]) list.ToArray(typeof(string));
        }
    }
}

