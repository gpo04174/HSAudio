﻿namespace CSScriptLibrary
{
    using csscript;
    using System;
    using System.Collections;
    using System.IO;
    using System.Threading;

    public class ScriptParser
    {
        public ApartmentState apartmentState;
        private ArrayList compilerOptions;
        private ArrayList fileParsers;
        private ArrayList ignoreNamespaces;
        private ArrayList packages;
        private ArrayList precompilers;
        private ArrayList referencedAssemblies;
        private ArrayList referencedNamespaces;
        private ArrayList referencedResources;
        private string scriptPath;
        public string[] SearchDirs;
        private bool throwOnError;

        public ScriptParser(string fileName)
        {
            this.throwOnError = true;
            this.apartmentState = ApartmentState.Unknown;
            this.fileParsers = new ArrayList();
            this.Init(fileName, null);
        }

        public ScriptParser(string fileName, string[] searchDirs)
        {
            this.throwOnError = true;
            this.apartmentState = ApartmentState.Unknown;
            this.fileParsers = new ArrayList();
            if (CSExecutor.ScriptCacheDir == "")
            {
                CSExecutor.SetScriptCacheDir(fileName);
            }
            this.Init(fileName, searchDirs);
        }

        public ScriptParser(string fileName, string[] searchDirs, bool throwOnError)
        {
            this.throwOnError = true;
            this.apartmentState = ApartmentState.Unknown;
            this.fileParsers = new ArrayList();
            this.throwOnError = throwOnError;
            if (CSExecutor.ScriptCacheDir == "")
            {
                CSExecutor.SetScriptCacheDir(fileName);
            }
            this.Init(fileName, searchDirs);
        }

        private void AddIfNotThere(ArrayList arr, string item)
        {
            if (arr.BinarySearch(item, new StringComparer()) < 0)
            {
                arr.Add(item);
            }
        }

        public void DeleteImportedFiles()
        {
            foreach (FileParser parser in this.fileParsers)
            {
                if (parser.Imported && (parser.fileNameImported != parser.fileName))
                {
                    try
                    {
                        File.SetAttributes(parser.FileToCompile, FileAttributes.Normal);
                        Utils.FileDelete(parser.FileToCompile);
                    }
                    catch
                    {
                    }
                }
            }
        }

        private void Init(string fileName, string[] searchDirs)
        {
            this.ScriptPath = fileName;
            this.packages = new ArrayList();
            this.referencedNamespaces = new ArrayList();
            this.referencedAssemblies = new ArrayList();
            this.referencedResources = new ArrayList();
            this.ignoreNamespaces = new ArrayList();
            this.compilerOptions = new ArrayList();
            this.precompilers = new ArrayList();
            FileParser parser = new FileParser(fileName, null, true, false, searchDirs, this.throwOnError);
            this.apartmentState = parser.ThreadingModel;
            foreach (string str in parser.Precompilers)
            {
                this.PushPrecompiler(str);
            }
            foreach (string str2 in parser.IgnoreNamespaces)
            {
                this.PushIgnoreNamespace(str2);
            }
            foreach (string str3 in parser.ReferencedNamespaces)
            {
                this.PushNamespace(str3);
            }
            foreach (string str4 in parser.ReferencedAssemblies)
            {
                this.PushAssembly(str4);
            }
            foreach (string str5 in parser.Packages)
            {
                this.PushPackage(str5);
            }
            foreach (string str6 in parser.ReferencedResources)
            {
                this.PushResource(str6);
            }
            foreach (string str7 in parser.CompilerOptions)
            {
                this.PushCompilerOptions(str7);
            }
            ArrayList list = new ArrayList();
            list.Add(Path.GetDirectoryName(parser.fileName));
            if (searchDirs != null)
            {
                list.AddRange(searchDirs);
            }
            foreach (string str8 in parser.ExtraSearchDirs)
            {
                if (Path.IsPathRooted(str8))
                {
                    list.Add(Path.GetFullPath(str8));
                }
                else
                {
                    list.Add(Path.Combine(Path.GetDirectoryName(parser.fileName), str8));
                }
            }
            this.SearchDirs = (string[]) list.ToArray(typeof(string));
            foreach (ScriptInfo info in parser.ReferencedScripts)
            {
                this.ProcessFile(info);
            }
            this.fileParsers.Insert(0, parser);
        }

        private void ProcessFile(ScriptInfo fileInfo)
        {
            FileParserComparer comparer = new FileParserComparer();
            FileParser parser = new FileParser(fileInfo.fileName, fileInfo.parseParams, false, true, this.SearchDirs, this.throwOnError);
            if (this.fileParsers.BinarySearch(parser, comparer) < 0)
            {
                if (File.Exists(parser.fileName))
                {
                    parser.ProcessFile();
                    this.fileParsers.Add(parser);
                    this.fileParsers.Sort(comparer);
                    foreach (string str in parser.ReferencedNamespaces)
                    {
                        this.PushNamespace(str);
                    }
                    foreach (string str2 in parser.ReferencedAssemblies)
                    {
                        this.PushAssembly(str2);
                    }
                    foreach (string str3 in parser.Precompilers)
                    {
                        this.PushPrecompiler(str3);
                    }
                    foreach (ScriptInfo info in parser.ReferencedScripts)
                    {
                        this.ProcessFile(info);
                    }
                    foreach (string str4 in parser.ReferencedResources)
                    {
                        this.PushResource(str4);
                    }
                    foreach (string str5 in parser.IgnoreNamespaces)
                    {
                        this.PushIgnoreNamespace(str5);
                    }
                    ArrayList list = new ArrayList(this.SearchDirs);
                    foreach (string str6 in parser.ExtraSearchDirs)
                    {
                        if (Path.IsPathRooted(str6))
                        {
                            list.Add(Path.GetFullPath(str6));
                        }
                        else
                        {
                            list.Add(Path.Combine(Path.GetDirectoryName(parser.fileName), str6));
                        }
                    }
                    this.SearchDirs = (string[]) list.ToArray(typeof(string));
                }
                else
                {
                    parser.fileNameImported = parser.fileName;
                    this.fileParsers.Add(parser);
                    this.fileParsers.Sort(comparer);
                }
            }
        }

        private void PushAssembly(string asmName)
        {
            this.PushItem(this.referencedAssemblies, asmName);
        }

        private void PushCompilerOptions(string option)
        {
            this.AddIfNotThere(this.compilerOptions, option);
        }

        private void PushIgnoreNamespace(string nameSpace)
        {
            this.PushItem(this.ignoreNamespaces, nameSpace);
        }

        private void PushItem(ArrayList collection, string item)
        {
            if (collection.Count > 1)
            {
                collection.Sort();
            }
            this.AddIfNotThere(collection, item);
        }

        private void PushNamespace(string nameSpace)
        {
            this.PushItem(this.referencedNamespaces, nameSpace);
        }

        private void PushPackage(string name)
        {
            this.PushItem(this.packages, name);
        }

        private void PushPrecompiler(string file)
        {
            this.PushItem(this.precompilers, file);
        }

        private void PushResource(string resName)
        {
            this.PushItem(this.referencedResources, resName);
        }

        public string[] ResolvePackages()
        {
            return this.ResolvePackages(false);
        }

        public string[] ResolvePackages(bool suppressDownloading)
        {
            return NuGet.Resolve(this.Packages, suppressDownloading);
        }

        public string[] SaveImportedScripts()
        {
            Path.GetDirectoryName(((FileParser) this.fileParsers[0]).fileName);
            ArrayList list = new ArrayList();
            foreach (FileParser parser in this.fileParsers)
            {
                if (parser.Imported)
                {
                    if (parser.fileNameImported != parser.fileName)
                    {
                        list.Add(parser.fileNameImported);
                    }
                    else
                    {
                        list.Add(parser.fileName);
                    }
                }
            }
            return (string[]) list.ToArray(typeof(string));
        }

        public string[] CompilerOptions
        {
            get
            {
                return (string[]) this.compilerOptions.ToArray(typeof(string));
            }
        }

        public string[] FilesToCompile
        {
            get
            {
                ArrayList list = new ArrayList();
                foreach (FileParser parser in this.fileParsers)
                {
                    list.Add(parser.FileToCompile);
                }
                return (string[]) list.ToArray(typeof(string));
            }
        }

        public string[] IgnoreNamespaces
        {
            get
            {
                return (string[]) this.ignoreNamespaces.ToArray(typeof(string));
            }
        }

        public string[] ImportedFiles
        {
            get
            {
                ArrayList list = new ArrayList();
                foreach (FileParser parser in this.fileParsers)
                {
                    if (parser.Imported)
                    {
                        list.Add(parser.fileName);
                    }
                }
                return (string[]) list.ToArray(typeof(string));
            }
        }

        public string[] Packages
        {
            get
            {
                return (string[]) this.packages.ToArray(typeof(string));
            }
        }

        public string[] Precompilers
        {
            get
            {
                return (string[]) this.precompilers.ToArray(typeof(string));
            }
        }

        public string[] ReferencedAssemblies
        {
            get
            {
                return (string[]) this.referencedAssemblies.ToArray(typeof(string));
            }
        }

        public string[] ReferencedNamespaces
        {
            get
            {
                return (string[]) this.referencedNamespaces.ToArray(typeof(string));
            }
        }

        public string[] ReferencedResources
        {
            get
            {
                return (string[]) this.referencedResources.ToArray(typeof(string));
            }
        }

        public string ScriptPath
        {
            get
            {
                return this.scriptPath;
            }
            private set
            {
                this.scriptPath = value;
            }
        }

        private class StringComparer : IComparer
        {
            public int Compare(object x, object y)
            {
                return string.Compare(x as string, y as string, true);
            }
        }
    }
}

