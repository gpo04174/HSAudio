﻿namespace CSScriptLibrary
{
    using csscript;
    using System;
    using System.Collections;
    using System.IO;
    using System.Reflection;
    using System.Runtime.InteropServices;
    using System.Text;

    internal class AsmBrowser : IAsmBrowser, IDisposable
    {
        private Assembly asm;
        private bool cachingEnabled = true;
        private Hashtable infoCache = new Hashtable();
        private Hashtable methodCache = new Hashtable();
        private string[] probingDirs = new string[0];
        private string workingDir;

        public AsmBrowser(Assembly asm)
        {
            if (asm == null)
            {
                throw new ArgumentNullException("asm");
            }
            if (ExecuteOptions.options == null)
            {
                ExecuteOptions.options = new ExecuteOptions();
            }
            this.asm = asm;
            try
            {
                if (!CSSUtils.IsDynamic(asm))
                {
                    this.workingDir = Path.GetDirectoryName(asm.Location);
                }
            }
            catch
            {
            }
            AppDomain.CurrentDomain.AssemblyResolve += new System.ResolveEventHandler(this.ResolveEventHandler);
        }

        public object CreateInstance(string typeName)
        {
            if (typeName.IndexOf("*") == -1)
            {
                return this.asm.CreateInstance(typeName);
            }
            if (typeName == "*")
            {
                foreach (Type type in this.asm.GetModules()[0].GetTypes())
                {
                    if (type.FullName != "<InteractiveExpressionClass>")
                    {
                        return this.asm.CreateInstance(type.FullName);
                    }
                }
                return null;
            }
            Type[] typeArray = this.asm.GetModules()[0].FindTypes(Module.FilterTypeName, typeName);
            if (typeArray.Length == 0)
            {
                throw new ApplicationException("Type " + typeName + " cannot be found.");
            }
            return this.asm.CreateInstance(typeArray[0].FullName);
        }

        public void Dispose()
        {
            AppDomain.CurrentDomain.AssemblyResolve -= new System.ResolveEventHandler(this.ResolveEventHandler);
        }

        private MethodInfo FindMethod(string methodName, object[] list)
        {
            Type[] args = new Type[list.Length];
            for (int i = 0; i < list.Length; i++)
            {
                args[i] = list[i].GetType();
            }
            return this.FindMethod(methodName, args);
        }

        private MethodInfo FindMethod(string methodName, Type[] args)
        {
            string[] strArray = methodName.Split(".".ToCharArray());
            if (strArray.Length < 2)
            {
                throw new ApplicationException("Invalid method name format (must be: \"<type>.<method>\")");
            }
            string name = strArray[strArray.Length - 1];
            string str2 = strArray[strArray.Length - 2];
            foreach (Module module in this.asm.GetModules())
            {
                Type[] types;
                if (strArray[0] == "*")
                {
                    types = module.GetTypes();
                }
                else
                {
                    types = module.FindTypes(Module.FilterTypeName, strArray[strArray.Length - 2]);
                }
                foreach (Type type in types)
                {
                    if (type.FullName != "<InteractiveExpressionClass>")
                    {
                        if (name == "*")
                        {
                            MethodInfo[] methods = type.GetMethods(BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Static);
                            if (methods.Length != 0)
                            {
                                return methods[0];
                            }
                        }
                        else
                        {
                            MethodInfo method = type.GetMethod(name, args);
                            if (method != null)
                            {
                                if (str2.EndsWith("*"))
                                {
                                    return method;
                                }
                                if (methodName == (method.DeclaringType.FullName + "." + name))
                                {
                                    return method;
                                }
                            }
                        }
                    }
                }
            }
            string str3 = "Method " + methodName + "(";
            if (args.Length > 0)
            {
                foreach (Type type2 in args)
                {
                    str3 = str3 + type2.ToString() + ", ";
                }
                str3 = str3.Remove(str3.Length - 2, 2);
            }
            throw new ApplicationException(str3 + ") cannot be found.");
        }

        public string[] GetMembersOf(object obj)
        {
            return Reflector.GetMembersOf(obj);
        }

        private FastInvokeDelegate GetMethodInvoker(MethodInfo method)
        {
            FastInvokeDelegate methodInvoker;
            try
            {
                if (!this.methodCache.ContainsKey(method))
                {
                    this.methodCache[method] = new FastInvoker(method);
                }
                methodInvoker = (this.methodCache[method] as FastInvoker).GetMethodInvoker();
            }
            catch (TargetInvocationException exception)
            {
                throw ((exception.InnerException != null) ? exception.InnerException : exception);
            }
            return methodInvoker;
        }

        public FastInvokeDelegate GetMethodInvoker(string methodName)
        {
            MethodInfo method = this.FindMethod(methodName, new Type[0]);
            return this.GetMethodInvoker(method);
        }

        public FastInvokeDelegate GetMethodInvoker(string methodName, object[] list)
        {
            MethodInfo method = this.FindMethod(methodName, list);
            return this.GetMethodInvoker(method);
        }

        public FastInvokeDelegate GetMethodInvoker(string methodName, Type[] list)
        {
            MethodInfo method = this.FindMethod(methodName, list);
            return this.GetMethodInvoker(method);
        }

        public object Invoke(string methodName, params object[] list)
        {
            return this.Invoke(null, methodName, list);
        }

        public object Invoke_Full(object obj, string methodName, params object[] list)
        {
            MethodInfo info;
            object obj2;
            if ((methodName.Split(".".ToCharArray()).Length < 2) && (obj != null))
            {
                methodName = obj.GetType().FullName + "." + methodName;
            }
            MethodSignature key = new MethodSignature(methodName, list);
            if (!this.infoCache.ContainsKey(key))
            {
                info = this.FindMethod(methodName, list);
                this.infoCache[key] = info;
            }
            else
            {
                info = (MethodInfo) this.infoCache[key];
            }
            try
            {
                obj2 = info.Invoke(obj, list);
            }
            catch (TargetInvocationException exception)
            {
                throw ((exception.InnerException != null) ? exception.InnerException : exception);
            }
            return obj2;
        }

        private Assembly ResolveEventHandler(object sender, ResolveEventArgs args)
        {
            Assembly assembly = AssemblyResolver.ResolveAssembly(args.Name, this.workingDir, false);
            if (assembly == null)
            {
                foreach (string str in this.probingDirs)
                {
                    assembly = AssemblyResolver.ResolveAssembly(args.Name, Environment.ExpandEnvironmentVariables(str), false);
                    if (assembly != null)
                    {
                        return assembly;
                    }
                }
            }
            return assembly;
        }

        public bool CachingEnabled
        {
            get
            {
                return this.cachingEnabled;
            }
            set
            {
                this.cachingEnabled = value;
            }
        }

        public string[] ProbingDirs
        {
            get
            {
                return this.probingDirs;
            }
            set
            {
                this.probingDirs = value;
            }
        }

        [StructLayout(LayoutKind.Sequential)]
        private struct MethodSignature
        {
            public string name;
            public Type[] parameters;
            public MethodSignature(string name, params object[] args)
            {
                this.name = name;
                this.parameters = new Type[args.Length];
                for (int i = 0; i < this.parameters.Length; i++)
                {
                    this.parameters[i] = args[i].GetType();
                }
            }

            public static bool operator ==(AsmBrowser.MethodSignature x, AsmBrowser.MethodSignature y)
            {
                return x.Equals(y);
            }

            public static bool operator !=(AsmBrowser.MethodSignature x, AsmBrowser.MethodSignature y)
            {
                return !x.Equals(y);
            }

            public override bool Equals(object obj)
            {
                AsmBrowser.MethodSignature signature = (AsmBrowser.MethodSignature) obj;
                if (this.name != signature.name)
                {
                    return false;
                }
                if (this.parameters.Length != signature.parameters.Length)
                {
                    return false;
                }
                for (int i = 0; i < this.parameters.Length; i++)
                {
                    if (this.parameters[i] != signature.parameters[i])
                    {
                        return false;
                    }
                }
                return true;
            }

            public override int GetHashCode()
            {
                StringBuilder builder = new StringBuilder(this.name);
                foreach (Type type in this.parameters)
                {
                    builder.Append(type.ToString());
                }
                return CSSUtils.GetHashCodeEx(builder.ToString());
            }
        }
    }
}

