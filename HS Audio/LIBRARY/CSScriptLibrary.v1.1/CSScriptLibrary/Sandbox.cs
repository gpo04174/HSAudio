﻿namespace CSScriptLibrary
{
    using System;
    using System.Runtime.CompilerServices;
    using System.Security;
    using System.Security.Permissions;

    public static class Sandbox
    {
        public static void Execute(PermissionSet permissions, Action action)
        {
            permissions.PermitOnly();
            try
            {
                action();
            }
            finally
            {
                CodeAccessPermission.RevertPermitOnly();
            }
        }

        public static PermissionSet With(params IPermission[] permissions)
        {
            PermissionSet set = new PermissionSet(PermissionState.None);
            foreach (IPermission permission in permissions)
            {
                set.AddPermission(permission);
            }
            return set;
        }

        public static PermissionSet With(SecurityPermissionFlag permissionsFlag)
        {
            PermissionSet set = new PermissionSet(PermissionState.None);
            set.AddPermission(new SecurityPermission(permissionsFlag));
            return set;
        }

        public delegate void Action();
    }
}

