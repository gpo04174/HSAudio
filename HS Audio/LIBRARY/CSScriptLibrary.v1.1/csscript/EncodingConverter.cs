﻿namespace csscript
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Globalization;
    using System.Text;

    internal class EncodingConverter : TypeConverter
    {
        private List<string> encodings = new List<string>();

        public EncodingConverter()
        {
            this.encodings.Add("default");
            foreach (EncodingInfo info in Encoding.GetEncodings())
            {
                this.encodings.Add(info.Name);
            }
        }

        public override bool CanConvertFrom(ITypeDescriptorContext context, Type sourceType)
        {
            return true;
        }

        public override object ConvertFrom(ITypeDescriptorContext context, CultureInfo culture, object value)
        {
            return value;
        }

        public override TypeConverter.StandardValuesCollection GetStandardValues(ITypeDescriptorContext context)
        {
            return new TypeConverter.StandardValuesCollection(this.encodings);
        }

        public override bool GetStandardValuesSupported(ITypeDescriptorContext context)
        {
            return true;
        }
    }
}

