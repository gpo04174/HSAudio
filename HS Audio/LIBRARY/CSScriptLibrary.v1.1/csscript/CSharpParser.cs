﻿namespace csscript
{
    using System;
    using System.Collections;
    using System.Globalization;
    using System.IO;
    using System.Runtime.InteropServices;
    using System.Text;
    using System.Threading;

    public class CSharpParser
    {
        private ArrayList args;
        private ArrayList cmdScripts;
        private string code;
        private static char[] codeDelimiters = new char[] { ';', '(', ')', '{' };
        private ArrayList commentRegions;
        private ArrayList compilerOptions;
        public Hashtable CustomDirectives;
        public static char[] DirectiveDelimiters = new char[] { ';', '(', ')', '{', '}', ',' };
        private ArrayList hostOptions;
        private ArrayList ignoreNamespaces;
        private ArrayList imports;
        private ArrayList inits;
        private string modifiedCode;
        private static bool NeedInitEnvironment = true;
        private ArrayList nugets;
        public static bool OpenEndDirectiveSyntax = true;
        private ArrayList precompilers;
        private ArrayList refAssemblies;
        private ArrayList refNamespaces;
        private ArrayList resFiles;
        private ArrayList searchDirs;
        private ArrayList stringRegions;
        private ApartmentState threadingModel;

        public CSharpParser(string code)
        {
            this.stringRegions = new ArrayList();
            this.commentRegions = new ArrayList();
            this.CustomDirectives = new Hashtable();
            this.searchDirs = new ArrayList();
            this.resFiles = new ArrayList();
            this.refAssemblies = new ArrayList();
            this.compilerOptions = new ArrayList();
            this.hostOptions = new ArrayList();
            this.cmdScripts = new ArrayList();
            this.inits = new ArrayList();
            this.nugets = new ArrayList();
            this.refNamespaces = new ArrayList();
            this.ignoreNamespaces = new ArrayList();
            this.imports = new ArrayList();
            this.precompilers = new ArrayList();
            this.args = new ArrayList();
            this.threadingModel = ApartmentState.Unknown;
            this.code = "";
            this.modifiedCode = "";
            InitEnvironment();
            this.Init(code, "");
        }

        public CSharpParser(string script, bool isFile)
        {
            this.stringRegions = new ArrayList();
            this.commentRegions = new ArrayList();
            this.CustomDirectives = new Hashtable();
            this.searchDirs = new ArrayList();
            this.resFiles = new ArrayList();
            this.refAssemblies = new ArrayList();
            this.compilerOptions = new ArrayList();
            this.hostOptions = new ArrayList();
            this.cmdScripts = new ArrayList();
            this.inits = new ArrayList();
            this.nugets = new ArrayList();
            this.refNamespaces = new ArrayList();
            this.ignoreNamespaces = new ArrayList();
            this.imports = new ArrayList();
            this.precompilers = new ArrayList();
            this.args = new ArrayList();
            this.threadingModel = ApartmentState.Unknown;
            this.code = "";
            this.modifiedCode = "";
            InitEnvironment();
            if (!isFile)
            {
                this.Init(script, "");
            }
            else
            {
                using (StreamReader reader = new StreamReader(script, Encoding.UTF8))
                {
                    this.Init(reader.ReadToEnd(), script);
                }
            }
        }

        public CSharpParser(string script, bool isFile, string[] directivesToSearch)
        {
            this.stringRegions = new ArrayList();
            this.commentRegions = new ArrayList();
            this.CustomDirectives = new Hashtable();
            this.searchDirs = new ArrayList();
            this.resFiles = new ArrayList();
            this.refAssemblies = new ArrayList();
            this.compilerOptions = new ArrayList();
            this.hostOptions = new ArrayList();
            this.cmdScripts = new ArrayList();
            this.inits = new ArrayList();
            this.nugets = new ArrayList();
            this.refNamespaces = new ArrayList();
            this.ignoreNamespaces = new ArrayList();
            this.imports = new ArrayList();
            this.precompilers = new ArrayList();
            this.args = new ArrayList();
            this.threadingModel = ApartmentState.Unknown;
            this.code = "";
            this.modifiedCode = "";
            if (!isFile)
            {
                this.Init(script, "", directivesToSearch);
            }
            else
            {
                using (StreamReader reader = new StreamReader(script))
                {
                    this.Init(reader.ReadToEnd(), script, directivesToSearch);
                }
            }
        }

        private int[] AllRawIndexOf(string pattern, int startIndex, int endIndex)
        {
            ArrayList list = new ArrayList();
            for (int i = this.code.IndexOf(pattern, startIndex, (int) (endIndex - startIndex)); i != -1; i = this.code.IndexOf(pattern, i + 1, (int) (endIndex - (i + 1))))
            {
                list.Add(i);
            }
            return (int[]) list.ToArray(typeof(int));
        }

        public void DoRenaming(string[][] renamingMap, bool preserveMain)
        {
            int stratPos = -1;
            ArrayList list = new ArrayList();
            int startIndex = this.FindStatement("Main", 0);
            while ((!preserveMain && (startIndex != -1)) && (stratPos == -1))
            {
                int charPos = this.code.LastIndexOfAny("{};".ToCharArray(), startIndex, startIndex);
                do
                {
                    if (!this.IsComment(charPos) || !this.IsString(charPos))
                    {
                        foreach (string str2 in this.StripNonStringStatementComments(this.code.Substring(charPos + 1, (startIndex - charPos) - 1)).Trim().Split("\n\r\t ".ToCharArray()))
                        {
                            if (str2.Trim() == "static")
                            {
                                stratPos = startIndex;
                            }
                        }
                        break;
                    }
                    charPos = this.code.LastIndexOfAny("{};".ToCharArray(), charPos - 1, charPos - 1);
                }
                while ((charPos != -1) && (stratPos == -1));
                startIndex = this.FindStatement("Main", startIndex + 1);
            }
            if (stratPos != -1)
            {
                list.Add(new RenamingInfo(stratPos, stratPos + "Main".Length, "i_Main"));
            }
            foreach (string[] strArray2 in renamingMap)
            {
                stratPos = -1;
                for (startIndex = this.FindStatement(strArray2[0], 0); (startIndex != -1) && (stratPos == -1); startIndex = this.FindStatement(strArray2[0], startIndex + 1))
                {
                    int num4 = this.code.LastIndexOfAny("{};".ToCharArray(), startIndex, startIndex);
                    do
                    {
                        if (!this.IsComment(num4) || !this.IsString(num4))
                        {
                            this.code.Substring(num4 + 1, (startIndex - num4) - 1);
                            foreach (string str4 in this.StripNonStringStatementComments(this.code.Substring(num4 + 1, (startIndex - num4) - 1)).Trim().Split("\n\r\t ".ToCharArray()))
                            {
                                if (str4.Trim() == "namespace")
                                {
                                    stratPos = startIndex;
                                    break;
                                }
                            }
                            break;
                        }
                        num4 = this.code.LastIndexOfAny("{};".ToCharArray(), num4 - 1, num4 - 1);
                    }
                    while ((num4 != -1) && (stratPos == -1));
                }
                if (stratPos != -1)
                {
                    list.Add(new RenamingInfo(stratPos, stratPos + strArray2[0].Length, strArray2[1]));
                }
            }
            list.Sort(new RenamingInfoComparer());
            StringBuilder builder = new StringBuilder();
            for (int i = 0; i < list.Count; i++)
            {
                RenamingInfo info = (RenamingInfo) list[i];
                int num6 = ((i - 1) >= 0) ? ((RenamingInfo) list[i - 1]).endPos : 0;
                builder.Append(this.code.Substring(num6, info.stratPos - num6));
                builder.Append(info.newValue);
                if (i == (list.Count - 1))
                {
                    builder.Append(this.code.Substring(info.endPos, this.code.Length - info.endPos));
                }
            }
            this.modifiedCode = builder.ToString();
        }

        public static string EscapeDirectiveDelimiters(string text)
        {
            foreach (char ch in DirectiveDelimiters)
            {
                text = text.Replace(ch.ToString(), new string(ch, 2));
            }
            return text;
        }

        private int FindStatement(string pattern, int start)
        {
            if (this.code.Length != 0)
            {
                for (int i = this.IndexOf(pattern, start, this.code.Length - 1); i != -1; i = this.IndexOf(pattern, i + 1, this.code.Length - 1))
                {
                    if (!this.IsString(i))
                    {
                        return i;
                    }
                }
            }
            return -1;
        }

        private string[] GetRawStatements(string pattern, int endIndex)
        {
            return this.GetRawStatements(this.code, pattern, endIndex, false);
        }

        private string[] GetRawStatements(string codeToAnalyse, string pattern, int endIndex, bool ignoreComments)
        {
            ArrayList list = new ArrayList();
            int index = codeToAnalyse.IndexOf(pattern);
            int num2 = -1;
            while ((index != -1) && (index <= endIndex))
            {
                if (this.IsDirectiveToken(index, pattern.Length) && (!ignoreComments || (ignoreComments && !this.IsComment(index))))
                {
                    index += pattern.Length;
                    if (OpenEndDirectiveSyntax)
                    {
                        num2 = this.IndexOfDelimiter(index, codeToAnalyse.Length - 1, new char[] { '\n', ';' });
                    }
                    else
                    {
                        num2 = this.IndexOfDelimiter(index, codeToAnalyse.Length - 1, new char[] { ';' });
                    }
                    if (num2 != -1)
                    {
                        list.Add(codeToAnalyse.Substring(index, num2 - index).Trim());
                    }
                }
                index = codeToAnalyse.IndexOf(pattern, (int) (index + 1));
            }
            return (string[]) list.ToArray(typeof(string));
        }

        private int IndexOf(string pattern, int startIndex, int endIndex)
        {
            for (int i = this.code.IndexOf(pattern, startIndex, (int) (endIndex - startIndex)); i != -1; i = this.code.IndexOf(pattern, i + 1, (int) (endIndex - (i + 1))))
            {
                if (!this.IsComment(i) && this.IsToken(i, pattern.Length))
                {
                    return i;
                }
            }
            return -1;
        }

        private int IndexOfDelimiter(int startIndex, int endIndex, params char[] delimiters)
        {
            char ch = '\0';
            for (int i = startIndex; i <= endIndex; i++)
            {
                char ch2 = this.code[i];
                if (ch != '\0')
                {
                    if (ch != ch2)
                    {
                        return (i - 1);
                    }
                    ch = '\0';
                }
                else
                {
                    foreach (char ch3 in delimiters)
                    {
                        if (this.code[i] == ch3)
                        {
                            ch = ch3;
                            break;
                        }
                    }
                }
            }
            return -1;
        }

        public void Init(string code, string file)
        {
            this.Init(code, file, null);
        }

        public void Init(string code, string file, string[] directivesToSearch)
        {
            string currentDirectory = Environment.CurrentDirectory;
            if (file != "")
            {
                currentDirectory = Path.GetDirectoryName(file);
            }
            this.code = code;
            this.NoteCommentsAndStrings();
            int index = code.IndexOf("class");
            int endIndex = code.Length - 1;
            while (index != -1)
            {
                if (this.IsToken(index, "class".Length) && !this.IsComment(index))
                {
                    endIndex = index;
                    break;
                }
                index = code.IndexOf("class", (int) (index + 1));
            }
            foreach (string str2 in this.GetRawStatements("//css_args", endIndex))
            {
                foreach (string str3 in SplitByDelimiter(str2, new char[] { ',' }))
                {
                    string str4 = str3.Trim();
                    if (str4.StartsWith("\""))
                    {
                        str4 = str4.Substring(1);
                    }
                    if (str4.EndsWith("\""))
                    {
                        str4 = str4.Remove(str4.Length - 1, 1);
                    }
                    this.args.Add(str4);
                }
            }
            foreach (string str5 in this.GetRawStatements("//css_pre", endIndex))
            {
                this.cmdScripts.Add(new CmdScriptInfo(str5.Trim(), true, file));
            }
            foreach (string str6 in this.GetRawStatements("//css_prescript", endIndex))
            {
                this.cmdScripts.Add(new CmdScriptInfo(str6.Trim(), true, file));
            }
            foreach (string str7 in this.GetRawStatements("//css_post", endIndex))
            {
                this.cmdScripts.Add(new CmdScriptInfo(str7.Trim(), false, file));
            }
            foreach (string str8 in this.GetRawStatements("//css_postscript", endIndex))
            {
                this.cmdScripts.Add(new CmdScriptInfo(str8.Trim(), false, file));
            }
            foreach (string str9 in this.GetRawStatements("//css_init", endIndex))
            {
                this.inits.Add(new InitInfo(str9.Trim()));
            }
            foreach (string str10 in this.GetRawStatements("//css_nuget", endIndex))
            {
                foreach (string str11 in SplitByDelimiter(str10, new char[] { ',' }))
                {
                    this.nugets.Add(str11.Trim());
                }
            }
            foreach (string str12 in this.GetRawStatements("//css_import", endIndex))
            {
                this.imports.Add(new ImportInfo(Environment.ExpandEnvironmentVariables(str12).Trim(), file));
            }
            foreach (string str13 in this.GetRawStatements("//css_imp", endIndex))
            {
                this.imports.Add(new ImportInfo(Environment.ExpandEnvironmentVariables(str13).Trim(), file));
            }
            foreach (string str14 in this.GetRawStatements("//css_include", endIndex))
            {
                this.imports.Add(new ImportInfo(Environment.ExpandEnvironmentVariables(str14).Trim() + ",preserve_main", file));
            }
            foreach (string str15 in this.GetRawStatements("//css_inc", endIndex))
            {
                this.imports.Add(new ImportInfo(Environment.ExpandEnvironmentVariables(str15).Trim() + ",preserve_main", file));
            }
            foreach (string str16 in this.GetRawStatements("//css_reference", endIndex))
            {
                this.refAssemblies.Add(Environment.ExpandEnvironmentVariables(UnescapeDirectiveDelimiters(str16)).Trim());
            }
            foreach (string str17 in this.GetRawStatements("//css_ref", endIndex))
            {
                this.refAssemblies.Add(Environment.ExpandEnvironmentVariables(UnescapeDirectiveDelimiters(str17)).Trim());
            }
            foreach (string str18 in this.GetRawStatements("//css_precompiler", endIndex))
            {
                this.precompilers.Add(Environment.ExpandEnvironmentVariables(UnescapeDirectiveDelimiters(str18)).Trim());
            }
            foreach (string str19 in this.GetRawStatements("//css_pc", endIndex))
            {
                this.precompilers.Add(Environment.ExpandEnvironmentVariables(UnescapeDirectiveDelimiters(str19)).Trim());
            }
            foreach (string str20 in this.GetRawStatements("//css_co", endIndex))
            {
                this.compilerOptions.Add(Environment.ExpandEnvironmentVariables(UnescapeDirectiveDelimiters(str20)).Trim());
            }
            foreach (string str21 in this.GetRawStatements("//css_host", endIndex))
            {
                this.hostOptions.Add(Environment.ExpandEnvironmentVariables(UnescapeDirectiveDelimiters(str21)).Trim());
            }
            foreach (string str22 in this.GetRawStatements("//css_ignore_namespace", endIndex))
            {
                this.ignoreNamespaces.Add(str22.Trim());
            }
            foreach (string str23 in this.GetRawStatements("//css_ignore_ns", endIndex))
            {
                this.ignoreNamespaces.Add(str23.Trim());
            }
            foreach (string str24 in this.GetRawStatements("//css_resource", endIndex))
            {
                this.resFiles.Add(UnescapeDirectiveDelimiters(str24).Trim());
            }
            foreach (string str25 in this.GetRawStatements("//css_res", endIndex))
            {
                this.resFiles.Add(UnescapeDirectiveDelimiters(str25).Trim());
            }
            foreach (string str26 in this.GetRawStatements("//css_searchdir", endIndex))
            {
                this.searchDirs.AddRange(CSSUtils.GetDirectories(currentDirectory, Environment.ExpandEnvironmentVariables(UnescapeDirectiveDelimiters(str26)).Trim()));
            }
            foreach (string str27 in this.GetRawStatements("//css_dir", endIndex))
            {
                this.searchDirs.AddRange(CSSUtils.GetDirectories(currentDirectory, Environment.ExpandEnvironmentVariables(UnescapeDirectiveDelimiters(str27)).Trim()));
            }
            foreach (string str28 in this.GetRawStatements(code, "using", endIndex, true))
            {
                if (!str28.StartsWith("("))
                {
                    this.refNamespaces.Add(str28.Trim().Replace("\t", "").Replace("\r", "").Replace("\n", "").Replace(" ", ""));
                }
            }
            for (index = code.IndexOf("TAThread]"); ((index != -1) && (index > 0)) && (this.threadingModel == ApartmentState.Unknown); index = code.IndexOf("TAThread]", (int) (index + 1)))
            {
                if ((!this.IsComment(index - 1) && !this.IsString(index - 1)) && this.IsToken(index - 2, "??TAThread]".Length))
                {
                    if (code[index - 1] == 'S')
                    {
                        this.threadingModel = ApartmentState.STA;
                    }
                    else if (code[index - 1] == 'M')
                    {
                        this.threadingModel = ApartmentState.MTA;
                    }
                }
            }
            this.CustomDirectives.Clear();
            if (directivesToSearch != null)
            {
                foreach (string str29 in directivesToSearch)
                {
                    this.CustomDirectives[str29] = new ArrayList();
                    foreach (string str30 in this.GetRawStatements(str29, endIndex))
                    {
                        (this.CustomDirectives[str29] as ArrayList).Add(str30.Trim());
                    }
                }
            }
        }

        private static void InitEnvironment()
        {
            if (NeedInitEnvironment)
            {
                if (Environment.GetEnvironmentVariable("css_nuget") == null)
                {
                    Environment.SetEnvironmentVariable("css_nuget", NuGet.NuGetCacheView);
                }
                NeedInitEnvironment = false;
            }
        }

        private bool IsComment(int charPos)
        {
            foreach (int[] numArray in this.commentRegions)
            {
                if (charPos < numArray[0])
                {
                    return false;
                }
                if ((numArray[0] <= charPos) && (charPos <= numArray[1]))
                {
                    return true;
                }
            }
            return false;
        }

        private bool IsDirectiveToken(int startPos, int length)
        {
            if (this.code.Length < (startPos + length))
            {
                return false;
            }
            if ((startPos != 0) && !char.IsWhiteSpace(this.code[startPos - 1]))
            {
                return false;
            }
            if ((this.code.Length > (startPos + length)) && !char.IsWhiteSpace(this.code[startPos + length]))
            {
                return false;
            }
            int num = startPos + length;
            char ch = '\0';
            for (int i = startPos; i <= num; i++)
            {
                char c = this.code[i];
                if (ch != '\0')
                {
                    if (ch != c)
                    {
                        return false;
                    }
                    ch = '\0';
                }
                else if (IsOneOf(c, DirectiveDelimiters))
                {
                    ch = c;
                }
            }
            return true;
        }

        internal static bool IsOneOf(char c, char[] items)
        {
            foreach (char ch in items)
            {
                if (c == ch)
                {
                    return true;
                }
            }
            return false;
        }

        private bool IsString(int charPos)
        {
            foreach (int[] numArray in this.stringRegions)
            {
                if (charPos < numArray[0])
                {
                    return false;
                }
                if ((numArray[0] <= charPos) && (charPos <= numArray[1]))
                {
                    return true;
                }
            }
            return false;
        }

        private bool IsToken(int startPos, int length)
        {
            if (this.code.Length < (startPos + length))
            {
                return false;
            }
            if (((startPos != 0) && !char.IsWhiteSpace(this.code[startPos - 1])) && !IsOneOf(this.code[startPos - 1], codeDelimiters))
            {
                return false;
            }
            if (((this.code.Length > (startPos + length)) && !char.IsWhiteSpace(this.code[startPos + length])) && !IsOneOf(this.code[startPos + length], codeDelimiters))
            {
                return false;
            }
            return true;
        }

        private bool IsTokenOld(int startPos, int length)
        {
            if (this.code.Length < (startPos + length))
            {
                return false;
            }
            int startIndex = (startPos != 0) ? (startPos - 1) : 0;
            int num2 = (this.code.Length == (startPos + length)) ? (startPos + length) : ((startPos + length) + 1);
            string str = this.code.Substring(startPos, length);
            return (this.code.Substring(startIndex, num2 - startIndex).Replace(";", "").Replace("(", "").Replace(")", "").Replace("{", "").Trim().Length == str.Length);
        }

        private void NoteCommentsAndStrings()
        {
            ArrayList list = new ArrayList();
            int endIndex = -1;
            int index = -1;
            int num3 = -1;
            int startIndex = 0;
            string str = "";
            string str2 = "";
            int num5 = -1;
            int num6 = -1;
            do
            {
                index = this.code.IndexOf("//", startIndex);
                num3 = this.code.IndexOf("/*", startIndex);
                if (index == Math.Min((index != -1) ? index : 0x7fff, (num3 != -1) ? num3 : 0x7fff))
                {
                    endIndex = index;
                    str2 = "//";
                    str = "\n";
                }
                else
                {
                    endIndex = num3;
                    str2 = "/*";
                    str = "*/";
                }
                if (endIndex != -1)
                {
                    num5 = this.code.IndexOf(str, (int) (endIndex + str2.Length));
                }
                if ((endIndex != -1) && (num5 != -1))
                {
                    int num7 = (this.commentRegions.Count == 0) ? 0 : (((int[]) this.commentRegions[this.commentRegions.Count - 1])[1] + 1);
                    int[] c = this.AllRawIndexOf("\"", num7, endIndex);
                    if ((c.Length % 2) != 0)
                    {
                        startIndex = endIndex + str2.Length;
                    }
                    else
                    {
                        this.commentRegions.Add(new int[] { endIndex, num5 });
                        list.AddRange(c);
                        startIndex = num5 + str.Length;
                    }
                }
            }
            while ((endIndex != -1) && (num5 != -1));
            if ((num6 != 0) && (startIndex < this.code.Length))
            {
                list.AddRange(this.AllRawIndexOf("\"", startIndex, this.code.Length));
            }
            for (int i = 0; i < list.Count; i++)
            {
                if ((i + 1) < this.stringRegions.Count)
                {
                    this.stringRegions.Add(new int[] { (int) list[i], (int) list[i + 1] });
                }
                else
                {
                    this.stringRegions.Add(new int[] { (int) list[i], -1 });
                }
                i++;
            }
        }

        internal static string[] SplitByDelimiter(string text, params char[] delimiters)
        {
            StringBuilder builder = new StringBuilder();
            ArrayList list = new ArrayList();
            char ch = '\0';
            foreach (char ch2 in text)
            {
                if (ch != '\0')
                {
                    if (ch2 != ch)
                    {
                        string str = builder.ToString();
                        if ((str != null) && (str.Trim() != ""))
                        {
                            list.Add(str.Trim());
                        }
                        builder.Length = 0;
                        if (IsOneOf(ch2, delimiters))
                        {
                            ch = ch2;
                        }
                        else
                        {
                            ch = '\0';
                        }
                    }
                    else
                    {
                        ch = '\0';
                    }
                }
                else if (IsOneOf(ch2, delimiters))
                {
                    ch = ch2;
                }
                if (ch == '\0')
                {
                    builder.Append(ch2);
                }
            }
            if (builder.Length > 0)
            {
                list.Add(builder.ToString());
            }
            return (string[]) list.ToArray(typeof(string));
        }

        private string StripNonStringStatementComments(string text)
        {
            StringBuilder builder = new StringBuilder();
            int num = -1;
            int index = -1;
            int num3 = -1;
            int startIndex = 0;
            string str = "";
            string str2 = "";
            int num5 = -1;
            int num6 = -1;
            do
            {
                index = text.IndexOf("//", startIndex);
                num3 = text.IndexOf("/*", startIndex);
                if (index == Math.Min((index != -1) ? index : 0x7fff, (num3 != -1) ? num3 : 0x7fff))
                {
                    num = index;
                    str2 = "//";
                    str = "\n";
                }
                else
                {
                    num = num3;
                    str2 = "/*";
                    str = "*/";
                }
                if (num != -1)
                {
                    num5 = text.IndexOf(str, (int) (num + str2.Length));
                }
                if ((num != -1) && (num5 != -1))
                {
                    string str3 = text.Substring(startIndex, num - startIndex);
                    builder.Append(str3);
                    startIndex = num5 + str.Length;
                }
            }
            while ((num != -1) && (num5 != -1));
            if ((num6 != 0) && (startIndex < this.code.Length))
            {
                string str4 = text.Substring(startIndex, text.Length - startIndex);
                builder.Append(str4);
            }
            return builder.ToString();
        }

        public static string UnescapeDirectiveDelimiters(string text)
        {
            foreach (char ch in DirectiveDelimiters)
            {
                text = text.Replace(new string(ch, 2), ch.ToString());
            }
            return text;
        }

        public string[] Args
        {
            get
            {
                return (string[]) this.args.ToArray(typeof(string));
            }
        }

        public CmdScriptInfo[] CmdScripts
        {
            get
            {
                return (CmdScriptInfo[]) this.cmdScripts.ToArray(typeof(CmdScriptInfo));
            }
        }

        public string Code
        {
            get
            {
                return this.code;
            }
        }

        public string[] CompilerOptions
        {
            get
            {
                return (string[]) this.compilerOptions.ToArray(typeof(string));
            }
        }

        public string[] ExtraSearchDirs
        {
            get
            {
                return (string[]) this.searchDirs.ToArray(typeof(string));
            }
        }

        public string[] HostOptions
        {
            get
            {
                return (string[]) this.hostOptions.ToArray(typeof(string));
            }
        }

        public string[] IgnoreNamespaces
        {
            get
            {
                return (string[]) this.ignoreNamespaces.ToArray(typeof(string));
            }
        }

        public ImportInfo[] Imports
        {
            get
            {
                return (ImportInfo[]) this.imports.ToArray(typeof(ImportInfo));
            }
        }

        public InitInfo[] Inits
        {
            get
            {
                return (InitInfo[]) this.inits.ToArray(typeof(InitInfo));
            }
        }

        public string ModifiedCode
        {
            get
            {
                return this.modifiedCode;
            }
        }

        public string[] NuGets
        {
            get
            {
                return (string[]) this.nugets.ToArray(typeof(string));
            }
        }

        public string[] Precompilers
        {
            get
            {
                return (string[]) this.precompilers.ToArray(typeof(string));
            }
        }

        public string[] RefAssemblies
        {
            get
            {
                return (string[]) this.refAssemblies.ToArray(typeof(string));
            }
        }

        public string[] References
        {
            get
            {
                ArrayList list = new ArrayList();
                list.AddRange(this.refAssemblies);
                list.AddRange(this.refNamespaces);
                return (string[]) list.ToArray(typeof(string));
            }
        }

        public string[] RefNamespaces
        {
            get
            {
                return (string[]) this.refNamespaces.ToArray(typeof(string));
            }
        }

        public string[] ResFiles
        {
            get
            {
                return (string[]) this.resFiles.ToArray(typeof(string));
            }
        }

        public ApartmentState ThreadingModel
        {
            get
            {
                return this.threadingModel;
            }
        }

        public class CmdScriptInfo
        {
            public bool abortOnError = true;
            public string[] args;
            public bool preScript;

            public CmdScriptInfo(string statement, bool preScript, string parentScript)
            {
                this.preScript = preScript;
                int num = -1;
                int index = statement.IndexOf("(");
                if (index != -1)
                {
                    ArrayList list = new ArrayList();
                    list.Add(CSSUtils.cmdFlagPrefix + "nl");
                    list.Add(statement.Substring(0, index).Trim());
                    num = statement.LastIndexOf(")");
                    if (num == -1)
                    {
                        throw new ApplicationException("Cannot parse statement (" + statement + ").");
                    }
                    foreach (string str2 in CSharpParser.SplitByDelimiter(statement.Substring(index + 1, (num - index) - 1), new char[] { ',' }))
                    {
                        string str = str2.Trim();
                        if (str != string.Empty)
                        {
                            list.Add(str.StartsWith("$this") ? ((str == "$this.name") ? Path.GetFileNameWithoutExtension(parentScript) : parentScript) : str);
                        }
                    }
                    this.args = (string[]) list.ToArray(typeof(string));
                    if (statement.Substring(num + 1).Trim().Replace(",", "") == "ignore")
                    {
                        this.abortOnError = false;
                    }
                }
                else
                {
                    int length = statement.LastIndexOfAny(new char[] { ' ', '\t', ';' });
                    if (length != -1)
                    {
                        this.args = new string[] { CSSUtils.cmdFlagPrefix + "nl", statement.Substring(0, length) };
                    }
                    else
                    {
                        this.args = new string[] { CSSUtils.cmdFlagPrefix + "nl", statement };
                    }
                }
            }
        }

        public class ImportInfo
        {
            public string file;
            public bool preserveMain;
            public string[][] renaming;

            public ImportInfo(string statement, string parentScript)
            {
                ArrayList list = new ArrayList();
                string[] strArray = CSharpParser.SplitByDelimiter(statement.Replace("($this.name)", Path.GetFileNameWithoutExtension(parentScript)).Replace("\t", "").Trim(), CSharpParser.DirectiveDelimiters);
                this.file = strArray[0];
                int index = 1;
                while (index < strArray.Length)
                {
                    strArray[index] = strArray[index].Trim();
                    if ((strArray[index] != "rename_namespace") || ((index + 2) >= strArray.Length))
                    {
                        if (strArray[index] != "preserve_main")
                        {
                            throw new ApplicationException("Cannot parse \"//css_import...\"");
                        }
                        this.preserveMain = true;
                        index++;
                    }
                    else
                    {
                        string[] strArray2 = new string[] { strArray[index + 1], strArray[index + 2].Replace(")", "") };
                        list.Add(strArray2);
                        index += 3;
                        continue;
                    }
                }
                if (list.Count == 0)
                {
                    this.renaming = new string[0][];
                }
                else
                {
                    this.renaming = (string[][]) list.ToArray(typeof(string[]));
                }
            }
        }

        public class InitInfo
        {
            private static char[] argDelimiters = new char[] { ',' };
            public bool CoInitializeSecurity;
            public int EoAuthnCap = 0x40;
            public int RpcImpLevel = 3;
            private static char[] tokenDelimiters = new char[] { '(', ')' };

            public InitInfo(string statement)
            {
                bool flag = !statement.StartsWith("CoInitializeSecurity");
                if (!flag)
                {
                    this.CoInitializeSecurity = true;
                    string[] strArray = statement.Split(tokenDelimiters);
                    if ((strArray.Length == 1) && (strArray[0].Trim() == "CoInitializeSecurity"))
                    {
                        return;
                    }
                    flag = strArray.Length < 2;
                    if (!flag)
                    {
                        string[] strArray2 = strArray[1].Split(argDelimiters);
                        if ((strArray2.Length != 1) || !(strArray2[0].Trim() == ""))
                        {
                            flag = ((strArray2.Length != 2) || !this.TryParseInt(strArray2[0], out this.RpcImpLevel)) || !this.TryParseInt(strArray2[1], out this.EoAuthnCap);
                        }
                    }
                }
                if (flag)
                {
                    throw new ApplicationException("Cannot parse //css_init directive. '" + statement + "' is in unexpected format.");
                }
            }

            private bool TryParseInt(string text, out int value)
            {
                text = text.TrimStart(new char[0]);
                if (text.StartsWith("0x", StringComparison.Ordinal))
                {
                    return int.TryParse(text.Substring(2), NumberStyles.HexNumber, CultureInfo.InvariantCulture, out value);
                }
                return int.TryParse(text, out value);
            }
        }

        private class RenamingInfo
        {
            public int endPos;
            public string newValue;
            public int stratPos;

            public RenamingInfo(int stratPos, int endPos, string newValue)
            {
                this.stratPos = stratPos;
                this.endPos = endPos;
                this.newValue = newValue;
            }
        }

        private class RenamingInfoComparer : IComparer
        {
            int IComparer.Compare(object x, object y)
            {
                int num = (x == null) ? -1 : ((y == null) ? 1 : 0);
                if (num == 0)
                {
                    return Comparer.Default.Compare(((CSharpParser.RenamingInfo) x).stratPos, ((CSharpParser.RenamingInfo) y).stratPos);
                }
                return num;
            }
        }
    }
}

