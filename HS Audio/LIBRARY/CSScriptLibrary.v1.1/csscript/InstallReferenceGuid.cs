﻿namespace csscript
{
    using System;
    using System.Runtime.InteropServices;

    [ComVisible(false)]
    internal class InstallReferenceGuid
    {
        public static readonly Guid FilePathGuid = new Guid("b02f9d65-fb77-4f7a-afa5-b391309f11c9");
        public static readonly Guid OpaqueStringGuid = new Guid("2ec93463-b0c3-45e1-8364-327e96aea856");
        public static readonly Guid UninstallSubkeyGuid = new Guid("8cedc215-ac4b-488b-93c0-a50a49cb2fb8");

        public static bool IsValidGuidScheme(Guid guid)
        {
            if ((!guid.Equals(UninstallSubkeyGuid) && !guid.Equals(FilePathGuid)) && !guid.Equals(OpaqueStringGuid))
            {
                return guid.Equals(Guid.Empty);
            }
            return true;
        }
    }
}

