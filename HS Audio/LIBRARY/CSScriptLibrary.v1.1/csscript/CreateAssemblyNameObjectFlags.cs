﻿namespace csscript
{
    using System;

    internal enum CreateAssemblyNameObjectFlags
    {
        CANOF_DEFAULT,
        CANOF_PARSE_DISPLAY_NAME,
        CANOF_SET_DEFAULT_VALUES
    }
}

