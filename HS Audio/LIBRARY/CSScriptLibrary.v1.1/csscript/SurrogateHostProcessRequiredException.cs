﻿namespace csscript
{
    using System;

    internal class SurrogateHostProcessRequiredException : ApplicationException
    {
        private string[] scriptArgs;
        private string scriptAssembly;
        private bool startDebugger;

        public SurrogateHostProcessRequiredException(string scriptAssembly, string[] scriptArgs, bool startDebugger)
        {
            this.ScriptAssembly = scriptAssembly;
            this.StartDebugger = startDebugger;
            this.ScriptArgs = scriptArgs;
        }

        public string[] ScriptArgs
        {
            get
            {
                return this.scriptArgs;
            }
            set
            {
                this.scriptArgs = value;
            }
        }

        public string ScriptAssembly
        {
            get
            {
                return this.scriptAssembly;
            }
            set
            {
                this.scriptAssembly = value;
            }
        }

        public bool StartDebugger
        {
            get
            {
                return this.startDebugger;
            }
            set
            {
                this.startDebugger = value;
            }
        }
    }
}

