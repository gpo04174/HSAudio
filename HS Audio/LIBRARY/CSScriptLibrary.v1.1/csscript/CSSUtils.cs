﻿namespace csscript
{
    using CSScriptLibrary;
    using Microsoft.CSharp;
    using System;
    using System.CodeDom.Compiler;
    using System.Collections;
    using System.Globalization;
    using System.IO;
    using System.Reflection;
    using System.Runtime.CompilerServices;
    using System.Text;
    using System.Threading;

    internal class CSSUtils
    {
        internal const string noDefaultPrecompilerSwitch = "nodefault";

        internal static string[] CollectPrecompillers(CSharpParser parser, ExecuteOptions options)
        {
            ArrayList list = new ArrayList();
            list.AddRange(options.preCompilers.Split(new char[] { ',' }));
            foreach (string str in parser.Precompilers)
            {
                list.AddRange(str.Split(new char[] { ',' }));
            }
            return Utils.RemoveDuplicates((string[]) list.ToArray(typeof(string)));
        }

        public static Assembly CompilePrecompilerScript(string sourceFile, string[] searchDirs)
        {
            Assembly assembly2;
            try
            {
                string s = Path.Combine(CSExecutor.GetCacheDirectory(sourceFile), Path.GetFileName(sourceFile) + ".compiled");
                using (Mutex mutex = new Mutex(false, "CSSPrecompiling." + GetHashCodeEx(s)))
                {
                    mutex.WaitOne(0xbb8, false);
                    if (File.Exists(s))
                    {
                        if (File.GetLastWriteTimeUtc(sourceFile) <= File.GetLastWriteTimeUtc(s))
                        {
                            return Assembly.LoadFrom(s);
                        }
                        Utils.FileDelete(s, true);
                    }
                    ScriptParser parser = new ScriptParser(sourceFile, searchDirs);
                    CompilerParameters options = new CompilerParameters {
                        IncludeDebugInformation = true,
                        GenerateExecutable = false,
                        GenerateInMemory = false,
                        OutputAssembly = s
                    };
                    ArrayList list = new ArrayList();
                    foreach (string str2 in parser.ReferencedNamespaces)
                    {
                        foreach (string str3 in AssemblyResolver.FindAssembly(str2, searchDirs))
                        {
                            list.Add(str3);
                        }
                    }
                    foreach (string str4 in parser.ReferencedAssemblies)
                    {
                        if (str4.StartsWith("\"") && str4.EndsWith("\""))
                        {
                            string str5 = str4.Replace("\"", "");
                            list.Add(str5);
                        }
                        else
                        {
                            string name = Utils.RemoveAssemblyExtension(str4);
                            string[] strArray = AssemblyResolver.FindAssembly(name, searchDirs);
                            if (strArray.Length > 0)
                            {
                                foreach (string str7 in strArray)
                                {
                                    list.Add(str7);
                                }
                            }
                            else
                            {
                                list.Add(name + ".dll");
                            }
                        }
                    }
                    foreach (string str8 in Utils.RemovePathDuplicates((string[]) list.ToArray(typeof(string))))
                    {
                        options.ReferencedAssemblies.Add(str8);
                    }
                    CompilerResults results = new CSharpCodeProvider().CreateCompiler().CompileAssemblyFromFile(options, sourceFile);
                    if (results.Errors.Count != 0)
                    {
                        throw CompilerException.Create(results.Errors, true);
                    }
                    if (!File.Exists(s))
                    {
                        throw new Exception("Unknown building error");
                    }
                    File.SetLastWriteTimeUtc(s, File.GetLastWriteTimeUtc(sourceFile));
                    assembly2 = Assembly.LoadFrom(s);
                }
            }
            catch (Exception exception)
            {
                throw new ApplicationException("Cannot load precompiler " + sourceFile + ": " + exception.Message);
            }
            return assembly2;
        }

        public static string ConvertSimpleExpToRegExp(string simpleExp)
        {
            StringBuilder builder = new StringBuilder();
            builder.Append("^");
            foreach (char ch in simpleExp)
            {
                switch (ch)
                {
                    case ' ':
                    case '#':
                    case '$':
                    case '(':
                    case ')':
                    case '+':
                    case '.':
                    case '[':
                    case '\\':
                    case '^':
                    case '{':
                    case '|':
                    {
                        builder.Append('\\').Append(ch);
                        continue;
                    }
                    case '*':
                    {
                        builder.Append(".*");
                        continue;
                    }
                    case '?':
                    {
                        builder.Append(".");
                        continue;
                    }
                }
                builder.Append(ch);
            }
            builder.Append("$");
            return builder.ToString();
        }

        public static string FindFile(string file, string[] searchDirs)
        {
            if (File.Exists(file))
            {
                return Path.GetFullPath(file);
            }
            if (!Path.IsPathRooted(file))
            {
                foreach (string str in searchDirs)
                {
                    if (File.Exists(Path.Combine(str, file)))
                    {
                        return Path.Combine(str, file);
                    }
                }
            }
            return null;
        }

        public static string FindImlementationFile(string file, string[] searchDirs)
        {
            string str = FindFile(file, searchDirs);
            if ((str == null) && !Path.HasExtension(file))
            {
                str = FindFile(file + ".cs", searchDirs);
                if (str == null)
                {
                    str = FindFile(file + ".dll", searchDirs);
                }
            }
            return str;
        }

        public static string GenerateAutoclass(string file)
        {
            StringBuilder builder = new StringBuilder(0x1000);
            builder.Append("//Auto-generated file\r\n");
            bool flag = false;
            using (StreamReader reader = new StreamReader(file, Encoding.UTF8))
            {
                string str;
                while ((str = reader.ReadLine()) != null)
                {
                    if ((!flag && !str.TrimStart(new char[0]).StartsWith("using ")) && (!str.StartsWith("//") && (str.Trim() != "")))
                    {
                        flag = true;
                        builder.Append("   public class ScriptClass\r\n");
                        builder.Append("   {\r\n");
                        builder.Append("   static public ");
                    }
                    builder.Append(str);
                    builder.Append("\r\n");
                }
            }
            builder.Append("   }\r\n");
            return SaveAsAutogeneratedScript(builder.ToString(), file);
        }

        internal static int GenerateCompilationContext(CSharpParser parser, ExecuteOptions options)
        {
            string[] strArray = CollectPrecompillers(parser, options);
            StringBuilder builder = new StringBuilder();
            foreach (string str in strArray)
            {
                if (str != "")
                {
                    builder.Append(FindImlementationFile(str, options.searchDirs));
                    builder.Append(",");
                }
            }
            return GetHashCodeEx(builder.ToString());
        }

        internal static string[] GetDirectories(string workingDir, string rootDir)
        {
            if (!Path.IsPathRooted(rootDir))
            {
                rootDir = Path.Combine(workingDir, rootDir);
            }
            return new string[] { rootDir };
        }

        private static int GetHashCode32(string s)
        {
            char[] chArray = s.ToCharArray();
            int num = chArray.Length - 1;
            int num2 = 0x15051505;
            int num3 = num2;
            int index = 0;
            while (index <= num)
            {
                char ch = chArray[index];
                char ch2 = (++index > num) ? '\0' : chArray[index];
                num2 = (((num2 << 5) + num2) + (num2 >> 0x1b)) ^ ((ch2 << 0x10) | ch);
                if (++index > num)
                {
                    break;
                }
                ch = chArray[index];
                ch2 = (++index > num) ? '\0' : chArray[index++];
                num3 = (((num3 << 5) + num3) + (num3 >> 0x1b)) ^ ((ch2 << 0x10) | ch);
            }
            return (num2 + (num3 * 0x5d588b65));
        }

        public static int GetHashCodeEx(string s)
        {
            if (ExecuteOptions.options.customHashing)
            {
                return GetHashCode32(s);
            }
            return s.GetHashCode();
        }

        internal static string GetScriptedCodeAttributeInjectionCode(string scriptFileName)
        {
            using (Mutex mutex = Utils.FileLock(scriptFileName, "GetScriptedCodeAttributeInjectionCode"))
            {
                mutex.WaitOne(0x3e8, false);
                string str = string.Format("[assembly: System.Reflection.AssemblyDescriptionAttribute(@\"{0}\")]", scriptFileName);
                string str2 = "";
                string path = Path.Combine(CSExecutor.GetCacheDirectory(scriptFileName), Path.GetFileNameWithoutExtension(scriptFileName) + ".attr.g.cs");
                Exception innerException = null;
                for (int i = 0; i < 3; i++)
                {
                    try
                    {
                        if (File.Exists(path))
                        {
                            using (StreamReader reader = new StreamReader(path))
                            {
                                str2 = reader.ReadToEnd();
                            }
                        }
                        if (str2 != str)
                        {
                            string directoryName = Path.GetDirectoryName(path);
                            if (!Directory.Exists(directoryName))
                            {
                                Directory.CreateDirectory(directoryName);
                            }
                            using (StreamWriter writer = new StreamWriter(path))
                            {
                                writer.Write(str);
                            }
                        }
                        break;
                    }
                    catch (Exception exception2)
                    {
                        innerException = exception2;
                    }
                    Thread.Sleep(200);
                }
                if (!File.Exists(path))
                {
                    throw new ApplicationException("Failed to create AttributeInjection file", innerException);
                }
                return path;
            }
        }

        public static bool HaveSameTimestamp(string file1, string file2)
        {
            FileInfo info = new FileInfo(file1);
            FileInfo info2 = new FileInfo(file2);
            return ((info2.LastWriteTime == info.LastWriteTime) && (info2.LastWriteTimeUtc == info.LastWriteTimeUtc));
        }

        public static bool IsDynamic(Assembly asm)
        {
            if (!asm.GetType().FullName.EndsWith("AssemblyBuilder") && (asm.Location != null))
            {
                return (asm.Location == "");
            }
            return true;
        }

        public static Hashtable LoadPrecompilers(ExecuteOptions options)
        {
            Hashtable hashtable = new Hashtable();
            if (!options.preCompilers.StartsWith("nodefault"))
            {
                ArrayList list = new ArrayList();
                list.Add(new DefaultPrecompiler());
                hashtable.Add(Assembly.GetExecutingAssembly().Location, list);
            }
            if (options.autoClass)
            {
                if (hashtable.ContainsKey(Assembly.GetExecutingAssembly().Location))
                {
                    (hashtable[Assembly.GetExecutingAssembly().Location] as ArrayList).Add(new AutoclassPrecompiler());
                }
                else
                {
                    ArrayList list2 = new ArrayList();
                    list2.Add(new AutoclassPrecompiler());
                    hashtable.Add(Assembly.GetExecutingAssembly().Location, list2);
                }
            }
            foreach (string str in Utils.RemoveDuplicates(options.preCompilers.Split(new char[] { ',' })))
            {
                string file = str.Trim();
                if ((file != "") && (file != "nodefault"))
                {
                    Assembly assembly;
                    string assemblyFile = FindImlementationFile(file, options.searchDirs);
                    if (assemblyFile == null)
                    {
                        throw new ApplicationException("Cannot find Precompiler file " + file);
                    }
                    if (assemblyFile.EndsWith(".dll", true, CultureInfo.InvariantCulture))
                    {
                        assembly = Assembly.LoadFrom(assemblyFile);
                    }
                    else
                    {
                        assembly = CompilePrecompilerScript(assemblyFile, options.searchDirs);
                    }
                    object obj2 = null;
                    foreach (Module module in assembly.GetModules())
                    {
                        if (obj2 != null)
                        {
                            break;
                        }
                        foreach (Type type in module.GetTypes())
                        {
                            if (type.Name.EndsWith("Precompiler"))
                            {
                                obj2 = assembly.CreateInstance(type.Name);
                                if (obj2 == null)
                                {
                                    throw new Exception("Precompiler " + assemblyFile + " cannot be loaded. CreateInstance returned null.");
                                }
                                break;
                            }
                        }
                    }
                    if (obj2 != null)
                    {
                        ArrayList list3 = new ArrayList();
                        list3.Add(obj2);
                        hashtable.Add(assemblyFile, list3);
                    }
                }
            }
            return hashtable;
        }

        internal static int ParseAppArgs(string[] args, IScriptExecutor executor)
        {
            ExecuteOptions options = executor.GetOptions();
            for (int i = 0; i < args.Length; i++)
            {
                if (File.Exists(args[i]))
                {
                    return i;
                }
                if (!args[i].StartsWith(cmdFlagPrefix))
                {
                    return i;
                }
                if (args[i] == (cmdFlagPrefix + "nl"))
                {
                    options.noLogo = true;
                }
                else if ((args[i] == (cmdFlagPrefix + "c")) && !options.supressExecution)
                {
                    options.useCompiled = true;
                }
                else if (args[i] == (cmdFlagPrefix + "sconfig"))
                {
                    options.useScriptConfig = true;
                }
                else if (args[i].StartsWith(cmdFlagPrefix + "sconfig:"))
                {
                    options.useScriptConfig = true;
                    options.customConfigFileName = args[i].Substring((cmdFlagPrefix + "sconfig:").Length);
                }
                else if (args[i].StartsWith(cmdFlagPrefix + "provider:"))
                {
                    try
                    {
                        options.altCompiler = Environment.ExpandEnvironmentVariables(args[i]).Substring((cmdFlagPrefix + "provider:").Length);
                    }
                    catch
                    {
                    }
                }
                else if (args[i] == (cmdFlagPrefix + "verbose"))
                {
                    options.verbose = true;
                }
                else if (args[i].StartsWith(cmdFlagPrefix + "dir:"))
                {
                    foreach (string str in args[i].Substring((cmdFlagPrefix + "dir:").Length).Split(new char[] { ',' }))
                    {
                        options.AddSearchDir(str.Trim());
                    }
                }
                else if (args[i].StartsWith(cmdFlagPrefix + "precompiler"))
                {
                    if (args[i].StartsWith(cmdFlagPrefix + "precompiler:"))
                    {
                        options.preCompilers = args[i].Substring((cmdFlagPrefix + "precompiler:").Length);
                    }
                    else
                    {
                        executor.ShowPrecompilerSample();
                        options.processFile = false;
                    }
                }
                else if (args[i].StartsWith(cmdFlagPrefix + "pc:"))
                {
                    options.preCompilers = args[i].Substring((cmdFlagPrefix + "pc:").Length);
                }
                else if (args[i].StartsWith(cmdFlagPrefix + "noconfig"))
                {
                    options.noConfig = true;
                    if (args[i].StartsWith(cmdFlagPrefix + "noconfig:"))
                    {
                        if (args[i] == (cmdFlagPrefix + "noconfig:out"))
                        {
                            executor.CreateDefaultConfigFile();
                            options.processFile = false;
                        }
                        else
                        {
                            options.altConfig = args[i].Substring((cmdFlagPrefix + "noconfig:").Length);
                        }
                    }
                }
                else if ((args[i] == (cmdFlagPrefix + "autoclass")) || (args[i] == (cmdFlagPrefix + "ac")))
                {
                    options.autoClass = true;
                }
                else if (args[i] == (cmdFlagPrefix + "nathash"))
                {
                    options.customHashing = false;
                }
                else if (args[i].StartsWith(cmdFlagPrefix + "ca"))
                {
                    options.useCompiled = true;
                    options.forceCompile = true;
                    options.supressExecution = true;
                }
                else if (args[i].StartsWith(cmdFlagPrefix + "co:"))
                {
                    string str2 = args[i].Substring((cmdFlagPrefix + "co:").Length);
                    if (!options.compilerOptions.Contains(str2))
                    {
                        options.compilerOptions = options.compilerOptions + " " + str2;
                    }
                }
                else if (args[i].StartsWith(cmdFlagPrefix + "cd"))
                {
                    options.supressExecution = true;
                    options.DLLExtension = true;
                }
                else if ((args[i] == (cmdFlagPrefix + "dbg")) || (args[i] == (cmdFlagPrefix + "d")))
                {
                    options.DBG = true;
                }
                else if (args[i] == (cmdFlagPrefix + "l"))
                {
                    options.local = true;
                }
                else if ((args[i] == (cmdFlagPrefix + "v")) || (args[i] == (cmdFlagPrefix + "V")))
                {
                    executor.ShowVersion();
                    options.processFile = false;
                    options.versionOnly = true;
                }
                else if (args[i].StartsWith(cmdFlagPrefix + "r:"))
                {
                    string[] strArray = args[i].Remove(0, 3).Split(",;".ToCharArray());
                    options.refAssemblies = strArray;
                }
                else if (args[i].StartsWith(cmdFlagPrefix + "e") && !options.buildExecutable)
                {
                    options.buildExecutable = true;
                    options.supressExecution = true;
                    options.buildWinExecutable = args[i].StartsWith(cmdFlagPrefix + "ew");
                }
                else
                {
                    if ((args[0] == (cmdFlagPrefix + "?")) || (args[0] == (cmdFlagPrefix + "help")))
                    {
                        executor.ShowHelp();
                        options.processFile = false;
                        break;
                    }
                    if (args[0] == (cmdFlagPrefix + "s"))
                    {
                        executor.ShowSample();
                        options.processFile = false;
                        break;
                    }
                }
            }
            return args.Length;
        }

        internal static PrecompilationContext Precompile(string scriptFile, string[] filesToCompile, ExecuteOptions options)
        {
            PrecompilationContext context = new PrecompilationContext {
                SearchDirs = options.searchDirs
            };
            Hashtable hashtable = new Hashtable();
            hashtable["NewDependencies"] = context.NewDependencies;
            hashtable["NewSearchDirs"] = context.NewSearchDirs;
            hashtable["NewReferences"] = context.NewReferences;
            hashtable["NewIncludes"] = context.NewIncludes;
            hashtable["SearchDirs"] = context.SearchDirs;
            hashtable["ConsoleEncoding"] = options.consoleEncoding;
            Hashtable hashtable2 = LoadPrecompilers(options);
            if (hashtable2.Count != 0)
            {
                for (int i = 0; i < filesToCompile.Length; i++)
                {
                    string content = File.ReadAllText(filesToCompile[i]);
                    bool flag = false;
                    foreach (string str2 in hashtable2.Keys)
                    {
                        foreach (object obj2 in hashtable2[str2] as ArrayList)
                        {
                            if (options.verbose && (i == 0))
                            {
                                VerbosePrint("  Precompilers: ", options);
                                int num2 = 0;
                                string[] strArray = filesToCompile;
                                for (int j = 0; j < strArray.Length; j++)
                                {
                                    string text1 = strArray[j];
                                    VerbosePrint(string.Concat(new object[] { "   ", num2++, " - ", obj2.GetType(), "\n       ", str2 }), options);
                                }
                                VerbosePrint("", options);
                            }
                            MethodInfo info = obj2.GetType().GetMethod("Compile");
                            CompileMethod method = (CompileMethod) Delegate.CreateDelegate(typeof(CompileMethod), info);
                            if (method(ref content, filesToCompile[i], filesToCompile[i] == scriptFile, hashtable))
                            {
                                context.NewDependencies.Add(str2);
                                flag = true;
                            }
                        }
                    }
                    if (flag)
                    {
                        filesToCompile[i] = SaveAsAutogeneratedScript(content, filesToCompile[i]);
                    }
                }
            }
            options.searchDirs = Utils.Concat(options.searchDirs, context.NewSearchDirs.ToArray());
            foreach (string str3 in context.NewReferences)
            {
                options.defaultRefAssemblies = options.defaultRefAssemblies + "," + str3;
            }
            return context;
        }

        public static string SaveAsAutogeneratedScript(string content, string originalFileName)
        {
            string path = Path.Combine(CSExecutor.GetCacheDirectory(originalFileName), Path.GetFileNameWithoutExtension(originalFileName) + ".g" + Path.GetExtension(originalFileName));
            if (File.Exists(path))
            {
                File.SetAttributes(path, FileAttributes.Normal);
            }
            using (StreamWriter writer = new StreamWriter(path, false, Encoding.UTF8))
            {
                writer.Write(content);
            }
            File.SetAttributes(path, FileAttributes.ReadOnly);
            return path;
        }

        public static void SetTimestamp(string fileDest, string fileSrc)
        {
            FileInfo info = new FileInfo(fileSrc);
            FileInfo info2 = new FileInfo(fileDest) {
                LastWriteTime = info.LastWriteTime,
                LastWriteTimeUtc = info.LastWriteTimeUtc
            };
        }

        internal static void VerbosePrint(string message, ExecuteOptions options)
        {
            if (options.verbose)
            {
                Console.WriteLine(message);
            }
        }

        internal static string cmdFlagPrefix
        {
            get
            {
                if (Utils.IsLinux())
                {
                    return "-";
                }
                return "/";
            }
        }

        public static bool IsRuntimeErrorReportingSupressed
        {
            get
            {
                return (Environment.GetEnvironmentVariable("CSS_IsRuntimeErrorReportingSupressed") != null);
            }
        }

        private delegate bool CompileMethod(ref string content, string scriptFile, bool IsPrimaryScript, Hashtable context);

        public delegate void ShowDocumentHandler();
    }
}

