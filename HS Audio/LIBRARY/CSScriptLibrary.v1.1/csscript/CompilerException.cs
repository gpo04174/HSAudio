﻿namespace csscript
{
    using System;
    using System.CodeDom.Compiler;
    using System.Runtime.Serialization;
    using System.Text;

    [Serializable]
    public class CompilerException : ApplicationException
    {
        public CompilerException()
        {
        }

        public CompilerException(string message) : base(message)
        {
        }

        public CompilerException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }

        public static CompilerException Create(CompilerErrorCollection Errors, bool hideCompilerWarnings)
        {
            StringBuilder builder = new StringBuilder();
            foreach (CompilerError error in Errors)
            {
                if (!error.IsWarning || !hideCompilerWarnings)
                {
                    builder.Append(error.FileName);
                    builder.Append("(");
                    builder.Append(error.Line);
                    builder.Append(",");
                    builder.Append(error.Column);
                    builder.Append("): ");
                    if (error.IsWarning)
                    {
                        builder.Append("warning ");
                    }
                    else
                    {
                        builder.Append("error ");
                    }
                    builder.Append(error.ErrorNumber);
                    builder.Append(": ");
                    builder.Append(error.ErrorText);
                    builder.Append(Environment.NewLine);
                }
            }
            CompilerException exception = new CompilerException(builder.ToString());
            exception.Data.Add("Errors", Errors);
            return exception;
        }
    }
}

