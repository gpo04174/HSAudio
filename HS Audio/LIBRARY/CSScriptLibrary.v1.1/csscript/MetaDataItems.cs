﻿namespace csscript
{
    using CSScriptLibrary;
    using System;
    using System.Collections;
    using System.Collections.Specialized;
    using System.IO;
    using System.Text;
    using System.Runtime.InteropServices;
    using System.Reflection;

    internal class MetaDataItems
    {
        private int intSize = Marshal.SizeOf(0);
        public ArrayList items = new ArrayList();
        private int stampID = CSSUtils.GetHashCodeEx(Assembly.GetExecutingAssembly().FullName.Split(",".ToCharArray())[1]);

        public void AddItem(string file, DateTime date, bool assembly)
        {
            this.items.Add(new MetaDataItem(file, date, assembly));
        }

        public string[] AddItems(StringCollection files, bool isAssembly, string[] searchDirs)
        {
            string[] array = new string[files.Count];
            files.CopyTo(array, 0);
            return this.AddItems(array, isAssembly, searchDirs);
        }

        public string[] AddItems(string[] files, bool isAssembly, string[] searchDirs)
        {
            ArrayList list = new ArrayList();
            if (isAssembly)
            {
                foreach (string str in files)
                {
                    if (File.Exists(str))
                    {
                        try
                        {
                            if (this.IsGACAssembly(str))
                            {
                                continue;
                            }
                            bool flag = false;
                            foreach (string str2 in searchDirs)
                            {
                                if (!this.IsGACAssembly(str) && (string.Compare(str2, Path.GetDirectoryName(str), true) == 0))
                                {
                                    flag = true;
                                    this.AddItem(Path.GetFileName(str), File.GetLastWriteTimeUtc(str), true);
                                    break;
                                }
                            }
                            if (!flag)
                            {
                                list.Add(Path.GetDirectoryName(str));
                                this.AddItem(str, File.GetLastWriteTimeUtc(str), true);
                            }
                        }
                        catch (NotSupportedException)
                        {
                        }
                        catch (ArgumentException)
                        {
                        }
                        catch (PathTooLongException)
                        {
                        }
                        catch
                        {
                        }
                    }
                }
            }
            else
            {
                foreach (string str3 in files)
                {
                    string fullPath = Path.GetFullPath(str3);
                    bool flag2 = false;
                    foreach (string str5 in searchDirs)
                    {
                        if (flag2 = string.Compare(str5, Path.GetDirectoryName(fullPath), true) == 0)
                        {
                            break;
                        }
                    }
                    if (flag2)
                    {
                        this.AddItem(Path.GetFileName(str3), File.GetLastWriteTimeUtc(str3), false);
                    }
                    else
                    {
                        this.AddItem(str3, File.GetLastWriteTimeUtc(str3), false);
                    }
                }
            }
            return (string[]) list.ToArray(typeof(string));
        }

        private bool IsGACAssembly(string file)
        {
            string str = file.ToLower();
            if (((str.IndexOf(@"microsoft.net\framework") == -1) && (str.IndexOf("microsoft.net/framework") == -1)) && ((str.IndexOf("gac_msil") == -1) && (str.IndexOf("gac_64") == -1)))
            {
                return (str.IndexOf("gac_32") != -1);
            }
            return true;
        }

        public static bool IsOutOfDate(string script, string assembly)
        {
            MetaDataItems items = new MetaDataItems();
            if (!items.ReadFileStamp(assembly))
            {
                return true;
            }
            string path = "";
            foreach (MetaDataItem item in items.items)
            {
                if (item.assembly)
                {
                    if (Path.IsPathRooted(item.file))
                    {
                        path = item.file;
                        CSExecutor.options.AddSearchDir(Path.GetDirectoryName(item.file));
                    }
                    else
                    {
                        foreach (string str2 in CSExecutor.options.searchDirs)
                        {
                            path = Path.Combine(str2, item.file);
                            if (File.Exists(path))
                            {
                                break;
                            }
                        }
                    }
                }
                else
                {
                    path = FileParser.ResolveFile(item.file, CSExecutor.options.searchDirs, false);
                }
                if (!File.Exists(path) || (File.GetLastWriteTimeUtc(path) != item.date))
                {
                    return true;
                }
            }
            return false;
        }

        private bool Parse(string data)
        {
            foreach (string str in data.Split("|".ToCharArray()))
            {
                if (str.Length > 0)
                {
                    string[] strArray = str.Split(";".ToCharArray());
                    if (strArray.Length != 3)
                    {
                        return false;
                    }
                    this.items.Add(new MetaDataItem(strArray[0], DateTime.FromFileTimeUtc(long.Parse(strArray[1])), strArray[2] == "Y"));
                }
            }
            return true;
        }

        public bool ReadFileStamp(string file)
        {
            try
            {
                using (FileStream stream = new FileStream(file, FileMode.Open, FileAccess.Read))
                {
                    using (BinaryReader reader = new BinaryReader(stream))
                    {
                        stream.Seek((long) -this.intSize, SeekOrigin.End);
                        if (reader.ReadInt32() == this.stampID)
                        {
                            stream.Seek((long) -(this.intSize * 2), SeekOrigin.End);
                            if (reader.ReadInt32() != CSSUtils.GetHashCodeEx(Environment.Version.ToString()))
                            {
                                return false;
                            }
                            stream.Seek((long) -(this.intSize * 3), SeekOrigin.End);
                            if (reader.ReadInt32() != CSExecutor.options.compilationContext)
                            {
                                return false;
                            }
                            stream.Seek((long) -(this.intSize * 4), SeekOrigin.End);
                            if (reader.ReadInt32() != (CSExecutor.options.DBG ? 1 : 0))
                            {
                                return false;
                            }
                            stream.Seek((long) -(this.intSize * 5), SeekOrigin.End);
                            int count = reader.ReadInt32();
                            if (count != 0)
                            {
                                stream.Seek((long) -((this.intSize * 5) + count), SeekOrigin.End);
                                return this.Parse(new string(reader.ReadChars(count)));
                            }
                            return true;
                        }
                        return false;
                    }
                }
            }
            catch
            {
            }
            return false;
        }

        public bool StampFile(string file)
        {
            try
            {
                using (FileStream stream = new FileStream(file, FileMode.Open))
                {
                    stream.Seek(0L, SeekOrigin.End);
                    using (BinaryWriter writer = new BinaryWriter(stream))
                    {
                        char[] chars = this.ToString().ToCharArray();
                        writer.Write(chars);
                        writer.Write(chars.Length);
                        writer.Write(CSExecutor.options.DBG ? 1 : 0);
                        writer.Write(CSExecutor.options.compilationContext);
                        writer.Write(CSSUtils.GetHashCodeEx(Environment.Version.ToString()));
                        writer.Write(this.stampID);
                    }
                }
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception);
                return false;
            }
            return true;
        }

        private string ToString()
        {
            StringBuilder builder = new StringBuilder();
            foreach (MetaDataItem item in this.items)
            {
                builder.Append(item.file);
                builder.Append(";");
                builder.Append(item.date.ToFileTimeUtc().ToString());
                builder.Append(";");
                builder.Append(item.assembly ? "Y" : "N");
                builder.Append("|");
            }
            return builder.ToString();
        }

        public class MetaDataItem
        {
            public bool assembly;
            public DateTime date;
            public string file;

            public MetaDataItem(string file, DateTime date, bool assembly)
            {
                this.file = file;
                this.date = date;
                this.assembly = assembly;
            }
        }
    }
}

