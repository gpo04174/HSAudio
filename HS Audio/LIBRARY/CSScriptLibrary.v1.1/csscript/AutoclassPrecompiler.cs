﻿namespace csscript
{
    using System;
    using System.Collections;
    using System.IO;
    using System.Runtime.InteropServices;
    using System.Text;
    using System.Text.RegularExpressions;

    internal class AutoclassPrecompiler
    {
        public static bool Compile(ref string content, string scriptFile, bool IsPrimaryScript, Hashtable context)
        {
            int num;
            int num2;
            if (!IsPrimaryScript)
            {
                return false;
            }
            content = Process(content, out num, out num2, (string) context["ConsoleEncoding"]);
            return true;
        }

        internal static string Process(string content, out int injectionPos, out int injectionLength, string consoleEncoding)
        {
            int index = -1;
            injectionPos = -1;
            injectionLength = 0;
            StringBuilder builder = new StringBuilder(0x1000);
            bool flag = false;
            using (StringReader reader = new StringReader(content))
            {
                string str;
                bool flag2 = false;
                while ((str = reader.ReadLine()) != null)
                {
                    if ((!flag && !str.TrimStart(new char[0]).StartsWith("using ")) && (!str.StartsWith("//") && (str.Trim() != "")))
                    {
                        flag = true;
                        injectionPos = builder.Length;
                        string str2 = "public class ScriptClass { public ";
                        builder.Append(str2);
                        injectionLength += str2.Length;
                        index = builder.Length;
                    }
                    if ((!flag2 && (index != -1)) && !Utils.IsNullOrWhiteSpace(str))
                    {
                        string input = str.TrimStart(new char[0]);
                        if (!input.StartsWith("//"))
                        {
                            foreach (Match match in Regex.Matches(input, @"\s+main\s*\(", RegexOptions.IgnoreCase))
                            {
                                if (match.Value.Contains("main"))
                                {
                                    bool flag3 = Regex.Matches(input, @"\s+main\s*\(\s*\)").Count != 0;
                                    bool flag4 = Regex.Matches(input, @"void\s+main\s*\(").Count != 0;
                                    string str4 = flag3 ? "" : "args";
                                    string str5 = "static int Main(string[] args) { ";
                                    if (string.Compare(consoleEncoding, "default", true) != 0)
                                    {
                                        str5 = str5 + "try { Console.OutputEncoding = System.Text.Encoding.GetEncoding(\"" + consoleEncoding + "\"); } catch {} ";
                                    }
                                    if (flag4)
                                    {
                                        str5 = str5 + "new ScriptClass().main(" + str4 + "); return 0;";
                                    }
                                    else
                                    {
                                        str5 = str5 + "return (int)new ScriptClass().main(" + str4 + ");";
                                    }
                                    str5 = str5 + "} ///CS-Script auto-class generation" + Environment.NewLine;
                                    injectionLength += str5.Length;
                                    builder.Insert(index, str5);
                                }
                                else if (match.Value.Contains("Main") && !match.Value.Contains("static"))
                                {
                                    string str6 = "static ";
                                    injectionLength += str6.Length;
                                    builder.Insert(index, str6);
                                }
                                flag2 = true;
                                break;
                            }
                        }
                    }
                    builder.Append(str);
                    builder.Append(Environment.NewLine);
                }
            }
            builder.Append("} ///CS-Script auto-class generation" + Environment.NewLine);
            return builder.ToString();
        }
    }
}

