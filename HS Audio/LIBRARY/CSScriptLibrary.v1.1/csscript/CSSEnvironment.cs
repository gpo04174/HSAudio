﻿namespace csscript
{
    using System;
    using System.IO;
    using System.Reflection;

    public class CSSEnvironment
    {
        private static string scriptFile;
        private static string scriptFileNamePrimary;

        private CSSEnvironment()
        {
        }

        private static string FindExecuteOptionsField(Assembly asm, string field)
        {
            Type type = asm.GetModules()[0].GetType("csscript.ExecuteOptions");
            if (type != null)
            {
                foreach (FieldInfo info in type.GetFields(BindingFlags.Public | BindingFlags.Static))
                {
                    if (info.Name == "options")
                    {
                        object obj2 = info.GetValue(null);
                        if (obj2 == null)
                        {
                            break;
                        }
                        object obj3 = obj2.GetType().GetField(field).GetValue(obj2);
                        if (obj3 == null)
                        {
                            break;
                        }
                        return obj3.ToString();
                    }
                }
            }
            return null;
        }

        public static string GetCacheDirectory(string file)
        {
            return CSExecutor.GetCacheDirectory(file);
        }

        public static string GetTempScriptName()
        {
            return CSExecutor.GetScriptTempFile();
        }

        public static string SaveAsTempScript(string content)
        {
            string scriptTempFile = CSExecutor.GetScriptTempFile();
            using (StreamWriter writer = new StreamWriter(scriptTempFile))
            {
                writer.Write(content);
            }
            return scriptTempFile;
        }

        public static void SetScriptTempDir(string path)
        {
            CSExecutor.SetScriptTempDir(path);
        }

        [Obsolete("This member may not work correctly in the hosted and cached scenarios. Use alternative techniques demonstrated in the ReflectScript.cs sample.Including environment variable 'EntryScript' and AssemblyDescriptionAttribute (of the script assembly) containing the full path of the script file.")]
        public static string PrimaryScriptFile
        {
            get
            {
                if (scriptFileNamePrimary == null)
                {
                    scriptFileNamePrimary = FindExecuteOptionsField(Assembly.GetExecutingAssembly(), "scriptFileNamePrimary");
                    if ((scriptFileNamePrimary == null) || (scriptFileNamePrimary == ""))
                    {
                        scriptFileNamePrimary = FindExecuteOptionsField(Assembly.GetEntryAssembly(), "scriptFileNamePrimary");
                    }
                }
                return scriptFileNamePrimary;
            }
        }

        [Obsolete("This member may not work correctly in the hosted and cached scenarios. Use alternative techniques demonstrated in the ReflectScript.cs sample.Including environment variable 'EntryScript' and AssemblyDescriptionAttribute (of the script assembly) containing the full path of the script file.")]
        public static string ScriptFile
        {
            get
            {
                scriptFile = FindExecuteOptionsField(Assembly.GetExecutingAssembly(), "scriptFileName");
                if (scriptFile == null)
                {
                    scriptFile = FindExecuteOptionsField(Assembly.GetEntryAssembly(), "scriptFileName");
                }
                return scriptFile;
            }
        }
    }
}

