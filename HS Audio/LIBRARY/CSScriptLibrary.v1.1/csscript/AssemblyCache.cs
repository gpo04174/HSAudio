﻿namespace csscript
{
    using System;
    using System.Runtime.InteropServices;

    [ComVisible(false)]
    internal class AssemblyCache
    {
        [DllImport("fusion.dll")]
        internal static extern int CreateAssemblyCache(out IAssemblyCache ppAsmCache, int reserved);
        public static void InstallAssembly(string assemblyPath, InstallReference reference, AssemblyCommitFlags flags)
        {
            if ((reference != null) && !InstallReferenceGuid.IsValidGuidScheme(reference.GuidScheme))
            {
                throw new ArgumentException("Invalid argument( reference guid).");
            }
            IAssemblyCache ppAsmCache = null;
            COM.CheckHR(CreateAssemblyCache(out ppAsmCache, 0));
            COM.CheckHR(ppAsmCache.InstallAssembly((int) flags, assemblyPath, reference));
        }

        public static string QueryAssemblyInfo(string assemblyName)
        {
            AssemblyInfo info;
            if (assemblyName == null)
            {
                throw new ArgumentException("Invalid argument (assemblyName)");
            }
            info = new AssemblyInfo {
                cchBuf = 0x400,
                currentAssemblyPath = "Path".PadLeft(0x400)//info.cchBuf)
            };
            IAssemblyCache ppAsmCache = null;
            COM.CheckHR(CreateAssemblyCache(out ppAsmCache, 0));
            COM.CheckHR(ppAsmCache.QueryAssemblyInfo(0, assemblyName, ref info));
            return info.currentAssemblyPath;
        }

        public static void UninstallAssembly(string assemblyName, InstallReference reference, out AssemblyCacheUninstallDisposition disp)
        {
            AssemblyCacheUninstallDisposition uninstalled = AssemblyCacheUninstallDisposition.Uninstalled;
            if ((reference != null) && !InstallReferenceGuid.IsValidGuidScheme(reference.GuidScheme))
            {
                throw new ArgumentException("Invalid argument (reference guid).");
            }
            IAssemblyCache ppAsmCache = null;
            COM.CheckHR(CreateAssemblyCache(out ppAsmCache, 0));
            COM.CheckHR(ppAsmCache.UninstallAssembly(0, assemblyName, reference, out uninstalled));
            disp = uninstalled;
        }
    }
}

