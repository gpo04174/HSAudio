﻿namespace csscript
{
    using System;
    using System.Collections;
    using System.Threading;

    internal class ExecuteOptions : ICloneable
    {
        public string altCompiler = "";
        public string altConfig = "";
        public ApartmentState apartmentState;
        public bool autoClass;
        public bool buildExecutable;
        public bool buildWinExecutable;
        public string cleanupShellCommand = "";
        public int compilationContext;
        public string compilerOptions = "";
        public string consoleEncoding = "utf-8";
        public string customConfigFileName = "";
        public bool customHashing = true;
        public bool DBG;
        public string defaultRefAssemblies = "";
        public bool DLLExtension;
        public uint doCleanupAfterNumberOfRuns = 20;
        public bool forceCompile;
        public string forceOutputAssembly = "";
        public bool hideCompilerWarnings;
        public Settings.HideOptions hideTemp = Settings.HideOptions.HideMostFiles;
        public object initContext;
        internal bool InjectScriptAssemblyAttribute = true;
        public bool inMemoryAsm;
        public bool local;
        public bool noConfig;
        public bool noLogo;
        public bool openEndDirectiveSyntax = true;
        public static ExecuteOptions options = new ExecuteOptions();
        public string postProcessor = "";
        public string preCompilers = "";
        public bool processFile = true;
        public string[] refAssemblies = new string[0];
        public bool reportDetailedErrorInfo;
        public string scriptFileName = "";
        public string scriptFileNamePrimary;
        public string[] searchDirs = new string[0];
        public bool shareHostRefAssemblies;
        public bool startDebugger;
        public bool supressExecution;
        public string TargetFramework = "v4.0";
        public bool useCompiled;
        public bool useScriptConfig;
        public bool useSmartCaching = true;
        public bool useSurrogateHostingProcess;
        public bool verbose;
        public bool versionOnly;

        public ExecuteOptions()
        {
            options = this;
        }

        public void AddSearchDir(string dir)
        {
            foreach (string str in this.searchDirs)
            {
                if (str == dir)
                {
                    return;
                }
            }
            string[] array = new string[this.searchDirs.Length + 1];
            this.searchDirs.CopyTo(array, 0);
            array[array.Length - 1] = dir;
            this.searchDirs = array;
        }

        public object Clone()
        {
            return new ExecuteOptions { 
                processFile = this.processFile, scriptFileName = this.scriptFileName, noLogo = this.noLogo, useCompiled = this.useCompiled, useSmartCaching = this.useSmartCaching, DLLExtension = this.DLLExtension, forceCompile = this.forceCompile, supressExecution = this.supressExecution, DBG = this.DBG, TargetFramework = this.TargetFramework, verbose = this.verbose, startDebugger = this.startDebugger, local = this.local, buildExecutable = this.buildExecutable, refAssemblies = (string[]) new ArrayList(this.refAssemblies).ToArray(typeof(string)), searchDirs = (string[]) new ArrayList(this.searchDirs).ToArray(typeof(string)), 
                buildWinExecutable = this.buildWinExecutable, useSurrogateHostingProcess = this.useSurrogateHostingProcess, altCompiler = this.altCompiler, consoleEncoding = this.consoleEncoding, preCompilers = this.preCompilers, postProcessor = this.postProcessor, compilerOptions = this.compilerOptions, reportDetailedErrorInfo = this.reportDetailedErrorInfo, hideCompilerWarnings = this.hideCompilerWarnings, apartmentState = this.apartmentState, openEndDirectiveSyntax = this.openEndDirectiveSyntax, forceOutputAssembly = this.forceOutputAssembly, cleanupShellCommand = this.cleanupShellCommand, versionOnly = this.versionOnly, noConfig = this.noConfig, altConfig = this.altConfig, 
                defaultRefAssemblies = this.defaultRefAssemblies, hideTemp = this.hideTemp, autoClass = this.autoClass, initContext = this.initContext, customHashing = this.customHashing, compilationContext = this.compilationContext, useScriptConfig = this.useScriptConfig, customConfigFileName = this.customConfigFileName, scriptFileNamePrimary = this.scriptFileNamePrimary, doCleanupAfterNumberOfRuns = this.doCleanupAfterNumberOfRuns, inMemoryAsm = this.inMemoryAsm, shareHostRefAssemblies = this.shareHostRefAssemblies
             };
        }

        public object Derive()
        {
            return new ExecuteOptions { 
                processFile = this.processFile, useSmartCaching = this.useSmartCaching, supressExecution = this.supressExecution, InjectScriptAssemblyAttribute = this.InjectScriptAssemblyAttribute, DBG = this.DBG, TargetFramework = this.TargetFramework, verbose = this.verbose, local = this.local, buildExecutable = this.buildExecutable, refAssemblies = (string[]) new ArrayList(this.refAssemblies).ToArray(typeof(string)), searchDirs = (string[]) new ArrayList(this.searchDirs).ToArray(typeof(string)), buildWinExecutable = this.buildWinExecutable, altCompiler = this.altCompiler, preCompilers = this.preCompilers, defaultRefAssemblies = this.defaultRefAssemblies, postProcessor = this.postProcessor, 
                compilerOptions = this.compilerOptions, reportDetailedErrorInfo = this.reportDetailedErrorInfo, hideCompilerWarnings = this.hideCompilerWarnings, openEndDirectiveSyntax = this.openEndDirectiveSyntax, apartmentState = this.apartmentState, forceOutputAssembly = this.forceOutputAssembly, versionOnly = this.versionOnly, cleanupShellCommand = this.cleanupShellCommand, noConfig = this.noConfig, compilationContext = this.compilationContext, autoClass = this.autoClass, customHashing = this.customHashing, altConfig = this.altConfig, hideTemp = this.hideTemp, initContext = this.initContext, scriptFileNamePrimary = this.scriptFileNamePrimary, 
                doCleanupAfterNumberOfRuns = this.doCleanupAfterNumberOfRuns, shareHostRefAssemblies = this.shareHostRefAssemblies, inMemoryAsm = this.inMemoryAsm
             };
        }

        public string[] ExtractShellCommand(string command)
        {
            int index = command.IndexOf("\"");
            string str = "\"";
            if ((index == -1) || (index != 0))
            {
                str = " ";
            }
            index = command.IndexOf(str, (int) (index + 1));
            if (index == -1)
            {
                return new string[] { command };
            }
            return new string[] { command.Substring(0, index).Replace("\"", ""), command.Substring(index + 1).Trim() };
        }
    }
}

