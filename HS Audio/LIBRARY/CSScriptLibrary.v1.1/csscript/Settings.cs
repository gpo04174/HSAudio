﻿namespace csscript
{
    using System;
    using System.ComponentModel;
    //using System.Drawing.Design;
    using System.IO;
    using System.Threading;
    //using System.Windows.Forms.Design;
    using System.Xml;

    public class Settings
    {
        private string cleanupShellCommand = "";
        private string consoleEncoding = "default";
        private bool customHashing = true;
        private ApartmentState defaultApartmentState;
        private string defaultArguments = (CSSUtils.cmdFlagPrefix + "c " + CSSUtils.cmdFlagPrefix + "sconfig " + CSSUtils.cmdFlagPrefix + "co:" + CSSUtils.cmdFlagPrefix + "warn:0");
        internal const string DefaultEncodingName = "default";
        private string defaultRefAssemblies;
        private uint doCleanupAfterNumberOfRuns = 30;
        private bool hideCompilerWarnings;
        private HideOptions hideOptions = HideOptions.HideMostFiles;
        private bool inMemoryAsm;
        private bool openEndDirectiveSyntax = true;
        private bool optimisticConcurrencyModel = true;
        private string precompiler = "";
        private bool reportDetailedErrorInfo = true;
        private string searchDirs = ("%CSSCRIPT_DIR%" + Path.DirectorySeparatorChar + "Lib;%CSSCRIPT_INC%;");
        private string targetFramework = "v4.0";
        private string useAlternativeCompiler = "";
        private string usePostProcessor = "";
        private bool useSurrogatepHostingProcess;

        public void AddSearchDir(string dir)
        {
            if (dir != "")
            {
                foreach (string str in this.searchDirs.Split(new char[] { ';' }))
                {
                    if ((str != "") && Utils.IsSamePath(Path.GetFullPath(str), Path.GetFullPath(dir)))
                    {
                        return;
                    }
                }
                this.searchDirs = this.searchDirs + ";" + dir;
            }
        }

        public string ExpandCleanupShellCommand()
        {
            return Environment.ExpandEnvironmentVariables(this.cleanupShellCommand);
        }

        public string ExpandDefaultRefAssemblies()
        {
            return Environment.ExpandEnvironmentVariables(this.DefaultRefAssemblies);
        }

        public string ExpandUseAlternativeCompiler()
        {
            return Environment.ExpandEnvironmentVariables(this.useAlternativeCompiler);
        }

        public string ExpandUsePostProcessor()
        {
            return Environment.ExpandEnvironmentVariables(this.usePostProcessor);
        }

        private string InitDefaultRefAssemblies()
        {
            return "";
        }

        public static Settings Load(string fileName)
        {
            return Load(fileName, true);
        }

        public static Settings Load(string fileName, bool createAlways)
        {
            Settings settings = new Settings();
            if (File.Exists(fileName))
            {
                try
                {
                    XmlDocument document = new XmlDocument();
                    document.Load(fileName);
                    XmlNode firstChild = document.FirstChild;
                    settings.defaultArguments = firstChild.SelectSingleNode("defaultArguments").InnerText;
                    settings.defaultApartmentState = (ApartmentState) Enum.Parse(typeof(ApartmentState), firstChild.SelectSingleNode("defaultApartmentState").InnerText, false);
                    settings.reportDetailedErrorInfo = firstChild.SelectSingleNode("reportDetailedErrorInfo").InnerText.ToLower() == "true";
                    settings.UseAlternativeCompiler = firstChild.SelectSingleNode("useAlternativeCompiler").InnerText;
                    settings.UsePostProcessor = firstChild.SelectSingleNode("usePostProcessor").InnerText;
                    settings.SearchDirs = firstChild.SelectSingleNode("searchDirs").InnerText;
                    settings.cleanupShellCommand = firstChild.SelectSingleNode("cleanupShellCommand").InnerText;
                    settings.doCleanupAfterNumberOfRuns = uint.Parse(firstChild.SelectSingleNode("doCleanupAfterNumberOfRuns").InnerText);
                    settings.hideOptions = (HideOptions) Enum.Parse(typeof(HideOptions), firstChild.SelectSingleNode("hideOptions").InnerText, true);
                    settings.hideCompilerWarnings = firstChild.SelectSingleNode("hideCompilerWarnings").InnerText.ToLower() == "true";
                    settings.inMemoryAsm = firstChild.SelectSingleNode("inMemoryAsm").InnerText.ToLower() == "true";
                    settings.TargetFramework = firstChild.SelectSingleNode("TragetFramework").InnerText;
                    settings.defaultRefAssemblies = firstChild.SelectSingleNode("defaultRefAssemblies").InnerText;
                    settings.useSurrogatepHostingProcess = firstChild.SelectSingleNode("useSurrogatepHostingProcess").InnerText.ToLower() == "true";
                    settings.OpenEndDirectiveSyntax = firstChild.SelectSingleNode("openEndDirectiveSyntax").InnerText.ToLower() == "true";
                    settings.Precompiler = firstChild.SelectSingleNode("Precompiler").InnerText;
                    settings.CustomHashing = firstChild.SelectSingleNode("CustomHashing").InnerText.ToLower() == "true";
                    settings.ConsoleEncoding = firstChild.SelectSingleNode("ConsoleEncoding").InnerText;
                }
                catch
                {
                    if (!createAlways)
                    {
                        settings = null;
                    }
                    else
                    {
                        settings.Save(fileName);
                    }
                }
                CSharpParser.OpenEndDirectiveSyntax = settings.OpenEndDirectiveSyntax;
            }
            return settings;
        }

        public void Save(string fileName)
        {
            try
            {
                XmlDocument document = new XmlDocument();
                document.LoadXml("<CSSConfig/>");
                document.DocumentElement.AppendChild(document.CreateElement("defaultArguments")).AppendChild(document.CreateTextNode(this.DefaultArguments));
                document.DocumentElement.AppendChild(document.CreateElement("defaultApartmentState")).AppendChild(document.CreateTextNode(this.DefaultApartmentState.ToString()));
                document.DocumentElement.AppendChild(document.CreateElement("reportDetailedErrorInfo")).AppendChild(document.CreateTextNode(this.ReportDetailedErrorInfo.ToString()));
                document.DocumentElement.AppendChild(document.CreateElement("useAlternativeCompiler")).AppendChild(document.CreateTextNode(this.UseAlternativeCompiler));
                document.DocumentElement.AppendChild(document.CreateElement("usePostProcessor")).AppendChild(document.CreateTextNode(this.UsePostProcessor));
                document.DocumentElement.AppendChild(document.CreateElement("searchDirs")).AppendChild(document.CreateTextNode(this.SearchDirs));
                document.DocumentElement.AppendChild(document.CreateElement("cleanupShellCommand")).AppendChild(document.CreateTextNode(this.CleanupShellCommand));
                document.DocumentElement.AppendChild(document.CreateElement("doCleanupAfterNumberOfRuns")).AppendChild(document.CreateTextNode(this.DoCleanupAfterNumberOfRuns.ToString()));
                document.DocumentElement.AppendChild(document.CreateElement("hideOptions")).AppendChild(document.CreateTextNode(this.hideOptions.ToString()));
                document.DocumentElement.AppendChild(document.CreateElement("hideCompilerWarnings")).AppendChild(document.CreateTextNode(this.HideCompilerWarnings.ToString()));
                document.DocumentElement.AppendChild(document.CreateElement("inMemoryAsm")).AppendChild(document.CreateTextNode(this.InMemoryAssembly.ToString()));
                document.DocumentElement.AppendChild(document.CreateElement("TragetFramework")).AppendChild(document.CreateTextNode(this.TargetFramework));
                document.DocumentElement.AppendChild(document.CreateElement("ConsoleEncoding")).AppendChild(document.CreateTextNode(this.ConsoleEncoding));
                document.DocumentElement.AppendChild(document.CreateElement("defaultRefAssemblies")).AppendChild(document.CreateTextNode(this.DefaultRefAssemblies));
                document.DocumentElement.AppendChild(document.CreateElement("useSurrogatepHostingProcess")).AppendChild(document.CreateTextNode(this.useSurrogatepHostingProcess.ToString()));
                document.DocumentElement.AppendChild(document.CreateElement("openEndDirectiveSyntax")).AppendChild(document.CreateTextNode(this.openEndDirectiveSyntax.ToString()));
                document.DocumentElement.AppendChild(document.CreateElement("Precompiler")).AppendChild(document.CreateTextNode(this.Precompiler));
                document.DocumentElement.AppendChild(document.CreateElement("CustomHashing")).AppendChild(document.CreateTextNode(this.CustomHashing.ToString()));
                document.Save(fileName);
            }
            catch
            {
            }
        }

        [Category("CustomCleanup"), Description("Command to be executed to perform custom cleanup.")]
        public string CleanupShellCommand
        {
            get
            {
                return this.cleanupShellCommand;
            }
            set
            {
                this.cleanupShellCommand = value;
            }
        }

        [Category("RuntimeSettings"), Description("Specifies the .NET Framework version that the script is compiled against (used by CSharpCodeProvider.CreateCompiler as the 'CompilerVersion' parameter).\nThis option is for the script compilation only.\nFor changing the script execution CLR use //css_host directive from the script.\nYou are discouraged from modifying this value thus if the change is required you need to edit css_config.xml file directly.")]
        public string CompilerFramework
        {
            get
            {
                return this.targetFramework;
            }
        }

        [Category("RuntimeSettings"), Description("Console output encoding. Use 'default' value if you want to use system default encoding."), TypeConverter(typeof(EncodingConverter))]
        public string ConsoleEncoding
        {
            get
            {
                return this.consoleEncoding;
            }
            set
            {
                if (this.consoleEncoding != value)
                {
                    this.consoleEncoding = Utils.ProcessNewEncoding(value);
                }
            }
        }

        [Browsable(false)]
        public bool CustomHashing
        {
            get
            {
                return this.customHashing;
            }
            set
            {
                this.customHashing = value;
            }
        }

        [Description("DefaultApartmentState is an ApartmemntState, which will be used at run-time if none specified in the code with COM threading model attributes."), Category("RuntimeSettings")]
        public ApartmentState DefaultApartmentState
        {
            get
            {
                return this.defaultApartmentState;
            }
            set
            {
                this.defaultApartmentState = value;
            }
        }

        [Description("Default command-line arguments (e.g./dbg) for all scripts."), Category("RuntimeSettings")]
        public string DefaultArguments
        {
            get
            {
                return this.defaultArguments;
            }
            set
            {
                this.defaultArguments = value;
            }
        }

        [Category("Extensibility"), Description("List of assembly names to be automatically referenced by the scripts (e.g. System.dll, System.Core.dll). Assembly extension is optional.")]
        public string DefaultRefAssemblies
        {
            get
            {
                if (this.defaultRefAssemblies == null)
                {
                    this.defaultRefAssemblies = this.InitDefaultRefAssemblies();
                }
                return this.defaultRefAssemblies;
            }
            set
            {
                this.defaultRefAssemblies = value;
            }
        }

        [Description("This value indicates frequency of the custom cleanup operation."), Category("CustomCleanup")]
        public uint DoCleanupAfterNumberOfRuns
        {
            get
            {
                return this.doCleanupAfterNumberOfRuns;
            }
            set
            {
                this.doCleanupAfterNumberOfRuns = value;
            }
        }

        [Category("RuntimeSettings"), Description("The value, which indicates if auto-generated files (if any) should should be hidden in the temporary directory.")]
        public HideOptions HideAutoGeneratedFiles
        {
            get
            {
                return this.hideOptions;
            }
            set
            {
                this.hideOptions = value;
            }
        }

        [Description("Indicates if compiler warnings should be included in script compilation output."), Category("RuntimeSettings")]
        public bool HideCompilerWarnings
        {
            get
            {
                return this.hideCompilerWarnings;
            }
            set
            {
                this.hideCompilerWarnings = value;
            }
        }

        [Description("Indicates the script assembly is to be loaded by CLR as an in-memory byte stream instead of the file."), Category("RuntimeSettings")]
        public bool InMemoryAssembly
        {
            get
            {
                return this.inMemoryAsm;
            }
            set
            {
                this.inMemoryAsm = value;
            }
        }

        [Browsable(false)]
        public bool OpenEndDirectiveSyntax
        {
            get
            {
                return this.openEndDirectiveSyntax;
            }
            set
            {
                this.openEndDirectiveSyntax = value;
            }
        }

        [Browsable(false)]
        public bool OptimisticConcurrencyModel
        {
            get
            {
                return this.optimisticConcurrencyModel;
            }
            set
            {
                this.optimisticConcurrencyModel = value;
            }
        }

        [Category("RuntimeSettings"), Description("Path to the precompiller script/assembly (see documentation for details). You can specify multiple recompiles separating them by semicolon.")]
        public string Precompiler
        {
            get
            {
                return this.precompiler;
            }
            set
            {
                this.precompiler = value;
            }
        }

        [Description("Indicates how much error details to be reported should error occur."), Category("RuntimeSettings")]
        public bool ReportDetailedErrorInfo
        {
            get
            {
                return this.reportDetailedErrorInfo;
            }
            set
            {
                this.reportDetailedErrorInfo = value;
            }
        }

        [Description("List of directories to be used to search (probing) for referenced assemblies and script files.\nThis setting is similar to the system environment variable PATH."), Category("Extensibility")]
        public string SearchDirs
        {
            get
            {
                return this.searchDirs;
            }
            set
            {
                this.searchDirs = value;
            }
        }

        [Browsable(false)]
        public string TargetFramework
        {
            get
            {
                return this.targetFramework;
            }
            set
            {
                this.targetFramework = value;
            }
        }

        [/*Editor(typeof(FileNameEditor), typeof(UITypeEditor)), */Description("Location of alternative code provider assembly. If set it forces script engine to use an alternative code compiler."), Category("Extensibility")]
        public string UseAlternativeCompiler
        {
            get
            {
                return this.useAlternativeCompiler;
            }
            set
            {
                this.useAlternativeCompiler = value;
            }
        }

        [/*Editor(typeof(FileNameEditor), typeof(UITypeEditor)), */Category("Extensibility"), Description("Location of PostProcessor assembly. If set it forces script engine to pass compiled script through PostProcessor before the execution.")]
        public string UsePostProcessor
        {
            get
            {
                return this.usePostProcessor;
            }
            set
            {
                this.usePostProcessor = value;
            }
        }

        public enum HideOptions
        {
            DoNotHide,
            HideMostFiles,
            HideAll
        }
    }
}

