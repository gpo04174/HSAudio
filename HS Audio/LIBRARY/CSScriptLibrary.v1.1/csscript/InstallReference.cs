﻿namespace csscript
{
    using System;
    using System.Runtime.InteropServices;

    [StructLayout(LayoutKind.Sequential)]
    internal class InstallReference
    {
        private int cbSize;
        private int flags;
        private Guid guidScheme;
        [MarshalAs(UnmanagedType.LPWStr)]
        private string identifier;
        [MarshalAs(UnmanagedType.LPWStr)]
        private string nonCannonicalData;
        public InstallReference(Guid guid, string id, string data)
        {
            this.cbSize = ((2 * IntPtr.Size) + 0x10) + ((id.Length + data.Length) * 2);
            this.flags = 0;
            this.guidScheme = guid;
            this.identifier = id;
            this.nonCannonicalData = data;
        }

        public Guid GuidScheme
        {
            get
            {
                return this.guidScheme;
            }
        }
    }
}

