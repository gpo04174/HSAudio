﻿namespace csscript
{
    using System;

    internal enum AssemblyCacheUninstallDisposition
    {
        Unknown,
        Uninstalled,
        StillInUse,
        AlreadyUninstalled,
        DeletePending,
        HasInstallReference,
        ReferenceNotFound
    }
}

