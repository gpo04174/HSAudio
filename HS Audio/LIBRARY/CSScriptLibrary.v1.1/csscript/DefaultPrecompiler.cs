﻿namespace csscript
{
    using System;
    using System.Collections;

    internal class DefaultPrecompiler
    {
        public static bool Compile(ref string code, string scriptFile, bool IsPrimaryScript, Hashtable context)
        {
            if (code.StartsWith("#!"))
            {
                code = "//" + code;
                return true;
            }
            return false;
        }
    }
}

