﻿namespace csscript
{
    using CSScriptLibrary;
    using System;
    using System.IO;
    using System.Reflection;
    using System.Threading;

    internal class RemoteExecutor : MarshalByRefObject
    {
        private string asmFile;
        public string[] searchDirs;

        public RemoteExecutor()
        {
            this.searchDirs = new string[0];
            this.asmFile = "";
        }

        public RemoteExecutor(string[] searchDirs)
        {
            this.searchDirs = new string[0];
            this.asmFile = "";
            this.searchDirs = searchDirs;
        }

        public void ExecuteAssembly(string filename, string[] args)
        {
            this.ExecuteAssembly(filename, args, null);
        }

        public void ExecuteAssembly(string filename, string[] args, Mutex asmLock)
        {
            Assembly assembly;
            AppDomain.CurrentDomain.AssemblyResolve += new System.ResolveEventHandler(this.ResolveEventHandler);
            AppDomain.CurrentDomain.ResourceResolve += new System.ResolveEventHandler(this.ResolveResEventHandler);
            this.asmFile = filename;
            if (!ExecuteOptions.options.inMemoryAsm)
            {
                assembly = Assembly.LoadFrom(filename);
                goto Label_00E3;
            }
            using (FileStream stream = new FileStream(filename, FileMode.Open))
            {
                byte[] buffer = new byte[stream.Length];
                stream.Read(buffer, 0, buffer.Length);
                string path = Path.ChangeExtension(filename, ".pdb");
                if (ExecuteOptions.options.DBG && File.Exists(path))
                {
                    using (FileStream stream2 = new FileStream(path, FileMode.Open))
                    {
                        byte[] buffer2 = new byte[stream2.Length];
                        stream2.Read(buffer2, 0, buffer2.Length);
                        assembly = Assembly.Load(buffer, buffer2);
                        goto Label_00DD;
                    }
                }
                assembly = Assembly.Load(buffer);
            }
        Label_00DD:
            Utils.ReleaseFileLock(asmLock);
        Label_00E3:
            this.InvokeStaticMain(assembly, args);
        }

        public void InvokeStaticMain(Assembly compiledAssembly, string[] scriptArgs)
        {
            MethodInfo method = null;
            foreach (Module module in compiledAssembly.GetModules())
            {
                foreach (Type type in module.GetTypes())
                {
                    BindingFlags bindingAttr = BindingFlags.InvokeMethod | BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Static;
                    foreach (MemberInfo info2 in type.GetMembers(bindingAttr))
                    {
                        if (info2.Name == "Main")
                        {
                            method = type.GetMethod(info2.Name, bindingAttr);
                        }
                        if (method != null)
                        {
                            break;
                        }
                    }
                    if (method != null)
                    {
                        break;
                    }
                }
                if (method != null)
                {
                    break;
                }
            }
            if (method == null)
            {
                throw new ApplicationException("Cannot find entry point. Make sure script file contains method: 'public static Main(...)'");
            }
            object obj2 = null;
            if (method.GetParameters().Length != 0)
            {
                obj2 = method.Invoke(new object(), new object[] { scriptArgs });
            }
            else
            {
                obj2 = method.Invoke(new object(), null);
            }
            if (obj2 != null)
            {
                try
                {
                    Environment.ExitCode = int.Parse(obj2.ToString());
                }
                catch
                {
                }
            }
        }

        public Assembly ResolveEventHandler(object sender, ResolveEventArgs args)
        {
            Assembly assembly = null;
            foreach (string str in this.searchDirs)
            {
                bool throwExceptions = false;
                assembly = AssemblyResolver.ResolveAssembly(args.Name, str, throwExceptions);
                if (assembly != null)
                {
                    return assembly;
                }
            }
            return assembly;
        }

        public Assembly ResolveResEventHandler(object sender, ResolveEventArgs args)
        {
            return Assembly.LoadFrom(this.asmFile);
        }
    }
}

