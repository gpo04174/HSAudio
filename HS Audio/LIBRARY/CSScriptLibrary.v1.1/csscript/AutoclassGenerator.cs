﻿namespace csscript
{
    using System;
    using System.Runtime.InteropServices;

    public class AutoclassGenerator
    {
        public static string ConsoleEncoding = "utf-8";

        public static string Process(string code, ref int position)
        {
            int num;
            int num2;
            string str = Process(code, out num, out num2);
            if (position > num)
            {
                position += num2;
            }
            return str;
        }

        public static string Process(string code, out int injectionPos, out int injectionLength)
        {
            return AutoclassPrecompiler.Process(code, out injectionPos, out injectionLength, ConsoleEncoding);
        }
    }
}

