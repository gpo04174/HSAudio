﻿namespace csscript
{
    using System;

    internal class CurrentDirGuard : IDisposable
    {
        private string currentDir = Environment.CurrentDirectory;
        private bool disposed;

        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                Environment.CurrentDirectory = this.currentDir;
            }
            this.disposed = true;
        }

        ~CurrentDirGuard()
        {
            this.Dispose(false);
        }
    }
}

