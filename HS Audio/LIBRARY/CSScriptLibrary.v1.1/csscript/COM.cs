﻿namespace csscript
{
    using System;
    using System.Runtime.InteropServices;

    internal class COM
    {
        public static void CheckHR(int hr)
        {
            if (hr < 0)
            {
                Marshal.ThrowExceptionForHR(hr);
            }
        }
    }
}

