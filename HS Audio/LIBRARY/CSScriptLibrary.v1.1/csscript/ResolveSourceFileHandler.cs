﻿namespace csscript
{
    using System;
    using System.Runtime.CompilerServices;

    public delegate string ResolveSourceFileHandler(string file, string[] searchDirs, bool throwOnError);
}

