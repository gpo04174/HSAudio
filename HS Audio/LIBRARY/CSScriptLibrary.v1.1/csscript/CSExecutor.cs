﻿namespace csscript
{
    using CSScriptLibrary;
    using Microsoft.CSharp;
    using System;
    using System.CodeDom.Compiler;
    using System.Collections;
    using System.Diagnostics;
    using System.Globalization;
    using System.IO;
    using System.Reflection;
    using System.Runtime.InteropServices;
    using System.Text;
    using System.Threading;

    internal class CSExecutor : IScriptExecutor
    {
        private static string cacheDir = "";
        internal CompilerResults LastCompileResult;
        public Exception lastException;
        internal static ExecuteOptions options = new ExecuteOptions();
        private static csscript.PrintDelegate print;
        private bool rethrow;
        private string[] scriptArgs;
        private static string tempDir = null;

        public CSExecutor()
        {
            this.rethrow = false;
            options = new ExecuteOptions();
        }

        public CSExecutor(bool rethrow, ExecuteOptions optionsBase)
        {
            this.rethrow = rethrow;
            options = new ExecuteOptions();
            options.noConfig = optionsBase.noConfig;
            options.altConfig = optionsBase.altConfig;
        }

        private void AddReferencedAssemblies(CompilerParameters compilerParams, string scriptFileName, ScriptParser parser)
        {
            string[] strArray = this.AggregateReferencedAssemblies(parser);
            compilerParams.ReferencedAssemblies.AddRange(strArray);
        }

        internal string[] AggregateReferencedAssemblies(ScriptParser parser)
        {
            UniqueAssemblyLocations locations = new UniqueAssemblyLocations();
            new ArrayList();
            if (options.shareHostRefAssemblies)
            {
                foreach (Assembly assembly in AppDomain.CurrentDomain.GetAssemblies())
                {
                    try
                    {
                        if (!CSSUtils.IsDynamic(assembly) && (File.Exists(assembly.Location) && !assembly.Location.Contains("mscorlib")))
                        {
                            locations.AddAssembly(assembly.Location);
                        }
                    }
                    catch
                    {
                    }
                }
            }
            string[] refAssemblies = options.refAssemblies;
            if (!options.useSurrogateHostingProcess)
            {
                foreach (string str in Utils.Concat(options.defaultRefAssemblies.Replace(" ", "").Split(";,".ToCharArray()), refAssemblies))
                {
                    if (str != "")
                    {
                        string[] strArray3 = AssemblyResolver.FindAssembly(str, options.searchDirs);
                        if (strArray3.Length > 0)
                        {
                            foreach (string str2 in strArray3)
                            {
                                locations.AddAssembly(this.NormalizeGacAssemblyPath(str2));
                            }
                        }
                        else
                        {
                            locations.AddAssembly(str);
                        }
                    }
                }
            }
            AssemblyResolver.ignoreFileName = Path.GetFileNameWithoutExtension(parser.ScriptPath) + ".dll";
            foreach (string str3 in parser.ResolvePackages())
            {
                locations.AddAssembly(str3);
            }
            foreach (string str4 in parser.ReferencedAssemblies)
            {
                string path = str4.Replace("\"", "");
                if (Path.IsPathRooted(path))
                {
                    locations.AddAssembly(this.NormalizeGacAssemblyPath(path));
                }
                else
                {
                    string[] strArray4 = AssemblyResolver.FindAssembly(path, options.searchDirs);
                    if (strArray4.Length > 0)
                    {
                        foreach (string str6 in strArray4)
                        {
                            locations.AddAssembly(this.NormalizeGacAssemblyPath(str6));
                        }
                    }
                    else
                    {
                        locations.AddAssembly(path);
                    }
                }
            }
            bool flag = false;
            if ((parser.IgnoreNamespaces.Length == 1) && (parser.IgnoreNamespaces[0] == "*"))
            {
                flag = true;
            }
            if (!flag)
            {
                foreach (string str7 in parser.ReferencedNamespaces)
                {
                    bool flag2 = false;
                    foreach (string str8 in parser.IgnoreNamespaces)
                    {
                        if (str8 == str7)
                        {
                            flag2 = true;
                        }
                    }
                    if (!flag2 && !locations.ContainsAssembly(str7))
                    {
                        foreach (string str9 in AssemblyResolver.FindAssembly(str7, options.searchDirs))
                        {
                            locations.AddAssembly(this.NormalizeGacAssemblyPath(str9));
                        }
                    }
                }
            }
            return (string[]) locations;
        }

        [DllImport("ole32.dll")]
        public static extern int CoInitializeSecurity(IntPtr pVoid, int cAuthSvc, IntPtr asAuthSvc, IntPtr pReserved1, int level, int impers, IntPtr pAuthList, int capabilities, IntPtr pReserved3);
        private static void ComInitSecurity(int RpcImpLevel, int EoAuthnCap)
        {
            CoInitializeSecurity(IntPtr.Zero, -1, IntPtr.Zero, IntPtr.Zero, 0, 3, IntPtr.Zero, 0x40, IntPtr.Zero);
        }

        private string Compile(string scriptFileName, string option = null)
        {
            CompilerResults results;
            bool buildExecutable = options.buildExecutable;
            string directoryName = Path.GetDirectoryName(scriptFileName);
            string path = "";
            if (options.searchDirs.Length == 0)
            {
                options.searchDirs = new string[] { directoryName };
            }
            ScriptParser parser = new ScriptParser(scriptFileName, options.searchDirs);
            options.searchDirs = Utils.RemoveDuplicates(Utils.Concat(parser.SearchDirs, Utils.GetAssemblyDirectoryName(Assembly.GetExecutingAssembly())));
            string[] filesToInject = new string[0];
            ICodeCompiler compiler = this.LoadCompiler(scriptFileName, ref filesToInject);
            CompilerParameters compilerParams = new CompilerParameters();
            foreach (string str3 in parser.Precompilers)
            {
                if (options.preCompilers == "")
                {
                    options.preCompilers = FileParser.ResolveFile(str3, options.searchDirs);
                }
                else
                {
                    options.preCompilers = options.preCompilers + "," + FileParser.ResolveFile(str3, options.searchDirs);
                }
            }
            if (options.compilerOptions != string.Empty)
            {
                Utils.AddCompilerOptions(compilerParams, options.compilerOptions);
            }
            foreach (string str4 in parser.CompilerOptions)
            {
                Utils.AddCompilerOptions(compilerParams, str4);
            }
            if (options.DBG)
            {
                Utils.AddCompilerOptions(compilerParams, "/d:DEBUG /d:TRACE");
            }
            //Modify by 박홍식(ParkHongSic)
            if (options.compilerOptions != null) Utils.AddCompilerOptions(compilerParams, options.compilerOptions);
            if (option != null) Utils.AddCompilerOptions(compilerParams, option);


            compilerParams.IncludeDebugInformation = options.DBG;
            compilerParams.GenerateExecutable = buildExecutable;
            compilerParams.GenerateInMemory = false;
            compilerParams.WarningLevel = options.hideCompilerWarnings ? -1 : 4;
            string[] destinationArray = new string[parser.FilesToCompile.Length];
            Array.Copy(parser.FilesToCompile, destinationArray, parser.FilesToCompile.Length);
            PrecompilationContext context = CSSUtils.Precompile(scriptFileName, destinationArray, options);
            if (context.NewIncludes.Count > 0)
            {
                for (int i = 0; i < context.NewIncludes.Count; i++)
                {
                    context.NewIncludes[i] = FileParser.ResolveFile(context.NewIncludes[i], options.searchDirs);
                }
                destinationArray = Utils.Concat(destinationArray, context.NewIncludes.ToArray());
                context.NewDependencies.AddRange(context.NewIncludes);
            }
            string[] additionalDependencies = context.NewDependencies.ToArray();
            this.AddReferencedAssemblies(compilerParams, scriptFileName, parser);
            foreach (string str5 in parser.ReferencedResources)
            {
                string str6 = null;
                foreach (string str7 in options.searchDirs)
                {
                    str6 = Path.IsPathRooted(str5) ? Path.GetFullPath(str5) : Path.Combine(str7, str5);
                    if (File.Exists(str6))
                    {
                        break;
                    }
                }
                if (str6 == null)
                {
                    str6 = str5;
                }
                Utils.AddCompilerOptions(compilerParams, "\"/res:" + str6 + "\"");
            }
            if (options.forceOutputAssembly != "")
            {
                path = options.forceOutputAssembly;
            }
            else if (buildExecutable)
            {
                path = Path.Combine(directoryName, Path.GetFileNameWithoutExtension(scriptFileName) + ".exe");
            }
            else if (options.useCompiled || options.DLLExtension)
            {
                if (options.DLLExtension)
                {
                    path = Path.Combine(directoryName, Path.GetFileNameWithoutExtension(scriptFileName) + ".dll");
                }
                else if (options.hideTemp != Settings.HideOptions.DoNotHide)
                {
                    path = Path.Combine(ScriptCacheDir, Path.GetFileName(scriptFileName) + ".compiled");
                }
                else
                {
                    path = scriptFileName + ".compiled";
                }
            }
            else
            {
                path = Path.ChangeExtension(GetScriptTempFile(), ".dll");
            }
            if (buildExecutable && options.buildWinExecutable)
            {
                Utils.AddCompilerOptions(compilerParams, "/target:winexe");
            }
            if (Path.GetExtension(path).ToLower() == ".pdb")
            {
                throw new ApplicationException("The specified assembly file name cannot have the reserved extension '.pdb'");
            }
            Utils.FileDelete(path, true);
            string str9 = Path.ChangeExtension(path, ".pdb");
            if (options.DBG && File.Exists(str9))
            {
                try
                {
                    Utils.FileDelete(str9);
                }
                catch
                {
                }
            }
            compilerParams.OutputAssembly = path;
            if (buildExecutable)
            {
                results = this.CompileAssembly(compiler, compilerParams, destinationArray);
                goto Label_0685;
            }
            if (filesToInject.Length != 0)
            {
                destinationArray = Utils.Concat(destinationArray, filesToInject);
            }
            CSSUtils.VerbosePrint("  Output file: \n       " + path, options);
            CSSUtils.VerbosePrint("", options);
            CSSUtils.VerbosePrint("  Files to compile: ", options);
            int num2 = 0;
            foreach (string str10 in destinationArray)
            {
                CSSUtils.VerbosePrint(string.Concat(new object[] { "   ", num2++, " - ", str10 }), options);
            }
            CSSUtils.VerbosePrint("", options);
            CSSUtils.VerbosePrint("  References: ", options);
            num2 = 0;
            foreach (string str11 in compilerParams.ReferencedAssemblies)
            {
                CSSUtils.VerbosePrint(string.Concat(new object[] { "   ", num2++, " - ", str11 }), options);
            }
            CSSUtils.VerbosePrint("> ----------------", options);
            string extension = Path.GetExtension(compilerParams.OutputAssembly);
            if (!(extension != ".dll"))
            {
                goto Label_066A;
            }
            compilerParams.OutputAssembly = Path.ChangeExtension(compilerParams.OutputAssembly, ".dll");
            Utils.FileDelete(compilerParams.OutputAssembly, true);
            results = this.CompileAssembly(compiler, compilerParams, destinationArray);
            if (!File.Exists(compilerParams.OutputAssembly))
            {
                goto Label_0685;
            }
            int num3 = 0;
        Label_061C:
            try
            {
                num3++;
                File.Move(compilerParams.OutputAssembly, Path.ChangeExtension(compilerParams.OutputAssembly, extension));
                goto Label_0685;
            }
            catch
            {
                if (num3 > 2)
                {
                    File.Copy(compilerParams.OutputAssembly, Path.ChangeExtension(compilerParams.OutputAssembly, extension), true);
                    goto Label_0685;
                }
                Thread.Sleep(100);
                goto Label_061C;
            }
        Label_066A:
            Utils.FileDelete(compilerParams.OutputAssembly, true);
            results = this.CompileAssembly(compiler, compilerParams, destinationArray);
        Label_0685:
            this.ProcessCompilingResult(results, compilerParams, parser, scriptFileName, path, additionalDependencies);
            if (options.useSurrogateHostingProcess)
            {
                new ScriptLauncherBuilder().BuildSurrogateLauncher(path, options.TargetFramework, compilerParams, options.apartmentState, options.consoleEncoding);
            }
            return path;
        }

        public string Compile(string scriptFile, string assemblyFile, bool debugBuild)
        {
            if (assemblyFile != null)
            {
                options.forceOutputAssembly = assemblyFile;
            }
            else
            {
                string str = Path.Combine(GetCacheDirectory(scriptFile), Path.GetFileName(scriptFile) + ".compiled");
                options.forceOutputAssembly = str;
            }
            if (debugBuild)
            {
                options.DBG = true;
            }
            return this.Compile(scriptFile, "/unsafe");
        }

        private CompilerResults CompileAssembly(ICodeCompiler compiler, CompilerParameters compilerParams, string[] filesToCompile)
        {
            CompilerResults results = compiler.CompileAssemblyFromFileBatch(compilerParams, filesToCompile);
            if (!results.Errors.HasErrors && (options.postProcessor != ""))
            {
                string text1 = compilerParams.OutputAssembly + ".raw";
                try
                {
                    MethodInfo method = Assembly.LoadFrom(options.postProcessor).GetType("CSSPostProcessor", true).GetMethod("Process");
                    string[] array = new string[compilerParams.ReferencedAssemblies.Count];
                    compilerParams.ReferencedAssemblies.CopyTo(array, 0);
                    method.Invoke(null, new object[] { compilerParams.OutputAssembly, array, options.searchDirs });
                }
                catch (Exception exception)
                {
                    throw new ApplicationException("Cannot post-process compiled script (set UsePostProcessor to \"null\" if the problem persist).\n" + exception.Message);
                }
            }
            return results;
        }

        public void CreateDefaultConfigFile()
        {
            string fullPath = Path.GetFullPath("css_config.xml");
            new Settings().Save(fullPath);
            print("The default config file has been created: " + fullPath);
        }

        public void Execute(string[] args, csscript.PrintDelegate printDelg, string primaryScript)
        {
            try
            {
                print = (printDelg != null) ? printDelg : new csscript.PrintDelegate(CSExecutor.VoidPrint);
                if (args.Length > 0)
                {
                    ArrayList appArgs = new ArrayList();
                    Settings persistedSettings = this.GetPersistedSettings(appArgs);
                    int index = CSSUtils.ParseAppArgs(args, this);
                    if (options.processFile)
                    {
                        if (args.Length <= index)
                        {
                            Environment.ExitCode = 1;
                            print("No script file was specified.");
                        }
                        else
                        {
                            if (options.scriptFileName == "")
                            {
                                options.scriptFileName = args[index];
                                index++;
                            }
                            for (int i = index; i < args.Length; i++)
                            {
                                if ((i == (args.Length - 1)) && (string.Compare(args[args.Length - 1], "//x", true, CultureInfo.InvariantCulture) == 0))
                                {
                                    options.startDebugger = true;
                                    options.DBG = true;
                                }
                                else
                                {
                                    appArgs.Add(args[i]);
                                }
                            }
                            this.scriptArgs = (string[]) appArgs.ToArray(typeof(string));
                            ArrayList list2 = new ArrayList();
                            using (new CurrentDirGuard())
                            {
                                if (options.local)
                                {
                                    Environment.CurrentDirectory = Path.GetDirectoryName(Path.GetFullPath(options.scriptFileName));
                                }
                                foreach (string str in options.searchDirs)
                                {
                                    list2.Add(Path.GetFullPath(str));
                                }
                                if (persistedSettings != null)
                                {
                                    foreach (string str2 in Environment.ExpandEnvironmentVariables(persistedSettings.SearchDirs).Split(",;".ToCharArray()))
                                    {
                                        if (str2.Trim() != "")
                                        {
                                            list2.Add(Path.GetFullPath(str2));
                                        }
                                    }
                                }
                            }
                            list2.Add(Utils.GetAssemblyDirectoryName(base.GetType().Assembly));
                            options.scriptFileName = FileParser.ResolveFile(options.scriptFileName, (string[]) list2.ToArray(typeof(string)));
                            if (primaryScript != null)
                            {
                                options.scriptFileNamePrimary = primaryScript;
                            }
                            else
                            {
                                options.scriptFileNamePrimary = options.scriptFileName;
                            }
                            if (ScriptCacheDir == "")
                            {
                                SetScriptCacheDir(options.scriptFileName);
                            }
                            list2.Insert(0, Path.GetDirectoryName(Path.GetFullPath(options.scriptFileName)));
                            if ((persistedSettings != null) && (persistedSettings.HideAutoGeneratedFiles != Settings.HideOptions.DoNotHide))
                            {
                                list2.Add(ScriptCacheDir);
                            }
                            options.searchDirs = (string[]) list2.ToArray(typeof(string));
                            CSharpParser.CmdScriptInfo[] infoArray = new CSharpParser.CmdScriptInfo[0];
                            CSharpParser parser = new CSharpParser(options.scriptFileName, true);
                            if (parser.Inits.Length != 0)
                            {
                                options.initContext = parser.Inits[0];
                            }
                            if ((parser.HostOptions.Length != 0) && (Environment.Version.Major >= 4))
                            {
                                foreach (string str3 in parser.HostOptions)
                                {
                                    foreach (string str4 in str3.Split(new char[] { ' ' }))
                                    {
                                        if (str4 == "/platform:x86")
                                        {
                                            options.compilerOptions = options.compilerOptions + " " + str4;
                                        }
                                        else if (str4.StartsWith("/version:"))
                                        {
                                            options.TargetFramework = str4.Replace("/version:", "");
                                        }
                                    }
                                }
                                options.useSurrogateHostingProcess = true;
                            }
                            if (File.Exists(options.scriptFileName))
                            {
                                if (parser.ThreadingModel != ApartmentState.Unknown)
                                {
                                    options.apartmentState = parser.ThreadingModel;
                                }
                                ArrayList list3 = new ArrayList(parser.CmdScripts);
                                foreach (CSharpParser.ImportInfo info in parser.Imports)
                                {
                                    try
                                    {
                                        string script = FileParser.ResolveFile(info.file, options.searchDirs);
                                        if (script.IndexOf(".g.cs") == -1)
                                        {
                                            list3.AddRange(new CSharpParser(script, true).CmdScripts);
                                        }
                                    }
                                    catch
                                    {
                                    }
                                }
                                infoArray = (CSharpParser.CmdScriptInfo[]) list3.ToArray(typeof(CSharpParser.CmdScriptInfo));
                                if (primaryScript == null)
                                {
                                    int num3 = CSSUtils.ParseAppArgs(parser.Args, this);
                                    if (num3 != -1)
                                    {
                                        for (int j = num3; j < parser.Args.Length; j++)
                                        {
                                            appArgs.Add(parser.Args[j]);
                                        }
                                    }
                                    this.scriptArgs = (string[]) appArgs.ToArray(typeof(string));
                                }
                            }
                            ExecuteOptions optionsBase = (ExecuteOptions) options.Clone();
                            string currentDirectory = Environment.CurrentDirectory;
                            foreach (CSharpParser.CmdScriptInfo info2 in infoArray)
                            {
                                if (info2.preScript)
                                {
                                    Environment.CurrentDirectory = currentDirectory;
                                    info2.args[1] = FileParser.ResolveFile(info2.args[1], optionsBase.searchDirs);
                                    CSExecutor executor = new CSExecutor(info2.abortOnError, optionsBase);
                                    if (optionsBase.DBG)
                                    {
                                        ArrayList list4 = new ArrayList();
                                        list4.AddRange(info2.args);
                                        list4.Insert(0, CSSUtils.cmdFlagPrefix + "dbg");
                                        info2.args = (string[]) list4.ToArray(typeof(string));
                                    }
                                    if (optionsBase.verbose)
                                    {
                                        ArrayList list5 = new ArrayList();
                                        list5.AddRange(info2.args);
                                        list5.Insert(0, CSSUtils.cmdFlagPrefix + "verbose");
                                        info2.args = (string[]) list5.ToArray(typeof(string));
                                    }
                                    if (info2.abortOnError)
                                    {
                                        executor.Execute(info2.args, printDelg, optionsBase.scriptFileName);
                                    }
                                    else
                                    {
                                        executor.Execute(info2.args, null, optionsBase.scriptFileName);
                                    }
                                }
                            }
                            options = optionsBase;
                            ExecuteOptions.options = optionsBase;
                            Environment.CurrentDirectory = currentDirectory;
                            options.compilationContext = CSSUtils.GenerateCompilationContext(parser, options);
                            Thread thread = new Thread(new ThreadStart(this.ExecuteImpl)) {
                                ApartmentState = options.apartmentState
                            };
                            thread.Start();
                            thread.Join();
                            if (this.lastException != null)
                            {
                                if (this.lastException is SurrogateHostProcessRequiredException)
                                {
                                    throw this.lastException;
                                }
                                throw new ApplicationException("Script " + options.scriptFileName + " cannot be executed.", this.lastException);
                            }
                            foreach (CSharpParser.CmdScriptInfo info3 in infoArray)
                            {
                                if (!info3.preScript)
                                {
                                    Environment.CurrentDirectory = currentDirectory;
                                    info3.args[1] = FileParser.ResolveFile(info3.args[1], optionsBase.searchDirs);
                                    CSExecutor executor2 = new CSExecutor(info3.abortOnError, optionsBase);
                                    if (optionsBase.DBG)
                                    {
                                        ArrayList list6 = new ArrayList();
                                        list6.AddRange(info3.args);
                                        list6.Insert(0, CSSUtils.cmdFlagPrefix + "dbg");
                                        info3.args = (string[]) list6.ToArray(typeof(string));
                                    }
                                    if (optionsBase.verbose)
                                    {
                                        ArrayList list7 = new ArrayList();
                                        list7.AddRange(info3.args);
                                        list7.Insert(0, CSSUtils.cmdFlagPrefix + "verbose");
                                        info3.args = (string[]) list7.ToArray(typeof(string));
                                    }
                                    if (info3.abortOnError)
                                    {
                                        executor2.Rethrow = true;
                                        executor2.Execute(info3.args, printDelg, optionsBase.scriptFileName);
                                    }
                                    else
                                    {
                                        executor2.Execute(info3.args, null, optionsBase.scriptFileName);
                                    }
                                }
                            }
                        }
                    }
                }
                else
                {
                    this.ShowHelp();
                }
            }
            catch (Surrogate86ProcessRequiredException)
            {
                throw;
            }
            catch (SurrogateHostProcessRequiredException)
            {
                throw;
            }
            catch (Exception exception)
            {
                Exception innerException = exception;
                if (exception is TargetInvocationException)
                {
                    innerException = exception.InnerException;
                }
                if (this.rethrow)
                {
                    throw innerException;
                }
                Environment.ExitCode = 1;
                if (!CSSUtils.IsRuntimeErrorReportingSupressed)
                {
                    if (options.reportDetailedErrorInfo)
                    {
                        print(innerException.ToString());
                    }
                    else
                    {
                        print(innerException.Message);
                    }
                }
            }
        }

        private void ExecuteImpl()
        {
            try
            {
                if (options.processFile)
                {
                    CSharpParser.InitInfo initContext = options.initContext as CSharpParser.InitInfo;
                    if ((initContext != null) && initContext.CoInitializeSecurity)
                    {
                        ComInitSecurity(initContext.RpcImpLevel, initContext.EoAuthnCap);
                    }
                    if (options.local)
                    {
                        Environment.CurrentDirectory = Path.GetDirectoryName(Path.GetFullPath(options.scriptFileName));
                    }
                    if (!options.noLogo)
                    {
                        Console.WriteLine(AppInfo.appLogo);
                    }
                    if (Environment.GetEnvironmentVariable("EntryScript") == null)
                    {
                        Environment.SetEnvironmentVariable("EntryScript", Path.GetFullPath(options.scriptFileName));
                    }
                    CSSUtils.VerbosePrint("> ----------------", options);
                    CSSUtils.VerbosePrint("  TragetFramework: " + options.TargetFramework, options);
                    CSSUtils.VerbosePrint("  Provider: " + options.altCompiler, options);
                    try
                    {
                        CSSUtils.VerbosePrint("  Engine: " + Assembly.GetExecutingAssembly().Location, options);
                    }
                    catch
                    {
                    }
                    try
                    {
                        CSSUtils.VerbosePrint(string.Format("  Console Encoding: {0} ({1}) - {2}", Console.OutputEncoding.WebName, Console.OutputEncoding.EncodingName, Utils.IsDeraultConsoleEncoding ? "system default" : "set by engine"), options);
                    }
                    catch
                    {
                    }
                    CSSUtils.VerbosePrint("  CurrentDirectory: " + Environment.CurrentDirectory, options);
                    if (!Utils.IsLinux())
                    {
                        CSSUtils.VerbosePrint("  NuGet manager: " + NuGet.NuGetExeView, options);
                        CSSUtils.VerbosePrint("  NuGet cache: " + NuGet.NuGetCacheView, options);
                    }
                    CSSUtils.VerbosePrint("  Executing: " + Path.GetFullPath(options.scriptFileName), options);
                    CSSUtils.VerbosePrint("  Script arguments: ", options);
                    for (int i = 0; i < this.scriptArgs.Length; i++)
                    {
                        CSSUtils.VerbosePrint(string.Concat(new object[] { "    ", i, " - ", this.scriptArgs[i] }), options);
                    }
                    CSSUtils.VerbosePrint("  SearchDirectories: ", options);
                    for (int j = 0; j < options.searchDirs.Length; j++)
                    {
                        CSSUtils.VerbosePrint(string.Concat(new object[] { "    ", j, " - ", options.searchDirs[j] }), options);
                    }
                    CSSUtils.VerbosePrint("> ----------------", options);
                    CSSUtils.VerbosePrint("", options);
                    bool flag = false;
                    using (Mutex mutex = Utils.FileLock(options.scriptFileName, "TimestampValidating"))
                    {
                        using (Mutex mutex2 = Utils.FileLock(options.scriptFileName, "Compiling"))
                        {
                            using (Mutex mutex3 = Utils.FileLock(options.scriptFileName, "Executing"))
                            {
                                try
                                {
                                    mutex.WaitOne(-1, false);
                                    string assembly = options.useCompiled ? this.GetAvailableAssembly(options.scriptFileName) : null;
                                    if ((options.useCompiled && options.useSmartCaching) && ((assembly != null) && MetaDataItems.IsOutOfDate(options.scriptFileName, assembly)))
                                    {
                                        assembly = null;
                                    }
                                    if (options.forceCompile && (assembly != null))
                                    {
                                        mutex2.WaitOne(0xbb8, false);
                                        Utils.FileDelete(assembly, true);
                                        assembly = null;
                                    }
                                    if (assembly != null)
                                    {
                                        Utils.ReleaseFileLock(mutex);
                                    }
                                    string environmentVariable = Environment.GetEnvironmentVariable("PATH");
                                    foreach (string str3 in options.searchDirs)
                                    {
                                        environmentVariable = environmentVariable + ";" + str3;
                                    }
                                    SetEnvironmentVariable("PATH", environmentVariable);
                                    string launcherName = ScriptLauncherBuilder.GetLauncherName(assembly);
                                    bool flag2 = options.useSurrogateHostingProcess && (!File.Exists(launcherName) || !CSSUtils.HaveSameTimestamp(launcherName, assembly));
                                    if (((options.buildExecutable || !options.useCompiled) || (options.useCompiled && (assembly == null))) || (options.forceCompile || flag2))
                                    {
                                        mutex2.WaitOne(0xbb8, false);
                                        if (mutex3.WaitOne(0xbb8, false))
                                        {
                                            Utils.WaitForFileIdle(assembly, 0x3e8);
                                        }
                                        try
                                        {
                                            CSSUtils.VerbosePrint("Compiling script...", options);
                                            CSSUtils.VerbosePrint("", options);
                                            TimeSpan elapsed = Profiler.Stopwatch.Elapsed;
                                            Profiler.Stopwatch.Reset();
                                            Profiler.Stopwatch.Start();
                                            assembly = this.Compile(options.scriptFileName);
                                            if (Profiler.Stopwatch.IsRunning)
                                            {
                                                Profiler.Stopwatch.Stop();
                                                TimeSpan span2 = Profiler.Stopwatch.Elapsed;
                                                CSSUtils.VerbosePrint("Initialization time: " + elapsed.TotalMilliseconds + " msec", options);
                                                CSSUtils.VerbosePrint("Compilation time:    " + span2.TotalMilliseconds + " msec", options);
                                                CSSUtils.VerbosePrint("> ----------------", options);
                                                CSSUtils.VerbosePrint("", options);
                                            }
                                        }
                                        catch
                                        {
                                            if (!CSSUtils.IsRuntimeErrorReportingSupressed)
                                            {
                                                print("Error: Specified file could not be compiled.\n");
                                            }
                                            throw;
                                        }
                                        finally
                                        {
                                            Utils.ReleaseFileLock(mutex);
                                            Utils.ReleaseFileLock(mutex2);
                                            flag = true;
                                        }
                                    }
                                    else
                                    {
                                        Profiler.Stopwatch.Stop();
                                        CSSUtils.VerbosePrint("  Loading script from cache...", options);
                                        CSSUtils.VerbosePrint("", options);
                                        CSSUtils.VerbosePrint("  Cache file: \n       " + assembly, options);
                                        CSSUtils.VerbosePrint("> ----------------", options);
                                        CSSUtils.VerbosePrint("Initialization time: " + Profiler.Stopwatch.Elapsed.TotalMilliseconds + " msec", options);
                                        CSSUtils.VerbosePrint("> ----------------", options);
                                    }
                                    if (!options.supressExecution)
                                    {
                                        try
                                        {
                                            if (options.useSurrogateHostingProcess)
                                            {
                                                throw new SurrogateHostProcessRequiredException(assembly, this.scriptArgs, options.startDebugger);
                                            }
                                            if (options.startDebugger)
                                            {
                                                SaveDebuggingMetadata(options.scriptFileName);
                                                Debugger.Launch();
                                                if (Debugger.IsAttached)
                                                {
                                                    Debugger.Break();
                                                }
                                            }
                                            if (options.useCompiled || (options.cleanupShellCommand != ""))
                                            {
                                                AssemblyResolver.CacheProbingResults = true;
                                                new RemoteExecutor(options.searchDirs).ExecuteAssembly(assembly, this.scriptArgs, mutex3);
                                            }
                                            else
                                            {
                                                new AssemblyExecutor(assembly, "AsmExecution").Execute(this.scriptArgs);
                                            }
                                        }
                                        catch (SurrogateHostProcessRequiredException)
                                        {
                                            throw;
                                        }
                                        catch
                                        {
                                            if (!CSSUtils.IsRuntimeErrorReportingSupressed)
                                            {
                                                print("Error: Specified file could not be executed.\n");
                                            }
                                            throw;
                                        }
                                        if ((File.Exists(assembly) && !options.useCompiled) && (options.cleanupShellCommand == ""))
                                        {
                                            Utils.FileDelete(assembly);
                                        }
                                        if (options.cleanupShellCommand != "")
                                        {
                                            try
                                            {
                                                string path = Path.Combine(GetScriptTempDir(), "counter.txt");
                                                int num3 = 0;
                                                if (File.Exists(path))
                                                {
                                                    using (StreamReader reader = new StreamReader(path))
                                                    {
                                                        num3 = int.Parse(reader.ReadToEnd());
                                                    }
                                                }
                                                if (num3 > options.doCleanupAfterNumberOfRuns)
                                                {
                                                    num3 = 1;
                                                    string[] strArray = options.ExtractShellCommand(options.cleanupShellCommand);
                                                    if (strArray.Length > 1)
                                                    {
                                                        Process.Start(strArray[0], strArray[1]);
                                                    }
                                                    else
                                                    {
                                                        Process.Start(strArray[0]);
                                                    }
                                                }
                                                else
                                                {
                                                    num3++;
                                                }
                                                using (StreamWriter writer = new StreamWriter(path))
                                                {
                                                    writer.Write(num3);
                                                }
                                            }
                                            catch
                                            {
                                            }
                                        }
                                    }
                                }
                                finally
                                {
                                    if (!flag)
                                    {
                                        Utils.ReleaseFileLock(mutex2);
                                    }
                                    Utils.ReleaseFileLock(mutex3);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                Exception innerException = exception;
                if (exception is TargetInvocationException)
                {
                    innerException = exception.InnerException;
                }
                if (this.rethrow || (exception is SurrogateHostProcessRequiredException))
                {
                    this.lastException = innerException;
                }
                else
                {
                    Environment.ExitCode = 1;
                    if (!CSSUtils.IsRuntimeErrorReportingSupressed)
                    {
                        if (options.reportDetailedErrorInfo)
                        {
                            print(innerException.ToString());
                        }
                        else
                        {
                            print(innerException.Message);
                        }
                    }
                }
            }
        }

        internal string GetAvailableAssembly(string scripFileName)
        {
            string str = null;
            string path = (options.hideTemp != Settings.HideOptions.DoNotHide) ? Path.Combine(ScriptCacheDir, Path.GetFileName(scripFileName) + ".compiled") : (scripFileName + ".c");
            if (File.Exists(path) && File.Exists(scripFileName))
            {
                FileInfo info = new FileInfo(scripFileName);
                FileInfo info2 = new FileInfo(path);
                if ((info2.LastWriteTime == info.LastWriteTime) && (info2.LastWriteTimeUtc == info.LastWriteTimeUtc))
                {
                    str = path;
                }
            }
            return str;
        }

        public static string GetCacheDirectory(string file)
        {
            string str = Path.Combine(GetScriptTempDir(), "Cache");
            string directoryName = Path.GetDirectoryName(Path.GetFullPath(file));
            if (!Utils.IsLinux())
            {
                directoryName = directoryName.ToLower();
            }
            string path = Path.Combine(str, CSSUtils.GetHashCodeEx(directoryName).ToString());
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
            return path;
        }

        internal string GetCustomAppConfig(string[] args)
        {
            try
            {
                if (args.Length > 0)
                {
                    int index = CSSUtils.ParseAppArgs(args, this);
                    if (args.Length > index)
                    {
                        Settings settings = null;
                        if (options.noConfig)
                        {
                            if (options.altConfig != "")
                            {
                                settings = Settings.Load(Path.GetFullPath(options.altConfig));
                            }
                        }
                        else if (!string.IsNullOrEmpty(Assembly.GetExecutingAssembly().Location))
                        {
                            settings = Settings.Load(Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "css_config.xml"));
                        }
                        if (!options.useScriptConfig && ((settings == null) || (settings.DefaultArguments.IndexOf(CSSUtils.cmdFlagPrefix + "sconfig") == -1)))
                        {
                            return "";
                        }
                        string fileName = args[index];
                        ArrayList list = new ArrayList();
                        string str2 = Environment.ExpandEnvironmentVariables("%CSSCRIPT_DIR%" + Path.DirectorySeparatorChar + "lib");
                        if (!str2.StartsWith("%"))
                        {
                            list.Add(str2);
                        }
                        if (settings != null)
                        {
                            list.AddRange(Environment.ExpandEnvironmentVariables(settings.SearchDirs).Split(",;".ToCharArray()));
                        }
                        list.Add(Utils.GetAssemblyDirectoryName(Assembly.GetExecutingAssembly()));
                        string[] extraDirs = (string[]) list.ToArray(typeof(string));
                        fileName = FileParser.ResolveFile(fileName, extraDirs);
                        if (options.customConfigFileName != "")
                        {
                            return Path.Combine(Path.GetDirectoryName(fileName), options.customConfigFileName);
                        }
                        if (File.Exists(fileName + ".config"))
                        {
                            return (fileName + ".config");
                        }
                        if (File.Exists(Path.ChangeExtension(fileName, ".exe.config")))
                        {
                            return Path.ChangeExtension(fileName, ".exe.config");
                        }
                    }
                }
            }
            catch
            {
            }
            return "";
        }

        public ExecuteOptions GetOptions()
        {
            return options;
        }

        private Settings GetPersistedSettings(ArrayList appArgs)
        {
            Settings settings = null;
            if (options.noConfig)
            {
                if (options.altConfig != "")
                {
                    settings = Settings.Load(Path.GetFullPath(options.altConfig));
                }
                else
                {
                    settings = Settings.Load(null, true);
                }
            }
            else if (!CSSUtils.IsDynamic(Assembly.GetExecutingAssembly()))
            {
                settings = Settings.Load(Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "css_config.xml"));
            }
            if (settings != null)
            {
                options.hideTemp = settings.HideAutoGeneratedFiles;
                if (options.preCompilers == "")
                {
                    options.preCompilers = settings.Precompiler;
                }
                options.altCompiler = settings.ExpandUseAlternativeCompiler();
                options.defaultRefAssemblies = settings.ExpandDefaultRefAssemblies();
                options.postProcessor = settings.ExpandUsePostProcessor();
                options.apartmentState = settings.DefaultApartmentState;
                options.reportDetailedErrorInfo = settings.ReportDetailedErrorInfo;
                options.openEndDirectiveSyntax = settings.OpenEndDirectiveSyntax;
                options.consoleEncoding = settings.ConsoleEncoding;
                options.cleanupShellCommand = settings.ExpandCleanupShellCommand();
                options.doCleanupAfterNumberOfRuns = settings.DoCleanupAfterNumberOfRuns;
                options.inMemoryAsm = settings.InMemoryAssembly;
                options.hideCompilerWarnings = settings.HideCompilerWarnings;
                options.TargetFramework = settings.TargetFramework;
                string[] args = Utils.RemoveEmptyStrings(settings.DefaultArguments.Split(" ".ToCharArray()));
                int index = CSSUtils.ParseAppArgs(args, this);
                if (index == args.Length)
                {
                    return settings;
                }
                options.scriptFileName = args[index];
                for (int i = index + 1; i < args.Length; i++)
                {
                    if (args[i].Trim().Length != 0)
                    {
                        appArgs.Add(args[i]);
                    }
                }
            }
            return settings;
        }

        public static string GetScriptTempDir()
        {
            if (tempDir == null)
            {
                tempDir = Path.Combine(Path.GetTempPath(), "CSSCRIPT");
                if (!Directory.Exists(tempDir))
                {
                    Directory.CreateDirectory(tempDir);
                }
            }
            return tempDir;
        }

        public static string GetScriptTempFile()
        {
            lock (typeof(CSExecutor))
            {
                return Path.Combine(GetScriptTempDir(), Guid.NewGuid().ToString() + ".tmp");
            }
        }

        internal static string GetScriptTempFile(string subDir)
        {
            lock (typeof(CSExecutor))
            {
                string path = Path.Combine(GetScriptTempDir(), subDir);
                if (!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path);
                }
                return Path.Combine(path, Guid.NewGuid() + ".tmp");
            }
        }

        [DllImport("kernel32.dll", SetLastError=true)]
        private static extern uint GetTempFileName(string lpPathName, string lpPrefixString, uint uUnique, [Out] StringBuilder lpTempFileName);
        private ICodeCompiler LoadCompiler(string scriptFileName, ref string[] filesToInject)
        {
            ICodeCompiler compiler;
            if (options.InjectScriptAssemblyAttribute && ((options.altCompiler == "") || scriptFileName.EndsWith(".cs")))
            {
                filesToInject = Utils.Concat(filesToInject, CSSUtils.GetScriptedCodeAttributeInjectionCode(scriptFileName));
            }
            if (options.altCompiler == "")
            {
                return this.LoadDefaultCompiler();
            }
            try
            {
                Assembly assembly = null;
                if (Path.IsPathRooted(options.altCompiler))
                {
                    if (File.Exists(options.altCompiler))
                    {
                        assembly = Assembly.LoadFrom(options.altCompiler);
                    }
                }
                else
                {
                    string fullPath = Path.GetFullPath(Utils.GetAssemblyDirectoryName(Assembly.GetExecutingAssembly()));
                    string path = Path.Combine(fullPath, options.altCompiler);
                    if (File.Exists(path))
                    {
                        assembly = Assembly.LoadFrom(path);
                    }
                    else
                    {
                        path = Path.Combine(Path.Combine(fullPath, "Lib"), options.altCompiler);
                        if (File.Exists(path))
                        {
                            assembly = Assembly.LoadFrom(path);
                        }
                        else
                        {
                            path = Path.Combine(Path.GetFullPath(Utils.GetAssemblyDirectoryName(base.GetType().Assembly)), options.altCompiler);
                            if (!File.Exists(path))
                            {
                                throw new ApplicationException("Cannot find alternative compiler \"" + options.altCompiler + "\"");
                            }
                            assembly = Assembly.LoadFrom(path);
                        }
                    }
                }
                compiler = (ICodeCompiler) assembly.GetModules()[0].FindTypes(Module.FilterTypeName, "CSSCodeProvider")[0].GetMethod("CreateCompiler").Invoke(null, new object[] { scriptFileName });
            }
            catch (Exception exception)
            {
                try
                {
                    if (!Assembly.GetExecutingAssembly().Location.ToLower().EndsWith("csscriptlibrary.dll"))
                    {
                        string environmentVariable = Environment.GetEnvironmentVariable("CSSCRIPT_DIR");
                        if (environmentVariable != null)
                        {
                            if (Directory.Exists(environmentVariable) && !File.Exists(options.altCompiler))
                            {
                                print("\nCannot find alternative compiler (" + options.altCompiler + "). Loading default compiler instead.");
                            }
                            options.altCompiler = "";
                            return this.LoadDefaultCompiler();
                        }
                    }
                }
                catch
                {
                }
                throw new ApplicationException("Cannot use alternative compiler (" + options.altCompiler + "). You may want to adjust 'CSSCRIPT_DIR' environment variable or disable alternative compiler by setting 'useAlternativeCompiler' to empty value in the css_config.xml file.\n\nError Details:", exception);
            }
            return compiler;
        }

        private ICodeCompiler LoadDefaultCompiler()
        {
            return new CSharpCodeProvider().CreateCompiler();
        }

        private string NormalizeGacAssemblyPath(string asm)
        {
            string str = string.Format("v{0}.{1}", Environment.Version.Major, Environment.Version.MajorRevision);
            if ((options.useSurrogateHostingProcess && (options.TargetFramework != str)) && (asm.IndexOf(@"\GAC_MSIL\") != -1))
            {
                return Path.GetFileName(asm);
            }
            return asm;
        }

        private void ProcessCompilingResult(CompilerResults results, CompilerParameters compilerParams, ScriptParser parser, string scriptFileName, string assemblyFileName, string[] additionalDependencies)
        {
            this.LastCompileResult = results;
            if (results.Errors.HasErrors)
            {
                throw CompilerException.Create(results.Errors, options.hideCompilerWarnings);
            }
            if (options.verbose)
            {
                Console.WriteLine("  Compiler Oputput: ", options);
                foreach (CompilerError error in results.Errors)
                {
                    Console.WriteLine("  {0}({1},{2}):{3} {4} {5}", new object[] { error.FileName, error.Line, error.Column, error.IsWarning ? "warning" : "error", error.ErrorNumber, error.ErrorText });
                }
                Console.WriteLine("> ----------------", options);
            }
            if (!options.DBG)
            {
                parser.DeleteImportedFiles();
                Utils.FileDelete(Path.Combine(Path.GetDirectoryName(assemblyFileName), Path.GetFileNameWithoutExtension(assemblyFileName) + ".pdb"));
            }
            if (options.useCompiled)
            {
                if (options.useSmartCaching)
                {
                    MetaDataItems items = new MetaDataItems();
                    string[] searchDirs = Utils.RemovePathDuplicates(options.searchDirs);
                    items.AddItems(parser.ImportedFiles, false, searchDirs);
                    items.AddItems(additionalDependencies, false, new string[0]);
                    foreach (string str2 in items.AddItems(compilerParams.ReferencedAssemblies, true, searchDirs))
                    {
                        options.AddSearchDir(str2);
                    }
                    items.StampFile(assemblyFileName);
                }
                FileInfo info = new FileInfo(scriptFileName);
                FileInfo info2 = new FileInfo(assemblyFileName);
                if ((info != null) && (info2 != null))
                {
                    info2.LastWriteTimeUtc = info.LastWriteTimeUtc;
                }
            }
        }

        private static void SaveDebuggingMetadata(string scriptFile)
        {
        }

        [DllImport("kernel32.dll", SetLastError=true)]
        private static extern bool SetEnvironmentVariable(string lpName, string lpValue);
        public static void SetScriptCacheDir(string scriptFile)
        {
            string cacheDirectory = GetCacheDirectory(scriptFile);
            string path = Path.Combine(cacheDirectory, "css_info.txt");
            try
            {
                using (StreamWriter writer = new StreamWriter(path))
                {
                    writer.Write(Environment.Version.ToString() + "\n" + Path.GetDirectoryName(Path.GetFullPath(scriptFile)) + "\n");
                }
            }
            catch
            {
            }
            cacheDir = cacheDirectory;
        }

        public static void SetScriptTempDir(string path)
        {
            tempDir = path;
        }

        public void ShowHelp()
        {
            print(HelpProvider.BuildCommandInterfaceHelp());
        }

        public void ShowPrecompilerSample()
        {
            print(HelpProvider.BuildPrecompilerSampleCode());
        }

        public void ShowSample()
        {
            print(HelpProvider.BuildSampleCode());
        }

        public void ShowVersion()
        {
            print(HelpProvider.BuildVersionInfo());
        }

        private static void VoidPrint(string msg)
        {
        }

        public bool Rethrow
        {
            get
            {
                return this.rethrow;
            }
            set
            {
                this.rethrow = value;
            }
        }

        public static string ScriptCacheDir
        {
            get
            {
                return cacheDir;
            }
        }

        private class UniqueAssemblyLocations
        {
            private Hashtable locations = new Hashtable();

            public void AddAssembly(string location)
            {
                string key = Path.GetFileName(location).ToUpperInvariant();
                if (!this.locations.ContainsKey(key))
                {
                    this.locations[key] = location;
                }
            }

            public bool ContainsAssembly(string name)
            {
                string str = name.ToUpperInvariant();
                foreach (string str2 in this.locations.Keys)
                {
                    if (Path.GetFileNameWithoutExtension(str2) == str)
                    {
                        return true;
                    }
                }
                return false;
            }

            public static explicit operator string[](CSExecutor.UniqueAssemblyLocations obj)
            {
                string[] array = new string[obj.locations.Count];
                obj.locations.Values.CopyTo(array, 0);
                return array;
            }
        }
    }
}

