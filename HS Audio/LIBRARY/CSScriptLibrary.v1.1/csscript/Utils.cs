﻿namespace csscript
{
    using System;
    using System.CodeDom.Compiler;
    using System.Collections;
    using System.Globalization;
    using System.IO;
    using System.Reflection;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using System.Threading;

    internal class Utils
    {
        public static bool IsDeraultConsoleEncoding = true;
        public static ProcessNewEncodingHandler ProcessNewEncoding = new ProcessNewEncodingHandler(Utils.DefaultProcessNewEncoding);

        public static void AddCompilerOptions(CompilerParameters compilerParams, string option)
        {
            compilerParams.CompilerOptions = compilerParams.CompilerOptions + option + " ";
        }

        public static string[] Concat(string[] array1, string[] array2)
        {
            string[] destinationArray = new string[array1.Length + array2.Length];
            Array.Copy(array1, 0, destinationArray, 0, array1.Length);
            Array.Copy(array2, 0, destinationArray, array1.Length, array2.Length);
            return destinationArray;
        }

        public static string[] Concat(string[] array1, string item)
        {
            string[] destinationArray = new string[array1.Length + 1];
            Array.Copy(array1, 0, destinationArray, 0, array1.Length);
            destinationArray[destinationArray.Length - 1] = item;
            return destinationArray;
        }

        public static bool ContainsPath(string path, string subPath)
        {
            return (PathCompare(path.Substring(0, subPath.Length), subPath) == 0);
        }

        private static string DefaultProcessNewEncoding(string requestedEncoding)
        {
            return requestedEncoding;
        }

        public static string[] Except(string[] array1, string[] array2)
        {
            ArrayList list = new ArrayList();
            foreach (string str in array1)
            {
                bool flag = false;
                foreach (string str2 in array2)
                {
                    if (str2 == str)
                    {
                        flag = true;
                        break;
                    }
                }
                if (!flag)
                {
                    list.Add(str);
                }
            }
            return (string[]) list.ToArray(typeof(string));
        }

        public static void FileDelete(string path)
        {
            FileDelete(path, false);
        }

        public static void FileDelete(string path, bool rethrow)
        {
            for (int i = 0; i < 3; i++)
            {
                try
                {
                    if (File.Exists(path))
                    {
                        File.Delete(path);
                    }
                    break;
                }
                catch
                {
                    if (rethrow && (i == 2))
                    {
                        throw;
                    }
                }
                Thread.Sleep(300);
            }
        }

        public static Mutex FileLock(string file)
        {
            return FileLock(file, "");
        }

        public static Mutex FileLock(string file, object context)
        {
            if (!IsLinux())
            {
                file = file.ToLower(CultureInfo.InvariantCulture);
            }
            return new Mutex(false, context.ToString() + "." + CSSUtils.GetHashCodeEx(file).ToString());
        }

        public static string GetAssemblyDirectoryName(Assembly asm)
        {
            if (CSSUtils.IsDynamic(asm))
            {
                return "";
            }
            return Path.GetDirectoryName(asm.Location);
        }

        public static string GetAssemblyFileName(Assembly asm)
        {
            if (CSSUtils.IsDynamic(asm))
            {
                return "";
            }
            return Path.GetFileName(asm.Location);
        }

        private static bool IsFileLocked(string file)
        {
            try
            {
                using (File.Open(file, FileMode.Open))
                {
                }
            }
            catch (IOException exception)
            {
                int num = Marshal.GetHRForException(exception) & 0xffff;
                return ((num == 0x20) || (num == 0x21));
            }
            return false;
        }

        public static bool IsLinux()
        {
            return (Environment.OSVersion.Platform == PlatformID.Unix);
        }

        public static bool IsNet20Plus()
        {
            return (Environment.Version.Major >= 2);
        }

        public static bool IsNet40Plus()
        {
            return (Environment.Version.Major >= 4);
        }

        public static bool IsNet45Plus()
        {
            return (Type.GetType("System.Reflection.ReflectionContext", false) != null);
        }

        public static bool IsNullOrWhiteSpace(string text)
        {
            if (text != null)
            {
                return (text.Trim() == "");
            }
            return true;
        }

        public static bool IsRuntimeCompatibleAsm(string file)
        {
            try
            {
                AssemblyName.GetAssemblyName(file);
                return true;
            }
            catch
            {
            }
            return false;
        }

        public static bool IsSamePath(string path1, string path2)
        {
            return (PathCompare(path1, path2) == 0);
        }

        public static int PathCompare(string path1, string path2)
        {
            if (IsLinux())
            {
                return string.Compare(path1, path2);
            }
            return string.Compare(path1, path2, true);
        }

        public static void ReleaseFileLock(Mutex @lock)
        {
            if (@lock != null)
            {
                try
                {
                    @lock.ReleaseMutex();
                }
                catch
                {
                }
            }
        }

        public static string RemoveAssemblyExtension(string asmName)
        {
            if (!asmName.ToLower().EndsWith(".dll") && !asmName.ToLower().EndsWith(".exe"))
            {
                return asmName;
            }
            return asmName.Substring(0, asmName.Length - 4);
        }

        public static string[] RemoveDuplicates(string[] list)
        {
            ArrayList list2 = new ArrayList();
            foreach (string str in list)
            {
                if ((str.Trim() != "") && !list2.Contains(str))
                {
                    list2.Add(str);
                }
            }
            return (string[]) list2.ToArray(typeof(string));
        }

        public static string[] RemoveEmptyStrings(string[] list)
        {
            ArrayList list2 = new ArrayList();
            foreach (string str in list)
            {
                if (str.Trim() != "")
                {
                    list2.Add(str);
                }
            }
            return (string[]) list2.ToArray(typeof(string));
        }

        public static string[] RemovePathDuplicates(string[] list)
        {
            ArrayList list2 = new ArrayList();
            foreach (string str in list)
            {
                string fullPath = Path.GetFullPath(str.Trim());
                bool flag = false;
                foreach (string str3 in list2)
                {
                    if (IsSamePath(str3, fullPath))
                    {
                        flag = true;
                        break;
                    }
                }
                if (!flag)
                {
                    list2.Add(fullPath);
                }
            }
            return (string[]) list2.ToArray(typeof(string));
        }

        public static bool WaitForFileIdle(string file, int delay)
        {
            if ((file == null) || !File.Exists(file))
            {
                return true;
            }
            int tickCount = Environment.TickCount;
            while (((Environment.TickCount - tickCount) <= delay) && IsFileLocked(file))
            {
                Thread.Sleep(200);
            }
            return IsFileLocked(file);
        }

        public delegate string ProcessNewEncodingHandler(string requestedEncoding);
    }
}

