﻿namespace csscript
{
    using System;
    using System.IO;
    using System.Reflection;

    internal class NuGet
    {
        private static string nuGetCache;
        private static string nuGetExe;

        private static bool IsPackageDownloaded(string packageDir)
        {
            return (Directory.Exists(packageDir) && (Directory.GetDirectories(packageDir).Length > 0));
        }

        public static string[] Resolve(string[] packages, bool supressDownloading)
        {
            return new string[0];
        }

        private static string NuGetCache
        {
            get
            {
                if (nuGetCache == null)
                {
                    nuGetCache = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData), "CS-Script" + Path.DirectorySeparatorChar + "nuget");
                    if (!Directory.Exists(nuGetCache))
                    {
                        Directory.CreateDirectory(nuGetCache);
                    }
                }
                return nuGetCache;
            }
        }

        public static string NuGetCacheView
        {
            get
            {
                if (!Directory.Exists(NuGetCache))
                {
                    return "<not found>";
                }
                return NuGetCache;
            }
        }

        private static string NuGetExe
        {
            get
            {
                if (nuGetExe == null)
                {
                    nuGetExe = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "nuget.exe");
                    if (!File.Exists(nuGetExe))
                    {
                        nuGetExe = Path.Combine(Path.Combine(Environment.ExpandEnvironmentVariables("%CSSCRIPT_DIR%"), "lib"), "nuget.exe");
                        if (!File.Exists(nuGetExe))
                        {
                            nuGetExe = null;
                        }
                    }
                }
                return nuGetExe;
            }
        }

        public static string NuGetExeView
        {
            get
            {
                if (!File.Exists(NuGetExe))
                {
                    return "<not found>";
                }
                return NuGetExe;
            }
        }
    }
}

