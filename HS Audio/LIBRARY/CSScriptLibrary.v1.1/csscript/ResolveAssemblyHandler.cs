﻿namespace csscript
{
    using System;
    using System.Runtime.CompilerServices;

    public delegate string[] ResolveAssemblyHandler(string file, string[] searchDirs);
}

