﻿namespace csscript
{
    using System;

    internal interface IScriptExecutor
    {
        void CreateDefaultConfigFile();
        ExecuteOptions GetOptions();
        void ShowHelp();
        void ShowPrecompilerSample();
        void ShowSample();
        void ShowVersion();
    }
}

