﻿namespace csscript
{
    using System;
    using System.IO;
    using System.Reflection;

    internal class AssemblyExecutor
    {
        private AppDomain appDomain;
        private string assemblyFileName;
        private RemoteExecutor remoteExecutor;

        public AssemblyExecutor(string fileNname, string domainName)
        {
            this.assemblyFileName = fileNname;
            AppDomainSetup info = new AppDomainSetup {
                ApplicationBase = Path.GetDirectoryName(this.assemblyFileName),
                PrivateBinPath = AppDomain.CurrentDomain.BaseDirectory,
                ApplicationName = Utils.GetAssemblyFileName(Assembly.GetExecutingAssembly()),
                ShadowCopyFiles = "true",
                ShadowCopyDirectories = Path.GetDirectoryName(this.assemblyFileName)
            };
            this.appDomain = AppDomain.CreateDomain(domainName, null, info);
            this.remoteExecutor = (RemoteExecutor) this.appDomain.CreateInstanceFromAndUnwrap(Assembly.GetExecutingAssembly().FullName, typeof(RemoteExecutor).ToString());
            this.remoteExecutor.searchDirs = ExecuteOptions.options.searchDirs;
        }

        public void Execute(string[] args)
        {
            this.remoteExecutor.ExecuteAssembly(this.assemblyFileName, args);
        }

        public void Unload()
        {
            AppDomain.Unload(this.appDomain);
            this.appDomain = null;
        }
    }
}

