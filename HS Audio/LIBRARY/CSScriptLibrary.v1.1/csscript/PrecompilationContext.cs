﻿namespace csscript
{
    using System;
    using System.Collections.Generic;

    internal class PrecompilationContext
    {
        public List<string> NewDependencies = new List<string>();
        public List<string> NewIncludes = new List<string>();
        public List<string> NewReferences = new List<string>();
        public List<string> NewSearchDirs = new List<string>();
        public string[] SearchDirs = new string[0];
    }
}

