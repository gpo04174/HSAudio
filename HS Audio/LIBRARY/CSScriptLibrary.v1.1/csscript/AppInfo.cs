﻿namespace csscript
{
    using System;
    using System.Reflection;

    internal class AppInfo
    {
        public static bool appConsole = false;
        public static string appName = "CSScriptLibrary";
        public static string appParams = "[/nl]:";
        public static string appParamsHelp = "nl\t-\tNo logo mode: No banner will be shown at execution time.\n";

        public static string appLogo
        {
            get
            {
                return ("C# Script execution engine. Version " + Assembly.GetExecutingAssembly().GetName().Version.ToString() + ".\nCopyright (C) 2004 Oleg Shilo.\n");
            }
        }

        public static string appLogoShort
        {
            get
            {
                return ("C# Script execution engine. Version " + Assembly.GetExecutingAssembly().GetName().Version.ToString() + ".\n");
            }
        }
    }
}

