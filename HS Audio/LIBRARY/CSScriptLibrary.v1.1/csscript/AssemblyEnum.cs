﻿namespace csscript
{
    using System;
    using System.Runtime.InteropServices;
    using System.Text;

    [ComVisible(false)]
    internal class AssemblyEnum
    {
        private IAssemblyEnum m_assemblyEnum;
        private bool m_done;

        public AssemblyEnum(string sAsmName)
        {
            IAssemblyName ppAssemblyNameObj = null;
            if (sAsmName != null)
            {
                COM.CheckHR(CreateAssemblyNameObject(out ppAssemblyNameObj, sAsmName, CreateAssemblyNameObjectFlags.CANOF_PARSE_DISPLAY_NAME, IntPtr.Zero));
            }
            COM.CheckHR(CreateAssemblyEnum(out this.m_assemblyEnum, IntPtr.Zero, ppAssemblyNameObj, AssemblyCacheFlags.GAC, IntPtr.Zero));
        }

        [DllImport("fusion.dll")]
        internal static extern int CreateAssemblyEnum(out IAssemblyEnum ppEnum, IntPtr pUnkReserved, IAssemblyName pName, AssemblyCacheFlags flags, IntPtr pvReserved);
        [DllImport("fusion.dll")]
        internal static extern int CreateAssemblyNameObject(out IAssemblyName ppAssemblyNameObj, [MarshalAs(UnmanagedType.LPWStr)] string szAssemblyName, CreateAssemblyNameObjectFlags flags, IntPtr pvReserved);
        private string GetFullName(IAssemblyName asmName)
        {
            StringBuilder pDisplayName = new StringBuilder(0x400);
            int capacity = pDisplayName.Capacity;
            COM.CheckHR(asmName.GetDisplayName(pDisplayName, ref capacity, 0xa7));
            return pDisplayName.ToString();
        }

        public string GetNextAssembly()
        {
            string fullName = null;
            if (!this.m_done)
            {
                IAssemblyName ppName = null;
                COM.CheckHR(this.m_assemblyEnum.GetNextAssembly(IntPtr.Zero, out ppName, 0));
                if (ppName != null)
                {
                    fullName = this.GetFullName(ppName);
                }
                this.m_done = fullName == null;
            }
            return fullName;
        }
    }
}

