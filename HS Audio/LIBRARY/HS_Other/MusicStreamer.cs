﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Text;
using HS_Audio_Lib;

namespace HS_Audio.LIBRARY
{
    //http://stackoverflow.com/questions/3435974/c-sharp-fmod-playing-stream-in-realtime
    //(2번째꺼는 스트리밍)
    class MusicStreamer
    {
        string FileName;

        HS_Audio_Lib.System sys;
        HS_Audio_Lib.Sound snd;
        CREATESOUNDEXINFO info;
        HS_Audio_Lib.Channel ch;
        HS_Audio_Lib.DSP dsp;
        HS_Audio_Lib.DSP_DESCRIPTION DSPDescription;
        HS_Audio_Lib.DSPConnection conn = new DSPConnection();


        RESULT result;

        public MusicStreamer(String FileName)
        {
            this.FileName = FileName;
            Init();
        }


        private FILE_OPENCALLBACK myopen = new FILE_OPENCALLBACK(OPENCALLBACK);
        private FILE_CLOSECALLBACK myclose = new FILE_CLOSECALLBACK(CLOSECALLBACK);
        private FILE_READCALLBACK myread = new FILE_READCALLBACK(READCALLBACK);
        private FILE_SEEKCALLBACK myseek = new FILE_SEEKCALLBACK(SEEKCALLBACK);
        void Init()
        {
            result = Factory.System_Create(ref sys);
            result = sys.setOutput(OUTPUTTYPE.NOSOUND);
            result = sys.attachFileSystem(myopen, myclose, myread, myseek);
            sys.createStream(FileName, MODE.CREATESTREAM, ref info, ref snd);
            sys.playSound(CHANNELINDEX.FREE, snd, true, ref ch);

            sys.createDSP(ref DSPDescription, ref dsp);

            ch.addDSP(dsp, ref conn);
            DSPDescription.read = dspreadcallback;
            //result = sys.setFileSystem(myopen, myclose, myread, myseek, 2048);
        }

        public void Start()
        {
            //ch.setPaused(false);
            uint length = 0;
            //BPMDetector bpm = new BPMDetector()

            snd.getLength(ref length, TIMEUNIT.MS);
            for (uint i = 0; i < length; i++)ch.setPosition(i, TIMEUNIT.MS);

        }

        HS_Audio_Lib.RESULT dspreadcallback(ref DSP_STATE dsp_state, IntPtr IN, IntPtr OUT, uint length, int inchannels, int outchannels)
        {
            unsafe
            {
                int count, count2, n, n1;
                float* inbuffer = (float*)IN.ToPointer();
                float* outbuffer = (float*)OUT.ToPointer();
                //if(A == b&c == d && e == f)

                for (count = 0; count < length; count++)
                {

                    for (count2 = 0; count2 < outchannels; count2+=2)
                    {
                        n = (count * 1) + count2;

                        n1 = (count * 2) + count2;
                        //float x = bb[inchannels].Update(b, READCALLBACK_BassBoosterFrequency, READCALLBACK_BassBoosterRatio, 48000.0f, READCALLBACK_BassBoosterLow);
                    }
                }
            }
            return RESULT.OK;
        }


        private static RESULT OPENCALLBACK([MarshalAs(UnmanagedType.LPWStr)]string name, int unicode, ref uint filesize, ref IntPtr handle, ref IntPtr userdata)
        {
            // You can ID the file from the name, then do any loading required here
            return RESULT.OK;
        }

        private static RESULT CLOSECALLBACK(IntPtr handle, IntPtr userdata)
        {
            // Do any closing required here
            return RESULT.OK;
        }

        private static RESULT READCALLBACK(IntPtr handle, IntPtr buffer, uint sizebytes, ref uint bytesread, IntPtr userdata)
        {
            byte[] readbuffer = new byte[sizebytes];

            // Populate readbuffer here with raw data

            Marshal.Copy(readbuffer, 0, buffer, (int)sizebytes);
            return RESULT.OK;
        }

        private static RESULT SEEKCALLBACK(IntPtr handle, int pos, IntPtr userdata)
        {
            // Seek your stream to desired position
            return RESULT.OK;
        }
    }
}
