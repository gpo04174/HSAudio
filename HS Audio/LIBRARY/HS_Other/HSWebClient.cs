﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Text;

namespace HS_Audio.LIBRARY.HSOther
{
    public class HSWebClient : WebClient
    {
        public Uri LastURL { get; private set; }
        public object Tag { get; set; }
        public new void DownloadFileAsync(Uri Address, string FileName)
        {
            base.DownloadFileAsync(Address, FileName);
            LastURL = Address;
        }
        public new void DownloadFile(Uri Address, string FileName)
        {
            base.DownloadFile(Address, FileName);
            LastURL = Address;
        }
        public int Timeout = 3000;

        protected override WebRequest GetWebRequest(Uri uri)
        {
            WebRequest lWebRequest = base.GetWebRequest(uri);
            lWebRequest.Timeout = Timeout;
            ((HttpWebRequest)lWebRequest).ReadWriteTimeout = Timeout;
            return lWebRequest;
        }
    }
}
