﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HS_Audio
{
    public class HSCaculateBPM
    {
        public static float GetBPMfromFile(string SoundPath)
        {
            float bpm = 0;
            return bpm;
        }
        public static float GetBPMfromStream(NAudio.Wave.WaveStream Stream)
        {
            return 0;
        }

        public HSCaculateBPM()
        {
        }
    }
    public class HSCaculateTapBPM
    {
        /// <summary>
        ///  The milliseconds in one minute.
        /// </summary>
        private const double OneMinute = 60000.0;

        /// <summary>
        ///  The highest BPM displayed.
        /// </summary>
        private const double MaxBPM = 999.9;

        /// <summary>
        ///  Captures the tempo tapped.
        /// </summary>
        private double bpm = 0.0;

        /// <summary>
        ///  Captures the times when the tempo button was tapped.
        /// </summary>
        private DateTime[] tapTimes = new DateTime[3];

        /// <summary>
        ///  Notifies listeners of changes.
        /// </summary>
        //public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        ///  Gets the text representation of the tempo.
        /// </summary>
        public string BPMString {  get {return string.Format("{0:F2} BPM", bpm); } }
        /// <summary>
        ///  Gets the tempo.
        /// </summary>
        public double BPM { get {return bpm;} }

        public void Tap()
        {
            var newTapTimes = new DateTime[3];

            Array.Copy(tapTimes, 0, newTapTimes, 1, 2);
            tapTimes = newTapTimes;
            tapTimes[0] = DateTime.Now;
            if (tapTimes[1] != default(DateTime))
            {
                var lastInterval = (tapTimes[0] - tapTimes[1]).TotalMilliseconds;
                if (tapTimes[2] != default(DateTime))
                {
                    var previousInterval = (tapTimes[1] - tapTimes[2]).TotalMilliseconds;
                    lastInterval = (lastInterval + previousInterval) / 2;
                }

                if (lastInterval > OneMinute / MaxBPM) bpm = OneMinute / lastInterval;
                else bpm = MaxBPM;

                //OnPropertyChanged("Bpm");
            }
        }

        public void Reset()
        {
            bpm = 0;
        }
    }
}
