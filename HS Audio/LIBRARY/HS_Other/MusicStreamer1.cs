﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading;
using HS_Audio_Lib;

namespace HS_Audio.LIBRARY.HSOther
{
    class MusicStreamer1 : IDisposable
    {
        public delegate void CompleteEventHandler(object sender, double[] BPMList, double BPM);
        public event CompleteEventHandler Complete;
        public delegate void ProgressEventHandler(object sender, uint Total, uint Current);
        public event ProgressEventHandler Progress;

        public bool IsComplete { get; private set; }

        ThreadStart start, start1;

        String FileName;
        HSAudioHelper Helper;

        RESULT result;
        HS_Audio_Lib.System sys;
        HS_Audio_Lib.Sound snd;

        public MusicStreamer1(String FileName)
        {
            this.FileName = FileName;
            result = Factory.System_Create(ref sys);
            try { result = sys.init(32, INITFLAGS.NORMAL, (IntPtr)null); } catch(Exception ex) { HS_Library.HSConsole.WriteLine(ex.ToString()); return;}
            result = sys.setOutput(OUTPUTTYPE.NOSOUND);

            sys.createStream(FileName, MODE.SOFTWARE|MODE._2D, ref snd);
            snd.setLoopCount(-1);

            start = new ThreadStart(ASNYC);
            start1 = new ThreadStart(PROGRESS);
        }
        public MusicStreamer1(HSAudioHelper Helper)
        {
            this.Helper = Helper;
            Helper.system.createStream(FileName, MODE.SOFTWARE | MODE._2D, ref snd);
            snd.setLoopCount(-1);

            start = new ThreadStart(ASNYC);
            start1 = new ThreadStart(PROGRESS);
        }
        public MusicStreamer1(string FileName, HSAudioHelper Helper)
        {
            this.FileName = FileName;
            this.Helper = Helper;
            Init();
            start = new ThreadStart(ASNYC);
            start1 = new ThreadStart(PROGRESS);
        }

        public SOUND_TYPE st;
        public SOUND_FORMAT sf;
        public int Channel;

        public uint Total {get; private set; }
        public uint Current{get; private set;}

        BPMDetector bmp_;
        ReadData rd;
        public double Start()
        {
            IsComplete = false;

            Close();
            Init();

            //double[] BPM_List = null;
            List<double> BPM_List = new List<double>();
            double BPM = 0;
            Current = 0;
            try
            {
                uint len = 0;
                result = snd.getLength(ref len, TIMEUNIT.PCMBYTES);
                Total = len;

                IntPtr ptr1 = IntPtr.Zero, ptr2 = IntPtr.Zero;
                uint len1 = 0, len2 = 0; ;
                uint read = 0;

                int bit = 0;

                result = snd.getFormat(ref st, ref sf, ref Channel, ref bit);

                //result = snd.@lock(0, len, ref ptr1, ref ptr2, ref len1, ref len2);
                

                uint Ext = len - ((len / 10) * 10);
                int SIZE = Ext == 0 ? 10 : 11;
                //BPM_List = new double[SIZE];
                short[] data_left_channel = new short[len / 4];//[len/10];//len2 / 1000 + 50];
                //short[] data_right_channel = new short[len/10];//len2 / 1000 + 50];

                rd = new ReadData(snd);
                short[] f;
                uint offset = 0;
                uint READ = 0;
                uint TOTALREAD = 0;

#if DEBUG
                String path = Path.GetTempFileName()+".wav";
                NAudio.Wave.WaveFileWriter ww = new NAudio.Wave.WaveFileWriter(path, new NAudio.Wave.WaveFormat((int)Helper.DefaultFrequency, 1));
#endif

                READ = rd.Read(out f);
                Current += READ;

                uint FLUSH = 0;
                while (READ != 0 && Current <= Total)
                {
                    for (int i = 0; i < f.Length; i++, offset++)//+=2, offset++)
                    //while(offset < data_left_channel.Length)
                    {
                        if(offset >= data_left_channel.Length)
                        {
                            offset = 0;
                            bmp_ = new BPMDetector(data_left_channel);//, data_right_channel);
                            //BPM_List[read++] = bmp_.getBPM();
                            BPM_List.Add(bmp_.getBPM());
#if DEBUG
                            ww.WriteData(data_left_channel, 0, data_left_channel.Length);
#endif
                            Array.Clear(data_left_channel, 0, data_left_channel.Length);
                            ++FLUSH;
                            break;
                        }
                        TOTALREAD++;
                        data_left_channel[offset] = f[i];
                        //data_right_channel[offset] = f[i + 1];
                        //if (f[0] == 0&&f[1]==0) { }
                    }
                    //TOTALREAD += READ;
                    //READ = rd.Read(out f);
                    //offset = 0;
                    READ = rd.Read_Mono(out f);
                    Current += READ;
                }
#if DEBUG
                ww.Dispose();
                ww.Close();
                HS_CSharpUtility.Utility.IOUtility.OpenExplorer(path);
                //System.Windows.Forms.MessageBox.Show(FLUSH.ToString());
#endif

                /*
                // build waveform data
                uint wscale = 1;
                string h = null;
                snd.getLength(ref len1, TIMEUNIT.PCMBYTES);
                snd.getLength(ref len2, TIMEUNIT.MS);
                len1 = (uint)((ulong)len1 * 1000 / len2);
                len2 *= wscale;
                uint samplerate = len1 / wscale;
                short peak, weak;
                int pos = 0, i;
                //short[] buffer = new short[samplerate * 2];
                BPM_List = new double[len2];
                uint samplerateb = samplerate * 4;
                IntPtr buff = System.Runtime.InteropServices.Marshal.AllocHGlobal((int)samplerateb);
                do
                {
                    snd.readData(buff, samplerateb, ref len1);
                    //System.Runtime.InteropServices.Marshal.Copy(buff, buffer, 0, (int)samplerateb / 2);
                    peak = weak = 0;
                    unsafe
                    {
                        short* gg = (short*)buff.ToPointer();
                        for (i = 0; i < len1 /4; i += 2)
                        {
                            data_left_channel[i] = gg[i];
                            data_right_channel[i] = gg[i + 1];
                            //if (buffer[i] > peak)peak = buffer[i];
                            //if (buffer[i] < weak)weak = buffer[i];
                        }
                    }
                    //data_left_channel[pos] = peak;
                    //data_right_channel[pos] = weak;
                    BPMDetector bmp_ = new BPMDetector(data_left_channel, data_right_channel);
                    //BPM_List[j] = bmp_.getBPM();
                    pos++;
                    if (pos < (len2 / 1000 + 5) && pos % 500 == 0)
                        h = string.Format("Opening File {0}%", (pos * 100 / (len2 / 1000 + 5)));
                } while (len1 == samplerateb);*/

                /*
                unsafe
                {
                    Total = len;
                    for (uint j = 0; j < SIZE; j++)
                    {
                        fixed (short* p = data_left_channel)
                        {
                            IntPtr buff = System.Runtime.InteropServices.Marshal.AllocHGlobal((int)data_left_channel.Length*2);
                            snd.readData(buff, (uint)(SIZE == 10 ? Ext : data_left_channel.Length), ref read);
                            for (uint i = 0; i < (SIZE == 10 ? Ext : len / 10); i++)
                            {
                                uint off = (j * i) + i; Current = off;
                                data_left_channel[i] = (short)(((int*)ptr1.ToPointer())[off] >> 16);
                                data_right_channel[i] = (short)((((int*)ptr1.ToPointer())[off] << 16) >> 16);
                            }
                        }
                        BPMDetector bmp_ = new BPMDetector(data_left_channel, data_right_channel);
                        BPM_List[j] = bmp_.getBPM();
                    }
                }
                snd.unlock(ptr1, ptr2, len1, len2);*/

                double SUM = 0;
                for (int i = 0; i < BPM_List.Count; i++) SUM += BPM_List[i];
                BPM = BPM_List[0];//SUM / SIZE;
            }
            catch (Exception ex) { HS_Library.HSConsole.WriteLine(ex.ToString()); }
            finally { IsComplete = true; Close(); try { th1.Abort(); } catch { } if (Complete != null)Complete(this, BPM_List.ToArray(), BPM); BPM_List.Clear();}
            return BPM;
        }

        Thread th, th1;
        public void Start_Async()
        {
            if(th!=null)th.Abort();
            if(th1!=null) th1.Abort();

            th1 = new Thread(start1);
            th = new Thread(start);
            th1.Start(); th.Start();
        }
        public void Stop()
        {
            if (th != null) th.Abort();
        }
        public void Dispose()
        {
            IsComplete = true;
            Close();
            FileName = null;
            Helper = null;
            Complete = null;
            Progress = null;
            GC.Collect();
        }
        protected internal void Close()
        {
            try
            {
                Total = Current = 0;
                bmp_ = null;
                if (rd != null) rd.Dispose(); rd = null;
                if (snd != null) snd.release(); snd = null;
                //if (sys != null) sys.close(); sys = null;
            }
            catch { }
            finally { GC.Collect(); }
        }
        protected internal void Init()
        {
            if ((FileName != null&&Helper!=null)||
                (FileName != null&&Helper==null))
            {
                /*
                result = Factory.System_Create(ref sys);
                try { result = sys.init(32, INITFLAGS.NORMAL, (IntPtr)null); result = sys.setOutput(OUTPUTTYPE.NOSOUND); }
                catch (Exception ex)
                {
                    HS_Library.HSConsole.WriteLine("Cannot initializing system!!\n└ " + ex.Message + "\n└ HS_Audio::LIBRARY::HSOther::MusicStreamer1::ctor(char FileName*, HSAudioHelper Helper*)\n");
                    sys.close(); sys = null;
                    sys = Helper.system;
                }*/
                sys = Helper.system;
            }

            try
            {
                /*
                HSAudioHelper.PlayingStatus stat = Helper.PlayStatus;

                if (stat != HSAudioHelper.PlayingStatus.Error && stat != HSAudioHelper.PlayingStatus.Play) Helper.Play();

                if (stat == HSAudioHelper.PlayingStatus.Pause || stat == HSAudioHelper.PlayingStatus.Ready) Helper.Pause();
                else if (stat == HSAudioHelper.PlayingStatus.Stop) Helper.Stop();*/

                result = Helper.system.createStream(FileName, MODE.UNICODE, ref snd);
                if (result != RESULT.OK)
                {
                    result = Helper.system.createSound(FileName, MODE.UNICODE, ref snd);
                    if (result != RESULT.OK) { 
                    //FileStream gs = new FileStream(FileName, FileMode.Append);
                    //gs.Close();
                    /*
                    HS_Library.HSConsole.WriteLine("Cannot create the stream!! Trying to create sound\n└ " + HS_Audio_Lib.Error.String(result) + "\n└ HS_Audio::LIBRARY::HSOther::MusicStreamer1::ctor(char FileName*, HSAudioHelper Helper*)\n");
                    result = Helper.system.createSound(FileName, MODE.SOFTWARE | MODE.UNICODE, ref snd);
                    if (result != RESULT.OK)*/
                    throw new SoundSystemException(result); }
                }
            }
            catch (Exception ex) { HS_Library.HSConsole.WriteLine("Cannot create the sound!!\n└ " + ex.Message + "\n└ HS_Audio::LIBRARY::HSOther::MusicStreamer1::ctor(char FileName*, HSAudioHelper Helper*)\n"); throw ex; }
            result = snd.setLoopCount(-1);
        }

        void ASNYC()
        {
            Start();
        }
        void PROGRESS()
        {
            while (true)
            {
                if (Progress != null) Progress.Invoke(this, Total, Current);
                Thread.Sleep(10);
            }
        }
    }
    class ReadData
    {
        HS_Audio_Lib.Sound sound;
        HS_Audio_Lib.RESULT result;

        uint len1, len2;
        uint samplerateb;
        uint samplerate;
        uint _chkBuffer;

        public ReadData(HS_Audio_Lib.Sound sound)
        {
            this.sound = sound;

            uint wscale = 1;
            //string h = null;
            sound.getLength(ref len1, TIMEUNIT.PCMBYTES);
            sound.getLength(ref len2, TIMEUNIT.MS);
            len1 = (uint)((ulong)len1 * 1000 / len2);
            len2 *= wscale;
            samplerate = len1 / wscale;

            //short peak, weak;
            //int pos = 0, i;
            Buf = new short[samplerate * 2];
            samplerateb = samplerate * 4;

            //this.Buffer = _chkBuffer = (uint)Buffer;
            buff = System.Runtime.InteropServices.Marshal.AllocHGlobal((int)samplerateb);
            //Buf = new short[Buffer];
            //buf = new byte[(int)samplerateb];
        }

        public long Length
        {
            get
            {
                uint l = 0;
                result = sound.getLength(ref l, TIMEUNIT.PCMBYTES);
                return l;
            }
        }
        [Obsolete("CrossThreadException!!")]
        public uint Buffer{ get {return (uint)Buf.Length;} }

        public uint Position{get; private set;}

        /*
        public override int Read(byte[] buffer, int offset, int count)
        {
            throw new NotImplementedException();
        }*/
        
        short[] Buf;
        IntPtr buff;

        public uint Read(out short[] array)
        {
            sound.readData(buff, samplerateb, ref len1);
            System.Runtime.InteropServices.Marshal.Copy(buff, Buf, 0, (int)samplerateb / 2);
            array = Buf;
            Position += len1;
            return len1;
        }

        short[] Buf_Mono;
        public uint Read_Mono(out short[] array)
        {
            /*
            sound.readData(buff, samplerateb, ref len1);
            short[] yy = new short[len1 / 4];
            unsafe
            {
                byte* aa = (byte*)buff.ToPointer();
                for (uint i = 0; i < len1; i += 4)
                {
                    //yy[i] = (short)(aa[i] << 8 | aa[i + 1] << 16);
                    //yy[i] = (short)((aa[i+1] << 8) + aa[i]);
                    //yy[i] = (short)((aa[i + 1] << 8) | (aa[i] << 0));
                    int L = ((aa[i + 1] << 8) | aa[i]);
                    int R = (aa[i + 3] << 8) | aa[i + 2];
                    yy[i/4] = (short)((L+R)/ 2);
                }
            }*/
            if(Buf_Mono==null) Buf_Mono = new short[Buf.Length / 2];
            Read(out Buf);
            for(int i = 0; i < Buf.Length; i+=2) Buf_Mono[i/2] = (short)((Buf[i]+Buf[i+1])/2);
            array = Buf_Mono;
            //Position += len1;
            return len1;
        }
        /*
        //(int)samplerateb);
        public unsafe uint Read(out short[] array)
        {
            try
            {
                fixed (short* aa = Buf)
                {
                    //samplerateb
                    //sound.readData(new IntPtr(aa), _chkBuffer, ref _chkBuffer);//len1);
                    
                }
                sound.readData(buff, Buffer*4, ref _chkBuffer);//len1);
                if (Buffer == _chkBuffer)
                {
                    //if (Buf.Length != Buffer) Buf = new short[(int)Buffer];
                    System.Runtime.InteropServices.Marshal.Copy(buff, Buf, 0, (int)(Buffer*2));//(int)samplerateb / 2);
                    Position += Buffer;//len1;
                                           //
                                           unsafe
                                           {
                                               short* gg = (short*)buff.ToPointer();
                                               for (int i = 0; i < len1 / 4; i += 2)
                                               {
                                                   //data_left_channel[i] = gg[i];
                                                   //data_right_channel[i] = gg[i + 1];
                                               }
                                           }//
                    array = Buf;
                    return _chkBuffer;
                }
                else array = null;
            }
            finally {}
            return 0;

        }*/

        public bool Seek(uint offset)//, SeekOrigin origin)
        {
            Position = offset;
            return sound.seekData(offset)==RESULT.OK;
        }

        public void Dispose()
        {
            System.Runtime.InteropServices.Marshal.FreeHGlobal(buff);
            /*
            unsafe
            {
                fixed (short* aa = Buf)
                {
                    System.Runtime.InteropServices.Marshal.FreeHGlobal(new IntPtr(aa));
                }
            }*/
            Buf = null;
            sound = null;
        }
            
    }
}
