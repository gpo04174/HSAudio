using System.Windows.Forms;

namespace SynchronizedVolumeControl
{
    public class SubclassHWND : NativeWindow
    {
        protected override void WndProc( ref Message m )
        {
            base.WndProc(ref m);
        }
    }
}
