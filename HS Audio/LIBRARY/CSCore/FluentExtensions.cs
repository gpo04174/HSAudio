﻿using CSCore.Streams.SampleConverter;
using System;

namespace CSCore
{
    public static class FluentExtensions
    {
        /// <summary>
        ///     Converts a <see cref="IWaveSource"/> to a <see cref="ISampleSource"/>.
        /// </summary>
        /// <param name="waveSource">The <see cref="IWaveSource"/> to convert to a <see cref="ISampleSource"/>.</param>
        /// <returns>The <see cref="ISampleSource"/> wrapped around the specified <paramref name="waveSource"/>.</returns>       
        public static ISampleSource ToSampleSource(this IWaveSource waveSource)
        {
            if (waveSource == null)
                throw new ArgumentNullException("waveSource");

            return WaveToSampleBase.CreateConverter(waveSource);
        }

        /// <summary>
        ///     Converts a SampleSource to either a Pcm (8, 16, or 24 bit) or IeeeFloat (32 bit) WaveSource.
        /// </summary>
        /// <param name="sampleSource">Sample source to convert to a wave source.</param>
        /// <param name="bits">Bits per sample.</param>
        /// <returns>Wave source</returns>
        public static IWaveSource ToWaveSource(this ISampleSource sampleSource, int bits)
        {
            if (sampleSource == null)
                throw new ArgumentNullException("sampleSource");

            switch (bits)
            {
                case 8:
                    return new SampleToPcm8(sampleSource);
                case 16:
                    return new SampleToPcm16(sampleSource);
                case 24:
                    return new SampleToPcm24(sampleSource);
                case 32:
                    return new SampleToIeeeFloat32(sampleSource);
                default:
                    throw new ArgumentOutOfRangeException("bits", "Must be 8, 16, 24 or 32 bits.");
            }
        }

        /// <summary>
        ///     Converts a <see cref="IWaveSource"/> to IeeeFloat (32bit) <see cref="IWaveSource"/>.
        /// </summary>
        /// <param name="sampleSource">The <see cref="ISampleSource"/> to convert to a <see cref="IWaveSource"/>.</param>
        /// <returns>The <see cref="IWaveSource"/> wrapped around the specified <paramref name="sampleSource"/>.</returns>
        public static IWaveSource ToWaveSource(this ISampleSource sampleSource)
        {
            if (sampleSource == null)
                throw new ArgumentNullException("sampleSource");

            return new SampleToIeeeFloat32(sampleSource);
        }
    }
}
