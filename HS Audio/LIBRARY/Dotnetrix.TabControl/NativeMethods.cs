﻿using System;
using System.Drawing;
using System.Runtime.InteropServices;

namespace Dotnetrix.Controls
{
    internal sealed class NativeMethods
    {
        public const int CCM_FIRST = 0x2000;
        public const int LAYOUT_BITMAPORIENTATIONPRESERVED = 8;
        public const int LAYOUT_RTL = 1;
        public const int NM_FIRST = 0;
        public const int OPAQUE = 2;
        public const int SRCCOPY = 0xcc0020;
        public const int TCM_FIRST = 0x1300;
        public const int TCN_FIRST = -550;
        public const int TRANSPARENT = 1;

        private NativeMethods()
        {
        }

        [return: MarshalAs(UnmanagedType.Bool)]
        [DllImport("gdi32.dll")]
        public static extern bool BitBlt(IntPtr hdcDest, int nXDest, int nYDest, int nWidth, int nHeight, IntPtr hdcSrc, int nXSrc, int nYSrc, int dwRop);
        [DllImport("gdi32.dll")]
        public static extern IntPtr CreateCompatibleDC(IntPtr hdc);
        [return: MarshalAs(UnmanagedType.Bool)]
        [DllImport("gdi32.dll")]
        public static extern bool DeleteDC(IntPtr hdc);
        [return: MarshalAs(UnmanagedType.Bool)]
        [DllImport("gdi32.dll")]
        public static extern bool DeleteObject(IntPtr hObject);
        [return: MarshalAs(UnmanagedType.I4)]
        [DllImport("user32.dll", CharSet = CharSet.Auto)]
        public static extern int DrawState(IntPtr hdc, IntPtr hbr, IntPtr lpOutputFunc, IntPtr lData, IntPtr wData, int x, int y, int cx, int cy, int fuFlags);
        [return: MarshalAs(UnmanagedType.I4)]
        [DllImport("user32.dll", CharSet = CharSet.Auto)]
        public static extern int DrawText(IntPtr hDC, string lpString, int nCount, ref RECT lpRect, DRAWTEXTFLAGS uFormat);
        [DllImport("uxtheme")]
        public static extern IntPtr GetWindowTheme(IntPtr hWnd);
        [DllImport("gdi32.dll")]
        public static extern IntPtr SelectObject(IntPtr hdc, IntPtr hgdiobj);
        [DllImport("user32.dll")]
        public static extern IntPtr SendMessage(IntPtr hWnd, int Msg, IntPtr wParam, IntPtr lParam);
        [DllImport("user32.dll")]
        public static extern IntPtr SendMessage(IntPtr hwnd, int msg, IntPtr wParam, ref TCHITTESTINFO lParam);
        [DllImport("gdi32.dll")]
        public static extern int SetBkMode(IntPtr hDC, int nBkMode);
        [DllImport("gdi32.dll")]
        public static extern int SetLayout(IntPtr hdc, int dwLayout);
        [DllImport("gdi32.dll")]
        public static extern int SetTextColor(IntPtr hdc, int crColor);
        [DllImport("uxtheme")]
        public static extern int SetWindowTheme(IntPtr hwnd, [MarshalAs(UnmanagedType.LPWStr)] string pszSubAppName, [MarshalAs(UnmanagedType.LPWStr)] string pszSubIdList);

        public enum ClassPartsTab
        {
            BODY = 10,
            PANE = 9,
            TABITEM = 1,
            TABITEMBOTHEDGE = 4,
            TABITEMLEFTEDGE = 2,
            TABITEMRIGHTEDGE = 3,
            TOPTABITEM = 5,
            TOPTABITEMBOTHEDGE = 8,
            TOPTABITEMLEFTEDGE = 6,
            TOPTABITEMRIGHTEDGE = 7
        }

        [Flags]
        public enum DrawStateFlags
        {
            BITMAP = 4,
            COMPLEX = 0,
            DISABLED = 0x20,
            HIDEPREFIX = 0x200,
            ICON = 3,
            PREFIXTEXT = 2,
            RIGHT = 0x8000,
            RTLREADING = 0x20000,
            TEXT = 1
        }

        [Flags]
        public enum DRAWTEXTFLAGS
        {
            BOTTOM = 8,
            CALCRECT = 0x400,
            CENTER = 1,
            EXPANDTABS = 0x40,
            EXTERNALLEADING = 0x200,
            HIDEPREFIX = 0x100000,
            INTERNAL = 0x1000,
            LEFT = 0,
            NOCLIP = 0x100,
            NOPREFIX = 0x800,
            RIGHT = 2,
            SINGLELINE = 0x20,
            TABSTOP = 0x80,
            TOP = 0,
            VCENTER = 4,
            WORDBREAK = 0x10
        }

        [StructLayout(LayoutKind.Sequential)]
        public struct NMHDR
        {
            public IntPtr hWnd;
            public int idFrom;
            public int code;
        }

        [StructLayout(LayoutKind.Sequential)]
        public struct NMOBJECTNOTIFY
        {
            public NativeMethods.NMHDR hdr;
            public int iItem;
            public IntPtr piid;
            public IntPtr pObject;
            public IntPtr hResult;
        }

        [StructLayout(LayoutKind.Sequential)]
        public struct RECT
        {
            public int Left;
            public int Top;
            public int Right;
            public int Bottom;
            public int Height
            {
                get
                {
                    return (this.Bottom - this.Top);
                }
            }
            public int Width
            {
                get
                {
                    return (this.Right - this.Left);
                }
            }
            public System.Drawing.Size Size
            {
                get
                {
                    return new System.Drawing.Size(this.Width, this.Height);
                }
            }
        }

        public enum TC_NOTIFYMESSAGECODE
        {
            CLICK = -2,
            FOCUSCHANGE = -554,
            GETOBJECT = -553,
            KEYDOWN = -550,
            RCLICK = -5,
            RELEASEDCAPTURE = -16,
            SELCHANGE = -551,
            SELCHANGING = -552
        }

        [Flags]
        public enum TCHITTESTFLAGS
        {
            NOWHERE = 1,
            ONITEM = 6,
            ONITEMICON = 2,
            ONITEMLABEL = 4
        }

        [StructLayout(LayoutKind.Sequential)]
        public struct TCHITTESTINFO
        {
            public Point pt;
            public NativeMethods.TCHITTESTFLAGS flags;
            public TCHITTESTINFO(int x, int y)
            {
                this.pt = new Point(x, y);
                this.flags = NativeMethods.TCHITTESTFLAGS.ONITEM;
            }
        }

        [StructLayout(LayoutKind.Sequential)]
        public struct TCKEYDOWN
        {
            public NativeMethods.NMHDR hdr;
            public short wVKey;
            public int flags;
        }

        public enum TCMessage
        {
            ADJUSTRECT = 0x1328,
            DELETEALLITEMS = 0x1309,
            DELETEITEM = 0x1308,
            DESELECTALL = 0x1332,
            GETCURFOCUS = 0x132f,
            GETCURSEL = 0x130b,
            GETEXTENDEDSTYLE = 0x1335,
            GETIMAGELIST = 0x1302,
            GETITEMA = 0x1305,
            GETITEMCOUNT = 0x1304,
            GETITEMRECT = 0x130a,
            GETITEMW = 0x133c,
            GETROWCOUNT = 0x132c,
            GETTOOLTIPS = 0x132d,
            HIGHLIGHTITEM = 0x1333,
            HITTEST = 0x130d,
            INSERTITEMA = 0x1307,
            INSERTITEMW = 0x133e,
            REMOVEIMAGE = 0x132a,
            SETCURFOCUS = 0x1330,
            SETCURSEL = 0x130c,
            SETEXTENDEDSTYLE = 0x1334,
            SETIMAGELIST = 0x1303,
            SETITEMA = 0x1306,
            SETITEMEXTRA = 0x130e,
            SETITEMSIZE = 0x1329,
            SETITEMW = 0x133d,
            SETMINTABWIDTH = 0x1331,
            SETPADDING = 0x132b,
            SETTOOLTIPS = 0x132e
        }

        public enum WinMessage
        {
            ACTIVATE = 6,
            ACTIVATEAPP = 0x1c,
            AFXFIRST = 0x360,
            AFXLAST = 0x37f,
            APP = 0x8000,
            APPCOMMAND = 0x319,
            ASKCBFORMATNAME = 780,
            CANCELJOURNAL = 0x4b,
            CANCELMODE = 0x1f,
            CAPTURECHANGED = 0x215,
            CHANGECBCHAIN = 0x30d,
            CHANGEUISTATE = 0x127,
            CHAR = 0x102,
            CHARTOITEM = 0x2f,
            CHILDACTIVATE = 0x22,
            CLEAR = 0x303,
            CLOSE = 0x10,
            COMMAND = 0x111,
            COMMNOTIFY = 0x44,
            COMPACTING = 0x41,
            COMPAREITEM = 0x39,
            CONTEXTMENU = 0x7b,
            COPY = 0x301,
            COPYDATA = 0x4a,
            CREATE = 1,
            CTLCOLORBTN = 0x135,
            CTLCOLORDLG = 310,
            CTLCOLOREDIT = 0x133,
            CTLCOLORLISTBOX = 0x134,
            CTLCOLORMSGBOX = 0x132,
            CTLCOLORSCROLLBAR = 0x137,
            CTLCOLORSTATIC = 0x138,
            CUT = 0x300,
            DEADCHAR = 0x103,
            DELETEITEM = 0x2d,
            DESTROY = 2,
            DESTROYCLIPBOARD = 0x307,
            DEVICECHANGE = 0x219,
            DEVMODECHANGE = 0x1b,
            DISPLAYCHANGE = 0x7e,
            DRAWCLIPBOARD = 0x308,
            DRAWITEM = 0x2b,
            DROPFILES = 0x233,
            ENABLE = 10,
            ENDSESSION = 0x16,
            ENTERIDLE = 0x121,
            ENTERMENULOOP = 0x211,
            ENTERSIZEMOVE = 0x231,
            ERASEBKGND = 20,
            EXITMENULOOP = 530,
            EXITSIZEMOVE = 0x232,
            FONTCHANGE = 0x1d,
            GETDLGCODE = 0x87,
            GETFONT = 0x31,
            GETHOTKEY = 0x33,
            GETICON = 0x7f,
            GETMINMAXINFO = 0x24,
            GETOBJECT = 0x3d,
            GETTEXT = 13,
            GETTEXTLENGTH = 14,
            HANDHELDFIRST = 0x358,
            HANDHELDLAST = 0x35f,
            HELP = 0x53,
            HOTKEY = 0x312,
            HSCROLL = 0x114,
            HSCROLLCLIPBOARD = 0x30e,
            ICONERASEBKGND = 0x27,
            IME_CHAR = 0x286,
            IME_COMPOSITION = 0x10f,
            IME_COMPOSITIONFULL = 0x284,
            IME_CONTROL = 0x283,
            IME_ENDCOMPOSITION = 270,
            IME_KEYDOWN = 0x290,
            IME_KEYLAST = 0x10f,
            IME_KEYUP = 0x291,
            IME_NOTIFY = 0x282,
            IME_REQUEST = 0x288,
            IME_SELECT = 0x285,
            IME_SETCONTEXT = 0x281,
            IME_STARTCOMPOSITION = 0x10d,
            INITDIALOG = 0x110,
            INITMENU = 0x116,
            INITMENUPOPUP = 0x117,
            INPUT = 0xff,
            INPUTLANGCHANGE = 0x51,
            INPUTLANGCHANGEREQUEST = 80,
            KEYDOWN = 0x100,
            KEYFIRST = 0x100,
            KEYLAST = 0x108,
            KEYLAST_XP = 0x109,
            KEYUP = 0x101,
            KILLFOCUS = 8,
            LBUTTONDBLCLK = 0x203,
            LBUTTONDOWN = 0x201,
            LBUTTONUP = 0x202,
            MBUTTONDBLCLK = 0x209,
            MBUTTONDOWN = 0x207,
            MBUTTONUP = 520,
            MDIACTIVATE = 0x222,
            MDICASCADE = 0x227,
            MDICREATE = 0x220,
            MDIDESTROY = 0x221,
            MDIGETACTIVE = 0x229,
            MDIICONARRANGE = 0x228,
            MDIMAXIMIZE = 0x225,
            MDINEXT = 0x224,
            MDIREFRESHMENU = 0x234,
            MDIRESTORE = 0x223,
            MDISETMENU = 560,
            MDITILE = 550,
            MEASUREITEM = 0x2c,
            MENUCHAR = 0x120,
            MENUCOMMAND = 0x126,
            MENUDRAG = 0x123,
            MENUGETOBJECT = 0x124,
            MENURBUTTONUP = 290,
            MENUSELECT = 0x11f,
            MOUSEACTIVATE = 0x21,
            MOUSEFIRST = 0x200,
            MOUSEHOVER = 0x2a1,
            MOUSELAST = 0x209,
            MOUSELAST_2K = 0x20d,
            MOUSELAST_NT = 0x20a,
            MOUSELEAVE = 0x2a3,
            MOUSEMOVE = 0x200,
            MOUSEWHEEL = 0x20a,
            MOVE = 3,
            MOVING = 0x216,
            NCACTIVATE = 0x86,
            NCCALCSIZE = 0x83,
            NCCREATE = 0x81,
            NCDESTROY = 130,
            NCHITTEST = 0x84,
            NCLBUTTONDBLCLK = 0xa3,
            NCLBUTTONDOWN = 0xa1,
            NCLBUTTONUP = 0xa2,
            NCMBUTTONDBLCLK = 0xa9,
            NCMBUTTONDOWN = 0xa7,
            NCMBUTTONUP = 0xa8,
            NCMOUSEHOVER = 0x2a0,
            NCMOUSELEAVE = 0x2a2,
            NCMOUSEMOVE = 160,
            NCPAINT = 0x85,
            NCRBUTTONDBLCLK = 0xa6,
            NCRBUTTONDOWN = 0xa4,
            NCRBUTTONUP = 0xa5,
            NCXBUTTONDBLCLK = 0xad,
            NCXBUTTONDOWN = 0xab,
            NCXBUTTONUP = 0xac,
            NEXTDLGCTL = 40,
            NEXTMENU = 0x213,
            NOTIFY = 0x4e,
            NOTIFYFORMAT = 0x55,
            NULL = 0,
            PAINT = 15,
            PAINTCLIPBOARD = 0x309,
            PAINTICON = 0x26,
            PALETTECHANGED = 0x311,
            PALETTEISCHANGING = 0x310,
            PARENTNOTIFY = 0x210,
            PASTE = 770,
            PENWINFIRST = 0x380,
            PENWINLAST = 0x38f,
            POWERBROADCAST = 0x218,
            PRINT = 0x317,
            PRINTCLIENT = 0x318,
            QUERYDRAGICON = 0x37,
            QUERYENDSESSION = 0x11,
            QUERYNEWPALETTE = 0x30f,
            QUERYOPEN = 0x13,
            QUERYUISTATE = 0x129,
            QUEUESYNC = 0x23,
            QUIT = 0x12,
            RBUTTONDBLCLK = 0x206,
            RBUTTONDOWN = 0x204,
            RBUTTONUP = 0x205,
            REFLECT = 0x2000,
            RENDERALLFORMATS = 0x306,
            RENDERFORMAT = 0x305,
            SETCURSOR = 0x20,
            SETFOCUS = 7,
            SETFONT = 0x30,
            SETHOTKEY = 50,
            SETICON = 0x80,
            SETREDRAW = 11,
            SETTEXT = 12,
            SETTINGCHANGE = 0x1a,
            SHOWWINDOW = 0x18,
            SIZE = 5,
            SIZECLIPBOARD = 0x30b,
            SIZING = 0x214,
            SPOOLERSTATUS = 0x2a,
            STYLECHANGED = 0x7d,
            STYLECHANGING = 0x7c,
            SYNCPAINT = 0x88,
            SYSCHAR = 0x106,
            SYSCOLORCHANGE = 0x15,
            SYSCOMMAND = 0x112,
            SYSDEADCHAR = 0x107,
            SYSKEYDOWN = 260,
            SYSKEYUP = 0x105,
            TABLET_FIRST = 0x2c0,
            TABLET_LAST = 0x2df,
            TASKBUTTONMENU = 0x313,
            TCARD = 0x52,
            THEMECHANGED = 0x31a,
            TIMECHANGE = 30,
            TIMER = 0x113,
            UNDO = 0x304,
            UNICHAR = 0x109,
            UNINITMENUPOPUP = 0x125,
            UPDATEUISTATE = 0x128,
            USER = 0x400,
            USERCHANGED = 0x54,
            VKEYTOITEM = 0x2e,
            VSCROLL = 0x115,
            VSCROLLCLIPBOARD = 0x30a,
            WINDOWPOSCHANGED = 0x47,
            WINDOWPOSCHANGING = 70,
            WININICHANGE = 0x1a,
            WTSSESSION_CHANGE = 0x2b1,
            XBUTTONDBLCLK = 0x20d,
            XBUTTONDOWN = 0x20b,
            XBUTTONUP = 0x20c
        }
    }
}

