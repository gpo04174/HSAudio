﻿namespace Dotnetrix.Controls
{
    using System;
    using System.ComponentModel;
    using System.Drawing;
    using System.Drawing.Drawing2D;
    using System.Drawing.Imaging;
    using System.Media;
    using System.Reflection;
    using System.Runtime.InteropServices;
    using System.Security.Permissions;
    using System.Windows.Forms;
    using System.Windows.Forms.VisualStyles;

    [ToolboxItemFilter("Containers"), ToolboxBitmap(typeof(System.Windows.Forms.TabControl))]
    public class TabControl : System.Windows.Forms.TabControl
    {
        private Bitmap BufferImage;
        private bool firstShown;
        private Color myBackColor = Color.Empty;
        private bool myDoubleBufferTabpages;
        private TabDrawMode myDrawMode;
        private Color myHotColor = SystemColors.HotTrack;
        private int myHotTabID = -1;
        private Color mySelectedTabColor = SystemColors.Control;
        private Color myTabColor = SystemColors.Control;
        private bool myUseBackColorBehindTabs;
        private bool rotateImageWithTab;

        public TabControl()
        {
            base.SetStyle(ControlStyles.AllPaintingInWmPaint | ControlStyles.SupportsTransparentBackColor | ControlStyles.ResizeRedraw | ControlStyles.UserPaint, true);
            base.SetStyle(ControlStyles.Opaque, false);
            this.DoubleBuffered = false;
        }

        private Bitmap CreateTabBaseBitmap(int id)
        {
            Rectangle tabRect = base.GetTabRect(id);
            if ((this.Appearance == TabAppearance.Normal) && (id == base.SelectedIndex))
            {
                tabRect.Inflate(2, 2);
            }
            Bitmap image = new Bitmap(tabRect.Width, tabRect.Height, PixelFormat.Format32bppPArgb);
            using (Graphics graphics = Graphics.FromImage(image))
            {
                GraphicsContainer container = graphics.BeginContainer();
                if (this.TabIsPartiallyTransparent(id) && ((this.BackColor.A < 0xff) || !this.UseBackColorBehindTabs))
                {
                    if (this.BufferImage == null)
                    {
                        this.DrawBufferImage();
                    }
                    if (this.BufferImage != null)
                    {
                        graphics.DrawImage(this.BufferImage, 0, 0, tabRect, GraphicsUnit.Pixel);
                    }
                }
                else
                {
                    graphics.Clear(this.BackColor);
                }
                graphics.EndContainer(container);
                graphics.Flush(FlushIntention.Sync);
            }
            switch (base.Alignment)
            {
                case TabAlignment.Bottom:
                    image.RotateFlip(RotateFlipType.Rotate180FlipX);
                    break;

                case TabAlignment.Left:
                    image.RotateFlip(RotateFlipType.Rotate90FlipX);
                    break;

                case TabAlignment.Right:
                    image.RotateFlip(RotateFlipType.Rotate270FlipNone);
                    break;
            }
            image = new Bitmap(image);
            using (Graphics graphics2 = Graphics.FromImage(image))
            {
                GraphicsContainer container2 = graphics2.BeginContainer();
                switch (this.Appearance)
                {
                    case TabAppearance.Buttons:
                        this.PaintButtonTab(graphics2, new Rectangle(Point.Empty, image.Size), id);
                        break;

                    case TabAppearance.FlatButtons:
                        this.PaintFlatTab(graphics2, new Rectangle(Point.Empty, image.Size), id);
                        break;

                    default:
                        this.Paint3DTab(graphics2, new Rectangle(Point.Empty, image.Size), id);
                        break;
                }
                graphics2.EndContainer(container2);
                graphics2.Flush(FlushIntention.Sync);
            }
            if (base.Alignment == TabAlignment.Bottom)
            {
                image.RotateFlip(RotateFlipType.Rotate180FlipX);
            }
            else if (base.Alignment == TabAlignment.Left)
            {
                image.RotateFlip(RotateFlipType.RotateNoneFlipX);
            }
            if ((this.RightToLeftLayout && (this.RightToLeft == RightToLeft.Yes)) && (this.DrawMode == TabDrawMode.Normal))
            {
                image.RotateFlip(RotateFlipType.RotateNoneFlipX);
            }
            return image;
        }

        private static Color DarkenColor(Color colorIn, int percent)
        {
            if ((percent < 0) || (percent > 100))
            {
                throw new ArgumentOutOfRangeException("percent");
            }
            int alpha = 0xff;
            int red = colorIn.R - ((colorIn.R / 100) * percent);
            int green = colorIn.G - ((colorIn.G / 100) * percent);
            int blue = colorIn.B - ((colorIn.B / 100) * percent);
            return Color.FromArgb(alpha, red, green, blue);
        }

        private void DrawBorder(Graphics graphics)
        {
            if (this.Appearance == TabAppearance.Normal)
            {
                Rectangle displayRectangle = this.DisplayRectangle;
                displayRectangle.Inflate(4, 4);
                switch (base.Alignment)
                {
                    case TabAlignment.Top:
                        displayRectangle.Y++;
                        displayRectangle.Height--;
                        break;

                    case TabAlignment.Bottom:
                        displayRectangle.Height++;
                        break;

                    case TabAlignment.Left:
                        displayRectangle.X++;
                        displayRectangle.Width--;
                        break;

                    case TabAlignment.Right:
                        displayRectangle.Width++;
                        break;
                }
                if (this.VisualStylesEnabled)
                {
                    new VisualStyleRenderer(VisualStyleElement.Tab.Pane.Normal).DrawBackground(graphics, displayRectangle);
                }
                else
                {
                    Rectangle empty = Rectangle.Empty;
                    if (base.SelectedIndex != -1)
                    {
                        empty = base.GetTabRect(base.SelectedIndex);
                        empty.Inflate(2, 2);
                    }
                    graphics.SetClip(empty, CombineMode.Exclude);
                    Rectangle rect = this.DisplayRectangle;
                    rect.Inflate(4, 4);
                    if (base.Alignment <= TabAlignment.Bottom)
                    {
                        rect.Height--;
                        if (base.Alignment == TabAlignment.Top)
                        {
                            rect.Y++;
                        }
                    }
                    else
                    {
                        rect.Width--;
                        if (base.Alignment == TabAlignment.Left)
                        {
                            rect.X++;
                        }
                    }
                    Color color = (base.SelectedIndex == -1) ? this.BackColor : (base.SelectedTab.UseVisualStyleBackColor ? Control.DefaultBackColor : base.SelectedTab.BackColor);
                    using (SolidBrush brush = new SolidBrush(color))
                    {
                        graphics.FillRectangle(brush, rect);
                    }
                    Color color2 = LightenColor(color, 40);
                    Color color3 = LightenColor(color, 80);
                    Color color4 = DarkenColor(color, 0x19);
                    Color color5 = DarkenColor(color, 40);
                    using (Pen pen = new Pen(color3))
                    {
                        Point[] points = new Point[] { new Point(rect.Left, rect.Bottom - 2), new Point(rect.Left, rect.Top), new Point(rect.Right - 2, rect.Top) };
                        graphics.DrawLines(pen, points);
                        pen.Color = color2;
                        Point[] pointArray2 = new Point[] { new Point(rect.Left + 1, rect.Bottom - 3), new Point(rect.Left + 1, rect.Top + 1), new Point(rect.Right - 3, rect.Top + 1) };
                        graphics.DrawLines(pen, pointArray2);
                        pen.Color = color5;
                        Point[] pointArray3 = new Point[] { new Point(rect.Right - 1, rect.Top), new Point(rect.Right - 1, rect.Bottom - 1), new Point(rect.Left, rect.Bottom - 1) };
                        graphics.DrawLines(pen, pointArray3);
                        pen.Color = color4;
                        Point[] pointArray4 = new Point[] { new Point(rect.Right - 2, rect.Top + 1), new Point(rect.Right - 2, rect.Bottom - 2), new Point(rect.Left + 1, rect.Bottom - 2) };
                        graphics.DrawLines(pen, pointArray4);
                    }
                    graphics.ResetClip();
                }
            }
            graphics.Flush(FlushIntention.Sync);
        }

        private void DrawBufferImage()
        {
            if (((base.Parent != null) && base.Created) && ((base.Width > 0) && (base.Height > 0)))
            {
                if ((this.BufferImage == null) || !this.BufferImage.Size.Equals(base.Size))
                {
                    this.BufferImage = new Bitmap(base.Width, base.Height, PixelFormat.Format32bppPArgb);
                }
                using (Graphics graphics = Graphics.FromImage(this.BufferImage))
                {
                    GraphicsContainer container = graphics.BeginContainer();
                    Rectangle bounds = base.Bounds;
                    graphics.TranslateTransform((float) -base.Left, (float) -base.Top);
                    PaintEventArgs e = new PaintEventArgs(graphics, bounds);
                    base.InvokePaintBackground(base.Parent, e);
                    base.InvokePaint(base.Parent, e);
                    graphics.ResetTransform();
                    graphics.EndContainer(container);
                    graphics.Flush(FlushIntention.Sync);
                }
                if ((this.RightToLeft == RightToLeft.Yes) && this.RightToLeftLayout)
                {
                    this.BufferImage.RotateFlip(RotateFlipType.RotateNoneFlipX);
                }
            }
        }

        private static void GdiDrawStateText(IntPtr hdc, string text, Font font, Color textColor, Rectangle bounds, int flags)
        {
            IntPtr hgdiobj = font.ToHfont();
            IntPtr ptr2 = NativeMethods.SelectObject(hdc, hgdiobj);
            int nBkMode = NativeMethods.SetBkMode(hdc, 1);
            int crColor = NativeMethods.SetTextColor(hdc, ColorTranslator.ToWin32(textColor));
            NativeMethods.DrawState(hdc, IntPtr.Zero, IntPtr.Zero, Marshal.StringToHGlobalAuto(text), new IntPtr(text.Length), bounds.Left, bounds.Top, bounds.Width, bounds.Height, flags);
            NativeMethods.SetTextColor(hdc, crColor);
            NativeMethods.SetBkMode(hdc, nBkMode);
            NativeMethods.SelectObject(hdc, ptr2);
            NativeMethods.DeleteObject(hgdiobj);
        }

        private TabPage GetFirstEnabledTab()
        {
            if (base.TabCount > 0)
            {
                TabPage page = null;
                for (int i = 0; i < base.TabCount; i++)
                {
                    page = base.TabPages[i];
                    if (page.Enabled)
                    {
                        return page;
                    }
                }
            }
            return null;
        }

        private TabPage GetLastEnabledTab()
        {
            if (base.TabCount > 0)
            {
                TabPage page = null;
                for (int i = base.TabCount - 1; i >= 0; i--)
                {
                    page = base.TabPages[i];
                    if (page.Enabled)
                    {
                        return page;
                    }
                }
            }
            return null;
        }

        private TabPage GetNextEnabledTab(bool forward, bool wrap)
        {
            if (forward)
            {
                for (int i = base.SelectedIndex + 1; i <= (base.TabCount - 1); i++)
                {
                    if (base.TabPages[i].Enabled)
                    {
                        return base.TabPages[i];
                    }
                }
                if (wrap)
                {
                    for (int j = 0; j <= base.SelectedIndex; j++)
                    {
                        if (base.TabPages[j].Enabled)
                        {
                            return base.TabPages[j];
                        }
                    }
                }
            }
            else
            {
                for (int k = base.SelectedIndex - 1; k >= 0; k--)
                {
                    if (base.TabPages[k].Enabled)
                    {
                        return base.TabPages[k];
                    }
                }
                if (wrap)
                {
                    for (int m = base.TabCount - 1; m > base.SelectedIndex; m--)
                    {
                        if (base.TabPages[m].Enabled)
                        {
                            return base.TabPages[m];
                        }
                    }
                }
            }
            return null;
        }

        private VisualStyleElement GetVisualStyleElement(int id)
        {
            bool flag = base.Alignment >= TabAlignment.Left;
            Rectangle tabRect = base.GetTabRect(id);
            bool flag2 = flag ? (tabRect.Top <= 3) : (tabRect.Left <= 3);
            bool flag3 = flag ? (tabRect.Bottom >= (base.Height - 4)) : (tabRect.Right >= (base.Width - 4));
            bool flag4 = flag ? ((base.Alignment == TabAlignment.Left) ? (tabRect.Left <= 3) : (tabRect.Right >= (base.Width - 3))) : (tabRect.Top <= 3);
            int part = 1;
            TabItemState normal = TabItemState.Normal;
            if (flag2)
            {
                part++;
            }
            if (flag3)
            {
                part += 2;
            }
            if (flag4)
            {
                part += 4;
            }
            if (id == base.SelectedIndex)
            {
                normal = TabItemState.Selected;
            }
            else
            {
                TabPage page = base.TabPages[id];
                if (page.Enabled)
                {
                    if (((base.TabPages[id] == this.TabUnderMouse) && base.HotTrack) && !base.DesignMode)
                    {
                        normal = TabItemState.Hot;
                    }
                }
                else
                {
                    normal = TabItemState.Disabled;
                }
            }
            return VisualStyleElement.CreateElement("TAB", part, (int) normal);
        }

        private bool HandleArrowKeys(Keys keys)
        {
            if (this.Appearance == TabAppearance.Normal)
            {
                Point point;
                Point point2;
                if (base.SelectedIndex == -1)
                {
                    base.SelectedTab = this.GetFirstEnabledTab();
                    return true;
                }
                Rectangle tabRect = base.GetTabRect(base.SelectedIndex);
                bool flag = base.Alignment >= TabAlignment.Left;
                TabPage nextEnabledTab = null;
                if ((this.RightToLeft == RightToLeft.Yes) && this.RightToLeftLayout)
                {
                    if (keys == Keys.Left)
                    {
                        keys = Keys.Right;
                    }
                    else if (keys == Keys.Right)
                    {
                        keys = Keys.Left;
                    }
                }
                switch (keys)
                {
                    case Keys.Left:
                        if (flag)
                        {
                            point = new Point(tabRect.Left - 3, tabRect.Top + (tabRect.Height / 2));
                            point2 = flag ? new Point(0, -3) : Point.Empty;
                            break;
                        }
                        nextEnabledTab = this.GetNextEnabledTab(false, false);
                        if (nextEnabledTab != null)
                        {
                            base.SelectedTab = nextEnabledTab;
                        }
                        return true;

                    case Keys.Up:
                        if (!flag)
                        {
                            point = new Point(tabRect.Left + (tabRect.Width / 2), tabRect.Top - 3);
                            point2 = flag ? Point.Empty : new Point(-3, 0);
                            break;
                        }
                        nextEnabledTab = this.GetNextEnabledTab(false, false);
                        if (nextEnabledTab != null)
                        {
                            base.SelectedTab = nextEnabledTab;
                        }
                        return true;

                    case Keys.Right:
                        if (flag)
                        {
                            point = new Point(tabRect.Right + 3, tabRect.Top + (tabRect.Height / 2));
                            point2 = flag ? new Point(0, -3) : Point.Empty;
                            break;
                        }
                        nextEnabledTab = this.GetNextEnabledTab(true, false);
                        if (nextEnabledTab != null)
                        {
                            base.SelectedTab = nextEnabledTab;
                        }
                        return true;

                    case Keys.Down:
                        if (!flag)
                        {
                            point = new Point(tabRect.Left + (tabRect.Width / 2), tabRect.Bottom + 3);
                            point2 = flag ? Point.Empty : new Point(-3, 0);
                            break;
                        }
                        nextEnabledTab = this.GetNextEnabledTab(true, false);
                        if (nextEnabledTab != null)
                        {
                            base.SelectedTab = nextEnabledTab;
                        }
                        return true;

                    default:
                        return false;
                }
                while (base.ClientRectangle.Contains(point) && !this.DisplayRectangle.Contains(point))
                {
                    nextEnabledTab = this.TabFromPoint(point);
                    if ((nextEnabledTab != null) && nextEnabledTab.Enabled)
                    {
                        base.SelectedTab = nextEnabledTab;
                        return true;
                    }
                    if (point2.IsEmpty)
                    {
                        return true;
                    }
                    point.Offset(point2);
                }
            }
            return true;
        }

        private static Color LightenColor(Color colorIn, int percent)
        {
            if ((percent < 0) || (percent > 100))
            {
                throw new ArgumentOutOfRangeException("percent");
            }
            int alpha = 0xff;
            int red = colorIn.R + ((int) (((255f - colorIn.R) / 100f) * percent));
            int green = colorIn.G + ((int) (((255f - colorIn.G) / 100f) * percent));
            int blue = colorIn.B + ((int) (((255f - colorIn.B) / 100f) * percent));
            return Color.FromArgb(alpha, red, green, blue);
        }

        protected override void OnBackColorChanged(EventArgs e)
        {
            base.OnBackColorChanged(e);
            base.Invalidate();
        }

        protected override void OnCreateControl()
        {
            base.OnCreateControl();
            this.OnFontChanged(EventArgs.Empty);
        }

        protected override void OnDeselected(TabControlEventArgs e)
        {
            base.OnDeselected(e);
            base.Invalidate();
        }

        protected override void OnDrawItem(DrawItemEventArgs e)
        {
            if (this.DrawMode == TabDrawMode.OwnerDrawFixed)
            {
                base.OnDrawItem(e);
            }
            else
            {
                TabPage page = base.TabPages[e.Index];
                Rectangle bounds = e.Bounds;
                bool flag = (this.RightToLeft == RightToLeft.Yes) && this.RightToLeftLayout;
                bool visualStylesEnabled = this.VisualStylesEnabled;
                bool flag3 = ((page.ImageIndex >= 0) || !string.IsNullOrEmpty(page.ImageKey)) && (base.ImageList != null);
                bool flag4 = e.Index == base.SelectedIndex;
                NativeMethods.RECT lpRect = new NativeMethods.RECT();
                IntPtr hdc = e.Graphics.GetHdc();
                IntPtr hgdiobj = this.Font.ToHfont();
                IntPtr ptr3 = NativeMethods.SelectObject(hdc, hgdiobj);
                NativeMethods.DrawText(hdc, page.Text, page.Text.Length, ref lpRect, NativeMethods.DRAWTEXTFLAGS.HIDEPREFIX | NativeMethods.DRAWTEXTFLAGS.CALCRECT);
                NativeMethods.SelectObject(hdc, ptr3);
                NativeMethods.DeleteObject(hgdiobj);
                e.Graphics.ReleaseHdc(hdc);
                Rectangle rectangle2 = new Rectangle(Point.Empty, lpRect.Size);
                Rectangle empty = Rectangle.Empty;
                Rectangle rectangle4 = rectangle2;
                if (flag3)
                {
                    empty.Size = base.ImageList.ImageSize;
                    rectangle4.Width += empty.Width + base.Padding.X;
                    rectangle4.Height = Math.Max(rectangle2.Height, empty.Height);
                }
                rectangle4.Offset((bounds.Width - rectangle4.Width) / 2, (bounds.Height - rectangle4.Height) / 2);
                empty.X = flag ? (rectangle4.Right - empty.Width) : rectangle4.Left;
                rectangle2.X = rectangle4.Right - rectangle2.Width;
                empty.Offset(0, (bounds.Height - empty.Height) / 2);
                rectangle2.Offset(0, (bounds.Height - lpRect.Size.Height) / 2);
                if (flag4)
                {
                    if (this.Appearance == TabAppearance.Normal)
                    {
                        empty.Offset(0, (base.Alignment == TabAlignment.Bottom) ? 2 : -2);
                        rectangle2.Offset(0, (base.Alignment == TabAlignment.Bottom) ? 2 : -2);
                    }
                    else
                    {
                        empty.Offset(1, 1);
                        rectangle2.Offset(flag ? -1 : 1, 1);
                    }
                }
                if (base.Alignment != TabAlignment.Bottom)
                {
                    empty.Offset(0, 1);
                    rectangle2.Offset(0, 1);
                }
                if (flag3)
                {
                    Bitmap bitmap;
                    if (page.ImageIndex == -1)
                    {
                        bitmap = (Bitmap) base.ImageList.Images[page.ImageKey];
                    }
                    else
                    {
                        bitmap = (Bitmap) base.ImageList.Images[page.ImageIndex];
                    }
                    if (!this.RotateImageWithTab)
                    {
                        switch (base.Alignment)
                        {
                            case TabAlignment.Left:
                                bitmap.RotateFlip(RotateFlipType.Rotate90FlipNone);
                                break;

                            case TabAlignment.Right:
                                bitmap.RotateFlip(RotateFlipType.Rotate270FlipNone);
                                break;
                        }
                    }
                    if (page.Enabled)
                    {
                        e.Graphics.DrawImage(bitmap, empty);
                    }
                    else
                    {
                        ControlPaint.DrawImageDisabled(e.Graphics, bitmap, empty.X, empty.Y, Color.Empty);
                    }
                    bitmap.Dispose();
                }
                NativeMethods.DrawStateFlags pREFIXTEXT = NativeMethods.DrawStateFlags.PREFIXTEXT;
                Color textColor = (((e.Index == this.HotTabID) && base.HotTrack) && ((this.Appearance != TabAppearance.FlatButtons) && !visualStylesEnabled)) ? this.HotColor : this.ForeColor;
                if (!page.Enabled)
                {
                    if (visualStylesEnabled)
                    {
                        textColor = SystemColors.GrayText;
                    }
                    else
                    {
                        pREFIXTEXT |= NativeMethods.DrawStateFlags.DISABLED;
                    }
                }
                if (!this.ShowKeyboardCues)
                {
                    pREFIXTEXT |= NativeMethods.DrawStateFlags.HIDEPREFIX;
                }
                if (this.RightToLeft == RightToLeft.Yes)
                {
                    pREFIXTEXT |= NativeMethods.DrawStateFlags.RTLREADING | NativeMethods.DrawStateFlags.RIGHT;
                }
                IntPtr ptr4 = e.Graphics.GetHdc();
                if (flag)
                {
                    NativeMethods.SetLayout(ptr4, 9);
                }
                e.Graphics.ReleaseHdc(ptr4);
                GdiDrawStateText(ptr4, page.Text, this.Font, textColor, rectangle2, (int) pREFIXTEXT);
                e.Graphics.Flush(FlushIntention.Sync);
            }
        }

        protected override void OnFontChanged(EventArgs e)
        {
            base.OnFontChanged(e);
            IntPtr wParam = this.Font.ToHfont();
            NativeMethods.SendMessage(base.Handle, 0x30, wParam, (IntPtr) (-1));
            NativeMethods.SendMessage(base.Handle, 0x1d, IntPtr.Zero, IntPtr.Zero);
            base.UpdateStyles();
        }

        protected override void OnHandleCreated(EventArgs e)
        {
            base.OnHandleCreated(e);
        }

        protected override void OnKeyDown(KeyEventArgs e)
        {
            if (this.Focused)
            {
                TabPage lastEnabledTab = null;
                switch (e.KeyCode)
                {
                    case Keys.Space:
                    case Keys.Enter:
                        if (this.Appearance != TabAppearance.Normal)
                        {
                            TabPage tabWithFocus = this.TabWithFocus;
                            if (tabWithFocus != null)
                            {
                                e.Handled = !tabWithFocus.Enabled;
                                if (e.Handled)
                                {
                                    SystemSounds.Beep.Play();
                                }
                            }
                        }
                        break;

                    case Keys.End:
                        e.Handled = true;
                        lastEnabledTab = this.GetLastEnabledTab();
                        if (lastEnabledTab != null)
                        {
                            base.SelectedTab = lastEnabledTab;
                        }
                        base.Focus();
                        break;

                    case Keys.Home:
                        e.Handled = true;
                        lastEnabledTab = this.GetFirstEnabledTab();
                        if (lastEnabledTab != null)
                        {
                            base.SelectedTab = lastEnabledTab;
                        }
                        base.Focus();
                        break;

                    case Keys.Left:
                    case Keys.Up:
                    case Keys.Right:
                    case Keys.Down:
                        if (this.Appearance == TabAppearance.Normal)
                        {
                            e.Handled = this.HandleArrowKeys(e.KeyCode);
                        }
                        break;
                }
            }
            base.OnKeyDown(e);
        }

        protected override void OnMouseLeave(EventArgs e)
        {
            base.OnMouseLeave(e);
            if (!base.DesignMode)
            {
                this.HotTabID = -1;
            }
        }

        protected override void OnMouseMove(MouseEventArgs e)
        {
            base.OnMouseMove(e);
            if (!base.DesignMode)
            {
                TabPage tabUnderMouse = this.TabUnderMouse;
                this.HotTabID = (tabUnderMouse == null) ? -1 : base.TabPages.IndexOf(tabUnderMouse);
            }
        }

        protected override void OnPaintBackground(PaintEventArgs pevent)
        {
            if ((base.Width > 0) && (base.Height > 0))
            {
                pevent.Graphics.Flush(FlushIntention.Sync);
                using (Bitmap bitmap = new Bitmap(base.Width, base.Height, PixelFormat.Format32bppPArgb))
                {
                    using (Graphics graphics = Graphics.FromImage(bitmap))
                    {
                        if (this.BufferImage != null)
                        {
                            graphics.DrawImage(this.BufferImage, Point.Empty);
                        }
                        Rectangle clientRectangle = base.ClientRectangle;
                        if (!this.myUseBackColorBehindTabs)
                        {
                            clientRectangle = this.DisplayRectangle;
                        }
                        using (SolidBrush brush = new SolidBrush(this.BackColor))
                        {
                            graphics.FillRectangle(brush, clientRectangle);
                        }
                        this.DrawBorder(graphics);
                        for (int i = 0; i < base.TabCount; i++)
                        {
                            if (i != base.SelectedIndex)
                            {
                                this.PaintTab(graphics, i);
                            }
                        }
                        if (base.SelectedIndex != -1)
                        {
                            this.PaintTab(graphics, base.SelectedIndex);
                        }
                        IntPtr hbitmap = bitmap.GetHbitmap();
                        IntPtr hdc = graphics.GetHdc();
                        IntPtr ptr3 = NativeMethods.CreateCompatibleDC(hdc);
                        IntPtr ptr4 = NativeMethods.SelectObject(ptr3, hbitmap);
                        IntPtr hdcDest = pevent.Graphics.GetHdc();
                        NativeMethods.BitBlt(hdcDest, 0, 0, base.Width, base.Height, ptr3, 0, 0, 0xcc0020);
                        pevent.Graphics.ReleaseHdc(hdcDest);
                        NativeMethods.SelectObject(ptr4, hbitmap);
                        NativeMethods.DeleteDC(ptr3);
                        graphics.ReleaseHdc(hdc);
                        NativeMethods.DeleteObject(hbitmap);
                    }
                }
            }
        }

        protected override void OnResize(EventArgs e)
        {
            base.OnResize(e);
            this.DrawBufferImage();
        }

        protected override void OnSelectedIndexChanged(EventArgs e)
        {
            base.OnSelectedIndexChanged(e);
            if (base.SelectedIndex != -1)
            {
                this.SetDoubleBuffered(base.SelectedTab);
            }
        }

        protected override void OnVisibleChanged(EventArgs e)
        {
            base.OnVisibleChanged(e);
            if (base.Visible && !this.firstShown)
            {
                this.firstShown = true;
                if (base.SelectedIndex != -1)
                {
                    base.SelectedTab = this.GetFirstEnabledTab();
                }
                if (Application.RenderWithVisualStyles && (this.Appearance == TabAppearance.Normal))
                {
                    NativeMethods.SetWindowTheme(base.Handle, "", "");
                    NativeMethods.SetWindowTheme(base.Handle, null, null);
                }
            }
            this.DrawBufferImage();
        }

        private void Paint3DTab(Graphics g, Rectangle r, int id)
        {
            if (id == -1)
            {
                return;
            }
            if (this.VisualStylesEnabled)
            {
                new VisualStyleRenderer(this.GetVisualStyleElement(id)).DrawBackground(g, r);
            }
            else
            {
                Color tabColor = this.TabColor;
                if (base.SelectedIndex == id)
                {
                    tabColor = this.SelectedTabColor;
                }
                Color color2 = LightenColor(tabColor, 40);
                Color color = LightenColor(tabColor, 80);
                Color color4 = DarkenColor(tabColor, 0x19);
                Color color5 = DarkenColor(tabColor, 40);
                if (tabColor.A != 0)
                {
                    Rectangle rect = r;
                    rect.Inflate(-2, -2);
                    rect.Height += 2;
                    using (SolidBrush brush = new SolidBrush(tabColor))
                    {
                        g.FillRectangle(brush, rect);
                    }
                }
                using (Pen pen = new Pen(color))
                {
                    switch (base.Alignment)
                    {
                        case TabAlignment.Top:
                        case TabAlignment.Left:
                        {
                            Point[] points = new Point[] { new Point(r.Left, r.Bottom), new Point(r.Left, r.Top + 2), new Point(r.Left + 2, r.Top), new Point(r.Right - 3, r.Top) };
                            g.DrawLines(pen, points);
                            pen.Color = color2;
                            Point[] pointArray2 = new Point[] { new Point(r.Left + 1, r.Bottom), new Point(r.Left + 1, r.Top + 2), new Point(r.Left + 2, r.Top + 1), new Point(r.Right - 3, r.Top + 1) };
                            g.DrawLines(pen, pointArray2);
                            pen.Color = color4;
                            Point[] pointArray3 = new Point[] { new Point(r.Right - 2, r.Top + 1), new Point(r.Right - 2, r.Bottom) };
                            g.DrawLines(pen, pointArray3);
                            pen.Color = color5;
                            Point[] pointArray4 = new Point[] { new Point(r.Right - 1, r.Top + 2), new Point(r.Right - 1, r.Bottom) };
                            g.DrawLines(pen, pointArray4);
                            goto Label_04F1;
                        }
                        case TabAlignment.Bottom:
                        case TabAlignment.Right:
                        {
                            Point[] pointArray5 = new Point[] { new Point(r.Left, r.Bottom), new Point(r.Left, r.Top + 2) };
                            g.DrawLines(pen, pointArray5);
                            pen.Color = color2;
                            Point[] pointArray6 = new Point[] { new Point(r.Left + 1, r.Bottom), new Point(r.Left + 1, r.Top + 1) };
                            g.DrawLines(pen, pointArray6);
                            pen.Color = color5;
                            Point[] pointArray7 = new Point[] { new Point(r.Left + 2, r.Top), new Point(r.Right - 3, r.Top), new Point(r.Right - 1, r.Top + 2), new Point(r.Right - 1, r.Bottom) };
                            g.DrawLines(pen, pointArray7);
                            pen.Color = color4;
                            Point[] pointArray8 = new Point[] { new Point(r.Left + 2, r.Top + 1), new Point(r.Right - 3, r.Top + 1), new Point(r.Right - 2, r.Top + 2), new Point(r.Right - 2, r.Bottom) };
                            g.DrawLines(pen, pointArray8);
                            goto Label_04F1;
                        }
                    }
                }
            }
        Label_04F1:
            g.Flush(FlushIntention.Sync);
        }

        private void PaintButtonTab(Graphics g, Rectangle r, int id)
        {
            Color tabColor = this.TabColor;
            TabPage page = base.TabPages[id];
            if (base.SelectedIndex == id)
            {
                tabColor = this.SelectedTabColor;
            }
            bool flag = (id == base.SelectedIndex) || (page == this.TabWithFocus);
            Color color2 = flag ? DarkenColor(tabColor, 0x19) : LightenColor(tabColor, 40);
            Color color = flag ? DarkenColor(tabColor, 40) : LightenColor(tabColor, 80);
            Color color4 = flag ? LightenColor(tabColor, 40) : DarkenColor(tabColor, 0x19);
            Color color5 = flag ? LightenColor(tabColor, 80) : DarkenColor(tabColor, 40);
            using (SolidBrush brush = new SolidBrush(tabColor))
            {
                g.FillRectangle(brush, r);
            }
            using (Pen pen = new Pen(color))
            {
                switch (base.Alignment)
                {
                    case TabAlignment.Top:
                    case TabAlignment.Left:
                    {
                        Point[] points = new Point[] { new Point(r.Left, r.Bottom - 1), new Point(r.Left, r.Top), new Point(r.Right - 1, r.Top) };
                        g.DrawLines(pen, points);
                        pen.Color = color5;
                        Point[] pointArray2 = new Point[] { new Point(r.Right - 1, r.Top), new Point(r.Right - 1, r.Bottom - 1), new Point(r.Left, r.Bottom - 1) };
                        g.DrawLines(pen, pointArray2);
                        pen.Color = color2;
                        Point[] pointArray3 = new Point[] { new Point(r.Left + 1, r.Bottom - 2), new Point(r.Left + 1, r.Top + 1), new Point(r.Right - 2, r.Top + 1) };
                        g.DrawLines(pen, pointArray3);
                        pen.Color = color4;
                        Point[] pointArray4 = new Point[] { new Point(r.Right - 2, r.Top + 1), new Point(r.Right - 2, r.Bottom - 2), new Point(r.Left + 1, r.Bottom - 2) };
                        g.DrawLines(pen, pointArray4);
                        goto Label_0500;
                    }
                    case TabAlignment.Bottom:
                    case TabAlignment.Right:
                    {
                        Point[] pointArray5 = new Point[] { new Point(r.Left, r.Top), new Point(r.Left, r.Bottom - 1), new Point(r.Right - 1, r.Bottom - 1) };
                        g.DrawLines(pen, pointArray5);
                        pen.Color = color5;
                        Point[] pointArray6 = new Point[] { new Point(r.Right - 1, r.Bottom - 1), new Point(r.Right - 1, r.Top), new Point(r.Left, r.Top) };
                        g.DrawLines(pen, pointArray6);
                        pen.Color = color2;
                        Point[] pointArray7 = new Point[] { new Point(r.Left + 1, r.Top + 1), new Point(r.Left + 1, r.Bottom - 2), new Point(r.Right - 2, r.Bottom - 2) };
                        g.DrawLines(pen, pointArray7);
                        pen.Color = color4;
                        Point[] pointArray8 = new Point[] { new Point(r.Right - 2, r.Bottom - 2), new Point(r.Right - 2, r.Top + 1), new Point(r.Left + 1, r.Top + 1) };
                        g.DrawLines(pen, pointArray8);
                        goto Label_0500;
                    }
                }
            }
        Label_0500:
            g.Flush(FlushIntention.Sync);
        }

        private void PaintFlatTab(Graphics g, Rectangle r, int id)
        {
            Color tabColor = this.TabColor;
            TabPage page = base.TabPages[id];
            if (base.SelectedIndex == id)
            {
                tabColor = this.SelectedTabColor;
            }
            bool flag = id == base.SelectedIndex;
            Color color2 = flag ? DarkenColor(tabColor, 0x19) : LightenColor(tabColor, 40);
            Color color = flag ? DarkenColor(tabColor, 40) : LightenColor(tabColor, 80);
            Color color4 = flag ? LightenColor(tabColor, 40) : DarkenColor(tabColor, 0x19);
            Color color5 = flag ? LightenColor(tabColor, 80) : DarkenColor(tabColor, 0x19);
            using (SolidBrush brush = new SolidBrush(tabColor))
            {
                g.FillRectangle(brush, r);
            }
            if (((page == this.TabWithFocus) || (id == base.SelectedIndex)) || ((id == this.HotTabID) && base.HotTrack))
            {
                using (Pen pen = new Pen(color))
                {
                    switch (base.Alignment)
                    {
                        case TabAlignment.Top:
                        case TabAlignment.Left:
                        {
                            Point[] points = new Point[] { new Point(r.Left, r.Bottom - 1), new Point(r.Left, r.Top), new Point(r.Right - 1, r.Top) };
                            g.DrawLines(pen, points);
                            pen.Color = color5;
                            Point[] pointArray2 = new Point[] { new Point(r.Right - 1, r.Top), new Point(r.Right - 1, r.Bottom - 1), new Point(r.Left, r.Bottom - 1) };
                            g.DrawLines(pen, pointArray2);
                            if (flag)
                            {
                                pen.Color = color2;
                                Point[] pointArray3 = new Point[] { new Point(r.Left + 1, r.Bottom - 2), new Point(r.Left + 1, r.Top + 1), new Point(r.Right - 2, r.Top + 1) };
                                g.DrawLines(pen, pointArray3);
                                pen.Color = color4;
                                Point[] pointArray4 = new Point[] { new Point(r.Right - 2, r.Top + 1), new Point(r.Right - 2, r.Bottom - 2), new Point(r.Left + 1, r.Bottom - 2) };
                                g.DrawLines(pen, pointArray4);
                            }
                            goto Label_0529;
                        }
                        case TabAlignment.Bottom:
                        case TabAlignment.Right:
                        {
                            Point[] pointArray5 = new Point[] { new Point(r.Left, r.Top), new Point(r.Left, r.Bottom - 1), new Point(r.Right - 1, r.Bottom - 1) };
                            g.DrawLines(pen, pointArray5);
                            pen.Color = color5;
                            Point[] pointArray6 = new Point[] { new Point(r.Right - 1, r.Bottom - 1), new Point(r.Right - 1, r.Top), new Point(r.Left, r.Top) };
                            g.DrawLines(pen, pointArray6);
                            if (flag)
                            {
                                pen.Color = color2;
                                Point[] pointArray7 = new Point[] { new Point(r.Left + 1, r.Top + 1), new Point(r.Left + 1, r.Bottom - 2), new Point(r.Right - 2, r.Bottom - 2) };
                                g.DrawLines(pen, pointArray7);
                                pen.Color = color4;
                                Point[] pointArray8 = new Point[] { new Point(r.Right - 2, r.Bottom - 2), new Point(r.Right - 2, r.Top + 1), new Point(r.Left + 1, r.Top + 1) };
                                g.DrawLines(pen, pointArray8);
                            }
                            goto Label_0529;
                        }
                    }
                }
            }
        Label_0529:
            g.Flush(FlushIntention.Sync);
        }

        private void PaintTab(Graphics graphics, int id)
        {
            if (id != -1)
            {
                TabAlignment alignment = base.Alignment;
                Bitmap image = this.CreateTabBaseBitmap(id);
                Rectangle rect = new Rectangle(Point.Empty, image.Size);
                using (Graphics graphics2 = Graphics.FromImage(image))
                {
                    IntPtr hdc = graphics2.GetHdc();
                    IntPtr hbitmap = image.GetHbitmap();
                    IntPtr ptr3 = NativeMethods.CreateCompatibleDC(hdc);
                    IntPtr ptr4 = NativeMethods.SelectObject(ptr3, hbitmap);
                    if (this.DrawMode == TabDrawMode.Normal)
                    {
                        using (Graphics graphics3 = Graphics.FromHdc(ptr3))
                        {
                            DrawItemEventArgs e = new DrawItemEventArgs(graphics3, this.Font, rect, id, DrawItemState.Default);
                            this.OnDrawItem(e);
                        }
                    }
                    NativeMethods.BitBlt(hdc, 0, 0, rect.Width, rect.Height, ptr3, 0, 0, 0xcc0020);
                    NativeMethods.SelectObject(ptr4, hbitmap);
                    NativeMethods.DeleteDC(ptr3);
                    NativeMethods.DeleteObject(hbitmap);
                    graphics2.ReleaseHdc(hdc);
                    graphics2.Flush(FlushIntention.Sync);
                }
                switch (base.Alignment)
                {
                    case TabAlignment.Left:
                        image.RotateFlip(RotateFlipType.Rotate270FlipNone);
                        break;

                    case TabAlignment.Right:
                        image.RotateFlip(RotateFlipType.Rotate90FlipNone);
                        break;
                }
                Rectangle tabRect = base.GetTabRect(id);
                if ((id == base.SelectedIndex) && (this.Appearance == TabAppearance.Normal))
                {
                    tabRect.Inflate(2, 2);
                }
                if (this.DrawMode == TabDrawMode.OwnerDrawFixed)
                {
                    bool flag = (this.RightToLeft == RightToLeft.Yes) && this.RightToLeftLayout;
                    if (flag)
                    {
                        image.RotateFlip(RotateFlipType.RotateNoneFlipX);
                    }
                    IntPtr hgdiobj = image.GetHbitmap();
                    using (Graphics graphics4 = Graphics.FromImage(image))
                    {
                        IntPtr ptr6 = graphics4.GetHdc();
                        IntPtr ptr7 = NativeMethods.CreateCompatibleDC(ptr6);
                        IntPtr ptr8 = NativeMethods.SelectObject(ptr7, hgdiobj);
                        Graphics graphics5 = Graphics.FromHdc(ptr7);
                        graphics5.TranslateTransform((float) -tabRect.Left, (float) -tabRect.Top);
                        DrawItemEventArgs args2 = new DrawItemEventArgs(graphics5, this.Font, tabRect, id, DrawItemState.Default);
                        this.OnDrawItem(args2);
                        NativeMethods.BitBlt(ptr6, 0, 0, tabRect.Width, tabRect.Height, ptr7, 0, 0, 0xcc0020);
                        graphics5.ResetTransform();
                        NativeMethods.SelectObject(ptr8, hgdiobj);
                        graphics5.Dispose();
                        NativeMethods.DeleteObject(ptr7);
                        graphics4.ReleaseHdc(ptr6);
                    }
                    NativeMethods.DeleteObject(hgdiobj);
                    if (flag)
                    {
                        image.RotateFlip(RotateFlipType.RotateNoneFlipX);
                    }
                }
                graphics.DrawImage(image, tabRect);
                image.Dispose();
                if (this.Appearance == TabAppearance.FlatButtons)
                {
                    using (Pen pen = new Pen(DarkenColor(this.BackColor, 0x19)))
                    {
                        graphics.DrawLine(pen, tabRect.Right + 4, tabRect.Top, tabRect.Right + 4, tabRect.Bottom);
                        pen.Color = LightenColor(this.BackColor, 80);
                        graphics.DrawLine(pen, tabRect.Right + 5, tabRect.Top, tabRect.Right + 5, tabRect.Bottom);
                    }
                }
                tabRect.Inflate(-2, -2);
                if ((this.Focused && this.ShowFocusCues) && (id == base.SelectedIndex))
                {
                    ControlPaint.DrawFocusRectangle(graphics, tabRect);
                }
                graphics.Flush(FlushIntention.Sync);
            }
        }

        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            bool flag = base.ProcessCmdKey(ref msg, keyData);
            if (flag)
            {
                return flag;
            }
            if ((keyData == (Keys.Control | Keys.Tab)) || (keyData == (Keys.Control | Keys.Shift | Keys.Tab)))
            {
                base.SelectedTab = this.GetNextEnabledTab((keyData & Keys.Shift) == Keys.None, true);
                base.Focus();
                return true;
            }
            if ((keyData != (Keys.Control | Keys.PageUp)) && (keyData != (Keys.Control | Keys.Next)))
            {
                return flag;
            }
            base.SelectedTab = this.GetNextEnabledTab((keyData & Keys.Next) == Keys.Next, true);
            base.Focus();
            return true;
        }

        protected override bool ProcessMnemonic(char charCode)
        {
            foreach (TabPage page in base.TabPages)
            {
                if (page.Enabled && Control.IsMnemonic(charCode, page.Text))
                {
                    base.SelectedTab = page;
                    return true;
                }
            }
            return base.ProcessMnemonic(charCode);
        }

        public void ResetBackColor()
        {
            this.BackColor = Color.Empty;
        }

        private void SetDoubleBuffered(TabPage page)
        {
            PropertyInfo property = page.GetType().GetProperty("DoubleBuffered", BindingFlags.NonPublic | BindingFlags.Instance);
            if (property != null)
            {
                property.SetValue(page, this.myDoubleBufferTabpages, null);
            }
        }

        private bool ShouldSerializeBackColor()
        {
            return !this.myBackColor.Equals(Color.Empty);
        }

        private TabPage TabFromPoint(Point point)
        {
            NativeMethods.TCHITTESTINFO lParam = new NativeMethods.TCHITTESTINFO(point.X, point.Y);
            int num = NativeMethods.SendMessage(base.Handle, 0x130d, IntPtr.Zero, ref lParam).ToInt32();
            if ((num >= 0) && (num < base.TabCount))
            {
                return base.TabPages[num];
            }
            return null;
        }

        private bool TabIsPartiallyTransparent(int id)
        {
            if (this.VisualStylesEnabled)
            {
                VisualStyleRenderer renderer = new VisualStyleRenderer(this.GetVisualStyleElement(id));
                return renderer.IsBackgroundPartiallyTransparent();
            }
            if (this.Appearance == TabAppearance.Normal)
            {
                return true;
            }
            if (id == base.SelectedIndex)
            {
                return (this.SelectedTabColor.A < 0xff);
            }
            return (this.TabColor.A < 0xff);
        }

        [PermissionSet(SecurityAction.Demand, Name="FullTrust")]
        protected override void WndProc(ref Message m)
        {
            switch (m.Msg)
            {
                case 0x201:
                {
                    TabPage tabUnderMouse = this.TabUnderMouse;
                    if ((tabUnderMouse != null) && !tabUnderMouse.Enabled)
                    {
                        m.Msg = 0;
                    }
                    break;
                }
                case 0x31a:
                    base.Invalidate(true);
                    break;

                case 20:
                    m.Msg = 0;
                    m.Result = IntPtr.Zero;
                    break;

                case 0x114:
                    base.Invalidate();
                    break;
            }
            base.WndProc(ref m);
        }

        public TabAppearance Appearance
        {
            get
            {
                return base.Appearance;
            }
            set
            {
                base.Appearance = value;
            }
        }

        [Browsable(true), EditorBrowsable(EditorBrowsableState.Always)]
        public override Color BackColor
        {
            get
            {
                if (this.myBackColor.Equals(Color.Empty))
                {
                    return base.BackColor;
                }
                return this.myBackColor;
            }
            set
            {
                this.myBackColor = value;
                this.OnBackColorChanged(EventArgs.Empty);
            }
        }

        public override Rectangle DisplayRectangle
        {
            get
            {
                int num;
                int height;
                int x = 0;
                if (this.Appearance == TabAppearance.Normal)
                {
                    x = 4;
                }
                if (base.Alignment <= TabAlignment.Bottom)
                {
                    height = base.ItemSize.Height;
                }
                else
                {
                    height = base.ItemSize.Width;
                }
                if (this.Appearance == TabAppearance.Normal)
                {
                    num = 5 + (height * base.RowCount);
                }
                else
                {
                    num = (3 + height) * base.RowCount;
                }
                switch (base.Alignment)
                {
                    case TabAlignment.Bottom:
                        return new Rectangle(x, x, base.Width - (x * 2), (base.Height - num) - x);

                    case TabAlignment.Left:
                        return new Rectangle(num, x, (base.Width - num) - x, base.Height - (x * 2));

                    case TabAlignment.Right:
                        return new Rectangle(x, x, (base.Width - num) - x, base.Height - (x * 2));
                }
                return new Rectangle(x, num, base.Width - (x * 2), (base.Height - num) - x);
            }
        }

        [Category("Behavior"), Description("Sets DoubleBuffer on TabPages to help with flicker. Should only be set if using transparency."), DefaultValue(false)]
        public bool DoubleBufferTabPages
        {
            get
            {
                return this.myDoubleBufferTabpages;
            }
            set
            {
                if (this.myDoubleBufferTabpages != value)
                {
                    this.myDoubleBufferTabpages = value;
                    if (base.SelectedIndex != -1)
                    {
                        this.SetDoubleBuffered(base.SelectedTab);
                    }
                }
            }
        }

        [DefaultValue(typeof(TabDrawMode), "Normal")]
        public TabDrawMode DrawMode
        {
            get
            {
                return this.myDrawMode;
            }
            set
            {
                this.myDrawMode = value;
                base.DrawMode = TabDrawMode.Normal;
                base.Invalidate();
            }
        }

        [EditorBrowsable(EditorBrowsableState.Always), Browsable(true)]
        public override Color ForeColor
        {
            get
            {
                return base.ForeColor;
            }
            set
            {
                base.ForeColor = value;
            }
        }

        [DefaultValue(typeof(Color), "HotTrack"), Description("The color of text on a tab which the mouse is over. Only applies if HotTrack is true."), Category("Appearance")]
        public Color HotColor
        {
            get
            {
                return this.myHotColor;
            }
            set
            {
                this.myHotColor = value;
                base.Invalidate();
            }
        }

        private int HotTabID
        {
            get
            {
                return this.myHotTabID;
            }
            set
            {
                if (this.myHotTabID != value)
                {
                    this.myHotTabID = value;
                    base.Invalidate();
                }
            }
        }

        [Description("Gets/sets whether or not a tabs Image should be rotated with the tab."), Category("Appearance"), DefaultValue(false)]
        public bool RotateImageWithTab
        {
            get
            {
                return this.rotateImageWithTab;
            }
            set
            {
                if (this.rotateImageWithTab != value)
                {
                    this.rotateImageWithTab = value;
                    base.Invalidate();
                }
            }
        }

        [Category("Appearance"), Description("The color of the selected tab without Visual Style."), DefaultValue(typeof(Color), "Control")]
        public Color SelectedTabColor
        {
            get
            {
                return this.mySelectedTabColor;
            }
            set
            {
                this.mySelectedTabColor = value;
                base.Invalidate();
            }
        }

        [Description("The color of unselected tabs without Visual Style."), Category("Appearance"), DefaultValue(typeof(Color), "Control")]
        public Color TabColor
        {
            get
            {
                return this.myTabColor;
            }
            set
            {
                this.myTabColor = value;
                base.Invalidate();
            }
        }

        private TabPage TabUnderMouse
        {
            get
            {
                Point point = base.PointToClient(Control.MousePosition);
                return this.TabFromPoint(point);
            }
        }

        private TabPage TabWithFocus
        {
            get
            {
                if (this.Appearance == TabAppearance.Normal)
                {
                    return base.SelectedTab;
                }
                int num = NativeMethods.SendMessage(base.Handle, 0x132f, IntPtr.Zero, IntPtr.Zero).ToInt32();
                if (num != -1)
                {
                    return base.TabPages[num];
                }
                return null;
            }
        }

        [DefaultValue(false), Category("Appearance"), Description("Gets/sets whether or not the BackColor should be painted behind tabs.")]
        public bool UseBackColorBehindTabs
        {
            get
            {
                return this.myUseBackColorBehindTabs;
            }
            set
            {
                this.myUseBackColorBehindTabs = value;
                base.Invalidate();
            }
        }

        private bool VisualStylesEnabled
        {
            get
            {
                if ((this.Appearance != TabAppearance.Normal) || !Application.RenderWithVisualStyles)
                {
                    return false;
                }
                if (base.DesignMode)
                {
                    return VisualStyleRenderer.IsSupported;
                }
                return (NativeMethods.GetWindowTheme(base.Handle) != IntPtr.Zero);
            }
        }
    }
}

