﻿namespace Polenter.Serialization
{
    using System;

    [AttributeUsage(AttributeTargets.Property, Inherited=true, AllowMultiple=false)]
    public sealed class ExcludeFromSerializationAttribute : Attribute
    {
    }
}

