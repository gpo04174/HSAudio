﻿namespace Polenter.Serialization
{
    using System;

    public enum BinarySerializationMode
    {
        SizeOptimized,
        Burst
    }
}

