﻿namespace Polenter.Serialization
{
    using Polenter.Serialization.Core;
    using System;
    using System.Globalization;
    using System.Runtime.CompilerServices;
    using System.Text;

    public sealed class SharpSerializerXmlSettings : SharpSerializerSettings<AdvancedSharpSerializerXmlSettings>
    {
        [CompilerGenerated]
        private CultureInfo _Culture;
        [CompilerGenerated]
        private System.Text.Encoding _Encoding;

        public SharpSerializerXmlSettings()
        {
            this.Culture = CultureInfo.InvariantCulture;
            this.Encoding = System.Text.Encoding.UTF8;
        }

        public CultureInfo Culture
        {
            [CompilerGenerated]
            get
            {
                return this._Culture;
            }
            [CompilerGenerated]
            set
            {
                this._Culture = value;
            }
        }

        public System.Text.Encoding Encoding
        {
            [CompilerGenerated]
            get
            {
                return this._Encoding;
            }
            [CompilerGenerated]
            set
            {
                this._Encoding = value;
            }
        }
    }
}

