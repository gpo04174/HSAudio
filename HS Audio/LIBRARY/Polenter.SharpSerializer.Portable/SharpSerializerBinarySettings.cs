﻿namespace Polenter.Serialization
{
    using Polenter.Serialization.Core;
    using System;
    using System.Runtime.CompilerServices;
    using System.Text;

    public sealed class SharpSerializerBinarySettings : SharpSerializerSettings<AdvancedSharpSerializerBinarySettings>
    {
        [CompilerGenerated]
        private System.Text.Encoding _Encoding;
        [CompilerGenerated]
        private BinarySerializationMode _Mode;

        public SharpSerializerBinarySettings()
        {
            this.Encoding = System.Text.Encoding.UTF8;
        }

        public SharpSerializerBinarySettings(BinarySerializationMode mode)
        {
            this.Encoding = System.Text.Encoding.UTF8;
            this.Mode = mode;
        }

        public System.Text.Encoding Encoding
        {
            [CompilerGenerated]
            get
            {
                return this._Encoding;
            }
            [CompilerGenerated]
            set
            {
                this._Encoding = value;
            }
        }

        public BinarySerializationMode Mode
        {
            [CompilerGenerated]
            get
            {
                return this._Mode;
            }
            [CompilerGenerated]
            set
            {
                this._Mode = value;
            }
        }
    }
}

