﻿namespace Polenter.Serialization
{
    using Polenter.Serialization.Advanced;
    using Polenter.Serialization.Advanced.Binary;
    using Polenter.Serialization.Advanced.Deserializing;
    using Polenter.Serialization.Advanced.Serializing;
    using Polenter.Serialization.Advanced.Xml;
    using Polenter.Serialization.Core;
    using Polenter.Serialization.Deserializing;
    using Polenter.Serialization.Serializing;
    using System;
    using System.IO;
    using System.Xml;

    public sealed class SharpSerializer
    {
        private IPropertyDeserializer _deserializer;
        private Polenter.Serialization.Advanced.PropertyProvider _propertyProvider;
        private string _rootName;
        private IPropertySerializer _serializer;

        public SharpSerializer()
        {
            this.initialize(new SharpSerializerXmlSettings());
        }

        public SharpSerializer(SharpSerializerBinarySettings settings)
        {
            if (settings == null)
            {
                throw new ArgumentNullException("settings");
            }
            this.initialize(settings);
        }

        public SharpSerializer(SharpSerializerXmlSettings settings)
        {
            if (settings == null)
            {
                throw new ArgumentNullException("settings");
            }
            this.initialize(settings);
        }

        public SharpSerializer(bool binarySerialization)
        {
            if (binarySerialization)
            {
                this.initialize(new SharpSerializerBinarySettings());
            }
            else
            {
                this.initialize(new SharpSerializerXmlSettings());
            }
        }

        public SharpSerializer(IPropertySerializer serializer, IPropertyDeserializer deserializer)
        {
            if (serializer == null)
            {
                throw new ArgumentNullException("serializer");
            }
            if (deserializer == null)
            {
                throw new ArgumentNullException("deserializer");
            }
            this._serializer = serializer;
            this._deserializer = deserializer;
        }

        public object Deserialize(Stream stream)
        {
            object obj2;
            try
            {
                this._deserializer.Open(stream);
                Property property = this._deserializer.Deserialize();
                this._deserializer.Close();
                obj2 = new ObjectFactory().CreateObject(property);
            }
            catch (Exception exception)
            {
                throw new DeserializingException("An error occured during the deserialization. Details are in the inner exception.", exception);
            }
            return obj2;
        }

        private void initialize(SharpSerializerBinarySettings settings)
        {
            this.PropertyProvider.PropertiesToIgnore = settings.AdvancedSettings.PropertiesToIgnore;
            this.PropertyProvider.AttributesToIgnore = settings.AdvancedSettings.AttributesToIgnore;
            this.RootName = settings.AdvancedSettings.RootName;
            ITypeNameConverter typeNameConverter = settings.AdvancedSettings.TypeNameConverter ?? DefaultInitializer.GetTypeNameConverter(settings.IncludeAssemblyVersionInTypeName, settings.IncludeCultureInTypeName, settings.IncludePublicKeyTokenInTypeName);
            IBinaryReader reader = null;
            IBinaryWriter writer = null;
            if (settings.Mode == BinarySerializationMode.Burst)
            {
                writer = new BurstBinaryWriter(typeNameConverter, settings.Encoding);
                reader = new BurstBinaryReader(typeNameConverter, settings.Encoding);
            }
            else
            {
                writer = new SizeOptimizedBinaryWriter(typeNameConverter, settings.Encoding);
                reader = new SizeOptimizedBinaryReader(typeNameConverter, settings.Encoding);
            }
            this._deserializer = new BinaryPropertyDeserializer(reader);
            this._serializer = new BinaryPropertySerializer(writer);
        }

        private void initialize(SharpSerializerXmlSettings settings)
        {
            this.PropertyProvider.PropertiesToIgnore = settings.AdvancedSettings.PropertiesToIgnore;
            this.PropertyProvider.AttributesToIgnore = settings.AdvancedSettings.AttributesToIgnore;
            this.RootName = settings.AdvancedSettings.RootName;
            ITypeNameConverter typeNameConverter = settings.AdvancedSettings.TypeNameConverter ?? DefaultInitializer.GetTypeNameConverter(settings.IncludeAssemblyVersionInTypeName, settings.IncludeCultureInTypeName, settings.IncludePublicKeyTokenInTypeName);
            ISimpleValueConverter valueConverter = settings.AdvancedSettings.SimpleValueConverter ?? DefaultInitializer.GetSimpleValueConverter(settings.Culture, typeNameConverter);
            XmlWriterSettings xmlWriterSettings = DefaultInitializer.GetXmlWriterSettings(settings.Encoding);
            XmlReaderSettings xmlReaderSettings = DefaultInitializer.GetXmlReaderSettings();
            DefaultXmlReader reader = new DefaultXmlReader(typeNameConverter, valueConverter, xmlReaderSettings);
            DefaultXmlWriter writer = new DefaultXmlWriter(typeNameConverter, valueConverter, xmlWriterSettings);
            this._serializer = new XmlPropertySerializer(writer);
            this._deserializer = new XmlPropertyDeserializer(reader);
        }

        public void Serialize(object data, Stream stream)
        {
            if (data == null)
            {
                throw new ArgumentNullException("data");
            }
            Property property = new PropertyFactory(this.PropertyProvider).CreateProperty(this.RootName, data);
            try
            {
                this._serializer.Open(stream);
                this._serializer.Serialize(property);
            }
            finally
            {
                this._serializer.Close();
            }
        }

        public Polenter.Serialization.Advanced.PropertyProvider PropertyProvider
        {
            get
            {
                if (this._propertyProvider == null)
                {
                    this._propertyProvider = new Polenter.Serialization.Advanced.PropertyProvider();
                }
                return this._propertyProvider;
            }
            set
            {
                this._propertyProvider = value;
            }
        }

        public string RootName
        {
            get
            {
                if (this._rootName == null)
                {
                    this._rootName = "Root";
                }
                return this._rootName;
            }
            set
            {
                this._rootName = value;
            }
        }
    }
}

