﻿namespace Polenter.Serialization.Advanced.Xml
{
    using System;
    using System.Collections.Generic;
    using System.IO;

    public interface IXmlReader
    {
        void Close();
        int[] GetAttributeAsArrayOfInt(string attributeName);
        int GetAttributeAsInt(string attributeName);
        object GetAttributeAsObject(string attributeName, Type expectedType);
        string GetAttributeAsString(string attributeName);
        Type GetAttributeAsType(string attributeName);
        void Open(Stream stream);
        string ReadElement();
        IEnumerable<string> ReadSubElements();
    }
}

