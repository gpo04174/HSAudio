﻿namespace Polenter.Serialization.Advanced.Xml
{
    using System;

    public interface ISimpleValueConverter
    {
        object ConvertFromString(string text, Type type);
        string ConvertToString(object value);
    }
}

