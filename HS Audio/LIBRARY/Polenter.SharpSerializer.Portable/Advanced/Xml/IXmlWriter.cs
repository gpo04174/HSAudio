﻿namespace Polenter.Serialization.Advanced.Xml
{
    using System;
    using System.IO;

    public interface IXmlWriter
    {
        void Close();
        void Open(Stream stream);
        void WriteAttribute(string attributeId, int number);
        void WriteAttribute(string attributeId, string text);
        void WriteAttribute(string attributeId, Type type);
        void WriteAttribute(string attributeId, int[] numbers);
        void WriteAttribute(string attributeId, object value);
        void WriteEndElement();
        void WriteStartElement(string elementId);
    }
}

