﻿namespace Polenter.Serialization.Advanced
{
    using Polenter.Serialization.Advanced.Serializing;
    using Polenter.Serialization.Advanced.Xml;
    using Polenter.Serialization.Core;
    using System;
    using System.Globalization;

    public sealed class SimpleValueConverter : ISimpleValueConverter
    {
        private readonly CultureInfo _cultureInfo;
        private readonly ITypeNameConverter _typeNameConverter;
        private const char NullChar = '\0';
        private const string NullCharAsString = "&#x0;";

        public SimpleValueConverter()
        {
            this._cultureInfo = CultureInfo.InvariantCulture;
            this._typeNameConverter = new TypeNameConverter();
        }

        public SimpleValueConverter(CultureInfo cultureInfo, ITypeNameConverter typeNameConverter)
        {
            this._cultureInfo = cultureInfo;
            this._typeNameConverter = typeNameConverter;
        }

        public object ConvertFromString(string text, Type type)
        {
            object obj2;
            try
            {
                if (type == typeof(string))
                {
                    return text;
                }
                if (type == typeof(bool))
                {
                    return Convert.ToBoolean(text, this._cultureInfo);
                }
                if (type == typeof(byte))
                {
                    return Convert.ToByte(text, this._cultureInfo);
                }
                if (type == typeof(char))
                {
                    if (text == "&#x0;")
                    {
                        return '\0';
                    }
                    return Convert.ToChar(text, this._cultureInfo);
                }
                if (type == typeof(DateTime))
                {
                    return Convert.ToDateTime(text, this._cultureInfo);
                }
                if (type == typeof(decimal))
                {
                    return Convert.ToDecimal(text, this._cultureInfo);
                }
                if (type == typeof(double))
                {
                    return Convert.ToDouble(text, this._cultureInfo);
                }
                if (type == typeof(short))
                {
                    return Convert.ToInt16(text, this._cultureInfo);
                }
                if (type == typeof(int))
                {
                    return Convert.ToInt32(text, this._cultureInfo);
                }
                if (type == typeof(long))
                {
                    return Convert.ToInt64(text, this._cultureInfo);
                }
                if (type == typeof(sbyte))
                {
                    return Convert.ToSByte(text, this._cultureInfo);
                }
                if (type == typeof(float))
                {
                    return Convert.ToSingle(text, this._cultureInfo);
                }
                if (type == typeof(ushort))
                {
                    return Convert.ToUInt16(text, this._cultureInfo);
                }
                if (type == typeof(uint))
                {
                    return Convert.ToUInt32(text, this._cultureInfo);
                }
                if (type == typeof(ulong))
                {
                    return Convert.ToUInt64(text, this._cultureInfo);
                }
                if (type == typeof(TimeSpan))
                {
                    return TimeSpan.Parse(text);
                }
                if (type == typeof(Guid))
                {
                    return new Guid(text);
                }
                if (type.IsEnum)
                {
                    return Enum.Parse(type, text, true);
                }
                if (type == typeof(byte[]))
                {
                    return Convert.FromBase64String(text);
                }
                if (!isType(type))
                {
                    throw new InvalidOperationException(string.Format("Unknown simple type: {0}", new object[] { type.FullName }));
                }
                obj2 = this._typeNameConverter.ConvertToType(text);
            }
            catch (Exception exception)
            {
                throw new SimpleValueParsingException(string.Format("Invalid value: {0}. See details in the inner exception.", new object[] { text }), exception);
            }
            return obj2;
        }

        public string ConvertToString(object value)
        {
            if (value == null)
            {
                return string.Empty;
            }
            if (value.GetType() == typeof(byte[]))
            {
                return Convert.ToBase64String((byte[]) value);
            }
            if (isType(value))
            {
                return this._typeNameConverter.ConvertToTypeName((Type) value);
            }
            if (value.Equals('\0'))
            {
                return "&#x0;";
            }
            return Convert.ToString(value, this._cultureInfo);
        }

        private static bool isType(object value)
        {
            return (value is Type);
        }
    }
}

