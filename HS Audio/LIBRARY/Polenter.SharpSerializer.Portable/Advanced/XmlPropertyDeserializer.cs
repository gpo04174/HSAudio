﻿namespace Polenter.Serialization.Advanced
{
    using Polenter.Serialization.Advanced.Deserializing;
    using Polenter.Serialization.Advanced.Xml;
    using Polenter.Serialization.Core;
    using Polenter.Serialization.Serializing;
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Reflection;

    public sealed class XmlPropertyDeserializer : IPropertyDeserializer
    {
        private readonly Dictionary<int, ReferenceTargetProperty> _propertyCache = new Dictionary<int, ReferenceTargetProperty>();
        private readonly IXmlReader _reader;

        public XmlPropertyDeserializer(IXmlReader reader)
        {
            this._reader = reader;
        }

        public void Close()
        {
            this._reader.Close();
        }

        private Property createProperty(int referenceId, string propertyName, Type propertyType)
        {
            ReferenceTargetProperty source = this._propertyCache[referenceId];
            ReferenceTargetProperty property2 = (ReferenceTargetProperty) Property.CreateInstance(source.Art, propertyName, propertyType);
            ReferenceInfo reference = source.Reference;
            reference.Count++;
            property2.MakeFlatCopyFrom(source);
            ReferenceInfo info = new ReferenceInfo();
            info.Id = referenceId;
            property2.Reference = info;
            return property2;
        }

        private Property deserialize(PropertyArt propertyArt, Type expectedType)
        {
            string attributeAsString = this._reader.GetAttributeAsString("name");
            Type attributeAsType = this._reader.GetAttributeAsType("type");
            if (attributeAsType == null)
            {
                attributeAsType = expectedType;
            }
            Property property = Property.CreateInstance(propertyArt, attributeAsString, attributeAsType);
            NullProperty property2 = property as NullProperty;
            if (property2 != null)
            {
                return property2;
            }
            SimpleProperty property3 = property as SimpleProperty;
            if (property3 != null)
            {
                this.parseSimpleProperty(this._reader, property3);
                return property3;
            }
            int attributeAsInt = this._reader.GetAttributeAsInt("id");
            ReferenceTargetProperty property4 = property as ReferenceTargetProperty;
            if ((property4 != null) && (attributeAsInt > 0))
            {
                ReferenceInfo info = new ReferenceInfo();
                info.Id = attributeAsInt;
                info.IsProcessed = true;
                property4.Reference = info;
                this._propertyCache.Add(attributeAsInt, property4);
            }
            if (property == null)
            {
                if (attributeAsInt < 1)
                {
                    return null;
                }
                property = this.createProperty(attributeAsInt, attributeAsString, attributeAsType);
                if (property == null)
                {
                    return null;
                }
                return property;
            }
            MultiDimensionalArrayProperty property5 = property as MultiDimensionalArrayProperty;
            if (property5 != null)
            {
                this.parseMultiDimensionalArrayProperty(property5);
                return property5;
            }
            SingleDimensionalArrayProperty property6 = property as SingleDimensionalArrayProperty;
            if (property6 != null)
            {
                this.parseSingleDimensionalArrayProperty(property6);
                return property6;
            }
            DictionaryProperty property7 = property as DictionaryProperty;
            if (property7 != null)
            {
                this.parseDictionaryProperty(property7);
                return property7;
            }
            CollectionProperty property8 = property as CollectionProperty;
            if (property8 != null)
            {
                this.parseCollectionProperty(property8);
                return property8;
            }
            ComplexProperty property9 = property as ComplexProperty;
            if (property9 != null)
            {
                this.parseComplexProperty(property9);
                return property9;
            }
            return property;
        }

        public Property Deserialize()
        {
            PropertyArt propertyArt = getPropertyArtFromString(this._reader.ReadElement());
            if (propertyArt == PropertyArt.Unknown)
            {
                return null;
            }
            return this.deserialize(propertyArt, null);
        }

        private static PropertyArt getPropertyArtFromString(string name)
        {
            if (name == "Simple")
            {
                return PropertyArt.Simple;
            }
            if (name == "Complex")
            {
                return PropertyArt.Complex;
            }
            if (name == "Collection")
            {
                return PropertyArt.Collection;
            }
            if (name == "SingleArray")
            {
                return PropertyArt.SingleDimensionalArray;
            }
            if (name == "Null")
            {
                return PropertyArt.Null;
            }
            if (name == "Dictionary")
            {
                return PropertyArt.Dictionary;
            }
            if (name == "MultiArray")
            {
                return PropertyArt.MultiDimensionalArray;
            }
            if (name == "ComplexReference")
            {
                return PropertyArt.Reference;
            }
            if (name == "Reference")
            {
                return PropertyArt.Reference;
            }
            return PropertyArt.Unknown;
        }

        public void Open(Stream stream)
        {
            this._reader.Open(stream);
        }

        private void parseCollectionProperty(CollectionProperty property)
        {
            property.ElementType = (property.Type != null) ? TypeInfo.GetTypeInfo(property.Type).ElementType : null;
            foreach (string str in this._reader.ReadSubElements())
            {
                switch (str)
                {
                    case "Properties":
                        this.readProperties(property.Properties, property.Type);
                        break;

                    case "Items":
                        this.readItems(property.Items, property.ElementType);
                        break;
                }
            }
        }

        private void parseComplexProperty(ComplexProperty property)
        {
            foreach (string str in this._reader.ReadSubElements())
            {
                if (str == "Properties")
                {
                    this.readProperties(property.Properties, property.Type);
                }
            }
        }

        private void parseDictionaryProperty(DictionaryProperty property)
        {
            if (property.Type != null)
            {
                TypeInfo typeInfo = TypeInfo.GetTypeInfo(property.Type);
                property.KeyType = typeInfo.KeyType;
                property.ValueType = typeInfo.ElementType;
            }
            foreach (string str in this._reader.ReadSubElements())
            {
                switch (str)
                {
                    case "Properties":
                        this.readProperties(property.Properties, property.Type);
                        break;

                    case "Items":
                        this.readDictionaryItems(property.Items, property.KeyType, property.ValueType);
                        break;
                }
            }
        }

        private void parseMultiDimensionalArrayProperty(MultiDimensionalArrayProperty property)
        {
            property.ElementType = (property.Type != null) ? TypeInfo.GetTypeInfo(property.Type).ElementType : null;
            foreach (string str in this._reader.ReadSubElements())
            {
                switch (str)
                {
                    case "Dimensions":
                        this.readDimensionInfos(property.DimensionInfos);
                        break;

                    case "Items":
                        this.readMultiDimensionalArrayItems(property.Items, property.ElementType);
                        break;
                }
            }
        }

        private void parseSimpleProperty(IXmlReader reader, SimpleProperty property)
        {
            property.Value = this._reader.GetAttributeAsObject("value", property.Type);
        }

        private void parseSingleDimensionalArrayProperty(SingleDimensionalArrayProperty property)
        {
            property.ElementType = (property.Type != null) ? TypeInfo.GetTypeInfo(property.Type).ElementType : null;
            property.LowerBound = this._reader.GetAttributeAsInt("lowerBound");
            foreach (string str in this._reader.ReadSubElements())
            {
                if (str == "Items")
                {
                    this.readItems(property.Items, property.ElementType);
                }
            }
        }

        private void readDictionaryItem(IList<KeyValueItem> items, Type expectedKeyType, Type expectedValueType)
        {
            Property key = null;
            Property property2 = null;
            foreach (string str in this._reader.ReadSubElements())
            {
                if ((key != null) && (property2 != null))
                {
                    break;
                }
                PropertyArt propertyArt = getPropertyArtFromString(str);
                if (propertyArt != PropertyArt.Unknown)
                {
                    if (key == null)
                    {
                        key = this.deserialize(propertyArt, expectedKeyType);
                    }
                    else
                    {
                        property2 = this.deserialize(propertyArt, expectedValueType);
                    }
                }
            }
            KeyValueItem item = new KeyValueItem(key, property2);
            items.Add(item);
        }

        private void readDictionaryItems(IList<KeyValueItem> items, Type expectedKeyType, Type expectedValueType)
        {
            foreach (string str in this._reader.ReadSubElements())
            {
                if (str == "Item")
                {
                    this.readDictionaryItem(items, expectedKeyType, expectedValueType);
                }
            }
        }

        private void readDimensionInfo(IList<DimensionInfo> dimensionInfos)
        {
            DimensionInfo item = new DimensionInfo();
            item.Length = this._reader.GetAttributeAsInt("length");
            item.LowerBound = this._reader.GetAttributeAsInt("lowerBound");
            dimensionInfos.Add(item);
        }

        private void readDimensionInfos(IList<DimensionInfo> dimensionInfos)
        {
            foreach (string str in this._reader.ReadSubElements())
            {
                if (str == "Dimension")
                {
                    this.readDimensionInfo(dimensionInfos);
                }
            }
        }

        private void readItems(ICollection<Property> items, Type expectedElementType)
        {
            foreach (string str in this._reader.ReadSubElements())
            {
                PropertyArt propertyArt = getPropertyArtFromString(str);
                if (propertyArt != PropertyArt.Unknown)
                {
                    Property item = this.deserialize(propertyArt, expectedElementType);
                    items.Add(item);
                }
            }
        }

        private void readMultiDimensionalArrayItem(IList<MultiDimensionalArrayItem> items, Type expectedElementType)
        {
            int[] attributeAsArrayOfInt = this._reader.GetAttributeAsArrayOfInt("indexes");
            foreach (string str in this._reader.ReadSubElements())
            {
                PropertyArt propertyArt = getPropertyArtFromString(str);
                if (propertyArt != PropertyArt.Unknown)
                {
                    Property property = this.deserialize(propertyArt, expectedElementType);
                    MultiDimensionalArrayItem item = new MultiDimensionalArrayItem(attributeAsArrayOfInt, property);
                    items.Add(item);
                }
            }
        }

        private void readMultiDimensionalArrayItems(IList<MultiDimensionalArrayItem> items, Type expectedElementType)
        {
            foreach (string str in this._reader.ReadSubElements())
            {
                if (str == "Item")
                {
                    this.readMultiDimensionalArrayItem(items, expectedElementType);
                }
            }
        }

        private void readProperties(PropertyCollection properties, Type ownerType)
        {
            foreach (string str in this._reader.ReadSubElements())
            {
                PropertyArt propertyArt = getPropertyArtFromString(str);
                if (propertyArt != PropertyArt.Unknown)
                {
                    string attributeAsString = this._reader.GetAttributeAsString("name");
                    if (!string.IsNullOrEmpty(attributeAsString))
                    {
                        PropertyInfo info = ownerType.GetProperty(attributeAsString);
                        if (info != null)
                        {
                            Property item = this.deserialize(propertyArt, info.PropertyType);
                            properties.Add(item);
                        }
                    }
                }
            }
        }
    }
}

