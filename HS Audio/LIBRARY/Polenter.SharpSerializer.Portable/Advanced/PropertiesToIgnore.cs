﻿namespace Polenter.Serialization.Advanced
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Runtime.CompilerServices;

    public sealed class PropertiesToIgnore
    {
        private readonly TypePropertiesToIgnoreCollection _propertiesToIgnore = new TypePropertiesToIgnoreCollection();

        public void Add(Type type, string propertyName)
        {
            TypePropertiesToIgnore ignore = this.getPropertiesToIgnore(type);
            if (!ignore.PropertyNames.Contains(propertyName))
            {
                ignore.PropertyNames.Add(propertyName);
            }
        }

        public bool Contains(Type type, string propertyName)
        {
            return this._propertiesToIgnore.ContainsProperty(type, propertyName);
        }

        private TypePropertiesToIgnore getPropertiesToIgnore(Type type)
        {
            TypePropertiesToIgnore item = this._propertiesToIgnore.TryFind(type);
            if (item == null)
            {
                item = new TypePropertiesToIgnore(type);
                this._propertiesToIgnore.Add(item);
            }
            return item;
        }

        private sealed class TypePropertiesToIgnore
        {
            private IList<string> _propertyNames;
            [CompilerGenerated]
            private System.Type _Type;

            public TypePropertiesToIgnore(System.Type type)
            {
                this.Type = type;
            }

            public IList<string> PropertyNames
            {
                get
                {
                    if (this._propertyNames == null)
                    {
                        this._propertyNames = new List<string>();
                    }
                    return this._propertyNames;
                }
                set
                {
                    this._propertyNames = value;
                }
            }

            public System.Type Type
            {
                [CompilerGenerated]
                get
                {
                    return this._Type;
                }
                [CompilerGenerated]
                set
                {
                    this._Type = value;
                }
            }
        }

        private sealed class TypePropertiesToIgnoreCollection : KeyedCollection<Type, PropertiesToIgnore.TypePropertiesToIgnore>
        {
            public bool ContainsProperty(Type type, string propertyName)
            {
                PropertiesToIgnore.TypePropertiesToIgnore ignore = this.TryFind(type);
                if (ignore == null)
                {
                    return false;
                }
                return ignore.PropertyNames.Contains(propertyName);
            }

            protected override Type GetKeyForItem(PropertiesToIgnore.TypePropertiesToIgnore item)
            {
                return item.Type;
            }

            public int IndexOf(Type type)
            {
                for (int i = 0; i < base.Count; i++)
                {
                    PropertiesToIgnore.TypePropertiesToIgnore ignore = base[i];
                    if (ignore.Type == type)
                    {
                        return i;
                    }
                }
                return -1;
            }

            public PropertiesToIgnore.TypePropertiesToIgnore TryFind(Type type)
            {
                foreach (PropertiesToIgnore.TypePropertiesToIgnore ignore in base.Items)
                {
                    if (ignore.Type == type)
                    {
                        return ignore;
                    }
                }
                return null;
            }
        }
    }
}

