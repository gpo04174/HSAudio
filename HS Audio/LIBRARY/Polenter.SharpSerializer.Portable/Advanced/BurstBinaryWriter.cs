﻿namespace Polenter.Serialization.Advanced
{
    using Polenter.Serialization.Advanced.Binary;
    using Polenter.Serialization.Advanced.Serializing;
    using Polenter.Serialization.Core.Binary;
    using System;
    using System.IO;
    using System.Text;

    public sealed class BurstBinaryWriter : IBinaryWriter
    {
        private readonly Encoding _encoding;
        private readonly ITypeNameConverter _typeNameConverter;
        private BinaryWriter _writer;

        public BurstBinaryWriter(ITypeNameConverter typeNameConverter, Encoding encoding)
        {
            if (typeNameConverter == null)
            {
                throw new ArgumentNullException("typeNameConverter");
            }
            if (encoding == null)
            {
                throw new ArgumentNullException("encoding");
            }
            this._encoding = encoding;
            this._typeNameConverter = typeNameConverter;
        }

        public void Close()
        {
            this._writer.Flush();
        }

        public void Open(Stream stream)
        {
            this._writer = new BinaryWriter(stream, this._encoding);
        }

        public void WriteElementId(byte id)
        {
            this._writer.Write(id);
        }

        public void WriteName(string name)
        {
            BinaryWriterTools.WriteString(name, this._writer);
        }

        public void WriteNumber(int number)
        {
            BinaryWriterTools.WriteNumber(number, this._writer);
        }

        public void WriteNumbers(int[] numbers)
        {
            BinaryWriterTools.WriteNumbers(numbers, this._writer);
        }

        public void WriteType(Type type)
        {
            if (type == null)
            {
                this._writer.Write(false);
            }
            else
            {
                this._writer.Write(true);
                this._writer.Write(this._typeNameConverter.ConvertToTypeName(type));
            }
        }

        public void WriteValue(object value)
        {
            BinaryWriterTools.WriteValue(value, this._writer);
        }
    }
}

