﻿namespace Polenter.Serialization.Advanced.Deserializing
{
    using Polenter.Serialization.Core;
    using System;
    using System.IO;

    public interface IPropertyDeserializer
    {
        void Close();
        Property Deserialize();
        void Open(Stream stream);
    }
}

