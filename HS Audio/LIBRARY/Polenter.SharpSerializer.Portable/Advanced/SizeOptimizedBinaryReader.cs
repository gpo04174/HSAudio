﻿namespace Polenter.Serialization.Advanced
{
    using Polenter.Serialization.Advanced.Binary;
    using Polenter.Serialization.Advanced.Serializing;
    using Polenter.Serialization.Core.Binary;
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Runtime.CompilerServices;
    using System.Text;

    public sealed class SizeOptimizedBinaryReader : IBinaryReader
    {
        private readonly Encoding _encoding;
        private readonly IList<string> _names = new List<string>();
        private BinaryReader _reader;
        private readonly ITypeNameConverter _typeNameConverter;
        private readonly IList<Type> _types = new List<Type>();

        public SizeOptimizedBinaryReader(ITypeNameConverter typeNameConverter, Encoding encoding)
        {
            if (typeNameConverter == null)
            {
                throw new ArgumentNullException("typeNameConverter");
            }
            if (encoding == null)
            {
                throw new ArgumentNullException("encoding");
            }
            this._typeNameConverter = typeNameConverter;
            this._encoding = encoding;
        }

        public void Close()
        {
        }

        public void Open(Stream stream)
        {
            this._reader = new BinaryReader(stream, this._encoding);
            this._names.Clear();
            readHeader<string>(this._reader, this._names, delegate (string text) {
                return text;
            });
            this._types.Clear();
            readHeader<Type>(this._reader, this._types, new HeaderCallback<Type>(this._typeNameConverter.ConvertToType));
        }

        public byte ReadElementId()
        {
            return this._reader.ReadByte();
        }

        private static void readHeader<T>(BinaryReader reader, IList<T> items, HeaderCallback<T> readCallback)
        {
            int num = BinaryReaderTools.ReadNumber(reader);
            for (int i = 0; i < num; i++)
            {
                string text = BinaryReaderTools.ReadString(reader);
                T item = readCallback(text);
                items.Add(item);
            }
        }

        public string ReadName()
        {
            int num = BinaryReaderTools.ReadNumber(this._reader);
            return this._names[num];
        }

        public int ReadNumber()
        {
            return BinaryReaderTools.ReadNumber(this._reader);
        }

        public int[] ReadNumbers()
        {
            return BinaryReaderTools.ReadNumbers(this._reader);
        }

        public Type ReadType()
        {
            int num = BinaryReaderTools.ReadNumber(this._reader);
            return this._types[num];
        }

        public object ReadValue(Type expectedType)
        {
            return BinaryReaderTools.ReadValue(expectedType, this._reader);
        }

        private delegate T HeaderCallback<T>(string text);
    }
}

