﻿namespace Polenter.Serialization.Advanced
{
    using Polenter.Serialization.Advanced.Serializing;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Text.RegularExpressions;

    public sealed class TypeNameConverter : ITypeNameConverter
    {
        private readonly Dictionary<Type, string> _cache;
        [CompilerGenerated]
        private bool _IncludeAssemblyVersion;
        [CompilerGenerated]
        private bool _IncludeCulture;
        [CompilerGenerated]
        private bool _IncludePublicKeyToken;

        public TypeNameConverter()
        {
            this._cache = new Dictionary<Type, string>();
        }

        public TypeNameConverter(bool includeAssemblyVersion, bool includeCulture, bool includePublicKeyToken)
        {
            this._cache = new Dictionary<Type, string>();
            this.IncludeAssemblyVersion = includeAssemblyVersion;
            this.IncludeCulture = includeCulture;
            this.IncludePublicKeyToken = includePublicKeyToken;
        }

        public Type ConvertToType(string typeName)
        {
            if (string.IsNullOrEmpty(typeName))
            {
                return null;
            }
            return Type.GetType(typeName, true);
        }

        public string ConvertToTypeName(Type type)
        {
            if (type == null)
            {
                return string.Empty;
            }
            if (this._cache.ContainsKey(type))
            {
                return this._cache[type];
            }
            string assemblyQualifiedName = type.AssemblyQualifiedName;
            if (!this.IncludeAssemblyVersion)
            {
                assemblyQualifiedName = removeAssemblyVersion(assemblyQualifiedName);
            }
            if (!this.IncludeCulture)
            {
                assemblyQualifiedName = removeCulture(assemblyQualifiedName);
            }
            if (!this.IncludePublicKeyToken)
            {
                assemblyQualifiedName = removePublicKeyToken(assemblyQualifiedName);
            }
            this._cache.Add(type, assemblyQualifiedName);
            return assemblyQualifiedName;
        }

        private static string removeAssemblyVersion(string typename)
        {
            return Regex.Replace(typename, @", Version=\d+.\d+.\d+.\d+", string.Empty);
        }

        private static string removeCulture(string typename)
        {
            return Regex.Replace(typename, @", Culture=\w+", string.Empty);
        }

        private static string removePublicKeyToken(string typename)
        {
            return Regex.Replace(typename, @", PublicKeyToken=\w+", string.Empty);
        }

        public bool IncludeAssemblyVersion
        {
            [CompilerGenerated]
            get
            {
                return this._IncludeAssemblyVersion;
            }
            [CompilerGenerated]
            private set
            {
                this._IncludeAssemblyVersion = value;
            }
        }

        public bool IncludeCulture
        {
            [CompilerGenerated]
            get
            {
                return this._IncludeCulture;
            }
            [CompilerGenerated]
            private set
            {
                this._IncludeCulture = value;
            }
        }

        public bool IncludePublicKeyToken
        {
            [CompilerGenerated]
            get
            {
                return this._IncludePublicKeyToken;
            }
            [CompilerGenerated]
            private set
            {
                this._IncludePublicKeyToken = value;
            }
        }
    }
}

