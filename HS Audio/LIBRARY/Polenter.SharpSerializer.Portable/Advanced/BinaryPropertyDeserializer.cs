﻿namespace Polenter.Serialization.Advanced
{
    using Polenter.Serialization.Advanced.Binary;
    using Polenter.Serialization.Advanced.Deserializing;
    using Polenter.Serialization.Core;
    using Polenter.Serialization.Core.Binary;
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Reflection;

    public sealed class BinaryPropertyDeserializer : IPropertyDeserializer
    {
        private readonly Dictionary<int, ReferenceTargetProperty> _propertyCache = new Dictionary<int, ReferenceTargetProperty>();
        private readonly IBinaryReader _reader;

        public BinaryPropertyDeserializer(IBinaryReader reader)
        {
            if (reader == null)
            {
                throw new ArgumentNullException("reader");
            }
            this._reader = reader;
        }

        public void Close()
        {
            this._reader.Close();
        }

        private static Property createProperty(byte elementId, string propertyName, Type propertyType)
        {
            switch (elementId)
            {
                case 1:
                case 10:
                    return new CollectionProperty(propertyName, propertyType);

                case 2:
                case 8:
                    return new ComplexProperty(propertyName, propertyType);

                case 3:
                case 11:
                    return new DictionaryProperty(propertyName, propertyType);

                case 4:
                case 13:
                    return new MultiDimensionalArrayProperty(propertyName, propertyType);

                case 5:
                    return new NullProperty(propertyName);

                case 6:
                    return new SimpleProperty(propertyName, propertyType);

                case 7:
                case 12:
                    return new SingleDimensionalArrayProperty(propertyName, propertyType);
            }
            return null;
        }

        private Property createProperty(int referenceId, string propertyName, Type propertyType)
        {
            ReferenceTargetProperty source = this._propertyCache[referenceId];
            ReferenceTargetProperty property2 = (ReferenceTargetProperty) Property.CreateInstance(source.Art, propertyName, propertyType);
            ReferenceInfo reference = source.Reference;
            reference.Count++;
            property2.MakeFlatCopyFrom(source);
            ReferenceInfo info = new ReferenceInfo();
            info.Id = referenceId;
            property2.Reference = info;
            return property2;
        }

        private Property deserialize(byte elementId, Type expectedType)
        {
            string propertyName = this._reader.ReadName();
            return this.deserialize(elementId, propertyName, expectedType);
        }

        private Property deserialize(byte elementId, string propertyName, Type expectedType)
        {
            Type propertyType = this._reader.ReadType();
            if (propertyType == null)
            {
                propertyType = expectedType;
            }
            int referenceId = 0;
            if ((elementId == 9) || Elements.IsElementWithId(elementId))
            {
                referenceId = this._reader.ReadNumber();
                if (elementId == 9)
                {
                    return this.createProperty(referenceId, propertyName, propertyType);
                }
            }
            Property property = createProperty(elementId, propertyName, propertyType);
            if (property == null)
            {
                return null;
            }
            NullProperty property2 = property as NullProperty;
            if (property2 != null)
            {
                return property2;
            }
            SimpleProperty property3 = property as SimpleProperty;
            if (property3 != null)
            {
                this.parseSimpleProperty(property3);
                return property3;
            }
            ReferenceTargetProperty property4 = property as ReferenceTargetProperty;
            if ((property4 != null) && (referenceId > 0))
            {
                property4.Reference = new ReferenceInfo();
                property4.Reference.Id = referenceId;
                property4.Reference.IsProcessed = true;
                this._propertyCache.Add(referenceId, property4);
            }
            MultiDimensionalArrayProperty property5 = property as MultiDimensionalArrayProperty;
            if (property5 != null)
            {
                this.parseMultiDimensionalArrayProperty(property5);
                return property5;
            }
            SingleDimensionalArrayProperty property6 = property as SingleDimensionalArrayProperty;
            if (property6 != null)
            {
                this.parseSingleDimensionalArrayProperty(property6);
                return property6;
            }
            DictionaryProperty property7 = property as DictionaryProperty;
            if (property7 != null)
            {
                this.parseDictionaryProperty(property7);
                return property7;
            }
            CollectionProperty property8 = property as CollectionProperty;
            if (property8 != null)
            {
                this.parseCollectionProperty(property8);
                return property8;
            }
            ComplexProperty property9 = property as ComplexProperty;
            if (property9 != null)
            {
                this.parseComplexProperty(property9);
                return property9;
            }
            return property;
        }

        public Property Deserialize()
        {
            byte elementId = this._reader.ReadElementId();
            return this.deserialize(elementId, null);
        }

        public void Open(Stream stream)
        {
            this._reader.Open(stream);
        }

        private void parseCollectionProperty(CollectionProperty property)
        {
            property.ElementType = this._reader.ReadType();
            this.readProperties(property.Properties, property.Type);
            this.readItems(property.Items, property.ElementType);
        }

        private void parseComplexProperty(ComplexProperty property)
        {
            this.readProperties(property.Properties, property.Type);
        }

        private void parseDictionaryProperty(DictionaryProperty property)
        {
            property.KeyType = this._reader.ReadType();
            property.ValueType = this._reader.ReadType();
            this.readProperties(property.Properties, property.Type);
            this.readDictionaryItems(property.Items, property.KeyType, property.ValueType);
        }

        private void parseMultiDimensionalArrayProperty(MultiDimensionalArrayProperty property)
        {
            property.ElementType = this._reader.ReadType();
            this.readDimensionInfos(property.DimensionInfos);
            this.readMultiDimensionalArrayItems(property.Items, property.ElementType);
        }

        private void parseSimpleProperty(SimpleProperty property)
        {
            property.Value = this._reader.ReadValue(property.Type);
        }

        private void parseSingleDimensionalArrayProperty(SingleDimensionalArrayProperty property)
        {
            property.ElementType = this._reader.ReadType();
            property.LowerBound = this._reader.ReadNumber();
            this.readItems(property.Items, property.ElementType);
        }

        private void readDictionaryItem(IList<KeyValueItem> items, Type expectedKeyType, Type expectedValueType)
        {
            byte elementId = this._reader.ReadElementId();
            Property key = this.deserialize(elementId, expectedKeyType);
            elementId = this._reader.ReadElementId();
            Property property2 = this.deserialize(elementId, expectedValueType);
            KeyValueItem item = new KeyValueItem(key, property2);
            items.Add(item);
        }

        private void readDictionaryItems(IList<KeyValueItem> items, Type expectedKeyType, Type expectedValueType)
        {
            int num = this._reader.ReadNumber();
            for (int i = 0; i < num; i++)
            {
                this.readDictionaryItem(items, expectedKeyType, expectedValueType);
            }
        }

        private void readDimensionInfo(IList<DimensionInfo> dimensionInfos)
        {
            DimensionInfo item = new DimensionInfo();
            item.Length = this._reader.ReadNumber();
            item.LowerBound = this._reader.ReadNumber();
            dimensionInfos.Add(item);
        }

        private void readDimensionInfos(IList<DimensionInfo> dimensionInfos)
        {
            int num = this._reader.ReadNumber();
            for (int i = 0; i < num; i++)
            {
                this.readDimensionInfo(dimensionInfos);
            }
        }

        private void readItems(ICollection<Property> items, Type expectedElementType)
        {
            int num = this._reader.ReadNumber();
            for (int i = 0; i < num; i++)
            {
                byte elementId = this._reader.ReadElementId();
                Property item = this.deserialize(elementId, expectedElementType);
                items.Add(item);
            }
        }

        private void readMultiDimensionalArrayItem(IList<MultiDimensionalArrayItem> items, Type expectedElementType)
        {
            int[] indexes = this._reader.ReadNumbers();
            byte elementId = this._reader.ReadElementId();
            Property property = this.deserialize(elementId, expectedElementType);
            MultiDimensionalArrayItem item = new MultiDimensionalArrayItem(indexes, property);
            items.Add(item);
        }

        private void readMultiDimensionalArrayItems(IList<MultiDimensionalArrayItem> items, Type expectedElementType)
        {
            int num = this._reader.ReadNumber();
            for (int i = 0; i < num; i++)
            {
                this.readMultiDimensionalArrayItem(items, expectedElementType);
            }
        }

        private void readProperties(PropertyCollection properties, Type ownerType)
        {
            int num = this._reader.ReadNumber();
            for (int i = 0; i < num; i++)
            {
                byte elementId = this._reader.ReadElementId();
                string name = this._reader.ReadName();
                PropertyInfo info = ownerType.GetProperty(name);
                Type expectedType = (info != null) ? info.PropertyType : null;
                Property item = this.deserialize(elementId, name, expectedType);
                properties.Add(item);
            }
        }
    }
}

