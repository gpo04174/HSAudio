﻿namespace Polenter.Serialization.Advanced
{
    using Polenter.Serialization.Advanced.Binary;
    using Polenter.Serialization.Advanced.Serializing;
    using Polenter.Serialization.Core.Binary;
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Runtime.CompilerServices;
    using System.Text;

    public sealed class SizeOptimizedBinaryWriter : IBinaryWriter
    {
        private List<WriteCommand> _cache;
        private readonly Encoding _encoding;
        private IndexGenerator<string> _names;
        private Stream _stream;
        private readonly ITypeNameConverter _typeNameConverter;
        private IndexGenerator<Type> _types;

        public SizeOptimizedBinaryWriter(ITypeNameConverter typeNameConverter, Encoding encoding)
        {
            if (typeNameConverter == null)
            {
                throw new ArgumentNullException("typeNameConverter");
            }
            if (encoding == null)
            {
                throw new ArgumentNullException("encoding");
            }
            this._encoding = encoding;
            this._typeNameConverter = typeNameConverter;
        }

        public void Close()
        {
            BinaryWriter writer = new BinaryWriter(this._stream, this._encoding);
            this.writeNamesHeader(writer);
            this.writeTypesHeader(writer);
            writeCache(this._cache, writer);
            writer.Flush();
        }

        public void Open(Stream stream)
        {
            this._stream = stream;
            this._cache = new List<WriteCommand>();
            this._types = new IndexGenerator<Type>();
            this._names = new IndexGenerator<string>();
        }

        private static void writeCache(List<WriteCommand> cache, BinaryWriter writer)
        {
            foreach (WriteCommand command in cache)
            {
                command.Write(writer);
            }
        }

        public void WriteElementId(byte id)
        {
            this._cache.Add(new ByteWriteCommand(id));
        }

        public void WriteName(string name)
        {
            int indexOfItem = this._names.GetIndexOfItem(name);
            this._cache.Add(new NumberWriteCommand(indexOfItem));
        }

        private void writeNamesHeader(BinaryWriter writer)
        {
            BinaryWriterTools.WriteNumber(this._names.Items.Count, writer);
            foreach (string str in this._names.Items)
            {
                BinaryWriterTools.WriteString(str, writer);
            }
        }

        public void WriteNumber(int number)
        {
            this._cache.Add(new NumberWriteCommand(number));
        }

        public void WriteNumbers(int[] numbers)
        {
            this._cache.Add(new NumbersWriteCommand(numbers));
        }

        public void WriteType(Type type)
        {
            int indexOfItem = this._types.GetIndexOfItem(type);
            this._cache.Add(new NumberWriteCommand(indexOfItem));
        }

        private void writeTypesHeader(BinaryWriter writer)
        {
            BinaryWriterTools.WriteNumber(this._types.Items.Count, writer);
            foreach (Type type in this._types.Items)
            {
                BinaryWriterTools.WriteString(this._typeNameConverter.ConvertToTypeName(type), writer);
            }
        }

        public void WriteValue(object value)
        {
            this._cache.Add(new ValueWriteCommand(value));
        }

        private sealed class ByteWriteCommand : SizeOptimizedBinaryWriter.WriteCommand
        {
            [CompilerGenerated]
            private byte _Data;

            public ByteWriteCommand(byte data)
            {
                this.Data = data;
            }

            public override void Write(BinaryWriter writer)
            {
                writer.Write(this.Data);
            }

            public byte Data
            {
                [CompilerGenerated]
                get
                {
                    return this._Data;
                }
                [CompilerGenerated]
                set
                {
                    this._Data = value;
                }
            }
        }

        private sealed class NumbersWriteCommand : SizeOptimizedBinaryWriter.WriteCommand
        {
            [CompilerGenerated]
            private int[] _Data;

            public NumbersWriteCommand(int[] data)
            {
                this.Data = data;
            }

            public override void Write(BinaryWriter writer)
            {
                BinaryWriterTools.WriteNumbers(this.Data, writer);
            }

            public int[] Data
            {
                [CompilerGenerated]
                get
                {
                    return this._Data;
                }
                [CompilerGenerated]
                set
                {
                    this._Data = value;
                }
            }
        }

        private sealed class NumberWriteCommand : SizeOptimizedBinaryWriter.WriteCommand
        {
            [CompilerGenerated]
            private int _Data;

            public NumberWriteCommand(int data)
            {
                this.Data = data;
            }

            public override void Write(BinaryWriter writer)
            {
                BinaryWriterTools.WriteNumber(this.Data, writer);
            }

            public int Data
            {
                [CompilerGenerated]
                get
                {
                    return this._Data;
                }
                [CompilerGenerated]
                set
                {
                    this._Data = value;
                }
            }
        }

        private sealed class ValueWriteCommand : SizeOptimizedBinaryWriter.WriteCommand
        {
            [CompilerGenerated]
            private object _Data;

            public ValueWriteCommand(object data)
            {
                this.Data = data;
            }

            public override void Write(BinaryWriter writer)
            {
                BinaryWriterTools.WriteValue(this.Data, writer);
            }

            public object Data
            {
                [CompilerGenerated]
                get
                {
                    return this._Data;
                }
                [CompilerGenerated]
                set
                {
                    this._Data = value;
                }
            }
        }

        private abstract class WriteCommand
        {
            protected WriteCommand()
            {
            }

            public abstract void Write(BinaryWriter writer);
        }
    }
}

