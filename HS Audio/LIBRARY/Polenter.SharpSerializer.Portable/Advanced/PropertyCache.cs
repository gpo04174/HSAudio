﻿namespace Polenter.Serialization.Advanced
{
    using System;
    using System.Collections.Generic;
    using System.Reflection;

    internal class PropertyCache
    {
        private readonly Dictionary<Type, IList<PropertyInfo>> _cache = new Dictionary<Type, IList<PropertyInfo>>();

        public void Add(Type key, IList<PropertyInfo> value)
        {
            lock (this._cache)
            {
                if (!this._cache.ContainsKey(key))
                {
                    this._cache.Add(key, value);
                }
            }
        }

        public IList<PropertyInfo> TryGetPropertyInfos(Type type)
        {
            lock (this._cache)
            {
                if (!this._cache.ContainsKey(type))
                {
                    return null;
                }
                return this._cache[type];
            }
        }
    }
}

