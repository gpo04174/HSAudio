﻿namespace Polenter.Serialization.Advanced
{
    using Polenter.Serialization.Advanced.Serializing;
    using Polenter.Serialization.Advanced.Xml;
    using System;
    using System.IO;
    using System.Text;
    using System.Xml;

    public sealed class DefaultXmlWriter : IXmlWriter
    {
        private readonly XmlWriterSettings _settings;
        private readonly ISimpleValueConverter _simpleValueConverter;
        private readonly ITypeNameConverter _typeNameProvider;
        private XmlWriter _writer;

        public DefaultXmlWriter(ITypeNameConverter typeNameProvider, ISimpleValueConverter simpleValueConverter, XmlWriterSettings settings)
        {
            if (typeNameProvider == null)
            {
                throw new ArgumentNullException("typeNameProvider");
            }
            if (simpleValueConverter == null)
            {
                throw new ArgumentNullException("simpleValueConverter");
            }
            if (settings == null)
            {
                throw new ArgumentNullException("settings");
            }
            this._simpleValueConverter = simpleValueConverter;
            this._settings = settings;
            this._typeNameProvider = typeNameProvider;
        }

        public void Close()
        {
            this._writer.WriteEndDocument();
            this._writer.Close();//Dispose();
        }

        private static string getArrayOfIntAsText(int[] values)
        {
            if (values.Length == 0)
            {
                return string.Empty;
            }
            StringBuilder builder = new StringBuilder();
            foreach (int num in values)
            {
                builder.Append(num.ToString());
                builder.Append(",");
            }
            return builder.ToString().TrimEnd(new char[] { ',' });
        }

        public void Open(Stream stream)
        {
            this._writer = XmlWriter.Create(stream, this._settings);
            this._writer.WriteStartDocument(true);
        }

        public void WriteAttribute(string attributeId, int number)
        {
            this._writer.WriteAttributeString(attributeId, number.ToString());
        }

        public void WriteAttribute(string attributeId, object value)
        {
            if (value != null)
            {
                string str = this._simpleValueConverter.ConvertToString(value);
                this._writer.WriteAttributeString(attributeId, str);
            }
        }

        public void WriteAttribute(string attributeId, string text)
        {
            if (text != null)
            {
                this._writer.WriteAttributeString(attributeId, text);
            }
        }

        public void WriteAttribute(string attributeId, Type type)
        {
            if (type != null)
            {
                string text = this._typeNameProvider.ConvertToTypeName(type);
                this.WriteAttribute(attributeId, text);
            }
        }

        public void WriteAttribute(string attributeId, int[] numbers)
        {
            string str = getArrayOfIntAsText(numbers);
            this._writer.WriteAttributeString(attributeId, str);
        }

        public void WriteEndElement()
        {
            this._writer.WriteEndElement();
        }

        public void WriteStartElement(string elementId)
        {
            this._writer.WriteStartElement(elementId);
        }
    }
}

