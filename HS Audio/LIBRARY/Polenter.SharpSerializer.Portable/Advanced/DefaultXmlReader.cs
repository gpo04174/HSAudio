﻿namespace Polenter.Serialization.Advanced
{
    using Polenter.Serialization.Advanced.Serializing;
    using Polenter.Serialization.Advanced.Xml;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.IO;
    using System.Runtime.CompilerServices;
    using System.Threading;
    using System.Xml;

    public sealed class DefaultXmlReader : IXmlReader
    {
        private XmlReader _currentReader;
        private Stack<XmlReader> _readerStack;
        private readonly XmlReaderSettings _settings;
        private readonly ITypeNameConverter _typeNameConverter;
        private readonly ISimpleValueConverter _valueConverter;

        public DefaultXmlReader(ITypeNameConverter typeNameConverter, ISimpleValueConverter valueConverter, XmlReaderSettings settings)
        {
            if (typeNameConverter == null)
            {
                throw new ArgumentNullException("typeNameConverter");
            }
            if (valueConverter == null)
            {
                throw new ArgumentNullException("valueConverter");
            }
            if (settings == null)
            {
                throw new ArgumentNullException("settings");
            }
            this._typeNameConverter = typeNameConverter;
            this._valueConverter = valueConverter;
            this._settings = settings;
        }

        public void Close()
        {
            this._currentReader.Close();//Dispose();
        }

        private static int[] getArrayOfIntFromText(string text)
        {
            if (string.IsNullOrEmpty(text))
            {
                return null;
            }
            string[] strArray = text.Split(new char[] { ',' });
            if (strArray.Length == 0)
            {
                return null;
            }
            List<int> list = new List<int>();
            foreach (string str in strArray)
            {
                int item = int.Parse(str);
                list.Add(item);
            }
            return list.ToArray();
        }

        public int[] GetAttributeAsArrayOfInt(string attributeName)
        {
            if (!this._currentReader.MoveToAttribute(attributeName))
            {
                return null;
            }
            return getArrayOfIntFromText(this._currentReader.Value);
        }

        public int GetAttributeAsInt(string attributeName)
        {
            if (!this._currentReader.MoveToAttribute(attributeName))
            {
                return 0;
            }
            return this._currentReader.ReadContentAsInt();
        }

        public object GetAttributeAsObject(string attributeName, Type expectedType)
        {
            string attributeAsString = this.GetAttributeAsString(attributeName);
            return this._valueConverter.ConvertFromString(attributeAsString, expectedType);
        }

        public string GetAttributeAsString(string attributeName)
        {
            if (!this._currentReader.MoveToAttribute(attributeName))
            {
                return null;
            }
            return this._currentReader.Value;
        }

        public Type GetAttributeAsType(string attributeName)
        {
            string attributeAsString = this.GetAttributeAsString(attributeName);
            return this._typeNameConverter.ConvertToType(attributeAsString);
        }

        public void Open(Stream stream)
        {
            this._readerStack = new Stack<XmlReader>();
            XmlReader reader = XmlReader.Create(stream, this._settings);
            this.pushCurrentReader(reader);
        }

        private void popCurrentReader()
        {
            if (this._readerStack.Count > 0)
            {
                this._readerStack.Pop();
            }
            if (this._readerStack.Count > 0)
            {
                this._currentReader = this._readerStack.Peek();
            }
            else
            {
                this._currentReader = null;
            }
        }

        private void pushCurrentReader(XmlReader reader)
        {
            this._readerStack.Push(reader);
            this._currentReader = reader;
        }

        public string ReadElement()
        {
            while (this._currentReader.Read())
            {
                if (this._currentReader.NodeType == XmlNodeType.Element)
                {
                    return this._currentReader.Name;
                }
            }
            return null;
        }

        public IEnumerable<string> ReadSubElements()
        {
            this._currentReader.MoveToElement();
            XmlReader reader = this._currentReader.ReadSubtree();
            reader.Read();
            this.pushCurrentReader(reader);
            for (string iteratorVariable1 = this.ReadElement(); !string.IsNullOrEmpty(iteratorVariable1); iteratorVariable1 = this.ReadElement())
            {
                yield return iteratorVariable1;
            }
        }

    }
}

