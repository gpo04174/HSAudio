﻿namespace Polenter.Serialization.Advanced.Binary
{
    using System;
    using System.IO;

    public interface IBinaryWriter
    {
        void Close();
        void Open(Stream stream);
        void WriteElementId(byte id);
        void WriteName(string name);
        void WriteNumber(int number);
        void WriteNumbers(int[] numbers);
        void WriteType(Type type);
        void WriteValue(object value);
    }
}

