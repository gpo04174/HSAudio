﻿namespace Polenter.Serialization.Advanced.Binary
{
    using System;
    using System.IO;

    public interface IBinaryReader
    {
        void Close();
        void Open(Stream stream);
        byte ReadElementId();
        string ReadName();
        int ReadNumber();
        int[] ReadNumbers();
        Type ReadType();
        object ReadValue(Type expectedType);
    }
}

