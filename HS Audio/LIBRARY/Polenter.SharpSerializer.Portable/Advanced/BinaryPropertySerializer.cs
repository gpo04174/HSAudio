﻿namespace Polenter.Serialization.Advanced
{
    using Polenter.Serialization.Advanced.Binary;
    using Polenter.Serialization.Advanced.Serializing;
    using Polenter.Serialization.Core;
    using Polenter.Serialization.Serializing;
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Reflection;

    public sealed class BinaryPropertySerializer : PropertySerializer
    {
        private readonly IBinaryWriter _writer;

        public BinaryPropertySerializer(IBinaryWriter writer)
        {
            if (writer == null)
            {
                throw new ArgumentNullException("writer");
            }
            this._writer = writer;
        }

        public override void Close()
        {
            this._writer.Close();
        }

        public override void Open(Stream stream)
        {
            this._writer.Open(stream);
        }

        protected override void SerializeCollectionProperty(PropertyTypeInfo<CollectionProperty> property)
        {
            if (!this.writePropertyHeaderWithReferenceId(10, property.Property.Reference, property.Name, property.ValueType))
            {
                this.writePropertyHeader(1, property.Name, property.ValueType);
            }
            this._writer.WriteType(property.Property.ElementType);
            this.writeProperties(property.Property.Properties, property.Property.Type);
            this.writeItems(property.Property.Items, property.Property.ElementType);
        }

        protected override void SerializeComplexProperty(PropertyTypeInfo<ComplexProperty> property)
        {
            if (!this.writePropertyHeaderWithReferenceId(8, property.Property.Reference, property.Name, property.ValueType))
            {
                this.writePropertyHeader(2, property.Name, property.ValueType);
            }
            this.writeProperties(property.Property.Properties, property.Property.Type);
        }

        protected override void SerializeDictionaryProperty(PropertyTypeInfo<DictionaryProperty> property)
        {
            if (!this.writePropertyHeaderWithReferenceId(11, property.Property.Reference, property.Name, property.ValueType))
            {
                this.writePropertyHeader(3, property.Name, property.ValueType);
            }
            this._writer.WriteType(property.Property.KeyType);
            this._writer.WriteType(property.Property.ValueType);
            this.writeProperties(property.Property.Properties, property.Property.Type);
            this.writeDictionaryItems(property.Property.Items, property.Property.KeyType, property.Property.ValueType);
        }

        protected override void SerializeMultiDimensionalArrayProperty(PropertyTypeInfo<MultiDimensionalArrayProperty> property)
        {
            if (!this.writePropertyHeaderWithReferenceId(13, property.Property.Reference, property.Name, property.ValueType))
            {
                this.writePropertyHeader(4, property.Name, property.ValueType);
            }
            this._writer.WriteType(property.Property.ElementType);
            this.writeDimensionInfos(property.Property.DimensionInfos);
            this.writeMultiDimensionalArrayItems(property.Property.Items, property.Property.ElementType);
        }

        protected override void SerializeNullProperty(PropertyTypeInfo<NullProperty> property)
        {
            this.writePropertyHeader(5, property.Name, property.ValueType);
        }

        protected override void SerializeReference(ReferenceTargetProperty referenceTarget)
        {
            this.writePropertyHeader(9, referenceTarget.Name, null);
            this._writer.WriteNumber(referenceTarget.Reference.Id);
        }

        protected override void SerializeSimpleProperty(PropertyTypeInfo<SimpleProperty> property)
        {
            this.writePropertyHeader(6, property.Name, property.ValueType);
            this._writer.WriteValue(property.Property.Value);
        }

        protected override void SerializeSingleDimensionalArrayProperty(PropertyTypeInfo<SingleDimensionalArrayProperty> property)
        {
            if (!this.writePropertyHeaderWithReferenceId(12, property.Property.Reference, property.Name, property.ValueType))
            {
                this.writePropertyHeader(7, property.Name, property.ValueType);
            }
            this._writer.WriteType(property.Property.ElementType);
            this._writer.WriteNumber(property.Property.LowerBound);
            this.writeItems(property.Property.Items, property.Property.ElementType);
        }

        private void writeDictionaryItem(KeyValueItem item, Type defaultKeyType, Type defaultValueType)
        {
            base.SerializeCore(new PropertyTypeInfo<Property>(item.Key, defaultKeyType));
            base.SerializeCore(new PropertyTypeInfo<Property>(item.Value, defaultValueType));
        }

        private void writeDictionaryItems(IList<KeyValueItem> items, Type defaultKeyType, Type defaultValueType)
        {
            this._writer.WriteNumber(items.Count);
            foreach (KeyValueItem item in items)
            {
                this.writeDictionaryItem(item, defaultKeyType, defaultValueType);
            }
        }

        private void writeDimensionInfo(DimensionInfo info)
        {
            this._writer.WriteNumber(info.Length);
            this._writer.WriteNumber(info.LowerBound);
        }

        private void writeDimensionInfos(IList<DimensionInfo> dimensionInfos)
        {
            this._writer.WriteNumber(dimensionInfos.Count);
            foreach (DimensionInfo info in dimensionInfos)
            {
                this.writeDimensionInfo(info);
            }
        }

        private void writeItems(ICollection<Property> items, Type defaultItemType)
        {
            this._writer.WriteNumber(items.Count);
            foreach (Property property in items)
            {
                base.SerializeCore(new PropertyTypeInfo<Property>(property, defaultItemType));
            }
        }

        private void writeMultiDimensionalArrayItem(MultiDimensionalArrayItem item, Type defaultItemType)
        {
            this._writer.WriteNumbers(item.Indexes);
            base.SerializeCore(new PropertyTypeInfo<Property>(item.Value, defaultItemType));
        }

        private void writeMultiDimensionalArrayItems(IList<MultiDimensionalArrayItem> items, Type defaultItemType)
        {
            this._writer.WriteNumber(items.Count);
            foreach (MultiDimensionalArrayItem item in items)
            {
                this.writeMultiDimensionalArrayItem(item, defaultItemType);
            }
        }

        private void writeProperties(PropertyCollection properties, Type ownerType)
        {
            this._writer.WriteNumber(Convert.ToInt16(properties.Count));
            foreach (Property property in properties)
            {
                PropertyInfo info = ownerType.GetProperty(property.Name);
                base.SerializeCore(new PropertyTypeInfo<Property>(property, info.PropertyType));
            }
        }

        private void writePropertyHeader(byte elementId, string name, Type valueType)
        {
            this._writer.WriteElementId(elementId);
            this._writer.WriteName(name);
            this._writer.WriteType(valueType);
        }

        private bool writePropertyHeaderWithReferenceId(byte elementId, ReferenceInfo info, string name, Type valueType)
        {
            if (info.Count < 2)
            {
                return false;
            }
            this.writePropertyHeader(elementId, name, valueType);
            this._writer.WriteNumber(info.Id);
            return true;
        }
    }
}

