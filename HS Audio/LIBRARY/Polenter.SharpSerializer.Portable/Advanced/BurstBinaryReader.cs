﻿namespace Polenter.Serialization.Advanced
{
    using Polenter.Serialization.Advanced.Binary;
    using Polenter.Serialization.Advanced.Serializing;
    using Polenter.Serialization.Core.Binary;
    using System;
    using System.IO;
    using System.Text;

    public sealed class BurstBinaryReader : IBinaryReader
    {
        private readonly Encoding _encoding;
        private BinaryReader _reader;
        private readonly ITypeNameConverter _typeNameConverter;

        public BurstBinaryReader(ITypeNameConverter typeNameConverter, Encoding encoding)
        {
            if (typeNameConverter == null)
            {
                throw new ArgumentNullException("typeNameConverter");
            }
            if (encoding == null)
            {
                throw new ArgumentNullException("encoding");
            }
            this._typeNameConverter = typeNameConverter;
            this._encoding = encoding;
        }

        public void Close()
        {
        }

        public void Open(Stream stream)
        {
            this._reader = new BinaryReader(stream, this._encoding);
        }

        public byte ReadElementId()
        {
            return this._reader.ReadByte();
        }

        public string ReadName()
        {
            return BinaryReaderTools.ReadString(this._reader);
        }

        public int ReadNumber()
        {
            return BinaryReaderTools.ReadNumber(this._reader);
        }

        public int[] ReadNumbers()
        {
            return BinaryReaderTools.ReadNumbers(this._reader);
        }

        public Type ReadType()
        {
            if (!this._reader.ReadBoolean())
            {
                return null;
            }
            string typeName = this._reader.ReadString();
            return this._typeNameConverter.ConvertToType(typeName);
        }

        public object ReadValue(Type expectedType)
        {
            return BinaryReaderTools.ReadValue(expectedType, this._reader);
        }
    }
}

