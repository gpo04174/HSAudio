﻿namespace Polenter.Serialization.Advanced.Serializing
{
    using System;

    public interface ITypeNameConverter
    {
        Type ConvertToType(string typeName);
        string ConvertToTypeName(Type type);
    }
}

