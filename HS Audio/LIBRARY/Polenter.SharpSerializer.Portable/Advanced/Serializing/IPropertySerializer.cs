﻿namespace Polenter.Serialization.Advanced.Serializing
{
    using Polenter.Serialization.Core;
    using System;
    using System.IO;

    public interface IPropertySerializer
    {
        void Close();
        void Open(Stream stream);
        void Serialize(Property property);
    }
}

