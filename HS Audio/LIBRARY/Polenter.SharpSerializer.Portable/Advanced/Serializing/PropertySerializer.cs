﻿namespace Polenter.Serialization.Advanced.Serializing
{
    using Polenter.Serialization.Core;
    using Polenter.Serialization.Serializing;
    using System;
    using System.IO;

    public abstract class PropertySerializer : IPropertySerializer
    {
        protected PropertySerializer()
        {
        }

        public abstract void Close();
        public abstract void Open(Stream stream);
        public void Serialize(Property property)
        {
            this.SerializeCore(new PropertyTypeInfo<Property>(property, null));
        }

        protected abstract void SerializeCollectionProperty(PropertyTypeInfo<CollectionProperty> property);
        protected abstract void SerializeComplexProperty(PropertyTypeInfo<ComplexProperty> property);
        protected void SerializeCore(PropertyTypeInfo<Property> property)
        {
            if (property == null)
            {
                throw new ArgumentNullException("property");
            }
            NullProperty property2 = property.Property as NullProperty;
            if (property2 != null)
            {
                this.SerializeNullProperty(new PropertyTypeInfo<NullProperty>(property2, property.ExpectedPropertyType, property.ValueType));
            }
            else
            {
                if ((property.ExpectedPropertyType != null) && (property.ExpectedPropertyType == property.ValueType))
                {
                    property.ValueType = null;
                }
                SimpleProperty property3 = property.Property as SimpleProperty;
                if (property3 != null)
                {
                    this.SerializeSimpleProperty(new PropertyTypeInfo<SimpleProperty>(property3, property.ExpectedPropertyType, property.ValueType));
                }
                else
                {
                    ReferenceTargetProperty property4 = property.Property as ReferenceTargetProperty;
                    if (property4 != null)
                    {
                        if (this.serializeReference(property4))
                        {
                            return;
                        }
                        if (this.serializeReferenceTarget(new PropertyTypeInfo<ReferenceTargetProperty>(property4, property.ExpectedPropertyType, property.ValueType)))
                        {
                            return;
                        }
                    }
                    throw new InvalidOperationException(string.Format("Unknown Property: {0}", new object[] { property.Property.GetType() }));
                }
            }
        }

        protected abstract void SerializeDictionaryProperty(PropertyTypeInfo<DictionaryProperty> property);
        protected abstract void SerializeMultiDimensionalArrayProperty(PropertyTypeInfo<MultiDimensionalArrayProperty> property);
        protected abstract void SerializeNullProperty(PropertyTypeInfo<NullProperty> property);
        private bool serializeReference(ReferenceTargetProperty property)
        {
            if ((property.Reference.Count > 1) && property.Reference.IsProcessed)
            {
                this.SerializeReference(property);
                return true;
            }
            return false;
        }

        protected abstract void SerializeReference(ReferenceTargetProperty referenceTarget);
        private bool serializeReferenceTarget(PropertyTypeInfo<ReferenceTargetProperty> property)
        {
            MultiDimensionalArrayProperty property2 = property.Property as MultiDimensionalArrayProperty;
            if (property2 != null)
            {
                property2.Reference.IsProcessed = true;
                this.SerializeMultiDimensionalArrayProperty(new PropertyTypeInfo<MultiDimensionalArrayProperty>(property2, property.ExpectedPropertyType, property.ValueType));
                return true;
            }
            SingleDimensionalArrayProperty property3 = property.Property as SingleDimensionalArrayProperty;
            if (property3 != null)
            {
                property3.Reference.IsProcessed = true;
                this.SerializeSingleDimensionalArrayProperty(new PropertyTypeInfo<SingleDimensionalArrayProperty>(property3, property.ExpectedPropertyType, property.ValueType));
                return true;
            }
            DictionaryProperty property4 = property.Property as DictionaryProperty;
            if (property4 != null)
            {
                property4.Reference.IsProcessed = true;
                this.SerializeDictionaryProperty(new PropertyTypeInfo<DictionaryProperty>(property4, property.ExpectedPropertyType, property.ValueType));
                return true;
            }
            CollectionProperty property5 = property.Property as CollectionProperty;
            if (property5 != null)
            {
                property5.Reference.IsProcessed = true;
                this.SerializeCollectionProperty(new PropertyTypeInfo<CollectionProperty>(property5, property.ExpectedPropertyType, property.ValueType));
                return true;
            }
            ComplexProperty property6 = property.Property as ComplexProperty;
            if (property6 != null)
            {
                property6.Reference.IsProcessed = true;
                this.SerializeComplexProperty(new PropertyTypeInfo<ComplexProperty>(property6, property.ExpectedPropertyType, property.ValueType));
                return true;
            }
            return false;
        }

        protected abstract void SerializeSimpleProperty(PropertyTypeInfo<SimpleProperty> property);
        protected abstract void SerializeSingleDimensionalArrayProperty(PropertyTypeInfo<SingleDimensionalArrayProperty> property);
    }
}

