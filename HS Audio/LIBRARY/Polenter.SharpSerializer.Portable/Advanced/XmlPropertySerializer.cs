﻿namespace Polenter.Serialization.Advanced
{
    using Polenter.Serialization.Advanced.Serializing;
    using Polenter.Serialization.Advanced.Xml;
    using Polenter.Serialization.Core;
    using Polenter.Serialization.Serializing;
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Reflection;

    public sealed class XmlPropertySerializer : PropertySerializer
    {
        private readonly IXmlWriter _writer;

        public XmlPropertySerializer(IXmlWriter writer)
        {
            if (writer == null)
            {
                throw new ArgumentNullException("writer");
            }
            this._writer = writer;
        }

        public override void Close()
        {
            this._writer.Close();
        }

        public override void Open(Stream stream)
        {
            this._writer.Open(stream);
        }

        protected override void SerializeCollectionProperty(PropertyTypeInfo<CollectionProperty> property)
        {
            this.writeStartProperty("Collection", property.Name, property.ValueType);
            if (property.Property.Reference.Count > 1)
            {
                this._writer.WriteAttribute("id", property.Property.Reference.Id);
            }
            this.writeProperties(property.Property.Properties, property.Property.Type);
            this.writeItems(property.Property.Items, property.Property.ElementType);
            this.writeEndProperty();
        }

        protected override void SerializeComplexProperty(PropertyTypeInfo<ComplexProperty> property)
        {
            this.writeStartProperty("Complex", property.Name, property.ValueType);
            if (property.Property.Reference.Count > 1)
            {
                this._writer.WriteAttribute("id", property.Property.Reference.Id);
            }
            this.writeProperties(property.Property.Properties, property.Property.Type);
            this.writeEndProperty();
        }

        protected override void SerializeDictionaryProperty(PropertyTypeInfo<DictionaryProperty> property)
        {
            this.writeStartProperty("Dictionary", property.Name, property.ValueType);
            if (property.Property.Reference.Count > 1)
            {
                this._writer.WriteAttribute("id", property.Property.Reference.Id);
            }
            this.writeProperties(property.Property.Properties, property.Property.Type);
            this.writeDictionaryItems(property.Property.Items, property.Property.KeyType, property.Property.ValueType);
            this.writeEndProperty();
        }

        protected override void SerializeMultiDimensionalArrayProperty(PropertyTypeInfo<MultiDimensionalArrayProperty> property)
        {
            this.writeStartProperty("MultiArray", property.Name, property.ValueType);
            if (property.Property.Reference.Count > 1)
            {
                this._writer.WriteAttribute("id", property.Property.Reference.Id);
            }
            this.writeDimensionInfos(property.Property.DimensionInfos);
            this.writeMultiDimensionalArrayItems(property.Property.Items, property.Property.ElementType);
            this.writeEndProperty();
        }

        protected override void SerializeNullProperty(PropertyTypeInfo<NullProperty> property)
        {
            this.writeStartProperty("Null", property.Name, property.ValueType);
            this.writeEndProperty();
        }

        protected override void SerializeReference(ReferenceTargetProperty referenceTarget)
        {
            this.writeStartProperty("Reference", referenceTarget.Name, null);
            this._writer.WriteAttribute("id", referenceTarget.Reference.Id);
            this.writeEndProperty();
        }

        protected override void SerializeSimpleProperty(PropertyTypeInfo<SimpleProperty> property)
        {
            if (property.Property.Value != null)
            {
                this.writeStartProperty("Simple", property.Name, property.ValueType);
                this._writer.WriteAttribute("value", property.Property.Value);
                this.writeEndProperty();
            }
        }

        protected override void SerializeSingleDimensionalArrayProperty(PropertyTypeInfo<SingleDimensionalArrayProperty> property)
        {
            this.writeStartProperty("SingleArray", property.Name, property.ValueType);
            if (property.Property.Reference.Count > 1)
            {
                this._writer.WriteAttribute("id", property.Property.Reference.Id);
            }
            if (property.Property.LowerBound != 0)
            {
                this._writer.WriteAttribute("lowerBound", property.Property.LowerBound);
            }
            this.writeItems(property.Property.Items, property.Property.ElementType);
            this.writeEndProperty();
        }

        private void writeDictionaryItem(KeyValueItem item, Type defaultKeyType, Type defaultValueType)
        {
            this._writer.WriteStartElement("Item");
            base.SerializeCore(new PropertyTypeInfo<Property>(item.Key, defaultKeyType));
            base.SerializeCore(new PropertyTypeInfo<Property>(item.Value, defaultValueType));
            this._writer.WriteEndElement();
        }

        private void writeDictionaryItems(IEnumerable<KeyValueItem> items, Type defaultKeyType, Type defaultValueType)
        {
            this._writer.WriteStartElement("Items");
            foreach (KeyValueItem item in items)
            {
                this.writeDictionaryItem(item, defaultKeyType, defaultValueType);
            }
            this._writer.WriteEndElement();
        }

        private void writeDimensionInfo(DimensionInfo info)
        {
            this._writer.WriteStartElement("Dimension");
            if (info.Length != 0)
            {
                this._writer.WriteAttribute("length", info.Length);
            }
            if (info.LowerBound != 0)
            {
                this._writer.WriteAttribute("lowerBound", info.LowerBound);
            }
            this._writer.WriteEndElement();
        }

        private void writeDimensionInfos(IEnumerable<DimensionInfo> infos)
        {
            this._writer.WriteStartElement("Dimensions");
            foreach (DimensionInfo info in infos)
            {
                this.writeDimensionInfo(info);
            }
            this._writer.WriteEndElement();
        }

        private void writeEndProperty()
        {
            this._writer.WriteEndElement();
        }

        private void writeItems(IEnumerable<Property> properties, Type defaultItemType)
        {
            this._writer.WriteStartElement("Items");
            foreach (Property property in properties)
            {
                base.SerializeCore(new PropertyTypeInfo<Property>(property, defaultItemType));
            }
            this._writer.WriteEndElement();
        }

        private void writeKeyType(Type type)
        {
            if (type != null)
            {
                this._writer.WriteAttribute("keyType", type);
            }
        }

        private void writeMultiDimensionalArrayItem(MultiDimensionalArrayItem item, Type defaultTypeOfItemValue)
        {
            this._writer.WriteStartElement("Item");
            this._writer.WriteAttribute("indexes", item.Indexes);
            base.SerializeCore(new PropertyTypeInfo<Property>(item.Value, defaultTypeOfItemValue));
            this._writer.WriteEndElement();
        }

        private void writeMultiDimensionalArrayItems(IEnumerable<MultiDimensionalArrayItem> items, Type defaultItemType)
        {
            this._writer.WriteStartElement("Items");
            foreach (MultiDimensionalArrayItem item in items)
            {
                this.writeMultiDimensionalArrayItem(item, defaultItemType);
            }
            this._writer.WriteEndElement();
        }

        private void writeProperties(ICollection<Property> properties, Type ownerType)
        {
            if (properties.Count != 0)
            {
                this._writer.WriteStartElement("Properties");
                foreach (Property property in properties)
                {
                    PropertyInfo info = ownerType.GetProperty(property.Name);
                    if (info != null)
                    {
                        base.SerializeCore(new PropertyTypeInfo<Property>(property, info.PropertyType));
                    }
                    else
                    {
                        base.SerializeCore(new PropertyTypeInfo<Property>(property, null));
                    }
                }
                this._writer.WriteEndElement();
            }
        }

        private void writeStartProperty(string elementId, string propertyName, Type propertyType)
        {
            this._writer.WriteStartElement(elementId);
            if (!string.IsNullOrEmpty(propertyName))
            {
                this._writer.WriteAttribute("name", propertyName);
            }
            if (propertyType != null)
            {
                this._writer.WriteAttribute("type", propertyType);
            }
        }

        private void writeValueType(Type type)
        {
            if (type != null)
            {
                this._writer.WriteAttribute("valueType", type);
            }
        }
    }
}

