﻿namespace Polenter.Serialization.Advanced
{
    using Polenter.Serialization.Serializing;
    using System;
    using System.Collections.Generic;
    using System.Reflection;

    public class PropertyProvider
    {
        private IList<Type> _attributesToIgnore;
        private static PropertyCache _cache;
        private Polenter.Serialization.Advanced.PropertiesToIgnore _propertiesToIgnore;

        protected bool ContainsExcludeFromSerializationAttribute(PropertyInfo property)
        {
            foreach (Type type in this.AttributesToIgnore)
            {
                if (property.GetCustomAttributes(type, false).Length > 0)
                {
                    return true;
                }
            }
            return false;
        }

        protected virtual PropertyInfo[] GetAllProperties(Type type)
        {
            return type.GetProperties(BindingFlags.Public | BindingFlags.Instance);
        }

        public IList<PropertyInfo> GetProperties(TypeInfo typeInfo)
        {
            IList<PropertyInfo> list = Cache.TryGetPropertyInfos(typeInfo.Type);
            if (list != null)
            {
                return list;
            }
            PropertyInfo[] allProperties = this.GetAllProperties(typeInfo.Type);
            List<PropertyInfo> list2 = new List<PropertyInfo>();
            foreach (PropertyInfo info in allProperties)
            {
                if (!this.IgnoreProperty(typeInfo, info))
                {
                    list2.Add(info);
                }
            }
            Cache.Add(typeInfo.Type, list2);
            return list2;
        }

        protected virtual bool IgnoreProperty(TypeInfo info, PropertyInfo property)
        {
            return (this.PropertiesToIgnore.Contains(info.Type, property.Name) || (this.ContainsExcludeFromSerializationAttribute(property) || ((!property.CanRead || !property.CanWrite) || (property.GetIndexParameters().Length > 0))));
        }

        public IList<Type> AttributesToIgnore
        {
            get
            {
                if (this._attributesToIgnore == null)
                {
                    this._attributesToIgnore = new List<Type>();
                }
                return this._attributesToIgnore;
            }
            set
            {
                this._attributesToIgnore = value;
            }
        }

        private static PropertyCache Cache
        {
            get
            {
                if (_cache == null)
                {
                    _cache = new PropertyCache();
                }
                return _cache;
            }
        }

        public Polenter.Serialization.Advanced.PropertiesToIgnore PropertiesToIgnore
        {
            get
            {
                if (this._propertiesToIgnore == null)
                {
                    this._propertiesToIgnore = new Polenter.Serialization.Advanced.PropertiesToIgnore();
                }
                return this._propertiesToIgnore;
            }
            set
            {
                this._propertiesToIgnore = value;
            }
        }
    }
}

