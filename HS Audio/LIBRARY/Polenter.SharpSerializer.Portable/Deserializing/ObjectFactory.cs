﻿namespace Polenter.Serialization.Deserializing
{
    using Polenter.Serialization.Core;
    using System;
    using System.Collections.Generic;
    using System.Reflection;
    using System.Runtime.CompilerServices;

    public sealed class ObjectFactory
    {
        private readonly object[] _emptyObjectArray = new object[0];
        private readonly Dictionary<int, object> _objectCache = new Dictionary<int, object>();

        private static Array createArrayInstance(Type elementType, int[] lengths, int[] lowerBounds)
        {
            return Array.CreateInstance(elementType, lengths);
        }

        public object CreateObject(Property property)
        {
            if (property == null)
            {
                throw new ArgumentNullException("property");
            }
            if (property is NullProperty)
            {
                return null;
            }
            if (property.Type == null)
            {
                throw new InvalidOperationException(string.Format("Property type is not defined. Property: \"{0}\"", new object[] { property.Name }));
            }
            SimpleProperty property3 = property as SimpleProperty;
            if (property3 != null)
            {
                return createObjectFromSimpleProperty(property3);
            }
            ReferenceTargetProperty property4 = property as ReferenceTargetProperty;
            if (property4 == null)
            {
                return null;
            }
            if ((property4.Reference != null) && !property4.Reference.IsProcessed)
            {
                return this._objectCache[property4.Reference.Id];
            }
            object obj2 = this.createObjectCore(property4);
            if (obj2 == null)
            {
                throw new InvalidOperationException(string.Format("Unknown Property type: {0}", new object[] { property.GetType().Name }));
            }
            return obj2;
        }

        private object createObjectCore(ReferenceTargetProperty property)
        {
            MultiDimensionalArrayProperty property2 = property as MultiDimensionalArrayProperty;
            if (property2 != null)
            {
                return this.createObjectFromMultidimensionalArrayProperty(property2);
            }
            SingleDimensionalArrayProperty property3 = property as SingleDimensionalArrayProperty;
            if (property3 != null)
            {
                return this.createObjectFromSingleDimensionalArrayProperty(property3);
            }
            DictionaryProperty property4 = property as DictionaryProperty;
            if (property4 != null)
            {
                return this.createObjectFromDictionaryProperty(property4);
            }
            CollectionProperty property5 = property as CollectionProperty;
            if (property5 != null)
            {
                return this.createObjectFromCollectionProperty(property5);
            }
            ComplexProperty property6 = property as ComplexProperty;
            if (property6 != null)
            {
                return this.createObjectFromComplexProperty(property6);
            }
            return null;
        }

        private object createObjectFromCollectionProperty(CollectionProperty property)
        {
            object obj2 = Tools.CreateInstance(property.Type);
            if (property.Reference != null)
            {
                this._objectCache.Add(property.Reference.Id, obj2);
            }
            this.fillProperties(obj2, property.Properties);
            MethodInfo method = obj2.GetType().GetMethod("Add");
            if ((method != null) && (method.GetParameters().Length == 1))
            {
                foreach (Property property2 in property.Items)
                {
                    object obj3 = this.CreateObject(property2);
                    method.Invoke(obj2, new object[] { obj3 });
                }
            }
            return obj2;
        }

        private object createObjectFromComplexProperty(ComplexProperty property)
        {
            object obj2 = Tools.CreateInstance(property.Type);
            if (property.Reference != null)
            {
                this._objectCache.Add(property.Reference.Id, obj2);
            }
            this.fillProperties(obj2, property.Properties);
            return obj2;
        }

        private object createObjectFromDictionaryProperty(DictionaryProperty property)
        {
            object obj2 = Tools.CreateInstance(property.Type);
            if (property.Reference != null)
            {
                this._objectCache.Add(property.Reference.Id, obj2);
            }
            this.fillProperties(obj2, property.Properties);
            MethodInfo method = obj2.GetType().GetMethod("Add");
            if ((method != null) && (method.GetParameters().Length == 2))
            {
                foreach (KeyValueItem item in property.Items)
                {
                    object obj3 = this.CreateObject(item.Key);
                    object obj4 = this.CreateObject(item.Value);
                    method.Invoke(obj2, new object[] { obj3, obj4 });
                }
            }
            return obj2;
        }

        private object createObjectFromMultidimensionalArrayProperty(MultiDimensionalArrayProperty property)
        {
            MultiDimensionalArrayCreatingInfo info = getMultiDimensionalArrayCreatingInfo(property.DimensionInfos);
            Array array = createArrayInstance(property.ElementType, info.Lengths, info.LowerBounds);
            if (property.Reference != null)
            {
                this._objectCache.Add(property.Reference.Id, array);
            }
            foreach (MultiDimensionalArrayItem item in property.Items)
            {
                object obj2 = this.CreateObject(item.Value);
                if (obj2 != null)
                {
                    array.SetValue(obj2, item.Indexes);
                }
            }
            return array;
        }

        private static object createObjectFromSimpleProperty(SimpleProperty property)
        {
            return property.Value;
        }

        private object createObjectFromSingleDimensionalArrayProperty(SingleDimensionalArrayProperty property)
        {
            int count = property.Items.Count;
            Array array = createArrayInstance(property.ElementType, new int[] { count }, new int[] { property.LowerBound });
            if (property.Reference != null)
            {
                this._objectCache.Add(property.Reference.Id, array);
            }
            for (int i = property.LowerBound; i < (property.LowerBound + count); i++)
            {
                Property property2 = property.Items[i];
                object obj2 = this.CreateObject(property2);
                if (obj2 != null)
                {
                    array.SetValue(obj2, i);
                }
            }
            return array;
        }

        private void fillProperties(object obj, IEnumerable<Property> properties)
        {
            foreach (Property property in properties)
            {
                PropertyInfo info = obj.GetType().GetProperty(property.Name);
                if (info != null)
                {
                    object obj2 = this.CreateObject(property);
                    if (obj2 != null)
                    {
                        info.SetValue(obj, obj2, this._emptyObjectArray);
                    }
                }
            }
        }

        private static MultiDimensionalArrayCreatingInfo getMultiDimensionalArrayCreatingInfo(IEnumerable<DimensionInfo> infos)
        {
            List<int> list = new List<int>();
            List<int> list2 = new List<int>();
            foreach (DimensionInfo info in infos)
            {
                list.Add(info.Length);
                list2.Add(info.LowerBound);
            }
            MultiDimensionalArrayCreatingInfo info2 = new MultiDimensionalArrayCreatingInfo();
            info2.Lengths = list.ToArray();
            info2.LowerBounds = list2.ToArray();
            return info2;
        }

        private class MultiDimensionalArrayCreatingInfo
        {
            [CompilerGenerated]
            private int[] _Lengths;
            [CompilerGenerated]
            private int[] _LowerBounds;

            public int[] Lengths
            {
                [CompilerGenerated]
                get
                {
                    return this._Lengths;
                }
                [CompilerGenerated]
                set
                {
                    this._Lengths = value;
                }
            }

            public int[] LowerBounds
            {
                [CompilerGenerated]
                get
                {
                    return this._LowerBounds;
                }
                [CompilerGenerated]
                set
                {
                    this._LowerBounds = value;
                }
            }
        }
    }
}

