﻿namespace Polenter.Serialization.Serializing
{
    using Polenter.Serialization.Core;
    using System;
    using System.Runtime.CompilerServices;

    public sealed class TypeInfo
    {
        private static TypeInfoCollection _cache;
        [CompilerGenerated]
        private int _ArrayDimensionCount;
        [CompilerGenerated]
        private System.Type _ElementType;
        [CompilerGenerated]
        private bool _IsArray;
        [CompilerGenerated]
        private bool _IsCollection;
        [CompilerGenerated]
        private bool _IsDictionary;
        [CompilerGenerated]
        private bool _IsEnumerable;
        [CompilerGenerated]
        private bool _IsSimple;
        [CompilerGenerated]
        private System.Type _KeyType;
        [CompilerGenerated]
        private System.Type _Type;

        private static bool fillKeyAndElementType(TypeInfo typeInfo, System.Type type)
        {
            if (!type.IsGenericType)
            {
                return false;
            }
            System.Type[] genericArguments = type.GetGenericArguments();
            if (typeInfo.IsDictionary)
            {
                typeInfo.KeyType = genericArguments[0];
                typeInfo.ElementType = genericArguments[1];
            }
            else
            {
                typeInfo.ElementType = genericArguments[0];
            }
            return (genericArguments.Length > 0);
        }

        public static TypeInfo GetTypeInfo(object obj)
        {
            if (obj == null)
            {
                throw new ArgumentNullException("obj");
            }
            return GetTypeInfo(obj.GetType());
        }

        public static TypeInfo GetTypeInfo(System.Type type)
        {
            TypeInfo typeInfo = Cache.TryGetTypeInfo(type);
            if (typeInfo == null)
            {
                typeInfo = new TypeInfo();
                typeInfo.Type = type;
                typeInfo.IsSimple = Tools.IsSimple(type);
                if (type == typeof(byte[]))
                {
                    typeInfo.ElementType = typeof(byte);
                }
                if (!typeInfo.IsSimple)
                {
                    typeInfo.IsArray = Tools.IsArray(type);
                    if (typeInfo.IsArray)
                    {
                        if (type.HasElementType)
                        {
                            typeInfo.ElementType = type.GetElementType();
                        }
                        typeInfo.ArrayDimensionCount = type.GetArrayRank();
                    }
                    else
                    {
                        typeInfo.IsEnumerable = Tools.IsEnumerable(type);
                        if (typeInfo.IsEnumerable)
                        {
                            typeInfo.IsCollection = Tools.IsCollection(type);
                            if (typeInfo.IsCollection)
                            {
                                bool flag;
                                typeInfo.IsDictionary = Tools.IsDictionary(type);
                                System.Type baseType = type;
                                do
                                {
                                    flag = fillKeyAndElementType(typeInfo, baseType);
                                    baseType = baseType.BaseType;
                                }
                                while ((!flag && (baseType != null)) && (baseType != typeof(object)));
                            }
                        }
                    }
                }
                Cache.AddIfNotExists(typeInfo);
            }
            return typeInfo;
        }

        public int ArrayDimensionCount
        {
            [CompilerGenerated]
            get
            {
                return this._ArrayDimensionCount;
            }
            [CompilerGenerated]
            set
            {
                this._ArrayDimensionCount = value;
            }
        }

        private static TypeInfoCollection Cache
        {
            get
            {
                if (_cache == null)
                {
                    _cache = new TypeInfoCollection();
                }
                return _cache;
            }
        }

        public System.Type ElementType
        {
            [CompilerGenerated]
            get
            {
                return this._ElementType;
            }
            [CompilerGenerated]
            set
            {
                this._ElementType = value;
            }
        }

        public bool IsArray
        {
            [CompilerGenerated]
            get
            {
                return this._IsArray;
            }
            [CompilerGenerated]
            set
            {
                this._IsArray = value;
            }
        }

        public bool IsCollection
        {
            [CompilerGenerated]
            get
            {
                return this._IsCollection;
            }
            [CompilerGenerated]
            set
            {
                this._IsCollection = value;
            }
        }

        public bool IsDictionary
        {
            [CompilerGenerated]
            get
            {
                return this._IsDictionary;
            }
            [CompilerGenerated]
            set
            {
                this._IsDictionary = value;
            }
        }

        public bool IsEnumerable
        {
            [CompilerGenerated]
            get
            {
                return this._IsEnumerable;
            }
            [CompilerGenerated]
            set
            {
                this._IsEnumerable = value;
            }
        }

        public bool IsSimple
        {
            [CompilerGenerated]
            get
            {
                return this._IsSimple;
            }
            [CompilerGenerated]
            set
            {
                this._IsSimple = value;
            }
        }

        public System.Type KeyType
        {
            [CompilerGenerated]
            get
            {
                return this._KeyType;
            }
            [CompilerGenerated]
            set
            {
                this._KeyType = value;
            }
        }

        public System.Type Type
        {
            [CompilerGenerated]
            get
            {
                return this._Type;
            }
            [CompilerGenerated]
            set
            {
                this._Type = value;
            }
        }
    }
}

