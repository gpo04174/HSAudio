﻿namespace Polenter.Serialization.Serializing
{
    using Polenter.Serialization.Advanced;
    using Polenter.Serialization.Core;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Reflection;

    public sealed class PropertyFactory
    {
        private int _currentReferenceId = 1;
        private readonly object[] _emptyObjectArray = new object[0];
        private readonly Dictionary<object, ReferenceTargetProperty> _propertyCache = new Dictionary<object, ReferenceTargetProperty>();
        private readonly PropertyProvider _propertyProvider;

        public PropertyFactory(PropertyProvider propertyProvider)
        {
            this._propertyProvider = propertyProvider;
        }

        public Property CreateProperty(string name, object value)
        {
            ReferenceTargetProperty property3;
            if (value == null)
            {
                return new NullProperty(name);
            }
            TypeInfo typeInfo = TypeInfo.GetTypeInfo(value);
            Property property = createSimpleProperty(name, typeInfo, value);
            if (property != null)
            {
                return property;
            }
            ReferenceTargetProperty property2 = createReferenceTargetInstance(name, typeInfo);
            if (this._propertyCache.TryGetValue(value, out property3))
            {
                ReferenceInfo reference = property3.Reference;
                reference.Count++;
                property2.MakeFlatCopyFrom(property3);
                return property2;
            }
            property2.Reference = new ReferenceInfo();
            property2.Reference.Id = this._currentReferenceId++;
            this._propertyCache.Add(value, property2);
            if (!((((this.fillSingleDimensionalArrayProperty(property2 as SingleDimensionalArrayProperty, typeInfo, value) || this.fillMultiDimensionalArrayProperty(property2 as MultiDimensionalArrayProperty, typeInfo, value)) || this.fillDictionaryProperty(property2 as DictionaryProperty, typeInfo, value)) || this.fillCollectionProperty(property2 as CollectionProperty, typeInfo, value)) || this.fillComplexProperty(property2 as ComplexProperty, typeInfo, value)))
            {
                throw new InvalidOperationException(string.Format("Property cannot be filled. Property: {0}", new object[] { property2 }));
            }
            return property2;
        }

        private static ReferenceTargetProperty createReferenceTargetInstance(string name, TypeInfo typeInfo)
        {
            if (typeInfo.IsArray)
            {
                if (typeInfo.ArrayDimensionCount < 2)
                {
                    return new SingleDimensionalArrayProperty(name, typeInfo.Type);
                }
                return new MultiDimensionalArrayProperty(name, typeInfo.Type);
            }
            if (typeInfo.IsDictionary)
            {
                return new DictionaryProperty(name, typeInfo.Type);
            }
            if (typeInfo.IsCollection)
            {
                return new CollectionProperty(name, typeInfo.Type);
            }
            if (typeInfo.IsEnumerable)
            {
                return new CollectionProperty(name, typeInfo.Type);
            }
            return new ComplexProperty(name, typeInfo.Type);
        }

        private static Property createSimpleProperty(string name, TypeInfo typeInfo, object value)
        {
            if (!typeInfo.IsSimple)
            {
                return null;
            }
            SimpleProperty property = new SimpleProperty(name, typeInfo.Type);
            property.Value = value;
            return property;
        }

        private bool fillCollectionProperty(CollectionProperty property, TypeInfo info, object value)
        {
            if (property == null)
            {
                return false;
            }
            this.parseProperties(property, info, value);
            this.parseCollectionItems(property, info, value);
            return true;
        }

        private bool fillComplexProperty(ComplexProperty property, TypeInfo typeInfo, object value)
        {
            if (property == null)
            {
                return false;
            }
            this.parseProperties(property, typeInfo, value);
            return true;
        }

        private bool fillDictionaryProperty(DictionaryProperty property, TypeInfo info, object value)
        {
            if (property == null)
            {
                return false;
            }
            this.parseProperties(property, info, value);
            this.parseDictionaryItems(property, info, value);
            return true;
        }

        private bool fillMultiDimensionalArrayProperty(MultiDimensionalArrayProperty property, TypeInfo info, object value)
        {
            if (property == null)
            {
                return false;
            }
            property.ElementType = info.ElementType;
            ArrayAnalyzer analyzer = new ArrayAnalyzer(value);
            property.DimensionInfos = analyzer.ArrayInfo.DimensionInfos;
            foreach (int[] numArray in analyzer.GetIndexes())
            {
                object obj2 = ((Array) value).GetValue(numArray);
                Property property2 = this.CreateProperty(null, obj2);
                property.Items.Add(new MultiDimensionalArrayItem(numArray, property2));
            }
            return true;
        }

        private bool fillSingleDimensionalArrayProperty(SingleDimensionalArrayProperty property, TypeInfo info, object value)
        {
            if (property == null)
            {
                return false;
            }
            property.ElementType = info.ElementType;
            ArrayAnalyzer analyzer = new ArrayAnalyzer(value);
            DimensionInfo info2 = analyzer.ArrayInfo.DimensionInfos[0];
            property.LowerBound = info2.LowerBound;
            foreach (object obj2 in analyzer.GetValues())
            {
                Property item = this.CreateProperty(null, obj2);
                property.Items.Add(item);
            }
            return true;
        }

        private void parseCollectionItems(CollectionProperty property, TypeInfo info, object value)
        {
            property.ElementType = info.ElementType;
            IEnumerable enumerable = (IEnumerable) value;
            foreach (object obj2 in enumerable)
            {
                Property item = this.CreateProperty(null, obj2);
                property.Items.Add(item);
            }
        }

        private void parseDictionaryItems(DictionaryProperty property, TypeInfo info, object value)
        {
            property.KeyType = info.KeyType;
            property.ValueType = info.ElementType;
            IDictionary dictionary = (IDictionary) value;
            foreach (DictionaryEntry entry in dictionary)
            {
                Property key = this.CreateProperty(null, entry.Key);
                Property property3 = this.CreateProperty(null, entry.Value);
                property.Items.Add(new KeyValueItem(key, property3));
            }
        }

        private void parseProperties(ComplexProperty property, TypeInfo typeInfo, object value)
        {
            foreach (PropertyInfo info in this._propertyProvider.GetProperties(typeInfo))
            {
                object obj2 = info.GetValue(value, this._emptyObjectArray);
                Property item = this.CreateProperty(info.Name, obj2);
                property.Properties.Add(item);
            }
        }
    }
}

