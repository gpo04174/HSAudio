﻿namespace Polenter.Serialization.Serializing
{
    using System;
    using System.Runtime.CompilerServices;

    public sealed class PropertyTypeInfo<TProperty> where TProperty: Polenter.Serialization.Core.Property
    {
        [CompilerGenerated]
        private Type _ExpectedPropertyType;
        [CompilerGenerated]
        private string _Name;
        [CompilerGenerated]
        private TProperty _Property;
        [CompilerGenerated]
        private Type _ValueType;

        public PropertyTypeInfo(TProperty property, Type valueType)
        {
            this.Property = property;
            this.ExpectedPropertyType = valueType;
            this.ValueType = property.Type;
            this.Name = property.Name;
        }

        public PropertyTypeInfo(TProperty property, Type expectedPropertyType, Type valueType)
        {
            this.Property = property;
            this.ExpectedPropertyType = expectedPropertyType;
            this.ValueType = valueType;
            this.Name = property.Name;
        }

        public Type ExpectedPropertyType
        {
            [CompilerGenerated]
            get
            {
                return this._ExpectedPropertyType;
            }
            [CompilerGenerated]
            set
            {
                this._ExpectedPropertyType = value;
            }
        }

        public string Name
        {
            [CompilerGenerated]
            get
            {
                return this._Name;
            }
            [CompilerGenerated]
            set
            {
                this._Name = value;
            }
        }

        public TProperty Property
        {
            [CompilerGenerated]
            get
            {
                return this._Property;
            }
            [CompilerGenerated]
            set
            {
                this._Property = value;
            }
        }

        public Type ValueType
        {
            [CompilerGenerated]
            get
            {
                return this._ValueType;
            }
            [CompilerGenerated]
            set
            {
                this._ValueType = value;
            }
        }
    }
}

