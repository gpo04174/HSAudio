﻿namespace Polenter.Serialization.Serializing
{
    using System;
    using System.Collections.ObjectModel;

    public sealed class TypeInfoCollection : KeyedCollection<Type, TypeInfo>
    {
        private readonly object _locker = new object();

        public void AddIfNotExists(TypeInfo item)
        {
            if (item == null)
            {
                throw new ArgumentNullException("item");
            }
            lock (this._locker)
            {
                if (!base.Contains(item.Type))
                {
                    base.Add(item);
                }
            }
        }

        protected override void ClearItems()
        {
            lock (this._locker)
            {
                base.ClearItems();
            }
        }

        protected override Type GetKeyForItem(TypeInfo item)
        {
            return item.Type;
        }

        protected override void InsertItem(int index, TypeInfo item)
        {
            lock (this._locker)
            {
                base.InsertItem(index, item);
            }
        }

        protected override void RemoveItem(int index)
        {
            lock (this._locker)
            {
                base.RemoveItem(index);
            }
        }

        protected override void SetItem(int index, TypeInfo item)
        {
            lock (this._locker)
            {
                base.SetItem(index, item);
            }
        }

        public TypeInfo TryGetTypeInfo(Type type)
        {
            lock (this._locker)
            {
                if (!base.Contains(type))
                {
                    return null;
                }
                return base[type];
            }
        }
    }
}

