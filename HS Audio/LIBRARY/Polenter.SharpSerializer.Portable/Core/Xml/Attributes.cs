﻿namespace Polenter.Serialization.Core.Xml
{
    using System;

    public static class Attributes
    {
        public const string DimensionCount = "dimensionCount";
        public const string ElementType = "elementType";
        public const string Indexes = "indexes";
        public const string KeyType = "keyType";
        public const string Length = "length";
        public const string LowerBound = "lowerBound";
        public const string Name = "name";
        public const string ReferenceId = "id";
        public const string Type = "type";
        public const string Value = "value";
        public const string ValueType = "valueType";
    }
}

