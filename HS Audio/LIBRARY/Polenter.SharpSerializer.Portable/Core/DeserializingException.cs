﻿namespace Polenter.Serialization.Core
{
    using System;

    public class DeserializingException : Exception
    {
        public DeserializingException()
        {
        }

        public DeserializingException(string message) : base(message)
        {
        }

        public DeserializingException(string message, Exception inner) : base(message, inner)
        {
        }
    }
}

