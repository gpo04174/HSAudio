﻿namespace Polenter.Serialization.Core
{
    using System;
    using System.Runtime.CompilerServices;

    public abstract class Property
    {
        [CompilerGenerated]
        private string _Name;
        [CompilerGenerated]
        private Property _Parent;
        [CompilerGenerated]
        private System.Type _Type;

        protected Property(string name, System.Type type)
        {
            this.Name = name;
            this.Type = type;
        }

        public static Property CreateInstance(PropertyArt art, string propertyName, System.Type propertyType)
        {
            switch (art)
            {
                case PropertyArt.Simple:
                    return new SimpleProperty(propertyName, propertyType);

                case PropertyArt.Complex:
                    return new ComplexProperty(propertyName, propertyType);

                case PropertyArt.Collection:
                    return new CollectionProperty(propertyName, propertyType);

                case PropertyArt.Dictionary:
                    return new DictionaryProperty(propertyName, propertyType);

                case PropertyArt.SingleDimensionalArray:
                    return new SingleDimensionalArrayProperty(propertyName, propertyType);

                case PropertyArt.MultiDimensionalArray:
                    return new MultiDimensionalArrayProperty(propertyName, propertyType);

                case PropertyArt.Null:
                    return new NullProperty(propertyName);

                case PropertyArt.Reference:
                    return null;
            }
            throw new InvalidOperationException(string.Format("Unknown PropertyArt {0}", new object[] { art }));
        }

        protected abstract PropertyArt GetPropertyArt();
        public override string ToString()
        {
            string str = this.Name ?? "null";
            string str2 = (this.Type == null) ? "null" : this.Type.Name;
            string str3 = (this.Parent == null) ? "null" : this.Parent.GetType().Name;
            return string.Format("{0}, Name={1}, Type={2}, Parent={3}", new object[] { base.GetType().Name, str, str2, str3 });
        }

        public PropertyArt Art
        {
            get
            {
                return this.GetPropertyArt();
            }
        }

        public string Name
        {
            [CompilerGenerated]
            get
            {
                return this._Name;
            }
            [CompilerGenerated]
            set
            {
                this._Name = value;
            }
        }

        public Property Parent
        {
            [CompilerGenerated]
            get
            {
                return this._Parent;
            }
            [CompilerGenerated]
            set
            {
                this._Parent = value;
            }
        }

        public System.Type Type
        {
            [CompilerGenerated]
            get
            {
                return this._Type;
            }
            [CompilerGenerated]
            set
            {
                this._Type = value;
            }
        }
    }
}

