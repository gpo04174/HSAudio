﻿namespace Polenter.Serialization.Core
{
    using System;

    public class CreatingInstanceException : Exception
    {
        public CreatingInstanceException()
        {
        }

        public CreatingInstanceException(string message) : base(message)
        {
        }

        public CreatingInstanceException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}

