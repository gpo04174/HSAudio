﻿namespace Polenter.Serialization.Core
{
    using System;
    using System.Runtime.CompilerServices;

    public sealed class SingleDimensionalArrayProperty : ReferenceTargetProperty
    {
        private PropertyCollection _items;
        [CompilerGenerated]
        private Type _ElementType;
        [CompilerGenerated]
        private int _LowerBound;

        public SingleDimensionalArrayProperty(string name, Type type) : base(name, type)
        {
        }

        protected override PropertyArt GetPropertyArt()
        {
            return PropertyArt.SingleDimensionalArray;
        }

        public override void MakeFlatCopyFrom(ReferenceTargetProperty source)
        {
            SingleDimensionalArrayProperty property = source as SingleDimensionalArrayProperty;
            if (property == null)
            {
                throw new InvalidCastException(string.Format("Invalid property type to make a flat copy. Expected {0}, current {1}", new object[] { typeof(SingleDimensionalArrayProperty), source.GetType() }));
            }
            base.MakeFlatCopyFrom(source);
            this.LowerBound = property.LowerBound;
            this.ElementType = property.ElementType;
            this.Items = property.Items;
        }

        public Type ElementType
        {
            [CompilerGenerated]
            get
            {
                return this._ElementType;
            }
            [CompilerGenerated]
            set
            {
                this._ElementType = value;
            }
        }

        public PropertyCollection Items
        {
            get
            {
                if (this._items == null)
                {
                    PropertyCollection propertys = new PropertyCollection();
                    propertys.Parent = this;
                    this._items = propertys;
                }
                return this._items;
            }
            set
            {
                this._items = value;
            }
        }

        public int LowerBound
        {
            [CompilerGenerated]
            get
            {
                return this._LowerBound;
            }
            [CompilerGenerated]
            set
            {
                this._LowerBound = value;
            }
        }
    }
}

