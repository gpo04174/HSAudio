﻿namespace Polenter.Serialization.Core
{
    using System;

    public class ComplexProperty : ReferenceTargetProperty
    {
        private PropertyCollection _properties;

        public ComplexProperty(string name, Type type) : base(name, type)
        {
        }

        protected override PropertyArt GetPropertyArt()
        {
            return PropertyArt.Complex;
        }

        public override void MakeFlatCopyFrom(ReferenceTargetProperty source)
        {
            ComplexProperty property = source as ComplexProperty;
            if (property == null)
            {
                throw new InvalidCastException(string.Format("Invalid property type to make a flat copy. Expected {0}, current {1}", new object[] { typeof(ComplexProperty), source.GetType() }));
            }
            base.MakeFlatCopyFrom(source);
            this.Properties = property.Properties;
        }

        public PropertyCollection Properties
        {
            get
            {
                if (this._properties == null)
                {
                    PropertyCollection propertys = new PropertyCollection();
                    propertys.Parent = this;
                    this._properties = propertys;
                }
                return this._properties;
            }
            set
            {
                this._properties = value;
            }
        }
    }
}

