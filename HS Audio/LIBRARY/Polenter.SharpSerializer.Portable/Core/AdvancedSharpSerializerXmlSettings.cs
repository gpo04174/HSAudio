﻿namespace Polenter.Serialization.Core
{
    using Polenter.Serialization.Advanced.Xml;
    using System;
    using System.Runtime.CompilerServices;

    public sealed class AdvancedSharpSerializerXmlSettings : AdvancedSharpSerializerSettings
    {
        [CompilerGenerated]
        private ISimpleValueConverter _SimpleValueConverter;

        public ISimpleValueConverter SimpleValueConverter
        {
            [CompilerGenerated]
            get
            {
                return this._SimpleValueConverter;
            }
            [CompilerGenerated]
            set
            {
                this._SimpleValueConverter = value;
            }
        }
    }
}

