﻿namespace Polenter.Serialization.Core
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    public sealed class MultiDimensionalArrayProperty : ReferenceTargetProperty
    {
        private IList<DimensionInfo> _dimensionInfos;
        private IList<MultiDimensionalArrayItem> _items;
        [CompilerGenerated]
        private Type _ElementType;

        public MultiDimensionalArrayProperty(string name, Type type) : base(name, type)
        {
        }

        protected override PropertyArt GetPropertyArt()
        {
            return PropertyArt.MultiDimensionalArray;
        }

        public override void MakeFlatCopyFrom(ReferenceTargetProperty source)
        {
            MultiDimensionalArrayProperty property = source as MultiDimensionalArrayProperty;
            if (property == null)
            {
                throw new InvalidCastException(string.Format("Invalid property type to make a flat copy. Expected {0}, current {1}", new object[] { typeof(SingleDimensionalArrayProperty), source.GetType() }));
            }
            base.MakeFlatCopyFrom(source);
            this.ElementType = property.ElementType;
            this.DimensionInfos = property.DimensionInfos;
            this.Items = property.Items;
        }

        public IList<DimensionInfo> DimensionInfos
        {
            get
            {
                if (this._dimensionInfos == null)
                {
                    this._dimensionInfos = new List<DimensionInfo>();
                }
                return this._dimensionInfos;
            }
            set
            {
                this._dimensionInfos = value;
            }
        }

        public Type ElementType
        {
            [CompilerGenerated]
            get
            {
                return this._ElementType;
            }
            [CompilerGenerated]
            set
            {
                this._ElementType = value;
            }
        }

        public IList<MultiDimensionalArrayItem> Items
        {
            get
            {
                if (this._items == null)
                {
                    this._items = new List<MultiDimensionalArrayItem>();
                }
                return this._items;
            }
            set
            {
                this._items = value;
            }
        }
    }
}

