﻿namespace Polenter.Serialization.Core
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Threading;

    public class ArrayAnalyzer
    {
        private readonly object _array;
        private readonly Polenter.Serialization.Core.ArrayInfo _arrayInfo;
        private IList<int[]> _indexes;

        public ArrayAnalyzer(object array)
        {
            this._array = array;
            Type arrayType = array.GetType();
            this._arrayInfo = this.getArrayInfo(arrayType);
        }

        private void addIndexes(int[] obj)
        {
            this._indexes.Add(obj);
        }

        private void forEach(IList<DimensionInfo> dimensionInfos, int dimension, IEnumerable<int> coordinates, Action<int[]> action)
        {
            DimensionInfo info = dimensionInfos[dimension];
            for (int i = info.LowerBound; i < (info.LowerBound + info.Length); i++)
            {
                List<int> list = new List<int>(coordinates);
                list.Add(i);
                if (dimension == (this._arrayInfo.DimensionInfos.Count - 1))
                {
                    action(list.ToArray());
                }
                else
                {
                    this.forEach(this._arrayInfo.DimensionInfos, dimension + 1, list, action);
                }
            }
        }

        public void ForEach(Action<int[]> action)
        {
            DimensionInfo info = this._arrayInfo.DimensionInfos[0];
            for (int i = info.LowerBound; i < (info.LowerBound + info.Length); i++)
            {
                List<int> coordinates = new List<int>();
                coordinates.Add(i);
                if (this._arrayInfo.DimensionInfos.Count < 2)
                {
                    action(coordinates.ToArray());
                }
                else
                {
                    this.forEach(this._arrayInfo.DimensionInfos, 1, coordinates, action);
                }
            }
        }

        private Polenter.Serialization.Core.ArrayInfo getArrayInfo(Type arrayType)
        {
            Polenter.Serialization.Core.ArrayInfo info = new Polenter.Serialization.Core.ArrayInfo();
            for (int i = 0; i < this.getRank(arrayType); i++)
            {
                DimensionInfo item = new DimensionInfo();
                item.Length = this.getLength(i, arrayType);
                item.LowerBound = this.getLowerBound(i, arrayType);
                info.DimensionInfos.Add(item);
            }
            return info;
        }

        private int getBound(string methodName, int dimension, Type arrayType)
        {
            return (int) arrayType.GetMethod(methodName).Invoke(this._array, new object[] { dimension });
        }

        public IEnumerable<int[]> GetIndexes()
        {
            if (this._indexes == null)
            {
                this._indexes = new List<int[]>();
                this.ForEach(new Action<int[]>(this.addIndexes));
            }
            foreach (int[] iteratorVariable0 in this._indexes)
            {
                yield return iteratorVariable0;
            }
        }

        private int getLength(int dimension, Type arrayType)
        {
            return (int) arrayType.GetMethod("GetLength").Invoke(this._array, new object[] { dimension });
        }

        private int getLowerBound(int dimension, Type arrayType)
        {
            return this.getBound("GetLowerBound", dimension, arrayType);
        }

        private int getRank(Type arrayType)
        {
            return arrayType.GetArrayRank();
        }

        public IEnumerable<object> GetValues()
        {
            foreach (int[] iteratorVariable0 in this.GetIndexes())
            {
                object iteratorVariable1 = ((Array) this._array).GetValue(iteratorVariable0);
                yield return iteratorVariable1;
            }
        }

        public Polenter.Serialization.Core.ArrayInfo ArrayInfo
        {
            get
            {
                return this._arrayInfo;
            }
        }


    }
}

