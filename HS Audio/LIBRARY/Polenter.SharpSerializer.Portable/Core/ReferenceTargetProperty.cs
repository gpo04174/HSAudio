﻿namespace Polenter.Serialization.Core
{
    using System;
    using System.Runtime.CompilerServices;

    public abstract class ReferenceTargetProperty : Property
    {
        [CompilerGenerated]
        private ReferenceInfo _Reference;

        protected ReferenceTargetProperty(string name, Type type) : base(name, type)
        {
        }

        public virtual void MakeFlatCopyFrom(ReferenceTargetProperty source)
        {
            this.Reference = source.Reference;
        }

        public override string ToString()
        {
            string str = base.ToString();
            string str2 = (this.Reference != null) ? this.Reference.ToString() : "null";
            return string.Format("{0}, Reference={1}", new object[] { str, str2 });
        }

        public ReferenceInfo Reference
        {
            [CompilerGenerated]
            get
            {
                return this._Reference;
            }
            [CompilerGenerated]
            set
            {
                this._Reference = value;
            }
        }
    }
}

