﻿namespace Polenter.Serialization.Core
{
    using System;
    using System.Collections;

    internal static class Tools
    {
        public static object CreateInstance(Type type)
        {
            object obj3;
            if (type == null)
            {
                return null;
            }
            try
            {
                obj3 = Activator.CreateInstance(type);
            }
            catch (Exception exception)
            {
                throw new CreatingInstanceException(string.Format("Error during creating an object. Please check if the type \"{0}\" has public parameterless constructor, or if the settings IncludeAssemblyVersionInTypeName, IncludeCultureInTypeName, IncludePublicKeyTokenInTypeName are set to true. Details are in the inner exception.", new object[] { type.AssemblyQualifiedName }), exception);
            }
            return obj3;
        }

        public static bool IsArray(Type type)
        {
            return type.IsArray;
        }

        public static bool IsCollection(Type type)
        {
            Type type2 = typeof(ICollection);
            return type2.IsAssignableFrom(type);
        }

        public static bool IsDictionary(Type type)
        {
            Type type2 = typeof(IDictionary);
            return type2.IsAssignableFrom(type);
        }

        public static bool IsEnumerable(Type type)
        {
            Type type2 = typeof(IEnumerable);
            return type2.IsAssignableFrom(type);
        }

        public static bool IsSimple(Type type)
        {
            return ((type == typeof(string)) || ((type == typeof(DateTime)) || ((type == typeof(TimeSpan)) || ((type == typeof(decimal)) || ((type == typeof(Guid)) || (((type == typeof(Type)) || type.IsSubclassOf(typeof(Type))) || (type.IsEnum || ((type == typeof(byte[])) || type.IsPrimitive))))))));
        }
    }
}

