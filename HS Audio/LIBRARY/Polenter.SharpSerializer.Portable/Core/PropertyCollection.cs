﻿namespace Polenter.Serialization.Core
{
    using System;
    using System.Collections.ObjectModel;
    using System.Runtime.CompilerServices;

    public sealed class PropertyCollection : Collection<Property>
    {
        [CompilerGenerated]
        private Property _Parent;

        protected override void ClearItems()
        {
            foreach (Property property in base.Items)
            {
                property.Parent = null;
            }
            base.ClearItems();
        }

        protected override void InsertItem(int index, Property item)
        {
            base.InsertItem(index, item);
            item.Parent = this.Parent;
        }

        protected override void RemoveItem(int index)
        {
            base.Items[index].Parent = null;
            base.RemoveItem(index);
        }

        protected override void SetItem(int index, Property item)
        {
            base.Items[index].Parent = null;
            base.SetItem(index, item);
            item.Parent = this.Parent;
        }

        public Property Parent
        {
            [CompilerGenerated]
            get
            {
                return this._Parent;
            }
            [CompilerGenerated]
            set
            {
                this._Parent = value;
            }
        }
    }
}

