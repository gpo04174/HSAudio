﻿namespace Polenter.Serialization.Core
{
    using System;
    using System.Runtime.CompilerServices;

    public sealed class SimpleProperty : Property
    {
        [CompilerGenerated]
        private object _Value;

        public SimpleProperty(string name, Type type) : base(name, type)
        {
        }

        protected override PropertyArt GetPropertyArt()
        {
            return PropertyArt.Simple;
        }

        public override string ToString()
        {
            string str = base.ToString();
            if (this.Value == null)
            {
                return string.Format("{0}, (null)", new object[] { str });
            }
            return string.Format("{0}, ({1})", new object[] { str, this.Value });
        }

        public object Value
        {
            [CompilerGenerated]
            get
            {
                return this._Value;
            }
            [CompilerGenerated]
            set
            {
                this._Value = value;
            }
        }
    }
}

