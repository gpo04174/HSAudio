﻿namespace Polenter.Serialization.Core
{
    using System;
    using System.Runtime.CompilerServices;

    public sealed class KeyValueItem
    {
        [CompilerGenerated]
        private Property _Key;
        [CompilerGenerated]
        private Property _Value;

        public KeyValueItem(Property key, Property value)
        {
            this.Key = key;
            this.Value = value;
        }

        public Property Key
        {
            [CompilerGenerated]
            get
            {
                return this._Key;
            }
            [CompilerGenerated]
            set
            {
                this._Key = value;
            }
        }

        public Property Value
        {
            [CompilerGenerated]
            get
            {
                return this._Value;
            }
            [CompilerGenerated]
            set
            {
                this._Value = value;
            }
        }
    }
}

