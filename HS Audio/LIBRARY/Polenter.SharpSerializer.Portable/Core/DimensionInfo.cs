﻿namespace Polenter.Serialization.Core
{
    using System;
    using System.Runtime.CompilerServices;

    public sealed class DimensionInfo
    {
        [CompilerGenerated]
        private int _Length;
        [CompilerGenerated]
        private int _LowerBound;

        public int Length
        {
            [CompilerGenerated]
            get
            {
                return this._Length;
            }
            [CompilerGenerated]
            set
            {
                this._Length = value;
            }
        }

        public int LowerBound
        {
            [CompilerGenerated]
            get
            {
                return this._LowerBound;
            }
            [CompilerGenerated]
            set
            {
                this._LowerBound = value;
            }
        }
    }
}

