﻿namespace Polenter.Serialization.Core.Binary
{
    using System;

    public class Attributes
    {
        public const byte DimensionCount = 0x65;
        public const byte ElementType = 0x66;
        public const byte Indexes = 0x67;
        public const byte KeyType = 0x68;
        public const byte Length = 0x69;
        public const byte LowerBound = 0x6a;
        public const byte Name = 0x6b;
        public const byte Type = 0x6c;
        public const byte Value = 0x6d;
        public const byte ValueType = 110;
    }
}

