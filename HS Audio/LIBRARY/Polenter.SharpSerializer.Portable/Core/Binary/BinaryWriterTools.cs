﻿namespace Polenter.Serialization.Core.Binary
{
    using System;
    using System.IO;

    public static class BinaryWriterTools
    {
        private static bool isType(Type type)
        {
            if (type != typeof(Type))
            {
                return type.IsSubclassOf(typeof(Type));
            }
            return true;
        }

        private static void writeArrayOfByte(byte[] data, BinaryWriter writer)
        {
            WriteNumber(data.Length, writer);
            writer.Write(data);
        }

        private static void writeDecimal(decimal value, BinaryWriter writer)
        {
            int[] bits = decimal.GetBits(value);
            writer.Write(bits[0]);
            writer.Write(bits[1]);
            writer.Write(bits[2]);
            writer.Write(bits[3]);
        }

        public static void WriteNumber(int number, BinaryWriter writer)
        {
            byte numberSize = NumberSize.GetNumberSize(number);
            writer.Write(numberSize);
            if (numberSize > 0)
            {
                switch (numberSize)
                {
                    case 1:
                        writer.Write((byte) number);
                        return;

                    case 2:
                        writer.Write((short) number);
                        return;
                }
                writer.Write(number);
            }
        }

        public static void WriteNumbers(int[] numbers, BinaryWriter writer)
        {
            WriteNumber(numbers.Length, writer);
            foreach (int num in numbers)
            {
                WriteNumber(num, writer);
            }
        }

        public static void WriteString(string text, BinaryWriter writer)
        {
            if (string.IsNullOrEmpty(text))
            {
                writer.Write(false);
            }
            else
            {
                writer.Write(true);
                writer.Write(text);
            }
        }

        public static void WriteValue(object value, BinaryWriter writer)
        {
            if (value == null)
            {
                writer.Write(false);
            }
            else
            {
                writer.Write(true);
                writeValueCore(value, writer);
            }
        }

        private static void writeValueCore(object value, BinaryWriter writer)
        {
            if (value == null)
            {
                throw new ArgumentNullException("value", "Written data can not be null.");
            }
            Type type = value.GetType();
            if (type == typeof(byte[]))
            {
                writeArrayOfByte((byte[]) value, writer);
            }
            else if (type == typeof(string))
            {
                writer.Write((string) value);
            }
            else if (type == typeof(bool))
            {
                writer.Write((bool) value);
            }
            else if (type == typeof(byte))
            {
                writer.Write((byte) value);
            }
            else if (type == typeof(char))
            {
                writer.Write((char) value);
            }
            else if (type == typeof(DateTime))
            {
                DateTime time = (DateTime) value;
                writer.Write(time.Ticks);
            }
            else if (type == typeof(Guid))
            {
                writer.Write(((Guid) value).ToByteArray());
            }
            else if (type == typeof(decimal))
            {
                writeDecimal((decimal) value, writer);
            }
            else if (type == typeof(double))
            {
                writer.Write((double) value);
            }
            else if (type == typeof(short))
            {
                writer.Write((short) value);
            }
            else if (type == typeof(int))
            {
                writer.Write((int) value);
            }
            else if (type == typeof(long))
            {
                writer.Write((long) value);
            }
            else if (type == typeof(sbyte))
            {
                writer.Write((sbyte) value);
            }
            else if (type == typeof(float))
            {
                writer.Write((float) value);
            }
            else if (type == typeof(ushort))
            {
                writer.Write((ushort) value);
            }
            else if (type == typeof(uint))
            {
                writer.Write((uint) value);
            }
            else if (type == typeof(ulong))
            {
                writer.Write((ulong) value);
            }
            else if (type == typeof(TimeSpan))
            {
                TimeSpan span = (TimeSpan) value;
                writer.Write(span.Ticks);
            }
            else if (type.IsEnum)
            {
                writer.Write(Convert.ToInt32(value));
            }
            else
            {
                if (!isType(type))
                {
                    throw new InvalidOperationException(string.Format("Unknown simple type: {0}", new object[] { type.FullName }));
                }
                writer.Write(((Type) value).AssemblyQualifiedName);
            }
        }
    }
}

