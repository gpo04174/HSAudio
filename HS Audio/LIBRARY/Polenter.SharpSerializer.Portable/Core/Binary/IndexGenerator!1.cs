﻿namespace Polenter.Serialization.Core.Binary
{
    using System;
    using System.Collections.Generic;

    internal sealed class IndexGenerator<T>
    {
        private readonly List<T> _items;

        public IndexGenerator()
        {
            this._items = new List<T>();
        }

        public int GetIndexOfItem(T item)
        {
            int index = this._items.IndexOf(item);
            if (index > -1)
            {
                return index;
            }
            this._items.Add(item);
            return (this._items.Count - 1);
        }

        public IList<T> Items
        {
            get
            {
                return this._items;
            }
        }
    }
}

