﻿namespace Polenter.Serialization.Core.Binary
{
    using System;

    public static class SubElements
    {
        public const byte Dimension = 0x33;
        public const byte Dimensions = 0x34;
        public const byte Eof = 0xff;
        public const byte Item = 0x35;
        public const byte Items = 0x36;
        public const byte Properties = 0x37;
        public const byte Unknown = 0xfe;
    }
}

