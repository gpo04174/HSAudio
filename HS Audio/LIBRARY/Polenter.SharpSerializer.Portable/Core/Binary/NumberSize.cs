﻿namespace Polenter.Serialization.Core.Binary
{
    using System;

    public static class NumberSize
    {
        public const byte B1 = 1;
        public const byte B2 = 2;
        public const byte B4 = 4;
        public const byte Zero = 0;

        public static byte GetNumberSize(int value)
        {
            if (value == 0)
            {
                return 0;
            }
            if ((value > 0x7fff) || (value < -32768))
            {
                return 4;
            }
            if ((value >= 0) && (value <= 0xff))
            {
                return 1;
            }
            return 2;
        }
    }
}

