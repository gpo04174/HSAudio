﻿namespace Polenter.Serialization.Core
{
    using System;
    using System.Runtime.CompilerServices;

    public sealed class ReferenceInfo
    {
        [CompilerGenerated]
        private int _Count;
        [CompilerGenerated]
        private int _Id;
        [CompilerGenerated]
        private bool _IsProcessed;

        public ReferenceInfo()
        {
            this.Count = 1;
        }

        public override string ToString()
        {
            return string.Format("{0}, Count={1}, Id={2}, IsProcessed={3}", new object[] { base.GetType().Name, this.Count, this._Id, this._IsProcessed });
        }

        public int Count
        {
            [CompilerGenerated]
            get
            {
                return this._Count;
            }
            [CompilerGenerated]
            set
            {
                this._Count = value;
            }
        }

        public int Id
        {
            [CompilerGenerated]
            get
            {
                return this._Id;
            }
            [CompilerGenerated]
            set
            {
                this._Id = value;
            }
        }

        public bool IsProcessed
        {
            [CompilerGenerated]
            get
            {
                return this._IsProcessed;
            }
            [CompilerGenerated]
            set
            {
                this._IsProcessed = value;
            }
        }
    }
}

