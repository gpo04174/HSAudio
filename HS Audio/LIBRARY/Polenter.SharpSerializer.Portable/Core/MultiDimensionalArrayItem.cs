﻿namespace Polenter.Serialization.Core
{
    using System;
    using System.Runtime.CompilerServices;

    public sealed class MultiDimensionalArrayItem
    {
        [CompilerGenerated]
        private int[] _Indexes;
        [CompilerGenerated]
        private Property _Value;

        public MultiDimensionalArrayItem(int[] indexes, Property value)
        {
            this.Indexes = indexes;
            this.Value = value;
        }

        public int[] Indexes
        {
            [CompilerGenerated]
            get
            {
                return this._Indexes;
            }
            [CompilerGenerated]
            set
            {
                this._Indexes = value;
            }
        }

        public Property Value
        {
            [CompilerGenerated]
            get
            {
                return this._Value;
            }
            [CompilerGenerated]
            set
            {
                this._Value = value;
            }
        }
    }
}

