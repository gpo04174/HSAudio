﻿namespace Polenter.Serialization.Core
{
    using Polenter.Serialization.Advanced;
    using Polenter.Serialization.Advanced.Serializing;
    using Polenter.Serialization.Advanced.Xml;
    using System;
    using System.Globalization;
    using System.Text;
    using System.Xml;

    internal static class DefaultInitializer
    {
        public static ISimpleValueConverter GetSimpleValueConverter(CultureInfo cultureInfo, ITypeNameConverter typeNameConverter)
        {
            return new SimpleValueConverter(cultureInfo, typeNameConverter);
        }

        public static ITypeNameConverter GetTypeNameConverter(bool includeAssemblyVersion, bool includeCulture, bool includePublicKeyToken)
        {
            return new TypeNameConverter(includeAssemblyVersion, includeCulture, includePublicKeyToken);
        }

        public static XmlReaderSettings GetXmlReaderSettings()
        {
            XmlReaderSettings settings = new XmlReaderSettings();
            settings.IgnoreComments = true;
            settings.IgnoreWhitespace = true;
            return settings;
        }

        public static XmlWriterSettings GetXmlWriterSettings()
        {
            return GetXmlWriterSettings(Encoding.UTF8);
        }

        public static XmlWriterSettings GetXmlWriterSettings(Encoding encoding)
        {
            XmlWriterSettings settings = new XmlWriterSettings();
            settings.Encoding = encoding;
            settings.Indent = true;
            settings.OmitXmlDeclaration = true;
            return settings;
        }
    }
}

