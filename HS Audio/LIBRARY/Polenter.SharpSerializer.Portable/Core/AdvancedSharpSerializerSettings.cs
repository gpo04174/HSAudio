﻿namespace Polenter.Serialization.Core
{
    using Polenter.Serialization;
    using Polenter.Serialization.Advanced;
    using Polenter.Serialization.Advanced.Serializing;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    public class AdvancedSharpSerializerSettings
    {
        private IList<Type> _attributesToIgnore;
        private Polenter.Serialization.Advanced.PropertiesToIgnore _propertiesToIgnore;
        [CompilerGenerated]
        private string _RootName;
        [CompilerGenerated]
        private ITypeNameConverter _TypeNameConverter;

        public AdvancedSharpSerializerSettings()
        {
            this.AttributesToIgnore.Add(typeof(ExcludeFromSerializationAttribute));
            this.RootName = "Root";
        }

        public IList<Type> AttributesToIgnore
        {
            get
            {
                if (this._attributesToIgnore == null)
                {
                    this._attributesToIgnore = new List<Type>();
                }
                return this._attributesToIgnore;
            }
            set
            {
                this._attributesToIgnore = value;
            }
        }

        public Polenter.Serialization.Advanced.PropertiesToIgnore PropertiesToIgnore
        {
            get
            {
                if (this._propertiesToIgnore == null)
                {
                    this._propertiesToIgnore = new Polenter.Serialization.Advanced.PropertiesToIgnore();
                }
                return this._propertiesToIgnore;
            }
            set
            {
                this._propertiesToIgnore = value;
            }
        }

        public string RootName
        {
            [CompilerGenerated]
            get
            {
                return this._RootName;
            }
            [CompilerGenerated]
            set
            {
                this._RootName = value;
            }
        }

        public ITypeNameConverter TypeNameConverter
        {
            [CompilerGenerated]
            get
            {
                return this._TypeNameConverter;
            }
            [CompilerGenerated]
            set
            {
                this._TypeNameConverter = value;
            }
        }
    }
}

