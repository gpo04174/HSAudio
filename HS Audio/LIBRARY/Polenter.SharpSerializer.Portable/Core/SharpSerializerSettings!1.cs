﻿namespace Polenter.Serialization.Core
{
    using System;
    using System.Runtime.CompilerServices;

    public abstract class SharpSerializerSettings<T> where T: AdvancedSharpSerializerSettings, new()
    {
        private T _advancedSettings;
        [CompilerGenerated]
        private bool _IncludeAssemblyVersionInTypeName;
        [CompilerGenerated]
        private bool _IncludeCultureInTypeName;
        [CompilerGenerated]
        private bool _IncludePublicKeyTokenInTypeName;

        protected SharpSerializerSettings()
        {
            this.IncludeAssemblyVersionInTypeName = true;
            this.IncludeCultureInTypeName = true;
            this.IncludePublicKeyTokenInTypeName = true;
        }

        public T AdvancedSettings
        {
            get
            {
                if (this._advancedSettings == default(T))
                {
                    this._advancedSettings = Activator.CreateInstance<T>();
                }
                return this._advancedSettings;
            }
            set
            {
                this._advancedSettings = value;
            }
        }

        public bool IncludeAssemblyVersionInTypeName
        {
            [CompilerGenerated]
            get
            {
                return this._IncludeAssemblyVersionInTypeName;
            }
            [CompilerGenerated]
            set
            {
                this._IncludeAssemblyVersionInTypeName = value;
            }
        }

        public bool IncludeCultureInTypeName
        {
            [CompilerGenerated]
            get
            {
                return this._IncludeCultureInTypeName;
            }
            [CompilerGenerated]
            set
            {
                this._IncludeCultureInTypeName = value;
            }
        }

        public bool IncludePublicKeyTokenInTypeName
        {
            [CompilerGenerated]
            get
            {
                return this._IncludePublicKeyTokenInTypeName;
            }
            [CompilerGenerated]
            set
            {
                this._IncludePublicKeyTokenInTypeName = value;
            }
        }
    }
}

