﻿namespace Polenter.Serialization.Core
{
    using System;

    public enum PropertyArt
    {
        Unknown,
        Simple,
        Complex,
        Collection,
        Dictionary,
        SingleDimensionalArray,
        MultiDimensionalArray,
        Null,
        Reference
    }
}

