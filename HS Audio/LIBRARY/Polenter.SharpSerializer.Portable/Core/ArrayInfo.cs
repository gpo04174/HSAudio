﻿namespace Polenter.Serialization.Core
{
    using System;
    using System.Collections.Generic;

    public sealed class ArrayInfo
    {
        private IList<DimensionInfo> _dimensionInfos;

        public IList<DimensionInfo> DimensionInfos
        {
            get
            {
                if (this._dimensionInfos == null)
                {
                    this._dimensionInfos = new List<DimensionInfo>();
                }
                return this._dimensionInfos;
            }
            set
            {
                this._dimensionInfos = value;
            }
        }
    }
}

