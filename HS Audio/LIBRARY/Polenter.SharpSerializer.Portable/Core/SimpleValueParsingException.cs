﻿namespace Polenter.Serialization.Core
{
    using System;

    public class SimpleValueParsingException : Exception
    {
        public SimpleValueParsingException()
        {
        }

        public SimpleValueParsingException(string message) : base(message)
        {
        }

        public SimpleValueParsingException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}

