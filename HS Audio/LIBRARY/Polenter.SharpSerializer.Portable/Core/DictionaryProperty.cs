﻿namespace Polenter.Serialization.Core
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    public sealed class DictionaryProperty : ComplexProperty
    {
        private IList<KeyValueItem> _items;
        [CompilerGenerated]
        private Type _KeyType;
        [CompilerGenerated]
        private Type _ValueType;

        public DictionaryProperty(string name, Type type) : base(name, type)
        {
        }

        protected override PropertyArt GetPropertyArt()
        {
            return PropertyArt.Dictionary;
        }

        public override void MakeFlatCopyFrom(ReferenceTargetProperty source)
        {
            DictionaryProperty property = source as DictionaryProperty;
            if (property == null)
            {
                throw new InvalidCastException(string.Format("Invalid property type to make a flat copy. Expected {0}, current {1}", new object[] { typeof(DictionaryProperty), source.GetType() }));
            }
            base.MakeFlatCopyFrom(source);
            this.KeyType = property.KeyType;
            this.ValueType = property.ValueType;
            this.Items = property.Items;
        }

        public IList<KeyValueItem> Items
        {
            get
            {
                if (this._items == null)
                {
                    this._items = new List<KeyValueItem>();
                }
                return this._items;
            }
            set
            {
                this._items = value;
            }
        }

        public Type KeyType
        {
            [CompilerGenerated]
            get
            {
                return this._KeyType;
            }
            [CompilerGenerated]
            set
            {
                this._KeyType = value;
            }
        }

        public Type ValueType
        {
            [CompilerGenerated]
            get
            {
                return this._ValueType;
            }
            [CompilerGenerated]
            set
            {
                this._ValueType = value;
            }
        }
    }
}

