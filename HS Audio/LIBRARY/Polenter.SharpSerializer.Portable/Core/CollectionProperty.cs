﻿namespace Polenter.Serialization.Core
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    public sealed class CollectionProperty : ComplexProperty
    {
        private IList<Property> _items;
        [CompilerGenerated]
        private Type _ElementType;

        public CollectionProperty(string name, Type type) : base(name, type)
        {
        }

        protected override PropertyArt GetPropertyArt()
        {
            return PropertyArt.Collection;
        }

        public override void MakeFlatCopyFrom(ReferenceTargetProperty source)
        {
            CollectionProperty property = source as CollectionProperty;
            if (property == null)
            {
                throw new InvalidCastException(string.Format("Invalid property type to make a flat copy. Expected {0}, current {1}", new object[] { typeof(CollectionProperty), source.GetType() }));
            }
            base.MakeFlatCopyFrom(source);
            this.ElementType = property.ElementType;
            this.Items = property.Items;
        }

        public Type ElementType
        {
            [CompilerGenerated]
            get
            {
                return this._ElementType;
            }
            [CompilerGenerated]
            set
            {
                this._ElementType = value;
            }
        }

        public IList<Property> Items
        {
            get
            {
                if (this._items == null)
                {
                    this._items = new List<Property>();
                }
                return this._items;
            }
            set
            {
                this._items = value;
            }
        }
    }
}

