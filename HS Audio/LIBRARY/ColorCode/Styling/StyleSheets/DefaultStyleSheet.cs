﻿namespace ColorCode.Styling.StyleSheets
{
    using ColorCode;
    using System;
    using System.Drawing;

    public class DefaultStyleSheet : IStyleSheet
    {
        public static readonly Color DullRed = Color.FromArgb(0xa3, 0x15, 0x15);
        private static readonly StyleDictionary styles;

        static DefaultStyleSheet()
        {
            StyleDictionary dictionary = new StyleDictionary();
            Style item = new Style("Plain Text") {
                Foreground = Color.Black,
                Background = Color.White,
                CssClassName = "plainText"
            };
            dictionary.Add(item);
            Style style2 = new Style("HTML Server-Side Script") {
                Background = Color.Yellow,
                CssClassName = "htmlServerSideScript"
            };
            dictionary.Add(style2);
            Style style3 = new Style("HTML Comment") {
                Foreground = Color.Green,
                CssClassName = "htmlComment"
            };
            dictionary.Add(style3);
            Style style4 = new Style("Html Tag Delimiter") {
                Foreground = Color.Blue,
                CssClassName = "htmlTagDelimiter"
            };
            dictionary.Add(style4);
            Style style5 = new Style("HTML Element ScopeName") {
                Foreground = DullRed,
                CssClassName = "htmlElementName"
            };
            dictionary.Add(style5);
            Style style6 = new Style("HTML Attribute ScopeName") {
                Foreground = Color.Red,
                CssClassName = "htmlAttributeName"
            };
            dictionary.Add(style6);
            Style style7 = new Style("HTML Attribute Value") {
                Foreground = Color.Blue,
                CssClassName = "htmlAttributeValue"
            };
            dictionary.Add(style7);
            Style style8 = new Style("HTML Operator") {
                Foreground = Color.Blue,
                CssClassName = "htmlOperator"
            };
            dictionary.Add(style8);
            Style style9 = new Style("Comment") {
                Foreground = Color.Green,
                CssClassName = "comment"
            };
            dictionary.Add(style9);
            Style style10 = new Style("XML Doc Tag") {
                Foreground = Color.Gray,
                CssClassName = "xmlDocTag"
            };
            dictionary.Add(style10);
            Style style11 = new Style("XML Doc Comment") {
                Foreground = Color.Green,
                CssClassName = "xmlDocComment"
            };
            dictionary.Add(style11);
            Style style12 = new Style("String") {
                Foreground = DullRed,
                CssClassName = "string"
            };
            dictionary.Add(style12);
            Style style13 = new Style("String (C# @ Verbatim)") {
                Foreground = DullRed,
                CssClassName = "stringCSharpVerbatim"
            };
            dictionary.Add(style13);
            Style style14 = new Style("Keyword") {
                Foreground = Color.Blue,
                CssClassName = "keyword"
            };
            dictionary.Add(style14);
            Style style15 = new Style("Preprocessor Keyword") {
                Foreground = Color.Blue,
                CssClassName = "preprocessorKeyword"
            };
            dictionary.Add(style15);
            Style style16 = new Style("HTML Entity") {
                Foreground = Color.Red,
                CssClassName = "htmlEntity"
            };
            dictionary.Add(style16);
            Style style17 = new Style("XML Attribute") {
                Foreground = Color.Red,
                CssClassName = "xmlAttribute"
            };
            dictionary.Add(style17);
            Style style18 = new Style("XML Attribute Quotes") {
                Foreground = Color.Black,
                CssClassName = "xmlAttributeQuotes"
            };
            dictionary.Add(style18);
            Style style19 = new Style("XML Attribute Value") {
                Foreground = Color.Blue,
                CssClassName = "xmlAttributeValue"
            };
            dictionary.Add(style19);
            Style style20 = new Style("XML CData Section") {
                Foreground = Color.Gray,
                CssClassName = "xmlCDataSection"
            };
            dictionary.Add(style20);
            Style style21 = new Style("XML Comment") {
                Foreground = Color.Green,
                CssClassName = "xmlComment"
            };
            dictionary.Add(style21);
            Style style22 = new Style("XML Delimiter") {
                Foreground = Color.Blue,
                CssClassName = "xmlDelimiter"
            };
            dictionary.Add(style22);
            Style style23 = new Style("XML Name") {
                Foreground = DullRed,
                CssClassName = "xmlName"
            };
            dictionary.Add(style23);
            Style style24 = new Style("Class Name") {
                Foreground = Color.MediumTurquoise,
                CssClassName = "className"
            };
            dictionary.Add(style24);
            Style style25 = new Style("CSS Selector") {
                Foreground = DullRed,
                CssClassName = "cssSelector"
            };
            dictionary.Add(style25);
            Style style26 = new Style("CSS Property Name") {
                Foreground = Color.Red,
                CssClassName = "cssPropertyName"
            };
            dictionary.Add(style26);
            Style style27 = new Style("CSS Property Value") {
                Foreground = Color.Blue,
                CssClassName = "cssPropertyValue"
            };
            dictionary.Add(style27);
            Style style28 = new Style("SQL System Function") {
                Foreground = Color.Magenta,
                CssClassName = "sqlSystemFunction"
            };
            dictionary.Add(style28);
            Style style29 = new Style("PowerShell PowerShellAttribute") {
                Foreground = Color.PowderBlue,
                CssClassName = "powershellAttribute"
            };
            dictionary.Add(style29);
            Style style30 = new Style("PowerShell Operator") {
                Foreground = Color.Gray,
                CssClassName = "powershellOperator"
            };
            dictionary.Add(style30);
            Style style31 = new Style("PowerShell Type") {
                Foreground = Color.Teal,
                CssClassName = "powershellType"
            };
            dictionary.Add(style31);
            Style style32 = new Style("PowerShell Variable") {
                Foreground = Color.OrangeRed,
                CssClassName = "powershellVariable"
            };
            dictionary.Add(style32);
            styles = dictionary;
        }

        public string Name
        {
            get
            {
                return "DefaultStyleSheet";
            }
        }

        public StyleDictionary Styles
        {
            get
            {
                return styles;
            }
        }
    }
}

