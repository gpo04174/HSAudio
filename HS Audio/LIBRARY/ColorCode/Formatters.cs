﻿namespace ColorCode
{
    using ColorCode.Formatting;
    using System;

    public static class Formatters
    {
        public static IFormatter Default
        {
            get
            {
                return new HtmlFormatter();
            }
        }
    }
}

