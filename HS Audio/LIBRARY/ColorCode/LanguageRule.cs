﻿namespace ColorCode
{
    using ColorCode.Common;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    public class LanguageRule
    {
        public LanguageRule(string regex, IDictionary<int, string> captures)
        {
            Guard.ArgNotNullAndNotEmpty(regex, "regex");
            Guard.EnsureParameterIsNotNullAndNotEmpty<int, string>(captures, "captures");
            this.Regex = regex;
            this.Captures = captures;
        }

        public IDictionary<int, string> Captures { get; private set; }

        public string Regex { get; private set; }
    }
}

