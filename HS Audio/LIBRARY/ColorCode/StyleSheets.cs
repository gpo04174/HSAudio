﻿namespace ColorCode
{
    using ColorCode.Styling.StyleSheets;
    using System;

    public static class StyleSheets
    {
        public static IStyleSheet Default
        {
            get
            {
                return new DefaultStyleSheet();
            }
        }
    }
}

