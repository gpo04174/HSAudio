﻿namespace ColorCode
{
    public interface IStyleSheet
    {
        StyleDictionary Styles { get; }
    }
}

