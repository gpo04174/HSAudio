﻿namespace ColorCode
{
    using Parsing;
    using System;
    using System.Collections.Generic;
    using System.IO;

    public interface IFormatter
    {
        void Write(string parsedSourceCode, IList<Scope> scopes, IStyleSheet styleSheet, TextWriter textWriter);
        void WriteFooter(IStyleSheet styleSheet, ILanguage language, TextWriter textWriter);
        void WriteHeader(IStyleSheet styleSheet, ILanguage language, TextWriter textWriter);
    }
}

