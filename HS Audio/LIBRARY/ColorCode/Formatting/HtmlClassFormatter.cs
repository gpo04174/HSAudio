﻿namespace ColorCode.Formatting
{
    using ColorCode;
    using ColorCode.Common;
    using ColorCode.Parsing;
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Web;

    public class HtmlClassFormatter : IFormatter
    {
        private static void BuildSpanForCapturedStyle(Scope scope, IStyleSheet styleSheet, TextWriter writer)
        {
            string cssClassName = "";
            if (styleSheet.Styles.Contains(scope.Name))
            {
                Style style = styleSheet.Styles[scope.Name];
                cssClassName = style.CssClassName;
            }
            WriteElementStart("span", cssClassName, writer);
        }

        private static void GetStyleInsertionsForCapturedStyle(Scope scope, ICollection<TextInsertion> styleInsertions)
        {
            TextInsertion item = new TextInsertion {
                Index = scope.Index,
                Scope = scope
            };
            styleInsertions.Add(item);
            foreach (Scope scope2 in scope.Children)
            {
                GetStyleInsertionsForCapturedStyle(scope2, styleInsertions);
            }
            TextInsertion insertion2 = new TextInsertion {
                Index = scope.Index + scope.Length,
                Text = "</span>"
            };
            styleInsertions.Add(insertion2);
        }

        public void Write(string parsedSourceCode, IList<Scope> scopes, IStyleSheet styleSheet, TextWriter textWriter)
        {
            List<TextInsertion> styleInsertions = new List<TextInsertion>();
            foreach (Scope scope in scopes)
            {
                GetStyleInsertionsForCapturedStyle(scope, styleInsertions);
            }
            styleInsertions.SortStable<TextInsertion>((x, y) => x.Index.CompareTo(y.Index));
            int startIndex = 0;
            foreach (TextInsertion insertion in styleInsertions)
            {
                textWriter.Write(HttpUtility.HtmlEncode(parsedSourceCode.Substring(startIndex, insertion.Index - startIndex)));
                if (string.IsNullOrEmpty(insertion.Text))
                {
                    BuildSpanForCapturedStyle(insertion.Scope, styleSheet, textWriter);
                }
                else
                {
                    textWriter.Write(insertion.Text);
                }
                startIndex = insertion.Index;
            }
            textWriter.Write(HttpUtility.HtmlEncode(parsedSourceCode.Substring(startIndex)));
        }

        private static void WriteElementEnd(string elementName, TextWriter writer)
        {
            writer.Write("</{0}>", elementName);
        }

        private static void WriteElementStart(string elementName, TextWriter writer)
        {
            WriteElementStart(elementName, "", writer);
        }

        private static void WriteElementStart(string elementName, string cssClassName, TextWriter writer)
        {
            writer.Write("<{0}", elementName);
            if (!string.IsNullOrEmpty(cssClassName))
            {
                writer.Write(" class=\"{0}\"", cssClassName);
            }
            writer.Write(">");
        }

        public void WriteFooter(IStyleSheet styleSheet, ILanguage language, TextWriter textWriter)
        {
            Guard.ArgNotNull(styleSheet, "styleSheet");
            Guard.ArgNotNull(language, "language");
            Guard.ArgNotNull(textWriter, "textWriter");
            textWriter.WriteLine();
            WriteHeaderPreEnd(textWriter);
            WriteHeaderDivEnd(textWriter);
        }

        public void WriteHeader(IStyleSheet styleSheet, ILanguage language, TextWriter textWriter)
        {
            Guard.ArgNotNull(styleSheet, "styleSheet");
            Guard.ArgNotNull(language, "language");
            Guard.ArgNotNull(textWriter, "textWriter");
            WriteHeaderDivStart(styleSheet, language, textWriter);
            WriteHeaderPreStart(textWriter);
            textWriter.WriteLine();
        }

        private static void WriteHeaderDivEnd(TextWriter writer)
        {
            WriteElementEnd("div", writer);
        }

        private static void WriteHeaderDivStart(IStyleSheet styleSheet, ILanguage language, TextWriter writer)
        {
            WriteElementStart("div", language.CssClassName, writer);
        }

        private static void WriteHeaderPreEnd(TextWriter writer)
        {
            WriteElementEnd("pre", writer);
        }

        private static void WriteHeaderPreStart(TextWriter writer)
        {
            WriteElementStart("pre", writer);
        }
    }
}

