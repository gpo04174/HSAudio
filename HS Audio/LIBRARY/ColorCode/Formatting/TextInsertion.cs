﻿namespace ColorCode.Formatting
{
    using ColorCode.Parsing;
    using System;
    using System.Runtime.CompilerServices;

    public class TextInsertion
    {
        public virtual int Index { get; set; }

        public virtual ColorCode.Parsing.Scope Scope { get; set; }

        public virtual string Text { get; set; }
    }
}

