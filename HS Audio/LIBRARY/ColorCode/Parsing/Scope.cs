﻿namespace ColorCode.Parsing
{
    using ColorCode.Common;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    public class Scope
    {
        public Scope(string name, int index, int length)
        {
            Guard.ArgNotNullAndNotEmpty(name, "name");
            this.Name = name;
            this.Index = index;
            this.Length = length;
            this.Children = new List<Scope>();
        }

        public void AddChild(Scope childScope)
        {
            if (childScope.Parent != null)
            {
                throw new InvalidOperationException("The child scope already has a parent.");
            }
            childScope.Parent = this;
            this.Children.Add(childScope);
        }

        public IList<Scope> Children { get; set; }

        public int Index { get; set; }

        public int Length { get; set; }

        public string Name { get; set; }

        public Scope Parent { get; set; }
    }
}

