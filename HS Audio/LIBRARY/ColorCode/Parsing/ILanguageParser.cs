﻿namespace ColorCode.Parsing
{
    using ColorCode;
    using System;

    public interface ILanguageParser
    {
        void Parse(string sourceCode, ILanguage language, System.Action<string, IList<Scope>> parseHandler);
    }
}

