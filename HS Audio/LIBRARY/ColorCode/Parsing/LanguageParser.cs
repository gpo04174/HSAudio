﻿namespace ColorCode.Parsing
{
    using ColorCode;
    using ColorCode.Common;
    using ColorCode.Compilation;
    using System;
    using System.Collections.Generic;
    using System.Text.RegularExpressions;

    public class LanguageParser : ILanguageParser
    {
        private readonly ILanguageCompiler languageCompiler;
        private readonly ILanguageRepository languageRepository;

        public LanguageParser(ILanguageCompiler languageCompiler, ILanguageRepository languageRepository)
        {
            this.languageCompiler = languageCompiler;
            this.languageRepository = languageRepository;
        }

        private static void AddScopeToNestedScopes(Scope scope, ref Scope currentScope, ICollection<Scope> capturedStyleTree)
        {
            if ((scope.Index >= currentScope.Index) && ((scope.Index + scope.Length) <= (currentScope.Index + currentScope.Length)))
            {
                currentScope.AddChild(scope);
                currentScope = scope;
            }
            else
            {
                currentScope = currentScope.Parent;
                if (currentScope != null)
                {
                    AddScopeToNestedScopes(scope, ref currentScope, capturedStyleTree);
                }
                else
                {
                    capturedStyleTree.Add(scope);
                }
            }
        }

        private void AppendCapturedStylesForNestedLanguage(Capture regexCapture, int offset, string nestedLanguageId, ICollection<Scope> capturedStyles)
        {
            ILanguage language = this.languageRepository.FindById(nestedLanguageId);
            if (language == null)
            {
                throw new InvalidOperationException("The nested language was not found in the language repository.");
            }
            CompiledLanguage compiledLanguage = this.languageCompiler.Compile(language);
            Match regexMatch = compiledLanguage.Regex.Match(regexCapture.Value, 0, regexCapture.Value.Length);
            if (regexMatch.Success)
            {
                while (regexMatch.Success)
                {
                    List<Scope> list2 = CreateCapturedStyleTree(this.GetCapturedStyles(regexMatch, 0, compiledLanguage));
                    foreach (Scope scope in list2)
                    {
                        IncreaseCapturedStyleIndicies(list2, offset);
                        capturedStyles.Add(scope);
                    }
                    regexMatch = regexMatch.NextMatch();
                }
            }
        }

        private void AppendCapturedStylesForRegexCapture(Capture regexCapture, int currentIndex, string styleName, ICollection<Scope> capturedStyles)
        {
            if (styleName.StartsWith("&"))
            {
                string nestedLanguageId = styleName.Substring(1);
                this.AppendCapturedStylesForNestedLanguage(regexCapture, regexCapture.Index - currentIndex, nestedLanguageId, capturedStyles);
            }
            else
            {
                capturedStyles.Add(new Scope(styleName, regexCapture.Index - currentIndex, regexCapture.Length));
            }
        }

        private static List<Scope> CreateCapturedStyleTree(IList<Scope> capturedStyles)
        {
            capturedStyles.SortStable<Scope>((x, y) => x.Index.CompareTo(y.Index));
            List<Scope> capturedStyleTree = new List<Scope>(capturedStyles.Count);
            Scope currentScope = null;
            foreach (Scope scope2 in capturedStyles)
            {
                if (currentScope == null)
                {
                    capturedStyleTree.Add(scope2);
                    currentScope = scope2;
                }
                else
                {
                    AddScopeToNestedScopes(scope2, ref currentScope, capturedStyleTree);
                }
            }
            return capturedStyleTree;
        }

        private List<Scope> GetCapturedStyles(Match regexMatch, int currentIndex, CompiledLanguage compiledLanguage)
        {
            List<Scope> capturedStyles = new List<Scope>();
            for (int i = 0; i < regexMatch.Groups.Count; i++)
            {
                Group group = regexMatch.Groups[i];
                string str = compiledLanguage.Captures[i];
                if ((group.Length != 0) && !string.IsNullOrEmpty(str))
                {
                    foreach (Capture capture in group.Captures)
                    {
                        this.AppendCapturedStylesForRegexCapture(capture, currentIndex, str, capturedStyles);
                    }
                }
            }
            return capturedStyles;
        }

        private static void IncreaseCapturedStyleIndicies(IList<Scope> capturedStyles, int amountToIncrease)
        {
            for (int i = 0; i < capturedStyles.Count; i++)
            {
                Scope scope = capturedStyles[i];
                scope.Index += amountToIncrease;
                if (scope.Children.Count > 0)
                {
                    IncreaseCapturedStyleIndicies(scope.Children, amountToIncrease);
                }
            }
        }

        private void Parse(string sourceCode, CompiledLanguage compiledLanguage, System.Action<string, IList<Scope>> parseHandler)
        {
            Match regexMatch = compiledLanguage.Regex.Match(sourceCode);
            if (!regexMatch.Success)
            {
                parseHandler(sourceCode, new List<Scope>());
            }
            else
            {
                int startIndex = 0;
                while (regexMatch.Success)
                {
                    string str = sourceCode.Substring(startIndex, regexMatch.Index - startIndex);
                    if (!string.IsNullOrEmpty(str))
                    {
                        parseHandler(str, new List<Scope>());
                    }
                    string str2 = sourceCode.Substring(regexMatch.Index, regexMatch.Length);
                    if (!string.IsNullOrEmpty(str2))
                    {
                        List<Scope> list2 = CreateCapturedStyleTree(this.GetCapturedStyles(regexMatch, regexMatch.Index, compiledLanguage));
                        parseHandler(str2, list2);
                    }
                    startIndex = regexMatch.Index + regexMatch.Length;
                    regexMatch = regexMatch.NextMatch();
                }
                string str3 = sourceCode.Substring(startIndex);
                if (!string.IsNullOrEmpty(str3))
                {
                    parseHandler(str3, new List<Scope>());
                }
            }
        }

        public void Parse(string sourceCode, ILanguage language, System.Action<string, IList<Scope>> parseHandler)
        {
            if (!string.IsNullOrEmpty(sourceCode))
            {
                CompiledLanguage compiledLanguage = this.languageCompiler.Compile(language);
                this.Parse(sourceCode, compiledLanguage, parseHandler);
            }
        }
    }
}

