﻿namespace ColorCode
{
    using System;
    using System.Collections.ObjectModel;

    public class StyleDictionary : KeyedCollection<string, Style>
    {
        protected override string GetKeyForItem(Style item)
        {
            return item.ScopeName;
        }
    }
}

