﻿namespace ColorCode
{
    using System;
    using System.IO;

    public interface ICodeColorizer
    {
        string Colorize(string sourceCode, ILanguage language);
        void Colorize(string sourceCode, ILanguage language, TextWriter textWriter);
        void Colorize(string sourceCode, ILanguage language, IFormatter formatter, IStyleSheet styleSheet, TextWriter textWriter);
    }
}

