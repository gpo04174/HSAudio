﻿namespace ColorCode.Compilation
{
    using ColorCode.Common;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Text.RegularExpressions;

    public class CompiledLanguage
    {
        public CompiledLanguage(string id, string name, System.Text.RegularExpressions.Regex regex, IList<string> captures)
        {
            Guard.ArgNotNullAndNotEmpty(id, "id");
            Guard.ArgNotNullAndNotEmpty(name, "name");
            Guard.ArgNotNull(regex, "regex");
            Guard.ArgNotNullAndNotEmpty<string>(captures, "captures");
            this.Id = id;
            this.Name = name;
            this.Regex = regex;
            this.Captures = captures;
        }

        public override string ToString()
        {
            return this.Name;
        }

        public IList<string> Captures { get; set; }

        public string Id { get; set; }

        public string Name { get; set; }

        public System.Text.RegularExpressions.Regex Regex { get; set; }
    }
}

