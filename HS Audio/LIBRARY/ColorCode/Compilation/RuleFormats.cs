﻿namespace ColorCode.Compilation
{
    using System;

    public static class RuleFormats
    {
        public static string JavaScript = string.Format("(?xs)(<)(script)\r\n                                        {0}[\\s\\n]+({1})[\\s\\n]*(=)[\\s\\n]*(\"{2}\"){0}[\\s\\n]*(>)\r\n                                        (.*?)\r\n                                        (</)(script)(>)", "(?:[\\s\\n]+([a-z0-9-_]+)[\\s\\n]*(=)[\\s\\n]*([^\\s\\n\"']+?)\r\n                                           |[\\s\\n]+([a-z0-9-_]+)[\\s\\n]*(=)[\\s\\n]*(\"[^\\n]+?\")\r\n                                           |[\\s\\n]+([a-z0-9-_]+)[\\s\\n]*(=)[\\s\\n]*('[^\\n]+?')\r\n                                           |[\\s\\n]+([a-z0-9-_]+) )*", "type|language", "[^\n]*javascript");
        public static string ServerScript = string.Format("(?xs)(<)(script)\r\n                                        {0}[\\s\\n]+({1})[\\s\\n]*(=)[\\s\\n]*(\"{2}\"){0}[\\s\\n]*(>)\r\n                                        (.*?)\r\n                                        (</)(script)(>)", "(?:[\\s\\n]+([a-z0-9-_]+)[\\s\\n]*(=)[\\s\\n]*([^\\s\\n\"']+?)\r\n                                           |[\\s\\n]+([a-z0-9-_]+)[\\s\\n]*(=)[\\s\\n]*(\"[^\\n]+?\")\r\n                                           |[\\s\\n]+([a-z0-9-_]+)[\\s\\n]*(=)[\\s\\n]*('[^\\n]+?')\r\n                                           |[\\s\\n]+([a-z0-9-_]+) )*", "runat", "server");
    }
}

