﻿namespace ColorCode.Compilation
{
    using System;
    using System.Collections.Generic;

    public static class RuleCaptures
    {
        public static IDictionary<int, string> CSharpScript = BuildCaptures("c#");
        public static IDictionary<int, string> JavaScript = BuildCaptures("javascript");
        public static IDictionary<int, string> VbDotNetScript = BuildCaptures("vb.net");

        private static IDictionary<int, string> BuildCaptures(string languageId)
        {
            Dictionary<int, string> dictionary = new Dictionary<int, string>();
            dictionary.Add(1, "Html Tag Delimiter");
            dictionary.Add(2, "HTML Element ScopeName");
            dictionary.Add(3, "HTML Attribute ScopeName");
            dictionary.Add(4, "HTML Operator");
            dictionary.Add(5, "HTML Attribute Value");
            dictionary.Add(6, "HTML Attribute ScopeName");
            dictionary.Add(7, "HTML Operator");
            dictionary.Add(8, "HTML Attribute Value");
            dictionary.Add(9, "HTML Attribute ScopeName");
            dictionary.Add(10, "HTML Operator");
            dictionary.Add(11, "HTML Attribute Value");
            dictionary.Add(12, "HTML Attribute ScopeName");
            dictionary.Add(13, "HTML Attribute ScopeName");
            dictionary.Add(14, "HTML Operator");
            dictionary.Add(15, "HTML Attribute Value");
            dictionary.Add(0x10, "HTML Attribute ScopeName");
            dictionary.Add(0x11, "HTML Operator");
            dictionary.Add(0x12, "HTML Attribute Value");
            dictionary.Add(0x13, "HTML Attribute ScopeName");
            dictionary.Add(20, "HTML Operator");
            dictionary.Add(0x15, "HTML Attribute Value");
            dictionary.Add(0x16, "HTML Attribute ScopeName");
            dictionary.Add(0x17, "HTML Operator");
            dictionary.Add(0x18, "HTML Attribute Value");
            dictionary.Add(0x19, "HTML Attribute ScopeName");
            dictionary.Add(0x1a, "Html Tag Delimiter");
            dictionary.Add(0x1b, string.Format("{0}{1}", "&", languageId));
            dictionary.Add(0x1c, "Html Tag Delimiter");
            dictionary.Add(0x1d, "HTML Element ScopeName");
            dictionary.Add(30, "Html Tag Delimiter");
            return dictionary;
        }
    }
}

