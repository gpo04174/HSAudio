﻿namespace ColorCode.Compilation
{
    using ColorCode;
    using ColorCode.Common;
    using System;
    using System.Collections.Generic;
    using System.Runtime.InteropServices;
    using System.Text;
    using System.Text.RegularExpressions;
    using System.Threading;

    public class LanguageCompiler : ILanguageCompiler
    {
        private readonly Dictionary<string, CompiledLanguage> compiledLanguages;
        //private readonly ReaderWriterLockSlim compileLock;
        object o = new object();
        object o1 = new object();
        private static readonly Regex numberOfCapturesRegex = new Regex(@"(?x)(?<!\\)\((?!\?)", RegexOptions.Compiled);

        public LanguageCompiler(Dictionary<string, CompiledLanguage> compiledLanguages)
        {
            this.compiledLanguages = compiledLanguages;
            //this.compileLock = new ReaderWriterLockSlim();
        }

        public CompiledLanguage Compile(ILanguage language)
        {
            CompiledLanguage language2;
            Guard.ArgNotNull(language, "language");
            if (string.IsNullOrEmpty(language.Id))
            {
                throw new ArgumentException("The language identifier must not be null.", "language");
            }
            //this.compileLock.EnterReadLock();
            Monitor.Enter(o);
            try
            {
                if (this.compiledLanguages.ContainsKey(language.Id))
                {
                    return this.compiledLanguages[language.Id];
                }
            }
            finally
            {
                //this.compileLock.ExitReadLock();
                Monitor.Exit(o);
            }
            //this.compileLock.EnterUpgradeableReadLock();
            Monitor.Enter(o1);
            try
            {
                if (this.compiledLanguages.ContainsKey(language.Id))
                {
                    language2 = this.compiledLanguages[language.Id];
                }
                else
                {
                    //this.compileLock.EnterWriteLock();
                    Monitor.Enter(o);
                    try
                    {
                        if (string.IsNullOrEmpty(language.Name))
                        {
                            throw new ArgumentException("The language name must not be null or empty.", "language");
                        }
                        if ((language.Rules == null) || (language.Rules.Count == 0))
                        {
                            throw new ArgumentException("The language rules collection must not be empty.", "language");
                        }
                        language2 = CompileLanguage(language);
                        this.compiledLanguages.Add(language2.Id, language2);
                    }
                    finally
                    {
                        //this.compileLock.ExitWriteLock();
                        Monitor.Exit(o);
                    }
                }
            }
            finally
            {
                //this.compileLock.ExitUpgradeableReadLock();
                Monitor.Exit(o1);
            }
            return language2;
        }

        private static CompiledLanguage CompileLanguage(ILanguage language)
        {
            Regex regex;
            IList<string> list;
            string id = language.Id;
            string name = language.Name;
            CompileRules(language.Rules, out regex, out list);
            return new CompiledLanguage(id, name, regex, list);
        }

        private static void CompileRule(LanguageRule languageRule, StringBuilder regex, ICollection<string> captures, bool isFirstRule)
        {
            if (!isFirstRule)
            {
                regex.AppendLine();
                regex.AppendLine();
                regex.AppendLine("|");
                regex.AppendLine();
            }
            regex.AppendFormat("(?-xis)(?m)({0})(?x)", languageRule.Regex);
            int numberOfCaptures = GetNumberOfCaptures(languageRule.Regex);
            for (int i = 0; i <= numberOfCaptures; i++)
            {
                string item = null;
                foreach (int num3 in languageRule.Captures.Keys)
                {
                    if (i == num3)
                    {
                        item = languageRule.Captures[num3];
                        break;
                    }
                }
                captures.Add(item);
            }
        }

        private static void CompileRules(IList<LanguageRule> rules, out Regex regex, out IList<string> captures)
        {
            StringBuilder builder = new StringBuilder();
            captures = new List<string>();
            builder.AppendLine("(?x)");
            captures.Add(null);
            CompileRule(rules[0], builder, captures, true);
            for (int i = 1; i < rules.Count; i++)
            {
                CompileRule(rules[i], builder, captures, false);
            }
            regex = new Regex(builder.ToString());
        }

        private static int GetNumberOfCaptures(string regex)
        {
            return numberOfCapturesRegex.Matches(regex).Count;
        }
    }
}

