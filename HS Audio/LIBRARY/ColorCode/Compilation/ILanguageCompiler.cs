﻿namespace ColorCode.Compilation
{
    using ColorCode;

    public interface ILanguageCompiler
    {
        CompiledLanguage Compile(ILanguage language);
    }
}

