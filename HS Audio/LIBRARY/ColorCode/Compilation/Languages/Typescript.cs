﻿namespace ColorCode.Compilation.Languages
{
    using ColorCode;
    using System;
    using System.Collections.Generic;

    public class Typescript : ILanguage
    {
        public bool HasAlias(string lang)
        {
            string str = lang.ToLower();
            return ((str != null) && (str == "ts"));
        }

        public override string ToString()
        {
            return this.Name;
        }

        public string CssClassName
        {
            get
            {
                return "typescript";
            }
        }

        public string FirstLinePattern
        {
            get
            {
                return null;
            }
        }

        public string Id
        {
            get
            {
                return "typescript";
            }
        }

        public string Name
        {
            get
            {
                return "Typescript";
            }
        }

        public IList<LanguageRule> Rules
        {
            get
            {
                List<LanguageRule> list = new List<LanguageRule>();
                Dictionary<int, string> captures = new Dictionary<int, string>();
                captures.Add(0, "Comment");
                list.Add(new LanguageRule(@"/\*([^*]|[\r\n]|(\*+([^*/]|[\r\n])))*\*+/", captures));
                Dictionary<int, string> dictionary2 = new Dictionary<int, string>();
                dictionary2.Add(1, "Comment");
                list.Add(new LanguageRule(@"(//.*?)\r?$", dictionary2));
                Dictionary<int, string> dictionary3 = new Dictionary<int, string>();
                dictionary3.Add(0, "String");
                list.Add(new LanguageRule(@"'[^\n]*?'", dictionary3));
                Dictionary<int, string> dictionary4 = new Dictionary<int, string>();
                dictionary4.Add(0, "String");
                list.Add(new LanguageRule("\"[^\\n]*?\"", dictionary4));
                Dictionary<int, string> dictionary5 = new Dictionary<int, string>();
                dictionary5.Add(1, "Keyword");
                list.Add(new LanguageRule(@"\b(abstract|any|bool|boolean|break|byte|case|catch|char|class|const|constructor|continue|debugger|declare|default|delete|do|double|else|enum|export|extends|false|final|finally|float|for|function|goto|if|implements|import|in|instanceof|int|interface|long|module|native|new|number|null|package|private|protected|public|return|short|static|string|super|switch|synchronized|this|throw|throws|transient|true|try|typeof|var|void|volatile|while|with)\b", dictionary5));
                return list;
            }
        }
    }
}

