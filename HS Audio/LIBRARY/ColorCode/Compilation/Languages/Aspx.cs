﻿namespace ColorCode.Compilation.Languages
{
    using ColorCode;
    using ColorCode.Compilation;
    using System;
    using System.Collections.Generic;

    public class Aspx : ILanguage
    {
        public bool HasAlias(string lang)
        {
            return false;
        }

        public override string ToString()
        {
            return this.Name;
        }

        public string CssClassName
        {
            get
            {
                return "aspx";
            }
        }

        public string FirstLinePattern
        {
            get
            {
                return null;
            }
        }

        public string Id
        {
            get
            {
                return "aspx";
            }
        }

        public string Name
        {
            get
            {
                return "ASPX";
            }
        }

        public IList<LanguageRule> Rules
        {
            get
            {
                List<LanguageRule> list = new List<LanguageRule>();
                Dictionary<int, string> captures = new Dictionary<int, string>();
                captures.Add(1, "HTML Server-Side Script");
                captures.Add(2, "HTML Comment");
                captures.Add(3, "HTML Server-Side Script");
                list.Add(new LanguageRule("(?s)(<%)(--.*?--)(%>)", captures));
                Dictionary<int, string> dictionary2 = new Dictionary<int, string>();
                dictionary2.Add(0, "HTML Comment");
                list.Add(new LanguageRule("(?s)<!--.*?-->", dictionary2));
                Dictionary<int, string> dictionary3 = new Dictionary<int, string>();
                dictionary3.Add(1, "HTML Server-Side Script");
                dictionary3.Add(2, "Html Tag Delimiter");
                dictionary3.Add(3, "HTML Element ScopeName");
                dictionary3.Add(4, "HTML Attribute ScopeName");
                dictionary3.Add(5, "HTML Operator");
                dictionary3.Add(6, "HTML Attribute Value");
                dictionary3.Add(7, "HTML Server-Side Script");
                list.Add(new LanguageRule("(?i)(<%)(@)(?:\\s+([a-z0-9]+))*(?:\\s+([a-z0-9]+)(=)(\"[^\\n]*?\"))*\\s*?(%>)", dictionary3));
                Dictionary<int, string> dictionary4 = new Dictionary<int, string>();
                dictionary4.Add(1, "HTML Server-Side Script");
                dictionary4.Add(2, "HTML Server-Side Script");
                list.Add(new LanguageRule("(?s)(?:(<%=|<%)(?!=|@|--))(?:.*?)(%>)", dictionary4));
                Dictionary<int, string> dictionary5 = new Dictionary<int, string>();
                dictionary5.Add(1, "Html Tag Delimiter");
                dictionary5.Add(2, "HTML Element ScopeName");
                dictionary5.Add(3, "HTML Attribute ScopeName");
                dictionary5.Add(4, "HTML Attribute Value");
                dictionary5.Add(5, "Html Tag Delimiter");
                list.Add(new LanguageRule("(?is)(<!)(DOCTYPE)(?:\\s+([a-z0-9]+))*(?:\\s+(\"[^\"]*?\"))*(>)", dictionary5));
                list.Add(new LanguageRule(RuleFormats.JavaScript, RuleCaptures.JavaScript));
                Dictionary<int, string> dictionary6 = new Dictionary<int, string>();
                dictionary6.Add(1, "Html Tag Delimiter");
                dictionary6.Add(2, "HTML Element ScopeName");
                dictionary6.Add(3, "Html Tag Delimiter");
                dictionary6.Add(4, "HTML Element ScopeName");
                dictionary6.Add(5, "HTML Attribute ScopeName");
                dictionary6.Add(6, "HTML Operator");
                dictionary6.Add(7, "HTML Attribute Value");
                dictionary6.Add(8, "HTML Attribute ScopeName");
                dictionary6.Add(9, "HTML Operator");
                dictionary6.Add(10, "HTML Attribute Value");
                dictionary6.Add(11, "HTML Attribute ScopeName");
                dictionary6.Add(12, "HTML Operator");
                dictionary6.Add(13, "HTML Attribute Value");
                dictionary6.Add(14, "HTML Attribute ScopeName");
                dictionary6.Add(15, "Html Tag Delimiter");
                list.Add(new LanguageRule("(?xi)(</?)\r\n                                         (?: ([a-z][a-z0-9-]*)(:) )*\r\n                                         ([a-z][a-z0-9-_]*)\r\n                                         (?:\r\n                                            [\\s\\n]+([a-z0-9-_]+)[\\s\\n]*(=)[\\s\\n]*([^\\s\\n\"']+?)\r\n                                           |[\\s\\n]+([a-z0-9-_]+)[\\s\\n]*(=)[\\s\\n]*(\"[^\\n]+?\")\r\n                                           |[\\s\\n]+([a-z0-9-_]+)[\\s\\n]*(=)[\\s\\n]*('[^\\n]+?')\r\n                                           |[\\s\\n]+([a-z0-9-_]+) )*\r\n                                         [\\s\\n]*\r\n                                         (/?>)", dictionary6));
                Dictionary<int, string> dictionary7 = new Dictionary<int, string>();
                dictionary7.Add(0, "HTML Entity");
                list.Add(new LanguageRule("(?i)&[a-z0-9]+?;", dictionary7));
                return list;
            }
        }
    }
}

