﻿namespace ColorCode.Compilation.Languages
{
    using ColorCode;
    using System;
    using System.Collections.Generic;

    public class Php : ILanguage
    {
        public bool HasAlias(string lang)
        {
            string str = lang.ToLower();
            return ((str != null) && (((str == "php3") || (str == "php4")) || (str == "php5")));
        }

        public override string ToString()
        {
            return this.Name;
        }

        public string CssClassName
        {
            get
            {
                return "php";
            }
        }

        public string FirstLinePattern
        {
            get
            {
                return null;
            }
        }

        public string Id
        {
            get
            {
                return "php";
            }
        }

        public string Name
        {
            get
            {
                return "PHP";
            }
        }

        public IList<LanguageRule> Rules
        {
            get
            {
                List<LanguageRule> list = new List<LanguageRule>();
                Dictionary<int, string> captures = new Dictionary<int, string>();
                captures.Add(0, "Comment");
                list.Add(new LanguageRule(@"/\*([^*]|[\r\n]|(\*+([^*/]|[\r\n])))*\*+/", captures));
                Dictionary<int, string> dictionary2 = new Dictionary<int, string>();
                dictionary2.Add(1, "Comment");
                list.Add(new LanguageRule(@"(//.*?)\r?$", dictionary2));
                Dictionary<int, string> dictionary3 = new Dictionary<int, string>();
                dictionary3.Add(1, "Comment");
                list.Add(new LanguageRule(@"(#.*?)\r?$", dictionary3));
                Dictionary<int, string> dictionary4 = new Dictionary<int, string>();
                dictionary4.Add(0, "String");
                list.Add(new LanguageRule(@"'[^\n]*?(?<!\\)'", dictionary4));
                Dictionary<int, string> dictionary5 = new Dictionary<int, string>();
                dictionary5.Add(0, "String");
                list.Add(new LanguageRule("\"[^\\n]*?(?<!\\\\)\"", dictionary5));
                Dictionary<int, string> dictionary6 = new Dictionary<int, string>();
                dictionary6.Add(1, "Keyword");
                list.Add(new LanguageRule(@"\b(abstract|and|array|as|break|case|catch|cfunction|class|clone|const|continue|declare|default|do|else|elseif|enddeclare|endfor|endforeach|endif|endswitch|endwhile|exception|extends|fclose|file|final|for|foreach|function|global|goto|if|implements|interface|instanceof|mysqli_fetch_object|namespace|new|old_function|or|php_user_filter|private|protected|public|static|switch|throw|try|use|var|while|xor|__CLASS__|__DIR__|__FILE__|__FUNCTION__|__LINE__|__METHOD__|__NAMESPACE__|die|echo|empty|exit|eval|include|include_once|isset|list|require|require_once|return|print|unset)\b", dictionary6));
                return list;
            }
        }
    }
}

