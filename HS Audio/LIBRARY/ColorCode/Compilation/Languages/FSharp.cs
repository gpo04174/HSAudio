﻿namespace ColorCode.Compilation.Languages
{
    using ColorCode;
    using System;
    using System.Collections.Generic;

    public class FSharp : ILanguage
    {
        public bool HasAlias(string lang)
        {
            string str = lang.ToLower();
            return ((str != null) && ((str == "fs") || (str == "f#")));
        }

        public override string ToString()
        {
            return this.Name;
        }

        public string CssClassName
        {
            get
            {
                return "FSharp";
            }
        }

        public string FirstLinePattern
        {
            get
            {
                return null;
            }
        }

        public string Id
        {
            get
            {
                return "f#";
            }
        }

        public string Name
        {
            get
            {
                return "F#";
            }
        }

        public IList<LanguageRule> Rules
        {
            get
            {
                List<LanguageRule> list = new List<LanguageRule>();
                Dictionary<int, string> captures = new Dictionary<int, string>();
                captures.Add(0, "Comment");
                list.Add(new LanguageRule(@"\(\*([^*]|[\r\n]|(\*+([^*)]|[\r\n])))*\*+\)", captures));
                Dictionary<int, string> dictionary2 = new Dictionary<int, string>();
                dictionary2.Add(1, "XML Doc Tag");
                dictionary2.Add(2, "XML Doc Tag");
                dictionary2.Add(3, "XML Doc Comment");
                list.Add(new LanguageRule("(///)(?:\\s*?(<[/a-zA-Z0-9\\s\"=]+>))*([^\\r\\n]*)", dictionary2));
                Dictionary<int, string> dictionary3 = new Dictionary<int, string>();
                dictionary3.Add(1, "Comment");
                list.Add(new LanguageRule(@"(//.*?)\r?$", dictionary3));
                Dictionary<int, string> dictionary4 = new Dictionary<int, string>();
                dictionary4.Add(0, "String");
                list.Add(new LanguageRule(@"'[^\n]*?(?<!\\)'", dictionary4));
                Dictionary<int, string> dictionary5 = new Dictionary<int, string>();
                dictionary5.Add(0, "String (C# @ Verbatim)");
                list.Add(new LanguageRule("(?s)@\"(?:\"\"|.)*?\"(?!\")", dictionary5));
                Dictionary<int, string> dictionary6 = new Dictionary<int, string>();
                dictionary6.Add(0, "String (C# @ Verbatim)");
                list.Add(new LanguageRule("(?s)\"\"\"(?:\"\"|.)*?\"\"\"(?!\")", dictionary6));
                Dictionary<int, string> dictionary7 = new Dictionary<int, string>();
                dictionary7.Add(0, "String");
                list.Add(new LanguageRule("(?s)(\"[^\\n]*?(?<!\\\\)\")", dictionary7));
                Dictionary<int, string> dictionary8 = new Dictionary<int, string>();
                dictionary8.Add(1, "Preprocessor Keyword");
                list.Add(new LanguageRule(@"^\s*(\#else|\#endif|\#if).*?$", dictionary8));
                Dictionary<int, string> dictionary9 = new Dictionary<int, string>();
                dictionary9.Add(1, "Keyword");
                list.Add(new LanguageRule(@"\b(let!|use!|do!|yield!|return!)\s", dictionary9));
                Dictionary<int, string> dictionary10 = new Dictionary<int, string>();
                dictionary10.Add(1, "Keyword");
                list.Add(new LanguageRule(@"\b(abstract|and|as|assert|base|begin|class|default|delegate|do|done|downcast|downto|elif|else|end|exception|extern|false|finally|for|fun|function|global|if|in|inherit|inline|interface|internal|lazy|let|match|member|module|mutable|namespace|new|null|of|open|or|override|private|public|rec|return|sig|static|struct|then|to|true|try|type|upcast|use|val|void|when|while|with|yield|atomic|break|checked|component|const|constraint|constructor|continue|eager|fixed|fori|functor|include|measure|method|mixin|object|parallel|params|process|protected|pure|recursive|sealed|tailcall|trait|virtual|volatile)\b", dictionary10));
                Dictionary<int, string> dictionary11 = new Dictionary<int, string>();
                dictionary11.Add(2, "Keyword");
                list.Add(new LanguageRule(@"(\w|\s)(->)(\w|\s)", dictionary11));
                return list;
            }
        }
    }
}

