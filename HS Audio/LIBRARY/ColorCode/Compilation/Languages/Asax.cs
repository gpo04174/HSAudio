﻿namespace ColorCode.Compilation.Languages
{
    using ColorCode;
    using System;
    using System.Collections.Generic;

    public class Asax : ILanguage
    {
        public bool HasAlias(string lang)
        {
            return false;
        }

        public override string ToString()
        {
            return this.Name;
        }

        public string CssClassName
        {
            get
            {
                return "asax";
            }
        }

        public string FirstLinePattern
        {
            get
            {
                return null;
            }
        }

        public string Id
        {
            get
            {
                return "asax";
            }
        }

        public string Name
        {
            get
            {
                return "ASAX";
            }
        }

        public IList<LanguageRule> Rules
        {
            get
            {
                List<LanguageRule> list = new List<LanguageRule>();
                Dictionary<int, string> captures = new Dictionary<int, string>();
                captures.Add(1, "HTML Server-Side Script");
                captures.Add(2, "HTML Comment");
                captures.Add(3, "HTML Server-Side Script");
                list.Add(new LanguageRule("(<%)(--.*?--)(%>)", captures));
                Dictionary<int, string> dictionary2 = new Dictionary<int, string>();
                dictionary2.Add(1, string.Format("{0}{1}", "&", "c#"));
                list.Add(new LanguageRule("(?is)(?<=<%@.+?language=\"c\\#\".*?%>.*?<script.*?runat=\"server\">)(.*)(?=</script>)", dictionary2));
                Dictionary<int, string> dictionary3 = new Dictionary<int, string>();
                dictionary3.Add(1, string.Format("{0}{1}", "&", "vb.net"));
                list.Add(new LanguageRule("(?is)(?<=<%@.+?language=\"vb\".*?%>.*?<script.*?runat=\"server\">)(.*)(?=</script>)", dictionary3));
                Dictionary<int, string> dictionary4 = new Dictionary<int, string>();
                dictionary4.Add(1, "Html Tag Delimiter");
                dictionary4.Add(2, "HTML Element ScopeName");
                dictionary4.Add(3, "Html Tag Delimiter");
                dictionary4.Add(4, "HTML Element ScopeName");
                dictionary4.Add(5, "HTML Attribute ScopeName");
                dictionary4.Add(6, "HTML Operator");
                dictionary4.Add(7, "HTML Attribute Value");
                dictionary4.Add(8, "HTML Attribute ScopeName");
                dictionary4.Add(9, "HTML Operator");
                dictionary4.Add(10, "HTML Attribute Value");
                dictionary4.Add(11, "HTML Attribute ScopeName");
                dictionary4.Add(12, "HTML Operator");
                dictionary4.Add(13, "HTML Attribute Value");
                dictionary4.Add(14, "HTML Attribute ScopeName");
                dictionary4.Add(15, "Html Tag Delimiter");
                list.Add(new LanguageRule("(?xi)(</?)\r\n                                         (?: ([a-z][a-z0-9-]*)(:) )*\r\n                                         ([a-z][a-z0-9-_]*)\r\n                                         (?:\r\n                                            [\\s\\n]+([a-z0-9-_]+)[\\s\\n]*(=)[\\s\\n]*([^\\s\\n\"']+?)\r\n                                           |[\\s\\n]+([a-z0-9-_]+)[\\s\\n]*(=)[\\s\\n]*(\"[^\\n]+?\")\r\n                                           |[\\s\\n]+([a-z0-9-_]+)[\\s\\n]*(=)[\\s\\n]*('[^\\n]+?')\r\n                                           |[\\s\\n]+([a-z0-9-_]+) )*\r\n                                         [\\s\\n]*\r\n                                         (/?>)", dictionary4));
                Dictionary<int, string> dictionary5 = new Dictionary<int, string>();
                dictionary5.Add(1, "HTML Server-Side Script");
                dictionary5.Add(2, "Html Tag Delimiter");
                dictionary5.Add(3, "HTML Element ScopeName");
                dictionary5.Add(4, "HTML Attribute ScopeName");
                dictionary5.Add(5, "HTML Operator");
                dictionary5.Add(6, "HTML Attribute Value");
                dictionary5.Add(7, "HTML Server-Side Script");
                list.Add(new LanguageRule("(<%)(@)(?:\\s+([a-zA-Z0-9]+))*(?:\\s+([a-zA-Z0-9]+)(=)(\"[^\\n]*?\"))*\\s*?(%>)", dictionary5));
                return list;
            }
        }
    }
}

