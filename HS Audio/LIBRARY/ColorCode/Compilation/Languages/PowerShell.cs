﻿namespace ColorCode.Compilation.Languages
{
    using ColorCode;
    using System;
    using System.Collections.Generic;

    public class PowerShell : ILanguage
    {
        public bool HasAlias(string lang)
        {
            string str = lang.ToLower();
            return ((str != null) && ((str == "posh") || (str == "ps1")));
        }

        public string CssClassName
        {
            get
            {
                return "powershell";
            }
        }

        public string FirstLinePattern
        {
            get
            {
                return null;
            }
        }

        public string Id
        {
            get
            {
                return "powershell";
            }
        }

        public string Name
        {
            get
            {
                return "PowerShell";
            }
        }

        public IList<LanguageRule> Rules
        {
            get
            {
                List<LanguageRule> list = new List<LanguageRule>();
                Dictionary<int, string> captures = new Dictionary<int, string>();
                captures.Add(1, "Comment");
                list.Add(new LanguageRule(@"(?s)(<\#.*?\#>)", captures));
                Dictionary<int, string> dictionary2 = new Dictionary<int, string>();
                dictionary2.Add(1, "Comment");
                list.Add(new LanguageRule(@"(\#.*?)\r?$", dictionary2));
                Dictionary<int, string> dictionary3 = new Dictionary<int, string>();
                dictionary3.Add(0, "String");
                list.Add(new LanguageRule(@"'[^\n]*?(?<!\\)'", dictionary3));
                Dictionary<int, string> dictionary4 = new Dictionary<int, string>();
                dictionary4.Add(0, "String (C# @ Verbatim)");
                list.Add(new LanguageRule("(?s)@\".*?\"@", dictionary4));
                Dictionary<int, string> dictionary5 = new Dictionary<int, string>();
                dictionary5.Add(0, "String");
                list.Add(new LanguageRule("(?s)(\"[^\\n]*?(?<!`)\")", dictionary5));
                Dictionary<int, string> dictionary6 = new Dictionary<int, string>();
                dictionary6.Add(0, "PowerShell Variable");
                list.Add(new LanguageRule(@"\$(?:[\d\w\-]+(?::[\d\w\-]+)?|\$|\?|\^)", dictionary6));
                Dictionary<int, string> dictionary7 = new Dictionary<int, string>();
                dictionary7.Add(0, "PowerShell Variable");
                list.Add(new LanguageRule(@"\${[^}]+}", dictionary7));
                Dictionary<int, string> dictionary8 = new Dictionary<int, string>();
                dictionary8.Add(1, "Keyword");
                list.Add(new LanguageRule(@"\b(begin|break|catch|continue|data|do|dynamicparam|elseif|else|end|exit|filter|finally|foreach|for|from|function|if|in|param|process|return|switch|throw|trap|try|until|while)\b", dictionary8));
                Dictionary<int, string> dictionary9 = new Dictionary<int, string>();
                dictionary9.Add(0, "PowerShell Operator");
                list.Add(new LanguageRule("-(?:c|i)?(?:eq|ne|gt|ge|lt|le|notlike|like|notmatch|match|notcontains|contains|replace)", dictionary9));
                Dictionary<int, string> dictionary10 = new Dictionary<int, string>();
                dictionary10.Add(0, "PowerShell Operator");
                list.Add(new LanguageRule("-(?:band|and|as|join|not|bxor|xor|bor|or|isnot|is|split)", dictionary10));
                Dictionary<int, string> dictionary11 = new Dictionary<int, string>();
                dictionary11.Add(0, "PowerShell Operator");
                list.Add(new LanguageRule(@"(?:\+=|-=|\*=|/=|%=|=|\+\+|--|\+|-|\*|/|%)", dictionary11));
                Dictionary<int, string> dictionary12 = new Dictionary<int, string>();
                dictionary12.Add(0, "PowerShell Operator");
                list.Add(new LanguageRule(@"(?:\>\>|2\>&1|\>|2\>\>|2\>)", dictionary12));
                Dictionary<int, string> dictionary13 = new Dictionary<int, string>();
                dictionary13.Add(1, "PowerShell PowerShellAttribute");
                list.Add(new LanguageRule(@"(?s)\[(CmdletBinding)[^\]]+\]", dictionary13));
                Dictionary<int, string> dictionary14 = new Dictionary<int, string>();
                dictionary14.Add(1, "PowerShell Operator");
                dictionary14.Add(2, "PowerShell Type");
                dictionary14.Add(3, "PowerShell Operator");
                dictionary14.Add(4, "PowerShell Operator");
                list.Add(new LanguageRule(@"(\[)([^\]]+)(\])(::)?", dictionary14));
                return list;
            }
        }
    }
}

