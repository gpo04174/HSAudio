﻿namespace ColorCode.Compilation.Languages
{
    using ColorCode;
    using System;
    using System.Collections.Generic;

    public class Html : ILanguage
    {
        public bool HasAlias(string lang)
        {
            return false;
        }

        public override string ToString()
        {
            return this.Name;
        }

        public string CssClassName
        {
            get
            {
                return "html";
            }
        }

        public string FirstLinePattern
        {
            get
            {
                return null;
            }
        }

        public string Id
        {
            get
            {
                return "html";
            }
        }

        public string Name
        {
            get
            {
                return "HTML";
            }
        }

        public IList<LanguageRule> Rules
        {
            get
            {
                List<LanguageRule> list = new List<LanguageRule>();
                Dictionary<int, string> captures = new Dictionary<int, string>();
                captures.Add(0, "HTML Comment");
                list.Add(new LanguageRule(@"\<![ \r\n\t]*(--([^\-]|[\r\n]|-[^\-])*--[ \r\n\t]*)\>", captures));
                Dictionary<int, string> dictionary2 = new Dictionary<int, string>();
                dictionary2.Add(1, "Html Tag Delimiter");
                dictionary2.Add(2, "HTML Element ScopeName");
                dictionary2.Add(3, "HTML Attribute ScopeName");
                dictionary2.Add(4, "HTML Attribute Value");
                dictionary2.Add(5, "Html Tag Delimiter");
                list.Add(new LanguageRule("(?i)(<!)(DOCTYPE)(?:\\s+([a-zA-Z0-9]+))*(?:\\s+(\"[^\"]*?\"))*(>)", dictionary2));
                Dictionary<int, string> dictionary3 = new Dictionary<int, string>();
                dictionary3.Add(1, "Html Tag Delimiter");
                dictionary3.Add(2, "HTML Element ScopeName");
                dictionary3.Add(3, "HTML Attribute ScopeName");
                dictionary3.Add(4, "HTML Operator");
                dictionary3.Add(5, "HTML Attribute Value");
                dictionary3.Add(6, "HTML Attribute ScopeName");
                dictionary3.Add(7, "HTML Operator");
                dictionary3.Add(8, "HTML Attribute Value");
                dictionary3.Add(9, "HTML Attribute ScopeName");
                dictionary3.Add(10, "HTML Operator");
                dictionary3.Add(11, "HTML Attribute Value");
                dictionary3.Add(12, "HTML Attribute ScopeName");
                dictionary3.Add(13, "Html Tag Delimiter");
                dictionary3.Add(14, string.Format("{0}{1}", "&", "javascript"));
                dictionary3.Add(15, "Html Tag Delimiter");
                dictionary3.Add(0x10, "HTML Element ScopeName");
                dictionary3.Add(0x11, "Html Tag Delimiter");
                list.Add(new LanguageRule("(?xis)(<)\r\n                                          (script)\r\n                                          (?:\r\n                                             [\\s\\n]+([a-z0-9-_]+)[\\s\\n]*(=)[\\s\\n]*([^\\s\\n\"']+?)\r\n                                            |[\\s\\n]+([a-z0-9-_]+)[\\s\\n]*(=)[\\s\\n]*(\"[^\\n]+?\")\r\n                                            |[\\s\\n]+([a-z0-9-_]+)[\\s\\n]*(=)[\\s\\n]*('[^\\n]+?')\r\n                                            |[\\s\\n]+([a-z0-9-_]+) )*\r\n                                          [\\s\\n]*\r\n                                          (>)\r\n                                          (.*?)\r\n                                          (</)(script)(>)", dictionary3));
                Dictionary<int, string> dictionary4 = new Dictionary<int, string>();
                dictionary4.Add(1, "Html Tag Delimiter");
                dictionary4.Add(2, "HTML Element ScopeName");
                dictionary4.Add(3, "Html Tag Delimiter");
                dictionary4.Add(4, "HTML Element ScopeName");
                dictionary4.Add(5, "HTML Attribute ScopeName");
                dictionary4.Add(6, "HTML Operator");
                dictionary4.Add(7, "HTML Attribute Value");
                dictionary4.Add(8, "HTML Attribute ScopeName");
                dictionary4.Add(9, "HTML Operator");
                dictionary4.Add(10, "HTML Attribute Value");
                dictionary4.Add(11, "HTML Attribute ScopeName");
                dictionary4.Add(12, "HTML Operator");
                dictionary4.Add(13, "HTML Attribute Value");
                dictionary4.Add(14, "HTML Attribute ScopeName");
                dictionary4.Add(15, "Html Tag Delimiter");
                list.Add(new LanguageRule("(?xis)(</?)\r\n                                          (?: ([a-z][a-z0-9-]*)(:) )*\r\n                                          ([a-z][a-z0-9-_]*)\r\n                                          (?:\r\n                                             [\\s\\n]+([a-z0-9-_]+)[\\s\\n]*(=)[\\s\\n]*([^\\s\\n\"']+?)\r\n                                            |[\\s\\n]+([a-z0-9-_]+)[\\s\\n]*(=)[\\s\\n]*(\"[^\\n]+?\")\r\n                                            |[\\s\\n]+([a-z0-9-_]+)[\\s\\n]*(=)[\\s\\n]*('[^\\n]+?')\r\n                                            |[\\s\\n]+([a-z0-9-_]+) )*\r\n                                          [\\s\\n]*\r\n                                          (/?>)", dictionary4));
                Dictionary<int, string> dictionary5 = new Dictionary<int, string>();
                dictionary5.Add(0, "HTML Entity");
                list.Add(new LanguageRule(@"(?i)&\#?[a-z0-9]+?;", dictionary5));
                return list;
            }
        }
    }
}

