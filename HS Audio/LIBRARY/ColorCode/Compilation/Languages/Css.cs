﻿namespace ColorCode.Compilation.Languages
{
    using ColorCode;
    using System;
    using System.Collections.Generic;

    public class Css : ILanguage
    {
        public bool HasAlias(string lang)
        {
            return false;
        }

        public override string ToString()
        {
            return this.Name;
        }

        public string CssClassName
        {
            get
            {
                return "css";
            }
        }

        public string FirstLinePattern
        {
            get
            {
                return null;
            }
        }

        public string Id
        {
            get
            {
                return "css";
            }
        }

        public string Name
        {
            get
            {
                return "CSS";
            }
        }

        public IList<LanguageRule> Rules
        {
            get
            {
                List<LanguageRule> list = new List<LanguageRule>();
                Dictionary<int, string> captures = new Dictionary<int, string>();
                captures.Add(3, "CSS Selector");
                captures.Add(5, "CSS Property Name");
                captures.Add(6, "CSS Property Value");
                captures.Add(4, "Comment");
                captures.Add(1, "Comment");
                list.Add(new LanguageRule("(?msi)(?:(\\s*/\\*.*?\\*/)|(([a-z0-9#. \\[\\]=\\\":_-]+)\\s*(?:,\\s*|{))+(?:(\\s*/\\*.*?\\*/)|(?:\\s*([a-z0-9 -]+\\s*):\\s*([a-z0-9#,<>\\?%. \\(\\)\\\\\\/\\*\\{\\}:'\\\"!_=-]+);?))*\\s*})", captures));
                return list;
            }
        }
    }
}

