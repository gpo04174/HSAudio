﻿namespace ColorCode.Compilation.Languages
{
    using ColorCode;
    using System;
    using System.Collections.Generic;

    public class Ashx : ILanguage
    {
        public bool HasAlias(string lang)
        {
            return false;
        }

        public override string ToString()
        {
            return this.Name;
        }

        public string CssClassName
        {
            get
            {
                return "ashx";
            }
        }

        public string FirstLinePattern
        {
            get
            {
                return null;
            }
        }

        public string Id
        {
            get
            {
                return "ashx";
            }
        }

        public string Name
        {
            get
            {
                return "ASHX";
            }
        }

        public IList<LanguageRule> Rules
        {
            get
            {
                List<LanguageRule> list = new List<LanguageRule>();
                Dictionary<int, string> captures = new Dictionary<int, string>();
                captures.Add(1, "HTML Server-Side Script");
                captures.Add(2, "HTML Comment");
                captures.Add(3, "HTML Server-Side Script");
                list.Add(new LanguageRule("(<%)(--.*?--)(%>)", captures));
                Dictionary<int, string> dictionary2 = new Dictionary<int, string>();
                dictionary2.Add(1, string.Format("{0}{1}", "&", "c#"));
                list.Add(new LanguageRule("(?is)(?<=<%@.+?language=\"c\\#\".*?%>)(.*)", dictionary2));
                Dictionary<int, string> dictionary3 = new Dictionary<int, string>();
                dictionary3.Add(1, string.Format("{0}{1}", "&", "vb.net"));
                list.Add(new LanguageRule("(?is)(?<=<%@.+?language=\"vb\".*?%>)(.*)", dictionary3));
                Dictionary<int, string> dictionary4 = new Dictionary<int, string>();
                dictionary4.Add(1, "HTML Server-Side Script");
                dictionary4.Add(2, "Html Tag Delimiter");
                dictionary4.Add(3, "HTML Element ScopeName");
                dictionary4.Add(4, "HTML Attribute ScopeName");
                dictionary4.Add(5, "HTML Operator");
                dictionary4.Add(6, "HTML Attribute Value");
                dictionary4.Add(7, "HTML Server-Side Script");
                list.Add(new LanguageRule("(<%)(@)(?:\\s+([a-zA-Z0-9]+))*(?:\\s+([a-zA-Z0-9]+)(=)(\"[^\\n]*?\"))*\\s*?(%>)", dictionary4));
                return list;
            }
        }
    }
}

