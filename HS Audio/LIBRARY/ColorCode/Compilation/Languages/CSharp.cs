﻿namespace ColorCode.Compilation.Languages
{
    using ColorCode;
    using System;
    using System.Collections.Generic;

    public class CSharp : ILanguage
    {
        public bool HasAlias(string lang)
        {
            string str = lang.ToLower();
            return ((str != null) && ((str == "cs") || (str == "c#")));
        }

        public override string ToString()
        {
            return this.Name;
        }

        public string CssClassName
        {
            get
            {
                return "csharp";
            }
        }

        public string FirstLinePattern
        {
            get
            {
                return null;
            }
        }

        public string Id
        {
            get
            {
                return "c#";
            }
        }

        public string Name
        {
            get
            {
                return "C#";
            }
        }

        public IList<LanguageRule> Rules
        {
            get
            {
                List<LanguageRule> list = new List<LanguageRule>();
                Dictionary<int, string> captures = new Dictionary<int, string>();
                captures.Add(0, "Comment");
                list.Add(new LanguageRule(@"/\*([^*]|[\r\n]|(\*+([^*/]|[\r\n])))*\*+/", captures));
                Dictionary<int, string> dictionary2 = new Dictionary<int, string>();
                dictionary2.Add(1, "XML Doc Tag");
                dictionary2.Add(2, "XML Doc Tag");
                dictionary2.Add(3, "XML Doc Comment");
                list.Add(new LanguageRule("(///)(?:\\s*?(<[/a-zA-Z0-9\\s\"=]+>))*([^\\r\\n]*)", dictionary2));
                Dictionary<int, string> dictionary3 = new Dictionary<int, string>();
                dictionary3.Add(1, "Comment");
                list.Add(new LanguageRule(@"(//.*?)\r?$", dictionary3));
                Dictionary<int, string> dictionary4 = new Dictionary<int, string>();
                dictionary4.Add(0, "String");
                list.Add(new LanguageRule(@"'[^\n]*?(?<!\\)'", dictionary4));
                Dictionary<int, string> dictionary5 = new Dictionary<int, string>();
                dictionary5.Add(0, "String (C# @ Verbatim)");
                list.Add(new LanguageRule("(?s)@\"(?:\"\"|.)*?\"(?!\")", dictionary5));
                Dictionary<int, string> dictionary6 = new Dictionary<int, string>();
                dictionary6.Add(0, "String");
                list.Add(new LanguageRule("(?s)(\"[^\\n]*?(?<!\\\\)\")", dictionary6));
                Dictionary<int, string> dictionary7 = new Dictionary<int, string>();
                dictionary7.Add(1, "Keyword");
                dictionary7.Add(2, "String");
                list.Add(new LanguageRule("\\[(assembly|module|type|return|param|method|field|property|event):[^\\]\"]*(\"[^\\n]*?(?<!\\\\)\")?[^\\]]*\\]", dictionary7));
                Dictionary<int, string> dictionary8 = new Dictionary<int, string>();
                dictionary8.Add(1, "Preprocessor Keyword");
                list.Add(new LanguageRule(@"^\s*(\#define|\#elif|\#else|\#endif|\#endregion|\#error|\#if|\#line|\#pragma|\#region|\#undef|\#warning).*?$", dictionary8));
                Dictionary<int, string> dictionary9 = new Dictionary<int, string>();
                dictionary9.Add(1, "Keyword");
                list.Add(new LanguageRule(@"\b(abstract|as|ascending|base|bool|break|by|byte|case|catch|char|checked|class|const|continue|decimal|default|delegate|descending|do|double|dynamic|else|enum|equals|event|explicit|extern|false|finally|fixed|float|for|foreach|from|get|goto|group|if|implicit|in|int|into|interface|internal|is|join|let|lock|long|namespace|new|null|object|on|operator|orderby|out|override|params|partial|private|protected|public|readonly|ref|return|sbyte|sealed|select|set|short|sizeof|stackalloc|static|string|struct|switch|this|throw|true|try|typeof|uint|ulong|unchecked|unsafe|ushort|using|var|virtual|void|volatile|where|while|yield)\b", dictionary9));
                return list;
            }
        }
    }
}

