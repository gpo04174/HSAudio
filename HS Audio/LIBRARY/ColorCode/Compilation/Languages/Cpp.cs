﻿namespace ColorCode.Compilation.Languages
{
    using ColorCode;
    using System;
    using System.Collections.Generic;

    public class Cpp : ILanguage
    {
        public bool HasAlias(string lang)
        {
            string str = lang.ToLower();
            return ((str != null) && ((str == "c++") || (str == "c")));
        }

        public override string ToString()
        {
            return this.Name;
        }

        public string CssClassName
        {
            get
            {
                return "cplusplus";
            }
        }

        public string FirstLinePattern
        {
            get
            {
                return null;
            }
        }

        public string Id
        {
            get
            {
                return "cpp";
            }
        }

        public string Name
        {
            get
            {
                return "C++";
            }
        }

        public IList<LanguageRule> Rules
        {
            get
            {
                List<LanguageRule> list = new List<LanguageRule>();
                Dictionary<int, string> captures = new Dictionary<int, string>();
                captures.Add(0, "Comment");
                list.Add(new LanguageRule(@"/\*([^*]|[\r\n]|(\*+([^*/]|[\r\n])))*\*+/", captures));
                Dictionary<int, string> dictionary2 = new Dictionary<int, string>();
                dictionary2.Add(1, "Comment");
                list.Add(new LanguageRule(@"(//.*?)\r?$", dictionary2));
                Dictionary<int, string> dictionary3 = new Dictionary<int, string>();
                dictionary3.Add(0, "String");
                list.Add(new LanguageRule("(?s)(\"[^\\n]*?(?<!\\\\)\")", dictionary3));
                Dictionary<int, string> dictionary4 = new Dictionary<int, string>();
                dictionary4.Add(0, "Keyword");
                list.Add(new LanguageRule(@"\b(abstract|array|auto|bool|break|case|catch|char|ref class|class|const|const_cast|continue|default|delegate|delete|deprecated|dllexport|dllimport|do|double|dynamic_cast|each|else|enum|event|explicit|export|extern|false|float|for|friend|friend_as|gcnew|generic|goto|if|in|initonly|inline|int|interface|literal|long|mutable|naked|namespace|new|noinline|noreturn|nothrow|novtable|nullptr|operator|private|property|protected|public|register|reinterpret_cast|return|safecast|sealed|selectany|short|signed|sizeof|static|static_cast|ref struct|struct|switch|template|this|thread|throw|true|try|typedef|typeid|typename|union|unsigned|using|uuid|value|virtual|void|volatile|wchar_t|while)\b", dictionary4));
                return list;
            }
        }
    }
}

