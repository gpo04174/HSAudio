﻿namespace ColorCode.Compilation.Languages
{
    using ColorCode;
    using System;
    using System.Collections.Generic;

    public class Xml : ILanguage
    {
        public bool HasAlias(string lang)
        {
            return false;
        }

        public override string ToString()
        {
            return this.Name;
        }

        public string CssClassName
        {
            get
            {
                return "xml";
            }
        }

        public string FirstLinePattern
        {
            get
            {
                return null;
            }
        }

        public string Id
        {
            get
            {
                return "xml";
            }
        }

        public string Name
        {
            get
            {
                return "XML";
            }
        }

        public IList<LanguageRule> Rules
        {
            get
            {
                List<LanguageRule> list = new List<LanguageRule>();
                Dictionary<int, string> captures = new Dictionary<int, string>();
                captures.Add(0, "HTML Comment");
                list.Add(new LanguageRule(@"\<![ \r\n\t]*(--([^\-]|[\r\n]|-[^\-])*--[ \r\n\t]*)\>", captures));
                Dictionary<int, string> dictionary2 = new Dictionary<int, string>();
                dictionary2.Add(1, "XML Delimiter");
                dictionary2.Add(2, "XML Name");
                dictionary2.Add(3, "XML Attribute");
                dictionary2.Add(4, "XML Attribute Quotes");
                dictionary2.Add(5, "XML Attribute Value");
                dictionary2.Add(6, "XML Attribute Quotes");
                dictionary2.Add(7, "XML Delimiter");
                list.Add(new LanguageRule("(?i)(<!)(doctype)(?:\\s+([a-z0-9]+))*(?:\\s+(\")([^\\n]*?)(\"))*(>)", dictionary2));
                Dictionary<int, string> dictionary3 = new Dictionary<int, string>();
                dictionary3.Add(1, "XML Delimiter");
                dictionary3.Add(2, "XML Name");
                dictionary3.Add(3, "XML Doc Tag");
                dictionary3.Add(4, "XML Delimiter");
                list.Add(new LanguageRule("(?i)(<\\?)(xml-stylesheet)((?:\\s+[a-z0-9]+=\"[^\\n\"]*\")*(?:\\s+[a-z0-9]+=\\'[^\\n\\']*\\')*\\s*?)(\\?>)", dictionary3));
                Dictionary<int, string> dictionary4 = new Dictionary<int, string>();
                dictionary4.Add(1, "XML Delimiter");
                dictionary4.Add(2, "XML Name");
                dictionary4.Add(3, "XML Attribute");
                dictionary4.Add(4, "XML Delimiter");
                dictionary4.Add(5, "XML Attribute Quotes");
                dictionary4.Add(6, "XML Attribute Value");
                dictionary4.Add(7, "XML Attribute Quotes");
                dictionary4.Add(8, "XML Attribute");
                dictionary4.Add(9, "XML Delimiter");
                dictionary4.Add(10, "XML Attribute Quotes");
                dictionary4.Add(11, "XML Attribute Value");
                dictionary4.Add(12, "XML Attribute Quotes");
                dictionary4.Add(13, "XML Delimiter");
                list.Add(new LanguageRule("(?i)(<\\?)([a-z][a-z0-9-]*)(?:\\s+([a-z0-9]+)(=)(\")([^\\n]*?)(\"))*(?:\\s+([a-z0-9]+)(=)(\\')([^\\n]*?)(\\'))*\\s*?(\\?>)", dictionary4));
                Dictionary<int, string> dictionary5 = new Dictionary<int, string>();
                dictionary5.Add(1, "XML Delimiter");
                dictionary5.Add(2, "XML Name");
                dictionary5.Add(3, "XML Delimiter");
                dictionary5.Add(4, "XML Name");
                dictionary5.Add(5, "XML Attribute");
                dictionary5.Add(6, "XML Delimiter");
                dictionary5.Add(7, "XML Attribute Quotes");
                dictionary5.Add(8, "XML Attribute Value");
                dictionary5.Add(9, "XML Attribute Quotes");
                dictionary5.Add(10, "XML Attribute");
                dictionary5.Add(11, "XML Delimiter");
                dictionary5.Add(12, "XML Attribute Quotes");
                dictionary5.Add(13, "XML Attribute Value");
                dictionary5.Add(14, "XML Attribute Quotes");
                dictionary5.Add(15, "XML Attribute");
                dictionary5.Add(0x10, "XML Delimiter");
                list.Add(new LanguageRule("(?xi)(</?)\r\n                                          (?: ([a-z][a-z0-9-]*)(:) )*\r\n                                          ([a-z][a-z0-9-_\\.]*)\r\n                                          (?:\r\n                                            |[\\s\\n]+([a-z0-9-_\\.:]+)[\\s\\n]*(=)[\\s\\n]*(\")([^\\n]+?)(\")\r\n                                            |[\\s\\n]+([a-z0-9-_\\.:]+)[\\s\\n]*(=)[\\s\\n]*(')([^\\n]+?)(')\r\n                                            |[\\s\\n]+([a-z0-9-_\\.:]+) )*\r\n                                          [\\s\\n]*\r\n                                          (/?>)", dictionary5));
                Dictionary<int, string> dictionary6 = new Dictionary<int, string>();
                dictionary6.Add(0, "XML Attribute");
                list.Add(new LanguageRule("(?i)&[a-z0-9]+?;", dictionary6));
                Dictionary<int, string> dictionary7 = new Dictionary<int, string>();
                dictionary7.Add(1, "XML Delimiter");
                dictionary7.Add(2, "XML CData Section");
                dictionary7.Add(3, "XML Delimiter");
                list.Add(new LanguageRule(@"(?s)(<!\[CDATA\[)(.*?)(\]\]>)", dictionary7));
                return list;
            }
        }
    }
}

