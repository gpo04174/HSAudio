﻿namespace ColorCode.Common
{
    using ColorCode;
    using System;
    using System.Collections.Generic;

    public interface ILanguageRepository
    {
        ILanguage FindById(string languageId);
        void Load(ILanguage language);

        IEnumerable<ILanguage> All { get; }
    }
}

