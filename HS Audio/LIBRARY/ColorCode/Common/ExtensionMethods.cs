﻿namespace ColorCode.Common
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Runtime.CompilerServices;

    public static class ExtensionMethods
    {
        public static void SortStable<T>(this IList<T> list, Comparison<T> comparison)
        {
            Guard.ArgNotNull(list, "list");
            int count = list.Count;
            for (int i = 1; i < count; i++)
            {
                T y = list[i];
                int num3 = i - 1;
                while ((num3 >= 0) && (comparison(list[num3], y) > 0))
                {
                    list[num3 + 1] = list[num3];
                    num3--;
                }
                list[num3 + 1] = y;
            }
        }

        public static string ToHtmlColor(this Color color)
        {
            if (color == Color.Empty)
            {
                throw new ArgumentException("You may not create a hex string from an empty color.");
            }
            return ColorTranslator.ToHtml(color);
        }
    }
}

