﻿namespace ColorCode.Common
{
    using ColorCode;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading;

    public class LanguageRepository : ILanguageRepository
    {
        private readonly Dictionary<string, ILanguage> loadedLanguages;
        //private readonly ReaderWriterLockSlim loadLock;

        object o = new object();
        object o1 = new object();

        public LanguageRepository(Dictionary<string, ILanguage> loadedLanguages)
        {
            this.loadedLanguages = loadedLanguages;
            //this.loadLock = new ReaderWriterLockSlim();
        }

        public ILanguage FindById(string languageId)
        {
            System.Func<KeyValuePair<string, ILanguage>, bool> predicate = null;
            Guard.ArgNotNullAndNotEmpty(languageId, "languageId");
            ILanguage language = null;
            //this.loadLock.EnterReadLock();
            Monitor.Enter(o);
            try
            {
                if (predicate == null)
                {
                    predicate = x => (x.Key.ToLower() == languageId.ToLower()) || x.Value.HasAlias(languageId);
                }
                language = this.loadedLanguages.FirstOrDefault<KeyValuePair<string, ILanguage>>(predicate).Value;
            }
            finally
            {
                //this.loadLock.ExitReadLock();
                Monitor.Exit(o);
            }
            return language;
        }

        public void Load(ILanguage language)
        {
            Guard.ArgNotNull(language, "language");
            if (string.IsNullOrEmpty(language.Id))
            {
                throw new ArgumentException("The language identifier must not be null or empty.", "language");
            }
            //this.loadLock.EnterWriteLock();
            Monitor.Enter(o);
            try
            {
                this.loadedLanguages[language.Id] = language;
            }
            finally
            {
                //this.loadLock.ExitWriteLock();
                Monitor.Exit(o);
            }
        }

        public IEnumerable<ILanguage> All
        {
            get
            {
                return this.loadedLanguages.Values;
            }
        }
    }
}

