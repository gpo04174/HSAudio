﻿namespace ColorCode
{
    using ColorCode.Common;
    using Compilation;
    using System;
    using System.Collections.Generic;

    public static class Languages
    {
        internal static Dictionary<string, CompiledLanguage> CompiledLanguages = new Dictionary<string, CompiledLanguage>();
        internal static readonly ColorCode.Common.LanguageRepository LanguageRepository = new ColorCode.Common.LanguageRepository(LoadedLanguages);
        internal static readonly Dictionary<string, ILanguage> LoadedLanguages = new Dictionary<string, ILanguage>();

        static Languages()
        {
            Load<ColorCode.Compilation.Languages.JavaScript>();
            Load<ColorCode.Compilation.Languages.Html>();
            Load<ColorCode.Compilation.Languages.CSharp>();
            Load<ColorCode.Compilation.Languages.VbDotNet>();
            Load<ColorCode.Compilation.Languages.Ashx>();
            Load<ColorCode.Compilation.Languages.Asax>();
            Load<ColorCode.Compilation.Languages.Aspx>();
            Load<ColorCode.Compilation.Languages.AspxCs>();
            Load<ColorCode.Compilation.Languages.AspxVb>();
            Load<ColorCode.Compilation.Languages.Sql>();
            Load<ColorCode.Compilation.Languages.Xml>();
            Load<ColorCode.Compilation.Languages.Php>();
            Load<ColorCode.Compilation.Languages.Css>();
            Load<ColorCode.Compilation.Languages.Cpp>();
            Load<ColorCode.Compilation.Languages.Java>();
            Load<ColorCode.Compilation.Languages.PowerShell>();
            Load<ColorCode.Compilation.Languages.Typescript>();
            Load<ColorCode.Compilation.Languages.FSharp>();
        }

        public static ILanguage FindById(string id)
        {
            return LanguageRepository.FindById(id);
        }

        private static void Load<T>() where T: ILanguage, new()
        {
            Load(new T());
        }

        public static void Load(ILanguage language)
        {
            LanguageRepository.Load(language);
        }

        public static IEnumerable<ILanguage> All
        {
            get
            {
                return LanguageRepository.All;
            }
        }

        public static ILanguage Asax
        {
            get
            {
                return LanguageRepository.FindById("asax");
            }
        }

        public static ILanguage Ashx
        {
            get
            {
                return LanguageRepository.FindById("ashx");
            }
        }

        public static ILanguage Aspx
        {
            get
            {
                return LanguageRepository.FindById("aspx");
            }
        }

        public static ILanguage AspxCs
        {
            get
            {
                return LanguageRepository.FindById("aspx(c#)");
            }
        }

        public static ILanguage AspxVb
        {
            get
            {
                return LanguageRepository.FindById("aspx(vb.net)");
            }
        }

        public static ILanguage Cpp
        {
            get
            {
                return LanguageRepository.FindById("cpp");
            }
        }

        public static ILanguage CSharp
        {
            get
            {
                return LanguageRepository.FindById("c#");
            }
        }

        public static ILanguage Css
        {
            get
            {
                return LanguageRepository.FindById("css");
            }
        }

        public static ILanguage FSharp
        {
            get
            {
                return LanguageRepository.FindById("f#");
            }
        }

        public static ILanguage Html
        {
            get
            {
                return LanguageRepository.FindById("html");
            }
        }

        public static ILanguage Java
        {
            get
            {
                return LanguageRepository.FindById("java");
            }
        }

        public static ILanguage JavaScript
        {
            get
            {
                return LanguageRepository.FindById("javascript");
            }
        }

        public static ILanguage Php
        {
            get
            {
                return LanguageRepository.FindById("php");
            }
        }

        public static ILanguage PowerShell
        {
            get
            {
                return LanguageRepository.FindById("powershell");
            }
        }

        public static ILanguage Sql
        {
            get
            {
                return LanguageRepository.FindById("sql");
            }
        }

        public static ILanguage Typescript
        {
            get
            {
                return LanguageRepository.FindById("typescript");
            }
        }

        public static ILanguage VbDotNet
        {
            get
            {
                return LanguageRepository.FindById("vb.net");
            }
        }

        public static ILanguage Xml
        {
            get
            {
                return LanguageRepository.FindById("xml");
            }
        }
    }
}

