﻿namespace ColorCode
{
    using ColorCode.Common;
    using ColorCode.Compilation;
    using ColorCode.Parsing;
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Text;

    public class CodeColorizer : ICodeColorizer
    {
        private readonly ILanguageParser languageParser;

        public CodeColorizer()
        {
            this.languageParser = new LanguageParser(new LanguageCompiler(Languages.CompiledLanguages), Languages.LanguageRepository);
        }

        public CodeColorizer(ILanguageParser languageParser)
        {
            Guard.ArgNotNull(languageParser, "languageParser");
            this.languageParser = languageParser;
        }

        public string Colorize(string sourceCode, ILanguage language)
        {
            StringBuilder sb = new StringBuilder(sourceCode.Length * 2);
            using (TextWriter writer = new StringWriter(sb))
            {
                this.Colorize(sourceCode, language, writer);
                writer.Flush();
            }
            return sb.ToString();
        }

        public void Colorize(string sourceCode, ILanguage language, TextWriter textWriter)
        {
            this.Colorize(sourceCode, language, Formatters.Default, StyleSheets.Default, textWriter);
        }

        public void Colorize(string sourceCode, ILanguage language, IFormatter formatter, IStyleSheet styleSheet, TextWriter textWriter)
        {
            Guard.ArgNotNull(language, "language");
            Guard.ArgNotNull(formatter, "formatter");
            Guard.ArgNotNull(styleSheet, "styleSheet");
            Guard.ArgNotNull(textWriter, "textWriter");
            formatter.WriteHeader(styleSheet, language, textWriter);
            this.languageParser.Parse(sourceCode, language, (parsedSourceCode, captures) => formatter.Write(parsedSourceCode, captures, styleSheet, textWriter));
            formatter.WriteFooter(styleSheet, language, textWriter);
        }
    }
}

