﻿namespace ColorCode
{
    using System;
    using System.Collections.Generic;

    public interface ILanguage
    {
        bool HasAlias(string lang);

        string CssClassName { get; }

        string FirstLinePattern { get; }

        string Id { get; }

        string Name { get; }

        IList<LanguageRule> Rules { get; }
    }
}

