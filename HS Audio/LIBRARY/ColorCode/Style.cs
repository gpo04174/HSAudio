﻿namespace ColorCode
{
    using ColorCode.Common;
    using System;
    using System.Drawing;
    using System.Runtime.CompilerServices;

    public class Style
    {
        public Style(string scopeName)
        {
            Guard.ArgNotNullAndNotEmpty(scopeName, "scopeName");
            this.ScopeName = scopeName;
        }

        public override string ToString()
        {
            return (this.ScopeName ?? string.Empty);
        }

        public Color Background { get; set; }

        public string CssClassName { get; set; }

        public Color Foreground { get; set; }

        public string ScopeName { get; set; }
    }
}

