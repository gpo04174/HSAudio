﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;

namespace HS_Audio.LIBRARY
{
    class LayerWindow
    {
        [Flags]
        public enum GWL
        {
            ExStyle = -20,
            WndProc = (-4),
            HInstance = (-6),
            HwndParent = (-8),
            Style = (-16),
            UserData = (-21),
            Id = (-12)
        }

        [Flags]
        public enum WS_EX
        {
            Transparent = 0x20,
            Layered = 0x80000
        }

        [Flags]
        public enum LWA
        {
            ColorKey = 0x1,
            Alpha = 0x2
        }

        [DllImport("user32.dll", EntryPoint = "GetWindowLong")]
        public static extern int GetWindowLong(IntPtr hWnd, GWL nIndex);

        [DllImport("user32.dll", EntryPoint = "SetWindowLong")]
        public static extern int SetWindowLong(IntPtr hWnd, GWL nIndex, int dwNewLong);

        [DllImport("user32.dll", EntryPoint = "SetLayeredWindowAttributes")]
        public static extern bool SetLayeredWindowAttributes(IntPtr hWnd, int crKey, byte alpha, LWA dwFlags);

        public static bool SetClickThru(IntPtr ControlHandle, byte Opacity, ref int OriginalStyle, bool ClickThru = true)
        {
            int wl = GetWindowLong(ControlHandle, GWL.ExStyle);
            if (ClickThru)
            {
                OriginalStyle = wl;
                wl = wl | (int)WS_EX.Layered | (int)WS_EX.Transparent;
                SetWindowLong(ControlHandle, GWL.ExStyle, wl);
                return SetLayeredWindowAttributes(ControlHandle, 0, Opacity, LWA.Alpha);
                //return SetWindowLong(ControlHandle, GWL.ExStyle, Convert.ToInt32(wl | (int)WS_EX.Layered | (int)WS_EX.Transparent)) == 0;
            }
            else
            {
                /*
                wl = wl | (int)WS_EX.Layered | (int)WS_EX.Transparent;
                SetWindowLong(ControlHandle, GWL.ExStyle, wl);
                return SetLayeredWindowAttributes(ControlHandle, 0, Opacity, LWA.Alpha);*/
                //return SetWindowLong(ControlHandle, GWL.ExStyle, (wl &= ~(int)WS_EX.Layered) | (int)WS_EX.Transparent) == 0;
                return SetWindowLong(ControlHandle, GWL.ExStyle, OriginalStyle) == 0;
            }
        }
    }
}
