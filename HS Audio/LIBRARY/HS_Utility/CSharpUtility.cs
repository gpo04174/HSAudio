﻿#define DOTVERSION

using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Security.Cryptography;
using System.Runtime.InteropServices;
using System.Diagnostics;
using System.Windows.Forms;
using Microsoft.Win32;
using System.Drawing;
using HS_CSharpUtility.Extension;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using HS_Library;

/// <summary>
/// C# 사용에 필요한 편리한(유용한) 함수 들을 모아 놨습니다. 
/// </summary>
namespace HS_CSharpUtility
{
    public static class Utility
    {
        public static bool IsWindowsXP_Higher{get{return Environment.OSVersion.Version.Major>5;}}
        /// <summary>
        /// 창 (폼)에 관한 유용한 함수이 들어있는 클래스 입니다.
        /// </summary>
        public static class Win32Utility
        {
            const int MF_BYPOSITION = 0x400;
            // 메뉴수정시 들어가는 플래그값 (어떤 방식으로 메뉴를 수정할것인가..)
            // MF_BYPOSITION(0부터 시작하는 위치에의해), MF_BYCOMMAND 도 있음 

            [DllImport("User32")]
            private static extern int RemoveMenu(IntPtr hMenu, int nPosition, int wFlags);
            [DllImport("User32")]
            private static extern IntPtr GetSystemMenu(IntPtr hWnd, bool bRevert);
            [DllImport("User32")]
            private static extern int GetMenuItemCount(IntPtr hWnd);
            /// <summary>
            /// 폼의 X버튼을 비활성화 합니다.
            /// </summary>
            /// <param name="Handle">숨길 창 (폼) 핸들입니다.</param>
            public static void HideXButton(IntPtr Handle)
            {
                IntPtr hMenu = GetSystemMenu(Handle, false);// 시스템 메뉴를 가져옵니다.
                int menuItemCount = GetMenuItemCount(hMenu); // 메뉴갯수를 가져오네요
                RemoveMenu(hMenu, menuItemCount - 1, MF_BYPOSITION);
            }

            #region 모니터 On/Off 관련
            // 모니터 On/Off 관련

            [System.Runtime.InteropServices.DllImport("User32")]
            private static extern int SendMessage(int hWnd, int hMsg, int wParam, int lParam);
            [System.Runtime.InteropServices.DllImport("User32")]
            private static extern int SendMessage(IntPtr hWnd, int hMsg, int wParam, IntPtr lParam);

            private const int WM_SYSCOMMAND = 0x0112;
            private const int SC_MONITORPOWER = 0xF170;
            public enum MonitorEnum
            {
                MONITOR_ON = -1,
                MONITOR_OFF = 2,
                MONITOR_STANBY = 1
            }
            public static void MonitorControl(IntPtr Handle, MonitorEnum Control)
            {
                SendMessage(Handle.ToInt32(), WM_SYSCOMMAND, SC_MONITORPOWER, (int)Control);
            }
            #endregion

            [DllImport("user32.dll", SetLastError = true)]
            public static extern bool GetWindowRect(IntPtr hWnd, out RECT lpRect);

            [StructLayout(LayoutKind.Sequential)]
            public struct RECT
            {
                public int Left;
                public int Top;
                public int Right;
                public int Bottom;


                private Size _Size;
                private System.Drawing.Point _Location;
                public int Width{get{return Right - Left;}}
                public int Height{get{return Bottom - Top;}}
                public Size Size{get{_Size.Width = Width;_Size.Height = Height; return _Size;}}
                public Point Location{get{_Location.X = Left;_Location.Y = Top; return _Location;}}
                public System.Drawing.Rectangle Rectangle{get{return new Rectangle(Left, Top, Width, Height);}}
            }

            [DllImport("user32.dll", SetLastError = true)]
            public static extern bool BringWindowToTop(IntPtr hWnd);
        }
        /// <summary>
        /// 프로세스에 대한 유용한 함수가 들어있는 클래스 입니다.
        /// </summary>
        public static class ProcessUtility
        {
            /// <summary>
            /// 현재 실행되고있는 프로그램을 강제로 끝냅니다. (※주의! 이 메서드를 호출하면 작업중인 데이터가 손실됩니다)
            /// </summary>
            /// <returns>성공적으로 프로세스를 끝냈으면 True 그렇지않으면 False를 반환합니다.</returns>
            //[Obsolete]
            public static void TerminateProcess()
            {
                Process.GetCurrentProcess().Kill();
                /*
                Process[] p = Process.GetProcesses();
                Process CurrentPro = //Process.GetCurrentProcess().Kill();
                try
                {
                    bool a = false;
                    foreach (Process pr in p)
                    {if (pr.Id == CurrentPro.Id) { pr.Kill(); return true; } else { a = false; } } return a;
                }
                catch { return false; }*/
            }
            /// <summary>
            /// 프로그램을 강제로 끝냅니다. (※주의: 같은 이름이있으면 모두 끝내집니다.)
            /// </summary>
            /// <param name="Processname">끝낼 프로그램 이름 (프로세스)입니다. (*.exe가 포함되면 안됩니다.)</param>
            /// <returns>성공적으로 프로세스를 끝냈으면 True 그렇지않으면 False를 반환합니다.</returns>
            public static bool TerminateProcess(string Processname)
            {
                Process[] p = Process.GetProcesses();
                string Currentprocess = Processname;
                try
                {
                    bool a = false;
                    foreach (Process pr in p)
                    { if (pr.ProcessName == Currentprocess) { pr.Kill(); return true; } else { a = false; } } return a;
                }
                catch { return false; }
            }
            /// <summary>
            /// 프로그램을 강제로 끝냅니다.
            /// </summary>
            /// <param name="PID">끝낼 프로그램 (프로세스) PID입니다.</param>
            /// <returns>성공적으로 프로세스를 끝냈으면 True 그렇지않으면 False를 반환합니다.</returns>
            public static bool TerminateProcess(int PID)
            {
                Process[] p = Process.GetProcesses();
                try
                {
                    bool a = false;
                    foreach (Process pr in p)
                    { if (pr.Id == PID) { pr.Kill(); return true; } else { a = false; } } return a;
                }
                catch { return false; }
            }
            [Obsolete]
            /// <summary>
            /// 프로그램을 강제로 끝냅니다.
            /// </summary>
            /// <param name="Processname">끝낼 프로그램 이름 (프로세스)입니다. (*.exe가 포함되면 안됩니다.)</param>
            /// <param name="PID">끝낼 프로그램 (프로세스) PID입니다.</param>
            /// <returns>성공적으로 프로세스를 끝냈으면 True 그렇지않으면 False를 반환합니다.</returns>
            public static bool TerminateProcess(string Processname, int PID)
            {
                Process[] p = Process.GetProcesses();
                string Currentprocess = Processname;
                try
                {
                    bool a = false;
                    foreach (Process pr in p)
                    { if (pr.ProcessName == Currentprocess && pr.Id == PID) { pr.Kill(); return true; } else { a = false; } } return a;
                }
                catch { return false; }
            }

            /// <summary>
            /// 프로세스가 실행되고있는지 여부를 검사합니다. (※경고: 이름이 같은 프로세스가 있으면 정확도는 떨어집니다.)
            /// </summary>
            /// <param name="Processname">프로그램 이름 (프로세스)입니다. (*.exe가 포함되면 안됩니다.)</param>
            /// <returns>프로세스가 실행되고있으면 True 실행되고있지 않다면 False를 반환합니다.</returns>
            //[Obsolete]
            public static bool IsRunningProcess(string Processname)
            {
                Process[] p = Process.GetProcesses();bool a = false;
                foreach (Process pr in p)
                {
                    try { if (pr.ProcessName == Processname) { a = true; break; } }
                    catch { }
                } return a;
            }
            /// <summary>
            /// 프로세스가 실행되고있는지 여부를 검사합니다. (※경고: 이름이 같은 프로세스가 있으면 정확도는 떨어집니다.)
            /// </summary>
            /// <param name="Processname">프로그램 이름 (프로세스)입니다. (*.exe가 포함되면 안됩니다.)</param>
            /// <param name="Pid">반환값이 True면 PID(프로세스 고유 식별자)를 반환하고 False면 -1를 반환합니다.</param>
            /// <returns>프로세스가 실행되고있으면 True 실행되고있지 않다면 False를 반환합니다.</returns>
            public static bool IsRunningProcess(string Processname, out int Pid)
            {
                Process[] p = Process.GetProcesses(); bool a = false; Pid = -1;
                foreach (Process pr in p)
                {
                    try { if (pr.ProcessName == Processname) { a = true; Pid = pr.Id; break; } }
                    catch { }
                } return a;
            }
            /// <summary>
            /// 프로그램을 강제로 끝냅니다.
            /// </summary>
            /// <param name="PID">프로세스 ID 입니다</param>
            /// <returns>프로세스가 실행되고있으면 True 실행되고있지 않다면 False를 반환합니다.</returns>
            public static bool IsRunningProcess(int PID)
            {
                Process[] p = Process.GetProcesses(); bool a = false;
                foreach (Process pr in p)
                {
                    try { if (pr.Id == PID) { a = true; break; } }
                    catch { }
                } return a;
            }
            [Obsolete]
            /// <summary>
            /// 프로세스가 실행되고있는지 여부를 검사합니다.
            /// </summary>
            /// <param name="Processname">프로그램 이름 (프로세스)입니다. (*.exe가 포함되면 안됩니다.)</param>
            /// <param name="PID">프로세스 ID 입니다.</param>
            /// <returns>프로세스가 실행되고있으면 True 실행되고있지 않다면 False를 반환합니다.</returns>
            public static bool IsRunningProcess(string Processname, int PID)
            {
                Process[] p = Process.GetProcesses(); bool a = false;
                foreach (Process pr in p)
                {
                    try { if (pr.ProcessName==Processname&&pr.Id == PID) { a = true; break; } }
                    catch { }
                } return a;
            }

            /// <summary>
            /// exe가 아닌 프로그램도 실행시 킬수 있게합니다. (반드시 파일이 실행가능한 파일이어야 합니다)
            /// </summary>
            /// <param name="lpCmdLine">파일 경로 입니다.</param>
            /// <param name="nCmdShow">보여주기: SW_SHOW, 윈도우 숨김: SW_HIDE,  전체화면으로 윈도우 출력: SW_SHOWMAXIMIZED, 상세 인수: https://msdn.microsoft.com/ko-kr/library/windows/desktop/ms633548(v=vs.85).aspx</param>
            /// <returns>1이면 성공 0이면 실패</returns>
            [DllImport("kernel32.dll", SetLastError = true, EntryPoint = "WinExec")]
            public static extern int WinExec(String lpCmdLine, Int32 nCmdShow = 1);
        }
        /// <summary>
        /// 보안에 대한 유용한 함수가 들어있는 클래스 입니다.
        /// </summary>
        public sealed class SecurityUtility
        {
            public class MakeKey
            {
                public static byte[] DefaultKey { get { byte[] a = { 0, 12, 23, 34, 45, 56, 67, 78 }; return a; } }
                public static byte[] Key(string 암호) { return ASCIIEncoding.ASCII.GetBytes(암호); }
            }
            byte[] DesKey = MakeKey.DefaultKey;
            byte[] IVKey = MakeKey.DefaultKey;

            public SecurityUtility() { }
            public SecurityUtility(byte[] Key) { this.DesKey = Key; this.IVKey = Key; }
            public SecurityUtility(byte[] DesKey, byte[] IVKey) { this.DesKey = DesKey; this.IVKey = IVKey; }

            /// <summary>
            /// 암호화된 문자열을 생성합니다.
            /// </summary>
            /// <param name="strKey">암호화할 문자열 입니다.</param>
            /// <returns>암호화된 문자열을 반환합니다.</returns>
            public String Encrypt_암호화(String strKey)
            {
                DESCryptoServiceProvider desCSP = new DESCryptoServiceProvider();
                desCSP.Mode = CipherMode.ECB;
                desCSP.Padding = PaddingMode.PKCS7;
                desCSP.Key = DesKey;
                desCSP.IV = IVKey;
                MemoryStream ms = new MemoryStream();
                CryptoStream cryptStream = new CryptoStream(ms, desCSP.CreateEncryptor(), CryptoStreamMode.Write);
                byte[] data = Encoding.UTF8.GetBytes(strKey.ToCharArray());
                cryptStream.Write(data, 0, data.Length);
                cryptStream.FlushFinalBlock();
                String strReturn = Convert.ToBase64String(ms.ToArray());
                cryptStream = null;
                ms = null;
                desCSP = null;
                return strReturn;
            }

            /// <summary>
            /// 암호화된 문자열을 생성합니다.
            /// </summary>
            /// <param name="strKey">암호화할 문자열 입니다.</param>
            /// <param name="Key">암호화 할 키값 입니다.</param>
            /// <returns>암호화된 문자열을 반환합니다.</returns>
            public static String Encrypt_암호화(String strKey, byte[] Key)
            {
                DESCryptoServiceProvider desCSP = new DESCryptoServiceProvider();
                desCSP.Mode = CipherMode.ECB;
                desCSP.Padding = PaddingMode.PKCS7;
                desCSP.Key = Key;
                desCSP.IV = Key;
                MemoryStream ms = new MemoryStream();
                CryptoStream cryptStream = new CryptoStream(ms, desCSP.CreateEncryptor(), CryptoStreamMode.Write);
                byte[] data = Encoding.UTF8.GetBytes(strKey.ToCharArray());
                cryptStream.Write(data, 0, data.Length);
                cryptStream.FlushFinalBlock();
                String strReturn = Convert.ToBase64String(ms.ToArray());
                cryptStream = null;
                ms = null;
                desCSP = null;
                return strReturn;
            }
            /// <summary>
            /// 암호화된 문자열을 생성합니다.
            /// </summary>
            /// <param name="strKey">암호화할 문자열 입니다.</param>
            /// <param name="DesKey">암호화 표준 비밀키 입니다.</param>
            /// <param name="IVKey">백터키 입니다.</param>
            /// <returns>암호화된 문자열을 반환합니다.</returns>
            public static String Encrypt_암호화(String strKey, byte[] DesKey, byte[] IVKey)
            {
                DESCryptoServiceProvider desCSP = new DESCryptoServiceProvider();
                desCSP.Mode = CipherMode.ECB;
                desCSP.Padding = PaddingMode.PKCS7;
                desCSP.Key = DesKey;
                desCSP.IV = IVKey;
                MemoryStream ms = new MemoryStream();
                CryptoStream cryptStream = new CryptoStream(ms, desCSP.CreateEncryptor(), CryptoStreamMode.Write);
                byte[] data = Encoding.UTF8.GetBytes(strKey.ToCharArray());
                cryptStream.Write(data, 0, data.Length);
                cryptStream.FlushFinalBlock();
                String strReturn = Convert.ToBase64String(ms.ToArray());
                cryptStream = null;
                ms = null;
                desCSP = null;
                return strReturn;
            }



            // DES 알고리즘으로 암호화된 문자열을 받아서 복호화 한 후 암호화 이전의 원래 문자열을 Return 합니다.

            /// <summary>
            /// 복호화된 문자열을 생성합니다.
            /// </summary>
            /// <param name="strKey">복호화할 문자열 입니다.</param>
            /// <returns>복호화된 문자열을 반환합니다.</returns>
            public String Decrypt_복호화(String strKey)
            {
                DESCryptoServiceProvider desCSP = new DESCryptoServiceProvider();
                desCSP.Mode = CipherMode.ECB;
                desCSP.Padding = PaddingMode.PKCS7;
                desCSP.Key = DesKey;
                desCSP.IV = IVKey;
                MemoryStream ms = new MemoryStream();
                CryptoStream cryptStream = new CryptoStream(ms, desCSP.CreateDecryptor(), CryptoStreamMode.Write);
                strKey = strKey.Replace(" ", "+");
                byte[] data = Convert.FromBase64String(strKey);
                cryptStream.Write(data, 0, data.Length);
                cryptStream.FlushFinalBlock();
                String strReturn = Encoding.UTF8.GetString(ms.GetBuffer());
                cryptStream = null;
                ms = null;
                desCSP = null;
                return strReturn;
            }

            /// <summary>
            /// 복호화된 문자열을 생성합니다.
            /// </summary>
            /// <param name="strKey">복호화할 문자열 입니다.</param>
            /// <param name="Key">복호화 표준 비밀키 입니다.</param>
            /// <returns>복호화된 문자열을 반환합니다.</returns>
            public static String Decrypt_복호화(String strKey, byte[] Key)
            {
                DESCryptoServiceProvider desCSP = new DESCryptoServiceProvider();
                desCSP.Mode = CipherMode.ECB;
                desCSP.Padding = PaddingMode.PKCS7;
                desCSP.Key = Key;
                desCSP.IV = Key;
                MemoryStream ms = new MemoryStream();
                CryptoStream cryptStream = new CryptoStream(ms, desCSP.CreateDecryptor(), CryptoStreamMode.Write);
                strKey = strKey.Replace(" ", "+");
                byte[] data = Convert.FromBase64String(strKey);
                cryptStream.Write(data, 0, data.Length);
                cryptStream.FlushFinalBlock();
                String strReturn = Encoding.UTF8.GetString(ms.GetBuffer());
                cryptStream = null;
                ms = null;
                desCSP = null;
                return strReturn;
            }
            /// <summary>
            /// 복호화된 문자열을 생성합니다.
            /// </summary>
            /// <param name="strKey">복호화할 문자열 입니다.</param>
            /// <param name="DesKey">복호화 표준 비밀키 입니다.</param>
            /// <param name="IVKey">백터키 입니다.</param>
            /// <returns>복호화된 문자열을 반환합니다.</returns>
            public static String Decrypt_복호화(String strKey, byte[] DesKey, byte[] IVKey)
            {
                DESCryptoServiceProvider desCSP = new DESCryptoServiceProvider();
                desCSP.Mode = CipherMode.ECB;
                desCSP.Padding = PaddingMode.PKCS7;
                desCSP.Key = DesKey;
                desCSP.IV = IVKey;
                MemoryStream ms = new MemoryStream();
                CryptoStream cryptStream = new CryptoStream(ms, desCSP.CreateDecryptor(), CryptoStreamMode.Write);
                strKey = strKey.Replace(" ", "+");
                byte[] data = Convert.FromBase64String(strKey);
                cryptStream.Write(data, 0, data.Length);
                cryptStream.FlushFinalBlock();
                String strReturn = Encoding.UTF8.GetString(ms.GetBuffer());
                cryptStream = null;
                ms = null;
                desCSP = null;
                return strReturn;
            }
        }
        /// <summary>
        /// 문자열에 대한 유용한 함수가 들어있는 클래스 입니다.
        /// </summary>
        public static class StringUtility
        {
            /// <summary>
            /// 새로운 줄을 만듭니다.
            /// </summary>
            public const string NewLine = "\r\n";
            

            /// <summary>
            /// 파일 이름을 가져옵니다.
            /// </summary>
            /// <param name="FilePath">파일 경로 입니다</param>
            /// <returns>파일 이름을 반환합니다.</returns>
            public static string GetFileName(string FilePath)
            {
                int a = 0; try { a = FilePath.LastIndexOf("\\") + 1; }catch { }
                try { return FilePath.Substring(a); }catch{return FilePath;}
            }
            /// <summary>
            /// 파일 이름을 가져옵니다.
            /// </summary>
            /// <param name="FilePath">파일 경로 입니다</param>
            /// <param name="IncludeExtension">확장자를 포함할지 여부입니다.</param>
            /// <returns>파일 이름을 반환합니다.</returns>
            public static string GetFileName(string FilePath, bool IncludeExtension)
            {
                if(IncludeExtension)
                { return GetFileName(FilePath);}
                else
                {
                    int a = 0;try {a= FilePath.LastIndexOf("\\") + 1; }catch{}
                    string b = FilePath; try { b = FilePath.Substring(a); }catch{}
                    try { int c = b.LastIndexOf("."); return b.Remove(c); }catch{return FilePath;}
                }
            }

            /// <summary>
            /// 디렉토리 경로를 가져옵니다.
            /// </summary>
            /// <param name="FilePath">파일 경로 입니다</param>
            /// <returns>디렉토리 경로를 반환합니다.</returns>
            public static string GetDirectoryPath(string FilePath)
            { int a = FilePath.LastIndexOf("\\"); return a == -1 ? FilePath : FilePath.Remove(a); }
            /// <summary>
            /// 디렉토리 경로를 가져옵니다.
            /// </summary>
            /// <param name="FilePath">파일 경로 입니다</param>
            /// <param name="IncludeBackslash">백슬래시(\) 를 포함할지 여부입니다.</param>
            /// <returns>디렉토리 경로를 반환합니다.</returns>
            public static string GetDirectoryPath(string FilePath, bool IncludeBackslash)
            {
                if(IncludeBackslash)
                {int a = FilePath.LastIndexOf("\\")+1;return a==-1?FilePath:FilePath.Remove(a);}
                else
                {int a = FilePath.LastIndexOf("\\");return a==-1?FilePath:FilePath.Remove(a);}
            }

            
            /// <summary>
            /// 파일 확장자를 가져옵니다.
            /// </summary>
            /// <param name="FilePath">파일 경로 입니다</param>
            /// <returns>파일 확장를 반환합니다.</returns>
            public static string GetExtension(string FilePath)
            {
                int a =-1; try { a = FilePath.LastIndexOf(".") + 1; }catch { }
                try { return FilePath.Substring(a); }catch{return "";}
            }
            /// <summary>
            /// 파일 확장자를 가져옵니다.
            /// </summary>
            /// <param name="FilePath">파일 경로 입니다</param>
            /// <param name="IncludeBackslash">점(.) 을 포함할지 여부입니다.</param>
            /// <returns>파일 확장를 반환합니다.</returns>
            public static string GetExtension(string FilePath, bool IncludeBackslash)
            {
                int a =-1; 
                if(IncludeBackslash)try { a = FilePath.LastIndexOf("."); }catch { }
                else try { a = FilePath.LastIndexOf(".") + 1; }catch { }
                try { return FilePath.Substring(a); }catch{return "";}
            }

            /// <summary>
            /// 배열형식 으로 된 문자열을 문자열로 바꾸어줍니다. 
            /// </summary>
            /// <param name="Array">문자열 배열입니다.</param>
            /// <param name="NewLine">True면 줄 바꿈(\r\n)을 합니다.</param>
            /// <returns>배열형식 으로 된 문자열을 문자열로 바꾸어 반환합니다.</returns>
            public static string ConvertArrayToString(string[] Array, bool NewLine)
            {
                StringBuilder a = new StringBuilder(Array[0]);
                if (NewLine)
                { for (long i = 1; i < Array.LongLength; i++) { a.Append("\r\n"); a.Append(Array[i]); } }
                else { for (long i = 1; i < Array.LongLength; i++) { a.Append(Array[i]); } }
                return a.ToString();
            }
            /// <summary>
            /// 배열 형식으로 된 문자열을 문자열로 바꾸어줍니다. 
            /// </summary>
            /// <param name="Array">문자열 배열입니다.</param>
            /// <param name="NewLine">분리할 문자열 입니다. (Null 이거나 빈 문자열이면 붙여진채로 반환됩니다.)</param>
            /// <param name="ThrowException">True면 NewLine의 값이 Null이거나 기타 예외가 발생하면 발생시키고, False면 붙여진채로 반환되고 모든 예외가 무시됩니다.</param>
            /// <returns>배열 형식으로 된 문자열을 문자열로 바꾸어 반환합니다.</returns>
            public static string ConvertArrayToString(string[] Array, string NewLine,bool ThrowException=false)
            {
                if (Array == null&&ThrowException) throw new ArgumentNullException("문자열 배열(string[] Array)값은 Null일수 없습니다.");
                if (Array.Length == 0 && ThrowException) throw new ArgumentNullException("문자열 배열(string[] Array) 의 크기는 0보다 커야 합니다.");
                if(Array == null||Array.Length == 0)return null;
                StringBuilder a = new StringBuilder(Array[0]);
                if (NewLine == null) { if (ThrowException)throw new ArgumentNullException("NewLine값은 Null일수 없습니다."); NewLine = ""; }
                for (long i = 1; i < Array.LongLength; i++) { a.Append(NewLine); a.Append(Array[i]); }
                return a.ToString();
            }

            /// <summary>
            /// 문자열을 문자열배열 형식으로 바꿔 반환합니다.
            /// </summary>
            /// <param name="Text">문자열 입니다.</param>
            /// <param name="NewLine">줄 바꿀 문자열입니다.(null이거나 빈 문자열이면 \n과 \r을 기준으로 배열합니다.)</param>
            /// <returns>문자열을 문자열배열 형식으로 바꿔 반환합니다.</returns>
            public static string[] ConvertStringToArray(string Text, string NewLine)
            {
                try
                {
                    if (NewLine == "" || NewLine == null)
                    {
                        string a = Text.Replace("\n", "\0\n").Replace("\r", "\0\n");
                        string[] a1 = { "\0\n" };
                        string[] a2 = Text.Split(a1, StringSplitOptions.RemoveEmptyEntries);
                        return ConvertStringToArray(a2[0], "\r\n");
                    }
                    else
                    {
                        //string a = Text.Replace(NewLine, "\0");
                        //char[] a1 = { '\0', '\n' };
                        string[] b = { NewLine };
                        return Text.Split(b, StringSplitOptions.RemoveEmptyEntries);
                    }
                }catch{return null;}
            }
            public static string[] ConvertStringToArray_Index(string Text, string Seperater)
            {
                if (Seperater != "" || Seperater != null && Text!=null|Seperater!="")
                {
                    int a = Text.IndexOf(Seperater);
                    if (a != -1) 
                    { string a1 = Text.Remove(a); string a2 = Text.Substring(a+1); 
                      return new string[]{a1, a2};}
                    else {return null;}//throw new Exception("분리할 문자열이 내용에 없습니다"); }
                }
                else{return null;}
            }

            /// <summary>
            /// 문자열을 지정된 Base64 인코딩으로 변환합니다.
            /// </summary>
            /// <param name="strData">원본 문자열 입니다.</param>
            /// <param name="encType">인코딩할 형식 입니다.</param>
            /// <returns>인코딩으로 변환된 문자열을 반환합니다.</returns>
            public static string Base64Encode(string strData, System.Text.Encoding encType)
            {
                byte[] bteData = encType.GetBytes(strData);
                return Convert.ToBase64String(bteData);
            }

            /// <summary>
            /// 문자열을 지정된 횟수만큼 추가합니다.
            /// </summary>
            /// <param name="Text">추가할 문자열 입니다.</param>
            /// <param name="Count">추가할 횟수 입니다.</param>
            /// <returns></returns>
            public static string AddString(string Text, long Count)
            {
                StringBuilder sb = new StringBuilder();
                for (long i = 0; i < Count; i++){ sb.Append(Text); }
                return sb.ToString();
            }

        }
        /// <summary>
        /// 파일과 디렉토리에 대한 유용한 함수가 들어있는 클래스 입니다.
        /// </summary>
        public static class IOUtility
        {
            /// <summary>
            /// 문자열(내용)을 파일에 씁니다. (파일이 있으면 덮어씁니다.)
            /// </summary>
            /// <param name="Path">문자열(내용)을 쓸 파일경로 입니다.</param>
            /// <param name="Text">문자열(내용) 입니다.</param>
            /// <returns>성공적으로 만들어진 여부를 반환합니다.</returns>
            public static bool CreateFile(string Path, string Text)
            {
                try
                {
                    StreamWriter sw = new StreamWriter(Path);
                    sw.Write(Text); sw.Close();

                    return true;
                }
                catch
                {
                    return false;
                }
            }
            /// <summary>
            /// 문자열(내용)을 파일에 씁니다. (파일이 있으면 덮어씁니다.)
            /// </summary>
            /// <param name="Path">문자열(내용)을 쓸 파일경로 입니다.</param>
            /// <param name="Text">문자열(내용) 입니다.</param>
            /// <param name="Exception">만약 오류가 나지않으면 null 이고, 오류가나면 예외가 전달되는 값입니다.</param>
            /// <returns>성공적으로 만들어진 여부를 반환합니다.</returns>
            public static bool CreateFile(string Path, string Text, ref Exception Exception)
            {
                try
                {
                    StreamWriter sw = new StreamWriter(Path);
                    sw.Write(Text); sw.Close();

                    return true;
                }
                catch (Exception e)
                {
                    Exception = e; return false;
                }
            }
            /// <summary>
            /// 문자열(내용)을 파일에 씁니다. (파일이 있으면 덮어씁니다.)
            /// </summary>
            /// <param name="Path">문자열(내용)을 쓸 파일경로 입니다.</param>
            /// <param name="Text">문자열(내용) 입니다.</param>
            /// <param name="Append">문자열을 파일끝에서 추가할지 여부를 나타냅니다. 파일이 아직없으면 새로 만듭니다.</param>
            /// <returns>성공적으로 만들어진 여부를 반환합니다.</returns>
            public static bool CreateFile(string Path, string Text, bool Append)
            {
                try
                {
                    if (Append)
                    {
                        File.AppendAllText(Path, Text);
                    }
                    else
                    {
                        StreamWriter sw = new StreamWriter(Path);
                        sw.Write(Text); sw.Close();
                        return File.Exists(Path);
                    }
                    return true;
                }
                catch
                {
                    return false;
                }
            }
            /// <summary>
            /// 문자열(내용)을 파일에 씁니다. (파일이 있으면 덮어씁니다.)
            /// </summary>
            /// <param name="Path">문자열(내용)을 쓸 파일경로 입니다.</param>
            /// <param name="Text">문자열(내용) 입니다.</param>
            /// <param name="Append">문자열을 파일끝에서 추가할지 여부를 나타냅니다. 파일이 아직없으면 새로 만듭니다.</param>
            /// <param name="Exception">만약 오류가 나지않으면 null 이고, 오류가나면 예외가 전달되는 값입니다.</param>
            /// <returns>성공적으로 만들어진 여부를 반환합니다.</returns>
            public static bool CreateFile(string Path, string Text, bool Append, ref Exception Exception)
            {
                try
                {
                    if (Append)
                    {
                        File.AppendAllText(Path, Text);
                    }
                    else
                    {
                        StreamWriter sw = new StreamWriter(Path);
                        sw.Write(Text); sw.Close();
                        return File.Exists(Path);
                    }
                    return true;
                }
                catch (Exception e)
                {
                    Exception = e; return false;
                }
            }
            /// <summary>
            /// 문자열(내용)을 지정된 형식으로 인코딩시켜 파일에 씁니다. (파일이 있으면 덮어씁니다.)
            /// </summary>
            /// <param name="Path">저장할 파일 경로입니다.</param>
            /// <param name="Text">문자열(내용) 입니다.</param>
            /// <param name="Encoding">문자열을 인코딩할 인코더형식 입나더.</param>
            /// <returns>성공적으로 만들어진 여부를 반환합니다.</returns>
            public static bool CreateFile(string Path, string Text, Encoding Encoding)
            {
                try
                {
                    System.IO.File.WriteAllText(Path, Text, Encoding);
                    return true;
                }
                catch
                {
                    return false;
                }
            }
            /// <summary>
            /// 문자열(내용)을 지정된 형식으로 인코딩시켜 파일에 씁니다. (파일이 있으면 덮어씁니다.)
            /// </summary>
            /// <param name="Path">저장할 파일 경로입니다.</param>
            /// <param name="Text">문자열(내용) 입니다.</param>
            /// <param name="Encoding">문자열을 인코딩할 인코더형식 입니다.</param>
            /// <param name="Append">문자열을 파일끝에서 추가할지 여부를 나타냅니다. 파일이 아직없으면 새로 만듭니다.</param>
            /// <returns>성공적으로 만들어진 여부를 반환합니다.</returns>
            public static bool CreateFile(string Path, string Text, Encoding Encoding, bool Append)
            {
                try
                {
                    System.IO.File.WriteAllText(Path, Text, Encoding);
                    return true;
                }
                catch
                {
                    return false;
                }
            }
            /// <summary>
            /// 문자열(내용)을 지정된 형식으로 인코딩시켜 파일에 씁니다. (파일이 있으면 덮어씁니다.)
            /// </summary>
            /// <param name="Path">저장할 파일 경로입니다.</param>
            /// <param name="Text">문자열(내용) 입니다.</param>
            /// <param name="Encoding">문자열을 인코딩할 인코더형식 입니다.</param>
            /// <param name="Exception">만약 오류가 나지않으면 null 이고, 오류가나면 예외가 전달되는 값입니다.</param>
            /// <returns>성공적으로 만들어진 여부를 반환합니다.</returns>
            public static bool CreateFile(string Path, string Text, Encoding Encoding, ref Exception Exception)
            {
                try
                {
                    System.IO.File.WriteAllText(Path, Text, Encoding);
                    return true;
                }
                catch (Exception e)
                {
                    Exception = e; return false;
                }
            }
            public static bool CreateFile(byte[] ByteArray, string Path)
            {
                try
                {
                    System.IO.File.WriteAllBytes(Path, ByteArray);
                    return true;
                }
                catch
                {
                    return false;
                }
            }

            /// <summary>
            /// 기존 파일을 읽습니다.
            /// </summary>
            /// <param name="Path">기존 파일을 읽을 경로 입니다.</param>
            /// <returns>파일내용을 반환합니다.</returns>
            public static string OpenFile(string Path)
            {
                StreamReader sr = new StreamReader(Path);
                string a = sr.ReadToEnd(); sr.Close(); return a;
            }
            /// <summary>
            /// 기존 파일을 읽습니다.
            /// </summary>
            /// <param name="Path">기존 파일을 읽을 경로 입니다.</param>
            /// <param name=" Encoding">기존 파일을 지정한 인코딩형식으로 읽습니다.</param>
            /// <returns>파일내용을 반환합니다.</returns>
            public static string OpenFile(string Path, Encoding Encoding)
            {
                StreamReader sr = new StreamReader(Path, Encoding);
                string a = sr.ReadToEnd(); sr.Close(); return a;
            }

            /// <summary>
            /// 무작위로 임시파일을 쓴다음 경로를 반환합니다.
            /// </summary>
            /// <param name="ByteArray">임시경로에다 쓸 바이트배열 입니다.</param>
            /// <returns></returns>
            public static string RandomWriteByte(byte[] ByteArray)
            {
                string path = Path.GetTempFileName();
                File.WriteAllBytes(path, ByteArray);
                return path;

            }

            /// <summary>
            /// 파일의 인코딩을 구합니다
            /// </summary>
            /// <param name="FileName">인코딩을 구할 파일경로 입니다.</param>
            /// <returns>파일의 인코딩을 반환합니다</returns>
            public static Encoding GetFileEncoding(string FileName)
            {
                // *** Use Default of Encoding.Default (Ansi CodePage)
                Encoding enc = Encoding.Default;
                try
                {
                    UTF8Encoding utf8 = new UTF8Encoding(false, true);
                    StreamReader sr = new StreamReader(FileName, utf8);
                    int a = sr.Read(); sr.Close();
                    if (a == -1)
                    {
                        throw new DecoderFallbackException();
                    }
                    enc = utf8;
                }
                catch
                {
                    // *** Detect byte order mark if any - otherwise assume default
                    byte[] buffer = new byte[5];
                    FileStream file = new FileStream(FileName, FileMode.Open);
                    file.Read(buffer, 0, 5);
                    file.Close();

                    if (buffer[0] == 0xef && buffer[1] == 0xbb && buffer[2] == 0xbf)
                        enc = Encoding.UTF8;
                    else if (buffer[0] == 0xfe && buffer[1] == 0xff)
                        enc = Encoding.Unicode;
                    else if (buffer[0] == 0 && buffer[1] == 0 && buffer[2] == 0xfe && buffer[3] == 0xff)
                        enc = Encoding.UTF32;
                    else if (buffer[0] == 0x2b && buffer[1] == 0x2f && buffer[2] == 0x76)
                        enc = Encoding.UTF7;
                    else if (buffer[0] == 0xFE && buffer[1] == 0xFF)
                        // 1201 unicodeFFFE Unicode (Big-Endian)
                        enc = Encoding.GetEncoding(1201);
                    else if (buffer[0] == 0xFF && buffer[1] == 0xFE)
                        // 1200 utf-16 Unicode
                        enc = Encoding.GetEncoding(1200);
                    else try
                        {
                            enc = HS_CSharpUtility.Text.EncodingDetector.DetectEncoding(buffer);
                        }
                        catch
                        {
                        }
                }
                return enc == null ? Encoding.Default : enc;
            }

            /// <summary>
            /// 파일의 인코딩을 구합니다
            /// </summary>
            /// <param name="FileName">인코딩을 구할 파일경로 입니다.</param>
            /// <param name="FastMode">True를 주면 구하는 과정이 빨라집니다.</param>/*빨라지긴 하지만 정확도가 떨어집니다.*/
            /// <returns>파일의 인코딩을 반환합니다</returns>
            public static Encoding GetFileEncoding(string FileName, bool FastMode)
            {
                // *** Use Default of Encoding.Default (Ansi CodePage)
                Encoding enc = Encoding.Default;
                try
                {
                    UTF8Encoding utf8 = new UTF8Encoding(false, true);
                    if (!FastMode)
                    {
                        System.IO.File.ReadAllText(FileName, utf8);
                        //System.IO.File.ReadAllText();
                        enc = utf8;
                    }
                    else
                    {
                        StreamReader sr = new StreamReader(FileName, utf8);
                        int a = sr.Read(); sr.Close();
                        if (a == -1)
                        {
                            throw new DecoderFallbackException();
                        }
                        enc = utf8;
                    }
                }
                catch
                {
                    // *** Detect byte order mark if any - otherwise assume default
                    byte[] buffer = new byte[5];
                    if (!System.IO.File.Exists(FileName)) return null;
                    FileStream file = new FileStream(FileName, FileMode.Open);
                    file.Read(buffer, 0, 5);
                    file.Close();

                    if (buffer[0] == 0xef && buffer[1] == 0xbb && buffer[2] == 0xbf)
                        enc = Encoding.UTF8;
                    else if (buffer[0] == 0xfe && buffer[1] == 0xff)
                        enc = Encoding.Unicode;
                    else if (buffer[0] == 0 && buffer[1] == 0 && buffer[2] == 0xfe && buffer[3] == 0xff)
                        enc = Encoding.UTF32;
                    else if (buffer[0] == 0x2b && buffer[1] == 0x2f && buffer[2] == 0x76)
                        enc = Encoding.UTF7;
                    else if (buffer[0] == 0xFE && buffer[1] == 0xFF)
                        // 1201 unicodeFFFE Unicode (Big-Endian)
                        enc = Encoding.GetEncoding(1201);
                    else if (buffer[0] == 0xFF && buffer[1] == 0xFE)
                        // 1200 utf-16 Unicode
                        enc = Encoding.GetEncoding(1200);
                    else if (!FastMode) try
                        {
                            enc = HS_CSharpUtility.Text.EncodingDetector.DetectEncoding(buffer);
                        }
                        catch
                        {
                        }
                }
                return enc == null ? Encoding.Default : enc;
            }

            public static string MakeValidFileName(string FileName, bool UnderBar = false)
            {
                if (UnderBar)
                {
                    //name = name.Replace('\'', '’'); // U+2019 right single quotation mark
                    FileName = FileName.Replace('"', '”'); // U+201D right double quotation mark
                    FileName = FileName.Replace('/', '⁄');  // U+2044 fraction slash
                    FileName = FileName.Replace(':', '：');
                    FileName = FileName.Replace('?', '？');
                    FileName = FileName.Replace('<', '＜');
                    FileName = FileName.Replace('>', '＞');
                    FileName = FileName.Replace('|', 'ǀ');
                }
                else foreach (char c in System.IO.Path.GetInvalidFileNameChars()) if (c != '\\') FileName = FileName.Replace(c, '_');
                return FileName;
            }
            /*
            public static string MakeValidFileName(string FileName)
            {
                string invalidChars = System.Text.RegularExpressions.Regex.Escape(new string(System.IO.Path.GetInvalidFileNameChars()));
                string invalidRegStr = string.Format(@"([{0}]*\.+$)|([{0}]+)", invalidChars);

                return System.Text.RegularExpressions.Regex.Replace(FileName, invalidRegStr, "_");
            }*/

            /*
            /// <summary>
            /// 
            /// </summary>
            /// <param name="Text"></param>
            /// <param name="encoding"></param>
            /// <returns></returns>
            public static string encodingtest_3(string Text, Encoding encoding)
            {
                string tarstr = encoding.GetString(encoding.GetBytes(Text));
                string[] tarstrarray = tarstr.Split('?');
                List<string> tarstrlist = new List<string>(tarstrarray);
                int charindex = 0;
                int questioncount = 0;
                foreach (string tarchar in tarstrarray)
                {
                    if (!questioncount.Equals(0))
                    {
                        tarstrlist.Insert(questioncount, System.Web.HttpUtility.UrlEncodeUnicode(
                            Text[tarstr.IndexOf("?", charindex)].ToString()
                            ).Replace("%u", "`u`")
                            );
                        questioncount++; // 하나 삽입했으니
                    }
                    questioncount++;

                    charindex += tarchar.Length;
                }
                if (questioncount.Equals(0) || questioncount.Equals(1))
                    return null;

                return string.Join(string.Empty, tarstrlist.ToArray());
            }
            /// <summary>
            /// 파일의 인코딩을 구합니다 (구할수 없으면 null을 반환합니다.)
            /// </summary>
            /// <param name="FileName">인코딩을 구할 파일이름 입니다.</param>
            /// <returns>인코딩을 반환합니다.</returns>
            public static Encoding GetEncoding(string FileName, bool Calc)
            {
                if (Calc)
                {
                    try
                    {
                        string a = System.IO.File.ReadAllText(FileName);
                        foreach (EncodingInfo a1 in Encoding.GetEncodings())
                        {
                            if (encodingtest_3(a, a1.GetEncoding()) == null) { return a1.GetEncoding(); }
                        }
                        return null;
                    }
                    catch { return null; }
                }
                else
                {
                    StreamReader sr = new StreamReader(FileName);
                    Encoding e = sr.CurrentEncoding; sr.Close();
                    return e;
                }
            }
            */
            public static Stream CopyStream(Stream Original, Stream Destination, int buffer = 1024)
            {
                if (Original == null) throw new ArgumentNullException("Original");
                if (Destination == null) throw new ArgumentNullException("Destination");

                // Ensure a reasonable size of buffer is used without being prohibitive.
                if (buffer < 128) throw new ArgumentException("Buffer is too small", "buffer");

                bool copying = true;

                byte[] Buffer = new byte[buffer];
                while (copying)
                {
                    int bytesRead = Original.Read(Buffer, 0, Buffer.Length);
                    if (bytesRead > 0) Destination.Write(Buffer, 0, bytesRead);
                    else
                    {
                        Destination.Flush();
                        copying = false;
                    }
                }
                return Destination;
            }

            public static void OpenExplorer(String Path)
            {
                try
                {
                    string a = Environment.GetEnvironmentVariable("windir") + "\\explorer.exe";
                    Process.Start(a, "/select," + Path);
                }
                catch { }
            }
        }
        /// <summary>
        /// 레지스트리에 대한 유용한 함수가 들어있는 클래스 입니다.
        /// </summary>
        public static class RegistryUtility
        {
            /// <summary>
            /// Windows 레지스트리 기본키 입니다.
            /// </summary>
            public enum RegistryKeyKind { HKEY_LOCAL_MACHINE, HKEY_CURRENT_USER, HKEY_CLASSES_ROOT, HKEY_USERS, HKEY_PERFORMANCE_DATA, HKEY_CURRENT_CONFIG, HKEY_DYN_DATA }

            public static Microsoft.Win32.RegistryKey ReturnRegestryKey(RegistryKeyKind basekeyName)
            {
                switch (basekeyName)
                {
                    case RegistryKeyKind.HKEY_CURRENT_USER:
                        return Registry.CurrentUser;
                    case RegistryKeyKind.HKEY_LOCAL_MACHINE:
                        return Registry.LocalMachine;
                    case RegistryKeyKind.HKEY_CLASSES_ROOT:
                        return Registry.ClassesRoot;
                    case RegistryKeyKind.HKEY_USERS:
                        return Registry.Users;
                    case RegistryKeyKind.HKEY_PERFORMANCE_DATA:
                        return Registry.PerformanceData;
                    case RegistryKeyKind.HKEY_CURRENT_CONFIG:
                        return Registry.CurrentConfig;
                    case RegistryKeyKind.HKEY_DYN_DATA:
                        return Registry.DynData;
                    default: return null;
                }
            }

            /// <summary>
            /// 레지스트리 키값이 있는지 여부를 검사합니다.
            /// </summary>
            /// <param name="RegistrySubkeyPath">레지스트리 하위키 경로입니다.</param>
            /// <param name="Registrykeykind">Windows 레지스트리 기본키 입니다.</param>
            /// <returns>키가 있으면 True 없으면 False를 반환합니다.</returns>
            public static bool IsRegistryKeyExits(string RegistrySubkeyPath, RegistryKeyKind Registrykeykind)
            {
                //string[] a = RegistrySubkeyPath.Split('\\');
                RegistryKey r = ReturnRegestryKey(Registrykeykind);
                RegistryKey registryKey = r.OpenSubKey(RegistrySubkeyPath);
                if (registryKey == null) { return false; } else { return true; }
            }

            /// <summary>
            /// 레지스트리 키를 문자열로 바꾸어줍니다. (*.reg 파일 형식)
            /// </summary>
            /// <param name="registryKey">레지스트리 키 입니다.</param>
            /// <returns>레지스트리 키를 문자열로 반환합니다. (*.reg 파일 형식)</returns>
            public static string RegistryRead(RegistryKey registryKey)
            {
                var export = new StringBuilder();
                export.AppendLine("[" + registryKey + "]");

                var values = registryKey.GetValueNames();

                foreach (var value in values)
                {
                    export.AppendFormat("\"{0}\"=", value);
                    if (registryKey.GetValue(value) != null)
                    { //problem converting values.
                        if (registryKey.GetValueKind(value).Equals(RegistryValueKind.Binary))
                        {
                            export.Append("hex:");
                        }
                        else if (registryKey.GetValueKind(value).Equals(RegistryValueKind.DWord))
                        {
                            export.Append("dword:");
                        }
                        else if (registryKey.GetValueKind(value).Equals(RegistryValueKind.QWord))
                        {
                            export.Append("hex(b):");
                        }
                        else if (registryKey.GetValueKind(value).Equals(RegistryValueKind.MultiString))
                        {
                            export.Append("hex(7):");
                        }
                        else if (registryKey.GetValueKind(value).Equals(RegistryValueKind.ExpandString))
                        {
                            export.Append("hex(2):");
                        }
                        export.AppendLine(registryKey.GetValue(value).ToString());
                    }
                }
                export.AppendLine();

                var keys = registryKey.GetSubKeyNames();

                foreach (var key in keys)
                {
                    var registry = registryKey.OpenSubKey(key);

                    export.Append(RegistryRead(registry));
                }

                return export.ToString();
            }
            /*
            /// <summary>
            /// 파일에서 레지스트리를 등록합니다.
            /// </summary>
            /// <param name="RegistryFilePath">레지스트리 파일이 있는 경로 입니다.</param>
            public static void RegistrySaveKey(string RegistryFilePath) 
            {
                string filepath = Application.StartupPath + "\\reg.exe";
                if (!File.Exists(filepath))
                {
                    byte[] h2 = CSharpUtility.Properties.Resources.reg;
                    FileStream fs = new FileStream(filepath, FileMode.CreateNew, FileAccess.Write, FileShare.None, h2.Length);
                    fs.Write(h2, 0, h2.Length); fs.Close();
                }

                ProcessStartInfo psi = new ProcessStartInfo(filepath, "/IMPORT " + RegistryFilePath); psi.CreateNoWindow = true;
                Process p = new Process(); p.StartInfo = psi;
                p.Start(); p.WaitForExit();
            }*/

            /// <summary>
            /// 파일에서 레지스트리를 등록합니다.
            /// </summary>
            /// <param name="RegistryFilePath">레지스트리 파일이 있는 경로 입니다.</param>
            private static void RegistrySaveKey(string RegistryFilePath)
            {
                string filepath = Environment.GetFolderPath(Environment.SpecialFolder.System) + "\\reg.exe";
                /* 이 부분은 필요할때만 사용하세요
                filepath = Application.StartupPath + "\\reg.exe";
                if (!File.Exists(filepath))
                {
                    byte[] h2 = CSharpUtility.Properties.Resources.reg;
                    FileStream fs = new FileStream(filepath, FileMode.CreateNew, FileAccess.Write, FileShare.None, h2.Length);
                    fs.Write(h2, 0, h2.Length); fs.Close();
                }*/

                ProcessStartInfo psi = new ProcessStartInfo(filepath, "/IMPORT " + RegistryFilePath); psi.CreateNoWindow = true;
                Process p = new Process(); p.StartInfo = psi;
                p.Start(); p.WaitForExit();
            }

            /// <summary>
            /// 문자열로 된 레지스트리 전체경로를 Microsoft.Win32.RegistryKey 형태로 반환합니다.
            /// </summary>
            /// <param name="RegistryPath">레지스트리 전체경로 입니다.</param>
            /// <returns></returns>
            /*private Microsoft.Win32.RegistryKey ConvertRegistryKey(string RegistryPath)
            {

            }*/

            //public void CreateRegestryValue(RegistryKeyKind Registrykeykind, string RegistrySubkeyPath);

        }
        /// <summary>
        /// 배열에 대한 유용한 함수가 들어있는 클래스 입니다.
        /// </summary>
        public static class ArrayUtility
        {
            
            /// <summary>
            /// 배열형식 으로 된 개체를 문자열로 바꾸어줍니다. 
            /// </summary>
            /// <param name="Array">문자열 배열입니다.</param>
            /// <param name="NewLine">True면 줄 바꿈(\r\n)을 합니다.</param>
            /// <returns>배열형식 으로 된 문자열을 문자열로 바꾸어 반환합니다.</returns>
            public static string ConvertArrayToString<T>(T[] Array, bool NewLine)
            {
                StringBuilder a = new StringBuilder(Array[0].ToString());
                if (NewLine)
                { for (long i = 1; i < Array.LongLength; i++) { a.Append("\r\n"); a.Append(Array[i].ToString()); } }
                else { for (long i = 1; i < Array.LongLength; i++) { a.Append(Array[i].ToString()); } }
                return a.ToString();
            }
            /// <summary>
            /// 배열형식 으로 된 개체를 문자열로 바꾸어줍니다. 
            /// </summary>
            /// <param name="Array">문자열 배열입니다.</param>
            /// <param name="NewLine">True면 줄 바꿈(\r\n)을 합니다.</param>
            /// <returns>배열형식 으로 된 문자열을 문자열로 바꾸어 반환합니다.</returns>
            public static string ConvertArrayToString<T>(T[] Array, string NewLine)
            {
                if (Array.Length > 0)
                {
                    StringBuilder a = new StringBuilder(Array[0].ToString());
                    if (NewLine != null)
                    { for (long i = 1; i < Array.LongLength; i++) { a.Append(NewLine); a.Append(Array[i].ToString()); } }
                    else { for (long i = 1; i < Array.LongLength; i++) { a.Append(Array[i].ToString()); } }
                    return a.ToString();
                }
                else return null;
            }
            /// <summary>
            /// 
            /// 배열 형식을 List형식으로 바꾸어 줍니다.
            /// </summary>
            /// <param name="Arrayvalue">배열 입니다.</param>
            /// <returns></returns>
            public static List<T> ConvertArrayToList<T>(T[] Arrayvalue)
            {
                List<T> a = new List<T>();
                for (long i = 0; i < Arrayvalue.LongLength; i++) { a.Add(Arrayvalue[i]); }
                return a;
            }

            /*
            /// <summary>
            /// List 형식을 배열형식으로 바꾸어 줍니다.
            /// </summary>
            /// <param name="Listvalue">List 입니다.</param>
            /// <returns></returns>
            public static object[] ConvertListToArray(List<object> Listvalue)
            {
                object[] a = new object[Listvalue.Count];
                for (int i = 0; i < Listvalue.Count; i++) { a[i] = Listvalue[i]; }
                return a;
            }*/
            /// <summary>
            /// List 형식을 배열형식으로 바꾸어 줍니다.
            /// </summary>
            /// <param name="Listvalue">List 입니다.</param>
            /// <returns></returns>
            public static T[] ConvertListToArray<T>(List<T> Listvalue)
            {
                T[] a = new T[Listvalue.Count];
                for (int i = 0; i < Listvalue.Count; i++) { a[i] = Listvalue[i]; }
                return a;
            }

            /// <summary>
            /// Object 배열에서 해당개체가 있는지 검사합니다.
            /// </summary>
            /// <param name="Item">개체를 검사할 Object 배열입니다.</param>
            /// <param name="Match">일치할 Object 개체 입니다.</param>
            /// <returns>개체가 존재하면 true 존재하지 않으면 false를 반환합니다.</returns>
            public static bool IsArrayItem(object[] Item, object Match)
            {
                for (long i = 0; i < Item.LongLength; i++)
                {
                    if (Item[i].GetType() == Match.GetType())
                    {
                        if (Item[i].Equals(Match)) { return true; }
                    }
                }
                return false;
            }
            /// <summary>
            /// Object 배열에서 해당개체가 있는지 검사합니다.
            /// </summary>
            /// <param name="Item">개체를 검사할 Object 배열입니다.</param>
            /// <param name="Match">일치할 Object 개체 입니다.</param>
            /// <param name="Index">개체가 일치한 인덱스값을 반환합니다. 일치하는 개체가 없으면 -1 을 반환합니다.</param>
            /// <returns>개체가 존재하면 true 존재하지 않으면 false를 반환합니다.</returns>
            public static bool IsArrayItem(object[] Item, object Match, out long Index)
            {
                for (long i = 0; i < Item.LongLength; i++)
                {
                    if (Item[i].GetType() == Match.GetType())
                    {
                        if (Item[i].Equals(Match)) { Index = i; return true; }
                    }
                }
                Index = -1; return false;
            }
            public static bool IsArrayItem<T>(T[] Item, T Match)
            {
                foreach(T a in Item)if (a.Equals(Match)) { return true; }
                return false;
            }
            /// <summary>
            /// String 배열에서 해당개체가 있는지 검사합니다.
            /// </summary>
            /// <param name="Item">개체를 검사할 배열입니다.</param>
            /// <param name="Match">일치할 개체 입니다.</param>
            /// <param name="IgnoreCapital">대,소문자를 무시할지 여부 입니다.</param>
            /// <returns>개체가 존재하면 true 존재하지 않으면 false를 반환합니다.</returns>
            public static bool IsArrayItem(string[] Item, string Match, bool IgnoreCapital=false)
            {
                for (long i = 0; i < Item.LongLength; i++)
                {
                    if (!IgnoreCapital)
                    {
                        if (Item[i].IndexOf(Match) != -1) return true;
                    }
                    else if (Item[i].ToLower().IndexOf(Match.ToLower()) != -1)return true;
                }
                return false;
            }
            /// <summary>
            /// 문자열 배열에서 해당개체가 있는지 검사합니다.
            /// </summary>
            /// <param name="Item">개체를 검사할 문자열 배열입니다.</param>
            /// <param name="Match">일치할 문자열 입니다.</param>
            /// <param name="Index">개체가 일치한 인덱스값을 반환합니다. 일치하는 개체가 없으면 -1 을 반환합니다.</param>
            /// <param name="IgnoreCapital">대,소문자를 무시할지 여부 입니다.</param>
            /// <returns>개체가 존재하면 true 존재하지 않으면 false를 반환합니다.</returns>
            public static bool IsArrayItem(string[] Item, string Match, out long Index, bool IgnoreCapital = false)
            {
                for (long i = 0; i < Item.LongLength; i++)
                {
                    if (IgnoreCapital) if (Item[i].IndexOf(Match) != -1) { Index = i; return true; }
                        else if (Item[i].ToLower().IndexOf(Match.ToLower()) != -1) { Index = i; return true; }
                }
                Index = -1; return false;
            }
            /// <summary>
            /// Object형을 ToString() 해서 일치값을 반환합니다.
            /// </summary>
            /// <param name="Item">개체를 검사할 배열입니다.</param>
            /// <param name="Match">일치할 개체 입니다.</param>
            /// <returns>개체가 존재하면 true 존재하지 않으면 false를 반환합니다.</returns>
            public static bool IsArrayItemforToString(object[] Item, object Match)
            {
                foreach (object o in Item) if (o.Equals(Match)) { return true; }
                return false;
            }
            /// <summary>
            /// Object형을 ToString() 해서 일치값을 반환합니다.
            /// </summary>
            /// <param name="Item">개체를 검사할 배열입니다.</param>
            /// <param name="Match">일치할 개체 입니다.</param>
            /// <param name="Index">개체가 일치한 인덱스값을 반환합니다. 일치하는 개체가 없으면 -1 을 반환합니다.</param>
            /// <returns>개체가 존재하면 true 존재하지 않으면 false를 반환합니다.</returns>
            public static bool IsArrayItemforToString(object[] Item, object Match, out long Index)
            {
                for (long i = 0; i < Item.LongLength; i++)
                {
                    if (Item[i].ToString() == Match.ToString()) { Index = i; return true; }
                }
                Index = -1; return false;
            }
        }
        /// <summary>
        /// 시간 및 날짜에 대한 유용한 함수가 들어있는 클래스 입니다.
        /// </summary>
        [Obsolete("아직 구현이 안된 클래스 입니다 (구현되면 public 으로 바꾸길...)")]
        public static class TimeUtility
        {
            
        }
        public sealed class ControlUtility
        {
            /// <summary>
            /// 숫자만 눌리도록 합니다
            /// </summary>
            /// <param name="e">키가 눌러질때 발생하는 이벤트 입니다.</param>
            /// <returns>엔터키가 눌렸으면 True 그렇지 읺으면 False를 반환합니다.</returns>
            public static bool OnlyInputNumber(KeyPressEventArgs e)
            {
                if (!(Char.IsDigit(e.KeyChar)) && e.KeyChar != Convert.ToChar(Keys.Back)) { e.Handled = true; }
                if (e.KeyChar == Convert.ToChar(Keys.Enter)) { return true; } else return false;
            }

            /// <summary>
            /// 크기(Width)에 따라 텍스트가 현재보다 크면 큰만큼 잘라내고 "..."을 반환합니다. (Width 가 0이면 10길이로 잘라냅니다.)
            /// </summary>
            /// <param name="Value">텍스트 입니다</param>
            /// <param name="Width">길이 입니다 (두 컨트롤 사이의 길이를 구하려면 Control1.Location.X - Control2.Location.X 로 해주시면 됩니다.)</param>
            /// <param name="font">폰트 입니다.</param>
            /// <param name="IsFileName">"..."뒤에 붙일말 입니다.</param>
            /// <returns></returns>
            public static string TextCut(string Value, int Width, Font font, string IsFileName = null)
            {
                if (Width == 0) return Value;
                int diff = Math.Abs(Width);
                int len = 10;
                byte PlusFlag = 0, MinusFlag = 0;

                Size sf = new Size();

                string tmp = "..." + IsFileName;
                try
                {
                    //HS_Audio.LIBRARY.HSConsole.WriteLine(string.Format("==========================================\nText = {0}\n==========================================", Value));

                    sf = TextRenderer.MeasureText(Value, font);
                    //HS_Audio.LIBRARY.HSConsole.WriteLine(string.Format("StringSize = {0}, diff = {1}, StringSize > diff = {2}\n", sf.Width, diff, sf.Width > diff));

                    if (sf.Width > diff) tmp = "..." + IsFileName;
                    else return Value;

                    if (len < Value.Length)HSConsole.WriteLine(string.Format("==========================================\nCutting Text...\nText = {0}\n==========================================", Value));
                    while (len < Value.Length)
                    {
                        sf = TextRenderer.MeasureText(Value.Remove(len) + tmp, font);
                        if (sf.Width < diff)  {len++; if (MinusFlag > 0) break; else PlusFlag++; }
                        else if (sf.Width > diff) {len--; if (PlusFlag > 0) break; else MinusFlag++; }
                        else break;
                        HSConsole.WriteLine(string.Format("StringSize={0}, len={1}, PlusFlag={2}, MinusFlag={3}", sf.Width, len, PlusFlag, MinusFlag, Value));
                    }
                    HSConsole.WriteLine(null);
                    return Value.Remove(len) + tmp;
                }
                catch (Exception ex)
                {
                    HSConsole.WriteLine(string.Format("Error! - [{0}]\n", ex.ToString()));
                    return (Value.Length > 10 ? Value.Remove(10) + "..." : Value);
                }
                finally {}
            }
        }
        /// <summary>
        /// 기타 유용한 함수가 들어있는 클래스 입니다.
        /// </summary>
        public sealed class EtcUtility//<T>:System.Collections.Generic.IEnumerable<T>
        {
            private int lastTick;
            private int lastFrameRate;
            private int frameRate;
            public int CalculateFrameRate()
            {
                if (System.Environment.TickCount - lastTick >= 1000)
                {
                    lastFrameRate = frameRate;
                    frameRate = 0;
                    lastTick = System.Environment.TickCount;
                }
                frameRate++;
                return lastFrameRate;
            }


            /// <summary>
            /// 바이트 배열을 스트림으로 바꿔줍니다.
            /// </summary>
            /// <param name="ByteArray">스트림으로 바꿀 바이트배열 입니다.</param>
            /// <returns></returns>
            public static Stream ConvertByteArrayToStream(byte[] ByteArray)
            {
                MemoryStream ms = new MemoryStream(ByteArray);
                return ms;
            }
            //public static Encoding FileEncoding(string FileName){}
            //public static T SerializeLoad(T ){}

            /// <summary>
            /// 설정값을 불러오거나 저장할때 폴더가 없으면 자동으로 생성해 주고 그 경로를 반환합니다. (기본이름은 Setting 이고 만약 실패하면 null을 반환합니다.)
            /// </summary>
            /// <param name="FolderName">폴더이름입니다. (경우에따라서는 \ 이(예: ExamplePath\Setting_1\Setting.ini) 추가로 들어갈수 있습니다.)</param>
            /// <returns></returns>
            public static string GetSettingDirectory(string FolderName="Settings")
            {
                string[] BanChar = { "?", "<", ">", ":", "*", "|" };
                string path=Path.GetDirectoryName(Process.GetCurrentProcess().MainModule.FileName);
                try
                {
                    if (FolderName.ArrayIncludeString(BanChar)) path = path + "\\Settings"; else path = path + "\\" + FolderName;
                    if (!Directory.Exists(path)) Directory.CreateDirectory(path);
                    return path;
                }
                catch { return null; }
            }

            public static Dictionary<string, string> LoadSetting(string[] Text)
            {
                return LoadSetting(Text, "=");
            }
            public static Dictionary<string, string> LoadSetting(string[] Text, string Separator ="=")
            {
                Dictionary<string, string> a = new Dictionary<string, string>();
                string[] b = new string[2];
                int LrCf = 0;
                for (int i = 0; i < Text.Length; i++)
                {
                    if (Text[i] == null || Text[i] == "") { b[0] = "\n" + (LrCf++); b[1] = null; }
                    else if (Text[i][0] == '/' || Text[i][0] == ';') { b[0] = Text[i]; b[1] = null; }
                    else b = StringUtility.ConvertStringToArray_Index(Text[i], Separator == null ? "=" : Separator);
                    //b = b == null ? b = new string[2] : b;if (b != null)
                    if(b!=null)
                    {
                        try
                        {
                            if (b.Length == 2) a.Add(b[0], b[1]);
                            else if (b.Length == 1) a.Add(b[0], null);
                            else if (Text[i] != null && Text[i] != "") a.Add(Text[i], null);
                        }
                        catch { }
                    }
                }
                return a;
            }

            public static string[] SaveSetting(Dictionary<string, string> Value) { return SaveSetting(Value, "="); }
            public static string[] SaveSettingWithoutFilter(Dictionary<string, string> Value) { return SaveSettingWithoutFilter(Value, "="); }
            public static string[] SaveSetting(Dictionary<string, string> Value, string Separator ="=")
            {
                string[] a = new string[Value.Count];
                int i = 0;
                foreach (KeyValuePair<string, string> s in Value)
                    a[i++] = s.Key[0] != '\n' ?
                        (s.Value == null ? s.Key : s.Key + (Separator == null ? "=" : Separator) + s.Value) : "";
                return a;
            }
            public static string[] SaveSettingWithoutFilter(Dictionary<string, string> Value, string Separator = "=")
            {
                string[] a = new string[Value.Count];
                int i = 0;
                foreach (KeyValuePair<string, string> s in Value)
                    a[i++] = s.Key[0] != '\n' ? 
                        (s.Key + (Separator == null ? "=" : Separator) + s.Value) : "";
                return a;
            }

            public static byte[] SerializableSaveToByteArray(object Value)
            {
                System.Runtime.Serialization.Formatters.Binary.BinaryFormatter bf
                    = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
                MemoryStream ms = new MemoryStream();
                bf.Serialize(ms, Value);// ms.Close();
             #if DEBUG
                System.IO.FileStream fs = new System.IO.FileStream(Application.StartupPath + "\\tmp.bin", System.IO.FileMode.Append);
                bf.Serialize(fs, Value); fs.Close();
             #endif
                return ms.GetBuffer();
            }
            public static MemoryStream SerializableSaveToStream(object Value)
            {
                System.Runtime.Serialization.Formatters.Binary.BinaryFormatter bf
                    = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
                MemoryStream ms = new MemoryStream();
                bf.Serialize(ms, Value);
                return ms;
            }

            public static object SerializableOpen(Stream SerializationStream)
            {
                System.Runtime.Serialization.Formatters.Binary.BinaryFormatter bf
                    = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
                return bf.Deserialize(SerializationStream);
            }
            public static object SerializableOpen(byte[] SerializationByte)
            {
                System.Runtime.Serialization.Formatters.Binary.BinaryFormatter bf
                    = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
                return bf.Deserialize(ConvertByteArrayToStream(SerializationByte));
            }

            /// <summary>
            /// 바이트 배열을 바이트 배열 문자열로 바꾸어 줍니다. (00 FF 55 … 이런식으로 반환 됩니다.)
            /// </summary>
            /// <param name="Value">문자열로바꿀 바이트 배열 입니다.</param>
            /// <returns>00 FF 55 … 이런식으로 반환 됩니다.</returns>
            public static string ConvertByteArrayTostring(byte[] Value)
            {
                StringBuilder sb = new StringBuilder();
                if (Value.LongLength > 1)
                {
                    foreach (byte a in Value)
                    { sb.Append(" " + a.ToString("x2")); }
                }
                sb.Remove(0, 1);
                return sb.ToString();
            }

            /// <summary>
            /// 바이트 배열 문자열을 바이트 배열로 바꾸어 줍니다.
            /// </summary>
            /// <param name="Value">바이트 배열 입니다. (반드시 형식은 00 FF 55 … 이런식 이어야 합니다.)</param>
            /// <param name="ThrowException">True 중간에 실패하면 예외를 발생시키고 False면 패스 합니다..</param>
            /// <returns></returns>
            public static byte[] ConvertStringToByteArray(string Value, bool ThrowException)
            {
                List<byte> a = new List<byte>();
                string[] sep = Value.Split(' ');
                foreach (string b in sep)
                {
                    try { a.Add(byte.Parse(b, System.Globalization.NumberStyles.HexNumber)); }
                    catch (Exception ex)
                    { if (ThrowException)throw ex;}
                }
                return a.ToArray();
            }


            /// <summary>
            /// 색깔을 문자열 형식으로 바꾸어 줍니다 (출력값: "A, R, G, B")
            /// </summary>
            /// <param name="c">색깔입니다</param>
            /// <returns>출력값: "A, R, G, B"</returns>
            public static string ColorToString(Color c)
            {
                return c.A + "," + c.R + "," + c.G + "," + c.B;
            }
            /// <summary>
            /// 문자열 형식의 색깔을 실제색깔로 변환시켜 줍니다. (반드시 "A, R, G, B" 포맷이어야 합니다.)
            /// </summary>
            /// <param name="s">색깔로 변환할 문자열 입니다.</param>
            /// <returns></returns>
            public static Color StringToColor(string s)
            {
                string[] a = s.Split(',');
                int A = int.Parse(a[0]); int R = int.Parse(a[1]); int G = int.Parse(a[2]); int B = int.Parse(a[3]);
                return Color.FromArgb(A, R, G, B);
            }

            public static byte ReverseByte(byte Value) {return (byte)(255 - Value); }

            public enum OperatingSystemVersion
            {Windows98}
            [Obsolete]
            public static void GetOperatingSystemVersion()
            {
            }
        }

        public static class ConvertUtility
        {
            const bool ThrowExcept = true;

            /// <summary>
            /// X,Y 로 된 문자열을 System.Drawing.Point 구조체로 변경해줍니다
            /// </summary>
            /// <param name="Setting">X,Y 로 된 문자열 입니다</param>
            /// <param name="ThrowException">변환 실패시 예외를 발생시킬지의 여부 입니다. (기본값은 True입니다.)</param>
            /// <returns></returns>
            public static Point StringToPoint(string Setting, bool ThrowException = ThrowExcept)
            {
                try
                {
                    string[] a = Setting.Trim().Split(',');
                    int X = 0;try{X = Convert.ToInt32(a[0]);}catch{X = (int)float.Parse(a[0]);}
                    int Y = 0;try{Y = Convert.ToInt32(a[1]);}catch{Y = (int)float.Parse(a[1]);}
                    return new Point(X, Y);
                }
                catch (Exception ex)
                {
                    if (ThrowException) throw ex;
                    else return new Point(0, 0);
                }
            }
            /// <summary>
            /// X.n,Y.n 로 된 문자열을 System.Drawing.PointF 구조체로 변경해줍니다
            /// </summary>
            /// <param name="Setting">X,Y 로 된 문자열 입니다</param>
            /// <param name="ThrowException">변환 실패시 예외를 발생시킬지의 여부 입니다. (기본값은 True입니다.)</param>
            /// <returns></returns>
            public static PointF StringToPointF(string Setting, bool ThrowException = ThrowExcept)
            {
                try
                {
                    string[] a = Setting.Trim().Split(',');
                    float X = 0;try{X = float.Parse(a[0]);}catch{X = Convert.ToInt32(a[0]);}
                    float Y = 0;try{Y = float.Parse(a[1]);}catch{Y = Convert.ToInt32(a[1]);}
                    return new PointF(X, Y);
                }
                catch (Exception ex)
                {
                    if (ThrowException) throw ex;
                    else return new PointF(0, 0);
                }
            }
                        
            /// <summary>
            /// Width,Height 로 된 문자열을 System.Drawing.Size 구조체로 변경해줍니다
            /// </summary>
            /// <param name="Setting">Width,Height 로 된 문자열 입니다</param>
            /// <param name="ThrowException">변환 실패시 예외를 발생시킬지의 여부 입니다. (기본값은 True입니다.)</param>
            /// <returns></returns>
            public static Size StringToSize(string Setting, bool ThrowException = ThrowExcept)
            {
                Point pt = StringToPoint(Setting, ThrowException);
                return new Size(pt);
            }
            /// <summary>
            /// Width.n,Height.n 로 된 문자열을 System.Drawing.Size 구조체로 변경해줍니다
            /// </summary>
            /// <param name="Setting">Width.n,Height.n 로 된 문자열 입니다</param>
            /// <param name="ThrowException">변환 실패시 예외를 발생시킬지의 여부 입니다. (기본값은 True입니다.)</param>
            /// <returns></returns>
            public static SizeF StringToSizeF(string Setting, bool ThrowException = ThrowExcept)
            {
                PointF pt = StringToPointF(Setting, ThrowException);
                return new SizeF(pt);
            }

            /// <summary>
            /// (Alpha,)Red,Green,Blue 로 된 문자열을 System.Drawing.Color 구조체로 변경해줍니다. (실패시 System.Drawing.Color.Empty (Null 인색)을 반환합니다.)
            /// </summary>
            /// <param name="Setting">(Alpha,)Red,Green,Blue 로 된 문자열 입니다</param>
            /// <param name="ThrowException">변환 실패시 예외를 발생시킬지의 여부 입니다. (기본값은 True입니다.)</param>
            /// <returns>실패시 Color.Empty (Null 인색)을 반환합니다.</returns>
            public static Color StringToColor(string Setting, bool ThrowException = ThrowExcept)
            {
                try
                {
                    string[] a = Setting.Trim().Split(',');
                    if(a.Length<3)throw new Exception("최소한 3개의 숫자가 필요합니다.");
                    byte A = 0;try{A = Convert.ToByte(a[0]);}catch{A = (byte)float.Parse(a[0]);}
                    byte R = 0;try{R = Convert.ToByte(a[1]);}catch{R = (byte)float.Parse(a[1]);}
                    byte G = 0;try{G = Convert.ToByte(a[2]);}catch{G = (byte)float.Parse(a[2]);}
                    if(a.Length==3)return Color.FromArgb(A, R, G);
                    byte B = 0;try{B = Convert.ToByte(a[3]);}catch{B = (byte)float.Parse(a[3]);}
                    return Color.FromArgb(A, R, G, B);
                }
                catch (Exception ex)
                {
                    if (ThrowException) throw ex;
                    else return Color.Empty;
                }
            } 

            public static string PointToString(Point pt, bool Trim = false) {return string.Format(Trim?"{0},{1}":"{0}, {1}", pt.X.ToString(), pt.Y.ToString());}
            public static string PointFToString(PointF pt, bool Trim = false){return string.Format(Trim?"{0},{1}":"{0}, {1}", pt.X.ToString(), pt.Y.ToString());}            
            public static string SizeToString(Size sz, bool Trim = false) {return string.Format(Trim?"{0},{1}":"{0}, {1}", sz.Width.ToString(), sz.Height.ToString());}
            public static string SizeFToString(SizeF sz, bool Trim = false){return string.Format(Trim?"{0},{1}":"{0}, {1}", sz.Width.ToString(), sz.Height.ToString());}

            /// <summary>
            /// 색깔 구조체를 ARGB 문자열 형태로 바꾸어 줍니다.
            /// </summary>
            /// <param name="ARGB">System.Drawing.Color 구조체 입니다.</param>
            /// <param name="Trim">True면 콤마사이를 붙이고 False면 콤마 사이를 띄웁니다.</param>
            /// <returns></returns>
            public static string ColorToString(Color ARGB, bool Trim = false){return string.Format(Trim?"{0},{1},{2},{3}":"{0}, {1}, {2}, {3}", ARGB.A, ARGB.R, ARGB.G, ARGB.B);}
            public static string ColorRGBToString(Color RGB, bool Trim = false){return string.Format(Trim?"{0},{1},{2}":"{0}, {1}, {2}", RGB.R, RGB.G, RGB.B);}
        }

        public static class MathUtility
        {
            public static float Sqrt_Fast(float x)
            {
                float xhalf = 0.5f * x;
                int i=0;
                unsafe {i = *(int*)&x;}
                i = 0x5f375a84 - (i >> 1);
                unsafe {x = *(float*)&i;}

                x = x * (1.5f - xhalf * x * x);
                return 1.0f / x;
            }
            public static double Sqrt_Fast(double x)
            {
                double xhalf = 0.5 * x;
                int i=0;
                unsafe {i = *(int*)&x;}
                i = 0x5f375a84 - (i >> 1);
                unsafe {x = *(double*)&i;}

                x = x * (1.5 - xhalf * x * x);
                return 1.0 / x;
            }
            public static double Pow(double num, int exp)
            {
                double result = 1.0;
                while (exp > 0)
                {
                    if (exp % 2 == 1)
                        result *= num;
                    exp >>= 1;
                    num *= num;
                }

                return result;
            }
            public static float Pow(float num, int exp)
            {
                float result = 1;
                while (exp > 0)
                {
                    if (exp % 2 == 1)
                        result *= num;
                    exp >>= 1;
                    num *= num;
                }

                return result;
            }


        }

        public static class ImageUtility
        {
            public static readonly ImageCodecInfo GetJPGCodec = GetEncoderInfo();
            /// <summary>
            /// 이미지 인코더를 가져옵니다
            /// </summary>
            /// <param name="mimeType">가져올 이미지타입 입니다. (기본으로 image/jpeg 가 들어가 있습니다.)</param>
            /// <returns></returns>
            public static ImageCodecInfo GetEncoderInfo(String mimeType = "image/jpeg")
            {
                int j;
                ImageCodecInfo[] encoders;
                encoders = ImageCodecInfo.GetImageEncoders();
                for (j = 0; j < encoders.Length; ++j)
                {
                    if (encoders[j].MimeType == mimeType)
                        return encoders[j];
                }
                return null;
            }
            /// <summary>
            /// 이미지의 품질을 설정합니다.
            /// </summary>
            /// <param name="Quality">저장될 이미지의 품질 입니다.</param>
            /// <returns></returns>
            public static EncoderParameters CompressionSetting(byte Quality = 90)
            {
                EncoderParameters eps = new EncoderParameters(1);
                eps.Param[0] = new EncoderParameter(System.Drawing.Imaging.Encoder.Quality, (long)Quality);
                return eps;
            }
            public static Bitmap ToMonochromatic(Image img, bool Dispose = false)
            {
                Bitmap bmp = new Bitmap(img);
                for(int i=0; i < bmp.Width; i++)
                    for(int j=0; j < bmp.Height; j++)
                    {
                        Color c = bmp.GetPixel(i, j);
                        int a = (c.R + c.G + c.B) / 3;
                        bmp.SetPixel(i, j, Color.FromArgb(c.A, a, a, a));
                    }
                img.Dispose();
                return bmp;
            }
            public static void ToMonochromaticInstance(Image img)
            {
                Bitmap bmp = (Bitmap)img;
                for (int i = 0; i < bmp.Width; i++)
                    for (int j = 0; j < bmp.Height; j++)
                    {
                        Color c = bmp.GetPixel(i, j);
                        int a = (c.R + c.G + c.B) / 3;
                        bmp.SetPixel(i, j, Color.FromArgb(c.A, a, a, a));
                    }
            }
        }
    }
    [Serializable]
    public class LoggingUtility
    {
        public static bool Logging(string LogMessage)
        {
            if (!Directory.Exists(Application.StartupPath + "\\Log")) Directory.CreateDirectory(Application.StartupPath + "\\Log");
            string Path = string.Format("{0}\\Log\\{1}-{2}-{3}.log", Application.StartupPath, DateTime.Today.Year.ToString("0000"), DateTime.Today.Month.ToString("00"), DateTime.Today.Day.ToString("00"));
            string time = DateTime.Now.ToLongTimeString();
            File.AppendAllText(Path, "==========" + time + "==========\r\n   " + LogMessage + "\r\n\r\n");
            return true;
        }
        public static bool Logging(Exception ex,string Description=null)
        {
            if (!Directory.Exists(Application.StartupPath + "\\Log")) Directory.CreateDirectory(Application.StartupPath + "\\Log");
            string Path = string.Format("{0}\\Log\\{1}-{2}-{3}.log", Application.StartupPath, DateTime.Today.Year.ToString("0000"), DateTime.Today.Month.ToString("00"), DateTime.Today.Day.ToString("00"));
            string time = DateTime.Now.ToLongTimeString();
            if (Description != null) File.AppendAllText(Path, string.Format("=========={0}==========\r\n예외:   {1}\r\n└객체:   {2}\r\n└메세지: {3}\r\n{4}\r\n└메서드: {5}\r\n└설명: {6}\r\n\r\n", time, ex.GetType(), ex.Source, ex.Message, ex.StackTrace.Substring(3).Insert(0, "└").Insert(4, "  "), ex.TargetSite.ToString(), Description));
            else File.AppendAllText(Path, string.Format("=========={0}==========\r\n예외:   {1}\r\n└객체:   {2}\r\n└메세지: {3}\r\n{4}\r\n└메서드: {5}\r\n\r\n", time, ex.GetType(), ex.Source, ex.Message, ex.StackTrace.Substring(3).Insert(0, "└").Insert(4, "  "), ex.TargetSite.ToString()));
            return true;
        }
        public static bool LoggingT<T>(T ex)
        {
            if (!Directory.Exists(Application.StartupPath + "\\Log")) Directory.CreateDirectory(Application.StartupPath + "\\Log");
            string Path = string.Format("{0}\\Log\\{1}-{2}-{3}.log", Application.StartupPath, DateTime.Today.Year.ToString("0000"), DateTime.Today.Month.ToString("00"), DateTime.Today.Day.ToString("00"));
            string time = DateTime.Now.ToLongTimeString();
            File.AppendAllText(Path, string.Format("=========={0}==========\r\n{1}\r\n\r\n", time, ex.ToString()));
            return true;
        }
        public static bool LoggingT<T>(string Add, T ex)
        {
            if (!Directory.Exists(Application.StartupPath + "\\Log")) Directory.CreateDirectory(Application.StartupPath + "\\Log");
            string Path = string.Format("{0}\\Log\\{1}-{2}-{3}.log", Application.StartupPath, DateTime.Today.Year.ToString("0000"), DateTime.Today.Month.ToString("00"), DateTime.Today.Day.ToString("00"));
            string time = DateTime.Now.ToLongTimeString();
            File.AppendAllText(Path, string.Format("=========={0}==========\r\n{1}\r\n{2}\r\n\r\n", time, Add, ex.ToString()));
            return true;
        }
    }

    public class CpuUsage
    {
        [System.Runtime.InteropServices.DllImport("kernel32.dll", SetLastError = true)]
        static extern bool GetSystemTimes(out FILETIME lpIdleTime,
                    out FILETIME lpKernelTime, out FILETIME lpUserTime);

        FILETIME _prevSysKernel;
        FILETIME _prevSysUser;
        FILETIME _prevSysIdle;

        TimeSpan _prevProcTotal;

        public CpuUsage()
        {
            _prevProcTotal = TimeSpan.MinValue;
        }

        public float GetUsage(out float processorCpuUsage)
        {
            processorCpuUsage = 0.0f;

            float _processCpuUsage = 0.0f;
            FILETIME sysIdle, sysKernel, sysUser;

            Process process = Process.GetCurrentProcess();
            TimeSpan procTime = process.TotalProcessorTime;

            if (!GetSystemTimes(out sysIdle, out sysKernel, out sysUser))
            {
                return 0.0f;
            }

            if (_prevProcTotal != TimeSpan.MinValue)
            {
                ulong sysKernelDiff = SubtractTimes(sysKernel, _prevSysKernel);
                ulong sysUserDiff = SubtractTimes(sysUser, _prevSysUser);
                ulong sysIdleDiff = SubtractTimes(sysIdle, _prevSysIdle);

                ulong sysTotal = sysKernelDiff + sysUserDiff;
                long kernelTotal = (long)(sysKernelDiff - sysIdleDiff);

                if (kernelTotal < 0)
                {
                    kernelTotal = 0;
                }

                processorCpuUsage = (float)((((ulong)kernelTotal + sysUserDiff) * 100.0) / sysTotal);

                long procTotal = (procTime.Ticks - _prevProcTotal.Ticks);

                if (sysTotal > 0)
                {
                    _processCpuUsage = (short)((100.0 * procTotal) / sysTotal);
                }
            }

            _prevProcTotal = procTime;
            _prevSysKernel = sysKernel;
            _prevSysUser = sysUser;
            _prevSysIdle = sysIdle;

            return _processCpuUsage;
        }

        private UInt64 SubtractTimes(FILETIME a, FILETIME b)
        {
            ulong aInt = ((ulong)a.dwHighDateTime << 32) | (uint)a.dwLowDateTime;
            ulong bInt = ((ulong)b.dwHighDateTime << 32) | (uint)b.dwLowDateTime;

            return aInt - bInt;
        }

        public class GetCPUUsage
        {
            static TimeSpan start;
            public static double CPUUsageTotal
            {
                get;
                private set;
            }
            public static double CPUUsageLastMinute
            {
                get;
                private set;
            }
            static TimeSpan oldCPUTime = new TimeSpan(0);
            static DateTime lastMonitorTime = DateTime.UtcNow;
            public static DateTime StartTime = DateTime.UtcNow;

            // Call it once everything is ready
            static void OnStartup()
            {
                start = Process.GetCurrentProcess().TotalProcessorTime;
            }

            // Call this every 30 seconds
            static void CallCPU()
            {
                TimeSpan newCPUTime = Process.GetCurrentProcess().TotalProcessorTime - start;
                CPUUsageLastMinute = (newCPUTime - oldCPUTime).TotalSeconds /
                                     (Environment.ProcessorCount * DateTime.UtcNow.Subtract(lastMonitorTime).TotalSeconds);
                lastMonitorTime = DateTime.UtcNow;
                CPUUsageTotal = newCPUTime.TotalSeconds /
                                (Environment.ProcessorCount * DateTime.UtcNow.Subtract(StartTime).TotalSeconds);
                oldCPUTime = newCPUTime;

            }
        }
        public class GetCPUInfo
        {
            public static string GetInfoMinute()
            {
                return String.Format("{0:0.0}", GetCPUUsage.CPUUsageLastMinute * 100);
            }

            public static string GetInfoTotal()
            {
                return String.Format("{0:0.0}", GetCPUUsage.CPUUsageTotal * 100);
            }
        }
    }

    /// <summary>
    /// 기본 프로그램 경로를 가져오거나 설정합니다.
    /// </summary>
    sealed public class DefaultProgram
    {
        /// <summary>
        /// 기본 웹브라우저의 경로를 가져오거나 설정합니다.
        /// </summary>
        public static string DefaultWebBrowser
        {
            get
            {
                Microsoft.Win32.RegistryKey regKey = Microsoft.Win32.Registry.ClassesRoot.OpenSubKey(@"HTTP\shell\open\command", false);
                //get rid of the enclosing quotes
                //string name = "";
                string name1 = regKey.GetValue(null).ToString().Replace("" + (char)34, "");
                string name2 = regKey.GetValue(null).ToString().ToLower().Replace("" + (char)34, "");
                //check to see if the value ends with .exe (this way we can remove any command line arguments)
                //if (!name.EndsWith("exe"))
                //get rid of all command line arguments (anything after the .exe must go)
                return name1.Remove(name2.LastIndexOf(".exe") + 4);
                //else { return ""; }
            }
            set
            {
                Microsoft.Win32.RegistryKey regKey = Microsoft.Win32.Registry.ClassesRoot.OpenSubKey(@"HTTP\shell\open\command", true);
                regKey.SetValue(null, value);
                regKey.Close();
            }
        }
    }

    public class HSRandom
    {
        Random rnd = new Random();

        public HSRandom(){}
        public HSRandom(bool 중복){this.중복 =  중복;}
        public HSRandom(int Seed){ rnd = new Random(Seed);}
        public HSRandom(int Seed, bool 중복){ rnd = new Random(Seed); this.중복 = 중복;}
        
        public bool 중복{get;set;}

        private bool UseMinValue;
        private List<int> 중복된수 = new List<int>();

        public void Clear(){중복된수.Clear();}
        public bool Remove(int 수){return 중복된수.Remove(수);}

        public int NextValue()
        {
            if (중복)
            {
                int Count=0;
            Retry:
                Count++;
                int a = rnd.Next();
                foreach(int 수 in 중복된수)
                    if(수 == a)goto Retry;
                중복된수.Add(a);
                return a;
            }
            else return rnd.Next();
        }
        public int NextValue_Safe()
        {
            if (중복)
            {
                int a = rnd.Next();
                foreach (int 수 in 중복된수)
                    if (수 == a)
                    {
                        a = rnd.Next();
                        중복된수.Add(a);
                        return a;
                    }
                중복된수.Add(a);
                return a;
            }
            else return rnd.Next();
        }
        public int NextValue(int MaxValue)
        {
            if (중복)
            {
                int Count=중복된수.Count;
            Retry:
                int a = rnd.Next(MaxValue);
                foreach (int 수 in 중복된수)
                    if (수 == a) goto Retry;
                중복된수.Add(a);
                return a;
            }
            else return rnd.Next(MaxValue);
        }
        public int NextValue(int MinValue, int MaxValue)
        {
            if (중복) {
                int count = 0;
            Retry:
                int a = rnd.Next(MinValue, MaxValue);
            foreach (int 수 in 중복된수)
            {
                if (수 == a){ count++;goto Retry;}
                if((Math.Max(MinValue, MaxValue)-Math.Min(MinValue, MaxValue)) <= count){Clear();break;}
            }
                중복된수.Add(a); return a; }
            else return rnd.Next(MinValue, MaxValue);
        }
        public int NextValue_Safe(int MinValue, int MaxValue)
        {
            if (중복)
            {
                int a = rnd.Next(MinValue, MaxValue);
                foreach (int 수 in 중복된수)
                    if (수 == a)
                    {
                        a = rnd.Next();
                        중복된수.Add(a);
                        return a;
                    }
                중복된수.Add(a);
                return a;
            }
            else return rnd.Next(MinValue, MaxValue);
        }
    }
}
namespace HS_CSharpUtility.Extension
{
    using System.Windows.Forms;
    using System.Reflection;
    using System.ComponentModel;

    /// <summary>
    /// 컨트롤의 쓰레드 동기화 확장 입니다. (컨트롤 크로스 쓰레드 오류가 있을떄 사용하십시오)
    /// 사용법1 (람다식 사용): textBox1.InvokeifNeeded(() => textBox1.Text="문자열");
    /// 출처: http://www.devpia.com/Maeul/Contents/Detail.aspx?BoardID=18&MAEULNO=8&no=1723&page=12
    /// </summary>
    public static class ControlExtensions
    {
        /// <summary>
        /// 컨트롤의 쓰레드 동기화 확장 입니다. (컨트롤 크로스 쓰레드 오류가 있을떄 사용하십시오)
        /// 사용법1 (람다식 사용): textBox1.InvokeifNeeded(() => textBox1.Text="문자열");
        /// 출처: http://www.devpia.com/Maeul/Contents/Detail.aspx?BoardID=18&MAEULNO=8&no=1723&page=12
        /// </summary>
        public static void InvokeIfNeeded(this Control control, Action action)
        {
            if (control.InvokeRequired)
                control.BeginInvoke(action);
            else
                action();
        }
        public static void InvokeIfNeeded<T>(this Control control, Action<T> action, T Args)
        {
            if (control.InvokeRequired)
                control.Invoke(action, Args);
            else
                action(Args);
        }
        public static Bitmap ConvertToBitmap(this Control control)
        {
            Bitmap bmp = new Bitmap(control.Width, control.Height);
            control.DrawToBitmap(bmp, new Rectangle(0, 0, control.Width, control.Height));
            return bmp;
        }
    }
    public static class StringArrayExtensions
    {
        public static string ConvertArrayStringToString(this string[] Args)
        {
            StringBuilder a = new StringBuilder(Args[0]);
            for (long i = 1; i < Args.LongLength; i++) { a.Append(Args[i] + "\r\n"); }
            return a.ToString();
        }
    }
    public static class StringExtensions
    {
        /// <summary>
        /// 문자열에서 문자열 배열중에 1개라도 일치하는 항목있는지 알아냅니다 
        /// </summary>
        /// <param name="Text">원본 문자열</param>
        /// <param name="Array">검사할 문자열 배열 입니다.</param>
        /// <returns>문자열에서 문자열 배열중에 1개라도 일치하는 항목있는지의 여부입니다.</returns>
        public static bool ArrayIncludeString(this string Text, string[] Array)
        {
            foreach (string a in Array)
            { if (Text.IndexOf(a) != -1)return true; }
            return false;
        }
        /*
        /// <summary>
        /// 문자열에서 캐릭터(char) 배열중에 1개라도 일치하는 항목있는지 알아냅니다 
        /// </summary>
        /// <param name="Text">원본 문자열</param>
        /// <param name="Array">검사할 캐릭터(char) 배열 입니다</param>
        /// <returns>문자열에서 캐릭터(char) 배열중에 1개라도 일치하는 항목있는지의 여부입니다.</returns>
        public static bool ArrayIncludeString(this string Text, char[] Array) { return ArrayIncludeString(Text, Array); }*/

        public static string MakeValidFileName(this string FileName, bool UnderBar = false)
        {
            if (UnderBar)
            {
                //FileFileName = FileName.Replace('\'', '’'); // U+2019 right single quotation mark
                FileName = FileName.Replace('"', '”'); // U+201D right double quotation mark
                FileName = FileName.Replace('/', '⁄');  // U+2044 fraction slash
                FileName = FileName.Replace(':', '：');
                FileName = FileName.Replace('?', '？');
                FileName = FileName.Replace('<', '＜');
                FileName = FileName.Replace('>', '＞');
                FileName = FileName.Replace('|', 'ǀ');
            }
            else foreach (char c in System.IO.Path.GetInvalidFileNameChars()) if (c != '\\') FileName = FileName.Replace(c, '_');
            return FileName;
        }
        /*
        public static string MakeValidFileName(string FileName)
        {
            string invalidChars = System.Text.RegularExpressions.Regex.Escape(new string(System.IO.Path.GetInvalidFileNameChars()));
            string invalidRegStr = string.Format(@"([{0}]*\.+$)|([{0}]+)", invalidChars);

            return System.Text.RegularExpressions.Regex.Replace(FileName, invalidRegStr, "_");
        }*/

        public static long ExtractNumbers(this string text)
        {
            char[] array = text.ToCharArray();
            StringBuilder sb = new StringBuilder("0");
            foreach (char a in array)if(char.IsDigit(a)) sb.Append(a);
            return long.Parse(sb.ToString());
        }
        public static string ExtractNumber(this string text)
        {
            char[] array = text.ToCharArray();
            StringBuilder sb = new StringBuilder();
            foreach (char a in array) if (char.IsDigit(a)) sb.Append(a);
            return sb.ToString();
        }
    }

    public static class BitmapExtension
    {
        /*
        public static Bitmap NegativeBitmap(this Bitmap sourceBitmap)
        {
            Bitmap bitmapResult = new Bitmap(sourceBitmap.Width, sourceBitmap.Height);
            for (int x = 0; x < sourceBitmap.Width; x++)
            {
                for (int y = 0; y < sourceBitmap.Height; y++)
                { bitmapResult.SetPixel(x, y,sourceBitmap.GetPixel(x, y).NegativeColor()); }
            }
            return bitmapResult;
        }*/
        public static Bitmap NegativeImage(this Image image)
        {
            Bitmap bitmapResult = (Bitmap)image;
            for (int x = 0; x < bitmapResult.Width; x++)
            {
                for (int y = 0; y < bitmapResult.Height; y++)
                { bitmapResult.SetPixel(x, y, bitmapResult.GetPixel(x, y).NegativeColor()); }
            }
            return bitmapResult;
        }
        
        public static Bitmap NegativeBitmap(this Bitmap image)
        {
            Bitmap bitmapResult = image;
            for (int x = 0; x < bitmapResult.Width; x++)
            {
                for (int y = 0; y < bitmapResult.Height; y++)
                { bitmapResult.SetPixel(x, y, bitmapResult.GetPixel(x, y).NegativeColor()); }
            }
            return bitmapResult;
        }
        public static Bitmap NegativeImageFast(this Image image, bool Alpha = false)
        {
            Bitmap bitmapResult = (Bitmap)image;
            return NegativeBitmapFast(bitmapResult, Alpha);
        }
        public static Bitmap NegativeBitmapFast(this Bitmap image, bool Alpha = false)
        {
            Bitmap bitmapResult = image;

            BitmapData bd = bitmapResult.LockBits(new Rectangle(0, 0, bitmapResult.Width, bitmapResult.Height), ImageLockMode.ReadWrite, Alpha?PixelFormat.Format32bppArgb:PixelFormat.Format24bppRgb);
            int Add = bd.Stride / bd.Width;//pf == PixelFormat.Format32bppArgb ? 4 : 3;
            //int Err = 4;//bd.Stride - (bd.Width * Add);
            unsafe
            {
                try
                {
                    byte* data = (byte*)bd.Scan0.ToPointer();
                    for (int i = 0; i < bd.Height; i++)
                    {
                        for (int j = 0; j < bd.Width; j++, data += Add)
                        {
                            //int a = j - Err;
                            data[0] = (byte)(255 - data[0]);
                            data[1] = (byte)(255 - data[1]);
                            data[2] = (byte)(255 - data[2]);
                            if (Alpha && Add == 4) data[3] = (byte)(255 - data[3]);
                        }
                    }
                }finally{bitmapResult.UnlockBits(bd);}
            }
            return bitmapResult;
        }

        public static Bitmap RotateImage(this Bitmap b, float angle)
        {
            //create a new empty bitmap to hold rotated image
            Bitmap returnBitmap = new Bitmap(b.Width, b.Height);
            //make a graphics object from the empty bitmap
            using (Graphics g = Graphics.FromImage(returnBitmap))
            {
                //move rotation point to center of image
                g.TranslateTransform((float)b.Width / 2, (float)b.Height / 2);
                //rotate
                g.RotateTransform(angle);
                //move image back
                g.TranslateTransform(-(float)b.Width / 2, -(float)b.Height / 2);
                //draw passed in image onto graphics object
                g.DrawImage(b, new Point(0, 0));
            }
            return returnBitmap;
        }
        /// <summary>
        /// 그림 주변에 테두리를 그립니다.
        /// </summary>
        /// <param name="image">테두리를 그릴 그림 입니다.</param>
        /// <param name="color">테두리의 색깔 입니다.</param>
        /// <param name="Bold">테두리의 두꼐 입니다.</param>
        /// <returns></returns>
        public static Bitmap DrawEdge(this Image image, Color color, byte Bold = 1)
        {
            Bitmap bitmapResult = (Bitmap)image;
            for (int i = 1; i < image.Width; i++) { for (byte j = 1; j < Bold + 1; j++) bitmapResult.SetPixel(i, j, color); }//윗줄
            for (int i = 1; i < image.Width; i++) { for (int j = image.Height - Bold - 1; j < image.Height - 1; j++) bitmapResult.SetPixel(i, j, color); }//밑줄
            for (int i = 1; i < image.Height; i++) { for (int j = 1; j < Bold + 1; j++) bitmapResult.SetPixel(j, i, color); }//왼쪽 줄
            for (int i = 1; i < image.Height; i++) { for (int j = image.Width - Bold - 1; j < image.Width - 1; j++) bitmapResult.SetPixel(j, i, color); }//오른쪽 줄
            return bitmapResult;
        }
        public static Bitmap DrawEdgeBitmap(this Bitmap image, Color color, byte Bold = 1)
        {
            Bitmap bitmapResult = image;
            for (int i = 1; i < image.Width; i++) { for (byte j = 1; j < Bold + 1; j++) bitmapResult.SetPixel(i, j, color); }//윗줄
            for (int i = 1; i < image.Width; i++) { for (int j = image.Height - Bold - 1; j < image.Height - 1; j++) bitmapResult.SetPixel(i, j, color); }//밑줄
            for (int i = 1; i < image.Height; i++) { for (int j = 1; j < Bold + 1; j++) bitmapResult.SetPixel(j, i, color); }//왼쪽 줄
            for (int i = 1; i < image.Height; i++) { for (int j = image.Width - Bold - 1; j < image.Width - 1; j++) bitmapResult.SetPixel(j, i, color); }//오른쪽 줄
            return bitmapResult;
        }
        public static Bitmap DrawEdgeBitmapFast(this Bitmap image, Color color, byte Bold = 1, bool UseAlpha = false)
        {
            Bitmap bitmapResult = image;
            BitmapData bd = bitmapResult.LockBits(new Rectangle(0, 0, bitmapResult.Width, bitmapResult.Height), ImageLockMode.ReadWrite, UseAlpha ? PixelFormat.Format32bppArgb : PixelFormat.Format24bppRgb);
            int Add = bd.Stride / bd.Width;//pf == PixelFormat.Format32bppArgb ? 4 : 3;
            int Err = bd.Stride - (bd.Width * Add);

            unsafe
            {
                try
                {
                    byte* point = (byte*)bd.Scan0.ToPointer();
                    int offset = 0;
                    for (int i = 0; i < bd.Height; i++)
                    {
                        for (int j = 0; j < Bold; j++)
                        {
                            offset = bd.Stride * i + (j * Add);
                            point[offset] = color.B;
                            point[offset + 1] = color.G;
                            point[offset + 2] = color.R;
                            if (UseAlpha && Add == 4) point[offset + 3] = color.A;
                            offset = bd.Stride * (i + 1) - ((j + 1) * Add);
                            point[offset] = color.B;
                            point[offset + 1] = color.G;
                            point[offset + 2] = color.R;
                            if (UseAlpha && Add == 4) point[offset + 3] = color.A;
                        }
                    }
                    //offset = bd.Stride * (bd.Height - 1);
                    for (int i = 0; i < bd.Width; i++)
                    {
                        for (int j = 0; j < Bold; j++)
                        {
                            offset = Add * i + (j * /*4+*/bd.Stride);
                            point[offset] = color.B;
                            point[offset + 1] = color.G;
                            point[offset + 2] = color.R;
                            if (UseAlpha && Add == 4) point[offset + 3] = color.A;
                            offset = bd.Stride*(bd.Height-j)-i*Add;
                            point[offset] = color.B;
                            point[offset + 1] = color.G;
                            point[offset + 2] = color.R;
                            if (UseAlpha && Add == 4) point[offset + 3] = color.A;
                        }
                    }
                }catch{}
                finally{bitmapResult.UnlockBits(bd);}
            }
            return bitmapResult;
            /*
            for (int i = 1; i < image.Width; i++) { for (byte j = 1; j < Bold + 1; j++) bitmapResult.SetPixel(i, j, color); }//윗줄
            for (int i = 1; i < image.Width; i++) { for (int j = image.Height - Bold - 1; j < image.Height - 1; j++) bitmapResult.SetPixel(i, j, color); }//밑줄
            for (int i = 1; i < image.Height; i++) { for (int j = 1; j < Bold + 1; j++) bitmapResult.SetPixel(j, i, color); }//왼쪽 줄
            for (int i = 1; i < image.Height; i++) { for (int j = image.Width - Bold - 1; j < image.Width - 1; j++) bitmapResult.SetPixel(j, i, color); }//오른쪽 줄
            return bitmapResult;*/
        }

        public static Bitmap Clear(this Bitmap image, bool UseAlpha = true)
        {
            for (int i = 0; i < image.Width; i++)
            {
                for (int j = 0; j < image.Height; j++)
                    if (UseAlpha) image.SetPixel(i, j, Color.Transparent);
                    else image.SetPixel(i, j, Color.White);
            }
            return image;
        }

        public static Bitmap CopyToSquareCanvas(this Bitmap sourceBitmap, int canvasWidthLenght)
        {
            float ratio = 1.0f;
            int maxSide = sourceBitmap.Width > sourceBitmap.Height ?
                          sourceBitmap.Width : sourceBitmap.Height;

            ratio = (float)maxSide / (float)canvasWidthLenght;

            Bitmap bitmapResult = (sourceBitmap.Width > sourceBitmap.Height ?
                                    new Bitmap(canvasWidthLenght, (int)(sourceBitmap.Height / ratio))
                                    : new Bitmap((int)(sourceBitmap.Width / ratio), canvasWidthLenght));

            using (Graphics graphicsResult = Graphics.FromImage(bitmapResult))
            {
                graphicsResult.CompositingQuality = CompositingQuality.HighQuality;
                graphicsResult.InterpolationMode = InterpolationMode.HighQualityBicubic;
                graphicsResult.PixelOffsetMode = PixelOffsetMode.HighQuality;

                graphicsResult.DrawImage(sourceBitmap,
                                        new Rectangle(0, 0,
                                            bitmapResult.Width, bitmapResult.Height),
                                        new Rectangle(0, 0,
                                            sourceBitmap.Width, sourceBitmap.Height),
                                            GraphicsUnit.Pixel);
                graphicsResult.Flush();
            }

            return bitmapResult;
        }
        public static Bitmap CopyToSquareCanvas_Fast(this Bitmap sourceBitmap, int canvasWidthLenght, bool DisposeImage = false)
        {
            float ratio = 1.0f;
            int maxSide = sourceBitmap.Width > sourceBitmap.Height ?
                          sourceBitmap.Width : sourceBitmap.Height;

            ratio = (float)maxSide / (float)canvasWidthLenght;

            PixelFormat pf = sourceBitmap.PixelFormat.SmartFormatConvert();

            Bitmap bitmapResult = (sourceBitmap.Width > sourceBitmap.Height ?
                                    new Bitmap(canvasWidthLenght, (int)(sourceBitmap.Height / ratio), pf)
                                    : new Bitmap((int)(sourceBitmap.Width / ratio), canvasWidthLenght, pf));
            sourceBitmap.ResizeImage_NearestNeighbor(bitmapResult, pf, false, DisposeImage);

            return bitmapResult;
        }

        #region CreateCursor에 필요한 것들
        public struct IconInfo
        {
            public bool fIcon;
            public int xHotspot;
            public int yHotspot;
            public IntPtr hbmMask;
            public IntPtr hbmColor;
        }
        [DllImport("user32.dll")]
        public static extern IntPtr CreateIconIndirect(ref IconInfo icon);

        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool GetIconInfo(IntPtr hIcon, ref IconInfo pIconInfo);
        #endregion
        public static Cursor CreateCursor(this Bitmap bmp, int xHotSpot = 1, int yHotSpot = 1)
        {
            IconInfo tmp = new IconInfo();
            GetIconInfo(bmp.GetHicon(), ref tmp);
            tmp.xHotspot = xHotSpot;
            tmp.yHotspot = yHotSpot;
            tmp.fIcon = false;
            return new Cursor(CreateIconIndirect(ref tmp));
        }

        public static void SaveJPG(this Bitmap image, string ImagePath, byte Quility = 90)
        {image.Save(ImagePath,Utility.ImageUtility.GetJPGCodec, Utility.ImageUtility.CompressionSetting(Quility)); }
        public static void SaveJPG(this Bitmap image, Stream ImageStream, byte Quility = 90)
        {image.Save(ImageStream,Utility.ImageUtility.GetJPGCodec, Utility.ImageUtility.CompressionSetting(Quility)); }

        public static Bitmap ChangeColor(this Bitmap bmp, Color OriginalColor, Color ChangeColor, bool ExcludeAlpha = true)
        {
            if (ExcludeAlpha)
            {
                for (int i = 0; i < bmp.Width; i++)
                    for (int j = 0; j < bmp.Height; j++)
                        if (bmp.GetPixel(i, j).R == OriginalColor.R &&
                           bmp.GetPixel(i, j).G == OriginalColor.G &&
                           bmp.GetPixel(i, j).B == OriginalColor.B) bmp.SetPixel(i, j, Color.FromArgb(bmp.GetPixel(i, j).A, ChangeColor));
            }
            else
            {
                for (int i = 0; i < bmp.Width; i++)
                    for (int j = 0; j < bmp.Height; j++)
                        if (bmp.GetPixel(i, j) == OriginalColor) bmp.SetPixel(i, j, ChangeColor);
            }
            return bmp;
        }

        public static Icon GetIcon(this Bitmap bmp)
        {
            IntPtr point = bmp.GetHicon();
            Icon i = Icon.FromHandle(point);
            return i;
        }

        public static Bitmap ToMonochromatic(this Bitmap img)
        {
            return Utility.ImageUtility.ToMonochromatic(img);
        }
        public static void ToMonochromaticInstance(this Bitmap img)
        {
            Utility.ImageUtility.ToMonochromaticInstance(img);
        }
    }
    public static class ImageExtension
    {
        public static Bitmap ToBitmap(this Image image)
        {
            Bitmap bmp = new Bitmap(image);
            return bmp;
        }
        /*
        public class BitmapFormat
        {
            public BitmapFormat(Bitmap bmp)
            {
                switch (bmp.PixelFormat)
                {
                    case PixelFormat.Format16bppArgb1555:
                    Bit = new byte[]{1, 5, 5, 5};Indexed = false; break;
                    case PixelFormat.:
                    Bit = new byte[] { 1, 5, 5, 5 }; Indexed = false; break;
                }
            }
            public byte[] Bit{get;private set;}
            public bool Indexed{get;private set;}
            public PixelFormat OriginalFormat{get;private set;}
        }*/
        public static PixelFormat SmartFormatConvert(this PixelFormat Format)
        {
            switch (Format)
            {
                case PixelFormat.Format16bppRgb555:case PixelFormat.Format16bppRgb565: return PixelFormat.Format24bppRgb;
                case PixelFormat.Format16bppArgb1555:return PixelFormat.Format32bppArgb;
                case PixelFormat.Format1bppIndexed: case PixelFormat.Format4bppIndexed:return PixelFormat.Format24bppRgb;//Format8bppIndexed;
                default: return Format;
            }
        }
        #region 이미지 크기조정 1 (관리)
        /// <summary>
        /// 이미지 크기를 조정합니다. (출처: http://www.mindstick.com/Articles/b1984687-85b1-469f-b874-c6b0fd4f1d48/Resizing%20Image%20in%20C)
        /// </summary>
        /// <param name="imgPhoto">변경할 대상 이미지 입니다.</param>
        /// <param name="Percent">변경할 비율 입니다.</param>
        /// <returns></returns>
        public static Bitmap ResizeImage(this Image imgPhoto, float Percent)
        {
            float nPercent = ((float)Percent / 100);

            int sourceWidth = imgPhoto.Width;      //store original width of source image.
            int sourceHeight = imgPhoto.Height;    //store original height of source image.
            int sourceX = 0;        //x-axis of source image.
            int sourceY = 0;         //y-axis of source image.

            int destX = 0;          //x-axis of destination image.
            int destY = 0;          //y-axis of destination image.

            //Calcuate height and width of resized image.
            int destWidth = (int)(sourceWidth * nPercent);
            int destHeight = (int)(sourceHeight * nPercent);

            //Create a new bitmap object.
            Bitmap bmPhoto = new Bitmap(destWidth, destHeight);

            //Set resolution of bitmap.
            bmPhoto.SetResolution(imgPhoto.HorizontalResolution, imgPhoto.VerticalResolution);

            Graphics grPhoto = null;
            BitmapData bd = null;
            try
            {
                //bd = bmPhoto.LockBits(new Rectangle(0, 0, bmPhoto.Width, bmPhoto.Height), ImageLockMode.ReadWrite, bmPhoto.PixelFormat);
                //Create a graphics object and set quality of graphics.
                grPhoto = Graphics.FromImage(bmPhoto);
                grPhoto.InterpolationMode = Percent < 26 ? InterpolationMode.HighQualityBicubic :
                    Percent < 51 ? InterpolationMode.Bicubic : InterpolationMode.Bilinear;



                //Draw image by using DrawImage() method of graphics class.
                grPhoto.DrawImage(imgPhoto,
                    new Rectangle(destX, destY, destWidth, destHeight),
                    new Rectangle(sourceX, sourceY, sourceWidth, sourceHeight),
                    GraphicsUnit.Pixel);
            }
            catch
            {
                if(bd!=null)bmPhoto.UnlockBits(bd);
                bmPhoto.Dispose();
                bmPhoto = null;
            }
            finally
            {
                //Dispose graphics object.
                if(grPhoto!=null)grPhoto.Dispose();
                if(bmPhoto!=null&&bd!=null)bmPhoto.UnlockBits(bd);
            }

            return bmPhoto;
        }        
        [Obsolete]
        /// <summary>
        /// 이미지 크기를 조정합니다. (출처: http://www.mindstick.com/Articles/b1984687-85b1-469f-b874-c6b0fd4f1d48/Resizing%20Image%20in%20C)
        /// </summary>
        /// <param name="imgPhoto">변경할 대상 이미지 입니다.</param>
        /// <param name="Percent">변경할 비율 입니다.</param>
        /// <returns></returns>
        public static Bitmap ResizeImageWithNoInstance(this Image imgPhoto, float Percent)
        {
            float nPercent = ((float)Percent / 100);

            int sourceWidth = imgPhoto.Width;      //store original width of source image.
            int sourceHeight = imgPhoto.Height;    //store original height of source image.
            int sourceX = 0;        //x-axis of source image.
            int sourceY = 0;         //y-axis of source image.

            int destX = 0;          //x-axis of destination image.
            int destY = 0;          //y-axis of destination image.

            //Calcuate height and width of resized image.
            int destWidth = (int)(sourceWidth * nPercent);
            int destHeight = (int)(sourceHeight * nPercent);

            float tmp1 = imgPhoto.HorizontalResolution, tmp2 = imgPhoto.VerticalResolution;
            //Create a new bitmap object.
            Bitmap bmPhoto = (Bitmap)imgPhoto.Clone();//new Bitmap(destWidth, destHeight);

            //Set resolution of bitmap.
            bmPhoto.SetResolution(imgPhoto.HorizontalResolution, imgPhoto.VerticalResolution);

            //Create a graphics object and set quality of graphics.
            Graphics grPhoto = Graphics.FromImage(bmPhoto);
            grPhoto.InterpolationMode = Percent < 26 ? InterpolationMode.HighQualityBicubic :
                Percent < 51 ? InterpolationMode.Bicubic : InterpolationMode.Bilinear;

            //Draw image by using DrawImage() method of graphics class.
            grPhoto.DrawImage(imgPhoto,
                new Rectangle(destX, destY, destWidth, destHeight),
                new Rectangle(sourceX, sourceY, sourceWidth, sourceHeight),
                GraphicsUnit.Pixel);

            //Dispose graphics object.
            grPhoto.Dispose();

            return bmPhoto;
        }
        /*
        [Obsolete]
        public static Bitmap ResizeImage(this Image image, byte Percent, bool Width,bool PreserveAspectRatio = true)
        {
            int newWidth=0;
            int newHeight=0;
            if (Width)
            {
                newWidth = image.Width.Percent(Percent);
                newHeight = image.Height - (image.Width - newWidth);
            }
            else
            {
                newHeight = image.Height.Percent(Percent);
                newWidth = image.Width - (image.Height - newHeight);
            }
            
            if (PreserveAspectRatio)
            {
                int originalWidth = image.Width;
                int originalHeight = image.Height;
                float percentWidth = (float)newWidth / (float)originalWidth;
                float percentHeight = (float)newHeight / (float)originalHeight;
                float percent = percentHeight < percentWidth ? percentHeight : percentWidth;
                newWidth = (int)(originalWidth * percent);
                newHeight = (int)(originalHeight * percent);
            }
            Bitmap newImage = new Bitmap(newWidth, newHeight);
            using (Graphics graphicsHandle = Graphics.FromImage(newImage))
            {
                graphicsHandle.InterpolationMode = InterpolationMode.HighQualityBicubic;
                graphicsHandle.DrawImage(image, 0, 0, newWidth, newHeight);
            }
            return newImage;
        }*/

        /// <summary>
        /// 이미지 크기를 조정합니다. (출처: http://www.codeproject.com/Articles/191424/Resizing-an-Image-On-The-Fly-using-NET)
        /// </summary>
        /// <param name="image">변경할 대상 이미지 입니다.</param>
        /// <param name="Width">변경할 가로 길이 입니다.</param>
        /// <param name="Height">변경할 세로 길이 입니다.</param>
        /// <param name="PreserveAspectRatio">True면 가로세로 비율을 유지하고 False면 유지하지 않습니다.</param>
        /// <returns></returns>
        public static Bitmap ResizeImage(this Image image, int Width, int Height, bool PreserveAspectRatio = true)
        {
            int newWidth = Width;
            int newHeight = Height;
            if (PreserveAspectRatio)
            {
                int originalWidth = image.Width;
                int originalHeight = image.Height;
                float percentWidth = (float)Width / (float)originalWidth;
                float percentHeight = (float)Height / (float)originalHeight;
                float percent = percentHeight < percentWidth ? percentHeight : percentWidth;
                newWidth = (int)(originalWidth * percent);
                newHeight = (int)(originalHeight * percent);
            }
            Bitmap newImage = new Bitmap(newWidth, newHeight);
            using (Graphics graphicsHandle = Graphics.FromImage(newImage))
            {
                graphicsHandle.InterpolationMode = InterpolationMode.HighQualityBicubic;
                graphicsHandle.DrawImage(image, 0, 0, newWidth, newHeight);
            }
            return newImage;
        }
        /// <summary>
        /// 이미지 크기를 조정합니다. (출처: http://www.codeproject.com/Articles/191424/Resizing-an-Image-On-The-Fly-using-NET)
        /// </summary>
        /// <param name="image">변경할 대상 이미지 입니다.</param>
        /// <param name="size">변경할 사이즈 입니다.</param>
        /// <param name="PreserveAspectRatio">True면 가로세로 비율을 유지하고 False면 유지하지 않습니다.</param>
        /// <returns></returns>
        public static Bitmap ResizeImage(this Image image, Size size, bool PreserveAspectRatio = true)
        {
            int newWidth = size.Width;
            int newHeight = size.Height;
            if (PreserveAspectRatio)
            {
                int originalWidth = image.Width;
                int originalHeight = image.Height;
                float percentWidth = (float)size.Width / (float)originalWidth;
                float percentHeight = (float)size.Height / (float)originalHeight;
                float percent = percentHeight < percentWidth ? percentHeight : percentWidth;
                newWidth = (int)(originalWidth * percent);
                newHeight = (int)(originalHeight * percent);
            }
            Bitmap newImage = new Bitmap(newWidth, newHeight);
            using (Graphics graphicsHandle = Graphics.FromImage(newImage))
            {
                graphicsHandle.InterpolationMode = InterpolationMode.HighQualityBicubic;
                graphicsHandle.DrawImage(image, 0, 0, newWidth, newHeight);
            }
            return newImage;
        }
        #endregion

        #region 이미지 크기조정(관리+비관리)
        public static Bitmap ResizeImage_Pointer(this Image imgPhoto, float Percent, bool DisposeImage = false)
        {
            float nPercent = ((float)Percent / 100);

            int sourceWidth = imgPhoto.Width;      //store original width of source image.
            int sourceHeight = imgPhoto.Height;    //store original height of source image.
            int sourceX = 0;        //x-axis of source image.
            int sourceY = 0;         //y-axis of source image.

            int destX = 0;          //x-axis of destination image.
            int destY = 0;          //y-axis of destination image.

            //Calcuate height and width of resized image.
            int destWidth = (int)(sourceWidth * nPercent);
            int destHeight = (int)(sourceHeight * nPercent);

            Bitmap src = ((Bitmap)imgPhoto);
            PixelFormat pf = src.PixelFormat;
            BitmapData data = src.LockBits(new Rectangle(0, 0, imgPhoto.Width, imgPhoto.Height), ImageLockMode.ReadWrite, pf);

            //Create a new bitmap object.
            Bitmap bmPhoto = new Bitmap(destWidth, destHeight, pf);
            //Set resolution of bitmap.
            bmPhoto.SetResolution(imgPhoto.HorizontalResolution, imgPhoto.VerticalResolution);

            //Create a graphics object and set quality of graphics.
            Graphics grPhoto = Graphics.FromImage(bmPhoto);
            grPhoto.InterpolationMode = Percent < 26 ? InterpolationMode.HighQualityBicubic :
                Percent < 51 ? InterpolationMode.Bicubic : InterpolationMode.Bilinear;



            //Draw image by using DrawImage() method of graphics class.
            grPhoto.DrawImage(imgPhoto,
                new Rectangle(destX, destY, destWidth, destHeight),
                new Rectangle(sourceX, sourceY, sourceWidth, sourceHeight),
                GraphicsUnit.Pixel);

            //Dispose graphics object.
            grPhoto.Dispose();

            src.UnlockBits(data);
            if (DisposeImage) src.Dispose();
            bmPhoto.Save("D:\\asdada.png", ImageFormat.Png);
            return bmPhoto;
        }        
        #endregion

        #region 이미지 크기조정 2 (비 관리)
        #region Bilinear
        /// <summary>
        /// 이미지 크기를 포인터와 Bilinear 알고리즘을 사용하여 조정합니다. (출처: http://code.google.com/p/aforge/source/browse/trunk/Sources/Imaging/Filters/Transform/ResizeBilinear.cs)
        /// </summary>
        /// <param name="image">변경할 대상 이미지 입니다.</param>
        /// <param name="Percent">변경할 크기를 비율을 사용하여 조정합니다. (범위: 1 ~ 100)</param>
        /// <param name="PreserveAspectRatio">True면 가로세로 비율을 유지하고 False면 유지하지 않습니다.</param>
        /// <param name="DisposeImage">끝난다음 기존에 있던 리소스를 해제할지의 여부 입니다</param>
        /// <returns></returns>
        public static Bitmap ResizeImage_Bilinear(this Image image, float Percent, bool PreserveAspectRatio = false, bool DisposeImage = false)
        {
            float nPercent = ((float)Percent / 100);

            int sourceWidth = image.Width;      //store original width of source image.
            int sourceHeight = image.Height;    //store original height of source image.

            //Calcuate height and width of resized image.
            int destWidth = (int)(sourceWidth * nPercent);
            int destHeight = (int)(sourceHeight * nPercent);

            return ResizeImage_Bilinear(image, destWidth, destHeight, PreserveAspectRatio, DisposeImage);
        }
        /// <summary>
        /// 이미지 크기를 포인터와 Bilinear 알고리즘을 사용하여 조정합니다. (출처: http://code.google.com/p/aforge/source/browse/trunk/Sources/Imaging/Filters/Transform/ResizeBilinear.cs)
        /// </summary>
        /// <param name="image">변경할 대상 이미지 입니다.</param>
        /// <param name="size">변경할 사이즈 입니다.</param>
        /// <param name="PreserveAspectRatio">True면 가로세로 비율을 유지하고 False면 유지하지 않습니다.</param>
        /// <param name="DisposeImage">끝난다음 기존에 있던 리소스를 해제할지의 여부 입니다</param>
        /// <returns></returns>
        public static Bitmap ResizeImage_ResizeBilinear(this Image image, Size size, bool PreserveAspectRatio = true, bool DisposeImage = false)
        {
            return ResizeImage_Bilinear(image, size.Width, size.Height, PreserveAspectRatio, DisposeImage);
        }
        /// <summary>
        /// 이미지 크기를 포인터와 Bilinear 알고리즘을 사용하여 조정합니다. (출처: http://code.google.com/p/aforge/source/browse/trunk/Sources/Imaging/Filters/Transform/ResizeBilinear.cs)
        /// </summary>
        /// <param name="image">변경할 대상 이미지 입니다.</param>
        /// <param name="Width">변경할 가로길이 입니다.</param>
        /// <param name="Height">변경할 세로길이 입니다.</param>
        /// <param name="PreserveAspectRatio">True면 가로세로 비율을 유지하고 False면 유지하지 않습니다.</param>
        /// <param name="DisposeImage">끝난다음 기존에 있던 리소스를 해제할지의 여부 입니다</param>
        /// <returns></returns>
        public static Bitmap ResizeImage_Bilinear(this Image image, int Width, int Height, bool PreserveAspectRatio = true, bool DisposeImage = false)
        {
            PixelFormat SmartFormat = SmartFormatConvert(image.PixelFormat);
            Bitmap bmp = new Bitmap(Width, Height, SmartFormat);
            return ResizeImage_Bilinear(image, bmp, SmartFormat,PreserveAspectRatio, DisposeImage);
        }
        /// <summary>
        /// Resize image using nearest neighbor algorithm.
        /// </summary>
        /// 
        /// <remarks><para>The class implements image resizing filter using nearest
        /// neighbor algorithm, which does not assume any interpolation.</para>
        /// 
        /// <para>The filter accepts 8 and 16 bpp grayscale images and 24, 32, 48 and 64 bpp
        /// color images for processing.</para></remarks>
        /// 
        /// <param name="image"></param>
        /// <param name="DestinationImage"></param>
        /// <param name="PreserveAspectRatio"></param>
        /// <param name="DisposeImage"></param>
        /// <returns></returns>
        public static unsafe Bitmap ResizeImage_Bilinear(this Image image, Bitmap DestinationImage, PixelFormat Format,bool PreserveAspectRatio = true, bool DisposeImage = false)
        {
            // get source image size
            Bitmap srcimg=(Bitmap)image;
            int width = srcimg.Width;
            int height = srcimg.Height;
            
            int newWidth = DestinationImage.Width;
            int newHeight = DestinationImage.Height;
            if (PreserveAspectRatio)
            {
                int originalWidth = image.Width;
                int originalHeight = image.Height;
                float percentWidth = (float)newWidth / (float)originalWidth;
                float percentHeight = (float)newHeight / (float)originalHeight;
                float percent = percentHeight < percentWidth ? percentHeight : percentWidth;
                newWidth = (int)(originalWidth * percent);
                newHeight = (int)(originalHeight * percent);
            }

            int pixelSize = Image.GetPixelFormatSize(Format) / 8;
            BitmapData sourceData = null, destinationData = null;
            try
            {
                sourceData = srcimg.LockBits(new Rectangle(0, 0, srcimg.Width, srcimg.Height), ImageLockMode.ReadWrite, Format);
                destinationData = DestinationImage.LockBits(new Rectangle(0, 0, DestinationImage.Width, DestinationImage.Height), ImageLockMode.ReadWrite, Format);

                int srcStride = sourceData.Stride;
                int dstOffset = destinationData.Stride - pixelSize * newWidth;

                double xFactor = (double)width / newWidth;
                double yFactor = (double)height / newHeight;


                // do the job
                byte* src = (byte*)sourceData.Scan0.ToPointer();
                byte* dst = (byte*)destinationData.Scan0.ToPointer();

                // coordinates of source points
                double ox, oy, dx1, dy1, dx2, dy2;
                int ox1, oy1, ox2, oy2;

                // width and height decreased by 1

                int ymax = height - 1;
                int xmax = width - 1;

                // temporary pointers

                byte* tp1, tp2;
                byte* p1, p2, p3, p4;

                // for each line

                for (int y = 0; y < newHeight; y++)
                {
                    // Y coordinates
                    oy = (double)y * yFactor;
                    oy1 = (int)oy;
                    oy2 = (oy1 == ymax) ? oy1 : oy1 + 1;
                    dy1 = oy - (double)oy1;
                    dy2 = 1.0 - dy1;

                    // get temp pointers
                    tp1 = src + oy1 * srcStride;
                    tp2 = src + oy2 * srcStride;


                    // for each pixel

                    for (int x = 0; x < newWidth; x++)
                    {
                        // X coordinates
                        ox = (double)x * xFactor;
                        ox1 = (int)ox;
                        ox2 = (ox1 == xmax) ? ox1 : ox1 + 1;
                        dx1 = ox - (double)ox1;
                        dx2 = 1.0 - dx1;

                        // get four points
                        p1 = tp1 + ox1 * pixelSize;
                        p2 = tp1 + ox2 * pixelSize;
                        p3 = tp2 + ox1 * pixelSize;
                        p4 = tp2 + ox2 * pixelSize;

                        // interpolate using 4 points
                        for (int i = 0; i < pixelSize; i++, dst++, p1++, p2++, p3++, p4++)
                        {
                            *dst = (byte)(
                                dy2 * (dx2 * (*p1) + dx1 * (*p2)) +
                                dy1 * (dx2 * (*p3) + dx1 * (*p4)));
                        }
                    }
                    dst += dstOffset;
                }
            }
            catch{return null;}
            finally
            {
                try{srcimg.UnlockBits(sourceData);}catch{}
                try{DestinationImage.UnlockBits(destinationData);}catch{}
                try{if(DisposeImage)image.Dispose();}catch{}
            }
            DestinationImage.Save("D:\\asdada.png");
            return DestinationImage;
        }
        #endregion

        #region Bicubic
        /// <summary>
        /// 이미지 크기를 포인터와 Bicubic 알고리즘을 사용하여 조정합니다. (출처: http://code.google.com/p/aforge/source/browse/trunk/Sources/Imaging/Filters/Transform/ResizeBicubic.cs)
        /// </summary>
        /// <param name="image">변경할 대상 이미지 입니다.</param>
        /// <param name="Percent">변경할 크기를 비율을 사용하여 조정합니다. (범위: 1 ~ 100)</param>
        /// <param name="PreserveAspectRatio">True면 가로세로 비율을 유지하고 False면 유지하지 않습니다.</param>
        /// <param name="DisposeImage">끝난다음 기존에 있던 리소스를 해제할지의 여부 입니다</param>
        /// <returns></returns>
        public static Bitmap ResizeImage_Bicubic(this Image image, float Percent, bool PreserveAspectRatio = false, bool DisposeImage = false)
        {
            float nPercent = ((float)Percent / 100);

            int sourceWidth = image.Width;      //store original width of source image.
            int sourceHeight = image.Height;    //store original height of source image.

            //Calcuate height and width of resized image.
            int destWidth = (int)(sourceWidth * nPercent);
            int destHeight = (int)(sourceHeight * nPercent);

            return ResizeImage_Bicubic(image, destWidth, destHeight, PreserveAspectRatio, DisposeImage);
        }
        /// <summary>
        /// 이미지 크기를 포인터와 Bicubic 알고리즘을 사용하여 조정합니다. (출처: http://code.google.com/p/aforge/source/browse/trunk/Sources/Imaging/Filters/Transform/ResizeBicubic.cs)
        /// </summary>
        /// <param name="image">변경할 대상 이미지 입니다.</param>
        /// <param name="size">변경할 사이즈 입니다.</param>
        /// <param name="PreserveAspectRatio">True면 가로세로 비율을 유지하고 False면 유지하지 않습니다.</param>
        /// <param name="DisposeImage">끝난다음 기존에 있던 리소스를 해제할지의 여부 입니다</param>
        /// <returns></returns>
        public static Bitmap ResizeImage_Bicubic(this Image image, Size size, bool PreserveAspectRatio = true, bool DisposeImage = false)
        {
            return ResizeImage_Bicubic(image, size.Width, size.Height, PreserveAspectRatio, DisposeImage);
        }
        /// <summary>
        /// 이미지 크기를 포인터와 Bicubic 알고리즘을 사용하여 조정합니다. (출처: http://code.google.com/p/aforge/source/browse/trunk/Sources/Imaging/Filters/Transform/ResizeBicubic.cs)
        /// </summary>
        /// <param name="image">변경할 대상 이미지 입니다.</param>
        /// <param name="Width">변경할 가로길이 입니다.</param>
        /// <param name="Height">변경할 세로길이 입니다.</param>
        /// <param name="PreserveAspectRatio">True면 가로세로 비율을 유지하고 False면 유지하지 않습니다.</param>
        /// <param name="DisposeImage">끝난다음 기존에 있던 리소스를 해제할지의 여부 입니다</param>
        /// <returns></returns>
        public static Bitmap ResizeImage_Bicubic(this Image image, int Width, int Height, bool PreserveAspectRatio = true, bool DisposeImage = false)
        {
            PixelFormat SmartFormat = SmartFormatConvert(image.PixelFormat);
            Bitmap bmp = new Bitmap(Width, Height, SmartFormat);
            return ResizeImage_Bicubic(image, bmp, SmartFormat, PreserveAspectRatio, DisposeImage);
        }
        public static unsafe Bitmap ResizeImage_Bicubic(this Image image, Bitmap DestinationImage, PixelFormat Format, bool PreserveAspectRatio = true, bool DisposeImage = false)
        {
            const short A = 3;
            const short B = 0;
            const short G = 1;
            const short R = 2;

            // get source image size
            Bitmap srcimg = (Bitmap)image;
            int width = srcimg.Width;
            int height = srcimg.Height;

            int newWidth = DestinationImage.Width;
            int newHeight = DestinationImage.Height;
            if (PreserveAspectRatio)
            {
                int originalWidth = image.Width;
                int originalHeight = image.Height;
                float percentWidth = (float)newWidth / (float)originalWidth;
                float percentHeight = (float)newHeight / (float)originalHeight;
                float percent = percentHeight < percentWidth ? percentHeight : percentWidth;
                newWidth = (int)(originalWidth * percent);
                newHeight = (int)(originalHeight * percent);
            }
            BitmapData sourceData = null, destinationData = null;

            try
            {
                sourceData = srcimg.LockBits(new Rectangle(0, 0, srcimg.Width, srcimg.Height), ImageLockMode.ReadWrite, Format);
                destinationData = DestinationImage.LockBits(new Rectangle(0, 0, DestinationImage.Width, DestinationImage.Height), ImageLockMode.ReadWrite, Format);

                int pixelSize = (sourceData.PixelFormat == PixelFormat.Format8bppIndexed) ? 1 : 3;
                int srcStride = sourceData.Stride;
                int dstOffset = destinationData.Stride - pixelSize * newWidth;
                double xFactor = (double)width / newWidth;
                double yFactor = (double)height / newHeight;

                // do the job
                byte* src = (byte*)sourceData.Scan0.ToPointer();
                byte* dst = (byte*)destinationData.Scan0.ToPointer();

                // coordinates of source points and cooefficiens
                double ox, oy, dx, dy, k1, k2;
                int ox1, oy1, ox2, oy2;

                // destination pixel values
                double r, g, b;

                // width and height decreased by 1
                int ymax = height - 1;
                int xmax = width - 1;

                // temporary pointer
                byte* p;

                // check pixel format
                if (destinationData.PixelFormat == PixelFormat.Format8bppIndexed)
                {
                    // grayscale
                    for (int y = 0; y < newHeight; y++)
                    {
                        // Y coordinates
                        oy = (double)y * yFactor - 0.5;
                        oy1 = (int)oy;
                        dy = oy - (double)oy1;

                        for (int x = 0; x < newWidth; x++, dst++)
                        {
                            // X coordinates
                            ox = (double)x * xFactor - 0.5f;
                            ox1 = (int)ox;
                            dx = ox - (double)ox1;

                            // initial pixel value
                            g = 0;

                            for (int n = -1; n < 3; n++)
                            {
                                // get Y cooefficient
                                k1 = BiCubicKernel(dy - (double)n);

                                oy2 = oy1 + n;
                                if (oy2 < 0)
                                    oy2 = 0;
                                if (oy2 > ymax)
                                    oy2 = ymax;

                                for (int m = -1; m < 3; m++)
                                {
                                    // get X cooefficient
                                    k2 = k1 * BiCubicKernel((double)m - dx);

                                    ox2 = ox1 + m;
                                    if (ox2 < 0)
                                        ox2 = 0;
                                    if (ox2 > xmax)
                                        ox2 = xmax;

                                    g += k2 * src[oy2 * srcStride + ox2];
                                }
                            }
                            *dst = (byte)Math.Max(0, Math.Min(255, g));
                        }
                        dst += dstOffset;
                    }
                }

                else
                {
                    // RGB
                    for (int y = 0; y < newHeight; y++)
                    {
                        // Y coordinates
                        oy = (double)y * yFactor - 0.5f;
                        oy1 = (int)oy;
                        dy = oy - (double)oy1;

                        for (int x = 0; x < newWidth; x++, dst += 3)
                        {
                            // X coordinates
                            ox = (double)x * xFactor - 0.5f;
                            ox1 = (int)ox;
                            dx = ox - (double)ox1;

                            // initial pixel value
                            r = g = b = 0;

                            for (int n = -1; n < 3; n++)
                            {
                                // get Y cooefficient
                                k1 = BiCubicKernel(dy - (double)n);

                                oy2 = oy1 + n;
                                if (oy2 < 0)
                                    oy2 = 0;
                                if (oy2 > ymax)
                                    oy2 = ymax;

                                for (int m = -1; m < 3; m++)
                                {
                                    // get X cooefficient
                                    k2 = k1 * BiCubicKernel((double)m - dx);

                                    ox2 = ox1 + m;
                                    if (ox2 < 0)
                                        ox2 = 0;
                                    if (ox2 > xmax)
                                        ox2 = xmax;

                                    // get pixel of original image
                                    p = src + oy2 * srcStride + ox2 * 3;
                                    r += k2 * p[R];
                                    g += k2 * p[G];
                                    b += k2 * p[B];
                                }
                            }

                            dst[R] = (byte)Math.Max(0, Math.Min(255, r));
                            dst[G] = (byte)Math.Max(0, Math.Min(255, g));
                            dst[B] = (byte)Math.Max(0, Math.Min(255, b));
                        }
                        dst += dstOffset;
                    }
                }
            }
            catch{}
            finally
            {   
                try{srcimg.UnlockBits(sourceData);}catch{}
                try{DestinationImage.UnlockBits(destinationData);}catch{}
                try{if(DisposeImage)image.Dispose();}catch{}
            }
            //DestinationImage.Save("D:\\asdada.png");
            return DestinationImage;
        }
        #endregion

        #region NearestNeighbor
        /// <summary>
        /// 이미지 크기를 포인터와 NearestNeighbor 알고리즘을 사용하여 조정합니다. (출처: http://code.google.com/p/aforge/source/browse/trunk/Sources/Imaging/Filters/Transform/http://code.google.com/p/aforge/source/browse/trunk/Sources/Imaging/Filters/Transform/ResizeNearestNeighbor.cs)
        /// </summary>
        /// <param name="image">변경할 대상 이미지 입니다.</param>
        /// <param name="Percent">변경할 크기를 비율을 사용하여 조정합니다. (범위: 1 ~ 100)</param>
        /// <param name="PreserveAspectRatio">True면 가로세로 비율을 유지하고 False면 유지하지 않습니다.</param>
        /// <param name="DisposeImage">끝난다음 기존에 있던 리소스를 해제할지의 여부 입니다</param>
        /// <returns></returns>
        public static Bitmap ResizeImage_NearestNeighbor(this Image image, float Percent, bool PreserveAspectRatio = false, bool DisposeImage = false)
        {
            float nPercent = ((float)Percent / 100);

            int sourceWidth = image.Width;      //store original width of source image.
            int sourceHeight = image.Height;    //store original height of source image.

            //Calcuate height and width of resized image.
            int destWidth = (int)(sourceWidth * nPercent);
            int destHeight = (int)(sourceHeight * nPercent);

            return ResizeImage_NearestNeighbor(image, destWidth, destHeight, PreserveAspectRatio, DisposeImage);
        }
        /// <summary>
        /// 이미지 크기를 포인터와 NearestNeighbor 알고리즘을 사용하여 조정합니다. (출처: http://code.google.com/p/aforge/source/browse/trunk/Sources/Imaging/Filters/Transform/http://code.google.com/p/aforge/source/browse/trunk/Sources/Imaging/Filters/Transform/ResizeNearestNeighbor.cs)
        /// </summary>
        /// <param name="image">변경할 대상 이미지 입니다.</param>
        /// <param name="size">변경할 사이즈 입니다.</param>
        /// <param name="PreserveAspectRatio">True면 가로세로 비율을 유지하고 False면 유지하지 않습니다.</param>
        /// <param name="DisposeImage">끝난다음 기존에 있던 리소스를 해제할지의 여부 입니다</param>
        /// <returns></returns>
        public static Bitmap ResizeImage_NearestNeighbor(this Image image, Size size, bool PreserveAspectRatio = true, bool DisposeImage = false)
        {
            return ResizeImage_NearestNeighbor(image, size.Width, size.Height, PreserveAspectRatio, DisposeImage);
        }
        /// <summary>
        /// 이미지 크기를 포인터와 NearestNeighbor 알고리즘을 사용하여 조정합니다. (출처: http://code.google.com/p/aforge/source/browse/trunk/Sources/Imaging/Filters/Transform/http://code.google.com/p/aforge/source/browse/trunk/Sources/Imaging/Filters/Transform/ResizeNearestNeighbor.cs)
        /// </summary>
        /// <param name="image">변경할 대상 이미지 입니다.</param>
        /// <param name="Width">변경할 가로길이 입니다.</param>
        /// <param name="Height">변경할 세로길이 입니다.</param>
        /// <param name="PreserveAspectRatio">True면 가로세로 비율을 유지하고 False면 유지하지 않습니다.</param>
        /// <param name="DisposeImage">끝난다음 기존에 있던 리소스를 해제할지의 여부 입니다</param>
        /// <returns></returns>
        public static Bitmap ResizeImage_NearestNeighbor(this Image image, int Width, int Height, bool PreserveAspectRatio = true, bool DisposeImage = false)
        {
            PixelFormat SmartFormat = SmartFormatConvert(image.PixelFormat);
            Bitmap bmp = new Bitmap(Width, Height, SmartFormat);
            return ResizeImage_NearestNeighbor(image, bmp, SmartFormat, PreserveAspectRatio, DisposeImage);
        }
        public static unsafe Bitmap ResizeImage_NearestNeighbor(this Image image, Bitmap DestinationImage, PixelFormat Format, bool PreserveAspectRatio = true, bool DisposeImage = false)
        {
            // get source image size
            Bitmap srcimg = (Bitmap)image;
            int width = srcimg.Width;
            int height = srcimg.Height;

            int newWidth = DestinationImage.Width;
            int newHeight = DestinationImage.Height;
            if (PreserveAspectRatio)
            {
                int originalWidth = image.Width;
                int originalHeight = image.Height;
                float percentWidth = (float)newWidth / (float)originalWidth;
                float percentHeight = (float)newHeight / (float)originalHeight;
                float percent = percentHeight < percentWidth ? percentHeight : percentWidth;
                newWidth = (int)(originalWidth * percent);
                newHeight = (int)(originalHeight * percent);
            }
            BitmapData sourceData = null, destinationData = null;

            try
            {
                sourceData = srcimg.LockBits(new Rectangle(0, 0, srcimg.Width, srcimg.Height), ImageLockMode.ReadWrite, Format);
                destinationData = DestinationImage.LockBits(new Rectangle(0, 0, DestinationImage.Width, DestinationImage.Height), ImageLockMode.ReadWrite, Format);

                int pixelSize = Image.GetPixelFormatSize(sourceData.PixelFormat) / 8;
                int srcStride = sourceData.Stride;
                int dstStride = destinationData.Stride;
                double xFactor = (double)width / newWidth;
                double yFactor = (double)height / newHeight;

                // do the job
                byte* baseSrc = (byte*)sourceData.Scan0.ToPointer();
                byte* baseDst = (byte*)destinationData.Scan0.ToPointer();

                // for each line
                for (int y = 0; y < newHeight; y++)
                {
                    byte* dst = baseDst + dstStride * y;
                    byte* src = baseSrc + srcStride * ((int)(y * yFactor));
                    byte* p;

                    // for each pixel
                    for (int x = 0; x < newWidth; x++)
                    {
                        p = src + pixelSize * ((int)(x * xFactor));

                        for (int i = 0; i < pixelSize; i++, dst++, p++)
                            *dst = *p;
                    }
                }
            }
            catch(OutOfMemoryException oex){throw oex;}//if(DestinationImage!=null)DestinationImage.Dispose();DestinationImage = null;}
            catch{}
            finally
            {   
                try{srcimg.UnlockBits(sourceData);}catch{}
                try{DestinationImage.UnlockBits(destinationData);}catch{}
                try{if(DisposeImage)image.Dispose();}catch{}
            }
            //DestinationImage.Save("D:\\asdada.png");
            return DestinationImage;
        }
        #endregion

        #region 관련 함수 및 클래스

        /// <summary>
        /// Bicubic kernel.
        /// </summary>
        /// 
        /// <param name="x">X value.</param>
        /// 
        /// <returns>Bicubic cooefficient.</returns>
        /// 
        /// <remarks><para>The function implements bicubic kernel W(x) as described on
        /// <a href="http://en.wikipedia.org/wiki/Bicubic_interpolation#Bicubic_convolution_algorithm">Wikipedia</a>
        /// (coefficient <b>a</b> is set to <b>-0.5</b>).</para></remarks>
        /// 
        public static double BiCubicKernel(double x)
        {
            if (x < 0) x = -x;

            double biCoef = 0;

            if (x <= 1) biCoef = (1.5 * x - 2.5) * x * x + 1;
            else if (x < 2) biCoef = ((-0.5 * x + 2.5) * x - 4) * x + 2;

            return biCoef;
        }
        #endregion
        #endregion

        public static void SaveJPG(this Image image, string ImagePath, byte Quility = 90)
        {image.Save(ImagePath,Utility.ImageUtility.GetJPGCodec, Utility.ImageUtility.CompressionSetting(Quility)); }
        public static void SaveJPG(this Image image, Stream ImageStream, byte Quility = 90)
        {image.Save(ImageStream,Utility.ImageUtility.GetJPGCodec, Utility.ImageUtility.CompressionSetting(Quility)); }

        public static Bitmap ToMonochromatic(this Image img, bool Dispose = false)
        {
            return Utility.ImageUtility.ToMonochromatic(img, Dispose);
        }
        public static void ToMonochromaticInstance(this Image img)
        {
            Utility.ImageUtility.ToMonochromaticInstance(img);
        }
    }
    public static class GraphicExtension
    {
        public enum Direction { Up, Right,Down,Left }
        public static void DrawTriangle(this Graphics g, Rectangle rect, Direction direction)
        {
            int halfWidth = rect.Width / 2;
            int halfHeight = rect.Height / 2;
            Point p0 = Point.Empty;
            Point p1 = Point.Empty;
            Point p2 = Point.Empty;

            switch (direction)
            {
                case Direction.Up:
                p0 = new Point(rect.Left + halfWidth, rect.Top);
                p1 = new Point(rect.Left, rect.Bottom);
                p2 = new Point(rect.Right, rect.Bottom);
                break;
                case Direction.Down:
                p0 = new Point(rect.Left + halfWidth, rect.Bottom);
                p1 = new Point(rect.Left, rect.Top);
                p2 = new Point(rect.Right, rect.Top);
                break;
                case Direction.Left:
                p0 = new Point(rect.Left, rect.Top + halfHeight);
                p1 = new Point(rect.Right, rect.Top);
                p2 = new Point(rect.Right, rect.Bottom);
                break;
                case Direction.Right:
                p0 = new Point(rect.Right, rect.Top + halfHeight);
                p1 = new Point(rect.Left, rect.Bottom);
                p2 = new Point(rect.Left, rect.Top);
                break;
            }
            g.FillPolygon(Brushes.Black, new Point[] { p0, p1, p2 });
        }
        public static void DrawTriangle(this Graphics g, Rectangle rect, Direction direction, SolidBrush Brush)
        {
            int halfWidth = rect.Width / 2;
            int halfHeight = rect.Height / 2;
            Point p0 = Point.Empty;
            Point p1 = Point.Empty;
            Point p2 = Point.Empty;

            switch (direction)
            {
                case Direction.Up:
                p0 = new Point(rect.Left + halfWidth, rect.Top);
                p1 = new Point(rect.Left, rect.Bottom);
                p2 = new Point(rect.Right, rect.Bottom);
                break;
                case Direction.Down:
                p0 = new Point(rect.Left + halfWidth, rect.Bottom);
                p1 = new Point(rect.Left, rect.Top);
                p2 = new Point(rect.Right, rect.Top);
                break;
                case Direction.Left:
                p0 = new Point(rect.Left, rect.Top + halfHeight);
                p1 = new Point(rect.Right, rect.Top);
                p2 = new Point(rect.Right, rect.Bottom);
                break;
                case Direction.Right:
                p0 = new Point(rect.Right, rect.Top + halfHeight);
                p1 = new Point(rect.Left, rect.Bottom);
                p2 = new Point(rect.Left, rect.Top);
                break;
            }
            g.FillPolygon(Brush, new Point[] { p0, p1, p2 });
        }

        // Create a matrix and rotate it 45 degrees.
        public static void DrawTriangle(this Graphics g, Rectangle rect, Direction direction,ushort Rotate)
        {
            // Create a matrix and rotate it 45 degrees.
            System.Drawing.Drawing2D.Matrix myMatrix = new System.Drawing.Drawing2D.Matrix();
            myMatrix.Rotate(45, System.Drawing.Drawing2D.MatrixOrder.Append);
            g.Transform = myMatrix;

            int halfWidth = rect.Width / 2;
            int halfHeight = rect.Height / 2;
            Point p0 = Point.Empty;
            Point p1 = Point.Empty;
            Point p2 = Point.Empty;

            switch (direction)
            {
                case Direction.Up:
                p0 = new Point(rect.Left + halfWidth, rect.Top);
                p1 = new Point(rect.Left, rect.Bottom);
                p2 = new Point(rect.Right, rect.Bottom);
                break;
                case Direction.Down:
                p0 = new Point(rect.Left + halfWidth, rect.Bottom);
                p1 = new Point(rect.Left, rect.Top);
                p2 = new Point(rect.Right, rect.Top);
                break;
                case Direction.Left:
                p0 = new Point(rect.Left, rect.Top + halfHeight);
                p1 = new Point(rect.Right, rect.Top);
                p2 = new Point(rect.Right, rect.Bottom);
                break;
                case Direction.Right:
                p0 = new Point(rect.Right, rect.Top + halfHeight);
                p1 = new Point(rect.Left, rect.Bottom);
                p2 = new Point(rect.Left, rect.Top);
                break;
            }
            /*
            var points = new[] {
            new Point(-(int) rect.Top, (int) rect.Top),
            new Point((int) -(rect.Top + rect.Height), (int) rect.Top - rect.Width/2),
            new Point((int) -(rect.Top + rect.Height), (int) rect.Top + rect.Width/2)};*/
            g.FillPolygon(Brushes.Black, new Point[] { p0, p1, p2 });
        }
        public static void DrawTriangle(this Graphics g, Rectangle rect, Direction direction, ushort Rotate, SolidBrush Brush)
        {
            // Create a matrix and rotate it 45 degrees.
            System.Drawing.Drawing2D.Matrix myMatrix = new System.Drawing.Drawing2D.Matrix();
            myMatrix.Rotate(45, System.Drawing.Drawing2D.MatrixOrder.Append);
            g.Transform = myMatrix;

            int halfWidth = rect.Width / 2;
            int halfHeight = rect.Height / 2;
            Point p0 = Point.Empty;
            Point p1 = Point.Empty;
            Point p2 = Point.Empty;

            switch (direction)
            {
                case Direction.Up:
                p0 = new Point(rect.Left + halfWidth, rect.Top);
                p1 = new Point(rect.Left, rect.Bottom);
                p2 = new Point(rect.Right, rect.Bottom);
                break;
                case Direction.Down:
                p0 = new Point(rect.Left + halfWidth, rect.Bottom);
                p1 = new Point(rect.Left, rect.Top);
                p2 = new Point(rect.Right, rect.Top);
                break;
                case Direction.Left:
                p0 = new Point(rect.Left, rect.Top + halfHeight);
                p1 = new Point(rect.Right, rect.Top);
                p2 = new Point(rect.Right, rect.Bottom);
                break;
                case Direction.Right:
                p0 = new Point(rect.Right, rect.Top + halfHeight);
                p1 = new Point(rect.Left, rect.Bottom);
                p2 = new Point(rect.Left, rect.Top);
                break;
            }
            /*
            var points = new[] {
            new Point(-(int) rect.Top, (int) rect.Top),
            new Point((int) -(rect.Top + rect.Height), (int) rect.Top - rect.Width/2),
            new Point((int) -(rect.Top + rect.Height), (int) rect.Top + rect.Width/2)};*/
            g.FillPolygon(Brush, new Point[] { p0, p1, p2 });
        }

        public static void Rotate(this Graphics g,float Angle,int Width,int Height)
        {
            //move rotation point to center of image
            g.TranslateTransform((float)Width / 2, (float)Height / 2);
            //rotate
            g.RotateTransform(Angle);
            //move image back
            g.TranslateTransform(-(float)Width / 2, -(float)Height / 2);
        }
        public static void Rotate(this Graphics g , float Angle, Size Size)
        {
            //move rotation point to center of image
            g.TranslateTransform((float)Size.Width / 2, (float)Size.Height / 2);
            //rotate
            g.RotateTransform(Angle);
            //move image back
            g.TranslateTransform(-(float)Size.Width / 2, -(float)Size.Height / 2);
        }
    }
    public static class ColorExtension
    {
        public static Color NegativeColor(this Color OriginColor, bool IncludeAlpha = false)
        {
            if (IncludeAlpha) return Color.FromArgb(255 - OriginColor.A, 255 - OriginColor.R, 255 - OriginColor.G, 255 - OriginColor.B);
            else return Color.FromArgb(OriginColor.A, 255 - OriginColor.R, 255 - OriginColor.G, 255 - OriginColor.B);
        }
    }

    public static class ExceptionExtension
    {
        /// <summary>
        /// 예외를 로깅합니다. (기본 폴더는 현재 이진 디렉터리\\Log 입니다. 그리고 날짜별로 파일이 만들어지고 시간별로 저장됩니다.)
        /// </summary>
        /// <param name="ex">로깅할 예외 입니다.</param>
        public static void Logging(this Exception ex, string Description = null)
        { HS_CSharpUtility.LoggingUtility.Logging(ex, Description); }
    }
    public static class NumberExtension
    {
        public static int Percent(this int Value, byte Percent)
        {
            try { return Percent==100?Value:(Value / 100) * Percent; }
            catch { return Value; }
        }
        public static int Percent(this int Value, float Percent)
        {
            try { return (int)(((float)Value / 100f) * Percent); }
            catch { return Value; }
        }

        public static string ToHexString(this long Value, byte 자릿수 = 0)
        {
            /*
            byte[] b = BitConverter.GetBytes(Value);
            StringBuilder Hex = new StringBuilder();
            for (int i = 0; i < b.Length; i++) Hex.Append(b[i].ToString("X2"));*/
            return (자릿수 == 0) ? Value.ToString("X") : Value.ToString("X" + 자릿수.ToString());
        }
        public static string ToHexString(this ulong Value, byte 자릿수 = 0)
        {return (자릿수 == 0) ? Value.ToString("X") : Value.ToString("X" + 자릿수.ToString()); }
        public static string ToHexString(this int Value, byte 자릿수 = 0)
        {return (자릿수 == 0) ? Value.ToString("X") : Value.ToString("X" + 자릿수.ToString()); }
        public static string ToHexString(this uint Value, byte 자릿수 = 0)
        {return (자릿수 == 0) ? Value.ToString("X") : Value.ToString("X" + 자릿수.ToString()); }
        public static string ToHexString(this short Value, byte 자릿수 = 0)
        {return (자릿수 == 0) ? Value.ToString("X") : Value.ToString("X" + 자릿수.ToString()); }
        public static string ToHexString(this ushort Value, byte 자릿수 = 0)
        {return (자릿수 == 0) ? Value.ToString("X") : Value.ToString("X" + 자릿수.ToString()); }
        public static string ToHexString(this byte Value, byte 자릿수 = 0)
        {return (자릿수 == 0) ? Value.ToString("X") : Value.ToString("X" + 자릿수.ToString()); }
        
        public static string ToHexString(this sbyte Value, byte 자릿수 = 0)
        {return (자릿수 == 0) ? Value.ToString("X") : Value.ToString("X" + 자릿수.ToString()); }
    }

    public static class DateTimeExtension
    {
        public static string ToKorean(this DateTime time, bool 오전오후 = true, bool MilliSecond = false)
        {
            StringBuilder sb = new StringBuilder(time.Year.ToString()); sb.Append("년 ");
            sb.Append(time.Month.ToString("00")); sb.Append("월 ");
            sb.Append(time.Day.ToString("00")); sb.AppendFormat("일 ");
            if (오전오후)
            {
                if (time.Hour == 0) sb.Append("오전 12시 ");
                else if (time.Hour < 12) { sb.Append("오전 "); sb.Append(time.Hour.ToString("00")); sb.Append("시 "); }
                else if (time.Hour == 12) sb.Append("오후 12시 ");
                else { sb.Append("오후 "); sb.Append((time.Hour - 12).ToString("00")); sb.Append("시 "); }
            }
            else { sb.Append(time.Hour.ToString("00")); sb.AppendFormat("시 "); }
            sb.Append(time.Minute.ToString("00")); sb.AppendFormat("분 ");
            if (MilliSecond) { sb.Append(time.Second.ToString("00")); sb.Append("."); sb.Append(time.Millisecond.ToString("000")); sb.Append("초"); }
            else sb.Append(time.Second.ToString("00")); sb.AppendFormat("초");
            return sb.ToString();
        }
        public static string ToKorean(this TimeSpan time, bool 오전오후 = true, bool MilliSecond = false)
        {
            StringBuilder sb = new StringBuilder();
            if (오전오후)
            {
                if (time.Hours == 0) sb.Append("오전 12시 ");
                else if (time.Hours < 12) { sb.Append("오전 "); sb.Append(time.Hours.ToString("00")); sb.Append("시 "); }
                else if (time.Hours == 12) sb.Append("오후 12시 ");
                else { sb.Append("오후 "); sb.Append((time.Hours - 12).ToString("00")); sb.Append("시 "); }
            }
            else { sb.Append(time.Hours.ToString("00")); sb.AppendFormat("시 "); }
            sb.Append(time.Minutes.ToString("00")); sb.AppendFormat("분 ");
            if (MilliSecond) { sb.Append(time.Seconds.ToString("00")); sb.Append("."); sb.Append(time.Milliseconds.ToString("000")); sb.Append("초"); }
            else sb.Append(time.Seconds.ToString("00")); sb.AppendFormat("초");
            return sb.ToString();
        }
    }

    public static class StringBuilderExtension
    {
        /// <summary>
        /// 모든 문자를 비웁니다. (StringBuilder.Clear() 함수가 없는 경우만 사용해 주세요.)
        /// </summary>
        /// <param name="Builder">문자를 제거할 StringBuilder 객체 입니다.</param>
        /// <returns></returns>
        public static StringBuilder Clear(this StringBuilder Builder)
        {
            return Builder.Remove(0, Builder.Length);
        }
    }

    public static class Extension
    {
        public static void SetNotifyIconText(this NotifyIcon ni, string text)
        {
            if (text.Length >= 128) throw new ArgumentOutOfRangeException("Text limited to 127 characters");
            Type t = typeof(NotifyIcon);
            BindingFlags hidden = BindingFlags.NonPublic | BindingFlags.Instance;
            t.GetField("text", hidden).SetValue(ni, text);
            if ((bool)t.GetField("added", hidden).GetValue(ni))
                t.GetMethod("UpdateIcon", hidden).Invoke(ni, new object[] { true });
        }
        public static Stream CopyStream(this Stream Original, Stream Destination,int buffer = 1024)
        {
            if (Original == null) throw new ArgumentNullException("Original");
            if (Destination == null) throw new ArgumentNullException("Destination");

			// Ensure a reasonable size of buffer is used without being prohibitive.
			if (buffer < 128) throw new ArgumentException("Buffer is too small", "buffer");

			bool copying = true;

            byte[] Buffer = new byte[buffer];
			while (copying) {
                int bytesRead = Original.Read(Buffer, 0, Buffer.Length);
				if (bytesRead > 0) {
                    Destination.Write(Buffer, 0, bytesRead);
				}
				else {
                    Destination.Flush();
					copying = false;
				}
			}
            return Destination;
        }
    }
    public static class EnumExtensions
    {
        private static void CheckIsEnum<T>(bool withFlags)
        {
            if (!typeof(T).IsEnum)
                throw new ArgumentException(string.Format("Type '{0}' is not an enum", typeof(T).FullName));
            if (withFlags && !Attribute.IsDefined(typeof(T), typeof(FlagsAttribute)))
                throw new ArgumentException(string.Format("Type '{0}' doesn't have the 'Flags' attribute", typeof(T).FullName));
        }

        public static bool IsFlagSetEx<T>(this T value, T flag) where T : struct
        {
            CheckIsEnum<T>(true);
            long lValue = Convert.ToInt64(value);
            long lFlag = Convert.ToInt64(flag);
            return (lValue & lFlag) != 0;
        }
        public static bool IsFlagSet(this Enum value, Enum flag)
        {
            //if(flag == Type(Enum))
            long lValue = Convert.ToInt64(value);
            long lFlag = Convert.ToInt64(flag);
            return (lValue & lFlag) != 0;
        }

        /*
        public static IEnumerable<T> GetFlags<T>(this T value) where T : struct
        {
            CheckIsEnum<T>(true);
            foreach (T flag in Enum.GetValues(typeof(T)).Cast<T>())
            {
                if (value.IsFlagSet(flag))
                    yield return flag;
            }
        }*/

        public static T SetFlags<T>(this T value, T flags, bool on) where T : struct
        {
            CheckIsEnum<T>(true);
            long lValue = Convert.ToInt64(value);
            long lFlag = Convert.ToInt64(flags);
            if (on)
            {
                lValue |= lFlag;
            }
            else
            {
                lValue &= (~lFlag);
            }
            return (T)Enum.ToObject(typeof(T), lValue);
        }

        public static T SetFlags<T>(this T value, T flags) where T : struct
        {
            return value.SetFlags(flags, true);
        }

        public static T ClearFlags<T>(this T value, T flags) where T : struct
        {
            return value.SetFlags(flags, false);
        }

        public static T CombineFlags<T>(this IEnumerable<T> flags) where T : struct
        {
            CheckIsEnum<T>(true);
            long lValue = 0;
            foreach (T flag in flags)
            {
                long lFlag = Convert.ToInt64(flag);
                lValue |= lFlag;
            }
            return (T)Enum.ToObject(typeof(T), lValue);
        }

        public static string GetDescription<T>(this T value) where T : struct
        {
            CheckIsEnum<T>(false);
            string name = Enum.GetName(typeof(T), value);
            if (name != null)
            {
                FieldInfo field = typeof(T).GetField(name);
                if (field != null)
                {
                    DescriptionAttribute attr = Attribute.GetCustomAttribute(field, typeof(DescriptionAttribute)) as DescriptionAttribute;
                    if (attr != null)
                    {
                        return attr.Description;
                    }
                }
            }
            return null;
        }
    }

    public delegate void Action();
}
namespace HS_CSharpUtility.Control
{
    public class HSListview : ListView
    {
        private bool isInWmPaintMsg = false;

        [StructLayout(LayoutKind.Sequential)]
        public struct NMHDR
        {
            public IntPtr hwndFrom;
            public IntPtr idFrom;
            public int code;
        }
        public HSListview(){DoubleBuffered=true;}
        protected override void WndProc(ref Message m)
        {
            switch (m.Msg)
            {
                case 0x0F: // WM_PAINT
                this.isInWmPaintMsg = true;
                base.WndProc(ref m);
                this.isInWmPaintMsg = false;
                break;
                case 0x204E: // WM_REFLECT_NOTIFY
                NMHDR nmhdr = (NMHDR)m.GetLParam(typeof(NMHDR));
                if (nmhdr.code == -12)
                { // NM_CUSTOMDRAW
                    if (this.isInWmPaintMsg)
                        base.WndProc(ref m);
                }
                else
                    base.WndProc(ref m);
                break;
                default:
                try{base.WndProc(ref m);}catch(OutOfMemoryException){GC.Collect();}
                break;
            }
        }

        [DllImport("uxtheme", CharSet = CharSet.Unicode)]
        public static extern int SetWindowTheme(IntPtr hWnd, String subAppName, String subIdList);

        bool _ApplyWindowTheme = false;
        [System.ComponentModel.Description("Windows Vista 이상에서 아이템에 투명 테마를 적용할지의 여부 입니다."), 
        System.ComponentModel.DefaultValue(false),
        System.ComponentModel.Category("HSListView")]
        public bool ApplyWindowTheme
        {
            get{return _ApplyWindowTheme;}
            set
            {
                try{_ApplyWindowTheme = !_ApplyWindowTheme;
                if(SetWindowTheme(this.Handle, value?"explorer":null, null)!=0)_ApplyWindowTheme = false;}
                catch{_ApplyWindowTheme = false;}
            }
        }
    }
}
namespace HS_CSharpUtility.Text
{
    ///<summary>
    /// EncodingDetector. Detects the Encoding used in byte arrays
    /// or files by testing the start of the file for a Byte Order Mark
    /// (called 'preamble' in .NET).
    ///
    /// Use ReadAllText() to read a file using a detected encoding.
    ///
    /// All encodings that have a preamble are supported.
    ///</summary>

    //http://www.mobzystems.com/code/detecting-text-encoding/encodingdetector-source/
    public class EncodingDetector
    {
        ///<summary>
        /// Helper class to store information about encodings
        /// with a preamble
        ///</summary>
        protected class PreambleInfo
        {
            protected Encoding _encoding;
            protected byte[] _preamble;

            ///<summary>
            /// Property Encoding (Encoding).
            ///</summary>
            public Encoding Encoding
            {
                get
                {
                    return this._encoding;
                }
            }

            ///<summary>
            /// Property Preamble (byte[]).
            ///</summary>
            public byte[] Preamble
            {
                get
                {
                    return this._preamble;
                }
            }

            ///<summary>
            /// Constructor with preamble and encoding
            ///</summary>
            ///<param name="encoding"></param>
            ///<param name="preamble"></param>
            public PreambleInfo(Encoding encoding, byte[] preamble)
            {
                this._encoding = encoding;
                this._preamble = preamble;
            }
        }

        // The list of encodings with a preamble,
        // sorted longest preamble first.
        protected static SortedList<int, PreambleInfo> _preambles = null;

        // Maximum length of all preamles
        protected static int _maxPreambleLength = 0;

        ///<summary>
        /// Read the contents of a text file as a string. Scan for a preamble first.
        /// If a preamble is found, the corresponding encoding is used.
        /// If no preamble is found, the supplied defaultEncoding is used.
        ///</summary>
        ///<param name="filename">The name of the file to read</param>
        ///<param name="defaultEncoding">The encoding to use if no preamble present</param>
        ///<param name="usedEncoding">The actual encoding used</param>
        ///<returns>The contents of the file as a string</returns>
        public static string ReadAllText(string filename, Encoding defaultEncoding, out Encoding usedEncoding)
        {
            // Read the contents of the file as an array of bytes
            byte[] bytes = File.ReadAllBytes(filename);

            // Detect the encoding of the file:
            usedEncoding = DetectEncoding(bytes);

            // If none found, use the default encoding.
            // Otherwise, determine the length of the encoding markers in the file
            int offset;
            if (usedEncoding == null)
            {
                offset = 0;
                usedEncoding = defaultEncoding;
            }
            else
            {
                offset = usedEncoding.GetPreamble().Length;
            }

            // Now interpret the bytes according to the encoding,
            // skipping the preample (if any)
            return usedEncoding.GetString(bytes, offset, bytes.Length - offset);
        }

        ///<summary>
        /// Detect the encoding in an array of bytes.
        ///</summary>
        ///<param name="bytes"></param>
        ///<returns>The encoding found, or null</returns>
        public static Encoding DetectEncoding(byte[] bytes)
        {
            // Scan for encodings if we haven't done so
            if (_preambles == null)
                ScanEncodings();

            // Try each preamble in turn
            foreach (PreambleInfo info in _preambles.Values)
            {
                // Match all bytes in the preamble
                bool match = true;

                if (bytes.Length >= info.Preamble.Length)
                {
                    for (int i = 0; i < info.Preamble.Length; i++)
                    {
                        if (bytes[i] != info.Preamble[i])
                        {
                            match = false;
                            break;
                        }
                    }
                    if (match)
                    {
                        return info.Encoding;
                    }
                }
            }

            return null;
        }

        ///<summary>
        /// Detect the encoding of a file. Reads just enough of
        /// the file to be able to detect a preamble.
        ///</summary>
        ///<param name="filename">The path name of the file</param>
        ///<returns>The encoding detected, or null if no preamble found</returns>
        public static Encoding DetectEncoding(string filename)
        {
            // Scan for encodings if we haven't done so
            if (_preambles == null)
                ScanEncodings();

            using (FileStream stream = File.OpenRead(filename))
            {
                // Never read more than the length of the file
                // or the maximum preamble length
                long n = stream.Length;

                // No bytes? No encoding!
                if (n == 0)
                    return null;

                // Read the minimum amount necessary
                if (n > _maxPreambleLength)
                    n = _maxPreambleLength;

                byte[] bytes = new byte[n];

                stream.Read(bytes, 0, (int)n);

                // Detect the encoding from the byte array
                return DetectEncoding(bytes);
            }
        }

        ///<summary>
        /// Loop over all available encodings and store those
        /// with a preamble in the _preambles list.
        /// The list is sorted by preamble length,
        /// longest preamble first. This prevents
        /// a short preamble 'masking' a longer one
        /// later in the list.
        ///</summary>
        protected static void ScanEncodings()
        {
            // Create a new sorted list of preambles
            _preambles = new SortedList<int, PreambleInfo>();

            // Loop over all encodings
            foreach (EncodingInfo encodingInfo in Encoding.GetEncodings())
            {
                // Do we have a preamble?
                byte[] preamble = encodingInfo.GetEncoding().GetPreamble();
                if (preamble.Length > 0)
                {
                    // Add it to the collection, inversely sorted by preamble length
                    // (and code page, to keep the keys unique)
                    _preambles.Add(-(preamble.Length * 1000000 + encodingInfo.CodePage),
                       new PreambleInfo(encodingInfo.GetEncoding(), preamble));

                    // Update the maximum preamble length if this one's longer
                    if (preamble.Length > _maxPreambleLength)
                    {
                        _maxPreambleLength = preamble.Length;
                    }
                }
            }
        }
    }
}

namespace System.Runtime.CompilerServices
{
    [AttributeUsage(AttributeTargets.Method | AttributeTargets.Class | AttributeTargets.Assembly)]
    public sealed class ExtensionAttribute : Attribute { }
}

// AForge Core Library
// AForge.NET framework
// http://www.aforgenet.com/framework/
//
// Copyright ⓒ Andrew Kirillov, 2007-2009
// andrew.kirillov@aforgenet.com
//
// Copyright ⓒ Israel Lot, 2008
// israel.lot@gmail.com
//

namespace HS_Library.Threading.Tasks
{
    using System;
    using System.Threading;

    /// <summary>
    /// The class provides support for parallel computations, paralleling loop's iterations.
    /// </summary>
    /// 
    /// <remarks><para>The class allows to parallel loop's iteration computing them in separate threads,
    /// what allows their simultaneous execution on multiple CPUs/cores.
    /// </para></remarks>
    ///
    public sealed class Parallel
    {
        /// <summary>
        /// Delegate defining for-loop's body.
        /// </summary>
        /// 
        /// <param name="index">Loop's index.</param>
        /// 
        public delegate void ForLoopBody( int index );

        // number of threads for parallel computations
        private static int threadsCount = System.Environment.ProcessorCount;
        // object used for synchronization
        private static object sync = new Object( );

        // single instance of the class to implement singleton pattern
        private static volatile Parallel instance = null;
        // background threads for parallel computation
        private Thread[] threads = null;

        // events to signal about job availability and thread availability
        private AutoResetEvent[] jobAvailable = null;
        private ManualResetEvent[] threadIdle = null;

        // loop's body and its current and stop index
        private int currentIndex;
        private int stopIndex;
        private ForLoopBody loopBody;

        /// <summary>
        /// Number of threads used for parallel computations.
        /// </summary>
        /// 
        /// <remarks><para>The property sets how many worker threads are created for paralleling
        /// loops' computations.</para>
        /// 
        /// <para>By default the property is set to number of CPU's in the system
        /// (see <see cref="System.Environment.ProcessorCount"/>).</para>
        /// </remarks>
        /// 
        public static int ThreadsCount
        {
            get { return threadsCount; }
            set
            {
                lock ( sync )
                {
                    threadsCount = Math.Max( 1, value );
                }
            }
        }

        /// <summary>
        /// Executes a for-loop in which iterations may run in parallel. 
        /// </summary>
        /// 
        /// <param name="start">Loop's start index.</param>
        /// <param name="stop">Loop's stop index.</param>
        /// <param name="loopBody">Loop's body.</param>
        /// 
        /// <remarks><para>The method is used to parallel for-loop running its iterations in
        /// different threads. The <b>start</b> and <b>stop</b> parameters define loop's
        /// starting and ending loop's indexes. The number of iterations is equal to <b>stop - start</b>.
        /// </para>
        /// 
        /// <para>Sample usage:</para>
        /// <code>
        /// Parallel.For( 0, 20, delegate( int i )
        /// // which is equivalent to
        /// // for ( int i = 0; i &lt; 20; i++ )
        /// {
        ///     System.Diagnostics.Debug.WriteLine( "Iteration: " + i );
        ///     // ...
        /// } );
        /// </code>
        /// </remarks>
        /// 
        public static void For( int start, int stop, ForLoopBody loopBody  )
        {
            lock ( sync )
            {
                // get instance of parallel computation manager
                Parallel instance = Instance;

                instance.currentIndex   = start - 1;
                instance.stopIndex      = stop;
                instance.loopBody       = loopBody;

                // signal about available job for all threads and mark them busy
                for ( int i = 0; i < threadsCount; i++ )
                {
                    instance.threadIdle[i].Reset( );
                    instance.jobAvailable[i].Set( );
                }

                // wait until all threads become idle
                for ( int i = 0; i < threadsCount; i++ )
                {
                    instance.threadIdle[i].WaitOne( );
                }

                instance.loopBody = null;
            }
        }

        // Private constructor to avoid class instantiation
        private Parallel( ) { }

        // Get instace of the Parallel class
        private static Parallel Instance
        {
            get
            {
                if ( instance == null )
                {
                    instance = new Parallel( );
                    instance.Initialize( );
                }
                else
                {
                    if ( instance.threads.Length != threadsCount )
                    {
                        // terminate old threads
                        instance.Terminate( );
                        // reinitialize
                        instance.Initialize( );

                        // TODO: change reinitialization to reuse already created objects
                    }
                }
                return instance;
            }
        }

        // Initialize Parallel class's instance creating required number of threads
        // and synchronization objects
        private void Initialize( )
        {
            // array of events, which signal about available job
            jobAvailable = new AutoResetEvent[threadsCount];
            // array of events, which signal about available thread
            threadIdle = new ManualResetEvent[threadsCount];
            // array of threads
            threads = new Thread[threadsCount];

            for ( int i = 0; i < threadsCount; i++ )
            {
                jobAvailable[i] = new AutoResetEvent( false );
                threadIdle[i]   = new ManualResetEvent( true );

                threads[i] = new Thread( new ParameterizedThreadStart( WorkerThread ) );
                threads[i].Name = "AForge.Parallel";
                threads[i].IsBackground = true;
                threads[i].Start( i );
            }
        }

        // Terminate all worker threads used for parallel computations and close all
        // synchronization objects
        private void Terminate( )
        {
            // finish thread by setting null loop body and signaling about available work
            loopBody = null;
            for ( int i = 0, threadsCount = threads.Length ; i < threadsCount; i++ )
            {
                jobAvailable[i].Set( );
                // wait for thread termination
                threads[i].Join( );

                // close events
                jobAvailable[i].Close( );
                threadIdle[i].Close( );
            }

            // clean all array references
            jobAvailable    = null;
            threadIdle      = null;
            threads         = null;
        }

        // Worker thread performing parallel computations in loop
        private void WorkerThread( object index )
        {
            int threadIndex = (int) index;
            int localIndex = 0;

            while ( true )
            {
                // wait until there is job to do
                jobAvailable[threadIndex].WaitOne( );

                // exit on null body
                if ( loopBody == null )
                    break;

                while ( true )
                {
                    // get local index incrementing global loop's current index
                    localIndex = Interlocked.Increment( ref currentIndex );

                    if ( localIndex >= stopIndex )
                        break;

                    // run loop's body
                    loopBody( localIndex );
                }

                // signal about thread availability
                threadIdle[threadIndex].Set( );
            }
        }
    }
}
namespace HS_Library
{
    using HS_Library.Input;
    using HS_Library.Input.Win32;
    public class HSConsole : IDisposable
    {
        public enum SwitchConsole{MainConsole, SubConsole}

        #region 정적 클래스
        #region 윈도우 API
        #region 콘솔 관련 API
        [DllImport("kernel32")]
        static extern bool AllocConsole();
        [DllImport("kernel32")]
        static extern bool FreeConsole();
        /// <summary>
        /// 기존 콘솔에 Attach
        /// </summary>
        /// <param name="dwProcessId">붙일 프로세스 ID (자신은 ATTACH_PARENT_PROCESS 사용)</param>
        /// <returns></returns>
        [DllImport("kernel32")]
        public static extern bool AttachConsole(int dwProcessId);
        [DllImport("kernel32.dll", ExactSpelling = true)]
        public static extern IntPtr GetConsoleWindow();

        // P/Invoke required:
        //private const int StdOutputHandle = -11;//0xFFFFFFF5;
        const int STD_INPUT_HANDLE = -10;
        const int STD_OUTPUT_HANDLE = -11;//0xFFFFFFF5;
        const int STD_ERROR_HANDLE = -12;

        const int FILE_SHARE_READ = 0x1;
        const int FILE_SHARE_WRITE = 0x2;

        const int GENERIC_READ = unchecked((int)0x80000000);
        const int GENERIC_WRITE = 0x40000000;

        const int EMPTY = 32;
        const int CONSOLE_TEXTMODE_BUFFER = 1;

        const int _DefaultConsoleBufferSize = 256;

        /// <summary>
        /// 부모 프로세스에 Attach
        /// </summary>
        public const int ATTACH_PARENT_PROCESS = -1;

        [DllImport("kernel32.dll")]
        private static extern IntPtr GetStdHandle(int nStdHandle);
        [DllImport("kernel32.dll")]
        private static extern void SetStdHandle(int nStdHandle, IntPtr handle);

        [DllImport("kernel32")]
        static extern IntPtr CreateConsoleScreenBuffer(int access, int share, IntPtr security, int flags, IntPtr reserved);
        [DllImport("kernel32")]
        static extern bool SetConsoleActiveScreenBuffer(IntPtr handle);
        [DllImport("kernel32")]
        static extern int GetConsoleOutputCP();
        [DllImport("kernel32")]
        static extern int GetConsoleCP();

        [DllImport("kernel32.dll", SetLastError = true)]
        static extern bool SetConsoleIcon(IntPtr hIcon);


        [DllImport("kernel32.dll")]
        private static extern bool CloseHandle(IntPtr hdl);
        #endregion

        #region 창 관련 API

        const int SC_CLOSE = 0xF060;
        const int MF_BYCOMMAND = 0x00000000;
        const UInt32 MF_ENABLED = 0x00000000;
        const UInt32 MF_GRAYED = 0x00000001;
        const UInt32 MF_DISABLED = 0x00000002;

        [DllImport("user32.dll")]
        private static extern int DeleteMenu(IntPtr hMenu, int nPosition, int wFlags);

        [DllImport("user32.dll")]
        private static extern IntPtr GetSystemMenu(IntPtr hWnd, bool bRevert);
        [DllImport("user32.dll")]
        static extern bool EnableMenuItem(IntPtr hMenu, uint uIDEnableItem, uint uEnable);
        #endregion

        #region Mono API
        /*
        static ManualResetEvent resetEvent = new ManualResetEvent(true);

        /// <summary>
        /// Resets the ReadKey function from another thread.
        /// </summary>
        public static void ReadKeyReset()
        {
            resetEvent.Set();
        }

        /// <summary>
        /// Reads a key from stdin
        /// </summary>
        /// <returns>The ConsoleKeyInfo for the pressed key.</returns>
        /// <param name='intercept'>Intercept the key</param>
        public static ConsoleKeyInfo ReadKey(bool intercept = false)
        {
            resetEvent.Reset();
            while (!Console.KeyAvailable)
            {
                if (resetEvent.WaitOne(50))
                    throw new GetKeyInteruptedException();
            }
            int x = CursorX, y = CursorY;
            ConsoleKeyInfo result = CreateKeyInfoFromInt(Console.In.Read(), false);
            if (intercept)
            {
                // Not really an intercept, but it works with mono at least
                if (result.Key != ConsoleKey.Backspace)
                {
                    Write(x, y, " ");
                    SetCursorPosition(x, y);
                }
                else
                {
                    if ((x == 0) && (y > 0))
                    {
                        y--;
                        x = WindowWidth - 1;
                    }
                    SetCursorPosition(x, y);
                }
            }
            return result;
        }*/
        #endregion

        #region 다른API
        #region User32

        [DllImport("user32")]
        static extern IntPtr SetParent(IntPtr hwnd, IntPtr hwnd2);

        [DllImport("user32")]
        static extern IntPtr GetParent(IntPtr hwnd);

        [DllImport("user32")]
        static extern bool SetWindowPos(IntPtr hWnd, IntPtr hWndInsertAfter, int x, int y, int cx, int cy, int flags);

        [DllImport("user32")]
        static extern int SetWindowLong(IntPtr hWnd, int nIndex, int newValue);

        [DllImport("user32")]
        static extern int GetWindowLong(IntPtr hWnd, int nIndex);

        const int WS_OVERLAPPED = 0x0;
        const int WS_CAPTION = 0xC00000; //WS_BORDER | WS_DLGFRAME
        const int WS_SYSMENU = 0x80000;
        const int WS_THICKFRAME = 0x40000;
        const int WS_CHILD = 0x40000000;
        const int WS_OVERLAPPEDWINDOW = (WS_OVERLAPPED | WS_CAPTION | WS_SYSMENU | WS_THICKFRAME | WS_MINIMIZEBOX | WS_MAXIMIZEBOX);

        const int WS_MINIMIZEBOX = 0x20000;
        const int WS_MAXIMIZEBOX = 0x10000;

        const int SWP_NOSIZE = 0x1;
        const int SWP_NOMOVE = 0x2;
        const int SWP_NOZORDER = 0x4;
        const int SWP_NOREDRAW = 0x8;
        const int SWP_NOACTIVATE = 0x10;

        const int GWL_STYLE = (-16);

        #endregion
        #endregion

        #region 콘솔 종료 이벤트 관련

        [DllImport("kernel32.dll", SetLastError = true)]
        private static extern bool SetConsoleCtrlHandler(ConsoleEventHandler callback, bool add);

        // A delegate type to be used as the handler routine 
        // for SetConsoleCtrlHandler.
        private delegate bool ConsoleEventHandler(CtrlTypes ctrlType);

        public delegate void ConsoleExitEventHandler(object sender, HSConsoleExitEventArgs e);

        private static bool _initedHooker;
        private static ConsoleEventHandler _d;
        private static ConsoleExitEventHandler tmp_close;

        /// <summary>
        /// 콘솔이 종료될때 발생합니다.
        /// </summary>
        public static event ConsoleExitEventHandler ConsoleExit;
        /* {
            add  {Init();tmp_close += value; }
            remove { tmp_close -= value; }
        }*/


        private static void ConsoleExitEventInit()
        {
            if (_initedHooker) return;
            _initedHooker = true;
            _d = ConsoleEventCallback;
            SetConsoleCtrlHandler(_d, true);
        }

        private static bool ConsoleEventCallback(CtrlTypes eventType)
        {
            HSConsoleExitEventArgs a = new HSConsoleExitEventArgs(eventType);
            if (ConsoleExit != null) ConsoleExit(GetConsoleWindow(), a);
            return a.Cancle;
        }


        // An enumerated type for the control messages
        // sent to the handler routine.
        public enum CtrlTypes
        {
            CTRL_C_EVENT = 0,
            CTRL_BREAK_EVENT,
            CTRL_CLOSE_EVENT,
            CTRL_LOGOFF_EVENT = 5,
            CTRL_SHUTDOWN_EVENT
        }

        #endregion
        #endregion

        public static IntPtr NewConsole()
        {
            bool a = AllocConsole();
            IntPtr currentStdout = IntPtr.Zero;
            if (a)
            {
                a = AttachConsole(ATTACH_PARENT_PROCESS);
                ConsoleExitEventInit();
                ConsoleExit += Exit_Key;

                /*
                IntPtr defaultStdout = new IntPtr(7);
                currentStdout = GetStdHandle(STD_OUTPUT_HANDLE);
                if (currentStdout != defaultStdout)
                    // reset stdout
                    SetStdHandle(STD_OUTPUT_HANDLE, defaultStdout);

                // reopen stdout
                TextWriter writer = new StreamWriter(Console.OpenStandardOutput())
                { AutoFlush = true };
                Console.SetOut(writer);
                */
                currentStdout = GetStdHandle(STD_OUTPUT_HANDLE);
                Microsoft.Win32.SafeHandles.SafeFileHandle safeFileHandle = new Microsoft.Win32.SafeHandles.SafeFileHandle(currentStdout, true);
                FileStream fileStream = new FileStream(safeFileHandle, FileAccess.Write);
                Encoding encoding = Encoding.UTF8;
                StreamWriter standardOutput = new StreamWriter(fileStream, encoding);
                standardOutput.AutoFlush = true;
                Console.SetOut(standardOutput);
            }
            return currentStdout; 
        }
        public static bool NewConsole(string Title)
        {
            bool a = NewConsole() != IntPtr.Zero;
            Console.Title = Title;
            return a; 
        }
        public static bool NewConsole(Icon icon)
        {
            bool a = NewConsole() != IntPtr.Zero;
            Console.Title = Title;
            return a;
        }
        public static bool NewConsole(string Title, Icon icon)
        {
            bool a = NewConsole() != IntPtr.Zero;
            Console.Title = Title;
            Icon = icon;
            return a; 
        }
        public static bool CloseConsole()
        {
            bool a = false;
            try
            {
                SetInputNull();
                Instance.Dispose();
                try{ConsoleExit -= Exit_Key;}catch{}
            }
            finally{a = FreeConsole();}
            return a;
        }

        public static bool ConsoleXButton(bool Show)
        {
            if (GetConsoleWindow() != IntPtr.Zero)return WinAPIFunction.DeleteXButton2(GetConsoleWindow(), Show);
            else throw new HSConsoleException("콘솔이 활성화되지 않았습니다. 콘솔을 먼저 활성화 해주세요");
        }

        static IntPtr buffer;
        static bool initialized;
        static bool breakHit;
        /// <summary>
        /// 콘솔에 문자열을 입력받을 수 있게 설정합니다.
        /// </summary>
        /// <returns>문자열을 입력받을 수 있는 택스트 스트림 입니다.</returns>
        public static void SetInputAvailable()
        {
            // stdout's handle seems to always be equal to 7
            IntPtr defaultStdout = new IntPtr(7);
            IntPtr currentStdout = GetStdHandle(STD_OUTPUT_HANDLE);

            if (currentStdout != defaultStdout)
                // reset stdout
                SetStdHandle(STD_OUTPUT_HANDLE, defaultStdout);
           
            // reopen stdout
            TextWriter writer = new StreamWriter(Console.OpenStandardOutput(), Encoding.Default) {AutoFlush = true};
            Console.SetOut(writer);
            //return writer;

            /*
            IntPtr hwnd = GetConsoleWindow();
            // Console app
            if (hwnd != IntPtr.Zero)
            {
                buffer = GetStdHandle(STD_OUTPUT_HANDLE);
                return;
            }

            // Windows app
            bool success = AllocConsole();
            if (!success)
                return;*/
            /*
            buffer = CreateConsoleScreenBuffer(GENERIC_READ | GENERIC_WRITE,
                FILE_SHARE_READ | FILE_SHARE_WRITE, IntPtr.Zero, CONSOLE_TEXTMODE_BUFFER, IntPtr.Zero);

            bool result = SetConsoleActiveScreenBuffer(buffer);

            SetStdHandle(STD_OUTPUT_HANDLE, buffer);
            SetStdHandle(STD_ERROR_HANDLE, buffer);

            Stream s = Console.OpenStandardInput(_DefaultConsoleBufferSize);
            StreamReader reader = null;
            if (s == Stream.Null)
                reader = StreamReader.Null;
            else
                reader = new StreamReader(s, Encoding.GetEncoding(GetConsoleCP()),
                    false, _DefaultConsoleBufferSize);

            Console.SetIn(reader);

            // Set up Console.Out
            StreamWriter writer = null;
            s = Console.OpenStandardOutput(_DefaultConsoleBufferSize);
            if (s == Stream.Null)
                writer = StreamWriter.Null;
            else
            {
                writer = new StreamWriter(s, Encoding.GetEncoding(GetConsoleOutputCP()),
                    _DefaultConsoleBufferSize);
                writer.AutoFlush = true;
            }

            Console.SetOut(writer);

            s = Console.OpenStandardError(_DefaultConsoleBufferSize);
            if (s == Stream.Null)
                writer = StreamWriter.Null;
            else
            {
                writer = new StreamWriter(s, Encoding.GetEncoding(GetConsoleOutputCP()),
                    _DefaultConsoleBufferSize);
                writer.AutoFlush = true;
            }

            Console.SetError(writer);*/
        }
        public static void SetInputNull()
        {
            TextWriter tw = Console.Out;
            Console.SetOut(TextWriter.Null);
            tw.Dispose();
        }

        public static HSConsole MakeInstance()
        {
            if (Instance != null) return Instance;
            else
            {
                _Instance = new HSConsole(GetConsoleWindow());
                return Instance;
            }
        }
        public static void BringToFront()
        {
            HS_CSharpUtility.Utility.Win32Utility.BringWindowToTop(Instance==null?GetConsoleWindow():Instance.ConsoleHandle);
        }

        public static void Write(object Value, SwitchConsole WhereToWrite = SwitchConsole.SubConsole){if(Switch == WhereToWrite)Console.Write(Value);}
        public static void Write(string Value, SwitchConsole WhereToWrite = SwitchConsole.SubConsole){if(Switch == WhereToWrite)Console.Write(Value);}     
        public static void Write(string Value, SwitchConsole WhereToWrite = SwitchConsole.SubConsole, params object[] format) { if(Switch == WhereToWrite)Console.Write(Value, format); }
        public static void WriteLine(object Value, SwitchConsole WhereToWrite = SwitchConsole.SubConsole) {if(Switch == WhereToWrite)Console.WriteLine(Value);}
        public static void WriteLine(string Value, SwitchConsole WhereToWrite = SwitchConsole.SubConsole) {if(Switch == WhereToWrite)Console.WriteLine(Value);}
        public static void WriteLine(string Value, SwitchConsole WhereToWrite = SwitchConsole.SubConsole, params object[] format) {if(Switch == WhereToWrite)Console.WriteLine(Value, format); }

        #region 프로퍼티
        private static SwitchConsole _Switch = SwitchConsole.SubConsole;
        public static SwitchConsole Switch{get{return _Switch;}set{_Switch = value;}}
        public static string Title{get{try{return Console.Title;}catch{return null;}}set{Console.Title = value;}}

        static bool _Ctrl_C_KeyCancle = false;
        static bool _Ctrl_C_KeyCancle_First;
        public static bool Exit_KeyCancle
        {
            get{ return _Ctrl_C_KeyCancle;}
            set
            {
                //Console.TreatControlCAsInput = value;
                //if (!_Ctrl_C_KeyCancle_First) Console.CancelKeyPress += Ctrl_C;
                _Ctrl_C_KeyCancle_First = true;
                _Ctrl_C_KeyCancle = value;
            }
        }
        #endregion

        static HSConsole _Instance = null;
        public static HSConsole Instance{get{return _Instance;}}

        /// <summary>
        /// Gets and sets a new parent hwnd to the console window
        /// </summary>
        /// <param name="window"></param>
        public static IntPtr ParentHandle
        {
            get
            {
                IntPtr hwnd = GetConsoleWindow();
                return GetParent(hwnd);
            }
            set
            {
                IntPtr hwnd = Instance==null?GetConsoleWindow():Instance.ConsoleHandle;
                if (hwnd == IntPtr.Zero)
                    return;

                SetParent(hwnd, value);
                int style = GetWindowLong(hwnd, GWL_STYLE);
                if (value == IntPtr.Zero)
                    SetWindowLong(hwnd, GWL_STYLE, (style & ~WS_CHILD) | WS_OVERLAPPEDWINDOW);
                else
                    SetWindowLong(hwnd, GWL_STYLE, (style | WS_CHILD) & ~WS_OVERLAPPEDWINDOW);
                SetWindowPos(hwnd, IntPtr.Zero, 0, 0, 0, 0, SWP_NOSIZE | SWP_NOZORDER | SWP_NOACTIVATE);
            }
        }

        static Icon _Icon;
        public static Icon Icon
        {
            get{return _Icon;}
            set
            {
                int WM_SETICON = 0x80;
                int ICON_SMALL = 0;
                int ICON_BIG = 1;

                if (SetConsoleIcon(value.Handle)) _Icon = value;
                else SendMessage(Process.GetCurrentProcess().MainWindowHandle, WM_SETICON, ICON_BIG, value.Handle);
            }
        }

        #region Private
        //static void Ctrl_C(object o, ConsoleCancelEventArgs cc) {try{cc.Cancel = _Ctrl_C_KeyCancle;} catch{}}
        static void Exit_Key(object sender, HSConsoleExitEventArgs e) {e.Cancle = Exit_KeyCancle;}

        #endregion
        #endregion

        #region 동적 클래스

        #region 이벤트
        public delegate void TextEventHandler(object sender, string Text);
        public event TextEventHandler TextEvent;
        public delegate void KeyEventHandler(object sender, ConsoleKeyInfo Key);
        public event KeyEventHandler KeyEvent;
        #endregion

        System.Threading.Thread[] Key_th = new System.Threading.Thread[2];
        System.Threading.ThreadStart[] Key_th_Loop = new System.Threading.ThreadStart[2];

        IntPtr _ConsoleHandle;
        IntPtr ConsoleHandle{get{return _ConsoleHandle;}}

        internal HSConsole(IntPtr ConsoleHandle)
        {
            if(_Instance!=null)throw new HSConsoleException("이미 인스턴스가 생성되어 있습니다.");
            if (GetConsoleWindow() == IntPtr.Zero) throw new HSConsoleException("HS_Audio.LIBRARY.HSConsole.ShowConsole() 로 콘솔창을 먼저 생성하여 주십시오.");

            this._ConsoleHandle = ConsoleHandle;

            Key_th_Loop[0] = new System.Threading.ThreadStart(KeyPressLoop);
            Key_th_Loop[1] = new System.Threading.ThreadStart(ReadLineLoop);
        }

        public void StartKeyPressEvent()
        {
            try{if (Key_th[0]!=null)Key_th[0].Abort();}catch{}
            Key_th[0] = new System.Threading.Thread(Key_th_Loop[0]);
            Key_th[0].Start();
        }
        public void StartTextEvent()
        {
            try{if (Key_th[1]!=null)Key_th[1].Abort();}catch{}
            Key_th[1] = new System.Threading.Thread(Key_th_Loop[1]);
            Key_th[1].Start();
        }
        public void StopKeyPressEvent()
        {
            try{if (Key_th[0]!=null)Key_th[0].Abort();}catch{}
        }
        public void StopTextEvent()
        {
            try{if (Key_th[1]!=null)Key_th[1].Abort();}catch{}
        }
        void KeyPressLoop()
        {
            try
            {
                while (true)
                {
                    ConsoleKeyInfo keyinfo = Console.ReadKey(KeyPressShowConsole);
                    if (KeyEvent != null)
                        try {KeyEvent(this, keyinfo); } catch {}
                }
            }
            catch
            {
            }
        }
        [DllImport("user32.dll", CharSet = CharSet.Auto)]
        static extern IntPtr SendMessage(IntPtr hWnd, UInt32 Msg, int wParam, int lParam);
        [DllImport("user32.dll", CharSet = CharSet.Auto)]
        static extern IntPtr SendMessage(IntPtr hWnd, int Msg, int wParam, IntPtr lParam);
        const UInt32 WM_KEYDOWN = 0x0100;
        const UInt32 WM_KEYUP = 0x101;
       void ReadLineLoop()
        {
        Restart:
            try
            {
                string a = "";
                while (true)
                {
                    a = Console.ReadLine();
                    /*
                    if (a == "")
                    {
                        IntPtr aa = SendMessage(ConsoleHandle, WM_KEYDOWN, (int)VirtualKeyCode.RETURN, 0);
                        aa = SendMessage(ConsoleHandle, WM_KEYUP, (int)VirtualKeyCode.RETURN, 0);
                    }*///InputSimulator.SimulateKeyPress(VirtualKeyCode.RETURN);

                    //Console.WriteLine(a);
                    //ConsoleKeyInfo keyinfo = Console.ReadKey(KeyPressShowConsole);
                    //if (keyinfo.Key == ConsoleKey.Enter)
                    {
                        if (TextEvent != null)
                            try{ TextEvent(this, a); } catch { } finally{a = "";}
                    }
                    //else {a += keyinfo.KeyChar; HSConsole.Write(a, SwitchConsole.MainConsole);}
                }
            }
            catch(System.Threading.ThreadAbortException){}
            catch(System.IO.IOException){}
            catch{System.Threading.Thread.Sleep(1);goto Restart;}
        }

        public void Dispose()
        {
            foreach(System.Threading.Thread dd in Key_th)try{if (dd!=null)dd.Abort();}catch{}
            _Instance = null;
            KeyEvent = null;
            TextEvent = null;
        }

         bool _KeyPressShowConsole = true;
        /// <summary>
        /// 콘솔에서 키를 눌렀을때 누른키를 콜솔창에 표시할지의 여부 입니다.
        /// </summary>
        public bool KeyPressShowConsole{get{return _KeyPressShowConsole;}set{_KeyPressShowConsole = value;}}
        #endregion

        public static class WinAPIFunction
        {
            public static void DeleteXButton(IntPtr Handle) {DeleteMenu(GetSystemMenu(GetConsoleWindow(), false), SC_CLOSE, MF_BYCOMMAND);}
            public static bool DeleteXButton2(IntPtr Handle, bool Enable = false)
            {
                IntPtr hSystemMenu = GetSystemMenu(Handle, false);
                return EnableMenuItem(hSystemMenu, SC_CLOSE, (uint)(MF_ENABLED | (Enable ? MF_ENABLED : MF_GRAYED)));
            }
        }
    }
    public class HSConsoleException : Exception
    {
        string _Message;
        Exception _InnerException;

        public HSConsoleException(){}
        public HSConsoleException(string Message){_Message = Message;}
        public HSConsoleException(Exception InnerException){_InnerException = InnerException;}
        public HSConsoleException(string Message, Exception InnerException){_Message = Message;_InnerException = InnerException;}

        public string Message{get{return _Message;}}
        public Exception InnerException{get{return _InnerException;}}
    }
    public class HSConsoleExitEventArgs : EventArgs
    {
        public HSConsoleExitEventArgs(HSConsole.CtrlTypes type){this.MessageType = type;}
        public bool Cancle{get;set;}
        public HSConsole.CtrlTypes MessageType{get; internal protected set;}
    }
}
namespace HS_Library.Input
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime.InteropServices;
    using System.Text;
    using HS_Library.Input.Win32;

    public static class InputSimulator
    {
        [DllImport("user32.dll", SetLastError=true)]
        private static extern short GetAsyncKeyState(ushort virtualKeyCode);
        [DllImport("user32.dll", SetLastError=true)]
        private static extern short GetKeyState(ushort virtualKeyCode);
        [DllImport("user32.dll")]
        private static extern IntPtr GetMessageExtraInfo();
        public static bool IsKeyDown(VirtualKeyCode keyCode)
        {
            return (GetKeyState((ushort) keyCode) < 0);
        }

        public static bool IsKeyDownAsync(VirtualKeyCode keyCode)
        {
            return (GetAsyncKeyState((ushort) keyCode) < 0);
        }

        public static bool IsTogglingKeyInEffect(VirtualKeyCode keyCode)
        {
            return ((GetKeyState((ushort) keyCode) & 1) == 1);
        }

        [DllImport("user32.dll", SetLastError=true)]
        private static extern uint SendInput(uint numberOfInputs, INPUT[] inputs, int sizeOfInputStructure);
        public static void SimulateKeyDown(VirtualKeyCode keyCode)
        {
            INPUT input = new INPUT();
            input.Type = 1;
            input.Data.Keyboard = new KEYBDINPUT();
            input.Data.Keyboard.Vk = (ushort) keyCode;
            input.Data.Keyboard.Scan = 0;
            input.Data.Keyboard.Flags = 0;
            input.Data.Keyboard.Time = 0;
            input.Data.Keyboard.ExtraInfo = IntPtr.Zero;
            INPUT[] inputs = new INPUT[] { input };
            if (SendInput(1, inputs, Marshal.SizeOf(typeof(INPUT))) == 0)
            {
                throw new Exception(string.Format("The key down simulation for {0} was not successful.", keyCode));
            }
        }

        public static void SimulateKeyPress(VirtualKeyCode keyCode)
        {
            INPUT input = new INPUT();
            input.Type = 1;
            input.Data.Keyboard = new KEYBDINPUT();
            input.Data.Keyboard.Vk = (ushort) keyCode;
            input.Data.Keyboard.Scan = 0;
            input.Data.Keyboard.Flags = 0;
            input.Data.Keyboard.Time = 0;
            input.Data.Keyboard.ExtraInfo = IntPtr.Zero;
            INPUT input2 = new INPUT();
            input2.Type = 1;
            input2.Data.Keyboard = new KEYBDINPUT();
            input2.Data.Keyboard.Vk = (ushort) keyCode;
            input2.Data.Keyboard.Scan = 0;
            input2.Data.Keyboard.Flags = 2;
            input2.Data.Keyboard.Time = 0;
            input2.Data.Keyboard.ExtraInfo = IntPtr.Zero;
            INPUT[] inputs = new INPUT[] { input, input2 };
            if (SendInput(2, inputs, Marshal.SizeOf(typeof(INPUT))) == 0)
            {
                throw new Exception(string.Format("The key press simulation for {0} was not successful.", keyCode));
            }
        }

        public static void SimulateKeyUp(VirtualKeyCode keyCode)
        {
            INPUT input = new INPUT();
            input.Type = 1;
            input.Data.Keyboard = new KEYBDINPUT();
            input.Data.Keyboard.Vk = (ushort) keyCode;
            input.Data.Keyboard.Scan = 0;
            input.Data.Keyboard.Flags = 2;
            input.Data.Keyboard.Time = 0;
            input.Data.Keyboard.ExtraInfo = IntPtr.Zero;
            INPUT[] inputs = new INPUT[] { input };
            if (SendInput(1, inputs, Marshal.SizeOf(typeof(INPUT))) == 0)
            {
                throw new Exception(string.Format("The key up simulation for {0} was not successful.", keyCode));
            }
        }

        public static void SimulateModifiedKeyStroke(IEnumerable<VirtualKeyCode> modifierKeyCodes, IEnumerable<VirtualKeyCode> keyCodes)
        {
            if (modifierKeyCodes != null)
            {
                Enumerable.ToList<VirtualKeyCode>(modifierKeyCodes).ForEach(delegate (VirtualKeyCode x) {
                    SimulateKeyDown(x);
                });
            }
            if (keyCodes != null)
            {
                Enumerable.ToList<VirtualKeyCode>(keyCodes).ForEach(delegate (VirtualKeyCode x) {
                    SimulateKeyPress(x);
                });
            }
            if (modifierKeyCodes != null)
            {
                Enumerable.ToList<VirtualKeyCode>(Enumerable.Reverse<VirtualKeyCode>(modifierKeyCodes)).ForEach(delegate (VirtualKeyCode x) {
                    SimulateKeyUp(x);
                });
            }
        }

        public static void SimulateModifiedKeyStroke(IEnumerable<VirtualKeyCode> modifierKeyCodes, VirtualKeyCode keyCode)
        {
            if (modifierKeyCodes != null)
            {
                Enumerable.ToList<VirtualKeyCode>(modifierKeyCodes).ForEach(delegate (VirtualKeyCode x) {
                    SimulateKeyDown(x);
                });
            }
            SimulateKeyPress(keyCode);
            if (modifierKeyCodes != null)
            {
                Enumerable.ToList<VirtualKeyCode>(Enumerable.Reverse<VirtualKeyCode>(modifierKeyCodes)).ForEach(delegate (VirtualKeyCode x) {
                    SimulateKeyUp(x);
                });
            }
        }

        public static void SimulateModifiedKeyStroke(VirtualKeyCode modifierKey, IEnumerable<VirtualKeyCode> keyCodes)
        {
            SimulateKeyDown(modifierKey);
            if (keyCodes != null)
            {
                Enumerable.ToList<VirtualKeyCode>(keyCodes).ForEach(delegate (VirtualKeyCode x) {
                    SimulateKeyPress(x);
                });
            }
            SimulateKeyUp(modifierKey);
        }

        public static void SimulateModifiedKeyStroke(VirtualKeyCode modifierKeyCode, VirtualKeyCode keyCode)
        {
            SimulateKeyDown(modifierKeyCode);
            SimulateKeyPress(keyCode);
            SimulateKeyUp(modifierKeyCode);
        }

        public static void SimulateTextEntry(string text)
        {
            if (text.Length > 0x7fffffffL)
            {
                throw new ArgumentException(string.Format("The text parameter is too long. It must be less than {0} characters.", 0x7fffffff), "text");
            }
            byte[] bytes = Encoding.ASCII.GetBytes(text);
            int length = bytes.Length;
            INPUT[] inputs = new INPUT[length * 2];
            for (int i = 0; i < length; i++)
            {
                ushort num3 = bytes[i];
                INPUT input = new INPUT();
                input.Type = 1;
                input.Data.Keyboard = new KEYBDINPUT();
                input.Data.Keyboard.Vk = 0;
                input.Data.Keyboard.Scan = num3;
                input.Data.Keyboard.Flags = 4;
                input.Data.Keyboard.Time = 0;
                input.Data.Keyboard.ExtraInfo = IntPtr.Zero;
                INPUT input2 = new INPUT();
                input2.Type = 1;
                input2.Data.Keyboard = new KEYBDINPUT();
                input2.Data.Keyboard.Vk = 0;
                input2.Data.Keyboard.Scan = num3;
                input2.Data.Keyboard.Flags = 6;
                input2.Data.Keyboard.Time = 0;
                input2.Data.Keyboard.ExtraInfo = IntPtr.Zero;
                if ((num3 & 0xff00) == 0xe000)
                {
                    input.Data.Keyboard.Flags |= 1;
                    input2.Data.Keyboard.Flags |= 1;
                }
                inputs[2 * i] = input;
                inputs[(2 * i) + 1] = input2;
            }
            SendInput((uint) (length * 2), inputs, Marshal.SizeOf(typeof(INPUT)));
        }
    }
}
namespace HS_Library.Input.Win32
{
    [StructLayout(LayoutKind.Sequential)]
    internal struct HARDWAREINPUT
    {
        public uint Msg;
        public ushort ParamL;
        public ushort ParamH;
    }
    [StructLayout(LayoutKind.Sequential)]
    internal struct INPUT
    {
        public uint Type;
        public MOUSEKEYBDHARDWAREINPUT Data;
    }
    [StructLayout(LayoutKind.Sequential)]
    internal struct KEYBDINPUT
    {
        public ushort Vk;
        public ushort Scan;
        public uint Flags;
        public uint Time;
        public IntPtr ExtraInfo;
    }
    [StructLayout(LayoutKind.Sequential)]
    internal struct MOUSEINPUT
    {
        public int X;
        public int Y;
        public uint MouseData;
        public uint Flags;
        public uint Time;
        public IntPtr ExtraInfo;
    }
    [StructLayout(LayoutKind.Explicit)]
    internal struct MOUSEKEYBDHARDWAREINPUT
    {
        [FieldOffset(0)]
        public HARDWAREINPUT Hardware;
        [FieldOffset(0)]
        public KEYBDINPUT Keyboard;
        [FieldOffset(0)]
        public MOUSEINPUT Mouse;
    }

    public enum InputType : uint
    {
        HARDWARE = 2,
        KEYBOARD = 1,
        MOUSE = 0
    }
    public enum KeyboardFlag : uint
    {
        EXTENDEDKEY = 1,
        KEYUP = 2,
        SCANCODE = 8,
        UNICODE = 4
    }
    public enum MouseFlag : uint
    {
        ABSOLUTE = 0x8000,
        LEFTDOWN = 2,
        LEFTUP = 4,
        MIDDLEDOWN = 0x20,
        MIDDLEUP = 0x40,
        MOVE = 1,
        RIGHTDOWN = 8,
        RIGHTUP = 0x10,
        VIRTUALDESK = 0x4000,
        WHEEL = 0x800,
        XDOWN = 0x80,
        XUP = 0x100
    }
    public enum XButton : uint
    {
        XBUTTON1 = 1,
        XBUTTON2 = 2
    }

    public enum VirtualKeyCode : ushort
    {
        ACCEPT = 30,
        ADD = 0x6b,
        APPS = 0x5d,
        ATTN = 0xf6,
        BACK = 8,
        BROWSER_BACK = 0xa6,
        BROWSER_FAVORITES = 0xab,
        BROWSER_FORWARD = 0xa7,
        BROWSER_HOME = 0xac,
        BROWSER_REFRESH = 0xa8,
        BROWSER_SEARCH = 170,
        BROWSER_STOP = 0xa9,
        CANCEL = 3,
        CAPITAL = 20,
        CLEAR = 12,
        CONTROL = 0x11,
        CONVERT = 0x1c,
        CRSEL = 0xf7,
        DECIMAL = 110,
        DELETE = 0x2e,
        DIVIDE = 0x6f,
        DOWN = 40,
        END = 0x23,
        EREOF = 0xf9,
        ESCAPE = 0x1b,
        EXECUTE = 0x2b,
        EXSEL = 0xf8,
        F1 = 0x70,
        F10 = 0x79,
        F11 = 0x7a,
        F12 = 0x7b,
        F13 = 0x7c,
        F14 = 0x7d,
        F15 = 0x7e,
        F16 = 0x7f,
        F17 = 0x80,
        F18 = 0x81,
        F19 = 130,
        F2 = 0x71,
        F20 = 0x83,
        F21 = 0x84,
        F22 = 0x85,
        F23 = 0x86,
        F24 = 0x87,
        F3 = 0x72,
        F4 = 0x73,
        F5 = 0x74,
        F6 = 0x75,
        F7 = 0x76,
        F8 = 0x77,
        F9 = 120,
        FINAL = 0x18,
        HANGEUL = 0x15,
        HANGUL = 0x15,
        HANJA = 0x19,
        HELP = 0x2f,
        HOME = 0x24,
        INSERT = 0x2d,
        JUNJA = 0x17,
        KANA = 0x15,
        KANJI = 0x19,
        LAUNCH_APP1 = 0xb6,
        LAUNCH_APP2 = 0xb7,
        LAUNCH_MAIL = 180,
        LAUNCH_MEDIA_SELECT = 0xb5,
        LBUTTON = 1,
        LCONTROL = 0xa2,
        LEFT = 0x25,
        LMENU = 0xa4,
        LSHIFT = 160,
        LWIN = 0x5b,
        MBUTTON = 4,
        MEDIA_NEXT_TRACK = 0xb0,
        MEDIA_PLAY_PAUSE = 0xb3,
        MEDIA_PREV_TRACK = 0xb1,
        MEDIA_STOP = 0xb2,
        MENU = 0x12,
        MODECHANGE = 0x1f,
        MULTIPLY = 0x6a,
        NEXT = 0x22,
        NONAME = 0xfc,
        NONCONVERT = 0x1d,
        NUMLOCK = 0x90,
        NUMPAD0 = 0x60,
        NUMPAD1 = 0x61,
        NUMPAD2 = 0x62,
        NUMPAD3 = 0x63,
        NUMPAD4 = 100,
        NUMPAD5 = 0x65,
        NUMPAD6 = 0x66,
        NUMPAD7 = 0x67,
        NUMPAD8 = 0x68,
        NUMPAD9 = 0x69,
        OEM_1 = 0xba,
        OEM_102 = 0xe2,
        OEM_2 = 0xbf,
        OEM_3 = 0xc0,
        OEM_4 = 0xdb,
        OEM_5 = 220,
        OEM_6 = 0xdd,
        OEM_7 = 0xde,
        OEM_8 = 0xdf,
        OEM_CLEAR = 0xfe,
        OEM_COMMA = 0xbc,
        OEM_MINUS = 0xbd,
        OEM_PERIOD = 190,
        OEM_PLUS = 0xbb,
        PA1 = 0xfd,
        PACKET = 0xe7,
        PAUSE = 0x13,
        PLAY = 250,
        PRINT = 0x2a,
        PRIOR = 0x21,
        PROCESSKEY = 0xe5,
        RBUTTON = 2,
        RCONTROL = 0xa3,
        RETURN = 13,
        RIGHT = 0x27,
        RMENU = 0xa5,
        RSHIFT = 0xa1,
        RWIN = 0x5c,
        SCROLL = 0x91,
        SELECT = 0x29,
        SEPARATOR = 0x6c,
        SHIFT = 0x10,
        SLEEP = 0x5f,
        SNAPSHOT = 0x2c,
        SPACE = 0x20,
        SUBTRACT = 0x6d,
        TAB = 9,
        UP = 0x26,
        VK_0 = 0x30,
        VK_1 = 0x31,
        VK_2 = 50,
        VK_3 = 0x33,
        VK_4 = 0x34,
        VK_5 = 0x35,
        VK_6 = 0x36,
        VK_7 = 0x37,
        VK_8 = 0x38,
        VK_9 = 0x39,
        VK_A = 0x41,
        VK_B = 0x42,
        VK_C = 0x43,
        VK_D = 0x44,
        VK_E = 0x45,
        VK_F = 70,
        VK_G = 0x47,
        VK_H = 0x48,
        VK_I = 0x49,
        VK_J = 0x4a,
        VK_K = 0x4b,
        VK_L = 0x4c,
        VK_M = 0x4d,
        VK_N = 0x4e,
        VK_O = 0x4f,
        VK_P = 80,
        VK_Q = 0x51,
        VK_R = 0x52,
        VK_S = 0x53,
        VK_T = 0x54,
        VK_U = 0x55,
        VK_V = 0x56,
        VK_W = 0x57,
        VK_X = 0x58,
        VK_Y = 0x59,
        VK_Z = 90,
        VOLUME_DOWN = 0xae,
        VOLUME_MUTE = 0xad,
        VOLUME_UP = 0xaf,
        XBUTTON1 = 5,
        XBUTTON2 = 6,
        ZOOM = 0xfb
    }
}
