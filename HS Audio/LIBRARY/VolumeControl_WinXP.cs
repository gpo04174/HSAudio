﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using SynchronizedVolumeControl;

namespace HS_Audio.LIBRARY
{
    public class HSVolumeControl_WinXP : SubclassHWND, IDisposable
    {
        #region 상수
        // --------------------------------------------------------------------------------------------

        // MIXER CONTROL and COMPONENT TYPES ... MIXER CONTROL and COMPONENT TYPES ... MIXER CONTROL an

        // CONTROL TYPES
        const int lineVolumeControl = MM.MIXERCONTROL_CONTROLTYPE_VOLUME;
        const int spkrVolumeControl = MM.MIXERCONTROL_CONTROLTYPE_VOLUME;

        const int lineMuteControl = MM.MIXERCONTROL_CONTROLTYPE_MUTE;
        const int spkrMuteControl = MM.MIXERCONTROL_CONTROLTYPE_MUTE;

        // CONTROL IDs
        int lineVolumeControlID = -1;
        int lineMuteControlID = -1;
        int spkrVolumeControlID = -1;
        int spkrMuteControlID = -1;

        const int lineDown = 0;      // These Tag values will serve to identify the clicked control
        const int lineUp = 1;
        const int spkrDown = 10;
        const int spkrUp = 11;
        #endregion

        /// <summary>
        /// COMPONENT TYPES
        /// </summary>
        public enum VolumeCategory
        {
            /// <summary>
            /// MIXERLINE_COMPONENTTYPE_DST_SPEAKERS
            /// </summary>
            Master=4,
            /// <summary>
            /// MIXERLINE_COMPONENTTYPE_SRC_FIRST+2
            /// </summary>
            Line=0x1000+2
        }

        public delegate void VolumeChangedEventHandler(object sender, VolumeCategory Category, int Volume);
        public event VolumeChangedEventHandler VolumeChanged;
        public delegate void MuteChangedEventHandler(object sender, VolumeCategory Category, bool Mute);
        public event MuteChangedEventHandler MuteChanged;
            
        public IntPtr UserHandle { get; protected internal set; }

        public MM.GetMixerControlClasss GetMixerControl(VolumeCategory Cat, bool VolumeControl=true, int hmixer = 0){return MM.GetControlID(hmixer, (int)Cat, VolumeControl ? spkrVolumeControl : spkrMuteControl);}
        public MM.GetMixerControlClasss GetMixerControl(bool VolumeControl = true, int hmixer = 0) { return MM.GetControlID(hmixer, (int)this.Category, VolumeControl ? spkrVolumeControl : spkrMuteControl); }

        public MM.GetMixerControlClasss Control;


        [Obsolete("창 핸들이 없으면 이벤트가 발생하지 않습니다.")]
        public HSVolumeControl_WinXP(int DeviceID=0)
        {
            Category = VolumeCategory.Master;
            if (MM.MMSYSERR_NOERROR == MM.CheckMixer())
            {
                lineVolumeControlID = MM.GetControlID((int)VolumeCategory.Line, lineVolumeControl);
                lineMuteControlID = MM.GetControlID((int)VolumeCategory.Line, lineMuteControl);
                spkrVolumeControlID = MM.GetControlID((int)VolumeCategory.Master, spkrVolumeControl);
                spkrMuteControlID = MM.GetControlID((int)VolumeCategory.Master, spkrMuteControl);

                if ((lineVolumeControlID >= 0) &&
                    (lineMuteControlID >= 0) &&
                    (spkrVolumeControlID >= 0) &&
                    (spkrMuteControlID >= 0))
                {
                    // Set up a window to receive MM_MIXM_CONTROL_CHANGE messages ...
                    SubclassHWND w = new SubclassHWND();
                    base.AssignHandle(this.Handle);

                    int iw = (int)this.Handle;        // Note that the window's handle needs to be cast as
                    // an integer before it can be used

                    // ... and we can now activate the message monitor 
                    bool b = MM.MonitorControl(iw);
                }
                else throw new Exception("초기화 하는동안 오류가 발생하였습니다.\n현재 운영체제가 XP가 맞는지 확인해 주십시오.");
            }
            else throw new Exception("초기화 하는동안 오류가 발생하였습니다.\n현재 운영체제가 XP가 맞는지 확인해 주십시오.");
        }
        public HSVolumeControl_WinXP(IntPtr UserHandle, int DeviceID = 0)
        {
            Category = VolumeCategory.Master;
            if (MM.MMSYSERR_NOERROR == MM.CheckMixer(DeviceID))
            {
                lineVolumeControlID = MM.GetControlID((int)VolumeCategory.Line, lineVolumeControl, 0, DeviceID).Control.dwControlID;
                lineMuteControlID = MM.GetControlID((int)VolumeCategory.Line, lineMuteControl,0,DeviceID).Control.dwControlID;
                spkrVolumeControlID = MM.GetControlID((int)VolumeCategory.Master, spkrVolumeControl,0,DeviceID).Control.dwControlID;
                spkrMuteControlID = MM.GetControlID((int)VolumeCategory.Master, spkrMuteControl,0,DeviceID).Control.dwControlID;

                if ((lineVolumeControlID >= 0) &&
                    (lineMuteControlID >= 0) &&
                    (spkrVolumeControlID >= 0) &&
                    (spkrMuteControlID >= 0))
                {
                    // Set up a window to receive MM_MIXM_CONTROL_CHANGE messages ...
                    //SubclassHWND w = new SubclassHWND();
                    this.UserHandle = UserHandle == IntPtr.Zero ? this.Handle : UserHandle;
                    base.AssignHandle(this.UserHandle);

                    int iw = (int)this.UserHandle;        // Note that the window's handle needs to be cast as
                    // an integer before it can be used

                    // ... and we can now activate the message monitor 
                    bool b = MM.MonitorControl(iw);
                }
                else throw new Exception("초기화 하는동안 오류가 발생하였습니다.\n현재 운영체제가 XP가 맞는지 확인해 주십시오.");
            }
            else throw new Exception("초기화 하는동안 오류가 발생하였습니다.\n현재 운영체제가 XP가 맞는지 확인해 주십시오.");
        }
        [Obsolete("창 핸들이 없으면 이벤트가 발생하지 않습니다.")]
        public HSVolumeControl_WinXP(VolumeCategory Category, int DeviceID = 0)
        {
            this.Category = Category;
            if (MM.MMSYSERR_NOERROR == MM.CheckMixer())
            {
                if (Category == VolumeCategory.Line)
                {
                    lineVolumeControlID = MM.GetControlID((int)VolumeCategory.Line, lineVolumeControl);
                    lineMuteControlID = MM.GetControlID((int)VolumeCategory.Line, lineMuteControl);

                    if ((lineVolumeControlID >= 0) &&
                        (lineMuteControlID >= 0)){ }
                    else throw new Exception("라인 볼륨을 초기화 하는동안 오류가 발생하였습니다.\n현재 운영체제가 XP가 맞는지 확인해 주십시오.");
                }
                else if (Category == VolumeCategory.Master)
                {
                    spkrVolumeControlID = MM.GetControlID((int)VolumeCategory.Master, spkrVolumeControl);
                    spkrMuteControlID = MM.GetControlID((int)VolumeCategory.Master, spkrMuteControl);

                    if ((spkrVolumeControlID >= 0) &&
                       (spkrMuteControlID >= 0)) {}
                    else throw new Exception("마스터 볼륨을 초기화 하는동안 오류가 발생하였습니다.\n현재 운영체제가 XP가 맞는지 확인해 주십시오.");
                }

                // Set up a window to receive MM_MIXM_CONTROL_CHANGE messages ...
                //SubclassHWND w = new SubclassHWND();
                base.AssignHandle(this.Handle);

                int iw = (int)this.Handle;        // Note that the window's handle needs to be cast as
                // an integer before it can be used

                // ... and we can now activate the message monitor 
                bool b = MM.MonitorControl(iw);
            }
            else throw new Exception("초기화 하는동안 오류가 발생하였습니다.\n현재 운영체제가 XP가 맞는지 확인해 주십시오.");
        }
        public HSVolumeControl_WinXP(VolumeCategory Category, IntPtr UserHandle, int DeviceID = 0)
        {
            this.Category = Category;
            if (MM.MMSYSERR_NOERROR == MM.CheckMixer())
            {
                if (Category == VolumeCategory.Line)
                {
                    lineVolumeControlID = MM.GetControlID((int)VolumeCategory.Line, lineVolumeControl);
                    lineMuteControlID = MM.GetControlID((int)VolumeCategory.Line, lineMuteControl);

                    if ((lineVolumeControlID >= 0) &&
                        (lineMuteControlID >= 0)) { }
                    else throw new Exception("라인 볼륨을 초기화 하는동안 오류가 발생하였습니다.\n현재 운영체제가 XP가 맞는지 확인해 주십시오.");
                }
                else if (Category == VolumeCategory.Master)
                {
                    spkrVolumeControlID = MM.GetControlID((int)VolumeCategory.Master, spkrVolumeControl);
                    spkrMuteControlID = MM.GetControlID((int)VolumeCategory.Master, spkrMuteControl);

                    if ((spkrVolumeControlID >= 0) &&
                       (spkrMuteControlID >= 0)) { }
                    else throw new Exception("마스터 볼륨을 초기화 하는동안 오류가 발생하였습니다.\n현재 운영체제가 XP가 맞는지 확인해 주십시오.");
                }

                // Set up a window to receive MM_MIXM_CONTROL_CHANGE messages ...
                //SubclassHWND w = new SubclassHWND();
                this.UserHandle = UserHandle == IntPtr.Zero ? this.Handle : UserHandle;
                base.AssignHandle(this.UserHandle);

                int iw = (int)this.UserHandle;        // Note that the window's handle needs to be cast as
                // an integer before it can be used

                // ... and we can now activate the message monitor 
                bool b = MM.MonitorControl(iw);
            }
            else throw new Exception("초기화 하는동안 오류가 발생하였습니다.\n현재 운영체제가 XP가 맞는지 확인해 주십시오.");
        }

        protected override void WndProc(ref Message m)
        {
            if (m.Msg == MM.MM_MIXM_CONTROL_CHANGE)     // Code 0x3D1 indicates a control change
            {
                int i = (int)(m.LParam);

                // We can't use switch so we must do it this way:
                bool b1 = i == lineVolumeControlID ? true : false;
                bool b2 = i == lineMuteControlID ? true : false;
                bool b3 = i == spkrVolumeControlID ? true : false;
                bool b4 = i == spkrMuteControlID ? true : false;
                //
                if (b1)
                {
                    if(VolumeChanged!=null){ try{
                    // Line volume update                                           LINE VOLUME
                        int v = MM.GetVolume(lineVolumeControl, (int)VolumeCategory.Line);
                    VolumeChanged(this, VolumeCategory.Line, v);}catch{}}
                }

                if (b2)
                {
                    if(MuteChanged!=null){ try{
                    // Line mute update                                             LINE MUTE
                        int muteStatus = MM.GetVolume(lineMuteControl, (int)VolumeCategory.Line);
                    MuteChanged(this, VolumeCategory.Line,muteStatus > 0 ? true : false);}catch{}}
                }
                if (b3)
                {
                    if(VolumeChanged!=null){ try{
                    // Spkr volume update                                           SPKR VOLUME
                        int v = MM.GetVolume(spkrVolumeControl, (int)VolumeCategory.Master);
                    VolumeChanged(this, VolumeCategory.Master, v);}catch{}}
                }

                if (b4)
                {
                     if(MuteChanged!=null){ try{
                    // Spkr mute update                                             SPKR MUTE
                         int muteStatus = MM.GetVolume(spkrMuteControl, (int)VolumeCategory.Master);
                    MuteChanged(this, VolumeCategory.Master,muteStatus > 0 ? true : false);}catch{}}
                } 
            }
            // The intercepted message, with all other messages is
            // now forwarded to base.WndProc for processing
            base.WndProc(ref m);
        }

        public VolumeCategory Category{get; internal protected set;}

        public int GetVolume(VolumeCategory Category)
        {
            if(Category == VolumeCategory.Master)
                return MM.GetVolume(spkrVolumeControl, (int)VolumeCategory.Master);
            else if(Category  == VolumeCategory.Line)
                return MM.GetVolume(lineVolumeControl, (int)VolumeCategory.Line);
            else return -1;
        }
        public float GetVolumeScala(VolumeCategory Category)
        {
            if (Category == VolumeCategory.Master)
                return MM.GetVolume(spkrVolumeControl, (int)VolumeCategory.Master) / 65535.0f;
            else if (Category == VolumeCategory.Line)
                return MM.GetVolume(lineVolumeControl, (int)VolumeCategory.Line) / 65535.0f;
            else return float.NaN;
        }

        public bool GetMute(VolumeCategory Category)
        {
            if (Category == VolumeCategory.Master)
                return MM.GetVolume(spkrMuteControl, (int)VolumeCategory.Master) > 0;
            else if (Category == VolumeCategory.Line)
                return MM.GetVolume(lineMuteControl, (int)VolumeCategory.Line)>0;
            else return false;
        }
        public bool GetMute(VolumeCategory Category, out int MuteValue)
        {
            if (Category == VolumeCategory.Master){MuteValue = MM.GetVolume(spkrMuteControl, (int)VolumeCategory.Master);return MuteValue > 0;}
            else if (Category == VolumeCategory.Line){MuteValue = MM.GetVolume(lineMuteControl, (int)VolumeCategory.Line);return MuteValue > 0;}
            else {MuteValue=-1;return false;}
        }

        internal protected bool SetVolume(VolumeCategory Category, int Volume, bool ThrowException = true)
        {
            if(Volume>65535)
                if (ThrowException) throw new ArgumentOutOfRangeException("음량은 65535보다 클 수 없습니다.");else Volume = 65535;
            else if (Volume < 1)
                if (ThrowException) throw new ArgumentOutOfRangeException("음량은 0보다 작을 수 없습니다.");else Volume = 0;
            else
            {
                if (Category == VolumeCategory.Master){try{MM.SetVolume(spkrVolumeControl, (int)VolumeCategory.Master, Volume);return true;}catch{}}
                else if (Category == VolumeCategory.Line){try{MM.SetVolume(lineVolumeControl, (int)VolumeCategory.Line, Volume);return true;}catch{}}
            }
            return false;
        }
        public bool SetVolume(VolumeCategory Category, ushort Volume, bool ThrowException = true){return SetVolume(Category, (int)Volume, false);}
        public bool SetVolumeScala(VolumeCategory Category, float Volume, bool ThrowException = true){return SetVolume(Category, (int)(Volume*65535), false);}
        public bool SetMute(VolumeCategory Category, bool Mute, bool ThrowException = true)
        {
            if (Category == VolumeCategory.Master){try{MM.SetVolume(spkrMuteControl, (int)VolumeCategory.Master, Mute?1:0);return true;}catch(Exception ex){if(ThrowException)throw ex;}}
            else if (Category == VolumeCategory.Line){try{MM.SetVolume(lineMuteControl, (int)VolumeCategory.Line, Mute?1:0);return true;}catch(Exception ex){if(ThrowException)throw ex;}}
            else if(ThrowException)throw new ArgumentException("볼륨 종류가 올바르지 않습니다.");
            return false;
        }

        public ushort Volume {get{return (ushort)GetVolume(Category);}set{SetVolume(Category, value);}}
        public float VolumeScala{get{return GetVolumeScala(Category);}set{Volume = (ushort)(value*65536);}}

        public bool Mute {get{return GetMute(Category);}set{SetMute(Category, value);}}

        public void Dispose()
        {
            base.ReleaseHandle();
        }
    }
}
