﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Windows.Forms;

//원본 출처: http://stackoverflow.com/questions/2450373/set-global-hotkeys-using-c-sharp
//수정: 박홍식
namespace HS_Audio.LIBRARY.Native
{
    public delegate void KeyPressedEventHandler(object sender, KeyPressedEventArgs e);
    public sealed class KeyboardHookEx : IDisposable
    {
        // Registers a hot key with Windows.
        [DllImport("user32.dll")]
        private static extern bool RegisterHotKey(IntPtr hWnd, int id, uint fsModifiers, uint vk);
        // Unregisters the hot key with Windows.
        [DllImport("user32.dll")]
        private static extern bool UnregisterHotKey(IntPtr hWnd, int id);

        private Window _window;
        private int _currentId;
        
        List<KeyboardHookClass> keys = new List<KeyboardHookClass>();
        public KeyboardHookEx()
        {
            _window = new Window(this);
            // register the event of the inner native window.
            /*
            _window.KeyPressed += delegate(object sender, KeyPressedEventArgs args)
            {
                if (KeyPressed != null)
                    KeyPressed(this, args);
            };*/
        }
        /// <summary>
        /// Registers a hot key in the system.
        /// </summary>
        /// <param name="key">The key itself that is associated with the hot key.</param>
        public void RegisterHotKey(Keys key)
        {
            keys.Add(new KeyboardHookClass(ModifierKeys.None,key));
            // increment the counter.
            _currentId = _currentId + 1;

            // register the hot key.
            if (!RegisterHotKey(_window.Handle, _currentId, (uint)0, (uint)key))
                throw new InvalidOperationException("글로벌 단축키(핫키)를 등록 할 수 없습니다");
        }
        /// <summary>
        /// Registers a hot key in the system.
        /// </summary>
        /// <param name="modifier">The modifiers that are associated with the hot key.</param>
        /// <param name="key">The key itself that is associated with the hot key.</param>
        public void RegisterHotKey(ModifierKeys modifier, Keys key)
        {
            keys.Add(new KeyboardHookClass(modifier,key));
            // increment the counter.
            _currentId = _currentId + 1;

            // register the hot key.
            if (!RegisterHotKey(_window.Handle, _currentId, (uint)modifier, (uint)key))
                throw new InvalidOperationException("글로벌 단축키(핫키)를 등록 할 수 없습니다");
        }
        public void RegisterHotKey(KeyboardHookClass Key)
        {
            keys.Add(Key);
            // increment the counter.
            _currentId = _currentId + 1;

            // register the hot key.
            if (!RegisterHotKey(_window.Handle, _currentId, (uint)Key.KeyEx, (uint)Key.Key))
                throw new InvalidOperationException("글로벌 단축키(핫키)를 등록 할 수 없습니다");
        }

        public void UnHook()
        {
            UnregisterHotKey(_window.Handle, _currentId);
        }

        /// <summary>
        /// A hot key has been pressed.
        /// </summary>
        public event KeyPressedEventHandler KeyPressed;

        #region IDisposable Members

        public void Dispose()
        {
            // unregister all the registered hot keys.
            for (int i = _currentId; i > 0; i--)
            {
                UnregisterHotKey(_window.Handle, i);
            }

            // dispose the inner native window.
            _window.Dispose();
        }

        #endregion

        /// <summary>
        /// Represents the window that is used internally to get the messages.
        /// </summary>
        private class Window : NativeWindow, IDisposable
        {
            private static int WM_HOTKEY = 0x0312;

            KeyboardHookEx kh;

            public Window()
            {
                // create the handle for the window.
                this.CreateHandle(new CreateParams());
            }
            public Window(KeyboardHookEx kh)
            {
                // create the handle for the window.
                this.CreateHandle(new CreateParams());
                this.kh = kh;
            }

            /// <summary>
            /// Overridden to get the notifications.
            /// </summary>
            /// <param name="m"></param>
            protected override void WndProc(ref Message m)
            {
                base.WndProc(ref m);

                // check if we got a hot key pressed.
                if (m.Msg == WM_HOTKEY)
                {
                    // get the keys.
                    Keys key = (Keys)(((int)m.LParam >> 16) & 0xFFFF);
                    ModifierKeys modifier = (ModifierKeys)((int)m.LParam & 0xFFFF);

                    // invoke the event to notify the parent.
                    if (KeyPressed != null){
                        KeyPressed(this, new KeyPressedEventArgs(modifier, key, (int)m.LParam));}
                    if (kh.KeyPressed != null) kh.KeyPressed(this, new KeyPressedEventArgs(modifier, key, (int)m.LParam));
                }
            }

            public event KeyPressedEventHandler KeyPressed;

            #region IDisposable Members

            public void Dispose()
            {
                this.DestroyHandle();
            }

            #endregion
        }
    }

    /// <summary>
    /// Event Args for the event that is fired after the hot key has been pressed.
    /// </summary>KeyPressedEventArgs
    public class KeyPressedEventArgs
    {
        private ModifierKeys _modifier;
        private Keys _key;
        private int _OriginalKey;

        internal KeyPressedEventArgs(ModifierKeys modifier, Keys key, int OriginalKey)
        {
            _modifier = modifier;
            _key = key;
            _OriginalKey=OriginalKey;
        }

        public ModifierKeys Modifier
        {
            get
            {
                return _modifier;
            }
        }

        public Keys Key
        {
            get
            {
                return _key;
            }
        }

        public int OriginalKey
        {
            get
            {
                return _OriginalKey;
            }
        }
    }

    /// <summary>
    /// The enumeration of possible modifiers.
    /// </summary>
    [Flags]
    public enum ModifierKeys : uint
    {
        None=0,
        NOREPEAT=0x4000,
        Alt = 1,
        Control = 2,
        Shift = 4,
        Win = 8
    }

    public class KeyboardHookClass
    {
        public KeyboardHookClass(){}
        public KeyboardHookClass(ModifierKeys KeyEx,Keys Key){this.Key = Key;this.KeyEx=KeyEx;}
        public Keys Key;
        public ModifierKeys KeyEx;
    }
}
