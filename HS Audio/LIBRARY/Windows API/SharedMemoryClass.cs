//===  Easy SharedMemory Class   spacebar@paran.com 
using System;
using System.Collections.Generic;
using System.Text;
 
using System.Runtime.InteropServices;
using System.Collections;
using System.Windows.Forms;
using System.Security.Permissions;


namespace HS_Audio.LIBRARY.Native
{
    public class SharedMemory
    {
        //    [DllImport("Kernel32.dll", EntryPoint = "CreateFileMapping", SetLastError = true, CharSet = CharSet.Unicode)]
        //    internal static extern IntPtr CreateFileMapping(uint hFile,
        //     SecurityAttributes lpAttributes, uint flProtect,
        //      uint dwMaximumSizeHigh, uint dwMaximumSizeLow, string lpName);
        #region Enums

        /// <summary>
        /// Way of accessing the physical memory.
        /// </summary>
        [Flags]
        protected enum ProtectionTypes : uint
        {
            /// <summary>
            /// Only read the memory.
            /// </summary>
            PageReadOnly = 0x2,
            /// <summary>
            /// Only write to memory.
            /// </summary>
            PageReadWrite = 0x4,
            /// <summary>
            /// Read and write to memory.
            /// </summary>
            PageWriteCopy = 0x8
        }

        /// <summary>
        /// Way of physical memory management.
        /// </summary>
        [Flags]
        public enum SectionTypes : uint
        {
            /// <summary>
            /// Do nothing with the memory.
            /// </summary>
            SecNone = 0,
            /// <summary>
            /// Commit all requested physical memory immediatelly after create.
            /// </summary>
            SecCommit = 0x8000000,
            /// <summary>
            /// Memory is a view of a EXE/DLL file.
            /// </summary>
            SecImage = 0x1000000,
            /// <summary>
            /// Don't cache the memory reads/writes.
            /// </summary>
            SecNoCache = 0x10000000,
            /// <summary>
            /// Reserved.
            /// </summary>
            SecReserve = 0x4000000
        }

        /// <summary>
        /// Way of accessing the file if mapped.
        /// </summary>
        [Flags]
        public enum AccessTypes : uint
        {
            /// <summary>
            /// Copy physical memory pages per process when writes performed.
            /// </summary>
            Copy = 0x1,
            /// <summary>
            /// Allow only read.
            /// </summary>
            ReadOnly = 0x4,
            /// <summary>
            /// Allow read and write operations.
            /// </summary>
            ReadWrite = ReadOnly | 0x2,
            /// <summary>
            /// Allow full control.
            /// </summary>
            Full = 0xF001F
        }

        #endregion

        #region Imports

        [DllImport("kernel32", EntryPoint = "CreateFileMappingW", CharSet = CharSet.Unicode, SetLastError = true)]
        private static extern IntPtr CreateFileMapping(IntPtr hFile, uint lpSecurityAttributes, uint flProtect, uint dwMaximumSizeHigh, uint dwMaximumSizeLow, string lpName);

        [DllImport("kernel32", EntryPoint = "OpenFileMappingW", CharSet = CharSet.Unicode, SetLastError = true)]
        private static extern IntPtr OpenFileMapping(AccessTypes dwDesiredAccess, bool bInheritHandle, string lpName);

        [DllImport("kernel32", SetLastError = true)]
        private static extern bool UnmapViewOfFile(IntPtr lpBaseAddress);

        [DllImport("kernel32", SetLastError = true)]
        protected static extern void CloseHandle(IntPtr handle);

        [DllImport("kernel32", SetLastError = true)]
        private static extern IntPtr MapViewOfFile(IntPtr hFileMappingObject, AccessTypes dwDesiredAccess, uint dwFileOffsetHigh, uint dwFileOffsetLow, uint dwNumberOfBytesToMap);

        /// <summary>
        /// Invalid handle value for Win32 objects.
        /// </summary>
        protected static readonly IntPtr InvalidHandleValue = new IntPtr(-1);

        #endregion

        private IntPtr hMappedFile = IntPtr.Zero;
        private IntPtr lpMemoryAddress = IntPtr.Zero;
        private uint memorySize;
        private ulong memoryOffset;

        /// <summary>
        /// Default constructor.
        /// </summary>
        public SharedMemory()
        {
        }

        /// <summary>
        /// Init constructor.
        /// </summary>
        public SharedMemory(uint size, string name, ulong offset, SectionTypes section, AccessTypes access)
        {
            Create(size, name, offset, section, access);
        }

        /// <summary>
        /// Creates named memory mapping object that then can be opened by the same call from another process.
        /// </summary>
        public void Create(uint size, string name, ulong offset, SectionTypes section, AccessTypes access)
        {
            hMappedFile = Create(InvalidHandleValue, ref size, ref offset, name, section, access);

            // in case of error try to open an existing shared memory object:
            if (hMappedFile == IntPtr.Zero && Marshal.GetLastWin32Error() != 0)
                hMappedFile = OpenFileMapping(access, false, name);

            if (hMappedFile != IntPtr.Zero)
                lpMemoryAddress = MapViewOfFile(hMappedFile, access, (uint)(offset >> 32) & 0xFFFFFFFF,
                                                (uint)(offset & 0xFFFFFFFF), size);

            if (lpMemoryAddress != IntPtr.Zero)
            {
                memoryOffset = offset;
                memorySize = size;
                OnCreateMapping(true);
            }
        }

        public void Create(uint size, string name)
        {
            Create(size, name, 0, SharedMemory.SectionTypes.SecNone, SharedMemory.AccessTypes.ReadWrite);
        }
        /// <summary>
        /// Creates named memory object with given properties.
        /// </summary>
        protected virtual IntPtr Create(IntPtr handle, ref uint size, ref ulong offset, string name, SectionTypes section, AccessTypes access)
        {
            ProtectionTypes protection;

            switch (access)
            {
                case AccessTypes.Copy:
                    protection = ProtectionTypes.PageWriteCopy;
                    break;
                case AccessTypes.ReadOnly:
                    protection = ProtectionTypes.PageReadOnly;
                    break;

                default:
                    protection = ProtectionTypes.PageReadWrite;
                    break;
            }

            return CreateFileMapping(handle, 0, (uint)protection | (uint)section, 0, size, name);
        }

        #region Properties

        /// <summary>
        /// Gets the handle to the mapped-memory object.
        /// </summary>
        public IntPtr Handle
        {
            get { return hMappedFile; }
        }

        /// <summary>
        /// Gets the native address of the mapped-memory.
        /// </summary>
        public IntPtr Address
        {
            get { return lpMemoryAddress; }
        }

        /// <summary>
        /// Gets the size of mapped memory block.
        /// </summary>
        public uint Size
        {
            get { return memorySize; }
        }

        /// <summary>
        /// Gets the offset of mapped memory block.
        /// </summary>
        public ulong Offset
        {
            get { return memoryOffset; }
        }

        #endregion

        #region Open / Close

        /// <summary>
        /// Opens specified memory mapped object for reading only.
        /// </summary>
        public void Open(uint size, string name)
        {
            Open(size, name, 0, AccessTypes.ReadOnly);
        }

        /// <summary>
        /// Opens specified memory mapped object.
        /// </summary>
        public void Open(uint size, string name, AccessTypes access)
        {
            Open(size, name, 0, access);
        }

        /// <summary>
        /// Opens specified memory mapped object.
        /// </summary>
        public void Open(uint size, string name, ulong offset)
        {
            Open(size, name, offset, AccessTypes.ReadOnly);
        }

        /// <summary>
        /// Opens specified memory mapped object.
        /// </summary>
        public void Open(uint size, string name, ulong offset, AccessTypes access)
        {
            hMappedFile = OpenFileMapping(access, false, name);

            if (hMappedFile != IntPtr.Zero)
                lpMemoryAddress = MapViewOfFile(hMappedFile, access, (uint)(offset >> 32) & 0xFFFFFFFF,
                                                (uint)(offset & 0xFFFFFFFF), size);

            if (lpMemoryAddress != IntPtr.Zero)
            {
                memoryOffset = offset;
                memorySize = size;
                OnCreateMapping(false);
            }
        }

        // compile with: /unsafe

        public byte ReadByte(int offset)
        {
            return Marshal.ReadByte(this.Address, offset);
        }
        public void WriteByte(byte data, int offset)
        {
            Marshal.WriteByte(this.Address, offset, data);
        }

        public int ReadInt32(int offset)
        {
            return Marshal.ReadInt32(this.Address, offset);
        }
        public void WriteInt32(int data, int offset)
        {
            Marshal.WriteInt32(this.Address, offset, data);
        }
        public void ReadBytes(byte[] data, int offset)
        {
            for (int i = 0; i < data.Length; i++)
                data[i] = Marshal.ReadByte(this.Address, offset + i);
        }


        public void WriteBytes(byte[] data, int offset)
        {
            for (int i = 0; i < data.Length; i++)
                Marshal.WriteByte(this.Address, offset + i, data[i]);
        }


        public object ReadDeserialize(int offset, int length)
        {
            byte[] binaryData = new byte[length];
            ReadBytes(binaryData, offset);
            System.Runtime.Serialization.Formatters.Binary.BinaryFormatter formatter
              = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
            System.IO.MemoryStream ms = new System.IO.MemoryStream(
             binaryData, 0, length, true, true);
            object data = formatter.Deserialize(ms);
            ms.Close();
            return data;
        }
        public void WriteSerialize(object data, int offset, int length)
        {
            System.Runtime.Serialization.Formatters.Binary.BinaryFormatter formatter
              = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
            byte[] binaryData = new byte[length];
            System.IO.MemoryStream ms = new System.IO.MemoryStream(
             binaryData, 0, length, true, true);
            formatter.Serialize(ms, data);
            ms.Flush();
            ms.Close();
            WriteBytes(binaryData, offset);
        }

        /// <summary>
        /// 바이트를 초기화 합니다. (새로 공유하컖E싶으시툈E이 함수를 호출해주세퓖E
        /// </summary>
        public void Clear()
        {
            //byte[] ZeroByte = new byte[this.Size];
            Marshal.Copy(new byte[this.Size], 0, this.Address, (int)this.Size);
        }

        public bool ReadByte(ref byte[] rets)
        {
            try
            {
                Byte[] bytes = new Byte[this.Size];

                Marshal.Copy(this.Address, bytes, 0, bytes.Length);
                rets = bytes;
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool WriteByte(byte[] rets)
        {
            try
            {
                Marshal.Copy(rets, 0, this.Address, rets.Length);
                return true;
            }
            catch{return false;}
        }

        public bool ReadString(ref string rets)
        {
           // string st = ""; int i = 0; int savesz = 0;
            try
            {
                Byte[] bytes = new Byte[this.Size];

                Marshal.Copy(this.Address, bytes, 0, bytes.Length);
                /*
                for (i = 1; i <= this.Size; i++)
                {
                    if (bytes[i - 1] == 0) break;
                    st = st +(char) bytes[i - 1];
                    savesz++;
                }
                if (savesz > 0)
                */
                if(bytes[0] != 0)rets = Encoding.UTF8.GetString(bytes);// st.Substring(0, savesz);
                // Console.WriteLine(rets);
                //rets = st;
                return true;
            }
            catch{return false;}
        }
        public bool ReadString(ref string rets, Encoding StringEncoding)
        {
            //string st = ""; int i = 0; int savesz = 0;
            try
            {
                Byte[] bytes = new Byte[this.Size];

                Marshal.Copy(this.Address, bytes, 0, bytes.Length);
                /*
                for (i = 1; i <= this.Size; i++)
                {
                    if (bytes[i - 1] == 0) break;
                    st = st +(char) bytes[i - 1];
                    savesz++;
                }
                if (savesz > 0)
                */

                rets = StringEncoding.GetString(bytes);// st.Substring(0, savesz);
                // Console.WriteLine(rets);
                //rets = st;
                return true;
            }
            catch{return false;}
        }

        public bool WriteString(string Wstr)
        {
            //int j=0;
            try
            {

                //Byte[] bytes = new Byte[Wstr.Length+1];
                Byte[] bytes = Encoding.UTF8.GetBytes(Wstr);
                /*
                for (j = 0; j < Wstr.Length; j++)
                {
                    bytes[j] = (byte)Wstr[j];
                }
                bytes[j] = (byte)0;*/
                Marshal.Copy(bytes, 0, this.Address, bytes.Length);
            }
            catch {return false; }
            return true;
        }
        /// <summary>
        /// 문자열을 씁니다.
        /// </summary>
        /// <param name="Wstr">문자열 입니다.</param>
        /// <param name="StringEncoding">한국엉踏 하과目다툈ESystem.Text.Encoding.GetEncoding("ecu-kr")를 넘겨주세퓖E</param>
        /// <returns></returns>
        public bool WriteString(string Wstr, Encoding StringEncoding)
        {
            //int j = 0;
            try
            {
                //Byte[] bytes = new Byte[Wstr.Length+1];
                Byte[] bytes = StringEncoding.GetBytes(Wstr);
                /*
                for (j = 0; j < Wstr.Length; j++)
                {
                    bytes[j] = (byte)Wstr[j];
                }
                bytes[j] = (byte)0;*/
                Marshal.Copy(bytes, 0, this.Address, bytes.Length);
            }
            catch{return false;}

            return true;
        }
        /// <summary>
        /// Release resources.
        /// </summary>
        public virtual void Close()
        {
            if (lpMemoryAddress != IntPtr.Zero)
            {
                UnmapViewOfFile(lpMemoryAddress);
                lpMemoryAddress = IntPtr.Zero;
            }

            if (hMappedFile != IntPtr.Zero)
            {
                CloseHandle(hMappedFile);
                hMappedFile = IntPtr.Zero;
                memorySize = 0;
                memoryOffset = 0;
            }
        }

        #endregion

        #region IDisposable Members

        /// <summary>
        /// Release internal resources.
        /// </summary>
        public void Dispose()
        {
            Close();
        }

        #endregion

        #region Virtual Members

        /// <summary>
        /// Method invoked when new mapping object is created or opened.
        /// </summary>
        protected virtual void OnCreateMapping(bool isCreated)
        {
        }

        #endregion


    }
}
