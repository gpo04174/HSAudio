﻿using System; 
using System.IO; 
using System.Collections; 
using System.Runtime.Serialization.Formatters.Binary; 
using System.Runtime.Serialization; 
using System.Runtime.InteropServices; 
using System.Security.Permissions; 
using System.Threading;
using System.Collections.Generic;

namespace HS_Audio.LIBRARY.Native
{
    /*
    public class SharedMemory
    {
        //주소: http://www.cyworld.com/SJoonho/3544939
        [assembly: SecurityPermission(SecurityAction.RequestMinimum, Execution = true)] // This class includes several Win32 interop definitions. internal class Win32 { 
        public static readonly IntPtr InvalidHandleValue = new IntPtr(-1); public const UInt32 FILE_MAP_WRITE = 2; public const UInt32 PAGE_READWRITE = 0x04;

        [System.Runtime.InteropServices.DllImport("Kernel32")]
        public static extern IntPtr CreateFileMapping(IntPtr hFile,
            IntPtr pAttributes, UInt32 flProtect, UInt32 dwMaximumSizeHigh, UInt32 dwMaximumSizeLow, String pName);

        [System.Runtime.InteropServices.DllImport("Kernel32")]
        public static extern IntPtr OpenFileMapping(UInt32 dwDesiredAccess,
            Boolean bInheritHandle, String name);

        [System.Runtime.InteropServices.DllImport("Kernel32")]
        public static extern Boolean CloseHandle(IntPtr handle);

        [System.Runtime.InteropServices.DllImport("Kernel32")]
        public static extern IntPtr MapViewOfFile(IntPtr hFileMappingObject,
            UInt32 dwDesiredAccess, UInt32 dwFileOffsetHigh, UInt32 dwFileOffsetLow, IntPtr dwNumberOfBytesToMap);

        [System.Runtime.InteropServices.DllImport("Kernel32")]
        public static extern Boolean UnmapViewOfFile(IntPtr address);

        [System.Runtime.InteropServices.DllImport("Kernel32")]
        public static extern Boolean DuplicateHandle(IntPtr hSourceProcessHandle,
            IntPtr hSourceHandle, IntPtr hTargetProcessHandle, ref IntPtr lpTargetHandle, UInt32 dwDesiredAccess, Boolean bInheritHandle, UInt32 dwOptions);
        public const UInt32 DUPLICATE_CLOSE_SOURCE = 0x00000001; public const UInt32 DUPLICATE_SAME_ACCESS = 0x00000002;

        [System.Runtime.InteropServices.DllImport("Kernel32")]
        public static extern IntPtr GetCurrentProcess();

        // This class wraps memory that can be simultaneously // shared by multiple AppDomains and Processes. Serializable public sealed class SharedMemory : ISerializable, IDisposable { 
        // The handle and string that identify // the Windows file-mapping object. private IntPtr m_hFileMap = IntPtr.Zero; private String m_name; 
        // The address of the memory-mapped file-mapping object. private IntPtr m_address; 
        // The constructors. public SharedMemory(Int32 size) : this(size, null) { } 
        public unsafe Byte* Address { get { return (Byte*)m_address; } }
        public unsafe IntPtr Address_Handle { get { return m_address; } }
        public void SharedMemory(Int32 size, String name)
        {
            m_hFileMap = Win32.CreateFileMapping(Win32.InvalidHandleValue, IntPtr.Zero, Win32.PAGE_READWRITE, 0, unchecked((UInt32)size), name);
            if (m_hFileMap == IntPtr.Zero)
            {
                throw new Exception("Could not create memory-mapped file.");
                m_name = name; m_address = Win32.MapViewOfFile(m_hFileMap, Win32.FILE_MAP_WRITE, 0, 0, IntPtr.Zero);
            }
        }
        // The cleanup methods. public void Dispose() { 
        //GC.SuppressFinalize(this); Dispose(true); 
        public void Dispose(Boolean disposing) 
        {
         Win32.UnmapViewOfFile(m_address); 
         Win32.CloseHandle(m_hFileMap); 
         m_address = IntPtr.Zero; 
         m_hFileMap = IntPtr.Zero; 
       

       //SendMessage public bool SendMessage(string Amsg) { 
       Byte[] bytes = new ByteAmsg.Length[1]; 
       for (int j = 0; j < bytes.Length; j++)
       {
       bytes[j] = (byte)Amsg[j];
       }
            
       }
        
        ~SharedMemory() { Dispose(false); } 
    }
}
     
    namespace Memory1
    {
        public class MemoryMappedFile : IDisposable
        {

            private IntPtr fileMapping;
            private readonly int size;
            private readonly FileAccess access;

            public enum FileAccess : int
            {
                ReadOnly = 2,
                ReadWrite = 4
            }

            private MemoryMappedFile(IntPtr fileMapping, int size, FileAccess access)
            {
                this.fileMapping = fileMapping;
                this.size = size;
                this.access = access;
            }

            public static MemoryMappedFile CreateFile(string name, FileAccess access, int size)
            {
                if (size < 0)
                    throw new ArgumentException("Size must not be negative", "size");

                IntPtr fileMapping = sharedMemoryController.CreateFileMapping(0xFFFFFFFFu, null, (uint)access, 0, (uint)size, name);
                if (fileMapping == IntPtr.Zero)
                    throw new MemoryMappingFailedException();

                return new MemoryMappedFile(fileMapping, size, access);
            }

            public MemoryMappedFileView CreateView(int offset, int size, MemoryMappedFileView.ViewAccess access)
            {
                if (this.access == FileAccess.ReadOnly && access == MemoryMappedFileView.ViewAccess.ReadWrite)
                    throw new ArgumentException("Only read access to views allowed on files without write access", "access");
                if (offset < 0)
                    throw new ArgumentException("Offset must not be negative", "size");
                if (size < 0)
                    throw new ArgumentException("Size must not be negative", "size");
                IntPtr mappedView = sharedMemoryController.MapViewOfFile(fileMapping, (uint)access, 0, (uint)offset, (uint)size);
                return new MemoryMappedFileView(mappedView, size, access);
            }

            #region IDisposable Member

            public void Dispose()
            {
                if (fileMapping != IntPtr.Zero)
                {
                    if (sharedMemoryController.CloseHandle((uint)fileMapping))
                        fileMapping = IntPtr.Zero;
                }
            }

            #endregion
        }
        //3. MemoryMappedFileView class
        public class MemoryMappedFileView : IDisposable
        {
            private IntPtr mappedView;
            private readonly int size;
            private readonly ViewAccess access;

            public enum ViewAccess : int
            {
                ReadWrite = 2,
                Read = 4,
            }

            internal MemoryMappedFileView(IntPtr mappedView, int size, ViewAccess access)
            {
                this.mappedView = mappedView;
                this.size = size;
                this.access = access;
            }

            public int Size
            {
                get { return size; }
            }

            public void ReadBytes(byte[] data)
            {
                ReadBytes(data, 0);
            }
            public void ReadBytes(byte[] data, int offset)
            {
                for (int i = 0; i < data.Length; i++)
                    data[i] = Marshal.ReadByte(mappedView, offset + i);
            }

            public void WriteBytes(byte[] data)
            {
                WriteBytes(data, 0);
            }
            public void WriteBytes(byte[] data, int offset)
            {
                for (int i = 0; i < data.Length; i++)
                    Marshal.WriteByte(mappedView, offset + i, data[i]);
            }

            #region Additional Accessors
            public byte ReadByte(int offset)
            {
                return Marshal.ReadByte(mappedView, offset);
            }
            public void WriteByte(byte data, int offset)
            {
                Marshal.WriteByte(mappedView, offset, data);
            }

            public short ReadInt16(int offset)
            {
                return Marshal.ReadInt16(mappedView, offset);
            }
            public void WriteInt16(short data, int offset)
            {
                Marshal.WriteInt16(mappedView, offset, data);
            }

            public int ReadInt32(int offset)
            {
                return Marshal.ReadInt32(mappedView, offset);
            }
            public void WriteInt32(int data, int offset)
            {
                Marshal.WriteInt32(mappedView, offset, data);
            }

            public long ReadInt64(int offset)
            {
                return Marshal.ReadInt64(mappedView, offset);
            }
            public void WriteInt64(long data, int offset)
            {
                Marshal.WriteInt64(mappedView, offset, data);
            }

            public object ReadStructure(Type structureType)
            {
                return Marshal.PtrToStructure(mappedView, structureType);
            }
            public void WriteStructure(object data)
            {
                Marshal.StructureToPtr(data, mappedView, true);
            }

            public object ReadDeserialize()
            {
                return ReadDeserialize(0, size);
            }
            public object ReadDeserialize(int offset)
            {
                return ReadDeserialize(offset, size - offset);
            }
            public object ReadDeserialize(int offset, int length)
            {
                byte[] binaryData = new byte[length];
                ReadBytes(binaryData, offset);
                System.Runtime.Serialization.Formatters.Binary.BinaryFormatter formatter
               = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
                System.IO.MemoryStream ms = new System.IO.MemoryStream(binaryData, 0, length, true, true);
                object data = formatter.Deserialize(ms);
                ms.Close();
                return data;
            }



            public void WriteSerialize(object data)
            {
                WriteSerialize(data, 0, size);
            }


            public void WriteSerialize(object data, int offset)
            {
                WriteSerialize(data, 0, size - offset);
            }


            public void WriteSerialize(object data, int offset, int length)
            {
                System.Runtime.Serialization.Formatters.Binary.BinaryFormatter formatter
                 = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
                byte[] binaryData = new byte[length];
                System.IO.MemoryStream ms = new System.IO.MemoryStream(binaryData, 0, length, true, true);
                formatter.Serialize(ms, data);
                ms.Flush();
                ms.Close();
                WriteBytes(binaryData, offset);
            }
            #endregion

            #region IDisposable Member

            public void Dispose()
            {
                if (mappedView != IntPtr.Zero)
                {
                    if (sharedMemoryController.UnmapViewOfFile(mappedView))
                        mappedView = IntPtr.Zero;
                }
            }

            #endregion
        }
        //4. MemoryMappingFailedException class
        [Serializable]
        public class MemoryMappingFailedException : Exception
        {
            public MemoryMappingFailedException()
                : base()
            {
            }
        }
    }
    public class SharedMemory1
    {
                private IntPtr MemPointer = new IntPtr();
                private IntPtr MappedMem = new IntPtr();
                public void SharedMemIO(string MemName , bool OpenMem , int Size)
                {
                        if(OpenMem)
                        {
                                MemPointer = OpenFileMapping((int)0x001f,false,MemName);
                                if(MemPointer == IntPtr.Zero)
                                throw new Exception("OpenFileMapping Error...");
                        }
                        else
                        {
                                MemPointer = CreateFileMapping(IntPtr.Zero,IntPtr.Zero,0x04,0,Size,MemName);
                                if(MemPointer == IntPtr.Zero)
                                throw new Exception("CreateFileMapping Error...");
                        }
                        MappedMem = MapViewOfFile(MemPointer,(int)0x001f,0,0,IntPtr.Zero);
                        if(MappedMem == IntPtr.Zero)
                        throw new Exception("MapViewOfFile Error...");
                }
 
                public void WriteString(string input)
                {
                        byte[] bytestowrite = System.Text.Encoding.UTF8.GetBytes(input);
                        byte[] Size = BitConverter.GetBytes(bytestowrite.Length);
                        List<byte> CC = new List<byte>();
                        CC.AddRange(Size);
                        CC.AddRange(bytestowrite);
                        bytestowrite = CC.ToArray();
                        Marshal.Copy(bytestowrite,0,MappedMem,bytestowrite.Length );
                }
                public string ReadString()
                {
                        byte[] Size = new byte[4];
                        Marshal.Copy(MappedMem,Size,0,4);
                        byte[] TT = new byte[BitConverter.ToInt32(Size,0) + 4];
                        Marshal.Copy(MappedMem,TT,0,TT.Length);
                        byte[] Done = new byte[TT.Length - 4];
                        Buffer.BlockCopy(TT,4,Done,0,Done.Length);
                        return System.Text.Encoding.UTF8.GetString(Done);
 
                }

 
                public void CloseSharedMemory()
                {
                        UnmapViewOfFile(MappedMem);
                        CloseHandle(MemPointer);
                }

 
                [DllImport("kernel32", SetLastError = true, CharSet = CharSet.Unicode)]
                public static extern IntPtr CreateFileMapping(IntPtr hFile, IntPtr lpAttributes, int flProtect, int dwMaximumSizeLow, int dwMaximumSizeHigh, string lpName);

                [DllImport("kernel32", SetLastError = true, CharSet = CharSet.Ansi)]
                public static extern IntPtr MapViewOfFile(IntPtr hFileMappingObject, int dwDesiredAccess, int dwFileOffsetHigh, int dwFileOffsetLow, IntPtr dwNumBytesToMap);

                [DllImport("kernel32", SetLastError = true, CharSet = CharSet.Unicode)]
                public static extern IntPtr OpenFileMapping(int dwDesiredAccess, [MarshalAs(UnmanagedType.Bool)] bool bInheritHandle, string lpName);

 
                [DllImport("kernel32", SetLastError = true, CharSet = CharSet.Ansi)]
                [return: MarshalAs(UnmanagedType.Bool)]
                public static extern bool UnmapViewOfFile(IntPtr lpBaseAddress);
 
                [DllImport("kernel32", SetLastError = true, CharSet = CharSet.Ansi)]
                [return: MarshalAs(UnmanagedType.Bool)]
                public static extern bool CloseHandle(IntPtr handle);
    }
}
     * /
/*
namespace System.ServiceModel.Activation
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.InteropServices;
    using System.Security.Principal;
    using System.ServiceModel.hannels;
    using System.Threading;
    using System.ServiceModel;
    using System.ComponentModel;

    unsafe class SharedMemory : IDisposable
    {
        SafeFileMappingHandle fileMapping;

        SharedMemory(SafeFileMappingHandle fileMapping)
        {
            this.fileMapping = fileMapping;
        }

        public static unsafe SharedMemory Create(string name, Guid content, List<securityidentifier> allowedSids)
        {
            int errorCode = UnsafeNativeMethods.ERROR_SUCCESS;
            byte[] binarySecurityDescriptor = SecurityDescriptorHelper.FromSecurityIdentifiers(allowedSids, UnsafeNativeMethods.GENERIC_READ);
            SafeFileMappingHandle fileMapping;
            UnsafeNativeMethods.SECURITY_ATTRIBUTES securityAttributes = new UnsafeNativeMethods.SECURITY_ATTRIBUTES();
            fixed (byte* pinnedSecurityDescriptor = binarySecurityDescriptor)
            {
                securityAttributes.lpSecurityDescriptor = (IntPtr)pinnedSecurityDescriptor;
                fileMapping = UnsafeNativeMethods.CreateFileMapping((IntPtr)(-1), securityAttributes, UnsafeNativeMethods.PAGE_READWRITE, 0, sizeof(SharedMemoryContents), name);
                errorCode = Marshal.GetLastWin32Error();
            }

            if (fileMapping.IsInvalid)
            {
                fileMapping.SetHandleAsInvalid();
                fileMapping.Close();
                throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(new Win32Exception(errorCode));
            }

            SharedMemory sharedMemory = new SharedMemory(fileMapping);
            SafeViewOfFileHandle view;

            // Ignore return value.
            GetView(fileMapping, true, out view);

            try
            {
                SharedMemoryContents* contents = (SharedMemoryContents*)view.DangerousGetHandle();
                contents->pipeGuid = content;
                Thread.MemoryBarrier();
                contents->isInitialized = true;
                return sharedMemory;
            }
            finally
            {
                view.Close();
            }
        }

        public void Dispose()
        {
            if (fileMapping != null)
            {
                fileMapping.Close();
                fileMapping = null;
            }
        }

        static bool GetView(SafeFileMappingHandle fileMapping, bool writable, out SafeViewOfFileHandle handle)
        {
            handle = UnsafeNativeMethods.MapViewOfFile(fileMapping, writable ? UnsafeNativeMethods.FILE_MAP_WRITE : UnsafeNativeMethods.FILE_MAP_READ, 0, 0, (IntPtr)sizeof(SharedMemoryContents));
            int errorCode = Marshal.GetLastWin32Error();
            if (!handle.IsInvalid)
            {
                return true;
            }
            else
            {
                handle.SetHandleAsInvalid();
                fileMapping.Close();

                // Only return false when it's reading time.
                if (!writable && errorCode == UnsafeNativeMethods.ERROR_FILE_NOT_FOUND)
                {
                    return false;
                }

                throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(new Win32Exception(errorCode));
            }
        }

        public static string Read(string name)
        {
            string content;
            if (Read(name, out content))
            {
                return content;
            }

            throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(new Win32Exception(UnsafeNativeMethods.ERROR_FILE_NOT_FOUND));
        }

        public static bool Read(string name, out string content)
        {
            content = null;

            SafeFileMappingHandle fileMapping = UnsafeNativeMethods.OpenFileMapping(UnsafeNativeMethods.FILE_MAP_READ, false, ListenerConstants.GlobalPrefix + name);
            int errorCode = Marshal.GetLastWin32Error();
            if (fileMapping.IsInvalid)
            {
                fileMapping.SetHandleAsInvalid();
                fileMapping.Close();
                if (errorCode == UnsafeNativeMethods.ERROR_FILE_NOT_FOUND)
                {
                    return false;
                }

                throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(new Win32Exception(errorCode));
            }

            try
            {
                SafeViewOfFileHandle view;
                if (!GetView(fileMapping, false, out view))
                {
                    return false;
                }

                try
                {
                    SharedMemoryContents* contents = (SharedMemoryContents*)view.DangerousGetHandle();
                    content = contents->isInitialized ? contents->pipeGuid.ToString() : null;
                    return true;
                }
                finally
                {
                    view.Close();
                }
            }
            finally
            {
                fileMapping.Close();
            }
        }

        [StructLayout(LayoutKind.Sequential)]
        struct SharedMemoryContents
        {
            public bool isInitialized;
            public Guid pipeGuid;
        }
    }
     */
}

// File provided for Reference Use Only by Microsoft Corporation (c) 2007.
// Copyright (c) Microsoft Corporation. All rights reserved.
