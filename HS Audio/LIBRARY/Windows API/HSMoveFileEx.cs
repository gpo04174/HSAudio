﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Text;

namespace HS_Audio.LIBRARY.Windows_API
{
    public class HSMoveFileEx
    {
        [Flags]
        public enum MoveFileFlags
        {
            /// <summary>
            ///If the file is to be moved to a different volume, the function simulates the move by using the CopyFile and DeleteFile functions.
            ///If the file is successfully copied to a different volume and the original file is unable to be deleted, the function succeeds leaving the source file intact.
            ///This value cannot be used with MOVEFILE_DELAY_UNTIL_REBOOT.
            /// </summary>
            MOVEFILE_COPY_ALLOWED = 0x00000002,
            /// <summary>
            /// If a file named lpNewFileName exists, the function replaces its contents with the contents of the lpExistingFileName file, provided that security requirements regarding access control lists (ACLs) are met. For more information, see the Remarks section of this topic.
            /// This value cannot be used if lpNewFileName or lpExistingFileName names a directory.
            /// </summary>
            MOVEFILE_REPLACE_EXISTING = 0x00000001,
            /// <summary>
            /// The system does not move the file until the operating system is restarted. The system moves the file immediately after AUTOCHK is executed, but before creating any paging files. Consequently, this parameter enables the function to delete paging files from previous startups.
            /// This value can be used only if the process is in the context of a user who belongs to the administrators group or the LocalSystem account.
            /// This value cannot be used with MOVEFILE_COPY_ALLOWED.
            /// Windows Server 2003 and Windows XP:  For information about special situations where this functionality can fail, and a suggested workaround solution, see Files are not exchanged when Windows Server 2003 restarts if you use the MoveFileEx function to schedule a replacement for some files in the Help and Support Knowledge Base.
            /// </summary>
            MOVEFILE_DELAY_UNTIL_REBOOT = 0x00000004,
            /// <summary>
            /// The function does not return until the file is actually moved on the disk.
            /// Setting this value guarantees that a move performed as a copy and delete operation is flushed to disk before the function returns. The flush occurs at the end of the copy operation.
            /// This value has no effect if MOVEFILE_DELAY_UNTIL_REBOOT is set.
            /// </summary>
            MOVEFILE_WRITE_THROUGH = 0x00000008,
            /// <summary>
            /// Reserved for future use.
            /// </summary>
            MOVEFILE_CREATE_HARDLINK = 0x00000010,
            /// <summary>
            /// The function fails if the source file is a link source, but the file cannot be tracked after the move. This situation can occur if the destination is a volume formatted with the FAT file system.
            /// </summary>
            MOVEFILE_FAIL_IF_NOT_TRACKABLE = 0x00000020
        }

        public static bool Move(string OriginalPath, string DescFilrPath)
        {
            return MoveFileEx(OriginalPath, DescFilrPath, MoveFileFlags.MOVEFILE_REPLACE_EXISTING) == 0;
        }
        public static bool Move(string OriginalPath, string DescFilrPath, MoveFileFlags Flag)
        {
            return MoveFileEx(OriginalPath, DescFilrPath, Flag) == 0;
        }

        [DllImport("kernel32.dll", SetLastError = true, CharSet = CharSet.Auto, EntryPoint = "MoveFileEx", BestFitMapping = false)]
        public static extern int MoveFileEx(string lpExistingFileName, string lpNewFileName,
           MoveFileFlags dwFlags);

        [DllImport("Kernel32")]
        private extern static Boolean CloseHandle(IntPtr handle);
    }
}
