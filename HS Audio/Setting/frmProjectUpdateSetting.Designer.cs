﻿namespace HS_Audio.Setting
{
    partial class frmProjectUpdateSetting
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.chk_PitchUpdate = new System.Windows.Forms.CheckBox();
            this.chk_DSPAutoUpdate = new System.Windows.Forms.CheckBox();
            this.btn모두선택 = new System.Windows.Forms.Button();
            this.btn확인 = new System.Windows.Forms.Button();
            this.btn취소 = new System.Windows.Forms.Button();
            this.btn모두선택해제 = new System.Windows.Forms.Button();
            this.chk_EQUpdate = new System.Windows.Forms.CheckBox();
            this.chk_VolumeUpdate = new System.Windows.Forms.CheckBox();
            this.chk_MixerUpdate = new System.Windows.Forms.CheckBox();
            this.chk_ReverbUpdate = new System.Windows.Forms.CheckBox();
            this.chk_PanUpdate = new System.Windows.Forms.CheckBox();
            this.chk_EtcSettingUpdate = new System.Windows.Forms.CheckBox();
            this.chk_FrequencyUpdate = new System.Windows.Forms.CheckBox();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // chk_PitchUpdate
            // 
            this.chk_PitchUpdate.AutoSize = true;
            this.chk_PitchUpdate.Checked = true;
            this.chk_PitchUpdate.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chk_PitchUpdate.Location = new System.Drawing.Point(4, 51);
            this.chk_PitchUpdate.Name = "chk_PitchUpdate";
            this.chk_PitchUpdate.Size = new System.Drawing.Size(100, 16);
            this.chk_PitchUpdate.TabIndex = 0;
            this.chk_PitchUpdate.Text = "피치 업데이트";
            this.chk_PitchUpdate.UseVisualStyleBackColor = true;
            this.chk_PitchUpdate.CheckedChanged += new System.EventHandler(this.chk_PitchUpdate_CheckedChanged);
            // 
            // chk_DSPAutoUpdate
            // 
            this.chk_DSPAutoUpdate.AutoSize = true;
            this.chk_DSPAutoUpdate.Checked = true;
            this.chk_DSPAutoUpdate.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chk_DSPAutoUpdate.Location = new System.Drawing.Point(155, 51);
            this.chk_DSPAutoUpdate.Name = "chk_DSPAutoUpdate";
            this.chk_DSPAutoUpdate.Size = new System.Drawing.Size(100, 16);
            this.chk_DSPAutoUpdate.TabIndex = 1;
            this.chk_DSPAutoUpdate.Text = "DSP 업데이트";
            this.chk_DSPAutoUpdate.UseVisualStyleBackColor = true;
            // 
            // btn모두선택
            // 
            this.btn모두선택.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btn모두선택.Location = new System.Drawing.Point(1, 114);
            this.btn모두선택.Name = "btn모두선택";
            this.btn모두선택.Size = new System.Drawing.Size(75, 23);
            this.btn모두선택.TabIndex = 2;
            this.btn모두선택.Text = "모두 선택";
            this.btn모두선택.UseVisualStyleBackColor = true;
            this.btn모두선택.Click += new System.EventHandler(this.btn모두선택_Click);
            // 
            // btn확인
            // 
            this.btn확인.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btn확인.Location = new System.Drawing.Point(270, 114);
            this.btn확인.Name = "btn확인";
            this.btn확인.Size = new System.Drawing.Size(75, 23);
            this.btn확인.TabIndex = 3;
            this.btn확인.Text = "확인";
            this.btn확인.UseVisualStyleBackColor = true;
            this.btn확인.Click += new System.EventHandler(this.btn확인_Click);
            // 
            // btn취소
            // 
            this.btn취소.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btn취소.Location = new System.Drawing.Point(189, 114);
            this.btn취소.Name = "btn취소";
            this.btn취소.Size = new System.Drawing.Size(75, 23);
            this.btn취소.TabIndex = 4;
            this.btn취소.Text = "취소";
            this.btn취소.UseVisualStyleBackColor = true;
            this.btn취소.Click += new System.EventHandler(this.btn취소_Click);
            // 
            // btn모두선택해제
            // 
            this.btn모두선택해제.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btn모두선택해제.Location = new System.Drawing.Point(82, 114);
            this.btn모두선택해제.Name = "btn모두선택해제";
            this.btn모두선택해제.Size = new System.Drawing.Size(95, 23);
            this.btn모두선택해제.TabIndex = 5;
            this.btn모두선택해제.Text = "모두 선택 해제";
            this.btn모두선택해제.UseVisualStyleBackColor = true;
            this.btn모두선택해제.Click += new System.EventHandler(this.btn모두선택해제_Click);
            // 
            // chk_EQUpdate
            // 
            this.chk_EQUpdate.AutoSize = true;
            this.chk_EQUpdate.Checked = true;
            this.chk_EQUpdate.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chk_EQUpdate.Location = new System.Drawing.Point(155, 7);
            this.chk_EQUpdate.Name = "chk_EQUpdate";
            this.chk_EQUpdate.Size = new System.Drawing.Size(163, 16);
            this.chk_EQUpdate.TabIndex = 6;
            this.chk_EQUpdate.Text = "이퀄라이저(EQ) 업데이트";
            this.chk_EQUpdate.UseVisualStyleBackColor = true;
            // 
            // chk_VolumeUpdate
            // 
            this.chk_VolumeUpdate.AutoSize = true;
            this.chk_VolumeUpdate.Checked = true;
            this.chk_VolumeUpdate.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chk_VolumeUpdate.Location = new System.Drawing.Point(4, 7);
            this.chk_VolumeUpdate.Name = "chk_VolumeUpdate";
            this.chk_VolumeUpdate.Size = new System.Drawing.Size(100, 16);
            this.chk_VolumeUpdate.TabIndex = 7;
            this.chk_VolumeUpdate.Text = "음량 업데이트";
            this.chk_VolumeUpdate.UseVisualStyleBackColor = true;
            // 
            // chk_MixerUpdate
            // 
            this.chk_MixerUpdate.AutoSize = true;
            this.chk_MixerUpdate.Checked = true;
            this.chk_MixerUpdate.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chk_MixerUpdate.Location = new System.Drawing.Point(4, 29);
            this.chk_MixerUpdate.Name = "chk_MixerUpdate";
            this.chk_MixerUpdate.Size = new System.Drawing.Size(100, 16);
            this.chk_MixerUpdate.TabIndex = 8;
            this.chk_MixerUpdate.Text = "믹서 업데이트";
            this.chk_MixerUpdate.UseVisualStyleBackColor = true;
            // 
            // chk_ReverbUpdate
            // 
            this.chk_ReverbUpdate.AutoSize = true;
            this.chk_ReverbUpdate.Checked = true;
            this.chk_ReverbUpdate.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chk_ReverbUpdate.Location = new System.Drawing.Point(155, 29);
            this.chk_ReverbUpdate.Name = "chk_ReverbUpdate";
            this.chk_ReverbUpdate.Size = new System.Drawing.Size(177, 16);
            this.chk_ReverbUpdate.TabIndex = 9;
            this.chk_ReverbUpdate.Text = "반향 효과(Reverb) 업데이트";
            this.chk_ReverbUpdate.UseVisualStyleBackColor = true;
            // 
            // chk_PanUpdate
            // 
            this.chk_PanUpdate.AutoSize = true;
            this.chk_PanUpdate.Checked = true;
            this.chk_PanUpdate.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chk_PanUpdate.Location = new System.Drawing.Point(4, 93);
            this.chk_PanUpdate.Name = "chk_PanUpdate";
            this.chk_PanUpdate.Size = new System.Drawing.Size(88, 16);
            this.chk_PanUpdate.TabIndex = 10;
            this.chk_PanUpdate.Text = "팬 업데이트";
            this.chk_PanUpdate.UseVisualStyleBackColor = true;
            // 
            // chk_EtcSettingUpdate
            // 
            this.chk_EtcSettingUpdate.AutoSize = true;
            this.chk_EtcSettingUpdate.Checked = true;
            this.chk_EtcSettingUpdate.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chk_EtcSettingUpdate.Location = new System.Drawing.Point(155, 73);
            this.chk_EtcSettingUpdate.Name = "chk_EtcSettingUpdate";
            this.chk_EtcSettingUpdate.Size = new System.Drawing.Size(128, 16);
            this.chk_EtcSettingUpdate.TabIndex = 11;
            this.chk_EtcSettingUpdate.Text = "기타 설정 업데이트";
            this.chk_EtcSettingUpdate.UseVisualStyleBackColor = true;
            // 
            // chk_FrequencyUpdate
            // 
            this.chk_FrequencyUpdate.AutoSize = true;
            this.chk_FrequencyUpdate.Checked = true;
            this.chk_FrequencyUpdate.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chk_FrequencyUpdate.Enabled = false;
            this.chk_FrequencyUpdate.Location = new System.Drawing.Point(21, 72);
            this.chk_FrequencyUpdate.Name = "chk_FrequencyUpdate";
            this.chk_FrequencyUpdate.Size = new System.Drawing.Size(112, 16);
            this.chk_FrequencyUpdate.TabIndex = 12;
            this.chk_FrequencyUpdate.Text = "주파수 업데이트";
            this.chk_FrequencyUpdate.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label1.Location = new System.Drawing.Point(2, 74);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(18, 12);
            this.label1.TabIndex = 13;
            this.label1.Text = "└";
            // 
            // frmProjectUpdateSetting
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(348, 138);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.chk_FrequencyUpdate);
            this.Controls.Add(this.chk_EtcSettingUpdate);
            this.Controls.Add(this.chk_PanUpdate);
            this.Controls.Add(this.chk_ReverbUpdate);
            this.Controls.Add(this.chk_MixerUpdate);
            this.Controls.Add(this.chk_VolumeUpdate);
            this.Controls.Add(this.chk_EQUpdate);
            this.Controls.Add(this.btn모두선택해제);
            this.Controls.Add(this.btn취소);
            this.Controls.Add(this.btn확인);
            this.Controls.Add(this.btn모두선택);
            this.Controls.Add(this.chk_DSPAutoUpdate);
            this.Controls.Add(this.chk_PitchUpdate);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmProjectUpdateSetting";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.Text = "프리셋 업데이트 설정";
            this.TopMost = true;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmProjectUpdateSetting_FormClosing);
            this.Load += new System.EventHandler(this.frmPresetUpdateSetting_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.CheckBox chk_PitchUpdate;
        private System.Windows.Forms.CheckBox chk_DSPAutoUpdate;
        private System.Windows.Forms.Button btn모두선택;
        private System.Windows.Forms.Button btn확인;
        private System.Windows.Forms.Button btn취소;
        private System.Windows.Forms.Button btn모두선택해제;
        private System.Windows.Forms.CheckBox chk_EQUpdate;
        private System.Windows.Forms.CheckBox chk_VolumeUpdate;
        private System.Windows.Forms.CheckBox chk_MixerUpdate;
        private System.Windows.Forms.CheckBox chk_ReverbUpdate;
        private System.Windows.Forms.CheckBox chk_PanUpdate;
        private System.Windows.Forms.CheckBox chk_EtcSettingUpdate;
        private System.Windows.Forms.CheckBox chk_FrequencyUpdate;
        private System.Windows.Forms.Label label1;
    }
}