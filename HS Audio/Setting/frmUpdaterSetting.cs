﻿using System;
using System.Windows.Forms;
using System.Collections.Generic;

namespace HS_Audio
{
    public partial class frmUpdaterSetting : Form
    {
        public delegate void UptaterSettingCompleteEventHandler(Dictionary<string, string> UptaterSetting);
        public static event UptaterSettingCompleteEventHandler UptaterSettingComplete;

        public frmUpdaterSetting()
        {
            InitializeComponent();
        }
        public frmUpdaterSetting(Dictionary<string, string> UptaterSetting)
        {
            InitializeComponent();
            this.UptaterSetting = UptaterSetting;
        }

        public Dictionary<string, string> UptaterSetting
        {
            get
            {
                System.Collections.Generic.Dictionary<string, string> a=
                    new System.Collections.Generic.Dictionary<string, string>();
                a.Add("Server",textBox1.Text);
                //a.Add("Port", numericUpDown1.Value.ToString());
                a.Add("AdvancedOption", checkBox1.Checked.ToString());
                a.Add("ServerVersionFilePath", textBox4.Text);
                a.Add("ServerUpdateFilePathUse", (!checkBox3.Checked).ToString());
                a.Add("ServerUpdateFilePath", textBox5.Text);
                a.Add("IsAnonymous", checkBox2.Checked.ToString());
                a.Add("ID", textBox2.Text);
                a.Add("Password", textBox3.Text);
                return a;
            }
            set
            {
                try { textBox1.Text = value["Server"]; }catch{}
                //try { numericUpDown1.Value = decimal.Parse(value["Port"]); }catch{}
                try { checkBox1.Checked = bool.Parse(value["Server"]); }catch{}
                try { textBox4.Text = value["ServerVersionFilePath"]; }catch{}
                try { checkBox3.Checked = !bool.Parse(value["ServerUpdateFilePathUse"]); }catch{}
                try { textBox5.Text = value["ServerUpdateFilePath"]; }catch{}
                try { checkBox2.Checked = bool.Parse(value["IsAnonymous"]); }catch{}
                try { textBox2.Text = value["ID"]; }catch{}
                try { textBox3.Text = value["Password"]; }catch{}
            }
        }

        private void frmUpdaterSetting_Load(object sender, EventArgs e)
        {

        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            groupBox1.Enabled = checkBox1.Checked;
        }

        private void checkBox2_CheckedChanged(object sender, EventArgs e)
        {
            groupBox2.Enabled = !checkBox2.Checked;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try { UptaterSettingComplete(UptaterSetting); }catch{ }
            this.Close();
        }

        private void checkBox3_CheckedChanged(object sender, EventArgs e)
        {
            textBox5.ReadOnly = checkBox3.Checked;
        }
    }
}
