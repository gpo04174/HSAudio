﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.IO;
using HS_CSharpUtility;
using HS_CSharpUtility.Extension;

namespace HS_Audio.Setting
{
    public partial class frmProjectUpdateSetting : Form
    {

        public delegate void ProjectUpdatedEventHandler(ProjectUpdateSetting Setting);
        public event ProjectUpdatedEventHandler ProjectUpdated;

        ProjectUpdateSetting _CurrentSetting;
        public ProjectUpdateSetting CurrentSetting
        { 
            get 
            { 
                _CurrentSetting.VolumeUpdate=chk_VolumeUpdate.Checked;
                _CurrentSetting.MixerUpdate = chk_MixerUpdate.Checked;
                _CurrentSetting.PitchUpdate = chk_PitchUpdate.Checked;
                _CurrentSetting.FrequencyUpdate = chk_FrequencyUpdate.Checked;
                _CurrentSetting.PanUpdate = chk_PanUpdate.Checked;
                _CurrentSetting.EQUpdate = chk_EQUpdate.Checked;
                _CurrentSetting.ReverbUpdate = chk_ReverbUpdate.Checked;
                _CurrentSetting.DSPAutoUpdate = chk_DSPAutoUpdate.Checked;
                _CurrentSetting.EtcSettingUpdate = chk_EtcSettingUpdate.Checked;
                return _CurrentSetting;
            } 
            set
            {
                chk_VolumeUpdate.Checked = value.VolumeUpdate;
                chk_MixerUpdate.Checked = value.MixerUpdate;
                chk_PitchUpdate.Checked = value.PitchUpdate;
                chk_FrequencyUpdate.Checked = value.FrequencyUpdate;
                chk_PanUpdate.Checked = value.PanUpdate;
                chk_EQUpdate.Checked = value.EQUpdate;
                chk_ReverbUpdate.Checked = value.ReverbUpdate;
                chk_DSPAutoUpdate.Checked = value.DSPAutoUpdate;
                chk_EtcSettingUpdate.Checked = value.EtcSettingUpdate;
            }
        }
        public frmProjectUpdateSetting()
        {
            InitializeComponent();
            try
            {
                string[] a = File.ReadAllLines(Utility.EtcUtility.GetSettingDirectory() + "\\frmPresetUpdateSetting.ini");
                Setting = HS_CSharpUtility.Utility.EtcUtility.LoadSetting(a);
            }
            catch (Exception ex) { /*ex.Logging("HS_Audio::Setting::frmPresetUpdateSetting ::.ctor() 에서 예외 발생");*/ } 
        }

        private void btn확인_Click(object sender, System.EventArgs e)
        {
            if(!UpdateSetting()){MessageBox.Show("설정 파일로 저장 실패!\n\n자세한 예외는 로그를 확인해 주세요.", "프리셋 업데이트 설정", MessageBoxButtons.OK, MessageBoxIcon.Error);}
            try { if(ProjectUpdated!=null)ProjectUpdated(CurrentSetting); }catch{}
            this.Hide();
        }

        private void chk_PitchUpdate_CheckedChanged(object sender, System.EventArgs e) { chk_FrequencyUpdate.Enabled = !chk_PitchUpdate.Checked; }

        
        
        public Dictionary<string, string> Setting
        {
            get
            {
                Dictionary<string, string> _Setting = new Dictionary<string, string>();
                _Setting.Add("VolumeUpdate", chk_VolumeUpdate.Checked.ToString());
                _Setting.Add("MixerUpdate", chk_MixerUpdate.Checked.ToString());
                _Setting.Add("PitchUpdate", chk_PitchUpdate.Checked.ToString());
                _Setting.Add("FrequencyUpdate", chk_FrequencyUpdate.Checked.ToString());
                _Setting.Add("PanUpdate", chk_PanUpdate.Checked.ToString());
                _Setting.Add("EQUpdate", chk_EQUpdate.Checked.ToString());
                _Setting.Add("ReverbUpdate", chk_ReverbUpdate.Checked.ToString());
                _Setting.Add("DSPAutoUpdate", chk_DSPAutoUpdate.Checked.ToString());
                _Setting.Add("EtcSettingUpdate", chk_EtcSettingUpdate.Checked.ToString());
                return _Setting;
            }
            set
            {
                try {chk_VolumeUpdate.Checked=bool.Parse(value["VolumeUpdate"]); }catch{}
                try {chk_MixerUpdate.Checked=bool.Parse(value["MixerUpdate"]); }catch{}
                try {chk_PitchUpdate.Checked=bool.Parse(value["PitchUpdate"]); }catch{}
                try {chk_FrequencyUpdate.Checked=bool.Parse(value["FrequencyUpdate"]); }catch{}
                try {chk_PanUpdate.Checked=bool.Parse(value["PanUpdate"]); }catch{}
                try {chk_EQUpdate.Checked=bool.Parse(value["EQUpdate"]); }catch{}
                try {chk_ReverbUpdate.Checked=bool.Parse(value["ReverbUpdate"]); }catch{}
                try {chk_DSPAutoUpdate.Checked=bool.Parse(value["DSPAutoUpdate"]); }catch{}
                try {chk_EtcSettingUpdate.Checked=bool.Parse(value["EtcSettingUpdate"]); }catch{}
            }
        }
        public bool UpdateSetting()
        {
            try { File.WriteAllLines(Utility.EtcUtility.GetSettingDirectory() + 
                "\\frmPresetUpdateSetting.ini", HS_CSharpUtility.Utility.EtcUtility.SaveSetting(Setting));return true;}
            catch (Exception ex) { ex.Logging("HS_Audio::Setting::frmPresetUpdateSetting::UpdateSetting() 에서 예외 발생!!"); return false; }
        }

        public struct ProjectUpdateSetting
        {
            public bool VolumeUpdate;
            public bool MixerUpdate;
            public bool PitchUpdate;
            public bool FrequencyUpdate;
            public bool PanUpdate;
            public bool EQUpdate;
            public bool ReverbUpdate;
            public bool DSPAutoUpdate;
            public bool EtcSettingUpdate;
            /*
            public PresetUpdateSetting()
            {
                VolumeUpdate = MixerUpdate = PitchUpdate = FrequencyUpdate = PanUpdate =
                EQUpdate = ReverbUpdate = DSPAutoUpdate = EtcSettingUpdate = true;
            }*/
            public ProjectUpdateSetting(bool VolumeUpdate = true, bool MixerUpdate = true, bool PitchUpdate = true, bool FrequencyUpdate = true,
                                       bool PanUpdate = true, bool EQUpdate = true, bool ReverbUpdate = true, bool DSPAutoUpdate = true, bool EtcSettingUpdate = true)
            {
                this.VolumeUpdate = VolumeUpdate; this.ReverbUpdate = ReverbUpdate;
                this.PitchUpdate = PitchUpdate; this.FrequencyUpdate = FrequencyUpdate;
                this.PanUpdate = PanUpdate; this.EQUpdate = EQUpdate; this.MixerUpdate = MixerUpdate;
                this.DSPAutoUpdate = DSPAutoUpdate; this.EtcSettingUpdate = EtcSettingUpdate;
            }
        }

        private void frmPresetUpdateSetting_Load(object sender, EventArgs e)
        {

        }

        private void btn모두선택해제_Click(object sender, EventArgs e)
        {
            chk_VolumeUpdate.Checked = chk_MixerUpdate.Checked = chk_PitchUpdate.Checked = 
            chk_FrequencyUpdate.Checked =chk_PanUpdate.Checked =chk_EQUpdate.Checked = chk_ReverbUpdate.Checked = 
            chk_DSPAutoUpdate.Checked =  chk_EtcSettingUpdate.Checked = false;
        }

        private void btn모두선택_Click(object sender, EventArgs e)
        {
            chk_VolumeUpdate.Checked = chk_MixerUpdate.Checked = chk_PitchUpdate.Checked = 
            chk_FrequencyUpdate.Checked =chk_PanUpdate.Checked =chk_EQUpdate.Checked = chk_ReverbUpdate.Checked = 
            chk_DSPAutoUpdate.Checked =  chk_EtcSettingUpdate.Checked = true;
        }

        private void btn취소_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void frmProjectUpdateSetting_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = true;
            this.Hide();
        }
    }
}
