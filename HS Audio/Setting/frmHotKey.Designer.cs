﻿namespace HS_Audio.Setting
{
    partial class frmHotKey
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.txtPlay = new System.Windows.Forms.TextBox();
            this.btnPlay = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.btnPrevious = new System.Windows.Forms.Button();
            this.txtPrevious = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.btnNext = new System.Windows.Forms.Button();
            this.txtNext = new System.Windows.Forms.TextBox();
            this.lblPlay = new System.Windows.Forms.Label();
            this.lblPrevious = new System.Windows.Forms.Label();
            this.lblNext = new System.Windows.Forms.Label();
            this.lblVolumeUp = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.btnVolUp = new System.Windows.Forms.Button();
            this.txtVolumeUp = new System.Windows.Forms.TextBox();
            this.lblVolumeDown = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.btnVolDown = new System.Windows.Forms.Button();
            this.txtVolumeDown = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.checkBox2 = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label1.ForeColor = System.Drawing.Color.Blue;
            this.label1.Location = new System.Drawing.Point(5, 7);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(424, 16);
            this.label1.TabIndex = 0;
            this.label1.Text = "아래의 텍스트 상자에다가 키를 눌렀다 떼시면 됩니다.";
            // 
            // txtPlay
            // 
            this.txtPlay.BackColor = System.Drawing.Color.White;
            this.txtPlay.Location = new System.Drawing.Point(13, 87);
            this.txtPlay.Name = "txtPlay";
            this.txtPlay.ReadOnly = true;
            this.txtPlay.Size = new System.Drawing.Size(83, 21);
            this.txtPlay.TabIndex = 1;
            this.txtPlay.TextChanged += new System.EventHandler(this.txtPlay_TextChanged);
            this.txtPlay.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtPlay_KeyDown);
            this.txtPlay.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtPlay_KeyPress);
            // 
            // btnPlay
            // 
            this.btnPlay.Location = new System.Drawing.Point(13, 128);
            this.btnPlay.Name = "btnPlay";
            this.btnPlay.Size = new System.Drawing.Size(83, 23);
            this.btnPlay.TabIndex = 2;
            this.btnPlay.Text = "설정 해제";
            this.btnPlay.UseVisualStyleBackColor = true;
            this.btnPlay.Click += new System.EventHandler(this.btnPlay_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label2.Location = new System.Drawing.Point(6, 72);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(100, 12);
            this.label2.TabIndex = 3;
            this.label2.Text = "재생 / 일시정지";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label3.Location = new System.Drawing.Point(123, 72);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(49, 12);
            this.label3.TabIndex = 6;
            this.label3.Text = "이전 곡";
            // 
            // btnPrevious
            // 
            this.btnPrevious.Location = new System.Drawing.Point(106, 128);
            this.btnPrevious.Name = "btnPrevious";
            this.btnPrevious.Size = new System.Drawing.Size(83, 23);
            this.btnPrevious.TabIndex = 5;
            this.btnPrevious.Text = "설정 해제";
            this.btnPrevious.UseVisualStyleBackColor = true;
            this.btnPrevious.Click += new System.EventHandler(this.btnPrevious_Click);
            // 
            // txtPrevious
            // 
            this.txtPrevious.BackColor = System.Drawing.Color.White;
            this.txtPrevious.Location = new System.Drawing.Point(106, 87);
            this.txtPrevious.Name = "txtPrevious";
            this.txtPrevious.ReadOnly = true;
            this.txtPrevious.Size = new System.Drawing.Size(83, 21);
            this.txtPrevious.TabIndex = 4;
            this.txtPrevious.TextChanged += new System.EventHandler(this.txtPrevious_TextChanged);
            this.txtPrevious.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtPrevious_KeyDown);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label4.Location = new System.Drawing.Point(213, 72);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(49, 12);
            this.label4.TabIndex = 9;
            this.label4.Text = "다음 곡";
            // 
            // btnNext
            // 
            this.btnNext.Location = new System.Drawing.Point(197, 128);
            this.btnNext.Name = "btnNext";
            this.btnNext.Size = new System.Drawing.Size(83, 23);
            this.btnNext.TabIndex = 8;
            this.btnNext.Text = "설정 해제";
            this.btnNext.UseVisualStyleBackColor = true;
            this.btnNext.Click += new System.EventHandler(this.btnNext_Click);
            // 
            // txtNext
            // 
            this.txtNext.BackColor = System.Drawing.Color.White;
            this.txtNext.Location = new System.Drawing.Point(197, 87);
            this.txtNext.Name = "txtNext";
            this.txtNext.ReadOnly = true;
            this.txtNext.Size = new System.Drawing.Size(83, 21);
            this.txtNext.TabIndex = 7;
            this.txtNext.TextChanged += new System.EventHandler(this.txtNext_TextChanged);
            this.txtNext.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtNext_KeyDown);
            // 
            // lblPlay
            // 
            this.lblPlay.AutoSize = true;
            this.lblPlay.Location = new System.Drawing.Point(12, 113);
            this.lblPlay.Name = "lblPlay";
            this.lblPlay.Size = new System.Drawing.Size(64, 12);
            this.lblPlay.TabIndex = 10;
            this.lblPlay.Text = "키 값: N/A";
            // 
            // lblPrevious
            // 
            this.lblPrevious.AutoSize = true;
            this.lblPrevious.Location = new System.Drawing.Point(105, 113);
            this.lblPrevious.Name = "lblPrevious";
            this.lblPrevious.Size = new System.Drawing.Size(64, 12);
            this.lblPrevious.TabIndex = 11;
            this.lblPrevious.Text = "키 값: N/A";
            // 
            // lblNext
            // 
            this.lblNext.AutoSize = true;
            this.lblNext.Location = new System.Drawing.Point(196, 113);
            this.lblNext.Name = "lblNext";
            this.lblNext.Size = new System.Drawing.Size(64, 12);
            this.lblNext.TabIndex = 12;
            this.lblNext.Text = "키 값: N/A";
            // 
            // lblVolumeUp
            // 
            this.lblVolumeUp.AutoSize = true;
            this.lblVolumeUp.Location = new System.Drawing.Point(285, 113);
            this.lblVolumeUp.Name = "lblVolumeUp";
            this.lblVolumeUp.Size = new System.Drawing.Size(64, 12);
            this.lblVolumeUp.TabIndex = 16;
            this.lblVolumeUp.Text = "키 값: N/A";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label8.Location = new System.Drawing.Point(296, 72);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(62, 12);
            this.label8.TabIndex = 15;
            this.label8.Text = "음량 높임";
            // 
            // btnVolUp
            // 
            this.btnVolUp.Location = new System.Drawing.Point(286, 128);
            this.btnVolUp.Name = "btnVolUp";
            this.btnVolUp.Size = new System.Drawing.Size(83, 23);
            this.btnVolUp.TabIndex = 14;
            this.btnVolUp.Text = "설정 해제";
            this.btnVolUp.UseVisualStyleBackColor = true;
            this.btnVolUp.Click += new System.EventHandler(this.btnVolumeUp_Click);
            // 
            // txtVolumeUp
            // 
            this.txtVolumeUp.BackColor = System.Drawing.Color.White;
            this.txtVolumeUp.Location = new System.Drawing.Point(286, 87);
            this.txtVolumeUp.Name = "txtVolumeUp";
            this.txtVolumeUp.ReadOnly = true;
            this.txtVolumeUp.Size = new System.Drawing.Size(83, 21);
            this.txtVolumeUp.TabIndex = 13;
            this.txtVolumeUp.TextChanged += new System.EventHandler(this.txtVolumeUp_TextChanged);
            this.txtVolumeUp.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtVolumeUp_KeyDown);
            // 
            // lblVolumeDown
            // 
            this.lblVolumeDown.AutoSize = true;
            this.lblVolumeDown.Location = new System.Drawing.Point(376, 113);
            this.lblVolumeDown.Name = "lblVolumeDown";
            this.lblVolumeDown.Size = new System.Drawing.Size(64, 12);
            this.lblVolumeDown.TabIndex = 20;
            this.lblVolumeDown.Text = "키 값: N/A";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label10.Location = new System.Drawing.Point(387, 72);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(62, 12);
            this.label10.TabIndex = 19;
            this.label10.Text = "음량 줄임";
            // 
            // btnVolDown
            // 
            this.btnVolDown.Location = new System.Drawing.Point(377, 128);
            this.btnVolDown.Name = "btnVolDown";
            this.btnVolDown.Size = new System.Drawing.Size(83, 23);
            this.btnVolDown.TabIndex = 18;
            this.btnVolDown.Text = "설정 해제";
            this.btnVolDown.UseVisualStyleBackColor = true;
            this.btnVolDown.Click += new System.EventHandler(this.btnVolumeDown_Click);
            // 
            // txtVolumeDown
            // 
            this.txtVolumeDown.BackColor = System.Drawing.Color.White;
            this.txtVolumeDown.Location = new System.Drawing.Point(377, 87);
            this.txtVolumeDown.Name = "txtVolumeDown";
            this.txtVolumeDown.ReadOnly = true;
            this.txtVolumeDown.Size = new System.Drawing.Size(83, 21);
            this.txtVolumeDown.TabIndex = 17;
            this.txtVolumeDown.TextChanged += new System.EventHandler(this.txtVolumeDown_TextChanged);
            this.txtVolumeDown.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtVolumeDown_KeyDown);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(1, 157);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(231, 28);
            this.button1.TabIndex = 21;
            this.button1.Text = "취소";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(238, 157);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(231, 28);
            this.button2.TabIndex = 22;
            this.button2.Text = "적용";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Location = new System.Drawing.Point(8, 49);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(349, 16);
            this.checkBox1.TabIndex = 23;
            this.checkBox1.Text = "글로벌 후킹 (프로그램이 보이지않을때도 핫키가 실행됩니다";
            this.checkBox1.UseVisualStyleBackColor = true;
            this.checkBox1.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
            this.checkBox1.Click += new System.EventHandler(this.checkBox1_Click);
            // 
            // checkBox2
            // 
            this.checkBox2.AutoSize = true;
            this.checkBox2.Location = new System.Drawing.Point(8, 29);
            this.checkBox2.Name = "checkBox2";
            this.checkBox2.Size = new System.Drawing.Size(154, 16);
            this.checkBox2.TabIndex = 24;
            this.checkBox2.Text = "단축키 (핫키) 방해 금지";
            this.checkBox2.UseVisualStyleBackColor = true;
            this.checkBox2.CheckedChanged += new System.EventHandler(this.checkBox2_CheckedChanged);
            this.checkBox2.Click += new System.EventHandler(this.checkBox2_Click);
            // 
            // frmHotKey
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(469, 189);
            this.Controls.Add(this.checkBox2);
            this.Controls.Add(this.checkBox1);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.lblVolumeDown);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.btnVolDown);
            this.Controls.Add(this.txtVolumeDown);
            this.Controls.Add(this.lblVolumeUp);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.btnVolUp);
            this.Controls.Add(this.txtVolumeUp);
            this.Controls.Add(this.lblNext);
            this.Controls.Add(this.lblPrevious);
            this.Controls.Add(this.lblPlay);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.btnNext);
            this.Controls.Add(this.txtNext);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.btnPrevious);
            this.Controls.Add(this.txtPrevious);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.btnPlay);
            this.Controls.Add(this.txtPlay);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "frmHotKey";
            this.Text = "단축키(핫키) 설정";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmHotKey_FormClosing);
            this.Load += new System.EventHandler(this.frmHotKey_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtPlay;
        private System.Windows.Forms.Button btnPlay;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnPrevious;
        private System.Windows.Forms.TextBox txtPrevious;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btnNext;
        private System.Windows.Forms.TextBox txtNext;
        private System.Windows.Forms.Label lblPlay;
        private System.Windows.Forms.Label lblPrevious;
        private System.Windows.Forms.Label lblNext;
        private System.Windows.Forms.Label lblVolumeUp;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button btnVolUp;
        private System.Windows.Forms.TextBox txtVolumeUp;
        private System.Windows.Forms.Label lblVolumeDown;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Button btnVolDown;
        private System.Windows.Forms.TextBox txtVolumeDown;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.CheckBox checkBox2;

    }
}