﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Windows.Forms;
using HS_CSharpUtility.Extension;

namespace HS_Audio.Setting
{
    public partial class frmHotKey : Form
    {
        public delegate void CancelButtonPressedEventHandler(frmHotKey sender);
        public delegate void ApplyButtonPressedEventHandler(frmHotKey sender);
        public event CancelButtonPressedEventHandler CancelButtonPressed;
        public event ApplyButtonPressedEventHandler ApplyButtonPressed;
        public frmHotKey()
        {
            InitializeComponent();
            //LoadSetting();
        }
        Dictionary<string, string> Key = new Dictionary<string, string>(); 
        Dictionary<string, string> KeyMemo = new Dictionary<string, string>();
        public void SaveSetting()
        {
            try
            {
                string[] a = HS_CSharpUtility.Utility.EtcUtility.SaveSetting(Key);
                string[] b = HS_CSharpUtility.Utility.EtcUtility.SaveSetting(KeyMemo);
                string c = HS_CSharpUtility.Utility.StringUtility.ConvertArrayToString(a, "\r\n", false);
                string d = HS_CSharpUtility.Utility.StringUtility.ConvertArrayToString(b, "\r\n", false);
                File.WriteAllText(HS_CSharpUtility.Utility.EtcUtility.GetSettingDirectory() + "\\MainHotKey.ini", "//아래의 값들은 절때 건드리지 마세요!\r\n");
                File.AppendAllText(HS_CSharpUtility.Utility.EtcUtility.GetSettingDirectory() + "\\MainHotKey.ini", c+"\r\n\r\n");
                //File.AppendAllText(HS_CSharpUtility.Utility.EtcUtility.GetSettingDirectory() + "\\MainHotKey.ini", "\r\n\r\n//여기 밑에 있는 내용은 지우거나 변경하셔도 됩니다.\r\n");
                File.AppendAllText(HS_CSharpUtility.Utility.EtcUtility.GetSettingDirectory() + "\\MainHotKey.ini", d);
            }catch(Exception ex){ex.Logging("HS_Audio::Setting::frmHotKey::SaveSetting() 에서 설정 저장중 에러발생!");}
        }
        public void LoadSetting()
        {
            if (File.Exists(HS_CSharpUtility.Utility.EtcUtility.GetSettingDirectory() + "\\MainHotKey.ini"))
            {
                try{string[] a = File.ReadAllLines(HS_CSharpUtility.Utility.EtcUtility.GetSettingDirectory() + "\\MainHotKey.ini");
                Dictionary<string, string> tmp = HS_CSharpUtility.Utility.EtcUtility.LoadSetting(a);
                
                try{ if (tmp.ContainsKey("PlayKey")) { Key["PlayKey"] = int.Parse(tmp["PlayKey"]).ToString(); lblPlay.Text = Key["PlayKey"]; } }catch{}
                try{ if (tmp.ContainsKey("PreviousKey")) { Key["PreviousKey"] = int.Parse(tmp["PreviousKey"]).ToString(); lblPrevious.Text = Key["PreviousKey"]; } }catch{}
                try{ if (tmp.ContainsKey("NextKey")) { Key["NextKey"] = int.Parse(tmp["NextKey"]).ToString(); lblNext.Text = Key["NextKey"]; } }catch{}
                try{ if (tmp.ContainsKey("VolumeUpKey")) { Key["VolumeUpKey"] = int.Parse(tmp["VolumeUpKey"]).ToString(); lblVolumeUp.Text = Key["VolumeUpKey"]; } }catch{}
                try{ if (tmp.ContainsKey("VolumeDownKey")) { Key["VolumeDownKey"] = int.Parse(tmp["VolumeDownKey"]).ToString(); lblVolumeDown.Text = Key["VolumeDownKey"]; } }catch{}

                try{ if (tmp.ContainsKey("GlobalKey")) { Key["GlobalKey"] = bool.Parse(tmp["GlobalKey"]).ToString(); checkBox1.Checked = bool.Parse(Key["GlobalKey"]); } }catch{}
                try{ if (tmp.ContainsKey("KeyHandle")) { Key["KeyHandle"] = bool.Parse(tmp["KeyHandle"]).ToString(); checkBox2.Checked = bool.Parse(Key["KeyHandle"]); } }catch{}

                //try{Keys kk =(Keys)Enum.Parse(typeof(Keys),tmp["PlayKeyMemo"]);}catch{}
                try
                {
                     if (tmp.ContainsKey("PlayKeyMemo"))
                     {
                         txtPlay.Text = tmp["PlayKeyMemo"];
                         KeyMemo["PlayKeyMemo"] = tmp["PlayKeyMemo"];
                     }
                } catch{}
                try
                {
                    if (tmp.ContainsKey("PreviousKeyMemo"))
                    {
                        txtPlay.Text = tmp["PreviousKeyMemo"];
                        KeyMemo["PreviousKeyMemo"] = tmp["PreviousKeyMemo"];
                    }
                }
                catch { }
                try
                {
                    if (tmp.ContainsKey("NextKeyMemo"))
                    {
                        txtPlay.Text = tmp["NextKeyMemo"];
                        KeyMemo["NextKeyMemo"] = tmp["NextKeyMemo"];
                    }
                }
                catch { }
                try
                {
                    if (tmp.ContainsKey("VolumeUpKeyMemo"))
                    {
                        txtPlay.Text = tmp["VolumeUpKeyMemo"];
                        KeyMemo["VolumeUpKeyMemo"] = tmp["VolumeUpKeyMemo"];
                    }
                }
                catch { }
                try
                {
                    if (tmp.ContainsKey("VolumeDownKeyMemo"))
                    {
                        txtPlay.Text = tmp["VolumeDownKeyMemo"];
                        KeyMemo["VolumeDownKeyMemo"] = tmp["VolumeDownKeyMemo"];
                    }
                }
                catch { }}catch{}

                button2_Click(true, null);
                //try{ApplyButtonPressed(this);}catch{}
            }
        }

        public int PlayKey{get;private set; }
        public int PreviousKey {get;private set; }
        public int NextKey {get;private set; }
        public int VolumeUpKey {get;private set; }
        public int VolumeDownKey {get;private set; }

        public bool GlobalKey{get;private set;}
        public bool KeyHandle{get;private set;}

        public string PlayKeyMemo {get{return txtPlay.Text;}private set{txtPlay.Text = value;}}
        public string PreviousKeyMemo {get{return txtPrevious.Text;}private set{txtPrevious.Text = value;}}
        public string NextKeyMemo {get{return txtNext.Text;}private set{txtNext.Text = value;}}
        public string VolumeUpKeyMemo {get{return txtVolumeUp.Text;}private set{txtVolumeUp.Text = value;}}
        public string VolumeDownKeyMemo {get{return txtVolumeDown.Text;}private set{txtVolumeDown.Text = value;}}

        #region KeyPress
        private void txtPlay_KeyPress(object sender, KeyPressEventArgs e)
        {
            /*
            try{Key["PlayKey"] =((short)e.KeyChar).ToString(); }
            catch(KeyNotFoundException){Key.Add("PlayKey", ((short)e.KeyChar).ToString());}
            catch{txtPlay.Text="(등록 실패!)";}*/
        }

        private void txtPrevious_KeyPress(object sender, KeyPressEventArgs e)
        {

        }

        private void txtNext_KeyPress(object sender, KeyPressEventArgs e)
        {

        }

        private void txtVolUp_KeyPress(object sender, KeyPressEventArgs e)
        {

        }

        private void txtVolDown_KeyPress(object sender, KeyPressEventArgs e)
        {

        }
        #endregion
        #region KeyDown
        List<KeyEventArgs> ke = new List<KeyEventArgs>();
        private void txtPlay_KeyDown(object sender, KeyEventArgs e)
        {
            /*txtPlay.Text = "";
            if (e.Control) txtPlay.Text = txtPlay.Text+(txtPlay.Text == "" ? "Ctrl" : "+Ctrl");
            if (e.Alt) txtPlay.Text = txtPlay.Text+(txtPlay.Text == "" ? "Alt" : "+Alt");
            if (e.Shift) txtPlay.Text = txtPlay.Text + (txtPlay.Text == "" ? "Shift" : "+Shift");
            if (e.KeyData == Keys.Space) txtPlay.Text = txtPlay.Text + (txtPlay.Text == "" ? "Space" : "+Space");*/

            //char a = (char)e.KeyValue;
            //if (e.KeyValue) txtPlay.Text = txtPlay.Text + (txtPlay.Text == "" ? "Shift" : "+Shift");
            txtPlay.Text = e.KeyData.ToString();
            lblPlay.Text = string.Format("키 값: {0}", ((int)e.KeyData).ToString());
            try{Key["PlayKey"] =((int)e.KeyData).ToString(); } catch{}
        }

        private void txtPrevious_KeyDown(object sender, KeyEventArgs e)
        {
            txtPrevious.Text = e.KeyData.ToString();
            lblPrevious.Text = string.Format("키 값: {0}", ((int)e.KeyData).ToString());
            try{Key["PreviousKey"] =((int)e.KeyData).ToString(); }catch{}
        }

        private void txtNext_KeyDown(object sender, KeyEventArgs e)
        {
            txtNext.Text = e.KeyData.ToString();
            lblNext.Text = string.Format("키 값: {0}", ((int)e.KeyData).ToString());
            try{Key["NextKey"] =((int)e.KeyData).ToString(); }catch{}
        }

        private void txtVolumeUp_KeyDown(object sender, KeyEventArgs e)
        {
            txtVolumeUp.Text = e.KeyData.ToString();
            lblVolumeUp.Text = string.Format("키 값: {0}", ((int)e.KeyData).ToString());
            try{Key["VolumeUpKey"] =((int)e.KeyData).ToString(); }catch{}
        }
        private void txtVolumeDown_KeyDown(object sender, KeyEventArgs e)
        {
            txtVolumeDown.Text = e.KeyData.ToString();
            lblVolumeDown.Text = string.Format("키 값: {0}", ((int)e.KeyData).ToString());
            try{Key["VolumeDownKey"] =((int)e.KeyData).ToString(); }catch{}
        }
        #endregion    
        #region 키설정 해제
        private void btnPlay_Click(object sender, EventArgs e)
        {
            txtPlay.Text = "";
            lblPlay.Text = "키 값: N/A";
            try{Key.Remove("PlayKey"); } catch{}
            try{KeyMemo.Remove("PlayKeyMemo"); } catch{}
        }
        private void btnPrevious_Click(object sender, EventArgs e)
        {
            txtPrevious.Text = "";
            lblPrevious.Text = "키 값: N/A";
            try{Key.Remove("PreviousKey"); } catch{}
            try{KeyMemo.Remove("PreviousKeyMemo"); } catch{}
        }
        private void btnNext_Click(object sender, EventArgs e)
        {
            txtNext.Text = "";
            lblNext.Text = "키 값: N/A";
            try{Key.Remove("NextKey"); } catch{}
            try{KeyMemo.Remove("NextKeyMemo"); } catch{}
        }
        private void btnVolumeUp_Click(object sender, EventArgs e)
        {
            txtVolumeUp.Text = "";
            lblVolumeUp.Text = "키 값: N/A";
            try{Key.Remove("VolumeUpKey"); } catch{}
            try{KeyMemo.Remove("VolumeUpKeyMemo"); } catch{}
        }
        private void btnVolumeDown_Click(object sender, EventArgs e)
        {
            txtVolumeDown.Text = "";
            lblVolumeDown.Text = "키 값: N/A";
            try{Key.Remove("VolumeDownKey"); } catch{}
            try{KeyMemo.Remove("VolumeDownMemo"); } catch{}
        }
        #endregion

        internal void button2_Click(object sender, EventArgs e)
        {
            try{ PlayKey=int.Parse(Key["PlayKey"]);}catch{PlayKey=-1;}
            try{ PreviousKey=int.Parse(Key["PreviousKey"]);}catch{PreviousKey=-1;}
            try{ NextKey=int.Parse(Key["NextKey"]);}catch{NextKey=-1;}
            try{ VolumeUpKey=int.Parse(Key["VolumeUpKey"]);}catch{VolumeUpKey=-1;}
            try{ VolumeDownKey=int.Parse(Key["VolumeDownKey"]);}catch{VolumeDownKey=-1;}
            
            Key["GlobalKey"] =checkBox1.Checked.ToString();
            Key["KeyHandle"] = checkBox2.Checked.ToString();
            /*
            if (txtPlay.Text != "") Key["PlayKeyMemo"] = txtPlay.Text;
            if (txtPrevious.Text != "") Key["PreviousKeyMemo"] = txtPrevious.Text;
            if (txtNext.Text != "") Key["NextKeyMemo"] = txtNext.Text;
            if (txtVolumeUp.Text != "") Key["VolumeUpKeyMemo"] = txtVolumeUp.Text;
            if (txtVolumeDown.Text != "") Key["VolumeDownKeyMemo"] = txtVolumeDown.Text;*/
            
            
            if(txtPlay.Text!="")KeyMemo["PlayKeyMemo"] = txtPlay.Text;
            if(txtPrevious.Text!="")KeyMemo["PreviousKeyMemo"] = txtPrevious.Text;
            if(txtNext.Text!="")KeyMemo["NextKeyMemo"] = txtNext.Text;
            if(txtVolumeUp.Text!="")KeyMemo["VolumeUpKeyMemo"] =txtVolumeUp.Text ;
            if(txtVolumeDown.Text!="")KeyMemo["VolumeDownKeyMemo"] = txtVolumeDown.Text;
            //try{Keys kk =(Keys)Enum.Parse(typeof(Keys),tmp["PlayKeyMemo"]);}catch{}

            if (sender == null || !typeof(bool).Equals(sender.GetType())) SaveSetting();
            LastKey = new Dictionary<string,string>(Key);
            LastKeyMemo = new Dictionary<string,string>(KeyMemo);
            this.Hide();
            try{ApplyButtonPressed(this);}catch{}
        }
        
        Dictionary<string, string> LastKey;
        Dictionary<string, string> LastKeyMemo;
        private void button1_Click(object sender, EventArgs e)
        {
            /*
            btnPlay_Click(null, null);
            btnPrevious_Click(null, null);
            btnNext_Click(null, null);
            btnVolumeUp_Click(null, null);
            btnVolumeDown_Click(null, null);*/
            Key = new Dictionary<string,string>(LastKey);
            KeyMemo = new Dictionary<string,string>(LastKeyMemo);

            try{lblPlay.Text = string.Format("키 값: {0}",Key["PlayKey"]);}catch{lblPlay.Text = "키 값: N/A";}
            try{lblPrevious.Text = string.Format("키 값: {0}",Key["PreviousKey"]);}catch{lblPrevious.Text = "키 값: N/A";}
            try{lblNext.Text = string.Format("키 값: {0}",Key["NextKey"]);}catch{lblNext.Text = "키 값: N/A";}
            try{lblVolumeUp.Text = string.Format("키 값: {0}",Key["VolumeUpKey"]);}catch{lblVolumeUp.Text = "키 값: N/A";}
            try{lblVolumeDown.Text = string.Format("키 값: {0}",Key["VolumeDownKey"]);}catch{lblVolumeDown.Text = "키 값: N/A";}

            try{txtPlay.Text=KeyMemo["PlayKeyMemo"];}catch{txtPlay.Text="";}
            try{txtPrevious.Text=KeyMemo["PreviousKeyMemo"];}catch{txtPrevious.Text="";}
            try{txtNext.Text=KeyMemo["NextKeyMemo"];}catch{txtNext.Text="";}
            try{txtVolumeUp.Text=KeyMemo["VolumeUpKeyMemo"];}catch{txtVolumeUp.Text="";}
            try{txtVolumeDown.Text=KeyMemo["VolumeDownKeyMemo"];}catch{txtVolumeDown.Text="";}
            this.Hide();
            try{ CancelButtonPressed(this);}catch{}
        }

        bool IsClose;
        private void frmHotKey_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = !IsClose;
            this.Hide();
        }

        private void txtPlay_TextChanged(object sender, EventArgs e)
        {
            KeyMemo["PlayKeyMemo"] =txtPlay.Text;
        }

        private void txtPrevious_TextChanged(object sender, EventArgs e)
        {
            KeyMemo["PreviousKeyMemo"] =txtPrevious.Text;
        }

        private void txtNext_TextChanged(object sender, EventArgs e)
        {
            KeyMemo["NextKeyMemo"] =txtNext.Text;
        }

        private void txtVolumeUp_TextChanged(object sender, EventArgs e)
        {
            KeyMemo["VolumeUpKeyMemo"] =txtVolumeUp.Text;
        }

        private void txtVolumeDown_TextChanged(object sender, EventArgs e)
        {
            KeyMemo["VolumeDownKeyMemo"] =txtVolumeDown.Text;
        }

        private void frmHotKey_Load(object sender, EventArgs e)
        {
            HS_CSharpUtility.Utility.Win32Utility.HideXButton(this.Handle);
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            Key["GlobalKey"] = checkBox1.Checked.ToString();
            GlobalKey = checkBox1.Checked;
            if (checkBox1.Checked) checkBox2.Checked = true;
        }

        private void checkBox2_CheckedChanged(object sender, EventArgs e)
        {
            Key["KeyHandle"] = checkBox2.Checked.ToString();
            KeyHandle = checkBox2.Checked;
        }

        private void checkBox1_Click(object sender, EventArgs e)
        {
            if (checkBox1.Checked) MessageBox.Show("이 기능은 프로그램 창이 보이지 않을때도 어디서나 단축키가 동작합니다.\n\n그러나 단축키 (핫키) 방해 금지가 항상 켜지게 됩니다.", "HS플레이어 단축키(핫키) 설정", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void checkBox2_Click(object sender, EventArgs e)
        {
            if (checkBox1.Checked)checkBox2.Checked = true;
            else if (checkBox2.Checked) MessageBox.Show("이 기능은 단축키(핫키)가 실행되고있을때\n단축키(핫키)의 입력을 다른 창에선 사용하지\n못하도록 블럭킹하는 기능입니다.", "HS플레이어 단축키(핫키) 설정", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }
    }
}
