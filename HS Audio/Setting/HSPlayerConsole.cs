﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Windows.Forms;
using HS_Library;
using HS_CSharpUtility.Extension;
using System.IO;

namespace HS_Audio
{
    public partial class frmMain
    {
        /*
        [DllImport("user32.dll")]
        public static extern int FindWindow(string lpClassName, string lpWindowName);*/

        bool IsShowConsole;
        readonly string ConsoleTitle = " Console [" + Process.GetCurrentProcess().Id + "]";
        bool ConsoleFirstLoad;
        private void 디버그창띄우기ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (HSConsole.GetConsoleWindow() == IntPtr.Zero)
            {
                if (HSConsole.NewConsole(Properties.Resources.ConsoleIcon))
                {
                    IsShowConsole = true;
                    Console.Title = "HS Player" + ConsoleTitle;
                    //IntPtr handle_console = new IntPtr(FindWindow(null, "HS " + Title));
                    HSConsole.ConsoleXButton(false);
                    HSConsole.Exit_KeyCancle = true;
                    HSConsole.ConsoleExit += ConsoleExit;

                    HSConsole.Title = LocalString.ProgramName + ConsoleTitle;
                    디버그창띄우기ToolStripMenuItem.Text = "콘솔 창 닫기";

                    HSConsole.Switch = HSConsole.SwitchConsole.MainConsole;
                    //TextWriter tw = 
                    HSConsole.SetInputAvailable();
                    WriteMainConsoleLine("Press \"Help\" to show help");
                    WriteMainConsole();

                    HSConsole.MakeInstance().TextEvent += ConsoleTextEvent;
                    HSConsole.Instance.KeyPressShowConsole = true;

                    HSConsole.Instance.StartTextEvent();
                    //HSConsole.Instance.StartKeyPressEvent();
                    if (!ConsoleFirstLoad) MessageBox.Show("콘솔모드는 플레이어 명령어를 입력하거나 \n플레이어에서 나오는 메세지를 모니터링 할 수도 있습니다.\n\n※콘솔을 강제로 닫으시게 되면 프로그램까지 끝나버리니 주의하시기 바랍니다.\n※콘솔모드는 코드페이지 호환성을 위해 영어로 작성했습니다.", LocalString.ProgramName + ConsoleTitle);
                    HSConsole.BringToFront();
                    ConsoleFirstLoad = true;
                }
                else MessageBox.Show("Fail to load the "+LocalString.ProgramName+ConsoleTitle, LocalString.ProgramName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                if (HSConsole.CloseConsole()){ IsShowConsole = false;디버그창띄우기ToolStripMenuItem.Text = "콘솔 창 띄우기";}
                else MessageBox.Show("Fail to unload the " + LocalString.ProgramName + ConsoleTitle, LocalString.ProgramName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        void ConsoleExit(object sender, HSConsoleExitEventArgs e)
        {
            e.Cancle = true;
            switch (e.MessageType)
            {
                case HSConsole.CtrlTypes.CTRL_C_EVENT: Console.Clear(); CTRL_C_1(); break;
                case HSConsole.CtrlTypes.CTRL_CLOSE_EVENT: e.Cancle = false; HSConsole.CloseConsole(); break;
            }
        }
        void ConsoleCtrl_C_Key_Press(object o, ConsoleCancelEventArgs ca)
        {
            try {ca.Cancel = true; }catch{}
            CTRL_C_1();
        }
        void CTRL_C_1()
        {
            //Console.TreatControlCAsInput = true;
            HSConsole.Switch = HSConsole.SwitchConsole.MainConsole;
            HSConsole.Title = LocalString.ProgramName + ConsoleTitle;

            //Console.Clear();
            WriteMainConsoleLine("Press \"Help\" to show help");
            WriteMainConsole();

            HSConsole.Instance.StartTextEvent();
            HSConsole.Instance.StopKeyPressEvent();
            //ThreadPool.QueueUserWorkItem(new WaitCallback((object o1)=>{Thread.Sleep(300);Console.TreatControlCAsInput = false;}));
        }
        void ConsoleTextEvent(object sender, string Text)
        {
            //WriteMainConsoleLine("Now temporarily can not use the command."); 
            string a1 = Text.Trim();
            string[] a2 = a1.Split(' ');
            switch (a2[0].ToUpper())
            {
                case "MONITOR":case "MON":
                string a = HSConsole.Title;
                string ti = "Press ESC to Exit Monitoring Mode";
                HSConsole.Title = (LocalString.ProgramName + " Console [" + Process.GetCurrentProcess().Id + "]") + " - " + ti;
                HSConsole.Switch = HSConsole.SwitchConsole.SubConsole;
                HSConsole.Instance.KeyPressShowConsole = false;
                //Console.Clear();
                HSConsole.Instance.StartKeyPressEvent();
                HSConsole.WriteLine("Monitoring Mode Start...\n"+ti+"\n");
                HSConsole.Instance.KeyEvent += ((object o, ConsoleKeyInfo Key) => {if (Key.Key == ConsoleKey.Escape)CTRL_C_1(); });
                HSConsole.Instance.StopTextEvent();
                break;
                case "CLEAR": case "CLR":  case "CLS":Console.Clear(); WriteMainConsole(); break;
                case "EXIT": this.InvokeIfNeeded(()=>{디버그창띄우기ToolStripMenuItem_Click(null, null);}); break;
                case "KILL": ExitProgram(); WriteMainConsoleLine("Cancle the kill this player\n");WriteMainConsole(); break;

                case "PLAY": try{Play();}catch(Exception Ex){WriteMainConsoleLine(Ex.ToString());} WriteMainConsole(); break;
                case "PAUSE": try{Pause();}catch(Exception Ex){WriteMainConsoleLine(Ex.ToString());} WriteMainConsole(); break;
                case "STOP": try{Stop();}catch(Exception Ex){WriteMainConsoleLine(Ex.ToString());} WriteMainConsole(); break;
                case "NEXTMUSIC":case "NM": try{this.InvokeIfNeeded(()=>NextMusic());}catch(Exception Ex){WriteMainConsoleLine(Ex.ToString());} WriteMainConsole(); break;
                case "BEFOREMUSIC":case "BM": try{this.InvokeIfNeeded(()=>BeforeMusic());}catch(Exception Ex){WriteMainConsoleLine(Ex.ToString());} WriteMainConsole(); break;
                case "RANDOMMUSIC":case "RDM": try{this.InvokeIfNeeded(()=>RandomMusic());}catch(Exception Ex){WriteMainConsoleLine(Ex.ToString());} WriteMainConsole(); break;
                case "VOLUME":case "VOL": if(a2.Length == 2){try{float g = Convert.ToSingle(a2[1])/100f; if(g>1||g<0)WriteMainConsoleLine("음량은 0.00000 ~ 100.00000 사이에 있어야 합니다.\n");else Helper.Volume = g;}catch(Exception Ex){WriteMainConsoleLine("\n"+Ex.ToString()+"\n");}}else WriteMainConsoleLine("Volume: "+(Helper.Volume*100).ToString()+"\n"); WriteMainConsole(); break;
                case "POSITION":case "POS":
                try
                {
                    if (a2.Length == 2) try{Helper.CurrentPosition = Convert.ToUInt32(a2[1]); }catch{WriteMainConsoleLine("잘못 입력하셨습니다. (예: Position 100,또는 POS 10)\n");}
                    else WriteMainConsoleLine(string.Format("Position: {0} [{1}]\n",Helper.CurrentPosition, ShowTime(Helper.CurrentPosition)));
                }
                catch (Exception Ex){WriteMainConsoleLine(Ex.Message); }
                WriteMainConsole(); break;

                case "SHOW":
                if (a2.Length < 2) WriteMainConsoleLine("Please input \"Show Help\"");
                else
                    switch (a2[1].ToUpper())
                    {
                        case "STAT": case "STATUS":WriteMainConsoleLine(string.Format("\n=========Show Status========\nStatus: {0}\nCurrentPosition: {1} ({2})\nTotalPosition: {3} ({4})\nVolume: {5}\nFrequency: {6} Hz\nMusic: {7}\nMusicName: {8}\nMusicPath: {9}\n", Helper.PlayStatus, Helper.CurrentPosition, ShowTime(Helper.CurrentPosition), Helper.TotalPosition, ShowTime(Helper.TotalPosition),
                             Helper.Volume * 100, Helper.Frequency, tag!=null?tag.Title:null, Path.GetFileName(Helper.MusicPath), Helper.MusicPath)); break;
                        case "?" : case "HELP":WriteMainConsoleLine("Available Command: Show Stat, Show Status, Show Music [what]: Show Music ?\n");break;
                        case "MUSIC" :
                            if (a2.Length < 3) WriteMainConsoleLine("Please input \"Show Music Help\"");
                            else
                                switch (a2[2].ToUpper())
                                {
                                    case "TAG": WriteMainConsoleLine("준비중 입니다~ (Show Music Tag)"); break;
                                    case "HELP":case "?": WriteMainConsoleLine("Available Command: Show Music Tag\n"); break;
                                    default: WriteMainConsoleLine("명령어를 잘못 입력하셨습니다. (" + a2[2] + ")\r\nShow Music ? 를 눌러서 사용가능한 명령어에대해 확인해보세요\n"); break;
                                }
                                break;
                        default:WriteMainConsoleLine("명령어를 잘못 입력하셨습니다. (" + a2[1] + ")\r\nShow ? 를 눌러서 사용가능한 명령어에대해 확인해보세요\n");break;
                    }
                    WriteMainConsole();break;

                case "RESTART": 프로그램다시시작ToolStripMenuItem_Click(null, null);WriteMainConsole(); break; 
                case "HELP": WriteMainConsoleLine("=========Help(도움말)=========\nLarge / lower case is not distinction (대/소문자 구별 안합니다.)\n\nAvailable command (사용 가능한 명령어): \n Start Monitoring Mode: Monitor, Mon\n Clear screen: Clear, clr, cls\n Kill this player: Kill\n Exit this colsole: Exit\n Restart this Player: Restart\n\n" +
                    " Get volume: Volume, Vol\n Set volume: Volume [Num], Vol [Num] (ex: Volume 10.56, Vol 10.56)\n Get position: Position, Pos\n Set position: Position [Num], Pos [Num] (ex: Position 10, Pos 10)\n\n"+
                    " Play music: Play\n Pause music: Pause\n Stop music: Stop\n Next music: NextMusic, NM\n Before music: BeforeMusic, BM\n Random music: RandomMusic, RDM\n\n Show [what]: Show Help, Show ?\n"); WriteMainConsole(); break;
                case "":WriteMainConsoleLine(Clipboard.GetText());break;
                case "": WriteMainConsole(); break;
                default: WriteMainConsoleLine("명령어를 잘못 입력하셨습니다. (" + Text + ")\r\nHelp 를 눌러서 사용가능한 명령어에대해 확인해보세요\n"); WriteMainConsole(); break;
            }
        }
        void WriteMainConsoleLine(string Text){HSConsole.WriteLine(Text, HSConsole.SwitchConsole.MainConsole);}
        void WriteMainConsole(string Text = ">"){HSConsole.Write(Text, HSConsole.SwitchConsole.MainConsole);/*Console.SetCursorPosition(Text.Length, Console.CursorTop);*/}
    }
}
