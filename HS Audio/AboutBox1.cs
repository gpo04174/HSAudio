﻿/*
 마지막 업데이트:  [양력 {2014년 05월 04일 일요일}]
                         (음력 {2014년 04월 06일 (평달))
                         (음력간지 {갑오년 기사월 을해일})


              이 프로그램은 FMOD를 바탕으로 
         제작되었으며 상업용으로 쓸 수 없습니다.

Copyright ⓒ, 박홍식 2013~2016  All right Reserved
 */
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;

using System.Reflection;
using System.Windows.Forms;
using System.Drawing.Drawing2D;

namespace HS_Audio
{
    partial class AboutBox1 : Form
    {
        public AboutBox1()
        {
            InitializeComponent();
            this.Text = string.Format("{0} {1} (Made by 박홍식)", global::HS_Audio.Properties.Resources.PlayerName, global::HS_Audio.Properties.Resources.PlayerVersion); 
            label1.Text = global::HS_Audio.Properties.Resources.PlayerIntro;
        }

        #region 어셈블리 특성 접근자

        public string AssemblyTitle
        {
            get
            {
                object[] attributes = Assembly.GetExecutingAssembly().GetCustomAttributes(typeof(AssemblyTitleAttribute), false);
                if (attributes.Length > 0)
                {
                    AssemblyTitleAttribute titleAttribute = (AssemblyTitleAttribute)attributes[0];
                    if (titleAttribute.Title != "")
                    {
                        return titleAttribute.Title;
                    }
                }
                return System.IO.Path.GetFileNameWithoutExtension(Assembly.GetExecutingAssembly().CodeBase);
            }
        }

        public string AssemblyVersion
        {
            get
            {
                return Assembly.GetExecutingAssembly().GetName().Version.ToString();
            }
        }

        public string AssemblyDescription
        {
            get
            {
                object[] attributes = Assembly.GetExecutingAssembly().GetCustomAttributes(typeof(AssemblyDescriptionAttribute), false);
                if (attributes.Length == 0)
                {
                    return "";
                }
                return ((AssemblyDescriptionAttribute)attributes[0]).Description;
            }
        }

        public string AssemblyProduct
        {
            get
            {
                object[] attributes = Assembly.GetExecutingAssembly().GetCustomAttributes(typeof(AssemblyProductAttribute), false);
                if (attributes.Length == 0)
                {
                    return "";
                }
                return ((AssemblyProductAttribute)attributes[0]).Product;
            }
        }

        public string AssemblyCopyright
        {
            get
            {
                object[] attributes = Assembly.GetExecutingAssembly().GetCustomAttributes(typeof(AssemblyCopyrightAttribute), false);
                if (attributes.Length == 0)
                {
                    return "";
                }
                return ((AssemblyCopyrightAttribute)attributes[0]).Copyright;
            }
        }

        public string AssemblyCompany
        {
            get
            {
                object[] attributes = Assembly.GetExecutingAssembly().GetCustomAttributes(typeof(AssemblyCompanyAttribute), false);
                if (attributes.Length == 0)
                {
                    return "";
                }
                return ((AssemblyCompanyAttribute)attributes[0]).Company;
            }
        }
        #endregion

        private void okButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void pictureBox1_MouseClick(object sender, MouseEventArgs e)
        {
            
        }
        static readonly Bitmap bmp = global::HS_Audio.Properties.Resources.Chunibyo_Icon_48;
        Bitmap bmp1 = global::HS_Audio.Properties.Resources.Chunibyo_Icon_48;
        Graphics g = Graphics.FromImage(bmp);
        private Bitmap rotateImage360(float angle)
        {
            //create a new empty bitmap to hold rotated image
            Graphics GFX = g;

            //move rotation point to center of image
            GFX.TranslateTransform((float)bmp1.Width / 2, (float)bmp1.Height / 2);
            //rotate
            GFX.RotateTransform(angle);
            //move image back
            GFX.TranslateTransform(-(float)bmp1.Width / 2, -(float)bmp1.Height / 2);
            //GFX.Clear(Color.Transparent);
            //GFX.DrawImage(Properties.Resources.testimg, new PointF(0, 0));
            //draw passed in image onto graphics object
            GFX.DrawImage(bmp1, 0, 0);
            return bmp1;
        }
        private Bitmap rotateImage(Bitmap returnBitmap, float angle)
        {
            //create a new empty bitmap to hold rotated image
            Graphics GFX = Graphics.FromImage(returnBitmap);
            //move rotation point to center of image
            GFX.TranslateTransform((float)returnBitmap.Width / 2, (float)returnBitmap.Height / 2);
            //rotate
            GFX.RotateTransform(angle);
            //move image back
            GFX.TranslateTransform(-(float)returnBitmap.Width / 2, -(float)returnBitmap.Height / 2);
            //GFX.Clear(Color.Transparent);
            //GFX.DrawImage(Properties.Resources.testimg, new PointF(0, 0));
            //draw passed in image onto graphics object
            GFX.DrawImage(returnBitmap, new Point(0, 0));
            return returnBitmap;
        }
        private void rotateImage(Image hands, float angle)
        {
            Graphics GFX = Graphics.FromImage(hands);
            Matrix matrix = new Matrix();
            // 이미지 중심축
            Point center = new Point((int)hands.Width / 2, (int)hands.Height / 2);
            // 회전각
            matrix.RotateAt(angle, center);
            GFX.Transform = matrix;
            GFX.Clear(Color.Transparent); // 회전하기전의 이미지 클리어
            // 드로잉
            GFX.DrawImage(hands, new PointF(0, 0));
            //GFX.Dispose();
        }

        private void button1_Click(object sender, EventArgs e)
        {
        }

        private void AboutBox1_Load(object sender, EventArgs e)
        {

        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            SaveFileDialog sv = new SaveFileDialog() 
            { Title = "사운드 시스템 엔진 업데이트 내역 저장",
              Filter="텍스트 파일 (*.txt)|*.txt|모든 파일 (*.*)|*.*",DefaultExt="*.txt",
              FileName = "사운드 시스템 엔진 업데이트 (2014-04-28 v4.44.33).txt"};
            if(sv.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {System.IO.File.WriteAllText(sv.FileName, global::HS_Audio.Properties.Resources.revision_4_44);}
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            System.Diagnostics.Process.Start("http://blog.naver.com/gpo04174");
        }

        private void linkLabel2_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            System.Diagnostics.Process.Start("http://cafe.naver.com/hstmplayer");
        }
    }
}
