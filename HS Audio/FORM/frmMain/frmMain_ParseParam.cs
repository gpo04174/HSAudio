﻿using HS_Audio.Languge;
using HS_Audio.Lyrics;
using HS_CSharpUtility.Extension;
using Huseyint.Windows7.WindowsForms;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace HS_Audio
{
    partial class frmMain
    {
        internal Mutex mtx;
        internal bool NewInstance;

        internal bool NoDelete = true;
        internal string tmpProfileSetting, tmpPlaylist;

        public frmMain(string[] args)
        {
            sm_check.Create(1, "HS_Audio_Add_PlayList_Check");
            sm.Create(5000, "HS_Audio_Add_PlayList");
            //MessageBox.Show(args[0].ToLower());
            bool CreateNew = true;
            mtx = new Mutex(true, "HS_Audio", out CreateNew);
            Mutex PlaylistMutex;
            long index = -1;

            #region 인자 파싱
            if (args.Length > 0)
            {
                if (HS_CSharpUtility.Utility.ArrayUtility.IsArrayItem(args, "/Restart", out index, true))
                {
                    bool Match = true;
                    try
                    {
                        int PID = Convert.ToInt32(args[index + 1]);
                        while (Match)
                        {
                            Match = Process.GetProcessById(PID) != null;
                            if (HS_CSharpUtility.Utility.ArrayUtility.IsArrayItem(args, "--Timeout", out index, true))
                                Thread.Sleep(Convert.ToInt32(args[index + 1]));
                            else Thread.Sleep(100);
                        }
                    }
                    catch { }
                    finally { CreateNew = true; }
                }
                StringBuilder sb = null;
                if (HS_CSharpUtility.Utility.ArrayUtility.IsArrayItem(args, "/CommandLineStart", true) ||
                    HS_CSharpUtility.Utility.ArrayUtility.IsArrayItem(args, "/CommadLineStart", true))
                {
                    sb = new StringBuilder();
                    for (int i = 0; i < args.Length; i++)
                        if ((args[i].ToUpper() == "/COMMANDLINESTART") || (args[i].ToUpper() == "-COMMANDLINESTART") || (args[i].ToUpper() == "COMMANDLINESTART") ||
                            (args[i].ToUpper() == "/COMMADLINESTART") || (args[i].ToUpper() == "-COMMADLINESTART") || (args[i].ToUpper() == "COMMADLINESTART")) continue;
                        else sb.Append((args[i].IndexOf(" ") == -1 ? args[i] : "\"" + args[i] + "\"") + " ");
                    //try { mtx.ReleaseMutex(); } catch { }
                    Process.Start(Process.GetCurrentProcess().MainModule.FileName, sb.ToString());
                    Kill();
                }
                else if (CreateNew || HS_CSharpUtility.Utility.ArrayUtility.IsArrayItem(args, "/NewInstance", true) || HS_CSharpUtility.Utility.ArrayUtility.IsArrayItem(args, "/CreateNew", true))
                {
                    NewInstance = true; bool suc = UpdateSetting(false, true); ParseParam(args);
                }
                 /*
                else if (!CreateNew && !(Directory.Exists(args[0]) || File.Exists(args[0]) || args[0] == "/add"))
                {
                }*/
                else
                { 
                    sb = new StringBuilder();
                    int off = args[0] == "/add"?1:0;
                    if ((File.Exists(args[off])|| Directory.Exists(args[off])) && !CreateNew)
                    {
                        for (int i = 0; i < args.Length; i++)
                        {
                            sb.AppendLine(args[i]);
                        }

                        //while (ba!=""||ba!=null) { sm.ReadString(ref ba); Thread.Sleep(500); }
                        while (sm_check.ReadByte(0) != 0)
                        {
                            Thread.Sleep(10);
                        }

                        int timeout = 75;
                        while (timeout > 0)
                        {
                            timeout--;
                            if (sm_check.ReadByte(0) == 0)
                            {
                                bool Writed = sm.WriteString(sb.ToString());
                                sm_check.WriteByte(1, 0);
                                break;
                                //string a = ""; sm.ReadString(ref a); MessageBox.Show(a);
                                //otifyIcon1.Dispose();
                            }
                            else Thread.Sleep(250);
                        }
                        Kill();
                    }
                    else if (!CreateNew)
                    {
                        MessageBox.Show(args[0]);
                        MessageBox.Show(args.Length > 0?
                            "지원하지 않거나 틀린 파라메타(매개변수) 입니다.":
                            "기존의 플레이어를 닫고 다시 실행해 주십시오.\n\n안닫고도 실행하고 싶으시다면 /CreateNew 나 /NewInstance를\n인수로 맨앞에 붙여주시기 바랍니다.",
                            "HS 플레이어 실행 오류", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        Kill();
                    }
                    else
                    {
                        if (!UpdateSetting(false, true)) base.AutoScaleMode = this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
                        ParseParam(args);
                    }
                }
            }
            else
            {
                if (!CreateNew)
                {
                    //MessageBox.Show(args[0]);
                    MessageBox.Show(args.Length > 0 ?
                        "지원하지 않거나 틀린 파라메타(매개변수) 입니다." :
                        "기존의 플레이어를 닫고 다시 실행해 주십시오.\n\n안닫고도 실행하고 싶으시다면 /CreateNew 나 /NewInstance를\n인수로 맨앞에 붙여주시기 바랍니다.",
                        "HS 플레이어 실행 오류", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    Kill();
                }
                else { InitializeComponent();/*if(!UpdateSetting(false, true))base.AutoScaleMode = this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None; CustomFont = this.Font;*/ }
            }

            AddPlaylist_ThreadStart = new ThreadStart(SharedMemory_Thread);
            AddPlaylist_Thread = new Thread(AddPlaylist_ThreadStart);
            Init_StringBuilder();
            //AddPlaylist_Thread.Start();
            timer_파일추가.Start();
            #endregion
            Program.localeManager.LocaleChanged += new LocaleManager.LocaleChangesEventhandler(localeManager_LocaleChange);
            bool Success = UpdateSetting(false);
            if (Environment.OSVersion.Version.Major >= 6 && Environment.OSVersion.Version.Minor >= 1)
            {
                try
                {
                    Huseyint.Windows7.WindowsForms.TaskBarExtensions.Attach();
                    this.thumbnailBarButtons = new List<ThumbnailBarButton>(){
                    new ThumbnailBarButton(Properties.Resources.Previous_32, "이전", false, true, false, true),
                    new ThumbnailBarButton(Properties.Resources.Play_29, "재생", false, true, false, true),
                    new ThumbnailBarButton(Properties.Resources.Stop_29, "정지", false, true, false, true),
                    new ThumbnailBarButton(Properties.Resources.Next_32, "다음", false, true, false, true)};
                    thumbnailBarButtons[0].Click += this.이전ToolStripMenuItem_Click;
                    thumbnailBarButtons[1].Click += this.button1_Click;//this.재생ThumbnailBarButton_Click;
                    thumbnailBarButtons[2].Click += this.button2_Click;//this.정지ThumbnailBarButton_Click;
                    thumbnailBarButtons[3].Click += this.다음ToolStripMenuItem_Click;//this.다음ThumbnailBarButton_Click;

                    ThreadPool.QueueUserWorkItem(new WaitCallback(AddAddThumbnailBarButton_Thread), this.Handle);
                }
                catch { }
            }
            if (IsDebugging) this.Text = this.Text.Insert(this.Text.Length, " (디버깅)");
            //MessageBox.Show(IsDebugging.ToString());
            frmlrcSearch.GetLyricsComplete += new Lyrics.frmLyricsSearch.GetLyricsCompleteEventHandler(frmlrcSearch_GetLyricsComplete);
            frmlrcSearch.GetLyricsFailed += new frmLyricsSearch.GetLyricsFailedEventHandler(lrcf.LyricsFailedException);
            lrcf.Lyricscache = Lyricscache;
            lrcf.FormStatusChange += new Lyrics.frmLyricsDesktop.FormStatusChangeEventHandler(lrcf_FormStatusChange);
            lrcf.UpdateLyricsIntervalChange += new Lyrics.frmLyricsDesktop.UpdateLyricsIntervalChangeEventHandler(lrcf_UpdateLyricsIntervalChange);
        }

        internal void ParseParam(string[] args)
        {
            InitializeComponent();
            //폰트 초기화
            CustomFont = this.Font;

            for (int i = 0; i < args.Length; i++)
            {
                #region 파일이 존재하면
                try
                {
                    bool Error = false;
                    try
                    {
                        if (Directory.Exists(args[i]))
                        {
                            //if (e.KeyState == (byte)KeyState_DragEvent.Ctrl_Left)폴더열기(null, a[i], 하위폴더검색ToolStripMenuItem.Checked); 
                            //else 폴더열기(pt, a[i], 하위폴더검색ToolStripMenuItem.Checked);
                            폴더열기(pt, args[i], 하위폴더검색ToolStripMenuItem.Checked);
                        }
                        else if (System.IO.Path.GetExtension(args[i].Replace("\"", "")).ToLower() == ".m3u")
                        { if (HS_CSharpUtility.Utility.ArrayUtility.IsArrayItem(args, "/Restart", true)) tmpPlaylist = args[i].Replace("\"", ""); 목록열기(pt, args[i].Replace("\"", "")); }
                        //else if(Path.GetExtension(args[i])=="proj"){}
                        else if (File.Exists(args[i]) &&
                                 !HS_CSharpUtility.Utility.ArrayUtility.IsArrayItem(args, "/Restart", true))
                        /*
                        (!HS_CSharpUtility.Utility.ArrayUtility.IsArrayItem(args, "/CommandLineStart", true)||
                        !HS_CSharpUtility.Utility.ArrayUtility.IsArrayItem(args, "/CommadLineStart", true))
                         */
                        {
                            if (Path.GetExtension(args[i]) != ".proj" && File.Exists(args[i]))
                            {
                                //파일열기(null, new string[]{args[i]});

                                //timer1.Start();
                                CurrentItem = new ListViewItem(listView1.Items.Count.ToString());
                                if (listView1.Items.Count == 0)
                                {
                                    Helper = new HS_Audio.HSAudioHelper(getSettingDeviceFormat()); Helper.PreLoading = PreLoading; trackBar1.Enabled = true;
                                    Helper.PlayingStatusChanged += new HS_Audio.HSAudioHelper.PlayingStatusChangedEventHandler(fh_PlayingStatusChanged);
                                    Helper.PlayingStatusChanged += new HSAudioHelper.PlayingStatusChangedEventHandler(lrcf.PlayingStatusChanged);
                                    Helper.MusicChanging += new HSAudioHelper.MusicChangeEventHandler(fh_MusicPathChanged);
                                    PreLoading:
                                    Helper.PreLoading = PreLoadingCheck;
                                    Helper.MusicPath = args[i];
                                    ps = new PlayerSetting(Helper, System.IO.Path.GetFileName(Helper.MusicPath), this);
                                    //fm.Add(fh);
                                    Helper.index = listView1.Items.Count;
                                    CurrentItem.SubItems.Add(LocalString.현재 + " (Ready)");
                                    if (ps == null) { ps = new PlayerSetting(Helper, System.IO.Path.GetFileName(Helper.MusicPath)); }
                                    ps.Title = string.Format("{0}", System.IO.Path.GetFileName(Helper.MusicPath));
                                    btn재생.Enabled = btn정지.Enabled = btn재생설정.Enabled = true;
                                }
                                else { CurrentItem.SubItems.Add(""); }
                                CurrentItem.SubItems.Add(System.IO.Path.GetFileName(args[i]));
                                CurrentItem.SubItems.Add(args[i]);
                                CurrentItem.SubItems.Add("");

                                CurrentItem.Font = CustomFont;
                                listView1.Items.Add(CurrentItem);
                                button1_Click(null, null);
                            }
                        }
                    }
                    catch (Exception ex) { timer1_Tick_sb.AppendLine(args[i]); HS_CSharpUtility.LoggingUtility.LoggingT(ex); Error = true; }
                }
                finally
                {
                    if (timer1_Tick_sb.Length > 0)
                    {
                        MessageBox.Show("일부항목은 파일이 존재하지 않거나\n올바르지 않은 음악 파일 이므로 추가하지 못했습니다.\n" +
                            "더 자세한 정보를 알고싶다면 로그를 분석해주시기 바랍니다.\n\n추가 못한 경로:\n" + timer1_Tick_sb.ToString(),
                            "일부 항목 추가실패!");
                        timer1_Tick_sb.Clear();
                    }
                }
                #endregion
                #region /mixing 인자를 주었을때
                if (args[i].ToLower() == "/mixing")
                {
                    panel1.Enabled = true;
                    cb반복.Visible = false;
                    음악믹싱모드ToolStripMenuItem.Checked = true;
                    cb반복.Visible = false;
                    btn재생설정.Text = "음향효과";
                    현재위치의웨이브저장ToolStripMenuItem.Visible = toolStripSeparator2.Visible = true;
                    파일로웨이브저장ToolStripMenuItem.Visible = toolStripSeparator7.Visible = false;
                    파일로웨이브저장ToolStripMenuItem.Text = "저장";
                    현재위치의웨이브저장ToolStripMenuItem.Text = "파일로 현재 재생위치의 웨이브정보 저장";
                    파일로웨이브저장ToolStripMenuItem.Text = "웨이브정보 저장";
                    columnHeader5.Width = 139;
                    //panel2.Location = new Point(167, 52);
                    cb반복.Visible = false;
                    button5.Visible = true;
                    columnHeader2.Width = 50;
                    columnHeader4.Width = 223;
                    안전모드ToolStripMenuItem.Visible = false;
                    //listView1.GridLines = false;
                }
                #endregion
                #region /repairsoundsystem 인자를 주었을떄
                int copycount = 0;
                if (args[i].ToLower() == "/repairsoundsystem")
                {
                    repair:
                    try
                    {
                        Program.DLLCopy_SoundEngin(false); Program.DLLCopy_SoundEngin(true);
                        copycount++;
                    }
                    catch (Exception ex)
                    {
                        if (copycount < 10) { System.Threading.Thread.Sleep(500); goto repair; }
                        else
                        {
                            MessageBox.Show("사운드 시스템 엔진 파일 복구를 실패하여 프로그램을 종료합니다.\n\n예외:\n " + ex.ToString(),
                                "사운드 시스템 엔진 파일 복구실패 알림", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            return;
                        }
                    }
                }
                #endregion
            }
            if (listView1 != null && listView1.Items.Count > 0)
            {
                long tmp;
                try
                {
                    if (HS_CSharpUtility.Utility.ArrayUtility.IsArrayItem(args, "-Index", out tmp, true))
                    {
                        int id = 0; try { id = Convert.ToInt32(args[tmp + 1]); } catch { }
                        CurrentItem = listView1.Items[id];
                        if (Helper == null)
                        {
                            //fm.Add(new HS_Audio_Helper.HSAudioHelper(listView1.SelectedItems[id].SubItems[3].Text));
                            Helper = new HS_Audio.HSAudioHelper(getSettingDeviceFormat()); Helper.PreLoading = PreLoading; trackBar1.Enabled = true;
                            Helper.PlayingStatusChanged += new HSAudioHelper.PlayingStatusChangedEventHandler(fh_PlayingStatusChanged);
                            Helper.MusicChanging += new HSAudioHelper.MusicChangeEventHandler(fh_MusicPathChanged);
                            PreLoading:
                            Helper.PreLoading = PreLoadingCheck;
                            Helper.MusicPath = listView1.SelectedItems[id].SubItems[3].Text;
                        }
                        else { Helper.MusicPath = CurrentItem.SubItems[3].Text; if (ps != null) { ps.Helper = Helper; ps.numPitch_ValueChanged(null, null); } else { ps = new PlayerSetting(Helper); } }
                        if (frmdsp != null) frmdsp.ReRegister(false);
                        //fm[0].Frequency = fm[0].DefaultFrequency + Frequency;
                        //try { ps.numFrenq.Value = (decimal)(fm[0].DefaultFrequency + Frequency); }
                        //catch { if (fm[0].Frequency < 0) ps.numFrenq.Minimum = (decimal)fm[0].Frequency; else ps.numFrenq.Maximum=(decimal)fm[0].Frequency; }
                        for (int j = 0; j < listView1.Items.Count; j++) { listView1.Items[j].Text = j.ToString(); listView1.Items[j].SubItems[1].Text = ""; }
                        listView1.Items[id].SubItems[1].Text = LocalString.현재 + " (Ready)";
                        //btn재생.Text = LocalString.일시정지;
                        btn재생.BackgroundImage = Properties.Resources.PauseHS;
                        Helper.index = int.Parse(listView1.Items[id].Text);
                        ps.Title = string.Format("{0}", System.IO.Path.GetFileName(Helper.MusicPath));
                        btnItemFocus_Click(null, null);
                    }
                }
                finally
                {
                    btn재생.Enabled = btn정지.Enabled = btn재생설정.Enabled = true;
                    timer1.Start();
                    StopNextMusicThread = false;

                    tmpStop = false;
                    Init_Sort(false);
                }

                try
                {
                    if (HS_CSharpUtility.Utility.ArrayUtility.IsArrayItem(args, "-Position", out tmp, true) && Helper != null)
                    {
                        uint Pos = Convert.ToUInt32(args[tmp + 1]);
                        if (Helper.TotalPosition >= Pos) Helper.CurrentPosition = Pos;
                    }
                }
                catch { }
                try
                {
                    if (HS_CSharpUtility.Utility.ArrayUtility.IsArrayItem(args, "-Project", out tmp, true) && Helper != null)
                    {
                        tmpProfileSetting = args[tmp + 1].Replace("\"", "");
                        string[] Profile = File.ReadAllLines(tmpProfileSetting);
                        if (ps == null) ps = new PlayerSetting(Helper);
                        ps.Setting = HS_CSharpUtility.Utility.EtcUtility.LoadSetting(Profile);
                    }
                }
                catch { }
                try { if (HS_CSharpUtility.Utility.ArrayUtility.IsArrayItem(args, "/Play", true) && Helper != null) { Helper.Play(); } } catch { }
                try { NoDelete = !HS_CSharpUtility.Utility.ArrayUtility.IsArrayItem(args, "/DeleteProject", true); } catch { }
            }
            try { AndroidCursor = HS_Audio.Properties.Resources.AndroidCursor.CreateCursor(); } catch { }
            //try { File.Delete(tmpProfileSetting); }catch{}try{File.Delete(tmpPlaylist);}catch{}
        }

        static void Kill()
        {
            Process p = Process.GetCurrentProcess();
            try {Process.Start(p.MainModule.FileName, "--KillProcess -PID " + p.Id);}catch {}finally  { p.Kill(); }
        }
    }
}
