﻿using System;
using HS_Audio.Languge;
using HS_Audio;
using System.Windows.Forms;
using System.Collections.Generic;
using HS_Audio.Plugin;

namespace HS_Audio
{
    partial class frmMain
    {

        enum KeyState_DragEvent_DragOver : byte
        {
            None = 1, Ctrl_Left = 9, Alt_Left = 33, Shift = 5,
            Ctrl_Alt = 41, Ctrl_Shift = 13, Alt_Shift = 37, Ctrl_Alt_Shift = 45
        }
        enum KeyState_DragEvent_DragDrop : byte
        {
            None = 1, Ctrl_Left = 8, Alt_Left = 33, Shift = 5,
            Ctrl_Alt = 41, Ctrl_Shift = 13, Alt_Shift = 37, Ctrl_Alt_Shift = 45
        }

        private void 재생ThumbnailBarButton_Click(object sender, EventArgs e)
        {

        }
        private void 정지ThumbnailBarButton_Click(object sender, EventArgs e)
        {

        }

        private void 다음ThumbnailBarButton_Click(object sender, EventArgs e)
        {
            NextMusic();
        }

        internal List<IHSAudioPlugin> Plugins = new List<IHSAudioPlugin>();

        public void CheckEnablePlugin()
        {
             for(int i = 0;i<플러그인PToolStripMenuItem.DropDownItems.Count;i++)
             {try{플러그인PToolStripMenuItem.DropDownItems[i].Enabled = Plugins[i].IsEnabled;}catch{}}
        }
        public void RegisterInstance()
        {
            foreach (var a in Plugins)
                if (a != null){try{a.Helper = Helper!=null?Helper:fm[0];a.LocaleChanged(this.local);}catch{}}
        }


        public bool Register(IHSAudioPlugin ipi)
        {
            //MenuItem mn = new MenuItem(ipi.Name, new EventHandler(NewLoad));
            //MessageBox.Show("Register");
            HS_Library.HSConsole.WriteLine("Plugin Registered Successfully!!: " + ipi.Name);
            ipi.Closing = false;
            //if (ipi.Icon != null) 플러그인PToolStripMenuItem.DropDownItems.Add(new ToolStripMenuItem(ipi.Name, ipi.Icon, new EventHandler(NewLoad)));
            //else 
            플러그인PToolStripMenuItem.DropDownItems.Add(new ToolStripMenuItem(ipi.Name, null, new EventHandler(NewLoad)));
            return true;
        }
        public void Command(CommandStatus cmd)
        {
            switch (cmd)
            {
                case CommandStatus.Stop:Stop();break;
                case CommandStatus.Pause:Pause();break;
                case CommandStatus.Next:NextMusic();break;
                default:Play();break;
            }
        }
        public void Command(CommandStatus cmd, int index)
        {
            switch (cmd)
            {
                case CommandStatus.Stop: Stop(index); break;
                case CommandStatus.Pause: Pause(index); break;
                case CommandStatus.Next: NextMusic(); break;
                default: Play(index); break;
            }
        }


        private void NewLoad(object sender, System.EventArgs e)
        {
            ToolStripMenuItem mn = (ToolStripMenuItem)sender;
            for (int i = 0; i < Plugins.Count; i++)
            { if (Plugins[i] != null) { if (Plugins[i].Name == mn.Text) { Plugins[i].Show(); break; } } }
            //플러그인PToolStripMenuItem.DropDownItems.Add(new ToolStripMenuItem(ipi.Name, null, new EventHandler(NewLoad)));
        }
        private void PluginInit(bool Closing=false)
        {
            //MessageBox.Show("PluginInit");
            if (Plugins != null)
            {
                for (int i = 0; i < Plugins.Count; i++)
                {
                    try
                    {
                        //string strType = mn.Text;
                        if (Plugins[i] != null)
                        {
                            Plugins[i].Helper = Helper!=null?Helper:fm[0];
                            Plugins[i].Host = this;
                            try { Plugins[i].LocaleChanged(this.local); } catch { }
                            Plugins[i].Closing = Closing;
                            Plugins[i].NewCreate();
                        }
                    }
                    catch { }
                }
            }
        }

        internal LocaleString LocalString = new ko_kr();
        internal Locale local = Locale.ko_kr;

        public void localeManager_LocaleChange(Locale local)
        {
            this.local = local;
            string PN = LocalString.ProgramName;

            LocaleString a = LocalString = LocaleManager.GetLocaleString(local);
            this.ProgramName = a.ProgramName + " " + a.ProgramArthur;
            this.Text = this.ProgramName;
            this.toolTip1.SetToolTip(this.textBox3, a.검색어를입력하세요);

            string a1 =label1.Text.Split(' ')[0];
            label1.Text = a1 + " " + a.개;
            #region 콘솔 언어 적용
            try{if(Console.Title!=""&& Console.Title!=null) Console.Title = Console.Title.Replace(PN, a.ProgramName);}catch{}
            #endregion
            #region 언어 적용
            this.파일FToolStripMenuItem.Text = this.파일FToolStripMenuItem1.Text = a.파일FToolStripMenuItem + "(&F)";
            this.트레이로보내기ToolStripMenuItem.Text = a.트레이로보내기ToolStripMenuItem;
            this.열기OToolStripMenuItem.Text = a.열기OToolStripMenuItem;
            this.파일열기ToolStripMenuItem.Text = a.파일열기ToolStripMenuItem;
            this.폴더열기ToolStripMenuItem.Text = this.폴더열기ToolStripMenuItem1.Text = a.폴더열기ToolStripMenuItem;
            this.재생목록열기ToolStripMenuItem.Text = a.재생목록열기ToolStripMenuItem;
            this.재생목록다른이름으로저장ToolStripMenuItem.Text = a.재생목록다른이름으로저장ToolStripMenuItem;
            this.파일로웨이브저장ToolStripMenuItem.Text = a.파일로웨이브저장ToolStripMenuItem;
            this.현재상태저장ToolStripMenuItem.Text = 현재상태저장ToolStripMenuItem1.Text = a.현재상태저장;
            this.원본사이즈사용ToolStripMenuItem.Text = a.원본사이즈사용ToolStripMenuItem;
            this.끝내기ToolStripMenuItem.Text = a.끝내기;

            this.설정ToolStripMenuItem.Text = 설정TToolStripMenuItem.Text = a.설정ToolStripMenuItem;
            this.재생목록창꾸미기ToolStripMenuItem.Text = a.재생목록창꾸미기ToolStripMenuItem;
             this.모눈선표시ToolStripMenuItem.Text = a.모눈선표시ToolStripMenuItem;
             this.선택한항목강조표시ToolStripMenuItem.Text = a.선택한항목강조표시ToolStripMenuItem;
             this.배경색상설정ToolStripMenuItem.Text = a.배경색상설정ToolStripMenuItem;
             this.삽입색상설정ToolStripMenuItem.Text=a.삽입색상설정ToolStripMenuItem;
             this.이미지불러오기ToolStripMenuItem.Text = a.이미지불러오기ToolStripMenuItem;
              this.타일보기ToolStripMenuItem.Text = a.타일보기ToolStripMenuItem;
            this.ImageDialog.Title = a.ImageDialog_Title;
            this.ImageDialog.Filter = a.ImageDialog_Filter;
             this.지우기ToolStripMenuItem.Text = a.지우기ToolStripMenuItem;
            this.맨위로설정ToolStripMenuItem.Text = a.맨위로설정;
            this.밀리초표시ToolStripMenuItem.Text = a.밀리초표시;
            this.핫키설정ToolStripMenuItem.Text=a.핫키설정ToolStripMenuItem;
            this.안전모드ToolStripMenuItem.Text = a.안전모드ToolStripMenuItem;
            this.asyncToolStripMenuItem.Text = a.syncToolStripMenuItem;
            this.음악믹싱모드ToolStripMenuItem.Text = a.음악믹싱모드ToolStripMenuItem;
            this.싱크가사로컬캐시ToolStripMenuItem.Text = a.싱크가사로컬캐시ToolStripMenuItem;
            싱크가사캐시정리ToolStripMenuItem.Text = a.싱크가사캐시정리ToolStripMenuItem;
            태그없는항목자동삽입ToolStripMenuItem.Text = a.태그없는항목자동삽입ToolStripMenuItem;
            싱크가사캐시관리ToolStripMenuItem.Text = a.싱크가사캐시관리ToolStripMenuItem;
            this.싱크가사우선순위ToolStripMenuItem.Text = a.싱크가사우선순위ToolStripMenuItem;
            this.파일복구및재설치ToolStripMenuItem.Text = 파일복구및재설치ToolStripMenuItem1.Text = a.파일복구및재설치ToolStripMenuItem;
            this.프로그램다시시작ToolStripMenuItem.Text = 프로그램다시시작ToolStripMenuItem1.Text = a.프로그램다시시작ToolStripMenuItem;
             this.toolTip1.SetToolTip(this.btn프로그램다시시작, a.프로그램다시시작ToolStripMenuItem);
            this.우선순위ToolStripMenuItem.Text = toolStripMenuItem7.Text = a.우선순위ToolStripMenuItem;
            높음ToolStripMenuItem.Text = 높음ToolStripMenuItem1.Text = a.높음ToolStripMenuItem;
            높은우선순위ToolStripMenuItem.Text = 높은우선순위ToolStripMenuItem1 .Text= a.높은우선순위ToolStripMenuItem;
            보통권장ToolStripMenuItem.Text = 보통권장ToolStripMenuItem1 .Text= a.보통권장ToolStripMenuItem;
            낮은우선순위ToolStripMenuItem.Text = 낮은우선순위ToolStripMenuItem1.Text = a.낮은우선순위ToolStripMenuItem;
            this.가비지커렉션가동ToolStripMenuItem.Text = a.가비지커렉션가동ToolStripMenuItem;
            this.언어ToolStripMenuItem.Text = toolStripMenuItem5.Text = a.언어;
            this.기본프로그램으로설정ToolStripMenuItem.Text = a.기본프로그램으로설정ToolStripMenuItem;

            this.보기VToolStripMenuItem.Text = 보기VToolStripMenuItem1 .Text= a.보기VToolStripMenuItem;
            this.싱크가사찾기ToolStripMenuItem.Text = this.싱크가사찾기ToolStripMenuItem1 .Text = a.싱크가사찾기ToolStripMenuItem;
            this.바탕화면싱크가사창띄우기ToolStripMenuItem.Text = 바탕화면싱크가사창띄우기ToolStripMenuItem1.Text = a.바탕화면싱크가사창띄우기ToolStripMenuItem;
            this.바탕화면싱크가사창닫기ToolStripMenuItem.Text = this.바탕화면싱크가사창닫기ToolStripMenuItem1.Text = a.바탕화면싱크가사창닫기ToolStripMenuItem;
            this.바탕화면싱크가사창새로고침ToolStripMenuItem.Text = 바탕화면싱크가사창새로고침ToolStripMenuItem1.Text = a.바탕화면싱크가사창새로고침;
            this.재생상세정보ToolStripMenuItem.Text = this.재생상세정보ToolStripMenuItem1.Text = a.재생상세정보ToolStripMenuItem;
            this.모니터끄기ToolStripMenuItem.Text = this.모니터끄기ToolStripMenuItem1.Text = a.모니터끄기ToolStripMenuItem;
            this.로그관리ToolStripMenuItem.Text = a.로그관리ToolStripMenuItem;
            this.로그폴더열기ToolStripMenuItem.Text = a.로그폴더열기ToolStripMenuItem;
            this.로그지우기ToolStripMenuItem.Text = a.로그지우기ToolStripMenuItem;
            this.상태표시줄업데이트ToolStripMenuItem.Text = a.상태표시줄업데이트ToolStripMenuItem;

            this.가사LToolStripMenuItem.Text = a.가사 + "(L)";
            this.가사ToolStripMenuItem.Text = a.가사 + "(&L)";

            this.플러그인PToolStripMenuItem.Text = a.플러그인PToolStripMenuItem;

            this.도움말HToolStripMenuItem.Text = 도움말HToolStripMenuItem1.Text = a.도움말HToolStripMenuItem;
            this.정보ToolStripMenuItem.Text = 정보ToolStripMenuItem1 .Text= a.정보ToolStripMenuItem;
            this.업데이트변경사항보기ToolStripMenuItem.Text = 업데이트변경사항보기ToolStripMenuItem1 .Text= a.업데이트변경사항보기ToolStripMenuItem;
            this.업데이트확인ToolStripMenuItem.Text = a.업데이트확인ToolStripMenuItem;
            //this.업데이트서버설정ToolStripMenuItem.Text = a.업데이트서버설정ToolStripMenuItem;
            this.디버깅ToolStripMenuItem.Text = 디버깅ToolStripMenuItem1.Text = a.디버깅ToolStripMenuItem;

            this.btn추가.Text = a.추가;
            int a2 = cb반복.SelectedIndex; this.cb반복.Items.Clear(); cb반복.Items.AddRange(a.cb반복); cb반복.SelectedIndex = a2<0?0:a2;
            this.columnHeader3.Text = 파일이름ToolStripMenuItem.Text = a.파일이름;
            this.columnHeader4.Text =파일경로ToolStripMenuItem.Text= a.파일경로;
            this.columnHeader2.Text = a.상태;
            this.lblSelectMusicPath.Text = a.lblSelectMusicPath;
            //this.btn재생설정.Text = a.btn재생설정;

            //if (!PlayText) btn재생.Text = a.재생; else btn재생.Text = a.일시정지;

            this.재생ToolStripMenuItem.Text = a.재생;
            this.다음ToolStripMenuItem.Text = a.다음;

            새인스턴스시작ToolStripMenuItem.Text = a.새인스턴스시작ToolStripMenuItem;
            this.일시정지ToolStripMenuItem.Text = a.일시정지;
            this.정지ToolStripMenuItem.Text = /*btn정지.Text =*/ a.정지;
            this.삭제ToolStripMenuItem.Text = a.삭제;
            this.현재위치의웨이브저장ToolStripMenuItem.Text = a.현재위치의웨이브저장ToolStripMenuItem;
            this.mD5해쉬값구하기ToolStripMenuItem.Text = a.mD5해쉬값구하기ToolStripMenuItem;
            this.가사찾기ToolStripMenuItem.Text = a.가사찾기ToolStripMenuItem;
            this.복사ToolStripMenuItem.Text = a.복사;
            this.탐색기에서해당파일열기ToolStripMenuItem.Text = a.탐색기에서해당파일열기ToolStripMenuItem;

            this.재생컨트롤ToolStripMenuItem.Text = a.재생컨트롤ToolStripMenuItem;
            this.재생설정열기ToolStripMenuItem.Text = a.재생설정열기ToolStripMenuItem;
            this.메인창열기ToolStripMenuItem.Text = a.메인창열기ToolStripMenuItem;
            this.toolStripStatusLabel1.Text = a.준비;
            try
            {
                for(int i = 0; i < fm.Count; i++)
                    this.fm_PlayingStatusChanged(fm[i].PlayStatus, CurrentItem.Index);
            } catch{}

            this.글자색상설정ToolStripMenuItem.Text = a.글자색상설정ToolStripMenuItem;
            this.예제사진ToolStripMenuItem.Text = a.예제사진ToolStripMenuItem;
             this.크리스마스1ToolStripMenuItem.Text = a.크리스마스1ToolStripMenuItem;
             this.일러스트1ToolStripMenuItem.Text = a.일러스트1ToolStripMenuItem;
              this.일러스트1ToolStripMenuItem.ToolTipText = a.일러스트1ToolStripMenuItem_Tooltip;
             this.일러스트2ToolStripMenuItem.Text = a.일러스트2ToolStripMenuItem;
              this.일러스트2ToolStripMenuItem.ToolTipText = a.일러스트2ToolStripMenuItem_Tooltip;

            this.lblTagName.Text = a.제목 + lblTagName.Text.Substring(lblTagName.Text.IndexOf(":"));
             this.lblTagArtist.Text = a.아티스트+ lblTagArtist.Text.Substring(lblTagArtist.Text.IndexOf(":"));
             this.lblTagAlbum.Text = a.앨범 + lblTagAlbum.Text.Substring(lblTagAlbum.Text.IndexOf(":"));
             this.lblTagBps.Text = a.비트레이트 + lblTagBps.Text.Substring(lblTagBps.Text.IndexOf(":"));
             this.lblTagSampling.Text = a.샘플레이트 + lblTagSampling.Text.Substring(lblTagSampling.Text.IndexOf(":"));
            #endregion

             #region 태그관련
            
             this.toolTip1.SetToolTip(this.lblTagArtist, (tag!=null&&(tag.Artists != null && tag.Artists.Length > 0) && tag.Artists[0] != null) ? tag.Artists[0] : "(" + a.아티스트존재X + ")");
             this.toolTip1.SetToolTip(this.lblTagAlbum, (tag!=null&&tag.Album != null) ? tag.Album : "(" + a.앨범존재X + ")");
             this.ImageTooltip.SetToolTip(this.picTagPic, picTagPic.Image != null ? a.앨범커버사진 : "(" + a.앨범사진존재X + ")");
             #endregion

             try { 이전ToolStripMenuItem.Text =  a.이전; if(thumbnailBarButtons!=null) thumbnailBarButtons[0].Tooltip = a.이전; }catch{}
             if (Helper!=null&&Helper.PlayStatus == HSAudioHelper.PlayingStatus.Play)
            { PlayText = true; /*btn재생.Text = LocalString.일시정지; thumbnailBarButtons[0].Icon = HS_Audio.Properties.Resources.Pause_29;*/ 재생ToolStripMenuItem1.Text =a.일시정지; if (thumbnailBarButtons != null) thumbnailBarButtons[1].Tooltip = a.일시정지; }
            else /*if (fh.PlayStatus == HSAudioHelper.PlayingStatus.Pause || fm[0].PlayStatus == HSAudioHelper.PlayingStatus.Stop)*/
            { PlayText = false; /*btn재생.Text = LocalString.재생; thumbnailBarButtons[0].Icon = HS_Audio.Properties.Resources.Play_29;(*/ 재생ToolStripMenuItem1.Text = a.재생; if (thumbnailBarButtons != null) thumbnailBarButtons[1].Tooltip = a.재생; }
            try { if (thumbnailBarButtons != null) thumbnailBarButtons[2].Tooltip = a.정지; }catch{}
            try { if (thumbnailBarButtons != null) thumbnailBarButtons[3].Tooltip = a.다음; }catch{}
        }
    }
}
