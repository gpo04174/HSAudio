﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using HS_Audio.Control;
using HS_CSharpUtility.Extension;
using TagLib;
using XPicbox;

namespace HS_Audio
{
    public  partial class frmMain
    {
        #region 사운드 유틸리티
        public static double mag_sqrd(double re, double im) { return (re * re + im * im); }

        public static double Decibels(double re, double im) {if(re==double.NegativeInfinity)return re; return ((re == 0 && im == 0) ? (double.NaN) : 10.0 * Math.Log10(((mag_sqrd(re, im))))); }
        public static double DecibelsforSound(double Sample, double Peak = 0) {if(Sample==double.NegativeInfinity)return Sample; return ((Sample == 0 && Peak == 0) ? (double.NegativeInfinity) : 10.0 * Math.Log10(((mag_sqrd(Sample, Peak)))));}
        #endregion
        void ThreadInit()
        {
            contextMenuStrip3.ImageScalingSize = new System.Drawing.Size(16, 16);
            LED눈금갯수 = 30;
            LyricsThreadStart = new ThreadStart(LyricsThread_Loop);
            MainThread[0] = Thread.CurrentThread;

            MainThreadStart[0] = new ThreadStart(TagThread_Loop);
            MainThreadStart[1] = new ThreadStart(KBpsThread_Loop);
            MainThreadStart[2] = new ThreadStart(PeakThread_Loop);

            toolStripComboBox6.SelectedIndex = 2;
        }
        ImageToolTip ImageTooltip = new ImageToolTip();

        void InitfrmMainExtend()
        {
            lblTagName.Text = LocalString.제목+": -";
            lblTagArtist.Text = LocalString.아티스트 + ": -";
            lblTagAlbum.Text = LocalString.앨범 + ": -";
            lblTagSampling.Text = LocalString.샘플레이트 + ": - Hz";
            lblTagBps.Text = LocalString.비트레이트 + ": - Kbps [- Kbps]";
            NowIndex = 0;
            TICK_Before = 0;
            //btnTagPicBefore.Enabled = btnTagPicNext.Enabled = false;
            piclist = null;

            if (picTagPic.Image != null) picTagPic.Image.Dispose();
            picTagPic.Image = null;

            if(ms != null)ms.Dispose(); ms = null;
        }

        private void pnlSwap2_SizeChanged(object sender, EventArgs e)
        {
            if (tag != null && tag.Album != null) lblTagAlbum.Text = HS_CSharpUtility.Utility.ControlUtility.TextCut(LocalString.앨범 + ": " + tag.Album, picTagPic.Width - pnlSwap2.Width, lblTagAlbum.Font);
            SetBpsString(BpsString);
        }
        

        IPicture[] piclist;
        TagLib.Tag tag;
        TagLib.File TAGFILE;
        MemoryStream ms = null;
        void TagThread_Loop()
        {
            FileStream MainStream = null;

            int Error = 0;

            ReadFile:
            try  {MainStream = new FileStream(Helper.MusicPath, FileMode.Open, FileAccess.Read, FileShare.Read); }
            catch
            {
                Thread.Sleep(1000);
                //MainStream = new FileStream(fh.MusicPath, FileMode.Open, FileAccess.Read, FileShare.Read);
                Error++;
                if (Error < 10) goto ReadFile;
            }
            
            try
            {
                if(TAGFILE!=null)TAGFILE.Dispose();
                TAGFILE = TagLib.File.Create(new StreamFileAbstraction(Helper.MusicPath, MainStream, MainStream));
                MainStream.Close();
                tag = TAGFILE.Tag;
                
                if (tag != null)
                {
                    lblTagSampling.Text = LocalString.샘플레이트 + ": - Hz";
                    lblTagBps.Text = LocalString.비트레이트 + ": - Kbps [- Kbps]";
                    this.InvokeControl(() =>
                    {
                        if (picTagPic.Image != null) picTagPic.Image.Dispose();
                        picTagPic.Image = null;

                        string tmp = Path.GetFileName(Helper.MusicPath);
                        if (tag.Title == null) lblTagName.Text = HS_CSharpUtility.Utility.ControlUtility.TextCut(LocalString.제목 + ": " + tmp, lblTagName.Location.X - lblTagBps.Location.X, lblTagName.Font, Path.GetExtension(Helper.MusicPath));
                        else lblTagName.Text = HS_CSharpUtility.Utility.ControlUtility.TextCut(LocalString.제목 + ": " + tag.Title, lblTagName.Location.X-lblTagBps.Location.X, lblTagName.Font);//(tag.Title.Length > 14 ? tag.Title.Remove(14) : tag.Title)

                        if (tag.Artists != null && tag.Artists.Length > 0)
                            lblTagArtist.Text = HS_CSharpUtility.Utility.ControlUtility.TextCut(LocalString.아티스트 + ": " + tag.Artists[0], lblTagArtist.Location.X - lblTagSampling.Location.X, lblTagArtist.Font);//(tag.Artists[0].Length > 14 ? tag.Artists[0].Remove(14) + "..." : tag.Artists[0]
                        if (tag.Album != null) lblTagAlbum.Text = HS_CSharpUtility.Utility.ControlUtility.TextCut(LocalString.앨범 + ": " + tag.Album, picTagPic.Width - pnlSwap2.Width, lblTagAlbum.Font);//(tag.Album.Length > 50 ? tag.Album.Remove(50) + "..." : tag.Album
                        Bitrate = TAGFILE.Properties.AudioBitrate;


                        if (tag.Title == null) this.toolTip1.SetToolTip(this.lblTagName, tmp);
                        else this.toolTip1.SetToolTip(this.lblTagName, tag.Title);
                        this.toolTip1.SetToolTip(this.lblTagArtist, ((tag.Artists != null && tag.Artists.Length > 0) && tag.Artists[0] != null) ? tag.Artists[0] : "("+LocalString.아티스트존재X+")");
                        this.toolTip1.SetToolTip(this.lblTagAlbum, tag.Album != null? tag.Album : "("+LocalString.앨범존재X+")");

                        piclist = tag.Pictures;
                        if(tag.Pictures.Length > 0)
                        {
                            ms = new MemoryStream(piclist[0].Data.Data);
                            //btnTagPicNext.Enabled = tag.Pictures.Length > 1;
                            try{picTagPic.Image = Bitmap.FromStream(ms);}catch{this.toolTip1.SetToolTip(this.picTagPic, tag.Album != null? tag.Album : "("+LocalString.앨범커버손상+")");}
                            ms.Dispose();
                        }
                    });
                }
            }
            catch{}
        }

        int Bitrate = 0;
        uint TICK_Before;
        string BpsString;
        void KBpsThread_Loop()
        {
            while (true)
            {
                try
                {
                    if (Helper != null)
                    {
                        uint TICK = Helper.GetCurrentTick(CurrentPreLoading ? HS_Audio_Lib.TIMEUNIT.PCMBYTES : HS_Audio_Lib.TIMEUNIT.RAWBYTES);
                        uint bps = Math.Min(TICK - TICK_Before, TICK_Before - TICK);
                        float Kbps = (float)(bps / 128f);
                        float KBps_Lamda = Kbps;
                        BpsString = string.Format(LocalString.비트레이트 + ": {0} Kbps{1} [{2} Kbps]", KBps_Lamda.ToString("0.0"), CurrentPreLoading ? " (PCM)" : "", Bitrate == 0 ? "-" : Bitrate.ToString());
                        SetBpsString(BpsString);
                        TICK_Before = TICK;

                        lblTagSampling.Text = LocalString.샘플레이트 + ": " + (Helper.DefaultFrequency == 0 || Helper.SoundFormat_Current == null ? "-" : Helper.DefaultFrequency.ToString()) + " Hz";
                    }
                }
                catch  { }
                System.Threading.Thread.Sleep(1000);
            }
        }
        object SetBpsString_Lock = new object();
        void SetBpsString(string BpsString) {lock(SetBpsString_Lock){ lblTagBps.InvokeControl(() => {lblTagBps.Text = HS_CSharpUtility.Utility.ControlUtility.TextCut(BpsString, lblTagBps.Location.X - btnFnSwap.Location.X, lblTagBps.Font);  });} }

        #region Peak
        void PeakThread_Loop()
        {
            while (true)
            {
            WORK:
                if (pnlSwap4.Visible)
                {
                    try
                    {
                        Work();
                        /*
                        progressBarGradient1.Value = (int)(progressBarGradient1.Step * DecibelsforSound(LDB));
                        progressBarGradient2.Value = (int)(progressBarGradient2.Step * DecibelsforSound(RDB));*/
                        
                        
                        //progressBarGradient1.InvokeIfNeeded(()=>
                        {
                            IProgressBarGradientInternal progressBar1 = progressBarGradient1;
                            IProgressBarGradientInternal progressBar2 = progressBarGradient2;

                        int tempFrameDelay = frameDelay;
                        int tempSampleDelay = _sampleDelay;

                        int MaxValue = progressBarGradient1.MaximumValue;
                        int MaxValue1 = progressBarGradient2.MaximumValue;

                        // for each channel, determine the step size necessary for each iteration
                        int leftGoal = 0;
                        int rightGoal = 0;

                        #region
                        {
                            double aa = _LED눈금갯수 * DecibelsforSound(LDB);
                            aa = Math.Max(aa, -MaxValue);
                            int b = (int)(MaxValue + aa);
                            //if()
                            if (SmoothPeak) leftGoal = (int)Math.Min(b, MaxValue);
                            else progressBar1.Value = (int)Math.Min(b, MaxValue);

                            aa = _LED눈금갯수 * DecibelsforSound(RDB);
                            aa = Math.Max(aa, -MaxValue1);
                            b = (int)(MaxValue1 + aa);

                            if (SmoothPeak) rightGoal = (int)Math.Min(b, MaxValue1);
                            else
                            {
                                progressBar2.Value = (int)Math.Min(b, MaxValue1);
                                //Thread.Sleep(progressBar1.AttackDelay);
                                Thread.Sleep(FrameDelay);
                                goto WORK;
                            }
                        }
                        #endregion


                        double range1 = leftGoal - progressBarGradient1.Value;
                        double range2 = rightGoal - progressBarGradient2.Value;

                        double exactValue1 = progressBarGradient1.Value;
                        double exactValue2 = progressBarGradient2.Value;

                        double stepSize1 = range1 / tempSampleDelay * tempFrameDelay;
                        if (Math.Abs(stepSize1) < .01)
                        {
                            stepSize1 = Math.Sign(range1) * .01;
                        }
                        double absStepSize1 = Math.Abs(stepSize1);

                        double stepSize2 = range2 / tempSampleDelay * tempFrameDelay;
                        if (Math.Abs(stepSize2) < .01)
                        {
                            stepSize2 = Math.Sign(range2) * .01;
                        }
                        double absStepSize2 = Math.Abs(stepSize2);

                        // increment/decrement the bars' values until both equal their desired goals,
                        // sleeping between iterations
                        if ((progressBar1.Value == leftGoal) && (progressBar2.Value == rightGoal))
                        {
                            Thread.Sleep(tempSampleDelay);
                        }
                        else
                        {
                            do
                            {
                                if (progressBar1.Value != leftGoal)
                                {
                                    if (absStepSize1 < Math.Abs(leftGoal - progressBar1.Value))
                                    {
                                        exactValue1 += stepSize1;
                                        progressBar1.Value = (int)Math.Round(exactValue1);
                                    }
                                    else
                                    {
                                        progressBar1.Value = leftGoal;
                                    }
                                }

                                if (progressBar2.Value != rightGoal)
                                {
                                    if (absStepSize2 < Math.Abs(rightGoal - progressBar2.Value))
                                    {
                                        exactValue2 += stepSize2;
                                        progressBar2.Value = (int)Math.Round(exactValue2);
                                    }
                                    else
                                    {
                                        progressBar2.Value = rightGoal;
                                    }
                                }

                                Thread.Sleep(tempFrameDelay);
                            } while ((progressBar1.Value != leftGoal) || (progressBar2.Value != rightGoal));
                        }
                        }//);
                    }
                    catch {Thread.Sleep(100); }
                    LDB = RDB = float.NegativeInfinity;
                }
                Thread.Sleep(FrameDelay / 2);
            }
        }

        double LDB = double.NegativeInfinity, RDB = double.NegativeInfinity;
        private void Work()
        {
            float LDB = float.NegativeInfinity, RDB = float.NegativeInfinity;
            #region
            if (Helper!=null)
            {
                float[] Ltmp = new float[SoundBuffer];

                if (GetData) if (Helper.channel != null) Helper.system.getWaveData(Ltmp, Ltmp.Length, 0);
                else if (Helper.channel!=null) Helper.channel.getWaveData(Ltmp, Ltmp.Length, 0);

                //if (Ltmp.Length == 0) { Ltmp = new float[DefaultLED+1]; } if (Rtmp.Length == 0) { Rtmp = new float[DefaultLED]; }
                if (Helper.SoundFormat.Channel == 2)
                {
                    float[] Rtmp = new float[SoundBuffer];

                    if (GetData)
                    {
                        Helper.system.getWaveData(Ltmp, Ltmp.Length, 0);
                        Helper.system.getWaveData(Rtmp, Rtmp.Length < 2 ? 1 : Rtmp.Length, 1);
                    }
                    else
                    {
                        Helper.channel.getWaveData(Ltmp, Ltmp.Length, 0);
                        Helper.channel.getWaveData(Rtmp, Rtmp.Length, 1);
                    }

                    for (int i = 0; i < Ltmp.Length; i++)
                    {
                        if (Math.Abs(Ltmp[i]) > LDB) LDB = Math.Abs(Ltmp[i]);
                        if (Math.Abs(Rtmp[i]) > RDB) RDB = Math.Abs(Rtmp[i]);
                    }
                    this.LDB = LDB; this.RDB = RDB;
                }
                else
                {

                    for (int i = 0; i < Ltmp.Length; i++)
                        if (Math.Abs(Ltmp[i]) > LDB) LDB = Math.Abs(Ltmp[i]);
                    this.LDB = this.RDB = LDB;
                }

                //MIC_L = DecibelsforSound(LDB);
                //MIC_R = DecibelsforSound(RDB);
                Thread.Sleep(25);
            }else Thread.Sleep(75);
            #endregion
        }

        int _LED눈금갯수 = 20000;
        public int LED눈금갯수
        {
            get {return progressBarGradient1.MaximumValue / _LED눈금갯수; }
            set
            {
                _LED눈금갯수 = value;
                progressBarGradient1.MaximumValue = progressBarGradient2.MaximumValue = value * _LED눈금갯수; 
                progressBarGradient1.MindB = progressBarGradient2.MindB = -value;
            }
        }
        public bool Smooth = true;
        public int SoundBuffer = 2048;
        bool GetData = true;

        private int _sampleDelay = 30;
        public int SampleDelay
		{
            get{return _sampleDelay;}
			set
			{
				_sampleDelay = Math.Max(0, value);
				if(frameDelay > _sampleDelay)
                    frameDelay = _sampleDelay;
			}
            /*
			get{return progressBarGradient1.ReleaseTime;}
			set{progressBarGradient1.ReleaseTime = progressBarGradient2.ReleaseTime = value;}*/
		}

        private int frameDelay = 10;
		public int FrameDelay
		{
            get{return frameDelay;}
			set{frameDelay = Math.Max(0, Math.Min(_sampleDelay, value));}
            /*
			get{return progressBarGradient1.AttackTime;}
			set{progressBarGradient1.AttackTime = progressBarGradient2.AttackTime = value;}*/
		}

        bool SmoothPeak = true;
        #endregion

        int NowIndex = 0;
        private void btnTagPicBefore_Click(object sender, EventArgs e)
        {
            NowIndex--;
            //btnTagPicBefore.Enabled = NowIndex > 0;

            picTagPic.Image.Dispose();
            MemoryStream ms = new MemoryStream(piclist[NowIndex].Data.Data);
            picTagPic.Image = Bitmap.FromStream(ms);
            ms.Dispose();

        }

        private void btnTagPicNext_Click(object sender, EventArgs e)
        {
            NowIndex++;
            //btnTagPicNext.Enabled = NowIndex < piclist.Length-1;

            picTagPic.Image.Dispose();
            MemoryStream ms = new MemoryStream(piclist[NowIndex].Data.Data);
            picTagPic.Image = Bitmap.FromStream(ms);
            ms.Dispose();
        }

        #region Peak 그래프
        private void peak새로고침ToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }
        //측정 범위 (눈금 갯수)
        private void toolStripTextBox4_TextChanged(object sender, EventArgs e)
        {
            try{LED눈금갯수 = Convert.ToInt16(toolStripTextBox4.Text);}catch{LED눈금갯수 = 30;}
           
        }
        //측정 속도
        private void toolStripTextBox5_TextChanged(object sender, EventArgs e)
        {

        }
        //측정 버퍼
        private void toolStripTextBox6_TextChanged(object sender, EventArgs e)
        {
            try{SoundBuffer = Convert.ToInt16(toolStripTextBox6.Text);}catch{SoundBuffer = 2048;}
        }
        private void 그래프부드럽게ToolStripMenuItem_CheckedChanged(object sender, EventArgs e)
        {
            SmoothPeak = 그래프부드럽게ToolStripMenuItem.Checked;
        }
        //그래프 어택 타임
        private void toolStripTextBox7_TextChanged(object sender, EventArgs e)
        {
             try{ this.FrameDelay = Convert.ToInt16(toolStripTextBox7.Text);}
            catch{ this.FrameDelay =10;}
        }
        //그래프 릴리즈 타임
        private void toolStripTextBox8_TextChanged(object sender, EventArgs e)
        {
            try{ this.SampleDelay = Convert.ToInt16(toolStripTextBox8.Text);}
            catch{ this.SampleDelay =30;}
        }
        #endregion

        #region dB레이블
        private void 음수부호표시ToolStripMenuItem_CheckedChanged(object sender, EventArgs e)
        {
            progressBarGradient1.ShowMinus = progressBarGradient2.ShowMinus = 음수부호표시ToolStripMenuItem.Checked;
        }

        private void dB문자표시ToolStripMenuItem_CheckedChanged(object sender, EventArgs e)
        {
            progressBarGradient1.ShowdB = progressBarGradient2.ShowdB = dB문자표시ToolStripMenuItem.Checked;
        }

        private void dB문자공백ToolStripMenuItem_CheckedChanged(object sender, EventArgs e)
        {
            progressBarGradient1.SpacedB = progressBarGradient2.SpacedB = dB문자공백ToolStripMenuItem.Checked;
        }

        private void dB레벨레이블ToolStripMenuItem_CheckedChanged(object sender, EventArgs e)
        {
            progressBarGradient1.VisibledB = progressBarGradient2.VisibledB = dB레벨레이블ToolStripMenuItem.Checked;
        }

        private void dB글자색변경ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ColorDialog colorDialog3 = new ColorDialog();
            colorDialog3.Color = progressBarGradient1.ForeColor;
            if (colorDialog3.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                progressBarGradient1.ForeColor = progressBarGradient2.ForeColor = colorDialog3.Color;
            }
        }

        private void dB굵게ToolStripMenuItem_CheckedChanged(object sender, EventArgs e)
        {
            if (dB굵게ToolStripMenuItem.Checked)
            {
                Font ft = new System.Drawing.Font(progressBarGradient1.Font, FontStyle.Bold);
                progressBarGradient1.Font = progressBarGradient2.Font = ft;
            }
            else
            {
                Font ft = new System.Drawing.Font(progressBarGradient1.Font, FontStyle.Regular);
                progressBarGradient1.Font = progressBarGradient2.Font = ft;
            }
        }

        private void toolStripComboBox6_SelectedIndexChanged(object sender, EventArgs e)
        {
            progressBarGradient1.IntervaldB = progressBarGradient2.IntervaldB = toolStripComboBox6.SelectedIndex + 1;
        }
        #endregion

        private void picTagPic_MouseHover(object sender, EventArgs e)
        {
            ImageTooltip.Image = (Bitmap)picTagPic.Image;
            //if (picTagPic.Image != null) ImageTooltip.Size = picTagPic.Image.Size;
            ImageTooltip.SetToolTip(this.picTagPic, picTagPic.Image != null ? LocalString.앨범커버사진 : "(" + LocalString.앨범사진존재X + ")");
        }

        XPicbox.XtendPicBoxForm xp;
        ComboBox cb = null;
        int W, H;
        private void picTagPic_Click(object sender, EventArgs e)
        {
            if (picTagPic.Image == null) MessageBox.Show(LocalString.picTagPic_Message, LocalString.ProgramName + " " + LocalString.태그뷰어, MessageBoxButtons.OK, MessageBoxIcon.Warning);
            else
            {
                try
                {
                    xp = new XPicbox.XtendPicBoxForm();
                    xp.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
                    Panel pnl = new Panel();
                    pnl.Dock = DockStyle.Fill;
                    xp.Controls.Add(pnl);

                    xp._WidthBorder = xp.Width - pnl.Width;

                    #region Init xpForm
                    {
                        Label lb = new Label()
                        {
                            Text = "맞춤:",
                            Location = new Point(1, 5),
                            AutoSize = true
                        };
                        xp.Controls.Add(lb);

                        cb = new ComboBox()
                        {
                            Location = new Point(lb.Width+2, 0),
                            Size = new Size(70, 20)
                        };
                        cb.Items.AddRange(new string[] { "없음", "맞추기", "늘이기", "가운데" });
                        xp.Controls.Add(cb);

                        xp._HeightBorder = (xp.Height - pnl.Height) + cb.Height;

                        Button btn = new Button()
                        {
                            Text = "조정",
                            Location = new Point(cb.Location.X + cb.Width + 2, 1),
                            Size = new Size(40, cb.Height)
                        };

                        xp.FormClosing += ((object sender1, FormClosingEventArgs ea)=> {xp.innerPicture.Image.Dispose();});
                        cb.SelectedIndexChanged += 
                            (object sender1, EventArgs e1) =>
                            {
                                try
                                {
                                    XtendPicBoxForm xp1 = (XtendPicBoxForm)((ComboBox)sender).Parent;
                                    //xp.innerPicture.Anchor = AnchorStyles.Left | AnchorStyles.Right | AnchorStyles.Top | AnchorStyles.Bottom;
                                    //xp.MaximumSize = new Size(0, 0);
                                    switch (((ComboBox)sender).SelectedIndex)
                                    {
                                        case 1: xp1.innerPicture.SizeMode = PictureBoxSizeMode.Zoom; break;
                                        case 2: xp1.innerPicture.SizeMode = PictureBoxSizeMode.StretchImage; break;
                                        case 3: xp1.innerPicture.SizeMode = PictureBoxSizeMode.CenterImage; break;
                                        default:
                                        xp1.innerPicture.Size = new System.Drawing.Size(xp1.Image.Size.Width, xp1.Image.Size.Height);
                                        xp1.innerPicture.SizeMode = PictureBoxSizeMode.Normal;
                                        //xp.Size = new Size(xp.Image.Size.Width + xp.WidthBorder, xp.Image.Size.Height + xp.HeightBorder); 
                                        //xp.MaximumSize = this.Size;
                                        //xp.innerPicture.Anchor = AnchorStyles.Top|AnchorStyles.Left;
                                        break;
                                    }
                                    xp_SizeChanged(xp1, null);
                                }
                                catch { }
                            };//((object sender1, EventArgs ea)=> { MessageBox.Show("앙 기모띠~");});
                        btn.Click += btn_Click;
                        xp.Load += xp_Load;

                        cb.DropDownStyle = ComboBoxStyle.DropDownList;
                        cb.SelectedIndex = 0;

                        xp.Controls.Add(btn);

                        xp.innerPicture.Location = new Point(0, cb.Height + 2);

                        //H += cb.Height + 2;
                    }
                    #endregion
                    /*
                int BorderWidth = (xp.Width - xp.ClientSize.Width) / 2;
                int TitlebarHeight = xp.Height - xp.ClientSize.Height - (2 * BorderWidth);*/
                    //HS_CSharpUtility.Utility.Win32Utility.RECT rec;
                    //HS_CSharpUtility.Utility.Win32Utility.GetWindowRect(xp.Handle, out rec);

                    pnl.Dispose(); pnl = null;

                    xp.Text = string.Format(LocalString.해상도 + ": {0} X {1} [{2}] - " + LocalString.ProgramName + " " + LocalString.태그뷰어, picTagPic.Image.Width, picTagPic.Image.Height,
                            (tag == null || tag.Title == null) ? Path.GetFileName(Helper.MusicPath) : tag.Title);
                    xp.innerPicture.MouseClick += xp_MouseClick;
                    xp.SizeChanged += xp_SizeChanged;
                    xp.Image = new Bitmap(picTagPic.Image);

                    xp.innerPicture.Size = new System.Drawing.Size(xp.Image.Size.Width, xp.Image.Size.Height);
                    //xp.Size = new Size(xp.Image.Size.Width + W, xp.Image.Size.Height + H);//xp.innerPicture.Size;//new Size(picTagPic.Image.Size.Width + BorderWidth, picTagPic.Image.Size.Height + (BorderWidth + TitlebarHeight));
                    //xp.MaximumSize = xp.Size;
                    xp.Show();
                }catch{}
            }
        }

        #region XtendPicBoxForm 각종 이벤트
        private void xp_Load(object sender, EventArgs e)
        {
            btn_Click(sender, e);
        }

        private void xp_SizeChanged(object sender, EventArgs e)
        {
            try
            {
                XtendPicBoxForm xp = (XtendPicBoxForm)sender;
                if (xp.innerPicture.SizeMode != PictureBoxSizeMode.Normal)
                {
                    xp.innerPicture.Size = new System.Drawing.Size(xp.Width - xp.WidthBorder, xp.Height - xp.HeightBorder);

                    ComboBox cb = null;
                    foreach (System.Windows.Forms.Control a in xp.Controls){ try{cb = (ComboBox)a; break; }catch{ } }
                    xp.innerPicture.Location = new Point(0, cb.Height + 2);
                }
            }
            catch{}
        }
        private void btn_Click(object sender, EventArgs e)
        {
            try
            {
                XtendPicBoxForm xp = null;
                try{xp = (XtendPicBoxForm)((Button)sender).Parent;}catch{xp = (XtendPicBoxForm)sender;}
                xp.innerPicture.Size = new System.Drawing.Size(xp.Image.Size.Width, xp.Image.Size.Height);
                xp.innerPicture.SizeMode = PictureBoxSizeMode.Normal;
                xp.Size = new Size(xp.Image.Size.Width + xp.WidthBorder, xp.Image.Size.Height + xp.HeightBorder);
            }
            catch { }
        }

        public void xp_MouseClick(object sender, EventArgs e)
        {
            try
            {
                XtendPicBoxForm xp = (XtendPicBoxForm)((PictureBox)sender).Parent;
                xp.Dispose();
            }catch{}
        }
        #endregion


        private void lblTagName_Click(object sender, EventArgs e)
        {
            MessageBox.Show(tag.Title == null ? Path.GetFileName(Helper.MusicPath) : tag.Title, LocalString.제목 + " - " + LocalString.ProgramName + " " + LocalString.태그뷰어);
        }

        private void lblTagArtist_Click(object sender, EventArgs e)
        {
            MessageBox.Show(((tag.Artists != null && tag.Artists.Length > 0) && tag.Artists[0] != null) ? tag.Artists[0] : "(아티스트명이 없습니다.)", LocalString.아티스트 + " - " + LocalString.ProgramName + " " + LocalString.태그뷰어);
        }

        private void lblTagAlbum_Click(object sender, EventArgs e)
        {
            MessageBox.Show(tag.Album != null ? tag.Album : "(앨범명이 없습니다.)", LocalString.앨범 + " - " + LocalString.ProgramName + " " + LocalString.태그뷰어);
        }

        private void toolStripMenuItem6_Click(object sender, EventArgs e)
        {

        }
    }

    public class ImageToolTip : ToolTip
    {
        public ImageToolTip(Bitmap Image)
        {
            this.Image = Image;
            this.Popup += new PopupEventHandler(this.OnPopup);
            this.Draw += new DrawToolTipEventHandler(this.OnDraw);
        }
        public ImageToolTip()
        {
            this.Popup += new PopupEventHandler(this.OnPopup);
            this.Draw += new DrawToolTipEventHandler(this.OnDraw);
        }

        bool _AutoSize = true;
        public bool AutoSize{get{return _AutoSize;}set {_AutoSize = value;}}

        Size _Size = new Size(200, 100);
        public Size Size{get{return _Size;}set{_Size = value;}}

        private void OnPopup(object sender, PopupEventArgs e)
        {
            if (AutoSize)
            {
                if (Image != null) e.ToolTipSize = new Size(Image.Width, Image.Height);
                else goto Dummy;
            }
            else e.ToolTipSize = new Size(_Size.Width, _Size.Height);

            Dummy: 
            Console.WriteLine("IHS_Audio::ImageToolTip::void OnPopup(object sender, PopupEventArgs e);");
            System.Diagnostics.Debug.WriteLine("IHS_Audio::ImageToolTip::void OnPopup(object sender, PopupEventArgs e);");
        }

        Bitmap _Image;
        public Bitmap Image{get{return _Image; }set{_Image = value; this.OwnerDraw = value!=null;}}

        string _ErrorMessage = "(이미지가 없습니다.)";
        [Obsolete("임시로 만들어둔 항목입니다.")]
        public string ErrorMessage{get{return _ErrorMessage;}set{_ErrorMessage = value;}}

        private void OnDraw(object sender, DrawToolTipEventArgs e)
        {
            Bitmap bmp = new Bitmap(Image);
            //bmp.Save("Z:\\tmp.png");
            Graphics g = e.Graphics;
            try{g.DrawImageUnscaled(bmp, new Point(0, 0));}catch{}
            finally{if(bmp!=null)bmp.Dispose();g.Dispose();GC.Collect();}
        }
    }
}
