﻿/*예전 어셈블리 이름: 
 
 HS_Audio -> FMOD_Audio
 HS_Audio_Helper -> FMOD_Helper
 HSAudio* -> FMOD*/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Threading;

using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;
using HS_Audio.LIBRARY.Native;
using HS_Audio.Languge;
using HS_Audio.Lyrics;
using HS_Audio.Setting;
using HS_Audio.Forms;
using HS_CSharpUtility;
using HS_CSharpUtility.Extension;
using Huseyint.Windows7.WindowsForms;
using HS_Audio.Plugin;
using HS_Library;
using HS_Audio.Bundles;
using HS_Audio.Commander;
using HS_Audio.Commander.Sender;

namespace HS_Audio
{
    public enum PlayListChange { Add, Remove, Change }
    public delegate void PlaylistChangedEventHandler(object sender, PlayListChange change, params int[] index);
    public partial class frmMain : HS_Audio.Control.HSCustomForm, IHSAudioPluginHost
    {
        public event PlaylistChangedEventHandler PlaylistChanged;
        public static Bitmap InvertImage(Bitmap OriginImage)
        {
            for (int i = 0; i < OriginImage.Width; i++)
                for (int j = 0; j < OriginImage.Height; j++)
                    OriginImage.SetPixel(i, j, HS_CSharpUtility.Extension.ColorExtension.NegativeColor(OriginImage.GetPixel(i, j)));
            return OriginImage;
        }

        protected override void OnShown(EventArgs e)
        {
            if (Environment.OSVersion.Version.Major >= 6 && Environment.OSVersion.Version.Minor >= 1)
            {
                try { TaskBarExtensions.AddThumbnailBarButtons(this, this.thumbnailBarButtons); } catch { }
            }
        }

        public Cursor AndroidCursor;
        string ProgramName = "HS™ 플레이어";
        #region 인코딩 관련
        /// <summary>
        /// 파일의 인코딩을 구합니다
        /// </summary>
        /// <param name="FileName">인코딩을 구할 파일경로 입니다.</param>
        /// <returns>파일의 인코딩을 반환합니다</returns>
        private static Encoding GetFileEncoding(string FileName)
        {
            // *** Use Default of Encoding.Default (Ansi CodePage)
            Encoding enc = Encoding.Default;
            try
            {
                UTF8Encoding utf8 = new UTF8Encoding(false, true);
                StreamReader sr = new StreamReader(FileName, utf8);
                int a = sr.Read();sr.Close(); 
                if (a == -1) { throw new DecoderFallbackException(); }
                enc = utf8;
            }
            catch
            {
                // *** Detect byte order mark if any - otherwise assume default
                byte[] buffer = new byte[5];
                FileStream file = new FileStream(FileName, FileMode.Open);
                file.Read(buffer, 0, 5);
                file.Close();

                if (buffer[0] == 0xef && buffer[1] == 0xbb && buffer[2] == 0xbf)
                    enc = Encoding.UTF8;
                else if (buffer[0] == 0xfe && buffer[1] == 0xff)
                    enc = Encoding.Unicode;
                else if (buffer[0] == 0 && buffer[1] == 0 && buffer[2] == 0xfe && buffer[3] == 0xff)
                    enc = Encoding.UTF32;
                else if (buffer[0] == 0x2b && buffer[1] == 0x2f && buffer[2] == 0x76)
                    enc = Encoding.UTF7;
                else if (buffer[0] == 0xFE && buffer[1] == 0xFF)
                    // 1201 unicodeFFFE Unicode (Big-Endian)
                    enc = Encoding.GetEncoding(1201);
                else if (buffer[0] == 0xFF && buffer[1] == 0xFE)
                    // 1200 utf-16 Unicode
                    enc = Encoding.GetEncoding(1200);
            }
            return enc;
        }
        /// <summary>
        /// 파일의 인코딩을 구합니다
        /// </summary>
        /// <param name="FileName">인코딩을 구할 파일경로 입니다.</param>
        /// <param name="FastMode">True를 주면 구하는 과정이 빨라집니다.</param>/*빨라지긴 하지만 정확도가 떨어집니다.*/
        /// <returns>파일의 인코딩을 반환합니다</returns>
        private static Encoding GetFileEncoding(string FileName, bool FastMode)
        {
            // *** Use Default of Encoding.Default (Ansi CodePage)
            Encoding enc = Encoding.Default;
            try
            {
                UTF8Encoding utf8 = new UTF8Encoding(false, true);
                if (!FastMode)
                {
                    System.IO.File.ReadAllText(FileName, utf8);
                    //System.IO.File.ReadAllText();
                    enc = utf8;
                }
                else 
                {
                    StreamReader sr = new StreamReader(FileName, utf8);
                    int a = sr.Read();sr.Close(); 
                    if (a == -1) { throw new DecoderFallbackException(); }
                    enc = utf8;
                }
            }
            catch
            {
                // *** Detect byte order mark if any - otherwise assume default
                byte[] buffer = new byte[5];
                if (!System.IO.File.Exists(FileName)) return null;
                FileStream file = new FileStream(FileName, FileMode.Open);
                file.Read(buffer, 0, 5);
                file.Close();

                if (buffer[0] == 0xef && buffer[1] == 0xbb && buffer[2] == 0xbf)
                    enc = Encoding.UTF8;
                else if (buffer[0] == 0xfe && buffer[1] == 0xff)
                    enc = Encoding.Unicode;
                else if (buffer[0] == 0 && buffer[1] == 0 && buffer[2] == 0xfe && buffer[3] == 0xff)
                    enc = Encoding.UTF32;
                else if (buffer[0] == 0x2b && buffer[1] == 0x2f && buffer[2] == 0x76)
                    enc = Encoding.UTF7;
                else if (buffer[0] == 0xFE && buffer[1] == 0xFF)
                    // 1201 unicodeFFFE Unicode (Big-Endian)
                    enc = Encoding.GetEncoding(1201);
                else if (buffer[0] == 0xFF && buffer[1] == 0xFE)
                    // 1200 utf-16 Unicode
                    enc = Encoding.GetEncoding(1200);
            }
            return enc;
        }
        #endregion

        [System.Runtime.InteropServices.DllImport("kernel32.dll", SetLastError = true, ExactSpelling = true)]
        static extern bool CheckRemoteDebuggerPresent(IntPtr hProcess, ref bool isDebuggerPresent);

        const long DateSize = 2048;
        bool IsDebugging{get { bool dd = false; CheckRemoteDebuggerPresent(Process.GetCurrentProcess().Handle, ref dd); return dd; }}
        
        InformationSender info;
        HSAudioHelper _Helper;
        internal HSAudioHelper Helper
        {
            get { return _Helper; }
            set
            {
                _Helper = value;
                try
                {
                    Helper.VolumeChanged += NoMixing_VolumeChanged;
                    //if (info_memory != null) info_memory.Dispose(); info_memory = new InformationSenderMemory(this) { AutoUpdate = true }; info_memory.Start();
                    if (info == null) info = new InformationSender(this);
                }
                catch(Exception ex) { }
            }
        }
        Lyrics.LyricsCache Lyricscache = new Lyrics.LyricsCache();
        public frmMain()
        {
            ICSharpCode.SharpZipLib.Zip.ZipFile zf = new ICSharpCode.SharpZipLib.Zip.ZipFile("");
            InitializeComponent();
            CustomFont = this.Font;

            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;

            UpdateSetting();
            sm.Create(100, "HS_Audio_Add_PlayList");
            sm.Create(1, "HS_Audio_Add_PlayList_Check");
            Program.localeManager.LocaleChanged+=new LocaleManager.LocaleChangesEventhandler(localeManager_LocaleChange);
            toolStripComboBox1.SelectedIndex = 0;
            toolStripComboBox1.Text = "한국어";
            if (Environment.OSVersion.Version.Major >= 6 && Environment.OSVersion.Version.Minor >= 1)
            {
                try
                {
                    Huseyint.Windows7.WindowsForms.TaskBarExtensions.Attach();
                    this.thumbnailBarButtons = new List<ThumbnailBarButton>(){
                    new ThumbnailBarButton(Properties.Resources.Previous_32, "이전", false, true, false, true),
                    new ThumbnailBarButton(Properties.Resources.Play_29, "재생", false, true, false, true),
                    new ThumbnailBarButton(Properties.Resources.Stop_29, "정지", false, true, false, true),
                    new ThumbnailBarButton(Properties.Resources.Next_32, "다음", false, true, false, true)};
                    thumbnailBarButtons[0].Click += this.이전ToolStripMenuItem_Click;
                    thumbnailBarButtons[1].Click += this.button1_Click;//this.재생ThumbnailBarButton_Click;
                    thumbnailBarButtons[2].Click += this.button2_Click;//this.정지ThumbnailBarButton_Click;
                    thumbnailBarButtons[3].Click += this.다음ToolStripMenuItem_Click;//this.다음ThumbnailBarButton_Click;
                    fh_PlayingStatusChanged(Helper.PlayStatus, Helper.index);
                    ThreadPool.QueueUserWorkItem(new WaitCallback(AddAddThumbnailBarButton_Thread), this.Handle);
                }
                catch { }
            }
            if (IsDebugging) this.Text = this.Text.Insert(this.Text.Length, " (디버깅)");
            //MessageBox.Show(IsDebugging.ToString());
            frmlrcSearch.GetLyricsComplete += new Lyrics.frmLyricsSearch.GetLyricsCompleteEventHandler(frmlrcSearch_GetLyricsComplete);
            frmlrcSearch.GetLyricsFailed += new frmLyricsSearch.GetLyricsFailedEventHandler(lrcf.LyricsFailedException);
            lrcf.Lyricscache = Lyricscache;
            lrcf.FormStatusChange+=new Lyrics.frmLyricsDesktop.FormStatusChangeEventHandler(lrcf_FormStatusChange);
            lrcf.UpdateLyricsIntervalChange += new Lyrics.frmLyricsDesktop.UpdateLyricsIntervalChangeEventHandler(lrcf_UpdateLyricsIntervalChange);
        }
        Thread AddPlaylist_Thread;
        ThreadStart AddPlaylist_ThreadStart;
        
        public bool MixingMode { get { return 음악믹싱모드ToolStripMenuItem.Checked; } set { 음악믹싱모드ToolStripMenuItem.Checked = value; } }

        List<HS_Audio.HSAudioHelper> fm = new List<HSAudioHelper>();

        #region 재생 관련
        internal delegate void PlayChangeEventHandler(bool Play);
        internal event PlayChangeEventHandler PlayChanged;
        public bool PlayText;
        string LOCK_COMMAND = "LOCK_COMMAND";
        internal void Play(int index = -1) { lock (LOCK_COMMAND) { if (index < 0) Helper.Play(); else fm[index].Play(); timer1.Start(); } }//timer2.Start(); }
        internal void Pause(int index = -1) { lock (LOCK_COMMAND) { try { if (index < 0) Helper.Pause(); else fm[index].Pause(); } catch { } } }
        internal void Stop(int index = -1) { lock (LOCK_COMMAND) { try { if (index < 0) Helper.Stop(); else fm[index].Stop(); timer2.Stop(); } catch { } } }
        internal void Delete(int index = -1) { lock (LOCK_COMMAND) { try { if (index < 0) Helper.Dispose(false); else fm[index].Dispose(); fm.RemoveAt(index); } catch { } } }
        internal void Foward(int index = -1) { lock (LOCK_COMMAND) { try { if (index < 0) Helper.CurrentPosition = 0; else fm[index].CurrentPosition = 0; } catch { } } }
        internal TimeSpan CurrentTick(int index = -1) {try{if (index < 0) { return TimeSpan.FromMilliseconds((double)(Helper.CurrentPosition)); } else { return fm.Count > 0 ? TimeSpan.FromMilliseconds((double)(fm[index].CurrentPosition)) : ts; }} catch{return new TimeSpan();}}
        internal uint _CurrentTick(int index = -1) {try{if (index < 0) { return Helper.CurrentPosition; } else { return fm.Count > 0 ? fm[index].CurrentPosition : 0/*ts*/; }} catch{return 0;}}
        internal TimeSpan TotalTick(int index = -1) { try{if (index < 0) { return TimeSpan.FromMilliseconds((double)(Helper.TotalPosition)); } else { return fm.Count > 0 ? TimeSpan.FromMilliseconds((double)(fm[index].TotalPosition)) : ts; } }catch{return new TimeSpan();}}
        internal uint _TotalTick(int index = -1) { try{if (index < 0) { return Helper.TotalPosition; } else { return fm.Count > 0 ? fm[index].TotalPosition : 0/*ts*/; } }catch{return 0;}}

        readonly TimeSpan ts= new TimeSpan();

        StringBuilder ShowTime_sb = new StringBuilder(7, 100);

        string my_string;
        byte ErrorCount;
        public string ShowTime(TimeSpan Time, bool MilliSecond=true)
        {
            try
            {
                my_string = null;
                if (ShowTime_sb.Length > 1) ShowTime_sb.Remove(0, ShowTime_sb.Length);
                /*sb.Append(Time.Hours);sb.Append(":");
                sb.Append(Time.Minutes);sb.Append(":");
                sb.Append(Time.Seconds);sb.Append(".");
                sb.Append(Time.Milliseconds);*/
                ShowTime_sb.Append(Time.Hours.ToString("00")); ShowTime_sb.Append(":");
                ShowTime_sb.Append(Time.Minutes.ToString("00")); ShowTime_sb.Append(":");
                ShowTime_sb.Append(Time.Seconds.ToString("00"));
                if (MilliSecond) { ShowTime_sb.Append("."); ShowTime_sb.Append(Time.Milliseconds.ToString("000")); }
                //my_string = ShowTime_sb.ToString();
                ErrorCount = 0;
                //my_string = (string)ShowTime_sb.GetType().GetField("m_StringValue", BindingFlags.NonPublic | BindingFlags.Instance).GetValue(ShowTime_sb);
                return (string)ShowTime_sb.GetType().GetField("m_StringValue", BindingFlags.NonPublic | BindingFlags.Instance).GetValue(ShowTime_sb);
            }
            catch (Exception ex) { if (ErrorCount > 5)ex.Logging("HS_Audio::frmMain::ShowTime(TimeSpan Time, bool MilliSecond=true) 에서 예외 발생!!"); GC.Collect(); ErrorCount++; return ""; }
        }

        StringBuilder ShowTime_sb1 = new StringBuilder(7, 100);
        string my_string1;
        byte ErrorCount1;

        long Hour_ShowTime = 0;
        int Second_ShowTime = 0;
        int Minute_ShowTime = 0;
        int MilliSecond_ShowTime = 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="Tick"></param>
        /// <param name="MilliSecond"></param>
        /// <returns></returns>
        public string ShowTime(long Tick, bool MilliSecond = true)
        {
            try
            {
                if (ShowTime_sb1.Length > 1) ShowTime_sb1.Remove(0, ShowTime_sb1.Length);
                /*
                TimeSpan ts = TimeSpan.FromMilliseconds((double)(Time));
                my_string1 = null;
                if (ShowTime_sb1.Length>1) ShowTime_sb1.Remove(0, ShowTime_sb1.Length);
                ShowTime_sb1.Append(ts.Hours.ToString("00")); ShowTime_sb1.Append(":");
                ShowTime_sb1.Append(ts.Minutes.ToString("00")); ShowTime_sb1.Append(":");
                ShowTime_sb1.Append(ts.Seconds.ToString("00"));
                if (MilliSecond) { ShowTime_sb1.Append("."); ShowTime_sb1.Append(ts.Milliseconds.ToString("000")); }
                //my_string = ShowTime_sb.ToString();
                //my_string = (string)ShowTime_sb.GetType().GetField("m_StringValue", BindingFlags.NonPublic | BindingFlags.Instance).GetValue(ShowTime_sb);*/
                if (MilliSecond)
                {
                    /*
                    MilliSecond_ShowTime = (int)(Time % 1000);
                    Second_ShowTime = (int)(Time / 1000) % 60;
                    Minute_ShowTime = (int)((Time / (1000 * 60)) % 60);
                    Hour_ShowTime = (int)(Time / (1000 * 3600));//% 24);*/
                    MilliSecond_ShowTime = (int)(Tick % 1000);
                    Tick = Tick / 1000;
                    Hour_ShowTime = Tick / 3600;
                    Tick = Tick % 3600;
                    Minute_ShowTime = (int)(Tick / 60);
                    Second_ShowTime = (int)(Tick % 60);
                }
                else
                {
                    Tick = Tick / 1000;
                    Hour_ShowTime = Tick / 3600;
                    Tick = Tick % 3600;
                    Minute_ShowTime = (int)(Tick / 60);
                    Second_ShowTime = (int)(Tick % 60);
                    //Minute_ShowTime = (int)((Time / 60) % 60);
                    //Hour_ShowTime = (int)((Time / (1000 * 60 * 60)));
                }
                ShowTime_sb1.Append(Hour_ShowTime.ToString("00")); ShowTime_sb1.Append(":");
                ShowTime_sb1.Append(Minute_ShowTime.ToString("00")); ShowTime_sb1.Append(":");
                ShowTime_sb1.Append(Second_ShowTime.ToString("00"));
                if (MilliSecond) { ShowTime_sb1.Append("."); ShowTime_sb1.Append(MilliSecond_ShowTime.ToString("000")); }
                ErrorCount1 = 0;
                return ShowTime_sb1.ToString();//(string)ShowTime_sb1.GetType().GetField("m_StringValue", BindingFlags.NonPublic | BindingFlags.Instance).GetValue(ShowTime_sb);//ShowTime_sb.ToString();
            }
            catch (Exception ex) { if (ErrorCount1 > 5)ex.Logging("HS_Audio::frmMain::ShowTime(uint Time, bool MilliSecond=true) 에서 예외 발생!!"); GC.Collect(); ErrorCount++; return ""; }
        }

        Stack<string> MusicIndexList = new Stack<string>();
        int ErrorCount_NextMusic;
        string tmpNextMusic;
        DateTime NextMusic_Time;

        public void NextMusic(int Timeout = 100)
        {
            lock (MusicIndexList)
            {
                if (Math.Abs(NextMusic_Time.Subtract(DateTime.Now).TotalMilliseconds) < Timeout) Thread.Sleep(100);
                NextMusic_Time = DateTime.Now;
                Debug.WriteLine("Next();");
#if DEBUG
                //return;
#endif
                ErrorCount_NextMusic = 0;
                String path = null;

                if (listView1.Items.Count > 0)
                {
                    if (tmpNextMusic != null || tmpNextMusic != Helper.MusicPath) MusicIndexList.Push(Helper.MusicPath);
                    string MusicPath = "";
                    int index = CurrentItem.Index;
                    Error:
                    ErrorCount_NextMusic++;
                    if (ErrorCount_NextMusic > listView1.Items.Count) return;
                    switch (cb반복.SelectedIndex)
                    {
                        case 0:
                            if (index == listView1.Items.Count - 1)
                                if (cb반복.SelectedIndex == 0) { CurrentItem = listView1.Items[0]; path = listView1.Items[0].SubItems[3].Text; break; }
                            goto default;
                        case 1:
                            int a = random.NextValue(0, listView1.Items.Count - 1);
                            CurrentItem = listView1.Items[a]; path = listView1.Items[a].SubItems[3].Text; break;
                        case 2: Helper.CurrentPosition = 0; return;
                        case 3: Stop(); timer1.Stop(); StopNextMusicThread = true; return;
                        default:
                            try { CurrentItem = listView1.Items[index + 1]; path = listView1.Items[index + 1].SubItems[3].Text; break; }
                            catch { /*timer1.Stop();*/ CurrentItem = listView1.Items[0]; path = listView1.Items[0].SubItems[3].Text; break; }
                    }

                    //this.Text = (ProgramName+" (디버깅) (재생 완료! {플레이어 인덱스: " + fm[0].index+"}{횟수: "+tmp++.ToString()+"})");
                    //if (fm[0].index == fm.Count) {  }
                    float Frequency = ps != null ? (float)ps.numFrenq.Value - Helper.DefaultFrequency : 0;
                    try
                    {
                        Init_Sort(false);
                        try
                        {
                            //if (cb반복.SelectedIndex != 4)
                            {
                                Helper.PreLoading = PreLoadingCheck;
                                //float Pitch = fh.LastPitch;
                                Helper.MusicPath = path;
                                if (Helper.PlayStatus == HSAudioHelper.PlayingStatus.Error ||
                                    Helper.PlayStatus == HSAudioHelper.PlayingStatus.None) { throw new HS_Audio.SoundSystemException(Helper.Result, "음악이나 사운드장치에 이상이 있는것 같습니다."); }

                                //fh.Complete = false;
                                tmpNextMusic = Helper.MusicPath;
                                timer1.Start();
                                StopNextMusicThread = false;
                                if (cb반복.SelectedIndex != 4) Helper.Play();
                                //if (fh.PlayStatus == HSAudioHelper.PlayingStatus.Error) Pitch = fh.LastPitch;
                                //if (ps != null) fh.Frequency = fh.DefaultFrequency + Frequency;

                                //if (frmdsp != null) frmdsp.ReRegister(false);
                            }
                            btn재생.InvokeIfNeeded(() => { btn재생.Enabled = true;/*재생.Text = LocalString.일시정지;*/btn재생.BackgroundImage = Properties.Resources.PauseHS; });
                            btn정지.InvokeIfNeeded(() => { btn정지.Enabled = true; });
                            btn재생설정.InvokeIfNeeded(() => { btn재생설정.Enabled = true; });
                            /*
                            btn재생.Enabled = btn정지.Enabled = btn재생설정.Enabled = true;
                            btn재생.Text = LocalString.일시정지;*/
                        }
                        catch (NullReferenceException) { return; }
                        catch { if (cb반복.SelectedIndex != 1) index = CurrentItem.Index/* + 1*/; Application.DoEvents(); goto Error; }
                        try { Helper.index = int.Parse(listView1.Items[Helper.index + 1].Text); }
                        catch { Helper.index = int.Parse(listView1.Items[0].Text); }
                        /*ps.numFrenq.Value = (decimal)(fm[0].DefaultFrequency + Frequency); */
                        /*fh.Frequency = fh.DefaultFrequency + Frequency;*/

                    }
                    catch
                    {
                        try
                        {
                            if (cb반복.SelectedIndex == 0)
                            {
                                PreLoading:
                                Helper.PreLoading = PreLoadingCheck;
                                Helper.index = 0; Helper.MusicPath = MusicPath;
                                //if (ps != null) fh.Frequency = fh.DefaultFrequency + Frequency; fh.Frequency = fh.DefaultFrequency + Frequency; 
                                Helper.Play(); Helper.index = 0;
                            }
                        }
                        catch { }
                    }
                    if (ps != null)
                    {
                        ps.InvokeIfNeeded(() =>
                        {
                            try { ps.Title = string.Format("{0}", System.IO.Path.GetFileName(MusicPath)); }
                            catch { ps.Title = null; }
                            ps.numPitch_ValueChanged(null, null);
                        });
                        //try { ps.numFrenq.Value = (decimal)fm[0].Frequency; }
                        //catch { if (fm[0].Frequency < 0) ps.numFrenq.Minimum = (decimal)fm[0].Frequency; else ps.numFrenq.Maximum=(decimal)fm[0].Frequency; }
                    }
                    //if (frmdsp != null) frmdsp.ReRegister();
                }
            }
        }

        public void BeforeMusic(bool ThrowError = false)
        {
            if(Helper!=null&&Helper.CurrentPosition>3000){Helper.CurrentPosition= 0;return;}
            if (listView1.Items.Count > 0)
            {
            Error:
                try{
                    if (MusicIndexList.Count == 0)
                    {
                        int index = 0;
                        for (int i = 0; i < listView1.Items.Count; i++)
                            if (listView1.Items[i].SubItems[3].Text == Helper.MusicPath) index = i;
                        switch (cb반복.SelectedIndex)
                        {
                            //case 0: if (index == 0)goto default;
                            //case 1: return;
                            //case 2: Stop(); Play(); return;
                            //case 3: Stop(); timer1.Stop(); StopNextMusicThread = true; return;
                            default:
                            try
                            {
                                if (index == 0)
                                {
                                    CurrentItem = listView1.Items[listView1.Items.Count - 1];
                                    MusicPath = listView1.Items[listView1.Items.Count - 1].SubItems[3].Text;
                                }
                                else
                                {
                                    CurrentItem = listView1.Items[index - 1];
                                    MusicPath = listView1.Items[index - 1].SubItems[3].Text;
                                } break;
                            }
                            catch
                            { /*timer1.Stop();*/
                                CurrentItem = listView1.Items[0];
                                MusicPath = listView1.Items[0].SubItems[3].Text; break;
                            }
                        }
                    }
                    else
                    {
                    Retry:
                        string Path = MusicIndexList.Pop();
                        if (Path == Helper.MusicPath)
                        {
                            Helper.CurrentPosition = 0; return;
                        }
                        ListViewItem li = null;
                        for (int i = 0; i < listView1.Items.Count; i++)
                            if (listView1.Items[i].SubItems[3].Text == Path) li = listView1.Items[i];
                        if(li!=null){CurrentItem = li;MusicPath = Path;}else if(MusicIndexList.Count==0)return; else goto Retry;
                    }
                    
                    try
                    {
                        Init_Sort(false);
                    PreLoading:
                        Helper.PreLoading = PreLoadingCheck;
                        //float Pitch = fh.LastPitch;
                        Helper.MusicPath = CurrentItem.SubItems[3].Text;
                        timer1.Start();
                        Helper.Play();
                        StopNextMusicThread = false;
                        //if (fh.PlayStatus == HSAudioHelper.PlayingStatus.Error) Pitch = fh.LastPitch;
                        //if (ps != null) fh.Frequency = fh.DefaultFrequency + Frequency;

                        //if (frmdsp != null) frmdsp.ReRegister(false);
                        
                        btn재생.InvokeIfNeeded(()=>{btn재생.Enabled = true;/*btn재생.Text = LocalString.일시정지;*/btn재생.BackgroundImage = Properties.Resources.PauseHS;});
                        btn정지.InvokeIfNeeded(()=>{btn정지.Enabled = true;});
                        btn재생설정.InvokeIfNeeded(()=>{btn재생설정.Enabled = true;});
                        /*
                        btn재생.Enabled = btn정지.Enabled = btn재생설정.Enabled = true;
                        btn재생.Text = LocalString.일시정지;*/
                    }
                    catch(NullReferenceException){return;}
                    catch { /*if (cb반복.SelectedIndex != 1)index = CurrentItem.Index - 1;*/ goto Error; }
                    try { Helper.index = int.Parse(listView1.Items[Helper.index + 1].Text); }
                    catch { Helper.index = int.Parse(listView1.Items[0].Text); }
                    /*ps.numFrenq.Value = (decimal)(fm[0].DefaultFrequency + Frequency); */
                    /*fh.Frequency = fh.DefaultFrequency + Frequency;*/
                    if (cb반복.SelectedIndex != 4) Helper.Play();
                }
                catch
                {
                    try
                    {
                        if (cb반복.SelectedIndex == 0)
                        {
                        PreLoading:
                            Helper.PreLoading = PreLoadingCheck;
                            Helper.index = 0; Helper.MusicPath = MusicPath;
                            //if (ps != null) fh.Frequency = fh.DefaultFrequency + Frequency; fh.Frequency = fh.DefaultFrequency + Frequency; 
                            Helper.Play(); Helper.index = 0;
                        }
                    }
                    catch { }
                }
                if (ps != null)
                {
                    ps.InvokeIfNeeded(() =>
                    {
                        try { ps.Title = string.Format("{0}", System.IO.Path.GetFileName(MusicPath)); }
                        catch {ps.Title = null; }
                        ps.numPitch_ValueChanged(null, null);
                    });
                    //try { ps.numFrenq.Value = (decimal)fm[0].Frequency; }
                    //catch { if (fm[0].Frequency < 0) ps.numFrenq.Minimum = (decimal)fm[0].Frequency; else ps.numFrenq.Maximum=(decimal)fm[0].Frequency; }
                }
                //if (frmdsp != null) frmdsp.ReRegister();
            }
        }
        public void RandomMusic()
        {
            int id = cb반복.SelectedIndex;
            cb반복.SelectedIndex = 1;
            NextMusic();
            cb반복.SelectedIndex = id;
        }
        #endregion

        [DllImport("gdi32.dll", CharSet = CharSet.Auto, SetLastError = true, ExactSpelling = true)]
        public static extern int GetDeviceCaps(IntPtr hDC, int nIndex);
        private float getScalingFactor()
        {
            float ScreenScalingFactor  = 0;
            using (Graphics g = Graphics.FromHwnd(IntPtr.Zero))
            {
                try
                {
                    IntPtr desktop = g.GetHdc();
                    int LogicalScreenHeight = GetDeviceCaps(desktop, 10);//DeviceCap.VERTRES
                    int PhysicalScreenHeight = GetDeviceCaps(desktop, 117);//DeviceCap.DESKTOPVERTRES

                    ScreenScalingFactor = (float)PhysicalScreenHeight / (float)LogicalScreenHeight;
                }catch{}
            }
            return ScreenScalingFactor; // 1.25 = 125%
        }

        bool PreLoading = true;
        Image LastCurrentImageforPlaylist;
        bool IsLoading = false;
        MemoryStream PlayListImageStream = null;
        Dictionary<string, string> Setting
        {
            get
            {
                Dictionary<string, string> a = new Dictionary<string,string>();
                a.Add("TopMost", 맨위로설정ToolStripMenuItem.Checked.ToString());
                a.Add("ScaleMode", this.AutoScaleMode.ToString());
                a.Add("ScaleDemension", HS_CSharpUtility.Utility.ConvertUtility.SizeFToString(this.AutoScaleDimensions, true));
                a.Add("AutoScaleMode", this.자동스케일ToolStripMenuItem.Checked.ToString());
                a.Add("ShowMillisecond", 밀리초표시ToolStripMenuItem.Checked.ToString());
                a.Add("PreLoadMode", PreLoadingCheck.ToString());
                a.Add("\r\n//Language", "0: 한국어, 1: 日本語");
                a.Add("Language", toolStripComboBox1.SelectedIndex.ToString());
                a.Add("PlayListShowGrid", 모눈선표시ToolStripMenuItem.Checked.ToString());
                a.Add("PlayListSelectItemHighlight", 선택한항목강조표시ToolStripMenuItem.Checked.ToString());
                a.Add("PlayListBackgroundColor", HS_CSharpUtility.Utility.ConvertUtility.ColorToString(listView1.BackColor, true));
                a.Add("PlayListTextColor", HS_CSharpUtility.Utility.ConvertUtility.ColorToString(listView1.ForeColor, true));
                a.Add("PlayListInsertColor", HS_CSharpUtility.Utility.ConvertUtility.ColorToString(colorDialog2.Color, true));

                a.Add("\r\n//아래의 값들은 절때 건드리지 마세요!",null);
                try
                {
                    byte[] array = Utility.EtcUtility.SerializableSaveToByteArray(CustomFont);
                    a.Add("PlayListFont", Utility.EtcUtility.ConvertByteArrayTostring(array));
                }  catch{}
                /*
                //if ((CurrentImageforPlaylist != null && CurrentImageforPlaylist.Equals(LastCurrentImageforPlaylist)) || LastCurrentImageforPlaylist == null)
                if (CurrentImageforPlaylist != null)
                {

                }*/
                
                return a;
            }
            set
            {
                IsLoading = true;
                Dictionary<string, string> a = value;

                try
                {
                    if (프리로드모드ToolStripMenuItem != null&& a.ContainsKey("PreLoadMode"))
                    {

                        PreLoading = PreLoadingCheck = bool.Parse(a["PreLoadMode"]);
                        if (fm != null && fm.Count > 0) for (int i = 0; i < fm.Count; i++) fm[i].PreLoading = PreLoading;
                        if (Helper != null) Helper.PreLoading = PreLoading;
                        프리로드모드ToolStripMenuItem.Checked = PreLoading;
                    }
                }catch{}

                if (맨위로설정ToolStripMenuItem!=null&&a.ContainsKey("TopMost"))try{this.TopMost=맨위로설정ToolStripMenuItem.Checked = bool.Parse(a["TopMost"]);}catch{}

                LockAutoScaleMode = true;
                System.Windows.Forms.AutoScaleMode scale = System.Windows.Forms.AutoScaleMode.Font;
                if (this.toolStripComboBox5 != null && a.ContainsKey("AutoScaleMode"))
                    try
                    {
                        bool AutoScalemode = true;

                        try { AutoScalemode = bool.Parse(a["AutoScaleMode"]); if (자동스케일ToolStripMenuItem != null) 자동스케일ToolStripMenuItem.Checked = AutoScalemode; } catch { }
                        try { scale = (System.Windows.Forms.AutoScaleMode)Enum.Parse(typeof(System.Windows.Forms.AutoScaleMode), a["ScaleMode"]); } catch { }

                        SizeF scaleF = this.AutoScaleDimensions;
                        try { scaleF = HS_CSharpUtility.Utility.ConvertUtility.StringToSizeF(a["ScaleDemension"]); } catch { }

                        this.AutoScaleMode = scale;
                        switch (scale)
                        {
                            case System.Windows.Forms.AutoScaleMode.Dpi:
                            float DPI = getScalingFactor();
                            if (AutoScalemode) scaleF = new SizeF(DPI == 0 ? 96 : DPI * 100, DPI == 0 ? 96 : DPI * 100);
                            this.AutoScaleDimensions = scaleF;
                            this.toolStripComboBox5.SelectedIndex = 1; break;
                            case System.Windows.Forms.AutoScaleMode.None:
                            this.AutoScaleDimensions = scaleF;
                            this.toolStripComboBox5.SelectedIndex = 2;
                            break;
                            default:
                            if (AutoScalemode) scaleF = new SizeF(System.Drawing.SystemFonts.DefaultFont.SizeInPoints, System.Drawing.SystemFonts.DefaultFont.Height);
                            this.AutoScaleDimensions = scaleF;
                            this.toolStripComboBox5.SelectedIndex = 0; break;
                        }
                    }
                    catch { }
                    finally { }

                if(밀리초표시ToolStripMenuItem!=null&&a.ContainsKey("ShowMillisecond"))try {밀리초표시ToolStripMenuItem.Checked = bool.Parse(a["ShowMillisecond"]); }catch{}
                if (toolStripComboBox1 != null && a.ContainsKey("Language"))
                try
                {
                    switch (Convert.ToInt32(a["Language"]))
                    {
                        case 1: toolStripComboBox1.SelectedIndex = 1; break;
                        default: toolStripComboBox1.SelectedIndex = 0; break;
                    }
                    toolStripComboBox1.Text = toolStripComboBox1.Items[toolStripComboBox1.SelectedIndex].ToString();
                }catch{toolStripComboBox1.SelectedIndex = 0; toolStripComboBox1.Text = "한국어";}

                LockAutoScaleMode = false;

                if(모눈선표시ToolStripMenuItem!=null&&a.ContainsKey("PlayListShowGrid"))try{모눈선표시ToolStripMenuItem.Checked = bool.Parse(a["PlayListShowGrid"]);}catch{}
                if(선택한항목강조표시ToolStripMenuItem!=null&&a.ContainsKey("PlayListSelectItemHighlight"))try{선택한항목강조표시ToolStripMenuItem.Checked = bool.Parse(a["PlayListSelectItemHighlight"]);}catch{}
                if(colorDialog1!=null&&a.ContainsKey("PlayListBackgroundColor"))try{colorDialog1.Color = listView1.BackColor= HS_CSharpUtility.Utility.ConvertUtility.StringToColor(a["PlayListBackgroundColor"]);}catch{}
                if(cdLabelText!=null&&a.ContainsKey("PlayListTextColor"))try{cdLabelText.Color =listView1.ForeColor=HS_CSharpUtility.Utility.ConvertUtility.StringToColor(a["PlayListTextColor"]); SearchPlayList(false);}catch{}
                if(colorDialog2!=null&&a.ContainsKey("PlayListInsertColor"))try{colorDialog2.Color =HS_CSharpUtility.Utility.ConvertUtility.StringToColor(a["PlayListInsertColor"]);}catch{}

                if (listView1 != null&&a.ContainsKey("PlayListFont"))
                try
                {
                    byte[] fb = Utility.EtcUtility.ConvertStringToByteArray(a["PlayListFont"], false);
                    Font fj = Utility.EtcUtility.SerializableOpen(fb) as Font;
                    //if (f != null && ff != null) label1.Font = new Font(ff, f.Size, f.Style);
                    //if (f != null) 
                    CustomFont = fontDialog1.Font = listView1.Font = fj;

                    for (int i = 0; i < listView1.Items.Count; i++) listView1.Items[i].Font = CustomFont;
                    CurrentItem = _CurrentItem;

                    //else if (ff != null) label1.Font = new Font(ff, label1.Font.Size, label1.Font.Style);
                    //else throw new Exception();
                }catch{}

                IsLoading = false;
            }
        }

        string ApplicationDir
        {
            get
            {
                string tmp = Process.GetCurrentProcess().MainModule.FileName; 
                return tmp.Remove(tmp.LastIndexOf("\\"));
            }
        }
        bool LockAutoScaleMode;
        public bool UpdateSetting(bool Save=true, bool First = false)
        {
            if (!IsLoading)
            {
                if (Save)
                {
                    try
                    {
                        /*
                        Dictionary <string, string> Pic = new Dictionary<string,string>();
                        Pic.Add("PlayListBackgroundImage", Setting["PlayListBackgroundImage"]);

                        Dictionary<string, string> tmpset = Setting;
                        if (tmpset.ContainsKey("PlayListBackgroundImage")) tmpset.Remove("PlayListBackgroundImage");*/

                        string[] a = HS_CSharpUtility.Utility.EtcUtility.SaveSettingWithoutFilter(Setting);
                        //if(!Directory.Exists(ApplicationDir+))
                        File.WriteAllLines(ApplicationDir + "\\MainSetting.ini", a);
                        return true;
                    }
                    catch(Exception ex){ex.Logging(); return false; }
                }
                else
                {
                    try
                    {
                        string[] a = File.ReadAllLines(ApplicationDir + "\\MainSetting.ini");
                        Dictionary<string, string> b = HS_CSharpUtility.Utility.EtcUtility.LoadSetting(a);
                        /*
                        if (First)
                        {
                            System.Windows.Forms.AutoScaleMode scale = System.Windows.Forms.AutoScaleMode.Font;
                            try
                            {
                                scale= (System.Windows.Forms.AutoScaleMode)Enum.Parse(typeof(System.Windows.Forms.AutoScaleMode), b["AutoScalemode"]);
                            
                                //switch (scale)
                                //{
                                //   case System.Windows.Forms.AutoScaleMode.Dpi: this.toolStripComboBox5.SelectedIndex = 2; break;
                                //    case System.Windows.Forms.AutoScaleMode.Font: this.toolStripComboBox5.SelectedIndex = 1; break;
                                //    case System.Windows.Forms.AutoScaleMode.None: this.toolStripComboBox5.SelectedIndex = 0; break;
                                //    default: this.toolStripComboBox5.SelectedIndex = 3; break;
                                //}
                            }
                            catch{}finally{ base.AutoScaleMode =this.AutoScaleMode = scale;}
                            try{this.TopMost=base.TopMost = bool.Parse(b["TopMost"]);}catch{}
                        }
                        else*/
                        Setting = b;

                        byte[] array = null;
                        if (listView1 != null)
                        try
                        {
                            if (PlayListImageStream != null) PlayListImageStream.Close();
                           
                            if (File.Exists(ApplicationDir + "\\PlayListBackgroundImage.ini"))
                            {
                                string[] aa = File.ReadAllLines(ApplicationDir + "\\PlayListBackgroundImage.ini");

                                array = HS_CSharpUtility.Utility.EtcUtility.ConvertStringToByteArray(aa[aa.Length > 1 ? 1 : 0], true);
                                PlayListImageStream = new MemoryStream(array);

                                CurrentImageforPlaylistFlag = false;
                                CurrentImageforPlaylist = Image.FromStream(PlayListImageStream);
                            }
                        }
                        catch{}
                        finally{}

                        return true;
                    }
                    catch{ } 
                }
            }
            return false;
        }
        public SoundDeviceFormat getSettingDeviceFormat()
        {
            string path = ApplicationDir + "\\Settings\\SoundDeviceFormat.ini";
            try
            {
                if (File.Exists(path))
                {
                    SoundDeviceFormat format = new SoundDeviceFormat();
                    SoundDeviceFormat _format = null;
                    if (Helper != null) _format = Helper.getDeviceFormat();
                    else if (fm != null && fm.Count > 0) _format = fm[0].getDeviceFormat();

                    string[] a = File.ReadAllLines(path);
                    Dictionary<string, string> b = HS_CSharpUtility.Utility.EtcUtility.LoadSetting(a);

                    try { if (b.ContainsKey("Apply")) format.IsDefault = !Convert.ToBoolean(b["Apply"]); } catch { }
                    try { if (b.ContainsKey("Samplerate")) format.Samplerate = Convert.ToInt32(b["Samplerate"]); } catch { format.Samplerate = _format == null ? 44100 : _format.Samplerate; }
                    try { if (b.ContainsKey("SamplerateAuto")) format.SamplerateAuto = Convert.ToBoolean(b["SamplerateAuto"]); } catch { }
                    try { if (b.ContainsKey("ResamplerMethod")) format.ResamplerMethod = (HS_Audio_Lib.DSP_RESAMPLER)Enum.Parse(typeof(HS_Audio_Lib.DSP_RESAMPLER), b["ResamplerMethod"]); } catch { format.ResamplerMethod = _format == null ? HS_Audio_Lib.DSP_RESAMPLER.LINEAR : _format.ResamplerMethod; }
                    try { if (b.ContainsKey("OuputFormat")) format.SoundFormat = (HS_Audio_Lib.SOUND_FORMAT)Enum.Parse(typeof(HS_Audio_Lib.SOUND_FORMAT), b["OuputFormat"]); } catch { format.SoundFormat = _format == null ? (Utility.IsWindowsXP_Higher ? HS_Audio_Lib.SOUND_FORMAT.PCMFLOAT : HS_Audio_Lib.SOUND_FORMAT.PCM16) : _format.SoundFormat; }
                    try { if (b.ContainsKey("OuputType")) format.OutputType = (HS_Audio_Lib.OUTPUTTYPE)Enum.Parse(typeof(HS_Audio_Lib.OUTPUTTYPE), b["OuputType"]); } catch { format.OutputType = _format == null ? HS_Audio_Lib.OUTPUTTYPE.AUTODETECT : _format.OutputType; }
                    try { if (b.ContainsKey("OutputChannel")) format.OutputChannel = Convert.ToInt32(b["OutputChannel"]); } catch { format.OutputChannel = _format == null ? 2 : _format.OutputChannel; }
                    try { if (b.ContainsKey("InputChannelMax")) format.MaxInputChannel = Convert.ToInt32(b["InputChannelMax"]); } catch { format.MaxInputChannel = _format == null ? 6 : _format.MaxInputChannel; }
                    return format;
                }
                else return null;
            }
            catch (Exception ex) { ex.Logging(); return null; }
        }
        public bool checkSettingDeviceFormat(SoundDeviceFormat format)
        {
            string path = ApplicationDir + "\\Settings\\SoundDeviceFormat.ini";
            if (!File.Exists(path))
            {
                try
                {
                    if (!Directory.Exists(ApplicationDir + "\\Settings")) Directory.CreateDirectory(ApplicationDir + "\\Settings");

                    Dictionary<string, string> a = new Dictionary<string, string>();
                    a.Add(";HS™ 플레이어 사운드 출력 품질 및 포맷 구성설정 파일 (장착된 사운드카드의 최대 한도까지만 설정해 주세요..)", null);
                    a.Add(";Sound output quality and format configuration file (Please set only the maximum limit of the installed sound card.)", null);
                    a.Add(";설정을 변경하시면 [설정] - [프리 로드 모드] 를 켜두시는걸 권장합니다.", null);
                    a.Add(";We recommend that you turn on  [설정] - [프리 로드 모드]  when you change the setting.", null);
                    a.Add(";\r\n====Whether to apply the settings (설정 적용 여부) [Exclude OuputType (OuputType 제외)]", null);
                    a.Add(";;;;;True: 설정 적용 (Apply settings), False: 설정 적용 안함 (Don't Apply settings)", null);
                    a.Add("Apply", false.ToString());

                    a.Add("\r\n;====Samplerate (샘플레이트)", null);
                    a.Add("Samplerate", format.Samplerate.ToString());

                    a.Add("\r\n;====Resampler method(Algorithm) (리 샘플링 방법(알고리즘))====", null);
                    a.Add(";;;;;NOINTERP [No interpolation. High frequency aliasing hiss will be audible depending on the sample rate of the sound. (보간 없음. 사운드의 샘플레이트에 따라 고주파수 잡음이 생길 수 있습니다.)]", null);
                    a.Add(";;;;;LINEAR (default(기본값)) [Linear interpolation. Fast and good quality, causes very slight lowpass effect on low frequency sounds. (선형 보간. 빠르지만 품질은 보통이고 약간의 로우패스 효과떄문에 고주파가 조금 깍입니다.)]", null);
                    a.Add(";;;;;CUBIC [Cubic interpolation. Slower than linear interpolation but better quality. (입방 보간. 선형 보간법보다 느리지만 품질은 우수합니다.)]", null);
                    a.Add(";;;;;SPLINE [5 point spline interpolation. Slowest resampling method but best quality. (5점 스플 라인 보간. 매우 느리지만 품질은 최상입니다.)]", null);
                    a.Add("ResamplerMethod", "LINEAR");

                    a.Add("\r\n;====Ouput format (출력 포맷)", null);
                    a.Add(";;;;;PCM8  [8bit integer PCM data (8 비트 정수)]", null);
                    a.Add(";;;;;PCM16 [16bit integer PCM data (16 비트 정수)]", null);
                    a.Add(";;;;;PCM24 [24bit integer PCM data (24 비트 정수)]", null);
                    a.Add(";;;;;PCM32 [32bit integer PCM data (32 비트 정수)]", null);
                    a.Add(";;;;;PCMFLOAT [32bit floating point PCM data (32 비트 부동소숫점)]", null);
                    a.Add("OuputFormat", format.SoundFormat.ToString());


                    a.Add("\r\n;====Channel to output (출력 채널) [Don't modify (수정하지 마십시오)]", null);
                    a.Add("OutputChannel", format.OutputChannel.ToString()); //2
                
                    a.Add("\r\n;====Channel to maximum input (최대 입력 채널)", null);
                    a.Add("InputChannelMax", format.MaxInputChannel.ToString()); //6


                    a.Add("\r\n\r\n;====Ouput type (출력 타입)", null);
                    a.Add(";;;;;AUTODETECT (Default(기본값)) [Auto detect (자동 선택)]", null);
                    a.Add(";;;;;DSOUND [DirectSound output]", null);
                    a.Add(";;;;;WASAPI [Windows Audio Session API]", null);
                    a.Add(";;;;;WINMM [Windows Multimedia output]", null);
                    a.Add(";;;;;ASIO [Low latency ASIO 2.0 driver]", null);
                    a.Add(";;;;;NOSOUND [Make no sound (소리 없음)]", null);
                    a.Add("OuputType", "AUTODETECT");


                    string[] settings = HS_CSharpUtility.Utility.EtcUtility.SaveSettingWithoutFilter(a);
                    File.WriteAllLines(path, settings);
                }
                catch (Exception ex) { ex.Logging(); return false; }
            }
            return true;
        }
        
        internal void button1_Click(object sender, EventArgs e)
        {
            if (음악믹싱모드ToolStripMenuItem.Checked) { 재생ToolStripMenuItem_Click(null, null); }//Play(listView1.SelectedItems[0].Index); }
            else 
            {
                if (!PlayText)
                {
                    Play(); 
                    PlayText = true; 
                    try{if(PlayChanged!=null) PlayChanged(true);}
                    catch{} 
                    //btn재생.Text = LocalString.일시정지;
                    btn재생.BackgroundImage = Properties.Resources.PauseHS;
                    재생ToolStripMenuItem1.Image.Dispose();
                    재생ToolStripMenuItem1.Image = InvertImage(HS_Audio.Properties.Resources.Pause_29);
                }
                else
                {
                    Pause(); 
                    PlayText = false; 
                    try{PlayChanged(false);}catch{ } 
                    //btn재생.Text = LocalString.재생;
                    btn재생.BackgroundImage = Properties.Resources.PlayHS;
                    재생ToolStripMenuItem1.Image.Dispose();
                    재생ToolStripMenuItem1.Image = InvertImage(HS_Audio.Properties.Resources.Play_29);
                }
                //Init_Sort(false);
                //timer1.Start(); 
                tmpStop = false;
                
                //for(int i=0;i<listView1.Items.Count;i++)
                //{ listView1.Items[i].SubItems[1].Text = ""; if (fm[0].index == i) { listView1.Items[i].SubItems[1].Text = "현재 ("+Status_NoMixing.ToString()+")"; } }
            }
            timer1.Start();
            StopNextMusicThread = false;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            HS_Audio.HSAudioHelper fh = null;
            
            try { fh = new HS_Audio.HSAudioHelper(textBox1.Text, getSettingDeviceFormat()); }
            catch (SoundSystemException ex)
            {
                if(ex.Result == HS_Audio_Lib.RESULT.ERR_MEMORY)
                MessageBox.Show("더 이상 파일을 추가할 수 없습니다.\n\n다른 항목을 제거하고 다시 시도하세요!", "파일 더이상 추가 불가", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                if(ex.Result == HS_Audio_Lib.RESULT.ERR_FILE_NOTFOUND)
                MessageBox.Show("파일을 찾을 수 없거나 올바른 사운드파일이 아닙니다.", "파일 추가 실패 알림", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            fh.PlayingStatusChanged += new HS_Audio.HSAudioHelper.PlayingStatusChangedEventHandler(fm_PlayingStatusChanged);
            fh.MusicChanging += new HSAudioHelper.MusicChangeEventHandler(fh_MusicPathChanged);
            if (true/*fh.HelperEx.Result == FMOD.RESULT.OK*/)
            {
                //if (MixingMode)
                {
                    //timer1.Start();
                    fm.Add(fh);
                    frmdsp1.Add(new HS_Audio.Forms.frmDSP(fh, false));
                    fh.Loop = true;
                    timer1.Start();
                    StopNextMusicThread = false;

                    fh.index = fm.Count - 1;
                    ListViewItem li = new ListViewItem(fh.index.ToString());
                    li.SubItems.Add(fh.PlayStatus.ToString());
                    li.SubItems.Add(System.IO.Path.GetFileName(fh.MusicPath));
                    li.SubItems.Add(fh.MusicPath);
                    li.SubItems.Add("");

                    li.Font = CustomFont;
                    listView1.Items.Add(li);
                    ps1.Add(new PlayerSetting(fh, System.IO.Path.GetFileName(fh.MusicPath)));
                    try { if (PlaylistChanged != null) PlaylistChanged(this, PlayListChange.Add, listView1.Items.Count - 1); } catch { }
                }
            }
            else { fh.Dispose(); }
        }

        private void openFileDialog1_FileOk(object sender, CancelEventArgs e)
        {
            
        }

        internal PlayerSetting ps;
        List<PlayerSetting> ps1 = new List<PlayerSetting>();
        private void button4_Click(object sender, EventArgs e)
        {
            if (음악믹싱모드ToolStripMenuItem.Checked)
            {
                for (int i = 0; i < listView1.SelectedItems.Count; i++)
                {
                    if (ps1.Count ==0)
                    {ps1.Add(new PlayerSetting(fm[listView1.SelectedItems[i].Index], listView1.SelectedItems[i].SubItems[2].Text));}
                    ps1[listView1.SelectedItems[i].Index].Show();
                    ps1[listView1.SelectedItems[i].Index].BringToFront();
                }
            }
            else
            {
                if (ps == null){ps = new PlayerSetting(Helper, System.IO.Path.GetFileName(Helper.MusicPath));}
                //System.IO.Path.GetFileName(fm[0].MusicPath); 
                ps.Show();
                ps.BringToFront();
            }
        }

        StringBuilder sb = new StringBuilder();
        private void listView1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (음악믹싱모드ToolStripMenuItem.Checked)
            {
                if (listView1.SelectedItems.Count > 0)
                {
                    if (listView1.SelectedItems.Count == 1) 
                    { 
                        indexing = listView1.SelectedItems[0].Index; 
                        현재위치의웨이브저장ToolStripMenuItem.Enabled = true;
                        toolStripProgressBar2.Visible = true;
                    }
                    else { 현재위치의웨이브저장ToolStripMenuItem.Enabled = false; }
                    timer1_Tick_sb.Append(fm[listView1.SelectedItems[0].Index].MusicPath);
                    for (int i = 1; i < listView1.SelectedItems.Count; i++)
                    {
                        timer1_Tick_sb.Append("\r\n" + fm[listView1.SelectedItems[i].Index].MusicPath);
                    }
                    textBox2.Text = timer1_Tick_sb.ToString();
                    timer1_Tick_sb.Remove(0, timer1_Tick_sb.Length);
                    btn재생.Enabled = btn정지.Enabled=button5.Enabled = btn재생설정.Enabled = 재생ToolStripMenuItem.Enabled = 정지ToolStripMenuItem.Enabled = 삭제ToolStripMenuItem.Enabled = 일시정지ToolStripMenuItem.Enabled = true;
                }
                else
                {
                    btn재생.Enabled = btn정지.Enabled = button5.Enabled = btn재생설정.Enabled = 재생ToolStripMenuItem.Enabled = 정지ToolStripMenuItem.Enabled = 삭제ToolStripMenuItem.Enabled = 일시정지ToolStripMenuItem.Enabled = false;
                    현재위치의웨이브저장ToolStripMenuItem.Enabled = false;
                    Init_Sort(true);
                    textBox2.Text = "";
                }
            }
            else
            {
                /*for (int i = 0; i < listView1.Items.Count; i++)
                {
                    listView1.Items[i].Text = i.ToString();
                    if (fm[0].index.ToString() == listView1.Items[i].Text)
                    { listView1.Items[i].SubItems[1].Text = "현재"; }
                    else { listView1.Items[i].SubItems[1].Text = ""; }
                }*/
                if (listView1.SelectedItems.Count > 0)
                {
                    timer1_Tick_sb.Append(listView1.SelectedItems[0].SubItems[3].Text);
                    for (int i = 1; i < listView1.SelectedItems.Count; i++)
                    {timer1_Tick_sb.Append("\r\n" + listView1.SelectedItems[i].SubItems[3].Text); }
                    textBox2.Text = timer1_Tick_sb.ToString();
                    timer1_Tick_sb.Remove(0, timer1_Tick_sb.Length);
                }
                else { textBox2.Text = ""; }
                //Init_Sort(false);
                if (listView1.SelectedItems.Count == 1)
                {재생ToolStripMenuItem.Enabled = 정지ToolStripMenuItem.Enabled = 일시정지ToolStripMenuItem.Enabled = 삭제ToolStripMenuItem.Enabled = true; }
                else if (listView1.SelectedItems.Count > 0)
                { 재생ToolStripMenuItem.Enabled = 정지ToolStripMenuItem.Enabled = 일시정지ToolStripMenuItem.Enabled = false; 삭제ToolStripMenuItem.Enabled = true;  }
                else { 재생ToolStripMenuItem.Enabled = 정지ToolStripMenuItem.Enabled = 삭제ToolStripMenuItem.Enabled = 일시정지ToolStripMenuItem.Enabled = false; }

                if (listView1.Items.Count == 0) {tmpNextMusic = null; MusicIndexList.Clear();}
                //button1.Enabled = button2.Enabled = button4.Enabled = 재생ToolStripMenuItem.Enabled = 정지ToolStripMenuItem.Enabled = 삭제ToolStripMenuItem.Enabled = 일시정지ToolStripMenuItem.Enabled = true; 
            }
            //if (asyncToolStripMenuItem.Checked) Application.DoEvents();
            //Application.DoEvents();
        }

        public void 재생ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            List<ListViewItem> items=new List<ListViewItem>(listView1.SelectedItems.Count);
            ListViewItem li = sender as ListViewItem;
            if (li != null) items.Add(li);
            else foreach (ListViewItem a in listView1.SelectedItems) items.Add(a);

            for (int i = 0; i < items.Count; i++)
            {
                try{
                if (음악믹싱모드ToolStripMenuItem.Checked)
                {
                    Play(items[i].Index);
                    items[i].SubItems[1].Text = LocalString.현재 + " (Play)";
                    frmdsp1[i].ReRegister(false);
                    //fm[i].PlayStatus.ToString();
                }
                else
                {
                    if (/*fm.Count > 0 */Helper!=null&&!새인스턴스시작ToolStripMenuItem.Checked&&
                        items[0].Text == Helper.index.ToString()) Play();
                    else
                    {
                        CurrentItem = items[i];
                        //float Frequency=0;
                        if (Helper == null)
                        {
                            Helper = new HS_Audio.HSAudioHelper(getSettingDeviceFormat()); Helper.PreLoading = PreLoading;
                            if (!PreLoading) Helper.setSoundDevice(0);
                            Helper.PlayingStatusChanged += new HSAudioHelper.PlayingStatusChangedEventHandler(fh_PlayingStatusChanged);
                            Helper.MusicChanging += new HSAudioHelper.MusicChangeEventHandler(fh_MusicPathChanged);
                            Helper.PreLoading = PreLoadingCheck;
                            Helper.MusicPath = CurrentItem.SubItems[3].Text;
                            trackBar1.Enabled = true;
                        }
                        else
                        {
                                Helper.CurrentPosition = 0;
                                //if ((tmpNextMusic != null && tmpNextMusic != "") && tmpNextMusic != fh.MusicPath) MusicIndexList.Push(fh.MusicPath);
                                //float Frequency = ps!=null?(float)ps.numFrenq.Value - fh.DefaultFrequency:0;
                             PreLoading:
                                Helper.PreLoading = PreLoadingCheck;
                                Helper.MusicPath = CurrentItem.SubItems[3].Text;
                                //if(ps!=null)fh.Frequency = fh.DefaultFrequency + Frequency;
                                if (frmdsp != null) frmdsp.ReRegister(false);
                                btn재생.Enabled = btn정지.Enabled = btn재생설정.Enabled = true;
                                //btn재생.Text = LocalString.일시정지;
                                btn재생.BackgroundImage = Properties.Resources.PauseHS;
                                timer1.Start();
                                StopNextMusicThread = false;

                                Helper.Play();
                                CurrentItem.EnsureVisible();
                            }
                        }
                        tmpNextMusic = Helper.MusicPath;
                        //fm[0].Frequency = fm[0].DefaultFrequency + Frequency;
                        //try { ps.numFrenq.Value = (decimal)(fm[0].DefaultFrequency + Frequency); }
                        //catch { if (fm[0].Frequency < 0) ps.numFrenq.Minimum = (decimal)fm[0].Frequency; else ps.numFrenq.Maximum=(decimal)fm[0].Frequency; }
                        for (int j = 0; j < listView1.Items.Count; j++) { listView1.Items[j].Text = j.ToString(); listView1.Items[j].SubItems[1].Text = ""; }
                        //listView1.SelectedItems[i].SubItems[1].Text = LocalString.현재+ " (Ready)";
                        Helper.index = int.Parse(CurrentItem.Text);
                        if (ps!=null) ps.Title = string.Format("{0}", System.IO.Path.GetFileName(Helper.MusicPath));
                        tmpStop = false;
                        Init_Sort(false);
                        if (tmpNextMusic!=null||tmpNextMusic != Helper.MusicPath) MusicIndexList.Push(Helper.MusicPath);
                    }
                }
                catch{}
            }
        }

        internal void 일시정지ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < listView1.SelectedItems.Count; i++)
            {
                if (음악믹싱모드ToolStripMenuItem.Checked)
                {
                    Pause(listView1.SelectedItems[i].Index);
                    listView1.SelectedItems[i].SubItems[1].Text = "Pause";
                }//fm[i].PlayStatus.ToString();
                else{Pause();}
            }
        }

        internal void 정지ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < listView1.SelectedItems.Count; i++)
            {
                if(음악믹싱모드ToolStripMenuItem.Checked){
                Stop(listView1.SelectedItems[i].Index);
                listView1.SelectedItems[i].SubItems[1].Text = "Stop";}//fm[i].PlayStatus.ToString();
                else{Stop();}
            }
        }

        private void 삭제ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DialogResult dr;
            if (listView1.SelectedItems.Count == 1) dr = MessageBox.Show(LocalString.삭제ToolStripMenuItem_Message, LocalString.삭제ToolStripMenuItem_MessageTitle, MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
            else dr = MessageBox.Show(LocalString.삭제ToolStripMenuItem_Message1, LocalString.삭제ToolStripMenuItem_MessageTitle, MessageBoxButtons.YesNo, MessageBoxIcon.Warning);

            if (dr == System.Windows.Forms.DialogResult.Yes)
            {
                tmpStop = true;
                List<int> delete = new List<int>();
                for (int j = listView1.SelectedItems.Count - 1; j > -1; j--)
                {
                    delete.Add(listView1.SelectedItems[j].Index);

                    if (asyncToolStripMenuItem.Checked){
                    if (안전모드ToolStripMenuItem.Checked)
                    {
                        if (IsDebugging)
                        {
                            this.Text = string.Format("{0} ({1}, {2}) [{3}{4}... {5}: {6} / {7}: {8}]", ProgramName, LocalString.디버깅, LocalString.안전모드,
                                (j + 1).ToString(),LocalString.목록삭제중,LocalString.이름 ,listView1.SelectedItems[j].SubItems[2].Text,LocalString.경로 ,listView1.SelectedItems[j].SubItems[3].Text);
                            toolStripStatusLabel1.Text = string.Format("{0}{1}... ({2}, {3}) [{4}: {5} / {6}: {7}]", (j + 1).ToString(), LocalString.목록삭제중, LocalString.디버깅, LocalString.안전모드,
                                LocalString.이름, listView1.SelectedItems[j].SubItems[2].Text, LocalString.경로, listView1.SelectedItems[j].SubItems[3].Text);

                        }
                        else
                        {
                            this.Text = string.Format("{0} ({1}) [{2}{3}... {4}: {5} / {6}: {7}]", ProgramName, LocalString.안전모드,
                                (j + 1).ToString(), LocalString.목록삭제중, LocalString.이름, listView1.SelectedItems[j].SubItems[2].Text, LocalString.경로, listView1.SelectedItems[j].SubItems[3].Text);
                            toolStripStatusLabel1.Text = string.Format("{0}{1}... ({2}) [{3}: {4} / {5}: {6}]", (j + 1).ToString(), LocalString.목록삭제중, LocalString.안전모드,
                                LocalString.이름, listView1.SelectedItems[j].SubItems[2].Text, LocalString.경로, listView1.SelectedItems[j].SubItems[3].Text);
                        }
                    }
                    else
                    {
                        if (IsDebugging)
                        {
                            this.Text = string.Format("{0} ({1}) [{2}{3}... {4}: {5} / {6}: {7}]", ProgramName, LocalString.디버깅,
                                (j + 1).ToString(), LocalString.목록삭제중, LocalString.이름, listView1.SelectedItems[j].SubItems[2].Text, LocalString.경로, listView1.SelectedItems[j].SubItems[3].Text);
                            toolStripStatusLabel1.Text = string.Format("{0}{1}... ({2}) [{3}: {4} / {5}: {6}]", (j + 1).ToString(), LocalString.목록삭제중, LocalString.디버깅,
                                LocalString.이름, listView1.SelectedItems[j].SubItems[2].Text, LocalString.경로, listView1.SelectedItems[j].SubItems[3].Text);
                        }
                        else
                        {
                            this.Text = string.Format("{0} [{1}{2}... {3}: {4} / {5}: {6}]", ProgramName,(j + 1).ToString(), 
                                LocalString.목록삭제중, LocalString.이름, listView1.SelectedItems[j].SubItems[2].Text, LocalString.경로, listView1.SelectedItems[j].SubItems[3].Text);
                            toolStripStatusLabel1.Text = string.Format("{0}{1}... [{2}: {3} / {4}: {5}]", (j + 1).ToString(), LocalString.목록삭제중, LocalString.이름, 
                                listView1.SelectedItems[j].SubItems[2].Text, LocalString.경로, listView1.SelectedItems[j].SubItems[3].Text);
                        }
                    }}
                    int i = listView1.SelectedItems[j].Index;
                    if (음악믹싱모드ToolStripMenuItem.Checked)
                    { Delete(i); fm.RemoveAt(i); ps1.RemoveAt(i); Init_Sort(true); listView1.Items.RemoveAt(i); delete.Add(i); tmpStop = false; }
                    else
                    {
                        if (listView1.Items[i].Equals(CurrentItem))//fm.Count>0&&fm[0].index.ToString() == listView1.Items[i].Text)
                        {
                            dr = MessageBox.Show(LocalString.삭제_Message,LocalString.삭제_MessageTitle, MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                            if (dr == System.Windows.Forms.DialogResult.Yes)
                            {
                                if(ps!=null)ps.Hide();
                                toolStripProgressBar2.Value = 0;
                                toolStripProgressBar2.Visible = false;
                                timer1.Stop();
                                StopNextMusicThread = true;
                                trackBar1.Enabled = false;

                                StopSyncLyrics();//timer3.Stop();
                                Delete();
                                //btn재생.Text = LocalString.재생;
                                btn재생.BackgroundImage = Properties.Resources.PlayHS;
                                btn재생.Enabled = btn재생설정.Enabled = btn정지.Enabled = false;
                                listView1.Items.RemoveAt(i);
                                delete.Add(i);

                                //Init_Sort(false);
                                CurrentItem = null;
                            }
                        }
                        else
                        { try { listView1.Items.RemoveAt(i); delete.Add(i); } catch{}}
                        //Init_Sort(false);
                    }
                    statusStrip1.Refresh();
                    SearchPlayList(false);
                    try { listView1.Refresh(); }catch { }
                    //if(asyncToolStripMenuItem.Checked) Application.DoEvents();
                    /*
                    for (int i1 = 0; i1 < listView1.Items.Count; i1++)
                    {listView1.Items[i1].SubItems[1].Text = i1.ToString();}*/
                }
                UpdateTitle();
                Init_Sort(false);
                tmpStop = false;

                try { if (PlaylistChanged != null) PlaylistChanged(this, PlayListChange.Remove, delete.ToArray()); } catch { }
            }
            if (listView1.Items.Count == 0)
            {
                tmpNextMusic = null; MusicIndexList.Clear();
                if (thumbnailBarButtons.Count > 3 && listView1 != null) thumbnailBarButtons[0].IsDisabled = thumbnailBarButtons[3].IsDisabled = !(listView1.Items.Count > 0);
            }
        }
        void EditTitle(string Text)
        {
            
        }
        public void Init_StringBuilder()
        {
           
        }
        public AutoResetEvent areStep1 = new AutoResetEvent(false);

        DateTime dt = new DateTime();
        int tmp;
        bool tmpStop;
        bool Once;
        StringBuilder timer1_Tick_sb = new StringBuilder();
        string timer1_Tick_sb_String;
        private void timer1_Tick(object sender, EventArgs e)
        {
            StringBuilder sb = timer1_Tick_sb;
            //timer1.Interval = 50;
            timer2.Start();
            if (음악믹싱모드ToolStripMenuItem.Checked)
            {
                try
                {
                    for (int i = 0; i < listView1.Items.Count; i++)
                    {
                        //timer1.Interval = 100;
                        listView1.Items[i].SubItems[4].Text = string.Format("{0} / {1}",ShowTime(CurrentTick(i)), ShowTime(TotalTick(i)));
                        //dt.AddMilliseconds((double)Tick(i)).ToString("tt hh:mm:ss:ff");
                    }
                }
                catch { }
            }
            else 
            {
                if (Helper != null)
                {
                    {
                        if (Helper.Complete && (
                            Helper.Result == HS_Audio_Lib.RESULT.OK||
                            Helper.Result == HS_Audio_Lib.RESULT.ERR_NEEDS3D)) NextMusic();

                        else if (Helper.Result != HS_Audio_Lib.RESULT.OK&&Helper.CurrentPosition == 0&&Helper.PlayStatus==HSAudioHelper.PlayingStatus.Error)
                            if(CurrentItem.Index == 0)timer1.Stop();
                            else NextMusic();
                    }
                }
                if(this.WindowState == FormWindowState.Normal/*&&this.WindowShowStatus == HS_Audio.Control.WindowShowState.Show*/)
                    ShowCurrentTime(밀리초표시ToolStripMenuItem.Checked);
                //if (lrcStop) timer3.Stop();else timer3.Start();
                //StartSyncLyrics();
                //if (fm.Count != 0)
            }
            try 
            { 
                //try { if (timer_파일추가.Enabled)timer_파일추가.Dispose(); timer_파일추가 = new System.Windows.Forms.Timer(); }catch (Exception ex) { HS_CSharpUtility.LoggingUtility.Logging(ex); } 
                if (timer_파일추가 != null&&!timer_파일추가.Enabled) timer_파일추가.Start();
            }
            catch (Exception ex) { HS_CSharpUtility.LoggingUtility.Logging(ex, "HS_Audio::frmMain::timer1_Tick(object sender, EventArgs e) 에서 발생!"); }
        }

        bool StopNextMusicThread = true;
        Thread NextMusicThread;
        void NextMusicThread_Loop(object o)
        {
            while (true)
            {
                try
                {
                    if (!StopNextMusicThread)
                    {
                        if (Helper != null)
                        {
                            if (Helper.Complete && Helper.Result != HS_Audio_Lib.RESULT.ERR_UNINITIALIZED /*|| fm[0].GetCurrentTick(FMOD.TIMEUNIT.PCMBYTES) == fm[0].GetTotalTick(FMOD.TIMEUNIT.PCMBYTES)*/)//
                            { NextMusic(); Thread.Sleep(500); }
                            if (Helper.Result == HS_Audio_Lib.RESULT.ERR_UNINITIALIZED) {this.InvokeIfNeeded(() => timer1.Stop());StopNextMusicThread = true;}
                        }
                    }
                }
                catch (Exception ex)
                {
                    //try{ MainThread.Resume(); }catch{ } 
                    HS_CSharpUtility.LoggingUtility.Logging(ex, "HS_Audio::frmMain::NextMusicThread_Loop(object sender, EventArgs e) 에서 발생!");
                }
                Thread.Sleep(25);
            }
        }

        HS_CSharpUtility.HSRandom random = new HS_CSharpUtility.HSRandom(true);
        bool CompleteLock;

        private void 재생목록열기ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (LastListOpenDirectory != null && Directory.Exists(LastListOpenDirectory)) 
            { openFileDialog2.InitialDirectory = LastListOpenDirectory; }
            openFileDialog2.Title = 재생목록열기ToolStripMenuItem.Text + "...";
            if (openFileDialog2.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                LastListOpenDirectory = Path.GetDirectoryName(openFileDialog2.FileName);
                foreach(string a in openFileDialog2.FileNames)목록열기(null, a);
            }
        }

        private void openFileDialog2_FileOk(object sender, CancelEventArgs e)
        {
            
        }

        internal HS_Audio.Forms.frmEqualizer fe;
        //internal List<HS_Audio_Helper.Forms.frmEqulazer> fe1 = new List<HS_Audio_Helper.Forms.frmEqulazer>();
        internal HS_Audio.Forms.frmDSP frmdsp;
        internal List<HS_Audio.Forms.frmDSP> frmdsp1 = new List<HS_Audio.Forms.frmDSP>();
        internal Lyrics.frmLyricsDesktop lrcf = new Lyrics.frmLyricsDesktop();

        private IList<ThumbnailBarButton> thumbnailBarButtons;
        
        private void Form1_Load(object sender, EventArgs e)
        {
            ThreadInit();
            프리로드모드ToolStripMenuItem.Checked = PreLoading;
            //toolStripComboBox5.SelectedIndex = 1;
            /*
            this.contextMenuStrip2.Items.Clear();
            this.contextMenuStrip2.Items.AddRange(new ToolStripItem[] {
            this.파일FToolStripMenuItem,
            this.설정ToolStripMenuItem,
            this.보기VToolStripMenuItem,
            this.도움말HToolStripMenuItem,
            this.toolStripSeparator15,
            this.재생컨트롤ToolStripMenuItem});*/
            cbMimetype.SelectedItem = cbMimetype.Items[0];
            toolStripComboBox3.SelectedIndex = toolStripComboBox4.SelectedIndex = cb반복.SelectedIndex = 0;
            //timer2.Start();
            lrcf.Form1Instance = this;
            lrcf.Show();
            timer2.Start();
            timer_파일추가.Start();
            StartSyncLyrics();
            hotkey.CancelButtonPressed+=new frmHotKey.CancelButtonPressedEventHandler(hotkey_ApplyButtonPressed);
            hotkey.ApplyButtonPressed+=new frmHotKey.ApplyButtonPressedEventHandler(hotkey_ApplyButtonPressed);
            hotkey.LoadSetting();
            NextMusicThread_ = new ParameterizedThreadStart(NextMusicThread_Loop);
            toolStripProgressBar2.Control.LocationChanged+=statusStrip1_LocationChanged;
            다음ToolStripMenuItem.Image = InvertImage(Properties.Resources.Next_32);
            이전ToolStripMenuItem.Image = InvertImage(Properties.Resources.Previous_32);
            정지ToolStripMenuItem1.Image = InvertImage(Properties.Resources.Stop_29);

            listView1_SizeChanged(null, null);
            btnFnSwap_Click(null, null);
            
            /*
#if DEBUG
            NextMusicThread = new Thread(NextMusicThread_);
            NextMusicThread.Name = "음악 다음으로 넘기기 스레드";
            NextMusicThread.Start();
#else
#endif*/
            //KeyboardHook kbh = new KeyboardHook();kbh.RegisterHotKey(LIBRARY.ModifierKeys.

            if(HS_CSharpUtility.Utility.IsWindowsXP_Higher)운영체제테마적용ToolStripMenuItem.Checked = true;
            if(Helper!=null)fh_MusicPathChanged(Helper, Helper.index, Helper.Error);
            picTagPic.Size = new Size(pnlSwap2.Height, pnlSwap2.Height);

#if DEBUG
            음악믹싱모드ToolStripMenuItem.Enabled = true;
#endif 
        }

        ParameterizedThreadStart NextMusicThread_;

        bool BreakLoop = false;
        private void toolStripStatusLabel2_Click(object sender, EventArgs e)
        {
            BreakLoop = true;
            toolStripProgressBar1.Visible = toolStripStatusLabel2.Visible = toolStripStatusLabel3.Visible = false; toolStripProgressBar1.Maximum = 0;
            UpdateTitle();
            toolStripProgressBar1.Value = 0;
        }

        private void 음악믹싱모드ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DialogResult dr = MessageBox.Show("변경사항을 적용하려면 프로그램을 다시 시작하여야 합니다.\n\n" +
            "다시 시작하시겠습니까?", "프로그램 다시시작 알림", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
            if (dr == System.Windows.Forms.DialogResult.Yes)
            {
                StringBuilder sb = new StringBuilder("#EXTM3U");
                foreach (ListViewItem a in listView1.Items) { try { sb.Append("\r\n" + a.SubItems[3].Text); } catch { } }

                string path = Path.GetTempFileName();
                try { File.Delete(path); File.Create(path + ".m3u").Close(); path = path + ".m3u"; } catch { }
                try { System.IO.File.WriteAllText(path, sb.ToString()); } catch { }

                try
                {
                    if (!음악믹싱모드ToolStripMenuItem.Checked)
                    {
                        System.Diagnostics.Process.Start(Application.ExecutablePath.Replace("/", "\\"), string.Format("/Restart {0} \"{1}\" ", NewInstance ? "/NewInstance" : "", path));
                        notifyIcon1.Dispose();
                        System.Diagnostics.Process.GetCurrentProcess().Kill();
                    }
                    else
                    {
                        System.Diagnostics.Process.Start(Application.ExecutablePath.Replace("/", "\\"), string.Format("/Restart /Mixing {0} \"{1}\" ", NewInstance ? "/NewInstance" : "", path));
                        notifyIcon1.Dispose();
                        System.Diagnostics.Process.GetCurrentProcess().Kill();
                    }
                }
                catch (Exception ex) { }
            }
        }


        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (음악믹싱모드ToolStripMenuItem.Checked) { 정지ToolStripMenuItem_Click(null, null); }//Stop(indexing); }
            else { Stop(); /*btn재생.Text = LocalString.재생; */btn재생.BackgroundImage = Properties.Resources.PlayHS;}//Init_Sort(false); }
        }

        private void contextMenuStrip1_Opening(object sender, CancelEventArgs e)
        {
            try
            {
                if (음악믹싱모드ToolStripMenuItem.Checked && fm[0].SizeLength > 0)
                {
                    saveFileDialog2.FileName = System.IO.Path.GetFileNameWithoutExtension(fm[0].MusicPath);
                    saveFileDialog2.Title = string.Format(LocalString.SaveWaveMessage, toolStripTextBox1.Text);
                    파일로웨이브저장ToolStripMenuItem.Enabled = true;
                }
                else
                {
                    일시정지ToolStripMenuItem.Enabled =
                        정지ToolStripMenuItem.Enabled = 
                        삭제ToolStripMenuItem.Enabled = false;
                }
                if (listView1.SelectedItems.Count == 1)
                {
                    if (listView1.SelectedItems[0].Text == Helper.index.ToString())
                    {
                        일시정지ToolStripMenuItem.Enabled =
                        정지ToolStripMenuItem.Enabled =
                        삭제ToolStripMenuItem.Enabled = true;
                    }
                    else{삭제ToolStripMenuItem.Enabled = true; }
                    가사찾기ToolStripMenuItem.Enabled = true; 
                }
                else { 가사찾기ToolStripMenuItem.Enabled = false; 삭제ToolStripMenuItem.Enabled = true; }

                if (listView1.SelectedItems.Count > 0)
                {
                    복사ToolStripMenuItem.Enabled =
                    탐색기에서해당파일열기ToolStripMenuItem.Enabled = 
                    mD5해쉬값구하기ToolStripMenuItem.Enabled = true;
                }
                else
                {
                    삭제ToolStripMenuItem.Enabled=
                    복사ToolStripMenuItem.Enabled =
                    탐색기에서해당파일열기ToolStripMenuItem.Enabled =
                        mD5해쉬값구하기ToolStripMenuItem.Enabled = false;
                }
            }
            catch { }
        }

        private void 파일로웨이브저장ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            int indexing = this.indexing;
            string h = long.Parse(toolStripTextBox1.Text.Replace(",", "")).ToString("##,###");

            saveFileDialog2.Title = string.Format("{0} {1} {2}... ({3} {4}: {5})", LocalString.웨이브, LocalString.데이터, LocalString.저장, LocalString.웨이브, LocalString.크기, h);

            saveFileDialog2.FileName = HS_CSharpUtility.Utility.StringUtility.GetDirectoryPath(Helper/*fm[0]*/.MusicPath, true) +
               HS_CSharpUtility.Utility.StringUtility.GetFileName(Helper/*fm[0]*/.MusicPath, false) + string.Format("_{0} {1} ({2}： {3})", LocalString.웨이브, LocalString.데이터, LocalString.크기, h);

            saveFileDialog2.InitialDirectory = HS_CSharpUtility.Utility.StringUtility.GetDirectoryPath(fm[indexing].MusicPath, false);

            DialogResult dr = saveFileDialog2.ShowDialog();
            if (dr == System.Windows.Forms.DialogResult.OK)
            {
                ThreadPool.QueueUserWorkItem(new WaitCallback(파일로웨이브저장));
            }
        }
        void 파일로웨이브저장_기록(long Current, long Total, int Channel, float Percent)
        {
            toolStripProgressBar1.Value = (int)Current;
            this.Text = string.Format("{0} ({1})", LocalString.ProgramName, toolStripStatusLabel3.Text);
            toolStripStatusLabel3.Text = string.Format("{0}... ({1}%)[{2} {3} ({4} / {5})] ", LocalString.저장중, Channel == 0 ? LocalString.좌 : LocalString.우, LocalString.채널,
            Percent.ToString("0.00"), Current.ToString("##,###"), Total.ToString("##,###"));
        }
        void 파일로웨이브저장(object obj)
        {
            string Extend = System.IO.Path.GetExtension(saveFileDialog2.FileName);
            string Path = HS_CSharpUtility.Utility.StringUtility.GetDirectoryPath(saveFileDialog2.FileName, true) + System.IO.Path.GetFileNameWithoutExtension(saveFileDialog2.FileName) + "_좌" + Extend;
            System.IO.StreamWriter sw = new System.IO.StreamWriter(Path);
            if (!asyncToolStripMenuItem.Checked) { toolStripMenuItem1.Text = LocalString.저장중+"... "; }
            if (fm[indexing].system != null)
            {
                try
                {
                    toolStripStatusLabel2.Visible = toolStripStatusLabel3.Visible = true;
                    long a1=long.Parse(toolStripTextBox1.Text.Replace(",", ""));
                    float[] f = new float[a1];
                    HS_Audio_Lib.RESULT res = 음악믹싱모드ToolStripMenuItem.Checked ? fm[indexing].system.getWaveData(f, f.Length - 1,0) : Helper.system.getWaveData(f, f.Length - 1, 0);
                    if (res == HS_Audio_Lib.RESULT.ERR_INVALID_PARAM) { MessageBox.Show(" 값보다 작은값을 입력해 주세요"); }
                    //string a = CSharpUtility.Utility.StringUtility.AddString("0", f.LongLength);
                    toolStripProgressBar1.Visible = true;

                    // List<string> b = new List<string>();
                    //System.IO.File.Create(Path);
                    //bool fd =System.IO.File.Exists(Path);
                    toolStripProgressBar1.Maximum = f.Length;
                    //toolStripStatusLabel1.Invalidate();toolStripStatusLabel2.Invalidate();
                    //toolStripStatusLabel1.Text = "저장중... (좌 채널)";
                    for (long i = 0; i < f.LongLength; i++)
                    {
                        if (!BreakLoop)
                        {
                            //statusStrip1.Refresh();
                            toolStripStatusLabel3.Text = string.Format("{0}... ({1}%)[좌 채널 ({2} / {3})] ",LocalString.저장중,
                                (((float)i / (float)f.Length) * 100).ToString("0.00"),
                                i.ToString("##,###"), f.LongLength.ToString("##,###"));

                            this.Text = string.Format("{0} ({1})", ProgramName,toolStripStatusLabel3.Text);
                            try { toolStripProgressBar1.Value = (int)i; }
                            catch { }
                            sw.Write(f[i].ToString() + "\r\n"/*string.Format("[{0}]{0}\r\n", i.ToString(a), f[i].ToString())*/);
                            //if (syncToolStripMenuItem.Checked) Application.DoEvents();
                        }
                        else { BreakLoop = false; break; }
                    }
                    sw.Close();
                    //System.IO.File.WriteAllLines(saveFileDialog2.FileName + "_좌", b.ToArray());
                    //b.Clear();

                    // toolStripStatusLabel1.Text = "저장중... (우 채널)";
                    res = 음악믹싱모드ToolStripMenuItem.Checked ? fm[indexing].system.getWaveData(f, f.Length - 1, 1) : Helper.system.getWaveData(f, f.Length - 1, 1);
                    //a = CSharpUtility.Utility.StringUtility.AddString("0", f.LongLength);
                    Path = HS_CSharpUtility.Utility.StringUtility.GetDirectoryPath(saveFileDialog2.FileName, true) + System.IO.Path.GetFileNameWithoutExtension(saveFileDialog2.FileName) + "_우" + Extend;
                    sw = new System.IO.StreamWriter(Path);
                    toolStripProgressBar1.Maximum = f.Length;
                    for (long i = 0; i < f.LongLength; i++)
                    {
                        if (!BreakLoop)
                        {
                            //statusStrip1.Refresh();
                            toolStripStatusLabel3.Text = string.Format("{0}... ({1}%)[우 채널({2} / {3})]",LocalString.저장중,
                                (((float)i / (float)f.Length) * 100).ToString("0.00"),
                                i.ToString("##,###"), f.LongLength.ToString("##,###"));

                            this.Text = string.Format("{0} ({1})", ProgramName, toolStripStatusLabel3.Text);
                            try { toolStripProgressBar1.Value = (int)i; }
                            catch { }
                            sw.Write(f[i].ToString() + "\r\n"/*string.Format("[{0}]{1}\r\n", i.ToString(a), f[i].ToString())*/);
                            //if (syncToolStripMenuItem.Checked) Application.DoEvents();
                        }
                        else { break; }
                    }
                }
                finally {
                toolStripStatusLabel3.Text = "";
                sw.Close();
                toolStripStatusLabel2.Visible = toolStripStatusLabel3.Visible = false;
                BreakLoop = false;
                this.Text = ProgramName + " " + LocalString.디버깅;
                toolStripProgressBar1.Visible = false;
                toolStripStatusLabel1.Text = LocalString.준비;}
            }
        }

        private void 재생목록다른이름으로저장ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            saveFileDialog1.InitialDirectory = LastListSaveDirectory;
            saveFileDialog1.ShowDialog();
        }

        private void saveFileDialog2_FileOk(object sender, CancelEventArgs e)
        {
            
        }

        private void saveFileDialog1_FileOk(object sender, CancelEventArgs e)
        {
            LastListSaveDirectory = Path.GetDirectoryName(saveFileDialog1.FileName);
            StringBuilder sb = new StringBuilder("#EXTM3U");
            foreach(ListViewItem a in listView1.Items){ if(a!=null)sb.Append("\r\n"+a.SubItems[3].Text);}
            if (saveFileDialog1.FilterIndex!=1) System.IO.File.WriteAllText(saveFileDialog1.FileName, sb.ToString());
            else System.IO.File.WriteAllText(saveFileDialog1.FileName, sb.ToString(), Encoding.UTF8);
        }

        private void 파일FToolStripMenuItem_DropDownOpening(object sender, EventArgs e)
        {
            try
            {
                if (Helper!=null&&Helper.SizeLength > 0) 
                {
                    //saveFileDialog2.Title = string.Format("웨이브 저장... (웨이브 크기: {0})", toolStripTextBox1.Text);
                    파일로웨이브저장ToolStripMenuItem.Enabled = true;
                } 
            } catch { }
            if (listView1.Items.Count > 0) { 재생목록다른이름으로저장ToolStripMenuItem.Enabled =현재상태저장ToolStripMenuItem.Enabled= true; }
            else { 재생목록다른이름으로저장ToolStripMenuItem.Enabled =현재상태저장ToolStripMenuItem.Enabled= false; }
        }

        private void toolStripTextBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!(char.IsDigit(e.KeyChar) || // 숫자만 입력
                e.KeyChar == Convert.ToChar(Keys.Back) || // backspace 키
                e.KeyChar == Convert.ToChar(".")) ||// . 키
                e.KeyChar == Convert.ToChar(Keys.Enter)) // Enter 키
            {
                if (e.KeyChar == Convert.ToChar(Keys.Enter)) { return; }
                e.Handled = true; // 이조건문으로 들어오는것은 키가 안먹게 하는부분
            }
            else
            {
                Input = true; try { long a = long.Parse(toolStripTextBox1.Text.Replace(",", "")); toolStripTextBox1.Text = (a > 16385 ? 16385 : a).ToString("##,###"); }
                catch { toolStripTextBox1.Text = DateSize.ToString("##,###"); /*원본사이즈사용ToolStripMenuItem_Click(null, null);*/ }
            }
        }

        bool Input;
        private void 파일로웨이브저장ToolStripMenuItem_DropDownOpening(object sender, EventArgs e)
        {
            if(!Input)toolStripTextBox1.Text = DateSize.ToString("##,###"); 
        }

        int indexing;
        private void 원본사이즈사용ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show(string.Format("{0}: {1} Byte\n\n{2}: {3} Byte",LocalString.원본+LocalString.데이터+LocalString.크기 ,
                fm[indexing].SizeLength.ToString("##,###"),LocalString.원본+LocalString.웨이브+LocalString.크기,
                fm[indexing].WaveLength.ToString("##,###")), LocalString.파일이름+": " + System.IO.Path.GetFileName(fm[indexing].MusicPath));
        }

        private void 정보ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AboutBox1 ab = new AboutBox1(); ab.Show();
        }

        private void Form1_SizeChanged(object sender, EventArgs e)
        {
            //timer1_Tick(null, null);
        }

        int CurrentIndex
        {
            get
            {
                for (int i = 0; i < listView1.Items.Count; i++){
                if (listView1.Items[i].SubItems[1].Text != "" || listView1.Items[i].SubItems[1].Text != null)return i; }
                return -1;
            }
        }

        ListViewItem _CurrentItem;
        public ListViewItem CurrentItem { get { return _CurrentItem; } set { if(value!=null)value.Font = new Font(value.Font, value.Font.Style|FontStyle.Bold); _CurrentItem = value; } }
        void Init_Sort(bool MixingMode)
        {
            if (MixingMode)
            {
                for (int i = 0; i < listView1.Items.Count; i++)
                { try { listView1.Items[i].Text = i.ToString(); fm[i].index = i; }catch{} }
            }
            else
            {
                if (Helper!=null)
                {
                    try
                    {
                        /*
                        int index = 0;
                        for (int i = 0; i < listView1.Items.Count; i++)
                        {
                            if (listView1.Items[i].SubItems[1].Text != "" || listView1.Items[i].SubItems[1].Text != null) { fm[0].index = index = i; }
                            listView1.Items[i].SubItems[1].Text = string.Format("{0} ({1})", LocalString.현재, Status_NoMixing.ToString()); break;

                            //if (fm[0].index.ToString() == listView1.Items[i].Text)
                            //{ listView1.Items[i].SubItems[1].Text = string.Format("{0} ({1})", LocalString.현재, Status_NoMixing.ToString()); }
                            //else { listView1.Items[i].SubItems[1].Text = ""; }

                        }*/
                        for (int i = 0; i < listView1.Items.Count; i++)
                        {
                            listView1.Items[i].SubItems[0].Text = i.ToString();
                            if (!listView1.Items[i].Equals(CurrentItem))
                            { listView1.Items[i].SubItems[1].Text = ""; listView1.Items[i].Font = CustomFont; }
                        }
                    }
                    catch { }
                    finally{ CurrentItem = _CurrentItem;}
                }
            }
        }
        void Init_Sort(bool MixingMode, int CurrentIndex)
        {
            if (MixingMode)
            {
                for (int i = 0; i < listView1.Items.Count; i++)
                { try { listView1.Items[i].Text = i.ToString(); fm[i].index = i; } catch { } }
            }
            else
            {
                if (Helper!=null)
                {
                    for (int i = 0; i < listView1.Items.Count; i++)
                    {
                        listView1.Items[i].SubItems[1].Text = "";
                        listView1.Items[i].SubItems[0].Text = i.ToString();
                    }
                    listView1.Items[CurrentIndex].SubItems[1].Text = string.Format("{0} ({1})", LocalString.현재, Status_NoMixing.ToString());
                }
            }
        }
        private void timer2_Tick(object sender, EventArgs e)
        {
            if (CurrentItem != null) CurrentItem.SubItems[1].Text = string.Format("{0} ({1})", LocalString.현재, Status_NoMixing.ToString());
        }

        private void fm_PlayingStatusChanged(HS_Audio.HSAudioHelper.PlayingStatus Status, int index)
        {
            //Init_Sort(true);
            //timer2_Tick(null, null);
            listView1.Items[index].SubItems[1].Text = Status.ToString();
        }

        HS_Audio.HSAudioHelper.PlayingStatus Status_NoMixing = HS_Audio.HSAudioHelper.PlayingStatus.Ready;
        bool fh_PlayingStatusChangedSizeChange =  true;
        private void fh_PlayingStatusChanged(HS_Audio.HSAudioHelper.PlayingStatus Status, int index)
        {
            Status_NoMixing = Status;

            if (Status == HSAudioHelper.PlayingStatus.Play){PlayText = true; btn재생.BackgroundImage = Properties.Resources.PauseHS;//btn재생.Text = LocalString.일시정지;
                toolStripProgressBar2.Value = 0; toolStripProgressBar2.Visible = true;
                Image img = 재생ToolStripMenuItem1.Image;
                재생ToolStripMenuItem1.Image = InvertImage(HS_Audio.Properties.Resources.Pause_29);
                img.Dispose();
                try { thumbnailBarButtons[1].Icon = HS_Audio.Properties.Resources.Pause_29; }catch{}
                try { thumbnailBarButtons[1].Tooltip = LocalString.일시정지; }catch{} }
            else if (Status == HSAudioHelper.PlayingStatus.Pause || Status == HSAudioHelper.PlayingStatus.Stop||Status == HSAudioHelper.PlayingStatus.Ready)
            { PlayText = false; btn재생.BackgroundImage = Properties.Resources.PlayHS;//btn재생.Text = LocalString.재생;
               toolStripProgressBar2.Value = 0; toolStripProgressBar2.Visible = true;
               Image img = 재생ToolStripMenuItem1.Image;
               재생ToolStripMenuItem1.Image = InvertImage(HS_Audio.Properties.Resources.Play_29);
               img.Dispose();
               try { thumbnailBarButtons[1].Icon = HS_Audio.Properties.Resources.Play_29; } catch { }
               try { thumbnailBarButtons[1].Tooltip = LocalString.재생;} catch { }
            }
            else
            {
                PlayText = false; btn재생.BackgroundImage = Properties.Resources.PlayHS;//btn재생.Text = LocalString.재생;
                Image img = 재생ToolStripMenuItem1.Image;
                재생ToolStripMenuItem1.Image = InvertImage(HS_Audio.Properties.Resources.Play_29);
                img.Dispose();
                try { thumbnailBarButtons[1].Icon = HS_Audio.Properties.Resources.Play_29; }catch { }
                try { thumbnailBarButtons[1].Tooltip = LocalString.재생; }catch { }
                if (ps != null) ps.Hide();
                toolStripProgressBar2.Value = 0; toolStripProgressBar2.Visible = false;
                timer1.Stop();
                StopNextMusicThread = true;
                StopSyncLyrics();//timer3.Stop();

                btn재생.Enabled = btn재생설정.Enabled = btn정지.Enabled = false;
            }
            toolStripProgressBar2.Maximum = (int)TotalTick().TotalMilliseconds;
            /*
            for (int i = 0; i < listView1.Items.Count; i++)
            {
                if (fm[0].index.ToString() == listView1.Items[i].Text)//index.ToString() == (int.Parse(listView1.Items[i].Text)-1).ToString())
                { listView1.Items[i].SubItems[1].Text = "현재 (" + Status_NoMixing.ToString() + ")"; }
                else { listView1.Items[i].SubItems[1].Text = ""; }
            }*/
            try { if(CurrentItem!=null) CurrentItem.SubItems[1].Text = string.Format("{0} ({1})", LocalString.현재, Status_NoMixing.ToString()); }catch{}
            try { thumbnailBarButtons[1].IsDisabled = !btn재생.Enabled; thumbnailBarButtons[2].IsDisabled = !btn정지.Enabled; }catch{}
            try{thumbnailBarButtons[0].IsDisabled=thumbnailBarButtons[3].IsDisabled=!(listView1.Items.Count>0);}catch{}
            
            try
            {
                if (statusStrip1_SizeChangedFirst)statusStrip1_SizeChangedFirst = false;
                else if (fh_PlayingStatusChangedSizeChange)
                {
                    //UpdateTitle();
                    fh_PlayingStatusChangedSizeChange = false;
                    statusStrip1_SizeChanged(null, null);
                    //if (Status == HSAudioHelper.PlayingStatus.Pause)
                    toolStripProgressBar2.Visible = true;
                }
            }catch{}
        }
        public string PlayStatustoString(HSAudioHelper.PlayingStatus Status)
        {
            switch (Status)
            {
                case HSAudioHelper.PlayingStatus.Ready:
                    return LocalString.준비;
                case HSAudioHelper.PlayingStatus.Play:
                    return LocalString.재생;
                case HSAudioHelper.PlayingStatus.Stop:
                    return LocalString.정지;
                case HSAudioHelper.PlayingStatus.Pause:
                    return LocalString.일시정지;
                default:
                    return LocalString.초기화;
            }
        }

        private void 파일복구및재설치ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DialogResult dr = MessageBox.Show(LocalString.파일복구및재설치ToolStripMenuItem_Message, 
                LocalString.파일복구및재설치ToolStripMenuItem_MessageTitle, MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation);
            if (dr == System.Windows.Forms.DialogResult.Yes) 
            {
                System.Diagnostics.Process.Start(Application.ExecutablePath.Replace("/", "\\"), "/RepairSoundSystem");
                System.Diagnostics.Process.GetCurrentProcess().Kill();
            }
        }


        private void toolStripTextBox2_Click(object sender, EventArgs e)
        {
            
        }

        private void toolStripMenuItem2_Click(object sender, EventArgs e)
        {
            원본사이즈사용ToolStripMenuItem_Click(null, null);
        }

        private void toolStripMenuItem1_Click(object sender, EventArgs e)
        {
            파일로웨이브저장ToolStripMenuItem_Click(null, null);
        }

        private void toolStripTextBox2_TextChanged(object sender, EventArgs e)
        {
            toolStripTextBox1.Text = toolStripTextBox2.Text;
        }

        private void toolStripTextBox2_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!(char.IsDigit(e.KeyChar) || // 숫자만 입력
                e.KeyChar == Convert.ToChar(Keys.Back) || // backspace 키
                e.KeyChar == Convert.ToChar(".")) ||// . 키
                e.KeyChar == Convert.ToChar(Keys.Enter)) // Enter 키
            {
                if (e.KeyChar == Convert.ToChar(Keys.Enter)) { return; }
                e.Handled = true; // 이조건문으로 들어오는것은 키가 안먹게 하는부분
                toolStripTextBox2.Text = DateSize.ToString("##,###");
            }
            else
            {
                Input = true; try { toolStripTextBox2.Text = long.Parse(toolStripTextBox2.Text.Replace(",", "")).ToString("##,###"); }
                catch { toolStripTextBox2.Text = DateSize.ToString("##,###") ; }
            }
        }

        private void listView1_DragEnter(object sender, DragEventArgs e)
        {
            /*if (e.Data.GetDataPresent(typeof(System.String)))
            {
                e.Effect = DragDropEffects.Copy;
                return;
            }*/
            if (/*e.Data.GetDataPresent(DataFormats.FileDrop)*/true)
            {
                string[] a = (string[])e.Data.GetData(DataFormats.FileDrop); 
                /*//파일 확인하기
                string b = "";
                foreach (string c in a) { b = b + c + "\n\n"; }
                MessageBox.Show(b, e.Data.GetType().ToString());*/
                if (a.Length > 0)
                {
                    e.Effect = DragDropEffects.All;
                    /*
                    if (System.IO.Path.GetExtension(a[0]).ToLower() == ".m3u")
                    {
                        if (a.Length == 1) { e.Effect = DragDropEffects.All; }
                        else { e.Effect = DragDropEffects.None; }
                    }*/
                    if (!안전모드ToolStripMenuItem.Checked)
                    {
                        try { if (Directory.Exists(a[0])) e.Effect = DragDropEffects.All; else throw new Exception(); }
                        catch
                        {
                            if (System.IO.Path.GetExtension(a[0]).ToLower() == ".mp3" ||
                                System.IO.Path.GetExtension(a[0]).ToLower() == ".ogg" ||
                                System.IO.Path.GetExtension(a[0]).ToLower() == ".wav" ||
                                System.IO.Path.GetExtension(a[0]).ToLower() == ".wma" ||
                                System.IO.Path.GetExtension(a[0]).ToLower() == ".flac" ||
                                System.IO.Path.GetExtension(a[0]).ToLower() == ".mid" ||
                                System.IO.Path.GetExtension(a[0]).ToLower() == ".aif" || System.IO.Path.GetExtension(a[0]).ToLower() == ".aiff" ||
                                System.IO.Path.GetExtension(a[0]).ToLower() == ".fsb" ||
                                System.IO.Path.GetExtension(a[0]).ToLower() == ".asf" ||
                                System.IO.Path.GetExtension(a[0]).ToLower() == ".asx" ||
                                System.IO.Path.GetExtension(a[0]).ToLower() == ".dls" ||
                                System.IO.Path.GetExtension(a[0]).ToLower() == ".wax" ||
                                System.IO.Path.GetExtension(a[0]).ToLower() == ".m3u")
                            {
                                e.Effect = DragDropEffects.All;
                            }
                            else { e.Effect = DragDropEffects.None; }
                        }
                    }
                    else { e.Effect = DragDropEffects.All; }
                }
            }
            else                                    // 아니면 이펙트를 모두 꺼서 금지표시가 나타나게 됨
                e.Effect = DragDropEffects.None;
        }

        private void 높음ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            높음ToolStripMenuItem.Checked = 높음ToolStripMenuItem1.Checked = true;
            높은우선순위ToolStripMenuItem.Checked = 높은우선순위ToolStripMenuItem1.Checked = false;
            보통권장ToolStripMenuItem.Checked = 보통권장ToolStripMenuItem1.Checked = false;
            낮은우선순위ToolStripMenuItem.Checked = 낮은우선순위ToolStripMenuItem1.Checked = false;
            System.Diagnostics.Process.GetCurrentProcess().PriorityClass = System.Diagnostics.ProcessPriorityClass.High;
        }

        private void 높은우선순위ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.GetCurrentProcess().PriorityClass = System.Diagnostics.ProcessPriorityClass.AboveNormal;
            높음ToolStripMenuItem.Checked = 높음ToolStripMenuItem1.Checked = false;
            높은우선순위ToolStripMenuItem.Checked = 높은우선순위ToolStripMenuItem1.Checked = true;
            보통권장ToolStripMenuItem.Checked = 보통권장ToolStripMenuItem1.Checked = false;
            낮은우선순위ToolStripMenuItem.Checked = 낮은우선순위ToolStripMenuItem1.Checked = false;
        }

        private void 보통권장ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.GetCurrentProcess().PriorityClass = System.Diagnostics.ProcessPriorityClass.Normal;
            높음ToolStripMenuItem.Checked = 높음ToolStripMenuItem1.Checked = false;
            높은우선순위ToolStripMenuItem.Checked = 높은우선순위ToolStripMenuItem1.Checked = false;
            보통권장ToolStripMenuItem.Checked = 보통권장ToolStripMenuItem1.Checked = true;
            낮은우선순위ToolStripMenuItem.Checked = 낮은우선순위ToolStripMenuItem1.Checked = false;
        }

        private void 낮은우선순위ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.GetCurrentProcess().PriorityClass = System.Diagnostics.ProcessPriorityClass.BelowNormal;
            높음ToolStripMenuItem.Checked = 높음ToolStripMenuItem1.Checked = false;
            높은우선순위ToolStripMenuItem.Checked = 높은우선순위ToolStripMenuItem1.Checked = false;
            보통권장ToolStripMenuItem.Checked = 보통권장ToolStripMenuItem1.Checked = false;
            낮은우선순위ToolStripMenuItem.Checked = 낮은우선순위ToolStripMenuItem1.Checked = true;
        }

        private void button5_Click(object sender, EventArgs e)
        {
            일시정지ToolStripMenuItem_Click(null, null);
        }

        System.Diagnostics.Process Process_LOL;
        private void 설정ToolStripMenuItem_DropDownOpening(object sender, EventArgs e)
        {
            foreach (System.Diagnostics.Process p in System.Diagnostics.Process.GetProcesses())
            {
                if (p.ProcessName.ToLower() == "league of legends")
                {
                    string a= p.MainModule.FileName.ToLower();
                    if (a.IndexOf("solutions\\lol_game_client_sln\\releases") != -1&&
                        a.IndexOf("deploy\\league of legends.exe")!=-1)
                    {
                        Process_LOL = p;
                        LOL우선순위ToolStripMenuItem.Enabled =
                        LOL높은우선순위ToolStripMenuItem.Enabled =
                        LOL보통권장ToolStripMenuItem.Enabled = true;
                        if (p.PriorityClass == System.Diagnostics.ProcessPriorityClass.AboveNormal)
                        {
                            LOL높은우선순위ToolStripMenuItem.Enabled =true;
                            LOL보통권장ToolStripMenuItem.Enabled = false;
                        }
                        else if (p.PriorityClass == System.Diagnostics.ProcessPriorityClass.Normal)
                        {
                            LOL높은우선순위ToolStripMenuItem.Enabled = true;
                            LOL보통권장ToolStripMenuItem.Enabled = false;
                        }
                        break;
                    }
                    else
                    {
                        LOL우선순위ToolStripMenuItem.Enabled =
                        LOL높은우선순위ToolStripMenuItem.Enabled =
                        LOL보통권장ToolStripMenuItem.Enabled = false;
                    }
                }
            }
            if (!IsRunAsAdmin()) NativeMethods.SetUacShield(기본프로그램으로설정ToolStripMenuItem);
        }
        public static class NativeMethods
        {
            [DllImport("Shell32.dll", SetLastError = false)]
            public static extern Int32 SHGetStockIconInfo(SHSTOCKICONID siid, SHGSI uFlags, ref SHSTOCKICONINFO psii);

            public enum SHSTOCKICONID : uint
            {
                SIID_SHIELD = 77
            }

            [Flags]
            public enum SHGSI : uint
            {
                SHGSI_ICON = 0x000000100,
                SHGSI_SMALLICON = 0x000000001
            }

            [StructLayoutAttribute(LayoutKind.Sequential, CharSet = CharSet.Unicode)]
            public struct SHSTOCKICONINFO
            {
                public UInt32 cbSize;
                public IntPtr hIcon;
                public Int32 iSysIconIndex;
                public Int32 iIcon;

                [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 260)]
                public string szPath;
            }
            public static void SetUacShield(ToolStripMenuItem menuItem)
            {
                try
                {
                    //if (Environment.OSVersion.Version.Major > 5)
                    {
                        NativeMethods.SHSTOCKICONINFO iconResult = new NativeMethods.SHSTOCKICONINFO();
                        iconResult.cbSize = (uint)System.Runtime.InteropServices.Marshal.SizeOf(iconResult);

                        NativeMethods.SHGetStockIconInfo(
                            NativeMethods.SHSTOCKICONID.SIID_SHIELD,
                            NativeMethods.SHGSI.SHGSI_ICON | NativeMethods.SHGSI.SHGSI_SMALLICON,
                            ref iconResult);

                        menuItem.DisplayStyle = ToolStripItemDisplayStyle.ImageAndText;
                        menuItem.Image = Bitmap.FromHicon(iconResult.hIcon);
                    }
                }
                catch{}
            }
        }


        private void 높은우선순위ToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Process_LOL.PriorityClass = System.Diagnostics.ProcessPriorityClass.AboveNormal;
            보통권장ToolStripMenuItem.Checked = false;
            LOL높은우선순위ToolStripMenuItem.Checked = true;
        }

        private void 보통권장ToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Process_LOL.PriorityClass = System.Diagnostics.ProcessPriorityClass.Normal;
            보통권장ToolStripMenuItem.Checked = true;
            LOL높은우선순위ToolStripMenuItem.Checked = false;
        }

        private void listView1_DragDrop(object sender, DragEventArgs e)
        {
            try { listView1.Items[listView1_MouseMove_List_Last].BackColor = listView1.BackColor; }catch (Exception ex) { HSConsole.WriteLine(ex.ToString()); }

            if (e.Data.GetDataPresent(DataFormats.FileDrop))
            {
                string[] a = (string[])e.Data.GetData(DataFormats.FileDrop);
                //if (!음악믹싱모드ToolStripMenuItem.Checked)
                e.Effect = DragDropEffects.All;
                StringBuilder ErrFile = new StringBuilder();
                Point pt = e.KeyState == (byte)KeyState_DragEvent_DragDrop.Ctrl_Left ? new Point(-1, -1) : this.listView1.PointToClient(new Point(e.X, e.Y));

                List<int> open = new List<int>();
                for (int i = 0; i < a.Length; i++)
                {
                    if (Directory.Exists(a[i])) 
                    { 
                        //if (e.KeyState == (byte)KeyState_DragEvent.Ctrl_Left)폴더열기(null, a[i], 하위폴더검색ToolStripMenuItem.Checked); 
                        //else 폴더열기(pt, a[i], 하위폴더검색ToolStripMenuItem.Checked);
                        폴더열기(pt, a[i], 하위폴더검색ToolStripMenuItem.Checked, asyncToolStripMenuItem.Checked);
                    }
                    else if (System.IO.Path.GetExtension(a[i]).ToLower() == ".m3u") { 목록열기(pt, a[i]); }
                    else
                    {
                        if (!음악믹싱모드ToolStripMenuItem.Checked)
                        {/*
                                if (System.IO.Path.GetExtension(a[i]).ToLower() == ".mp3" ||
                                    System.IO.Path.GetExtension(a[i]).ToLower() == ".ogg" ||
                                    System.IO.Path.GetExtension(a[i]).ToLower() == ".wav" ||
                                    System.IO.Path.GetExtension(a[i]).ToLower() == ".wma")*/

                            { /*if (e.KeyState == (byte)KeyState_DragEvent.Ctrl_Left)파일열기(null, a); else*/ if (IsValidMusicFile(a[i]))파일열기(pt, new string[] { a[i] });}
                            //else { ErrFile.AppendLine(a[i]); }
                        }
                        #region
                        else
                        {
                            //StringBuilder ErrFile = new StringBuilder();
                            try
                            {
                                HS_Audio.HSAudioHelper fh = new HS_Audio.HSAudioHelper(a[i], getSettingDeviceFormat()); fh.PreLoading = false;
                                fh.PlayingStatusChanged += new HS_Audio.HSAudioHelper.PlayingStatusChangedEventHandler(fh_PlayingStatusChanged);
                                fh.MusicChanging += new HSAudioHelper.MusicChangeEventHandler(fh_MusicPathChanged);
                                if (fh.HelperEx.Result != HS_Audio_Lib.RESULT.ERR_FILE_BAD ||
                                    fh.HelperEx.Result != HS_Audio_Lib.RESULT.ERR_FORMAT)
                                {
                                    //if (MixingMode)
                                    {
                                        //timer1.Start();
                                        fm.Add(fh);
                                        fh.Loop = true;
                                        timer1.Start();
                                        StopNextMusicThread = false;

                                        fh.index = fm.Count - 1;
                                        ListViewItem li = new ListViewItem(fh.index.ToString()) { BackColor = Color.Transparent, ForeColor = listView1.ForeColor };
                                        li.SubItems.Add(fh.PlayStatus.ToString());
                                        li.SubItems.Add(System.IO.Path.GetFileName(fh.MusicPath));
                                        li.SubItems.Add(fh.MusicPath);
                                        li.SubItems.Add("");

                                        li.Font = CustomFont;
                                        listView1.Items.Add(li);
                                        open.Add(listView1.Items.Count - 1);
                                        ps1.Add(new PlayerSetting(fh, System.IO.Path.GetFileName(fh.MusicPath)));
                                    }
                                }
                                else { throw new Exception(); }
                            }
                            catch { ErrFile.AppendLine("    " + a[i]); }
                        }
                        #endregion
                    }
                }

                try { if (PlaylistChanged != null) PlaylistChanged(this, PlayListChange.Add, open.ToArray()); } catch { }

                if (ErrFile.Length > 0)
                {
                    MessageBox.Show("일부항목은 올바르지 않거나 손상된 음악 파일 이므로 추가하지 못했습니다.\n\n추가 못한 경로:\n" + ErrFile.ToString(),
                        "일부 항목 추가실패!");
                }

                // else { e.Effect = DragDropEffects.None; }

            }
        }

        private void 안전모드ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (안전모드ToolStripMenuItem.Checked)
            {
                안전모드ToolStripMenuItem.Checked = false;
                openFileDialog1.Filter =
                    "지원하는 모든 음악 파일|*.mp3; *.ogg; *.wav; *.wma; *.flac; *.aif; *.aiff; *.mid|" +
                    "MP3 Audio 파일 (*.mp3)|*.mp3|" +
                    "Windows Media Audio 파일 (*.wma)|*.wma|" +
                    "Wave (Microsoft) 파일 (*.wav)|*.wav|" +
                    "OggVorbis 파일 (*.ogg)|*.ogg|" +
                    "Free Lossless Audio Codec 파일 (*.flac)|*.flac|" +
                    "Audio Interchange File Format 파일 (*.aif, *.aiff)|*.aif; *.aiff|" +
                    "Standard MIDI 파일 (*.mid)|*.mid|" +
                    "지원하는 모든 특수 파일|*.fsb; *.wax; *.asf; *.asx; *.dls|" +
                    "FMOD's Sample Bank 파일 (*.fsb)|*.fsb|" +
                    "Windows Media Audio Redirector 파일 (*.wax)|*.wax|" +
                    "네트워크 스트리밍 파일 (*.asf, *.asx, *.dls)|*.asf; *.asx; *.dls";
            }
            else
            {
                DialogResult dr = MessageBox.Show("안전모드를 사용하면 모든파일을 다 선택할수 있지만\n\n여는데 시간이 많이 걸립니다. 안전모드를 사용하시겠습니까?", 
                    "안전모드 사용", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                if (dr == System.Windows.Forms.DialogResult.Yes)
                {
                    안전모드ToolStripMenuItem.Checked = true;
                    openFileDialog1.Filter =
                    "지원하는 모든 음악 파일|*.mp3; *.ogg; *.wav; *.wma; *.flac; *.aif; *.aiff; *.mid|" +
                    "MP3 Audio 파일 (*.mp3)|*.mp3|" +
                    "Windows Media Audio 파일 (*.wma)|*.wma|" +
                    "Wave (Microsoft) 파일 (*.wav)|*.wav|" +
                    "OggVorbis 파일 (*.ogg)|*.ogg|" +
                    "Free Lossless Audio Codec 파일 (*.flac)|*.flac|" +
                    "Audio Interchange File Format 파일 (*.aif, *.aiff)|*.aif; *.aiff|" +
                    "Standard MIDI 파일 (*.mid)|*.mid|" +
                    "지원하는 모든 특수 파일|*.fsb; *.wax; *.asf; *.asx; *.dls|" +
                    "FMOD's Sample Bank 파일 (*.fsb)|*.fsb|" +
                    "Windows Media Audio Redirector 파일 (*.wax)|*.wax|" +
                    "네트워크 스트리밍 파일 (*.asf, *.asx, *.dls)|*.asf; *.asx; *.dls|"+
                    "모든 파일 (*.*)|*.*";
                }
            }
            UpdateTitle();
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            CloseForm = false;
            if (!CloseForm)
            {
                e.Cancel = true;
                ExitProgram();
            }
            else e.Cancel = false;
        }
        private void ExitProgram()
        {
            DialogResult dr = MessageBox.Show(LocalString.닫기_Message, LocalString.닫기_MessageTitle, MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
            if (dr == System.Windows.Forms.DialogResult.Yes) { /*fm[0].Dispose();*/ notifyIcon1.Dispose();
            if (!NoDelete) { try { File.Delete(tmpProfileSetting); } catch { } try { File.Delete(tmpPlaylist); } catch { } } /*Dispose(true);*/Process.GetCurrentProcess().Kill();}
        }

        private void 끝내기ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void 디버깅ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DialogResult dr = MessageBox.Show(LocalString.디버깅ToolStripMenuItem_Message, LocalString.디버깅ToolStripMenuItem_MessageTitle,
                MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
            if (dr == System.Windows.Forms.DialogResult.Yes)
            {
                if (dr == System.Windows.Forms.DialogResult.Yes){Debugger.Launch();}
                UpdateTitle();
            }

        }

        private void 도움말HToolStripMenuItem_DropDownOpening(object sender, EventArgs e)
        {
            if (IsDebugging) 
            { 디버깅ToolStripMenuItem.Enabled = false;
              디버깅ToolStripMenuItem.ToolTipText = "디버깅 중이므로 비활성화 되었습니다.";}
            else 
            {디버깅ToolStripMenuItem.Enabled = true;
             디버깅ToolStripMenuItem.ToolTipText = "디버깅을 시작합니다.";}
        }

        private void syncToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (asyncToolStripMenuItem.Checked)
            {
                DialogResult dr = MessageBox.Show("비동기 모드를 해제하게되면 진행중인 작업" +
                    "\n\n예) 여러파일 및 재생목록 열기, 웨이브 데이터 저장하기, 여러 항목 선택하기\n\n" +
                    "이 완료될때까지 이 프로그램이 응답하지 않을 수 있습니다. (작업 중이었다면 즉시 적용됩니다.)\n\n" +
                    "대신 속도가 조금 빨라집니다. 상황에 맞게 사용하십시오.\n\n정말로 비동기 모드를 해제하시겠습니까?",
                    "비동기 모드 해제 알림", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                if (dr == System.Windows.Forms.DialogResult.Yes)
                {
                    asyncToolStripMenuItem.Checked = false;
                    NextMusicThread.Abort();
                }
            }
            else
            {
                DialogResult dr = MessageBox.Show("비동기 모드를 설정하게되면 진행중인 작업" +
                    "\n\n예) 여러파일 및 재생목록 열기, 웨이브 데이터 저장하기, 여러 항목 선택하기등\n\n" +
                    "이 완료될때까지 이 프로그램을 조작할 수 있습니다. (작업 중이었다면 즉시 적용됩니다.)\n\n" +
                    "대신 불안정 합니다. 상황에 맞게 사용하십시오.\n\n정말로 비동기 모드를 설정하시겠습니까?",
                    "비동기 모드 설정 알림 (추천하지 않습니다.)", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                if (dr == System.Windows.Forms.DialogResult.Yes)
                {
                    asyncToolStripMenuItem.Checked = true;
                    
                    NextMusicThread = new Thread(NextMusicThread_);
                    NextMusicThread.Name = "음악 다음으로 넘기기 스레드";
                    NextMusicThread.Start();
                }
            }
        }

        private void listView1_KeyPress(object sender, KeyPressEventArgs e)
        {
            //if (e.KeyChar == 13){if (btn재생.Enabled) button1_Click(null, null);}
            if (e.KeyChar == 6)
            {
                if (listView1.SelectedItems.Count == 1) { 가사찾기ToolStripMenuItem_Click(null, null); }
                else { 싱크가사찾기ToolStripMenuItem_Click(null, null); }
            }
        }


        TimeSpan LastKeyStrokeTime;
        private void listView1_KeyUp(object sender, KeyEventArgs e)
        {
            DateTime dt = DateTime.Now;
            TimeSpan ts = new TimeSpan(dt.Day, dt.Hour, dt.Minute, dt.Second, dt.Millisecond);
            if (listView1.Items.Count > 0 /*&& ts .TotalMilliseconds> LastKeyStrokeTime.TotalMilliseconds + 800*/)
            {
                //MessageBox.Show(e.KeyData.ToString(), e.KeyValue.ToString());
                /*
                if (e.KeyValue == 32) //Space 키
                {if (button1.Enabled) button1_Click(null, null);}*/
                if (e.KeyValue == 46)//Delete 키
                { if (listView1.SelectedItems.Count > 0) { 삭제ToolStripMenuItem_Click(null, null); } }
                else if (e.KeyValue == 13) {  재생ToolStripMenuItem_Click(null, null); }
            }
            LastKeyStrokeTime = ts;
        }

        private void listView1_DoubleClick(object sender, EventArgs e)
        {
            if (listView1.SelectedItems.Count == 1)
            {
                StopNextMusicThread = true; 
                try{재생ToolStripMenuItem_Click(null, null); }catch{}
                StopNextMusicThread = false;
            }
        }

        public void 프로그램다시시작ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DialogResult dr = System.Windows.Forms.DialogResult.Yes;
            bool Direct = false;

            try { Direct = (bool)sender; if(Direct) goto START;  } catch { }
            dr = MessageBox.Show(LocalString.프로그램다시시작ToolStripMenuItem_Message, LocalString.프로그램다시시작ToolStripMenuItem_MessageTitle, MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning);

            START:
            if (dr == System.Windows.Forms.DialogResult.Yes)
            {
                //ps.Helper.Restart();
                //button1.Text = "재생";
                Process p = System.Diagnostics.Process.GetCurrentProcess();
                StringBuilder sb = new StringBuilder("#EXTM3U");
                foreach (ListViewItem a in listView1.Items) { try{sb.Append("\r\n" + a.SubItems[3].Text);} catch{}}

                string path = Path.GetTempFileName();
                try { File.Delete(path); File.Create(path + ".m3u").Close();path=path + ".m3u"; }catch{}
                try { System.IO.File.WriteAllText(path, sb.ToString()); }catch{}

                string ProfilePath = Path.GetTempFileName();
                try { File.Delete(ProfilePath); File.Create(ProfilePath + ".proj").Close(); ProfilePath=ProfilePath + ".proj";}catch{}
                if (ps!=null) try { File.WriteAllLines(ProfilePath, HS_CSharpUtility.Utility.EtcUtility.SaveSetting(ps.Setting)); }catch{}

                string Args = Direct ? string.Format("/Restart {0} --Timeout 100 ", p.Id) : "";
                try { Args += string.Format("/DeleteProject {0} {1} \"{2}\" -index {3} -Position {4} -Project \"{5}\"" ,NewInstance ? "/NewInstance" : "", Helper.PlayStatus == HSAudioHelper.PlayingStatus.Play ? "/Play" : "", path, CurrentItem != null ? CurrentItem.Index : 0, Helper.CurrentPosition, ProfilePath, p.StartInfo.Arguments); }
                catch { }
                /*
                Dictionary<string, string> a = new Dictionary<string, string>();
                foreach (System.Collections.DictionaryEntry a1 in p.StartInfo.EnvironmentVariables)
                { a.Add(a1.Key.ToString(), a1.Value.ToString());}*/

                try { mtx.ReleaseMutex(); notifyIcon1.Dispose(); } catch { }
                try { info.Dispose(); } catch { }
                //bool Exist = false; try { Exist = File.Exists((string)sender); } catch { }
                //if (Exist) Process.Start(p.MainModule.FileName, "--UpdateProgram " + (string)sender + p.MainModule.FileName + " -Params " + Args);z

                try
                {
                    ProcessStartInfo info = new ProcessStartInfo(p.ProcessName, Args);
                    info.UseShellExecute = false;
                    Process p1 = Process.Start(info);
                    if (!(p1 != null && p1.Id != 0)) MessageBox.Show("프로그램을 시작하지 못했습니다.\n수동으로 시작해주시기 바랍니다.", "HS 플레이어", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                catch(Exception ex)
                {
                    bool succ = LIBRARY.HSOther.HSProcessStart.Start(p.ProcessName, Args);
                    if(!succ) MessageBox.Show("프로그램을 시작하지 못했습니다.\n수동으로 시작해주시기 바랍니다.", "HS 플레이어", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

                if (Direct) Thread.Sleep(1000);
                if (!NoDelete) { try { File.Delete(tmpProfileSetting); } catch { } try { File.Delete(tmpPlaylist); } catch { } }

                try { Process.Start(p.MainModule.FileName, "--KillProcess -PID " + p.Id); } catch { } finally { p.Kill(); }
            }
            else if (dr == System.Windows.Forms.DialogResult.No)
            {
                Process p = System.Diagnostics.Process.GetCurrentProcess();
                try { notifyIcon1.Dispose(); mtx.ReleaseMutex(); }catch { }
                Process.Start(p.MainModule.FileName,NewInstance ? "/NewInstance" : "");
                p.Kill();
            }
        }

        Dictionary<string, string> LastPath = new Dictionary<string, string>();


        string LastFolderOpenDirectory
        {
            get
            {
                if (File.Exists(Application.StartupPath + "\\LastPath.ini"))
                {
                    string[] a = File.ReadAllLines(Application.StartupPath + "\\LastPath.ini");
                    string a1 = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
                    if (LastPath.Count == 0)LastPath=HS_CSharpUtility.Utility.EtcUtility.LoadSetting(a);
                    try { a1 = HS_CSharpUtility.Utility.EtcUtility.LoadSetting(a)["LastFolderOpenDirectory"]; }
                    catch { a1 = Environment.GetFolderPath(Environment.SpecialFolder.Desktop); }
                    /*
                    try { LastPath["LastFileOpenDirectory"] = a1; }
                    catch { LastPath.Add("LastFileOpenDirectory", a1); }
                    try { string c = LastPath["LastListOpenDirectory"]; }
                    catch { LastPath.Add("LastListOpenDirectory", a1); }
                    try { string c = LastPath["LastListSaveDirectory"]; }
                    catch { LastPath.Add("LastListSaveDirectory", a1); }*/
                    if (a1 != null || a1 != "") { return Directory.Exists(a1) ? a1 : Environment.GetFolderPath(Environment.SpecialFolder.Desktop); }
                    else { return Environment.GetFolderPath(Environment.SpecialFolder.Desktop); }
                }
                else { return Environment.GetFolderPath(Environment.SpecialFolder.Desktop); }
            }
            set
            {
                /*
                if (LastPath.Count < 2)
                {
                    LastPath.Add("LastFileOpenDirectory", value);
                    LastPath.Add("LastListOpenDirectory", Environment.GetFolderPath(Environment.SpecialFolder.Desktop));
                    LastPath.Add("LastListSaveDirectory", Environment.GetFolderPath(Environment.SpecialFolder.Desktop));
                }*/
                try { LastPath["LastFolderOpenDirectory"] = value; }
                catch { LastPath.Add("LastFolderOpenDirectory", value); }
                File.WriteAllLines(Application.StartupPath + "\\LastPath.ini", HS_CSharpUtility.Utility.EtcUtility.SaveSetting(LastPath));
            }
        }
        string LastFileOpenDirectory
        {
            get
            {
                if (File.Exists(Application.StartupPath + "\\LastPath.ini"))
                {
                    string[] a = File.ReadAllLines(Application.StartupPath + "\\LastPath.ini");
                    string a1 = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
                    if (LastPath.Count == 0) LastPath=HS_CSharpUtility.Utility.EtcUtility.LoadSetting(a);
                    try { a1 = HS_CSharpUtility.Utility.EtcUtility.LoadSetting(a)["LastFileOpenDirectory"]; }
                    catch { a1 = Environment.GetFolderPath(Environment.SpecialFolder.Desktop); }
                    /*
                    try { LastPath["LastFileOpenDirectory"] = a1; }
                    catch { LastPath.Add("LastFileOpenDirectory", a1); }
                    try { string c = LastPath["LastListOpenDirectory"]; }
                    catch { LastPath.Add("LastListOpenDirectory", a1); }
                    try { string c = LastPath["LastListSaveDirectory"]; }
                    catch { LastPath.Add("LastListSaveDirectory", a1); }
                     * */
                    if (a1 != null || a1 != "") { return Directory.Exists(a1) ? a1 : Environment.GetFolderPath(Environment.SpecialFolder.Desktop); }
                    else { return Environment.GetFolderPath(Environment.SpecialFolder.Desktop); }
                }
                else { return Environment.GetFolderPath(Environment.SpecialFolder.Desktop); }
            }
            set
            {
                /*
                if (LastPath.Count < 2)
                {
                    LastPath.Add("LastFileOpenDirectory", value);
                    LastPath.Add("LastListOpenDirectory", Environment.GetFolderPath(Environment.SpecialFolder.Desktop));
                    LastPath.Add("LastListSaveDirectory", Environment.GetFolderPath(Environment.SpecialFolder.Desktop));
                }*/
                try { LastPath["LastFileOpenDirectory"] = value; }
                catch { LastPath.Add("LastFileOpenDirectory", value); }
                File.WriteAllLines(Application.StartupPath + "\\LastPath.ini", HS_CSharpUtility.Utility.EtcUtility.SaveSetting(LastPath));
            }
        }
        string LastListOpenDirectory
        {
            get
            {
                if (File.Exists(Application.StartupPath + "\\LastPath.ini"))
                {
                    string[] a = File.ReadAllLines(Application.StartupPath + "\\LastPath.ini");
                    string a1 = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
                    if (LastPath.Count == 0) LastPath=HS_CSharpUtility.Utility.EtcUtility.LoadSetting(a);
                    try { a1 = HS_CSharpUtility.Utility.EtcUtility.LoadSetting(a)["LastListOpenDirectory"]; }
                    catch { a1 = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);  }
                    /*
                    try { LastPath["LastListOpenDirectory"] = a1; }
                    catch { LastPath.Add("LastListOpenDirectory", a1); }
                    try { string c=LastPath["LastListSaveDirectory"]; }
                    catch { LastPath.Add("LastListSaveDirectory", a1); }
                    try { string c = LastPath["LastFileOpenDirectory"]; }
                    catch { LastPath.Add("LastFileOpenDirectory", a1); }*/
                    if (a1 != null || a1 != "") { return Directory.Exists(a1) ? a1 : Environment.GetFolderPath(Environment.SpecialFolder.Desktop); }
                    else { return Environment.GetFolderPath(Environment.SpecialFolder.Desktop); }
                }
                else { return Environment.GetFolderPath(Environment.SpecialFolder.Desktop); }
            }
            set
            {
                /*
                if (LastPath.Count < 3)
                {
                    LastPath.Clear(); LastPath.Add("LastFileOpenDirectory", Environment.GetFolderPath(Environment.SpecialFolder.Desktop));
                    LastPath.Add("LastListOpenDirectory", value);
                    LastPath.Add("LastListSaveDirectory", Environment.GetFolderPath(Environment.SpecialFolder.Desktop));
                }
                else { LastPath["LastListOpenDirectory"] = value; }*/
                try { LastPath["LastListOpenDirectory"] = value; }
                catch { LastPath.Add("LastListOpenDirectory", value); }
                File.WriteAllLines(Application.StartupPath + "\\LastPath.ini", HS_CSharpUtility.Utility.EtcUtility.SaveSetting(LastPath));
            }
        }
        string LastListSaveDirectory
        {
            get
            {
                if (File.Exists(Application.StartupPath + "\\LastPath.ini"))
                {
                    string[] a = File.ReadAllLines(Application.StartupPath + "\\LastPath.ini");
                    string a1 = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
                    if (LastPath.Count == 0) HS_CSharpUtility.Utility.EtcUtility.LoadSetting(a);
                    try { a1 = HS_CSharpUtility.Utility.EtcUtility.LoadSetting(a)["LastListSaveDirectory"]; }
                    catch { a1 = Environment.GetFolderPath(Environment.SpecialFolder.Desktop); }
                    /*
                    try { LastPath["LastListSaveDirectory"] = a1; }
                    catch { LastPath.Add("LastListSaveDirectory", a1); }
                    try {string c=LastPath["LastFileOpenDirectory"]; }
                    catch { LastPath.Add("LastFileOpenDirectory", a1); }
                    try { string c = LastPath["LastListOpenDirectory"]; }
                    catch { LastPath.Add("LastListOpenDirectory", a1); }*/
                    if (a1 != null || a1 != "") { return Directory.Exists(a1) ? a1 : Environment.GetFolderPath(Environment.SpecialFolder.Desktop); }
                    else { return Environment.GetFolderPath(Environment.SpecialFolder.Desktop); }
                }
                else { return Environment.GetFolderPath(Environment.SpecialFolder.Desktop); }
            }
            set
            {
                /*
                if (LastPath.Count < 3)
                {
                    LastPath.Clear(); LastPath.Add("LastFileOpenDirectory", Environment.GetFolderPath(Environment.SpecialFolder.Desktop));
                    LastPath.Add("LastListOpenDirectory", Environment.GetFolderPath(Environment.SpecialFolder.Desktop));
                    LastPath.Add("LastListSaveDirectory", value);
                }
                else { LastPath["LastListSaveDirectory"] = value; }*/
                try { LastPath["LastListSaveDirectory"] = value; }
                catch { LastPath.Add("LastListSaveDirectory", value); }
                File.WriteAllLines(Application.StartupPath + "\\LastPath.ini", HS_CSharpUtility.Utility.EtcUtility.SaveSetting(LastPath));
            }
        }

        private void 업데이트변경사항보기ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ViewReadme vr = new ViewReadme(); vr.Show();
        }

        private void 음악이름ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            StringBuilder sb = new StringBuilder(listView1.SelectedItems[0].SubItems[2].Text);
            for (int i = 1; i < listView1.SelectedItems.Count; i++)
            {sb.Append("\r\n"+listView1.SelectedItems[i].SubItems[2].Text); }
            Clipboard.SetText(sb.ToString());
        }

        private void 음악경로ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            StringBuilder sb = new StringBuilder(listView1.SelectedItems[0].SubItems[3].Text);
            for (int i = 1; i < listView1.SelectedItems.Count; i++)
            { sb.Append("\r\n" + listView1.SelectedItems[i].SubItems[3].Text); }
            Clipboard.SetText(sb.ToString());
        }

        private void 탐색기에서해당파일열기ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                string a = Environment.GetEnvironmentVariable("windir") + "\\explorer.exe";
                for (int i = 0; i < listView1.SelectedItems.Count; i++)
                {Process.Start(a, "/select," + listView1.SelectedItems[i].SubItems[3].Text);}
            }
            catch { }
        }

        private void 싱크가사찾기ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Lyrics.frmLyricsSearch frmlrcSearch = new Lyrics.frmLyricsSearch(); frmlrcSearch.SetForm1Class(this); frmlrcSearch.Show();
        }

        internal Lyrics.frmLyricsSearch frmlrcSearch = new Lyrics.frmLyricsSearch();
        private void 가사찾기ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Lyrics.frmLyricsSearch frmlrcSearch = new Lyrics.frmLyricsSearch(listView1.SelectedItems[0].SubItems[3].Text);
            frmlrcSearch.SetForm1Class(this); frmlrcSearch.Show();
        }

        Thread LyricsThread;
        ThreadStart LyricsThreadStart;
        internal void 바탕화면싱크가사창띄우기ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                if (LyricsThread != null) StopSyncLyrics();
                /*timer3.Dispose(); timer3.Start(); */
                StartSyncLyrics();
                lrcf.Show(); lrcf.Status = frmLyricsDesktop.FormStatus.Show; lrcf.BringToFront();
            }
            catch { MessageBox.Show("바탕 화면 싱크가사를 띄울수 없습니다.\n\n프로그램을 재시작 해주세요."); }
        }
        
        private void 바탕화면싱크가사창새로고침ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //bool a = lrcf.checkBox1.Checked;
            //lrcf.checkBox1.Checked = !a;
            //lrcf.Refresh();
            //lrcf.checkBox1.Checked = a;
            //lrcf.Refresh();
            //lrcf.Hide();
            //lrcf.Show();
            
            lrcf.InvokeIfNeeded(()=>{lrcf.panel2_DoubleClick(null, null);});
            lrcf.InvokeIfNeeded(()=>{lrcf.panel2_DoubleClick(null, null);});
            /*
            ThreadPool.QueueUserWorkItem(new WaitCallback((object o) =>
            {
            }));*/
        }

        public void frmlrcSearch_GetLyricsComplete(/*LyricsClass*/ LyricAlsong[] lc)
        { try{frmlrcSearch_GetLyricsComplete(lc[0]);} catch { }  }
        public void frmlrcSearch_GetLyricsComplete(Lyrics.LyricAlsong lc)
        {
            try
            {
                if (th != null) th.Abort();
                //MessageBox.Show(lc.ToString());
                
                HS_Audio.Lyrics.LyricClass a = lc.GetLyrics;
                    if (lc.Title == "" || lc.Title == null)
                        lc.Title = HS_CSharpUtility.Utility.StringUtility.GetFileName(MusicPath, false);
                    if (lc.Length == "" || lc.Length == null)
                        lc.Length=a.Length = ShowTime(TimeSpan.FromMilliseconds(Helper.TotalPosition));
                //lrcf.Lyrics = new LyricsEz(lc.GetLyrics, lc.MD5Hash);
                string a1 = a.ToString();
                lrcf.Lyric = a1;
                if (싱크가사로컬캐시ToolStripMenuItem.Checked && (lc != null || lc.Lyric != null || lc.Lyric != "")) Lyricscache.SetLyrics(a1, lc.MD5Hash);
                //MessageBox.Show(lc.LyricsInfo.ToString(), lc.MD5Hash);

                //ths = new System.Threading.T2hreadStart(th_Tick); 
                //th = new System.Threading.Thread(ths);
                //th.Start();
            }
            catch { }
        }

        //0번쨰: 현재 플레이어 UI 쓰레드
        //1번째: 음악 태그 정보 쓰레드
        //2번째: Kbps 쓰레드
        Thread[] MainThread = new Thread[5];
        ThreadStart[] MainThreadStart = new ThreadStart[4];
        bool CurrentPreLoading;
        string MD5Hash;
        public string MusicPath { get; private set; }
        void fh_MusicPathChanged(HSAudioHelper Helper, int index, bool Error)
        {
            checkSettingDeviceFormat(Helper.DeviceFormat);
            this.CurrentPreLoading = this.Helper.PreLoading;
            this.MusicPath = Helper.MusicPath;
            this.lrcf.MusicPath = Helper.MusicPath;
            //PluginInit();
            lrcf.Lyric = "";
            try
            {
                MD5Hash = Lyrics.GetLyrics.GenerateMusicHash(MusicPath);
                if (!싱크가사우선순위ToolStripMenuItem.Checked)
                {
                    string a = Lyricscache.GetLyrics(MD5Hash);
                    lrcf.MD5Hash = Lyricscache.MD5Hash_Music = MD5Hash;
                    if (a == null || a == "") frmlrcSearch.FindLyrics(MusicPath);
                    else
                    {
                        if (태그없는항목자동삽입ToolStripMenuItem.Checked){
                            bool Change=false;
                            Lyrics.LyricParser lc = new Lyrics.LyricParser(a);
                            if (lc.Title == "" || lc.Title == null){
                                lc.Title = HS_CSharpUtility.Utility.StringUtility.GetFileName(MusicPath, false);Change=true;}
                            if (lc.Length == "" || lc.Length == null){
                                lc.Length = ShowTime(TimeSpan.FromMilliseconds(this.Helper.TotalPosition));Change=true;}
                            string a1=lc.ToString();
                            lrcf.Lyric = a1;
                            if (싱크가사로컬캐시ToolStripMenuItem.Checked&&Change) Lyricscache.SetLyrics(a1, MD5Hash);}
                        
                        else {lrcf.Lyric =a;}
                    } 
                }
                else { frmlrcSearch.FindLyrics(MusicPath); }
            }
            catch { frmlrcSearch.FindLyrics(MusicPath); }
            try { if (ps != null) { ps.Helper = this.Helper; ps.numPitch_ValueChanged(null, null); }/* else { ps = new PlayerSetting(fh); }*/ }catch{}
            
            try{
            InitfrmMainExtend();
            for (int i = 1; i < MainThread.Length; i++) if (MainThread[i] != null) MainThread[i].Abort();
            MainThread[1] = new Thread(MainThreadStart[0]){ Name = "HS 플레이어 메인창 음악 태그 스레드" };
            MainThread[2] = new Thread(MainThreadStart[1]){ Name = "HS 플레이어 메인창 Kbps 스레드" };
            MainThread[3] = new Thread(MainThreadStart[2]){ Name = "HS 플레이어 메인창 Peak미터 스레드" };
            for (int i = 1; i < MainThread.Length; i++) if(MainThread[i]!=null) MainThread[i].Start();}catch{}

            //if (lrcf != null && lrcf.Status == frmLyricsDesktop.FormStatus.Show) StartSyncLyrics();
            /*timer3.Dispose(); timer3.Start(); */
            StartSyncLyrics();
        }

        System.Threading.Thread th;
        System.Threading.ThreadStart ths;
        private void th_Tick()
        {
            //lrcf.GetLyric(fm[0].CurrentPosition);
            //th.Start();
        }

        
        private void lrcf_FormStatusChange(HS_Audio.Lyrics.frmLyricsDesktop.FormStatus Status)
        {
            if (Status != Lyrics.frmLyricsDesktop.FormStatus.Show) StopSyncLyrics();//timer3.Stop();
            else StartSyncLyrics();//timer3.Start();
        }
        int UpdateTime = 25;
        private void lrcf_UpdateLyricsIntervalChange(int Uptime){ timer3.Interval=UpdateTime = Uptime; }

        bool StartSyncLyrics()
        {
            try
            {
                StopSyncLyrics();
                LyricsThread = new Thread(LyricsThreadStart){Name = "싱크가사 스레드"};
                LyricsThread.Start();
                return true;
            }
            catch{return false;}
        }
        bool StopSyncLyrics()
        {
            try{if (LyricsThread != null) LyricsThread.Abort(); return true;}catch{return false;}
        }
        private void LyricsThread_Loop()
        {
            while (true)
            {
                timer3_Tick(null, null);
                Thread.Sleep(UpdateTime);
            }
        }
        private void timer3_Tick(object sender, EventArgs e)
        {
            try { if (Helper != null && lrcf!=null)lrcf.GetLyric(Helper.CurrentPosition * 0.001); }catch { }
        }

        private void 맨위로설정ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.TopMost = 맨위로설정ToolStripMenuItem.Checked;
            UpdateSetting(true);
        }

        private void 상태표시줄업데이트ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            UpdateTitle();
            //toolStripProgressBar1.Visible = toolStripStatusLabel2.Visible=false;
            if (!tmpStop) toolStripProgressBar1.Visible = toolStripStatusLabel2.Visible = toolStripStatusLabel2.Visible = false; 
            else toolStripProgressBar1.Visible = true;
            if (Helper != null && Helper.channel != null)
            {
                toolStripProgressBar2.Visible = true;
                toolStripStatusLabel1.Text = LocalString.준비+(밀리초표시ToolStripMenuItem.Checked?" [00:00:00.000 / 00:00:00.000]":"[00:00:00 / 00:00:00]");
                statusStrip1_SizeChanged(null, null);
            }
            else { toolStripProgressBar1.Visible = toolStripStatusLabel2.Visible = toolStripStatusLabel2.Visible = false;
                   toolStripStatusLabel1.Text = LocalString.준비;}
            toolStripStatusLabel1.GetCurrentParent().Update();
        }

        private void 파일열기ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            openFileDialog1.InitialDirectory = LastFileOpenDirectory;
            if (openFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                LastFileOpenDirectory = Path.GetDirectoryName(openFileDialog1.FileName); 
                파일열기(null, openFileDialog1.FileNames); 
            }
        }

        private void 폴더열기ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            folderBrowserDialog1.SelectedPath = LastFolderOpenDirectory;

            if (folderBrowserDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                LastFolderOpenDirectory =folderBrowserDialog1.SelectedPath;
                폴더열기(null, folderBrowserDialog1.SelectedPath,하위폴더검색ToolStripMenuItem.Checked,false);
            }
        }

        readonly string[] Filters = { ".mp3; .ogg; .wav; .wma; .flac; .aif; .aiff; .mid",
                                      ".mp3",".wma",".wav",".ogg",".flac",".aif; .aiff",".mid",
                                      ".fsb; .wax; .asf; .asx; .dls",".fsb",".wax",".asf; .asx; .dls"};
        string[] FilterFilePath(string[] Path, string Filter, bool IncludeDirectory = false)
        {
            string[] _Filter = Filter.Split(new string[]{";", " "}, StringSplitOptions.RemoveEmptyEntries);
            List<string> item = new List<string>();
            int j = 0;//decimal i=0;string tmp;
            foreach (string a in Path)
            {
                for(j=0;j<_Filter.Length;j++){
                    if(System.IO.Path.GetExtension(a)==_Filter[j].ToLower() || (IncludeDirectory && Directory.Exists(a)))
                        item.Add(a);}
            }
            return item.ToArray();
        }
        List<string> FilterFilePath(List<string> Path, string Filter)
        {
            string[] _Filter = Filter.Split(new string[] { ";", " " }, StringSplitOptions.RemoveEmptyEntries);
            List<string> item = new List<string>();
            int j = 0;//decimal i=0;string tmp;
            foreach (string a in Path)
            {
                for (j = 0; j < _Filter.Length; j++)
                {
                    if (System.IO.Path.GetExtension(a) == _Filter[j].ToLower())
                        item.Add(a);
                }
            }
            return item;
        }

        private void 프로젝트열기ToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void 재생컨트롤ToolStripMenuItem_DropDownOpening(object sender, EventArgs e)
        {
            정지ToolStripMenuItem1.Enabled = btn정지.Enabled;
            재생ToolStripMenuItem1.Enabled = btn재생.Enabled;
            btn재생.BackgroundImage = Properties.Resources.PlayHS;
            정지ToolStripMenuItem1.Text = LocalString.정지;
            다음ToolStripMenuItem.Enabled = 이전ToolStripMenuItem.Enabled = listView1.Items.Count > 0;
            if (Helper!=null) 재생ToolStripMenuItem1.Text = Helper.PlayStatus == HSAudioHelper.PlayingStatus.Play ? LocalString.일시정지 : LocalString.재생;
        }

        private void notifyIcon1_MouseMove(object sender, MouseEventArgs e)
        {
            if (Helper != null)
            {
                string a = null;
                string b = CurrentTick().ToString();
                string c = TotalTick().ToString();
                try{a = string.Format("{0} / {1}",b.Remove(b.LastIndexOf(".")), c.Remove(c.LastIndexOf(".")));}catch{}
                try
                {
                    if(Environment.OSVersion.Version.Major<=5) notifyIcon1.Text = string.Format(LocalString.ProgramName+" ({0})\n{1}\n{2}", PlayStatustoString(Status_NoMixing),
                        System.IO.Path.GetFileName(Helper.MusicPath), a == null ? "" : LocalString.재생시간 + ": " + a);
                    else notifyIcon1.SetNotifyIconText (string.Format(LocalString.ProgramName+" ({0})\n{1}\n{2}", PlayStatustoString(Status_NoMixing),
                        System.IO.Path.GetFileName(Helper.MusicPath), a == null ? "" : LocalString.재생시간 + ": " + a));
                }
                catch
                {
                    try
                    {
                        if (Environment.OSVersion.Version.Major <= 5)
                            notifyIcon1.Text = string.Format("{0}\n{2}",
                                System.IO.Path.GetFileName(Helper.MusicPath), a == null ? "" : LocalString.재생시간 + ": " + a);
                        else
                            notifyIcon1.SetNotifyIconText(string.Format("{0}\n{2}",
                                System.IO.Path.GetFileName(Helper.MusicPath), a == null ? "" : LocalString.재생시간 + ": " + a));
                    }
                    catch
                    {
                        try
                        {
                            if (Environment.OSVersion.Version.Major >= 5)
                                notifyIcon1.Text = string.Format(LocalString.ProgramName + " ({0}) - [{1}]", PlayStatustoString(Status_NoMixing),
                                System.IO.Path.GetFileName(Helper.MusicPath));
                            else
                                notifyIcon1.SetNotifyIconText(string.Format(LocalString.ProgramName + " ({0}) - [{1}]", PlayStatustoString(Status_NoMixing),
                                System.IO.Path.GetFileName(Helper.MusicPath)));

                            notifyIcon1.Text = string.Format(LocalString.ProgramName+" ({0}) - [{1}]", PlayStatustoString(Status_NoMixing),
                            System.IO.Path.GetFileName(Helper.MusicPath));
                        }
                        catch
                        {
                            try { notifyIcon1.Text = string.Format("{0}", System.IO.Path.GetFileName(MusicPath)); }
                            catch{ notifyIcon1.Text = string.Format(LocalString.ProgramName + " ({0})" ,PlayStatustoString(Status_NoMixing)); }
                        }
                    }
                }

            }
        }

        private void 메인창열기ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Show();
            this.BringToFront();
            //ThreadPool.QueueUserWorkItem(new WaitCallback(AddAddThumbnailBarButton_Thread), this.Handle);
            if (Environment.OSVersion.Version.Major >= 6 && Environment.OSVersion.Version.Minor >= 1){
            try { TaskBarExtensions.AddThumbnailBarButtons(this, this.thumbnailBarButtons); }catch{}}
            this.TopMost = false;
            this.TopMost = true;
            this.TopMost = 맨위로설정ToolStripMenuItem.Checked;
        }

        private void 트레이로보내기ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //WindowState = FormWindowState.Minimized;
            this.Hide();
        }

        private void notifyIcon1_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            메인창열기ToolStripMenuItem_Click(null, null);
        }

        private void contextMenuStrip2_Opening(object sender, CancelEventArgs e)
        {
            재생설정열기ToolStripMenuItem.Enabled=btn재생설정.Enabled;
            끝내기XToolStripMenuItem.Text = 끝내기ToolStripMenuItem.Text;
            toolStripComboBox2.Text = toolStripComboBox1.Text;
        }

        private void mD5해쉬값구하기ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                for (int i = 0; i < listView1.SelectedItems.Count; i++)
                {
                    MessageBox.Show("MD5 Hash: " + Lyrics.GetLyrics.GenerateMusicHash(listView1.SelectedItems[i].SubItems[3].Text),
                        Path.GetFileName(listView1.SelectedItems[i].SubItems[3].Text));
                }
            }
            catch { }
        }

        private void 싱크가사캐시정리ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("싱크가사 로컬 캐시 정리를 하게되면 \n가사가 없는 해시값은 지워집니다.\n사용자의 플레이어에는 아무영향도 안미칩니다.\n\n계속 하시겠습니까?",
                "싱크가사 로컬 캐시 정리", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == System.Windows.Forms.DialogResult.Yes)
            { Lyricscache.CacheCleaner(); }
        }

        private void 싱크가사캐시관리ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            HS_Audio.Lyrics.frmLyricsCacheManager LyricsCacheManager = new Lyrics.frmLyricsCacheManager(Lyricscache);
            LyricsCacheManager.Show();
        }

        private void 재생중인파일상세정보ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmSoundSystemInfo fsi = new frmSoundSystemInfo(Helper);
            fsi.Show();
        }

        private void 보기VToolStripMenuItem_DropDownOpening(object sender, EventArgs e)
        {
            if (/*fm != null && fm.Count != 0 && */Helper != null && Helper.system != null) 재생상세정보ToolStripMenuItem.Enabled = true;
            else 재생상세정보ToolStripMenuItem.Enabled = false;
        }

        private void 가비지커렉션가동ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show(string.Format(LocalString.가비지커렉션가동ToolStripMenuItem_Message, GC.GetTotalMemory(false).ToString("##,###")),
                LocalString.가비지커렉션가동ToolStripMenuItem_MessageTitle, MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == System.Windows.Forms.DialogResult.Yes)
            System.GC.Collect();
        }


        internal static frmUpdate fu;
        private void 업데이트확인ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if(fu == null || fu.IsDisposed)fu = new frmUpdate(this);
            fu.Show();
            fu.BringToFront();
        }
        private void testToolStripMenuItem_Click(object sender, EventArgs e)
        {
            TaskBarExtensions.AddThumbnailBarButtons(this, this.thumbnailBarButtons);
        }
        int thi = 0;
        private void AddAddThumbnailBarButton_Thread(object o)
        {
            IntPtr Handle = (IntPtr)o;
            while (thi<100000) {
                try
                { /*TaskBarExtensions.AddThumbnailBarButtons((IntPtr)o, this.thumbnailBarButtons);*/
                    TaskBarExtensions.AddThumbnailBarButtons(Handle, this.thumbnailBarButtons);
                    thumbnailBarButtons[1].IsDisabled = !btn재생.Enabled;
                    thumbnailBarButtons[2].IsDisabled = !btn정지.Enabled;
                    thumbnailBarButtons[0].IsDisabled =
                    thumbnailBarButtons[3].IsDisabled = !(listView1.Items.Count > 0); 
                    break;
                }
                catch (InvalidOperationException ex)
                { /*Huseyint.Windows7.WindowsForms.TaskBarExtensions.Attach();*/
                }
            finally {thi++; } } thi=0;}

        private void button1_EnabledChanged(object sender, EventArgs e)
        {
            try { thumbnailBarButtons[1].IsDisabled = !btn재생.Enabled; }catch{}
            btnItemFocus.Enabled = btn재생.Enabled;
            btn재생.BackgroundImage = btn재생.Enabled ? Properties.Resources.PlayHS : Properties.Resources.PlayHS.ToMonochromatic(true);
        }

        private void btn정지_EnabledChanged(object sender, EventArgs e)
        {
            try { thumbnailBarButtons[2].IsDisabled = !btn정지.Enabled; }catch{}
            btn정지.BackgroundImage = btn정지.Enabled ? Properties.Resources.StopHS : Properties.Resources.StopHS.ToMonochromatic(true);
        }

        private void toolStripComboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (toolStripComboBox1.SelectedIndex)
            {
                case 2:
                    Program.localeManager.CurrentLocale=Locale.en_us;break;
                case 1:
                    Program.localeManager.CurrentLocale=Locale.ja_jp;break;
                default:
                    Program.localeManager.CurrentLocale=Locale.ko_kr;break;
            }
            if (!LockAutoScaleMode) UpdateSetting();
        }

        internal void UpdateTitle()
        {

            if (안전모드ToolStripMenuItem.Checked)
            {
                //조장찡 플레이어 (디버깅, 안전모드); 준비 (디버깅, 안전모드)
                if (IsDebugging)
                {
                    this.Text = string.Format("{0} ({1}, {2})", ProgramName, LocalString.디버깅, LocalString.안전모드);
                    toolStripStatusLabel1.Text = string.Format("{0} ({1}, {2})", LocalString.준비, LocalString.디버깅, LocalString.안전모드);
                }
                //조장찡 플레이어 (안전모드); 준비 (안전모드)
                else { this.Text = string.Format("{0} ({1})", ProgramName, LocalString.안전모드); toolStripStatusLabel1.Text = string.Format("{0} ({1})", LocalString.준비, LocalString.안전모드); }
            }
            else
            {
                //조장찡 플레이어 (디버깅); 준비 (안전모드)
                if (IsDebugging) { this.Text = string.Format("{0} ({1})", ProgramName, LocalString.디버깅); toolStripStatusLabel1.Text = string.Format("{0} ({1})", LocalString.준비, LocalString.디버깅); }
                //조장찡 플레이어; 준비
                else { this.Text = ProgramName; toolStripStatusLabel1.Text = LocalString.준비; }
            }
        }

        private void toolStripComboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            toolStripComboBox1.SelectedIndex = toolStripComboBox2.SelectedIndex;
        }

        private void 로그폴더열기ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try { if (!Directory.Exists(Application.StartupPath + "\\Log"))Directory.CreateDirectory(Application.StartupPath + "\\Log"); }catch{}
            string a=Environment.GetFolderPath(Environment.SpecialFolder.System);
            Process.Start(a.Remove(a.LastIndexOf("\\")) + "\\explorer.exe", Application.StartupPath + "\\Log");
        }

        private void 로그지우기ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if(MessageBox.Show(LocalString.로그지우기ToolStripMenuItem_Message, LocalString.로그지우기ToolStripMenuItem, MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == System.Windows.Forms.DialogResult.Yes)
                try { Directory.Delete(Application.StartupPath + "\\Log", true); }
                catch (Exception ex) { MessageBox.Show(LocalString.로그지우기ToolStripMenuItem_MessageEx + ex.ToString(), LocalString.오류, MessageBoxButtons.OK, MessageBoxIcon.Error); }
        }

        private void 로그관리ToolStripMenuItem_DropDownOpening(object sender, EventArgs e)
        {
            로그지우기ToolStripMenuItem.Enabled = Directory.Exists(Application.StartupPath + "\\Log");
        }

        int MousePositionX;
        bool bSamePosition;

        private void listView1_MouseUp(object sender, MouseEventArgs e)
        {
            try
            {
                if (listView1.Items.Count > 0 && listView1.Items[listView1_MouseMove_List_Last].BackColor != listView1.BackColor)
                {
                    listView1.Items[listView1_MouseMove_List_Last].BackColor = Color.Transparent;
                    listView1.Items[listView1_MouseMove_List_Last].ForeColor = listView1.ForeColor;
                }
            }
            catch (Exception ex) { HSConsole.WriteLine(ex.ToString()); }

            bool bSamePosition = MouseClick = false;
            this.Cursor = Cursors.Arrow;
            ListViewItem selected = this.listView1.GetItemAt(e.X, e.Y);
            if (null != selected)
            {
                List<int> insert = new List<int>();
                foreach (ListViewItem l in listView1.SelectedItems)
                {
                    if (l.Index == selected.Index)
                    {
                        bSamePosition = true;
                    }
                }

                if (!bSamePosition)
                {
                    List<ListViewItem> sl = new List<ListViewItem>();
                    foreach (ListViewItem l in listView1.SelectedItems)
                    {
                        sl.Add(l);
                        l.Remove();
                        if (MousePositionX < e.X) { listView1.Items.Insert(selected.Index + 1, l); insert.Add(selected.Index + 1); }
                        else { listView1.Items.Insert(selected.Index, l); insert.Add(selected.Index); }//toolTip1.Show(listView1.SelectedItems.
                        HSConsole.WriteLine("\nselected.Index=" + selected.Index);
                        //if (asyncToolStripMenuItem.Checked) Application.DoEvents();
                    }
                    Init_Sort(false);
                    try { if (PlaylistChanged != null) PlaylistChanged(this, PlayListChange.Change, insert.ToArray()); } catch { }
                }
            }
            pt.X = -1; pt.Y = -1;
            SearchPlayList(false);
            MousePositionX = e.X;
        }

        private void btnScrollUp_Click(object sender, EventArgs e)
        {
            //listView1.AutoScrollOffset = new Point(0, 0);
            try { if(listView1.Items.Count>0)listView1.Items[0].EnsureVisible(); }catch{}
        }
        private void btnScrollDown_Click(object sender, EventArgs e)
        {
            try { if(listView1.Items.Count>0)listView1.Items[listView1.Items.Count - 1].EnsureVisible(); }catch{}
        }

        private void btnItemFocus_Click(object sender, EventArgs e)
        {
            try {
                //Color a = CurrentItem.BackColor;
                //Color b = CurrentItem.ForeColor;
                if (CurrentItem != null) { CurrentItem.Focused = true; CurrentItem.EnsureVisible(); }
                //CurrentItem.BackColor = frmEqualizerGraph.CalculateNegativeColor(a);
                //CurrentItem.ForeColor = frmEqualizerGraph.CalculateNegativeColor(b);
                //CurrentItem.BackColor = a; CurrentItem.ForeColor = b;
            }catch { }
        }

        ListViewItem MouseDownItem;
        private void listView1_MouseDown(object sender, MouseEventArgs e)
        {
            MouseClick = true;
            MouseDownItem = this.listView1.GetItemAt(e.X, e.Y);
        }

        Point pt;
        private void listView1_MouseMove(object sender, MouseEventArgs e)
        {
            HighLighting(e.X, e.Y);
        }

        private void 색상설정ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (colorDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                listView1.BackColor = colorDialog1.Color;
                UpdateSetting();
            }
        }

        OpenFileDialog ImageDialog = new OpenFileDialog()
        {
            Filter = "일반 사진 파일|*.jpg;*.jpeg;*.bmp;*.png;*.gif;*.tif;*.tiff;|특수 그림 파일 (*.wmf, *.Exif, *.Emf)|*.wmf;*.Exif;*.Emf",
            Title="재생 목록창에 사용할 사진 열기...",
        };

        Image _CurrentImageforPlaylist;
        bool CurrentImageforPlaylistFlag = true;
        Image CurrentImageforPlaylist
        {
            get{return _CurrentImageforPlaylist;}
            set
            {
                if (_CurrentImageforPlaylist != null) _CurrentImageforPlaylist.Dispose();
                _CurrentImageforPlaylist = null;

                try{int a = value.Width;}
                catch
                {
                    listView1.BackgroundImage = null;
                    if (CurrentImageforPlaylistFlag) File.Delete(ApplicationDir + "\\PlayListBackgroundImage.ini");
                    return;
                }


                _CurrentImageforPlaylist = value;
                listView1.BackgroundImage = value;

                if(CurrentImageforPlaylistFlag){
                //LastCurrentImageforPlaylist = (Image)CurrentImageforPlaylist.Clone();
                try
                {
                    Image img = value;
                    if (value == null)
                    {
                        File.Delete(ApplicationDir + "\\PlayListBackgroundImage.ini");
                        return;
                    }

                    MemoryStream ms = new MemoryStream();

                    if (img.RawFormat.Guid == System.Drawing.Imaging.ImageFormat.Bmp.Guid) img.SaveJPG(ms, 95);
                    else img.Save(ms, img.RawFormat);
                    //a.Add("\nPlayListBackgroundImage", HS_CSharpUtility.Utility.EtcUtility.ConvertByteArrayTostring(ms.ToArray()));

                    string a = "//이 파일을 절때 수정하지 마세요!! (Please don't edit this file!!)\r\n"; a += HS_CSharpUtility.Utility.EtcUtility.ConvertByteArrayTostring(ms.ToArray());

                    //if (!Directory.Exists(ApplicationDir + "\\Setting")) Directory.CreateDirectory(ApplicationDir + "\\Setting");
                    File.WriteAllText(ApplicationDir + "\\PlayListBackgroundImage.ini", a);
                }
#if DEBUG
                    catch(Exception ex){LoggingUtility.Logging(ex, "재생목록 이미지 저장오류!!");MessageBox.Show(ex.ToString(), "재생목록 이미지 저장오류!!");}
#else
                catch (IOException ex) {LoggingUtility.Logging(ex, "재생목록 이미지 저장오류!!"); }
                catch (Exception ex)
                {
                    LoggingUtility.Logging(ex, "재생목록 이미지 저장오류!!");
                    if (PlayListImageStream != null)
                    {
                        string a = "//이 파일을 절때 수정하지 마세요!! (Please don't edit this file!!)\r\n"; a += HS_CSharpUtility.Utility.EtcUtility.ConvertByteArrayTostring(PlayListImageStream.ToArray());
                        File.WriteAllText(ApplicationDir + "\\PlayListBackgroundImage.ini", a);
                    }
                    //a.Add("\nPlayListBackgroundImage", HS_CSharpUtility.Utility.EtcUtility.ConvertByteArrayTostring(PlayListImageStream.ToArray()));
                }
#endif
                finally { if(ms!=null)ms.Close(); }}
                else CurrentImageforPlaylistFlag = true;
            }
        }
        private void 이미지불러오기ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (ImageDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            { try {CurrentImageforPlaylist=Image.FromFile(ImageDialog.FileName); listView1_SizeChanged(null, null);UpdateSetting();}
            catch(Exception ex) {MessageBox.Show(LocalString.이미지불러오기ToolStripMenuItem_Message+ex.ToString(), 
                LocalString.이미지불러오기ToolStripMenuItem_MessageTitle, MessageBoxButtons.OK, MessageBoxIcon.Error); } }
        }

        private void 지우기ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show(LocalString.지우기ToolStripMenuItem_Message, LocalString.지우기ToolStripMenuItem_MessageTitle,
                MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == System.Windows.Forms.DialogResult.Yes)
            {
                try
                {
                    if (listView1.BackgroundImage != null) listView1.BackgroundImage.Dispose();
                    if (CurrentImageforPlaylist != null) CurrentImageforPlaylist.Dispose();
                }catch{}
                listView1.BackgroundImage = CurrentImageforPlaylist = null;
            }
            UpdateSetting();
        }

        Bitmap bmp;
        SolidBrush br1 = new SolidBrush(Color.Transparent);
        private void listView1_SizeChanged(object sender, EventArgs e)
        {
            if (CurrentImageforPlaylist != null)
            {
                br1.Color = listView1.BackColor;
                try { if(bmp!=null)bmp.Dispose(); }catch { }
                try
                {
                    switch (toolStripComboBox3.SelectedIndex)
                    {
                        case 0:
                            bmp = new Bitmap(CurrentImageforPlaylist, listView1.Width, listView1.Height);
                            listView1.BackgroundImage = bmp; break;
                        default:
                            if (!타일보기ToolStripMenuItem.Checked)
                            {
                                bmp = new Bitmap(listView1.Width, listView1.Height);
                                using (Graphics g = Graphics.FromImage(bmp))
                                {
                                    g.FillRectangle(br1, 0, 0, listView1.Width, listView1.Height);
                                    switch (toolStripComboBox4.SelectedIndex)
                                    {
                                        case 3: g.DrawImage(CurrentImageforPlaylist, listView1.Width - CurrentImageforPlaylist.Width, listView1.Height - CurrentImageforPlaylist.Height); break;
                                        case 2: g.DrawImage(CurrentImageforPlaylist, 0, listView1.Height - CurrentImageforPlaylist.Height); break;
                                        case 1: g.DrawImage(CurrentImageforPlaylist, listView1.Width - CurrentImageforPlaylist.Width, 0); break;
                                        default: listView1.BackgroundImage = CurrentImageforPlaylist; goto Exit;
                                    }
                                    listView1.BackgroundImage = bmp;
                                Exit:
                                    g.Dispose();
                                }
                            }
                            else { listView1.BackgroundImage = CurrentImageforPlaylist; }
                            break;
                    } 
                }catch{}
                
            }
        }

        private void toolStripComboBox3_SelectedIndexChanged(object sender, EventArgs e)
        {
            toolStripComboBox4.Enabled = toolStripComboBox3.SelectedIndex == 1;
            listView1_SizeChanged(null, null);
        }

        private void 모눈선표시ToolStripMenuItem_CheckedChanged(object sender, EventArgs e)
        {
            listView1.GridLines = 모눈선표시ToolStripMenuItem.Checked;
            UpdateSetting();
        }

        private void 삽입색상설정ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            colorDialog2.ShowDialog();
            UpdateSetting();
        }

        private void 타일보기ToolStripMenuItem_CheckedChanged(object sender, EventArgs e)
        {
            listView1.BackgroundImageTiled = 타일보기ToolStripMenuItem.Checked;
        }


        Point listView1_DragOver_Point;
        private void listView1_DragOver(object sender, DragEventArgs e)
        {
            //MessageBox.Show(e.KeyState.ToString());
            if (e.Effect != DragDropEffects.None && e.KeyState != (byte)KeyState_DragEvent_DragOver.Ctrl_Left)
            {
                DragDropMove = true;
                listView1_DragOver_Point.X = e.X; listView1_DragOver_Point.Y = e.Y;
                HighLighting(listView1.PointToClient(listView1_DragOver_Point));
            }
        }

        #region 파일&목록*&폴더 열기
        void BackgroundWorker_Init()
        {
            Worker_파일열기.DoWork += Worker_파일열기_DoWork;
            Worker_파일열기.RunWorkerCompleted += Worker_파일열기_RunWorkerCompleted;
            Worker_파일열기.ProgressChanged += Worker_파일열기_ProgressChanged;
            Worker_목록열기.DoWork += Worker_목록열기_DoWork;
            Worker_목록열기.RunWorkerCompleted += Worker_목록열기_RunWorkerCompleted;
            Worker_목록열기.ProgressChanged += Worker_목록열기_ProgressChanged;
            Worker_폴더열기.DoWork += Worker_폴더열기_DoWork;
            Worker_폴더열기.RunWorkerCompleted += Worker_폴더열기_RunWorkerCompleted;
            Worker_폴더열기.ProgressChanged += Worker_폴더열기_ProgressChanged;
        }
        BackgroundWorker Worker_파일열기 = new BackgroundWorker { WorkerReportsProgress = true };
        BackgroundWorker Worker_목록열기 = new BackgroundWorker { WorkerReportsProgress = true };
        BackgroundWorker Worker_폴더열기 = new BackgroundWorker { WorkerReportsProgress = true };
        struct 파일열기Struct
        {
            public object point; public string[] FilePath;
            public 파일열기Struct(string[] FilePath) { this.point = null; this.FilePath = FilePath; }
            public 파일열기Struct(string[] FilePath, object point) { this.point = point; this.FilePath = FilePath; }
        }
        struct 목록열기Struct
        {
            public object point; public string PlaylistPath;
            public 목록열기Struct(string PlaylistPath) { this.point = null; this.PlaylistPath = PlaylistPath; }
            public 목록열기Struct(string PlaylistPath, object point) { this.point = point; this.PlaylistPath = PlaylistPath; }
        }
        struct 폴더열기Struct
        {
            public object point; public string FolderPath; public bool IncludeSubDirectory; public bool IsIncludeSubDirectory; public bool ShowMessage;
            public 폴더열기Struct(string FolderPath) { this.point = null; this.FolderPath = FolderPath; IncludeSubDirectory = false; IsIncludeSubDirectory = false;ShowMessage = true; }
            public 폴더열기Struct(string FolderPath, object point) { this.point = point; this.FolderPath = FolderPath; IncludeSubDirectory = false; IsIncludeSubDirectory = false; ShowMessage = true; }
            public 폴더열기Struct(string FolderPath, bool IncludeSubDirectory) { point = null; this.FolderPath = FolderPath; this.IncludeSubDirectory = IncludeSubDirectory; IsIncludeSubDirectory = true; ShowMessage = true; }
            public 폴더열기Struct(string FolderPath, object point, bool IncludeSubDirectory) { this.point = point; this.FolderPath = FolderPath; this.IncludeSubDirectory = IncludeSubDirectory; IsIncludeSubDirectory = true; ShowMessage = true; }
            public 폴더열기Struct(string FolderPath, object point, bool IncludeSubDirectory, bool ShowMessage) { this.point = point; this.FolderPath = FolderPath; this.IncludeSubDirectory = IncludeSubDirectory; IsIncludeSubDirectory = true; this.ShowMessage = ShowMessage; }
        }
        int 파일연갯수;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="point"></param>
        /// <param name="FilePath"></param>
        internal void 파일열기(object point, string[] FilePath, bool Async = false)
        {
            파일연갯수 = 0;
            if (Async) Worker_파일열기.RunWorkerAsync(new 파일열기Struct(FilePath, point));
            else Worker_파일열기_DoWork(false, new DoWorkEventArgs(new 파일열기Struct(FilePath, point)));
            //return 0;
        }

        internal void 목록열기(object point, string PlaylistPath, bool Async = false)
        {
            if (Async) Worker_목록열기.RunWorkerAsync(new 목록열기Struct(PlaylistPath, point));
            else Worker_목록열기_DoWork(false, new DoWorkEventArgs(new 목록열기Struct(PlaylistPath, point)));
        }
        void Invoke_목록열기(long i, string FilePath, Encoding enc)
        {
            if (안전모드ToolStripMenuItem.Checked)
            {
                if (IsDebugging)
                {
                    toolStripStatusLabel1.Text = string.Format("{0}... ({1}, {2}) [{3}: {4}]",
                        LocalString.목록여는중, LocalString.디버깅, LocalString.안전모드ToolStripMenuItem, LocalString.파일이름, System.IO.Path.GetFileName(FilePath));
                }
                else
                {
                    toolStripStatusLabel1.Text = string.Format("{0}... ({1}) [{2}: {3}]",
                        LocalString.목록여는중, LocalString.안전모드ToolStripMenuItem, LocalString.파일이름, System.IO.Path.GetFileName(FilePath));
                }
            }
            else
            {
                if (IsDebugging)
                {
                    string.Format("{0}... ({1}) [{2}: {3}]",
                        LocalString.목록여는중, LocalString.디버깅, LocalString.파일이름, System.IO.Path.GetFileName(FilePath));
                }
                else
                {
                    string.Format("{0}... [{1}: {2}]",
                        LocalString.목록여는중, LocalString.파일이름, System.IO.Path.GetFileName(FilePath));
                }
            }

            try { toolStripProgressBar1.Value = i > toolStripProgressBar1.Value ? toolStripProgressBar1.Maximum : (int)i; }
            catch { toolStripProgressBar1.Value = toolStripProgressBar1.Maximum; }
        }

        internal void 폴더열기(object point, string FolderPath, bool IncludeSubDirectory, bool Async)
        {
            if (Async) Worker_폴더열기.RunWorkerAsync(new 폴더열기Struct(FolderPath, point, IncludeSubDirectory));
            else Worker_폴더열기_DoWork(false, new DoWorkEventArgs(new 폴더열기Struct(FolderPath, point, IncludeSubDirectory)));
        }
        internal void 폴더열기(object point, string FolderPath, bool IncludeSubDirectory, bool Async, bool ShowMessage, object thisisDummyObj)
        {
            if (Async) Worker_폴더열기.RunWorkerAsync(new 폴더열기Struct(FolderPath, point, IncludeSubDirectory, ShowMessage));
            else Worker_폴더열기_DoWork(false, new DoWorkEventArgs(new 폴더열기Struct(FolderPath, point, IncludeSubDirectory, ShowMessage)));
        }
        bool IncludeSubDirectory;
        internal void 폴더열기(object point, string FolderPath, bool Async = false)
        {
            if (Async) Worker_폴더열기.RunWorkerAsync(new 폴더열기Struct(FolderPath, point));
            else Worker_폴더열기_DoWork(false, new DoWorkEventArgs(new 폴더열기Struct(FolderPath, point)));
        }
        internal void 폴더열기(object point, string FolderPath, bool Async, bool ShowMessage, object thisisDummyObj)
        {
            폴더열기Struct s = new 폴더열기Struct(FolderPath, point);
            s.ShowMessage = ShowMessage;
            if (Async) Worker_폴더열기.RunWorkerAsync(s);
            else Worker_폴더열기_DoWork(false, new DoWorkEventArgs(s));
        }

        private void Worker_파일열기_DoWork(object sender, DoWorkEventArgs e)
        {
            int g = 0;
            파일열기Struct a = (파일열기Struct)e.Argument;
            string[] FilePath = a.FilePath;
            object point = a.point;
            if (FilePath != null && FilePath.LongLength > 0)
            {
                //LastFileOpenDirectory = Path.GetDirectoryName(FilePath[0]);
                tmpStop = true;
                StringBuilder sb = new StringBuilder();
                bool Async = true; try { Async = (bool)sender; }
                catch { }
                if (!음악믹싱모드ToolStripMenuItem.Checked)
                {
                    try
                    {
                        toolStripProgressBar1.Maximum = FilePath.Length;
                        toolStripProgressBar1.Visible = toolStripStatusLabel2.Visible = true;
                        List<int> open = new List<int>();
                        List<ListViewItem> list = new List<ListViewItem>();
                        for (int i = 0; i < FilePath.Length; i++)
                        {
                            bool Error = false;
                            statusStrip1.Refresh();
                            try
                            {
                                if (Async) Worker_파일열기.ReportProgress(i, FilePath[i]);
                                else { Worker_파일열기_ProgressChanged(null, new ProgressChangedEventArgs(i, FilePath[i])); }

                                if (안전모드ToolStripMenuItem.Checked)
                                {
                                    toolStripProgressBar1.Visible =
                                    toolStripStatusLabel2.Visible = true;
                                    if (BreakLoop) { break; }

                                    if (this.Helper == null)
                                    {
                                        HS_Audio.HSAudioHelper fh = new HS_Audio.HSAudioHelper(); fh.PreLoading = false; fh.MusicPath = FilePath[i];
                                        if (fh.Result == HS_Audio_Lib.RESULT.ERR_FORMAT ||
                                            fh.Result.ToString().IndexOf("ERR_FILE_") != -1) throw new SoundSystemException(fh.Result);
                                        fh.Dispose(); fh = null;
                                    }
                                    else
                                    {
                                        HS_Audio_Lib.Sound snd = null;
                                        HS_Audio_Lib.RESULT res = Helper.system.createStream(FilePath[i], HS_Audio_Lib.MODE.UNICODE, ref snd);
                                        if (snd == null) throw new SoundSystemException(Helper.Result);
                                        else snd.release();
                                    }
                                }

                                //timer1.Start();
                                list.Add(new ListViewItem((listView1.Items.Count + list.Count).ToString()));
                                if (listView1.Items.Count == 0 && list.Count < 2)
                                {
                                    this.Helper = new HS_Audio.HSAudioHelper(getSettingDeviceFormat());
                                    if (!PreLoading) Helper.setSoundDevice(0);
                                    Helper.PreLoading = 프리로드모드ToolStripMenuItem == null ? true : 프리로드모드ToolStripMenuItem.Checked;
                                    Helper.PlayingStatusChanged += new HS_Audio.HSAudioHelper.PlayingStatusChangedEventHandler(fh_PlayingStatusChanged);
                                    Helper.PlayingStatusChanged += new HSAudioHelper.PlayingStatusChangedEventHandler(lrcf.PlayingStatusChanged);
                                    Helper.MusicChanging += new HSAudioHelper.MusicChangeEventHandler(fh_MusicPathChanged);
                                    Helper.PreLoading = PreLoadingCheck;
                                    Helper.MusicPath = FilePath[i];
                                    trackBar1.Enabled = true;
                                    //ps = new PlayerSetting(fh, System.IO.Path.GetFileName(FilePath[i]), this);
                                    //fm.Add(fh);
                                    Helper.index = listView1.Items.Count;
                                    CurrentItem = list[0];
                                    CurrentItem.SubItems.Add(string.Format("{0} ({1})", LocalString.현재, Status_NoMixing.ToString()));
                                    CurrentItem.SubItems.Add(System.IO.Path.GetFileName(FilePath[i]));
                                    CurrentItem.SubItems.Add(FilePath[i]);
                                    CurrentItem.SubItems.Add("");
                                    //if (ps == null) { ps = new PlayerSetting(fh, System.IO.Path.GetFileName(fh.MusicPath)); }
                                    if (ps != null) ps.Title = string.Format("{0}", System.IO.Path.GetFileName(Helper.MusicPath));
                                    btn재생.Enabled = btn정지.Enabled = btn재생설정.Enabled = true;
                                }
                                else
                                {
                                    list[i].SubItems.Add("");
                                    list[i].SubItems.Add(System.IO.Path.GetFileName(FilePath[i]));
                                    list[i].SubItems.Add(FilePath[i]);
                                    list[i].SubItems.Add("");
                                    list[i].BackColor = Color.Transparent; list[i].ForeColor = listView1.ForeColor;
                                }
                            }
                            catch (Exception ex) { list.RemoveAt(list.Count - 1); sb.AppendLine(FilePath[i]); HS_CSharpUtility.LoggingUtility.LoggingT(ex); Error = true; }
                            if (asyncToolStripMenuItem.Checked && !Error) listView1.Refresh();
                        }
                        ListViewItem tmp = null; try { Point pt = (Point)point; tmp = listView1.GetItemAt(pt.X, pt.Y); }
                        catch { }
                        if (list.Count > 0)
                        {
                            if (tmp != null)
                            {
                                for (int i = 0; i < list.Count; i++)
                                { listView1.Items.Insert(tmp.Index + i, list[i]); open.Add(tmp.Index + i); g++; }
                            }
                            else { foreach (ListViewItem li in list) {li.Font = CustomFont; listView1.Items.Add(li); open.Add(listView1.Items.Count - 1); g++; } }
                            CurrentItem = _CurrentItem;
                        }
                    }
                    finally
                    {
                        tmpStop = false;
                        toolStripProgressBar1.Value = 0;
                        toolStripProgressBar1.Visible = toolStripStatusLabel2.Visible = false;
                        상태표시줄업데이트ToolStripMenuItem_Click(null, null);
                        try { if (PlaylistChanged != null) PlaylistChanged(this, PlayListChange.Add, listView1.Items.Count - 1); } catch { }
                        if (sb.Length > 0) {MessageBox.Show(LocalString.파일열기ToolStripMenuItem_Message + sb.ToString(), LocalString.파일열기ToolStripMenuItem_MessageTitle); }
                    }
                }
                else { textBox1.Text = openFileDialog1.FileName; }
            }
            파일연갯수 = g;
            e.Result = 파일연갯수;
        }
        private void Worker_파일열기_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            int i = 0;
            try
            {
                string FilePath = e.UserState.ToString();
                i = e.ProgressPercentage;
                if (안전모드ToolStripMenuItem.Checked)
                {
                    if (IsDebugging)
                    {
                        this.Text = string.Format("{0} ({1}, {2}) [{3}{4}... {5}: {6} / {7}: {8}]", ProgramName, LocalString.디버깅, LocalString.안전모드, (i + 1).ToString(), LocalString.파일여는중,
                            LocalString.이름, System.IO.Path.GetFileName(FilePath), LocalString.경로, FilePath);
                        toolStripStatusLabel1.Text = string.Format("{0}{1}... ({2}, {3}) [{4}: {5} / {6}: {7}]", (i + 1).ToString(), LocalString.파일여는중,
                            LocalString.디버깅, LocalString.안전모드, LocalString.이름, System.IO.Path.GetFileName(FilePath), LocalString.경로, FilePath);

                    }
                    else
                    {
                        this.Text = string.Format("{0} ({1}) [{2}{3}... {4}: {5} / {6}: {7}]", ProgramName, LocalString.안전모드, (i + 1).ToString(), LocalString.파일여는중,
                            LocalString.이름, System.IO.Path.GetFileName(FilePath), LocalString.경로, FilePath);
                        toolStripStatusLabel1.Text = string.Format("{0}{1}... ({2}) [{3}: {4} / {5}: {6}]", (i + 1).ToString(), LocalString.파일여는중,
                            LocalString.안전모드, LocalString.이름, System.IO.Path.GetFileName(FilePath), LocalString.경로, FilePath);
                    }
                }
                else
                {
                    if (IsDebugging)
                    {
                        this.Text = string.Format("{0} ({1}) [{2}{3}... {4}: {5} / {6}: {7}]", ProgramName, LocalString.디버깅, (i + 1).ToString(), LocalString.파일여는중,
                            LocalString.이름, System.IO.Path.GetFileName(FilePath), LocalString.경로, FilePath);
                        toolStripStatusLabel1.Text = string.Format("{0}{1}... ({2}) [{3}: {4} / {5}: {6}]", (i + 1).ToString(), LocalString.파일여는중,
                            LocalString.디버깅, LocalString.이름, System.IO.Path.GetFileName(FilePath), LocalString.경로, FilePath);
                    }
                    else
                    {
                        this.Text = string.Format("{0} [{1}{2}... {3}: {4} / {5}: {6}]", ProgramName, (i + 1).ToString(), LocalString.파일여는중,
                            LocalString.이름, System.IO.Path.GetFileName(FilePath), LocalString.경로, FilePath);
                        toolStripStatusLabel1.Text = string.Format("{0}{1}... [{2}: {3} / {4}: {5}]", (i + 1).ToString(), LocalString.파일여는중,
                            LocalString.이름, System.IO.Path.GetFileName(FilePath), LocalString.경로, FilePath);
                    }
                }
            }
            catch { }
            try { toolStripProgressBar1.Value = i > toolStripProgressBar1.Value ? toolStripProgressBar1.Maximum : i; }
            catch { toolStripProgressBar1.Value = toolStripProgressBar1.Maximum; }
        }
        private void Worker_파일열기_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            tmpStop = false;
            toolStripProgressBar1.Value = 0;
            toolStripProgressBar1.Visible = toolStripStatusLabel2.Visible = false;
            상태표시줄업데이트ToolStripMenuItem_Click(null, null);
        }

        private void Worker_목록열기_DoWork(object sender, DoWorkEventArgs e)
        {
            if (e == null) { e.Result = true; return; }
            목록열기Struct asd = new 목록열기Struct();
            try { asd = (목록열기Struct)e.Argument; }
            catch { return; }
            string PlaylistPath = asd.PlaylistPath;
            object point = asd.point;

            if (PlaylistPath != null && PlaylistPath != "")
            {
                List<int> open = new List<int>();
                Encoding ec = GetFileEncoding(PlaylistPath, false);
                if (ec == null) goto Exit;
                StreamReader sr = new StreamReader(PlaylistPath, ec);
                string a = sr.ReadToEnd(); sr.Close();//System.IO.File.ReadAllText(PlaylistPath,ec);


                if (안전모드ToolStripMenuItem.Checked)
                {
                    if (IsDebugging) { this.Text = string.Format("{0} ({1}, {2}) [{3}... {4}]", ProgramName, LocalString.디버깅, LocalString.안전모드, LocalString.재생목록여는중, "{" + LocalString.인코딩 + ": " + ec.ToString() + "}"); }
                    else { this.Text = string.Format("{0} ({1}) [{2}... {3}]", ProgramName, LocalString.안전모드, LocalString.재생목록여는중, "{" + LocalString.인코딩 + ": " + ec.ToString() + "}"); }
                }
                else
                {
                    if (IsDebugging) { this.Text = string.Format("{0} ({1}) [{2}... {3}]", ProgramName, LocalString.디버깅, LocalString.재생목록여는중, "{" + LocalString.인코딩 + ": " + ec.ToString() + "}"); }
                    else { this.Text = string.Format("{0} [{1}... {2}]", ProgramName, LocalString.재생목록여는중, "{" + LocalString.인코딩 + ": " + ec.ToString() + "}"); }
                }


                if (안전모드ToolStripMenuItem.Checked)
                {
                    if (IsDebugging) { toolStripStatusLabel1.Text = string.Format("{0}... ({1}, {2}) {3}", LocalString.재생목록여는중, LocalString.디버깅, LocalString.안전모드, "{" + LocalString.인코딩 + ":" + ec.ToString() + "}"); }
                    else { toolStripStatusLabel1.Text = string.Format("{0}... ({1}) {2}", LocalString.재생목록여는중, LocalString.안전모드, "{" + LocalString.인코딩 + ": " + ec.ToString() + "}"); }
                }
                else
                {
                    if (IsDebugging) { toolStripStatusLabel1.Text = string.Format("{0}... ({1}) {2}", LocalString.재생목록여는중, LocalString.디버깅, "{" + LocalString.인코딩 + ": " + ec.ToString() + "}"); }
                    else { toolStripStatusLabel1.Text = toolStripStatusLabel1.Text = string.Format("{0}... {1}", LocalString.재생목록여는중, "{" + LocalString.인코딩 + ": " + ec.ToString() + "}"); ; }
                }

                /*if(ANISToolStripMenuItem.Checked){System.IO.File.ReadAllText(PlaylistPath, Encoding.Default);}
                if (UTF8ToolStripMenuItem.Checked) { System.IO.File.ReadAllText(PlaylistPath, Encoding.UTF8); }
                if (유니코드ToolStripMenuItem.Checked) { System.IO.File.ReadAllText(PlaylistPath, Encoding.Unicode); }*/

                //if (encodingtest_3(a, Encoding.Default) != null) { a = encodingtest_3(a, Encoding.UTF8); }
                string[] b = HS_CSharpUtility.Utility.StringUtility.ConvertStringToArray(a, null);
                toolStripProgressBar1.Maximum = b.Length; toolStripProgressBar1.Visible = toolStripStatusLabel2.Visible = true;
                //List<string> h = b.ToList();
                if (asyncToolStripMenuItem.Checked) Application.DoEvents();
                int i1 = 0;

                StringBuilder ErrFile = new StringBuilder();
                bool Async = true; try { Async = (bool)sender; }
                catch { }
                if (!음악믹싱모드ToolStripMenuItem.Checked)
                {
                    if (true)//listView1.Items.Count == 0
                    {
                    //if (fm.Count != 0) { fm[0].Dispose(); }
                    again:
                        try
                        {
                            for (; i1 < b.Length; )
                            {
                                if (IsValidMusicFile(b[i1]))
                                {
                                    try
                                    {
                                        if (Helper == null)
                                        {
                                            //fm.Add(new HS_Audio_Helper.HSAudioHelper());
                                            this.Helper = new HS_Audio.HSAudioHelper(getSettingDeviceFormat());
                                            Helper.PlayingStatusChanged += new HS_Audio.HSAudioHelper.PlayingStatusChangedEventHandler(fh_PlayingStatusChanged);
                                            Helper.MusicChanging += new HSAudioHelper.MusicChangeEventHandler(fh_MusicPathChanged);
                                        PreLoadingCheck:
                                            Helper.PreLoading = PreLoadingCheck;
                                            if (!PreLoadingCheck) Helper.setSoundDevice(0);
                                            Helper.MusicPath = b[i1];
                                            btn재생.Enabled = btn정지.Enabled = btn재생설정.Enabled = true;
                                            trackBar1.Enabled = true;
                                        }
                                    }
                                    catch (SoundSystemException ex)
                                    {
                                        HS_CSharpUtility.LoggingUtility.LoggingT(ex);
                                        if (ex.Result == HS_Audio_Lib.RESULT.ERR_MEMORY)
                                        {
                                            DialogResult dr1 = MessageBox.Show("재생목록을 여는중 내부에서 메모리 오류가 발생했습니다.\n\n" +
                                            "프로그램을 다시 시작하여야만 합니다.\n\n다시 시작 하시겠습니까?", "프로그램 다시 시작 알림", MessageBoxButtons.YesNo, MessageBoxIcon.Stop);
                                            if (dr1 == System.Windows.Forms.DialogResult.Yes) { Application.Restart(); }
                                        }
                                        else { throw new SoundSystemException(); }
                                    }
                                    catch (Exception ex)
                                    {
                                        MessageBox.Show("재생목록 " + i1 + "번째 파일을 여는 중 알 수 없는 오류발생!\n로그를 확인해 주세요.", "오류 발생", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                        HS_CSharpUtility.LoggingUtility.LoggingT(ex);
                                    }
                                    if (asyncToolStripMenuItem.Checked) Application.DoEvents();
                                    break;
                                }
                                else { if (b[i1].ToLower() != "#extm3u")ErrFile.AppendLine("    " + b[i1]); throw new SoundSystemException(); }
                            }
                        }
                        catch { i1++; goto again; }
                    }
                }
                List<ListViewItem> list = new List<ListViewItem>();
                int listtmp;
                int ErrorCount = 0;
                for (int i = i1; i < b.Length; i++)
                {
                    if (BreakLoop) { BreakLoop = true; break; }
                    #region File Exists
                    if (IsValidMusicFile(b[i]))
                    {
                        if (Async) Worker_목록열기.ReportProgress(i, b[i]);
                        else { Worker_목록열기_ProgressChanged(null, new ProgressChangedEventArgs(i, b[i])); }
                        try
                        {
                            #region 음악믹싱모드 Checked
                            if (음악믹싱모드ToolStripMenuItem.Checked)
                            {
                                HS_Audio.HSAudioHelper fh;
                                try { fh = new HS_Audio.HSAudioHelper(b[i], getSettingDeviceFormat()); fh.PreLoading = PreLoading;}
                                catch (SoundSystemException ex)
                                {
                                    if (ex.Result == HS_Audio_Lib.RESULT.ERR_MEMORY)
                                        MessageBox.Show("더 이상 목록을 추가할 수 없습니다.\n\n다른 항목을 제거하고 다시 시도하세요!", "목록 더이상 추가 불가 알림", MessageBoxButtons.OK, MessageBoxIcon.Error); break;
                                }

                                if (asyncToolStripMenuItem.Checked) Application.DoEvents();
                                fh.PlayingStatusChanged += new HS_Audio.HSAudioHelper.PlayingStatusChangedEventHandler(fm_PlayingStatusChanged);
                                fh.MusicChanging += new HSAudioHelper.MusicChangeEventHandler(fh_MusicPathChanged);
                                if (fh.HelperEx.Result == HS_Audio_Lib.RESULT.OK ||
                                    fh.HelperEx.Result == HS_Audio_Lib.RESULT.ERR_REVERB_INSTANCE ||
                                    fh.HelperEx.Result == HS_Audio_Lib.RESULT.ERR_NEEDS3D)
                                {
                                    fh.Loop = true;
                                    toolStripProgressBar1.Value = toolStripProgressBar1.Value + 1;
                                    fm.Add(fh);
                                    frmdsp1.Add(new HS_Audio.Forms.frmDSP(fh, false));
                                    if (!timer1.Enabled) { timer1.Start(); }
                                    fh.index = fm.Count - 1;
                                    ListViewItem li = new ListViewItem(fh.index.ToString()) { BackColor = Color.Transparent, ForeColor = listView1.ForeColor };
                                    li.SubItems.Add(fh.PlayStatus.ToString());
                                    li.SubItems.Add(System.IO.Path.GetFileName(b[i]));
                                    li.SubItems.Add(b[i]);

                                    listView1.Items.Add(li);
                                    open.Add(listView1.Items.Count - 1);
                                    ps1.Add(new PlayerSetting(fh, System.IO.Path.GetFileName(fh.MusicPath)));
                                }
                                else { toolStripProgressBar1.Maximum = toolStripProgressBar1.Maximum - 1; }
                            }
                            #endregion
                            else
                            {
                                if (i1 > 0) listtmp = i - 1; else listtmp = i;
                                listtmp = listtmp - ErrorCount;
                                list.Add(new ListViewItem(listtmp.ToString()));

                                if (i1 == i && listView1.Items.Count == 0) {  CurrentItem = list[listtmp];list[listtmp].SubItems.Add(LocalString.현재 + " (Ready)"); /*i = 1;*/  }
                                else { list[listtmp].SubItems.Add(""); }
                                list[listtmp].SubItems.Add(System.IO.Path.GetFileName(b[i]));
                                list[listtmp].SubItems.Add(b[i]);
                                list[listtmp].SubItems.Add("");
                                list[listtmp].BackColor = Color.Transparent; list[listtmp].ForeColor = listView1.ForeColor;
                                //Init_Sort(false);
                            }

                        }
                        catch { ErrFile.AppendLine("    " + b[i]); }
                    }
                    #endregion
                    else
                    {
                        toolStripProgressBar1.Maximum = toolStripProgressBar1.Maximum - 1; ErrorCount++;
                        if (b[i].ToLower() != "#extm3u") ErrFile.AppendLine("    " + b[i]);
                    }
                }
                /*
                int tmp = -1; try { tmp = (int)point; }catch { }
                if (list.Count > 0)
                {
                    if (tmp > -1)
                    {
                        for (int i = 0; i < list.Count; i++)
                        { listView1.Items.Insert(tmp + i, list[i]); }
                    }
                    else { foreach (ListViewItem li in list) { listView1.Items.Add(li); } }
                }*/
                ListViewItem tmp = null;
                try { if (point != null && listView1 != null) { Point pt = (Point)point; tmp = listView1.GetItemAt(pt.X, pt.Y); } }catch { }
                if (list.Count > 0)
                {
                    if (tmp != null)
                    {
                        for (int i = 0; i < list.Count; i++)
                        { listView1.Items.Insert(tmp.Index + i, list[i]); open.Add(tmp.Index + i); }
                    }
                    else { foreach (ListViewItem li in list) { li.Font = CustomFont; listView1.Items.Add(li); open.Add(listView1.Items.Count - 1); } }
                    CurrentItem = _CurrentItem;
                }
                if (ErrFile.Length > 0)
                {
                    MessageBox.Show(LocalString.목록열기_Message + ErrFile.ToString(), LocalString.목록열기_MessageTitle + Path.GetFileName(PlaylistPath) + ")");
                }
                Init_Sort(MixingMode);
                toolStripProgressBar1.Value = 0;

            Exit:
                try { if (PlaylistChanged != null) PlaylistChanged(this, PlayListChange.Add, open.ToArray()); } catch { }
                if (안전모드ToolStripMenuItem.Checked)
                {
                    //조장찡 플레이어 (디버깅, 안전모드); 준비 (디버깅, 안전모드)
                    if (IsDebugging)
                    {
                        this.Text = string.Format("{0} ({1}, {2})", ProgramName, LocalString.디버깅, LocalString.안전모드);
                        toolStripStatusLabel1.Text = string.Format("{0} ({1}, {2})", LocalString.준비, LocalString.디버깅, LocalString.안전모드);
                    }//조장찡 플레이어 (안전모드); 준비 (안전모드)
                    else { this.Text = string.Format("{0} ({1})", ProgramName, LocalString.안전모드); toolStripStatusLabel1.Text = string.Format("{0} ({1})", LocalString.준비, LocalString.안전모드); }
                }
                else
                {
                    //조장찡 플레이어 (디버깅); 준비 (안전모드)
                    if (IsDebugging)
                    {
                        this.Text = string.Format("{0} ({1})", ProgramName, LocalString.디버깅);
                        toolStripStatusLabel1.Text = string.Format("{0} ({1})", LocalString.준비, LocalString.디버깅);
                    }
                    //조장찡 플레이어; 준비
                    else { this.Text = ProgramName; toolStripStatusLabel1.Text = LocalString.준비; }
                }
                toolStripProgressBar1.Visible = toolStripStatusLabel2.Visible = toolStripStatusLabel3.Visible = false; toolStripProgressBar1.Maximum = 0;
                상태표시줄업데이트ToolStripMenuItem_Click(null, null);
            }
            e.Result = true;
        }
        private void Worker_목록열기_ProgressChanged(object sender, ProgressChangedEventArgs e) { Worker_파일열기_ProgressChanged(sender, e); }
        private void Worker_목록열기_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e) { Worker_파일열기_RunWorkerCompleted(sender, e); }

        private void Worker_폴더열기_DoWork(object sender, DoWorkEventArgs e)
        {
            if (e == null) { e.Result = true; return; }
            폴더열기Struct asd = new 폴더열기Struct();
            try { asd = (폴더열기Struct)e.Argument; }catch { return; }
            string FolderPath = asd.FolderPath;
            object point = asd.point;
            if (FolderPath != null && FolderPath != "")
            {
                List<int> open = new List<int>();
                tmpStop = true;
                string[] path = new string[0];
                //if (asd.IsIncludeSubDirectory ? asd.IncludeSubDirectory : IncludeSubDirectory) { try { path = Directory.GetFiles(FolderPath, "*.*", SearchOption.AllDirectories); } catch { } }
                if (asd.IncludeSubDirectory) { try { path = Directory.GetFiles(FolderPath, "*.*", SearchOption.AllDirectories); } catch { } }
                else { path = Directory.GetFiles(FolderPath); }
                path = FilterFilePath(path, Filters[cbMimetype.SelectedIndex < 0 ? 0 : cbMimetype.SelectedIndex]);

                StringBuilder sb = new StringBuilder();
                List<ListViewItem> list = new List<ListViewItem>();
                bool Async = true; try { Async = (bool)sender; }catch { }
                if (!음악믹싱모드ToolStripMenuItem.Checked)
                {
                    try
                    {
                        toolStripProgressBar1.Maximum = path.Length;
                        toolStripProgressBar1.Visible = toolStripStatusLabel2.Visible = true;
                        int i = 0;
                        foreach (string p in path)
                        {
                            if (p == "" || p == null) goto Here;
                            bool Error = false;
                            try
                            {
                                if (Async) Worker_폴더열기.ReportProgress(i, p);
                                else { Worker_폴더열기_ProgressChanged(null, new ProgressChangedEventArgs(i, p)); }
                                if (안전모드ToolStripMenuItem.Checked)
                                {
                                    toolStripProgressBar1.Visible =
                                    toolStripStatusLabel2.Visible = true;
                                    if (BreakLoop) { break; }
                                    HS_Audio.HSAudioHelper fh = new HS_Audio.HSAudioHelper(); fh.PreLoading = false; fh.MusicPath = p;
                                    if (fh.Result == HS_Audio_Lib.RESULT.ERR_FORMAT ||
                                       fh.Result.ToString().IndexOf("ERR_FILE_") != -1)
                                    { throw new Exception(); }
                                    fh.Dispose();
                                }
                                //timer1.Start();
                                list.Add(new ListViewItem((listView1.Items.Count + list.Count).ToString()));
                                if (listView1.Items.Count == 0 && list.Count < 2)
                                {
                                    this.Helper = new HS_Audio.HSAudioHelper(getSettingDeviceFormat()); 
                                    Helper.PlayingStatusChanged += new HS_Audio.HSAudioHelper.PlayingStatusChangedEventHandler(fh_PlayingStatusChanged);
                                    Helper.PlayingStatusChanged += new HSAudioHelper.PlayingStatusChangedEventHandler(lrcf.PlayingStatusChanged);
                                    Helper.MusicChanging += new HSAudioHelper.MusicChangeEventHandler(fh_MusicPathChanged);
                                 PreLoading:
                                    if (!PreLoadingCheck) Helper.setSoundDevice(0);
                                    Helper.PreLoading = PreLoadingCheck;
                                    Helper.MusicPath = p;
                                    ps = new PlayerSetting(Helper, System.IO.Path.GetFileName(p), this);
                                    //fm.Add(fh);
                                    Helper.index = listView1.Items.Count;
                                    CurrentItem = list[0];
                                    CurrentItem.SubItems.Add(string.Format("{0} ({1})", LocalString.현재, Status_NoMixing.ToString()));
                                    CurrentItem.SubItems.Add(System.IO.Path.GetFileName(p));
                                    CurrentItem.SubItems.Add(p);
                                    CurrentItem.SubItems.Add("");
                                    if (ps == null) { ps = new PlayerSetting(Helper, System.IO.Path.GetFileName(Helper.MusicPath)); }
                                    ps.Title = string.Format("{0}", System.IO.Path.GetFileName(Helper.MusicPath));
                                    btn재생.Enabled = btn정지.Enabled = btn재생설정.Enabled = true;
                                    trackBar1.Enabled = true;
                                }
                                else
                                {
                                    list[list.Count - 1].SubItems.Add("");
                                    list[list.Count - 1].SubItems.Add(System.IO.Path.GetFileName(p));
                                    list[list.Count - 1].SubItems.Add(p);
                                    list[list.Count - 1].SubItems.Add("");
                                }
                            }
                            catch (Exception ex) { list.RemoveAt(list.Count - 1); sb.AppendLine(p); HS_CSharpUtility.LoggingUtility.LoggingT(ex); Error = true; }
                            if (asyncToolStripMenuItem.Checked && !Error) Application.DoEvents();
                        Here:
                            i++;
                        }
                        /*
                        int tmp = -1; try { tmp = (int)point; }catch { }
                        if (list.Count > 0)
                        {
                            if (tmp > -1)
                            {
                                for (int i1 = 0; i1 < list.Count; i1++)
                                { listView1.Items.Insert(tmp + i1, list[i1]); }
                            }
                            else { foreach (ListViewItem li in list) { listView1.Items.Add(li); } }
                        }*/
                        ListViewItem tmp = null; try { Point pt = (Point)point; tmp = listView1.GetItemAt(pt.X, pt.Y); }
                        catch { }
                        if (list.Count > 0)
                        {
                            if (tmp != null)
                            {
                                for (int i1 = 0; i1 < list.Count; i1++)
                                { listView1.Items.Insert(tmp.Index + i1, list[i1]); open.Add(tmp.Index + i1); }
                            }
                            else { for (int i1 = 0; i1 < list.Count; i1++) { list[i].Font = CustomFont; list[i].BackColor = Color.Transparent; list[i].ForeColor = listView1.ForeColor; listView1.Items.Add(list[i]); open.Add(listView1.Items.Count - 1); } }
                            CurrentItem = _CurrentItem;
                        }
                        else
                        {
                            if (asd.ShowMessage)
                            {
                                int IIDX = FolderPath.LastIndexOf("\\") + 1;
                                MessageBox.Show(LocalString.폴더열기_Message.Replace("*%1*", FolderPath.Substring(IIDX < 0 ? 0 : IIDX)), LocalString.폴더열기_MessageTitle, MessageBoxButtons.OK, MessageBoxIcon.Information);
                                폴더열기MessageBoxShow = true;
                            }
                        }
                    }
                    finally
                    {
                        toolStripProgressBar1.Value = 0;
                        toolStripProgressBar1.Visible = toolStripStatusLabel2.Visible = false;
                        tmpStop = false;
                        상태표시줄업데이트ToolStripMenuItem_Click(null, null);
                        if (sb.Length > 0) { MessageBox.Show(LocalString.파일열기ToolStripMenuItem_Message + sb.ToString(), LocalString.파일열기ToolStripMenuItem_MessageTitle); }

                        StringBuilder sb1 = new StringBuilder();
                        sb1.Append(EventKind.PlayListChanged.ToString()).Append("\n");
                        sb1.Append("Mode=").Append(Command_PlayListMode.Add.ToString()).Append("\n");
                        info.SetEvent(sb1.ToString(), open.ToArray());
                    }
                }
                else { textBox1.Text = HS_CSharpUtility.Utility.StringUtility.ConvertArrayToString(path, true); }
            }
        }
        private void Worker_폴더열기_ProgressChanged(object sender, ProgressChangedEventArgs e) { Worker_파일열기_ProgressChanged(sender, e); }
        private void Worker_폴더열기_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e) { Worker_파일열기_RunWorkerCompleted(sender, e); }

        #endregion


        int listView1_MouseMove_List,
            listView1_MouseMove_List_Last;
        bool DragDropMove,
             MouseClick;

        void HighLighting(int X, int Y)
        {
            if (MouseClick || DragDropMove)
            {
                try
                {
                    if (this.listView1.GetItemAt(X, Y) != null)
                    {
                        listView1_MouseMove_List = this.listView1.GetItemAt(X, Y).Index;
                        if (listView1_MouseMove_List.Equals(listView1_MouseMove_List_Last))
                        {
                            HSConsole.Write(listView1.Items[listView1_MouseMove_List].Text);
                            listView1.Items[listView1_MouseMove_List].BackColor = colorDialog2.Color;
                            listView1.Items[listView1_MouseMove_List].EnsureVisible();

                        }
                        else { listView1.Items[listView1_MouseMove_List_Last].BackColor = listView1.BackColor; listView1.Items[listView1_MouseMove_List_Last].ForeColor = listView1.ForeColor; SearchPlayList(false); }
                        listView1_MouseMove_List_Last = listView1_MouseMove_List;
                    }
                }
                catch (Exception ex) { HSConsole.Write(ex.ToString() + "\n\n"); }
                DragDropMove = false;
            }
        }
        void HighLighting(Point pt)
        {
            if (MouseClick || DragDropMove)
            {
                try
                {
                    if (this.listView1.GetItemAt(pt.X, pt.Y) != null)
                    {
                        listView1_MouseMove_List = this.listView1.GetItemAt(pt.X, pt.Y).Index;
                        if (listView1_MouseMove_List.Equals(listView1_MouseMove_List_Last))
                        {
                            HSConsole.Write(listView1.Items[listView1_MouseMove_List].Text);
                            listView1.Items[listView1_MouseMove_List].BackColor = colorDialog2.Color;
                            listView1.Items[listView1_MouseMove_List].EnsureVisible();

                        }
                        else { listView1.Items[listView1_MouseMove_List_Last].BackColor = listView1.BackColor; listView1.Items[listView1_MouseMove_List_Last].ForeColor = listView1.ForeColor; SearchPlayList(false); }
                        listView1_MouseMove_List_Last = listView1_MouseMove_List;
                    }
                }
                catch (Exception ex) { HSConsole.Write(ex.ToString() + "\n\n"); }
                DragDropMove = false;
            }
        }

        public bool FileMatch(byte[] File1, byte[] File2)
        {
            for (long i = 0; i < File1.LongLength; i++)
            { if (File1[i] != File2[i])return false; }
            return true;
        }
        System.Diagnostics.ProcessStartInfo psi = new ProcessStartInfo(System.Diagnostics.Process.GetCurrentProcess().MainModule.FileName);//new ProcessStartInfo("HongSic_RegisterFileAssosication.exe");
        //readonly string FileREGISter = Application.StartupPath + "\\HongSic_RegisterFileAssosication.exe";
        private void 기본프로그램으로설정ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {/*
                if (!File.Exists(FileREGISter))
                    File.WriteAllBytes(FileREGISter, HS_Audio.Properties.Resources.RegisterFileAssosication_exe);
                else
                {
                    if (!FileMatch(File.ReadAllBytes(FileREGISter), HS_Audio.Properties.Resources.RegisterFileAssosication_exe))
                        File.WriteAllBytes(FileREGISter, HS_Audio.Properties.Resources.RegisterFileAssosication_exe);
                }*/

                psi.Arguments = string.Format("-addExt \"{0}\" \"{1}\" \"{2}\" \"\"", Application.ExecutablePath, "HS 플레이어 재생목록에 추가(&E)", LocalString.ProgramName);
                if (IsRunAsAdmin()) Process.Start(psi);
                else
                {
                    if (!Elevate(psi.Arguments))
                        MessageBox.Show(LocalString.기본프로그램으로설정ToolStripMenuItem_Message, LocalString.ProgramName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(LocalString.기본프로그램으로설정ToolStripMenuItem_Message, LocalString.ProgramName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                HS_CSharpUtility.LoggingUtility.Logging(ex);
            }
        }
        #region Windows Vista이상 UAC관련
        internal static bool IsRunAsAdmin()
        {
            var Principle = new System.Security.Principal.WindowsPrincipal(System.Security.Principal.WindowsIdentity.GetCurrent());
            return Principle.IsInRole(System.Security.Principal.WindowsBuiltInRole.Administrator);
        }

        private static bool Elevate(string Arguments)
        {
            var SelfProc = new ProcessStartInfo
            {
                UseShellExecute = true,
                WorkingDirectory = Environment.CurrentDirectory,
                FileName = Application.ExecutablePath,
                Verb = "runas",
                Arguments = Arguments
            };
            try
            {
                Process.Start(SelfProc);
                return true;
            }
            catch {return false;}
        }
        #endregion


        SharedMemory sm_check = new SharedMemory();
        SharedMemory sm = new SharedMemory();


        #region 파일 추가 스레드&대리자
        string SharedMemory_String;
        //string SharedMemory_Check_String;
        internal void SharedMemory_Thread()
        {
            while (false)//true
            {
                timer_파일추가_Tick(null, null);
                sm.ReadString(ref SharedMemory_String);
                //MessageBox.Show(SharedMemory_String);
                if (SharedMemory_String != null && SharedMemory_String != "" && SharedMemory_String.IndexOf("\0") > 5)
                {
                    sm.Clear();
                    try
                    {
                        List<string> sb = new List<string>();
                        string[] a = SharedMemory_String.Split(new string[] { "\r\n" }, StringSplitOptions.RemoveEmptyEntries);
                        for (int i = 0; i < a.Length; i++)
                        {
                            if (a[i].IndexOf("\0") == -1)
                            {
                                if (Directory.Exists(a[i])) { 폴더열기(null, a[i]); }
                                else if (Path.GetExtension(a[i]) == ".m3u" || Path.GetExtension(a[i]) == ".m3u8")
                                { 목록열기(null, a[i]); }
                                else { if (IsValidMusicFile(a[i]))sb.Add(a[i]); }
                            }
                        }
                        if (sb.Count > 0) 파일열기(null, sb.ToArray());
                    }
                    catch (Exception ex) { try { HS_CSharpUtility.LoggingUtility.Logging(ex); } catch { } }
                }
                SharedMemory_String = null;
                Thread.Sleep(1000);
            }
        }
        MyTaskProgressChangedEventArgs eArgs;
        private delegate void MyTaskWorkerDelegate(object point, string Playlist, bool Dir, AsyncOperation async);
        public event AsyncCompletedEventHandler MyTaskCompleted;
        public class MyTaskProgressChangedEventArgs : EventArgs
        {
            public object point { get; private set; }
            public string file { get; private set; }
            public bool Dir { get; private set; }
            public string[] Playlist { get; private set; }

            public MyTaskProgressChangedEventArgs(object point, string file, bool Dir)
                : base()
            { this.point = point; this.file = file; this.Dir = Dir; this.Playlist = Playlist; }
            public MyTaskProgressChangedEventArgs(object point, string[] Playlist)
                : base()
            { this.point = point; this.Playlist = Playlist; }
        }

        public event EventHandler<MyTaskProgressChangedEventArgs> MyTaskProgressChanged;
        protected virtual void OnMyTaskProgressChanged(MyTaskProgressChangedEventArgs e)
        {
            if (MyTaskProgressChanged != null) MyTaskProgressChanged(this, e);
        }

        private void MyTaskWorker(object point, string Playlist, bool Dir, AsyncOperation async)
        {

        }
        #endregion

        private void 하위폴더검색ToolStripMenuItem_CheckedChanged(object sender, EventArgs e) { IncludeSubDirectory = 하위폴더검색ToolStripMenuItem.Checked; }


        ulong sm_check_int = 3;
        string CHK = null;
        List<string> sm_sb = new List<string>();
        bool 폴더열기MessageBoxShow = true;
        bool Add;
        private void timer_파일추가_Tick(object sender, EventArgs e)
        {
            //MessageBox.Show(SharedMemory_String);
            if (sm_check_int == 2 && sm_sb.Count>0)
            {
                try
                {
                    bool IsFile = true;
                    int off = 0;
                    //MessageBox.Show(sm_sb[0]);
                    if (sm_sb[0] == "/add")off = 1;
                    for (int i = off; i < sm_sb.Count; i++)
                    {
                        if (Directory.Exists(sm_sb[i])){if(폴더열기MessageBoxShow)폴더열기(null, sm_sb[i], 하위폴더검색ToolStripMenuItem.Checked, asyncToolStripMenuItem.Checked, false, null);Add = true;sm_sb.Remove(sm_sb[i]);IsFile = false;}
                        else if (Path.GetExtension(sm_sb[i]) == ".m3u" || Path.GetExtension(sm_sb[i]) == ".m3u8") {목록열기(null, sm_sb[i]); IsFile = false;}
                        else {파일열기(null, new string[]{sm_sb[i]}, false); IsFile = true;}
                    }
                    if (!Add && sm_sb.Count<2)
                    {
                        try
                        {
                            if (off == 0)
                            {
                                int gg = IsFile ? listView1.Items.Count - 파일연갯수 : 파일연갯수;
                                if (IsValidMusicFile(listView1.Items[gg].SubItems[3].Text))
                                    재생ToolStripMenuItem_Click(listView1.Items[gg], null);
                            }
                        }catch{}
                        //listView1.Items[listView1.Items.Count - 파일연갯수].Selected = true;
                        
                    }
                } finally { sm_sb.Clear();Add=false; }
            }
            
            if (/*sm_check.ReadByte(1) == 1 &&*/ sm_check.ReadByte(0)==1)//SharedMemory_String != null && SharedMemory_String != "" && SharedMemory_String.IndexOf("\0") > 5)
            {
                sm.ReadString(ref SharedMemory_String);
                //CHK = SharedMemory_String;
                sm.Clear();
                try
                {
                    string[] a = SharedMemory_String.Split(new string[] { "\r\n" }, StringSplitOptions.RemoveEmptyEntries);

                    for (int i = 0; i < a.Length; i++)
                    {
                        if (a[i].IndexOf("\0") == -1)
                        {
                            if (IsValidMusicFile(a[i], true) || Directory.Exists(a[i])) { sm_sb.Add(a[i]); sm_check_int = 0; }
                            if (!Add) Add = a[i].ToLower() == "/add";
                            /*
                            if (Directory.Exists(a[i]))
                            {
                                eArgs = new MyTaskProgressChangedEventArgs(null, a[i], true);
                            }
                            else if (Path.GetExtension(a[i]) == ".m3u" || Path.GetExtension(a[i]) == ".m3u8")
                            {
                                eArgs = new MyTaskProgressChangedEventArgs(null, a[i], false);
                            }
                            else { if (File.Exists(a[i]))sb.Add(a[i]); }*/
                        }
                    }
                    //if (sm_sb.Count > 0&&sm_check_int>1) { 파일열기(null, sm_sb.ToArray(), false); listView1.Items[listView1.Items.Count - 파일연갯수].Selected = true; 재생ToolStripMenuItem_Click(null, null); }
                }
                catch (Exception ex) { try { HS_CSharpUtility.LoggingUtility.Logging(ex); } catch { } }
                finally {  }
            }
            SharedMemory_String = null;
            sm_check.WriteByte(0, 0);
            if (sm_check_int > 1000) sm_check_int = 3; else sm_check_int++;
        }

        private void 크리스마스1ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CurrentImageforPlaylist=HS_Audio.Properties.Resources.BG_xmas;
            listView1_SizeChanged(null, null);
            UpdateSetting();
        }

        private void 일러스트1ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CurrentImageforPlaylist = HS_Audio.Properties.Resources.BG_konosuba_kazuma;
            listView1_SizeChanged(null, null);
            UpdateSetting();
        }

        private void 일러스트2ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CurrentImageforPlaylist = HS_Audio.Properties.Resources.BG_konosuba_meguminn;
            listView1_SizeChanged(null, null);
            UpdateSetting();
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {

        }

        long SearchCount = 0;
        private void btnSearchPlayList_Click(object sender, EventArgs e)
        {
            SearchPlayList();
        }
        public void SearchPlayList(bool Focusing = true)
        {
            if (listView1.Items.Count > 0)
            {
                SearchCount = 0;
                foreach (ListViewItem li in listView1.Items)
                {
                    try
                    {
                        foreach (ListViewItem.ListViewSubItem li1 in li.SubItems)
                        {
                            if (li1.Text.ToLower().Contains(textBox3.Text.ToLower()) && textBox3.Text != "") { SearchCount++; li.BackColor = frmEqualizerGraph.CalculateNegativeColor(li.BackColor); 
                            li.ForeColor = frmEqualizerGraph.CalculateNegativeColor(listView1.ForeColor); 
                                if (Focusing)li.EnsureVisible(); break; }
                            else { li.BackColor = listView1.BackColor; li.ForeColor = listView1.ForeColor; }
                        }
                    }
                    catch { }
                }
                label1.Text = string.Format("{0} {1}", SearchCount.ToString(), LocalString.개);
                listView1.ForeColor = cdLabelText.Color;
            }
            
        }

        private void textBox3_KeyPress(object sender, KeyPressEventArgs e)
        {
            //엔터키가 눌릴때
            if (e.KeyChar == '\r') { SearchPlayList(); }
        }

        private void 글자색상설정ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (cdLabelText.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                listView1.ForeColor = cdLabelText.Color;
                SearchPlayList(false);
                UpdateSetting();
            }
        }

        bool 플러그인PToolStripMenuItemOnce;
        private void 플러그인PToolStripMenuItem_DropDownOpening(object sender, EventArgs e)
        {
            if (!플러그인PToolStripMenuItemOnce||Plugins.Count==0) {
            if (!Directory.Exists(Application.StartupPath + "\\Plugins")) try{Directory.CreateDirectory(Application.StartupPath + "\\Plugins");}
                                                                          catch(Exception ex){ex.Logging(); MessageBox.Show("플러그인 폴더를 만드는중 오류 발생!\n\n자세한건 로그를 확인해 주세요.");return;}
            ICollection<IHSAudioPlugin> plugins = null;
            try { plugins = HSPluginLoader<IHSAudioPlugin>.LoadPlugins(Application.StartupPath + "\\Plugins"); } catch (Exception ex) { ex.Logging(); }
            if (plugins == null){플러그인이존재하지않습니다ToolStripMenuItem.Text = "(플러그인 로드 중 오류 발생!)";return;}
            foreach (var item in plugins)Plugins.Add(item);
                /*
            string path = Application.StartupPath;
            if (!Directory.Exists(path + "\\Plugins")) { Directory.CreateDirectory(path + "\\Plugins"); }
            if (!File.Exists(path + "\\Plugins\\HS 플레이어.dll")) { File.Copy(Application.ExecutablePath, path + "\\Plugins\\HS 플레이어.dll"); }
            string[] pluginFiles = Directory.GetFiles(path + "\\Plugins", "*.dll");
            Plugins = new IHSAudioPlugin[pluginFiles.Length];

            for (int i = 0; i < pluginFiles.Length; i++)
            {
                string args = pluginFiles[i].Substring(
                    pluginFiles[i].LastIndexOf("\\") + 1,
                    pluginFiles[i].IndexOf(".dll") -
                    pluginFiles[i].LastIndexOf("\\") - 1);

                Type ObjType = null;
                //IPlugin ipi;
                // load the dll
                try
                {
                    // load it
                    Assembly ass = null;
                    //ass = Assembly.Load(Application.StartupPath+"\\Plugins\\"+args+".dll");
                    ass = Assembly.LoadFrom(pluginFiles[i]);
                    if (ass != null)
                    {
                        ObjType = ass.GetType(args + ".HSAudioPlugin");
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
                try
                {
                    // OK Lets create the object as we have the Report Type
                    if (ObjType != null)
                    {
                        object o = Activator.CreateInstance(ObjType, true);
                        Plugins[i] = o as IHSAudioPlugin;
                        Plugins[i].Host = this;
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                } 

                //				Start(assemname);  
            }*/
            PluginInit();

            if (Plugins.Count > 0)
            {
                플러그인PToolStripMenuItem.DropDownItems.Clear();
                foreach (IHSAudioPlugin a in Plugins)Register(a);
            }
            //MessageBox.Show(ConfigurationSettings.AppSettings["App"]);
            플러그인PToolStripMenuItemOnce = true;}
            
            RegisterInstance();CheckEnablePlugin();
        }

        private void 선택한항목강조표시ToolStripMenuItem_CheckedChanged(object sender, EventArgs e)
        {
            listView1.HideSelection = !선택한항목강조표시ToolStripMenuItem.Checked;
            UpdateSetting();
        }

        private void 다음ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            NextMusic();
        }

        private void toolStripProgressBar2_MouseLeave(object sender, EventArgs e)
        {
            toolStripProgressBar2MouseEnter = false;
            this.Cursor = System.Windows.Forms.Cursors.Default;
            timer2_Tick(null, null);
        }

        bool toolStripProgressBar2MouseEnter;
        private void toolStripProgressBar2_MouseEnter(object sender, EventArgs e)
        {
            toolStripProgressBar2MouseEnter = true;
            this.Cursor = AndroidCursor;
            
        }

        private double SetProgressBarValue(double MousePosition)
        {
            //this.toolStripProgressBar2.Value = this.toolStripProgressBar2.Minimum;
            double ratio = MousePosition / this.toolStripProgressBar2.Width;
            double ProgressBarValue = ratio * this.toolStripProgressBar2.Maximum;
            return ProgressBarValue;
        }

        private void toolStripProgressBar2_MouseDown(object sender, MouseEventArgs e)
        {
            double MousePosition = e.Location.X;
            Helper.CurrentPosition = (uint)SetProgressBarValue(MousePosition);
        }

        private void toolStripProgressBar2_MouseMove(object sender, MouseEventArgs e)
        {
            double MousePosition = e.X;
            uint a = (uint)SetProgressBarValue(MousePosition);

            if (toolStripProgressBar2MouseEnter &&
                listView1.Items.Count > 0 && Helper != null &&
                Helper.TotalPosition > 0)
                toolStripStatusLabel1.Text = string.Format("{0} [{1} / {2}]", LocalString.이동, ShowTime(a, 밀리초표시ToolStripMenuItem.Checked), ShowTime(_TotalTick(), 밀리초표시ToolStripMenuItem.Checked));

            if (e.Button == MouseButtons.Left && toolStripProgressBar2MouseEnter) Helper.CurrentPosition = a;
        }

        internal void ShowCurrentTime(bool MilliSecond=true)
        {
            //Init_Sort(false);
            if (!tmpStop)
            {
                if (!Once) Once = MilliSecond; if (!음악믹싱모드ToolStripMenuItem.Checked) toolStripProgressBar2.Visible = MilliSecond;
                try { if (!음악믹싱모드ToolStripMenuItem.Checked)toolStripProgressBar2.Value = (int)CurrentTick().TotalMilliseconds; }
                catch { }

                toolStripProgressBar2.Visible = !(Status_NoMixing == HSAudioHelper.PlayingStatus.Ready || Status_NoMixing == HSAudioHelper.PlayingStatus.None);

                if (!toolStripProgressBar2MouseEnter)
                {
                    if (안전모드ToolStripMenuItem.Checked)
                    {
                        if (IsDebugging)
                        {
                            if (음악믹싱모드ToolStripMenuItem.Checked)
                            {
                                sb.Append(ProgramName); sb.Append(" ("); sb.Append(LocalString.디버깅); sb.Append(", "); sb.Append(LocalString.안전모드); sb.Append(")");
                                this.Text = sb.ToString(); sb.Remove(0, sb.Length);
                                sb.Append(LocalString.준비); sb.Append(" ("); sb.Append(LocalString.디버깅); sb.Append(", "); sb.Append(LocalString.안전모드); sb.Append(")");
                                //timer1_Tick_sb_String = (string)sb.GetType().GetField("m_StringValue", BindingFlags.NonPublic | BindingFlags.Instance).GetValue(sb);
                                toolStripStatusLabel1.Text = sb.ToString(); sb.Remove(0, sb.Length);
                            }
                            else
                            {
                                if (Status_NoMixing != HSAudioHelper.PlayingStatus.Ready)
                                {
                                    sb.Append(PlayStatustoString(Status_NoMixing)); sb.Append("("); sb.Append(LocalString.디버깅); sb.Append(", "); sb.Append(LocalString.안전모드); sb.Append(")");
                                    sb.Append("["); sb.Append(ShowTime(CurrentTick(), MilliSecond)); sb.Append(" / "); sb.Append(ShowTime(TotalTick(), MilliSecond)); sb.Append("]");
                                    //timer1_Tick_sb_String = (string)sb.GetType().GetField("m_StringValue", BindingFlags.NonPublic | BindingFlags.Instance).GetValue(sb);
                                    toolStripStatusLabel1.Text = sb.ToString(); sb.Remove(0, sb.Length);

                                }
                                else { toolStripStatusLabel1.Text = string.Format("{0} ({1}, {2})", LocalString.준비, LocalString.디버깅, LocalString.안전모드); }
                            }
                        }
                        else
                        {
                            if (음악믹싱모드ToolStripMenuItem.Checked)
                            {
                                this.Text = string.Format("{0} ({1})", ProgramName, LocalString.안전모드);
                                toolStripStatusLabel1.Text = string.Format("{0} ({1})", LocalString.준비, LocalString.안전모드);
                            }
                            else
                            {
                                if (Status_NoMixing != HSAudioHelper.PlayingStatus.Ready)
                                {
                                    toolStripStatusLabel1.Text = string.Format("{0} ({1}) [{2} / {3}]",
                                        PlayStatustoString(Status_NoMixing), LocalString.안전모드, ShowTime(_CurrentTick(), MilliSecond), ShowTime(_TotalTick(), MilliSecond));
                                }
                                else { toolStripStatusLabel1.Text = string.Format("{0} ({1})", LocalString.준비, LocalString.안전모드); }
                            }
                        }
                    }
                    else
                    {
                        if (IsDebugging)
                        {
                            if (음악믹싱모드ToolStripMenuItem.Checked)
                            {
                                this.Text = string.Format("{0} ({1})", ProgramName, LocalString.디버깅);
                                toolStripStatusLabel1.Text = string.Format("{0} ({1})", LocalString.준비, LocalString.디버깅);
                            }
                            else
                            {
                                if (Status_NoMixing != HSAudioHelper.PlayingStatus.Ready)
                                {
                                    toolStripStatusLabel1.Text = string.Format("{0} ({1}) [{2} / {3}]",
                                        PlayStatustoString(Status_NoMixing), LocalString.디버깅, ShowTime(_CurrentTick(), MilliSecond), ShowTime(_TotalTick(), MilliSecond));
                                }
                                else { toolStripStatusLabel1.Text = string.Format("{0} ({1})", LocalString.준비, LocalString.디버깅); }
                            }
                        }
                        else
                        {
                            if (음악믹싱모드ToolStripMenuItem.Checked)
                            {
                                this.Text = string.Format("{0} ({1})", ProgramName, LocalString.디버깅); toolStripStatusLabel1.Text = LocalString.준비;
                            }
                            else
                            {
                                if (Status_NoMixing != HSAudioHelper.PlayingStatus.Ready)
                                {
                                    /*
                                    toolStripStatusLabel1.Text = string.Format("{0} [{1}:{2}:{3}:{4} / {5}:{6}:{7}:{8}]",
                                        PlayStatustoString(Status_NoMixing), 
                                        CurrentTick(0).Hours.ToString("00"),CurrentTick(0).Minutes.ToString("00"), CurrentTick(0).Seconds.ToString("00"), CurrentTick(0).Milliseconds.ToString("000"),
                                        TotalTick(0).Hours.ToString("00"), TotalTick(0).Minutes.ToString("00"), TotalTick(0).Seconds.ToString("00"), TotalTick(0).Milliseconds.ToString("000"));*/
                                    sb.Append(PlayStatustoString(Status_NoMixing)); sb.Append(" ["); sb.Append(ShowTime(_CurrentTick(), MilliSecond)); sb.Append(" / "); sb.Append(ShowTime(_TotalTick(), MilliSecond)); sb.Append("]");
                                    //timer1_Tick_sb_String = (string)sb.GetType().GetField("m_StringValue", BindingFlags.NonPublic | BindingFlags.Instance).GetValue(sb);
                                    toolStripStatusLabel1.Text = sb.ToString(); sb.Remove(0, sb.Length);
                                }
                                else { toolStripStatusLabel1.Text = LocalString.준비; }
                            }
                        }
                    }
                }
            }
            else { toolStripProgressBar2.Visible = Once = false; }
        }

        private void 태그없는항목자동삽입ToolStripMenuItem_CheckedChanged(object sender, EventArgs e)
        {
            if (태그없는항목자동삽입ToolStripMenuItem.Checked)MessageBox.Show("다음번 재생부터 적용됩니다.");
        }

        private void frmMain_MinimumSizeChanged(object sender, EventArgs e)
        {

        }

        private void 밀리초표시ToolStripMenuItem_CheckedChanged(object sender, EventArgs e)
        {timer1.Interval = 밀리초표시ToolStripMenuItem.Checked ? 25 : 500;if(timer1.Enabled)timer1_Tick(null, null);statusStrip1_SizeChanged(null, null);}

        private void 현재상태저장ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string Now = DateTime.Now.ToKorean(true);

            saveFileDialog4.FileName = "HS 플레이어 실행 ("+Now+")";
            if(saveFileDialog4.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                Now = saveFileDialog4.FileName.Substring(saveFileDialog4.FileName.LastIndexOf("\\") + 1);
                Now = Now.Remove(Now.LastIndexOf("."));
                Process p = System.Diagnostics.Process.GetCurrentProcess();
                StringBuilder sb = new StringBuilder("#EXTM3U");
                foreach (ListViewItem a in listView1.Items) { sb.Append("\r\n" + a.SubItems[3].Text); }

                if (!Directory.Exists(Application.StartupPath.Replace("/", "\\") + "\\Playlists\\Temp")) Directory.CreateDirectory(Application.StartupPath.Replace("/", "\\") + "\\Playlists\\Temp");

                string PlayListPath = Application.StartupPath.Replace("/","\\") + "\\Playlists\\Temp\\"+ Now+ ".m3u";
                //try { File.Create(path + ".m3u").Close();}catch{}
                try { System.IO.File.WriteAllText(PlayListPath, sb.ToString()); }catch{}

                if (!Directory.Exists(Application.StartupPath.Replace("/", "\\") + "\\Project\\Temp")) Directory.CreateDirectory(Application.StartupPath.Replace("/", "\\") + "\\Project\\Temp");
                string ProfilePath = Application.StartupPath.Replace("/", "\\") + "\\Project\\Temp\\"+Now+".proj";
                if (ps!=null) try { File.WriteAllLines(ProfilePath, HS_CSharpUtility.Utility.EtcUtility.SaveSetting(ps.Setting)); }catch{}

                string Args=string.Format("@echo off\r\n@title {0}\r\n", LocalString.ProgramName);
                try { Args += string.Format("\"{0}\" /CommandLineStart {1} {2} \"{3}\" -index {4} -Position {5} -Project \"{6}\"", 
                    p.MainModule.FileName,
                    NewInstance ? "/NewInstance" : "", 
                    Helper.PlayStatus == HSAudioHelper.PlayingStatus.Play ? "/Play" : "", 
                    PlayListPath,
                    CurrentItem != null ? CurrentItem.Index : 0, Helper.CurrentPosition, 
                    ProfilePath, 
                    p.StartInfo.Arguments); }catch { }
                Args += "\r\n@echo 이제 이 창을 닫아도 됩니다. (3초후 자동으로 닫힙니다.)\r\nping -n 3 localhost > nul";
                File.WriteAllText(saveFileDialog4.FileName,Args, Encoding.Default);
            }
        }

        private void frmMain_KeyPress(object sender, KeyPressEventArgs e)
        {

        }

        frmHotKey hotkey = new frmHotKey();
        private void 핫키설정ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try{kh.UnHook();}catch{}
            hotkey.Show();
            hotkey.BringToFront();
        }
        GlobalKeyboardHook gkh;
        KeyboardHookEx kh;
        private void hotkey_ApplyButtonPressed(frmHotKey sender)
        {
            string a;
            List<string> a1;
            List<Keys> b;
            if(sender.GlobalKey)
            {
                /*
                gkh = new GlobalKeyboardHook();
                //gkh.HookedKeys.RemoveRange(0,5);
                gkh.HookedKeys.Add((Keys)sender.PlayKey);
                gkh.HookedKeys.Add((Keys)sender.PreviousKey);
                gkh.HookedKeys.Add((Keys)sender.NextKey);
                gkh.HookedKeys.Add((Keys)sender.VolumeUpKey);
                gkh.HookedKeys.Add((Keys)sender.VolumeDownKey);
                gkh.KeyDown+=new KeyEventHandler(GlobalKeyboardHook_KeyDown);*/
                try{if(kh!=null){kh.Dispose();}}
                finally{kh = new KeyboardHookEx();}
                kh.KeyPressed += new EventHandler<KeyPressedEventArgs>(GlobalKeyboardHook_KeyPress);//new KeyPressedEventHandler(GlobalKeyboardHook_KeyPress);
                

                #region Play버튼
                a = ((Keys)sender.PlayKey).ToString();
                a1 = new List<string>(a.Split(','));
                b = new List<Keys>();
                foreach(string c in a1)b.Add((Keys)Enum.Parse(typeof(Keys), c));
                if (b.Count > 1)
                {
                    a1.RemoveAt(0);
                    LIBRARY.Native.ModifierKeys k=(ModifierKeys)Enum.Parse(typeof(ModifierKeys), a1[0]);
                    a1.RemoveAt(0);
                    foreach(string d in a1)k|=(ModifierKeys)Enum.Parse(typeof(ModifierKeys), d);
                    kh.RegisterHotKey(k,b[0]);
                }
                else kh.RegisterHotKey(b[0]);
                #endregion

                #region 이전 버튼
                if (sender.NextKey != -1)
                {
                    a = ((Keys)sender.PreviousKey).ToString();
                    a1 = new List<string>(a.Split(','));
                    b = new List<Keys>();
                    foreach (string c in a1) b.Add((Keys)Enum.Parse(typeof(Keys), c));
                    if (b.Count > 1)
                    {
                        a1.RemoveAt(0);
                        LIBRARY.Native.ModifierKeys k = (ModifierKeys)Enum.Parse(typeof(ModifierKeys), a1[0]);
                        a1.RemoveAt(0);
                        foreach (string d in a1) k |= (ModifierKeys)Enum.Parse(typeof(ModifierKeys), d);
                        kh.RegisterHotKey(k, b[0]);
                    }
                    else kh.RegisterHotKey(b[0]);
                }
                #endregion

                #region 다음 버튼
                if (sender.NextKey != -1)
                {
                    a = ((Keys)sender.NextKey).ToString();
                    a1 = new List<string>(a.Split(','));
                    b = new List<Keys>();
                    foreach (string c in a1) b.Add((Keys)Enum.Parse(typeof(Keys), c));
                    if (b.Count > 1)
                    {
                        a1.RemoveAt(0);
                        LIBRARY.Native.ModifierKeys k = (ModifierKeys)Enum.Parse(typeof(ModifierKeys), a1[0]);
                        a1.RemoveAt(0);
                        foreach (string d in a1) k |= (ModifierKeys)Enum.Parse(typeof(ModifierKeys), d);
                        kh.RegisterHotKey(k, b[0]);
                    }
                    else kh.RegisterHotKey(b[0]);
                }
                #endregion

                #region 볼륨 업 버튼
                if (sender.VolumeUpKey != -1)
                {
                    a = ((Keys)sender.VolumeUpKey).ToString();
                    a1 = new List<string>(a.Split(','));
                    b = new List<Keys>();
                    foreach (string c in a1) b.Add((Keys)Enum.Parse(typeof(Keys), c));
                    if (b.Count > 1)
                    {
                        a1.RemoveAt(0);
                        LIBRARY.Native.ModifierKeys k = (ModifierKeys)Enum.Parse(typeof(ModifierKeys), a1[0]);
                        a1.RemoveAt(0);
                        foreach (string d in a1) k |= (ModifierKeys)Enum.Parse(typeof(ModifierKeys), d);
                        kh.RegisterHotKey(k, b[0]);
                    }
                    else kh.RegisterHotKey(b[0]);
                }
                #endregion

                #region 볼륨 다운 버튼
                if (sender.VolumeDownKey != -1)
                {
                    a = ((Keys)sender.VolumeDownKey).ToString();
                    a1 = new List<string>(a.Split(','));
                    b = new List<Keys>();
                    foreach (string c in a1) b.Add((Keys)Enum.Parse(typeof(Keys), c));
                    if (b.Count > 1)
                    {
                        a1.RemoveAt(0);
                        LIBRARY.Native.ModifierKeys k = (ModifierKeys)Enum.Parse(typeof(ModifierKeys), a1[0]);
                        a1.RemoveAt(0);
                        foreach (string d in a1) k |= (ModifierKeys)Enum.Parse(typeof(ModifierKeys), d);
                        kh.RegisterHotKey(k, b[0]);
                    }
                    else kh.RegisterHotKey(b[0]);
                }
                #endregion

                this.KeyPreview=false;
            }
            else {this.KeyPreview=true; if(kh!=null) kh.Dispose();kh = null;/*gkh.unhook();gkh.HookedKeys.Clear();gkh = null;*/}
        }
        private void GlobalKeyboardHook_KeyPress(object sender, KeyPressedEventArgs e)
        {
            //
            string a;
            List<string> a1;
            string b;
            List<string> b1;

            //이전 키
            if ((int)e.Key == hotkey.PreviousKey) ;

            #region 재생 키
            //재생 
            a = e.Key.ToString();
            b= ((Keys)hotkey.PlayKey).ToString();
            a1 = new List<string>(e.Modifier.ToString().Replace(" ","").Split(','));
            b1 = new List<string>(b.Replace(" ","").Split(',',' '));
            if (b1[0] == e.Key.ToString())
            {
                if (b1.Count > 1)
                {
                    int j=0;
                    for (int i = 0; i < a1.Count; i++)if(HS_CSharpUtility.Utility.ArrayUtility.IsArrayItem(b1.ToArray(),a1[i]))j++;
                    if(j==a1.Count)button1_Click(null, null);
                }
                else button1_Click(null, null);
            }
            #endregion

            #region 이전 키
            //이전 키
            a = e.Key.ToString();
            b= ((Keys)hotkey.PreviousKey).ToString();
            a1 = new List<string>(e.Modifier.ToString().Replace(" ","").Split(','));
            b1 = new List<string>(b.Replace(" ","").Split(',',' '));
            if (b1[0] == e.Key.ToString())
            {
                if (b1.Count > 1)
                {
                    int j=0;
                    for (int i = 0; i < a1.Count; i++)if(HS_CSharpUtility.Utility.ArrayUtility.IsArrayItem(b1.ToArray(),a1[i]))j++;
                    if (j == a1.Count) BeforeMusic();//button1_Click(null, null);
                }
                else BeforeMusic();// button1_Click(null, null);
            }
            #endregion

            #region 다음 키
            //다음 키
            a = e.Key.ToString();
            b= ((Keys)hotkey.NextKey).ToString();
            a1 = new List<string>(e.Modifier.ToString().Replace(" ","").Split(','));
            b1 = new List<string>(b.Replace(" ","").Split(',',' '));
            if (b1[0] == e.Key.ToString())
            {
                if (b1.Count > 1)
                {
                    int j=0;
                    for (int i = 0; i < a1.Count; i++)if(HS_CSharpUtility.Utility.ArrayUtility.IsArrayItem(b1.ToArray(),a1[i]))j++;
                    if(j==a1.Count)다음ToolStripMenuItem_Click(null, null);
                }
                else 다음ToolStripMenuItem_Click(null, null);
            }
            #endregion

            #region 볼륨 업 키
            //재생 키
            a = e.Key.ToString();
            b= ((Keys)hotkey.VolumeUpKey).ToString();
            a1 = new List<string>(e.Modifier.ToString().Replace(" ","").Split(','));
            b1 = new List<string>(b.Replace(" ","").Split(',',' '));
            if (b1[0] == e.Key.ToString())
            {
                if (b1.Count > 1)
                {
                    int j=0;
                    for (int i = 0; i < a1.Count; i++)if(HS_CSharpUtility.Utility.ArrayUtility.IsArrayItem(b1.ToArray(),a1[i]))j++;
                    if(j==a1.Count)                    
                    {
                        //if (fh != null) ps = new PlayerSetting(fh);
                        try{if (Helper != null) Helper.Volume = Helper.Volume + 0.01f; }
                        catch{System.Media.SystemSounds.Beep.Play();}
                    }
                }
                else 
                {
                     //if (fh != null) ps = new PlayerSetting(fh);
                     try{if (Helper != null) Helper.Volume = Helper.Volume + 0.01f; }
                     catch{System.Media.SystemSounds.Beep.Play();}
                }
            }
            #endregion

            #region 볼륨 다운 키
            //재생 키
            a = e.Key.ToString();
            b= ((Keys)hotkey.VolumeDownKey).ToString();
            a1 = new List<string>(e.Modifier.ToString().Replace(" ","").Split(','));
            b1 = new List<string>(b.Replace(" ","").Split(',',' '));
            if (b1[0] == e.Key.ToString())
            {
                if (b1.Count > 1)
                {
                    int j=0;
                    for (int i = 0; i < a1.Count; i++)if(HS_CSharpUtility.Utility.ArrayUtility.IsArrayItem(b1.ToArray(),a1[i]))j++;
                    if (j == a1.Count)
                    {
                        //if (fh != null) ps = new PlayerSetting(fh);
                        try{if (Helper != null) Helper.Volume = Helper.Volume - 0.01f; }
                        catch{System.Media.SystemSounds.Beep.Play();}
                    }
                }
                else
                {
                    //if (fh != null) ps = new PlayerSetting(fh);
                    try{if (Helper != null) Helper.Volume = Helper.Volume - 0.01f; }
                    catch{System.Media.SystemSounds.Beep.Play();}
                }
            }
            #endregion
            //e.Handled=hotkey.KeyHandle;
        }
        private void frmMain_KeyDown(object sender, KeyEventArgs e)
        { 
            //이전 키
            if ((int)e.KeyData == hotkey.PreviousKey) BeforeMusic();
            //다음 키
            if ((int)e.KeyData == hotkey.NextKey) 다음ToolStripMenuItem_Click(null, null);
            //재생 키
            if ((int)e.KeyData ==hotkey.PlayKey) button1_Click(null, null);
            //볼륨 업 키
            if ((int)e.KeyData == hotkey.VolumeUpKey)
            {
                //if (fh != null) ps = new PlayerSetting(fh);
                try{if(Helper!=null)Helper.Volume = Helper.Volume + 0.01f;}
                catch{System.Media.SystemSounds.Beep.Play();}
            }
            //볼륨 다운 키
            if ((int)e.KeyData == hotkey.VolumeDownKey)
            {
                //if (fh != null) ps = new PlayerSetting(fh);
                try{if(Helper!=null)Helper.Volume = Helper.Volume - 0.01f;}
                catch{System.Media.SystemSounds.Beep.Play();}
            }
            if (Helper != null && e.KeyData == Keys.Right && Helper.CurrentPosition < Helper.TotalPosition)
            {
                uint a = Helper.CurrentPosition + 1000;
                Helper.CurrentPosition = a >= Helper.TotalPosition ? Helper.TotalPosition : a;
            }
            if (Helper != null && e.KeyData == Keys.Left && Helper.CurrentPosition > 0)
            {
                uint a = Helper.CurrentPosition - 1000;
                Helper.CurrentPosition = a <= 0 ? 0 : a;
            }
            e.Handled=hotkey.KeyHandle;
        }

        private void 화면끄기ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Utility.Win32Utility.MonitorControl(this.Handle, Utility.Win32Utility.MonitorEnum.MONITOR_OFF);
        }

        private void 자동스케일ToolStripMenuItem_CheckedChanged(object sender, EventArgs e)
        {
            if (!LockAutoScaleMode) toolStripComboBox5_SelectedIndexChanged(null, null);
        }

        private void toolStripComboBox5_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!LockAutoScaleMode)
            {

                System.Windows.Forms.AutoScaleMode scale = System.Windows.Forms.AutoScaleMode.Font;
                SizeF scaleF = this.AutoScaleDimensions;
                try
                {
                    자동스케일ToolStripMenuItem.Enabled = true;
                    bool AutoScalemode=자동스케일ToolStripMenuItem.Checked;
                    //try{scale = (System.Windows.Forms.AutoScaleMode)Enum.Parse(typeof(System.Windows.Forms.AutoScaleMode), Setting["ScaleMode"]);}catch{}

                    //try{scaleF = HS_CSharpUtility.Utility.ConvertUtility.StringToSizeF(a["ScaleDemension"]);}catch{}

                    switch (toolStripComboBox5.SelectedIndex)
                    {
                        case 0:

                            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
                            if (AutoScalemode) scaleF = new SizeF(System.Drawing.SystemFonts.DefaultFont.SizeInPoints, System.Drawing.SystemFonts.DefaultFont.Height);
                            else try{scaleF = HS_CSharpUtility.Utility.ConvertUtility.StringToSizeF(Setting["ScaleDemension"]);}catch{}
                            this.AutoScaleDimensions = scaleF;
                            break;
                        case 1:
                            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
                            float DPI = getScalingFactor();
                            if (AutoScalemode) scaleF = new SizeF(DPI==0?96:DPI*100, DPI==0?96:DPI*100);
                            else try{scaleF = HS_CSharpUtility.Utility.ConvertUtility.StringToSizeF(Setting["ScaleDemension"]);}catch{}
                            this.AutoScaleDimensions = scaleF;
                            break;
                        default:
                            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None; 
                            try{scaleF = HS_CSharpUtility.Utility.ConvertUtility.StringToSizeF(Setting["ScaleDemension"]);}catch{}
                            this.Scale(scaleF);
                            자동스케일ToolStripMenuItem.Enabled = false;
                            break;
                    }
                }
                catch{ }
                finally
                {
                    UpdateSetting();
                    MessageBox.Show("만약 변화가 없으면 플레이어를 재시작 해주세요!\r재시작 방법: '[설정(T)]->[플레이어 다시 시작]' 클릭 또는 상단의 '새로고침' 모양의 버튼을 누름", "HS 플레이어 스케일 조정", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
        }

        bool statusStrip1_SizeChangedFirst = true;
        private void statusStrip1_SizeChanged(object sender, EventArgs e)
        {
            if (statusStrip1_SizeChangedFirst) statusStrip1_SizeChangedFirst = false;
            else
            {
                toolStripProgressBar2.Size = new Size(statusStrip1.Size.Width - toolStripStatusLabel1.Size.Width, statusStrip1.Size.Height-2);
                toolStripProgressBar2.Size = new Size(statusStrip1.Size.Width - toolStripProgressBar2.Control.Location.X - 2, toolStripProgressBar2.Size.Height);
            }
            //Application.DoEvents();
        }
        private void statusStrip1_LocationChanged(object sender, EventArgs e)
        {
            //statusStrip1_SizeChanged(null, null);
        }

        bool PreLoadingCheck = true;
        private void 프리로드모드ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //프리로드모드ToolStripMenuItem.Checked = !프리로드모드ToolStripMenuItem.Checked;
            PreLoadingCheck = 프리로드모드ToolStripMenuItem.Checked;
            UpdateSetting();
            //DialogResult dr = MessageBox.Show("변경사항은 프로그램을 재시작해야 적용됩니다.\n지금 다시 시작 하시겠습니까?", "HS 플레이어 음악 로딩 방식 변경",MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
            DialogResult dr = MessageBox.Show("변경사항은 다음번 재생부터 적용됩니다.", "HS 플레이어 음악 로딩 방식 변경",MessageBoxButtons.OK, MessageBoxIcon.Information);
            if(dr == System.Windows.Forms.DialogResult.Yes)프로그램다시시작ToolStripMenuItem_Click(null, null);
            //try {fh.PreLoading = 프리로드모드ToolStripMenuItem.Checked; }catch{}
            //try{for (int i = 0; i < fm.Count; i++) fm[i].PreLoading = 프리로드모드ToolStripMenuItem.Checked;}catch{}
        }

        private void 프리로드모드ToolStripMenuItem_CheckedChanged(object sender, EventArgs e)
        {
            프리로드모드ToolStripMenuItem.Text = 프리로드모드ToolStripMenuItem.Checked ?
                "프리 로드 모드 (로딩 느림, 태그 변경 가능)" :
                "프리 로드 모드 (로딩 빠름, 태그 변경 불가능)";
            //PreLoadingCheck = 프리로드모드ToolStripMenuItem.Checked;
        }

        private void listView1_KeyDown(object sender, KeyEventArgs e)
        {
            if (Helper != null && e.KeyData == Keys.Right && Helper.CurrentPosition <Helper.TotalPosition)
            {
                uint a = Helper.CurrentPosition + 1000;
                Helper.CurrentPosition = a >= Helper.TotalPosition ? Helper.TotalPosition : a;
            }
            if (Helper != null && e.KeyData == Keys.Left && Helper.CurrentPosition > 0)
            {
                uint a = Helper.CurrentPosition - 1000;
                Helper.CurrentPosition = a <= 0 ? 0 : a;
            }
        }

        private void 이전ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            BeforeMusic();
        }

        private void cb반복_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (Helper != null)Helper.Loop = cb반복.SelectedIndex == 2;
        }

        bool timer1_Start;
        private void frmMain_WindowStateChanged(object sender, EventArgs e)
        {
            if (this.WindowShowStatus == HS_Audio.Control.WindowShowState.Show&&Environment.OSVersion.Version.Major >= 6 && Environment.OSVersion.Version.Minor >= 1){
            try { TaskBarExtensions.AddThumbnailBarButtons(this, this.thumbnailBarButtons); }catch{}}
            /*
            if(WindowState== FormWindowState.Minimized||this.WindowShowStatus == WindowShowState.Hide){timer1.Stop();}
            else {timer1_Tick(null, null); timer1.Start();}*/
        }

        Font CustomFont;
        private void 글꼴설정ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            fontDialog1.Font = CustomFont;
            if (fontDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                FontStyle fs = Enum.IsDefined(typeof(FontStyle), FontStyle.Bold) ?
                fontDialog1.Font.Style & ~FontStyle.Bold :
                fontDialog1.Font.Style;
                CustomFont = new System.Drawing.Font(fontDialog1.Font, fs);

                UpdateSetting();

                listView1.Font = fontDialog1.Font;
                for (int i = 0; i < listView1.Items.Count; i++) listView1.Items[i].Font = CustomFont;
                CurrentItem = _CurrentItem;
            }
            else
            {
                listView1.Font = CustomFont;
                for (int i = 0; i < listView1.Items.Count; i++) listView1.Items[i].Font = CustomFont;
                CurrentItem = _CurrentItem;
            }
        }

        private void fontDialog1_Apply(object sender, EventArgs e)
        {
            listView1.Font = fontDialog1.Font;

            FontStyle fs = Enum.IsDefined(typeof(FontStyle), FontStyle.Bold) ?
                fontDialog1.Font.Style & ~FontStyle.Bold :
                fontDialog1.Font.Style;
            Font ff = new System.Drawing.Font(fontDialog1.Font, fs);

            for (int i = 0; i < listView1.Items.Count; i++) listView1.Items[i].Font = ff;
            CurrentItem = _CurrentItem;
        }

        private void trackBar1_ValueChanged(object sender, EventArgs e)
        {
            label4.Text = trackBar1.Value.ToString();
        }

        bool Lock_Scroll = false;
        private void trackBar1_Scroll(object sender, EventArgs e)
        {
            Lock_Scroll = true;
            try { if (Helper != null) Helper.Volume = trackBar1.Value / 100f; } catch { }
            Lock_Scroll = false;
        }
        private void NoMixing_VolumeChanged(HSAudioHelper Helper, float Volume)
        {
            if(!Lock_Scroll) trackBar1.Value = (int)Math.Round(Helper.Volume * 100);//Math.Min(Helper.Volume * 100, 100)
        }

        int btnFnSwapCheck = 1;
        private void btnFnSwap_Click(object sender, EventArgs e)
        {
            switch (btnFnSwapCheck)
            {
                case 1:
                try
                {
                    btnFnSwap.Text = "<->\n\n[2]";
                    btnFnSwapCheck = 2;
                    pnlSwap1.Visible = false;
                    pnlSwap2.Visible = true;
                }catch{}
                break;
                case 2:
                    btnFnSwap.Text = "<->\n\n[3]";
                    btnFnSwapCheck = 3;
                    pnlSwap2.Visible = false;
                    pnlSwap3.Visible = true;
                    btnFnSwap_Click(null, null);
                break;
                case 3:
                    btnFnSwap.Text = "<->\n\n[4]";
                    btnFnSwapCheck = 4;
                    pnlSwap3.Visible = false;
                    pnlSwap4.Visible = true;
                break;
                default:
                    btnFnSwap.Text = "<->\n\n[1]";
                    btnFnSwapCheck = 1;
                    pnlSwap4.Visible = false;
                    pnlSwap1.Visible = true;
                break;
            }
        }

        private void 운영체제테마적용ToolStripMenuItem_CheckedChanged(object sender, EventArgs e)
        {
            listView1.ApplyWindowTheme = 운영체제테마적용ToolStripMenuItem.Checked;
            선택한항목강조표시ToolStripMenuItem.Enabled = !운영체제테마적용ToolStripMenuItem.Checked;
        }

        private void 재생목록창꾸미기ToolStripMenuItem_DropDownOpening(object sender, EventArgs e)
        {
            운영체제테마적용ToolStripMenuItem.Enabled = HS_CSharpUtility.Utility.IsWindowsXP_Higher;
        }

        private void btn재생설정_EnabledChanged(object sender, EventArgs e)
        {
            btn재생설정.BackgroundImage = btn재생설정.Enabled ? Properties.Resources.Setting : Properties.Resources.Setting.ToMonochromatic(true);
        }

        private void 설정ToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        int OriginalStyle = -1;
        private void 클릭통과ToolStripMenuItem_CheckedChanged(object sender, EventArgs e)
        {
            
            HS_Audio.LIBRARY.LayerWindow.SetClickThru(lrcf.Handle, (byte)(lrcf.Opacity * 255), ref OriginalStyle, 클릭통과ToolStripMenuItem.Checked);

            HS_Audio.LIBRARY.LayerWindow.SetClickThru(lrcf.playControl1.Handle, (byte)(lrcf.Opacity * 255), ref OriginalStyle, false);
            //HS_Audio.LIBRARY.LayerWindow.SetLayerWindow(this.lrcf.playControl1.Handle, false);
        }

        private void 바탕화면싱크가사창닫기ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            lrcf.Hide();
        }


        bool IsValidMusicFile(string Path, bool All = false, bool IncludeDirectory = false)
        {
            List<string> _Filter = new List<string>(Filters[0].Split(new string[] { ";", " " }, StringSplitOptions.RemoveEmptyEntries));
            _Filter.Add(".m3u"); _Filter.Add(".m3u8"); _Filter.Add(".fsb");
            for (int j = 0; j < _Filter.Count; j++)
            {
                if (System.IO.Path.GetExtension(Path) == _Filter[j].ToLower() || (IncludeDirectory && Directory.Exists(Path)))
                    return true;
            }
            return false;
        }
    }
}
