﻿using HS_Audio.Bundles;
using HS_Audio.LIBRARY;
using HS_Audio_Lib;
using HS_CSharpUtility.Extension;
using NAudio.CoreAudioApi;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

namespace HS_Audio
{
    public partial class PlayerSetting
    {
        bool First_cbOutputDevice_Change = true;
        SoundDevice LastDevice;
        MMDevice mmd;
        private void cbOutputDevice_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!ComboBoxLockAll)
            {
                try
                {
                    if (!First_cbOutputDevice_Change)
                    {
                        if (cbOutputDevice.SelectedIndex == 0)
                        {
                            //string msg = sender as string;
                            if (cbOutputDevice.Items.Count - 1 != Helper.getSoundDevices().Length && !ComboBoxLock) btnSoundDeviceRefresh_Click(null, null);
                            else
                            {
                                LastDevice = Helper.DefaultSoundDevice;
                                RESULT result = Helper.setSoundDevice(LastDevice);
                                lblFMODResult.Text = result.ToString();
                                if (result == RESULT.ERR_UNINITIALIZED) MessageBox.Show("사운드 장치를 변경하던 중 심각한 문제가 발생했습니다. \n이는 해당 사운드장치가 연결이 안되있거나 문제가 발생했음을 의미합니다. \n\n프로그램을 재 시작 해주시기 바랍니다.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
                            }
                        }
                        else
                        {
                            trackBar3.Value = 0; checkBox2.Checked = false; label12.Text = "N/A";
                            if (cbOutputDevice.Items.Count - 1 != Helper.getSoundDevices().Length && !ComboBoxLock) { try { btnSoundDeviceRefresh_Click(null, null); } catch { } }
                            LastDevice = (SoundDevice)cbOutputDevice.SelectedItem;
                            RESULT result = Helper.setSoundDevice((SoundDevice)cbOutputDevice.SelectedItem);
                            lblFMODResult.Text = result.ToString();
                            if (result == RESULT.ERR_UNINITIALIZED) MessageBox.Show("사운드 장치를 변경하던 중 심각한 문제가 발생했습니다. \n이는 해당 사운드장치가 연결이 안되있거나 문제가 발생했음을 의미합니다. \n\n프로그램을 재 시작 해주시기 바랍니다.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
                            /*
                            if (!((Device)cbOutputDevice.SelectedItem).Equals(LastDevice))
                            {

                            }*/
                        }
                    }
                }
                catch { }
            }
            try
            {
                if (WindowsXP_Higher)
                {
                    //if (WASAPIPeak_Thread != null) WASAPIPeak_Thread.Dispose();
                    WASAPIPeak_Thread.Stop();
                    this.mmd = null;
                    foreach (MMDevice mmd in mmdc)
                    {
                        int a = mmd.ID.LastIndexOf(".");
                        string a1 = mmd.ID.Substring(a + 1);
                        SoundDevice dd = Helper.SoundDevice;//cbOutputDevice.SelectedIndex > 0 ? (Device)cbOutputDevice.SelectedItem : Helper.DefaultSoundDevice;
                        if (a1.ToUpper() == dd.GUID.ToString(true).ToUpper())
                        {
                            if (this.mmd != null && this.mmd.AudioEndpointVolume != null) this.mmd.AudioEndpointVolume.OnVolumeNotification -= AudioEndpointVolume_OnVolumeNotification;
                            /*MessageBox.Show(mmd.DeviceFriendlyName);*/
                            this.mmd = mmd; mmd.AudioEndpointVolume.OnVolumeNotification += AudioEndpointVolume_OnVolumeNotification;
                            checkBox2.Checked = mmd.AudioEndpointVolume.Mute;
                            trackBar3.Value = (int)(10000 * (checkBox7.Checked ? mmd.AudioEndpointVolume.MasterVolumeLevelPercent : mmd.AudioEndpointVolume.MasterVolumeLevelScalar));
                            WASAPIPeak_Thread.Start();// = new System.Threading.Timer(new System.Threading.TimerCallback(WASAPIPeak_ThreadLoop)); break;
                        }
                    }
                }
                else
                {
                    /*
                    WindowsXPVolumeControl.Dispose();
                    GC.Collect();
                    WindowsXPVolumeControl = new LIBRARY.HSVolumeControl_WinXP(this.Handle,cbOutputDevice.SelectedIndex-1);
                    WindowsXPVolumeControl.VolumeChanged += WindowsXPVolumeControl_VolumeChanged;
                    WindowsXPVolumeControl.MuteChanged += WindowsXPVolumeControl_MuteChanged;*/
                    mi.DeviceId = LastDevice.ID;
                    WindowsXPLine = mi.Lines[0];
                    WindowsXPLine_MixerLineChanged(mi, WindowsXPLine);
                }
            }
            catch (Exception ex) { ComboBoxLockAll = false; }
            //else{if(DectectDefaultSoundDevice_Thread!=null)DectectDefaultSoundDevice_Thread.Abort(); System.Threading.ThreadPool.QueueUserWorkItem(new System.Threading.WaitCallback(DectectDefaultSoundDevice_ThreadLoop));}
            //First_cbOutputDevice_Change = false;

            //RESULT result = RESULT.OK;
            //label15.Text = string.Format("출력 타입: {0} (결과: {1})", Helper.getOutputType(out result), result);
        }

        System.Threading.Thread DectectDefaultSoundDevice_Thread;
        // Device LastDefaultDevice = new Device(0, "", new GUID());

        SoundDevice[] BeforeDevices, CurrentDevices;
        SoundDevice LastDeviceDefault;
        bool FirstLoop = true;
        NAudio.Wave.WaveOutCapabilities[] LastWaveOutCapabilities;
        int waveOutDevices = 0;
        Exception DectectDefaultSoundDevice_ThreadLoop_Ex;
        bool RunSoundDeviceDetect = true;
        void DectectDefaultSoundDevice_ThreadLoop(object o)
        {
            /*
            if (FirstLoop)
            {
                before = Helper != null ? Helper.getSoundDevice() : null;
                Before = Helper.DefaultSoundDevice;
                asdasd = null;
                FirstLoop = false;
            }*/

            Retry:
            try
            {
                while (Helper != null && (cbOutputDevice.SelectedIndex == 0 || RunSoundDeviceDetect))
                {
                    //MessageBox.Show(before.Name, Helper.DefaultSoundDevice.Name);
                    try
                    {
                        /*
                        RESULT[] results = null;
                        CurrentDevices = Helper.getSoundDevices(out results);
                        //만약 사운드장치를 못찾겠으면 0.5 초 대기후 다시시도.
                        if (BeforeDevices == null || BeforeDevices.Length < 0) { System.Threading.Thread.Sleep(1000); goto Retry; }

                        //만약 0번쨰 장치(기본장치)가 서로 틀리다면
                        if (!BeforeDevices[0].Equals(LastDeviceDefault) ||
                            //전의 장치갯수와 현재 장치갯수가 틀리다면
                            BeforeDevices.Length != CurrentDevices.Length ||
                            //현재 장치 목록하고 전의 장치목록이 틀리다면
                            !Device.MatchDeviceArray(BeforeDevices, CurrentDevices))
                        {
                            //디버그 메세지
                            //MessageBox.Show(string.Format("{0},{1},{2}\n\n{3} <-> {4}", BeforeDevices[0].Equals(LastDevice), BeforeDevices.Length == CurrentDevices.Length, Device.MatchDeviceArray(BeforeDevices, CurrentDevices), BeforeDevices[0], LastDevice));

                            //UI Thread 에서 장치 새로고침 버튼 누름
                            this.InvokeIfNeeded(() => { btnSoundDeviceRefresh_Click(null, null); });
                            //2.5(finally +0.5) 초동안 대기
                            System.Threading.Thread.Sleep(2500);
                        }*/

                        if (LastWaveOutCapabilities == null || waveOutDevices != NAudio.Wave.WaveOut.DeviceCount)
                            LastWaveOutCapabilities = new NAudio.Wave.WaveOutCapabilities[NAudio.Wave.WaveOut.DeviceCount];

                        DectectDefaultSoundDevice_Thread = System.Threading.Thread.CurrentThread;

                        bool Detect = false;
                        for (int i = 0; i < waveOutDevices; i++)
                        {
                            //Console.WriteLine("NAudio Device: {0}({1})\nLast Device: {2}({3}\n",NAudio.Wave.WaveOut.GetCapabilities(i).ProductGuid,NAudio.Wave.WaveOut.GetCapabilities(i).ProductName, LastWaveOutCapabilities[i].ProductGuid, LastWaveOutCapabilities[i].ProductName);
                            //FirstLoop
                            NAudio.Wave.WaveOutCapabilities wo = NAudio.Wave.WaveOut.GetCapabilities(i);

                            if (!LastWaveOutCapabilities[i].ProductGuid.Equals(new System.Guid()) && (
                                LastWaveOutCapabilities[i].ProductGuid.CompareTo(wo.ProductGuid) != 0 ||
                                LastWaveOutCapabilities[i].ManufacturerGuid.CompareTo(wo.ManufacturerGuid) != 0 ||
                                LastWaveOutCapabilities[i].ProductName != wo.ProductName ||
                                waveOutDevices != NAudio.Wave.WaveOut.DeviceCount) &&
                                !Detect)
                            {
                                Detect = true;
                                this.InvokeIfNeeded(() =>
                                {
                                    cbOutputDevice_SelectedIndexChanged(null, null);
                                    btnSoundDeviceRefresh_Click(null, null);
                                });
                                System.Threading.Thread.Sleep(500);
                            }
                            LastWaveOutCapabilities[i] = NAudio.Wave.WaveOut.GetCapabilities(i);
                        }
                        waveOutDevices = NAudio.Wave.WaveOut.DeviceCount;

                        /*
                        asdasd = Helper.getSoundDevice();
                        //Console.WriteLine(Before.Name + "," + Helper.DefaultSoundDevice.Name + "   " + asdasd.Length + "," + before.Length);

                        //MessageBox.Show(before.Length.ToString(), asd.Length.ToString());
                        if (Before.GUID.GetGuid != Helper.DefaultSoundDevice.GUID.GetGuid ||   before.Length != asdasd.Length)
                        {
                            before =asdasd;
                            Before = Helper.DefaultSoundDevice;

                            this.InvokeIfNeeded(()=>{btnSoundDeviceRefresh_Click(null, null);});
                            //btnSoundDeviceRefresh_Click_Safe();
                        }
                         }catch{/*mm = new MMDeviceEnumerator();*/
                    }
                    finally { System.Threading.Thread.Sleep(1000); }
                }
            }
            catch (System.Threading.ThreadAbortException) { }
            catch (Exception ex) { if (DectectDefaultSoundDevice_ThreadLoop_Ex == null && !ex.Equals(DectectDefaultSoundDevice_ThreadLoop_Ex)) ex.Logging(); }
            finally { System.Threading.Thread.Sleep(1000); }
        }

        bool ComboBoxLock = false;
        bool ComboBoxLockAll = false;
        private void btnSoundDeviceRefresh_Click(object sender, EventArgs e)
        {
            bool tmpRunSoundDeviceDetect = RunSoundDeviceDetect;
            RunSoundDeviceDetect = false; //사운드장치 감시 스레드 일시중지

            SoundDevice[] di = null;
            SoundDevice LastDevice = null;
            OUTPUTTYPE type = OUTPUTTYPE.UNKNOWN;
            try
            {
                RESULT result = RESULT.OK;
                type = Helper.getOutputType(out result);

                ComboBoxLockAll = true;
                ClearSoundDevices();

                if (type == OUTPUTTYPE.DSOUND || type == OUTPUTTYPE.WASAPI || type == OUTPUTTYPE.WINMM || type == OUTPUTTYPE.ASIO)
                {
                    //cbOutputDevice_SelectedIndexChanged(null, null);

                    di = Helper.getSoundDevices();
                    BeforeDevices = di;
                    if (di.Length == 0 || di == null)
                    {
                        RunSoundDeviceDetect = true;
                        cbOutputDevice.Items.Add("(사운드 장치가 없습니다!)");
                        cbOutputDevice.Text = cbOutputDevice.Items[0].ToString();
                        ComboBoxLockAll = false;
                        return;
                    }
                    else cbOutputDevice.Enabled = true;
                }
                else
                {
                    groupBox12.Enabled = false;
                    cbOutputDevice.Items.Add("(사운드 장치를 표시할 수 없는 출력입니다.)");
                    cbOutputDevice.Text = cbOutputDevice.Items[0].ToString();
                    ComboBoxLockAll = false;
                    RunSoundDeviceDetect = false;
                    return;
                }
            }
            catch (Exception ex)
            {
                ComboBoxLockAll = false; ex.Logging(); return;
            }//if (di.Length == 0 || di == null) return;
            finally { }

            try
            {
                if (di.Length > 0 && di != null)
                {
                    LastDeviceDefault = di[0];

                    cbOutputDevice.Items.Clear();
                    //MessageBox.Show("");
                    cbOutputDevice.Enabled = true;
                    cbOutputDevice.Items.Add("(기본 출력 장치)");
                    cbOutputDevice.Items.AddRange(di);

                    bool FindDevice = false;
                    if (LastDevice != null)
                    {
                        for (int i = 0; i < cbOutputDevice.Items.Count; i++)
                        {
                            SoundDevice d = cbOutputDevice.Items[i] as SoundDevice;
                            if (d != null && d.GUID.Equals(LastDevice.GUID))
                            {
                                cbOutputDevice.SelectedIndex = i;
                                FindDevice = true;
                                break;
                            }
                        }
                    }
                    else LastDevice = di[0];
                    if (!FindDevice)
                    {
                        ComboBoxLock = true;
                        cbOutputDevice.SelectedIndex = 0;
                        ComboBoxLock = false;
                    }
                    lblFMODResult.Text = Helper.Result.ToString();
                    //if (cbOutputDevice.Text == "") cbOutputDevice.SelectedIndex = 0;
                    //trackBar3.Enabled = false;
                    if (type == OUTPUTTYPE.DSOUND || type == OUTPUTTYPE.WASAPI || type == OUTPUTTYPE.WINMM)
                    {
                        try
                        {
                            if (WASAPIPeak_Thread != null) try { WASAPIPeak_Thread.Stop(); } catch { }
                            if (WindowsXP_Higher)
                            {
                                //if (WASAPIPeak_Thread != null) WASAPIPeak_Thread.Dispose();
                                mmd_Default = mm.GetDefaultAudioEndpoint(DataFlow.Render, Role.Multimedia);
                                mmdc = mm.EnumerateAudioEndPoints(DataFlow.Render, DeviceState.All);
                                foreach (MMDevice mmd in mmdc)
                                {
                                    int a = mmd.ID.LastIndexOf(".");
                                    string a1 = mmd.ID.Substring(a + 1);
                                    SoundDevice dd = cbOutputDevice.SelectedIndex > 0 ? (SoundDevice)cbOutputDevice.SelectedItem : Helper.DefaultSoundDevice;
                                    if (a1.ToUpper() == dd.GUID.ToString(true).ToUpper())
                                    {
                                        if (this.mmd != null && this.mmd.AudioEndpointVolume != null) this.mmd.AudioEndpointVolume.OnVolumeNotification -= AudioEndpointVolume_OnVolumeNotification;
                                        /*MessageBox.Show(mmd.DeviceFriendlyName);*/
                                        this.mmd = mmd; mmd.AudioEndpointVolume.OnVolumeNotification += AudioEndpointVolume_OnVolumeNotification;
                                        trackBar3.Enabled = checkBox2.Enabled = checkBox7.Enabled = checkBox7.Visible = progressBar1.Visible = true;
                                        trackBar3.Value = (int)(10000 * (checkBox7.Checked ? mmd.AudioEndpointVolume.MasterVolumeLevelPercent : mmd.AudioEndpointVolume.MasterVolumeLevelScalar));
                                        //System.Threading.ThreadPool.QueueUserWorkItem(new System.Threading.WaitCallback(WASAPIPeak_ThreadLoop)); break;
                                        WASAPIPeak_Thread.Start();
                                    }
                                }
                            }
                            else
                            {
                                if (mi != null) mi.Dispose();
                                mi = new WaveLib.AudioMixer.Mixer(WaveLib.AudioMixer.MixerType.Playback);
                                WindowsXPLine = mi.Lines[0];
                                mi.MixerLineChanged += WindowsXPLine_MixerLineChanged;
                                trackBar3.Value = (int)(WindowsXPLine.VolumeScala * 10000);
                                trackBar3.Enabled = checkBox2.Enabled = true;

                            }
                            /*
                            ComboBoxLock = true;
                            cbOutputDevice.SelectedIndex = 0;
                            ComboBoxLock = false;*/
                        }
                        catch
                        {
                            cbOutputDevice.Enabled = false;
                            ComboBoxLockAll = true;
                            cbOutputDevice.Items.Clear();
                            cbOutputDevice.Items.Add("(시스템 사운드 장치오류!)");
                            cbOutputDevice.Text = cbOutputDevice.Items[0].ToString();
                            ComboBoxLockAll = false;
                        }
                    }
                }
            }
            catch (Exception ex) { }
            finally { RunSoundDeviceDetect = true; First_cbOutputDevice_Change = false; ComboBoxLock = false; ComboBoxLockAll = false; }
            //RESULT result = RESULT.OK;
            //label15.Text = string.Format("출력 타입: {0} (결과: {1})", Helper.getOutputType(out result), result);
            //if (DectectDefaultSoundDevice_Thread != null) DectectDefaultSoundDevice_Thread.Abort();
            //System.Threading.ThreadPool.QueueUserWorkItem(new System.Threading.WaitCallback(DectectDefaultSoundDevice_ThreadLoop));
        }
        void ClearSoundDevices()
        {
            cbOutputDevice.Enabled = trackBar3.Enabled = checkBox2.Enabled = checkBox7.Enabled = false;
            checkBox2.Checked = false; label12.Text = "N/A";
            label11.Text = "-Inf. dB"; label14.Text = "-Inf.";
            progressBar1.Value = progressBar1.Minimum; progressBar2.Value = progressBar2.Minimum;
            //string a = cbOutputDevice.SelectedItem.ToString();
            LastDevice = cbOutputDevice.SelectedIndex == -1 ? null : cbOutputDevice.SelectedItem as SoundDevice;

            cbOutputDevice.Items.Clear();
            trackBar3.Value = 0;
        }

        private void WindowsXPVolumeControl_MuteChanged(object sender, LIBRARY.HSVolumeControl_WinXP.VolumeCategory Category, bool Mute)
        {
            if (Category == LIBRARY.HSVolumeControl_WinXP.VolumeCategory.Master) checkBox2.Checked = Mute;
        }

        private void WindowsXPVolumeControl_VolumeChanged(object sender, LIBRARY.HSVolumeControl_WinXP.VolumeCategory Category, int Volume)
        {
            HSVolumeControl_WinXP xp = sender as HSVolumeControl_WinXP;
            if (Category == LIBRARY.HSVolumeControl_WinXP.VolumeCategory.Master)
                if (xp != null) if (!LockVolume) trackBar3.Value = (int)(xp.VolumeScala * 10000);
                    else if (!LockVolume) trackBar3.Value = (int)(Volume / 65535.0) * 10000;
        }
        private void AudioEndpointVolume_OnVolumeNotification(AudioVolumeNotificationData data)
        {
            //float Vol = (mmd.AudioEndpointVolume.MasterVolumeLevel-mmd.AudioEndpointVolume.VolumeRange.MinDecibels)/(mmd.AudioEndpointVolume.VolumeRange.MaxDecibels-mmd.AudioEndpointVolume.VolumeRange.MinDecibels);
            if (!LockVolume) trackBar3.Value = (int)(10000 * (checkBox7.Checked ? mmd.AudioEndpointVolume.MasterVolumeLevelPercent : mmd.AudioEndpointVolume.MasterVolumeLevelScalar));
            checkBox2.Checked = data.Muted;
        }
    }
}
