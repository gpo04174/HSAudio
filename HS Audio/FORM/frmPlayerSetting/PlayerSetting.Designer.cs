﻿namespace HS_Audio
{
    partial class PlayerSetting
    {
        /// <summary> 
        /// 필수 디자이너 변수입니다.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 사용 중인 모든 리소스를 정리합니다.
        /// </summary>
        /// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
        protected override void Dispose(bool disposing)
        {
            Program.Project = new System.Collections.Generic.Dictionary<string, string>(Setting);
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
            if (Helper != null) Helper.Dispose();
        }

        #region 구성 요소 디자이너에서 생성한 코드

        /// <summary> 
        /// 디자이너 지원에 필요한 메서드입니다. 
        /// 이 메서드의 내용을 코드 편집기로 수정하지 마십시오.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PlayerSetting));
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.numPitch = new System.Windows.Forms.NumericUpDown();
            this.chk재생오류패치 = new System.Windows.Forms.CheckBox();
            this.numFrenq = new System.Windows.Forms.NumericUpDown();
            this.trackBar1 = new System.Windows.Forms.TrackBar();
            this.checkBox8 = new System.Windows.Forms.CheckBox();
            this.button12 = new System.Windows.Forms.Button();
            this.btn구간반복 = new System.Windows.Forms.Button();
            this.progressBar2 = new System.Windows.Forms.ProgressBar();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.fMODSystemToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.fMODChannelToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.button6 = new System.Windows.Forms.Button();
            this.comboBox6 = new System.Windows.Forms.ComboBox();
            this.label19 = new System.Windows.Forms.Label();
            this.button13 = new System.Windows.Forms.Button();
            this.btnExit = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.lblVolumeLeft = new System.Windows.Forms.Label();
            this.trbVolume = new System.Windows.Forms.TrackBar();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.checkBox5 = new System.Windows.Forms.CheckBox();
            this.button11 = new System.Windows.Forms.Button();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.button1 = new System.Windows.Forms.Button();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.button2 = new System.Windows.Forms.Button();
            this.numPan = new System.Windows.Forms.NumericUpDown();
            this.label8 = new System.Windows.Forms.Label();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.label5 = new System.Windows.Forms.Label();
            this.numSpeed = new System.Windows.Forms.NumericUpDown();
            this.comboBox4 = new System.Windows.Forms.ComboBox();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.button8 = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.chk반복재생 = new System.Windows.Forms.CheckBox();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.numGain = new System.Windows.Forms.NumericUpDown();
            this.label7 = new System.Windows.Forms.Label();
            this.groupBox10 = new System.Windows.Forms.GroupBox();
            this.label3 = new System.Windows.Forms.Label();
            this.trackBar2 = new System.Windows.Forms.TrackBar();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.comboBox2 = new System.Windows.Forms.ComboBox();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this.comboBox3 = new System.Windows.Forms.ComboBox();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.groupBox13 = new System.Windows.Forms.GroupBox();
            this.pnl_Output = new System.Windows.Forms.Panel();
            this.label18 = new System.Windows.Forms.Label();
            this.cbOutputSampler = new System.Windows.Forms.ComboBox();
            this.label17 = new System.Windows.Forms.Label();
            this.chkOutputSampleAuto = new System.Windows.Forms.CheckBox();
            this.numOuputSampler = new System.Windows.Forms.NumericUpDown();
            this.label16 = new System.Windows.Forms.Label();
            this.numOutputChannel = new System.Windows.Forms.NumericUpDown();
            this.label15 = new System.Windows.Forms.Label();
            this.cbOutputFormat = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.cbOutputType = new System.Windows.Forms.ComboBox();
            this.btnOutputCustom = new System.Windows.Forms.Button();
            this.btnOutputNow = new System.Windows.Forms.Button();
            this.btnOutputOK = new System.Windows.Forms.Button();
            this.chkOutputApply = new System.Windows.Forms.CheckBox();
            this.lblOuputStatus = new System.Windows.Forms.Label();
            this.groupBox12 = new System.Windows.Forms.GroupBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.chkAutoDeviceDetect = new System.Windows.Forms.CheckBox();
            this.checkBox7 = new System.Windows.Forms.CheckBox();
            this.checkBox2 = new System.Windows.Forms.CheckBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.trackBar3 = new System.Windows.Forms.TrackBar();
            this.btnSoundDeviceRefresh = new System.Windows.Forms.Button();
            this.cbOutputDevice = new System.Windows.Forms.ComboBox();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel2 = new System.Windows.Forms.ToolStripStatusLabel();
            this.lblFMODResult = new System.Windows.Forms.ToolStripStatusLabel();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.파일FToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.프로젝트열기OToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.프로젝트저장ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.닫기XToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.설정ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.맨위로설정ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.사운드출력방법선택ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripComboBox1 = new System.Windows.Forms.ToolStripComboBox();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.프리셋관리ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.프리셋업데이트ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator8 = new System.Windows.Forms.ToolStripSeparator();
            this.자동업데이트ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.파일에서업데이트ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.업데이트설정ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator10 = new System.Windows.Forms.ToolStripSeparator();
            this.개발자옵션ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.음장효과설정ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.음장효과부가기능ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.테스트ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.peak그래프ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.번Peak그래프ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.번Peak그래프ToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.스펙트럼분석ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.이퀄라이저그래프ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator6 = new System.Windows.Forms.ToolStripSeparator();
            this.dSP설정ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dSP설정ToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator9 = new System.Windows.Forms.ToolStripSeparator();
            this.aGC설정ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.이퀄라이저EQ설정ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.반향효과Reverb설정ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.가상서라운드설정ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator7 = new System.Windows.Forms.ToolStripSeparator();
            this.d리스너설정ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.플러그인설정ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator12 = new System.Windows.Forms.ToolStripSeparator();
            this.사운드스크립트ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator11 = new System.Windows.Forms.ToolStripSeparator();
            this.마이크입력노래방ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.출력사운드녹음ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.도움말HToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.구간반복도움말ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            ((System.ComponentModel.ISupportInitialize)(this.numPitch)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numFrenq)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar1)).BeginInit();
            this.contextMenuStrip1.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trbVolume)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numPan)).BeginInit();
            this.groupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numSpeed)).BeginInit();
            this.groupBox6.SuspendLayout();
            this.groupBox7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numGain)).BeginInit();
            this.groupBox10.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar2)).BeginInit();
            this.groupBox8.SuspendLayout();
            this.groupBox9.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.groupBox13.SuspendLayout();
            this.pnl_Output.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numOuputSampler)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numOutputChannel)).BeginInit();
            this.groupBox12.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar3)).BeginInit();
            this.statusStrip1.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // toolTip1
            // 
            this.toolTip1.AutoPopDelay = 5000;
            this.toolTip1.InitialDelay = 500;
            this.toolTip1.ReshowDelay = 50;
            this.toolTip1.ShowAlways = true;
            // 
            // numPitch
            // 
            this.numPitch.DecimalPlaces = 1;
            this.numPitch.Increment = new decimal(new int[] {
            50,
            0,
            0,
            0});
            this.numPitch.Location = new System.Drawing.Point(3, 21);
            this.numPitch.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.numPitch.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.numPitch.Minimum = new decimal(new int[] {
            10000,
            0,
            0,
            -2147483648});
            this.numPitch.Name = "numPitch";
            this.numPitch.Size = new System.Drawing.Size(80, 21);
            this.numPitch.TabIndex = 6;
            this.toolTip1.SetToolTip(this.numPitch, "만약 음악이 바뀔때 업데이트 하고싶지 않으면\r\n[설정(T)]->[프리셋 관리]->[자동 업데이트] 를\r\n해제해 주시기 바랍니다.");
            this.numPitch.ValueChanged += new System.EventHandler(this.numPitch_ValueChanged);
            // 
            // chk재생오류패치
            // 
            this.chk재생오류패치.AutoSize = true;
            this.chk재생오류패치.Enabled = false;
            this.chk재생오류패치.Location = new System.Drawing.Point(259, 301);
            this.chk재생오류패치.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.chk재생오류패치.Name = "chk재생오류패치";
            this.chk재생오류패치.Size = new System.Drawing.Size(100, 16);
            this.chk재생오류패치.TabIndex = 72;
            this.chk재생오류패치.Text = "재생 오류패치";
            this.toolTip1.SetToolTip(this.chk재생오류패치, "만약 음악재생이 끝났는데 뒤에 남은 부분이 있어서 \r\n계속 재생될경우 체크해주세요.\r\n(※주의: 반드시 처음으로 이동을할때는 이 기능을 끄고 이동" +
        "해 주세요)");
            this.chk재생오류패치.UseVisualStyleBackColor = true;
            this.chk재생오류패치.Visible = false;
            this.chk재생오류패치.CheckedChanged += new System.EventHandler(this.checkBox2_CheckedChanged);
            // 
            // numFrenq
            // 
            this.numFrenq.DecimalPlaces = 1;
            this.numFrenq.Increment = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.numFrenq.Location = new System.Drawing.Point(6, 15);
            this.numFrenq.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.numFrenq.Maximum = new decimal(new int[] {
            360000,
            0,
            0,
            0});
            this.numFrenq.Minimum = new decimal(new int[] {
            360000,
            0,
            0,
            -2147483648});
            this.numFrenq.Name = "numFrenq";
            this.numFrenq.Size = new System.Drawing.Size(73, 21);
            this.numFrenq.TabIndex = 6;
            this.toolTip1.SetToolTip(this.numFrenq, "만약 음악이 바뀔때 업데이트 하고싶지 않으면\r\n[설정(T)]->[프리셋 관리]->[자동 업데이트] 를\r\n해제해 주시기 바랍니다.");
            this.numFrenq.Value = new decimal(new int[] {
            44100,
            0,
            0,
            0});
            this.numFrenq.ValueChanged += new System.EventHandler(this.numericUpDown4_ValueChanged);
            // 
            // trackBar1
            // 
            this.trackBar1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.trackBar1.AutoSize = false;
            this.trackBar1.Location = new System.Drawing.Point(4, 344);
            this.trackBar1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.trackBar1.Maximum = 0;
            this.trackBar1.Name = "trackBar1";
            this.trackBar1.Size = new System.Drawing.Size(433, 33);
            this.trackBar1.TabIndex = 67;
            this.toolTip1.SetToolTip(this.trackBar1, "※주의: 처음으로 이동을할때는\r\n           반드시 재생 오류패치를 끄고 이동해 주세요!!");
            this.trackBar1.Scroll += new System.EventHandler(this.trackBar1_Scroll);
            this.trackBar1.ValueChanged += new System.EventHandler(this.trackBar1_ValueChanged);
            // 
            // checkBox8
            // 
            this.checkBox8.AutoSize = true;
            this.checkBox8.Location = new System.Drawing.Point(8, 15);
            this.checkBox8.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.checkBox8.Name = "checkBox8";
            this.checkBox8.Size = new System.Drawing.Size(50, 16);
            this.checkBox8.TabIndex = 67;
            this.checkBox8.Text = "Gain";
            this.toolTip1.SetToolTip(this.checkBox8, "체크하면 절대음량이 됩니다.");
            this.checkBox8.UseVisualStyleBackColor = true;
            this.checkBox8.CheckedChanged += new System.EventHandler(this.checkBox8_CheckedChanged);
            // 
            // button12
            // 
            this.button12.Location = new System.Drawing.Point(13, 171);
            this.button12.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button12.Name = "button12";
            this.button12.Size = new System.Drawing.Size(38, 20);
            this.button12.TabIndex = 7;
            this.button12.Text = "500";
            this.button12.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.toolTip1.SetToolTip(this.button12, "믹서를 새로고침 합니다.");
            this.button12.UseVisualStyleBackColor = true;
            this.button12.Click += new System.EventHandler(this.button12_Click);
            // 
            // btn구간반복
            // 
            this.btn구간반복.AutoSize = true;
            this.btn구간반복.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btn구간반복.Enabled = false;
            this.btn구간반복.Location = new System.Drawing.Point(227, 277);
            this.btn구간반복.Name = "btn구간반복";
            this.btn구간반복.Size = new System.Drawing.Size(63, 22);
            this.btn구간반복.TabIndex = 78;
            this.btn구간반복.Text = "구간반복";
            this.toolTip1.SetToolTip(this.btn구간반복, "버튼을 클릭하여 구간반복 설정");
            this.btn구간반복.UseVisualStyleBackColor = true;
            this.btn구간반복.Click += new System.EventHandler(this.btn구간반복_Click);
            // 
            // progressBar2
            // 
            this.progressBar2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.progressBar2.Location = new System.Drawing.Point(11, 114);
            this.progressBar2.MarqueeAnimationSpeed = 25;
            this.progressBar2.Maximum = 10000;
            this.progressBar2.Name = "progressBar2";
            this.progressBar2.Size = new System.Drawing.Size(337, 8);
            this.progressBar2.TabIndex = 12;
            this.toolTip1.SetToolTip(this.progressBar2, "시스템 음략 표시기");
            // 
            // progressBar1
            // 
            this.progressBar1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.progressBar1.Location = new System.Drawing.Point(11, 101);
            this.progressBar1.MarqueeAnimationSpeed = 25;
            this.progressBar1.Maximum = 40;
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(337, 8);
            this.progressBar1.TabIndex = 10;
            this.toolTip1.SetToolTip(this.progressBar1, "시스템 음량 표시기 (데시벨)");
            this.progressBar1.Visible = false;
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fMODSystemToolStripMenuItem,
            this.fMODChannelToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(143, 48);
            // 
            // fMODSystemToolStripMenuItem
            // 
            this.fMODSystemToolStripMenuItem.Name = "fMODSystemToolStripMenuItem";
            this.fMODSystemToolStripMenuItem.Size = new System.Drawing.Size(142, 22);
            this.fMODSystemToolStripMenuItem.Text = "Main Output";
            this.fMODSystemToolStripMenuItem.Click += new System.EventHandler(this.fMODSystemToolStripMenuItem_Click);
            // 
            // fMODChannelToolStripMenuItem
            // 
            this.fMODChannelToolStripMenuItem.Checked = true;
            this.fMODChannelToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.fMODChannelToolStripMenuItem.Name = "fMODChannelToolStripMenuItem";
            this.fMODChannelToolStripMenuItem.Size = new System.Drawing.Size(142, 22);
            this.fMODChannelToolStripMenuItem.Text = "Channel";
            this.fMODChannelToolStripMenuItem.Click += new System.EventHandler(this.fMODChannelToolStripMenuItem_Click);
            // 
            // timer1
            // 
            this.timer1.Interval = 25;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // saveFileDialog1
            // 
            this.saveFileDialog1.DefaultExt = "*.proj";
            this.saveFileDialog1.FileName = "제목 없음";
            this.saveFileDialog1.Filter = "프로젝트 파일 (*.proj)|*.proj";
            this.saveFileDialog1.Title = "다른 이름으로 프로젝트 파일 저장";
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.DefaultExt = "*.proj";
            this.openFileDialog1.FileName = "제목 없음";
            this.openFileDialog1.Filter = "프로젝트 파일 (*.proj)|*.proj";
            this.openFileDialog1.Title = "프로젝트 파일 열기";
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 24);
            this.tabControl1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(456, 408);
            this.tabControl1.TabIndex = 77;
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.SystemColors.Control;
            this.tabPage1.Controls.Add(this.button6);
            this.tabPage1.Controls.Add(this.comboBox6);
            this.tabPage1.Controls.Add(this.label19);
            this.tabPage1.Controls.Add(this.btn구간반복);
            this.tabPage1.Controls.Add(this.button13);
            this.tabPage1.Controls.Add(this.btnExit);
            this.tabPage1.Controls.Add(this.groupBox2);
            this.tabPage1.Controls.Add(this.groupBox1);
            this.tabPage1.Controls.Add(this.groupBox3);
            this.tabPage1.Controls.Add(this.groupBox4);
            this.tabPage1.Controls.Add(this.chk재생오류패치);
            this.tabPage1.Controls.Add(this.label8);
            this.tabPage1.Controls.Add(this.groupBox5);
            this.tabPage1.Controls.Add(this.comboBox4);
            this.tabPage1.Controls.Add(this.groupBox6);
            this.tabPage1.Controls.Add(this.chk반복재생);
            this.tabPage1.Controls.Add(this.groupBox7);
            this.tabPage1.Controls.Add(this.label7);
            this.tabPage1.Controls.Add(this.trackBar1);
            this.tabPage1.Controls.Add(this.groupBox10);
            this.tabPage1.Controls.Add(this.groupBox8);
            this.tabPage1.Controls.Add(this.groupBox9);
            this.tabPage1.Controls.Add(this.button3);
            this.tabPage1.Controls.Add(this.button4);
            this.tabPage1.Controls.Add(this.button5);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tabPage1.Size = new System.Drawing.Size(448, 382);
            this.tabPage1.TabIndex = 1;
            this.tabPage1.Text = "기본 설정";
            // 
            // button6
            // 
            this.button6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button6.Enabled = false;
            this.button6.Location = new System.Drawing.Point(378, 5);
            this.button6.Margin = new System.Windows.Forms.Padding(2);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(68, 20);
            this.button6.TabIndex = 81;
            this.button6.Text = "새로고침";
            this.button6.UseVisualStyleBackColor = true;
            // 
            // comboBox6
            // 
            this.comboBox6.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.comboBox6.Enabled = false;
            this.comboBox6.FormattingEnabled = true;
            this.comboBox6.Location = new System.Drawing.Point(61, 6);
            this.comboBox6.Margin = new System.Windows.Forms.Padding(2);
            this.comboBox6.Name = "comboBox6";
            this.comboBox6.Size = new System.Drawing.Size(313, 20);
            this.comboBox6.TabIndex = 80;
            this.comboBox6.Text = "(제목 없음)";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Enabled = false;
            this.label19.Location = new System.Drawing.Point(3, 11);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(53, 12);
            this.label19.TabIndex = 79;
            this.label19.Text = "프로젝트";
            // 
            // button13
            // 
            this.button13.Location = new System.Drawing.Point(350, 247);
            this.button13.Name = "button13";
            this.button13.Size = new System.Drawing.Size(48, 22);
            this.button13.TabIndex = 76;
            this.button13.Text = "Test";
            this.button13.UseVisualStyleBackColor = true;
            this.button13.Visible = false;
            this.button13.Click += new System.EventHandler(this.button13_Click);
            // 
            // btnExit
            // 
            this.btnExit.Location = new System.Drawing.Point(387, 295);
            this.btnExit.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(52, 22);
            this.btnExit.TabIndex = 75;
            this.btnExit.Text = "닫기";
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Visible = false;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.lblVolumeLeft);
            this.groupBox2.Controls.Add(this.trbVolume);
            this.groupBox2.Location = new System.Drawing.Point(1, 31);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox2.Size = new System.Drawing.Size(64, 194);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "음량";
            // 
            // lblVolumeLeft
            // 
            this.lblVolumeLeft.AutoSize = true;
            this.lblVolumeLeft.BackColor = System.Drawing.Color.Transparent;
            this.lblVolumeLeft.ForeColor = System.Drawing.Color.Black;
            this.lblVolumeLeft.Location = new System.Drawing.Point(15, 177);
            this.lblVolumeLeft.Name = "lblVolumeLeft";
            this.lblVolumeLeft.Size = new System.Drawing.Size(23, 12);
            this.lblVolumeLeft.TabIndex = 5;
            this.lblVolumeLeft.Text = "100";
            // 
            // trbVolume
            // 
            this.trbVolume.AutoSize = false;
            this.trbVolume.Location = new System.Drawing.Point(8, 16);
            this.trbVolume.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.trbVolume.Maximum = 100;
            this.trbVolume.Name = "trbVolume";
            this.trbVolume.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.trbVolume.Size = new System.Drawing.Size(45, 159);
            this.trbVolume.TabIndex = 2;
            this.trbVolume.TickFrequency = 5;
            this.trbVolume.TickStyle = System.Windows.Forms.TickStyle.Both;
            this.trbVolume.Value = 100;
            this.trbVolume.Scroll += new System.EventHandler(this.trbVolume_Scroll);
            this.trbVolume.ValueChanged += new System.EventHandler(this.trbVolumeLeft_ValueChanged);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.numPitch);
            this.groupBox1.Location = new System.Drawing.Point(137, 105);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox1.Size = new System.Drawing.Size(90, 48);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "피치설정";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.checkBox5);
            this.groupBox3.Controls.Add(this.button11);
            this.groupBox3.Controls.Add(this.checkBox1);
            this.groupBox3.Controls.Add(this.button1);
            this.groupBox3.Controls.Add(this.comboBox1);
            this.groupBox3.Location = new System.Drawing.Point(137, 31);
            this.groupBox3.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox3.Size = new System.Drawing.Size(274, 67);
            this.groupBox3.TabIndex = 2;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "반향 효과(Reverb) 설정";
            // 
            // checkBox5
            // 
            this.checkBox5.AutoSize = true;
            this.checkBox5.Enabled = false;
            this.checkBox5.Location = new System.Drawing.Point(96, 16);
            this.checkBox5.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.checkBox5.Name = "checkBox5";
            this.checkBox5.Size = new System.Drawing.Size(172, 16);
            this.checkBox5.TabIndex = 9;
            this.checkBox5.Text = "라우드니스 이퀼라이제이션";
            this.checkBox5.UseVisualStyleBackColor = true;
            this.checkBox5.Visible = false;
            // 
            // button11
            // 
            this.button11.Enabled = false;
            this.button11.Location = new System.Drawing.Point(290, 57);
            this.button11.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button11.Name = "button11";
            this.button11.Size = new System.Drawing.Size(57, 21);
            this.button11.TabIndex = 7;
            this.button11.Text = "프리셋";
            this.button11.UseVisualStyleBackColor = true;
            this.button11.Visible = false;
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Enabled = false;
            this.checkBox1.Location = new System.Drawing.Point(3, 16);
            this.checkBox1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(72, 16);
            this.checkBox1.TabIndex = 2;
            this.checkBox1.Text = "부드럽게";
            this.checkBox1.UseVisualStyleBackColor = true;
            this.checkBox1.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(211, 38);
            this.button1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(57, 20);
            this.button1.TabIndex = 1;
            this.button1.Text = "프리셋";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // comboBox1
            // 
            this.comboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.ItemHeight = 12;
            this.comboBox1.Items.AddRange(new object[] {
            "없음",
            "일반",
            "방음실",
            "방",
            "욕실",
            "거실",
            "석조실",
            "강당",
            "콘서트 홀",
            "동굴",
            "경기장",
            "격납고",
            "카페트가 깔린 복도",
            "복도",
            "석조 회랑",
            "골목길",
            "숲",
            "도시",
            "산",
            "채석장",
            "평원",
            "주차장",
            "하수도",
            "지하수",
            "(사용자 설정)"});
            this.comboBox1.Location = new System.Drawing.Point(3, 38);
            this.comboBox1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.comboBox1.MaxDropDownItems = 19;
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(201, 20);
            this.comboBox1.TabIndex = 0;
            this.comboBox1.SelectedValueChanged += new System.EventHandler(this.comboBox1_SelectedValueChanged);
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.button2);
            this.groupBox4.Controls.Add(this.numPan);
            this.groupBox4.Location = new System.Drawing.Point(259, 159);
            this.groupBox4.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox4.Size = new System.Drawing.Size(106, 62);
            this.groupBox4.TabIndex = 3;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "팬(밸런스) 설정";
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(3, 37);
            this.button2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(97, 22);
            this.button2.TabIndex = 9;
            this.button2.Text = "기본값";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // numPan
            // 
            this.numPan.DecimalPlaces = 7;
            this.numPan.Increment = new decimal(new int[] {
            5,
            0,
            0,
            196608});
            this.numPan.Location = new System.Drawing.Point(3, 15);
            this.numPan.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.numPan.Maximum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numPan.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            -2147483648});
            this.numPan.Name = "numPan";
            this.numPan.Size = new System.Drawing.Size(97, 21);
            this.numPan.TabIndex = 4;
            this.numPan.ValueChanged += new System.EventHandler(this.numPan_ValueChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(190, 326);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(31, 12);
            this.label8.TabIndex = 71;
            this.label8.Text = "0 / 0";
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.label5);
            this.groupBox5.Controls.Add(this.numSpeed);
            this.groupBox5.Enabled = false;
            this.groupBox5.Location = new System.Drawing.Point(232, 105);
            this.groupBox5.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox5.Size = new System.Drawing.Size(78, 48);
            this.groupBox5.TabIndex = 8;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "속도설정";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(66, 24);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(0, 12);
            this.label5.TabIndex = 7;
            // 
            // numSpeed
            // 
            this.numSpeed.DecimalPlaces = 1;
            this.numSpeed.Increment = new decimal(new int[] {
            5,
            0,
            0,
            65536});
            this.numSpeed.Location = new System.Drawing.Point(6, 21);
            this.numSpeed.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.numSpeed.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            -2147483648});
            this.numSpeed.Name = "numSpeed";
            this.numSpeed.Size = new System.Drawing.Size(64, 21);
            this.numSpeed.TabIndex = 6;
            // 
            // comboBox4
            // 
            this.comboBox4.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox4.FormattingEnabled = true;
            this.comboBox4.Location = new System.Drawing.Point(7, 321);
            this.comboBox4.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.comboBox4.Name = "comboBox4";
            this.comboBox4.Size = new System.Drawing.Size(178, 20);
            this.comboBox4.TabIndex = 70;
            this.comboBox4.SelectedIndexChanged += new System.EventHandler(this.comboBox4_SelectedIndexChanged);
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.button8);
            this.groupBox6.Controls.Add(this.label6);
            this.groupBox6.Controls.Add(this.numFrenq);
            this.groupBox6.Location = new System.Drawing.Point(137, 158);
            this.groupBox6.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox6.Size = new System.Drawing.Size(112, 63);
            this.groupBox6.TabIndex = 8;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "주파수";
            // 
            // button8
            // 
            this.button8.Location = new System.Drawing.Point(6, 38);
            this.button8.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(99, 22);
            this.button8.TabIndex = 8;
            this.button8.Text = "기본값";
            this.button8.UseVisualStyleBackColor = true;
            this.button8.Click += new System.EventHandler(this.button8_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(85, 20);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(20, 12);
            this.label6.TabIndex = 7;
            this.label6.Text = "Hz";
            // 
            // chk반복재생
            // 
            this.chk반복재생.AutoSize = true;
            this.chk반복재생.Location = new System.Drawing.Point(183, 302);
            this.chk반복재생.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.chk반복재생.Name = "chk반복재생";
            this.chk반복재생.Size = new System.Drawing.Size(76, 16);
            this.chk반복재생.TabIndex = 69;
            this.chk반복재생.Text = "반복 재생";
            this.chk반복재생.UseVisualStyleBackColor = true;
            this.chk반복재생.Visible = false;
            this.chk반복재생.CheckedChanged += new System.EventHandler(this.chk반복재생_CheckedChanged);
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.numGain);
            this.groupBox7.Enabled = false;
            this.groupBox7.Location = new System.Drawing.Point(317, 105);
            this.groupBox7.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox7.Size = new System.Drawing.Size(70, 48);
            this.groupBox7.TabIndex = 9;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Gain";
            this.groupBox7.Visible = false;
            // 
            // numGain
            // 
            this.numGain.DecimalPlaces = 2;
            this.numGain.Increment = new decimal(new int[] {
            5,
            0,
            0,
            65536});
            this.numGain.Location = new System.Drawing.Point(6, 21);
            this.numGain.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.numGain.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.numGain.Minimum = new decimal(new int[] {
            1000,
            0,
            0,
            -2147483648});
            this.numGain.Name = "numGain";
            this.numGain.Size = new System.Drawing.Size(59, 21);
            this.numGain.TabIndex = 6;
            this.numGain.ValueChanged += new System.EventHandler(this.numGain_ValueChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(2, 305);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(183, 12);
            this.label7.TabIndex = 68;
            this.label7.Text = "시간: 00:00:00:000 / 00:00:00:000";
            // 
            // groupBox10
            // 
            this.groupBox10.Controls.Add(this.checkBox8);
            this.groupBox10.Controls.Add(this.button12);
            this.groupBox10.Controls.Add(this.label3);
            this.groupBox10.Controls.Add(this.trackBar2);
            this.groupBox10.Location = new System.Drawing.Point(67, 31);
            this.groupBox10.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox10.Name = "groupBox10";
            this.groupBox10.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox10.Size = new System.Drawing.Size(64, 194);
            this.groupBox10.TabIndex = 66;
            this.groupBox10.TabStop = false;
            this.groupBox10.Text = "믹서";
            this.groupBox10.MouseHover += new System.EventHandler(this.groupBox10_MouseHover);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(71, 144);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(23, 12);
            this.label3.TabIndex = 6;
            this.label3.Text = "100";
            // 
            // trackBar2
            // 
            this.trackBar2.AutoSize = false;
            this.trackBar2.ContextMenuStrip = this.contextMenuStrip1;
            this.trackBar2.Location = new System.Drawing.Point(8, 32);
            this.trackBar2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.trackBar2.Maximum = 500;
            this.trackBar2.Name = "trackBar2";
            this.trackBar2.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.trackBar2.Size = new System.Drawing.Size(45, 137);
            this.trackBar2.TabIndex = 2;
            this.trackBar2.TickFrequency = 5;
            this.trackBar2.TickStyle = System.Windows.Forms.TickStyle.Both;
            this.trackBar2.Value = 500;
            this.trackBar2.ValueChanged += new System.EventHandler(this.trackBar2_ValueChanged);
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this.comboBox2);
            this.groupBox8.Enabled = false;
            this.groupBox8.Location = new System.Drawing.Point(4, 233);
            this.groupBox8.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox8.Size = new System.Drawing.Size(132, 40);
            this.groupBox8.TabIndex = 11;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "채널 설정 (구현X)";
            // 
            // comboBox2
            // 
            this.comboBox2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox2.FormattingEnabled = true;
            this.comboBox2.Items.AddRange(new object[] {
            "STEREO",
            "MONO",
            "QUAD",
            "SURROUND",
            "_5POINT1",
            "_7POINT1",
            "SRS5_1_MATRIX",
            "MYEARS",
            "MAX",
            "RAW"});
            this.comboBox2.Location = new System.Drawing.Point(6, 15);
            this.comboBox2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.comboBox2.Name = "comboBox2";
            this.comboBox2.Size = new System.Drawing.Size(119, 20);
            this.comboBox2.TabIndex = 0;
            this.comboBox2.SelectedIndexChanged += new System.EventHandler(this.comboBox2_SelectedIndexChanged);
            // 
            // groupBox9
            // 
            this.groupBox9.Controls.Add(this.comboBox3);
            this.groupBox9.Enabled = false;
            this.groupBox9.Location = new System.Drawing.Point(141, 233);
            this.groupBox9.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox9.Size = new System.Drawing.Size(183, 40);
            this.groupBox9.TabIndex = 12;
            this.groupBox9.TabStop = false;
            this.groupBox9.Text = "사운드 모드 (전문가용)";
            // 
            // comboBox3
            // 
            this.comboBox3.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox3.FormattingEnabled = true;
            this.comboBox3.Location = new System.Drawing.Point(6, 15);
            this.comboBox3.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.comboBox3.Name = "comboBox3";
            this.comboBox3.Size = new System.Drawing.Size(172, 20);
            this.comboBox3.TabIndex = 1;
            this.comboBox3.SelectedIndexChanged += new System.EventHandler(this.comboBox3_SelectedIndexChanged);
            this.comboBox3.SelectedValueChanged += new System.EventHandler(this.comboBox3_SelectedValueChanged);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(4, 277);
            this.button3.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(64, 22);
            this.button3.TabIndex = 13;
            this.button3.Text = "재생";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(74, 277);
            this.button4.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(78, 22);
            this.button4.TabIndex = 14;
            this.button4.Text = "일시정지";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(155, 277);
            this.button5.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(66, 22);
            this.button5.TabIndex = 15;
            this.button5.Text = "정지";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // tabPage2
            // 
            this.tabPage2.BackColor = System.Drawing.SystemColors.Control;
            this.tabPage2.Controls.Add(this.groupBox13);
            this.tabPage2.Controls.Add(this.groupBox12);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tabPage2.Size = new System.Drawing.Size(448, 382);
            this.tabPage2.TabIndex = 0;
            this.tabPage2.Text = "고급 설정";
            // 
            // groupBox13
            // 
            this.groupBox13.Controls.Add(this.pnl_Output);
            this.groupBox13.Controls.Add(this.btnOutputCustom);
            this.groupBox13.Controls.Add(this.btnOutputNow);
            this.groupBox13.Controls.Add(this.btnOutputOK);
            this.groupBox13.Controls.Add(this.chkOutputApply);
            this.groupBox13.Controls.Add(this.lblOuputStatus);
            this.groupBox13.ForeColor = System.Drawing.Color.Blue;
            this.groupBox13.Location = new System.Drawing.Point(6, 164);
            this.groupBox13.Name = "groupBox13";
            this.groupBox13.Size = new System.Drawing.Size(329, 151);
            this.groupBox13.TabIndex = 3;
            this.groupBox13.TabStop = false;
            this.groupBox13.Text = "     ";
            // 
            // pnl_Output
            // 
            this.pnl_Output.Controls.Add(this.label18);
            this.pnl_Output.Controls.Add(this.cbOutputSampler);
            this.pnl_Output.Controls.Add(this.label17);
            this.pnl_Output.Controls.Add(this.chkOutputSampleAuto);
            this.pnl_Output.Controls.Add(this.numOuputSampler);
            this.pnl_Output.Controls.Add(this.label16);
            this.pnl_Output.Controls.Add(this.numOutputChannel);
            this.pnl_Output.Controls.Add(this.label15);
            this.pnl_Output.Controls.Add(this.cbOutputFormat);
            this.pnl_Output.Controls.Add(this.label9);
            this.pnl_Output.Controls.Add(this.label4);
            this.pnl_Output.Controls.Add(this.cbOutputType);
            this.pnl_Output.Enabled = false;
            this.pnl_Output.Location = new System.Drawing.Point(2, 18);
            this.pnl_Output.Name = "pnl_Output";
            this.pnl_Output.Size = new System.Drawing.Size(326, 105);
            this.pnl_Output.TabIndex = 19;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.ForeColor = System.Drawing.Color.Black;
            this.label18.Location = new System.Drawing.Point(0, 56);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(69, 12);
            this.label18.TabIndex = 15;
            this.label18.Text = "샘플링 방법";
            // 
            // cbOutputSampler
            // 
            this.cbOutputSampler.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbOutputSampler.FormattingEnabled = true;
            this.cbOutputSampler.Items.AddRange(new object[] {
            "NOINTERP (보간 없음)",
            "LINEAR (선형 보간)",
            "CUBIC (입방 보간)",
            "SPLINE (5점 스플 라인 보간)",
            "MAX"});
            this.cbOutputSampler.Location = new System.Drawing.Point(76, 53);
            this.cbOutputSampler.Name = "cbOutputSampler";
            this.cbOutputSampler.Size = new System.Drawing.Size(185, 20);
            this.cbOutputSampler.TabIndex = 14;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.ForeColor = System.Drawing.Color.Black;
            this.label17.Location = new System.Drawing.Point(145, 31);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(20, 12);
            this.label17.TabIndex = 13;
            this.label17.Text = "Hz";
            // 
            // chkOutputSampleAuto
            // 
            this.chkOutputSampleAuto.AutoSize = true;
            this.chkOutputSampleAuto.Enabled = false;
            this.chkOutputSampleAuto.ForeColor = System.Drawing.Color.Black;
            this.chkOutputSampleAuto.Location = new System.Drawing.Point(170, 30);
            this.chkOutputSampleAuto.Name = "chkOutputSampleAuto";
            this.chkOutputSampleAuto.Size = new System.Drawing.Size(48, 16);
            this.chkOutputSampleAuto.TabIndex = 12;
            this.chkOutputSampleAuto.Text = "자동";
            this.chkOutputSampleAuto.UseVisualStyleBackColor = true;
            this.chkOutputSampleAuto.Visible = false;
            // 
            // numOuputSampler
            // 
            this.numOuputSampler.Increment = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.numOuputSampler.Location = new System.Drawing.Point(76, 27);
            this.numOuputSampler.Maximum = new decimal(new int[] {
            192000,
            0,
            0,
            0});
            this.numOuputSampler.Minimum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.numOuputSampler.Name = "numOuputSampler";
            this.numOuputSampler.Size = new System.Drawing.Size(67, 21);
            this.numOuputSampler.TabIndex = 11;
            this.numOuputSampler.ThousandsSeparator = true;
            this.numOuputSampler.Value = new decimal(new int[] {
            48000,
            0,
            0,
            0});
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.ForeColor = System.Drawing.Color.Black;
            this.label16.Location = new System.Drawing.Point(0, 31);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(69, 12);
            this.label16.TabIndex = 10;
            this.label16.Text = "출력 샘플링";
            // 
            // numOutputChannel
            // 
            this.numOutputChannel.Location = new System.Drawing.Point(234, 79);
            this.numOutputChannel.Maximum = new decimal(new int[] {
            1024,
            0,
            0,
            0});
            this.numOutputChannel.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numOutputChannel.Name = "numOutputChannel";
            this.numOutputChannel.Size = new System.Drawing.Size(54, 21);
            this.numOutputChannel.TabIndex = 9;
            this.numOutputChannel.Value = new decimal(new int[] {
            2,
            0,
            0,
            0});
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.ForeColor = System.Drawing.Color.Black;
            this.label15.Location = new System.Drawing.Point(171, 83);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(57, 12);
            this.label15.TabIndex = 8;
            this.label15.Text = "출력 채널";
            // 
            // cbOutputFormat
            // 
            this.cbOutputFormat.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbOutputFormat.FormattingEnabled = true;
            this.cbOutputFormat.Items.AddRange(new object[] {
            "32 (FLOAT)",
            "32",
            "24",
            "16",
            "8"});
            this.cbOutputFormat.Location = new System.Drawing.Point(76, 78);
            this.cbOutputFormat.Name = "cbOutputFormat";
            this.cbOutputFormat.Size = new System.Drawing.Size(89, 20);
            this.cbOutputFormat.TabIndex = 6;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.ForeColor = System.Drawing.Color.Black;
            this.label9.Location = new System.Drawing.Point(0, 82);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(57, 12);
            this.label9.TabIndex = 5;
            this.label9.Text = "출력 비트";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(0, 5);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(57, 12);
            this.label4.TabIndex = 4;
            this.label4.Text = "출력 타입";
            // 
            // cbOutputType
            // 
            this.cbOutputType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbOutputType.FormattingEnabled = true;
            this.cbOutputType.Items.AddRange(new object[] {
            "AUTO (기본값, 자동)",
            "DSOUND (DirectSound)",
            "WASAPI (Windows Audio Session API)",
            "WINMM (Windows Multimedia)",
            "ASIO (Low latency ASIO 2.0)",
            "NOSOUND (출력 안 함)"});
            this.cbOutputType.Location = new System.Drawing.Point(76, 2);
            this.cbOutputType.Name = "cbOutputType";
            this.cbOutputType.Size = new System.Drawing.Size(245, 20);
            this.cbOutputType.TabIndex = 1;
            // 
            // btnOutputCustom
            // 
            this.btnOutputCustom.ForeColor = System.Drawing.Color.Black;
            this.btnOutputCustom.Location = new System.Drawing.Point(152, 124);
            this.btnOutputCustom.Name = "btnOutputCustom";
            this.btnOutputCustom.Size = new System.Drawing.Size(55, 23);
            this.btnOutputCustom.TabIndex = 18;
            this.btnOutputCustom.Text = "설정값";
            this.btnOutputCustom.UseVisualStyleBackColor = true;
            // 
            // btnOutputNow
            // 
            this.btnOutputNow.ForeColor = System.Drawing.Color.Black;
            this.btnOutputNow.Location = new System.Drawing.Point(209, 124);
            this.btnOutputNow.Name = "btnOutputNow";
            this.btnOutputNow.Size = new System.Drawing.Size(55, 23);
            this.btnOutputNow.TabIndex = 17;
            this.btnOutputNow.Text = "현재값";
            this.btnOutputNow.UseVisualStyleBackColor = true;
            // 
            // btnOutputOK
            // 
            this.btnOutputOK.ForeColor = System.Drawing.Color.Black;
            this.btnOutputOK.Location = new System.Drawing.Point(268, 124);
            this.btnOutputOK.Name = "btnOutputOK";
            this.btnOutputOK.Size = new System.Drawing.Size(56, 23);
            this.btnOutputOK.TabIndex = 16;
            this.btnOutputOK.Text = "적용";
            this.btnOutputOK.UseVisualStyleBackColor = true;
            // 
            // chkOutputApply
            // 
            this.chkOutputApply.AutoSize = true;
            this.chkOutputApply.Location = new System.Drawing.Point(10, 0);
            this.chkOutputApply.Name = "chkOutputApply";
            this.chkOutputApply.Size = new System.Drawing.Size(144, 16);
            this.chkOutputApply.TabIndex = 7;
            this.chkOutputApply.Text = "사운드 출력 직접 설정";
            this.chkOutputApply.UseVisualStyleBackColor = true;
            // 
            // lblOuputStatus
            // 
            this.lblOuputStatus.AutoSize = true;
            this.lblOuputStatus.Font = new System.Drawing.Font("Gulim", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblOuputStatus.ForeColor = System.Drawing.Color.Black;
            this.lblOuputStatus.Location = new System.Drawing.Point(4, 131);
            this.lblOuputStatus.Name = "lblOuputStatus";
            this.lblOuputStatus.Size = new System.Drawing.Size(60, 12);
            this.lblOuputStatus.TabIndex = 3;
            this.lblOuputStatus.Text = "결과: OK";
            // 
            // groupBox12
            // 
            this.groupBox12.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox12.Controls.Add(this.label14);
            this.groupBox12.Controls.Add(this.label11);
            this.groupBox12.Controls.Add(this.progressBar2);
            this.groupBox12.Controls.Add(this.chkAutoDeviceDetect);
            this.groupBox12.Controls.Add(this.progressBar1);
            this.groupBox12.Controls.Add(this.checkBox7);
            this.groupBox12.Controls.Add(this.checkBox2);
            this.groupBox12.Controls.Add(this.label13);
            this.groupBox12.Controls.Add(this.label12);
            this.groupBox12.Controls.Add(this.trackBar3);
            this.groupBox12.Controls.Add(this.btnSoundDeviceRefresh);
            this.groupBox12.Controls.Add(this.cbOutputDevice);
            this.groupBox12.ForeColor = System.Drawing.Color.Blue;
            this.groupBox12.Location = new System.Drawing.Point(6, 5);
            this.groupBox12.Name = "groupBox12";
            this.groupBox12.Size = new System.Drawing.Size(434, 153);
            this.groupBox12.TabIndex = 2;
            this.groupBox12.TabStop = false;
            this.groupBox12.Text = "사운드 출력 장치";
            // 
            // label14
            // 
            this.label14.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label14.Location = new System.Drawing.Point(358, 114);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(22, 13);
            this.label14.TabIndex = 14;
            this.label14.Text = "0.0";
            // 
            // label11
            // 
            this.label11.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label11.Location = new System.Drawing.Point(358, 99);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(59, 13);
            this.label11.TabIndex = 13;
            this.label11.Text = "-320.00 dB";
            // 
            // chkAutoDeviceDetect
            // 
            this.chkAutoDeviceDetect.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.chkAutoDeviceDetect.AutoSize = true;
            this.chkAutoDeviceDetect.ForeColor = System.Drawing.Color.Black;
            this.chkAutoDeviceDetect.Location = new System.Drawing.Point(346, 44);
            this.chkAutoDeviceDetect.Name = "chkAutoDeviceDetect";
            this.chkAutoDeviceDetect.Size = new System.Drawing.Size(76, 16);
            this.chkAutoDeviceDetect.TabIndex = 11;
            this.chkAutoDeviceDetect.Text = "자동 감지";
            this.chkAutoDeviceDetect.UseVisualStyleBackColor = true;
            this.chkAutoDeviceDetect.CheckedChanged += new System.EventHandler(this.chkAutoDeviceDetect_CheckedChanged);
            // 
            // checkBox7
            // 
            this.checkBox7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.checkBox7.AutoSize = true;
            this.checkBox7.ForeColor = System.Drawing.Color.Purple;
            this.checkBox7.Location = new System.Drawing.Point(288, 133);
            this.checkBox7.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.checkBox7.Name = "checkBox7";
            this.checkBox7.Size = new System.Drawing.Size(139, 16);
            this.checkBox7.TabIndex = 9;
            this.checkBox7.Text = "dB 퍼센트(%) 단위로";
            this.checkBox7.UseVisualStyleBackColor = true;
            this.checkBox7.Visible = false;
            this.checkBox7.CheckedChanged += new System.EventHandler(this.checkBox7_CheckedChanged);
            // 
            // checkBox2
            // 
            this.checkBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.checkBox2.AutoSize = true;
            this.checkBox2.ForeColor = System.Drawing.Color.Purple;
            this.checkBox2.Location = new System.Drawing.Point(4, 133);
            this.checkBox2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.checkBox2.Name = "checkBox2";
            this.checkBox2.Size = new System.Drawing.Size(88, 16);
            this.checkBox2.TabIndex = 8;
            this.checkBox2.Text = "모두 음소거";
            this.checkBox2.UseVisualStyleBackColor = true;
            this.checkBox2.Click += new System.EventHandler(this.checkBox2_Click);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Gulim", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label13.ForeColor = System.Drawing.Color.Purple;
            this.label13.Location = new System.Drawing.Point(6, 45);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(295, 12);
            this.label13.TabIndex = 7;
            this.label13.Text = "선택한 사운드 출력 장치의 마스터 볼륨 조절하기";
            // 
            // label12
            // 
            this.label12.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Gulim", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label12.ForeColor = System.Drawing.Color.Purple;
            this.label12.Location = new System.Drawing.Point(358, 70);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(28, 12);
            this.label12.TabIndex = 6;
            this.label12.Text = "N/A";
            // 
            // trackBar3
            // 
            this.trackBar3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.trackBar3.AutoSize = false;
            this.trackBar3.Enabled = false;
            this.trackBar3.Location = new System.Drawing.Point(4, 65);
            this.trackBar3.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.trackBar3.Maximum = 10000;
            this.trackBar3.Name = "trackBar3";
            this.trackBar3.Size = new System.Drawing.Size(351, 31);
            this.trackBar3.SmallChange = 2;
            this.trackBar3.TabIndex = 5;
            this.trackBar3.TickFrequency = 100;
            this.trackBar3.Scroll += new System.EventHandler(this.trackBar3_Scroll);
            this.trackBar3.ValueChanged += new System.EventHandler(this.trackBar3_ValueChanged);
            // 
            // btnSoundDeviceRefresh
            // 
            this.btnSoundDeviceRefresh.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSoundDeviceRefresh.ForeColor = System.Drawing.Color.Black;
            this.btnSoundDeviceRefresh.Location = new System.Drawing.Point(346, 20);
            this.btnSoundDeviceRefresh.Name = "btnSoundDeviceRefresh";
            this.btnSoundDeviceRefresh.Size = new System.Drawing.Size(73, 20);
            this.btnSoundDeviceRefresh.TabIndex = 3;
            this.btnSoundDeviceRefresh.Text = "새로고침";
            this.btnSoundDeviceRefresh.UseVisualStyleBackColor = true;
            this.btnSoundDeviceRefresh.Click += new System.EventHandler(this.btnSoundDeviceRefresh_Click);
            // 
            // cbOutputDevice
            // 
            this.cbOutputDevice.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cbOutputDevice.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbOutputDevice.FormattingEnabled = true;
            this.cbOutputDevice.Items.AddRange(new object[] {
            "(기본 사운드 장치)"});
            this.cbOutputDevice.Location = new System.Drawing.Point(6, 20);
            this.cbOutputDevice.Name = "cbOutputDevice";
            this.cbOutputDevice.Size = new System.Drawing.Size(334, 20);
            this.cbOutputDevice.TabIndex = 1;
            this.cbOutputDevice.SelectedIndexChanged += new System.EventHandler(this.cbOutputDevice_SelectedIndexChanged);
            // 
            // statusStrip1
            // 
            this.statusStrip1.AutoSize = false;
            this.statusStrip1.BackColor = System.Drawing.Color.Transparent;
            this.statusStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1,
            this.toolStripStatusLabel2,
            this.lblFMODResult});
            this.statusStrip1.Location = new System.Drawing.Point(0, 432);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(456, 16);
            this.statusStrip1.TabIndex = 63;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Font = new System.Drawing.Font("Gulim", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(67, 11);
            this.toolStripStatusLabel1.Text = "내부 상태:";
            this.toolStripStatusLabel1.ToolTipText = "FMOD 상태 (OK가 아니면 설정실패!)";
            // 
            // toolStripStatusLabel2
            // 
            this.toolStripStatusLabel2.Font = new System.Drawing.Font("Gulim", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.toolStripStatusLabel2.ForeColor = System.Drawing.SystemColors.Control;
            this.toolStripStatusLabel2.IsLink = true;
            this.toolStripStatusLabel2.Name = "toolStripStatusLabel2";
            this.toolStripStatusLabel2.Size = new System.Drawing.Size(0, 11);
            // 
            // lblFMODResult
            // 
            this.lblFMODResult.Font = new System.Drawing.Font("Gulim", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblFMODResult.ForeColor = System.Drawing.Color.Blue;
            this.lblFMODResult.Name = "lblFMODResult";
            this.lblFMODResult.Size = new System.Drawing.Size(24, 11);
            this.lblFMODResult.Text = "OK";
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.파일FToolStripMenuItem,
            this.설정ToolStripMenuItem,
            this.음장효과설정ToolStripMenuItem,
            this.도움말HToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(456, 24);
            this.menuStrip1.TabIndex = 76;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // 파일FToolStripMenuItem
            // 
            this.파일FToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.프로젝트열기OToolStripMenuItem,
            this.프로젝트저장ToolStripMenuItem,
            this.toolStripSeparator1,
            this.닫기XToolStripMenuItem});
            this.파일FToolStripMenuItem.Name = "파일FToolStripMenuItem";
            this.파일FToolStripMenuItem.Size = new System.Drawing.Size(57, 20);
            this.파일FToolStripMenuItem.Text = "파일(&F)";
            // 
            // 프로젝트열기OToolStripMenuItem
            // 
            this.프로젝트열기OToolStripMenuItem.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.프로젝트열기OToolStripMenuItem.Image = global::HS_Audio.Properties.Resources.folder;
            this.프로젝트열기OToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.프로젝트열기OToolStripMenuItem.Name = "프로젝트열기OToolStripMenuItem";
            this.프로젝트열기OToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.O)));
            this.프로젝트열기OToolStripMenuItem.Size = new System.Drawing.Size(209, 22);
            this.프로젝트열기OToolStripMenuItem.Text = "프로젝트 열기(&O)";
            this.프로젝트열기OToolStripMenuItem.Click += new System.EventHandler(this.프로젝트열기OToolStripMenuItem_Click);
            // 
            // 프로젝트저장ToolStripMenuItem
            // 
            this.프로젝트저장ToolStripMenuItem.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.프로젝트저장ToolStripMenuItem.Image = global::HS_Audio.Properties.Resources.disk;
            this.프로젝트저장ToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.프로젝트저장ToolStripMenuItem.Name = "프로젝트저장ToolStripMenuItem";
            this.프로젝트저장ToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
            this.프로젝트저장ToolStripMenuItem.Size = new System.Drawing.Size(209, 22);
            this.프로젝트저장ToolStripMenuItem.Text = "프로젝트 저장(&S)";
            this.프로젝트저장ToolStripMenuItem.Click += new System.EventHandler(this.프로젝트저장ToolStripMenuItem_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(206, 6);
            // 
            // 닫기XToolStripMenuItem
            // 
            this.닫기XToolStripMenuItem.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.닫기XToolStripMenuItem.Image = global::HS_Audio.Properties.Resources.FillLeftHS;
            this.닫기XToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.닫기XToolStripMenuItem.Name = "닫기XToolStripMenuItem";
            this.닫기XToolStripMenuItem.Size = new System.Drawing.Size(209, 22);
            this.닫기XToolStripMenuItem.Text = "닫기(&X)";
            this.닫기XToolStripMenuItem.Click += new System.EventHandler(this.닫기XToolStripMenuItem_Click);
            // 
            // 설정ToolStripMenuItem
            // 
            this.설정ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.맨위로설정ToolStripMenuItem,
            this.toolStripSeparator5,
            this.사운드출력방법선택ToolStripMenuItem,
            this.toolStripSeparator3,
            this.프리셋관리ToolStripMenuItem,
            this.업데이트설정ToolStripMenuItem,
            this.toolStripSeparator10,
            this.개발자옵션ToolStripMenuItem});
            this.설정ToolStripMenuItem.Name = "설정ToolStripMenuItem";
            this.설정ToolStripMenuItem.Size = new System.Drawing.Size(58, 20);
            this.설정ToolStripMenuItem.Text = "설정(&T)";
            this.설정ToolStripMenuItem.DropDownOpening += new System.EventHandler(this.설정ToolStripMenuItem_DropDownOpening);
            // 
            // 맨위로설정ToolStripMenuItem
            // 
            this.맨위로설정ToolStripMenuItem.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.맨위로설정ToolStripMenuItem.CheckOnClick = true;
            this.맨위로설정ToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("맨위로설정ToolStripMenuItem.Image")));
            this.맨위로설정ToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.맨위로설정ToolStripMenuItem.Name = "맨위로설정ToolStripMenuItem";
            this.맨위로설정ToolStripMenuItem.Size = new System.Drawing.Size(200, 22);
            this.맨위로설정ToolStripMenuItem.Text = "맨 위로 설정";
            this.맨위로설정ToolStripMenuItem.CheckedChanged += new System.EventHandler(this.맨위로설정ToolStripMenuItem_CheckedChanged);
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(197, 6);
            this.toolStripSeparator5.Visible = false;
            // 
            // 사운드출력방법선택ToolStripMenuItem
            // 
            this.사운드출력방법선택ToolStripMenuItem.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.사운드출력방법선택ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripComboBox1});
            this.사운드출력방법선택ToolStripMenuItem.Enabled = false;
            this.사운드출력방법선택ToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.사운드출력방법선택ToolStripMenuItem.Name = "사운드출력방법선택ToolStripMenuItem";
            this.사운드출력방법선택ToolStripMenuItem.Size = new System.Drawing.Size(200, 22);
            this.사운드출력방법선택ToolStripMenuItem.Text = "사운드 출력 방법 선택";
            this.사운드출력방법선택ToolStripMenuItem.Visible = false;
            // 
            // toolStripComboBox1
            // 
            this.toolStripComboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.toolStripComboBox1.Font = new System.Drawing.Font("Gulim", 8.780488F);
            this.toolStripComboBox1.Items.AddRange(new object[] {
            "AUTODETECT",
            "DSOUND",
            "WINMM",
            "WASAPI",
            "ASIO  ",
            "UNKNOWN",
            "NOSOUND",
            "WAVWRITER",
            "NOSOUND_NRT",
            "WAVWRITER_NRT"});
            this.toolStripComboBox1.Name = "toolStripComboBox1";
            this.toolStripComboBox1.Size = new System.Drawing.Size(121, 20);
            this.toolStripComboBox1.ToolTipText = "사운드 출력방법을 설정합니다.\r\n(다음번 재생시 적용됩니다.)";
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(197, 6);
            // 
            // 프리셋관리ToolStripMenuItem
            // 
            this.프리셋관리ToolStripMenuItem.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.프리셋관리ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.프리셋업데이트ToolStripMenuItem,
            this.toolStripSeparator8,
            this.자동업데이트ToolStripMenuItem,
            this.파일에서업데이트ToolStripMenuItem});
            this.프리셋관리ToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.프리셋관리ToolStripMenuItem.Name = "프리셋관리ToolStripMenuItem";
            this.프리셋관리ToolStripMenuItem.Size = new System.Drawing.Size(200, 22);
            this.프리셋관리ToolStripMenuItem.Text = "프로젝트 관리";
            this.프리셋관리ToolStripMenuItem.ToolTipText = "만약 이 항목이 비활성화 되있으면\r\n파일에서 프로젝트 파일을 열어주시기 바랍니다.\r\n그럼 활성화 됩니다.";
            // 
            // 프리셋업데이트ToolStripMenuItem
            // 
            this.프리셋업데이트ToolStripMenuItem.Name = "프리셋업데이트ToolStripMenuItem";
            this.프리셋업데이트ToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F5;
            this.프리셋업데이트ToolStripMenuItem.Size = new System.Drawing.Size(192, 22);
            this.프리셋업데이트ToolStripMenuItem.Text = "프로젝트 업데이트";
            this.프리셋업데이트ToolStripMenuItem.Click += new System.EventHandler(this.프리셋업데이트ToolStripMenuItem_Click);
            // 
            // toolStripSeparator8
            // 
            this.toolStripSeparator8.Name = "toolStripSeparator8";
            this.toolStripSeparator8.Size = new System.Drawing.Size(189, 6);
            // 
            // 자동업데이트ToolStripMenuItem
            // 
            this.자동업데이트ToolStripMenuItem.Checked = true;
            this.자동업데이트ToolStripMenuItem.CheckOnClick = true;
            this.자동업데이트ToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.자동업데이트ToolStripMenuItem.Name = "자동업데이트ToolStripMenuItem";
            this.자동업데이트ToolStripMenuItem.Size = new System.Drawing.Size(192, 22);
            this.자동업데이트ToolStripMenuItem.Text = "자동 업데이트";
            // 
            // 파일에서업데이트ToolStripMenuItem
            // 
            this.파일에서업데이트ToolStripMenuItem.Checked = true;
            this.파일에서업데이트ToolStripMenuItem.CheckOnClick = true;
            this.파일에서업데이트ToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.파일에서업데이트ToolStripMenuItem.Name = "파일에서업데이트ToolStripMenuItem";
            this.파일에서업데이트ToolStripMenuItem.Size = new System.Drawing.Size(192, 22);
            this.파일에서업데이트ToolStripMenuItem.Text = "파일에서 업데이트";
            this.파일에서업데이트ToolStripMenuItem.ToolTipText = "체크를 해제하게 되면 마지막으로\r\n파일에서 업데이트한 프로젝트가 적용됩니다.";
            // 
            // 업데이트설정ToolStripMenuItem
            // 
            this.업데이트설정ToolStripMenuItem.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.업데이트설정ToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.업데이트설정ToolStripMenuItem.Name = "업데이트설정ToolStripMenuItem";
            this.업데이트설정ToolStripMenuItem.Size = new System.Drawing.Size(200, 22);
            this.업데이트설정ToolStripMenuItem.Text = "프로젝트 업데이트 설정";
            this.업데이트설정ToolStripMenuItem.Click += new System.EventHandler(this.업데이트설정ToolStripMenuItem_Click);
            // 
            // toolStripSeparator10
            // 
            this.toolStripSeparator10.Name = "toolStripSeparator10";
            this.toolStripSeparator10.Size = new System.Drawing.Size(197, 6);
            // 
            // 개발자옵션ToolStripMenuItem
            // 
            this.개발자옵션ToolStripMenuItem.CheckOnClick = true;
            this.개발자옵션ToolStripMenuItem.Name = "개발자옵션ToolStripMenuItem";
            this.개발자옵션ToolStripMenuItem.Size = new System.Drawing.Size(200, 22);
            this.개발자옵션ToolStripMenuItem.Text = "개발자 모드";
            this.개발자옵션ToolStripMenuItem.CheckedChanged += new System.EventHandler(this.개발자옵션ToolStripMenuItem_CheckedChanged);
            // 
            // 음장효과설정ToolStripMenuItem
            // 
            this.음장효과설정ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.음장효과부가기능ToolStripMenuItem,
            this.toolStripSeparator6,
            this.dSP설정ToolStripMenuItem,
            this.이퀄라이저EQ설정ToolStripMenuItem,
            this.반향효과Reverb설정ToolStripMenuItem,
            this.가상서라운드설정ToolStripMenuItem,
            this.toolStripSeparator7,
            this.d리스너설정ToolStripMenuItem,
            this.플러그인설정ToolStripMenuItem,
            this.toolStripSeparator12,
            this.사운드스크립트ToolStripMenuItem,
            this.toolStripSeparator11,
            this.마이크입력노래방ToolStripMenuItem,
            this.출력사운드녹음ToolStripMenuItem});
            this.음장효과설정ToolStripMenuItem.Name = "음장효과설정ToolStripMenuItem";
            this.음장효과설정ToolStripMenuItem.Size = new System.Drawing.Size(116, 20);
            this.음장효과설정ToolStripMenuItem.Text = "음장 효과 설정(&M)";
            // 
            // 음장효과부가기능ToolStripMenuItem
            // 
            this.음장효과부가기능ToolStripMenuItem.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.음장효과부가기능ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.테스트ToolStripMenuItem,
            this.peak그래프ToolStripMenuItem,
            this.toolStripSeparator4,
            this.스펙트럼분석ToolStripMenuItem,
            this.이퀄라이저그래프ToolStripMenuItem});
            this.음장효과부가기능ToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.음장효과부가기능ToolStripMenuItem.Name = "음장효과부가기능ToolStripMenuItem";
            this.음장효과부가기능ToolStripMenuItem.Size = new System.Drawing.Size(233, 22);
            this.음장효과부가기능ToolStripMenuItem.Text = "음악 비주얼라이저 (Visualizer)";
            // 
            // 테스트ToolStripMenuItem
            // 
            this.테스트ToolStripMenuItem.Name = "테스트ToolStripMenuItem";
            this.테스트ToolStripMenuItem.Size = new System.Drawing.Size(173, 22);
            this.테스트ToolStripMenuItem.Text = "웨이브 그래프";
            this.테스트ToolStripMenuItem.Click += new System.EventHandler(this.테스트ToolStripMenuItem_Click);
            // 
            // peak그래프ToolStripMenuItem
            // 
            this.peak그래프ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.번Peak그래프ToolStripMenuItem,
            this.번Peak그래프ToolStripMenuItem1});
            this.peak그래프ToolStripMenuItem.Name = "peak그래프ToolStripMenuItem";
            this.peak그래프ToolStripMenuItem.Size = new System.Drawing.Size(173, 22);
            this.peak그래프ToolStripMenuItem.Text = "Peak 그래프";
            // 
            // 번Peak그래프ToolStripMenuItem
            // 
            this.번Peak그래프ToolStripMenuItem.Name = "번Peak그래프ToolStripMenuItem";
            this.번Peak그래프ToolStripMenuItem.Size = new System.Drawing.Size(191, 22);
            this.번Peak그래프ToolStripMenuItem.Text = "1번 Peak 그래프";
            this.번Peak그래프ToolStripMenuItem.Click += new System.EventHandler(this.번Peak그래프ToolStripMenuItem_Click);
            // 
            // 번Peak그래프ToolStripMenuItem1
            // 
            this.번Peak그래프ToolStripMenuItem1.Name = "번Peak그래프ToolStripMenuItem1";
            this.번Peak그래프ToolStripMenuItem1.Size = new System.Drawing.Size(191, 22);
            this.번Peak그래프ToolStripMenuItem1.Text = "2번 Peak그래프 (추천)";
            this.번Peak그래프ToolStripMenuItem1.Click += new System.EventHandler(this.번Peak그래프ToolStripMenuItem1_Click);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(170, 6);
            // 
            // 스펙트럼분석ToolStripMenuItem
            // 
            this.스펙트럼분석ToolStripMenuItem.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.스펙트럼분석ToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.스펙트럼분석ToolStripMenuItem.Name = "스펙트럼분석ToolStripMenuItem";
            this.스펙트럼분석ToolStripMenuItem.Size = new System.Drawing.Size(173, 22);
            this.스펙트럼분석ToolStripMenuItem.Text = "스펙트럼 분석";
            this.스펙트럼분석ToolStripMenuItem.Click += new System.EventHandler(this.스펙트럼분석ToolStripMenuItem_Click);
            // 
            // 이퀄라이저그래프ToolStripMenuItem
            // 
            this.이퀄라이저그래프ToolStripMenuItem.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.이퀄라이저그래프ToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.이퀄라이저그래프ToolStripMenuItem.Name = "이퀄라이저그래프ToolStripMenuItem";
            this.이퀄라이저그래프ToolStripMenuItem.Size = new System.Drawing.Size(173, 22);
            this.이퀄라이저그래프ToolStripMenuItem.Text = "이퀄라이저 그래프";
            this.이퀄라이저그래프ToolStripMenuItem.Click += new System.EventHandler(this.이퀄라이저그래프ToolStripMenuItem_Click);
            // 
            // toolStripSeparator6
            // 
            this.toolStripSeparator6.Name = "toolStripSeparator6";
            this.toolStripSeparator6.Size = new System.Drawing.Size(230, 6);
            // 
            // dSP설정ToolStripMenuItem
            // 
            this.dSP설정ToolStripMenuItem.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.dSP설정ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.dSP설정ToolStripMenuItem1,
            this.toolStripSeparator9,
            this.aGC설정ToolStripMenuItem});
            this.dSP설정ToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.dSP설정ToolStripMenuItem.Name = "dSP설정ToolStripMenuItem";
            this.dSP설정ToolStripMenuItem.Size = new System.Drawing.Size(233, 22);
            this.dSP설정ToolStripMenuItem.Text = "DSP 설정";
            // 
            // dSP설정ToolStripMenuItem1
            // 
            this.dSP설정ToolStripMenuItem1.Name = "dSP설정ToolStripMenuItem1";
            this.dSP설정ToolStripMenuItem1.Size = new System.Drawing.Size(149, 22);
            this.dSP설정ToolStripMenuItem1.Text = "내장 DSP 설정";
            this.dSP설정ToolStripMenuItem1.Click += new System.EventHandler(this.dSP설정ToolStripMenuItem_Click);
            // 
            // toolStripSeparator9
            // 
            this.toolStripSeparator9.Name = "toolStripSeparator9";
            this.toolStripSeparator9.Size = new System.Drawing.Size(146, 6);
            // 
            // aGC설정ToolStripMenuItem
            // 
            this.aGC설정ToolStripMenuItem.Name = "aGC설정ToolStripMenuItem";
            this.aGC설정ToolStripMenuItem.Size = new System.Drawing.Size(149, 22);
            this.aGC설정ToolStripMenuItem.Text = "HS™ DSP 설정";
            this.aGC설정ToolStripMenuItem.Click += new System.EventHandler(this.aGC설정ToolStripMenuItem_Click);
            // 
            // 이퀄라이저EQ설정ToolStripMenuItem
            // 
            this.이퀄라이저EQ설정ToolStripMenuItem.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.이퀄라이저EQ설정ToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.이퀄라이저EQ설정ToolStripMenuItem.Name = "이퀄라이저EQ설정ToolStripMenuItem";
            this.이퀄라이저EQ설정ToolStripMenuItem.Size = new System.Drawing.Size(233, 22);
            this.이퀄라이저EQ설정ToolStripMenuItem.Text = "이퀄라이저(EQ) 설정";
            this.이퀄라이저EQ설정ToolStripMenuItem.Click += new System.EventHandler(this.이퀄라이저EQ설정ToolStripMenuItem_Click);
            // 
            // 반향효과Reverb설정ToolStripMenuItem
            // 
            this.반향효과Reverb설정ToolStripMenuItem.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.반향효과Reverb설정ToolStripMenuItem.Enabled = false;
            this.반향효과Reverb설정ToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.반향효과Reverb설정ToolStripMenuItem.Name = "반향효과Reverb설정ToolStripMenuItem";
            this.반향효과Reverb설정ToolStripMenuItem.Size = new System.Drawing.Size(233, 22);
            this.반향효과Reverb설정ToolStripMenuItem.Text = "반향 효과 (Reverb) 설정";
            this.반향효과Reverb설정ToolStripMenuItem.ToolTipText = "반향 효과 (울리는 효과)를 설정 합니다.\r\n\r\n(만약 이 항목이 비활성화 되있으면 먼저  반향효과를 선택해 주시기 바랍니다.)";
            this.반향효과Reverb설정ToolStripMenuItem.Click += new System.EventHandler(this.반향효과Reverb설정ToolStripMenuItem_Click);
            // 
            // 가상서라운드설정ToolStripMenuItem
            // 
            this.가상서라운드설정ToolStripMenuItem.Enabled = false;
            this.가상서라운드설정ToolStripMenuItem.Name = "가상서라운드설정ToolStripMenuItem";
            this.가상서라운드설정ToolStripMenuItem.Size = new System.Drawing.Size(233, 22);
            this.가상서라운드설정ToolStripMenuItem.Text = "가상 서라운드(SR) 설정";
            this.가상서라운드설정ToolStripMenuItem.ToolTipText = "기능 준비중 입니다";
            this.가상서라운드설정ToolStripMenuItem.Click += new System.EventHandler(this.가상서라운드설정ToolStripMenuItem_Click);
            // 
            // toolStripSeparator7
            // 
            this.toolStripSeparator7.Name = "toolStripSeparator7";
            this.toolStripSeparator7.Size = new System.Drawing.Size(230, 6);
            // 
            // d리스너설정ToolStripMenuItem
            // 
            this.d리스너설정ToolStripMenuItem.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.d리스너설정ToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.d리스너설정ToolStripMenuItem.Name = "d리스너설정ToolStripMenuItem";
            this.d리스너설정ToolStripMenuItem.Size = new System.Drawing.Size(233, 22);
            this.d리스너설정ToolStripMenuItem.Text = "3D리스너 설정";
            this.d리스너설정ToolStripMenuItem.Click += new System.EventHandler(this.d리스너설정ToolStripMenuItem_Click);
            // 
            // 플러그인설정ToolStripMenuItem
            // 
            this.플러그인설정ToolStripMenuItem.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.플러그인설정ToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.플러그인설정ToolStripMenuItem.Name = "플러그인설정ToolStripMenuItem";
            this.플러그인설정ToolStripMenuItem.Size = new System.Drawing.Size(233, 22);
            this.플러그인설정ToolStripMenuItem.Text = "플러그인 설정";
            this.플러그인설정ToolStripMenuItem.Click += new System.EventHandler(this.플러그인설정ToolStripMenuItem_Click);
            // 
            // toolStripSeparator12
            // 
            this.toolStripSeparator12.Name = "toolStripSeparator12";
            this.toolStripSeparator12.Size = new System.Drawing.Size(230, 6);
            // 
            // 사운드스크립트ToolStripMenuItem
            // 
            this.사운드스크립트ToolStripMenuItem.Image = global::HS_Audio.Properties.Resources.CSharp_Script;
            this.사운드스크립트ToolStripMenuItem.Name = "사운드스크립트ToolStripMenuItem";
            this.사운드스크립트ToolStripMenuItem.Size = new System.Drawing.Size(233, 22);
            this.사운드스크립트ToolStripMenuItem.Text = "사운드 스크립트";
            this.사운드스크립트ToolStripMenuItem.Click += new System.EventHandler(this.사운드스크립트ToolStripMenuItem_Click);
            // 
            // toolStripSeparator11
            // 
            this.toolStripSeparator11.Name = "toolStripSeparator11";
            this.toolStripSeparator11.Size = new System.Drawing.Size(230, 6);
            // 
            // 마이크입력노래방ToolStripMenuItem
            // 
            this.마이크입력노래방ToolStripMenuItem.Name = "마이크입력노래방ToolStripMenuItem";
            this.마이크입력노래방ToolStripMenuItem.Size = new System.Drawing.Size(233, 22);
            this.마이크입력노래방ToolStripMenuItem.Text = "마이크 입력 (노래방)";
            this.마이크입력노래방ToolStripMenuItem.Click += new System.EventHandler(this.마이크입력노래방ToolStripMenuItem_Click);
            // 
            // 출력사운드녹음ToolStripMenuItem
            // 
            this.출력사운드녹음ToolStripMenuItem.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.출력사운드녹음ToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.출력사운드녹음ToolStripMenuItem.Name = "출력사운드녹음ToolStripMenuItem";
            this.출력사운드녹음ToolStripMenuItem.Size = new System.Drawing.Size(233, 22);
            this.출력사운드녹음ToolStripMenuItem.Text = "출력 사운드 녹음 (베타)";
            this.출력사운드녹음ToolStripMenuItem.Click += new System.EventHandler(this.출력사운드녹음ToolStripMenuItem_Click);
            // 
            // 도움말HToolStripMenuItem
            // 
            this.도움말HToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.구간반복도움말ToolStripMenuItem});
            this.도움말HToolStripMenuItem.Name = "도움말HToolStripMenuItem";
            this.도움말HToolStripMenuItem.Size = new System.Drawing.Size(72, 20);
            this.도움말HToolStripMenuItem.Text = "도움말(&H)";
            // 
            // 구간반복도움말ToolStripMenuItem
            // 
            this.구간반복도움말ToolStripMenuItem.Name = "구간반복도움말ToolStripMenuItem";
            this.구간반복도움말ToolStripMenuItem.Size = new System.Drawing.Size(161, 22);
            this.구간반복도움말ToolStripMenuItem.Text = "구간반복 도움말";
            this.구간반복도움말ToolStripMenuItem.Click += new System.EventHandler(this.구간반복도움말ToolStripMenuItem_Click);
            // 
            // PlayerSetting
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(456, 448);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.menuStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "PlayerSetting";
            this.Text = "재생 설정";
            this.WindowStateChanged += new HS_Audio.Control.HSCustomForm.WindowStateChangedEventHandler(this.PlayerSetting_WindowStateChanged);
            this.WindowShowChanged += new HS_Audio.Control.WindowShowChangedEventHandler(this.PlayerSetting_WindowStateChanged);
            this.Activated += new System.EventHandler(this.PlayerSetting_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.PlayerSetting_FormClosing);
            this.Load += new System.EventHandler(this.PlayerSetting_Load);
            this.VisibleChanged += new System.EventHandler(this.PlayerSetting_VisibleChanged);
            this.Resize += new System.EventHandler(this.PlayerSetting_Resize);
            ((System.ComponentModel.ISupportInitialize)(this.numPitch)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numFrenq)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar1)).EndInit();
            this.contextMenuStrip1.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trbVolume)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.numPan)).EndInit();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numSpeed)).EndInit();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.groupBox7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.numGain)).EndInit();
            this.groupBox10.ResumeLayout(false);
            this.groupBox10.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar2)).EndInit();
            this.groupBox8.ResumeLayout(false);
            this.groupBox9.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.groupBox13.ResumeLayout(false);
            this.groupBox13.PerformLayout();
            this.pnl_Output.ResumeLayout(false);
            this.pnl_Output.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numOuputSampler)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numOutputChannel)).EndInit();
            this.groupBox12.ResumeLayout(false);
            this.groupBox12.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar3)).EndInit();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label lblVolumeLeft;
        private System.Windows.Forms.TrackBar trbVolume;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.NumericUpDown numPan;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.NumericUpDown numGain;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.ComboBox comboBox2;
        private System.Windows.Forms.GroupBox groupBox9;
        private System.Windows.Forms.ComboBox comboBox3;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel2;
        private System.Windows.Forms.ToolStripStatusLabel lblFMODResult;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.GroupBox groupBox10;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TrackBar trackBar2;
        private System.Windows.Forms.TrackBar trackBar1;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.CheckBox chk반복재생;
        private System.Windows.Forms.ComboBox comboBox4;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button button8;
        internal System.Windows.Forms.NumericUpDown numFrenq;
        private System.Windows.Forms.CheckBox checkBox5;
        private System.Windows.Forms.Button button11;
        private System.Windows.Forms.Button button12;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fMODSystemToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem fMODChannelToolStripMenuItem;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem 파일FToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 설정ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 사운드출력방법선택ToolStripMenuItem;
        private System.Windows.Forms.ToolStripComboBox toolStripComboBox1;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.ToolStripMenuItem 프로젝트저장ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 프로젝트열기OToolStripMenuItem;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.ToolStripMenuItem 맨위로설정ToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem 닫기XToolStripMenuItem;
        private System.Windows.Forms.Button btnExit;
        internal System.Windows.Forms.NumericUpDown numPitch;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripMenuItem 프리셋관리ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 파일에서업데이트ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 프리셋업데이트ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 자동업데이트ToolStripMenuItem;
        private System.Windows.Forms.Button button13;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.ToolStripMenuItem 음장효과설정ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 음장효과부가기능ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 스펙트럼분석ToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator6;
        private System.Windows.Forms.ToolStripMenuItem dSP설정ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 이퀄라이저EQ설정ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 반향효과Reverb설정ToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator7;
        private System.Windows.Forms.ToolStripMenuItem d리스너설정ToolStripMenuItem;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.NumericUpDown numSpeed;
        private System.Windows.Forms.ToolStripMenuItem 이퀄라이저그래프ToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator8;
        private System.Windows.Forms.ToolStripMenuItem 업데이트설정ToolStripMenuItem;
        private System.Windows.Forms.GroupBox groupBox12;
        private System.Windows.Forms.ComboBox cbOutputDevice;
        private System.Windows.Forms.GroupBox groupBox13;
        private System.Windows.Forms.ComboBox cbOutputType;
        private System.Windows.Forms.Label lblOuputStatus;
        private System.Windows.Forms.Button btnSoundDeviceRefresh;
        private System.Windows.Forms.ToolStripMenuItem 플러그인설정ToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripMenuItem 테스트ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem dSP설정ToolStripMenuItem1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator9;
        private System.Windows.Forms.ToolStripMenuItem aGC설정ToolStripMenuItem;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TrackBar trackBar3;
        private System.Windows.Forms.CheckBox checkBox2;
        private System.Windows.Forms.CheckBox checkBox7;
        private System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.CheckBox checkBox8;
        private System.Windows.Forms.CheckBox chkAutoDeviceDetect;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.ProgressBar progressBar2;
        private System.Windows.Forms.CheckBox chk재생오류패치;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator10;
        private System.Windows.Forms.ToolStripMenuItem 개발자옵션ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem peak그래프ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 번Peak그래프ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 번Peak그래프ToolStripMenuItem1;
        private System.Windows.Forms.Button btn구간반복;
        private System.Windows.Forms.ToolStripMenuItem 도움말HToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 구간반복도움말ToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator11;
        private System.Windows.Forms.ToolStripMenuItem 마이크입력노래방ToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator12;
        private System.Windows.Forms.ToolStripMenuItem 사운드스크립트ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 출력사운드녹음ToolStripMenuItem;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.ComboBox comboBox6;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Button btnOutputNow;
        private System.Windows.Forms.Button btnOutputOK;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.ComboBox cbOutputSampler;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.CheckBox chkOutputSampleAuto;
        private System.Windows.Forms.NumericUpDown numOuputSampler;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.NumericUpDown numOutputChannel;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.CheckBox chkOutputApply;
        private System.Windows.Forms.ComboBox cbOutputFormat;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btnOutputCustom;
        private System.Windows.Forms.Panel pnl_Output;
        private System.Windows.Forms.ToolStripMenuItem 가상서라운드설정ToolStripMenuItem;
    }
}
