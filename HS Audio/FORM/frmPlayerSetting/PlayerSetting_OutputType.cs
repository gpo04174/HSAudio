﻿using HS_Audio.Bundles;
using HS_CSharpUtility;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Windows.Forms;

namespace HS_Audio
{
    public partial class PlayerSetting
    {
        string OutputTypeFilePath;
        private void OutputType_Init()
        {
            chkOutputApply.CheckedChanged += (o, e) => pnl_Output.Enabled = chkOutputApply.Checked;

            string Appdir = Process.GetCurrentProcess().MainModule.FileName;
            OutputTypeFilePath = Appdir.Remove(Appdir.LastIndexOf("\\")) + "\\Settings\\SoundDeviceFormat.ini";
            try { SoundDeviceFormat fm = getOuputType_Setting(); OutputType_Refresh(fm==null ? Helper.getDeviceFormat() : fm); } catch { OutputType_Refresh(Helper.getDeviceFormat(), false); }
            //OutputType_Refresh((Helper == null || Helper.PlayStatus != HSAudioHelper.PlayingStatus.None) ? Helper.getDeviceFormat() : getOuputType_Setting());
            try
            {
                btnOutputNow.Click += (s, e) => OutputType_Refresh(Helper.getDeviceFormat(), false);
                btnOutputCustom.Click += (s, e) => OutputType_Refresh(getOuputType_Setting());
                btnOutputOK.Click += (s, e) =>
                {
                    try
                    {
                        SoundDeviceFormat format = new SoundDeviceFormat() { Samplerate = (int)numOuputSampler.Value, SamplerateAuto = chkOutputSampleAuto.Checked, OutputChannel = (int)numOutputChannel.Value };
                        switch (cbOutputType.SelectedIndex)
                        {
                            case 0: format.OutputType = HS_Audio_Lib.OUTPUTTYPE.AUTODETECT; break;
                            case 1: format.OutputType = HS_Audio_Lib.OUTPUTTYPE.DSOUND; break;
                            case 2: format.OutputType = HS_Audio_Lib.OUTPUTTYPE.WASAPI; break;
                            case 3: format.OutputType = HS_Audio_Lib.OUTPUTTYPE.WINMM; break;
                            case 4: format.OutputType = HS_Audio_Lib.OUTPUTTYPE.ASIO; break;
                            case 5: format.OutputType = HS_Audio_Lib.OUTPUTTYPE.NOSOUND; break;
                        }

                        switch (cbOutputFormat.SelectedIndex)
                        {
                            case 0: format.SoundFormat = HS_Audio_Lib.SOUND_FORMAT.PCMFLOAT; break;
                            case 1: format.SoundFormat = HS_Audio_Lib.SOUND_FORMAT.PCM32; break;
                            case 2: format.SoundFormat = HS_Audio_Lib.SOUND_FORMAT.PCM24; break;
                            case 3: format.SoundFormat = HS_Audio_Lib.SOUND_FORMAT.PCM16; break;
                            case 4: format.SoundFormat = HS_Audio_Lib.SOUND_FORMAT.PCM8; break;
                        }

                        switch (cbOutputSampler.SelectedIndex)
                        {
                            case 0: format.ResamplerMethod = HS_Audio_Lib.DSP_RESAMPLER.NOINTERP; break;
                            case 1: format.ResamplerMethod = HS_Audio_Lib.DSP_RESAMPLER.LINEAR; break;
                            case 2: format.ResamplerMethod = HS_Audio_Lib.DSP_RESAMPLER.SPLINE; break;
                            case 3: format.ResamplerMethod = HS_Audio_Lib.DSP_RESAMPLER.SPLINE; break;
                            case 4: format.ResamplerMethod = HS_Audio_Lib.DSP_RESAMPLER.MAX; break;
                        }

                        Dictionary<string, string> b = HS_CSharpUtility.Utility.EtcUtility.LoadSetting(Properties.Resources.SoundDeviceFormat.Split(new string[] { "\r\n" }, StringSplitOptions.None));
                        b["Apply"] = chkOutputApply.Checked.ToString();
                        b["Samplerate"] = format.Samplerate.ToString();
                        b["SamplerateAuto"] = format.SamplerateAuto.ToString();
                        b["ResamplerMethod"] = format.ResamplerMethod.ToString();
                        b["OuputFormat"] = format.SoundFormat.ToString();
                        b["OuputType"] = format.OutputType.ToString();
                        b["OutputChannel"] = numOutputChannel.Value.ToString();
                        //b["InputChannelMax"] = ;
                        File.WriteAllLines(OutputTypeFilePath, Utility.EtcUtility.SaveSetting(b));

                        MessageBox.Show("적용이 완료되었습니다.\n플레이어를 재시작해주시기 바랍니다.", Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    catch (Exception ex) { MessageBox.Show(ex.ToString(), this.Text); }
                };
            }
            catch { }
        }

        private void OutputType_Refresh(SoundDeviceFormat format, bool CheckApply = true)
        {
            if (format == null) return;
            switch (format.OutputType)
            {
                case HS_Audio_Lib.OUTPUTTYPE.AUTODETECT: cbOutputType.SelectedIndex = 0; break;
                case HS_Audio_Lib.OUTPUTTYPE.DSOUND: cbOutputType.SelectedIndex = 1; break;
                case HS_Audio_Lib.OUTPUTTYPE.WASAPI: cbOutputType.SelectedIndex = 2; break;
                case HS_Audio_Lib.OUTPUTTYPE.WINMM: cbOutputType.SelectedIndex = 3; break;
                case HS_Audio_Lib.OUTPUTTYPE.ASIO: cbOutputType.SelectedIndex = 4; break;
                case HS_Audio_Lib.OUTPUTTYPE.NOSOUND: cbOutputDevice.SelectedIndex = 5; break;
            }

            switch(format.SoundFormat)
            {
                case HS_Audio_Lib.SOUND_FORMAT.PCMFLOAT: cbOutputFormat.SelectedIndex = 0; break;
                case HS_Audio_Lib.SOUND_FORMAT.PCM32: cbOutputFormat.SelectedIndex = 1; break;
                case HS_Audio_Lib.SOUND_FORMAT.PCM24: cbOutputFormat.SelectedIndex = 2; break;
                case HS_Audio_Lib.SOUND_FORMAT.PCM16: cbOutputFormat.SelectedIndex = 3; break;
                case HS_Audio_Lib.SOUND_FORMAT.PCM8: cbOutputFormat.SelectedIndex = 4; break;
            }

            switch (format.ResamplerMethod)
            {
                case HS_Audio_Lib.DSP_RESAMPLER.NOINTERP: cbOutputSampler.SelectedIndex = 0; break;
                case HS_Audio_Lib.DSP_RESAMPLER.LINEAR: cbOutputSampler.SelectedIndex = 1; break;
                case HS_Audio_Lib.DSP_RESAMPLER.CUBIC: cbOutputSampler.SelectedIndex = 2; break;
                case HS_Audio_Lib.DSP_RESAMPLER.SPLINE: cbOutputSampler.SelectedIndex = 3; break;
                case HS_Audio_Lib.DSP_RESAMPLER.MAX: cbOutputSampler.SelectedIndex = 4; break;
            }

            numOuputSampler.Value = Math.Max(numOuputSampler.Minimum, Math.Min(numOuputSampler.Maximum, format.Samplerate));
            numOutputChannel.Value = Math.Max(numOutputChannel.Minimum, Math.Min(numOutputChannel.Maximum, format.OutputChannel));
            chkOutputSampleAuto.Checked = format.SamplerateAuto;
            if(CheckApply) chkOutputApply.Checked = !format.IsDefault;
        }
        private SoundDeviceFormat getOuputType_Setting()
        {
            string path = OutputTypeFilePath;
            try
            {
                if (File.Exists(path))
                {
                    SoundDeviceFormat format = new SoundDeviceFormat();
                    SoundDeviceFormat _format = null;
                    if (Helper != null) _format = Helper.getDeviceFormat();
                    else if (Helper != null) _format = Helper.getDeviceFormat();

                    string[] a = File.ReadAllLines(path);
                    Dictionary<string, string> b = HS_CSharpUtility.Utility.EtcUtility.LoadSetting(a);

                    try { if (b.ContainsKey("Apply")) format.IsDefault = !Convert.ToBoolean(b["Apply"]); } catch { }
                    try { if (b.ContainsKey("Samplerate")) format.Samplerate = Convert.ToInt32(b["Samplerate"]); } catch { format.Samplerate = _format == null ? 48000 : _format.Samplerate; }
                    try { if (b.ContainsKey("SamplerateAuto")) format.SamplerateAuto = Convert.ToBoolean(b["SamplerateAuto"]); } catch { }
                    try { if (b.ContainsKey("ResamplerMethod")) format.ResamplerMethod = (HS_Audio_Lib.DSP_RESAMPLER)Enum.Parse(typeof(HS_Audio_Lib.DSP_RESAMPLER), b["ResamplerMethod"]); } catch { format.ResamplerMethod = _format == null ? HS_Audio_Lib.DSP_RESAMPLER.LINEAR : _format.ResamplerMethod; }
                    try { if (b.ContainsKey("OuputFormat")) format.SoundFormat = (HS_Audio_Lib.SOUND_FORMAT)Enum.Parse(typeof(HS_Audio_Lib.SOUND_FORMAT), b["OuputFormat"]); } catch { format.SoundFormat = _format == null ? (Utility.IsWindowsXP_Higher ? HS_Audio_Lib.SOUND_FORMAT.PCMFLOAT : HS_Audio_Lib.SOUND_FORMAT.PCM16) : _format.SoundFormat; }
                    try { if (b.ContainsKey("OuputType")) format.OutputType = (HS_Audio_Lib.OUTPUTTYPE)Enum.Parse(typeof(HS_Audio_Lib.OUTPUTTYPE), b["OuputType"]); } catch { format.OutputType = _format == null ? HS_Audio_Lib.OUTPUTTYPE.AUTODETECT : _format.OutputType; }
                    try { if (b.ContainsKey("OutputChannel")) format.OutputChannel = Convert.ToInt32(b["OutputChannel"]); } catch { format.OutputChannel = _format == null ? 2 : _format.OutputChannel; }
                    try { if (b.ContainsKey("InputChannelMax")) format.MaxInputChannel = Convert.ToInt32(b["InputChannelMax"]); } catch { format.MaxInputChannel = _format == null ? 6 : _format.MaxInputChannel; }
                    return format;
                }
                else { MessageBox.Show(OutputTypeFilePath + " 파일이 존재하지 않습니다.", Text, MessageBoxButtons.OK, MessageBoxIcon.Error); return null; }
            }
            catch (Exception ex) { MessageBox.Show(ex.ToString(), Text); return null; }
        }
    }
}
